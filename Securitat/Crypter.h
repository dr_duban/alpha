/*
** safe-fail securitat
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

extern "C"
namespace Crypt {
	// the only suported mode here is CBC for block cypher
	// because of CBC mode, each key must have a size key_size + cypher_block_size, and cypher_block_size must be initialized to 0
/*
	modes:
		ECB		- raw encryption
		CBC		- each new message block is xored with previous cipher block (first block is xored with IV)
		PCBC	- each new message block is xored with previous message block and previous cypher block
		CFB		- each new cypher block is xored with new message block, and is the input of the new coding step (first step is to code IV)
		OFB		- each new cypher block is xored with new message block, cypher block before XOR is the input of the new coding step (first step is to code IV)
		CTR		- cypher block is produced by xoring text messsage block with cypher block of a counter (counter must have a very long period)
*/


// aes cypher_block_size - 16
	void forward_aes128(char * message, unsigned int mlength, char * ckey, char * xmessage);
	void reverse_aes128(char * xmessage, unsigned int xmlength, char * ckey, char * message);

	void forward_aes192(char * message, unsigned int mlength, char * ckey, char * xmessage);
	void reverse_aes192(char * xmessage, unsigned int xmlength, char * ckey, char * message);

	void forward_aes256(char * message, unsigned int mlength, char * ckey, char * xmessage);
	void reverse_aes256(char * xmessage, unsigned int xmlength, char * ckey, char * message);

	extern void forward_aes128_SSSE3(char * message, unsigned int mlength, char * ckey, char * xmessage);
	extern void reverse_aes128_SSSE3(char * xmessage, unsigned int xmlength, char * ckey, char * message);

	extern void forward_aes256_SSSE3(char * message, unsigned int mlength, char * ckey, char * xmessage);
	extern void reverse_aes256_SSSE3(char * xmessage, unsigned int xmlength, char * ckey, char * message);


// des cypher_block_size - 8
	extern void forward_DES(char * message, unsigned int mlength, char * ckey, char * xmessage);
	extern void reverse_DES(char * xmessage, unsigned int mlength, char * ckey, char * message);

	extern void forward_TDES(char * message, unsigned int mlength, char * ckey, char * xmessage);
	extern void reverse_TDES(char * xmessage, unsigned int mlength, char * ckey, char * message);



// hash-funcs
	void ADLER_32(unsigned int & hash, const unsigned char * meesage, unsigned int mlength);
	void ADLER_32s(unsigned int & hash, const unsigned char * meesage, unsigned int mlength);

	void CRC_32(unsigned int & hash, const unsigned char * message, unsigned int mlength);
	void CRC_32(unsigned int & hash, const unsigned char * message, unsigned int mlength);
	void CRC_32s(unsigned int & hash, const unsigned char * message, unsigned int mlength);
	void CRC_16(unsigned short & hash, const unsigned char * message, unsigned int mlength, int selector = 0);	
	void CRC_16s(unsigned short & hash, const unsigned char * message, unsigned int mlength);

	extern void sha256_SSSE3(unsigned int * hash, const char * message, unsigned int mlength, unsigned int);
	void sha256(unsigned int * hash, const char * message, unsigned int mlength, unsigned int);

	void md5(unsigned int * hash, const char * message, unsigned int mlength, bool);
	void slow_md5(unsigned int * hash, const char * message, unsigned int mlength, bool);

	extern void sha512_SSSE3(unsigned long long * hash, const char * message, unsigned long long mlength, unsigned int);
	void sha512(unsigned long long * hash, const char * message, unsigned long long mlength, unsigned int);

	
}