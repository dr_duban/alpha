/*
** safe-fail securitat
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Securitat\Crypter.h"
#include "System\SysUtils.h"

extern "C" const unsigned char forward_map;
extern "C" const unsigned char reverse_map;
extern "C" const unsigned int zombie_val;


void aes_key_expantion(unsigned int * aes_expanded_key, const char * ckey, int klength, int rcount) {
	unsigned int temp(0), factor(1), remi(0);
	const unsigned int * okey = reinterpret_cast<const unsigned int *>(ckey);
	unsigned char * ekey = reinterpret_cast<unsigned char *>(&temp);
	const unsigned char * fmap = &forward_map;

	rcount <<= 2;

	for (int i(0);i<klength;i++) aes_expanded_key[i] = okey[i];

	for (int i(0);i<rcount;i++) {
		temp = aes_expanded_key[i + klength - 1];

		remi = i % klength;
		if (remi == 0) {
			temp = (temp>>8) | (temp<<24);		

			ekey[0] = fmap[ekey[0]];
			ekey[1] = fmap[ekey[1]];
			ekey[2] = fmap[ekey[2]];
			ekey[3] = fmap[ekey[3]];

			if (i) factor <<= 1;
			if (factor>255) factor ^= 0x11B;

			temp ^= factor;
		} else {
			if ((klength>6) && (remi == 4)) {
				ekey[0] = fmap[ekey[0]];
				ekey[1] = fmap[ekey[1]];
				ekey[2] = fmap[ekey[2]];
				ekey[3] = fmap[ekey[3]];
			}
		}

		aes_expanded_key[i + klength] = temp ^ aes_expanded_key[i];

	}
}

void aes_forward_state(unsigned int * aes_expanded_key, unsigned char * state, char * xmess, int rcount) {
	const unsigned char * fmap = &forward_map;
	unsigned int * block = reinterpret_cast<unsigned int *>(state);
	unsigned int * tempb = aes_expanded_key + 112;
	unsigned char * txcc = reinterpret_cast<unsigned char *>(tempb);
	unsigned char tempc;

	block[0] ^= aes_expanded_key[0];
	block[1] ^= aes_expanded_key[1];
	block[2] ^= aes_expanded_key[2];
	block[3] ^= aes_expanded_key[3];
		

	for (int i(1);i<rcount;i++) {
// s_box
		for (int j(0);j<16;j++) state[j] = fmap[state[j]];
	
// s_row
		tempc = state[1];
		for (int j(0);j<3;j++) state[1+j*4] = state[1+(j+1)*4];
		state[1+3*4] = tempc;

		tempc = state[2];
		state[2] = state[2+2*4];
		state[2+2*4] = tempc;
		tempc = state[2+4];
		state[2+4] = state[2+3*4];
		state[2+3*4] = tempc;

		tempc = state[3+3*4];
		for (int j(2);j>=0;j--) state[3+(j+1)*4] = state[3+j*4];
		state[3] = tempc;

// m_col
		for (int k(0);k<4;k++) {
			tempb[0] = block[k];
			for (int l(0);l<4;l++) {

				tempb[4] = txcc[l];
				tempb[4] <<= 1;
				if (tempb[4]>255) tempb[4] ^= 0x11B;
				
				tempb[5] = txcc[(l+1)%4];
				tempb[5] <<= 1;
				if (tempb[5]>255) tempb[5] ^= 0x11B;
				tempb[5] ^= txcc[(l+1)%4];

				state[k*4+l] = tempb[4] ^ tempb[5] ^ txcc[(l+2)%4] ^ txcc[(l+3)%4];
			}

		}

// add
		block[0] ^= aes_expanded_key[4*i];
		block[1] ^= aes_expanded_key[4*i+1];
		block[2] ^= aes_expanded_key[4*i+2];
		block[3] ^= aes_expanded_key[4*i+3];
		
	}

	for (int j(0);j<16;j++) state[j] = fmap[state[j]];
	
	tempc = state[1];
	for (int j(0);j<3;j++) state[1+j*4] = state[1+(j+1)*4];
	state[1+3*4] = tempc;

	tempc = state[2];
	state[2] = state[2+2*4];
	state[2+2*4] = tempc;
	tempc = state[2+4];
	state[2+4] = state[2+3*4];
	state[2+3*4] = tempc;

	tempc = state[3+3*4];
	for (int j(2);j>=0;j--) state[3+(j+1)*4] = state[3+j*4];
	state[3] = tempc;

	block[0] ^= aes_expanded_key[4*rcount];
	block[1] ^= aes_expanded_key[4*rcount+1];
	block[2] ^= aes_expanded_key[4*rcount+2];
	block[3] ^= aes_expanded_key[4*rcount+3];

	for (int j(0);j<16;j++) xmess[j] = state[j];

}


void aes_reverse_state(unsigned int * aes_expanded_key, unsigned char * state, char * xmess, int rcount) {
	const unsigned char * rmap = &reverse_map;
	unsigned int * block = reinterpret_cast<unsigned int *>(state);
	unsigned int * tempb = aes_expanded_key + 112;
	unsigned char * txcc = reinterpret_cast<unsigned char *>(tempb);
	unsigned char tempc;

	block[0] ^= aes_expanded_key[4*rcount];
	block[1] ^= aes_expanded_key[4*rcount+1];
	block[2] ^= aes_expanded_key[4*rcount+2];
	block[3] ^= aes_expanded_key[4*rcount+3];

	--rcount;

	for (int i(rcount);i>0;i--) {
	
		tempc = state[1+3*4];
		for (int j(2);j>=0;j--) state[1+(j+1)*4] = state[1+j*4];
		state[1] = tempc;

		tempc = state[2];
		state[2] = state[2+2*4];
		state[2+2*4] = tempc;
		tempc = state[2+4];
		state[2+4] = state[2+3*4];
		state[2+3*4] = tempc;

		tempc = state[3];
		for (int j(0);j<3;j++) state[3+j*4] = state[3+(j+1)*4];
		state[3+3*4] = tempc;

		for (int j(0);j<16;j++) state[j] = rmap[state[j]];

		block[0] ^= aes_expanded_key[4*i];
		block[1] ^= aes_expanded_key[4*i+1];
		block[2] ^= aes_expanded_key[4*i+2];
		block[3] ^= aes_expanded_key[4*i+3];


		for (int k(0);k<4;k++) {
			tempb[0] = block[k];
			for (int l(0);l<4;l++) {

				tempb[4] = txcc[l];
				tempb[4] <<= 1;
				if (tempb[4]>255) tempb[4] ^= 0x11B;
				tempb[5] = tempb[4];
				tempb[4] <<= 1;
				if (tempb[4]>255) tempb[4] ^= 0x11B;
				tempb[5] ^= tempb[4];
				tempb[4] <<= 1;
				if (tempb[4]>255) tempb[4] ^= 0x11B;
				tempb[4] ^= tempb[5];
				
				tempb[5] = txcc[(l+1)%4];
				tempb[5] <<= 1;
				if (tempb[5]>255) tempb[5] ^= 0x11B;
				tempb[6] = tempb[5] ^ txcc[(l+1)%4];
				tempb[5] <<= 1;
				if (tempb[5]>255) tempb[5] ^= 0x11B;
				tempb[5] <<= 1;
				if (tempb[5]>255) tempb[5] ^= 0x11B;
				tempb[5] ^= tempb[6];

				tempb[6] = txcc[(l+2)%4];
				tempb[6] <<= 1;
				if (tempb[6]>255) tempb[6] ^= 0x11B;
				tempb[6] <<= 1;
				if (tempb[6]>255) tempb[6] ^= 0x11B;
				tempb[7] = tempb[6] ^ txcc[(l+2)%4];
				tempb[6] <<= 1;
				if (tempb[6]>255) tempb[6] ^= 0x11B;
				tempb[6] ^= tempb[7];

				tempb[7] = txcc[(l+3)%4];
				tempb[7] <<= 1;
				if (tempb[7]>255) tempb[7] ^= 0x11B;
				tempb[7] <<= 1;
				if (tempb[7]>255) tempb[7] ^= 0x11B;
				tempb[7] <<= 1;
				if (tempb[7]>255) tempb[7] ^= 0x11B;
				tempb[7] = tempb[7] ^ txcc[(l+3)%4];


				state[k*4+l] = tempb[4] ^ tempb[5] ^ tempb[6] ^ tempb[7];
			}

		}

		
	}

	tempc = state[1+3*4];
	for (int j(2);j>=0;j--) state[1+(j+1)*4] = state[1+j*4];
	state[1] = tempc;

	tempc = state[2];
	state[2] = state[2+2*4];
	state[2+2*4] = tempc;
	tempc = state[2+4];
	state[2+4] = state[2+3*4];
	state[2+3*4] = tempc;

	tempc = state[3];
	for (int j(0);j<3;j++) state[3+j*4] = state[3+(j+1)*4];
	state[3+3*4] = tempc;

	for (int j(0);j<16;j++) state[j] = rmap[state[j]];

	block[0] ^= aes_expanded_key[0];
	block[1] ^= aes_expanded_key[1];
	block[2] ^= aes_expanded_key[2];
	block[3] ^= aes_expanded_key[3];

	for (int j(0);j<16;j++) xmess[j] = state[j];

}

// aes ==============================================================================================================================================================================================


void Crypt::forward_aes128(char * message, unsigned int mlength, char * ckey, char * xmessage) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];
	unsigned int mcount(mlength >> 4), leftover(mlength & 15);

// iv setup	
	
	if (ckey[17] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount+(leftover>0);
		i_vector[1] = ~i_vector[0];
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+3)[0];

		if (i_vector[0]>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(i_vector[0]<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		
		reinterpret_cast<unsigned int *>(ckey+16)[0] = -1;
		for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(ckey+20)[i] = i_vector[12+2*i];
		
	}

// coding
	aes_key_expantion(aes_expanded_key, ckey, 4, 10);

	for (unsigned int i(0);i<mcount;i++) {
		for (int j(0);j<16;j++) state[j] = message[i*16+j] ^ ckey[20+j];
		aes_forward_state(aes_expanded_key, state, xmessage+i*16, 10);
		for (int j(0);j<16;j++) ckey[20+j] = xmessage[i*16+j];
	}
	
	if (leftover) {
		for (unsigned int j(0);j<leftover;j++) state[j] = message[mcount*16+j] ^ ckey[20+j];
		aes_forward_state(aes_expanded_key, state, xmessage+mcount*16, 10);
		for (int j(0);j<16;j++) ckey[20+j] = xmessage[mcount*16+j];
	}
	

}

void Crypt::reverse_aes128(char * xmessage, unsigned int xmlength, char * ckey, char * message) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];	
	unsigned int mcount((xmlength+15) >> 4);

	aes_key_expantion(aes_expanded_key, ckey, 4, 10);

	for (int j(0);j<16;j++) {
		reinterpret_cast<char *>(i_vector)[j] = state[j] = xmessage[j];
	}
	aes_reverse_state(aes_expanded_key, state, message, 10);

	for (unsigned int i(1);i<mcount;i++) {
		for (int j(0);j<16;j++) {
			reinterpret_cast<char *>(i_vector+(i & 1)*16)[j] = state[j] = xmessage[i*16+j];
		}
		aes_reverse_state(aes_expanded_key, state, message+i*16, 10);
		for (unsigned int m(0);m<4;m++) reinterpret_cast<unsigned int *>(message+i*16)[m] ^= i_vector[(i ^ 1)*4+m];
	}

// first block recover
	if (ckey[17] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount;
		i_vector[1] = ~mcount;
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+3)[0];

		if (mcount>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(mcount<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		reinterpret_cast<unsigned int *>(ckey+16)[0] = -1;
		for (unsigned int i(0);i<4;i++) {
			reinterpret_cast<unsigned int *>(ckey+20)[i] = i_vector[((mcount-1)&1)*4+i];
			i_vector[i] = i_vector[12+2*i];

		}
	} else {
		for (int j(0);j<4;j++) {
			i_vector[12] = i_vector[((mcount-1)&1)*4+j];
			i_vector[j] = reinterpret_cast<unsigned int *>(ckey+20)[j];
			reinterpret_cast<unsigned int *>(ckey+20)[j] = i_vector[12];
		}
		
	}

	for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(message)[i] ^= i_vector[i];
}

void Crypt::forward_aes192(char * message, unsigned int mlength, char * ckey, char * xmessage) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];
	unsigned int mcount(mlength >> 4), leftover(mlength & 15);

// iv setup	
	
	if (ckey[25] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount+(leftover>0);
		i_vector[1] = ~i_vector[0];
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+3)[0];

		if (i_vector[0]>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(i_vector[0]<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		
		reinterpret_cast<unsigned int *>(ckey+24)[0] = -1;
		for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(ckey+28)[i] = i_vector[12+2*i];
		
	}

// coding
	aes_key_expantion(aes_expanded_key, ckey, 6, 12);

	for (unsigned int i(0);i<mcount;i++) {
		for (int j(0);j<16;j++) state[j] = message[i*16+j] ^ ckey[28+j];
		aes_forward_state(aes_expanded_key, state, xmessage+i*16, 12);
		for (int j(0);j<16;j++) ckey[28+j] = xmessage[i*16+j];
	}
	
	if (leftover) {
		for (unsigned int j(0);j<leftover;j++) state[j] = message[mcount*16+j] ^ ckey[28+j];
		aes_forward_state(aes_expanded_key, state, xmessage+mcount*16, 12);
		for (int j(0);j<16;j++) ckey[28+j] = xmessage[mcount*16+j];
	}
	

}

void Crypt::reverse_aes192(char * xmessage, unsigned int xmlength, char * ckey, char * message) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];	
	unsigned int mcount((xmlength+15) >> 4);

	aes_key_expantion(aes_expanded_key, ckey, 6, 12);

	for (int j(0);j<16;j++) {
		reinterpret_cast<char *>(i_vector)[j] = state[j] = xmessage[j];
	}
	aes_reverse_state(aes_expanded_key, state, message, 12);

	for (unsigned int i(1);i<mcount;i++) {
		for (int j(0);j<16;j++) {
			reinterpret_cast<char *>(i_vector+(i & 1)*16)[j] = state[j] = xmessage[i*16+j];
		}
		aes_reverse_state(aes_expanded_key, state, message+i*16, 12);
		for (unsigned int m(0);m<4;m++) reinterpret_cast<unsigned int *>(message+i*16)[m] ^= i_vector[(i ^ 1)*4+m];
	}

// first block recover
	if (ckey[25] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount;
		i_vector[1] = ~mcount;
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+3)[0];

		if (mcount>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(mcount<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		reinterpret_cast<unsigned int *>(ckey+24)[0] = -1;
		for (unsigned int i(0);i<4;i++) {
			reinterpret_cast<unsigned int *>(ckey+28)[i] = i_vector[((mcount-1)&1)*4+i];
			i_vector[i] = i_vector[12+2*i];

		}
	} else {
		for (int j(0);j<4;j++) {
			i_vector[12] = i_vector[((mcount-1)&1)*4+j];
			i_vector[j] = reinterpret_cast<unsigned int *>(ckey+28)[j];
			reinterpret_cast<unsigned int *>(ckey+28)[j] = i_vector[12];
		}
		
	}

	for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(message)[i] ^= i_vector[i];
}

void Crypt::forward_aes256(char * message, unsigned int mlength, char * ckey, char * xmessage) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];
	unsigned int mcount(mlength >> 4), leftover(mlength & 15);

// iv setup	
	
	if (ckey[33] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount+(leftover>0);
		i_vector[1] = ~i_vector[0];
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+13)[0];

		if (i_vector[0]>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(i_vector[0]<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		
		reinterpret_cast<unsigned int *>(ckey+32)[0] = -1;
		for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(ckey+36)[i] = i_vector[12+2*i];
		
	}

// coding
	aes_key_expantion(aes_expanded_key, ckey, 8, 14);

	for (unsigned int i(0);i<mcount;i++) {
		for (int j(0);j<16;j++) state[j] = message[i*16+j] ^ ckey[36+j];
		aes_forward_state(aes_expanded_key, state, xmessage+i*16, 14);
		for (int j(0);j<16;j++) ckey[36+j] = xmessage[i*16+j];
	}
	
	if (leftover) {
		for (unsigned int j(0);j<leftover;j++) state[j] = message[mcount*16+j] ^ ckey[36+j];
		aes_forward_state(aes_expanded_key, state, xmessage+mcount*16, 14);
		for (int j(0);j<16;j++) ckey[36+j] = xmessage[mcount*16+j];
	}
	

}

void Crypt::reverse_aes256(char * xmessage, unsigned int xmlength, char * ckey, char * message) {
	unsigned int aes_expanded_key[128];
	unsigned int i_vector[20];
	unsigned char state[16];	
	unsigned int mcount((xmlength+15) >> 4);

	aes_key_expantion(aes_expanded_key, ckey, 8, 14);

	for (int j(0);j<16;j++) {
		reinterpret_cast<char *>(i_vector)[j] = state[j] = xmessage[j];
	}
	aes_reverse_state(aes_expanded_key, state, message, 14);

	for (unsigned int i(1);i<mcount;i++) {
		for (int j(0);j<16;j++) {
			reinterpret_cast<char *>(i_vector+(i & 1)*16)[j] = state[j] = xmessage[i*16+j];
		}
		aes_reverse_state(aes_expanded_key, state, message+i*16, 14);
		for (unsigned int m(0);m<4;m++) reinterpret_cast<unsigned int *>(message+i*16)[m] ^= i_vector[(i ^ 1)*4+m];
	}

// first block recover
	if (ckey[33] == 0) {
		for (unsigned int i(0);i<12;i++) i_vector[i] = 0;
		i_vector[0] = mcount;
		i_vector[1] = ~mcount;
		i_vector[3] = reinterpret_cast<unsigned int *>(ckey+13)[0];

		if (mcount>4) {
			for (unsigned int i(0);i<4;i++) i_vector[4+2*i] = reinterpret_cast<unsigned int *>(message+16)[i];
			for (unsigned int i(0);i<4;i++) i_vector[5+2*i] = reinterpret_cast<unsigned int *>(message+(mcount<<3))[i];
		}
		sha256(i_vector+12, reinterpret_cast<const char *>(i_vector), 47, 0);
		reinterpret_cast<unsigned int *>(ckey+32)[0] = -1;
		for (unsigned int i(0);i<4;i++) {
			reinterpret_cast<unsigned int *>(ckey+36)[i] = i_vector[((mcount-1)&1)*4+i];
			i_vector[i] = i_vector[12+2*i];

		}
	} else {
		for (int j(0);j<4;j++) {
			i_vector[12] = i_vector[((mcount-1)&1)*4+j];
			i_vector[j] = reinterpret_cast<unsigned int *>(ckey+36)[j];
			reinterpret_cast<unsigned int *>(ckey+36)[j] = i_vector[12];
		}
		
	}

	for (unsigned int i(0);i<4;i++) reinterpret_cast<unsigned int *>(message)[i] ^= i_vector[i];
}

// TDES ================================================================================================================================================================

void Crypt::forward_TDES(char * message, unsigned int mlength, char * ckey, char * xmessage) {
	forward_DES(message, mlength, ckey, xmessage);
	
	mlength += 7; mlength &= (~7);
	forward_DES(xmessage, mlength, ckey+16, xmessage);
	forward_DES(xmessage, mlength, ckey+32, xmessage);
}


void Crypt::reverse_TDES(char * xmessage, unsigned int mlength, char * ckey, char * message) {
	reverse_DES(xmessage, mlength, ckey+32, message);	
	reverse_DES(message, mlength, ckey+16, message);
	reverse_DES(message, mlength, ckey, message);
}


