
; safe-fail securitat
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



EXTERN	sha256_SSSE3:PROC

PUBLIC forward_map, reverse_map

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
ALIGN 16

forward_map		BYTE	063h, 07ch, 077h, 07bh, 0f2h, 06bh, 06fh, 0c5h, 030h, 001h, 067h, 02bh, 0feh, 0d7h, 0abh, 076h
				BYTE	0cah, 082h, 0c9h, 07dh, 0fah, 059h, 047h, 0f0h, 0adh, 0d4h, 0a2h, 0afh, 09ch, 0a4h, 072h, 0c0h
				BYTE	0b7h, 0fdh, 093h, 026h, 036h, 03fh, 0f7h, 0cch, 034h, 0a5h, 0e5h, 0f1h, 071h, 0d8h, 031h, 015h
				BYTE	004h, 0c7h, 023h, 0c3h, 018h, 096h, 005h, 09ah, 007h, 012h, 080h, 0e2h, 0ebh, 027h, 0b2h, 075h
				BYTE	009h, 083h, 02ch, 01ah, 01bh, 06eh, 05ah, 0a0h, 052h, 03bh, 0d6h, 0b3h, 029h, 0e3h, 02fh, 084h
				BYTE	053h, 0d1h, 000h, 0edh, 020h, 0fch, 0b1h, 05bh, 06ah, 0cbh, 0beh, 039h, 04ah, 04ch, 058h, 0cfh
				BYTE	0d0h, 0efh, 0aah, 0fbh, 043h, 04dh, 033h, 085h, 045h, 0f9h, 002h, 07fh, 050h, 03ch, 09fh, 0a8h
				BYTE	051h, 0a3h, 040h, 08fh, 092h, 09dh, 038h, 0f5h, 0bch, 0b6h, 0dah, 021h, 010h, 0ffh, 0f3h, 0d2h
				BYTE	0cdh, 00ch, 013h, 0ech, 05fh, 097h, 044h, 017h, 0c4h, 0a7h, 07eh, 03dh, 064h, 05dh, 019h, 073h
				BYTE	060h, 081h, 04fh, 0dch, 022h, 02ah, 090h, 088h, 046h, 0eeh, 0b8h, 014h, 0deh, 05eh, 00bh, 0dbh
				BYTE	0e0h, 032h, 03ah, 00ah, 049h, 006h, 024h, 05ch, 0c2h, 0d3h, 0ach, 062h, 091h, 095h, 0e4h, 079h
				BYTE	0e7h, 0c8h, 037h, 06dh, 08dh, 0d5h, 04eh, 0a9h, 06ch, 056h, 0f4h, 0eah, 065h, 07ah, 0aeh, 008h
				BYTE	0bah, 078h, 025h, 02eh, 01ch, 0a6h, 0b4h, 0c6h, 0e8h, 0ddh, 074h, 01fh, 04bh, 0bdh, 08bh, 08ah
				BYTE	070h, 03eh, 0b5h, 066h, 048h, 003h, 0f6h, 00eh, 061h, 035h, 057h, 0b9h, 086h, 0c1h, 01dh, 09eh
				BYTE	0e1h, 0f8h, 098h, 011h, 069h, 0d9h, 08eh, 094h, 09bh, 01eh, 087h, 0e9h, 0ceh, 055h, 028h, 0dfh
				BYTE	08ch, 0a1h, 089h, 00dh, 0bfh, 0e6h, 042h, 068h, 041h, 099h, 02dh, 00fh, 0b0h, 054h, 0bbh, 016h

reverse_map		BYTE	052h, 009h, 06ah, 0d5h, 030h, 036h, 0a5h, 038h, 0bfh, 040h, 0a3h, 09eh, 081h, 0f3h, 0d7h, 0fbh
				BYTE	07ch, 0e3h, 039h, 082h, 09bh, 02fh, 0ffh, 087h, 034h, 08eh, 043h, 044h, 0c4h, 0deh, 0e9h, 0cbh
				BYTE	054h, 07bh, 094h, 032h, 0a6h, 0c2h, 023h, 03dh, 0eeh, 04ch, 095h, 00bh, 042h, 0fah, 0c3h, 04eh
				BYTE	008h, 02eh, 0a1h, 066h, 028h, 0d9h, 024h, 0b2h, 076h, 05bh, 0a2h, 049h, 06dh, 08bh, 0d1h, 025h
				BYTE	072h, 0f8h, 0f6h, 064h, 086h, 068h, 098h, 016h, 0d4h, 0a4h, 05ch, 0cch, 05dh, 065h, 0b6h, 092h
				BYTE	06ch, 070h, 048h, 050h, 0fdh, 0edh, 0b9h, 0dah, 05eh, 015h, 046h, 057h, 0a7h, 08dh, 09dh, 084h
				BYTE	090h, 0d8h, 0abh, 000h, 08ch, 0bch, 0d3h, 00ah, 0f7h, 0e4h, 058h, 005h, 0b8h, 0b3h, 045h, 006h
				BYTE	0d0h, 02ch, 01eh, 08fh, 0cah, 03fh, 00fh, 002h, 0c1h, 0afh, 0bdh, 003h, 001h, 013h, 08ah, 06bh
				BYTE	03ah, 091h, 011h, 041h, 04fh, 067h, 0dch, 0eah, 097h, 0f2h, 0cfh, 0ceh, 0f0h, 0b4h, 0e6h, 073h
				BYTE	096h, 0ach, 074h, 022h, 0e7h, 0adh, 035h, 085h, 0e2h, 0f9h, 037h, 0e8h, 01ch, 075h, 0dfh, 06eh
				BYTE	047h, 0f1h, 01ah, 071h, 01dh, 029h, 0c5h, 089h, 06fh, 0b7h, 062h, 00eh, 0aah, 018h, 0beh, 01bh
				BYTE	0fch, 056h, 03eh, 04bh, 0c6h, 0d2h, 079h, 020h, 09ah, 0dbh, 0c0h, 0feh, 078h, 0cdh, 05ah, 0f4h
				BYTE	01fh, 0ddh, 0a8h, 033h, 088h, 007h, 0c7h, 031h, 0b1h, 012h, 010h, 059h, 027h, 080h, 0ech, 05fh
				BYTE	060h, 051h, 07fh, 0a9h, 019h, 0b5h, 04ah, 00dh, 02dh, 0e5h, 07ah, 09fh, 093h, 0c9h, 09ch, 0efh
				BYTE	0a0h, 0e0h, 03bh, 04dh, 0aeh, 02ah, 0f5h, 0b0h, 0c8h, 0ebh, 0bbh, 03ch, 083h, 053h, 099h, 061h
				BYTE	017h, 02bh, 004h, 07eh, 0bah, 077h, 0d6h, 026h, 0e1h, 069h, 014h, 063h, 055h, 021h, 00ch, 07dh

ALIGN 16

shuf_map		DWORD	0C080400h, 0D090501h, 0E0A0602h, 0F0B0703h
high_mask		DWORD	0FEFEFEFEh, 0FEFEFEFEh, 0FEFEFEFEh, 0FEFEFEFEh
low_mask		DWORD	01010101h, 01010101h, 01010101h, 01010101h
oflow_mask		DWORD	01B1B1B1Bh, 01B1B1B1Bh, 01B1B1B1Bh, 01B1B1B1Bh

prog_aes128 SEGMENT PAGE EXECUTE


ALIGN 16
forward_aes128_SSSE3	PROC
				PUSH r15
				PUSH r12
				PUSH rsi
				PUSH rdi
				PUSH rbx
				MOV r15, rsp
					SUB rsp, 320
					
;		TEST r9, r9
;		CMOVZ r9, rcx

						MOV r10, rcx

						MOV eax, edx
						XOR edx, edx
						MOV ecx, 16						
						DIV ecx

						MOV r11d, eax

						SHR rsp, 4
						SHL rsp, 4

						XOR r12, r12
						CMP r12d, [r8+16]
						JNZ no_vi
							MOV [rsp+32], r11d
							MOV [rsp+36], edx
							MOV [rsp+40], r10
							MOV [rsp+48], r9
							MOV [rsp+56], r8

							MOV [rsp+64], r12
							MOV [rsp+72], r12
							MOV [rsp+80], r12
							MOV [rsp+88], r12
							MOV [rsp+96], r12
							MOV [rsp+104], r12

							TEST edx, edx
							JZ no_edinc
								INC eax
							no_edinc:

							MOV [rsp+64], eax
							NOT eax
							MOV [rsp+68], eax
							NOT eax
							MOV edx, [r8+3]
							MOV [rsp+76], edx

							CMP eax, 4
							JBE no_mloads
								MOV edx, [r10+16]
								MOV [rsp+80], edx
								MOV edx, [r10+20]
								MOV [rsp+88], edx
								MOV edx, [r10+24]
								MOV [rsp+96], edx
								MOV edx, [r10+28]
								MOV [rsp+104], edx

								SHL rax, 3

								MOV edx, [r10+rax]
								MOV [rsp+84], edx
								MOV edx, [r10+rax+4]
								MOV [rsp+92], edx
								MOV edx, [r10+rax+8]
								MOV [rsp+100], edx
								MOV edx, [r10+rax+12]
								MOV [rsp+108], edx
						align 16
							no_mloads:

							LEA rcx, [rsp+112]
							LEA rdx, [rsp+64]
							MOV r8d, 47
							XOR r9, r9

							CALL sha256_SSSE3

							MOV r11d, [rsp+32]
							MOV edx, [rsp+36]
							MOV r10, [rsp+40]
							MOV r9, [rsp+48]
							MOV r8, [rsp+56]

							MOV DWORD PTR [r8+16], 0FFFFFFFFh

							MOV eax, [rsp+112]
							MOV [r8+20], eax
							MOV eax, [rsp+120]
							MOV [r8+24], eax
							MOV eax, [rsp+128]
							MOV [r8+28], eax
							MOV eax, [rsp+136]
							MOV [r8+32], eax

						no_vi:
						
						MOVDQU xmm1, [r8]
						MOVDQU xmm13, [r8+20]

						MOVDQA xmm8, XMMWORD PTR [shuf_map]
						MOVDQA xmm12, XMMWORD PTR [high_mask]
						MOVDQA xmm10, XMMWORD PTR [low_mask]
						MOVDQA xmm15, XMMWORD PTR [oflow_mask]

						
						MOV eax, [r8+12]
						MOV r12, rsp						

						MOV cl, 1
						MOV ch, 10
						
						LEA rbx, [forward_map]

align 16
						keyexpansionloop:
							
							MOVDQA xmm2, xmm1
							PSHUFB xmm2, xmm8
						
							MOVDQA [r12], xmm2
							MOVDQA [r12+16], xmm1
							
							ROR eax, 8
							ADD r12, 16
							
							XLATB
							XOR al, cl
							ROR eax, 8 

							SHL cl, 1
							JNC nocladjust
								XOR cl, 1Bh
							nocladjust:

							XLATB
							ROR eax, 8 
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8 
														
							
							XOR eax, [r12]
							MOV [r12], eax
							XOR eax, [r12+4]
							MOV [r12+4], eax
							XOR eax, [r12+8]
							MOV [r12+8], eax
							XOR eax, [r12+12]
							MOV [r12+12], eax
							
							MOVDQA xmm1, [r12]							

						DEC ch
						JNZ keyexpansionloop

						PSHUFB xmm1, xmm8
						MOVDQA [r12], xmm1
												
						
align 16
						messageloop:

							MOVDQU xmm0, [r10]
							PXOR xmm0, xmm13

							PSHUFB xmm0, xmm8
							ADD r10, 16

							MOV r12, rsp

							MOV ecx, 10

align 16
							encryptionloop:

								PXOR xmm0, [r12]								

								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8

								MOVD xmm4, eax
								
								PSRLDQ xmm0, 4
								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 16

								MOVD xmm5, eax

								PSRLDQ xmm0, 4
								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 8

								MOVD xmm6, eax
								
								PSRLDQ xmm0, 4
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB

								MOVD xmm0, eax
							
								
								PSLLDQ xmm0, 12
								PSLLDQ xmm6, 8

								POR xmm0, xmm6
								PSLLDQ xmm5, 4

								POR xmm5, xmm4
								POR xmm0, xmm5
																
								ADD r12, 16

								DEC ecx
								JZ storethecode
									
									MOVDQA xmm5, xmm0
									MOVDQA xmm4, xmm0
									
									PSLLD xmm4, 1
									PAND xmm4, xmm12

									PSRLD xmm5, 7									
									PAND xmm5, xmm10
									
									MOVDQA xmm6, xmm0

									PXOR xmm7, xmm7
									PSUBSB xmm7, xmm5
									PAND xmm7, xmm15

									PSHUFD xmm6, xmm6, 4Eh

									PXOR xmm7, xmm4							
									MOVDQA xmm4, xmm7

									PXOR xmm7, xmm0
														
									PSHUFD xmm7, xmm7, 39h									
									PXOR xmm4, xmm7
									PSHUFD xmm0, xmm0, 93h
																		
									PXOR xmm0, xmm4									
									PXOR xmm0, xmm6
									

							JMP encryptionloop
align 16
							storethecode:
								PXOR xmm0, [r12]
								PSHUFB xmm0, xmm8
								
								MOVDQA xmm13, xmm0
								MOVDQU [r9], xmm0								

								ADD r9, 16

						DEC r11d
						JNZ messageloop

						TEST edx, edx
						JZ noextraloops
							MOV ecx, edx
							MOV edx, 0
							MOV r11d, 1

							LEA rdi, [rsp+176]

							MOV rsi, r10
							MOV r10, rdi							
							
							REP MOVSB

							JMP messageloop
						noextraloops:
						

					MOVDQU [r8+20], xmm13

					MOV rsp, r15
					POP rbx
					POP rdi
					POP rsi
					POP r12
					POP r15
				RET
forward_aes128_SSSE3	ENDP

ALIGN 16
reverse_aes128_SSSE3	PROC
				PUSH r15
				PUSH r13
				PUSH r12

				PUSH rbx	
				MOV r15, rsp
					SUB rsp, 320
						PXOR xmm13, xmm13
						MOVDQU xmm1, [r8]

						MOV r12, r9

						XOR rax, rax
						CMP eax, [r8+16]
						JZ no_vi
							MOVDQU xmm13, [r8+20]
						no_vi:

						MOV r10, rcx

						ADD edx, 15
						SHR edx, 4
						MOV r13d, edx

						SHR rsp, 4
						SHL rsp, 4

						MOV cl, 1
						MOV ch, 10
						
						LEA r11, [rsp+160]

						MOVDQA xmm8, XMMWORD PTR [shuf_map]
						MOVDQA xmm9, XMMWORD PTR [high_mask]
						MOVDQA xmm10, XMMWORD PTR [low_mask]
						MOVDQA xmm11, XMMWORD PTR [oflow_mask]

						LEA rbx, [forward_map]										
						
						MOV eax, [r8+12]

align 16
						keyexpansionloop:
							MOVDQA xmm2, xmm1
							PSHUFB xmm2, xmm8
							
							MOVDQA [r11], xmm2
							MOVDQA [r11-16], xmm1
														
							ROR eax, 8
							SUB r11, 16

							XLATB
							XOR al, cl
							ROR eax, 8 

							SHL cl, 1
							JNC nocladjust
								XOR cl, 1Bh
							nocladjust:

							XLATB
							ROR eax, 8 
							XLATB
							ROR eax, 8 
							XLATB
							ROR eax, 8 
														
							
							XOR eax, [r11]
							MOV [r11], eax
							XOR eax, [r11+4]
							MOV [r11+4], eax
							XOR eax, [r11+8]
							MOV [r11+8], eax
							XOR eax, [r11+12]
							MOV [r11+12], eax							

							
							MOVDQA xmm1, [r11]							

						DEC ch
						JNZ keyexpansionloop

						PSHUFB xmm1, xmm8
						MOVDQA [r11], xmm1


						LEA rbx, [reverse_map]

						
align 16
						messageloop:

							MOVDQU xmm0, [r10]
							MOVDQA xmm14, xmm0

							PSHUFB xmm0, xmm8
							ADD r10, 16

							MOV r11, rsp

							MOV ecx, 10

							PXOR xmm0, [r11]

align 16
							encryptionloop:
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8

								MOVD xmm4, eax
								
								PSRLDQ xmm0, 4

								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB

								MOVD xmm5, eax

								PSRLDQ xmm0, 4
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 8

								MOVD xmm6, eax
								
								PSRLDQ xmm0, 4

								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 16

								MOVD xmm0, eax
								
								
								PSLLDQ xmm0, 12
								PSLLDQ xmm6, 8

								POR xmm0, xmm6
								PSLLDQ xmm5, 4

								POR xmm5, xmm4
								POR xmm0, xmm5
																			

								ADD r11, 16
								PXOR xmm0, [r11]

								DEC ecx
								JZ storethecode
									
									MOVDQA xmm5, xmm0																		
									MOVDQA xmm4, xmm0

									PSLLD xmm4, 1									
									PAND xmm4, xmm9
									
									PSRLD xmm5, 7
									PAND xmm5, xmm10

									MOVDQA xmm6, xmm0
									MOVDQA xmm1, xmm0
																		
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm5
									
									PAND xmm3, xmm11

									PXOR xmm4, xmm3
									MOVDQA xmm2, xmm4

									PSLLD xmm2, 1
									PAND xmm2, xmm9
									
									PSRLD xmm6, 6
									PAND xmm6, xmm10
									
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm6
									PAND xmm3, xmm11

									PXOR xmm2, xmm3																									
									MOVDQA xmm7, xmm2

									PSLLD xmm7, 1
									PAND xmm7, xmm9

									PSRLD xmm1, 5
									PAND xmm1, xmm10
									
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm1
									PAND xmm3, xmm11

									PXOR xmm7, xmm3
									PXOR xmm0, xmm7
									
									PXOR xmm7, xmm4
									PXOR xmm4, xmm0
									

									PXOR xmm7, xmm2
									PXOR xmm2, xmm0
									
									PSHUFD xmm2, xmm2, 4Eh
									PSHUFD xmm4, xmm4, 39h

									PXOR xmm4, xmm2
									PSHUFD xmm0, xmm0, 93h

									PXOR xmm0, xmm7									
									PXOR xmm0, xmm4

							JMP encryptionloop
align 16
							storethecode:								
								PSHUFB xmm0, xmm8
								
								PXOR xmm0, xmm13
								MOVDQU [r9], xmm0

								MOVDQA xmm13, xmm14

								ADD r9, 16

						DEC edx
						JNZ messageloop

					XOR rax, rax
					CMP eax, [r8+16]
					JNZ nofcorrection

							MOV [rsp+64], rax
							MOV [rsp+72], rax
							MOV [rsp+80], rax
							MOV [rsp+88], rax
							MOV [rsp+96], rax
							MOV [rsp+104], rax


							MOV [rsp+64], r13d
							NOT r13d
							MOV [rsp+68], r13d
							NOT r13d
							MOV edx, [r8+3]
							MOV [rsp+76], edx

							
							CMP r13d, 4
							JBE no_mloads
								MOV edx, [r12+16]
								MOV [rsp+80], edx
								MOV edx, [r12+20]
								MOV [rsp+88], edx
								MOV edx, [r12+24]
								MOV [rsp+96], edx
								MOV edx, [r12+28]
								MOV [rsp+104], edx

								SHL r13, 3

								MOV edx, [r12+r13]
								MOV [rsp+84], edx
								MOV edx, [r12+r13+4]
								MOV [rsp+92], edx
								MOV edx, [r12+r13+8]
								MOV [rsp+100], edx
								MOV edx, [r12+r13+12]
								MOV [rsp+108], edx
						align 16
							no_mloads:

							MOV r13, r8

							LEA rcx, [rsp+112]
							LEA rdx, [rsp+64]
							MOV r8d, 47
							XOR r9, r9

							CALL sha256_SSSE3

							MOV r8, r13

							MOV DWORD PTR [r8+16], 0FFFFFFFFh

							MOV eax, [rsp+112]
							MOV [r8+20], eax
							MOV eax, [rsp+120]
							MOV [r8+24], eax
							MOV eax, [rsp+128]
							MOV [r8+28], eax
							MOV eax, [rsp+136]
							MOV [r8+32], eax

							MOVDQU xmm14, [r8+20]
							MOVDQU xmm0, [r12]
							PXOR xmm0, xmm14
							MOVDQU [r12], xmm0
					nofcorrection:
					MOVDQU [r8+20], xmm13

					MOV rsp, r15
					POP rbx
					POP r12
					POP r13
					POP r15
				RET

reverse_aes128_SSSE3		ENDP


ALIGN 16
forward_aes256_SSSE3	PROC
				PUSH r15
				PUSH r12
				PUSH rsi
				PUSH rdi
				PUSH rbx
				MOV r15, rsp
					SUB rsp, 320

						
	;			TEST r9, r9
	;			CMOVZ r9, rcx

						MOV r10, rcx

						MOV eax, edx
						XOR edx, edx
						MOV ecx, 16						
						DIV ecx

						MOV r11d, eax

						SHR rsp, 4
						SHL rsp, 4


						XOR r12, r12
						CMP r12d, [r8+32]
						JNZ no_vi
							MOV [rsp+32], r11d
							MOV [rsp+36], edx
							MOV [rsp+40], r10
							MOV [rsp+48], r9
							MOV [rsp+56], r8

							MOV [rsp+64], r12
							MOV [rsp+72], r12
							MOV [rsp+80], r12
							MOV [rsp+88], r12
							MOV [rsp+96], r12
							MOV [rsp+104], r12

							TEST edx, edx
							JZ no_edinc
								INC eax
							no_edinc:

							MOV [rsp+64], eax
							NOT eax
							MOV [rsp+68], eax
							NOT eax
							MOV edx, [r8+13]
							MOV [rsp+76], edx

							CMP eax, 4
							JBE no_mloads
								MOV edx, [r10+16]
								MOV [rsp+80], edx
								MOV edx, [r10+20]
								MOV [rsp+88], edx
								MOV edx, [r10+24]
								MOV [rsp+96], edx
								MOV edx, [r10+28]
								MOV [rsp+104], edx

								SHL rax, 3

								MOV edx, [r10+rax]
								MOV [rsp+84], edx
								MOV edx, [r10+rax+4]
								MOV [rsp+92], edx
								MOV edx, [r10+rax+8]
								MOV [rsp+100], edx
								MOV edx, [r10+rax+12]
								MOV [rsp+108], edx
						align 16
							no_mloads:

							LEA rcx, [rsp+112]
							LEA rdx, [rsp+64]
							MOV r8d, 47
							XOR r9, r9

							CALL sha256_SSSE3

							MOV r11d, [rsp+32]
							MOV edx, [rsp+36]
							MOV r10, [rsp+40]
							MOV r9, [rsp+48]
							MOV r8, [rsp+56]

							MOV DWORD PTR [r8+32], 0FFFFFFFFh

							MOV eax, [rsp+112]
							MOV [r8+36], eax
							MOV eax, [rsp+120]
							MOV [r8+40], eax
							MOV eax, [rsp+128]
							MOV [r8+44], eax
							MOV eax, [rsp+136]
							MOV [r8+48], eax

						no_vi:
						
						MOVDQU xmm3, [r8]
						MOVDQU xmm1, [r8+16]

						MOVDQU xmm13, [r8+36]




						MOVDQA xmm8, XMMWORD PTR [shuf_map]
						MOVDQA xmm12, XMMWORD PTR [high_mask]
						MOVDQA xmm10, XMMWORD PTR [low_mask]
						MOVDQA xmm15, XMMWORD PTR [oflow_mask]

						
						MOV eax, [r8+28]
						MOV r12, rsp

						MOV cl, 1
						MOV ch, 7
						
						LEA rbx, [forward_map]

align 16
						keyexpansionloop:
							
							MOVDQA xmm2, xmm3
							PSHUFB xmm2, xmm8
						
							MOVDQA [r12], xmm2
							MOVDQA [r12+16], xmm3
							
							ROR eax, 8
							ADD r12, 16
							
							XLATB
							XOR al, cl
							ROR eax, 8 

							SHL cl, 1
							JNC nocladjust
								XOR cl, 1Bh
							nocladjust:

							XLATB
							ROR eax, 8 
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8 
														
							XOR eax, [r12]
							MOV [r12], eax
							XOR eax, [r12+4]
							MOV [r12+4], eax
							XOR eax, [r12+8]
							MOV [r12+8], eax
							XOR eax, [r12+12]
							MOV [r12+12], eax
							
							MOVDQA xmm3, [r12]							



							MOVDQA xmm2, xmm1
							PSHUFB xmm2, xmm8
						
							MOVDQA [r12], xmm2
							MOVDQA [r12+16], xmm1

							ADD r12, 16
							
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							
							XOR eax, [r12]
							MOV [r12], eax
							XOR eax, [r12+4]
							MOV [r12+4], eax
							XOR eax, [r12+8]
							MOV [r12+8], eax
							XOR eax, [r12+12]
							MOV [r12+12], eax
							
							MOVDQA xmm1, [r12]


						DEC ch
						JNZ keyexpansionloop

						PSHUFB xmm3, xmm8
						MOVDQA [r12], xmm3
												
						
align 16
						messageloop:

							MOVDQU xmm0, [r10]
							PXOR xmm0, xmm13

							PSHUFB xmm0, xmm8
							ADD r10, 16

							MOV r12, rsp

							MOV ecx, 14

align 16
							encryptionloop:

								PXOR xmm0, [r12]								

								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8

								MOVD xmm4, eax
								
								PSRLDQ xmm0, 4
								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 16

								MOVD xmm5, eax

								PSRLDQ xmm0, 4
								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 8

								MOVD xmm6, eax
								
								PSRLDQ xmm0, 4
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB

								MOVD xmm0, eax
							
								
								PSLLDQ xmm0, 12
								PSLLDQ xmm6, 8

								POR xmm0, xmm6
								PSLLDQ xmm5, 4

								POR xmm5, xmm4
								POR xmm0, xmm5
																
								ADD r12, 16

								DEC ecx
								JZ storethecode
									
									MOVDQA xmm5, xmm0
									MOVDQA xmm4, xmm0
									
									PSLLD xmm4, 1
									PAND xmm4, xmm12

									PSRLD xmm5, 7									
									PAND xmm5, xmm10
									
									MOVDQA xmm6, xmm0

									PXOR xmm7, xmm7
									PSUBSB xmm7, xmm5
									PAND xmm7, xmm15

									PSHUFD xmm6, xmm6, 4Eh

									PXOR xmm7, xmm4							
									MOVDQA xmm4, xmm7

									PXOR xmm7, xmm0
														
									PSHUFD xmm7, xmm7, 39h									
									PXOR xmm4, xmm7
									PSHUFD xmm0, xmm0, 93h
																		
									PXOR xmm0, xmm4									
									PXOR xmm0, xmm6
									

							JMP encryptionloop
align 16
							storethecode:
								PXOR xmm0, [r12]
								PSHUFB xmm0, xmm8
								
								MOVDQA xmm13, xmm0
								MOVDQU [r9], xmm0

								ADD r9, 16

						DEC r11d
						JNZ messageloop

						TEST edx, edx
						JZ noextraloops
							MOV ecx, edx
							XOR edx, edx
							MOV r11d, 1

							LEA rdi, [rsp+240]

							MOV rsi, r10
							MOV r10, rdi							
							
							REP MOVSB

							JMP messageloop
						noextraloops:
						
					MOVDQU [r8+36], xmm13

					MOV rsp, r15
					POP rbx
					POP rdi
					POP rsi
					POP r12
					POP r15
				RET


	RET
forward_aes256_SSSE3	ENDP


ALIGN 16
reverse_aes256_SSSE3	PROC
				PUSH r15
				PUSH r13
				PUSH r12
				PUSH rbx	
				MOV r15, rsp
					SUB rsp, 320
						PXOR xmm13, xmm13

						XOR rax, rax
						CMP eax, [r8+32]
						JZ no_vi
							MOVDQU xmm13, [r8+36]
						no_vi:


						MOVDQU xmm3, [r8]
						MOVDQU xmm1, [r8+16]

;		TEST r9, r9
;		CMOVZ r9, rcx
						MOV r12, r9

						MOV r10, rcx

						ADD edx, 15
						SHR edx, 4
						MOV r13d, edx

						SHR rsp, 4
						SHL rsp, 4

						MOV cl, 1
						MOV ch, 7
						
						LEA r11, [rsp+224]

						MOVDQA xmm8, XMMWORD PTR [shuf_map]
						MOVDQA xmm9, XMMWORD PTR [high_mask]
						MOVDQA xmm10, XMMWORD PTR [low_mask]
						MOVDQA xmm11, XMMWORD PTR [oflow_mask]

						LEA rbx, [forward_map]												
						MOV eax, [r8+28]

align 16
						keyexpansionloop:
							MOVDQA xmm2, xmm3
							PSHUFB xmm2, xmm8
						
							MOVDQA [r11], xmm2
							MOVDQA [r11-16], xmm3
							
							ROR eax, 8
							SUB r11, 16
							
							XLATB
							XOR al, cl
							ROR eax, 8 

							SHL cl, 1
							JNC nocladjust
								XOR cl, 1Bh
							nocladjust:

							XLATB
							ROR eax, 8 
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8 
														
							XOR eax, [r11]
							MOV [r11], eax
							XOR eax, [r11+4]
							MOV [r11+4], eax
							XOR eax, [r11+8]
							MOV [r11+8], eax
							XOR eax, [r11+12]
							MOV [r11+12], eax
							
							MOVDQA xmm3, [r11]							



							MOVDQA xmm2, xmm1
							PSHUFB xmm2, xmm8
						
							MOVDQA [r11], xmm2
							MOVDQA [r11-16], xmm1

							SUB r11, 16
							
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							XLATB
							ROR eax, 8
							
							XOR eax, [r11]
							MOV [r11], eax
							XOR eax, [r11+4]
							MOV [r11+4], eax
							XOR eax, [r11+8]
							MOV [r11+8], eax
							XOR eax, [r11+12]
							MOV [r11+12], eax
							
							MOVDQA xmm1, [r11]


						DEC ch
						JNZ keyexpansionloop

						PSHUFB xmm3, xmm8
						MOVDQA [r11], xmm3


						LEA rbx, [reverse_map]

						
align 16
						messageloop:

							MOVDQU xmm0, [r10]
							MOVDQA xmm14, xmm0
							PSHUFB xmm0, xmm8
							ADD r10, 16

							MOV r11, rsp

							MOV ecx, 14

							PXOR xmm0, [r11]

align 16
							encryptionloop:
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8

								MOVD xmm4, eax
								
								PSRLDQ xmm0, 4

								MOVD eax, xmm0
								
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB

								MOVD xmm5, eax

								PSRLDQ xmm0, 4
								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 8

								MOVD xmm6, eax
								
								PSRLDQ xmm0, 4

								MOVD eax, xmm0
							
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROR eax, 8
								XLATB
								ROL eax, 16

								MOVD xmm0, eax
								
								
								PSLLDQ xmm0, 12
								PSLLDQ xmm6, 8

								POR xmm0, xmm6
								PSLLDQ xmm5, 4

								POR xmm5, xmm4
								POR xmm0, xmm5
																			

								ADD r11, 16
								PXOR xmm0, [r11]

								DEC ecx
								JZ storethecode
									
									MOVDQA xmm5, xmm0																		
									MOVDQA xmm4, xmm0

									PSLLD xmm4, 1									
									PAND xmm4, xmm9
									
									PSRLD xmm5, 7
									PAND xmm5, xmm10

									MOVDQA xmm6, xmm0
									MOVDQA xmm1, xmm0
																		
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm5
									
									PAND xmm3, xmm11

									PXOR xmm4, xmm3
									MOVDQA xmm2, xmm4

									PSLLD xmm2, 1
									PAND xmm2, xmm9
									
									PSRLD xmm6, 6
									PAND xmm6, xmm10
									
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm6
									PAND xmm3, xmm11

									PXOR xmm2, xmm3																									
									MOVDQA xmm7, xmm2

									PSLLD xmm7, 1
									PAND xmm7, xmm9

									PSRLD xmm1, 5
									PAND xmm1, xmm10
									
									PXOR xmm3, xmm3
									PSUBSB xmm3, xmm1
									PAND xmm3, xmm11

									PXOR xmm7, xmm3
									PXOR xmm0, xmm7
									
									PXOR xmm7, xmm4
									PXOR xmm4, xmm0
									

									PXOR xmm7, xmm2
									PXOR xmm2, xmm0
									
									PSHUFD xmm2, xmm2, 4Eh
									PSHUFD xmm4, xmm4, 39h

									PXOR xmm4, xmm2
									PSHUFD xmm0, xmm0, 93h

									PXOR xmm0, xmm7									
									PXOR xmm0, xmm4

							JMP encryptionloop
align 16
							storethecode:								
								PSHUFB xmm0, xmm8
								
								PXOR xmm0, xmm13
								MOVDQU [r9], xmm0
								MOVDQA xmm13, xmm14

								ADD r9, 16

						DEC edx
						JNZ messageloop

					XOR rax, rax
					CMP eax, [r8+32]
					JNZ nofcorrection

							MOV [rsp+64], rax
							MOV [rsp+72], rax
							MOV [rsp+80], rax
							MOV [rsp+88], rax
							MOV [rsp+96], rax
							MOV [rsp+104], rax


							MOV [rsp+64], r13d
							NOT r13d
							MOV [rsp+68], r13d
							NOT r13d
							MOV edx, [r8+13]
							MOV [rsp+76], edx

							
							CMP r13d, 4
							JBE no_mloads
								MOV edx, [r12+16]
								MOV [rsp+80], edx
								MOV edx, [r12+20]
								MOV [rsp+88], edx
								MOV edx, [r12+24]
								MOV [rsp+96], edx
								MOV edx, [r12+28]
								MOV [rsp+104], edx

								SHL r13, 3

								MOV edx, [r12+r13]
								MOV [rsp+84], edx
								MOV edx, [r12+r13+4]
								MOV [rsp+92], edx
								MOV edx, [r12+r13+8]
								MOV [rsp+100], edx
								MOV edx, [r12+r13+12]
								MOV [rsp+108], edx
						align 16
							no_mloads:

							MOV r13, r8

							LEA rcx, [rsp+112]
							LEA rdx, [rsp+64]
							MOV r8d, 47
							XOR r9, r9

							CALL sha256_SSSE3

							MOV r8, r13

							MOV DWORD PTR [r8+32], 0FFFFFFFFh

							MOV eax, [rsp+112]
							MOV [r8+36], eax
							MOV eax, [rsp+120]
							MOV [r8+40], eax
							MOV eax, [rsp+128]
							MOV [r8+44], eax
							MOV eax, [rsp+136]
							MOV [r8+48], eax

							MOVDQU xmm14, [r8+36]
							MOVDQU xmm0, [r12]
							PXOR xmm0, xmm14
							MOVDQU [r12], xmm0
					nofcorrection:
					MOVDQU [r8+36], xmm13


					MOV rsp, r15
					POP rbx
					POP r12
					POP r13
					POP r15
				RET


	RET
reverse_aes256_SSSE3	ENDP




prog_aes128 ENDS

END
