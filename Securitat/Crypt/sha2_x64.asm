
; safe-fail securitat
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



PUBLIC	h256_l, r256_cl, h256_0, h256_00, r256_c, h512_l, r512_cl, h512_0, h512_00, r512_c, pr_md5, ph_md5

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
ALIGN 16

h512_l			QWORD	06a09e667f3bcc908h, 0bb67ae8584caa73bh, 03c6ef372fe94f82bh, 0a54ff53a5f1d36f1h, 0510e527fade682d1h, 09b05688c2b3e6c1fh, 01f83d9abfb41bd6bh, 05be0cd19137e2179h

r512_cl			QWORD	0428a2f98d728ae22h, 07137449123ef65cdh, 0b5c0fbcfec4d3b2fh, 0e9b5dba58189dbbch, 03956c25bf348b538h, 059f111f1b605d019h, 0923f82a4af194f9bh, 0ab1c5ed5da6d8118h
				QWORD	0d807aa98a3030242h, 012835b0145706fbeh, 0243185be4ee4b28ch, 0550c7dc3d5ffb4e2h, 072be5d74f27b896fh, 080deb1fe3b1696b1h, 09bdc06a725c71235h, 0c19bf174cf692694h
				QWORD	0e49b69c19ef14ad2h, 0efbe4786384f25e3h, 00fc19dc68b8cd5b5h, 0240ca1cc77ac9c65h, 02de92c6f592b0275h, 04a7484aa6ea6e483h, 05cb0a9dcbd41fbd4h, 076f988da831153b5h
				QWORD	0983e5152ee66dfabh, 0a831c66d2db43210h, 0b00327c898fb213fh, 0bf597fc7beef0ee4h, 0c6e00bf33da88fc2h, 0d5a79147930aa725h, 006ca6351e003826fh, 0142929670a0e6e70h
				QWORD	027b70a8546d22ffch, 02e1b21385c26c926h, 04d2c6dfc5ac42aedh, 053380d139d95b3dfh, 0650a73548baf63deh, 0766a0abb3c77b2a8h, 081c2c92e47edaee6h, 092722c851482353bh
				QWORD	0a2bfe8a14cf10364h, 0a81a664bbc423001h, 0c24b8b70d0f89791h, 0c76c51a30654be30h, 0d192e819d6ef5218h, 0d69906245565a910h, 0f40e35855771202ah, 0106aa07032bbd1b8h
				QWORD	019a4c116b8d2d0c8h, 01e376c085141ab53h, 02748774cdf8eeb99h, 034b0bcb5e19b48a8h, 0391c0cb3c5c95a63h, 04ed8aa4ae3418acbh, 05b9cca4f7763e373h, 0682e6ff3d6b2b8a3h
				QWORD	0748f82ee5defb2fch, 078a5636f43172f60h, 084c87814a1f0ab72h, 08cc702081a6439ech, 090befffa23631e28h, 0a4506cebde82bde9h, 0bef9a3f7b2c67915h, 0c67178f2e372532bh
				QWORD	0ca273eceea26619ch, 0d186b8c721c0c207h, 0eada7dd6cde0eb1eh, 0f57d4f7fee6ed178h, 006f067aa72176fbah, 00a637dc5a2c898a6h, 0113f9804bef90daeh, 01b710b35131c471bh
				QWORD	028db77f523047d84h, 032caab7b40c72493h, 03c9ebe0a15c9bebch, 0431d67c49c100d4ch, 04cc5d4becb3e42b6h, 0597f299cfc657e2ah, 05fcb6fab3ad6faech, 06c44198c4a475817h


h512_0			QWORD	06A09E667F3BCC908h, 0BB67AE8584CAA73Bh, 01E3779B97F4A7C15h, 052A7FA9D2F8E9B78h, 0A887293FD6F34168h, 0CD82B446159F360Fh, 007E0F66AFED06F5Ah, 016F8334644DF885Eh
h512_00			QWORD	0921FB54442D18469h, 0D1ED52076FBE93E9h, 0B860BB5CAE7FE9B8h, 0CD82B446159F360Fh, 00B0E6AF779D94F9Bh, 032EEE757704167B6h, 07A4DA180D19C1412h, 0A3AD12A1DA160544h

r512_c			QWORD	0428A2F98D728AE22h, 07137449123EF65CDh, 0B5C0FBCFEC4D3B2Fh, 0E9B5DBA58189DBBCh, 01CAB612DF9A45A9Ch, 02CF888F8DB02E80Ch, 0491FC152578CA7CDh, 0558E2F6AED36C08Ch
				QWORD	06C03D54C51818121h, 08941AD80A2B837DFh, 09218C2DF27725946h, 0AA863EE1EAFFDA71h, 0B95F2EBA793DC4B7h, 0C06F58FF1D8B4B58h, 0CDEE035392E3891Ah, 0E0CDF8BA67B4934Ah
				QWORD	0EC9BAF60B9F7FCEEh, 0F24DB4E0CF78A569h, 0F7DF23C31C2792F1h, 003F06771A2E3356Dh, 0090328731DEB2719h, 00B7A4B1BD64AC09Dh, 0129D212A9BA9B920h, 0172C2A772F507EF5h
				QWORD	01DBE6236A0C454EDh, 0260F9454BB99B7EAh, 02A0C719B4B6D0C84h, 02C00C9F2263EC84Fh, 02FD65FF1EFBBC3B9h, 031B802FCCF6A23F0h, 03569E451E4C2A9C9h, 041B298D47800E09Bh
				QWORD	0450A4A59C2839B9Ch, 049EDC2A151B48BFFh, 04B86C84E1709B249h, 0534B1B7F16B10ABBh, 054CE0344E7656CF7h, 059429CD522EBD8F7h, 05D9A82AECF1DECAAh, 06070B24B91FB6BB9h
				QWORD	0649C8B2145208D4Eh, 068AFFA28533C40D9h, 06A069992EF108C00h, 07092E2DC343E25E4h, 071DB1468C1952F8Ch, 07464BA0675BBD486h, 075A6418915596A44h, 07D038D6155DC480Ah
				QWORD	0841AA81C0CAEF46Eh, 086693045AE34B432h, 0878DDB0214506AD4h, 089D21DD337E3BAE6h, 08D2C2F2D7866D22Ah, 08E47032CF1725698h, 093B62A92B8D062B2h, 096E73293DDD8F8DCh
				QWORD	09A0B9BFCF5ACAE28h, 09D23E0BB977BECBFh, 09E2958DBD0C5CBD8h, 0A1321E05287C2ADCh, 0A331C08206990E7Bh, 0A42FBFFE88D8C78Ah, 0A9141B3AF7A0AF7Ah, 0AFBE68FDECB19E45h
				QWORD	0B19C5E3CB8DC94CAh, 0B289CFB3BA899867h, 0B461AE31C8703081h, 0BAB69F75B3783AC7h, 0BD5F53DFFB9BB45Eh, 0C1BC19EA9C85DBEEh, 0C298DF7168B22629h, 0C44FE6012FBE436Bh
				QWORD	0C6DC42CD44C711C6h, 0CA36DDFD48C11F61h, 0CCB2AADED031C924h, 0CF27AF8285726FAFh, 0D0C759F127040353h, 0D331752FB2CF90ADh, 0D65FCA673F195F8Ah, 0D7F2DBEACEB5BEBBh



ALIGN 16

h256_l			DWORD	06a09e667h, 0bb67ae85h, 03c6ef372h, 0a54ff53ah, 0510e527fh, 09b05688ch, 01f83d9abh, 05be0cd19h

r256_cl			DWORD	0428a2f98h, 071374491h, 0b5c0fbcfh, 0e9b5dba5h, 03956c25bh, 059f111f1h, 0923f82a4h, 0ab1c5ed5h
				DWORD	0d807aa98h, 012835b01h, 0243185beh, 0550c7dc3h, 072be5d74h, 080deb1feh, 09bdc06a7h, 0c19bf174h
				DWORD	0e49b69c1h, 0efbe4786h, 00fc19dc6h, 0240ca1cch, 02de92c6fh, 04a7484aah, 05cb0a9dch, 076f988dah
				DWORD	0983e5152h, 0a831c66dh, 0b00327c8h, 0bf597fc7h, 0c6e00bf3h, 0d5a79147h, 006ca6351h, 014292967h
				DWORD	027b70a85h, 02e1b2138h, 04d2c6dfch, 053380d13h, 0650a7354h, 0766a0abbh, 081c2c92eh, 092722c85h
				DWORD	0a2bfe8a1h, 0a81a664bh, 0c24b8b70h, 0c76c51a3h, 0d192e819h, 0d6990624h, 0f40e3585h, 0106aa070h
				DWORD	019a4c116h, 01e376c08h, 02748774ch, 034b0bcb5h, 0391c0cb3h, 04ed8aa4ah, 05b9cca4fh, 0682e6ff3h
				DWORD	0748f82eeh, 078a5636fh, 084c87814h, 08cc70208h, 090befffah, 0a4506cebh, 0bef9a3f7h, 0c67178f2h


h256_0			DWORD	06A09E667h, 0BB67AE85h, 01E3779B9h, 052A7FA9Dh, 0A887293Fh, 0CD82B446h, 007E0F66Ah, 016F83346h
h256_00			DWORD	0921FB544h, 0D1ED5207h, 0B860BB5Ch, 0CD82B446h, 00B0E6AF7h, 032EEE757h, 07A4DA180h, 0A3AD12A1h

r256_c			DWORD	0428A2F98h, 071374491h, 0B5C0FBCFh, 0E9B5DBA5h, 01CAB612Dh, 02CF888F8h, 0491FC152h, 0558E2F6Ah
				DWORD	06C03D54Ch, 08941AD80h, 09218C2DFh, 0AA863EE1h, 0B95F2EBAh, 0C06F58FFh, 0CDEE0353h, 0E0CDF8BAh
				DWORD	0EC9BAF60h, 0F24DB4E0h, 0F7DF23C3h, 003F06771h, 009032873h, 00B7A4B1Bh, 0129D212Ah, 0172C2A77h
				DWORD	01DBE6236h, 0260F9454h, 02A0C719Bh, 02C00C9F2h, 02FD65FF1h, 031B802FCh, 03569E451h, 041B298D4h
				DWORD	0450A4A59h, 049EDC2A1h, 04B86C84Eh, 0534B1B7Fh, 054CE0344h, 059429CD5h, 05D9A82AEh, 06070B24Bh
				DWORD	0649C8B21h, 068AFFA28h, 06A069992h, 07092E2DCh, 071DB1468h, 07464BA06h, 075A64189h, 07D038D61h
				DWORD	0841AA81Ch, 086693045h, 0878DDB02h, 089D21DD3h, 08D2C2F2Dh, 08E47032Ch, 093B62A92h, 096E73293h
				DWORD	09A0B9BFCh, 09D23E0BBh, 09E2958DBh, 0A1321E05h, 0A331C082h, 0A42FBFFEh, 0A9141B3Ah, 0AFBE68FDh


ALIGN 16
pr_md5			DWORD	0d76aa478h, 0e8c7b756h, 0242070dbh, 0c1bdceeeh, 0f57c0fafh, 04787c62ah, 0a8304613h, 0fd469501h
				DWORD	0698098d8h, 08b44f7afh, 0ffff5bb1h, 0895cd7beh, 06b901122h, 0fd987193h, 0a679438eh, 049b40821h
				DWORD	0f61e2562h, 0c040b340h, 0265e5a51h, 0e9b6c7aah, 0d62f105dh, 002441453h, 0d8a1e681h, 0e7d3fbc8h
				DWORD	021e1cde6h, 0c33707d6h, 0f4d50d87h, 0455a14edh, 0a9e3e905h, 0fcefa3f8h, 0676f02d9h, 08d2a4c8ah
				DWORD	0fffa3942h, 08771f681h, 06d9d6122h, 0fde5380ch, 0a4beea44h, 04bdecfa9h, 0f6bb4b60h, 0bebfbc70h
				DWORD	0289b7ec6h, 0eaa127fah, 0d4ef3085h, 004881d05h, 0d9d4d039h, 0e6db99e5h, 01fa27cf8h, 0c4ac5665h
				DWORD	0f4292244h, 0432aff97h, 0ab9423a7h, 0fc93a039h, 0655b59c3h, 08f0ccc92h, 0ffeff47dh, 085845dd1h
				DWORD	06fa87e4fh, 0fe2ce6e0h, 0a3014314h, 04e0811a1h, 0f7537e82h, 0bd3af235h, 02ad7d2bbh, 0eb86d391h

ph_md5			DWORD	067452301h, 0efcdab89h, 098badcfeh, 010325476h



ALIGN 16
swap_shuf		DWORD	00010203h, 04050607h, 08090A0Bh, 0C0D0E0Fh 
swap_shuf_r		DWORD	0C0D0E0Fh, 08090A0Bh, 04050607h, 00010203h 


.CODE

ALIGN 16
sha512_SSSE3	PROC		
				PUSH r15
				PUSH r14
				PUSH r13
				PUSH r12
				PUSH rsi
				PUSH rdi

				PUSH rbp
					MOV rbp, rsp

					SUB rsp, 2304
					SHR rsp, 4
					SHL rsp, 4

						XOR rax, rax
						MOV rdi, rsp

						MOV r10, rcx
						MOV r11, rdx

						MOV rcx, 256
						REP STOSQ

						XOR rdx, rdx
						MOV rax, r8
						MOV rcx, 128

						DIV rcx

						MOV r12, 1 ; correctionPlate
						
						LEA rdi, [rsp+1024]

						TEST rdx, rdx ; leftover
						JZ noleftover
							MOV rcx, rdx
							
							MOV rsi, r11
							SHL rax, 6
							ADD rsi, rax
							SHR rax, 6

							REP MOVSB
						noleftover:

						MOV BYTE PTR [rdi], 80h

						SHL r8, 3
						BSWAP r8

						LEA rdi, [rsp + 1144]

						CMP rdx, 112
						JB noextrames
							ADD rdi, 128
							MOV r12, 2
						noextrames:

						MOV [rdi], r8
						
						LEA rsi, [h512_l]
						LEA r13, [r512_cl]

						MOV r8d, r9d
						AND r9d, 0Fh

						CMP r9d, 1
						JNE option2
							LEA r13, [r512_c]
							LEA rsi, [h512_00]

							JMP option3
						option2:
						CMP r9d, 2
						JNE option3
							LEA rsi, [h512_0]
							LEA r13, [r512_c]

						option3:


						TEST r8d, 80000000h
						CMOVNZ rsi, r10


						MOVDQU xmm12, [rsi]
						MOVDQU xmm13, [rsi+16]
						MOVDQU xmm14, [rsi+32]
						MOVDQU xmm15, [rsi+48]

						PSHUFD xmm12, xmm12, 4Eh
						PSHUFD xmm13, xmm13, 4Eh
						PSHUFD xmm14, xmm14, 4Eh
						PSHUFD xmm15, xmm15, 4Eh

						JZ default_hash
							PSHUFB xmm12, XMMWORD PTR [swap_shuf]
							PSHUFB xmm13, XMMWORD PTR [swap_shuf]
							PSHUFB xmm14, XMMWORD PTR [swap_shuf]
							PSHUFB xmm15, XMMWORD PTR [swap_shuf]
						default_hash:

						MOVDQA xmm8, xmm12
						MOVDQA xmm9, xmm13
						MOVDQA xmm10, xmm14
						MOVDQA xmm11, xmm15

						MOV rdx, -1
						MOVD xmm7, rdx
						MOVDQA xmm6, xmm7
						PSLLDQ xmm7, 8

						MOV rsi, r11
						MOV rdx, rax
						TEST rdx, rdx

	ALIGN 16
						messageloop:
							JNZ leftoverloop
								CMP r12, 0
								JZ hashingcomplete
								LEA rsi, [rsp+1024]

								MOV rdx, r12
								XOR r12, r12
							leftoverloop:


							MOV rdi, rsp
							MOV rcx, 16

							isetloop:
								LODSQ
								BSWAP rax
								STOSQ
							LOOP isetloop

							MOVDQA xmm0, [rdi-16]

							MOV ecx, 32
	ALIGN 16
							expansionloop:								
								MOVDQU xmm1, [rdi-56]
								MOVDQU xmm3, [rdi-120]
								MOVDQA xmm2, [rdi-128]
								
								PADDQ xmm1, xmm2 

								MOVDQA xmm4, xmm3
								MOVDQA xmm5, xmm3
								MOVDQA xmm2, xmm3
			
								PSRLQ xmm4, 1
								PSLLQ xmm5, 63
								POR xmm4, xmm5

								MOVDQA xmm5, xmm2
								PSRLQ xmm5, 8
								PSLLQ xmm2, 56
								POR xmm5, xmm2

								PXOR xmm4, xmm5
								PSRLQ xmm3, 7

								PXOR xmm3, xmm4
								PADDQ xmm3, xmm1

								MOVDQA xmm4, xmm0
								MOVDQA xmm5, xmm0
								MOVDQA xmm2, xmm0
			
								PSRLQ xmm4, 19
								PSLLQ xmm5, 45
								POR xmm4, xmm5

								MOVDQA xmm5, xmm2
								PSRLQ xmm5, 61
								PSLLQ xmm2, 3
								POR xmm5, xmm2

								PXOR xmm4, xmm5
								PSRLQ xmm0, 6
								PXOR xmm0, xmm4
											
								PADDQ xmm0, xmm3

								MOVDQA [rdi], xmm0

								ADD rdi, 16

								SUB ecx, 1
							JNZ expansionloop


							MOV r11, rsp
							MOV rdi, r13

							MOV ecx, 40
	ALIGN 16
							compressionloop:
								MOVDQA xmm1, xmm14

								MOVDQA xmm5, [r11]	
								PADDQ xmm5, xmm15
								PADDQ xmm5, [rdi]

								PUNPCKHQDQ xmm15, xmm1
								PSHUFD xmm15, xmm15, 4Eh

								PXOR xmm1, xmm7
								PAND xmm1, xmm15

								MOVD r15, xmm15
								MOVD r14, xmm15
								MOVD r9, xmm15

								ROR r15, 14
								ROR r14, 18
								ROR r9, 41

								XOR r15, r14								
								XOR r15, r9

								MOVD r14, xmm1
								PSRLDQ xmm1, 8
								MOVD r9, xmm1

								XOR r14, r9

								ADD r15, r14

								MOVD r14, xmm5
								PSRLDQ xmm5, 8
								ADD r15, r14
								MOVD xmm3, r15
								PSLLDQ xmm3, 8
								
								MOVD r8, xmm13

								ADD r8, r15
								MOVD xmm2, r8
	
								MOV r14, r8
								MOV r9, r8

								ROR r8, 14
								ROR r14, 18
								ROR r9, 41

								XOR r8, r14								
								XOR r8, r9

								MOVDQA xmm15, xmm14
							
								PUNPCKLQDQ xmm2, xmm2
								PXOR xmm2, xmm6
								PAND xmm14, xmm2
							
								MOVD r14, xmm14
								PSRLDQ xmm14, 8
								MOVD r9, xmm14

								XOR r14, r9
								MOVD r9, xmm5

								ADD r14, r9
								ADD r8, r14
								PUNPCKHQDQ xmm13, xmm13

								MOVD r9, xmm13
								
								ADD r9, r8
								MOVD xmm14, r9

								PSRLDQ xmm2, 8
								PUNPCKLQDQ xmm2, xmm14
								MOVDQA xmm14, xmm2
								
								MOVDQA xmm0, xmm12

								PAND xmm13, xmm12

								PSHUFD xmm1, xmm13, 0Eh
								PXOR xmm1, xmm13
								MOVDQA xmm13, xmm12

								PSRLDQ xmm12, 8

								MOVD r9, xmm12
								MOVD r14, xmm12

								ROR r9, 28
								ROR r14, 34
								XOR r9, r14
								MOVD r14, xmm12
								ROR r14, 39

								XOR r9, r14

								ADD r15, r9

								MOVD r9, xmm12
								MOVD r14, xmm0
								AND r9, r14
								MOVD r14, xmm1
								XOR r14, r9

								ADD r15, r14
								MOVD r14, xmm12

								MOVD xmm12, r15

								AND r14, r15
								XOR r14, r9
								MOVD r9, xmm0
								AND r9, r15
								XOR r14, r9

								ADD r8, r14

								MOV r9, r15
								MOV r14, r15

								ROR r9, 28
								ROR r14, 34
								ROR r15, 39

								XOR r9, r14
								XOR r9, r15

								ADD r8, r9
								MOVD xmm2, r8
								PUNPCKLQDQ xmm12, xmm2

								ADD rdi, 16
								ADD r11, 16
								SUB ecx, 1

							JNZ compressionloop			

							PADDQ xmm12, xmm8
							PADDQ xmm13, xmm9
							PADDQ xmm14, xmm10
							PADDQ xmm15, xmm11
				
							MOVDQA xmm11, xmm15
							MOVDQA xmm10, xmm14
							MOVDQA xmm9, xmm13
							MOVDQA xmm8, xmm12

							SUB rdx, 1
						JMP messageloop

					hashingcomplete:

						PSHUFB xmm12, XMMWORD PTR [swap_shuf_r]
						PSHUFB xmm13, XMMWORD PTR [swap_shuf_r]
						PSHUFB xmm14, XMMWORD PTR [swap_shuf_r]
						PSHUFB xmm15, XMMWORD PTR [swap_shuf_r]


						MOVDQU [r10+48], xmm15
						MOVDQU [r10+32], xmm14
						MOVDQU [r10+16], xmm13
						MOVDQU [r10], xmm12

					MOV rsp, rbp
				POP rbp
				POP rdi
				POP rsi
				POP r12
				POP r13
				POP r14
				POP r15
			RET
sha512_SSSE3	ENDP

ALIGN 16
sha256_SSSE3	PROC
					PUSH r15
					PUSH r14
					PUSH r13
					PUSH r12
					PUSH rsi
					PUSH rdi
					PUSH rbx
					PUSH rbp
					MOV rbp, rsp

						SUB rsp, 1280
						SHR rsp, 4
						SHL rsp, 4

						MOV r10, rcx
						MOV r11, rdx

						XOR rax, rax
						MOV rdi, rsp

						MOV rcx, 144
						REP STOSQ

						XOR edx, edx
						MOV eax, r8d
						MOV ecx, 64

						DIV ecx

						MOV DWORD PTR [rsp+1032], 1 ; correctionPlate
						MOVDQA xmm15, XMMWORD PTR [swap_shuf]

						LEA rdi, [rsp + 512]

						TEST edx, edx ; leftover
						JZ noleftover
							MOV ecx, edx
							
							MOV rsi, r11
							SHL rax, 6
							ADD rsi, rax
							SHR rax, 6

							REP MOVSB
						noleftover:

						MOV BYTE PTR [rdi], 80h

						SHL r8, 3
						BSWAP r8

						LEA rdi, [rsp + 572]

						CMP edx, 56
						JB noextrames
							ADD rdi, 64
							MOV DWORD PTR [rsp+1032], 2
						noextrames:

						MOV [rdi-4], r8

						LEA r13, [r256_cl]
						LEA rsi, [h256_l]

						MOV r8d, r9d
						AND r9d, 0Fh
						CMP r9d, 1
						JNE option2
							LEA r13, [r256_c]
							LEA rsi, [h256_00]

							JMP option3
						option2:
						CMP r9d, 2
						JNE option3
							LEA r13, [r256_c]
							LEA rsi, [h256_0]
						
						option3:

						TEST r8d, 80000000h
						CMOVNZ rsi, r10

						MOV [rsp+1024], r10
						MOV [rsp+1040], r13

						MOVDQU xmm6, [rsi]
						MOVDQU xmm7, [rsi+16]

						PSHUFD xmm6, xmm6, 01Bh
						PSHUFD xmm7, xmm7, 01Bh

						JZ default_hash
							PSHUFB xmm6, XMMWORD PTR [swap_shuf]
							PSHUFB xmm7, XMMWORD PTR [swap_shuf]
						default_hash:


						MOVDQA xmm8, xmm6
						MOVDQA xmm9, xmm7
							
						MOV rsi, r11					
						
						MOV r15d, eax
						TEST r15d, r15d

						JNZ messageloop
							CMP DWORD PTR [rsp+1032], 0
							JZ hashingcomplete
							LEA rsi, [rsp+512]

							MOV r15d, [rsp+1032]
							MOV DWORD PTR [rsp+1032], 0

	align 16
						messageloop:
							
							MOV r11, rsp
						
							MOVDQU xmm2, [rsi]
							MOVDQU xmm3, [rsi+16]
							MOVDQU xmm4, [rsi+32]
							MOVDQU xmm5, [rsi+48]

						PREFETCHNTA [rsi+64]

							ADD rsi, 64
							MOV [rsp+1048], rsi

							MOV ecx, 12

							MOVDQA [r11], xmm7
							MOVDQA [r11+16], xmm6

							PSHUFB xmm2, xmm15
							PSHUFB xmm3, xmm15
							
							MOVDQA xmm10, xmm7

							MOV r13d, [r11+4] ; g
							MOV r10d, [r11+8] ; f
							MOV eax, [r11+12] ; e
							MOV r14d, [r11+16] ; d
							MOV r12d, [r11+20] ; c
							MOV r8d, [r11+24] ; b
							MOV edx, [r11+28] ; a
							
							PSHUFB xmm4, xmm15
							PSHUFB xmm5, xmm15
						

							MOV r9d, r8d
							AND r9d, r12d

							MOV rsi, [rsp+1040]
							SUB rsi, r11
							
							

	align 16
							expansionloop:
								MOVDQA xmm1, xmm2
								PSRLDQ xmm1, 4	
																
									MOV ebx, eax
									AND ebx, r10d

									NOT eax

									AND r13d, eax
																	
									XOR ebx, r13d ; Ch
									MOV r13d, r10d

								MOVDQA xmm0, xmm2								
								PADDD xmm2, xmm10

									NOT eax
																											
									MOV edi, eax
									ROR edi, 6
									MOV r10d, eax

								PADDD xmm2, [rsi+r11]							
								MOVDQA xmm13, xmm2
								
									ROR eax, 25									
									XOR eax, edi
									ROR edi, 5
									XOR eax, edi ; S1
									ADD eax, ebx ; S1 + Ch

								MOVDQA xmm2, xmm3								
								PSLLDQ xmm3, 12

									MOV edi, edx
									ROR edi, 13									
									
								MOVD ebx, xmm13
								PSRLDQ xmm13, 4
													
									ADD eax, ebx
									MOV ebx, edx
									
								
								POR xmm1, xmm3 ; W15
								MOVDQA xmm6, xmm5
									

									ROR edx, 2									
									XOR edx, edi					
									ROR edi, 9									
									XOR edx, edi ; S0																		
									
								
								PSLLDQ xmm6, 12
								MOVDQA xmm3, xmm4
								
									ADD edx, eax ; T1+S0
									MOV edi, r12d
									AND edi, ebx ; ac

									ADD eax, r14d																
									

								PSRLDQ xmm4, 4
								POR xmm6, xmm4 ; W7									

									MOV r14d, [r11+24]
									AND ebx, r14d  ; ab 
										
									XOR edi, ebx
									XOR edi, r9d ; Maj						
																											
									ADD edx, edi ; T2 + T2								
									MOV [r11+32], edx

								PADDD xmm0, xmm6 ; W7 + W16
								MOVDQA xmm7, xmm1
								
									MOV r9d, eax
									AND r9d, r10d

									NOT eax
									
								PSRLD xmm7, 3
								MOVDQA xmm4, xmm5								

									AND r13d, eax
									XOR r9d, r13d
																	
									MOV r13d, r10d
									
									NOT eax
												
								MOVDQA xmm6, xmm1
								PSLLD xmm1, 14
									MOV edi, eax
									ROR edi, 6

									MOV r10d, eax
									
									ROR eax, 25									
									XOR eax, edi
																	
								PSRLD xmm6, 18
								POR xmm6, xmm1
																									
									ROR edi, 5
									XOR eax, edi ; S1
									ADD eax, r9d
																		
								MOVD r9d, xmm13
								PSRLDQ xmm13, 4

									ADD eax, r9d

									MOV edi, edx
									MOV r9d, edx

									ROR edi, 13
	
								PXOR xmm6, xmm7
								PSRLD xmm7, 4
									
									ROR edx, 2									
									XOR edx, edi
									ROR edi, 9									
									XOR edx, edi
																		
								MOVD xmm10, r10d
								PSRLDQ xmm5, 8 ; W2

									ADD edx, eax									
									MOV edi, r14d
									AND edi, r9d ; ac
									XOR edi, ebx
									
								PSLLD xmm1, 11
								POR xmm7, xmm1

									ADD eax, r12d

									MOV r12d, [r11+28]
									AND r9d, r12d ; ab										
									
									XOR edi, r9d ; Maj						

								PXOR xmm6, xmm7
								PADDD xmm0, xmm6 ; W7 + W16 + s0(W15)									
									ADD edx, edi ; T2 + T2	
									MOV [r11+36], edx

									MOV ebx, eax
									NOT eax
									AND r13d, eax
																
								MOVDQA xmm6, xmm5
								PSRLD xmm5, 10

									AND ebx, r10d									
									XOR ebx, r13d
								
									NOT eax									
									MOV r13d, r10d
																	
								MOVDQA xmm7, xmm6
								PSLLD xmm6, 13

									MOV r10d, eax
									MOV edi, eax

									ROR edi, 6

								MOVD xmm11, eax
								PSRLD xmm7, 19
																
									ROR eax, 25									
									XOR eax, edi
									ROR edi, 5
									XOR eax, edi ; S1

								
								POR xmm7, xmm6
								PXOR xmm7, xmm5

									ADD eax, ebx
									
									MOV edi, edx
									MOV ebx, edx
																		
									ROR edi, 13
																
								PSRLD xmm5, 7
								PSLLD xmm6, 2
									
									ROR edx, 2									
									XOR edx, edi
									ROR edi, 9									
									XOR edx, edi
																		
								POR xmm5, xmm6
								PXOR xmm7, xmm5 ; s1(W2)


								
								MOVD edi, xmm13
								PSRLDQ xmm13, 4

									ADD eax, edi
									ADD edx, eax

									MOV edi, r12d
									ADD eax, r14d
									
									AND edi, ebx ; ac

								MOVD xmm12, eax	
								PSLLDQ xmm11, 4

									MOV r14d, [r11+32]
									AND ebx, r14d ; ab
								
									XOR edi, ebx
									XOR edi, r9d ; Maj						
																											

								MOVDQA xmm1, xmm7									
								PSLLDQ xmm12, 8												
																	
									ADD edx, edi ; T2 + T2	
									MOV [r11+40], edx

								PADDD xmm7, xmm0
								MOVDQA xmm5, xmm7

									MOV r9d, eax									

									AND r9d, r10d
									NOT eax
									AND r13d, eax
									
									XOR r9d, r13d									
									
								POR xmm10, xmm11
								MOVDQA xmm6, xmm7

									NOT eax
									MOV r13d, r10d
									
								
								PSLLD xmm7, 13
								PSRLD xmm6, 19								
										
									MOV r10d, eax																	
									MOV edi, eax							
									ROR edi, 6
															
								POR xmm6, xmm7
								PSRLD xmm5, 10
								
									ROR eax, 25									
									XOR eax, edi
									ROR edi, 5
									XOR eax, edi ; S1

								
								PXOR xmm6, xmm5
								PSRLD xmm5, 7

									ADD eax, r9d
									
									MOV edi, edx
									MOV r9d, edx
									
									ROR edi, 13
																								
								POR xmm10, xmm12
								PSLLD xmm7, 2

									ROR edx, 2									
									XOR edx, edi
									ROR edi, 9									
									XOR edx, edi									
																
								POR xmm5, xmm7
								PXOR xmm5, xmm6 ; s1(W2)
								
								
								MOVD edi, xmm13
									ADD eax, edi
									ADD edx, eax									
									ADD eax, r12d																		

								MOVD xmm11, eax
								PSLLDQ xmm11, 12

									MOV edi, r14d
									AND edi, r9d ; ac
									XOR edi, ebx

								POR xmm10, xmm11
								PSLLDQ xmm5, 8

									MOV r12d, [r11+36]
									AND r9d, r12d ; ab
									XOR edi, r9d ; Maj
								
								POR xmm5, xmm1; s1(W2)
								PADDD xmm5, xmm0 ; Wj								
																											
									ADD edx, edi ; T2 + T2									
									MOV [r11+44], edx
						
								ADD r11, 16
								
								SUB ecx, 1
							JNZ expansionloop
							
							
																							
							MOVDQA [r11+80], xmm3
							PADDD xmm2, xmm10

							MOVDQA [r11+96], xmm4
							MOVDQA [r11+112], xmm5				
							
							MOV ecx, 4
			align 16
							compressionloop:								

								MOV ebx, eax
								NOT eax

								AND ebx, r10d
								AND r13d, eax
							
								XOR ebx, r13d ; Ch
					
							PADDD xmm2, [rsi+r11]

								NOT eax									
								MOV r13d, r10d
								MOV r10d, eax
																		
								MOV edi, eax

														
								ROR edi, 6
								ROR eax, 25									
								XOR eax, edi
								ROR edi, 5
								XOR eax, edi ; S1

								ADD eax, ebx ; S1 + Ch

							MOVD ebx, xmm2
							PSRLDQ xmm2, 4

								ADD eax, ebx
	
								MOV edi, edx
								MOV ebx, edx


								ROR edi, 13
								ROR edx, 2									
								XOR edx, edi
								ROR edi, 9									
								XOR edx, edi ; S0
																		
								ADD edx, eax ; T1+S0
							
								ADD eax, r14d
									
								MOV edi, r12d
								MOV r14d, [r11+24]

								AND edi, ebx ; ac
								AND ebx, r14d  ; ab 
										
								XOR edi, ebx
								XOR edi, r9d ; Maj						
																											
								ADD edx, edi ; T2 + T2									
																				
								MOV [r11+32], edx

								MOV r9d, eax
								NOT eax

								AND r9d, r10d
								AND r13d, eax
									
								XOR r9d, r13d
									
								NOT eax									
								MOV r13d, r10d
								MOV r10d, eax
																		
								MOV edi, eax
								
								ROR edi, 6
								ROR eax, 25									
								XOR eax, edi
								ROR edi, 5
								XOR eax, edi ; S1

								ADD eax, r9d
																		
							MOVD r9d, xmm2
							PSRLDQ xmm2, 4

								ADD eax, r9d

								MOV edi, edx
								MOV r9d, edx
								
								ROR edi, 13
								ROR edx, 2									
								XOR edx, edi
								ROR edi, 9									
								XOR edx, edi
																		
								ADD edx, eax
							
								ADD eax, r12d
									
								MOV edi, r14d
								MOV r12d, [r11+28]

								AND edi, r9d ; ac
								AND r9d, r12d ; ab
							
							MOVD xmm3, r10d
										
								XOR edi, ebx
								XOR edi, r9d ; Maj
																															
								ADD edx, edi ; T2 + T2									
																			
								MOV [r11+36], edx

								MOV ebx, eax
								NOT eax

								AND ebx, r10d
								AND r13d, eax
							
								XOR ebx, r13d
								
								MOV r13d, r10d
								NOT eax									
								
								MOV r10d, eax																		
								MOV edi, eax
							MOVD xmm4, eax
																
								ROR edi, 6
								ROR eax, 25									
								XOR eax, edi
								ROR edi, 5
								XOR eax, edi ; S1

								ADD eax, ebx
									
							MOVD ebx, xmm2
							PSRLDQ xmm2, 4

								ADD eax, ebx

								MOV edi, edx
								MOV ebx, edx

								ROR edi, 13
								ROR edx, 2									
								XOR edx, edi
								ROR edi, 9									
								XOR edx, edi
																		
								ADD edx, eax
							
								ADD eax, r14d

								MOV edi, r12d
								MOV r14d, [r11+32]

							MOVD xmm5, eax
								
								AND edi, ebx ; ac
								AND ebx, r14d ; ab
										
								XOR edi, ebx
								XOR edi, r9d ; Maj						
								
							PSLLDQ xmm4, 4
							PSLLDQ xmm5, 8
																											
								ADD edx, edi ; T2 + T2									
																				
								MOV [r11+40], edx															
								
								MOV r9d, eax
								NOT eax
									
								AND r9d, r10d
								AND r13d, eax
								
							POR xmm3, xmm4
							POR xmm3, xmm5
									
								XOR r9d, r13d
								
								NOT eax									
									
								MOV r13d, r10d
								MOV r10d, eax
																										
								MOV edi, eax
								
								ROR edi, 6
								ROR eax, 25									
								XOR eax, edi
								ROR edi, 5
								XOR eax, edi ; S1

								ADD eax, r9d
									
							MOVD r9d, xmm2
								ADD eax, r9d

								MOV edi, edx
								MOV r9d, edx
									
								ROR edi, 13
								ROR edx, 2									
								XOR edx, edi
								ROR edi, 9									
								XOR edx, edi
																	
								ADD edx, eax									
								ADD eax, r12d

								MOV edi, r14d
								MOV r12d, [r11+36]
									
							MOVD xmm2, eax
							PSLLDQ xmm2, 12
								
								AND edi, r9d ; ac
								AND r9d, r12d ; ab
										
								XOR edi, ebx
								XOR edi, r9d ; Maj						
																											
								ADD edx, edi ; T2 + T2									
								
																		
								MOV [r11+44], edx
							
							POR xmm2, xmm3
							PADDD xmm2, [r11+80]
									
								ADD r11, 16
									
							SUB ecx, 1
							JNZ compressionloop	
							
								
; offload registers
							
							PADDD xmm8, [r11+16]							
							PADDD xmm9, xmm2

							MOVDQA xmm6, xmm8
							MOVDQA xmm7, xmm9
							

							MOV rsi, [rsp+1048]

							SUB r15d, 1
						JNZ messageloop
							CMP DWORD PTR [rsp+1032], 0
							JZ hashingcomplete
								LEA rsi, [rsp+512]

								MOV r15d, [rsp+1032]
								MOV DWORD PTR [rsp+1032], 0
								JMP messageloop

					hashingcomplete:							
						
						MOV r10, [rsp+1024]

						PSHUFB xmm7, XMMWORD PTR [swap_shuf_r]
						PSHUFB xmm6, XMMWORD PTR [swap_shuf_r]

						MOVDQU [r10+16], xmm7
						MOVDQU [r10], xmm6
	
					MOV rsp, rbp
				POP rbp
				POP rbx
				POP rdi
				POP rsi
				POP r12
				POP r13
				POP r14
				POP r15
			RET
sha256_SSSE3	ENDP



ALIGN 16
md5	PROC
	PUSH r15
	PUSH rdi
	PUSH rsi
	PUSH rbp
		MOV rbp, rsp

						SUB rsp, 1280
						SHR rsp, 4
						SHL rsp, 4

						MOV r10, rcx
						MOV r11, rdx

						XOR rax, rax
						MOV rdi, rsp

						MOV rcx, 144
						REP STOSQ

						XOR edx, edx
						MOV eax, r8d
						MOV ecx, 64

						DIV ecx

						MOV DWORD PTR [rsp+1032], 1 ; correctionPlate
						
						LEA rdi, [rsp + 512]

						TEST edx, edx ; leftover
						JZ noleftover
							MOV ecx, edx
							
							MOV rsi, r11
							SHL rax, 6
							ADD rsi, rax
							SHR rax, 6

							REP MOVSB
						noleftover:

						MOV BYTE PTR [rdi], 80h

						SHL r8, 3						

						LEA rdi, [rsp + 572]

						CMP edx, 56
						JB noextrames
							ADD rdi, 64
							MOV DWORD PTR [rsp+1032], 2
						noextrames:

						MOV [rdi-4], r8

						LEA rdx, [pr_md5] ; Ki
							
						TEST r9d, r9d
						JNZ no_rsi_load
							LEA rsi, [ph_md5]

							MOV r9, [rsi]
							MOV [r10], r9
							MOV r9, [rsi+8]
							MOV [r10+8], r9

						no_rsi_load:

													
						MOV rsi, r11 ; Mi

						MOV r15d, eax
						TEST r15d, r15d

						JNZ messageloop
							CMP DWORD PTR [rsp+1032], 0
							JZ hashingcomplete
							LEA rsi, [rsp+512]

							MOV r15d, [rsp+1032]
							MOV DWORD PTR [rsp+1032], 0

					align 16
						messageloop:
							MOV r8d, [r10] ; a
							MOV r9d, [r10+4] ; b
							MOV r11d, [r10+8] ; c
							MOV edi, [r10+12] ; d




							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi]
								ADD eax, [rdx]
							MOV r8d, ecx
							ROL eax, 7
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+4]
								ADD eax, [rdx+4]
							MOV r8d, ecx
							ROL eax, 12
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+8]
								ADD eax, [rdx+8]
							MOV r8d, ecx
							ROL eax, 17
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+12]
								ADD eax, [rdx+12]
							MOV r8d, ecx
							ROL eax, 22
							ADD r9d, eax

							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+16]
								ADD eax, [rdx+16]
							MOV r8d, ecx
							ROL eax, 7
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+20]
								ADD eax, [rdx+20]
							MOV r8d, ecx
							ROL eax, 12
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+24]
								ADD eax, [rdx+24]
							MOV r8d, ecx
							ROL eax, 17
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+28]
								ADD eax, [rdx+28]
							MOV r8d, ecx
							ROL eax, 22
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+32]
								ADD eax, [rdx+32]
							MOV r8d, ecx
							ROL eax, 7
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+36]
								ADD eax, [rdx+36]
							MOV r8d, ecx
							ROL eax, 12
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+40]
								ADD eax, [rdx+40]
							MOV r8d, ecx
							ROL eax, 17
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+44]
								ADD eax, [rdx+44]
							MOV r8d, ecx
							ROL eax, 22
							ADD r9d, eax

						
							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+48]
								ADD eax, [rdx+48]
							MOV r8d, ecx
							ROL eax, 7
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+52]
								ADD eax, [rdx+52]
							MOV r8d, ecx
							ROL eax, 12
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+56]
								ADD eax, [rdx+56]
							MOV r8d, ecx
							ROL eax, 17
							ADD r9d, eax


							MOV eax, r9d
							NOT eax
							AND eax, edi
							MOV ecx, edi
								MOV edi, r11d
								AND r11d, r9d
								OR eax, r11d
								MOV r11d, r9d							
								ADD eax, r8d
								ADD eax, [rsi+60]
								ADD eax, [rdx+60]
							MOV r8d, ecx
							ROL eax, 22
							ADD r9d, eax

; H

							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+4]
							ADD eax, [rdx+64]
							ROL eax, 5
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+24]
							ADD eax, [rdx+68]
							ROL eax, 9
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+44]
							ADD eax, [rdx+72]
							ROL eax, 14
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi]
							ADD eax, [rdx+76]
							ROL eax, 20
							ADD r9d, eax



							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+20]
							ADD eax, [rdx+80]
							ROL eax, 5
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+40]
							ADD eax, [rdx+84]
							ROL eax, 9
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+60]
							ADD eax, [rdx+88]
							ROL eax, 14
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+16]
							ADD eax, [rdx+92]
							ROL eax, 20
							ADD r9d, eax




							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+36]
							ADD eax, [rdx+96]
							ROL eax, 5
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+56]
							ADD eax, [rdx+100]
							ROL eax, 9
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+12]
							ADD eax, [rdx+104]
							ROL eax, 14
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+32]
							ADD eax, [rdx+108]
							ROL eax, 20
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+52]
							ADD eax, [rdx+112]
							ROL eax, 5
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+8]
							ADD eax, [rdx+116]
							ROL eax, 9
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+28]
							ADD eax, [rdx+120]
							ROL eax, 14
							ADD r9d, eax


							MOV ecx, edi
							MOV eax, edi
							NOT eax
							AND eax, r11d
							AND ecx, r9d
							OR eax, ecx
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+48]
							ADD eax, [rdx+124]
							ROL eax, 20
							ADD r9d, eax

; H
							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+20]
							ADD eax, [rdx+128]
							ROL eax, 4
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+32]
							ADD eax, [rdx+132]
							ROL eax, 11
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+44]
							ADD eax, [rdx+136]
							ROL eax, 16
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+56]
							ADD eax, [rdx+140]
							ROL eax, 23
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+4]
							ADD eax, [rdx+144]
							ROL eax, 4
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+16]
							ADD eax, [rdx+148]
							ROL eax, 11
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+28]
							ADD eax, [rdx+152]
							ROL eax, 16
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+40]
							ADD eax, [rdx+156]
							ROL eax, 23
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+52]
							ADD eax, [rdx+160]
							ROL eax, 4
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi]
							ADD eax, [rdx+164]
							ROL eax, 11
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+12]
							ADD eax, [rdx+168]
							ROL eax, 16
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+24]
							ADD eax, [rdx+172]
							ROL eax, 23
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+36]
							ADD eax, [rdx+176]
							ROL eax, 4
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+48]
							ADD eax, [rdx+180]
							ROL eax, 11
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+60]
							ADD eax, [rdx+184]
							ROL eax, 16
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax


							MOV eax, r9d
							XOR eax, r11d
							XOR eax, edi
							ADD eax, r8d
							ADD eax, [rsi+8]
							ADD eax, [rdx+188]
							ROL eax, 23
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD r9d, eax



; I
							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi]
							ADD eax, [rdx+192]
							ROL eax, 6
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+28]
							ADD eax, [rdx+196]
							ROL eax, 10
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+56]
							ADD eax, [rdx+200]
							ROL eax, 15
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+20]
							ADD eax, [rdx+204]
							ROL eax, 21
							ADD r9d, eax



							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+48]
							ADD eax, [rdx+208]
							ROL eax, 6
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+12]
							ADD eax, [rdx+212]
							ROL eax, 10
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+40]
							ADD eax, [rdx+216]
							ROL eax, 15
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+4]
							ADD eax, [rdx+220]
							ROL eax, 21
							ADD r9d, eax


							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+32]
							ADD eax, [rdx+224]
							ROL eax, 6
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+60]
							ADD eax, [rdx+228]
							ROL eax, 10
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+24]
							ADD eax, [rdx+232]
							ROL eax, 15
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+52]
							ADD eax, [rdx+236]
							ROL eax, 21
							ADD r9d, eax


							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+16]
							ADD eax, [rdx+240]
							ROL eax, 6
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+44]
							ADD eax, [rdx+244]
							ROL eax, 10
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+8]
							ADD eax, [rdx+248]
							ROL eax, 15
							ADD r9d, eax

							MOV eax, edi
							NOT eax
							OR eax, r9d
							XOR eax, r11d
							ADD eax, r8d
							MOV r8d, edi
							MOV edi, r11d
							MOV r11d, r9d
							ADD eax, [rsi+36]
							ADD eax, [rdx+252]
							ROL eax, 21
							ADD r9d, eax






							ADD [r10], r8d ; a
							ADD [r10+4], r9d ; b
							ADD [r10+8], r11d ; c
							ADD [r10+12], edi ; d



							ADD rsi, 64


							SUB r15d, 1
						JNZ messageloop
							CMP DWORD PTR [rsp+1032], 0
							JZ hashingcomplete
								LEA rsi, [rsp+512]

								MOV r15d, [rsp+1032]
								MOV DWORD PTR [rsp+1032], 0
								JMP messageloop

					hashingcomplete:


						
	
			MOV rsp, rbp
		POP rbp
		POP rsi
		POP rdi
		POP r15
	RET
md5	ENDP




END

