/*
** safe-fail securitat
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Securitat\Crypter.h"

extern "C" const unsigned int h256_l;
extern "C" const unsigned int r256_cl;
extern "C" const unsigned int h256_0;
extern "C" const unsigned int h256_00;
extern "C" const unsigned int r256_c;

extern "C" const unsigned long long h512_l;
extern "C" const unsigned long long r512_cl;
extern "C" const unsigned long long h512_0;
extern "C" const unsigned long long h512_00;
extern "C" const unsigned long long r512_c;

extern "C" const unsigned int pr_md5;
extern "C" const unsigned int ph_md5;

extern "C" const unsigned int crc32_table;
extern "C" const unsigned short crc16_table;



unsigned int bswap32(unsigned int ival) {
	return (ival >> 24) | (ival << 24) | ((ival >> 8) & 0xFF00) | ((ival & 0xFF00) << 8);
}

unsigned long long bswap64(unsigned long long ival) {
	return (unsigned long long)bswap32(ival>>32) | ((unsigned long long)bswap32(ival & 0xFFFFFFFF) << 32);
}


unsigned int s1(unsigned int vl) {
	return ((vl>>17) | (vl<<15)) ^ ((vl>>19) | (vl<<13)) ^ (vl>>10);
}

unsigned int s0(unsigned int vl) {
	return ((vl>>18) | (vl<<14)) ^ ((vl>>7) | (vl<<25)) ^ (vl>>3);
}

unsigned int S0(unsigned int vl) {
	return ((vl>>2) | (vl<<30)) ^ ((vl>>13) | (vl<<19)) ^ ((vl>>22) | (vl<<10));
}

unsigned int S1(unsigned int vl) {
	return ((vl>>6) | (vl<<26)) ^ ((vl>>11) | (vl<<21)) ^ ((vl>>25) | (vl<<7));
}

unsigned int Ch(unsigned int * po) {
	return (po[0] & po[1]) ^ (~po[0] & po[2]);
}

unsigned int Maj(unsigned int * po) {
	return (po[0] & po[1]) ^ (po[0] & po[2]) ^ (po[1] & po[2]);
}

unsigned long long s1_5(unsigned long long vl) {
	return ((vl>>19) | (vl<<45)) ^ ((vl>>61) | (vl<<3)) ^ (vl>>6);
}

unsigned long long s0_5(unsigned long long vl) {
	return ((vl>>1) | (vl<<63)) ^ ((vl>>8) | (vl<<56)) ^ (vl>>7);
}

unsigned long long S0_5(unsigned long long vl) {
	return ((vl>>28) | (vl<<36)) ^ ((vl>>34) | (vl<<30)) ^ ((vl>>39) | (vl<<25));
}

unsigned long long S1_5(unsigned long long vl) {
	return ((vl>>14) | (vl<<50)) ^ ((vl>>18) | (vl<<46)) ^ ((vl>>41) | (vl<<23));
}

unsigned long long Ch_5(unsigned long long * po) {
	return (po[0] & po[1]) ^ (~po[0] & po[2]);
}

unsigned long long Maj_5(unsigned long long * po) {
	return (po[0] & po[1]) ^ (po[0] & po[2]) ^ (po[1] & po[2]);
}




void Crypt::sha256(unsigned int * hash, const char * message, unsigned int mlength, unsigned int map_sel) {
	unsigned int w[64], totM(0), T1(0), T2(0), mumu[8], l0;
	char emes[128];
	const unsigned int * mess = reinterpret_cast<const unsigned int *>(message);
	const unsigned int * p_h256(&h256_l), * p_r256(&r256_cl);

	for (unsigned int i(0);i<128;i++) emes[i] = 0;
	
	switch (map_sel & 0x0F) {
		case 1:
			p_h256 = &h256_00;
			p_r256 = &r256_c;
		break;
		case 2:
			p_h256 = &h256_0;
			p_r256 = &r256_c;
		break;

	}

	
	totM = mlength / 64;

	l0 = mlength % 64;
	for (unsigned int i(0);i<l0;i++) emes[i] = message[totM*64+i];
	emes[l0] = 0x80;

	if (l0<56) {
		reinterpret_cast<unsigned int *>(emes+56)[0] = bswap32(mlength>>29);
		reinterpret_cast<unsigned int *>(emes+56)[1] = bswap32(mlength<<3);
	} else {
		reinterpret_cast<unsigned int *>(emes+120)[0] = bswap32(mlength>>29);		
		reinterpret_cast<unsigned int *>(emes+120)[1] = bswap32(mlength<<3);		
	}

	if (!(map_sel & 0x80000000)) for (unsigned int i(0);i<8;i++) hash[i] = p_h256[i];
	else for (unsigned int i(0);i<8;i++) hash[i] = bswap32(hash[i]);
	
	
	for (unsigned int mc(0);mc<totM;mc++) {
// expansion		
		for (unsigned int i(0);i<16;i++) w[i] = bswap32(mess[mc*16+i]);
		
		for (unsigned int i(16);i<64;i++) w[i] = w[i-7] + w[i-16] + s1(w[i-2]) + s0(w[i-15]);

// compression
		for (unsigned int i(0);i<8;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<64;i++) {
			T1 = mumu[7] + S1(mumu[4]) + Ch(mumu+4) + w[i] + p_r256[i];
			T2 = S0(mumu[0]) + Maj(mumu);

			mumu[7] = mumu[6];
			mumu[6] = mumu[5];
			mumu[5] = mumu[4];
			mumu[4] = T1 + mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] = mumu[0];
			mumu[0] = T1 + T2;
		}

		for (unsigned int i(0);i<8;i++) hash[i] += mumu[i];
	}

	l0 = (l0 / 56) + 1;
	mess = reinterpret_cast<unsigned int *>(emes);

	for (unsigned int mc(0);mc<l0;mc++) {
// expansion		
		for (unsigned int i(0);i<16;i++) w[i] = bswap32(mess[mc*16+i]);
		for (unsigned int i(16);i<64;i++) w[i] = w[i-7] + w[i-16] + s1(w[i-2]) + s0(w[i-15]);

// compression
		for (unsigned int i(0);i<8;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<64;i++) {
			T1 = mumu[7] + S1(mumu[4]) + Ch(mumu+4) + w[i] + p_r256[i];
			T2 = S0(mumu[0]) + Maj(mumu);

			mumu[7] = mumu[6];
			mumu[6] = mumu[5];
			mumu[5] = mumu[4];
			mumu[4] = T1 + mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] = mumu[0];
			mumu[0] = T1 + T2;
		}

		for (unsigned int i(0);i<8;i++) hash[i] += mumu[i];
	}

	for (unsigned int i(0);i<8;i++) hash[i] = bswap32(hash[i]);


}



void Crypt::sha512(unsigned long long * hash, const char * message, unsigned long long mlength, unsigned int map_sel) {
	unsigned long long w[80], totM(0), T1(0), T2(0), mumu[8], l0;
	char emes[256];
	const unsigned long long * mess = reinterpret_cast<const unsigned long long *>(message);
	const unsigned long long * p_h512(&h512_l), * p_r512(&r512_cl);

	for (unsigned int i(0);i<256;i++) emes[i] = 0;

	switch (map_sel & 0x0F) {
		case 1:
			p_h512 = &h512_00;
			p_r512 = &r512_c;
		break;
		case 2:
			p_h512 = &h512_0;
			p_r512 = &r512_c;
		break;

	}

	
	totM = mlength / 128;

	l0 = mlength % 128;
	for (unsigned int i(0);i<l0;i++) emes[i] = message[totM*128+i];
	emes[l0] = 0x80;

	if (l0<112) {
		reinterpret_cast<unsigned long long *>(emes+112)[0] = bswap64(mlength>>61);
		reinterpret_cast<unsigned long long *>(emes+112)[1] = bswap64(mlength<<3);
	} else {
		reinterpret_cast<unsigned long long *>(emes+240)[0] = bswap64(mlength>>61);
		reinterpret_cast<unsigned long long *>(emes+240)[1] = bswap64(mlength<<3);
	}

	if (map_sel & 0x80000000) for (unsigned int i(0);i<8;i++) hash[i] = bswap64(hash[i]);
	else for (unsigned int i(0);i<8;i++) hash[i] = p_h512[i];
	
	
	for (unsigned int mc(0);mc<totM;mc++) {
// expansion		
		for (unsigned int i(0);i<16;i++) w[i] = bswap64(mess[mc*16+i]);

		for (unsigned int i(16);i<80;i++) w[i] = w[i-7] + w[i-16] + s1_5(w[i-2]) + s0_5(w[i-15]);

// compression
		for (unsigned int i(0);i<8;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<80;i++) {
			T1 = mumu[7] + S1_5(mumu[4]) + Ch_5(mumu+4) + w[i] + p_r512[i];
			T2 = S0_5(mumu[0]) + Maj_5(mumu);

			mumu[7] = mumu[6];
			mumu[6] = mumu[5];
			mumu[5] = mumu[4];
			mumu[4] = T1 + mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] = mumu[0];
			mumu[0] = T1 + T2;
		}

		for (unsigned int i(0);i<8;i++) hash[i] += mumu[i];
	}

	l0 = (l0 / 112) + 1;
	mess = reinterpret_cast<unsigned long long *>(emes);

	for (unsigned long long mc(0);mc<l0;mc++) {
// expansion		
		for (unsigned int i(0);i<16;i++) w[i] = bswap64(mess[mc*16+i]);

		for (unsigned int i(16);i<80;i++) w[i] = w[i-7] + w[i-16] + s1_5(w[i-2]) + s0_5(w[i-15]);

// compression
		for (unsigned int i(0);i<8;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<80;i++) {
			T1 = mumu[7] + S1_5(mumu[4]) + Ch_5(mumu+4) + w[i] + p_r512[i];
			T2 = S0_5(mumu[0]) + Maj_5(mumu);

			mumu[7] = mumu[6];
			mumu[6] = mumu[5];
			mumu[5] = mumu[4];
			mumu[4] = T1 + mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] = mumu[0];
			mumu[0] = T1 + T2;
		}

		for (unsigned int i(0);i<8;i++) hash[i] += mumu[i];
	}

	for (unsigned int i(0);i<8;i++) hash[i] = bswap64(hash[i]);

}

// md5 =================================================================================================================================================================================


const unsigned int ro_md5[] = {	7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
								5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
								4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
								6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
							};

unsigned int F_md5(unsigned int * po) {
	return (po[0] & po[1]) | (~po[0] & po[2]);
}

unsigned int G_md5(unsigned int * po) {
	return (po[0] & po[2]) | (po[1] & (~po[2]));
}

unsigned int H_md5(unsigned int * po) {
	return (po[0] ^ po[1] ^ po[2]);
}

unsigned int I_md5(unsigned int * po) {
	return (po[1] ^ (po[0] | (~po[2])));
}

unsigned int roleft32(unsigned int ival, unsigned int sval) {
	return (ival << sval) | (ival >> (32-sval));
}

void Crypt::slow_md5(unsigned int * hash, const char * message, unsigned int mlength, bool hist) {
	unsigned int totM(0), OO(0), mumu[4], l0;
	const unsigned int * p_r5(&pr_md5), * p_h5(&ph_md5);
	char emes[128];
	const unsigned int * mess = reinterpret_cast<const unsigned int *>(message);

	for (unsigned int i(0);i<128;i++) emes[i] = 0;	
	
	totM = mlength / 64;

	l0 = mlength % 64;
	for (unsigned int i(0);i<l0;i++) emes[i] = message[totM*64+i];
	emes[l0] = 0x80;

	if (l0<56) {
		reinterpret_cast<unsigned int *>(emes+56)[0] = mlength<<3;
		reinterpret_cast<unsigned int *>(emes+56)[1] = mlength>>29;
	} else {
		reinterpret_cast<unsigned int *>(emes+120)[0] = mlength<<3;
		reinterpret_cast<unsigned int *>(emes+120)[1] = mlength>>29;
	}

	if (!hist) for (unsigned int i(0);i<4;i++) hash[i] = p_h5[i];
	
	
	for (unsigned int mc(0);mc<totM;mc++) {
		for (unsigned int i(0);i<4;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<64;i++) {
			OO = mumu[0] + p_r5[i];
			switch (i>>4) {
				case 0:	OO += F_md5(mumu+1) + mess[mc*16+i]; break;
				case 1:	OO += G_md5(mumu+1) + mess[mc*16+(5*i+1)%16]; break;
				case 2:	OO += H_md5(mumu+1) + mess[mc*16+(3*i+5)%16]; break;
				case 3:	OO += I_md5(mumu+1) + mess[mc*16+(7*i)%16]; break;

			}

			mumu[0] = mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] += roleft32(OO, ro_md5[i]);
			
		}

		for (unsigned int i(0);i<4;i++) hash[i] += mumu[i];
	}

	l0 = (l0 / 56) + 1;
	mess = reinterpret_cast<unsigned int *>(emes);

	for (unsigned int mc(0);mc<l0;mc++) {
		for (unsigned int i(0);i<4;i++) mumu[i] = hash[i];

		for (unsigned int i(0);i<64;i++) {
			OO = mumu[0] + p_r5[i];
			switch (i>>4) {
				case 0:	OO += F_md5(mumu+1) + mess[mc*16+i]; break;
				case 1:	OO += G_md5(mumu+1) + mess[mc*16+(5*i+1)%16]; break;
				case 2:	OO += H_md5(mumu+1) + mess[mc*16+(3*i+5)%16]; break;
				case 3:	OO += I_md5(mumu+1) + mess[mc*16+(7*i)%16]; break;

			}

			mumu[0] = mumu[3];
			mumu[3] = mumu[2];
			mumu[2] = mumu[1];
			mumu[1] += roleft32(OO, ro_md5[i]);
		}

		for (unsigned int i(0);i<4;i++) hash[i] += mumu[i];
	}

}


// CRC ================================================================================================================================================================
void Crypt::CRC_16s(unsigned short & crc_val, const unsigned char * d_buf, unsigned int d_len) {
	const unsigned short * p_crct(&crc16_table);

	crc_val ^= -1;
	for (unsigned int i(0);i<d_len;i++) crc_val = p_crct[(crc_val ^ d_buf[i]) & 0xFF] ^ (crc_val >> 8);	
	crc_val ^= -1;

}

void Crypt::CRC_32s(unsigned int & crc_val, const unsigned char * d_buf, unsigned int d_len) {
	const unsigned int * p_crct(&crc32_table);
	
	crc_val ^= -1;
	for (unsigned int i(0);i<d_len;i++) crc_val = p_crct[(crc_val ^ d_buf[i]) & 0xFF] ^ (crc_val >> 8);	
	crc_val ^= -1;
}

// adler-32 =============================================================================================================================================================

void Crypt::ADLER_32s(unsigned int & crc_val, const unsigned char * d_buf, unsigned int d_len) {
	unsigned int a_val(crc_val & 0xFFFF), b_val(crc_val >> 16);
	
	for (unsigned int i(0);i<d_len;i++) {
		a_val += d_buf[i];
		b_val += a_val;

		a_val %= 65521;
		b_val %= 65521;	
	}

	crc_val = a_val | (b_val << 16);
}
