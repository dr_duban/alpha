/*
** safe-fail data stream
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "CoreLibrary\SFScore.h"
#include "System\Memory.h"


#define STREAM_REPEAT_FLAG		0x0000000100000000
#define STREAM_BUFFER_FAILURE	0x0000000080000000

namespace Data {
	class Stream : public SFSco::Object {
		protected:
			static Memory::Allocator * stream_pool;

			struct Header {
				UI_64 _status, _size;
			};			

			static UI_64 Initialize() {
				wchar_t pool_name[] = {11, L's',L't', L'r', L'e', L'a', L'm', L' ', L'p', L'o', L'o', L'l'};
				UI_64 result(0);

				if (stream_pool) return -1;

				if (stream_pool = new Memory::Allocator(0)) {
					result = stream_pool->Init(new Memory::Buffer(), 0x040000, pool_name);
				} else result = ERROR_OUTOFMEMORY;
				
				return result;			
			}

			static UI_64 Finalize() {
				if (stream_pool) delete stream_pool;
				stream_pool = 0;

				return 0;
			}

		public:	
			UI_64 GetStatus() {
				UI_64 result(0);

				if (Header * s_hdr = reinterpret_cast<Header *>(Acquire())) {
					result = s_hdr->_status;
					Release();
				}

				return result;
			}			

			virtual unsigned char * JumpLock(UI_64 &, UI_64) {return 0;};
			virtual unsigned char * MoveLock(UI_64 &, UI_64) {return 0;};
			virtual unsigned char * MoveLock(UI_64 &, double) {return 0;};
			virtual unsigned char * IncLock(UI_64 &) {return 0;};
			virtual unsigned char * DecLock(UI_64 &) {return 0;};
			virtual void Reset() {};

			virtual UI_64 Write(const void *, UI_64) {return 0;};

			virtual bool NoError() {
				bool result(false);

				if (Header * s_hdr = reinterpret_cast<Header *>(Acquire())) {
					result = ((s_hdr->_status & 0xFFFFFFFF) == 0);
					Release();
				}
				
				return result;
			}

			virtual UI_64 GetSize() {
				UI_64 result(0);

				if (Header * s_hdr = reinterpret_cast<Header *>(Acquire())) {
					result = s_hdr->_size;
					Release();
				}

				return result;
			}

	};



}


