/*
** safe-fail data stream
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "CoreLibrary\SFScore.h"
#include "Data\Stream.h"

#ifdef _DEBUG
	#include <iostream>
#endif

namespace Data {
	class Socket: public Stream {
		private:
			static const UI_64 _tid = 961749317;

			unsigned int _flags, t_seq[8];

			sockaddr_in _addr4, _destination4;
			sockaddr_in6 _addr6, _destination6;
			
			sockaddr * _destination, * _self;

			int da_size;

			SOCKET _socket;
			HANDLE read_thread;

			static DWORD __stdcall ReadLoop(void *);

		protected:
			UI_64 access_mark;
//			SFSco::FIFO data_queue;
			unsigned int _mpsize;

			virtual UI_64 ReadProcessor(char * d_ptr, unsigned int d_size, const sockaddr * saddr) {
#ifdef _DEBUG
	std::cout << std::endl << "source IP:\t" << inet_ntoa(reinterpret_cast<const sockaddr_in *>(saddr)->sin_addr) << ":" << reinterpret_cast<const sockaddr_in *>(saddr)->sin_port << std::endl << "pack size:\t" << d_size << std::endl;

#endif
				return 0;
			};
			
		public:
			Socket();
			virtual ~Socket();


			virtual unsigned char * IncLock(UI_64 &) {
				return 0;
			};			
			virtual unsigned char * DecLock(UI_64 & pop) {
				return 0;
			};
			virtual unsigned char * MoveLock(UI_64 & pop, UI_64) {
				return 0;
			};
			virtual unsigned char * MoveLock(UI_64 & pop, double) {
				return 0;
			};
			virtual unsigned char * JumpLock(UI_64 & pop, UI_64) {
				return 0;
			};


			virtual void Unlock() {};
			
			virtual UI_64 Write(const void *, UI_64);

			UI_64 Init(unsigned short family, int stype, int sprotocol, sockaddr * = 0);
			
			unsigned int GetAddr(sockaddr *) const;
			
			bool ISDST(const sockaddr *);
			bool ISELF(const sockaddr *);

			void SetSelfDest();
			int SetDestination(const sockaddr *);
			int SetDestination(const char *);
			int SetDestination(const wchar_t *);
	};



}

