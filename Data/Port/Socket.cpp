/*
** safe-fail data stream
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <WinSock2.h>
#include <ws2tcpip.h>


#define SO_MAXDG                    0x7009

#include "Data\Socket.h"
#include "System\SysUtils.h"


#define MAX_BUS_INTERFACES	16

Data::Socket::Socket() : _socket(INVALID_SOCKET), _mpsize(1), read_thread(0), _flags(0), access_mark(0), _destination(0), _self(0), da_size(0) {
	_addr4.sin_family = 0;
	_addr4.sin_port = 0;
	_addr4.sin_addr.S_un.S_addr = 0;
	_addr4.sin_zero[0] = _addr4.sin_zero[1] = _addr4.sin_zero[2] = _addr4.sin_zero[3] = _addr4.sin_zero[4] = _addr4.sin_zero[5] = _addr4.sin_zero[6] = _addr4.sin_zero[7] = 0;

	_destination4.sin_family = 0;
	_destination4.sin_port = 0;
	_destination4.sin_addr.S_un.S_addr = 0;	
	_destination4.sin_zero[0] = _destination4.sin_zero[1] = _destination4.sin_zero[2] = _destination4.sin_zero[3] = _destination4.sin_zero[4] = _destination4.sin_zero[5] = _destination4.sin_zero[6] = _destination4.sin_zero[7] = 0;

	_addr6.sin6_family = 0;
	_addr6.sin6_port = 0;
	_addr6.sin6_flowinfo = 0;
	_addr6.sin6_addr.u.Word[0] = _addr6.sin6_addr.u.Word[1] = _addr6.sin6_addr.u.Word[2] = _addr6.sin6_addr.u.Word[3] = _addr6.sin6_addr.u.Word[4] = _addr6.sin6_addr.u.Word[5] = _addr6.sin6_addr.u.Word[6] = _addr6.sin6_addr.u.Word[7] = 0;
	_addr6.sin6_scope_id = 0;

	_destination6.sin6_family = 0;	
	_destination6.sin6_port = 0;
	_destination6.sin6_flowinfo = 0;
	_destination6.sin6_addr.u.Word[0] = _destination6.sin6_addr.u.Word[1] = _destination6.sin6_addr.u.Word[2] = _destination6.sin6_addr.u.Word[3] = _destination6.sin6_addr.u.Word[4] = _destination6.sin6_addr.u.Word[5] = _destination6.sin6_addr.u.Word[6] = _destination6.sin6_addr.u.Word[7] = 0;
	_destination6.sin6_scope_id = 0;
	
}

Data::Socket::~Socket() {

	if (_socket != INVALID_SOCKET) {		

		if (read_thread) {
			SetDestination(_self);
			Write(t_seq, 32);

			WaitForSingleObject(read_thread, INFINITE);
			CloseHandle(read_thread);
		}

		closesocket(_socket);
	}
}


UI_64 Data::Socket::Init(unsigned short family, int stype, int sprotocol, sockaddr * raddr) {
	DWORD result(0);
	unsigned int hval(0),iii(0);
	unsigned int local(0);
	int sa_size(0);

	INTERFACE_INFO ifo[MAX_BUS_INTERFACES];

	if (family == AF_INET) {
		_addr4.sin_family = AF_INET;
		if (raddr) {
			_addr4.sin_port = reinterpret_cast<sockaddr_in *>(raddr)->sin_port;
			_addr4.sin_addr.S_un.S_addr = reinterpret_cast<sockaddr_in *>(raddr)->sin_addr.S_un.S_addr;
						
		} else {
			_addr4.sin_port = 0;
			_addr4.sin_addr.S_un.S_addr = 0;
		}

		_addr4.sin_zero[0] = _addr4.sin_zero[1] = _addr4.sin_zero[2] = _addr4.sin_zero[3] = _addr4.sin_zero[4] = _addr4.sin_zero[5] = _addr4.sin_zero[6] = _addr4.sin_zero[7] = 0;

		_self = reinterpret_cast<sockaddr *>(&_addr4);
		sa_size = sizeof(sockaddr_in);
	} else {
		if (family == AF_INET6) {
			_addr6.sin6_family = AF_INET6;
			if (raddr) {
				_addr6.sin6_port = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_port;
				_addr6.sin6_flowinfo = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_flowinfo;

				_addr6.sin6_addr.u.Word[0] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[0];
				_addr6.sin6_addr.u.Word[1] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[1];
				_addr6.sin6_addr.u.Word[2] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[2];
				_addr6.sin6_addr.u.Word[3] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[3];
				_addr6.sin6_addr.u.Word[4] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[4];
				_addr6.sin6_addr.u.Word[5] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[5];
				_addr6.sin6_addr.u.Word[6] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[6];
				_addr6.sin6_addr.u.Word[7] = reinterpret_cast<sockaddr_in6 *>(raddr)->sin6_addr.u.Word[7];

			} else {
				_addr6.sin6_port = 0;
				_addr6.sin6_flowinfo = 0;
				_addr6.sin6_addr.u.Word[0] = _addr6.sin6_addr.u.Word[1] = _addr6.sin6_addr.u.Word[2] = _addr6.sin6_addr.u.Word[3] = _addr6.sin6_addr.u.Word[4] = _addr6.sin6_addr.u.Word[5] = _addr6.sin6_addr.u.Word[6] = _addr6.sin6_addr.u.Word[7] = 0;
			}

			_addr6.sin6_scope_id = 0;
			_self = reinterpret_cast<sockaddr *>(&_addr6);
			sa_size = sizeof(sockaddr_in6);
		} else {
			// error

		}

	}
	

	SOCKET tsock = WSASocket(family, stype, sprotocol, 0, 0, WSA_FLAG_OVERLAPPED);
	if (tsock != INVALID_SOCKET) {
		System::MemoryFill(ifo, MAX_BUS_INTERFACES*sizeof(INTERFACE_INFO), 0);
		
		result = MAX_BUS_INTERFACES*sizeof(INTERFACE_INFO);

		if (!WSAIoctl(tsock, SIO_GET_INTERFACE_LIST, 0, 0, ifo, result, &result, 0, 0)) {
				
			hval = result/sizeof(INTERFACE_INFO);
			if (hval>MAX_BUS_INTERFACES) hval = MAX_BUS_INTERFACES;
			result = 0;

			if (family == AF_INET) {
				for (iii=0;iii<hval;iii++) {
					if ((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr != 0x0100007F) && (ifo[iii].iiFlags & IFF_UP)) {
						
						if (((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr & 0xFF) == 10) || ((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr & 0xFFFF) == 0xA8C0) || ((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr & 0xFF) == 172) && (((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr & 0xFF00) >= 0x1000) && ((ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr & 0xFF00) < 0x2000))) {
							local = iii;
						} else {
							break;
						}
			
					}
				}
			} else {


			}

				
			if (iii >= hval) iii = local;

			if (family == AF_INET) {
				_addr4.sin_addr.S_un.S_addr = ifo[iii].iiAddress.AddressIn.sin_addr.S_un.S_addr;
			} else {
				_addr6.sin6_addr.u.Word[0] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[0];
				_addr6.sin6_addr.u.Word[1] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[1];
				_addr6.sin6_addr.u.Word[2] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[2];
				_addr6.sin6_addr.u.Word[3] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[3];
				_addr6.sin6_addr.u.Word[4] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[4];
				_addr6.sin6_addr.u.Word[5] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[5];
				_addr6.sin6_addr.u.Word[6] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[6];
				_addr6.sin6_addr.u.Word[7] = ifo[iii].iiAddress.AddressIn6.sin6_addr.u.Word[7];
			//	sa_size = sizeof(sockaddr_in6_old);
			}
					
		}
		closesocket(tsock);
	}

	_socket = WSASocket(family, stype, sprotocol, 0, 0, WSA_FLAG_OVERLAPPED);

	if (SOCKET_ERROR == bind(_socket, _self, sa_size)) {
		result = WSAGetLastError();
		closesocket(_socket);
		_socket = INVALID_SOCKET;
	} else {
					
		getsockname(_socket, _self, &sa_size);
		iii = 4;
		getsockopt(_socket, SOL_SOCKET, SO_MAXDG, reinterpret_cast<char *>(&_mpsize), (int *)&iii);

		SFSco::sha256(t_seq, reinterpret_cast<char *>(_self), sa_size, 1);
				
#ifdef _DEBUG
	std::cout << std::endl << "current cfg:\t" << inet_ntoa(reinterpret_cast<sockaddr_in *>(_self)->sin_addr) << ":" << reinterpret_cast<sockaddr_in *>(_self)->sin_port << std::endl;

#endif

	}


	if (!result) {
		if (read_thread = CreateThread(0, 0x20000, ReadLoop, this, 0, 0)) {
			access_mark = GetTickCount64();
		} else {
			result = GetLastError();
		}
	}

	return result;
}


UI_64 Data::Socket::Write(const void * xbuf, UI_64 bsize) {
	DWORD result(0);
	WSABUF sbuf;

	sbuf.len = bsize;
	sbuf.buf = reinterpret_cast<char *>(const_cast<void *>(xbuf));

	if ((_socket != INVALID_SOCKET) && (_destination) && (da_size)) {
		if (WSASendTo(_socket, &sbuf, 1, &result, 0, _destination, da_size, 0, 0) == SOCKET_ERROR) { // &_olapped
			result = WSAGetLastError();
		}
	}

	return result;
}

DWORD __stdcall Data::Socket::ReadLoop(void * cntx) {
	char __declspec(align(16)) read_buf[65536];
	DWORD result(0), terminator(1);
	
	sockaddr * xaddr(reinterpret_cast<sockaddr *>(read_buf+16));
	int asize(sizeof(sockaddr_in6));
	WSABUF sbuf;
	sbuf.buf = read_buf+128;
	sbuf.len = 40960;

	reinterpret_cast<UI_64 *>(read_buf)[0] = 128;
	reinterpret_cast<UI_64 *>(read_buf)[1] = 0;


	if (Socket * xsock = reinterpret_cast<Socket *>(cntx)) {
		for (;terminator;) {
			if (WSARecvFrom(xsock->_socket, &sbuf, 1, &result, (LPDWORD)&xsock->_flags, xaddr, &asize, 0, 0) == 0) {
				if (!xsock->ISELF(xaddr)) {
					result = xsock->ReadProcessor(sbuf.buf, result, xaddr);
					xsock->access_mark = GetTickCount64();
					//_flags = MSG_PARTIAL;
				} else {
					if (result == 32) {
						terminator = 0;

						for (unsigned int i(0);i<8;i++) {
							if (xsock->t_seq[i] != reinterpret_cast<unsigned int *>(sbuf.buf)[i]) {
								terminator = 1;
								break;
							}
						}
					}
				}				
				
			} else {
				result = WSAGetLastError();
			}
		}
	}

	return result;

}


unsigned int Data::Socket::GetAddr(sockaddr * aout) const {
	
	if (_self)
	if (_self->sa_family == AF_INET) {
		System::MemoryCopy(aout, _self, sizeof(sockaddr_in));
		return sizeof(sockaddr_in);
	} else {
		System::MemoryCopy(aout, _self, sizeof(sockaddr_in6));
		return sizeof(sockaddr_in6);
	}

	return 0;
}


bool Data::Socket::ISDST(const sockaddr * taddr) {
	bool result(false);

	if (_destination)
	if (taddr->sa_family == _destination->sa_family) {
		if (_destination->sa_family == AF_INET) {
			if ((_destination4.sin_port == reinterpret_cast<const sockaddr_in *>(taddr)->sin_port) && (_destination4.sin_addr.S_un.S_addr == reinterpret_cast<const sockaddr_in *>(taddr)->sin_addr.S_un.S_addr)) result = true;
		} else {
			if (_destination->sa_family == AF_INET6) {
				if (Strings::SearchBlock(_destination, 24, taddr, 24)) result = true;
			}
		}
	}

	return result;
}

bool Data::Socket::ISELF(const sockaddr * taddr) {
	bool result(false);

	if (taddr->sa_family == AF_INET) {
		if ((_addr4.sin_port == reinterpret_cast<const sockaddr_in *>(taddr)->sin_port) && (_addr4.sin_addr.S_un.S_addr == reinterpret_cast<const sockaddr_in *>(taddr)->sin_addr.S_un.S_addr)) result = true;
	} else {
		if (taddr->sa_family == AF_INET6) {
			if (Strings::SearchBlock(&_addr6, 24, taddr, 24)) result = true;
		}		
	}

	return result;
}

void Data::Socket::SetSelfDest() {
	_destination = _self;
}

int Data::Socket::SetDestination(const sockaddr * dst) {
	int result = 0;

	if (dst->sa_family == AF_INET) {
		_destination4.sin_family = AF_INET;
		_destination4.sin_port = reinterpret_cast<const sockaddr_in *>(dst)->sin_port;
		_destination4.sin_addr.S_un.S_addr = reinterpret_cast<const sockaddr_in *>(dst)->sin_addr.S_un.S_addr;		

		_destination = reinterpret_cast<sockaddr *>(&_destination4);
		result = da_size = sizeof(sockaddr_in);
	} else {
		if (dst->sa_family == AF_INET6) {
			_destination6.sin6_family = AF_INET6;
			_destination6.sin6_port = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_port;
			_destination6.sin6_flowinfo = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_flowinfo;

			_destination6.sin6_addr.u.Word[0] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[0];
			_destination6.sin6_addr.u.Word[1] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[1];
			_destination6.sin6_addr.u.Word[2] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[2];
			_destination6.sin6_addr.u.Word[3] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[3];
			_destination6.sin6_addr.u.Word[4] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[4];
			_destination6.sin6_addr.u.Word[5] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[5];
			_destination6.sin6_addr.u.Word[6] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[6];
			_destination6.sin6_addr.u.Word[7] = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_addr.u.Word[7];

			_destination6.sin6_scope_id = reinterpret_cast<const sockaddr_in6 *>(dst)->sin6_scope_id;

			_destination = reinterpret_cast<sockaddr *>(&_destination6);
			result = da_size = sizeof(sockaddr_in6);

		} else {
			_destination = 0;
			da_size = 0;
		}
	}

	return result;
}

int Data::Socket::SetDestination(const char * astr) {

	return 0;
}

int Data::Socket::SetDestination(const wchar_t * astr) {

	return 0;
}


