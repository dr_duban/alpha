/*
** safe-fail data stream
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include <Windows.h>
#include "Data\Stream.h"
#include "CoreLibrary\SFScore.h"
#include "System\Control.h"

#define FILE_READER		0x00000001
#define FILE_WRITER		0x00000002


namespace Data {
	class File: public Stream {
		private:

			static const UI_64 _tid = 961749307;
			static const UI_64 service_queue_tid = 961749253;
			static HANDLE service_thread; //  , service_light;
			static UI_64 terminator;
			static Control::SimpleQueue service_queue;
			static DWORD __stdcall ServiceLoop(void *);
			
			struct Header : public Stream::Header {
				struct Page {
					static const DWORD default_size = 0x01000000;
					unsigned char * _root;

					LARGE_INTEGER file_offset;
					UI_64 _pointer, _top, _nneset;

				} page[2];

				HANDLE _handle, _service_block;
				UI_64 reserved_size;				
				UI_64 disco_size;
				void * disco_ptr;

				DWORD _current_page;
				int compact, _type, direction, map_break;


				Page & LoadPage(UI_64, UI_64);				

				Page & NextPage(UI_64, UI_64 = -1);
				Page & PrevPage(UI_64, UI_64 = -1);

			};

			void PageFlip();
			void PageInc();
			void PageDec();


		public:
			static UI_64 Initialize();
			static UI_64 Finalize();
			
			static UI_64 Open(File &, const wchar_t *);
					

			virtual unsigned char * JumpLock(UI_64 &, UI_64);
			virtual unsigned char * MoveLock(UI_64 &, UI_64);
			virtual unsigned char * MoveLock(UI_64 &, double);
			virtual unsigned char * IncLock(UI_64 & r_size);
			virtual unsigned char * DecLock(UI_64 & r_size);


			virtual UI_64 Write(const void *, UI_64);

			virtual void Destroy();
	};




}

