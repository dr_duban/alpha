#include "Data\Files.h"
#include "System\SysUtils.h"

#include "System\Memory.h"

HANDLE Data::File::service_thread = 0;

UI_64 Data::File::terminator = 1;

Control::SimpleQueue Data::File::service_queue;

Memory::Allocator * Data::Stream::stream_pool = 0;



DWORD __stdcall Data::File::ServiceLoop(void *) {
	UI_64 ii(0);
	I_64 pool_index[2*MAX_POOL_COUNT];

	File fles;

	Memory::Chain::RegisterThread(pool_index);


	if (HANDLE service_light = CreateEvent(0, 0, 0, 0)) {

		for (; terminator;) {
			if (ii = service_queue.Pull(&fles)) {
				for (;; ii = service_queue.Pull(&fles)) {
					if (Header * f_hdr = reinterpret_cast<Header *>(fles.Acquire())) {

						if (f_hdr->_type) { // increment
							f_hdr->NextPage(f_hdr->_current_page ^ 1, f_hdr->page[f_hdr->_current_page].file_offset.QuadPart);
						}
						else {
							f_hdr->PrevPage(f_hdr->_current_page ^ 1, f_hdr->page[f_hdr->_current_page].file_offset.QuadPart);
						}

						f_hdr->_status = 0;
						SetEvent(f_hdr->_service_block);

						fles.Release();
					}
					if (ii == 2) break;
				}

			}
			else {
				WaitForSingleObject(service_light, 67);
			}
		}

		CloseHandle(service_light);
	}	

	Memory::Chain::UnregisterThread();

	return 0;
}

UI_64 Data::File::Initialize() {
	UI_64 result = Stream::Initialize();
	wchar_t q_name[] = {18, L'f', L'i', L'l', L'e', L' ', L's', L'e', L'r', L'v', L'i', L'c', L'e', L' ', L'q', L'u', L'e', L'u', L'e'};
	
	if (!result)
	if (!service_thread) {
		service_thread = CreateThread(0, 0x0020000, ServiceLoop, 0, 0, 0);
			
		if (!service_thread) {
			result = GetLastError();
		}

		if (!result) {
			result = service_queue.Init(sizeof(File), 512, q_name, SFSco::mem_mgr, service_queue_tid);

			if (result != -1) {
				result = 0;
			}

		}
	}

	return result;
}

UI_64 Data::File::Finalize() {
	if (service_thread) {
		terminator = 0;
		WaitForSingleObject(service_thread, INFINITE);

		CloseHandle(service_thread);

		service_thread = 0;
		
		service_queue.Destroy();
	}

	Stream::Finalize();

	return 0;
}


void Data::File::Destroy() {
	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		__super::Destroy();
		
		SpinWaitRC(1);

		if (f_hdr->_service_block) {
//			WaitForSingleObject(_service_block, 360000);
			CloseHandle(f_hdr->_service_block);
		}

		if (f_hdr->disco_ptr) {
			VirtualFree(f_hdr->disco_ptr, 0, MEM_RELEASE);
		}		
			
		if (f_hdr->_handle != INVALID_HANDLE_VALUE) CloseHandle(f_hdr->_handle);

		if (f_hdr->page[0]._root) VirtualFree(f_hdr->page[0]._root, 0, MEM_RELEASE);
		if (f_hdr->page[1]._root) VirtualFree(f_hdr->page[1]._root, 0, MEM_RELEASE);

		Release();
	}
	
}

unsigned char * Data::File::IncLock(UI_64 & r_size) {
	unsigned char * result(0);
	UI_64 lover(0);


	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		f_hdr->_status = 0;
		
		if ((f_hdr->map_break == 1) && (r_size <= f_hdr->disco_size)) {
			f_hdr->map_break = 2;
			result = reinterpret_cast<unsigned char *>(f_hdr->disco_ptr);
		
		} else {
			f_hdr->map_break = 0;
		}

		if (!result)
		if (r_size <= Header::Page::default_size) {
			if (r_size <= f_hdr->reserved_size) {
				if ((r_size + f_hdr->page[f_hdr->_current_page]._pointer) <= f_hdr->page[f_hdr->_current_page]._top) {
					result = f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer;
				
					f_hdr->page[f_hdr->_current_page]._pointer += r_size;

					if (!f_hdr->compact)
					if ((f_hdr->page[f_hdr->_current_page]._nneset) && (f_hdr->page[f_hdr->_current_page]._pointer > (f_hdr->page[f_hdr->_current_page]._top>>1))) PageInc();
				} else {
					if (f_hdr->page[f_hdr->_current_page]._pointer != f_hdr->page[f_hdr->_current_page]._top) {
						if (r_size > f_hdr->disco_size) {
							if (f_hdr->disco_ptr) {
								VirtualFree(f_hdr->disco_ptr, 0, MEM_RELEASE);
							}
				
							if (f_hdr->disco_ptr = VirtualAlloc(0, r_size, MEM_COMMIT, PAGE_READWRITE)) {
								f_hdr->disco_size = r_size;
							} else {
								f_hdr->_status = GetLastError();
							}
						}

						if (result = reinterpret_cast<unsigned char *>(f_hdr->disco_ptr)) {
							f_hdr->_status = 0;
							lover = f_hdr->page[f_hdr->_current_page]._top - f_hdr->page[f_hdr->_current_page]._pointer;
							
							System::MemoryCopy(result, f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer, lover);

							if (f_hdr->page[f_hdr->_current_page]._top != f_hdr->reserved_size) {
								PageFlip();

								f_hdr->page[f_hdr->_current_page]._pointer = r_size-lover;
								System::MemoryCopy(result + lover, f_hdr->page[f_hdr->_current_page]._root, f_hdr->page[f_hdr->_current_page]._pointer);
							} else {
								f_hdr->page[f_hdr->_current_page]._pointer = f_hdr->page[f_hdr->_current_page]._top;
								r_size = lover;
							}


							f_hdr->map_break = 2;				
				
						} else {
							f_hdr->_status = STREAM_BUFFER_FAILURE;
						}

					} else {
						if ((f_hdr->page[f_hdr->_current_page]._top == f_hdr->reserved_size) || (f_hdr->page[f_hdr->_current_page]._top < Header::Page::default_size)) {
							f_hdr->_status = STREAM_REPEAT_FLAG;
						}

						if (!f_hdr->direction) PageInc();
						PageFlip();

						if (r_size >  f_hdr->page[f_hdr->_current_page]._top) {
							r_size = f_hdr->page[f_hdr->_current_page]._top;
						}

						result = f_hdr->page[f_hdr->_current_page]._root;
						f_hdr->page[f_hdr->_current_page]._pointer = r_size;
					}
				}
			} else {
				if (f_hdr->page[f_hdr->_current_page]._pointer == f_hdr->page[f_hdr->_current_page]._top) {
					f_hdr->_status = STREAM_REPEAT_FLAG;
				}

				f_hdr->page[f_hdr->_current_page]._pointer = f_hdr->page[f_hdr->_current_page]._top;
				r_size = f_hdr->page[f_hdr->_current_page]._top;
				result = f_hdr->page[f_hdr->_current_page]._root;
			}
		}

		f_hdr->direction = 1;


		Release();
	}

	return result;
}

unsigned char * Data::File::DecLock(UI_64 & r_size) {
	unsigned char * result(0);
	UI_64 lover(0);


	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		f_hdr->_status = 0;

		if ((f_hdr->map_break == 1) && (f_hdr->disco_ptr)) {
			f_hdr->map_break = 2;
			result = reinterpret_cast<unsigned char *>(f_hdr->disco_ptr);
		} else {
			f_hdr->map_break = 0;
		}

		if (!result)
		if (r_size <= Header::Page::default_size) {
			if (r_size <= f_hdr->reserved_size) {
				if (f_hdr->page[f_hdr->_current_page]._pointer >= r_size) {
					f_hdr->page[f_hdr->_current_page]._pointer -= r_size;
					result = f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer;
		
					if (!f_hdr->compact)
					if ((f_hdr->page[f_hdr->_current_page]._nneset) && (f_hdr->page[f_hdr->_current_page]._pointer < (f_hdr->page[f_hdr->_current_page]._top>>1))) PageDec();
				} else {
					if (f_hdr->page[f_hdr->_current_page]._pointer) {
						if (r_size > f_hdr->disco_size) {
							if (f_hdr->disco_ptr) {
								VirtualFree(f_hdr->disco_ptr, 0, MEM_RELEASE);					
							}
				
							if (f_hdr->disco_ptr = VirtualAlloc(0, r_size, MEM_COMMIT, PAGE_READWRITE)) {
								f_hdr->disco_size = r_size;
							} else {
								f_hdr->_status = GetLastError();
							}
						}

						if (result = reinterpret_cast<unsigned char *>(f_hdr->disco_ptr)) {
							f_hdr->_status = 0;
							lover = r_size - f_hdr->page[f_hdr->_current_page]._pointer;
								
							System::MemoryCopy(result + lover, f_hdr->page[f_hdr->_current_page]._root, f_hdr->page[f_hdr->_current_page]._pointer);


							if (f_hdr->page[f_hdr->_current_page]._top <= Header::Page::default_size) {
								if (f_hdr->direction) PageDec();
							}
							
							PageFlip();

							f_hdr->page[f_hdr->_current_page]._pointer = f_hdr->page[f_hdr->_current_page]._top - lover;
							System::MemoryCopy(result, f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer, lover);				

							f_hdr->map_break = 2;
				
						} else {
							f_hdr->_status = STREAM_BUFFER_FAILURE;
						}

					} else {
						if (f_hdr->direction) PageDec();
						PageFlip();

						f_hdr->page[f_hdr->_current_page]._pointer = f_hdr->page[f_hdr->_current_page]._top - r_size;
						result = f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer;
					}
				}
			} else {			
				r_size = f_hdr->page[f_hdr->_current_page]._top;
				result = f_hdr->page[f_hdr->_current_page]._root;
				f_hdr->page[f_hdr->_current_page]._pointer = 0;
			}
		}

		f_hdr->direction = 0;

		Release();
	}

	return result;
}

unsigned char * Data::File::MoveLock(UI_64 & r_size, double posi) {
	if (_size) {		
		return MoveLock(r_size, (UI_64)(posi*_size));
	}

	return 0;
}

unsigned char * Data::File::JumpLock(UI_64 & r_size, UI_64 offset) {
	unsigned char * result(0);

	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		result = MoveLock(r_size, f_hdr->page[f_hdr->_current_page].file_offset.QuadPart + f_hdr->page[f_hdr->_current_page]._pointer + offset - r_size);

		Release();
	}

	return result;
}

unsigned char * Data::File::MoveLock(UI_64 & r_size, UI_64 posi) {
	unsigned char * result(0);
	UI_64 lover(0);


	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		f_hdr->_status = 0;

		f_hdr->map_break = 0;

		f_hdr->_type = 1;

		if (r_size <= f_hdr->reserved_size) {
			posi %= f_hdr->reserved_size;

			if (f_hdr->page[f_hdr->_current_page]._top != f_hdr->reserved_size) {
				if (r_size <= Header::Page::default_size) {
					if (f_hdr->compact) {
						if (posi > Header::Page::default_size) {
							f_hdr->_current_page = 1;
							f_hdr->page[1]._pointer = posi - Header::Page::default_size;
						} else {
							f_hdr->_current_page = 0;
							f_hdr->page[0]._pointer = posi;
						}

					} else {
						f_hdr->LoadPage(f_hdr->_current_page, posi);
						f_hdr->page[f_hdr->_current_page]._pointer = posi - f_hdr->page[f_hdr->_current_page].file_offset.QuadPart;
					}
				
				
					if ((f_hdr->page[f_hdr->_current_page]._pointer + r_size) <= f_hdr->page[f_hdr->_current_page]._top) {
						result = f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer;
					} else {
						if (r_size > f_hdr->disco_size) {
							if (f_hdr->disco_ptr) {
								VirtualFree(f_hdr->disco_ptr, 0, MEM_RELEASE);					
							}
				
							if (f_hdr->disco_ptr = VirtualAlloc(0, r_size, MEM_COMMIT, PAGE_READWRITE)) {
								f_hdr->disco_size = r_size;
							} else {
								f_hdr->_status = GetLastError();
							}
						}

						if (result = reinterpret_cast<unsigned char *>(f_hdr->disco_ptr)) {
							f_hdr->_status = 0;
							lover = f_hdr->page[f_hdr->_current_page]._top - f_hdr->page[f_hdr->_current_page]._pointer;

							System::MemoryCopy(result, f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer, lover);

							f_hdr->_current_page ^= 1;
							f_hdr->NextPage(f_hdr->_current_page, f_hdr->page[f_hdr->_current_page ^ 1].file_offset.QuadPart);

							f_hdr->page[f_hdr->_current_page]._pointer = r_size-lover;
							System::MemoryCopy(result + lover, f_hdr->page[f_hdr->_current_page]._root, f_hdr->page[f_hdr->_current_page]._pointer);				

							f_hdr->map_break = 1;
						
						} else {
							f_hdr->_status = STREAM_BUFFER_FAILURE;
						}
					}
				}
			} else {
				f_hdr->page[f_hdr->_current_page]._pointer = posi;

				if ((f_hdr->page[f_hdr->_current_page]._pointer + r_size) <= f_hdr->page[f_hdr->_current_page]._top) {
					result = f_hdr->page[f_hdr->_current_page]._root + f_hdr->page[f_hdr->_current_page]._pointer;
				} else {
					// break error
				}
			}
		} else {
			if (posi >= f_hdr->page[f_hdr->_current_page]._top) posi = 0;

			r_size = f_hdr->page[f_hdr->_current_page]._top - posi;
			f_hdr->page[f_hdr->_current_page]._pointer = posi;
			result = f_hdr->page[f_hdr->_current_page]._root;
		}

		f_hdr->direction = 1;

		Release();
	}

	return result;
}

void Data::File::PageFlip() {
	HANDLE s_block(0);

	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		if (f_hdr->page[f_hdr->_current_page]._top != f_hdr->reserved_size) {
			s_block = f_hdr->_service_block;
		}
		Release();

		if (s_block) {
			WaitForSingleObject(s_block, 360000);

			if (f_hdr = reinterpret_cast<Header *>(Acquire())) {
				f_hdr->_current_page ^= 1;
				Release();
			}
		}
	}
}

void Data::File::PageInc() {
	if (Header * f_hdr = reinterpret_cast<Header * >(Acquire())) {
		f_hdr->page[f_hdr->_current_page ^ 1]._pointer = 0;
		f_hdr->page[f_hdr->_current_page]._nneset = 0;

		if (f_hdr->page[f_hdr->_current_page]._top != f_hdr->reserved_size)
		if ((f_hdr->page[f_hdr->_current_page ^ 1].file_offset.QuadPart != (f_hdr->page[f_hdr->_current_page].file_offset.QuadPart + Header::Page::default_size)) && !((f_hdr->page[f_hdr->_current_page ^ 1].file_offset.QuadPart == 0) && ((f_hdr->page[f_hdr->_current_page].file_offset.QuadPart + Header::Page::default_size) >= f_hdr->reserved_size))) {
	
			ResetEvent(f_hdr->_service_block);
			f_hdr->_type = 1;

			service_queue.Push(this);
		}

		Release();	
	}
}

void Data::File::PageDec() {
	if (Header * f_hdr = reinterpret_cast<Header *>(Acquire())) {
		f_hdr->page[f_hdr->_current_page ^ 1]._pointer = f_hdr->page[f_hdr->_current_page ^ 1]._top;
		f_hdr->page[f_hdr->_current_page]._nneset = 0;

		if (f_hdr->page[f_hdr->_current_page]._top != f_hdr->reserved_size)
		if ((f_hdr->page[f_hdr->_current_page].file_offset.QuadPart != (f_hdr->page[f_hdr->_current_page ^ 1].file_offset.QuadPart + Header::Page::default_size)) && !((f_hdr->page[f_hdr->_current_page].file_offset.QuadPart == 0) && ((f_hdr->page[f_hdr->_current_page ^ 1].file_offset.QuadPart + Header::Page::default_size) >= f_hdr->reserved_size))) {
	
			ResetEvent(f_hdr->_service_block);
			f_hdr->_type = 0;

			service_queue.Push(this);
		}

		Release();
	}
}


UI_64 Data::File::Open(File & f_obj, const wchar_t * fpath) {
	UI_64 result(0);

	f_obj.Blank();

	result = f_obj.New(_tid, sizeof(Header), stream_pool);

	if (result != -1)
	if (Header * f_hdr = reinterpret_cast<Header *>(f_obj.Acquire())) {
		f_hdr->page[0]._nneset = 1;
		f_hdr->page[0].file_offset.QuadPart = -1;

		f_hdr->page[1]._nneset = 1;
		f_hdr->page[1].file_offset.QuadPart = -1;

		f_hdr->_handle = CreateFile(fpath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
		if (f_hdr->_handle != INVALID_HANDLE_VALUE) {
			GetFileSizeEx(f_hdr->_handle, reinterpret_cast<PLARGE_INTEGER>(&f_hdr->_size));
			f_hdr->reserved_size = f_hdr->_size;
			
			f_hdr->compact = (f_hdr->_size <= 2*Header::Page::default_size);

			f_hdr->page[0]._root = (unsigned char *)VirtualAlloc(0, Header::Page::default_size, MEM_COMMIT, PAGE_READWRITE);
			f_hdr->page[1]._root = (unsigned char *)VirtualAlloc(0, Header::Page::default_size, MEM_COMMIT, PAGE_READWRITE);

			if (f_hdr->page[0]._root && f_hdr->page[1]._root) {

				f_hdr->_service_block = CreateEvent(0, TRUE, TRUE, 0);
				if (!result) f_hdr->_status = GetLastError();
				else {
					if (f_hdr->reserved_size <= Header::Page::default_size) {
						f_hdr->LoadPage(0, 0);						
					} else {
						f_hdr->LoadPage(0, 0);
						if (f_hdr->compact) f_hdr->LoadPage(1, Header::Page::default_size);
					}
				}
			} else {
				f_hdr->_status = GetLastError();
			}

		} else {
			f_hdr->_status = GetLastError();
		}
		f_obj.Release();
	}

	return result;
}



Data::File::Header::Page & Data::File::Header::LoadPage(UI_64 p_selector, UI_64 offs) {
	DWORD byre(0);

	if (offs >= reserved_size) {
		offs = (reserved_size & (Page::default_size-1));
		if (offs) offs = reserved_size - offs;
		else offs = reserved_size - Page::default_size;
	}

	if ((offs >= page[p_selector].file_offset.QuadPart) && (offs < (page[p_selector].file_offset.QuadPart + Page::default_size))) {
		page[p_selector]._pointer = offs - page[p_selector].file_offset.QuadPart;
	} else {
		page[p_selector].file_offset.QuadPart = offs/Page::default_size;
		page[p_selector].file_offset.QuadPart *= Page::default_size;
		
//		_pointer = offs - file_offset.QuadPart;
		page[p_selector]._top = reserved_size - page[p_selector].file_offset.QuadPart;
		if (page[p_selector]._top > Page::default_size) page[p_selector]._top = Page::default_size;
		
		SetFilePointerEx(_handle, page[p_selector].file_offset, 0, 0);
		ReadFile(_handle, page[p_selector]._root, Page::default_size, &byre, 0);
	

		if (_type) {
			page[p_selector]._pointer = 0;
		} else {
			page[p_selector]._pointer = page[p_selector]._top;
		}

		page[p_selector]._nneset = 1;
	}

	return page[p_selector];
}

Data::File::Header::Page & Data::File::Header::NextPage(UI_64 p_selector, UI_64 foff) {
	if (foff == -1) foff = page[p_selector].file_offset.QuadPart;
	if (foff < (reserved_size - Page::default_size)) LoadPage(p_selector, foff + Page::default_size);
	else LoadPage(p_selector, 0);

	return page[p_selector];
}

Data::File::Header::Page & Data::File::Header::PrevPage(UI_64 p_selector, UI_64 foff) {
	if (foff == -1) foff = page[p_selector].file_offset.QuadPart;
	
	if (foff >= Page::default_size) LoadPage(p_selector, foff - Page::default_size);
	else LoadPage(p_selector, reserved_size);

	return page[p_selector];
}

UI_64 Data::File::Write(const void *, UI_64) {
	UI_64 result(0);


	return result;
}



/*
// book ==============================================================================================================================================================================================================================

Book::Book() : _handle(INVALID_HANDLE_VALUE), _mapping(0), _protection(0), _access(0), _status(0), _currentPage(0) {
	for (int i(0);i<MAX_PAGE_COUNT;i++) _pages[i]._book = this;
	reserved_size.QuadPart = _actualSize.QuadPart = 0;
}


Book::~Book() {
	Free();
}

void Book::Free() {
	for (int i(0);i<MAX_PAGE_COUNT;i++) _pages[i].Release();
		
	if (_mapping) CloseHandle(_mapping);
	if (_handle != INVALID_HANDLE_VALUE) {
		SetFilePointerEx(_handle, _actualSize, 0, 0);
		CloseHandle(_handle);
	}
}

void Book::Close() {
	delete this;
}

void * Book::Open(const wchar_t * path, int rwflag) {
	void * result(0);

	Free();

	_status = _marks.Init(0); // do not forget break

	if (!_status) {

		if (rwflag & FILE_READER) {
			_handle = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
			_access = FILE_MAP_READ;
			_protection = PAGE_READONLY;
		} else {
			_handle = CreateFile(path, GENERIC_READ | GENERIC_WRITE, 0, 0, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);
			_access = FILE_MAP_WRITE;
			_protection = PAGE_READWRITE;
		}

		if (_handle != INVALID_HANDLE_VALUE) {
			GetFileSizeEx(_handle, &_actualSize);
		
			if (rwflag & FILE_READER) {
				if (_actualSize.QuadPart) {
					reserved_size.QuadPart = 0;
				} else {
					CloseHandle(_handle);
					_handle = INVALID_HANDLE_VALUE;
				}
			} else {
				if (_actualSize.QuadPart) {
					reserved_size.QuadPart = (_actualSize.QuadPart + FileView::_size - 1)/FileView::_size;
					reserved_size.QuadPart *= FileView::_size;
				} else {
					reserved_size.QuadPart = FileView::_size;
				}
			}	
			

			if (_handle != INVALID_HANDLE_VALUE) {
				if (_mapping = CreateFileMapping(_handle, 0, _protection, reserved_size.HighPart, reserved_size.LowPart, 0)) {
					_pages[0].Load(0);
					_pages[1] = _pages[0].Next();
					_pages[2] = _pages[1].Next();

					if (!_status) result = _pages[0]._root;
				} else {
					_status = GetLastError();
				}
			}
		
		} else {
			_status = GetLastError();
		}

	}

	return result;
}

void * Book::JumpTo(UI_64 offs) {
	return _pages[++_currentPage %= MAX_PAGE_COUNT].Load(offs)._root;
}

void * Book::NextPage() {
	_pages[(_currentPage+1) % MAX_PAGE_COUNT] = _pages[_currentPage].Next();
	return _pages[++_currentPage %= MAX_PAGE_COUNT]._root;
}

void * Book::PrevPage() {
	_pages[(_currentPage+1) % MAX_PAGE_COUNT] = _pages[_currentPage].Prev();
	return _pages[++_currentPage %= MAX_PAGE_COUNT]._root;
}

void * Book::NextMark() {

}

void * Book::PrevMark() {


}

*/
