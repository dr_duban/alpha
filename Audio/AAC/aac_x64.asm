
; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE




.CODE



ALIGN 16
?StereoAdjustAll@AAC@Audio@@CAXPEAMI@Z	PROC
	
		MOVAPS xmm0, [rcx - 16384]
		MOVAPS xmm1, [rcx - 16384 + 32]
		MOVAPS xmm2, [rcx - 16384 + 64]
		MOVAPS xmm3, [rcx - 16384 + 96]

		MOVAPS xmm4, xmm0
		MOVAPS xmm5, xmm1
		MOVAPS xmm6, xmm2
		MOVAPS xmm7, xmm3

		ADDPS xmm0, [rcx]
		SUBPS xmm4, [rcx]
		ADDPS xmm1, [rcx + 32]
		SUBPS xmm5, [rcx + 32]
		ADDPS xmm2, [rcx + 64]
		SUBPS xmm6, [rcx + 64]
		ADDPS xmm3, [rcx + 96]
		SUBPS xmm7, [rcx + 96]

		MOVAPS [rcx - 16384], xmm0
		MOVAPS [rcx - 16384 + 16], xmm0
		MOVAPS [rcx - 16384 + 32], xmm1
		MOVAPS [rcx - 16384 + 48], xmm1
		MOVAPS [rcx - 16384 + 64], xmm2
		MOVAPS [rcx - 16384 + 80], xmm2
		MOVAPS [rcx - 16384 + 96], xmm3
		MOVAPS [rcx - 16384 + 112], xmm3

		MOVAPS [rcx], xmm4
		MOVAPS [rcx + 16], xmm4
		MOVAPS [rcx + 32], xmm5
		MOVAPS [rcx + 48], xmm5
		MOVAPS [rcx + 64], xmm6
		MOVAPS [rcx + 80], xmm6
		MOVAPS [rcx + 96], xmm7
		MOVAPS [rcx + 112], xmm7

		ADD rcx, 128

	DEC edx
	JNZ ?StereoAdjustAll@AAC@Audio@@CAXPEAMI@Z

	RET
?StereoAdjustAll@AAC@Audio@@CAXPEAMI@Z	ENDP


ALIGN 16
?TNSFilterUp@AAC@Audio@@CAXPEAMHPEBM@Z	PROC
	SHR edx, 2

; max allowed filter coefs is 20
	
	XORPS xmm5, xmm5
	XORPS xmm6, xmm6
	XORPS xmm7, xmm7
	XORPS xmm8, xmm8
	XORPS xmm9, xmm9		

	MOVUPS xmm10, [r8]
	MOVUPS xmm11, [r8+16]
	MOVUPS xmm12, [r8+32]
	MOVUPS xmm13, [r8+48]
	MOVUPS xmm14, [r8+64]
	
align 16
	filter_loop:


		MOVAPS xmm15, [rcx]
		MOVSS xmm5, xmm15

		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0

		MOVSS xmm5, xmm0
		
		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 39h
		MOVSS xmm5, xmm15



		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9		

		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 39h
		MOVSS xmm5, xmm15



		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 39h
		MOVSS xmm5, xmm15



		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 39h

		MOVAPS [rcx], xmm15

		ADD rcx, 32
	DEC edx
	JNZ filter_loop


	RET
?TNSFilterUp@AAC@Audio@@CAXPEAMHPEBM@Z	ENDP


ALIGN 16
?TNSFilterDown@AAC@Audio@@CAXPEAMHPEBM@Z	PROC
	SHR edx, 2
	
	SUB rcx, 32
; max allowed filter coefs is 20
	
	XORPS xmm5, xmm5
	XORPS xmm6, xmm6
	XORPS xmm7, xmm7
	XORPS xmm8, xmm8
	XORPS xmm9, xmm9
		

	MOVUPS xmm10, [r8]
	MOVUPS xmm11, [r8+16]
	MOVUPS xmm12, [r8+32]
	MOVUPS xmm13, [r8+48]
	MOVUPS xmm14, [r8+64]
	
align 16
	filter_loop:


		MOVAPS xmm15, [rcx]
		SHUFPS xmm15, xmm15, 93h
		MOVSS xmm5, xmm15


		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9

		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 93h
		MOVSS xmm5, xmm15




		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 93h
		MOVSS xmm5, xmm15



		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		SHUFPS xmm15, xmm15, 93h
		MOVSS xmm5, xmm15



		MOVAPS xmm0, xmm5
		MOVAPS xmm1, xmm6
		MOVAPS xmm2, xmm7
		MOVAPS xmm3, xmm8
		MOVAPS xmm4, xmm9
	
		MULPS xmm0, xmm10
		MULPS xmm1, xmm11
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13
		MULPS xmm4, xmm14

		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2
		ADDPS xmm0, xmm4

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0	

		MOVSS xmm5, xmm0

		SHUFPS xmm9, xmm9, 93h
		SHUFPS xmm8, xmm8, 93h
		MOVSS xmm9, xmm8
		SHUFPS xmm7, xmm7, 93h
		MOVSS xmm8, xmm7
		SHUFPS xmm6, xmm6, 93h
		MOVSS xmm7, xmm6
		SHUFPS xmm5, xmm5, 93h
		MOVSS xmm6, xmm5

		MOVSS xmm15, xmm0
		
		MOVAPS [rcx], xmm15

		SUB rcx, 32
	DEC edx
	JNZ filter_loop

	RET
?TNSFilterDown@AAC@Audio@@CAXPEAMHPEBM@Z	ENDP


ALIGN 16
?Format8Window@AAC@Audio@@CAXPEAM_KPEBM@Z	PROC
	LEA rdx, [rcx + rdx*4 + 2048*4]
	

	MOV r9, rcx

	SUB r8, rcx


	MOV r10d, 8

align 16
	win_00:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]

		MULPS xmm0, [r8 + rcx]
		MULPS xmm1, [r8 + rcx + 16]
		MULPS xmm2, [r8 + rcx + 32]
		MULPS xmm3, [r8 + rcx + 48]

		MOVAPS [r9], xmm0
		MOVAPS [r9 + 16], xmm1
		MOVAPS [r9 + 32], xmm2
		MOVAPS [r9 + 48], xmm3

		ADD rcx, 64
		ADD r9, 64

	DEC r10d
	JNZ win_00
	
	

	MOV r10d, 8
	MOV r11d, 7

align 16
	win_17:
			MOVAPS xmm0, [rcx]
			MOVAPS xmm1, [rcx + 16]
			MOVAPS xmm2, [rcx + 32]
			MOVAPS xmm3, [rcx + 48]

			MULPS xmm0, [r8 + rcx]
			MULPS xmm1, [r8 + rcx + 16]
			MULPS xmm2, [r8 + rcx + 32]
			MULPS xmm3, [r8 + rcx + 48]


			MOVAPS xmm4, [rcx + 512]
			MOVAPS xmm5, [rcx + 512 + 16]
			MOVAPS xmm6, [rcx + 512 + 32]
			MOVAPS xmm7, [rcx + 512 + 48]

			MULPS xmm4, [r8 + rcx + 512]
			MULPS xmm5, [r8 + rcx + 512 + 16]
			MULPS xmm6, [r8 + rcx + 512 + 32]
			MULPS xmm7, [r8 + rcx + 512 + 48]

			ADDPS xmm0, xmm4
			ADDPS xmm1, xmm5
			ADDPS xmm2, xmm6
			ADDPS xmm3, xmm7

			MOVAPS [r9], xmm0
			MOVAPS [r9 + 16], xmm1
			MOVAPS [r9 + 32], xmm2
			MOVAPS [r9 + 48], xmm3

			ADD rcx, 64
			ADD r9, 64

		DEC r10d
		JNZ win_17
		
		ADD rcx, 512

		MOV r10d, 8
	DEC r11d
	JNZ win_17


	MOV r10d, 8

align 16
	win_80:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]

		MULPS xmm0, [r8 + rcx]
		MULPS xmm1, [r8 + rcx + 16]
		MULPS xmm2, [r8 + rcx + 32]
		MULPS xmm3, [r8 + rcx + 48]

		MOVAPS [r9], xmm0
		MOVAPS [r9 + 16], xmm1
		MOVAPS [r9 + 32], xmm2
		MOVAPS [r9 + 48], xmm3

		ADD rcx, 64
		ADD r9, 64

	DEC r10d
	JNZ win_80
	


	XORPS xmm4, xmm4


	MOV r10d, 28
align 16
	clear_1600:		

		MOVAPS [rcx - 16], xmm4
		MOVAPS [rcx - 32], xmm4
		MOVAPS [rcx - 48], xmm4
		MOVAPS [rcx - 64], xmm4

		SUB rcx, 64
	DEC r10d
	JNZ clear_1600

	SUB r9, rcx

	MOV r10d, 36

align 16
	copy_loop_right:
		MOVAPS xmm0, [r9 + rcx - 16]
		MOVAPS xmm1, [r9 + rcx - 32]
		MOVAPS xmm2, [r9 + rcx - 48]
		MOVAPS xmm3, [r9 + rcx - 64]


		MOVAPS [rcx - 16], xmm0
		MOVAPS [rcx - 32], xmm1
		MOVAPS [rcx - 48], xmm2
		MOVAPS [rcx - 64], xmm3

		SUB rcx, 64

	DEC r10d
	JNZ copy_loop_right


	SUB rdx, rcx

	MOV r10d, 36

align 16
	copy_loop_left:
		MOVAPS xmm0, [r9 + rcx - 16]
		MOVAPS xmm1, [r9 + rcx - 32]
		MOVAPS xmm2, [r9 + rcx - 48]
		MOVAPS xmm3, [r9 + rcx - 64]

		ADDPS xmm0, [rdx + rcx - 16]
		ADDPS xmm1, [rdx + rcx - 32]
		ADDPS xmm2, [rdx + rcx - 48]
		ADDPS xmm3, [rdx + rcx - 64]

		MOVAPS [rcx - 16], xmm0
		MOVAPS [rcx - 32], xmm1
		MOVAPS [rcx - 48], xmm2
		MOVAPS [rcx - 64], xmm3

		SUB rcx, 64
		
	DEC r10d
	JNZ copy_loop_left



	MOV r10d, 28
align 16
	clear_448:
		MOVAPS xmm0, [rdx + rcx - 16]
		MOVAPS xmm1, [rdx + rcx - 32]
		MOVAPS xmm2, [rdx + rcx - 48]
		MOVAPS xmm3, [rdx + rcx - 64]


		MOVAPS [rcx - 16], xmm0
		MOVAPS [rcx - 32], xmm1
		MOVAPS [rcx - 48], xmm2
		MOVAPS [rcx - 64], xmm3

		SUB rcx, 64
		
	DEC r10d
	JNZ clear_448



	RET
?Format8Window@AAC@Audio@@CAXPEAM_KPEBM@Z	ENDP


END

