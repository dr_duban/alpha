/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Audio\AAC.h"


#include "System\SysUtils.h"

#include "Math\CalcSpecial.h"

#include <math.h>


#pragma warning(disable:4244)

int Audio::AAC::de_quant[8192];
int Audio::AAC::scale_factor[512];
int Audio::AAC::tns_sin[2][16];

__declspec(align(16)) float Audio::AAC::cos_table_0[5120];
__declspec(align(16)) float Audio::AAC::window_table[4][4][2048];

Audio::AAC::AAC(unsigned int aut) : access_unit_type(aut), audio_object_type(0), channel_idc(0), x_aot(0), sampling_frequency(0), sf_idx(0), x_sampling_frequency(0), xsf_idx(0), layer_number(0), num_of_sub_frame(0), layer_length(0), aac_resilience_flags(0), ep_config(0), direct_mapping_flag(0), sync_xtype(0), frame_lines(128), current_program(0), max_tns(20) {
	for (unsigned int i(0);i<MAX_APROGRAM_COUNT;i++) program_cfg[i].Clear();
	for (unsigned int i(0);i<MAX_CHANNEL_COUNT;i++) {
		previous_shape[i] = 0;

	}

	stream_cfg.sdata_offset = DefaultRenderer::frame_cfg_size + 2048;
	stream_cfg.frame_size = DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE;	
}


Audio::AAC::~AAC() {

}


	
void Audio::AAC::JumpOn() {


}

UI_64 Audio::AAC::Unpack(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0);

	unsigned int protection_absent(1), mpeg_id(1), frame_length(0), raw_block_count(0), sync_word(0);

	if ((data_ptr[0] == 0xFF) && ((data_ptr[1] & 0xF0) == 0xF0)) {
// adts frame
		

		for (;data_length>7;) {
	// adts fixed header
			decoder_cfg.SetSource(data_ptr, data_length);


			sync_word = decoder_cfg.GetNumber(12); // syncword
			if (sync_word != 0x0FFF) {

				break;
			}


			mpeg_id = decoder_cfg.NextBit();

			decoder_cfg.GetNumber(2); // layer

			protection_absent = decoder_cfg.NextBit();

			audio_object_type = decoder_cfg.GetNumber(2) + 1; // profile objectType


			sf_idx = decoder_cfg.GetNumber(4);
			sampling_frequency = sampling_frequencies[sf_idx];

			decoder_cfg.NextBit(); // private bit

			channel_idc = decoder_cfg.GetNumber(3);
						

			switch (channel_idc) {
				case 1:
					program_cfg[0].center_present = 1;
					program_cfg[0].chan_count = 1;
				break;
				case 2:
					program_cfg[0].chan_count = 2;
					program_cfg[0].num_channel_elements[0] = 2;
				break;
				case 3:
					program_cfg[0].chan_count = 3;
					program_cfg[0].center_present = 1;
					program_cfg[0].num_channel_elements[0] = 2;
				break;
				case 4:
					program_cfg[0].chan_count = 4;
					program_cfg[0].center_present = 1;
					program_cfg[0].num_channel_elements[0] = 2;
					program_cfg[0].num_channel_elements[2] = 1;
				break;
				case 5:
					program_cfg[0].chan_count = 5;
					program_cfg[0].center_present = 1;

					program_cfg[0].num_channel_elements[0] = 2;
					program_cfg[0].num_channel_elements[2] = 2;
				break;
				case 6:
					program_cfg[0].chan_count = 6;
					program_cfg[0].center_present = 1;

					program_cfg[0].num_channel_elements[0] = 2;
					program_cfg[0].num_channel_elements[2] = 2;
					program_cfg[0].num_channel_elements[3] = 1;
				break;
				case 7:
					program_cfg[0].chan_count = 8;
					program_cfg[0].center_present = 1;

					program_cfg[0].num_channel_elements[0] = 2;
					program_cfg[0].num_channel_elements[1] = 2;
					program_cfg[0].num_channel_elements[2] = 2;
					program_cfg[0].num_channel_elements[3] = 1;
				break;
			}

			decoder_cfg.NextBit(); // original copy

			decoder_cfg.NextBit(); // home

	// adts variable header
			decoder_cfg.NextBit(); // copyright id bit
			decoder_cfg.NextBit(); // copyright id start

			frame_length = decoder_cfg.GetNumber(13);

			decoder_cfg.GetNumber(11); // adts buffer fullness

			if (raw_block_count = decoder_cfg.GetNumber(2)) {
				for (unsigned int i(0);i<raw_block_count;i++) {
					decoder_cfg.GetNumber(16); // data block position
				}
			}
			
			if (protection_absent == 0) {					
				decoder_cfg.GetNumber(16); // CRC16
			}
			
			for (unsigned int i(0);i<=raw_block_count;i++) {
				result |= DecodeUnit(0, 0, time_mark);

				if (raw_block_count && (protection_absent == 0)) {
					decoder_cfg.GetNumber(16);
				}
			}

			data_ptr += frame_length;
			data_length -= frame_length;
		}


	} else {
// adif syntax
		if ((data_ptr[0] == 'A') && (data_ptr[0] == 'D') && (data_ptr[0] == 'I') && (data_ptr[0] == 'F')) {
			

		} else {
			result = 0;
		}

	}


	return result;
}


UI_64 Audio::AAC::DecodeUnit(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(1);
	unsigned int x_val(0);
		
	if (data_ptr) decoder_cfg.SetSource(data_ptr, data_length);

	if (decoder_cfg.src_ptr)
	switch (access_unit_type) {
		case 0: // 14496-3 1.6
			if (time_mark == -1) { // decoder specific info
				audio_object_type = decoder_cfg.GetNumber(5);

				if (audio_object_type == 31) {
					audio_object_type = 32 + decoder_cfg.GetNumber(6);
				}

				sf_idx = decoder_cfg.GetNumber(4);
				sampling_frequency = sampling_frequencies[sf_idx];
				if (!sampling_frequency) {
					sampling_frequency = decoder_cfg.GetNumber(24);
				}
				

				channel_idc = decoder_cfg.GetNumber(4);


				switch (channel_idc) {
					case 1:
						program_cfg[0].center_present = 1;
						program_cfg[0].chan_count = 1;						
					break;
					case 2:
						program_cfg[0].chan_count = 2;
						program_cfg[0].num_channel_elements[0] = 2;

					break;
					case 3:
						program_cfg[0].chan_count = 3;
						program_cfg[0].center_present = 1;
						program_cfg[0].num_channel_elements[0] = 2;
					break;
					case 4:
						program_cfg[0].chan_count = 4;
						program_cfg[0].center_present = 1;
						program_cfg[0].num_channel_elements[0] = 2;
						program_cfg[0].num_channel_elements[2] = 1;
					break;
					case 5:
						program_cfg[0].chan_count = 5;
						program_cfg[0].center_present = 1;

						program_cfg[0].num_channel_elements[0] = 2;
						program_cfg[0].num_channel_elements[2] = 2;
					break;
					case 6:
						program_cfg[0].chan_count = 6;
						program_cfg[0].center_present = 1;

						program_cfg[0].num_channel_elements[0] = 2;
						program_cfg[0].num_channel_elements[2] = 2;
						program_cfg[0].num_channel_elements[3] = 1;
					break;
					case 7:
						program_cfg[0].chan_count = 8;
						program_cfg[0].center_present = 1;

						program_cfg[0].num_channel_elements[0] = 2;
						program_cfg[0].num_channel_elements[1] = 2;
						program_cfg[0].num_channel_elements[2] = 2;
						program_cfg[0].num_channel_elements[3] = 1;
					break;
				}


				x_aot = 0;
				if (audio_object_type == 5) {
					x_aot = 5;

					xsf_idx = decoder_cfg.GetNumber(4);
					x_sampling_frequency = sampling_frequencies[xsf_idx];
					if (!x_sampling_frequency) {
						x_sampling_frequency = decoder_cfg.GetNumber(24);
					}

					audio_object_type = decoder_cfg.GetNumber(5);

					if (audio_object_type == 31) {
						audio_object_type = 32 + decoder_cfg.GetNumber(6);
					}

				}

				switch (audio_object_type) {
					case 3:
						frame_lines = 32;
					case 1:
					case 2:
					case 4:
					case 6:
					case 7:
					case 17:
					case 19:
					case 20:
					case 21:
					case 22:
					case 23: // generic coding (subpart 4)


						
						if (decoder_cfg.NextBit()) frame_lines = 120; // frame lenth flag

						if (decoder_cfg.NextBit()) {
							core_coder_delay = decoder_cfg.GetNumber(14); // delay in samples to be applied to the up-sampled core decoder output
						}

						x_val = decoder_cfg.NextBit(); // extension flag




						if (channel_idc == 0) {
							GetProgramConfig();
						}

						if ((audio_object_type == 6) || (audio_object_type == 20)) {
							layer_number = decoder_cfg.GetNumber(3); // AAC layer number in scalable syntax
						}

						if (x_val) { // shall be 0 for audio types 1, 2, 3, 4, 6, 7
							switch (audio_object_type) {
								case 22:
									num_of_sub_frame = decoder_cfg.GetNumber(5); // number of sub-frames in super-frame
									layer_length = decoder_cfg.GetNumber(11); // average layer length in bytes
								break;
								case 17: case 19: case 20: case 23:
									aac_resilience_flags = decoder_cfg.GetNumber(3);
								break;

							}

							if (decoder_cfg.NextBit()) { // tbd in version 3 (reserved)


							}
						}
					break;
					case 8: // Celp

					break;
					case 9: // Hvxc

					break;
					case 12: // TTS

					break;
					case 13:
					case 14:
					case 15:
					case 16: // structured audio

					break;
					case 24: // error resilient Celp

					break;
					case 25: // error resilirnt Hvxc
					
					break;
					case 26: case 27: // parametric

					break;
					case 28: // SSC

					break;
					case 32:
					case 33:
					case 34: // MPEG 1/2

					break;
					case 35: // DST

					break;

				}

				switch (audio_object_type) {
					case 17: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26: case 27:
						ep_config = decoder_cfg.GetNumber(2);
						if (ep_config > 1) {

						}
						if (ep_config == 3) {
							direct_mapping_flag = decoder_cfg.NextBit();

							if (!direct_mapping_flag) {
								// tbd
							}
						}

					break;
				}

				if ((x_aot != 5) && (decoder_cfg.IsSource(16))) {
					sync_xtype = decoder_cfg.GetNumber(11);

					if (sync_xtype == 0x02B7) {
						x_aot = decoder_cfg.GetNumber(5);

						if (x_aot == 31) {
							x_aot = 32 + decoder_cfg.GetNumber(6);
						}

						if (x_aot == 5) {
							if (decoder_cfg.NextBit()) {
								decoder_cfg.GetNumber(4);
								x_sampling_frequency = sampling_frequencies[xsf_idx];

								if (!x_sampling_frequency) {
									x_sampling_frequency = decoder_cfg.GetNumber(24);
								}
							}
						}
					}
				}

				if (stream_cfg.ini_block == 0) {
					if (stream_cfg.ini_block = CreateEvent(0, 0, 0, 0)) {
						mixer_thread = CreateThread(0, 0, Audio::DefaultRenderer::MixerLoop, &stream_cfg, 0, 0);	
						if (stream_cfg.set_complete & COMPLETE_CONTAINER) WaitForSingleObject(stream_cfg.ini_block, INFINITE);
					}
				}


			} else {
				if (!stream_cfg.frame_buff) {
					stream_cfg.frame_buff.New(frame_buf_tid, MAX_FRAME_COUNT*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE)*sizeof(float), SFSco::large_mem_mgr);
				}

				if (decoder_cfg.spectral_data = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {
					
					previous_frame_delta = stream_cfg.current_frame++;
					stream_cfg.current_frame &= (MAX_FRAME_COUNT - 1);
					
					if (previous_frame_delta < stream_cfg.current_frame) previous_frame_delta = -1;
					previous_frame_delta *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);

					decoder_cfg.spectral_data += stream_cfg.current_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);
					decoder_cfg.frame_cfg = reinterpret_cast<DefaultRenderer::FrameCfg *>(decoder_cfg.spectral_data);
					decoder_cfg.spectral_data += DefaultRenderer::frame_cfg_size;

					
					System::MemoryFill_SSE3(decoder_cfg.frame_cfg, (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE)*sizeof(float), 0, 0);

					decoder_cfg.frame_cfg->time_mark = time_mark;
					decoder_cfg.frame_cfg->source_frequency = sampling_frequency;
					decoder_cfg.frame_cfg->channel_offset = AAC_CHANNEL_SIZE;














#ifdef _DEBUG
	if (time_mark >= 0x00979b) {
		x_val = 1;
	}

#endif
					


					switch (audio_object_type) {

						case 1:
						case 2:
						case 3:
						case 4:
						case 5:					
						case 7: // generic coding

							result = GenericRaw();

						break;

						case 6: // scalable syntax 1.6.2.2.2.1.3
							result = ScalableRaw();
						break;

						case 8: // Celp (subpart 3)

						break;

						case 9: // Hvxc (subpart 2)

						break;

						case 12: // TTS (subpart 6)

						break;

						case 13:
						case 14:
						case 15:
						case 16: // structured audio (subpart 5)

						break;



						case 17:
						case 19:
						case 20:
						case 21:
						case 22:
						case 23: // error resilient syntax 1.6.2.2.2.1.4
							result = ERRaw();

						break;


						case 24: // error resilient Celp (subpart 3)

						break;

						case 25: // error resilirnt Hvxc (subpart 2)
					
						break;

						case 26: case 27: // parametric (subpart 2/7)

						break;

						case 28: // SSC (subpart 8)

						break;
						case 32:
						case 33:
						case 34: // MPEG 1/2 (subpart 9)

						break;
						case 35: // DST (subpart 10)

						break;
						
					}

					stream_cfg.frame_buff.Release();

					if (result & RESULT_DECODE_COMPLETE) {

						if (stream_cfg.ini_block == 0) {
							if (stream_cfg.ini_block = CreateEvent(0, 0, 0, 0)) {
								mixer_thread = CreateThread(0, 0, Audio::DefaultRenderer::MixerLoop, &stream_cfg, 0, 0);	
								if (stream_cfg.set_complete) WaitForSingleObject(stream_cfg.ini_block, INFINITE);
							}
						}

						if (stream_cfg.flush_sign == -1) stream_cfg.flush_sign = -2;
					} else {
						if (stream_cfg.current_frame) stream_cfg.current_frame--;
						else stream_cfg.current_frame = MAX_FRAME_COUNT - 1;
					}
				}

				
			}
		break;
		case 1: // 13818-7
			if (time_mark == -1) { // adif_header()


			} else {



			}

		break;

	}

	
	if ((decoder_cfg.frame_cfg) && (result == RESULT_DECODE_COMPLETE)) {
		decoder_cfg.frame_cfg->valid_sign = time_delta + decoder_cfg.frame_cfg->window_count*decoder_cfg.frame_cfg->window_sample_count*10000;
		time_delta = (decoder_cfg.frame_cfg->valid_sign % decoder_cfg.frame_cfg->source_frequency);
		decoder_cfg.frame_cfg->valid_sign /= decoder_cfg.frame_cfg->source_frequency;

		time_mark = decoder_cfg.frame_cfg->valid_sign;			
	}

	return result;
}

void Audio::AAC::ProgramConfig::Clear() {
	System::MemoryFill(this, sizeof(Audio::AAC::ProgramConfig), 0);
}

void Audio::AAC::ChannelConfig::ICSClear() {
	System::MemoryFill(this, 2*sizeof(ICS), 0);
}

void Audio::AAC::ChannelConfig::GroupClear() {
	System::MemoryFill(group, sizeof(ChannelConfig) - 2*sizeof(ICS), 0);

}

void Audio::AAC::GetProgramConfig() {
	current_program = decoder_cfg.GetNumber(4);

	program_cfg[current_program].Clear();

	program_cfg[current_program].o_type = decoder_cfg.GetNumber(2); // 0 - AAC MAin, 1 - AAC LC, 2 - AAC SSR, 3 - AAC LTP

	sf_idx = decoder_cfg.GetNumber(4);
	program_cfg[current_program].sampling_frequency = sampling_frequencies[sf_idx];
	
	program_cfg[current_program].num_channel_elements[0] = decoder_cfg.GetNumber(4);
	program_cfg[current_program].num_channel_elements[1] = decoder_cfg.GetNumber(4);
	program_cfg[current_program].num_channel_elements[2] = decoder_cfg.GetNumber(4);
	program_cfg[current_program].num_channel_elements[3] = decoder_cfg.GetNumber(2);

	program_cfg[current_program].num_assoc_data_elements = decoder_cfg.GetNumber(3);
	program_cfg[current_program].num_channel_elements[5] = decoder_cfg.GetNumber(4);
	
	if (decoder_cfg.NextBit()) {
		program_cfg[current_program].mono_mixdown_element_number = decoder_cfg.GetNumber(4);
	}

	if (decoder_cfg.NextBit()) {
		program_cfg[current_program].stereo_mixdown_element_number = decoder_cfg.GetNumber(4);
	}

	if (decoder_cfg.NextBit()) {
		program_cfg[current_program].matrix_mixdown_idx = decoder_cfg.GetNumber(2);
		program_cfg[current_program].pseudo_surround_flag = decoder_cfg.NextBit();
	}

	
	for (unsigned int i(0);i<program_cfg[current_program].num_channel_elements[0];i++) {
		program_cfg[current_program].elements[0][i].is_cpe = decoder_cfg.NextBit();
		program_cfg[current_program].elements[0][i].tag_select = decoder_cfg.GetNumber(4);
	}

	for (unsigned int i(0);i<program_cfg[current_program].num_channel_elements[1];i++) {
		program_cfg[current_program].elements[1][i].is_cpe = decoder_cfg.NextBit();
		program_cfg[current_program].elements[1][i].tag_select = decoder_cfg.GetNumber(4);
	}

	for (unsigned int i(0);i<program_cfg[current_program].num_channel_elements[2];i++) {
		program_cfg[current_program].elements[2][i].is_cpe = decoder_cfg.NextBit();
		program_cfg[current_program].elements[2][i].tag_select = decoder_cfg.GetNumber(4);
	}

	for (unsigned int i(0);i<program_cfg[current_program].num_channel_elements[3];i++) {
		program_cfg[current_program].elements[3][i].tag_select = decoder_cfg.GetNumber(4);
	}

	for (unsigned int i(0);i<program_cfg[current_program].num_assoc_data_elements;i++) {
		program_cfg[current_program].elements[4][i].tag_select = decoder_cfg.GetNumber(4);
	}

	for (unsigned int i(0);i<program_cfg[current_program].num_channel_elements[5];i++) {
		program_cfg[current_program].elements[5][i].is_cpe = decoder_cfg.NextBit();
		program_cfg[current_program].elements[5][i].tag_select = decoder_cfg.GetNumber(4);
	}

	decoder_cfg.ByteAlign();

	decoder_cfg.Skip(decoder_cfg.NextByte());

}



unsigned int Audio::AAC::GetSatCount(unsigned int * sat_desc) {
	UI_64 f_off(0);
	unsigned int result(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);

		f_ptr += f_off;
		
		result = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
		for (unsigned int i(0);i<8;i++) sat_desc[i] = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->chan_type[i];

		stream_cfg.frame_buff.Release();
	}


	return result;
}

void Audio::AAC::GetSatInfo(unsigned int i_id, OpenGL::StringsBoard::InfoLine & i_line) {
	UI_64 f_off(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);

		f_ptr += f_off;
		
		switch (i_id) {
			case 0:
				i_line.cap[0] = L'A'; i_line.cap[1] = L'A'; i_line.cap[2] = L'C';i_line.cap[3] = L' '; i_line.cap[4] = L'L'; i_line.cap[5] = L'C'; i_line.cap[6] = 0;
				i_line.cap_size = 6;
				i_line.cap_flags = 0;
			break;
			case 1:
				i_line.cap[0] = L'S'; i_line.cap[1] = L'F'; i_line.cap[2] = L':'; i_line.cap[3] = L'H'; i_line.cap[4] = L'z'; i_line.cap[5] = 0;
				i_line.cap_size = 0x00020003;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeDouble | OpenGL::StringsBoard::InfoTypeFormatted | 1;

				reinterpret_cast<double &>(i_line.val) = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			break;
			case 2:
				i_line.cap[0] = L'c'; i_line.cap[1] = L'h'; i_line.cap[2] = L'a'; i_line.cap[3] = L'n'; i_line.cap[4] = L'n'; i_line.cap[5] = L'e'; i_line.cap[6] = L'l'; i_line.cap[7] = L's'; i_line.cap[8] = 0;
				i_line.cap_size = 8;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeINT64 | OpenGL::StringsBoard::InfoDataPrefix;

				i_line.val = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
			break;
		}

		stream_cfg.frame_buff.Release();
	}
}

			
UI_64 Audio::AAC::FilterFrame(unsigned int *) {
	UI_64 result(0);

	return result;
}


UI_64 Audio::AAC::FilterSatFrame(float * vector_ptr, OpenGL::DecodeRecord & de_rec) {
	float inc_val(0.0f);
	UI_64 result(0), last_frame(de_rec.target_time & 0xFFFFFFFF), last_adjust(de_rec.target_time >> 32), new_adjust(0), f_off(0);

	if (last_frame == stream_cfg.current_frame) return 0;

	if (de_rec.start_time == 0) last_frame = -1;

	if (float * f_ptr = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {

		result = -System::GetUCount(de_rec.start_time, 0);
		de_rec.start_time = System::GetTime();

		if (last_frame != -1) {
			result *= reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			result /= 10000;

			last_adjust = 1024 - last_adjust;

			if (result >= last_adjust) {
				result -= last_adjust;
				last_adjust = result % 1024;

				last_frame++;
			} else {
				last_adjust = 1024 - last_adjust + result;
			}

			last_frame += (result/1024);
			last_frame &= (MAX_FRAME_COUNT - 1);

		} else {
			result = 8192;
			last_adjust = 0;

			last_frame = 8;
		}


		if (last_frame > stream_cfg.current_frame) last_frame = stream_cfg.current_frame;

		de_rec.target_time = last_adjust;
		de_rec.target_time <<= 32;
		de_rec.target_time |= last_frame;


		result = de_rec.width - last_adjust;		
		new_adjust = (result % 1024);
		result = (result/1024);

		
		if (new_adjust) {
			f_off = (last_frame - result - 1) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*AAC_CHANNEL_SIZE + 1024 - new_adjust;


			for (unsigned int si(0);si<new_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}	
		


		for (;result > 0;result--) {
			f_off = (last_frame - result)*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*AAC_CHANNEL_SIZE;
			
			for (unsigned int si(0);si<1024;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}

		if (last_adjust) {
			f_off = last_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*AAC_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*AAC_CHANNEL_SIZE;
			
			for (unsigned int si(0);si<last_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}	

		stream_cfg.frame_buff.Release();

		result = 0x00010000;
	}

	// maybe normalize vector


	return result;
}



UI_64 Audio::AAC::UnitTest(const float *) {
	return 0;
}
			
UI_64 Audio::AAC::CfgOverride(UI_64, UI_64) {
	UI_64 result(0);

	return result;
}

UI_64 Audio::AAC::Initialize() {
	float f_val(0);
	int * fi_ptr(reinterpret_cast<int *>(&f_val));


	for (unsigned int i(0);i<8192;i++) {
		f_val = pow(i, 1.33333333333333333333f);
		de_quant[i] = fi_ptr[0];
	}

	scale_factor[0] = 0;
	for (int i(1);i<512;i++) {
		f_val = pow(2, 0.25f*(i - 200));
		scale_factor[i] = fi_ptr[0];
	}


	for (int j(-8);j<0;j++) {
		f_val = Calc::RealSin(90.0*j/4.5);
		tns_sin[0][j+8] = fi_ptr[0];
	}
	for (int j(0);j<8;j++) {
		f_val = Calc::RealSin(90.0*j/3.5);
		tns_sin[0][j+8] = fi_ptr[0];
	}


	for (int j(-8);j<0;j++) {
		f_val = Calc::RealSin(90.0*j/8.5);
		tns_sin[1][j+8] = fi_ptr[0];
	}
	for (int j(0);j<8;j++) {
		f_val = Calc::RealSin(90.0*j/7.5);
		tns_sin[1][j+8] = fi_ptr[0];
	}

		
	GenCosTable(cos_table_0 + GenCosTable(cos_table_0, 2048), 256);





// ONLY_LONG ============================================================
// 0: sin-sin	
	for (unsigned int i(0);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][0])[i] = SIN_1024[i];
	}
// 1: sin-kbd
	for (unsigned int i(0);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][1])[i] = SIN_1024[i];
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][1])[i] = KBD_1024[i];
	}

// 2: kbd-sin
	for (unsigned int i(0);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][2])[i] = KBD_1024[i];
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][2])[i] = SIN_1024[i];
	}

// 3: kbd-kbd
	for (unsigned int i(0);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[ONLY_LONG_SEQUENCE][3])[i] = KBD_1024[i];
	}

// LONG_START ============================================================
// 0: sin-sin	
	for (unsigned int i(0);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][0])[i] = SIN_1024[i];
	}
	for (unsigned int i(1024);i<1472;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][0])[i] = 0x3F800000;
	}
	for (unsigned int i(1472);i<1600;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][0])[i] = SIN_128[i - 1472 + 128];
	}
	for (unsigned int i(1600);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][0])[i] = 0;
	}


// 1: sin-kbd
	for (unsigned int i(0);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][1])[i] = SIN_1024[i];
	}
	for (unsigned int i(1024);i<1472;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][1])[i] = 0x3F800000;
	}
	for (unsigned int i(1472);i<1600;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][1])[i] = KBD_128[i - 1472 + 128];
	}
	for (unsigned int i(1600);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][1])[i] = 0;
	}

// 2: kbd-sin
	for (unsigned int i(0);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][2])[i] = KBD_1024[i];
	}
	for (unsigned int i(1024);i<1472;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][2])[i] = 0x3F800000;
	}
	for (unsigned int i(1472);i<1600;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][2])[i] = SIN_128[i - 1472 + 128];
	}
	for (unsigned int i(1600);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][2])[i] = 0;
	}

// 3: kbd-kbd
	for (unsigned int i(0);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][3])[i] = KBD_1024[i];
	}
	for (unsigned int i(1024);i<1472;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][3])[i] = 0x3F800000;
	}
	for (unsigned int i(1472);i<1600;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][3])[i] = KBD_128[i - 1472 + 128];
	}
	for (unsigned int i(1600);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_START_SEQUENCE][3])[i] = 0;
	}

// EIGHT_SHORT ==========================================================
// 0: sin-sin	
	for (unsigned int w(0);w<8;w++)
		for (unsigned int i(0);i<256;i++) {
			reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][0])[w*256 + i] = SIN_128[i];
		}
// 1: sin-kbd
	for (unsigned int i(0);i<128;i++) {
		reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][1])[i] = SIN_128[i];
	}
	for (unsigned int i(128);i<256;i++) {
		reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][1])[i] = KBD_128[i];
	}

	for (unsigned int w(1);w<8;w++)
		for (unsigned int i(0);i<256;i++) {
			reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][1])[w*256 + i] = KBD_128[i];
		}

// 2: kbd-sin
	for (unsigned int i(0);i<128;i++) {
		reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][2])[i] = KBD_128[i];
	}
	for (unsigned int i(128);i<256;i++) {
		reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][2])[i] = SIN_128[i];
	}

	for (unsigned int w(1);w<8;w++)
		for (unsigned int i(0);i<256;i++) {
			reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][2])[w*256 + i] = SIN_128[i];
		}

// 3: kbd-kbd
	for (unsigned int w(0);w<8;w++)
		for (unsigned int i(0);i<256;i++) {
			reinterpret_cast<unsigned int *>(window_table[EIGHT_SHORT_SEQUENCE][3])[w*256 + i] = KBD_128[i];
		}






// LONG_STOP ============================================================
// 0: sin-sin	
	for (unsigned int i(0);i<448;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][0])[i] = 0;
	}
	for (unsigned int i(448);i<576;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][0])[i] = SIN_128[i - 448];
	}
	for (unsigned int i(576);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][0])[i] = 0x3F800000;
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][0])[i] = SIN_1024[i];
	}

// 1: sin-kbd
	for (unsigned int i(0);i<448;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][1])[i] = 0;
	}
	for (unsigned int i(448);i<576;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][1])[i] = SIN_128[i - 448];
	}
	for (unsigned int i(576);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][1])[i] = 0x3F800000;
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][1])[i] = KBD_1024[i];
	}

// 2: kbd-sin
	for (unsigned int i(0);i<448;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][2])[i] = 0;
	}
	for (unsigned int i(448);i<576;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][2])[i] = KBD_128[i - 448];
	}
	for (unsigned int i(576);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][2])[i] = 0x3F800000;
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][2])[i] = SIN_1024[i];
	}

// 3: kbd-kbd
	for (unsigned int i(0);i<448;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][3])[i] = 0;
	}
	for (unsigned int i(448);i<576;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][3])[i] = KBD_128[i - 448];
	}
	for (unsigned int i(576);i<1024;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][3])[i] = 0x3F800000;
	}
	for (unsigned int i(1024);i<2048;i++) {
		reinterpret_cast<unsigned int *>(window_table[LONG_STOP_SEQUENCE][3])[i] = KBD_1024[i];
	}


	return 0;
}


// ===========================================================================================================================================================================================================
const unsigned int Audio::AAC::sampling_frequencies[] = {	96000, 88200, 64000, 48000,
															44100, 32000, 24000, 22050,
															16000, 12000, 11025, 8000,
															7350, 1, 1, 0
														};

const unsigned int Audio::AAC::num_swb_short_window[] = {	12, 12, 12, 14,
															14, 14, 15, 15,
															15, 15, 15, 15,
															15, 1, 1, 0
														};

const unsigned short Audio::AAC::pred_sfb_max[] = {	33, 33, 38, 40,
													40, 40, 41, 41,
													37, 37, 37, 34,
													34, 34, 34, 34};

const short Audio::AAC::tns_max_sfb[16][2] = {	{31, 9}, {31, 9}, {34, 9}, {40, 14},
													{42, 14}, {51, 14}, {46, 14}, {46, 14},
													{42, 14}, {42, 14}, {42, 14}, {39, 14},
													{0, 0}, {0, 0}, {0, 0}, {0, 0}
												};

const unsigned short Audio::AAC::swb_offset_short[16][16] = {
// 0 1
	0,	4,	8,	12,	16,	20,	24,	32,	40,	48,	64,	92,	8192,	0,	0,	0,
	0,	4,	8,	12,	16,	20,	24,	32,	40,	48,	64,	92,	8192,	0,	0,	0,

//	2
	0,	4,	8,	12,	16,	20,	24,	32,	40,	48,	64,	92,	8192,	0,	0,	0,

// 3, 4, 5
	0,	4,	8,	12,	16,	20,	28,	36,	44,	56,	68,	80, 96, 112,	8192,	0,
	0,	4,	8,	12,	16,	20,	28,	36,	44,	56,	68,	80, 96, 112,	8192,	0,
	0,	4,	8,	12,	16,	20,	28,	36,	44,	56,	68,	80, 96, 112,	8192,	0,

// 6 7
	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	64,	76,	92,	108,	8192,
	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	64,	76,	92,	108,	8192,

// 8 9 10
	0,	4,	8,	12,	16,	20,	24,	28,	32,	40,	48,	60,	72,	88,	108,	8192,
	0,	4,	8,	12,	16,	20,	24,	28,	32,	40,	48,	60,	72,	88,	108,	8192,
	0,	4,	8,	12,	16,	20,	24,	28,	32,	40,	48,	60,	72,	88,	108,	8192,

// 11
	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	60,	72,	88,	108,	8192,

	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	60,	72,	88,	108,	8192,
	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	60,	72,	88,	108,	8192,
	0,	4,	8,	12,	16,	20,	24,	28,	36,	44,	52,	60,	72,	88,	108,	8192

};

const unsigned int Audio::AAC::num_swb_long_window[] = {	41, 41, 47, 49,
															49, 51, 47, 47,
															43, 43, 43, 40,
															40, 1, 1, 0
														};


const unsigned short Audio::AAC::swb_offset_long[16][52] = {
// 0 1
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	44,	48,	52,	56,	64,	72,	80,	88,	96,	108,	120,	132,	144,	156,	172,	188,	212,	240,	276,	320,	384,	448,	512,	576,	640,	704,	768,	832,	896,	960,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	44,	48,	52,	56,	64,	72,	80,	88,	96,	108,	120,	132,	144,	156,	172,	188,	212,	240,	276,	320,	384,	448,	512,	576,	640,	704,	768,	832,	896,	960,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

// 2
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	44,	48,	52,	56,	64,	72,	80,	88,	100,	112,	124,	140,	156,	172,	192,	216,	240,	268,	304,	344,	384,	424,	464,	504,	544,	584,	624,	664,	704,	744,	784,	824,	864,	904,	944,	984,	8192,	0,	0,	0,	0,

// 3 4
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	48,	56,	64,	72,	80,	88,	96,	108,	120,	132,	144,	160,	176,	196,	216,	240,	264,	292,	320,	352,	384,	416,	448,	480,	512,	544,	576,	608,	640,	672,	704,	736,	768,	800,	832,	864,	896,	928,	8192,	0,	0,
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	48,	56,	64,	72,	80,	88,	96,	108,	120,	132,	144,	160,	176,	196,	216,	240,	264,	292,	320,	352,	384,	416,	448,	480,	512,	544,	576,	608,	640,	672,	704,	736,	768,	800,	832,	864,	896,	928,	8192,	0,	0,

// 5
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	48,	56,	64,	72,	80,	88,	96,	108,	120,	132,	144,	160,	176,	196,	216,	240,	264,	292,	320,	352,	384,	416,	448,	480,	512,	544,	576,	608,	640,	672,	704,	736,	768,	800,	832,	864,	896,	928,	960,	992,	8192,

// 6 7
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	44,	52,	60,	68,	76,	84,	92,	100,	108,	116,	124,	136,	148,	160,	172,	188,	204,	220,	240,	260,	284,	308,	336,	364,	396,	432,	468,	508,	552,	600,	652,	704,	768,	832,	896,	960,	8192,	0,	0,	0,	0,
	0,	4,	8,	12,	16,	20,	24,	28,	32,	36,	40,	44,	52,	60,	68,	76,	84,	92,	100,	108,	116,	124,	136,	148,	160,	172,	188,	204,	220,	240,	260,	284,	308,	336,	364,	396,	432,	468,	508,	552,	600,	652,	704,	768,	832,	896,	960,	8192,	0,	0,	0,	0,

// 8 9 10
	0,	8,	16,	24,	32,	40,	48,	56,	64,	72,	80,	88,	100,	112,	124,	136,	148,	160,	172,	184,	196,	212,	228,	244,	260,	280,	300,	320,	344,	368,	396,	424,	456,	492,	532,	572,	616,	664,	716,	772,	832,	896,	960,	8192,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	8,	16,	24,	32,	40,	48,	56,	64,	72,	80,	88,	100,	112,	124,	136,	148,	160,	172,	184,	196,	212,	228,	244,	260,	280,	300,	320,	344,	368,	396,	424,	456,	492,	532,	572,	616,	664,	716,	772,	832,	896,	960,	8192,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	8,	16,	24,	32,	40,	48,	56,	64,	72,	80,	88,	100,	112,	124,	136,	148,	160,	172,	184,	196,	212,	228,	244,	260,	280,	300,	320,	344,	368,	396,	424,	456,	492,	532,	572,	616,	664,	716,	772,	832,	896,	960,	8192,	0,	0,	0,	0,	0,	0,	0,	0,

// 11
	0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	172,	188,	204,	220,	236,	252,	268,	288,	308,	328,	348,	372,	396,	420,	448,	476,	508,	544,	580,	620,	664,	712,	764,	820,	880,	944,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	172,	188,	204,	220,	236,	252,	268,	288,	308,	328,	348,	372,	396,	420,	448,	476,	508,	544,	580,	620,	664,	712,	764,	820,	880,	944,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	172,	188,	204,	220,	236,	252,	268,	288,	308,	328,	348,	372,	396,	420,	448,	476,	508,	544,	580,	620,	664,	712,	764,	820,	880,	944,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	172,	188,	204,	220,	236,	252,	268,	288,	308,	328,	348,	372,	396,	420,	448,	476,	508,	544,	580,	620,	664,	712,	764,	820,	880,	944,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	172,	188,	204,	220,	236,	252,	268,	288,	308,	328,	348,	372,	396,	420,	448,	476,	508,	544,	580,	620,	664,	712,	764,	820,	880,	944,	8192,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0
};

const unsigned short Audio::AAC::swb_offset_shortlong[2][8][40] = {
// 1024
	{
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 68, 76, 84, 92, 100, 112, 124, 136, 148, 164, 184, 208, 236, 268, 300, 332, 364, 396, 428, 460, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 68, 76, 84, 92, 100, 112, 124, 136, 148, 164, 184, 208, 236, 268, 300, 332, 364, 396, 428, 460, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 68, 76, 84, 92, 100, 112, 124, 136, 148, 164, 184, 208, 236, 268, 300, 332, 364, 396, 428, 460, 0, 0, 0, 0,

// 3 4
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 68, 76, 84, 92, 100, 112, 124, 136, 148, 164, 184, 208, 236, 268, 300, 332, 364, 396, 428, 460, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 68, 76, 84, 92, 100, 112, 124, 136, 148, 164, 184, 208, 236, 268, 300, 332, 364, 396, 428, 460, 0, 0, 0, 0,

// 5
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 160, 176, 192, 212, 236, 260, 288, 320, 352, 384, 416, 448, 480, 0, 0, 0,

// 6 7
		0, 4, 8, 12, 16, 20, 24, 28, 32, 39, 40, 44, 52, 60, 68, 80, 92, 104, 120, 140, 164, 192, 224, 256, 288, 320, 352, 384, 416, 448, 480, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 39, 40, 44, 52, 60, 68, 80, 92, 104, 120, 140, 164, 192, 224, 256, 288, 320, 352, 384, 416, 448, 480, 0, 0, 0, 0, 0, 0, 0, 0, 0
	},







// 960
	{
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 156, 172, 188, 212, 240, 272, 304, 336, 368, 400, 432, 0, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 156, 172, 188, 212, 240, 272, 304, 336, 368, 400, 432, 0, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 156, 172, 188, 212, 240, 272, 304, 336, 368, 400, 432, 0, 0, 0, 0, 0,

// 3 4
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 156, 172, 188, 212, 240, 272, 304, 336, 368, 400, 432, 0, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 72, 80, 88, 96, 108, 120, 132, 144, 156, 172, 188, 212, 240, 272, 304, 336, 368, 400, 432, 0, 0, 0, 0, 0,

// 5
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 74, 80, 88, 96, 104, 112, 124, 136, 148, 164, 180, 200, 224, 256, 288, 320, 352, 384, 416, 448, 0, 0, 0,

// 6 7
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 52, 60, 68, 80, 92, 104, 120, 140, 164, 192, 224, 256, 288, 320, 352, 384, 416, 448, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 52, 60, 68, 80, 92, 104, 120, 140, 164, 192, 224, 256, 288, 320, 352, 384, 416, 448, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	}




};





// Huffman tables ==========================================================================================================================================================================================================================================================

const unsigned char Audio::AAC::codebook_cfg[MAX_CODE_BOOK] = {0x00, 0x04, 0x04, 0x05, 0x05, 0x02, 0x02, 0x03, 0x03, 0x03, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00};

const unsigned int Audio::AAC::Huffman_LC[MAX_CODE_BOOK][MAX_CODE_LENGTH][2] = {
// scalefacror
	{{1, 0x00000001}, {0, 0x00000000}, {1, 0x00000005}, {3, 0x0000000D}, {2, 0x0000001C}, {4, 0x0000003C}, {3, 0x0000007B}, {5, 0x000000FB}, {4, 0x000001FA}, {6, 0x000003FA}, {6, 0x000007FA}, {6, 0x00000FFA}, {5, 0x00001FF9}, {8, 0x00003FFA}, {4, 0x00007FF8}, {7, 0x0000FFF7}, {3, 0x0001FFF1}, {7, 0x0003FFE9}, {46, 0x00080000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},

// spectra
	{{1, 0x00000001}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {8, 0x00000018}, {0, 0x00000000}, {24, 0x00000078}, {0, 0x00000000}, {24, 0x000001F8}, {8, 0x000003F8}, {16, 0x00000800}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {1, 0x00000001}, {1, 0x00000003}, {7, 0x0000000D}, {24, 0x00000032}, {15, 0x00000073}, {19, 0x000000F9}, {14, 0x00000200}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{1, 0x00000001}, {0, 0x00000000}, {0, 0x00000000}, {4, 0x0000000C}, {2, 0x00000001A}, {6, 0x0000003A}, {3, 0x00000077}, {5, 0x000000F3}, {15, 0x000001F5}, {15, 0x000003F9}, {8, 0x000007FA}, {9, 0x00000FFD}, {3, 0x000001FFD}, {3, 0x00003FFD}, {5, 0x00007FFF}, {2, 0x00010000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},


	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {10, 0x0000000A}, {6, 0x0000001A}, {0, 0x00000000}, {9, 0x00000071}, {21, 0x000000F7}, {8, 0x0000001F6}, {14, 0x000003FA}, {11, 0x000007FF}, {2, 0x00001000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{1, 0x00000001}, {0, 0x00000000}, {0, 0x00000000}, {4, 0x0000000C}, {4, 0x0000001C}, {0, 0x00000000}, {4, 0x00000074}, {12, 0x000000F4}, {12, 0x0000001F4}, {12, 0x0000003F4}, {18, 0x000007FA}, {10, 0x00000FFE}, {4, 0x00002000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {9, 0x00000009}, {0, 0x00000000}, {16, 0x00000034}, {13, 0x00000075}, {8, 0x000000F2}, {23, 0x000001FB}, {8, 0x000003FE}, {4, 0x00000800}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{1, 0x00000001}, {0, 0x00000000}, {2, 0x00000006}, {1, 0x0000000D}, {0, 0x0000000}, {4, 0x00000038}, {5, 0x00000075}, {10, 0x000000F4}, {14, 0x0000001F6}, {15, 0x000003FB}, {8, 0x000007FE}, {4, 0x000001000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},


	{{0, 0x00000000}, {0, 0x00000000}, {1, 0x00000001}, {5, 0x00000007}, {7, 0x00000015}, {10, 0x00000034}, {14, 0x00000076}, {15, 0x000000FB}, {8, 0x000001FE}, {4, 0x00000400}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{1, 0x00000001}, {0, 0x00000000}, {2, 0x00000006}, {1, 0x0000000D}, {0, 0x00000000}, {4, 0x00000038}, {3, 0x00000073}, {8, 0x000000EE}, {11, 0x000001E7}, {20, 0x000003E2}, {31, 0x000007E3}, {38, 0x00000FEC}, {32, 0x00001FF8}, {14, 0x00003FFE}, {4, 0x00008000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {3, 0x00000003}, {8, 0x0000000E}, {14, 0x0000002A}, {17, 0x00000065}, {25, 0x000000E3}, {31, 0x0000001E5}, {41, 0x000003F3}, {22, 0x000007FC}, {8, 0x00001000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {2, 0x00000002}, {6, 0x0000000A}, {7, 0x0000001B}, {16, 0x00000046}, {59, 0x000000C7}, {55, 0x000001C5}, {95, 0x000003E9}, {43, 0x000007FD}, {6, 0x00001000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},


	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}},
	{{0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}, {0, 0x00000000}}

};


const char Audio::AAC::mod_lav[MAX_CODE_BOOK][320][4] = {
	{
		0,	0,	0,	0,
		-1,	0,	0,	0,
		1,	0,	0,	0,
		-2,	0,	0,	0,
		2,	0,	0,	0,
		-3,	0,	0,	0,
		3,	0,	0,	0,
		-4,	0,	0,	0,
		4,	0,	0,	0,
		-5,	0,	0,	0,
		5,	0,	0,	0,
		6,	0,	0,	0,
		-6,	0,	0,	0,
		7,	0,	0,	0,
		-7,	0,	0,	0,
		8,	0,	0,	0,
		-8,	0,	0,	0,
		9,	0,	0,	0,
		-9,	0,	0,	0,
		10,	0,	0,	0,
		-10,	0,	0,	0,
		-11,	0,	0,	0,
		11,	0,	0,	0,
		12,	0,	0,	0,
		-12,	0,	0,	0,
		13,	0,	0,	0,
		-13,	0,	0,	0,
		14,	0,	0,	0,
		-14,	0,	0,	0,
		16,	0,	0,	0,
		15,	0,	0,	0,
		17,	0,	0,	0,
		18,	0,	0,	0,
		-15,	0,	0,	0,
		-17,	0,	0,	0,
		-16,	0,	0,	0,
		19,	0,	0,	0,
		-18,	0,	0,	0,
		-19,	0,	0,	0,
		20,	0,	0,	0,
		-20,	0,	0,	0,
		21,	0,	0,	0,
		-21,	0,	0,	0,
		22,	0,	0,	0,
		-22,	0,	0,	0,
		23,	0,	0,	0,
		-23,	0,	0,	0,
		-25,	0,	0,	0,
		25,	0,	0,	0,
		-27,	0,	0,	0,
		-24,	0,	0,	0,
		-26,	0,	0,	0,
		24,	0,	0,	0,
		-28,	0,	0,	0,
		27,	0,	0,	0,
		29,	0,	0,	0,
		-30,	0,	0,	0,
		-29,	0,	0,	0,
		26,	0,	0,	0,
		-31,	0,	0,	0,
		-34,	0,	0,	0,
		-33,	0,	0,	0,
		-32,	0,	0,	0,
		-36,	0,	0,	0,
		28,	0,	0,	0,
		-35,	0,	0,	0,
		-38,	0,	0,	0,
		-37,	0,	0,	0,
		30,	0,	0,	0,
		-39,	0,	0,	0,
		-41,	0,	0,	0,
		-57,	0,	0,	0,
		-59,	0,	0,	0,
		-58,	0,	0,	0,
		-60,	0,	0,	0,
		38,	0,	0,	0,
		39,	0,	0,	0,
		40,	0,	0,	0,
		41,	0,	0,	0,
		42,	0,	0,	0,
		57,	0,	0,	0,
		37,	0,	0,	0,
		31,	0,	0,	0,
		32,	0,	0,	0,
		33,	0,	0,	0,
		34,	0,	0,	0,
		35,	0,	0,	0,
		36,	0,	0,	0,
		44,	0,	0,	0,
		51,	0,	0,	0,
		52,	0,	0,	0,
		53,	0,	0,	0,
		54,	0,	0,	0,
		55,	0,	0,	0,
		56,	0,	0,	0,
		50,	0,	0,	0,
		45,	0,	0,	0,
		46,	0,	0,	0,
		47,	0,	0,	0,
		48,	0,	0,	0,
		49,	0,	0,	0,
		58,	0,	0,	0,
		-54,	0,	0,	0,
		-52,	0,	0,	0,
		-51,	0,	0,	0,
		-50,	0,	0,	0,
		-55,	0,	0,	0,
		43,	0,	0,	0,
		60,	0,	0,	0,
		59,	0,	0,	0,
		-56,	0,	0,	0,
		-53,	0,	0,	0,
		-45,	0,	0,	0,
		-44,	0,	0,	0,
		-42,	0,	0,	0,
		-40,	0,	0,	0,
		-43,	0,	0,	0,
		-49,	0,	0,	0,
		-48,	0,	0,	0,
		-46,	0,	0,	0,
		-47,	0,	0,	0
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		-1,	0,	0,	0,
		0,	0,	0,	-1,
		0,	1,	0,	0,
		0,	0,	0,	1,
		0,	0,	-1,	0,
		0,	0,	1,	0,
		0,	-1,	0,	0,
		1,	-1,	0,	0,
		-1,	1,	0,	0,
		0,	0,	-1,	1,
		0,	1,	-1,	0,
		0,	-1,	1,	0,
		0,	0,	1,	-1,
		1,	1,	0,	0,
		0,	0,	-1,	-1,
		-1,	-1,	0,	0,
		0,	-1,	-1,	0,
		1,	0,	-1,	0,
		0,	1,	0,	-1,
		-1,	0,	1,	0,
		0,	0,	1,	1,
		1,	0,	1,	0,
		0,	-1,	0,	1,
		0,	1,	1,	0,
		0,	1,	0,	1,
		-1,	0,	-1,	0,
		1,	0,	0,	1,
		-1,	0,	0,	-1,
		1,	0,	0,	-1,
		-1,	0,	0,	1,
		0,	-1,	0,	-1,
		1,	1,	-1,	0,
		-1,	1,	-1,	0,
		1,	-1,	1,	0,
		0,	1,	1,	-1,
		0,	1,	-1,	1,
		0,	-1,	1,	1,
		0,	-1,	1,	-1,
		1,	-1,	-1,	0,
		1,	0,	-1,	1,
		0,	1,	-1,	-1,
		-1,	1,	1,	0,
		-1,	0,	1,	-1,
		-1,	-1,	1,	0,
		0,	-1,	-1,	1,
		1,	-1,	0,	1,
		1,	-1,	0,	-1,
		-1,	1,	0,	-1,
		-1,	-1,	-1,	0,
		0,	-1,	-1,	-1,
		0,	1,	1,	1,
		1,	0,	1,	-1,
		1,	1,	0,	1,
		-1,	1,	0,	1,
		1,	1,	1,	0,
		-1,	-1,	0,	1,
		-1,	0,	-1,	-1,
		1,	1,	0,	-1,
		1,	0,	-1,	-1,
		-1,	0,	-1,	1,
		-1,	-1,	0,	-1,
		-1,	0,	1,	1,
		1,	0,	1,	1,
		1,	-1,	1,	-1,
		-1,	1,	-1,	1,
		-1,	1,	1,	-1,
		1,	-1,	-1,	1,
		1,	1,	1,	1,
		-1,	-1,	1,	1,
		1,	1,	-1,	-1,
		-1,	-1,	1,	-1,
		-1,	-1,	-1,	-1,
		1,	1,	-1,	1,
		1,	-1,	1,	1,
		-1,	1,	1,	1,
		-1,	1,	-1,	-1,
		-1,	-1,	-1,	1,
		1,	-1,	-1,	-1,
		1,	1,	1,	-1
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		-1,	0,	0,	0,
		0,	0,	0,	1,
		0,	0,	-1,	0,
		0,	0,	0,	-1,
		0,	-1,	0,	0,
		0,	0,	1,	0,
		0,	1,	0,	0,
		0,	-1,	1,	0,
		-1,	1,	0,	0,
		0,	1,	-1,	0,
		0,	0,	1,	-1,
		0,	1,	0,	-1,
		0,	0,	-1,	1,
		-1,	0,	0,	-1,
		1,	-1,	0,	0,
		1,	0,	-1,	0,
		-1,	-1,	0,	0,
		0,	0,	-1,	-1,
		1,	0,	1,	0,
		1,	0,	0,	1,
		0,	-1,	0,	1,
		-1,	0,	1,	0,
		0,	1,	0,	1,
		0,	-1,	-1,	0,
		-1,	0,	0,	1,
		0,	-1,	0,	-1,
		-1,	0,	-1,	0,
		1,	1,	0,	0,
		0,	1,	1,	0,
		0,	0,	1,	1,
		1,	0,	0,	-1,
		0,	1,	-1,	1,
		1,	0,	-1,	1,
		-1,	1,	-1,	0,
		0,	-1,	1,	-1,
		1,	-1,	1,	0,
		1,	1,	0,	-1,
		1,	0,	1,	1,
		-1,	1,	1,	0,
		0,	-1,	-1,	1,
		1,	1,	1,	0,
		-1,	0,	1,	-1,
		-1,	-1,	-1,	0,
		-1,	0,	-1,	1,
		1,	-1,	-1,	0,
		1,	1,	-1,	0,
		1,	-1,	0,	1,
		-1,	1,	0,	-1,
		-1,	-1,	1,	0,
		-1,	0,	1,	1,
		-1,	-1,	0,	1,
		-1,	-1,	0,	-1,
		0,	-1,	-1,	-1,
		1,	0,	1,	-1,
		1,	0,	-1,	-1,
		0,	1,	-1,	-1,
		0,	1,	1,	1,
		-1,	1,	0,	1,
		-1,	0,	-1,	-1,
		0,	1,	1,	-1,
		1,	-1,	0,	-1,
		0,	-1,	1,	1,
		1,	1,	0,	1,
		1,	-1,	1,	-1,
		-1,	1,	-1,	1,
		1,	-1,	-1,	1,
		-1,	-1,	-1,	-1,
		-1,	1,	1,	-1,
		-1,	1,	1,	1,
		1,	1,	1,	1,
		-1,	-1,	1,	-1,
		1,	-1,	1,	1,
		-1,	1,	-1,	-1,
		-1,	-1,	1,	1,
		1,	1,	-1,	-1,
		1,	-1,	-1,	-1,
		-1,	-1,	-1,	1,
		1,	1,	-1,	1,
		1,	1,	1,	-1
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		0,	0,	0,	1,
		0,	1,	0,	0,
		0,	0,	1,	0,
		1,	1,	0,	0,
		0,	0,	1,	1,
		0,	1,	1,	0,
		0,	1,	0,	1,
		1,	0,	1,	0,
		0,	1,	1,	1,
		1,	0,	0,	1,
		1,	1,	1,	0,
		1,	1,	1,	1,
		1,	0,	1,	1,
		1,	1,	0,	1,
		2,	0,	0,	0,
		0,	0,	0,	2,
		0,	0,	1,	2,
		2,	1,	0,	0,
		1,	2,	1,	0,
		0,	0,	2,	1,
		0,	1,	2,	1,
		1,	2,	0,	0,
		0,	1,	1,	2,
		2,	1,	1,	0,
		0,	0,	2,	0,
		0,	2,	1,	0,
		0,	1,	2,	0,
		0,	2,	0,	0,
		0,	1,	0,	2,
		2,	0,	1,	0,
		1,	2,	1,	1,
		0,	2,	1,	1,
		1,	1,	2,	0,
		1,	1,	2,	1,
		1,	2,	0,	1,
		1,	0,	2,	0,
		1,	0,	2,	1,
		0,	2,	0,	1,
		2,	1,	1,	1,
		1,	1,	1,	2,
		2,	1,	0,	1,
		1,	0,	1,	2,
		0,	0,	2,	2,
		0,	1,	2,	2,
		2,	2,	1,	0,
		1,	2,	2,	0,
		1,	0,	0,	2,
		2,	0,	0,	1,
		0,	2,	2,	1,
		2,	2,	0,	0,
		1,	2,	2,	1,
		1,	1,	0,	2,
		2,	0,	1,	1,
		1,	1,	2,	2,
		2,	2,	1,	1,
		0,	2,	2,	0,
		0,	2,	1,	2,
		1,	0,	2,	2,
		2,	2,	0,	1,
		2,	1,	2,	0,
		2,	2,	2,	0,
		0,	2,	2,	2,
		2,	2,	2,	1,
		2,	1,	2,	1,
		1,	2,	1,	2,
		1,	2,	2,	2,
		0,	2,	0,	2,
		2,	0,	2,	0,
		1,	2,	0,	2,
		2,	0,	2,	1,
		2,	1,	1,	2,
		2,	1,	0,	2,
		2,	2,	2,	2,
		2,	2,	1,	2,
		2,	1,	2,	2,
		2,	0,	1,	2,
		2,	0,	0,	2,
		2,	2,	0,	2,
		2,	0,	2,	2
	},

	{
		1,	1,	1,	1,
		0,	1,	1,	1,
		1,	1,	0,	1,
		1,	1,	1,	0,
		1,	0,	1,	1,
		1,	0,	0,	0,
		1,	1,	0,	0,
		0,	0,	0,	0,
		0,	0,	1,	1,
		1,	0,	1,	0,
		1,	0,	0,	1,
		0,	1,	1,	0,
		0,	0,	0,	1,
		0,	1,	0,	1,
		0,	0,	1,	0,
		0,	1,	0,	0,
		2,	1,	1,	1,
		1,	1,	2,	1,
		1,	2,	1,	1,
		1,	1,	1,	2,
		2,	1,	1,	0,
		2,	1,	0,	1,
		1,	2,	1,	0,
		2,	0,	1,	1,
		0,	1,	2,	1,
		0,	1,	1,	2,
		1,	1,	2,	0,
		0,	2,	1,	1,
		1,	0,	1,	2,
		1,	2,	0,	1,
		1,	1,	0,	2,
		1,	0,	2,	1,
		2,	1,	0,	0,
		2,	0,	1,	0,
		1,	2,	0,	0,
		2,	0,	0,	1,
		0,	1,	0,	2,
		0,	2,	1,	0,
		0,	0,	1,	2,
		0,	1,	2,	0,
		0,	2,	0,	1,
		1,	0,	0,	2,
		0,	0,	2,	1,
		1,	0,	2,	0,
		2,	0,	0,	0,
		0,	0,	0,	2,
		0,	2,	0,	0,
		0,	0,	2,	0,
		1,	2,	2,	1,
		2,	2,	1,	1,
		2,	1,	2,	1,
		1,	1,	2,	2,
		1,	2,	1,	2,
		2,	1,	1,	2,
		1,	2,	2,	0,
		2,	2,	1,	0,
		2,	1,	2,	0,
		0,	2,	2,	1,
		0,	1,	2,	2,
		2,	2,	0,	1,
		0,	2,	1,	2,
		2,	0,	2,	1,
		1,	0,	2,	2,
		2,	2,	2,	1,
		1,	2,	0,	2,
		2,	0,	1,	2,
		2,	1,	0,	2,
		1,	2,	2,	2,
		2,	1,	2,	2,
		2,	2,	1,	2,
		0,	2,	2,	0,
		2,	2,	0,	0,
		0,	0,	2,	2,
		2,	0,	2,	0,
		0,	2,	0,	2,
		2,	0,	0,	2,
		2,	2,	2,	2,
		0,	2,	2,	2,
		2,	2,	2,	0,
		2,	2,	0,	2,
		2,	0,	2,	2
	},

	{
		0,	0,	0,	0,
		-1,	0,	0,	0,
		1,	0,	0,	0,
		0,	0,	1,	0,
		0,	0,	-1,	0,
		1,	0,	-1,	0,
		-1,	0,	1,	0,
		-1,	0,	-1,	0,
		1,	0,	1,	0,
		-2,	0,	0,	0,
		0,	0,	2,	0,
		2,	0,	0,	0,
		0,	0,	-2,	0,
		-2,	0,	-1,	0,
		2,	0,	1,	0,
		-1,	0,	-2,	0,
		1,	0,	2,	0,
		-2,	0,	1,	0,
		2,	0,	-1,	0,
		-1,	0,	2,	0,
		1,	0,	-2,	0,
		-3,	0,	0,	0,
		3,	0,	0,	0,
		0,	0,	-3,	0,
		0,	0,	3,	0,
		-3,	0,	-1,	0,
		1,	0,	3,	0,
		3,	0,	1,	0,
		-1,	0,	-3,	0,
		-3,	0,	1,	0,
		3,	0,	-1,	0,
		1,	0,	-3,	0,
		-1,	0,	3,	0,
		-2,	0,	2,	0,
		2,	0,	2,	0,
		-2,	0,	-2,	0,
		2,	0,	-2,	0,
		-3,	0,	-2,	0,
		3,	0,	-2,	0,
		-2,	0,	3,	0,
		2,	0,	-3,	0,
		3,	0,	2,	0,
		2,	0,	3,	0,
		-3,	0,	2,	0,
		-2,	0,	-3,	0,
		0,	0,	-4,	0,
		-4,	0,	0,	0,
		4,	0,	1,	0,
		4,	0,	0,	0,
		-4,	0,	-1,	0,
		0,	0,	4,	0,
		4,	0,	-1,	0,
		-1,	0,	-4,	0,
		1,	0,	4,	0,
		-1,	0,	4,	0,
		-4,	0,	1,	0,
		1,	0,	-4,	0,
		3,	0,	-3,	0,
		-3,	0,	-3,	0,
		-3,	0,	3,	0,
		-2,	0,	4,	0,
		-4,	0,	-2,	0,
		4,	0,	2,	0,
		2,	0,	-4,	0,
		2,	0,	4,	0,
		3,	0,	3,	0,
		-4,	0,	2,	0,
		-2,	0,	-4,	0,
		4,	0,	-2,	0,
		3,	0,	-4,	0,
		-4,	0,	-3,	0,
		-4,	0,	3,	0,
		3,	0,	4,	0,
		-3,	0,	4,	0,
		4,	0,	3,	0,
		4,	0,	-3,	0,
		-3,	0,	-4,	0,
		4,	0,	-4,	0,
		-4,	0,	4,	0,
		4,	0,	4,	0,
		-4,	0,	-4,	0
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		0,	0,	-1,	0,
		0,	0,	1,	0,
		-1,	0,	0,	0,
		1,	0,	1,	0,
		-1,	0,	1,	0,
		1,	0,	-1,	0,
		-1,	0,	-1,	0,
		2,	0,	-1,	0,
		2,	0,	1,	0,
		-2,	0,	1,	0,
		-2,	0,	-1,	0,
		-2,	0,	0,	0,
		-1,	0,	2,	0,
		2,	0,	0,	0,
		1,	0,	-2,	0,
		1,	0,	2,	0,
		0,	0,	-2,	0,
		-1,	0,	-2,	0,
		0,	0,	2,	0,
		2,	0,	-2,	0,
		-2,	0,	2,	0,
		-2,	0,	-2,	0,
		2,	0,	2,	0,
		-3,	0,	1,	0,
		3,	0,	1,	0,
		3,	0,	-1,	0,
		-1,	0,	3,	0,
		-3,	0,	-1,	0,
		1,	0,	3,	0,
		1,	0,	-3,	0,
		-1,	0,	-3,	0,
		3,	0,	0,	0,
		-3,	0,	0,	0,
		0,	0,	-3,	0,
		0,	0,	3,	0,
		3,	0,	2,	0,
		-3,	0,	-2,	0,
		-2,	0,	3,	0,
		2,	0,	3,	0,
		3,	0,	-2,	0,
		2,	0,	-3,	0,
		-2,	0,	-3,	0,
		-3,	0,	2,	0,
		3,	0,	3,	0,
		3,	0,	-3,	0,
		-3,	0,	-3,	0,
		-3,	0,	3,	0,
		1,	0,	-4,	0,
		-1,	0,	-4,	0,
		4,	0,	1,	0,
		-4,	0,	1,	0,
		-4,	0,	-1,	0,
		1,	0,	4,	0,
		4,	0,	-1,	0,
		-1,	0,	4,	0,
		0,	0,	-4,	0,
		-4,	0,	2,	0,
		-4,	0,	-2,	0,
		2,	0,	4,	0,
		-2,	0,	-4,	0,
		-4,	0,	0,	0,
		4,	0,	2,	0,
		4,	0,	-2,	0,
		-2,	0,	4,	0,
		4,	0,	0,	0,
		2,	0,	-4,	0,
		0,	0,	4,	0,
		-3,	0,	-4,	0,
		-3,	0,	4,	0,
		3,	0,	-4,	0,
		4,	0,	-3,	0,
		3,	0,	4,	0,
		4,	0,	3,	0,
		-4,	0,	3,	0,
		-4,	0,	-3,	0,
		4,	0,	4,	0,
		-4,	0,	4,	0,
		-4,	0,	-4,	0,
		4,	0,	-4,	0
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		0,	0,	1,	0,
		1,	0,	1,	0,
		2,	0,	1,	0,
		1,	0,	2,	0,
		2,	0,	0,	0,
		0,	0,	2,	0,
		3,	0,	1,	0,
		1,	0,	3,	0,
		2,	0,	2,	0,
		3,	0,	0,	0,
		0,	0,	3,	0,
		2,	0,	3,	0,
		3,	0,	2,	0,
		1,	0,	4,	0,
		4,	0,	1,	0,
		1,	0,	5,	0,
		5,	0,	1,	0,
		3,	0,	3,	0,
		2,	0,	4,	0,
		0,	0,	4,	0,
		4,	0,	0,	0,
		4,	0,	2,	0,
		2,	0,	5,	0,
		5,	0,	2,	0,
		0,	0,	5,	0,
		6,	0,	1,	0,
		5,	0,	0,	0,
		1,	0,	6,	0,
		4,	0,	3,	0,
		3,	0,	5,	0,
		3,	0,	4,	0,
		5,	0,	3,	0,
		2,	0,	6,	0,
		6,	0,	2,	0,
		1,	0,	7,	0,
		3,	0,	6,	0,
		0,	0,	6,	0,
		6,	0,	0,	0,
		4,	0,	4,	0,
		7,	0,	1,	0,
		4,	0,	5,	0,
		7,	0,	2,	0,
		5,	0,	4,	0,
		6,	0,	3,	0,
		2,	0,	7,	0,
		7,	0,	3,	0,
		6,	0,	4,	0,
		5,	0,	5,	0,
		4,	0,	6,	0,
		3,	0,	7,	0,
		7,	0,	0,	0,
		0,	0,	7,	0,
		6,	0,	5,	0,
		5,	0,	6,	0,
		7,	0,	4,	0,
		4,	0,	7,	0,
		5,	0,	7,	0,
		7,	0,	5,	0,
		7,	0,	6,	0,
		6,	0,	6,	0,
		6,	0,	7,	0,
		7,	0,	7,	0
	},

	{
		1,	0,	1,	0,
		2,	0,	1,	0,
		1,	0,	0,	0,
		1,	0,	2,	0,
		0,	0,	1,	0,
		2,	0,	2,	0,
		0,	0,	0,	0,
		2,	0,	0,	0,
		0,	0,	2,	0,
		3,	0,	1,	0,
		1,	0,	3,	0,
		3,	0,	2,	0,
		2,	0,	3,	0,
		3,	0,	3,	0,
		4,	0,	1,	0,
		1,	0,	4,	0,
		4,	0,	2,	0,
		2,	0,	4,	0,
		3,	0,	0,	0,
		0,	0,	3,	0,
		4,	0,	3,	0,
		3,	0,	4,	0,
		5,	0,	2,	0,
		5,	0,	1,	0,
		2,	0,	5,	0,
		1,	0,	5,	0,
		5,	0,	3,	0,
		3,	0,	5,	0,
		4,	0,	4,	0,
		5,	0,	4,	0,
		0,	0,	4,	0,
		4,	0,	5,	0,
		4,	0,	0,	0,
		2,	0,	6,	0,
		6,	0,	2,	0,
		6,	0,	1,	0,
		1,	0,	6,	0,
		3,	0,	6,	0,
		6,	0,	3,	0,
		5,	0,	5,	0,
		5,	0,	0,	0,
		6,	0,	4,	0,
		0,	0,	5,	0,
		4,	0,	6,	0,
		7,	0,	1,	0,
		7,	0,	2,	0,
		2,	0,	7,	0,
		6,	0,	5,	0,
		7,	0,	3,	0,
		1,	0,	7,	0,
		5,	0,	6,	0,
		3,	0,	7,	0,
		6,	0,	6,	0,
		7,	0,	4,	0,
		6,	0,	0,	0,
		4,	0,	7,	0,
		0,	0,	6,	0,
		7,	0,	5,	0,
		7,	0,	6,	0,
		6,	0,	7,	0,
		5,	0,	7,	0,
		7,	0,	0,	0,
		0,	0,	7,	0,
		7,	0,	7,	0
	},

	{
		0,	0,	0,	0,
		1,	0,	0,	0,
		0,	0,	1,	0,
		1,	0,	1,	0,
		2,	0,	1,	0,
		1,	0,	2,	0,
		2,	0,	0,	0,
		0,	0,	2,	0,
		3,	0,	1,	0,
		2,	0,	2,	0,
		1,	0,	3,	0,
		3,	0,	0,	0,
		0,	0,	3,	0,
		2,	0,	3,	0,
		3,	0,	2,	0,
		1,	0,	4,	0,
		4,	0,	1,	0,
		2,	0,	4,	0,
		1,	0,	5,	0,
		4,	0,	2,	0,
		3,	0,	3,	0,
		0,	0,	4,	0,
		4,	0,	0,	0,
		5,	0,	1,	0,
		2,	0,	5,	0,
		1,	0,	6,	0,
		3,	0,	4,	0,
		5,	0,	2,	0,
		6,	0,	1,	0,
		4,	0,	3,	0,
		0,	0,	5,	0,
		2,	0,	6,	0,
		5,	0,	0,	0,
		1,	0,	7,	0,
		3,	0,	5,	0,
		1,	0,	8,	0,
		8,	0,	1,	0,
		4,	0,	4,	0,
		5,	0,	3,	0,
		6,	0,	2,	0,
		7,	0,	1,	0,
		0,	0,	6,	0,
		8,	0,	2,	0,
		2,	0,	8,	0,
		3,	0,	6,	0,
		2,	0,	7,	0,
		4,	0,	5,	0,
		9,	0,	1,	0,
		1,	0,	9,	0,
		7,	0,	2,	0,
		6,	0,	0,	0,
		5,	0,	4,	0,
		6,	0,	3,	0,
		8,	0,	3,	0,
		0,	0,	7,	0,
		9,	0,	2,	0,
		3,	0,	8,	0,
		4,	0,	6,	0,
		3,	0,	7,	0,
		0,	0,	8,	0,
		10,	0,	1,	0,
		6,	0,	4,	0,
		2,	0,	9,	0,
		5,	0,	5,	0,
		8,	0,	0,	0,
		7,	0,	0,	0,
		7,	0,	3,	0,
		10,	0,	2,	0,
		9,	0,	3,	0,
		8,	0,	4,	0,
		1,	0,	10,	0,
		7,	0,	4,	0,
		6,	0,	5,	0,
		5,	0,	6,	0,
		4,	0,	8,	0,
		4,	0,	7,	0,
		3,	0,	9,	0,
		11,	0,	1,	0,
		5,	0,	8,	0,
		9,	0,	0,	0,
		8,	0,	5,	0,
		10,	0,	3,	0,
		2,	0,	10,	0,
		0,	0,	9,	0,
		11,	0,	2,	0,
		9,	0,	4,	0,
		6,	0,	6,	0,
		12,	0,	1,	0,
		4,	0,	9,	0,
		8,	0,	6,	0,
		1,	0,	11,	0,
		9,	0,	5,	0,
		10,	0,	4,	0,
		5,	0,	7,	0,
		7,	0,	5,	0,
		2,	0,	11,	0,
		1,	0,	12,	0,
		12,	0,	2,	0,
		11,	0,	3,	0,
		3,	0,	10,	0,
		5,	0,	9,	0,
		6,	0,	7,	0,
		8,	0,	7,	0,
		11,	0,	4,	0,
		0,	0,	10,	0,
		7,	0,	6,	0,
		12,	0,	3,	0,
		10,	0,	0,	0,
		10,	0,	5,	0,
		4,	0,	10,	0,
		6,	0,	8,	0,
		2,	0,	12,	0,
		9,	0,	6,	0,
		9,	0,	7,	0,
		4,	0,	11,	0,
		11,	0,	0,	0,
		6,	0,	9,	0,
		3,	0,	11,	0,
		5,	0,	10,	0,
		8,	0,	8,	0,
		7,	0,	8,	0,
		12,	0,	5,	0,
		3,	0,	12,	0,
		11,	0,	5,	0,
		7,	0,	7,	0,
		12,	0,	4,	0,
		11,	0,	6,	0,
		10,	0,	6,	0,
		4,	0,	12,	0,
		7,	0,	9,	0,
		5,	0,	11,	0,
		0,	0,	11,	0,
		12,	0,	6,	0,
		6,	0,	10,	0,
		12,	0,	0,	0,
		10,	0,	7,	0,
		5,	0,	12,	0,
		7,	0,	10,	0,
		9,	0,	8,	0,
		0,	0,	12,	0,
		11,	0,	7,	0,
		8,	0,	9,	0,
		9,	0,	9,	0,
		10,	0,	8,	0,
		7,	0,	11,	0,
		12,	0,	7,	0,
		6,	0,	11,	0,
		8,	0,	11,	0,
		11,	0,	8,	0,
		7,	0,	12,	0,
		6,	0,	12,	0,
		8,	0,	10,	0,
		10,	0,	9,	0,
		8,	0,	12,	0,
		9,	0,	10,	0,
		9,	0,	11,	0,
		9,	0,	12,	0,
		10,	0,	11,	0,
		12,	0,	9,	0,
		10,	0,	10,	0,
		11,	0,	9,	0,
		12,	0,	8,	0,
		11,	0,	10,	0,
		12,	0,	10,	0,
		12,	0,	11,	0,
		10,	0,	12,	0,
		11,	0,	11,	0,
		11,	0,	12,	0,
		12,	0,	12,	0
	},

	{
		1,	0,	1,	0,
		1,	0,	2,	0,
		2,	0,	1,	0,
		2,	0,	2,	0,
		1,	0,	0,	0,
		0,	0,	1,	0,
		1,	0,	3,	0,
		3,	0,	2,	0,
		3,	0,	1,	0,
		2,	0,	3,	0,
		3,	0,	3,	0,
		2,	0,	0,	0,
		0,	0,	2,	0,
		2,	0,	4,	0,
		4,	0,	2,	0,
		1,	0,	4,	0,
		4,	0,	1,	0,
		0,	0,	0,	0,
		4,	0,	3,	0,
		3,	0,	4,	0,
		3,	0,	0,	0,
		0,	0,	3,	0,
		4,	0,	4,	0,
		2,	0,	5,	0,
		5,	0,	2,	0,
		1,	0,	5,	0,
		5,	0,	1,	0,
		5,	0,	3,	0,
		3,	0,	5,	0,
		5,	0,	4,	0,
		4,	0,	5,	0,
		6,	0,	2,	0,
		2,	0,	6,	0,
		6,	0,	3,	0,
		4,	0,	0,	0,
		6,	0,	1,	0,
		0,	0,	4,	0,
		1,	0,	6,	0,
		3,	0,	6,	0,
		5,	0,	5,	0,
		6,	0,	4,	0,
		4,	0,	6,	0,
		6,	0,	5,	0,
		7,	0,	2,	0,
		3,	0,	7,	0,
		2,	0,	7,	0,
		5,	0,	6,	0,
		8,	0,	2,	0,
		7,	0,	3,	0,
		5,	0,	0,	0,
		7,	0,	1,	0,
		0,	0,	5,	0,
		8,	0,	1,	0,
		1,	0,	7,	0,
		8,	0,	3,	0,
		7,	0,	4,	0,
		4,	0,	7,	0,
		2,	0,	8,	0,
		6,	0,	6,	0,
		7,	0,	5,	0,
		1,	0,	8,	0,
		3,	0,	8,	0,
		8,	0,	4,	0,
		4,	0,	8,	0,
		5,	0,	7,	0,
		8,	0,	5,	0,
		5,	0,	8,	0,
		7,	0,	6,	0,
		6,	0,	7,	0,
		9,	0,	2,	0,
		6,	0,	0,	0,
		6,	0,	8,	0,
		9,	0,	3,	0,
		3,	0,	9,	0,
		9,	0,	1,	0,
		2,	0,	9,	0,
		0,	0,	6,	0,
		8,	0,	6,	0,
		9,	0,	4,	0,
		4,	0,	9,	0,
		10,	0,	2,	0,
		1,	0,	9,	0,
		7,	0,	7,	0,
		8,	0,	7,	0,
		9,	0,	5,	0,
		7,	0,	8,	0,
		10,	0,	3,	0,
		5,	0,	9,	0,
		10,	0,	4,	0,
		2,	0,	10,	0,
		10,	0,	1,	0,
		3,	0,	10,	0,
		9,	0,	6,	0,
		6,	0,	9,	0,
		8,	0,	0,	0,
		4,	0,	10,	0,
		7,	0,	0,	0,
		11,	0,	2,	0,
		7,	0,	9,	0,
		11,	0,	3,	0,
		10,	0,	6,	0,
		1,	0,	10,	0,
		11,	0,	1,	0,
		9,	0,	7,	0,
		0,	0,	7,	0,
		8,	0,	8,	0,
		10,	0,	5,	0,
		3,	0,	11,	0,
		5,	0,	10,	0,
		8,	0,	9,	0,
		11,	0,	5,	0,
		0,	0,	8,	0,
		11,	0,	4,	0,
		2,	0,	11,	0,
		7,	0,	10,	0,
		6,	0,	10,	0,
		10,	0,	7,	0,
		4,	0,	11,	0,
		1,	0,	11,	0,
		12,	0,	2,	0,
		9,	0,	8,	0,
		12,	0,	3,	0,
		11,	0,	6,	0,
		5,	0,	11,	0,
		12,	0,	4,	0,
		11,	0,	7,	0,
		12,	0,	5,	0,
		3,	0,	12,	0,
		6,	0,	11,	0,
		9,	0,	0,	0,
		10,	0,	8,	0,
		10,	0,	0,	0,
		12,	0,	1,	0,
		0,	0,	9,	0,
		4,	0,	12,	0,
		9,	0,	9,	0,
		12,	0,	6,	0,
		2,	0,	12,	0,
		8,	0,	10,	0,
		9,	0,	10,	0,
		1,	0,	12,	0,
		11,	0,	8,	0,
		12,	0,	7,	0,
		7,	0,	11,	0,
		5,	0,	12,	0,
		6,	0,	12,	0,
		10,	0,	9,	0,
		8,	0,	11,	0,
		12,	0,	8,	0,
		0,	0,	10,	0,
		7,	0,	12,	0,
		11,	0,	0,	0,
		10,	0,	10,	0,
		11,	0,	9,	0,
		11,	0,	10,	0,
		0,	0,	11,	0,
		11,	0,	11,	0,
		9,	0,	11,	0,
		10,	0,	11,	0,
		12,	0,	0,	0,
		8,	0,	12,	0,
		12,	0,	9,	0,
		10,	0,	12,	0,
		9,	0,	12,	0,
		11,	0,	12,	0,
		12,	0,	11,	0,
		0,	0,	12,	0,
		12,	0,	10,	0,
		12,	0,	12,	0
	},

	{
		0,	0,	0,	0,
		1,	0,	1,	0,
		16,	0,	16,	0,
		1,	0,	0,	0,
		0,	0,	1,	0,
		2,	0,	1,	0,
		1,	0,	2,	0,
		2,	0,	2,	0,
		1,	0,	3,	0,
		3,	0,	1,	0,
		3,	0,	2,	0,
		2,	0,	0,	0,
		2,	0,	3,	0,
		0,	0,	2,	0,
		3,	0,	3,	0,
		4,	0,	1,	0,
		1,	0,	4,	0,
		4,	0,	2,	0,
		2,	0,	4,	0,
		4,	0,	3,	0,
		3,	0,	4,	0,
		3,	0,	0,	0,
		0,	0,	3,	0,
		5,	0,	1,	0,
		5,	0,	2,	0,
		2,	0,	5,	0,
		4,	0,	4,	0,
		1,	0,	5,	0,
		5,	0,	3,	0,
		3,	0,	5,	0,
		5,	0,	4,	0,
		4,	0,	5,	0,
		6,	0,	2,	0,
		2,	0,	6,	0,
		6,	0,	1,	0,
		6,	0,	3,	0,
		3,	0,	6,	0,
		1,	0,	6,	0,
		4,	0,	16,	0,
		3,	0,	16,	0,
		16,	0,	5,	0,
		16,	0,	3,	0,
		16,	0,	4,	0,
		6,	0,	4,	0,
		16,	0,	6,	0,
		4,	0,	0,	0,
		4,	0,	6,	0,
		0,	0,	4,	0,
		2,	0,	16,	0,
		5,	0,	5,	0,
		5,	0,	16,	0,
		16,	0,	7,	0,
		16,	0,	2,	0,
		16,	0,	8,	0,
		2,	0,	7,	0,
		7,	0,	2,	0,
		3,	0,	7,	0,
		6,	0,	5,	0,
		5,	0,	6,	0,
		6,	0,	16,	0,
		16,	0,	10,	0,
		7,	0,	3,	0,
		7,	0,	1,	0,
		16,	0,	9,	0,
		7,	0,	16,	0,
		1,	0,	16,	0,
		1,	0,	7,	0,
		4,	0,	7,	0,
		16,	0,	11,	0,
		7,	0,	4,	0,
		16,	0,	12,	0,
		8,	0,	16,	0,
		16,	0,	1,	0,
		6,	0,	6,	0,
		9,	0,	16,	0,
		2,	0,	8,	0,
		5,	0,	7,	0,
		10,	0,	16,	0,
		16,	0,	13,	0,
		8,	0,	3,	0,
		8,	0,	2,	0,
		3,	0,	8,	0,
		5,	0,	0,	0,
		16,	0,	14,	0,
		11,	0,	16,	0,
		7,	0,	5,	0,
		4,	0,	8,	0,
		6,	0,	7,	0,
		7,	0,	6,	0,
		0,	0,	5,	0,
		8,	0,	4,	0,
		16,	0,	15,	0,
		12,	0,	16,	0,
		1,	0,	8,	0,
		8,	0,	1,	0,
		14,	0,	16,	0,
		5,	0,	8,	0,
		13,	0,	16,	0,
		3,	0,	9,	0,
		8,	0,	5,	0,
		7,	0,	7,	0,
		2,	0,	9,	0,
		8,	0,	6,	0,
		9,	0,	2,	0,
		9,	0,	3,	0,
		15,	0,	16,	0,
		4,	0,	9,	0,
		6,	0,	8,	0,
		6,	0,	0,	0,
		9,	0,	4,	0,
		5,	0,	9,	0,
		8,	0,	7,	0,
		7,	0,	8,	0,
		1,	0,	9,	0,
		10,	0,	3,	0,
		0,	0,	6,	0,
		10,	0,	2,	0,
		9,	0,	1,	0,
		9,	0,	5,	0,
		4,	0,	10,	0,
		2,	0,	10,	0,
		9,	0,	6,	0,
		3,	0,	10,	0,
		6,	0,	9,	0,
		10,	0,	4,	0,
		8,	0,	8,	0,
		10,	0,	5,	0,
		9,	0,	7,	0,
		11,	0,	3,	0,
		1,	0,	10,	0,
		7,	0,	0,	0,
		10,	0,	6,	0,
		7,	0,	9,	0,
		3,	0,	11,	0,
		5,	0,	10,	0,
		10,	0,	1,	0,
		4,	0,	11,	0,
		11,	0,	2,	0,
		13,	0,	2,	0,
		6,	0,	10,	0,
		13,	0,	3,	0,
		2,	0,	11,	0,
		16,	0,	0,	0,
		5,	0,	11,	0,
		11,	0,	5,	0,
		11,	0,	4,	0,
		9,	0,	8,	0,
		7,	0,	10,	0,
		8,	0,	9,	0,
		0,	0,	16,	0,
		4,	0,	13,	0,
		0,	0,	7,	0,
		3,	0,	13,	0,
		11,	0,	6,	0,
		13,	0,	1,	0,
		13,	0,	4,	0,
		12,	0,	3,	0,
		2,	0,	13,	0,
		13,	0,	5,	0,
		8,	0,	10,	0,
		6,	0,	11,	0,
		10,	0,	8,	0,
		10,	0,	7,	0,
		14,	0,	2,	0,
		12,	0,	4,	0,
		1,	0,	11,	0,
		4,	0,	12,	0,
		11,	0,	1,	0,
		3,	0,	12,	0,
		1,	0,	13,	0,
		12,	0,	2,	0,
		7,	0,	11,	0,
		3,	0,	14,	0,
		5,	0,	12,	0,
		5,	0,	13,	0,
		14,	0,	4,	0,
		4,	0,	14,	0,
		11,	0,	7,	0,
		14,	0,	3,	0,
		12,	0,	5,	0,
		13,	0,	6,	0,
		12,	0,	6,	0,
		8,	0,	0,	0,
		11,	0,	8,	0,
		2,	0,	12,	0,
		9,	0,	9,	0,
		14,	0,	5,	0,
		6,	0,	13,	0,
		10,	0,	10,	0,
		15,	0,	2,	0,
		8,	0,	11,	0,
		9,	0,	10,	0,
		14,	0,	6,	0,
		10,	0,	9,	0,
		5,	0,	14,	0,
		11,	0,	9,	0,
		14,	0,	1,	0,
		2,	0,	14,	0,
		6,	0,	12,	0,
		1,	0,	12,	0,
		13,	0,	8,	0,
		0,	0,	8,	0,
		13,	0,	7,	0,
		7,	0,	12,	0,
		12,	0,	7,	0,
		7,	0,	13,	0,
		15,	0,	3,	0,
		12,	0,	1,	0,
		6,	0,	14,	0,
		2,	0,	15,	0,
		15,	0,	5,	0,
		15,	0,	4,	0,
		1,	0,	14,	0,
		9,	0,	11,	0,
		4,	0,	15,	0,
		14,	0,	7,	0,
		8,	0,	13,	0,
		13,	0,	9,	0,
		8,	0,	12,	0,
		5,	0,	15,	0,
		3,	0,	15,	0,
		10,	0,	11,	0,
		11,	0,	10,	0,
		12,	0,	8,	0,
		15,	0,	6,	0,
		15,	0,	7,	0,
		8,	0,	14,	0,
		15,	0,	1,	0,
		7,	0,	14,	0,
		9,	0,	0,	0,
		0,	0,	9,	0,
		9,	0,	13,	0,
		9,	0,	12,	0,
		12,	0,	9,	0,
		14,	0,	8,	0,
		10,	0,	13,	0,
		14,	0,	9,	0,
		12,	0,	10,	0,
		6,	0,	15,	0,
		7,	0,	15,	0,
		9,	0,	14,	0,
		15,	0,	8,	0,
		11,	0,	11,	0,
		11,	0,	14,	0,
		1,	0,	15,	0,
		10,	0,	12,	0,
		10,	0,	14,	0,
		13,	0,	11,	0,
		13,	0,	10,	0,
		11,	0,	13,	0,
		11,	0,	12,	0,
		8,	0,	15,	0,
		14,	0,	11,	0,
		13,	0,	12,	0,
		12,	0,	13,	0,
		15,	0,	9,	0,
		14,	0,	10,	0,
		10,	0,	0,	0,
		12,	0,	11,	0,
		9,	0,	15,	0,
		0,	0,	10,	0,
		12,	0,	12,	0,
		11,	0,	0,	0,
		12,	0,	14,	0,
		10,	0,	15,	0,
		13,	0,	13,	0,
		0,	0,	13,	0,
		14,	0,	12,	0,
		15,	0,	10,	0,
		15,	0,	11,	0,
		11,	0,	15,	0,
		14,	0,	13,	0,
		13,	0,	0,	0,
		0,	0,	11,	0,
		13,	0,	14,	0,
		15,	0,	12,	0,
		15,	0,	13,	0,
		12,	0,	15,	0,
		14,	0,	0,	0,
		14,	0,	14,	0,
		13,	0,	15,	0,
		12,	0,	0,	0,
		14,	0,	15,	0,
		0,	0,	14,	0,
		0,	0,	12,	0,
		15,	0,	14,	0,
		15,	0,	0,	0,
		0,	0,	15,	0,
		15,	0,	15,	0
	}


};


const unsigned int Audio::AAC::SIN_1024[2048] = {

	0x3a490fd9,	0x3b16cbdb,	0x3b7b53a9,	0x3bafeda8,	0x3be23160,	0x3c0a3a7b,	0x3c235c30,	0x3c3c7dcc,	0x3c559f4c,	0x3c6ec0aa,	0x3c83f0f2,	0x3c90817a,	0x3c9d11ec,	0x3ca9a246,	0x3cb63286,	0x3cc2c2a9,	
	0x3ccf52af,	0x3cdbe294,	0x3ce87258,	0x3cf501f8,	0x3d00c8b9,	0x3d071062,	0x3d0d57f6,	0x3d139f75,	0x3d19e6dd,	0x3d202e2d,	0x3d267564,	0x3d2cbc82,	0x3d330385,	0x3d394a6c,	0x3d3f9137,	0x3d45d7e4,	
	0x3d4c1e73,	0x3d5264e2,	0x3d58ab31,	0x3d5ef15f,	0x3d65376a,	0x3d6b7d51,	0x3d71c315,	0x3d7808b3,	0x3d7e4e2b,	0x3d8249be,	0x3d856c52,	0x3d888ed2,	0x3d8bb13c,	0x3d8ed391,	0x3d91f5d0,	0x3d9517f9,	
	0x3d983a0a,	0x3d9b5c05,	0x3d9e7de7,	0x3da19fb0,	0x3da4c161,	0x3da7e2f8,	0x3dab0476,	0x3dae25d9,	0x3db14721,	0x3db4684e,	0x3db7895f,	0x3dbaaa54,	0x3dbdcb2c,	0x3dc0ebe6,	0x3dc40c83,	0x3dc72d02,	
	0x3dca4d62,	0x3dcd6da3,	0x3dd08dc4,	0x3dd3adc5,	0x3dd6cda5,	0x3dd9ed64,	0x3ddd0d02,	0x3de02c7d,	0x3de34bd6,	0x3de66b0c,	0x3de98a1f,	0x3deca90d,	0x3defc7d7,	0x3df2e67c,	0x3df604fb,	0x3df92354,	
	0x3dfc4187,	0x3dff5f94,	0x3e013ebc,	0x3e02cd9b,	0x3e045c65,	0x3e05eb1b,	0x3e0779bc,	0x3e090848,	0x3e0a96bf,	0x3e0c2521,	0x3e0db36d,	0x3e0f41a4,	0x3e10cfc4,	0x3e125dce,	0x3e13ebc1,	0x3e15799e,	
	0x3e170763,	0x3e189511,	0x3e1a22a8,	0x3e1bb027,	0x3e1d3d8e,	0x3e1ecadd,	0x3e205813,	0x3e21e530,	0x3e237235,	0x3e24ff20,	0x3e268bf2,	0x3e2818aa,	0x3e29a548,	0x3e2b31cc,	0x3e2cbe36,	0x3e2e4a85,	
	0x3e2fd6b9,	0x3e3162d2,	0x3e32eecf,	0x3e347ab2,	0x3e360678,	0x3e379222,	0x3e391db0,	0x3e3aa921,	0x3e3c3476,	0x3e3dbfad,	0x3e3f4ac7,	0x3e40d5c4,	0x3e4260a3,	0x3e43eb64,	0x3e457607,	0x3e47008b,	
	0x3e488af1,	0x3e4a1538,	0x3e4b9f5f,	0x3e4d2967,	0x3e4eb350,	0x3e503d19,	0x3e51c6c1,	0x3e535049,	0x3e54d9b1,	0x3e5662f8,	0x3e57ec1d,	0x3e597522,	0x3e5afe05,	0x3e5c86c6,	0x3e5e0f65,	0x3e5f97e2,	
	0x3e61203c,	0x3e62a874,	0x3e643089,	0x3e65b87a,	0x3e674048,	0x3e68c7f3,	0x3e6a4f79,	0x3e6bd6dc,	0x3e6d5e1a,	0x3e6ee533,	0x3e706c28,	0x3e71f2f7,	0x3e7379a1,	0x3e750026,	0x3e768685,	0x3e780cbe,	
	0x3e7992d0,	0x3e7b18bc,	0x3e7c9e82,	0x3e7e2420,	0x3e7fa998,	0x3e809774,	0x3e815a08,	0x3e821c88,	0x3e82def4,	0x3e83a14c,	0x3e84638f,	0x3e8525bf,	0x3e85e7d9,	0x3e86a9df,	0x3e876bd0,	0x3e882dad,	
	0x3e88ef74,	0x3e89b126,	0x3e8a72c3,	0x3e8b344b,	0x3e8bf5bd,	0x3e8cb71a,	0x3e8d7861,	0x3e8e3992,	0x3e8efaad,	0x3e8fbbb2,	0x3e907ca1,	0x3e913d79,	0x3e91fe3c,	0x3e92bee7,	0x3e937f7d,	0x3e943ffb,	
	0x3e950062,	0x3e95c0b3,	0x3e9680ec,	0x3e97410e,	0x3e980119,	0x3e98c10d,	0x3e9980e9,	0x3e9a40ad,	0x3e9b0059,	0x3e9bbfee,	0x3e9c7f6a,	0x3e9d3ecf,	0x3e9dfe1b,	0x3e9ebd4f,	0x3e9f7c6a,	0x3ea03b6d,	
	0x3ea0fa57,	0x3ea1b928,	0x3ea277e0,	0x3ea3367f,	0x3ea3f505,	0x3ea4b372,	0x3ea571c5,	0x3ea62fff,	0x3ea6ee1f,	0x3ea7ac25,	0x3ea86a12,	0x3ea927e5,	0x3ea9e59d,	0x3eaaa33b,	0x3eab60bf,	0x3eac1e29,	
	0x3eacdb78,	0x3ead98ac,	0x3eae55c6,	0x3eaf12c5,	0x3eafcfa8,	0x3eb08c71,	0x3eb1491e,	0x3eb205b0,	0x3eb2c227,	0x3eb37e82,	0x3eb43ac1,	0x3eb4f6e5,	0x3eb5b2ec,	0x3eb66ed8,	0x3eb72aa7,	0x3eb7e65b,	
	0x3eb8a1f1,	0x3eb95d6c,	0x3eba18ca,	0x3ebad40b,	0x3ebb8f2f,	0x3ebc4a36,	0x3ebd0521,	0x3ebdbfee,	0x3ebe7a9e,	0x3ebf3530,	0x3ebfefa5,	0x3ec0a9fd,	0x3ec16437,	0x3ec21e53,	0x3ec2d851,	0x3ec39231,	
	0x3ec44bf2,	0x3ec50596,	0x3ec5bf1b,	0x3ec67882,	0x3ec731ca,	0x3ec7eaf3,	0x3ec8a3fd,	0x3ec95ce9,	0x3eca15b5,	0x3ecace63,	0x3ecb86f1,	0x3ecc3f60,	0x3eccf7af,	0x3ecdafde,	0x3ece67ee,	0x3ecf1fde,	
	0x3ecfd7ae,	0x3ed08f5e,	0x3ed146ee,	0x3ed1fe5e,	0x3ed2b5ad,	0x3ed36cdb,	0x3ed423ea,	0x3ed4dad7,	0x3ed591a4,	0x3ed6484f,	0x3ed6feda,	0x3ed7b543,	0x3ed86b8b,	0x3ed921b2,	0x3ed9d7b7,	0x3eda8d9b,	
	0x3edb435d,	0x3edbf8fd,	0x3edcae7c,	0x3edd63d8,	0x3ede1912,	0x3edece2a,	0x3edf831f,	0x3ee037f2,	0x3ee0eca3,	0x3ee1a130,	0x3ee2559b,	0x3ee309e3,	0x3ee3be08,	0x3ee4720a,	0x3ee525e9,	0x3ee5d9a4,	
	0x3ee68d3c,	0x3ee740b1,	0x3ee7f401,	0x3ee8a72e,	0x3ee95a37,	0x3eea0d1c,	0x3eeabfdd,	0x3eeb727a,	0x3eec24f3,	0x3eecd747,	0x3eed8976,	0x3eee3b81,	0x3eeeed67,	0x3eef9f28,	0x3ef050c5,	0x3ef1023c,	
	0x3ef1b38e,	0x3ef264bb,	0x3ef315c2,	0x3ef3c6a4,	0x3ef47761,	0x3ef527f8,	0x3ef5d868,	0x3ef688b3,	0x3ef738d8,	0x3ef7e8d7,	0x3ef898b0,	0x3ef94862,	0x3ef9f7ee,	0x3efaa753,	0x3efb5692,	0x3efc05aa,	
	0x3efcb49b,	0x3efd6365,	0x3efe1207,	0x3efec083,	0x3eff6ed8,	0x3f000e82,	0x3f006585,	0x3f00bc74,	0x3f01134f,	0x3f016a17,	0x3f01c0ca,	0x3f021769,	0x3f026df4,	0x3f02c46b,	0x3f031ace,	0x3f03711d,	
	0x3f03c757,	0x3f041d7e,	0x3f04738f,	0x3f04c98d,	0x3f051f75,	0x3f05754a,	0x3f05cb0a,	0x3f0620b5,	0x3f06764b,	0x3f06cbcd,	0x3f07213a,	0x3f077692,	0x3f07cbd5,	0x3f082103,	0x3f08761c,	0x3f08cb20,	
	0x3f092010,	0x3f0974e9,	0x3f09c9ae,	0x3f0a1e5e,	0x3f0a72f8,	0x3f0ac77d,	0x3f0b1bec,	0x3f0b7046,	0x3f0bc48b,	0x3f0c18ba,	0x3f0c6cd3,	0x3f0cc0d7,	0x3f0d14c5,	0x3f0d689d,	0x3f0dbc5f,	0x3f0e100c,	
	0x3f0e63a2,	0x3f0eb723,	0x3f0f0a8e,	0x3f0f5de2,	0x3f0fb121,	0x3f100449,	0x3f10575b,	0x3f10aa57,	0x3f10fd3d,	0x3f11500c,	0x3f11a2c5,	0x3f11f567,	0x3f1247f3,	0x3f129a68,	0x3f12ecc7,	0x3f133f0f,	
	0x3f139140,	0x3f13e35b,	0x3f14355e,	0x3f14874b,	0x3f14d921,	0x3f152ae0,	0x3f157c88,	0x3f15ce19,	0x3f161f93,	0x3f1670f6,	0x3f16c241,	0x3f171376,	0x3f176493,	0x3f17b598,	0x3f180687,	0x3f18575d,	
	0x3f18a81d,	0x3f18f8c4,	0x3f194955,	0x3f1999cd,	0x3f19ea2e,	0x3f1a3a77,	0x3f1a8aa8,	0x3f1adac2,	0x3f1b2ac3,	0x3f1b7aad,	0x3f1bca7f,	0x3f1c1a38,	0x3f1c69da,	0x3f1cb963,	0x3f1d08d5,	0x3f1d582e,	
	0x3f1da76f,	0x3f1df697,	0x3f1e45a7,	0x3f1e949f,	0x3f1ee37e,	0x3f1f3245,	0x3f1f80f3,	0x3f1fcf89,	0x3f201e06,	0x3f206c6a,	0x3f20bab6,	0x3f2108e9,	0x3f215703,	0x3f21a504,	0x3f21f2ec,	0x3f2240bb,	
	0x3f228e71,	0x3f22dc0e,	0x3f232992,	0x3f2376fd,	0x3f23c44f,	0x3f241187,	0x3f245ea6,	0x3f24abac,	0x3f24f898,	0x3f25456b,	0x3f259224,	0x3f25dec4,	0x3f262b4a,	0x3f2677b7,	0x3f26c40a,	0x3f271043,	
	0x3f275c62,	0x3f27a868,	0x3f27f454,	0x3f284026,	0x3f288bde,	0x3f28d77c,	0x3f292300,	0x3f296e69,	0x3f29b9b9,	0x3f2a04ef,	0x3f2a500a,	0x3f2a9b0b,	0x3f2ae5f2,	0x3f2b30be,	0x3f2b7b70,	0x3f2bc608,	
	0x3f2c1085,	0x3f2c5ae7,	0x3f2ca52f,	0x3f2cef5d,	0x3f2d396f,	0x3f2d8367,	0x3f2dcd44,	0x3f2e1707,	0x3f2e60ae,	0x3f2eaa3b,	0x3f2ef3ad,	0x3f2f3d03,	0x3f2f863f,	0x3f2fcf60,	0x3f301865,	0x3f306150,	
	0x3f30aa1f,	0x3f30f2d3,	0x3f313b6b,	0x3f3183e9,	0x3f31cc4b,	0x3f321491,	0x3f325cbc,	0x3f32a4cc,	0x3f32ecc0,	0x3f333498,	0x3f337c55,	0x3f33c3f6,	0x3f340b7b,	0x3f3452e5,	0x3f349a33,	0x3f34e165,	
	0x3f35287b,	0x3f356f75,	0x3f35b653,	0x3f35fd15,	0x3f3643bb,	0x3f368a45,	0x3f36d0b3,	0x3f371704,	0x3f375d3a,	0x3f37a353,	0x3f37e950,	0x3f382f30,	0x3f3874f4,	0x3f38ba9c,	0x3f390027,	0x3f394595,	
	0x3f398ae7,	0x3f39d01d,	0x3f3a1535,	0x3f3a5a31,	0x3f3a9f10,	0x3f3ae3d3,	0x3f3b2879,	0x3f3b6d01,	0x3f3bb16d,	0x3f3bf5bc,	0x3f3c39ee,	0x3f3c7e03,	0x3f3cc1fb,	0x3f3d05d6,	0x3f3d4993,	0x3f3d8d34,	
	0x3f3dd0b7,	0x3f3e141d,	0x3f3e5766,	0x3f3e9a91,	0x3f3edd9f,	0x3f3f208f,	0x3f3f6362,	0x3f3fa617,	0x3f3fe8af,	0x3f402b2a,	0x3f406d86,	0x3f40afc5,	0x3f40f1e7,	0x3f4133ea,	0x3f4175d0,	0x3f41b798,	
	0x3f41f942,	0x3f423ace,	0x3f427c3c,	0x3f42bd8c,	0x3f42febe,	0x3f433fd2,	0x3f4380c8,	0x3f43c1a0,	0x3f44025a,	0x3f4442f5,	0x3f448372,	0x3f44c3d1,	0x3f450411,	0x3f454433,	0x3f458437,	0x3f45c41c,	
	0x3f4603e3,	0x3f46438b,	0x3f468315,	0x3f46c280,	0x3f4701cc,	0x3f4740fa,	0x3f478008,	0x3f47bef9,	0x3f47fdca,	0x3f483c7c,	0x3f487b10,	0x3f48b985,	0x3f48f7da,	0x3f493611,	0x3f497429,	0x3f49b222,	
	0x3f49effb,	0x3f4a2db6,	0x3f4a6b51,	0x3f4aa8cd,	0x3f4ae62a,	0x3f4b2367,	0x3f4b6085,	0x3f4b9d84,	0x3f4bda63,	0x3f4c1723,	0x3f4c53c4,	0x3f4c9045,	0x3f4ccca6,	0x3f4d08e8,	0x3f4d450a,	0x3f4d810d,	
	0x3f4dbcf0,	0x3f4df8b3,	0x3f4e3456,	0x3f4e6fda,	0x3f4eab3e,	0x3f4ee681,	0x3f4f21a5,	0x3f4f5ca9,	0x3f4f978d,	0x3f4fd252,	0x3f500cf6,	0x3f504779,	0x3f5081dd,	0x3f50bc21,	0x3f50f644,	0x3f513047,	
	0x3f516a2a,	0x3f51a3ed,	0x3f51dd8f,	0x3f521711,	0x3f525073,	0x3f5289b4,	0x3f52c2d5,	0x3f52fbd5,	0x3f5334b5,	0x3f536d74,	0x3f53a612,	0x3f53de90,	0x3f5416ed,	0x3f544f2a,	0x3f548745,	0x3f54bf40,	
	0x3f54f71a,	0x3f552ed4,	0x3f55666c,	0x3f559de3,	0x3f55d53a,	0x3f560c70,	0x3f564384,	0x3f567a78,	0x3f56b14a,	0x3f56e7fb,	0x3f571e8c,	0x3f5754fb,	0x3f578b48,	0x3f57c175,	0x3f57f780,	0x3f582d6a,	
	0x3f586333,	0x3f5898da,	0x3f58ce60,	0x3f5903c5,	0x3f593908,	0x3f596e29,	0x3f59a329,	0x3f59d807,	0x3f5a0cc4,	0x3f5a415f,	0x3f5a75d9,	0x3f5aaa31,	0x3f5ade67,	0x3f5b127b,	0x3f5b466e,	0x3f5b7a3e,	
	0x3f5baded,	0x3f5be17a,	0x3f5c14e6,	0x3f5c482f,	0x3f5c7b56,	0x3f5cae5b,	0x3f5ce13e,	0x3f5d13ff,	0x3f5d469e,	0x3f5d791b,	0x3f5dab76,	0x3f5dddaf,	0x3f5e0fc5,	0x3f5e41b9,	0x3f5e738b,	0x3f5ea53a,	
	0x3f5ed6c8,	0x3f5f0833,	0x3f5f397b,	0x3f5f6aa1,	0x3f5f9ba5,	0x3f5fcc86,	0x3f5ffd44,	0x3f602de0,	0x3f605e5a,	0x3f608eb0,	0x3f60bee5,	0x3f60eef6,	0x3f611ee5,	0x3f614eb1,	0x3f617e5b,	0x3f61ade1,	
	0x3f61dd45,	0x3f620c86,	0x3f623ba4,	0x3f626aa0,	0x3f629978,	0x3f62c82d,	0x3f62f6c0,	0x3f63252f,	0x3f63537b,	0x3f6381a5,	0x3f63afab,	0x3f63dd8e,	0x3f640b4e,	0x3f6438eb,	0x3f646665,	0x3f6493bb,	
	0x3f64c0ee,	0x3f64edfe,	0x3f651aeb,	0x3f6547b4,	0x3f65745a,	0x3f65a0dc,	0x3f65cd3b,	0x3f65f977,	0x3f66258f,	0x3f665184,	0x3f667d55,	0x3f66a903,	0x3f66d48d,	0x3f66fff3,	0x3f672b36,	0x3f675655,	
	0x3f678151,	0x3f67ac29,	0x3f67d6dd,	0x3f68016d,	0x3f682bda,	0x3f685623,	0x3f688047,	0x3f68aa49,	0x3f68d426,	0x3f68fddf,	0x3f692774,	0x3f6950e6,	0x3f697a33,	0x3f69a35d,	0x3f69cc62,	0x3f69f543,	
	0x3f6a1e01,	0x3f6a469a,	0x3f6a6f0f,	0x3f6a9760,	0x3f6abf8c,	0x3f6ae795,	0x3f6b0f79,	0x3f6b3739,	0x3f6b5ed5,	0x3f6b864c,	0x3f6bada0,	0x3f6bd4ce,	0x3f6bfbd9,	0x3f6c22bf,	0x3f6c4980,	0x3f6c701e,	
	0x3f6c9696,	0x3f6cbcea,	0x3f6ce31a,	0x3f6d0925,	0x3f6d2f0c,	0x3f6d54ce,	0x3f6d7a6c,	0x3f6d9fe4,	0x3f6dc539,	0x3f6dea68,	0x3f6e0f73,	0x3f6e3459,	0x3f6e591a,	0x3f6e7db7,	0x3f6ea22f,	0x3f6ec682,	
	0x3f6eeab0,	0x3f6f0eba,	0x3f6f329e,	0x3f6f565e,	0x3f6f79f8,	0x3f6f9d6e,	0x3f6fc0bf,	0x3f6fe3eb,	0x3f7006f2,	0x3f7029d4,	0x3f704c91,	0x3f706f28,	0x3f70919b,	0x3f70b3e9,	0x3f70d611,	0x3f70f814,	
	0x3f7119f3,	0x3f713bac,	0x3f715d3f,	0x3f717eae,	0x3f719ff7,	0x3f71c11b,	0x3f71e21a,	0x3f7202f4,	0x3f7223a8,	0x3f724437,	0x3f7264a0,	0x3f7284e4,	0x3f72a503,	0x3f72c4fc,	0x3f72e4d0,	0x3f73047e,	
	0x3f732407,	0x3f73436b,	0x3f7362a8,	0x3f7381c1,	0x3f73a0b4,	0x3f73bf81,	0x3f73de28,	0x3f73fcaa,	0x3f741b07,	0x3f74393e,	0x3f74574f,	0x3f74753a,	0x3f749300,	0x3f74b0a0,	0x3f74ce1a,	0x3f74eb6f,	
	0x3f75089d,	0x3f7525a6,	0x3f754289,	0x3f755f47,	0x3f757bde,	0x3f759850,	0x3f75b49c,	0x3f75d0c2,	0x3f75ecc2,	0x3f76089c,	0x3f762450,	0x3f763fde,	0x3f765b46,	0x3f767688,	0x3f7691a4,	0x3f76ac9a,	
	0x3f76c76b,	0x3f76e215,	0x3f76fc99,	0x3f7716f6,	0x3f77312e,	0x3f774b40,	0x3f77652b,	0x3f777ef1,	0x3f779890,	0x3f77b209,	0x3f77cb5c,	0x3f77e488,	0x3f77fd8f,	0x3f78166f,	0x3f782f29,	0x3f7847bc,	
	0x3f78602a,	0x3f787871,	0x3f789091,	0x3f78a88c,	0x3f78c060,	0x3f78d80e,	0x3f78ef95,	0x3f7906f6,	0x3f791e30,	0x3f793544,	0x3f794c32,	0x3f7962f9,	0x3f79799a,	0x3f799014,	0x3f79a668,	0x3f79bc95,	
	0x3f79d29c,	0x3f79e87c,	0x3f79fe36,	0x3f7a13c9,	0x3f7a2936,	0x3f7a3e7c,	0x3f7a539b,	0x3f7a6894,	0x3f7a7d66,	0x3f7a9212,	0x3f7aa697,	0x3f7abaf5,	0x3f7acf2d,	0x3f7ae33d,	0x3f7af728,	0x3f7b0aeb,	
	0x3f7b1e88,	0x3f7b31fe,	0x3f7b454e,	0x3f7b5876,	0x3f7b6b78,	0x3f7b7e53,	0x3f7b9107,	0x3f7ba395,	0x3f7bb5fc,	0x3f7bc83b,	0x3f7bda55,	0x3f7bec47,	0x3f7bfe12,	0x3f7c0fb7,	0x3f7c2134,	0x3f7c328b,	
	0x3f7c43bb,	0x3f7c54c4,	0x3f7c65a6,	0x3f7c7661,	0x3f7c86f5,	0x3f7c9762,	0x3f7ca7a9,	0x3f7cb7c8,	0x3f7cc7c0,	0x3f7cd792,	0x3f7ce73c,	0x3f7cf6c0,	0x3f7d061c,	0x3f7d1551,	0x3f7d2460,	0x3f7d3347,	
	0x3f7d4207,	0x3f7d50a0,	0x3f7d5f13,	0x3f7d6d5e,	0x3f7d7b82,	0x3f7d897e,	0x3f7d9754,	0x3f7da503,	0x3f7db28a,	0x3f7dbfeb,	0x3f7dcd24,	0x3f7dda36,	0x3f7de721,	0x3f7df3e5,	0x3f7e0082,	0x3f7e0cf7,	
	0x3f7e1946,	0x3f7e256d,	0x3f7e316d,	0x3f7e3d46,	0x3f7e48f7,	0x3f7e5482,	0x3f7e5fe5,	0x3f7e6b21,	0x3f7e7635,	0x3f7e8123,	0x3f7e8be9,	0x3f7e9688,	0x3f7ea100,	0x3f7eab50,	0x3f7eb579,	0x3f7ebf7b,	
	0x3f7ec955,	0x3f7ed309,	0x3f7edc95,	0x3f7ee5f9,	0x3f7eef37,	0x3f7ef84d,	0x3f7f013c,	0x3f7f0a03,	0x3f7f12a3,	0x3f7f1b1c,	0x3f7f236d,	0x3f7f2b97,	0x3f7f339a,	0x3f7f3b75,	0x3f7f4329,	0x3f7f4ab6,	
	0x3f7f521b,	0x3f7f5959,	0x3f7f606f,	0x3f7f675f,	0x3f7f6e26,	0x3f7f74c7,	0x3f7f7b40,	0x3f7f8191,	0x3f7f87bb,	0x3f7f8dbe,	0x3f7f9399,	0x3f7f994d,	0x3f7f9eda,	0x3f7fa43f,	0x3f7fa97d,	0x3f7fae93,	
	0x3f7fb382,	0x3f7fb849,	0x3f7fbce9,	0x3f7fc161,	0x3f7fc5b2,	0x3f7fc9dc,	0x3f7fcdde,	0x3f7fd1b9,	0x3f7fd56c,	0x3f7fd8f8,	0x3f7fdc5c,	0x3f7fdf99,	0x3f7fe2af,	0x3f7fe59d,	0x3f7fe863,	0x3f7feb02,	
	0x3f7fed7a,	0x3f7fefca,	0x3f7ff1f3,	0x3f7ff3f4,	0x3f7ff5ce,	0x3f7ff780,	0x3f7ff90b,	0x3f7ffa6e,	0x3f7ffbaa,	0x3f7ffcbe,	0x3f7ffdab,	0x3f7ffe70,	0x3f7fff0e,	0x3f7fff85,	0x3f7fffd4,	0x3f7ffffb,	
	0x3f7ffffb,	0x3f7fffd4,	0x3f7fff85,	0x3f7fff0e,	0x3f7ffe70,	0x3f7ffdab,	0x3f7ffcbe,	0x3f7ffbaa,	0x3f7ffa6e,	0x3f7ff90b,	0x3f7ff780,	0x3f7ff5ce,	0x3f7ff3f4,	0x3f7ff1f3,	0x3f7fefca,	0x3f7fed7a,	
	0x3f7feb02,	0x3f7fe863,	0x3f7fe59d,	0x3f7fe2af,	0x3f7fdf99,	0x3f7fdc5c,	0x3f7fd8f8,	0x3f7fd56c,	0x3f7fd1b9,	0x3f7fcdde,	0x3f7fc9dc,	0x3f7fc5b2,	0x3f7fc161,	0x3f7fbce9,	0x3f7fb849,	0x3f7fb382,	
	0x3f7fae93,	0x3f7fa97d,	0x3f7fa43f,	0x3f7f9eda,	0x3f7f994d,	0x3f7f9399,	0x3f7f8dbe,	0x3f7f87bb,	0x3f7f8191,	0x3f7f7b40,	0x3f7f74c7,	0x3f7f6e26,	0x3f7f675f,	0x3f7f606f,	0x3f7f5959,	0x3f7f521b,	
	0x3f7f4ab6,	0x3f7f4329,	0x3f7f3b75,	0x3f7f339a,	0x3f7f2b97,	0x3f7f236d,	0x3f7f1b1c,	0x3f7f12a3,	0x3f7f0a03,	0x3f7f013c,	0x3f7ef84d,	0x3f7eef37,	0x3f7ee5f9,	0x3f7edc95,	0x3f7ed309,	0x3f7ec955,	
	0x3f7ebf7b,	0x3f7eb579,	0x3f7eab50,	0x3f7ea100,	0x3f7e9688,	0x3f7e8be9,	0x3f7e8123,	0x3f7e7635,	0x3f7e6b21,	0x3f7e5fe5,	0x3f7e5482,	0x3f7e48f7,	0x3f7e3d46,	0x3f7e316d,	0x3f7e256d,	0x3f7e1946,	
	0x3f7e0cf7,	0x3f7e0082,	0x3f7df3e5,	0x3f7de721,	0x3f7dda36,	0x3f7dcd24,	0x3f7dbfeb,	0x3f7db28a,	0x3f7da503,	0x3f7d9754,	0x3f7d897e,	0x3f7d7b82,	0x3f7d6d5e,	0x3f7d5f13,	0x3f7d50a0,	0x3f7d4207,	
	0x3f7d3347,	0x3f7d2460,	0x3f7d1551,	0x3f7d061c,	0x3f7cf6c0,	0x3f7ce73c,	0x3f7cd792,	0x3f7cc7c0,	0x3f7cb7c8,	0x3f7ca7a9,	0x3f7c9762,	0x3f7c86f5,	0x3f7c7661,	0x3f7c65a6,	0x3f7c54c4,	0x3f7c43bb,	
	0x3f7c328b,	0x3f7c2134,	0x3f7c0fb7,	0x3f7bfe12,	0x3f7bec47,	0x3f7bda55,	0x3f7bc83b,	0x3f7bb5fc,	0x3f7ba395,	0x3f7b9107,	0x3f7b7e53,	0x3f7b6b78,	0x3f7b5876,	0x3f7b454e,	0x3f7b31fe,	0x3f7b1e88,	
	0x3f7b0aeb,	0x3f7af728,	0x3f7ae33d,	0x3f7acf2d,	0x3f7abaf5,	0x3f7aa697,	0x3f7a9212,	0x3f7a7d66,	0x3f7a6894,	0x3f7a539b,	0x3f7a3e7c,	0x3f7a2936,	0x3f7a13c9,	0x3f79fe36,	0x3f79e87c,	0x3f79d29c,	
	0x3f79bc95,	0x3f79a668,	0x3f799014,	0x3f79799a,	0x3f7962f9,	0x3f794c32,	0x3f793544,	0x3f791e30,	0x3f7906f6,	0x3f78ef95,	0x3f78d80e,	0x3f78c060,	0x3f78a88c,	0x3f789091,	0x3f787871,	0x3f78602a,	
	0x3f7847bc,	0x3f782f29,	0x3f78166f,	0x3f77fd8f,	0x3f77e488,	0x3f77cb5c,	0x3f77b209,	0x3f779890,	0x3f777ef1,	0x3f77652b,	0x3f774b40,	0x3f77312e,	0x3f7716f6,	0x3f76fc99,	0x3f76e215,	0x3f76c76b,	
	0x3f76ac9a,	0x3f7691a4,	0x3f767688,	0x3f765b46,	0x3f763fde,	0x3f762450,	0x3f76089c,	0x3f75ecc2,	0x3f75d0c2,	0x3f75b49c,	0x3f759850,	0x3f757bde,	0x3f755f47,	0x3f754289,	0x3f7525a6,	0x3f75089d,	
	0x3f74eb6f,	0x3f74ce1a,	0x3f74b0a0,	0x3f749300,	0x3f74753a,	0x3f74574f,	0x3f74393e,	0x3f741b07,	0x3f73fcaa,	0x3f73de28,	0x3f73bf81,	0x3f73a0b4,	0x3f7381c1,	0x3f7362a8,	0x3f73436b,	0x3f732407,	
	0x3f73047e,	0x3f72e4d0,	0x3f72c4fc,	0x3f72a503,	0x3f7284e4,	0x3f7264a0,	0x3f724437,	0x3f7223a8,	0x3f7202f4,	0x3f71e21a,	0x3f71c11b,	0x3f719ff7,	0x3f717eae,	0x3f715d3f,	0x3f713bac,	0x3f7119f3,	
	0x3f70f814,	0x3f70d611,	0x3f70b3e9,	0x3f70919b,	0x3f706f28,	0x3f704c91,	0x3f7029d4,	0x3f7006f2,	0x3f6fe3eb,	0x3f6fc0bf,	0x3f6f9d6e,	0x3f6f79f8,	0x3f6f565e,	0x3f6f329e,	0x3f6f0eba,	0x3f6eeab0,	
	0x3f6ec682,	0x3f6ea22f,	0x3f6e7db7,	0x3f6e591a,	0x3f6e3459,	0x3f6e0f73,	0x3f6dea68,	0x3f6dc539,	0x3f6d9fe4,	0x3f6d7a6c,	0x3f6d54ce,	0x3f6d2f0c,	0x3f6d0925,	0x3f6ce31a,	0x3f6cbcea,	0x3f6c9696,	
	0x3f6c701e,	0x3f6c4980,	0x3f6c22bf,	0x3f6bfbd9,	0x3f6bd4ce,	0x3f6bada0,	0x3f6b864c,	0x3f6b5ed5,	0x3f6b3739,	0x3f6b0f79,	0x3f6ae795,	0x3f6abf8c,	0x3f6a9760,	0x3f6a6f0f,	0x3f6a469a,	0x3f6a1e01,	
	0x3f69f543,	0x3f69cc62,	0x3f69a35d,	0x3f697a33,	0x3f6950e6,	0x3f692774,	0x3f68fddf,	0x3f68d426,	0x3f68aa49,	0x3f688047,	0x3f685623,	0x3f682bda,	0x3f68016d,	0x3f67d6dd,	0x3f67ac29,	0x3f678151,	
	0x3f675655,	0x3f672b36,	0x3f66fff3,	0x3f66d48d,	0x3f66a903,	0x3f667d55,	0x3f665184,	0x3f66258f,	0x3f65f977,	0x3f65cd3b,	0x3f65a0dc,	0x3f65745a,	0x3f6547b4,	0x3f651aeb,	0x3f64edfe,	0x3f64c0ee,	
	0x3f6493bb,	0x3f646665,	0x3f6438eb,	0x3f640b4e,	0x3f63dd8e,	0x3f63afab,	0x3f6381a5,	0x3f63537b,	0x3f63252f,	0x3f62f6c0,	0x3f62c82d,	0x3f629978,	0x3f626aa0,	0x3f623ba4,	0x3f620c86,	0x3f61dd45,	
	0x3f61ade1,	0x3f617e5b,	0x3f614eb1,	0x3f611ee5,	0x3f60eef6,	0x3f60bee5,	0x3f608eb0,	0x3f605e5a,	0x3f602de0,	0x3f5ffd44,	0x3f5fcc86,	0x3f5f9ba5,	0x3f5f6aa1,	0x3f5f397b,	0x3f5f0833,	0x3f5ed6c8,	
	0x3f5ea53a,	0x3f5e738b,	0x3f5e41b9,	0x3f5e0fc5,	0x3f5dddaf,	0x3f5dab76,	0x3f5d791b,	0x3f5d469e,	0x3f5d13ff,	0x3f5ce13e,	0x3f5cae5b,	0x3f5c7b56,	0x3f5c482f,	0x3f5c14e6,	0x3f5be17a,	0x3f5baded,	
	0x3f5b7a3e,	0x3f5b466e,	0x3f5b127b,	0x3f5ade67,	0x3f5aaa31,	0x3f5a75d9,	0x3f5a415f,	0x3f5a0cc4,	0x3f59d807,	0x3f59a329,	0x3f596e29,	0x3f593908,	0x3f5903c5,	0x3f58ce60,	0x3f5898da,	0x3f586333,	
	0x3f582d6a,	0x3f57f780,	0x3f57c175,	0x3f578b48,	0x3f5754fb,	0x3f571e8c,	0x3f56e7fb,	0x3f56b14a,	0x3f567a78,	0x3f564384,	0x3f560c70,	0x3f55d53a,	0x3f559de3,	0x3f55666c,	0x3f552ed4,	0x3f54f71a,	
	0x3f54bf40,	0x3f548745,	0x3f544f2a,	0x3f5416ed,	0x3f53de90,	0x3f53a612,	0x3f536d74,	0x3f5334b5,	0x3f52fbd5,	0x3f52c2d5,	0x3f5289b4,	0x3f525073,	0x3f521711,	0x3f51dd8f,	0x3f51a3ed,	0x3f516a2a,	
	0x3f513047,	0x3f50f644,	0x3f50bc21,	0x3f5081dd,	0x3f504779,	0x3f500cf6,	0x3f4fd252,	0x3f4f978d,	0x3f4f5ca9,	0x3f4f21a5,	0x3f4ee681,	0x3f4eab3e,	0x3f4e6fda,	0x3f4e3456,	0x3f4df8b3,	0x3f4dbcf0,	
	0x3f4d810d,	0x3f4d450a,	0x3f4d08e8,	0x3f4ccca6,	0x3f4c9045,	0x3f4c53c4,	0x3f4c1723,	0x3f4bda63,	0x3f4b9d84,	0x3f4b6085,	0x3f4b2367,	0x3f4ae62a,	0x3f4aa8cd,	0x3f4a6b51,	0x3f4a2db6,	0x3f49effb,	
	0x3f49b222,	0x3f497429,	0x3f493611,	0x3f48f7da,	0x3f48b985,	0x3f487b10,	0x3f483c7c,	0x3f47fdca,	0x3f47bef9,	0x3f478008,	0x3f4740fa,	0x3f4701cc,	0x3f46c280,	0x3f468315,	0x3f46438b,	0x3f4603e3,	
	0x3f45c41c,	0x3f458437,	0x3f454433,	0x3f450411,	0x3f44c3d1,	0x3f448372,	0x3f4442f5,	0x3f44025a,	0x3f43c1a0,	0x3f4380c8,	0x3f433fd2,	0x3f42febe,	0x3f42bd8c,	0x3f427c3c,	0x3f423ace,	0x3f41f942,	
	0x3f41b798,	0x3f4175d0,	0x3f4133ea,	0x3f40f1e7,	0x3f40afc5,	0x3f406d86,	0x3f402b2a,	0x3f3fe8af,	0x3f3fa617,	0x3f3f6362,	0x3f3f208f,	0x3f3edd9f,	0x3f3e9a91,	0x3f3e5766,	0x3f3e141d,	0x3f3dd0b7,	
	0x3f3d8d34,	0x3f3d4993,	0x3f3d05d6,	0x3f3cc1fb,	0x3f3c7e03,	0x3f3c39ee,	0x3f3bf5bc,	0x3f3bb16d,	0x3f3b6d01,	0x3f3b2879,	0x3f3ae3d3,	0x3f3a9f10,	0x3f3a5a31,	0x3f3a1535,	0x3f39d01d,	0x3f398ae7,	
	0x3f394595,	0x3f390027,	0x3f38ba9c,	0x3f3874f4,	0x3f382f30,	0x3f37e950,	0x3f37a353,	0x3f375d3a,	0x3f371704,	0x3f36d0b3,	0x3f368a45,	0x3f3643bb,	0x3f35fd15,	0x3f35b653,	0x3f356f75,	0x3f35287b,	
	0x3f34e165,	0x3f349a33,	0x3f3452e5,	0x3f340b7b,	0x3f33c3f6,	0x3f337c55,	0x3f333498,	0x3f32ecc0,	0x3f32a4cc,	0x3f325cbc,	0x3f321491,	0x3f31cc4b,	0x3f3183e9,	0x3f313b6b,	0x3f30f2d3,	0x3f30aa1f,	
	0x3f306150,	0x3f301865,	0x3f2fcf60,	0x3f2f863f,	0x3f2f3d03,	0x3f2ef3ad,	0x3f2eaa3b,	0x3f2e60ae,	0x3f2e1707,	0x3f2dcd44,	0x3f2d8367,	0x3f2d396f,	0x3f2cef5d,	0x3f2ca52f,	0x3f2c5ae7,	0x3f2c1085,	
	0x3f2bc608,	0x3f2b7b70,	0x3f2b30be,	0x3f2ae5f2,	0x3f2a9b0b,	0x3f2a500a,	0x3f2a04ef,	0x3f29b9b9,	0x3f296e69,	0x3f292300,	0x3f28d77c,	0x3f288bde,	0x3f284026,	0x3f27f454,	0x3f27a868,	0x3f275c62,	
	0x3f271043,	0x3f26c40a,	0x3f2677b7,	0x3f262b4a,	0x3f25dec4,	0x3f259224,	0x3f25456b,	0x3f24f898,	0x3f24abac,	0x3f245ea6,	0x3f241187,	0x3f23c44f,	0x3f2376fd,	0x3f232992,	0x3f22dc0e,	0x3f228e71,	
	0x3f2240bb,	0x3f21f2ec,	0x3f21a504,	0x3f215703,	0x3f2108e9,	0x3f20bab6,	0x3f206c6a,	0x3f201e06,	0x3f1fcf89,	0x3f1f80f3,	0x3f1f3245,	0x3f1ee37e,	0x3f1e949f,	0x3f1e45a7,	0x3f1df697,	0x3f1da76f,	
	0x3f1d582e,	0x3f1d08d5,	0x3f1cb963,	0x3f1c69da,	0x3f1c1a38,	0x3f1bca7f,	0x3f1b7aad,	0x3f1b2ac3,	0x3f1adac2,	0x3f1a8aa8,	0x3f1a3a77,	0x3f19ea2e,	0x3f1999cd,	0x3f194955,	0x3f18f8c4,	0x3f18a81d,	
	0x3f18575d,	0x3f180687,	0x3f17b598,	0x3f176493,	0x3f171376,	0x3f16c241,	0x3f1670f6,	0x3f161f93,	0x3f15ce19,	0x3f157c88,	0x3f152ae0,	0x3f14d921,	0x3f14874b,	0x3f14355e,	0x3f13e35b,	0x3f139140,	
	0x3f133f0f,	0x3f12ecc7,	0x3f129a68,	0x3f1247f3,	0x3f11f567,	0x3f11a2c5,	0x3f11500c,	0x3f10fd3d,	0x3f10aa57,	0x3f10575b,	0x3f100449,	0x3f0fb121,	0x3f0f5de2,	0x3f0f0a8e,	0x3f0eb723,	0x3f0e63a2,	
	0x3f0e100c,	0x3f0dbc5f,	0x3f0d689d,	0x3f0d14c5,	0x3f0cc0d7,	0x3f0c6cd3,	0x3f0c18ba,	0x3f0bc48b,	0x3f0b7046,	0x3f0b1bec,	0x3f0ac77d,	0x3f0a72f8,	0x3f0a1e5e,	0x3f09c9ae,	0x3f0974e9,	0x3f092010,	
	0x3f08cb20,	0x3f08761c,	0x3f082103,	0x3f07cbd5,	0x3f077692,	0x3f07213a,	0x3f06cbcd,	0x3f06764b,	0x3f0620b5,	0x3f05cb0a,	0x3f05754a,	0x3f051f75,	0x3f04c98d,	0x3f04738f,	0x3f041d7e,	0x3f03c757,	
	0x3f03711d,	0x3f031ace,	0x3f02c46b,	0x3f026df4,	0x3f021769,	0x3f01c0ca,	0x3f016a17,	0x3f01134f,	0x3f00bc74,	0x3f006585,	0x3f000e82,	0x3eff6ed8,	0x3efec083,	0x3efe1207,	0x3efd6365,	0x3efcb49b,	
	0x3efc05aa,	0x3efb5692,	0x3efaa753,	0x3ef9f7ee,	0x3ef94862,	0x3ef898b0,	0x3ef7e8d7,	0x3ef738d8,	0x3ef688b3,	0x3ef5d868,	0x3ef527f8,	0x3ef47761,	0x3ef3c6a4,	0x3ef315c2,	0x3ef264bb,	0x3ef1b38e,	
	0x3ef1023c,	0x3ef050c5,	0x3eef9f28,	0x3eeeed67,	0x3eee3b81,	0x3eed8976,	0x3eecd747,	0x3eec24f3,	0x3eeb727a,	0x3eeabfdd,	0x3eea0d1c,	0x3ee95a37,	0x3ee8a72e,	0x3ee7f401,	0x3ee740b1,	0x3ee68d3c,	
	0x3ee5d9a4,	0x3ee525e9,	0x3ee4720a,	0x3ee3be08,	0x3ee309e3,	0x3ee2559b,	0x3ee1a130,	0x3ee0eca3,	0x3ee037f2,	0x3edf831f,	0x3edece2a,	0x3ede1912,	0x3edd63d8,	0x3edcae7c,	0x3edbf8fd,	0x3edb435d,	
	0x3eda8d9b,	0x3ed9d7b7,	0x3ed921b2,	0x3ed86b8b,	0x3ed7b543,	0x3ed6feda,	0x3ed6484f,	0x3ed591a4,	0x3ed4dad7,	0x3ed423ea,	0x3ed36cdb,	0x3ed2b5ad,	0x3ed1fe5e,	0x3ed146ee,	0x3ed08f5e,	0x3ecfd7ae,	
	0x3ecf1fde,	0x3ece67ee,	0x3ecdafde,	0x3eccf7af,	0x3ecc3f60,	0x3ecb86f1,	0x3ecace63,	0x3eca15b5,	0x3ec95ce9,	0x3ec8a3fd,	0x3ec7eaf3,	0x3ec731ca,	0x3ec67882,	0x3ec5bf1b,	0x3ec50596,	0x3ec44bf2,	
	0x3ec39231,	0x3ec2d851,	0x3ec21e53,	0x3ec16437,	0x3ec0a9fd,	0x3ebfefa5,	0x3ebf3530,	0x3ebe7a9e,	0x3ebdbfee,	0x3ebd0521,	0x3ebc4a36,	0x3ebb8f2f,	0x3ebad40b,	0x3eba18ca,	0x3eb95d6c,	0x3eb8a1f1,	
	0x3eb7e65b,	0x3eb72aa7,	0x3eb66ed8,	0x3eb5b2ec,	0x3eb4f6e5,	0x3eb43ac1,	0x3eb37e82,	0x3eb2c227,	0x3eb205b0,	0x3eb1491e,	0x3eb08c71,	0x3eafcfa8,	0x3eaf12c5,	0x3eae55c6,	0x3ead98ac,	0x3eacdb78,	
	0x3eac1e29,	0x3eab60bf,	0x3eaaa33b,	0x3ea9e59d,	0x3ea927e5,	0x3ea86a12,	0x3ea7ac25,	0x3ea6ee1f,	0x3ea62fff,	0x3ea571c5,	0x3ea4b372,	0x3ea3f505,	0x3ea3367f,	0x3ea277e0,	0x3ea1b928,	0x3ea0fa57,	
	0x3ea03b6d,	0x3e9f7c6a,	0x3e9ebd4f,	0x3e9dfe1b,	0x3e9d3ecf,	0x3e9c7f6a,	0x3e9bbfee,	0x3e9b0059,	0x3e9a40ad,	0x3e9980e9,	0x3e98c10d,	0x3e980119,	0x3e97410e,	0x3e9680ec,	0x3e95c0b3,	0x3e950062,	
	0x3e943ffb,	0x3e937f7d,	0x3e92bee7,	0x3e91fe3c,	0x3e913d79,	0x3e907ca1,	0x3e8fbbb2,	0x3e8efaad,	0x3e8e3992,	0x3e8d7861,	0x3e8cb71a,	0x3e8bf5bd,	0x3e8b344b,	0x3e8a72c3,	0x3e89b126,	0x3e88ef74,	
	0x3e882dad,	0x3e876bd0,	0x3e86a9df,	0x3e85e7d9,	0x3e8525bf,	0x3e84638f,	0x3e83a14c,	0x3e82def4,	0x3e821c88,	0x3e815a08,	0x3e809774,	0x3e7fa998,	0x3e7e2420,	0x3e7c9e82,	0x3e7b18bc,	0x3e7992d0,	
	0x3e780cbe,	0x3e768685,	0x3e750026,	0x3e7379a1,	0x3e71f2f7,	0x3e706c28,	0x3e6ee533,	0x3e6d5e1a,	0x3e6bd6dc,	0x3e6a4f79,	0x3e68c7f3,	0x3e674048,	0x3e65b87a,	0x3e643089,	0x3e62a874,	0x3e61203c,	
	0x3e5f97e2,	0x3e5e0f65,	0x3e5c86c6,	0x3e5afe05,	0x3e597522,	0x3e57ec1d,	0x3e5662f8,	0x3e54d9b1,	0x3e535049,	0x3e51c6c1,	0x3e503d19,	0x3e4eb350,	0x3e4d2967,	0x3e4b9f5f,	0x3e4a1538,	0x3e488af1,	
	0x3e47008b,	0x3e457607,	0x3e43eb64,	0x3e4260a3,	0x3e40d5c4,	0x3e3f4ac7,	0x3e3dbfad,	0x3e3c3476,	0x3e3aa921,	0x3e391db0,	0x3e379222,	0x3e360678,	0x3e347ab2,	0x3e32eecf,	0x3e3162d2,	0x3e2fd6b9,	
	0x3e2e4a85,	0x3e2cbe36,	0x3e2b31cc,	0x3e29a548,	0x3e2818aa,	0x3e268bf2,	0x3e24ff20,	0x3e237235,	0x3e21e530,	0x3e205813,	0x3e1ecadd,	0x3e1d3d8e,	0x3e1bb027,	0x3e1a22a8,	0x3e189511,	0x3e170763,	
	0x3e15799e,	0x3e13ebc1,	0x3e125dce,	0x3e10cfc4,	0x3e0f41a4,	0x3e0db36d,	0x3e0c2521,	0x3e0a96bf,	0x3e090848,	0x3e0779bc,	0x3e05eb1b,	0x3e045c65,	0x3e02cd9b,	0x3e013ebc,	0x3dff5f94,	0x3dfc4187,	
	0x3df92354,	0x3df604fb,	0x3df2e67c,	0x3defc7d7,	0x3deca90d,	0x3de98a1f,	0x3de66b0c,	0x3de34bd6,	0x3de02c7d,	0x3ddd0d02,	0x3dd9ed64,	0x3dd6cda5,	0x3dd3adc5,	0x3dd08dc4,	0x3dcd6da3,	0x3dca4d62,	
	0x3dc72d02,	0x3dc40c83,	0x3dc0ebe6,	0x3dbdcb2c,	0x3dbaaa54,	0x3db7895f,	0x3db4684e,	0x3db14721,	0x3dae25d9,	0x3dab0476,	0x3da7e2f8,	0x3da4c161,	0x3da19fb0,	0x3d9e7de7,	0x3d9b5c05,	0x3d983a0a,	
	0x3d9517f9,	0x3d91f5d0,	0x3d8ed391,	0x3d8bb13c,	0x3d888ed2,	0x3d856c52,	0x3d8249be,	0x3d7e4e2b,	0x3d7808b3,	0x3d71c315,	0x3d6b7d51,	0x3d65376a,	0x3d5ef15f,	0x3d58ab31,	0x3d5264e2,	0x3d4c1e73,	
	0x3d45d7e4,	0x3d3f9137,	0x3d394a6c,	0x3d330385,	0x3d2cbc82,	0x3d267564,	0x3d202e2d,	0x3d19e6dd,	0x3d139f75,	0x3d0d57f6,	0x3d071062,	0x3d00c8b9,	0x3cf501f8,	0x3ce87258,	0x3cdbe294,	0x3ccf52af,	
	0x3cc2c2a9,	0x3cb63286,	0x3ca9a246,	0x3c9d11ec,	0x3c90817a,	0x3c83f0f2,	0x3c6ec0aa,	0x3c559f4c,	0x3c3c7dcc,	0x3c235c30,	0x3c0a3a7b,	0x3be23160,	0x3bafeda8,	0x3b7b53a9,	0x3b16cbdb,	0x3a490fd9
};

const unsigned int Audio::AAC::SIN_128[256] = {
	0x3bc90f88,	0x3c96c9b6,	0x3cfb49ba,	0x3d2fe007,	0x3d621469,	0x3d8a200a,	0x3da3308c,	0x3dbc3ac3,	0x3dd53db9,	0x3dee3876,	0x3e039502,	0x3e1008b7,	0x3e1c76de,	0x3e28defc,	0x3e354098,	0x3e419b37,	
	0x3e4dee60,	0x3e5a3997,	0x3e667c66,	0x3e72b651,	0x3e7ee6e1,	0x3e8586ce,	0x3e8b9507,	0x3e919ddd,	0x3e97a117,	0x3e9d9e78,	0x3ea395c5,	0x3ea986c4,	0x3eaf713a,	0x3eb554ec,	0x3ebb31a0,	0x3ec1071e,	
	0x3ec6d529,	0x3ecc9b8b,	0x3ed25a09,	0x3ed8106b,	0x3eddbe79,	0x3ee363fa,	0x3ee900b7,	0x3eee9479,	0x3ef41f07,	0x3ef9a02d,	0x3eff17b2,	0x3f0242b1,	0x3f04f484,	0x3f07a136,	0x3f0a48ad,	0x3f0cead0,	
	0x3f0f8784,	0x3f121eb0,	0x3f14b039,	0x3f173c07,	0x3f19c200,	0x3f1c420c,	0x3f1ebc12,	0x3f212ff9,	0x3f239da9,	0x3f26050a,	0x3f286605,	0x3f2ac082,	0x3f2d1469,	0x3f2f61a5,	0x3f31a81d,	0x3f33e7bc,	
	0x3f36206c,	0x3f385216,	0x3f3a7ca4,	0x3f3ca003,	0x3f3ebc1b,	0x3f40d0da,	0x3f42de29,	0x3f44e3f5,	0x3f46e22a,	0x3f48d8b3,	0x3f4ac77f,	0x3f4cae79,	0x3f4e8d90,	0x3f5064af,	0x3f5233c6,	0x3f53fac3,	
	0x3f55b993,	0x3f577026,	0x3f591e6a,	0x3f5ac450,	0x3f5c61c7,	0x3f5df6be,	0x3f5f8327,	0x3f6106f2,	0x3f628210,	0x3f63f473,	0x3f655e0b,	0x3f66becc,	0x3f6816a8,	0x3f696591,	0x3f6aab7b,	0x3f6be858,	
	0x3f6d1c1d,	0x3f6e46be,	0x3f6f6830,	0x3f708066,	0x3f718f57,	0x3f7294f8,	0x3f73913f,	0x3f748422,	0x3f756d97,	0x3f764d97,	0x3f772417,	0x3f77f110,	0x3f78b47b,	0x3f796e4e,	0x3f7a1e84,	0x3f7ac516,	
	0x3f7b61fc,	0x3f7bf531,	0x3f7c7eb0,	0x3f7cfe73,	0x3f7d7474,	0x3f7de0b1,	0x3f7e4323,	0x3f7e9bc9,	0x3f7eea9d,	0x3f7f2f9d,	0x3f7f6ac7,	0x3f7f9c18,	0x3f7fc38f,	0x3f7fe129,	0x3f7ff4e6,	0x3f7ffec4,	
	0x3f7ffec4,	0x3f7ff4e6,	0x3f7fe129,	0x3f7fc38f,	0x3f7f9c18,	0x3f7f6ac7,	0x3f7f2f9d,	0x3f7eea9d,	0x3f7e9bc9,	0x3f7e4323,	0x3f7de0b1,	0x3f7d7474,	0x3f7cfe73,	0x3f7c7eb0,	0x3f7bf531,	0x3f7b61fc,	
	0x3f7ac516,	0x3f7a1e84,	0x3f796e4e,	0x3f78b47b,	0x3f77f110,	0x3f772417,	0x3f764d97,	0x3f756d97,	0x3f748422,	0x3f73913f,	0x3f7294f8,	0x3f718f57,	0x3f708066,	0x3f6f6830,	0x3f6e46be,	0x3f6d1c1d,	
	0x3f6be858,	0x3f6aab7b,	0x3f696591,	0x3f6816a8,	0x3f66becc,	0x3f655e0b,	0x3f63f473,	0x3f628210,	0x3f6106f2,	0x3f5f8327,	0x3f5df6be,	0x3f5c61c7,	0x3f5ac450,	0x3f591e6a,	0x3f577026,	0x3f55b993,	
	0x3f53fac3,	0x3f5233c6,	0x3f5064af,	0x3f4e8d90,	0x3f4cae79,	0x3f4ac77f,	0x3f48d8b3,	0x3f46e22a,	0x3f44e3f5,	0x3f42de29,	0x3f40d0da,	0x3f3ebc1b,	0x3f3ca003,	0x3f3a7ca4,	0x3f385216,	0x3f36206c,	
	0x3f33e7bc,	0x3f31a81d,	0x3f2f61a5,	0x3f2d1469,	0x3f2ac082,	0x3f286605,	0x3f26050a,	0x3f239da9,	0x3f212ff9,	0x3f1ebc12,	0x3f1c420c,	0x3f19c200,	0x3f173c07,	0x3f14b039,	0x3f121eb0,	0x3f0f8784,	
	0x3f0cead0,	0x3f0a48ad,	0x3f07a136,	0x3f04f484,	0x3f0242b1,	0x3eff17b2,	0x3ef9a02d,	0x3ef41f07,	0x3eee9479,	0x3ee900b7,	0x3ee363fa,	0x3eddbe79,	0x3ed8106b,	0x3ed25a09,	0x3ecc9b8b,	0x3ec6d529,	
	0x3ec1071e,	0x3ebb31a0,	0x3eb554ec,	0x3eaf713a,	0x3ea986c4,	0x3ea395c5,	0x3e9d9e78,	0x3e97a117,	0x3e919ddd,	0x3e8b9507,	0x3e8586ce,	0x3e7ee6e1,	0x3e72b651,	0x3e667c66,	0x3e5a3997,	0x3e4dee60,	
	0x3e419b37,	0x3e354098,	0x3e28defc,	0x3e1c76de,	0x3e1008b7,	0x3e039502,	0x3dee3876,	0x3dd53db9,	0x3dbc3ac3,	0x3da3308c,	0x3d8a200a,	0x3d621469,	0x3d2fe007,	0x3cfb49ba,	0x3c96c9b6,	0x3bc90f88

};

const unsigned int Audio::AAC::KBD_1024[2048] = {

	0x399962f2,	0x39e16fb3,	0x3a0f5326,	0x3a2ba86e,	0x3a46e3a0,	0x3a619970,	0x3a7c1fa8,	0x3a8b5668,	0x3a98b309,	0x3aa6330f,	0x3ab3e07b,	0x3ac1c30f,	0x3acfe100,	0x3ade3f64,	0x3aece283,	0x3afbce05,	
	0x3b05828f,	0x3b0d4550,	0x3b15308d,	0x3b1d456e,	0x3b258506,	0x3b2df053,	0x3b368843,	0x3b3f4dba,	0x3b48418f,	0x3b516491,	0x3b5ab788,	0x3b643b33,	0x3b6df04f,	0x3b77d793,	0x3b80f8d9,	0x3b861fae,	
	0x3b8b609e,	0x3b90bbff,	0x3b963224,	0x3b9bc362,	0x3ba17009,	0x3ba7386c,	0x3bad1cdc,	0x3bb31da8,	0x3bb93b21,	0x3bbf7596,	0x3bc5cd57,	0x3bcc42b1,	0x3bd2d5f3,	0x3bd9876c,	0x3be05769,	0x3be74638,	
	0x3bee5426,	0x3bf58182,	0x3bfcce97,	0x3c021dd9,	0x3c05e491,	0x3c09bb9a,	0x3c0da319,	0x3c119b35,	0x3c15a414,	0x3c19bddc,	0x3c1de8b4,	0x3c2224c1,	0x3c26722a,	0x3c2ad114,	0x3c2f41a7,	0x3c33c406,	
	0x3c385859,	0x3c3cfec5,	0x3c41b770,	0x3c468280,	0x3c4b601b,	0x3c505065,	0x3c555385,	0x3c5a69a0,	0x3c5f92dc,	0x3c64cf5f,	0x3c6a1f4d,	0x3c6f82cb,	0x3c74fa00,	0x3c7a8511,	0x3c801211,	0x3c82ebac,	
	0x3c85cf6d,	0x3c88bd66,	0x3c8bb5a8,	0x3c8eb848,	0x3c91c556,	0x3c94dce6,	0x3c97ff09,	0x3c9b2bd2,	0x3c9e6354,	0x3ca1a59f,	0x3ca4f2c7,	0x3ca84add,	0x3cabadf3,	0x3caf1c1b,	0x3cb29568,	0x3cb619ea,	
	0x3cb9a9b5,	0x3cbd44d8,	0x3cc0eb67,	0x3cc49d72,	0x3cc85b0c,	0x3ccc2445,	0x3ccff930,	0x3cd3d9dd,	0x3cd7c65d,	0x3cdbbec3,	0x3cdfc31e,	0x3ce3d380,	0x3ce7effb,	0x3cec189e,	0x3cf04d7b,	0x3cf48ea3,	
	0x3cf8dc25,	0x3cfd3613,	0x3d00ce3f,	0x3d0307ba,	0x3d054784,	0x3d078da5,	0x3d09da24,	0x3d0c2d09,	0x3d0e865d,	0x3d10e628,	0x3d134c71,	0x3d15b940,	0x3d182c9d,	0x3d1aa690,	0x3d1d2720,	0x3d1fae55,	
	0x3d223c37,	0x3d24d0cc,	0x3d276c1d,	0x3d2a0e31,	0x3d2cb70f,	0x3d2f66bf,	0x3d321d47,	0x3d34daae,	0x3d379efd,	0x3d3a6a3a,	0x3d3d3c6c,	0x3d401599,	0x3d42f5c9,	0x3d45dd03,	0x3d48cb4c,	0x3d4bc0ad,	
	0x3d4ebd2b,	0x3d51c0cc,	0x3d54cb98,	0x3d57dd95,	0x3d5af6c9,	0x3d5e173a,	0x3d613eef,	0x3d646ded,	0x3d67a43a,	0x3d6ae1dd,	0x3d6e26dc,	0x3d71733b,	0x3d74c702,	0x3d782235,	0x3d7b84da,	0x3d7eeef6,	
	0x3d813048,	0x3d82ecd6,	0x3d84ad27,	0x3d86713f,	0x3d883920,	0x3d8a04cd,	0x3d8bd447,	0x3d8da791,	0x3d8f7eae,	0x3d9159a1,	0x3d93386a,	0x3d951b0d,	0x3d97018b,	0x3d98ebe8,	0x3d9ada24,	0x3d9ccc43,	
	0x3d9ec246,	0x3da0bc2e,	0x3da2b9ff,	0x3da4bbba,	0x3da6c160,	0x3da8caf4,	0x3daad877,	0x3dace9ec,	0x3daeff52,	0x3db118ad,	0x3db335fe,	0x3db55747,	0x3db77c88,	0x3db9a5c3,	0x3dbbd2f9,	0x3dbe042d,	
	0x3dc0395f,	0x3dc27290,	0x3dc4afc1,	0x3dc6f0f5,	0x3dc9362b,	0x3dcb7f64,	0x3dcdcca2,	0x3dd01de6,	0x3dd27330,	0x3dd4cc82,	0x3dd729db,	0x3dd98b3e,	0x3ddbf0a9,	0x3dde5a1f,	0x3de0c79e,	0x3de33929,	
	0x3de5aec0,	0x3de82862,	0x3deaa610,	0x3ded27ca,	0x3defad91,	0x3df23765,	0x3df4c546,	0x3df75734,	0x3df9ed2e,	0x3dfc8736,	0x3dff254a,	0x3e00e3b5,	0x3e0236cc,	0x3e038be9,	0x3e04e30b,	0x3e063c33,	
	0x3e079761,	0x3e08f494,	0x3e0a53cc,	0x3e0bb508,	0x3e0d1849,	0x3e0e7d8d,	0x3e0fe4d5,	0x3e114e20,	0x3e12b96e,	0x3e1426be,	0x3e15960f,	0x3e170762,	0x3e187ab5,	0x3e19f008,	0x3e1b675b,	0x3e1ce0ac,	
	0x3e1e5bfb,	0x3e1fd947,	0x3e215890,	0x3e22d9d5,	0x3e245d14,	0x3e25e24e,	0x3e276980,	0x3e28f2ac,	0x3e2a7dce,	0x3e2c0ae7,	0x3e2d99f6,	0x3e2f2af9,	0x3e30bdf0,	0x3e3252d9,	0x3e33e9b3,	0x3e35827e,	
	0x3e371d37,	0x3e38b9de,	0x3e3a5872,	0x3e3bf8f2,	0x3e3d9b5b,	0x3e3f3fae,	0x3e40e5e7,	0x3e428e07,	0x3e44380c,	0x3e45e3f4,	0x3e4791be,	0x3e494168,	0x3e4af2f1,	0x3e4ca657,	0x3e4e5b9a,	0x3e5012b6,	
	0x3e51cbab,	0x3e538677,	0x3e554318,	0x3e57018d,	0x3e58c1d3,	0x3e5a83ea,	0x3e5c47ce,	0x3e5e0d7f,	0x3e5fd4fb,	0x3e619e3f,	0x3e63694a,	0x3e65361a,	0x3e6704ac,	0x3e68d500,	0x3e6aa712,	0x3e6c7ae1,	
	0x3e6e506b,	0x3e7027ad,	0x3e7200a6,	0x3e73db53,	0x3e75b7b2,	0x3e7795c0,	0x3e79757d,	0x3e7b56e5,	0x3e7d39f6,	0x3e7f1ead,	0x3e808284,	0x3e817683,	0x3e826b52,	0x3e8360ef,	0x3e84575a,	0x3e854e90,	
	0x3e864692,	0x3e873f5d,	0x3e8838f0,	0x3e89334a,	0x3e8a2e6a,	0x3e8b2a4e,	0x3e8c26f5,	0x3e8d245e,	0x3e8e2287,	0x3e8f216f,	0x3e902114,	0x3e912176,	0x3e922292,	0x3e932468,	0x3e9426f5,	0x3e952a39,	
	0x3e962e31,	0x3e9732dd,	0x3e98383b,	0x3e993e4a,	0x3e9a4507,	0x3e9b4c72,	0x3e9c5489,	0x3e9d5d4a,	0x3e9e66b4,	0x3e9f70c5,	0x3ea07b7b,	0x3ea186d6,	0x3ea292d4,	0x3ea39f72,	0x3ea4acaf,	0x3ea5ba8a,	
	0x3ea6c901,	0x3ea7d812,	0x3ea8e7bb,	0x3ea9f7fc,	0x3eab08d2,	0x3eac1a3b,	0x3ead2c37,	0x3eae3ec2,	0x3eaf51dc,	0x3eb06583,	0x3eb179b4,	0x3eb28e6f,	0x3eb3a3b1,	0x3eb4b978,	0x3eb5cfc4,	0x3eb6e691,	
	0x3eb7fddf,	0x3eb915ab,	0x3eba2df4,	0x3ebb46b7,	0x3ebc5ff4,	0x3ebd79a8,	0x3ebe93d0,	0x3ebfae6d,	0x3ec0c97a,	0x3ec1e4f8,	0x3ec300e3,	0x3ec41d3a,	0x3ec539fb,	0x3ec65724,	0x3ec774b3,	0x3ec892a6,	
	0x3ec9b0fb,	0x3ecacfb1,	0x3ecbeec5,	0x3ecd0e36,	0x3ece2e01,	0x3ecf4e25,	0x3ed06e9f,	0x3ed18f6d,	0x3ed2b08f,	0x3ed3d200,	0x3ed4f3c1,	0x3ed615ce,	0x3ed73825,	0x3ed85ac5,	0x3ed97dac,	0x3edaa0d7,	
	0x3edbc444,	0x3edce7f2,	0x3ede0bdf,	0x3edf3008,	0x3ee0546b,	0x3ee17906,	0x3ee29dd8,	0x3ee3c2dd,	0x3ee4e815,	0x3ee60d7d,	0x3ee73312,	0x3ee858d4,	0x3ee97ebf,	0x3eeaa4d2,	0x3eebcb0b,	0x3eecf167,	
	0x3eee17e5,	0x3eef3e82,	0x3ef0653c,	0x3ef18c12,	0x3ef2b300,	0x3ef3da05,	0x3ef50120,	0x3ef6284c,	0x3ef74f8a,	0x3ef876d6,	0x3ef99e2e,	0x3efac591,	0x3efbecfc,	0x3efd146d,	0x3efe3be2,	0x3eff6359,	
	0x3f004567,	0x3f00d921,	0x3f016cd9,	0x3f02008e,	0x3f02943e,	0x3f0327e8,	0x3f03bb8c,	0x3f044f29,	0x3f04e2bd,	0x3f057647,	0x3f0609c6,	0x3f069d3a,	0x3f0730a1,	0x3f07c3fa,	0x3f085744,	0x3f08ea7e,	
	0x3f097da6,	0x3f0a10bd,	0x3f0aa3c1,	0x3f0b36b0,	0x3f0bc98a,	0x3f0c5c4e,	0x3f0ceefb,	0x3f0d818f,	0x3f0e1409,	0x3f0ea669,	0x3f0f38ae,	0x3f0fcad6,	0x3f105ce1,	0x3f10eecc,	0x3f118098,	0x3f121244,	
	0x3f12a3ce,	0x3f133535,	0x3f13c678,	0x3f145796,	0x3f14e88f,	0x3f157961,	0x3f160a0b,	0x3f169a8c,	0x3f172ae3,	0x3f17bb0f,	0x3f184b10,	0x3f18dae3,	0x3f196a89,	0x3f19fa00,	0x3f1a8947,	0x3f1b185d,	
	0x3f1ba741,	0x3f1c35f3,	0x3f1cc470,	0x3f1d52b9,	0x3f1de0cc,	0x3f1e6ea9,	0x3f1efc4e,	0x3f1f89ba,	0x3f2016ed,	0x3f20a3e5,	0x3f2130a1,	0x3f21bd21,	0x3f224964,	0x3f22d569,	0x3f23612e,	0x3f23ecb3,	
	0x3f2477f7,	0x3f2502f9,	0x3f258db8,	0x3f261833,	0x3f26a26a,	0x3f272c5a,	0x3f27b605,	0x3f283f67,	0x3f28c882,	0x3f295153,	0x3f29d9da,	0x3f2a6217,	0x3f2aea07,	0x3f2b71ab,	0x3f2bf901,	0x3f2c8009,	
	0x3f2d06c1,	0x3f2d8d2a,	0x3f2e1341,	0x3f2e9907,	0x3f2f1e7a,	0x3f2fa39a,	0x3f302865,	0x3f30acdc,	0x3f3130fd,	0x3f31b4c7,	0x3f32383a,	0x3f32bb54,	0x3f333e16,	0x3f33c07d,	0x3f34428b,	0x3f34c43d,	
	0x3f354593,	0x3f35c68c,	0x3f364727,	0x3f36c765,	0x3f374743,	0x3f37c6c2,	0x3f3845e0,	0x3f38c49d,	0x3f3942f9,	0x3f39c0f1,	0x3f3a3e87,	0x3f3abbb8,	0x3f3b3885,	0x3f3bb4ed,	0x3f3c30ef,	0x3f3cac8a,	
	0x3f3d27be,	0x3f3da28a,	0x3f3e1ced,	0x3f3e96e8,	0x3f3f1078,	0x3f3f899e,	0x3f40025a,	0x3f407aa9,	0x3f40f28d,	0x3f416a03,	0x3f41e10c,	0x3f4257a8,	0x3f42cdd4,	0x3f434392,	0x3f43b8e0,	0x3f442dbe,	
	0x3f44a22b,	0x3f451627,	0x3f4589b2,	0x3f45fcca,	0x3f466f6f,	0x3f46e1a1,	0x3f47535f,	0x3f47c4a9,	0x3f48357f,	0x3f48a5df,	0x3f4915c9,	0x3f49853d,	0x3f49f43b,	0x3f4a62c2,	0x3f4ad0d2,	0x3f4b3e69,	
	0x3f4bab88,	0x3f4c182f,	0x3f4c845d,	0x3f4cf011,	0x3f4d5b4b,	0x3f4dc60b,	0x3f4e3051,	0x3f4e9a1c,	0x3f4f036b,	0x3f4f6c3f,	0x3f4fd497,	0x3f503c72,	0x3f50a3d1,	0x3f510ab3,	0x3f517118,	0x3f51d6ff,	
	0x3f523c68,	0x3f52a154,	0x3f5305c0,	0x3f5369af,	0x3f53cd1e,	0x3f54300e,	0x3f54927f,	0x3f54f471,	0x3f5555e2,	0x3f55b6d4,	0x3f561745,	0x3f567736,	0x3f56d6a6,	0x3f573595,	0x3f579403,	0x3f57f1f0,	
	0x3f584f5c,	0x3f58ac46,	0x3f5908af,	0x3f596496,	0x3f59bffb,	0x3f5a1ade,	0x3f5a753e,	0x3f5acf1d,	0x3f5b2879,	0x3f5b8153,	0x3f5bd9aa,	0x3f5c317f,	0x3f5c88d1,	0x3f5cdfa0,	0x3f5d35ed,	0x3f5d8bb7,	
	0x3f5de0fe,	0x3f5e35c2,	0x3f5e8a03,	0x3f5eddc1,	0x3f5f30fd,	0x3f5f83b6,	0x3f5fd5eb,	0x3f60279e,	0x3f6078ce,	0x3f60c97b,	0x3f6119a6,	0x3f61694e,	0x3f61b873,	0x3f620715,	0x3f625535,	0x3f62a2d3,	
	0x3f62efee,	0x3f633c87,	0x3f63889e,	0x3f63d433,	0x3f641f46,	0x3f6469d7,	0x3f64b3e6,	0x3f64fd74,	0x3f654681,	0x3f658f0c,	0x3f65d716,	0x3f661ea0,	0x3f6665a8,	0x3f66ac30,	0x3f66f238,	0x3f6737bf,	
	0x3f677cc7,	0x3f67c14e,	0x3f680556,	0x3f6848df,	0x3f688be9,	0x3f68ce74,	0x3f691080,	0x3f69520e,	0x3f69931d,	0x3f69d3af,	0x3f6a13c3,	0x3f6a535a,	0x3f6a9274,	0x3f6ad111,	0x3f6b0f31,	0x3f6b4cd5,	
	0x3f6b89fe,	0x3f6bc6ab,	0x3f6c02dc,	0x3f6c3e93,	0x3f6c79cf,	0x3f6cb490,	0x3f6ceed8,	0x3f6d28a6,	0x3f6d61fb,	0x3f6d9ad7,	0x3f6dd33b,	0x3f6e0b26,	0x3f6e429a,	0x3f6e7996,	0x3f6eb01b,	0x3f6ee629,	
	0x3f6f1bc2,	0x3f6f50e4,	0x3f6f8591,	0x3f6fb9c9,	0x3f6fed8c,	0x3f7020db,	0x3f7053b6,	0x3f70861e,	0x3f70b813,	0x3f70e996,	0x3f711aa6,	0x3f714b45,	0x3f717b73,	0x3f71ab30,	0x3f71da7c,	0x3f720959,	
	0x3f7237c7,	0x3f7265c6,	0x3f729357,	0x3f72c079,	0x3f72ed2f,	0x3f731977,	0x3f734553,	0x3f7370c3,	0x3f739bc8,	0x3f73c662,	0x3f73f091,	0x3f741a57,	0x3f7443b3,	0x3f746ca6,	0x3f749531,	0x3f74bd55,	
	0x3f74e511,	0x3f750c66,	0x3f753355,	0x3f7559de,	0x3f758003,	0x3f75a5c2,	0x3f75cb1e,	0x3f75f016,	0x3f7614ab,	0x3f7638de,	0x3f765caf,	0x3f76801f,	0x3f76a32e,	0x3f76c5dd,	0x3f76e82c,	0x3f770a1c,	
	0x3f772bae,	0x3f774ce2,	0x3f776db9,	0x3f778e33,	0x3f77ae51,	0x3f77ce13,	0x3f77ed7a,	0x3f780c87,	0x3f782b3a,	0x3f784994,	0x3f786795,	0x3f78853e,	0x3f78a290,	0x3f78bf8b,	0x3f78dc2f,	0x3f78f87e,	
	0x3f791478,	0x3f79301d,	0x3f794b6f,	0x3f79666d,	0x3f798118,	0x3f799b72,	0x3f79b57a,	0x3f79cf31,	0x3f79e897,	0x3f7a01ae,	0x3f7a1a76,	0x3f7a32ef,	0x3f7a4b1b,	0x3f7a62f9,	0x3f7a7a8a,	0x3f7a91d0,	
	0x3f7aa8ca,	0x3f7abf79,	0x3f7ad5de,	0x3f7aebf9,	0x3f7b01cb,	0x3f7b1754,	0x3f7b2c96,	0x3f7b4190,	0x3f7b5644,	0x3f7b6ab2,	0x3f7b7eda,	0x3f7b92be,	0x3f7ba65d,	0x3f7bb9b8,	0x3f7bccd0,	0x3f7bdfa6,	
	0x3f7bf23a,	0x3f7c048d,	0x3f7c169f,	0x3f7c2871,	0x3f7c3a03,	0x3f7c4b57,	0x3f7c5c6c,	0x3f7c6d43,	0x3f7c7ddd,	0x3f7c8e3b,	0x3f7c9e5c,	0x3f7cae43,	0x3f7cbdee,	0x3f7ccd5f,	0x3f7cdc96,	0x3f7ceb95,	
	0x3f7cfa5a,	0x3f7d08e8,	0x3f7d173e,	0x3f7d255e,	0x3f7d3347,	0x3f7d40fb,	0x3f7d4e79,	0x3f7d5bc3,	0x3f7d68d8,	0x3f7d75bb,	0x3f7d826a,	0x3f7d8ee7,	0x3f7d9b32,	0x3f7da74c,	0x3f7db335,	0x3f7dbeee,	
	0x3f7dca77,	0x3f7dd5d1,	0x3f7de0fc,	0x3f7debfa,	0x3f7df6c9,	0x3f7e016c,	0x3f7e0be3,	0x3f7e162d,	0x3f7e204c,	0x3f7e2a40,	0x3f7e3409,	0x3f7e3da9,	0x3f7e471f,	0x3f7e506c,	0x3f7e5991,	0x3f7e628e,	
	0x3f7e6b63,	0x3f7e7412,	0x3f7e7c9a,	0x3f7e84fc,	0x3f7e8d39,	0x3f7e9550,	0x3f7e9d44,	0x3f7ea513,	0x3f7eacbe,	0x3f7eb446,	0x3f7ebbac,	0x3f7ec2ef,	0x3f7eca11,	0x3f7ed112,	0x3f7ed7f1,	0x3f7edeb0,	
	0x3f7ee550,	0x3f7eebcf,	0x3f7ef230,	0x3f7ef872,	0x3f7efe96,	0x3f7f049b,	0x3f7f0a84,	0x3f7f1050,	0x3f7f15ff,	0x3f7f1b92,	0x3f7f2109,	0x3f7f2665,	0x3f7f2ba6,	0x3f7f30cc,	0x3f7f35d9,	0x3f7f3acb,	
	0x3f7f3fa5,	0x3f7f4465,	0x3f7f490d,	0x3f7f4d9c,	0x3f7f5214,	0x3f7f5674,	0x3f7f5abd,	0x3f7f5ef0,	0x3f7f630c,	0x3f7f6712,	0x3f7f6b02,	0x3f7f6edd,	0x3f7f72a3,	0x3f7f7655,	0x3f7f79f2,	0x3f7f7d7b,	
	0x3f7f80f1,	0x3f7f8453,	0x3f7f87a3,	0x3f7f8adf,	0x3f7f8e0a,	0x3f7f9122,	0x3f7f9428,	0x3f7f971e,	0x3f7f9a02,	0x3f7f9cd5,	0x3f7f9f98,	0x3f7fa24a,	0x3f7fa4ed,	0x3f7fa780,	0x3f7faa03,	0x3f7fac78,	
	0x3f7faede,	0x3f7fb135,	0x3f7fb37e,	0x3f7fb5b9,	0x3f7fb7e6,	0x3f7fba05,	0x3f7fbc18,	0x3f7fbe1d,	0x3f7fc016,	0x3f7fc202,	0x3f7fc3e2,	0x3f7fc5b6,	0x3f7fc77e,	0x3f7fc93b,	0x3f7fcaec,	0x3f7fcc93,	
	0x3f7fce2e,	0x3f7fcfbf,	0x3f7fd145,	0x3f7fd2c1,	0x3f7fd434,	0x3f7fd59c,	0x3f7fd6fb,	0x3f7fd850,	0x3f7fd99c,	0x3f7fdae0,	0x3f7fdc1a,	0x3f7fdd4c,	0x3f7fde75,	0x3f7fdf97,	0x3f7fe0b0,	0x3f7fe1c1,	
	0x3f7fe2ca,	0x3f7fe3cc,	0x3f7fe4c7,	0x3f7fe5ba,	0x3f7fe6a7,	0x3f7fe78c,	0x3f7fe86b,	0x3f7fe943,	0x3f7fea15,	0x3f7feae1,	0x3f7feba6,	0x3f7fec65,	0x3f7fed1f,	0x3f7fedd3,	0x3f7fee82,	0x3f7fef2b,	
	0x3f7fefce,	0x3f7ff06d,	0x3f7ff107,	0x3f7ff19b,	0x3f7ff22b,	0x3f7ff2b7,	0x3f7ff33d,	0x3f7ff3c0,	0x3f7ff43e,	0x3f7ff4b8,	0x3f7ff52e,	0x3f7ff5a0,	0x3f7ff60e,	0x3f7ff678,	0x3f7ff6df,	0x3f7ff742,	
	0x3f7ff7a1,	0x3f7ff7fe,	0x3f7ff857,	0x3f7ff8ac,	0x3f7ff8ff,	0x3f7ff94f,	0x3f7ff99c,	0x3f7ff9e6,	0x3f7ffa2d,	0x3f7ffa72,	0x3f7ffab4,	0x3f7ffaf3,	0x3f7ffb31,	0x3f7ffb6b,	0x3f7ffba4,	0x3f7ffbda,	
	0x3f7ffc0e,	0x3f7ffc40,	0x3f7ffc70,	0x3f7ffc9e,	0x3f7ffcca,	0x3f7ffcf5,	0x3f7ffd1d,	0x3f7ffd44,	0x3f7ffd69,	0x3f7ffd8d,	0x3f7ffdaf,	0x3f7ffdd0,	0x3f7ffdef,	0x3f7ffe0d,	0x3f7ffe29,	0x3f7ffe44,	
	0x3f7ffe5e,	0x3f7ffe77,	0x3f7ffe8e,	0x3f7ffea5,	0x3f7ffeba,	0x3f7ffece,	0x3f7ffee2,	0x3f7ffef4,	0x3f7fff05,	0x3f7fff16,	0x3f7fff26,	0x3f7fff34,	0x3f7fff42,	0x3f7fff50,	0x3f7fff5c,	0x3f7fff68,	
	0x3f7fff73,	0x3f7fff7e,	0x3f7fff88,	0x3f7fff91,	0x3f7fff9a,	0x3f7fffa3,	0x3f7fffaa,	0x3f7fffb2,	0x3f7fffb9,	0x3f7fffbf,	0x3f7fffc5,	0x3f7fffca,	0x3f7fffd0,	0x3f7fffd5,	0x3f7fffd9,	0x3f7fffdd,	
	0x3f7fffe1,	0x3f7fffe5,	0x3f7fffe8,	0x3f7fffeb,	0x3f7fffee,	0x3f7ffff0,	0x3f7ffff3,	0x3f7ffff5,	0x3f7ffff7,	0x3f7ffff8,	0x3f7ffffa,	0x3f7ffffb,	0x3f7ffffc,	0x3f7ffffd,	0x3f7ffffe,	0x3f7fffff,	
	0x3f7fffff,	0x3f7ffffe,	0x3f7ffffd,	0x3f7ffffc,	0x3f7ffffb,	0x3f7ffffa,	0x3f7ffff8,	0x3f7ffff7,	0x3f7ffff5,	0x3f7ffff3,	0x3f7ffff0,	0x3f7fffee,	0x3f7fffeb,	0x3f7fffe8,	0x3f7fffe5,	0x3f7fffe1,	
	0x3f7fffdd,	0x3f7fffd9,	0x3f7fffd5,	0x3f7fffd0,	0x3f7fffca,	0x3f7fffc5,	0x3f7fffbf,	0x3f7fffb9,	0x3f7fffb2,	0x3f7fffaa,	0x3f7fffa3,	0x3f7fff9a,	0x3f7fff91,	0x3f7fff88,	0x3f7fff7e,	0x3f7fff73,	
	0x3f7fff68,	0x3f7fff5c,	0x3f7fff50,	0x3f7fff42,	0x3f7fff34,	0x3f7fff26,	0x3f7fff16,	0x3f7fff05,	0x3f7ffef4,	0x3f7ffee2,	0x3f7ffece,	0x3f7ffeba,	0x3f7ffea5,	0x3f7ffe8e,	0x3f7ffe77,	0x3f7ffe5e,	
	0x3f7ffe44,	0x3f7ffe29,	0x3f7ffe0d,	0x3f7ffdef,	0x3f7ffdd0,	0x3f7ffdaf,	0x3f7ffd8d,	0x3f7ffd69,	0x3f7ffd44,	0x3f7ffd1d,	0x3f7ffcf5,	0x3f7ffcca,	0x3f7ffc9e,	0x3f7ffc70,	0x3f7ffc40,	0x3f7ffc0e,	
	0x3f7ffbda,	0x3f7ffba4,	0x3f7ffb6b,	0x3f7ffb31,	0x3f7ffaf3,	0x3f7ffab4,	0x3f7ffa72,	0x3f7ffa2d,	0x3f7ff9e6,	0x3f7ff99c,	0x3f7ff94f,	0x3f7ff8ff,	0x3f7ff8ac,	0x3f7ff857,	0x3f7ff7fe,	0x3f7ff7a1,	
	0x3f7ff742,	0x3f7ff6df,	0x3f7ff678,	0x3f7ff60e,	0x3f7ff5a0,	0x3f7ff52e,	0x3f7ff4b8,	0x3f7ff43e,	0x3f7ff3c0,	0x3f7ff33d,	0x3f7ff2b7,	0x3f7ff22b,	0x3f7ff19b,	0x3f7ff107,	0x3f7ff06d,	0x3f7fefce,	
	0x3f7fef2b,	0x3f7fee82,	0x3f7fedd3,	0x3f7fed1f,	0x3f7fec65,	0x3f7feba6,	0x3f7feae1,	0x3f7fea15,	0x3f7fe943,	0x3f7fe86b,	0x3f7fe78c,	0x3f7fe6a7,	0x3f7fe5ba,	0x3f7fe4c7,	0x3f7fe3cc,	0x3f7fe2ca,	
	0x3f7fe1c1,	0x3f7fe0b0,	0x3f7fdf97,	0x3f7fde75,	0x3f7fdd4c,	0x3f7fdc1a,	0x3f7fdae0,	0x3f7fd99c,	0x3f7fd850,	0x3f7fd6fb,	0x3f7fd59c,	0x3f7fd434,	0x3f7fd2c1,	0x3f7fd145,	0x3f7fcfbf,	0x3f7fce2e,	
	0x3f7fcc93,	0x3f7fcaec,	0x3f7fc93b,	0x3f7fc77e,	0x3f7fc5b6,	0x3f7fc3e2,	0x3f7fc202,	0x3f7fc016,	0x3f7fbe1d,	0x3f7fbc18,	0x3f7fba05,	0x3f7fb7e6,	0x3f7fb5b9,	0x3f7fb37e,	0x3f7fb135,	0x3f7faede,	
	0x3f7fac78,	0x3f7faa03,	0x3f7fa780,	0x3f7fa4ed,	0x3f7fa24a,	0x3f7f9f98,	0x3f7f9cd5,	0x3f7f9a02,	0x3f7f971e,	0x3f7f9428,	0x3f7f9122,	0x3f7f8e0a,	0x3f7f8adf,	0x3f7f87a3,	0x3f7f8453,	0x3f7f80f1,	
	0x3f7f7d7b,	0x3f7f79f2,	0x3f7f7655,	0x3f7f72a3,	0x3f7f6edd,	0x3f7f6b02,	0x3f7f6712,	0x3f7f630c,	0x3f7f5ef0,	0x3f7f5abd,	0x3f7f5674,	0x3f7f5214,	0x3f7f4d9c,	0x3f7f490d,	0x3f7f4465,	0x3f7f3fa5,	
	0x3f7f3acb,	0x3f7f35d9,	0x3f7f30cc,	0x3f7f2ba6,	0x3f7f2665,	0x3f7f2109,	0x3f7f1b92,	0x3f7f15ff,	0x3f7f1050,	0x3f7f0a84,	0x3f7f049b,	0x3f7efe96,	0x3f7ef872,	0x3f7ef230,	0x3f7eebcf,	0x3f7ee550,	
	0x3f7edeb0,	0x3f7ed7f1,	0x3f7ed112,	0x3f7eca11,	0x3f7ec2ef,	0x3f7ebbac,	0x3f7eb446,	0x3f7eacbe,	0x3f7ea513,	0x3f7e9d44,	0x3f7e9550,	0x3f7e8d39,	0x3f7e84fc,	0x3f7e7c9a,	0x3f7e7412,	0x3f7e6b63,	
	0x3f7e628e,	0x3f7e5991,	0x3f7e506c,	0x3f7e471f,	0x3f7e3da9,	0x3f7e3409,	0x3f7e2a40,	0x3f7e204c,	0x3f7e162d,	0x3f7e0be3,	0x3f7e016c,	0x3f7df6c9,	0x3f7debfa,	0x3f7de0fc,	0x3f7dd5d1,	0x3f7dca77,	
	0x3f7dbeee,	0x3f7db335,	0x3f7da74c,	0x3f7d9b32,	0x3f7d8ee7,	0x3f7d826a,	0x3f7d75bb,	0x3f7d68d8,	0x3f7d5bc3,	0x3f7d4e79,	0x3f7d40fb,	0x3f7d3347,	0x3f7d255e,	0x3f7d173e,	0x3f7d08e8,	0x3f7cfa5a,	
	0x3f7ceb95,	0x3f7cdc96,	0x3f7ccd5f,	0x3f7cbdee,	0x3f7cae43,	0x3f7c9e5c,	0x3f7c8e3b,	0x3f7c7ddd,	0x3f7c6d43,	0x3f7c5c6c,	0x3f7c4b57,	0x3f7c3a03,	0x3f7c2871,	0x3f7c169f,	0x3f7c048d,	0x3f7bf23a,	
	0x3f7bdfa6,	0x3f7bccd0,	0x3f7bb9b8,	0x3f7ba65d,	0x3f7b92be,	0x3f7b7eda,	0x3f7b6ab2,	0x3f7b5644,	0x3f7b4190,	0x3f7b2c96,	0x3f7b1754,	0x3f7b01cb,	0x3f7aebf9,	0x3f7ad5de,	0x3f7abf79,	0x3f7aa8ca,	
	0x3f7a91d0,	0x3f7a7a8a,	0x3f7a62f9,	0x3f7a4b1b,	0x3f7a32ef,	0x3f7a1a76,	0x3f7a01ae,	0x3f79e897,	0x3f79cf31,	0x3f79b57a,	0x3f799b72,	0x3f798118,	0x3f79666d,	0x3f794b6f,	0x3f79301d,	0x3f791478,	
	0x3f78f87e,	0x3f78dc2f,	0x3f78bf8b,	0x3f78a290,	0x3f78853e,	0x3f786795,	0x3f784994,	0x3f782b3a,	0x3f780c87,	0x3f77ed7a,	0x3f77ce13,	0x3f77ae51,	0x3f778e33,	0x3f776db9,	0x3f774ce2,	0x3f772bae,	
	0x3f770a1c,	0x3f76e82c,	0x3f76c5dd,	0x3f76a32e,	0x3f76801f,	0x3f765caf,	0x3f7638de,	0x3f7614ab,	0x3f75f016,	0x3f75cb1e,	0x3f75a5c2,	0x3f758003,	0x3f7559de,	0x3f753355,	0x3f750c66,	0x3f74e511,	
	0x3f74bd55,	0x3f749531,	0x3f746ca6,	0x3f7443b3,	0x3f741a57,	0x3f73f091,	0x3f73c662,	0x3f739bc8,	0x3f7370c3,	0x3f734553,	0x3f731977,	0x3f72ed2f,	0x3f72c079,	0x3f729357,	0x3f7265c6,	0x3f7237c7,	
	0x3f720959,	0x3f71da7c,	0x3f71ab30,	0x3f717b73,	0x3f714b45,	0x3f711aa6,	0x3f70e996,	0x3f70b813,	0x3f70861e,	0x3f7053b6,	0x3f7020db,	0x3f6fed8c,	0x3f6fb9c9,	0x3f6f8591,	0x3f6f50e4,	0x3f6f1bc2,	
	0x3f6ee629,	0x3f6eb01b,	0x3f6e7996,	0x3f6e429a,	0x3f6e0b26,	0x3f6dd33b,	0x3f6d9ad7,	0x3f6d61fb,	0x3f6d28a6,	0x3f6ceed8,	0x3f6cb490,	0x3f6c79cf,	0x3f6c3e93,	0x3f6c02dc,	0x3f6bc6ab,	0x3f6b89fe,	
	0x3f6b4cd5,	0x3f6b0f31,	0x3f6ad111,	0x3f6a9274,	0x3f6a535a,	0x3f6a13c3,	0x3f69d3af,	0x3f69931d,	0x3f69520e,	0x3f691080,	0x3f68ce74,	0x3f688be9,	0x3f6848df,	0x3f680556,	0x3f67c14e,	0x3f677cc7,	
	0x3f6737bf,	0x3f66f238,	0x3f66ac30,	0x3f6665a8,	0x3f661ea0,	0x3f65d716,	0x3f658f0c,	0x3f654681,	0x3f64fd74,	0x3f64b3e6,	0x3f6469d7,	0x3f641f46,	0x3f63d433,	0x3f63889e,	0x3f633c87,	0x3f62efee,	
	0x3f62a2d3,	0x3f625535,	0x3f620715,	0x3f61b873,	0x3f61694e,	0x3f6119a6,	0x3f60c97b,	0x3f6078ce,	0x3f60279e,	0x3f5fd5eb,	0x3f5f83b6,	0x3f5f30fd,	0x3f5eddc1,	0x3f5e8a03,	0x3f5e35c2,	0x3f5de0fe,	
	0x3f5d8bb7,	0x3f5d35ed,	0x3f5cdfa0,	0x3f5c88d1,	0x3f5c317f,	0x3f5bd9aa,	0x3f5b8153,	0x3f5b2879,	0x3f5acf1d,	0x3f5a753e,	0x3f5a1ade,	0x3f59bffb,	0x3f596496,	0x3f5908af,	0x3f58ac46,	0x3f584f5c,	
	0x3f57f1f0,	0x3f579403,	0x3f573595,	0x3f56d6a6,	0x3f567736,	0x3f561745,	0x3f55b6d4,	0x3f5555e2,	0x3f54f471,	0x3f54927f,	0x3f54300e,	0x3f53cd1e,	0x3f5369af,	0x3f5305c0,	0x3f52a154,	0x3f523c68,	
	0x3f51d6ff,	0x3f517118,	0x3f510ab3,	0x3f50a3d1,	0x3f503c72,	0x3f4fd497,	0x3f4f6c3f,	0x3f4f036b,	0x3f4e9a1c,	0x3f4e3051,	0x3f4dc60b,	0x3f4d5b4b,	0x3f4cf011,	0x3f4c845d,	0x3f4c182f,	0x3f4bab88,	
	0x3f4b3e69,	0x3f4ad0d2,	0x3f4a62c2,	0x3f49f43b,	0x3f49853d,	0x3f4915c9,	0x3f48a5df,	0x3f48357f,	0x3f47c4a9,	0x3f47535f,	0x3f46e1a1,	0x3f466f6f,	0x3f45fcca,	0x3f4589b2,	0x3f451627,	0x3f44a22b,	
	0x3f442dbe,	0x3f43b8e0,	0x3f434392,	0x3f42cdd4,	0x3f4257a8,	0x3f41e10c,	0x3f416a03,	0x3f40f28d,	0x3f407aa9,	0x3f40025a,	0x3f3f899e,	0x3f3f1078,	0x3f3e96e8,	0x3f3e1ced,	0x3f3da28a,	0x3f3d27be,	
	0x3f3cac8a,	0x3f3c30ef,	0x3f3bb4ed,	0x3f3b3885,	0x3f3abbb8,	0x3f3a3e87,	0x3f39c0f1,	0x3f3942f9,	0x3f38c49d,	0x3f3845e0,	0x3f37c6c2,	0x3f374743,	0x3f36c765,	0x3f364727,	0x3f35c68c,	0x3f354593,	
	0x3f34c43d,	0x3f34428b,	0x3f33c07d,	0x3f333e16,	0x3f32bb54,	0x3f32383a,	0x3f31b4c7,	0x3f3130fd,	0x3f30acdc,	0x3f302865,	0x3f2fa39a,	0x3f2f1e7a,	0x3f2e9907,	0x3f2e1341,	0x3f2d8d2a,	0x3f2d06c1,	
	0x3f2c8009,	0x3f2bf901,	0x3f2b71ab,	0x3f2aea07,	0x3f2a6217,	0x3f29d9da,	0x3f295153,	0x3f28c882,	0x3f283f67,	0x3f27b605,	0x3f272c5a,	0x3f26a26a,	0x3f261833,	0x3f258db8,	0x3f2502f9,	0x3f2477f7,	
	0x3f23ecb3,	0x3f23612e,	0x3f22d569,	0x3f224964,	0x3f21bd21,	0x3f2130a1,	0x3f20a3e5,	0x3f2016ed,	0x3f1f89ba,	0x3f1efc4e,	0x3f1e6ea9,	0x3f1de0cc,	0x3f1d52b9,	0x3f1cc470,	0x3f1c35f3,	0x3f1ba741,	
	0x3f1b185d,	0x3f1a8947,	0x3f19fa00,	0x3f196a89,	0x3f18dae3,	0x3f184b10,	0x3f17bb0f,	0x3f172ae3,	0x3f169a8c,	0x3f160a0b,	0x3f157961,	0x3f14e88f,	0x3f145796,	0x3f13c678,	0x3f133535,	0x3f12a3ce,	
	0x3f121244,	0x3f118098,	0x3f10eecc,	0x3f105ce1,	0x3f0fcad6,	0x3f0f38ae,	0x3f0ea669,	0x3f0e1409,	0x3f0d818f,	0x3f0ceefb,	0x3f0c5c4e,	0x3f0bc98a,	0x3f0b36b0,	0x3f0aa3c1,	0x3f0a10bd,	0x3f097da6,	
	0x3f08ea7e,	0x3f085744,	0x3f07c3fa,	0x3f0730a1,	0x3f069d3a,	0x3f0609c6,	0x3f057647,	0x3f04e2bd,	0x3f044f29,	0x3f03bb8c,	0x3f0327e8,	0x3f02943e,	0x3f02008e,	0x3f016cd9,	0x3f00d921,	0x3f004567,	
	0x3eff6359,	0x3efe3be2,	0x3efd146d,	0x3efbecfc,	0x3efac591,	0x3ef99e2e,	0x3ef876d6,	0x3ef74f8a,	0x3ef6284c,	0x3ef50120,	0x3ef3da05,	0x3ef2b300,	0x3ef18c12,	0x3ef0653c,	0x3eef3e82,	0x3eee17e5,	
	0x3eecf167,	0x3eebcb0b,	0x3eeaa4d2,	0x3ee97ebf,	0x3ee858d4,	0x3ee73312,	0x3ee60d7d,	0x3ee4e815,	0x3ee3c2dd,	0x3ee29dd8,	0x3ee17906,	0x3ee0546b,	0x3edf3008,	0x3ede0bdf,	0x3edce7f2,	0x3edbc444,	
	0x3edaa0d7,	0x3ed97dac,	0x3ed85ac5,	0x3ed73825,	0x3ed615ce,	0x3ed4f3c1,	0x3ed3d200,	0x3ed2b08f,	0x3ed18f6d,	0x3ed06e9f,	0x3ecf4e25,	0x3ece2e01,	0x3ecd0e36,	0x3ecbeec5,	0x3ecacfb1,	0x3ec9b0fb,	
	0x3ec892a6,	0x3ec774b3,	0x3ec65724,	0x3ec539fb,	0x3ec41d3a,	0x3ec300e3,	0x3ec1e4f8,	0x3ec0c97a,	0x3ebfae6d,	0x3ebe93d0,	0x3ebd79a8,	0x3ebc5ff4,	0x3ebb46b7,	0x3eba2df4,	0x3eb915ab,	0x3eb7fddf,	
	0x3eb6e691,	0x3eb5cfc4,	0x3eb4b978,	0x3eb3a3b1,	0x3eb28e6f,	0x3eb179b4,	0x3eb06583,	0x3eaf51dc,	0x3eae3ec2,	0x3ead2c37,	0x3eac1a3b,	0x3eab08d2,	0x3ea9f7fc,	0x3ea8e7bb,	0x3ea7d812,	0x3ea6c901,	
	0x3ea5ba8a,	0x3ea4acaf,	0x3ea39f72,	0x3ea292d4,	0x3ea186d6,	0x3ea07b7b,	0x3e9f70c5,	0x3e9e66b4,	0x3e9d5d4a,	0x3e9c5489,	0x3e9b4c72,	0x3e9a4507,	0x3e993e4a,	0x3e98383b,	0x3e9732dd,	0x3e962e31,	
	0x3e952a39,	0x3e9426f5,	0x3e932468,	0x3e922292,	0x3e912176,	0x3e902114,	0x3e8f216f,	0x3e8e2287,	0x3e8d245e,	0x3e8c26f5,	0x3e8b2a4e,	0x3e8a2e6a,	0x3e89334a,	0x3e8838f0,	0x3e873f5d,	0x3e864692,	
	0x3e854e90,	0x3e84575a,	0x3e8360ef,	0x3e826b52,	0x3e817683,	0x3e808284,	0x3e7f1ead,	0x3e7d39f6,	0x3e7b56e5,	0x3e79757d,	0x3e7795c0,	0x3e75b7b2,	0x3e73db53,	0x3e7200a6,	0x3e7027ad,	0x3e6e506b,	
	0x3e6c7ae1,	0x3e6aa712,	0x3e68d500,	0x3e6704ac,	0x3e65361a,	0x3e63694a,	0x3e619e3f,	0x3e5fd4fb,	0x3e5e0d7f,	0x3e5c47ce,	0x3e5a83ea,	0x3e58c1d3,	0x3e57018d,	0x3e554318,	0x3e538677,	0x3e51cbab,	
	0x3e5012b6,	0x3e4e5b9a,	0x3e4ca657,	0x3e4af2f1,	0x3e494168,	0x3e4791be,	0x3e45e3f4,	0x3e44380c,	0x3e428e07,	0x3e40e5e7,	0x3e3f3fae,	0x3e3d9b5b,	0x3e3bf8f2,	0x3e3a5872,	0x3e38b9de,	0x3e371d37,	
	0x3e35827e,	0x3e33e9b3,	0x3e3252d9,	0x3e30bdf0,	0x3e2f2af9,	0x3e2d99f6,	0x3e2c0ae7,	0x3e2a7dce,	0x3e28f2ac,	0x3e276980,	0x3e25e24e,	0x3e245d14,	0x3e22d9d5,	0x3e215890,	0x3e1fd947,	0x3e1e5bfb,	
	0x3e1ce0ac,	0x3e1b675b,	0x3e19f008,	0x3e187ab5,	0x3e170762,	0x3e15960f,	0x3e1426be,	0x3e12b96e,	0x3e114e20,	0x3e0fe4d5,	0x3e0e7d8d,	0x3e0d1849,	0x3e0bb508,	0x3e0a53cc,	0x3e08f494,	0x3e079761,	
	0x3e063c33,	0x3e04e30b,	0x3e038be9,	0x3e0236cc,	0x3e00e3b5,	0x3dff254a,	0x3dfc8736,	0x3df9ed2e,	0x3df75734,	0x3df4c546,	0x3df23765,	0x3defad91,	0x3ded27ca,	0x3deaa610,	0x3de82862,	0x3de5aec0,	
	0x3de33929,	0x3de0c79e,	0x3dde5a1f,	0x3ddbf0a9,	0x3dd98b3e,	0x3dd729db,	0x3dd4cc82,	0x3dd27330,	0x3dd01de6,	0x3dcdcca2,	0x3dcb7f64,	0x3dc9362b,	0x3dc6f0f5,	0x3dc4afc1,	0x3dc27290,	0x3dc0395f,	
	0x3dbe042d,	0x3dbbd2f9,	0x3db9a5c3,	0x3db77c88,	0x3db55747,	0x3db335fe,	0x3db118ad,	0x3daeff52,	0x3dace9ec,	0x3daad877,	0x3da8caf4,	0x3da6c160,	0x3da4bbba,	0x3da2b9ff,	0x3da0bc2e,	0x3d9ec246,	
	0x3d9ccc43,	0x3d9ada24,	0x3d98ebe8,	0x3d97018b,	0x3d951b0d,	0x3d93386a,	0x3d9159a1,	0x3d8f7eae,	0x3d8da791,	0x3d8bd447,	0x3d8a04cd,	0x3d883920,	0x3d86713f,	0x3d84ad27,	0x3d82ecd6,	0x3d813048,	
	0x3d7eeef6,	0x3d7b84da,	0x3d782235,	0x3d74c702,	0x3d71733b,	0x3d6e26dc,	0x3d6ae1dd,	0x3d67a43a,	0x3d646ded,	0x3d613eef,	0x3d5e173a,	0x3d5af6c9,	0x3d57dd95,	0x3d54cb98,	0x3d51c0cc,	0x3d4ebd2b,	
	0x3d4bc0ad,	0x3d48cb4c,	0x3d45dd03,	0x3d42f5c9,	0x3d401599,	0x3d3d3c6c,	0x3d3a6a3a,	0x3d379efd,	0x3d34daae,	0x3d321d47,	0x3d2f66bf,	0x3d2cb70f,	0x3d2a0e31,	0x3d276c1d,	0x3d24d0cc,	0x3d223c37,	
	0x3d1fae55,	0x3d1d2720,	0x3d1aa690,	0x3d182c9d,	0x3d15b940,	0x3d134c71,	0x3d10e628,	0x3d0e865d,	0x3d0c2d09,	0x3d09da24,	0x3d078da5,	0x3d054784,	0x3d0307ba,	0x3d00ce3f,	0x3cfd3613,	0x3cf8dc25,	
	0x3cf48ea3,	0x3cf04d7b,	0x3cec189e,	0x3ce7effb,	0x3ce3d380,	0x3cdfc31e,	0x3cdbbec3,	0x3cd7c65d,	0x3cd3d9dd,	0x3ccff930,	0x3ccc2445,	0x3cc85b0c,	0x3cc49d72,	0x3cc0eb67,	0x3cbd44d8,	0x3cb9a9b5,	
	0x3cb619ea,	0x3cb29568,	0x3caf1c1b,	0x3cabadf3,	0x3ca84add,	0x3ca4f2c7,	0x3ca1a59f,	0x3c9e6354,	0x3c9b2bd2,	0x3c97ff09,	0x3c94dce6,	0x3c91c556,	0x3c8eb848,	0x3c8bb5a8,	0x3c88bd66,	0x3c85cf6d,	
	0x3c82ebac,	0x3c801211,	0x3c7a8511,	0x3c74fa00,	0x3c6f82cb,	0x3c6a1f4d,	0x3c64cf5f,	0x3c5f92dc,	0x3c5a69a0,	0x3c555385,	0x3c505065,	0x3c4b601b,	0x3c468280,	0x3c41b770,	0x3c3cfec5,	0x3c385859,	
	0x3c33c406,	0x3c2f41a7,	0x3c2ad114,	0x3c26722a,	0x3c2224c1,	0x3c1de8b4,	0x3c19bddc,	0x3c15a414,	0x3c119b35,	0x3c0da319,	0x3c09bb9a,	0x3c05e491,	0x3c021dd9,	0x3bfcce97,	0x3bf58182,	0x3bee5426,	
	0x3be74638,	0x3be05769,	0x3bd9876c,	0x3bd2d5f3,	0x3bcc42b1,	0x3bc5cd57,	0x3bbf7596,	0x3bb93b21,	0x3bb31da8,	0x3bad1cdc,	0x3ba7386c,	0x3ba17009,	0x3b9bc362,	0x3b963224,	0x3b90bbff,	0x3b8b609e,	
	0x3b861fae,	0x3b80f8d9,	0x3b77d793,	0x3b6df04f,	0x3b643b33,	0x3b5ab788,	0x3b516491,	0x3b48418f,	0x3b3f4dba,	0x3b368843,	0x3b2df053,	0x3b258506,	0x3b1d456e,	0x3b15308d,	0x3b0d4550,	0x3b05828f,	
	0x3afbce05,	0x3aece283,	0x3ade3f64,	0x3acfe100,	0x3ac1c30f,	0x3ab3e07b,	0x3aa6330f,	0x3a98b309,	0x3a8b5668,	0x3a7c1fa8,	0x3a619970,	0x3a46e3a0,	0x3a2ba86e,	0x3a0f5326,	0x39e16fb3,	0x399962f2

};

const unsigned int Audio::AAC::KBD_128[256] = {

	0x3837b147,	0x38f8e089,	0x3971ec83,	0x39cc3229,	0x3a1ecf68,	0x3a69d4e7,	0x3aa5607b,	0x3ae2c6f4,	0x3b17a699,	0x3b46aa70,	0x3b7fb8c8,	0x3ba219f1,	0x3bcacad0,	0x3bfab72d,	0x3c1958d7,	0x3c39cb44,	
	0x3c5f2531,	0x3c84ee44,	0x3c9d3508,	0x3cb8a44e,	0x3cd77a1b,	0x3cf9f48d,	0x3d1028af,	0x3d2566af,	0x3d3cd1ef,	0x3d56870e,	0x3d72a159,	0x3d889d3f,	0x3d99351e,	0x3dab230e,	0x3dbe7092,	0x3dd325c9,	
	0x3de9494b,	0x3e007004,	0x3e0cf697,	0x3e1a3907,	0x3e2836ff,	0x3e36ef28,	0x3e465f20,	0x3e568370,	0x3e675791,	0x3e78d5e2,	0x3e857bda,	0x3e8edaa2,	0x3e9882e4,	0x3ea26fbc,	0x3eac9bcc,	0x3eb70144,	
	0x3ec199eb,	0x3ecc5f23,	0x3ed749fe,	0x3ee25340,	0x3eed736e,	0x3ef8a2de,	0x3f01ece0,	0x3f078819,	0x3f0d1f24,	0x3f12ae10,	0x3f1830f6,	0x3f1da402,	0x3f23037a,	0x3f284bc5,	0x3f2d7972,	0x3f32893b,	
	0x3f377811,	0x3f3c431c,	0x3f40e7c3,	0x3f4563af,	0x3f49b4cf,	0x3f4dd95a,	0x3f51cfd3,	0x3f559709,	0x3f592e18,	0x3f5c9469,	0x3f5fc9b2,	0x3f62cdf2,	0x3f65a171,	0x3f6844bd,	0x3f6ab8a3,	0x3f6cfe2f,	
	0x3f6f16a6,	0x3f71037d,	0x3f72c659,	0x3f746104,	0x3f75d56a,	0x3f772592,	0x3f785396,	0x3f7961a0,	0x3f7a51de,	0x3f7b2683,	0x3f7be1be,	0x3f7c85b2,	0x3f7d1477,	0x3f7d9013,	0x3f7dfa73,	0x3f7e5570,	
	0x3f7ea2c3,	0x3f7ee40c,	0x3f7f1aca,	0x3f7f485d,	0x3f7f6e07,	0x3f7f8ceb,	0x3f7fa60d,	0x3f7fba54,	0x3f7fca8c,	0x3f7fd766,	0x3f7fe17c,	0x3f7fe953,	0x3f7fef5a,	0x3f7ff3ee,	0x3f7ff75f,	0x3f7ff9ec,	
	0x3f7ffbc9,	0x3f7ffd21,	0x3f7ffe15,	0x3f7ffebf,	0x3f7fff33,	0x3f7fff80,	0x3f7fffb3,	0x3f7fffd3,	0x3f7fffe7,	0x3f7ffff3,	0x3f7ffff9,	0x3f7ffffd,	0x3f7fffff,	0x3f800000,	0x3f800000,	0x3f800000,	
	0x3f800000,	0x3f800000,	0x3f800000,	0x3f7fffff,	0x3f7ffffd,	0x3f7ffff9,	0x3f7ffff3,	0x3f7fffe7,	0x3f7fffd3,	0x3f7fffb3,	0x3f7fff80,	0x3f7fff33,	0x3f7ffebf,	0x3f7ffe15,	0x3f7ffd21,	0x3f7ffbc9,	
	0x3f7ff9ec,	0x3f7ff75f,	0x3f7ff3ee,	0x3f7fef5a,	0x3f7fe953,	0x3f7fe17c,	0x3f7fd766,	0x3f7fca8c,	0x3f7fba54,	0x3f7fa60d,	0x3f7f8ceb,	0x3f7f6e07,	0x3f7f485d,	0x3f7f1aca,	0x3f7ee40c,	0x3f7ea2c3,	
	0x3f7e5570,	0x3f7dfa73,	0x3f7d9013,	0x3f7d1477,	0x3f7c85b2,	0x3f7be1be,	0x3f7b2683,	0x3f7a51de,	0x3f7961a0,	0x3f785396,	0x3f772592,	0x3f75d56a,	0x3f746104,	0x3f72c659,	0x3f71037d,	0x3f6f16a6,	
	0x3f6cfe2f,	0x3f6ab8a3,	0x3f6844bd,	0x3f65a171,	0x3f62cdf2,	0x3f5fc9b2,	0x3f5c9469,	0x3f592e18,	0x3f559709,	0x3f51cfd3,	0x3f4dd95a,	0x3f49b4cf,	0x3f4563af,	0x3f40e7c3,	0x3f3c431c,	0x3f377811,	
	0x3f32893b,	0x3f2d7972,	0x3f284bc5,	0x3f23037a,	0x3f1da402,	0x3f1830f6,	0x3f12ae10,	0x3f0d1f24,	0x3f078819,	0x3f01ece0,	0x3ef8a2de,	0x3eed736e,	0x3ee25340,	0x3ed749fe,	0x3ecc5f23,	0x3ec199eb,	
	0x3eb70144,	0x3eac9bcc,	0x3ea26fbc,	0x3e9882e4,	0x3e8edaa2,	0x3e857bda,	0x3e78d5e2,	0x3e675791,	0x3e568370,	0x3e465f20,	0x3e36ef28,	0x3e2836ff,	0x3e1a3907,	0x3e0cf697,	0x3e007004,	0x3de9494b,	
	0x3dd325c9,	0x3dbe7092,	0x3dab230e,	0x3d99351e,	0x3d889d3f,	0x3d72a159,	0x3d56870e,	0x3d3cd1ef,	0x3d2566af,	0x3d1028af,	0x3cf9f48d,	0x3cd77a1b,	0x3cb8a44e,	0x3c9d3508,	0x3c84ee44,	0x3c5f2531,	
	0x3c39cb44,	0x3c1958d7,	0x3bfab72d,	0x3bcacad0,	0x3ba219f1,	0x3b7fb8c8,	0x3b46aa70,	0x3b17a699,	0x3ae2c6f4,	0x3aa5607b,	0x3a69d4e7,	0x3a1ecf68,	0x39cc3229,	0x3971ec83,	0x38f8e089,	0x3837b147

};



