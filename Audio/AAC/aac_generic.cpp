/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\AAC.h"

#include "System\SysUtils.h"



__forceinline void Audio::AAC::LTP_Data(unsigned int idx) {
	unsigned int x_val(40);

	if (x_val > channel_cfg.ics[idx].max_sfb) x_val = channel_cfg.ics[idx].max_sfb;

	if (audio_object_type == 23) {
		if (decoder_cfg.NextBit()) {
			channel_cfg.ics[idx].ltp_lag = decoder_cfg.GetNumber(10);
		}

		channel_cfg.ics[idx].ltp_coef = decoder_cfg.GetNumber(3);

		for (unsigned int i(0);i<x_val;i++) {
			channel_cfg.ics[idx].ltp_long_used[i] = decoder_cfg.NextBit();
		}

	} else {
		channel_cfg.ics[0].ltp_lag = decoder_cfg.GetNumber(11);

		channel_cfg.ics[0].ltp_coef = decoder_cfg.GetNumber(3);

		if (channel_cfg.ics[0].window_sequence != EIGHT_SHORT_SEQUENCE) {
			for (unsigned int i(0);i<x_val;i++) {
				channel_cfg.ics[0].ltp_long_used[i] = decoder_cfg.NextBit();
			}
		}
	}
}



__forceinline void Audio::AAC::ICS_Info(unsigned int common_window, unsigned int idx) {
	unsigned short sfb_rc(pred_sfb_max[sf_idx]);

	decoder_cfg.NextBit(); // serserved

	channel_cfg.ics[idx].window_sequence = decoder_cfg.GetNumber(2);

	decoder_cfg.frame_cfg->window_sample_count = 1024;
	decoder_cfg.frame_cfg->window_count = 1;

	if (channel_cfg.ics[idx].window_sequence == EIGHT_SHORT_SEQUENCE) {
/*
		decoder_cfg.frame_cfg->window_sample_count = 128;
		decoder_cfg.frame_cfg->window_count = 8;
		decoder_cfg.frame_cfg->window_offset = 128;
*/

		sfb_offsets = swb_offset_short[sf_idx];
		channel_cfg.ics[idx].sfb_count = num_swb_short_window[sf_idx];
		channel_cfg.ics[idx].line_count = frame_lines;			


	} else {

		sfb_offsets = swb_offset_long[sf_idx];
		channel_cfg.ics[idx].sfb_count = num_swb_long_window[sf_idx];
		channel_cfg.ics[idx].line_count = (frame_lines << 3);
	}

	channel_cfg.ics[idx].scale_fac = 0x38000000;
	reinterpret_cast<float *>(&channel_cfg.ics[idx].scale_fac)[0] /= channel_cfg.ics[idx].line_count;


	channel_cfg.ics[idx].window_shape = decoder_cfg.NextBit();

	if (channel_cfg.ics[idx].window_sequence == EIGHT_SHORT_SEQUENCE) {
		channel_cfg.ics[idx].max_sfb = decoder_cfg.GetNumber(4);
		channel_cfg.ics[idx].scale_factor_grouping = decoder_cfg.GetNumber(7);

		channel_cfg.ics[idx].num_windows = 8;
		channel_cfg.ics[idx].num_window_groups = 1;
		channel_cfg.ics[idx].window_group_size[0] = 1;

		
		for (unsigned int x_val(64);x_val;x_val>>=1) {
			if (channel_cfg.ics[idx].scale_factor_grouping & x_val) {
				channel_cfg.ics[idx].window_group_size[channel_cfg.ics[idx].num_window_groups - 1]++;
			} else {				
				channel_cfg.ics[idx].window_group_size[channel_cfg.ics[idx].num_window_groups++] = 1;
			}
		}
	} else {
		channel_cfg.ics[idx].max_sfb = decoder_cfg.GetNumber(6);

		channel_cfg.ics[idx].num_windows = 1;
		channel_cfg.ics[idx].num_window_groups = 1;
		channel_cfg.ics[idx].window_group_size[0] = 1;

		if (decoder_cfg.NextBit()) { // predictor present
			if (audio_object_type == 1) {
				if (decoder_cfg.NextBit()) {
					channel_cfg.ics[idx].predictor_reset_group_number = decoder_cfg.GetNumber(5);
				}

				if (channel_cfg.ics[idx].sfb_count > channel_cfg.ics[idx].max_sfb) sfb_rc = channel_cfg.ics[idx].max_sfb;

				for (unsigned short i(0);i<sfb_rc;i++) {
					channel_cfg.ics[idx].prediction_used[i] = decoder_cfg.NextBit();

				}
			} else {
				if (decoder_cfg.NextBit()) {
					LTP_Data(idx);
				}

				if (common_window) {
					if (decoder_cfg.NextBit()) {
						LTP_Data(idx);
					}
				}
			}
		}
	}
}


__forceinline void Audio::AAC::SectionData(unsigned int idx) {
	unsigned int sesc_val(31), sect_len_bits(5), top_val(0), sl_inc(0);

	if (channel_cfg.ics[idx].window_sequence == EIGHT_SHORT_SEQUENCE) {
		sesc_val = 7;
		sect_len_bits = 3;		
	}

	for (unsigned short g(0);g<channel_cfg.ics[idx].num_window_groups;g++) {
		for (channel_cfg.group[g].section_count = 0, top_val = 0;top_val < channel_cfg.ics[idx].max_sfb;channel_cfg.group[g].section_count++) {
			channel_cfg.group[g].section[channel_cfg.group[g].section_count].code_book_idx = decoder_cfg.GetNumber(4);

			for (sl_inc = decoder_cfg.GetNumber(sect_len_bits);sl_inc == sesc_val;sl_inc = decoder_cfg.GetNumber(sect_len_bits)) {
				channel_cfg.group[g].section[channel_cfg.group[g].section_count].length += sl_inc;
			}

			channel_cfg.group[g].section[channel_cfg.group[g].section_count].length += sl_inc;
			
			channel_cfg.group[g].section[channel_cfg.group[g].section_count].start = top_val;
			top_val += channel_cfg.group[g].section[channel_cfg.group[g].section_count].length;
		}

		if (top_val > channel_cfg.ics[idx].max_sfb) {
			channel_cfg.group[g].section[channel_cfg.group[g].section_count-1].length -= (top_val - channel_cfg.ics[idx].max_sfb);

		}
	}
}

__forceinline void Audio::AAC::ScaleFactorData(unsigned int idx) {
	int npcm(1);
	short pred_val0(channel_cfg.ics[idx].global_gain), pred_val1(0), pred_val2(0);

	for (unsigned short g(0);g<channel_cfg.ics[idx].num_window_groups;g++) {
		
		for (unsigned short i(0);i<channel_cfg.group[g].section_count;i++) {
			switch (channel_cfg.group[g].section[i].code_book_idx) {				
				case 13:
					if (npcm > 0) {						
						// dpcm noise ngr
						pred_val2 += decoder_cfg.GetNumber(9);
						channel_cfg.gain[g][channel_cfg.group[g].section[i].start] = pred_val2;
					}

					for (unsigned short j(npcm>0);j<channel_cfg.group[g].section[i].length;j++) {
						npcm--;
						// temporal noise decoding
						pred_val2 += mod_lav[0][decoder_cfg.GetHuffmaVal(Huffman_LC[0], 20)][0];

						channel_cfg.gain[g][channel_cfg.group[g].section[i].start + j] = pred_val2;
					}
						
				break;

				case 14: case 15: // stereo position decoding, codebook 0 is used
					for (unsigned short j(0);j<channel_cfg.group[g].section[i].length;j++) {
						// scale factor decoding
						pred_val1 += mod_lav[0][decoder_cfg.GetHuffmaVal(Huffman_LC[0], 20)][0];

						channel_cfg.gain[g][channel_cfg.group[g].section[i].start + j] = pred_val1;
					}
					
				break;

				default:
					for (unsigned short j(0);j<channel_cfg.group[g].section[i].length;j++) {
						// scale factor decoding
						pred_val0 += mod_lav[0][decoder_cfg.GetHuffmaVal(Huffman_LC[0], 20)][0];

						channel_cfg.gain[g][channel_cfg.group[g].section[i].start + j] = pred_val0;
					}
				case 0:;
			}
		}
	}
}

__forceinline void Audio::AAC::PulseData(unsigned int idx) {	
	unsigned int number = decoder_cfg.GetNumber(2); // number_pulse
	unsigned int o_val = sfb_offsets[decoder_cfg.GetNumber(6)];

	for (unsigned int i(0);i<=number;i++) {
		o_val += decoder_cfg.GetNumber(5); // pulse offset
		reinterpret_cast<int *>(decoder_cfg.spectral_data)[((o_val & 0xFFFFFFFC)<<1) + 4 + (o_val & 3)] = decoder_cfg.GetNumber(4); // pulse amp
	}
}


__forceinline void Audio::AAC::TNSData(unsigned int idx) {
	static const int sign_val[8][2] = {{0, 0}, {0x01, 0xFFFFFFFE},  {0x02, 0xFFFFFFFC}, {0x04, 0xFFFFFFF8}, {0x08, 0xFFFFFFF0}, {0x10, 0xFFFFFFE0}, {0x20, 0xFFFFFFC0}, {0x40, 0xFFFFFFF8}};
	float b_set[32], a_set[32];
	unsigned int coef_bcount(3), bcount_adjust(0);
	
	if (channel_cfg.ics[idx].window_sequence == EIGHT_SHORT_SEQUENCE) {
		for (unsigned int w(0);w<8;w++) {
			coef_bcount = 3;

			if (channel_cfg.ics[idx].tns_cfg[w].filter_count = decoder_cfg.NextBit()) {
				coef_bcount += decoder_cfg.NextBit();
				channel_cfg.ics[idx].tns_cfg[w].filter[0].length = decoder_cfg.GetNumber(4);

				if (channel_cfg.ics[idx].tns_cfg[w].filter[0].order = decoder_cfg.GetNumber(3)) {
					channel_cfg.ics[idx].tns_cfg[w].filter[0].direction_flag = decoder_cfg.NextBit();
					bcount_adjust = decoder_cfg.NextBit();

					for (unsigned short i(1);i<=channel_cfg.ics[idx].tns_cfg[w].filter[0].order;i++) {
						reinterpret_cast<int *>(b_set)[i] = decoder_cfg.GetNumber(coef_bcount - bcount_adjust);

						if (reinterpret_cast<int *>(b_set)[i] & sign_val[coef_bcount - bcount_adjust][0]) reinterpret_cast<int *>(b_set)[i] |= sign_val[coef_bcount - bcount_adjust][1];
						reinterpret_cast<int *>(b_set)[i] = tns_sin[coef_bcount-3][8 + reinterpret_cast<int *>(b_set)[i]];
					}

// setup linear prediction coefficients
					channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs[0] = 1.0f;
					for (int m(1);m<=channel_cfg.ics[idx].tns_cfg[w].filter[0].order;m++) {
						for (int i(1);i<m;i++) {
							a_set[i] = channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs[i] + b_set[m]*channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs[m-i];
						}
						for (int i(1);i<m;i++) {
							channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs[i] = a_set[i];
						}
						channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs[m] = b_set[m];
					}

					for (int m(1);m<=channel_cfg.ics[idx].tns_cfg[w].filter[0].order;m++) {
						reinterpret_cast<int *>(channel_cfg.ics[idx].tns_cfg[w].filter[0].coefs)[m] ^= 0x80000000;
					}
				}
			}
		}
	} else {
		if (channel_cfg.ics[idx].tns_cfg[0].filter_count = decoder_cfg.GetNumber(2)) {
			coef_bcount += decoder_cfg.NextBit();		

			for (unsigned short f(0);f<channel_cfg.ics[idx].tns_cfg[0].filter_count;f++) {
				channel_cfg.ics[idx].tns_cfg[0].filter[f].length = decoder_cfg.GetNumber(6);

				if (channel_cfg.ics[idx].tns_cfg[0].filter[f].order = decoder_cfg.GetNumber(5)) {
					channel_cfg.ics[idx].tns_cfg[0].filter[f].direction_flag = decoder_cfg.NextBit();
				
					bcount_adjust = decoder_cfg.NextBit();

					for (unsigned short i(1);i<=channel_cfg.ics[idx].tns_cfg[0].filter[f].order;i++) {
						reinterpret_cast<int *>(b_set)[i] = decoder_cfg.GetNumber(coef_bcount - bcount_adjust);

						if (reinterpret_cast<int *>(b_set)[i] & sign_val[coef_bcount - bcount_adjust][0]) reinterpret_cast<int *>(b_set)[i] |= sign_val[coef_bcount - bcount_adjust][1];
						reinterpret_cast<int *>(b_set)[i] = tns_sin[coef_bcount-3][8 + reinterpret_cast<int *>(b_set)[i]];
					}

					channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs[0] = 1.0f;
					for (int m(1);m<=channel_cfg.ics[idx].tns_cfg[0].filter[f].order;m++) {
						for (int i(1);i<m;i++) {
							a_set[i] = channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs[i] + b_set[m]*channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs[m-i];
						}
						for (int i(1);i<m;i++) {
							channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs[i] = a_set[i];
						}
						channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs[m] = b_set[m];
					}

					for (int m(1);m<=channel_cfg.ics[idx].tns_cfg[0].filter[f].order;m++) {
						reinterpret_cast<int *>(channel_cfg.ics[idx].tns_cfg[0].filter[f].coefs)[m] ^= 0x80000000;
					}

				}
			}
		}
	}
}

__forceinline void Audio::AAC::GainControlData(unsigned int idx) {
	channel_cfg.ics[idx].max_band = decoder_cfg.GetNumber(2);

	switch (channel_cfg.ics[idx].window_sequence) {
		case ONLY_LONG_SEQUENCE:
			for (unsigned short i(0);i<channel_cfg.ics[idx].max_band;i++) {
				channel_cfg.ics[idx].gain_control[i][0].number = decoder_cfg.GetNumber(3);

				for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][0].number;j++) {
					channel_cfg.ics[idx].gain_control[i][0].level[j] = decoder_cfg.GetNumber(4);
					channel_cfg.ics[idx].gain_control[i][0].location[j] = decoder_cfg.GetNumber(5);
				}
			}
		break;
		case LONG_START_SEQUENCE:
			for (unsigned short i(0);i<channel_cfg.ics[idx].max_band;i++) {
				channel_cfg.ics[idx].gain_control[i][0].number = decoder_cfg.GetNumber(3);

				for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][0].number;j++) {
					channel_cfg.ics[idx].gain_control[i][0].level[j] = decoder_cfg.GetNumber(4);
					channel_cfg.ics[idx].gain_control[i][0].location[j] = decoder_cfg.GetNumber(4);
				}

				channel_cfg.ics[idx].gain_control[i][1].number = decoder_cfg.GetNumber(3);

				for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][1].number;j++) {
					channel_cfg.ics[idx].gain_control[i][1].level[j] = decoder_cfg.GetNumber(4);
					channel_cfg.ics[idx].gain_control[i][1].location[j] = decoder_cfg.GetNumber(2);
				}
			}
		break;
		case EIGHT_SHORT_SEQUENCE:
			for (unsigned short i(0);i<channel_cfg.ics[idx].max_band;i++) {
				for (unsigned w(0);w<8;w++) {
					channel_cfg.ics[idx].gain_control[i][w].number = decoder_cfg.GetNumber(3);

					for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][w].number;j++) {
						channel_cfg.ics[idx].gain_control[i][w].level[j] = decoder_cfg.GetNumber(4);
						channel_cfg.ics[idx].gain_control[i][w].location[j] = decoder_cfg.GetNumber(2);
					}
				}
			}
		break;
		case LONG_STOP_SEQUENCE:
			for (unsigned short i(0);i<channel_cfg.ics[idx].max_band;i++) {
				channel_cfg.ics[idx].gain_control[i][0].number = decoder_cfg.GetNumber(3);

				for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][0].number;j++) {
					channel_cfg.ics[idx].gain_control[i][0].level[j] = decoder_cfg.GetNumber(4);
					channel_cfg.ics[idx].gain_control[i][0].location[j] = decoder_cfg.GetNumber(4);
				}

				channel_cfg.ics[idx].gain_control[i][1].number = decoder_cfg.GetNumber(3);

				for (unsigned short j(0);j<channel_cfg.ics[idx].gain_control[i][1].number;j++) {
					channel_cfg.ics[idx].gain_control[i][1].level[j] = decoder_cfg.GetNumber(4);
					channel_cfg.ics[idx].gain_control[i][1].location[j] = decoder_cfg.GetNumber(5);
				}
			}
		break;
	}
}


void Audio::AAC::SpectralData(unsigned int idx) {
	int do_val(0), length_sb(0);
	unsigned int h_val(0), window_i(0);
	int * section_ptr(0), * window_ptr(0);
	
	
	for (unsigned short g(0);g<channel_cfg.ics[idx].num_window_groups;g++) {

		for (unsigned short i(0);i<channel_cfg.group[g].section_count;i++) { // section count
			section_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data) + (sfb_offsets[channel_cfg.group[g].section[i].start]<<1);


			if (((channel_cfg.group[g].section[i].code_book_idx - 1) & 15) < 11) {

				for (unsigned short sf_i(0);sf_i<channel_cfg.group[g].section[i].length;sf_i++) { // length is set in term of scale bands
					length_sb = sfb_offsets[channel_cfg.group[g].section[i].start + sf_i + 1];
					if (length_sb > channel_cfg.ics[idx].line_count) length_sb = channel_cfg.ics[idx].line_count;
					length_sb -= sfb_offsets[channel_cfg.group[g].section[i].start + sf_i];


					for (unsigned short w(0);w<channel_cfg.ics[idx].window_group_size[g];w++) {
						window_ptr = section_ptr + ((window_i + w) << 8);

						if (codebook_cfg[channel_cfg.group[g].section[i].code_book_idx] & 4) { // 4- way book
					
							for (int li(0);li<length_sb;li+=4, window_ptr += 8) {
								h_val = decoder_cfg.GetHuffmaVal(Huffman_LC[channel_cfg.group[g].section[i].code_book_idx], 17);


								window_ptr[0] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][0];
								window_ptr[1] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][1];
								window_ptr[2] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][2];
								window_ptr[3] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][3];


								if (codebook_cfg[channel_cfg.group[g].section[i].code_book_idx] & 1) { // unsigned 
																		
									if (window_ptr[0]) {										
										window_ptr[0] = de_quant[window_ptr[0] + window_ptr[4]];

										if (decoder_cfg.NextBit()) {
											window_ptr[0] |= 0x80000000;
										}
									}
									if (window_ptr[1]) {
										window_ptr[1] = de_quant[window_ptr[1] + window_ptr[5]];

										if (decoder_cfg.NextBit()) {
											window_ptr[1] |= 0x80000000;
										}
									}
									if (window_ptr[2]) {
										window_ptr[2] = de_quant[window_ptr[2] + window_ptr[6]];

										if (decoder_cfg.NextBit()) {
											window_ptr[2] |= 0x80000000;
										}
									}
									if (window_ptr[3]) {
										window_ptr[3] = de_quant[window_ptr[3] + window_ptr[7]];

										if (decoder_cfg.NextBit()) {
											window_ptr[3] |= 0x80000000;
										}
									}
								} else {									
									if (window_ptr[0] > 0) {
										window_ptr[0] = de_quant[window_ptr[0] + window_ptr[4]];
									} else {
										window_ptr[0] = de_quant[window_ptr[4] - window_ptr[0]] | 0x80000000;
									}
																		
									if (window_ptr[1] > 0) {
										window_ptr[1] = de_quant[window_ptr[1] + window_ptr[5]];
									} else {
										window_ptr[1] = de_quant[window_ptr[5] - window_ptr[1]] | 0x80000000;
									}
																		
									if (window_ptr[2] > 0) {
										window_ptr[2] = de_quant[window_ptr[2] + window_ptr[6]];
									} else {										
										window_ptr[2] = de_quant[window_ptr[6] - window_ptr[2]] | 0x80000000;
									}
																		
									if (window_ptr[3] > 0) {
										window_ptr[3] = de_quant[window_ptr[3] + window_ptr[7]];
									} else {
										window_ptr[3] = de_quant[window_ptr[7] - window_ptr[3]] | 0x80000000;
									}							
								}

								window_ptr[4] = window_ptr[5] = window_ptr[6] = window_ptr[7] = scale_factor[channel_cfg.gain[g][channel_cfg.group[g].section[i].start + sf_i] + 100];
							}
						} else {				
							if (codebook_cfg[channel_cfg.group[g].section[i].code_book_idx] & 2) { // 2 way book
								for (int li(0);li<length_sb;li+=4, window_ptr += 8) {
									h_val = decoder_cfg.GetHuffmaVal(Huffman_LC[channel_cfg.group[g].section[i].code_book_idx], 17);
					

									window_ptr[0] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][0];
									window_ptr[1] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][2];

									if (codebook_cfg[channel_cfg.group[g].section[i].code_book_idx] & 1) { // unsigned 
										if (window_ptr[0]) {
											window_ptr[0] = de_quant[window_ptr[0] + window_ptr[4]];

											if (decoder_cfg.NextBit()) {
												window_ptr[0] |= 0x80000000;
											}
										}
										if (window_ptr[1]) {
											window_ptr[1] = de_quant[window_ptr[1] + window_ptr[5]];

											if (decoder_cfg.NextBit()) {
												window_ptr[1] |= 0x80000000;
											}
										}
									} else {										
										if (window_ptr[0] > 0) {
											window_ptr[0] = de_quant[window_ptr[0] + window_ptr[4]];
										} else {
											window_ptr[0] = de_quant[window_ptr[4] - window_ptr[0]] | 0x80000000;
										}
																				
										if (window_ptr[1] > 0) {
											window_ptr[1] = de_quant[window_ptr[1] + window_ptr[5]];
										} else {
											window_ptr[1] = de_quant[window_ptr[5] - window_ptr[1]] | 0x80000000;
										}					
									}


									h_val = decoder_cfg.GetHuffmaVal(Huffman_LC[channel_cfg.group[g].section[i].code_book_idx], 17);


									window_ptr[2] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][0];
									window_ptr[3] = mod_lav[channel_cfg.group[g].section[i].code_book_idx][h_val][2];


									if (codebook_cfg[channel_cfg.group[g].section[i].code_book_idx] & 1) { // unssigned
										if (window_ptr[2]) {
											window_ptr[2] = de_quant[window_ptr[2] + window_ptr[6]];

											if (decoder_cfg.NextBit()) {
												window_ptr[2] |= 0x80000000;
											}
										}
										if (window_ptr[3]) {
											window_ptr[3] = de_quant[window_ptr[3] + window_ptr[7]];

											if (decoder_cfg.NextBit()) {
												window_ptr[3] |= 0x80000000;
											}
										}
									} else {										
										if (window_ptr[2] > 0) {
											window_ptr[2] = de_quant[window_ptr[6] + window_ptr[2]];
										} else {
											window_ptr[2] = de_quant[window_ptr[6] - window_ptr[2]] | 0x80000000;
										}
																				
										if (window_ptr[3] > 0) {
											window_ptr[3] = de_quant[window_ptr[7] + window_ptr[3]];
										} else {
											window_ptr[3] = de_quant[window_ptr[7] - window_ptr[3]] | 0x80000000;
										}					
									}

									window_ptr[4] = window_ptr[5] = window_ptr[6] = window_ptr[7] = scale_factor[channel_cfg.gain[g][channel_cfg.group[g].section[i].start + sf_i] + 100];
								}

							} else {
// escape book
								for (int li(0);li<length_sb;li+=4, window_ptr += 8) {
									h_val = decoder_cfg.GetHuffmaVal(Huffman_LC[11], 13);


									window_ptr[0] = mod_lav[11][h_val][0];
									window_ptr[1] = mod_lav[11][h_val][2];


									if (window_ptr[0]) {
										if (decoder_cfg.NextBit()) {
											window_ptr[4] |= 0x80000000;
										}
									}
									if (window_ptr[1]) {
										if (decoder_cfg.NextBit()) {
											window_ptr[5] |= 0x80000000;
										}
									}


									if (window_ptr[0] == 16) {
										for (h_val = 4;decoder_cfg.NextBit();h_val++);
										window_ptr[0] = (1<<h_val) + decoder_cfg.GetNumber(h_val);
									}

									if (window_ptr[1] == 16) {
										for (h_val = 4;decoder_cfg.NextBit();h_val++);
										window_ptr[1] = (1<<h_val) + decoder_cfg.GetNumber(h_val);
									}

									window_ptr[0] += (window_ptr[4] & 0x7FFFFFFF);
									window_ptr[0] = de_quant[window_ptr[0]] | (window_ptr[4] & 0x80000000);

									window_ptr[1] += (window_ptr[5] & 0x7FFFFFFF);
									window_ptr[1] = de_quant[window_ptr[1]] | (window_ptr[5] & 0x80000000);





									h_val = decoder_cfg.GetHuffmaVal(Huffman_LC[11], 13);
									window_ptr[2] = mod_lav[11][h_val][0];
									window_ptr[3] = mod_lav[11][h_val][2];

									if (window_ptr[2]) {
										if (decoder_cfg.NextBit()) {
											window_ptr[6] |= 0x80000000;
										}
									}
									if (window_ptr[3]) {
										if (decoder_cfg.NextBit()) {
											window_ptr[7] |= 0x80000000;
										}
									}


									if (window_ptr[2] == 16) {
										for (h_val = 4;decoder_cfg.NextBit();h_val++);
										window_ptr[2] = (1<<h_val) + decoder_cfg.GetNumber(h_val);
									}

									if (window_ptr[3] == 16) {
										for (h_val = 4;decoder_cfg.NextBit();h_val++);
										window_ptr[3] = (1<<h_val) + decoder_cfg.GetNumber(h_val);
									}

									window_ptr[2] += (window_ptr[6] & 0x7FFFFFFF);
									window_ptr[2] = de_quant[window_ptr[2]] | (window_ptr[6] & 0x80000000);

									window_ptr[3] += (window_ptr[7] & 0x7FFFFFFF);
									window_ptr[3] = de_quant[window_ptr[3]] | (window_ptr[7] & 0x80000000);



									window_ptr[4] = window_ptr[5] = window_ptr[6] = window_ptr[7] = scale_factor[channel_cfg.gain[g][channel_cfg.group[g].section[i].start + sf_i] + 100];
								}
							}
						}
					}

					section_ptr += (length_sb<<1);
				}
			} else {
				// load ones

			}
		}

		window_i += channel_cfg.ics[idx].window_group_size[g];
	}
}


void Audio::AAC::GetChannel(unsigned int cwv, unsigned int idx) {	
	channel_cfg.GroupClear();

	channel_cfg.ics[idx].global_gain = decoder_cfg.GetNumber(8);
	
	if (cwv == 0) {		
		ICS_Info(cwv, idx);
	}

	SectionData(idx);

	ScaleFactorData(idx);

	if (decoder_cfg.NextBit()) { // pulse present
		PulseData(idx);

	}

	if (decoder_cfg.NextBit()) { // tns present
		TNSData(idx);

	}

	if (decoder_cfg.NextBit()) { // gain control present
		GainControlData(idx);

	}

	SpectralData(idx);


}

void Audio::AAC::GetCouplingChannel() {
	unsigned int x_val(0), x_flag(0);

	decoder_cfg.GetNumber(4);

	x_flag = decoder_cfg.NextBit();

	coupling_cfg.element_count = decoder_cfg.GetNumber(3) + 1;
	coupling_cfg.gain_element_list_count = 0;

	for (unsigned int i(0);i<coupling_cfg.element_count;i++) {
		coupling_cfg.gain_element_list_count++;

		x_val = decoder_cfg.NextBit();
		coupling_cfg.target_tag[i] = decoder_cfg.GetNumber(4);

		if (x_val) {
			coupling_cfg.lr_state[i] = decoder_cfg.NextBit();
			coupling_cfg.lr_state[i] |= (decoder_cfg.NextBit()<<1);

			if (coupling_cfg.lr_state[i] == 3) coupling_cfg.gain_element_list_count++;
		}
	}


	coupling_cfg.domain = decoder_cfg.NextBit();
	coupling_cfg.gain_element_sign = decoder_cfg.NextBit();
	coupling_cfg.gain_element_scale = decoder_cfg.GetNumber(2);

	channel_cfg.ICSClear();
	GetChannel(0, 0);




	for (unsigned int i(0);i<coupling_cfg.gain_element_list_count;i++) {
		if (x_flag) {
			x_val = x_flag;
		} else {
			x_val = decoder_cfg.NextBit();
		}
					
		if (x_val) {
			coupling_cfg.common_gain[i] = decoder_cfg.GetHuffmaVal(Huffman_LC[0], 20);
		} else {
			for (unsigned int g(0);g<channel_cfg.ics[0].num_window_groups;g++) {
				for (unsigned int s(0);s<channel_cfg.group[g].section_count;s++) {
					if (channel_cfg.group[g].section[s].code_book_idx) {
						for (unsigned int sf_i(0);sf_i<channel_cfg.ics[0].max_sfb;sf_i++) {								
							coupling_cfg.gain[i][g][sf_i] = decoder_cfg.GetHuffmaVal(Huffman_LC[channel_cfg.group[g].section[s].code_book_idx], 17);
						}
					}
				}
			}
		}
	}
}

UI_64 Audio::AAC::GenericRaw() {
	UI_64 result(RESULT_DECODE_COMPLETE);
	unsigned int x_val(0), x_flag(0);
	

	if (program_cfg[current_program].center_present) {
		program_cfg[current_program].load_runner[0] = 1;
		program_cfg[current_program].load_runner[1] = -1;
	} else {
		program_cfg[current_program].load_runner[0] = program_cfg[current_program].num_channel_elements[0];
		program_cfg[current_program].load_runner[1] = 0;
	}
	
	program_cfg[current_program].load_runner[2] = -1;
	program_cfg[current_program].load_runner[3] = 0;

	for (x_val = decoder_cfg.GetNumber(3);(x_val ^ 7);x_val = decoder_cfg.GetNumber(3)) {
		if (x_val < 4)
		if (decoder_cfg.frame_cfg->source_chan_count >= program_cfg[current_program].chan_count) {
			result = 0;
			break;
		}

		switch (x_val) {
			case 3: // lfe channel element
				program_cfg[current_program].load_runner[2] = decoder_cfg.frame_cfg->source_chan_count;
			case 0: // single channel eleemnt				
				program_cfg[current_program].load_runner[3]++; // channel_selector				

				x_val = decoder_cfg.GetNumber(4);
				
				channel_cfg.ICSClear();

				GetChannel(0, 0);



				Rescale(decoder_cfg.spectral_data, frame_lines>>2);

				Restore(0);

				IntensityAdjust(0);

			// coupling

				TNSAdjust(0);

				if (channel_cfg.ics[0].window_sequence == EIGHT_SHORT_SEQUENCE) {
					for (unsigned int w(0);w<8;w++) {
						IDCT_IV(decoder_cfg.spectral_data + (w<<8), cos_table_0 + 4096, channel_cfg.ics[0].line_count, decoder_cfg.spectral_data + (w<<8) + 2048, channel_cfg.ics[0].scale_fac);
					}
					Format8Window(decoder_cfg.spectral_data + 2048, previous_frame_delta, window_table[EIGHT_SHORT_SEQUENCE][previous_shape[decoder_cfg.frame_cfg->source_chan_count] | channel_cfg.ics[0].window_shape]);
				} else {						
					IDCT_IV(decoder_cfg.spectral_data, cos_table_0, channel_cfg.ics[0].line_count, 0, channel_cfg.ics[0].scale_fac);
					FormatWindow(decoder_cfg.spectral_data + 2048, previous_frame_delta, window_table[channel_cfg.ics[0].window_sequence][previous_shape[decoder_cfg.frame_cfg->source_chan_count] | channel_cfg.ics[0].window_shape], channel_cfg.ics[0].line_count);
				}


				previous_shape[decoder_cfg.frame_cfg->source_chan_count] = (channel_cfg.ics[0].window_shape << 1);

				if (--program_cfg[current_program].load_runner[0] == 0) {
					if (++program_cfg[current_program].load_runner[1] < 5) {
						program_cfg[current_program].load_runner[0] = program_cfg[current_program].num_channel_elements[program_cfg[current_program].load_runner[1]];
					}
				}
				
				decoder_cfg.spectral_data += AAC_CHANNEL_SIZE;
				decoder_cfg.frame_cfg->source_chan_count++;
			break;
			case 1: // channel pair element
				channel_cfg.ICSClear();

				x_val = decoder_cfg.GetNumber(4);						

				if (x_flag = decoder_cfg.NextBit()) {	 // common window				
					
					ICS_Info(1, 0);

					channel_cfg.ics[0].ms_mask = decoder_cfg.GetNumber(2);
					

					if (channel_cfg.ics[0].ms_mask == 1) {
						for (unsigned short g(0);g<channel_cfg.ics[0].num_window_groups;g++) {
							for (unsigned short i(0);i<channel_cfg.ics[0].max_sfb;i++) {
								channel_cfg.ics[0].ms_used[g][i] = decoder_cfg.NextBit();

							}
						}
					}

					System::MemoryCopy(channel_cfg.ics + 1, channel_cfg.ics, sizeof(ChannelConfig::ICS));
				}
				

				GetChannel(x_flag, 0);



				Rescale(decoder_cfg.spectral_data, frame_lines>>2);

			

				if (--program_cfg[current_program].load_runner[0] == 0) {
					if (++program_cfg[current_program].load_runner[1] < 5) {
						program_cfg[current_program].load_runner[0] = program_cfg[current_program].num_channel_elements[program_cfg[current_program].load_runner[1]];
					}
				}






				decoder_cfg.spectral_data += AAC_CHANNEL_SIZE;


				GetChannel(x_flag, 1);

	
	
				Rescale(decoder_cfg.spectral_data, frame_lines >> 2);



				switch (channel_cfg.ics[0].ms_mask) {
					case 2:
						StereoAdjustAll(decoder_cfg.spectral_data, frame_lines >> 1);
					break;
					case 1:
						StereoAdjust();
					break;
				}

				Restore(1);

				IntensityAdjust(1);

			// coupling

				TNSAdjust(1);
				

				if (channel_cfg.ics[0].window_sequence == EIGHT_SHORT_SEQUENCE) {
					for (unsigned int w(0);w<8;w++) { // channel_cfg.ics.num_windows
						IDCT_IV(decoder_cfg.spectral_data + (w<<8) - AAC_CHANNEL_SIZE, cos_table_0 + 4096, channel_cfg.ics[0].line_count, decoder_cfg.spectral_data + (w<<8) - 2048, channel_cfg.ics[0].scale_fac);
					}
					Format8Window(decoder_cfg.spectral_data - 2048, previous_frame_delta, window_table[EIGHT_SHORT_SEQUENCE][previous_shape[decoder_cfg.frame_cfg->source_chan_count] | channel_cfg.ics[0].window_shape]);
				} else {
					IDCT_IV(decoder_cfg.spectral_data - AAC_CHANNEL_SIZE, cos_table_0, channel_cfg.ics[0].line_count, 0, channel_cfg.ics[0].scale_fac);
					FormatWindow(decoder_cfg.spectral_data - 2048, previous_frame_delta, window_table[channel_cfg.ics[0].window_sequence][previous_shape[decoder_cfg.frame_cfg->source_chan_count] | channel_cfg.ics[0].window_shape], channel_cfg.ics[0].line_count);
				}
				



				if (channel_cfg.ics[1].window_sequence == EIGHT_SHORT_SEQUENCE) {
					for (unsigned int w(0);w<8;w++) { // channel_cfg.ics[1].num_windows
						IDCT_IV(decoder_cfg.spectral_data + (w<<8), cos_table_0 + 4096, channel_cfg.ics[1].line_count, decoder_cfg.spectral_data + (w<<8) + 2048, channel_cfg.ics[1].scale_fac);
					}
					Format8Window(decoder_cfg.spectral_data + 2048, previous_frame_delta, window_table[EIGHT_SHORT_SEQUENCE][previous_shape[decoder_cfg.frame_cfg->source_chan_count + 1] | channel_cfg.ics[1].window_shape]);
				} else {
					IDCT_IV(decoder_cfg.spectral_data, cos_table_0, channel_cfg.ics[1].line_count, 0, channel_cfg.ics[1].scale_fac);
					FormatWindow(decoder_cfg.spectral_data + 2048, previous_frame_delta, window_table[channel_cfg.ics[1].window_sequence][previous_shape[decoder_cfg.frame_cfg->source_chan_count + 1] | channel_cfg.ics[1].window_shape], channel_cfg.ics[1].line_count);
				}


				previous_shape[decoder_cfg.frame_cfg->source_chan_count] = (channel_cfg.ics[0].window_shape << 1);
				previous_shape[decoder_cfg.frame_cfg->source_chan_count + 1] = (channel_cfg.ics[1].window_shape << 1);

				if (--program_cfg[current_program].load_runner[0] == 0) {
					if (++program_cfg[current_program].load_runner[1] < 5) {
						program_cfg[current_program].load_runner[0] = program_cfg[current_program].num_channel_elements[program_cfg[current_program].load_runner[1]];
					}
				}

				decoder_cfg.spectral_data += AAC_CHANNEL_SIZE;
				decoder_cfg.frame_cfg->source_chan_count += 2;
			break;
			case 2: // coupling channel element
				GetCouplingChannel();

			break;
								

			case 4: // data steam element
				decoder_cfg.GetNumber(4);

				x_flag = decoder_cfg.NextBit();
				x_val = decoder_cfg.GetNumber(8);
				if (x_val == 255) {
					x_val += decoder_cfg.GetNumber(8);
				}

				if (x_flag) decoder_cfg.ByteAlign();

				decoder_cfg.Skip(x_val);

			break;
	
			case 5: // program config element
				GetProgramConfig();

				if (program_cfg[current_program].center_present) {
					program_cfg[current_program].load_runner[0] = 1;
					program_cfg[current_program].load_runner[2] = -1;
					program_cfg[current_program].load_runner[3] = 0;
				} else {
					program_cfg[current_program].load_runner[0] = program_cfg[current_program].num_channel_elements[0];
					program_cfg[current_program].load_runner[2] = -1;
					program_cfg[current_program].load_runner[3] = 0;
				}
				program_cfg[current_program].load_runner[1] = 0;

			break;
			case 6: // fill element
				x_val = decoder_cfg.GetNumber(4);
				if (x_val == 15) x_val += (decoder_cfg.GetNumber(8) - 1);
				// extension payload to be imleneted

				decoder_cfg.Skip(x_val);
			break;
		}

		
	}

	decoder_cfg.ByteAlign();
		
	if (result) {
		switch (decoder_cfg.frame_cfg->source_chan_count) {
			case 1:
				decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
			break;
			case 2:
				if (program_cfg[current_program].load_runner[2] != -1) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_LOW_FREQUENCY;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
				}
			break;
			case 3:
				if (program_cfg[current_program].load_runner[2] != -1) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_LOW_FREQUENCY;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
				}
			break;
			case 4:
				if (program_cfg[current_program].load_runner[2] != -1) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_LOW_FREQUENCY;

			} else {
				if (program_cfg[current_program].load_runner[3]) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_CENTER;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_RIGHT;
				}
			}

			break;
			case 5:
				if (program_cfg[current_program].load_runner[2] != -1) {
					if (program_cfg[current_program].load_runner[3] > 1) {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_CENTER;

					} else {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_LEFT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_RIGHT;
					}
					
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_LOW_FREQUENCY;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;
				}

			break;
			case 6:
				if (program_cfg[current_program].load_runner[2] != -1) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;
					decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_LOW_FREQUENCY;

				} else {
					if (program_cfg[current_program].load_runner[3]) {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
						decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;
						decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_CENTER;

					} else {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_SIDE_LEFT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_SIDE_RIGHT;
						decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_LEFT;
						decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_RIGHT;
					}
				}

			break;
			case 7:
				if (program_cfg[current_program].load_runner[2] != -1) {
					if (program_cfg[current_program].load_runner[3] > 1) {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
						decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;
						decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_CENTER;

					} else {
						decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
						decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
						decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_SIDE_LEFT;
						decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_SIDE_RIGHT;
						decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_LEFT;
						decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_RIGHT;
					}

					decoder_cfg.frame_cfg->chan_type[6] = SPEAKER_LOW_FREQUENCY;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_SIDE_LEFT;
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_SIDE_RIGHT;
					decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[6] = SPEAKER_BACK_RIGHT;
				}

			break;
			case 8:
				if (program_cfg[current_program].load_runner[2] != -1) {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_SIDE_LEFT;
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_SIDE_RIGHT;
					decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[6] = SPEAKER_BACK_RIGHT;
					decoder_cfg.frame_cfg->chan_type[7] = SPEAKER_LOW_FREQUENCY;
				} else {
					decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;
					decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_LEFT;
					decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
					decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_SIDE_LEFT;
					decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_SIDE_RIGHT;
					decoder_cfg.frame_cfg->chan_type[5] = SPEAKER_BACK_LEFT;
					decoder_cfg.frame_cfg->chan_type[6] = SPEAKER_BACK_RIGHT;
					decoder_cfg.frame_cfg->chan_type[7] = SPEAKER_BACK_CENTER;
				}

			break;
			
		}

		decoder_cfg.spectral_data -= decoder_cfg.frame_cfg->source_chan_count*AAC_CHANNEL_SIZE;
	}



	return result;
}




void Audio::AAC::StereoAdjust() {
	float * l_ptr = decoder_cfg.spectral_data - AAC_CHANNEL_SIZE;
	float * r_ptr = decoder_cfg.spectral_data;

	float tmp_val[8];
	int sfb_dist(0);


	for (unsigned int g(0);g<channel_cfg.ics[0].num_window_groups;g++) {
		for (unsigned int w(0);w<channel_cfg.ics[0].window_group_size[g];w++) {
			for (unsigned int i(0);i<channel_cfg.ics[0].max_sfb;i++) {
				sfb_dist = sfb_offsets[i+1];
				if (sfb_dist > channel_cfg.ics[0].line_count) sfb_dist = channel_cfg.ics[0].line_count;
				sfb_dist -= sfb_offsets[i];

				if (channel_cfg.ics[0].ms_used[g][i]) {
					for (int sf_i(0);sf_i<sfb_dist;sf_i+=4) {
						tmp_val[0] = l_ptr[0] - r_ptr[0];
						tmp_val[1] = l_ptr[1] - r_ptr[1];
						tmp_val[2] = l_ptr[2] - r_ptr[2];
						tmp_val[3] = l_ptr[3] - r_ptr[3];
						tmp_val[4] = l_ptr[4] - r_ptr[4];
						tmp_val[5] = l_ptr[5] - r_ptr[5];
						tmp_val[6] = l_ptr[6] - r_ptr[6];
						tmp_val[7] = l_ptr[7] - r_ptr[7];

						l_ptr[0] += r_ptr[0];
						l_ptr[1] += r_ptr[1];
						l_ptr[2] += r_ptr[2];
						l_ptr[3] += r_ptr[3];
						l_ptr[4] += r_ptr[4];
						l_ptr[5] += r_ptr[5];
						l_ptr[6] += r_ptr[6];
						l_ptr[7] += r_ptr[7];
						
						r_ptr[0] = tmp_val[0];
						r_ptr[1] = tmp_val[1];
						r_ptr[2] = tmp_val[2];
						r_ptr[3] = tmp_val[3];
						r_ptr[4] = tmp_val[4];
						r_ptr[5] = tmp_val[5];
						r_ptr[6] = tmp_val[6];
						r_ptr[7] = tmp_val[7];

						l_ptr += 8;
						r_ptr += 8;
					}
				} else {
					sfb_dist <<= 1;
					l_ptr += sfb_dist;
					r_ptr += sfb_dist;
				}
			}


			sfb_dist = channel_cfg.ics[0].line_count - sfb_offsets[channel_cfg.ics[0].max_sfb];
			if ((int)sfb_dist > 0) {				
				sfb_dist <<= 1;
				l_ptr += sfb_dist;
				r_ptr += sfb_dist;
			}
		}
	}
}

void Audio::AAC::Restore(unsigned int chan_count) {


}


void Audio::AAC::IntensityAdjust(unsigned int chan_count) {


}

void Audio::AAC::TNSAdjust(unsigned int double_chan) {
	int top(0), bottom(0), band_size(0), short0(channel_cfg.ics[0].window_sequence == EIGHT_SHORT_SEQUENCE), short1(channel_cfg.ics[1].window_sequence == EIGHT_SHORT_SEQUENCE);
	float * band_ptr(0), * window_ptr(decoder_cfg.spectral_data - double_chan*AAC_CHANNEL_SIZE);

	for (unsigned int w(0);w<channel_cfg.ics[0].num_windows;w++) {
		bottom = channel_cfg.ics[0].sfb_count;		

		for (unsigned int f(0);f<channel_cfg.ics[0].tns_cfg[w].filter_count;f++) {
			top = bottom;
			if (top > channel_cfg.ics[0].max_sfb) top = channel_cfg.ics[0].max_sfb;
			if (top > tns_max_sfb[sf_idx][short0]) top = tns_max_sfb[sf_idx][short0];

			bottom -= channel_cfg.ics[0].tns_cfg[w].filter[f].length;
			if (bottom < 0) bottom = 0;
			
			if (bottom >= top) continue;
			if (channel_cfg.ics[0].tns_cfg[w].filter[f].order == 0) continue;

			band_size = sfb_offsets[top];
			if (band_size > channel_cfg.ics[0].line_count) band_size = channel_cfg.ics[0].line_count;

			
			if (channel_cfg.ics[0].tns_cfg[w].filter[f].direction_flag) {
				band_ptr = window_ptr + (band_size<<1);

				band_size -= sfb_offsets[bottom];
				TNSFilterDown(band_ptr, band_size, channel_cfg.ics[0].tns_cfg[w].filter[f].coefs);
			} else {
				band_ptr = window_ptr + (sfb_offsets[bottom]<<1);

				band_size -= sfb_offsets[bottom];
				TNSFilterUp(band_ptr, band_size, channel_cfg.ics[0].tns_cfg[w].filter[f].coefs);
			}
		}

		window_ptr += (channel_cfg.ics[0].line_count<<1);
	}

	window_ptr = decoder_cfg.spectral_data;

	if (double_chan) {
		for (unsigned int w(0);w<channel_cfg.ics[1].num_windows;w++) {
			bottom = channel_cfg.ics[1].sfb_count;
			
			for (unsigned int f(0);f<channel_cfg.ics[1].tns_cfg[w].filter_count;f++) {
				top = bottom;				
				if (top > channel_cfg.ics[1].max_sfb) top = channel_cfg.ics[1].max_sfb;
				if (top > tns_max_sfb[sf_idx][short1]) top = tns_max_sfb[sf_idx][short1];
				
				bottom -= channel_cfg.ics[1].tns_cfg[w].filter[f].length;
				if (bottom < 0) bottom = 0;
								
				if (bottom >= top) continue;
				if (channel_cfg.ics[1].tns_cfg[w].filter[f].order == 0) continue;

				band_size = sfb_offsets[top];
				if (band_size > channel_cfg.ics[1].line_count) band_size = channel_cfg.ics[1].line_count;

			
				if (channel_cfg.ics[1].tns_cfg[w].filter[f].direction_flag) {
					band_ptr = window_ptr + (band_size<<1);

					band_size -= sfb_offsets[bottom];
					TNSFilterDown(band_ptr, band_size, channel_cfg.ics[1].tns_cfg[w].filter[f].coefs);
				} else {
					band_ptr = window_ptr + (sfb_offsets[bottom]<<1);

					band_size -= sfb_offsets[bottom];
					TNSFilterUp(band_ptr, band_size, channel_cfg.ics[1].tns_cfg[w].filter[f].coefs);
				}
			}

			window_ptr += (channel_cfg.ics[1].line_count<<1);
		}
	}
}





