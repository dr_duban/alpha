/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\ID3.h"
#include "System\SysUtils.h"

#include "Audio\AudioRenderer.h"


Memory::Allocator * Audio::ID3::id3_pool = 0;

Audio::ID3::ID3(unsigned int pflag) : Program(pflag), _sequence(0), terminator(1), reciever_thread(0), version(0), temp_offset(0), sample_runner(0), sample_top(0), frame_tag(0), frame_size(0), audio_offset(0), b_position(0), fb_count(0) {
	
}

Audio::ID3::~ID3() {
	terminator = 0;
	if (reciever_thread) {
		SetEvent(program_block[0]);
		if (reciever_thread) {
			WaitForSingleObject(reciever_thread, INFINITE);
			CloseHandle(reciever_thread);
		}
	}

//	if (source) delete source;
	source.Destroy();
}



UI_64 Audio::ID3::Initialize() {
	wchar_t pool_name[] = {8, L'I', L'D', L'3', L' ', L'p', L'o', L'o', L'l'};

	UI_64 result(0);

	if (id3_pool) return -1;

	if (id3_pool = new Memory::Allocator(0)) {
		result = id3_pool->Init(new Memory::Buffer(), 0x0100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;


	return result;
}

UI_64 Audio::ID3::Finalize() {
	if (id3_pool) delete id3_pool;


	return 0;
}

bool Audio::ID3::IsID3(Data::Stream & src) {
	UI_64 b_size(4096);
	bool result(false);
	
	if (unsigned char * src_ptr = reinterpret_cast<unsigned char *>(src.MoveLock(b_size, (UI_64)0))) {		
		if ((src_ptr[0] == 'I') && (src_ptr[1] == 'D') && (src_ptr[2] == '3')) {
			result = true;
		}		
	}

	return result;
}

bool Audio::ID3::IsMP3(Data::Stream & src) {
	UI_64 b_size(4096);
	bool result(false);
	
	if (unsigned char * src_ptr = reinterpret_cast<unsigned char *>(src.MoveLock(b_size, (UI_64)0))) {
		for (;(src_ptr[0] == 0) && (b_size);src_ptr++, b_size--);

		if ((src_ptr[0] == 'I') && (src_ptr[1] == 'D') && (src_ptr[2] == '3')) {
			result = true;
		} else {
			if ((src_ptr[0] == 0xFF) && ((src_ptr[1] & 0xF0) == 0xF0)) {
				result = true;
			}
		}
	}

	return result;
}




Media::Source * Audio::ID3::GetVSource(UI_64 &) {
	return 0;
}

Media::Source * Audio::ID3::GetASource(UI_64 & s_id) {
	s_id = 0;
	return _sequence;
}

Media::Program * Audio::ID3::SetSource(const Data::Stream & src, unsigned int preview_flag) {
	ID3 * result(0);
	UI_64 eval(0);
		
	if (result = new ID3(preview_flag)) {
		result->sample_runner = 0;
		result->sample_top = 8192;

		result->source = src;

		eval = 0;
		if (result->program_block[0]) {
			if (result->reciever_thread = CreateThread(0, 0, ReceiverLoop, result, 0, 0)) {

			} else {
				eval = GetLastError();
			}
		}
		
		if (eval) {
			delete result;
			result = 0;
		}
	}


	return result;
}


UI_64 Audio::ID3::GetSpan() {
	UI_64 result(0), block_size(0), frame_size(0), start_po(0), complete_flag(0);
	I_64 t_val(0);

	Media::CodedSource * c_source(0);
	const unsigned char * block_ptr(0);
	unsigned int x_val(0), defect_size(0);
	
	start_mark = stop_mark = 0;
	
	for (;result != -1;) {
		block_size = FILE_BUFFER_SIZE;
		if (block_ptr = source.IncLock(block_size)) {
			if (block_size < FILE_BUFFER_SIZE) complete_flag = 0x40000000;
			else complete_flag = 0;

			if (source.GetStatus() & STREAM_REPEAT_FLAG) {
				result = 1;
				break;
			} else {
				if (defect_size == -1) {
					result = -1;
					break;
				}

				for (;block_size;) {

					if (!c_source)
					if ((block_size >= 10) && (audio_offset == 0)) {
						for (t_val = 0;(block_ptr[t_val] == 0) && (block_size>10);t_val++);

						if ((block_ptr[t_val] == 'I') && (block_ptr[t_val + 1] == 'D') && (block_ptr[t_val + 2] == '3')) {
							
							c_source = Media::CodedSource::Create(3);
							c_source->SetComplete(COMPLETE_CONTAINER);
							

							version = block_ptr[t_val + 3];
							x_val = block_ptr[t_val + 5]; // flags

							audio_offset = block_ptr[t_val + 6];
							audio_offset <<= 7; audio_offset |= block_ptr[t_val + 7];
							audio_offset <<= 7; audio_offset |= block_ptr[t_val + 8];
							audio_offset <<= 7; audio_offset |= block_ptr[t_val + 9];
								
							if (x_val & 0x10) audio_offset += 10; // footer present
							audio_offset += 10;

//							block_ptr += 10; block_size -= 10;

						} else {
							if ((block_ptr[t_val] == 0xFF) && ((block_ptr[t_val + 1] & 0xF0) == 0xF0)) {								
								c_source = Media::CodedSource::Create(3);
								c_source->SetComplete(COMPLETE_CONTAINER);
							}
						}

						audio_offset += t_val;
						start_po = audio_offset;
					}



					if (start_po) {
						// jump to audio 
						if (start_po >= block_size) {
							start_po -= block_size;
							block_size = 0;
						} else {
							block_ptr += start_po;
							block_size -= start_po;
							start_po = 0;

							for (;block_size;block_size--, block_ptr++, audio_offset++) {
								if ((block_ptr[0] == 0xFF) && ((block_ptr[1] & 0xF0) == 0xF0)) {
									break;
								}
							}
						}
					}

					if (c_source) {
						if (block_size) {
							t_val = -2;
							result = c_source->DecodeUnit(block_ptr, block_size | complete_flag, t_val);

							if (result & RESULT_CODEC_SPECIAL) {
								result >>= 32;

								if (block_size >= result) {
									block_size -= result;
									block_ptr += result;
								} else {
									block_size = 0;
								}

								stop_mark += t_val;

							} else {
								if (result & RESULT_STREAM_BREAK) {
									block_size = 0;
								} else {
									if (block_size < (FILE_BUFFER_SIZE>>2)) {
										defect_size = -1;
										block_size = 0;
									} else {
										result = -1;
										break;
									}
								}
							}
						}
					} else {
						result = -1;
						break;
					}
				}
			}
		} else {
			result = -1;
			break;
		}

	}

	if (result != -1) {
		c_source->SetSpecialDescriptor(stop_mark);
		c_source->InitViewBuf();
		_sequence = c_source;


		// set descriptor fields
		if (program_block[1]) {
			SetEvent(program_block[1]);
			WaitForSingleObject(program_block[0], INFINITE);
		} else {
			_sequence->On();
		}
	} else {
		result = -2;
	}

//	_sequence = c_source;
	block_size = FILE_BUFFER_SIZE;
	source.MoveLock(block_size, (UI_64)0);

	fb_count = 0;

	current_time = -1;
	b_position = 0;

	if (program_block[1]) result <<= 1;
	return result;
}

DWORD __stdcall Audio::ID3::ReceiverLoop(void * cntx) {
	UI_64 result('strt'), block_size(0), x_val(0), complete_flag(0);
	I_64 t_val(0);

	I_64 pool_index[2*MAX_POOL_COUNT];

	float a_val(0);

	ID3 * id3_ptr = reinterpret_cast<ID3 *>(cntx);
	const unsigned char * block_ptr(0);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL); // THREAD_PRIORITY_ABOVE_NORMAL THREAD_PRIORITY_TIME_CRITICAL
		
	Memory::Chain::RegisterThread(pool_index);

	if (id3_ptr->GetSpan() & 1)
	for (;id3_ptr->terminator;) {



		switch (id3_ptr->search_mode) {
			case 1:
				if (id3_ptr->search_mark == id3_ptr->stop_mark) id3_ptr->search_mark = 0;

				if (id3_ptr->search_mark < id3_ptr->current_time) {
					// jump to the beggining
					block_size = FILE_BUFFER_SIZE;
					id3_ptr->source.MoveLock(block_size, (UI_64)0);

					id3_ptr->current_time = 0;
					id3_ptr->b_position = 0;

					block_size = 0;
					block_ptr = 0;

					result = 'strt';
				}
				
				for (;(id3_ptr->search_mark > id3_ptr->current_time);) {
					if (block_size == 0) {
						block_size = FILE_BUFFER_SIZE;
						block_ptr = id3_ptr->source.IncLock(block_size);
												

						if (result == 'strt') {					
							if (id3_ptr->audio_offset <= block_size) {
								block_ptr += id3_ptr->audio_offset;
								block_size -= id3_ptr->audio_offset;
						
								result = 0;
							} else {
								block_ptr = 0;
							}
						}

						if (block_size < FILE_BUFFER_SIZE) complete_flag = 0x40000000;
						else complete_flag = 0;
					}

					if (block_ptr)
					for (;(block_size) && (id3_ptr->search_mark > id3_ptr->current_time);) {

						t_val = -2;
						if (id3_ptr->current_time == 0) t_val = -3;
							
						result = id3_ptr->_sequence->DecodeUnit(block_ptr, block_size | complete_flag, t_val);

						if (result & RESULT_CODEC_SPECIAL) {
							result >>= 32;

							if (block_size >= result) {
								block_size -= result;
								block_ptr += result;
							} else {
								block_size = 0;
							}

							id3_ptr->current_time += t_val;
						} else {
							if (result & RESULT_STREAM_BREAK) {
								block_size = 0;						
							}
						}
					}
				}
				
				id3_ptr->ResetPTS();
				id3_ptr->_sequence->Reset();
			
				id3_ptr->fb_count = 0;

				id3_ptr->b_position = id3_ptr->current_time;

				id3_ptr->search_mode = 0;
			break;
			case 11:
				if (id3_ptr->_sequence) {
					a_val = reinterpret_cast<double *>(&id3_ptr->search_mark)[0];
					id3_ptr->_sequence->SetVolume(&a_val);
				}
				
				id3_ptr->search_mode = 0;
			break;
		}



	

		
		if (!block_ptr) {
			block_size = FILE_BUFFER_SIZE;
			block_ptr = id3_ptr->source.IncLock(block_size);

			if (id3_ptr->source.GetStatus() & STREAM_REPEAT_FLAG) {
				if (id3_ptr->fb_count) {
					id3_ptr->_sequence->Flush(-1, 0);

					id3_ptr->SetPTS(id3_ptr->current_time);
					
				}

				if (id3_ptr->pending_req.element_obj) {
					id3_ptr->pending_req.flags = OpenGL::DecodeRecord::FlagSSNext;
					id3_ptr->pending_req.pic_id = -1;
					Decode(id3_ptr->pending_req);
				}


				id3_ptr->SetPTS(id3_ptr->current_time + 20000);


				if (id3_ptr->terminator == 0) break;

				id3_ptr->ResetPTS();
				id3_ptr->_sequence->Reset();


//				block_size = FILE_BUFFER_SIZE;
//				id3_ptr->source->MoveLock(block_size, (UI_64)0);

				id3_ptr->fb_count = 0;

				id3_ptr->current_time = -1;
				id3_ptr->b_position = 0;
				
				if (id3_ptr->audio_offset <= block_size) {
					block_ptr += id3_ptr->audio_offset;
					block_size -= id3_ptr->audio_offset;
				} else {
					block_ptr = 0;
					result = 'strt';
				}
				
			} else {
				if (result == 'strt') {					
					if (id3_ptr->audio_offset <= block_size) {
						block_ptr += id3_ptr->audio_offset;
						block_size -= id3_ptr->audio_offset;
						
						result = 0;
					} else {
						block_ptr = 0;
					}
				}
			}
		}

		if (block_ptr) {
			result = id3_ptr->ParseBlock(block_ptr, block_size);

			if (((result >> 40) ^ 0x00334449) == 0) {
				id3_ptr->terminator = 0;

				if (id3_ptr->pending_req.element_obj) {
					id3_ptr->pending_req.flags = OpenGL::DecodeRecord::FlagSSNext;
					id3_ptr->pending_req.pic_id = -1;
					Decode(id3_ptr->pending_req);
				}

			} else {
				if (!block_size) {
					block_ptr = 0;
				}
			}
		} else {
			// analyze p_size

		}

	}	

	id3_ptr->Terminate();

	if (id3_ptr->_sequence) {
		delete id3_ptr->_sequence;
		id3_ptr->_sequence = 0;
	}
	
	Memory::Chain::UnregisterThread();

	return result;
}



UI_64 Audio::ID3::ParseBlock(const unsigned char *& b_ptr, UI_64 & b_size) {
	UI_64 result(0), x_val(0), defect_size(0);
	I_64 t_val(0);



	for (;(b_size) && (terminator) && (!search_mode);) {
		if (fb_count) t_val = 0;

		if ((b_size) && (_sequence)) {

			t_val = current_time;
			result = _sequence->DecodeUnit(b_ptr, b_size, t_val);

			if (result >= RESULT_DECODE_COMPLETE) {
				if (result & RESULT_DECODE_COMPLETE) {												

					result >>= 32;
					if (b_size >= result) {
						b_ptr += result;
						b_size -= result;
					} else {
						b_size = 0;
					}
				
					current_time += t_val;

					if (pending_req.program) {
						pending_req.flags |= OpenGL::DecodeRecord::FlagSourceOn;
						Decode(pending_req);

						pending_req.program = 0;
					}
						

					if (++fb_count >= (MAX_FRAME_COUNT/8)) {							
						_sequence->Flush(-1, 0);

						SetPTS(b_position);
						b_position = current_time;

						fb_count = 0;
					}
				
				} else {
					if (result & RESULT_STREAM_BREAK) {
						b_size = 0;
					} else {
						result >>= 32;

						// some analysis for ecodes maybe

						result = 0x33444900;
						result <<= 32;
						break;
					}
				}
			}
		}
	}

	return result;
}


UI_64 Audio::ID3::Pack(const void *, UI_64) {

	return 0;
}

//==================================================================================================================


/*


			if ((b_ptr[0] == '3') && (b_ptr[1] == 'D') && (b_ptr[2] == 'I')) {
			// footer

				frame_tag = 0;
			}


			frame_tag = b_ptr[0]; b_ptr++; b_size--;
			frame_tag <<= 8; frame_id |= b_ptr[0]; b_ptr++; b_size--;
			frame_tag <<= 8; frame_id |= b_ptr[0]; b_ptr++; b_size--;
			frame_tag <<= 8; frame_id |= b_ptr[0]; b_ptr++; b_size--;

			if (version == 4) {
				frame_size = b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 7; frame_size |= b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 7; frame_size |= b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 7; frame_size |= b_ptr[0]; b_ptr++; b_size--;
			} else {
				frame_size = b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;
				frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;
			}
		
			frame_flags = b_ptr[0]; b_ptr++; b_size--;
			frame_flags <<= 8; frame_flags |= b_ptr[0]; b_ptr++; b_size--;

			if (version == 4) {
				frame_size <<= 2;
			}



				if (x_val & 0x40) { // extended header present
					frame_size = b_ptr[0]; b_ptr++; b_size--;
					frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;
					frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;
					frame_size <<= 8; frame_size |= b_ptr[0]; b_ptr++; b_size--;

					if (version == 4) {
						frame_size <<= 2;
					}				
				}



		switch (frame_tag) {

			case 'AENC': // Audio encryption
			break;
			case 'APIC': // Attached picture
			break;
			case 'COMM': // Comments
			break;
			case 'COMR': // Commercial frame
			break;
			case 'ENCR': // Encryption method registration
			break;
			case 'EQUA': // Equalization
			break;
			case 'ETCO': // Event timing codes
			break;
			case 'GEOB': // General encapsulated object
			break;
			case 'GRID': // Group identification registration
			break;
			case 'IPLS': // Involved people list
			break;
			case 'LINK': // Linked information
			break;
			case 'MCDI': // Music CD identifier
			break;
			case 'MLLT': // MPEG location lookup table
			break;
			case 'OWNE': // Ownership frame
			break;
			case 'PRIV': // Private frame
			break;
			case 'PCNT': // Play counter
			break;
			case 'POPM': // Popularimeter
			break;
			case 'POSS': // Position synchronisation frame
			break;
			case 'RBUF': // Recommended buffer size
			break;
			case 'RVAD': // Relative volume adjustment
			break;
			case 'RVRB': // Reverb
			break;
			case 'SYLT': // Synchronized lyric/text
			break;
			case 'SYTC': // Synchronized tempo codes
			break;
			case 'TALB': // Album/Movie/Show title
			break;
			case 'TBPM': // BPM (beats per minute)
			break;
			case 'TCOM': // Composer
			break;
			case 'TCON': // Content type
			break;
			case 'TCOP': // Copyright message
			break;
			case 'TDAT': // Date
			break;
			case 'TDLY': // Playlist delay
			break;
			case 'TENC': // Encoded by
			break;
			case 'TEXT': // Lyricist/Text writer
			break;
			case 'TFLT': // File type
			break;
			case 'TIME': // Time
			break;
			case 'TIT1': // Content group description
			break;
			case 'TIT2': // Title/songname/content description
			break;
			case 'TIT3': // Subtitle/Description refinement
			break;
			case 'TKEY': // Initial key
			break;
			case 'TLAN': // Language(s)
			break;
			case 'TLEN': // Length
			break;
			case 'TMED': // Media type
			break;
			case 'TOAL': // Original album/movie/show title
			break;
			case 'TOFN': // Original filename
			break;
			case 'TOLY': // Original lyricist(s)/text writer(s)
			break;
			case 'TOPE': // Original artist(s)/performer(s)
			break;
			case 'TORY': // Original release year
			break;
			case 'TOWN': // File owner/licensee
			break;
			case 'TPE1': // Lead performer(s)/Soloist(s)
			break;
			case 'TPE2': // Band/orchestra/accompaniment
			break;
			case 'TPE3': // Conductor/performer refinement
			break;
			case 'TPE4': // Interpreted, remixed, or otherwise modified by
			break;
			case 'TPOS': // Part of a set
			break;
			case 'TPUB': // Publisher
			break;
			case 'TRCK': // Track number/Position in set
			break;
			case 'TRDA': // Recording dates
			break;
			case 'TRSN': // Internet radio station name
			break;
			case 'TRSO': // Internet radio station owner
			break;
			case 'TSIZ': // Size
			break;
			case 'TSRC': // ISRC (international standard recording code)
			break;
			case 'TSSE': // Software/Hardware and settings used for encoding
			break;
			case 'TYER': // Year
			break;
			case 'UFID': // Unique file identifier
			break;
			case 'USER': // Terms of use
			break;
			case 'USLT': // Unsychronized lyric/text transcription
			break;
			case 'WCOM': // Commercial information
			break;
			case 'WCOP': // Copyright/Legal information
			break;
			case 'WOAF': // Official audio file webpage
			break;
			case 'WOAR': // Official artist/performer webpage
			break;
			case 'WOAS': // Official audio source webpage
			break;
			case 'WORS': // Official internet radio station homepage
			break;
			case 'WPAY': // Payment
			break;
			case 'WPUB': // Publishers official webpage
			break;
			default:
				b_ptr += frame_size;
				b_size -= frame_size;

		}
	}
*/
