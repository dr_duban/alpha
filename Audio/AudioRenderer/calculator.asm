; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE

EXTERN ?vs_color_table@DefaultRenderer@Audio@@0PAIA:DWORD

.CONST

ALIGN 16
x_sign		QWORD	8000000000000000h, 0
y_sign		QWORD	0, 8000000000000000h
double_4	QWORD	4010000000000000h, 4010000000000000h
d_4095		QWORD	40AFFE0000000000h, 40AFFE0000000000h
d_2047		QWORD	409FFC0000000000h, 409FFC0000000000h
d_480		QWORD	407E000000000000h, 407E000000000000h
d_384		QWORD	4078000000000000h, 4078000000000000h
double_16	QWORD	4030000000000000h, 4030000000000000h
double_1	QWORD	3FF0000000000000h, 3FF0000000000000h
double_05	QWORD	3FE0000000000000h, 3FE0000000000000h

.CODE


ALIGN 16
?CalculateVS@DefaultRenderer@Audio@@CAXPEA_J@Z	PROC
	PUSH r12
	PUSH r13
	PUSH r14

	PUSH rbx

	MOV rbx, rcx

	MOV r8, [rbx + 16] ; width
	MOV r9, [rbx + 24] ; height
	MOV r11, r8


	MOV r12d, [rbx + 64]
	MOV r13d, [rbx + 64 + 4]
	


	XORPD xmm3, xmm3
	XORPD xmm6, xmm6
	
	MOVAPD xmm4, [rbx + 32] ; offset
	MOVAPD xmm5, xmm4

	MOVSD xmm3, QWORD PTR [rbx + 48]
	SHUFPD xmm6, xmm3, 1
			
	MOVAPD xmm7, XMMWORD PTR [y_sign]
	MOVAPD xmm2, XMMWORD PTR [double_4]

	LEA rcx, [rbx + 128]
	XOR r14, r14

	align 16
		calc_loop:

			MOV eax, [rcx] ; runlength
			AND eax, eax
			JZ no_14a
				AND r14, r14
				CMOVZ r14, rcx
				JZ no_14u
					ADD [r14], eax
				no_14u:

				SHL rax, 5
				LEA rcx, [rcx + rax]
				SHR rax, 5
								
				CVTSI2SD xmm0, eax
				MULPD xmm0, xmm3
				ADDPD xmm5, xmm0

			no_14a:
			
			INC eax

			SUB r8d, eax
			JS ca_co

			MOV edx, [rcx + 4]

			MOVAPD xmm0, [rcx + 16]

		align 16
			run_loop:
				
				ADD edx, 8

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0
				

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				JC run_esc

			SUB eax, 1
			JNZ run_loop

		run_esc:
			MOV eax, [rcx]
			ADC eax, 0
			CMOVZ r14, rax
			JZ no_rl
				AND r14, r14
				CMOVZ r14, rcx

				ADD [r14], eax

				CMP r12d, edx
				CMOVA r12d, edx
				CMP r13d, edx
				CMOVB r13d, edx

			no_rl:
			
			ADDPD xmm5, xmm3

			MOVNTI [rcx], eax
			MOVNTI [rcx + 4], edx

			MOVNTPD [rcx + 16], xmm0
		
			ADD rcx, 32

			CMP r8d, 0
			JA calc_loop

	align 16
		ca_co:
			XOR r14, r14
			MOV r8, r11

			ADDPD xmm4, xmm6
			MOVAPD xmm5, xmm4

		DEC r9d
		JNZ calc_loop

		MOV [rbx + 64], r12d
		MOV [rbx + 64 + 4], r13d

		INC DWORD PTR [rbx]

		SFENCE

	POP rbx

	POP r14
	POP r13
	POP r12
	
	RET
?CalculateVS@DefaultRenderer@Audio@@CAXPEA_J@Z	ENDP


ALIGN 16
?ShakeVS@DefaultRenderer@Audio@@CAXPEA_J@Z	PROC
	PUSH rbp

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	PUSH rbx


	MOV rbx, rcx

	

	
	MOV r8, [rbx + 16] ; width
	MOV r11, r8
	MOV r9, [rbx + 24] ; height




	MOV r12d, [rbx + 64]
	MOV r13d, [rbx + 64 + 4]
	

	XOR r15, r15	

	MOV eax, [rbx + 80]
	CMP eax, 0
	JNL no_rsign
		NEG eax
	no_rsign:

	XOR rdx, rdx
	MUL r9
	ADD r15, rax



	MOV eax, [rbx + 84]
	CMP eax, 0
	JNL no_csign
		NEG eax
	no_csign:

	XOR rdx, rdx
	MUL r8

	ADD r15, rax

	MOV rax, [rbx + 8]
	SHR rax, 1

	LEA rax, [rax + r15 - 1]

	XOR rdx, rdx
	DIV r15

	MOV rbp, rax	

	XORPD xmm3, xmm3
	XORPD xmm6, xmm6
	
; do proper offset

	MOVAPD xmm4, [rbx + 32] ; offset	

	MOVSD xmm3, QWORD PTR [rbx + 48] ; step
	SHUFPD xmm6, xmm3, 1
			
	MOVAPD xmm7, XMMWORD PTR [y_sign]


	MOVAPD xmm2, XMMWORD PTR [double_4]



	XORPD xmm8, xmm8
	XORPD xmm9, xmm9
	XORPD xmm10, xmm10
	MOVAPD xmm11, XMMWORD PTR [double_1]





	LEA rcx, [rbx + 128]

	XOR r15, r15
	MOV rsi, 32

	MOV rdi, r8
	SHL rdi, 5
	

	CMP DWORD PTR [rbx + 84], 0
	JL go_down
		MOV eax, [rbx + 84]
		IMUL rdi, rax
		
		SUB r9d, eax
				
		CVTSI2SD xmm0, eax
		SHUFPD xmm0, xmm0, 0
		MULPD xmm0, xmm6
		ADDPD xmm4, xmm0

		CMP DWORD PTR [rbx + 80], 0
		JL go_lu
			MOV eax, [rbx + 80]
			SUB r8d, eax
			MOV r11d, r8d
			
			SHL rax, 5
			LEA rdi, [rdi + rax]
			SHR rax, 5

			CVTSI2SD xmm0, eax
		
			MULPD xmm0, xmm3
			ADDPD xmm4, xmm0
			MOVAPD xmm5, xmm4

			MOVNTPD [rbx + 32], xmm4
			

		JMP update_loop
align 16
	go_lu:
		SHL r8, 5		
		LEA rcx, [rcx + r8 - 32]		
		LEA r15, [r8*2]
		SHR r8, 5
		
		NEG rsi		

		MOVSXD rax, DWORD PTR [rbx + 80]
		SHL rax, 5
		LEA rdi, [rdi + rax]
		SAR rax, 5

		CVTSI2SD xmm0, eax
		MULPD xmm0, xmm3
		ADDPD xmm0, xmm4
		MOVNTPD [rbx + 32], xmm0

		ADD r8d, eax
		MOV r11d, r8d

		CVTSI2SD xmm0, r8d
		
		MULPD xmm0, xmm3
		ADDPD xmm4, xmm0
		MOVAPD xmm5, xmm4		

		ORPD xmm3, XMMWORD PTR [x_sign]

	JMP update_loop



align 16
	go_down:
		MOVSXD rax, DWORD PTR [rbx + 84]
		ADD r9d, eax

		IMUL rdi, rax

		CVTSI2SD xmm5, eax
		SHUFPD xmm5, xmm5, 0
		MULPD xmm5, xmm6
		ADDPD xmm5, xmm4
				
		MOV rax, [rbx + 8]
		SHL rax, 5
		LEA rcx, [rcx + rax]

		

		CVTSI2SD xmm0, r9d
		SHUFPD xmm0, xmm0, 0
		MULPD xmm0, xmm6
		ADDPD xmm4, xmm0

		ORPD xmm6, xmm7
		
		CMP DWORD PTR [rbx + 80], 0
		JL go_ld
			SHL r8, 5
			
			SUB rcx, r8

			LEA r15, [r8*2]
			NEG r15

			SHR r8, 5
			
			MOV eax, [rbx + 80]
			SUB r8d, eax
			MOV r11d, r8d

			SHL rax, 5
			LEA rdi, [rdi + rax]
			SHR rax, 5

			CVTSI2SD xmm0, eax
			
		
			MULPD xmm0, xmm3
			ADDPD xmm4, xmm0

			ADDPD xmm5, xmm0
			MOVNTPD [rbx + 32], xmm5

			MOVAPD xmm5, xmm4

			
		JMP update_loop
align 16
	go_ld:
		
		LEA rcx, [rcx - 32]

		NEG rsi		

		MOVSXD rax, DWORD PTR [rbx + 80]
		SHL rax, 5
		LEA rdi, [rdi + rax]
		SAR rax, 5

		CVTSI2SD xmm0, eax
		MULPD xmm0, xmm3
		ADDPD xmm5, xmm0
		MOVNTPD [rbx + 32], xmm5

		ADD r8d, eax
		MOV r11d, r8d

		CVTSI2SD xmm0, r8d
		
		MULPD xmm0, xmm3
		ADDPD xmm4, xmm0
		MOVAPD xmm5, xmm4		

		ORPD xmm3, XMMWORD PTR [x_sign]


		


	align 16
		update_loop:
			MOV eax, [rcx + rdi]			
			MOV edx, [rcx + rdi + 4]
			MOVAPD xmm0, [rcx + rdi + 16]

			AND eax, eax
			JNZ skip_update

				ADD edx, 2
			

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
							

												
				ADC eax, 0
				JZ skip_update_mm			
					CMP r12d, edx
					CMOVA r12d, edx
					CMP r13d, edx
					CMOVB r13d, edx
					
					JMP skip_update
			align 16
				skip_update_mm:					
					MOVAPD xmm12, xmm5
					ADDPD xmm8, xmm12
					MULPD xmm12, xmm12
					ADDPD xmm9, xmm12
					ADDPD xmm10, xmm11
		skip_update:
			ADDPD xmm5, xmm3

			MOVNTI [rcx], eax
			MOVNTI [rcx + 4], edx

			MOVNTPD [rcx + 16], xmm0
		


			ADD rcx, rsi

			DEC r8d
			JNZ update_loop

; col adjust
			MOV r8d, [rbx + 80]
			AND r8d, r8d
			JZ no_cola
			JNS no_wsign
				NEG r8d
			no_wsign:
				MOV r10d, ebp

				XORPD xmm0, xmm0
				XOR edx, edx
				XOR eax, eax
			
		align 16
			cola_loop:
				ADD edx, 4

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1
				
				COMISD xmm2, xmm1
				SBB edx, 0				
				
				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1
				
				COMISD xmm2, xmm1
				SBB edx, 0

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1
				
				COMISD xmm2, xmm1
				SBB edx, 0



				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1
				
				COMISD xmm2, xmm1				
				JC cola_complete

				
				SUB r10d, 1
				JNZ cola_loop
			cola_complete:
				ADC eax, 0
				JZ no_mma					
					CMP r12d, edx
					CMOVA r12d, edx
					CMP r13d, edx
					CMOVB r13d, edx


			no_mma:

				MOVNTI [rcx], eax
				MOVNTI [rcx + 4], edx
				MOVNTPD [rcx + 16], xmm0

				ADDPD xmm5, xmm3

				XORPD xmm0, xmm0
				XOR edx, edx
				XOR eax, eax

				ADD rcx, rsi
				MOV r10d, ebp

			DEC r8d
			JNZ cola_loop


		no_cola:

			
			MOV r8, r11

			ADDPD xmm4, xmm6
			MOVAPD xmm5, xmm4

			ADD rcx, r15

		DEC r9d
		JNZ update_loop


		MOV r9d, [rbx + 84]
		AND r9d, r9d
		JZ no_rowa
		JNS no_hsign
			NEG r9d
		no_hsign:

			MOV r11, [rbx + 16]
			MOV r8, r11


			XOR r14, r14
			MOV r10d, ebp
			
			MOV [rbx + 88], rcx

	align 16
		calc_loop:

			CMP r10d, ebp
			JZ first_line

				MOV eax, [rcx] ; runlength
				AND eax, eax
				JZ no_14a
					AND r14, r14
					CMOVZ r14, rcx
					JZ no_14u
						ADD [r14], eax
					no_14u:

					CVTSI2SD xmm0, eax

					MOV rdx, rax
					IMUL rdx, rsi
					
					LEA rcx, [rcx + rdx]
										
					MULPD xmm0, xmm3
					ADDPD xmm5, xmm0

				no_14a:
			
				INC eax

				SUB r8d, eax
				JS ca_co

				MOV edx, [rcx + 4]

				MOVAPD xmm0, [rcx + 16]

				JMP run_loop
		align 16
			first_line:
				MOV eax, 1

				SUB r8d, 1
				XOR edx, edx

				XORPD xmm0, xmm0

				MOV [rcx], edx




		align 16
			run_loop:
				
				ADD edx, 4

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0


				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				SBB edx, 0

				MOVAPD xmm1, xmm0
				SHUFPD xmm1, xmm1, 1
				MULPD xmm1, xmm0
				MULPD xmm0, xmm0
				ORPD xmm0, xmm7
				HADDPD xmm0, xmm1
				ADDPD xmm0, xmm5

				MOVAPD xmm1, xmm0
				MULPD xmm1, xmm1
				HADDPD xmm1, xmm1

				COMISD xmm2, xmm1
				JC run_esc

			SUB eax, 1
			JNZ run_loop

		run_esc:
			MOV eax, [rcx]
			ADC eax, 0
			CMOVZ r14, rax
			JZ no_rl
				AND r14, r14
				CMOVZ r14, rcx

				ADD [r14], eax

				CMP r12d, edx
				CMOVA r12d, edx
				CMP r13d, edx
				CMOVB r13d, edx

			no_rl:
			
			ADDPD xmm5, xmm3

			MOVNTI [rcx], eax
			MOVNTI [rcx + 4], edx

			MOVNTPD [rcx + 16], xmm0
		
			ADD rcx, rsi

			CMP r8d, 0
			JA calc_loop

	align 16
		ca_co:
			XOR r14, r14
			MOV r8, r11

			MOVAPD xmm5, xmm4

			DEC r10d
			CMOVNZ rcx, [rbx + 88]
			JNZ calc_loop

			
			ADDPD xmm4, xmm6
			MOVAPD xmm5, xmm4
			
			ADD rcx, r15

			MOV [rbx + 88], rcx
			MOV r10d, ebp
		DEC r9d
		JNZ calc_loop
		




	no_rowa:

		MOVAPD xmm12, xmm8
		MULPD xmm12, xmm12
		MULPD xmm9, xmm10
		SUBPD xmm9, xmm12
		MOVAPD xmm12, [rbx + 48]

		MOVAPD xmm11, [rbx + 32]
		DIVPD xmm11, xmm12
		
		MULPD xmm12, xmm10
		DIVPD xmm8, xmm12
		MULPD xmm12, xmm12
		DIVPD xmm9, xmm12

		CVTSD2SI r8, xmm9
		SHUFPD xmm9, xmm9, 1
		CVTSD2SI r9, xmm9


		SUBPD xmm8, xmm11

		XORPD xmm11, xmm11
		XORPD xmm12, xmm12		
		CVTSI2SD xmm11, QWORD PTR [rbx + 16]
		CVTSI2SD xmm12, QWORD PTR [rbx + 24]
		SHUFPD xmm12, xmm12, 1

		ORPD xmm12, xmm11

		MULPD xmm12, XMMWORD PTR [double_05]
		SUBPD xmm8, xmm12

		MOVAPD xmm11, xmm12
		SHUFPD xmm11, xmm11, 0
		DIVPD xmm11, xmm12
		
		MULPD xmm8, xmm11

		MOVAPD xmm11, xmm8
		SHUFPD xmm11, xmm11, 1

		ADDSUBPD xmm8, xmm11
		MOVMSKPD eax, xmm8




		AND eax, eax
		JNZ no_zerom
			; go up
			MOV DWORD PTR [rbx + 80], 0
			MOV DWORD PTR [rbx + 84], -1
			CMP r8, 0011000h
			JB cmp_u2
				MOV DWORD PTR [rbx + 80], -1
				JMP exit_op
		align 16
			cmp_u2:
				CMP r8, 0001000h
				JA exit_op
					MOV DWORD PTR [rbx + 80], 1
			JMP exit_op
	align 16
		no_zerom:
			CMP eax, 1
			JNZ no_onem
				; go left
				MOV DWORD PTR [rbx + 80], -1
				MOV DWORD PTR [rbx + 84], 0
				CMP r9, 0011000h
				JB cmp_l2
					MOV DWORD PTR [rbx + 84], 1
					JMP exit_op
			align 16
				cmp_l2:
					CMP r9, 0001000h
					JA exit_op
						MOV DWORD PTR [rbx + 84], -1
				JMP exit_op
			
		align 16
			no_onem:
				CMP eax, 2
				JNZ no_twom
					; go right
					MOV DWORD PTR [rbx + 80], 1
					MOV DWORD PTR [rbx + 84], 0
					CMP r9, 0011000h
					JB cmp_r2
						MOV DWORD PTR [rbx + 84], -1
						JMP exit_op
				align 16
					cmp_r2:
						CMP r9, 0001000h
						JA exit_op
							MOV DWORD PTR [rbx + 84], 1
					JMP exit_op


				
			align 16
				no_twom:
					; go down

					MOV DWORD PTR [rbx + 80], 0
					MOV DWORD PTR [rbx + 84], 1
					CMP r8, 0011000h
					JB cmp_d2
						MOV DWORD PTR [rbx + 80], 1
						JMP exit_op
				align 16
					cmp_d2:
						CMP r8, 0001000h
						JA exit_op
							MOV DWORD PTR [rbx + 80], -1


	exit_op:




		MOV [rbx + 64], r12d
		MOV [rbx + 64 + 4], r13d

		INC DWORD PTR [rbx]

		SFENCE

	POP rbx

	POP r15
	POP r14
	POP r13
	POP r12
	
	POP rdi
	POP rsi

	POP rbp

	RET
?ShakeVS@DefaultRenderer@Audio@@CAXPEA_J@Z	ENDP



ALIGN 16
?VSColorMap@DefaultRenderer@Audio@@SAXPEAIPEA_J@Z	PROC

	LEA r11, [?vs_color_table@DefaultRenderer@Audio@@0PAIA]

	
	MOV eax, [rdx]
	MOV [rdx + 4], eax

	MOV r8d, [rdx + 64]
	MOV r9d, [rdx + 64 + 4]
	MOV r10, [rdx + 8]
	SHR r10, 4
	
	MOV rax, rdx
	LEA rdx, [rdx + 128]
	
		
	SUB r9d, r8d
	JBE exit

	DEC r8d	

	CMP r9d, 640
	JB small_loop
		ADD r9d, 1
		MOV [rax + 96], r9d

		FLD1
		FILD DWORD PTR [rax + 96]
		FYL2X
				
		FDIVR QWORD PTR [d_384]

		


align 16
	color_loop:
		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_val0
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val0
				SUB eax, 640
				MOV [rdx + 8], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8]
				MOV eax, [rdx + 8]

				ADD eax, 640
		set_val0:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_val1
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val1
				SUB eax, 640
				MOV [rdx + 8 + 32], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 32]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 32]
				MOV eax, [rdx + 8 + 32]

				ADD eax, 640
		set_val1:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_val2
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val2
				SUB eax, 640
				MOV [rdx + 8 + 64], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 64]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 64]
				MOV eax, [rdx + 8 + 64]

				ADD eax, 640
		set_val2:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_val3
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val3
				SUB eax, 640
				MOV [rdx + 8 + 96], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 96]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 96]
				MOV eax, [rdx + 8 + 96]

				ADD eax, 640
		set_val3:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]









		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_val4
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val4
				SUB eax, 640
				MOV [rdx + 8], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8]
				MOV eax, [rdx + 8]
				ADD eax, 640
		set_val4:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_val5
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val5
				SUB eax, 640
				MOV [rdx + 8 + 32], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 32]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 32]
				MOV eax, [rdx + 8 + 32]
				ADD eax, 640
		set_val5:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_val6
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val6
				SUB eax, 640
				MOV [rdx + 8 + 64], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 64]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 64]
				MOV eax, [rdx + 8 + 64]
				ADD eax, 640
		set_val6:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_val7
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val7
				SUB eax, 640
				MOV [rdx + 8 + 96], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 96]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 96]
				MOV eax, [rdx + 8 + 96]
				ADD eax, 640
		set_val7:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]












		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_val8
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val8
				SUB eax, 640
				MOV [rdx + 8], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8]
				MOV eax, [rdx + 8]
				ADD eax, 640
		set_val8:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_val9
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val9
				SUB eax, 640
				MOV [rdx + 8 + 32], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 32]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 32]
				MOV eax, [rdx + 8 + 32]
				ADD eax, 640
		set_val9:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_val10
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val10
				SUB eax, 640
				MOV [rdx + 8 + 64], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 64]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 64]
				MOV eax, [rdx + 8 + 64]
				ADD eax, 640
		set_val10:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_val11
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val11
				SUB eax, 640
				MOV [rdx + 8 + 96], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 96]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 96]
				MOV eax, [rdx + 8 + 96]
				ADD eax, 640
		set_val11:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]










		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_val12
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val12
				SUB eax, 640
				MOV [rdx + 8], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8]
				MOV eax, [rdx + 8]
				ADD eax, 640
		set_val12:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_val13
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val13
				SUB eax, 640
				MOV [rdx + 8 + 32], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 32]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 32]
				MOV eax, [rdx + 8 + 32]
				ADD eax, 640
		set_val13:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_val14
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val14
				SUB eax, 640
				MOV [rdx + 8 + 64], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 64]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 64]
				MOV eax, [rdx + 8 + 64]
				ADD eax, 640
		set_val14:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_val15
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_val15
				SUB eax, 640
				MOV [rdx + 8 + 96], eax
			
				FLD st(0)
				FILD DWORD PTR [rdx + 8 + 96]
				FYL2XP1

				FISTTP DWORD PTR [rdx + 8 + 96]
				MOV eax, [rdx + 8 + 96]
				ADD eax, 640
		set_val15:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]


	DEC r10
	JNZ color_loop

	FSTP DWORD PTR [rdx - 24]

	JMP exit

align 16
	small_loop:

		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_sval0
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval0
				MOV eax, 640
		set_sval0:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_sval1
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval1
				MOV eax, 640
		set_sval1:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_sval2
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval2
				MOV eax, 640
		set_sval2:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_sval3
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval3
				MOV eax, 640
		set_sval3:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]









		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_sval4
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval4
				MOV eax, 640
		set_sval4:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_sval5
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval5
				MOV eax, 640
		set_sval5:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_sval6
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval6
				MOV eax, 640
		set_sval6:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_sval7
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval7
				MOV eax, 640
		set_sval7:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]











		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_sval8
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval8
				MOV eax, 640
		set_sval8:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_sval9
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval9
				MOV eax, 640
		set_sval9:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_sval10
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval10
				MOV eax, 640
		set_sval10:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_sval11
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval11
				MOV eax, 640
		set_sval11:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]









		XOR eax, eax
		
		CMP DWORD PTR [rdx], 0
		JZ set_sval12
			MOV eax, [rdx + 4]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval12
				MOV eax, 640
		set_sval12:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx], eax

		
		XOR eax, eax
		
		CMP DWORD PTR [rdx + 32], 0
		JZ set_sval13
			MOV eax, [rdx + 4 + 32]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval13
				MOV eax, 640
		set_sval13:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 4], eax

		XOR eax, eax
		
		CMP DWORD PTR [rdx + 64], 0
		JZ set_sval14
			MOV eax, [rdx + 4 + 64]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval14
				MOV eax, 640
		set_sval14:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 8], eax


		XOR eax, eax
		
		CMP DWORD PTR [rdx + 96], 0
		JZ set_sval15
			MOV eax, [rdx + 4 + 96]
			SUB eax, r8d
			CMP eax, 640
			JBE set_sval15
				MOV eax, 640
		set_sval15:
		
		MOV eax, [r11 + rax*4]
		MOVNTI [rcx + 12], eax

		LEA rcx, [rcx + 16]
		LEA rdx, [rdx + 128]


	DEC r10
	JNZ small_loop







exit:
	SFENCE


	RET
?VSColorMap@DefaultRenderer@Audio@@SAXPEAIPEA_J@Z	ENDP


END



