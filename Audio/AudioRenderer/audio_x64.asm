
; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE

.CONST

ALIGN 16
float_05	DWORD	3F000000h, 3F000000h, 3F000000h, 3F000000h
sign_bit	DWORD	80000000h, 80000000h, 80000000h, 80000000h

.CODE

ALIGN 16
?IDCT_II@Audio@@YAXPEAMPEBM_K02@Z	PROC
	MOVSS xmm13, DWORD PTR [rsp + 40]
	SHUFPS xmm13, xmm13, 0


	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	MOVAPS xmm15, XMMWORD PTR [float_05]
	MOVAPS xmm14, XMMWORD PTR [sign_bit]

	MOV rdi, r9
		

	MOV r10, 1
	MOV r9, 1

	MOV rbx, rcx
	LEA r11, [rbx + r8*2 - 16]
	
	XOR rax, rax
	

	SHR r8, 1	
CMP r8, 16
JBE loop_8

align 16
	scale_loop:
				MOVAPS xmm0, [rcx]			
				MOVAPS xmm1, [r11]								
				
				MOVAPS xmm5, [rcx + r8*2]
				NEG r8
				MOVAPS xmm4, [r11 + r8*2]
				
				SHUFPS xmm1, xmm1, 1Bh
				SHUFPS xmm5, xmm5, 1Bh

				MOVAPS xmm2, xmm0
				ADDPS xmm0, xmm1
				SUBPS xmm2, xmm1


				MOVAPS xmm6, xmm4
				ADDPS xmm4, xmm5
				SUBPS xmm6, xmm5


				MULPS xmm2, [rdx + rax]
				MULPS xmm6, [rdx + rax + 16]

				
				LEA r13, [r11 + r8*2]

				MOVAPS [rcx], xmm0
				MOVAPS [r13], xmm4
											
				NEG r8

				LEA r13, [rcx + r8*2]
				MOVAPS [r13], xmm2
				MOVAPS [r11], xmm6


				LEA rcx, [rcx + 16]
				LEA r11, [r11 - 16]

				ADD rax, 32
			CMP rax, r8
			JBE scale_loop

			LEA rcx, [rcx + r8*2]
			LEA rcx, [rcx + r8]

			LEA r11, [rcx + r8*4 - 16]

			XOR rax, rax

		DEC r9
		JNZ scale_loop

		SHL r10, 1
		MOV r9, r10		

		MOV rcx, rbx
		LEA r11, [rbx + r8*2 - 16]		

		LEA rdx, [rdx + r8*2]
		
		SHR r8, 1

	CMP r8, 8
	JA scale_loop




align 16
	loop_8:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]

		SHUFPS xmm1, xmm1, 1Bh

		MOVAPS xmm2, xmm0
		ADDPS xmm0, xmm1
		SUBPS xmm2, xmm1


		MULPS xmm2, [rdx]

		MOVDQA xmm1, xmm0
		SHUFPS xmm0, xmm0, 050h ; even
		SHUFPS xmm1, xmm1, 0AFh ; odd

		ADDSUBPS xmm0, xmm1 ; 0-3, 0+3, 1-2, 1+2

		MULPS xmm0, [rdx + 16]

		SHUFPS xmm0, xmm0, 8Dh; low - even high - odd
		MOVAPS xmm1, xmm0

		SHUFPS xmm1, xmm1, 0B1h

		ADDSUBPS xmm0, xmm1 ; 00-01, 01+00, 10-11, 11+10

		MULPS xmm0, [rdx + 32]

		SHUFPS xmm0, xmm0, 0B1h


		MOVDQA xmm1, xmm2
		SHUFPS xmm2, xmm2, 050h ; even
		SHUFPS xmm1, xmm1, 0AFh ; odd

		ADDSUBPS xmm2, xmm1

		MULPS xmm2, [rdx + 16]

		SHUFPS xmm2, xmm2, 8Dh; low - even high - odd
		MOVAPS xmm1, xmm2

		SHUFPS xmm1, xmm1, 0B1h

		ADDSUBPS xmm2, xmm1
		MULPS xmm2, [rdx + 32]

		SHUFPS xmm2, xmm2, 0B1h

;;;;;;


		MOVHLPS xmm1, xmm0
		MOVAPS xmm6, xmm1
		SHUFPS xmm6, xmm6, 1
		MULSS xmm1, xmm15
		SUBSS xmm6, xmm1
		UNPCKLPS xmm1, xmm6
		MOVLHPS xmm0, xmm1


		MOVHLPS xmm1, xmm2
		MOVAPS xmm6, xmm1
		SHUFPS xmm6, xmm6, 1
		MULSS xmm1, xmm15
		SUBSS xmm6, xmm1
		UNPCKLPS xmm1, xmm6
		MOVLHPS xmm2, xmm1



		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm2

		LEA rcx, [rcx + 32]

	DEC r9
	JNZ loop_8



	ADD rdx, 48

	MOV rcx, rbx

	MOV r8, 4

	MOV r11, r10

	

align 16
	unroll_loop:
				LEA rcx, [rcx + r8*4]
				MOV r9, rdx

				MOV r12d, [r9]
				MOV r13d, [r9 + 4]
				MOV r14d, [r9 + 8]
				MOV r15d, [r9 + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				MULSS xmm0, xmm15
				MOVSS DWORD PTR [rcx + r12], xmm0

				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS DWORD PTR [rcx + r13], xmm1

				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS DWORD PTR [rcx + r14], xmm2

				MOVSS xmm3, DWORD PTR [rcx + r15]
				SUBSS xmm3, xmm2
				MOVSS DWORD PTR [rcx + r15], xmm3			


				MOV rax, r8
				SUB rax, 4
				JZ no_spin

		align 16
			spin_loop:

				ADD r9, 16

				MOV r12d, [r9]
				MOV r13d, [r9 + 4]
				MOV r14d, [r9 + 8]
				MOV r15d, [r9 + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS DWORD PTR [rcx + r12], xmm0

				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS DWORD PTR [rcx + r13], xmm1

				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS DWORD PTR [rcx + r14], xmm2

				MOVSS xmm3, DWORD PTR [rcx + r15]
				SUBSS xmm3, xmm2
				MOVSS DWORD PTR [rcx + r15], xmm3
				

			SUB rax, 4
			JNZ spin_loop
		align 16
			no_spin:

				LEA rcx, [rcx + r8*4]
		DEC r11
		JNZ unroll_loop

		LEA rdx, [rdx + r8*4]

		MOV rcx, rbx

		SHL r8, 1

		SHR r10, 1
		MOV r11, r10		

	JNZ unroll_loop





; unpack






	MOV rcx, rbx

	MOV rax, r8
	SHR rax, 3 ; quater
	MOV r10, rax


	AND rdi, rdi
	JNZ explicit_out
		
		MOV rdi, rbx

	explicit_out:




	LEA rdi, [rdi + r8*4]
	LEA rdi, [rdi + r8*2] ; 3/4


	SUB rdx, rdi
	
	

align 16
	unpack_loop_3:
		MOV r12d, [rdx + rdi]
		MOV r13d, [rdx + rdi + 4]
		MOV r14d, [rdx + rdi + 8]
		MOV r15d, [rdx + rdi + 12]

		MOVSS xmm0, DWORD PTR [rcx + r12]
		MOVSS xmm1, DWORD PTR [rcx + r13]
		MOVSS xmm2, DWORD PTR [rcx + r14]
		MOVSS xmm3, DWORD PTR [rcx + r15]		


		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2

		MULPS xmm0, xmm13
		XORPS xmm0, xmm14

		MOVAPS [rdi], xmm0

		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_3


	LEA rdx, [rdx + r8*4]	
	NEG r8
	LEA rdi, [rdi + r8*4]
	

	MOV rax, r10


align 16
	unpack_loop_20:
		MOV r12d, [rdx + rdi]
		MOV r13d, [rdx + rdi + 4]
		MOV r14d, [rdx + rdi + 8]
		MOV r15d, [rdx + rdi + 12]

		MOVSS xmm0, DWORD PTR [rcx + r12]
		MOVSS xmm1, DWORD PTR [rcx + r13]
		MOVSS xmm2, DWORD PTR [rcx + r14]
		MOVSS xmm3, DWORD PTR [rcx + r15]		


		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2

		MULPS xmm0, xmm13

		MOVAPS [rdi], xmm0

		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_20


	MOV rax, r10
	SHR rax, 2
	
	LEA rdi, [rdi + r8*2]
	LEA rdi, [rdi + r8*4]

	NEG r8
	LEA r11, [r8*4]

	

align 16
	overwrite_loop:
		MOVDQA xmm0, [r11 + rdi]
		MOVDQA xmm1, [r11 + rdi + 16]
		MOVDQA xmm2, [r11 + rdi + 32]
		MOVDQA xmm3, [r11 + rdi + 48]

		MOVDQA [rdi], xmm0
		MOVDQA [rdi + 16], xmm1
		MOVDQA [rdi + 32], xmm2
		MOVDQA [rdi + 48], xmm3

		LEA rdi, [rdi + 64]

	DEC rax
	JNZ overwrite_loop
		





	MOV r9d, 80000000h

	MOV [rdi], eax

	LEA r11, [rdi - 4]
	LEA rdi, [rdi + 4]
	
	LEA rax, [r10*4 - 1]

align 16
	unpack_loop_1:

		MOV edx, [r11]
		XOR edx, r9d
		MOV [rdi], edx

		LEA rdi, [rdi + 4]
		LEA r11, [r11 - 4]

	DEC rax
	JNZ unpack_loop_1


	MOV edx, [r11]
	XOR edx, r9d
	MOV [rdi], edx


	LEA r11, [r11 + r8*8 - 4]
	LEA rdi, [rdi + 4]

	LEA rax, [r10*4 - 1]


align 16
	unpack_loop_2:

		MOV edx, [r11]		
		MOV [rdi], edx

		LEA rdi, [rdi + 4]
		LEA r11, [r11 - 4]

	DEC rax
	JNZ unpack_loop_2








	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx

	POP rdi



	RET
?IDCT_II@Audio@@YAXPEAMPEBM_K02@Z ENDP

ALIGN 16
?IDCT_IV@Audio@@YAXPEAMPEBM_K02@Z	PROC
	MOVSS xmm13, DWORD PTR [rsp + 40]
	SHUFPS xmm13, xmm13, 0


	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	MOVAPS xmm15, XMMWORD PTR [float_05]
	MOVAPS xmm14, XMMWORD PTR [sign_bit]


	MOV rdi, r9

	LEA rbx, [rcx + 16]
	MOV r10, r8

	; rcx - data pointer
	; rdx cos table
	; r8 - window size
		
	LEA r12, [rdx + r8*4]		
	LEA r11, [rcx + r8*8 - 32] ; last one
	

	SHL r8, 1

	XOR rax, rax


align 16
	first_step:

		MOVAPS xmm0, [rcx]
		MOVAPS xmm4, [rcx + 32]

		MOVAPS xmm1, [r11]
		MOVAPS xmm5, [r11 - 32]

		MULPS xmm0, [rdx + rax*2]
		MULPS xmm1, [rdx + rax*2 + 16]
		MULPS xmm4, [rdx + rax*2 + 32]
		MULPS xmm5, [rdx + rax*2 + 48]


		SHUFPS xmm1, xmm1, 1Bh
		SHUFPS xmm5, xmm5, 1Bh

		MOVAPS xmm2, xmm0
		MOVAPS xmm6, xmm4

		ADDPS xmm0, xmm1
		ADDPS xmm4, xmm5

		SUBPS xmm2, xmm1
		SUBPS xmm6, xmm5

		MULPS xmm2, [r12 + rax]
		MULPS xmm6, [r12 + rax + 16]

		MOVAPS [rcx + 16], xmm0
		MOVAPS [rcx + 32 + 16], xmm4

		MOVAPS [rcx + r8*2 + 16], xmm2 ; second half
		MOVAPS [rcx + r8*2 + 32 + 16], xmm6
					

		LEA rcx, [rcx + 64]
		LEA r11, [r11 - 64]

		ADD rax, 32
	CMP rax, r8
	JB first_step






; main cycle


	MOV r10, 2
	MOV r9, 2

	MOV rcx, rbx
	LEA r11, [rbx + r8*2 - 32]

	LEA rdx, [r12 + r8]

	XOR rax, rax

	SHR r8, 1
CMP r8, 16
JBE loop_8

align 16
	scale_loop:
				MOVAPS xmm0, [rcx]			
				MOVAPS xmm1, [r11]

				MOVAPS xmm5, [rcx + r8*2]
				NEG r8
				MOVAPS xmm4, [r11 + r8*2]
				
				SHUFPS xmm1, xmm1, 1Bh
				SHUFPS xmm5, xmm5, 1Bh

				MOVAPS xmm2, xmm0
				ADDPS xmm0, xmm1
				SUBPS xmm2, xmm1


				MOVAPS xmm6, xmm4
				ADDPS xmm4, xmm5
				SUBPS xmm6, xmm5

				MULPS xmm2, [rdx + rax]
				MULPS xmm6, [rdx + rax + 16]


				MOVAPS [rcx], xmm0
				MOVAPS [r11 + r8*2], xmm4
				NEG r8
				MOVAPS [rcx + r8*2], xmm2
				MOVAPS [r11], xmm6




				LEA rcx, [rcx + 32]
				LEA r11, [r11 - 32]

				ADD rax, 32
			CMP rax, r8
			JB scale_loop

			LEA rcx, [rcx + r8*2]
			LEA rcx, [rcx + r8]

			LEA r11, [rcx + r8*4 - 32]

			XOR rax, rax

		DEC r9
		JNZ scale_loop

		SHL r10, 1
		MOV r9, r10		

		MOV rcx, rbx
		LEA r11, [rbx + r8*2 - 32]		

		LEA rdx, [rdx + r8]
		
		SHR r8, 1

	CMP r8, 16
	JA scale_loop




align 16
	loop_8:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 32]

		SHUFPS xmm1, xmm1, 1Bh

		MOVAPS xmm2, xmm0
		ADDPS xmm0, xmm1
		SUBPS xmm2, xmm1

		MULPS xmm2, [rdx]

		MOVDQA xmm1, xmm0
		SHUFPS xmm0, xmm0, 050h ; even
		SHUFPS xmm1, xmm1, 0AFh ; odd

		ADDSUBPS xmm0, xmm1 ; 0-3, 0+3, 1-2, 1+2
		MULPS xmm0, [rdx + 16]

		SHUFPS xmm0, xmm0, 8Dh; low - even high - odd
		MOVAPS xmm1, xmm0

		SHUFPS xmm1, xmm1, 0B1h

		ADDSUBPS xmm0, xmm1 ; 00-01, 01+00, 10-11, 11+10
		MULPS xmm0, [rdx + 32]

		SHUFPS xmm0, xmm0, 0B1h
		


		MOVDQA xmm1, xmm2
		SHUFPS xmm2, xmm2, 050h ; even
		SHUFPS xmm1, xmm1, 0AFh ; odd

		ADDSUBPS xmm2, xmm1
		MULPS xmm2, [rdx + 16]

		SHUFPS xmm2, xmm2, 8Dh; low - even high - odd
		MOVAPS xmm1, xmm2

		SHUFPS xmm1, xmm1, 0B1h

		ADDSUBPS xmm2, xmm1
		MULPS xmm2, [rdx + 32]

		SHUFPS xmm2, xmm2, 0B1h
		




		MOVHLPS xmm1, xmm0
		MOVAPS xmm3, xmm1
		SHUFPS xmm3, xmm3, 1
		MULSS xmm1, xmm15
		SUBSS xmm3, xmm1
		UNPCKLPS xmm1, xmm3
		MOVLHPS xmm0, xmm1


		MOVHLPS xmm1, xmm2
		MOVAPS xmm3, xmm1
		SHUFPS xmm3, xmm3, 1
		MULSS xmm1, xmm15
		SUBSS xmm3, xmm1
		UNPCKLPS xmm1, xmm3
		MOVLHPS xmm2, xmm1


		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 32], xmm2

		LEA rcx, [rcx + 64]

	DEC r9
	JNZ loop_8

	ADD rdx, 48




	MOV rcx, rbx

	MOV r8, 4

	MOV r11, r10

	

align 16
	unroll_loop:
				LEA rcx, [rcx + r8*8]
				MOV r9, rdx

				MOV r12d, [r9]
				MOV r13d, [r9 + 4]
				MOV r14d, [r9 + 8]
				MOV r15d, [r9 + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				MULSS xmm0, xmm15
				MOVSS DWORD PTR [rcx + r12], xmm0

				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS DWORD PTR [rcx + r13], xmm1

				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS DWORD PTR [rcx + r14], xmm2

				MOVSS xmm3, DWORD PTR [rcx + r15]
				SUBSS xmm3, xmm2
				MOVSS DWORD PTR [rcx + r15], xmm3


				
				


				MOV rax, r8
				SUB rax, 4
				JZ no_spin

		align 16
			spin_loop:

				ADD r9, 16

				MOV r12d, [r9]
				MOV r13d, [r9 + 4]
				MOV r14d, [r9 + 8]
				MOV r15d, [r9 + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS DWORD PTR [rcx + r12], xmm0

				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS DWORD PTR [rcx + r13], xmm1

				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS DWORD PTR [rcx + r14], xmm2

				MOVSS xmm3, DWORD PTR [rcx + r15]
				SUBSS xmm3, xmm2
				MOVSS DWORD PTR [rcx + r15], xmm3


				

			SUB rax, 4
			JNZ spin_loop
		align 16
			no_spin:

				LEA rcx, [rcx + r8*8]
		DEC r11
		JNZ unroll_loop

		LEA rdx, [rdx + r8*4]

		MOV rcx, rbx

		SHL r8, 1

		SHR r10, 1
		MOV r11, r10		

	JNZ unroll_loop















	MOV rcx, rbx

	MOV rax, r8
	MOV r10, r8
	SHR rax, 3 ; quater

	MOVSS xmm1, DWORD PTR [rcx]
	MULSS xmm1, xmm15
	MOVSS DWORD PTR [rcx], xmm1

	XORPS xmm3, xmm3

	AND rdi, rdi
	JNZ explicit_out
	
		LEA rdi, [rbx + r8*8 - 16]

	explicit_out:



	MOV r11d, DWORD PTR [rsp + 92]

	CMP r11d, 0
	JNZ map_1





		LEA rdi, [rdi + r8*4]
		LEA rdi, [rdi + r8*2] ; 3/4



		SUB rdx, rdi
	
	

align 16
	unpack_loop_30:
		MOV r12d, [rdx + rdi]
		MOV r13d, [rdx + rdi + 4]
		MOV r14d, [rdx + rdi + 8]
		MOV r15d, [rdx + rdi + 12]

		MOVSS xmm0, DWORD PTR [rcx + r12]
		SUBSS xmm0, xmm3
		MOVSS xmm1, DWORD PTR [rcx + r13]
		SUBSS xmm1, xmm0
		MOVSS xmm2, DWORD PTR [rcx + r14]
		SUBSS xmm2, xmm1
		MOVSS xmm3, DWORD PTR [rcx + r15]		
		SUBSS xmm3, xmm2



		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2

		XORPS xmm0, xmm14
		MULPS xmm0, xmm13

		MOVAPS [rdi], xmm0

		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_30


	MOV rax, r10
	SHR rax, 3

	NEG r10
	LEA rdi, [rdi + r10*8]
	NEG r10
	LEA rdx, [rdx + r10*8]
	

align 16
	unpack_loop_00:
		MOV r12d, [rdx + rdi]
		MOV r13d, [rdx + rdi + 4]
		MOV r14d, [rdx + rdi + 8]
		MOV r15d, [rdx + rdi + 12]

		MOVSS xmm0, DWORD PTR [rcx + r12]
		SUBSS xmm0, xmm3
		MOVSS xmm1, DWORD PTR [rcx + r13]
		SUBSS xmm1, xmm0
		MOVSS xmm2, DWORD PTR [rcx + r14]
		SUBSS xmm2, xmm1
		MOVSS xmm3, DWORD PTR [rcx + r15]		
		SUBSS xmm3, xmm2


		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2
		MULPS xmm0, xmm13
		
		MOVAPS [rdi], xmm0

		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_00



	LEA r11, [rdi - 16]
	
	MOV rax, r10
	SHR rax, 3


align 16
	unpack_loop_10:
		MOVAPS xmm0, [r11]
		XORPS xmm0, xmm14
		SHUFPS xmm0, xmm0, 1Bh
		
		MOVAPS [rdi], xmm0

		SUB r11, 16
		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_10



	LEA r11, [r11 + r10*8]
	
	MOV rax, r10
	SHR rax, 3


align 16
	unpack_loop_20:
		MOVAPS xmm0, [r11]
		SHUFPS xmm0, xmm0, 1Bh
		
		MOVAPS [rdi], xmm0

		SUB r11, 16
		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_20



	JMP exit


align 16
	map_1:
		CMP r11d, 1
		JNZ map_2

			SUB rdx, rdi


		align 16
			unpack_loop_01:
				MOV r12d, [rdx + rdi]
				MOV r13d, [rdx + rdi + 4]
				MOV r14d, [rdx + rdi + 8]
				MOV r15d, [rdx + rdi + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS xmm3, DWORD PTR [rcx + r15]				
				SUBSS xmm3, xmm2


				UNPCKLPS xmm0, xmm1
				UNPCKLPS xmm2, xmm3
				MOVLHPS xmm0, xmm2

				XORPS xmm0, xmm14
				MULPS xmm0, xmm13

				MOVAPS [rdi], xmm0

				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_01


			MOV rax, r10
			SHR rax, 3


		align 16
			unpack_loop_11:
				MOV r12d, [rdx + rdi]
				MOV r13d, [rdx + rdi + 4]
				MOV r14d, [rdx + rdi + 8]
				MOV r15d, [rdx + rdi + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS xmm3, DWORD PTR [rcx + r15]				
				SUBSS xmm3, xmm2


				UNPCKLPS xmm0, xmm1
				UNPCKLPS xmm2, xmm3
				MOVLHPS xmm0, xmm2
				MULPS xmm0, xmm13
		
				MOVAPS [rdi], xmm0

				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_11



			LEA r11, [rdi - 16]
				
			MOV rax, r10
			SHR rax, 3


		align 16
			unpack_loop_21:
				MOVAPS xmm0, [r11]
				XORPS xmm0, xmm14
				SHUFPS xmm0, xmm0, 1Bh
		
				MOVAPS [rdi], xmm0

				SUB r11, 16
				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_21

			
	
			MOV rax, r10
			SHR rax, 3


		align 16
			unpack_loop_31:
				MOVAPS xmm0, [r11]				
				SHUFPS xmm0, xmm0, 1Bh
		
				MOVAPS [rdi], xmm0

				SUB r11, 16
				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_31






		JMP exit
align 16
	map_2:


		LEA rdi, [rdi + r8*4]

		SUB rdx, rdi	

		align 16
			unpack_loop_22:
				MOV r12d, [rdx + rdi]
				MOV r13d, [rdx + rdi + 4]
				MOV r14d, [rdx + rdi + 8]
				MOV r15d, [rdx + rdi + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS xmm3, DWORD PTR [rcx + r15]				
				SUBSS xmm3, xmm2


				UNPCKLPS xmm0, xmm1
				UNPCKLPS xmm2, xmm3
				MOVLHPS xmm0, xmm2

				XORPS xmm0, xmm14
				MULPS xmm0, xmm13

				MOVAPS [rdi], xmm0

				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_22


			MOV rax, r10
			SHR rax, 3

	

		align 16
			unpack_loop_32:
				MOV r12d, [rdx + rdi]
				MOV r13d, [rdx + rdi + 4]
				MOV r14d, [rdx + rdi + 8]
				MOV r15d, [rdx + rdi + 12]

				MOVSS xmm0, DWORD PTR [rcx + r12]
				SUBSS xmm0, xmm3
				MOVSS xmm1, DWORD PTR [rcx + r13]
				SUBSS xmm1, xmm0
				MOVSS xmm2, DWORD PTR [rcx + r14]
				SUBSS xmm2, xmm1
				MOVSS xmm3, DWORD PTR [rcx + r15]				
				SUBSS xmm3, xmm2


				UNPCKLPS xmm0, xmm1
				UNPCKLPS xmm2, xmm3
				MOVLHPS xmm0, xmm2
				MULPS xmm0, xmm13
		
				MOVAPS [rdi], xmm0

				ADD rdi, 16

			DEC rax
			JNZ unpack_loop_32



	LEA r11, [rdi - 16]

	NEG r8
	LEA rdi, [rdi + r8*8]

	
	MOV rax, r10
	SHR rax, 3


align 16
	unpack_loop_02:
		MOVAPS xmm0, [r11]
		XORPS xmm0, xmm14
		SHUFPS xmm0, xmm0, 1Bh
		
		MOVAPS [rdi], xmm0

		SUB r11, 16
		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_02



	MOV rax, r10
	SHR rax, 3


align 16
	unpack_loop_12:
		MOVAPS xmm0, [r11]
		SHUFPS xmm0, xmm0, 1Bh
		
		MOVAPS [rdi], xmm0

		SUB r11, 16
		ADD rdi, 16

	DEC rax
	JNZ unpack_loop_12



exit:

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx

	POP rdi

	RET
?IDCT_IV@Audio@@YAXPEAMPEBM_K02@Z	ENDP


ALIGN 16
?FormatWindow@Audio@@YAXPEAM_KPEBMI@Z	PROC

	LEA rdx, [rcx + rdx*4]
	LEA rdx, [rdx + r9*4]

	SHR r9d, 4
	MOV r10d, r9d

	SUB r8, rcx
	SUB rdx, rcx

align 16
	left_loop:

		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]

		MULPS xmm0, [r8 + rcx]
		MULPS xmm1, [r8 + rcx + 16]
		MULPS xmm2, [r8 + rcx + 32]
		MULPS xmm3, [r8 + rcx + 48]

		ADDPS xmm0, [rdx + rcx]
		ADDPS xmm1, [rdx + rcx + 16]
		ADDPS xmm2, [rdx + rcx + 32]
		ADDPS xmm3, [rdx + rcx + 48]

		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm1
		MOVAPS [rcx + 32], xmm2
		MOVAPS [rcx + 48], xmm3

		ADD rcx, 64		



	DEC r10d
	JNZ left_loop


align 16
	right_loop:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]

		MULPS xmm0, [r8 + rcx]
		MULPS xmm1, [r8 + rcx + 16]
		MULPS xmm2, [r8 + rcx + 32]
		MULPS xmm3, [r8 + rcx + 48]

		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm1
		MOVAPS [rcx + 32], xmm2
		MOVAPS [rcx + 48], xmm3

		ADD rcx, 64

	DEC r9d
	JNZ right_loop



	RET
?FormatWindow@Audio@@YAXPEAM_KPEBMI@Z	ENDP


ALIGN 16
?Rescale@Audio@@YAXPEAMI@Z	PROC	

		MOVAPS xmm0, [rcx]
		MULPS xmm0, [rcx+16]

		MOVAPS xmm1, [rcx+32]
		MULPS xmm1, [rcx+48]

		MOVAPS xmm2, [rcx+64]
		MULPS xmm2, [rcx+80]

		MOVAPS xmm3, [rcx+96]
		MULPS xmm3, [rcx+112]

		MOVAPS xmm4, [rcx+128]
		MULPS xmm4, [rcx+144]

		MOVAPS xmm5, [rcx+160]
		MULPS xmm5, [rcx+176]

		MOVAPS xmm6, [rcx+192]
		MULPS xmm6, [rcx+208]

		MOVAPS xmm7, [rcx+224]
		MULPS xmm7, [rcx+240]


		MOVAPS [rcx], xmm0
		MOVAPS [rcx+16], xmm0
		MOVAPS [rcx+32], xmm1
		MOVAPS [rcx+48], xmm1
		MOVAPS [rcx+64], xmm2
		MOVAPS [rcx+80], xmm2
		MOVAPS [rcx+96], xmm3
		MOVAPS [rcx+112], xmm3
		MOVAPS [rcx+128], xmm4
		MOVAPS [rcx+144], xmm4
		MOVAPS [rcx+160], xmm5
		MOVAPS [rcx+176], xmm5
		MOVAPS [rcx+192], xmm6
		MOVAPS [rcx+208], xmm6
		MOVAPS [rcx+224], xmm7
		MOVAPS [rcx+240], xmm7


		ADD rcx, 256
	DEC edx
	JNZ ?Rescale@Audio@@YAXPEAMI@Z


	RET
?Rescale@Audio@@YAXPEAMI@Z	ENDP


ALIGN 16
?Deinterleave@Audio@@YAXPEAMI@Z	PROC
	XOR rax, rax
	SHL edx, 3

align 16
	de_loop1:	
		MOV r8d, [rcx + rax*2]
		MOV r9d, [rcx + rax*2 + 8]
		MOV r10d, [rcx + rax*2 + 32]
		MOV r11d, [rcx + rax*2 + 40]

		MOV [rcx + rax], r8d
		MOV [rcx + rax + 4], r9d
		MOV [rcx + rax + 8], r10d
		MOV [rcx + rax + 12], r11d

		ADD rax, 32

	CMP rax, rdx
	JB de_loop1


	LEA rdx, [rcx + rdx]
	SUB rax, 32

align 16
	de_loop2:
		MOV r8d, [rcx + rax*2 + 20]
		MOV r9d, [rcx + rax*2 + 28]
		MOV r10d, [rcx + rax*2 + 52]
		MOV r11d, [rcx + rax*2 + 60]

		MOV [rdx + rax + 12], r11d
		MOV [rdx + rax + 8], r10d
		MOV [rdx + rax + 4], r9d
		MOV [rdx + rax], r8d		

	SUB rax, 32
	JGE de_loop2

	RET
?Deinterleave@Audio@@YAXPEAMI@Z	ENDP


ALIGN 16
?AddChannel@Audio@@YAXPEAMPEBMIM@Z	PROC
	SUB rdx, rcx
	SHR r8d, 4

	SHUFPS xmm3, xmm3, 0	

align 16
	calc_loop:
		MOVAPS xmm4, [rcx + rdx]
		MOVAPS xmm5, [rcx + rdx + 16]
		MOVAPS xmm6, [rcx + rdx + 32]
		MOVAPS xmm7, [rcx + rdx + 48]

		MULPS xmm4, xmm3
		MULPS xmm5, xmm3
		MULPS xmm6, xmm3
		MULPS xmm7, xmm3
		
		ADDPS xmm4, [rcx]
		ADDPS xmm5, [rcx + 16]
		ADDPS xmm6, [rcx + 32]
		ADDPS xmm7, [rcx + 48]

		MOVAPS [rcx], xmm4
		MOVAPS [rcx + 16], xmm5
		MOVAPS [rcx + 32], xmm6
		MOVAPS [rcx + 48], xmm7

		LEA rcx, [rcx + 64]

	DEC r8d
	JNZ calc_loop

	RET
?AddChannel@Audio@@YAXPEAMPEBMIM@Z	ENDP

END

