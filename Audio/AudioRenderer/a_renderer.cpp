/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\AudioRenderer.h"
#include "System\SysUtils.h"

#include <Mmdeviceapi.h>
#include <Audioclient.h>
#include <devicetopology.h>

#include "Audio\AAC.h"
#include "Audio\AC3.h"
#include "Audio\MP3.h"
#include "Audio\DTS.h"

#include "Math\CalcSpecial.h"

#pragma warning(disable:4244)




// #include <cstdio>


#define SYNC_DELTA_VAL		600



const GUID CLSID_MMDeviceEnumerator = {0xBCDE0395, 0xE52F, 0x467C, {0x8E, 0x3D, 0xC4, 0x57, 0x92, 0x91, 0x69, 0x2E}}; //__uuidof(MMDeviceEnumerator);
const GUID IID_IMMDeviceEnumerator = {0xA95664D2, 0x9614, 0x4F35, {0xA7, 0x46, 0xDE, 0x8D, 0xB6, 0x36, 0x17, 0xE6}}; // __uuidof(IMMDeviceEnumerator);
const GUID IID_IAudioClient = {0x1CB9AD4C, 0xDBFA, 0x4c32, {0xB1, 0x78, 0xC2, 0xF5, 0x68, 0xA7, 0x03, 0xB2}}; // __uuidof(IAudioClient);
const GUID IID_IAudioRenderClient = {0xF294ACFC, 0x3146, 0x4483, {0xA7, 0xBF, 0xAD, 0xDC, 0xA7, 0xC2, 0x60, 0xE2}}; // __uuidof(IAudioRenderClient);
const GUID IID_ISimpleAudioVolume = {0x87CE5498, 0x68D6, 0x44E5, {0x92, 0x15, 0x6D, 0xA4, 0x7E, 0xF8, 0x83, 0xD8}}; // __uuidof(ISimpleAudioVolume);
const GUID IID_IAudioClockAdjustment = {0xF6E4C0A0, 0x46D9, 0x4FB8, {0xBE, 0x21, 0x57, 0xA3, 0xEF, 0x2B, 0x62, 0x6C}}; // __uuidof(IAudioClockAdjustment);
const GUID IID_IAudioChannelConfig = {0xBB11C46F, 0xEC28, 0x493C, {0xB8, 0x8A, 0x5D, 0xB8, 0x80, 0x62, 0xCE, 0x98}};

SFSco::Object Audio::DefaultRenderer::view_buf;

UI_64 Audio::DefaultRenderer::loop_counter = 0;


unsigned int Audio::DefaultRenderer::StreamData::view_on_default = 3;

const UI_64 Audio::DefaultRenderer::volume_set[] = {
	20,
	0x3F80000000000000, 0x3F86A09E667F3BCD, 0x3F90000000000000, 0x3F96A09E667F3BCD, 0x3FA0000000000000, 0x3FA6A09E667F3BCD, 0x03FB0000000000000, 0x3FB6A09E667F3BCD,
	0x3FC0000000000000, 0x3FC6A09E667F3BCD, 0x3FD0000000000000, 0x3FD6A09E667F3BCD, 0x3FE0000000000000, 0x3FE6A09E667F3BCD, 0x3FF0000000000000,
	0x3FF6A09E667F3BCD, 0x4000000000000000, 0x4006A09E667F3BCD, 0x4010000000000000, 0x4016A09E667F3BCD, 0x4020000000000000
};


/*
#define SPEAKER_FRONT_LEFT              0x1
#define SPEAKER_FRONT_RIGHT             0x2
#define SPEAKER_FRONT_CENTER            0x4
#define SPEAKER_LOW_FREQUENCY           0x8

#define SPEAKER_BACK_LEFT               0x10
#define SPEAKER_BACK_RIGHT              0x20
#define SPEAKER_FRONT_LEFT_OF_CENTER    0x40
#define SPEAKER_FRONT_RIGHT_OF_CENTER   0x80

#define SPEAKER_BACK_CENTER             0x100
#define SPEAKER_SIDE_LEFT               0x200
#define SPEAKER_SIDE_RIGHT              0x400
#define SPEAKER_TOP_CENTER              0x800

#define SPEAKER_TOP_FRONT_LEFT          0x1000
#define SPEAKER_TOP_FRONT_CENTER        0x2000
#define SPEAKER_TOP_FRONT_RIGHT         0x4000
#define SPEAKER_TOP_BACK_LEFT           0x8000


*/

const float Audio::DefaultRenderer::dmix_matrix[16][16] = {
	{ 1.000000f, 0.000000f, 0.707107f, 0.000000f,	0.353553f, 0.000000f, 0.000000f, 0.000000f,		0.250000f, 0.500000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 1.000000f, 0.707107f, 0.000000f,	0.000000f, 0.353553f, 0.000000f, 0.000000f,		0.250000f, 0.000000f, 0.500000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.707107f, 0.707107f, 1.000000f, 0.000000f,	0.250000f, 0.250000f, 0.000000f, 0.000000f,		0.125000f, 0.500000f, 0.500000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 1.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },

	{ 0.353553f, 0.000000f, 0.250000f, 0.000000f,	1.000000f, 0.000000f, 0.000000f, 0.000000f,		0.707107f, 0.500000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.353553f, 0.250000f, 0.000000f,	0.000000f, 1.000000f, 0.000000f, 0.000000f,		0.707107f, 0.000000f, 0.500000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },

	{ 0.125000f, 0.250000f, 0.250000f, 0.000000f,	0.707107f, 0.707107f, 0.000000f, 0.000000f,		1.000000f, 0.500000f, 0.500000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.500000f, 0.000000f, 0.000000f, 0.000000f,	0.500000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 1.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.500000f, 0.000000f, 0.000000f,	0.000000f, 0.500000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 1.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },

	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f },
	{ 0.000000f, 0.000000f, 0.000000f, 0.000000f,	0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f,		0.000000f, 0.000000f, 0.000000f, 0.000000f }

};

__declspec(align(16)) float Audio::DefaultRenderer::ls_tab[8192];
__declspec(align(16)) unsigned int Audio::DefaultRenderer::vs_color_table[2048];












DWORD __stdcall Audio::DefaultRenderer::MixerLoop(void * cntx) {	
	float * wave_ptr(0), * frame_ptr(0), * pcm_pointer(0);
	FrameCfg * frame_cfg(0);
	
	StreamData * stream_cfg(reinterpret_cast<StreamData *>(cntx));

	WAVEFORMATEXTENSIBLE * mix_format(0), src_a_format;

	IMMDeviceEnumerator * i_numerator(0);
	IMMDevice * a_device(0);
	IAudioClient * a_client(0);
	IAudioRenderClient * r_client(0);
	IAudioClockAdjustment * a_adjust(0);


	UI_64 render_buffer_size(40000000), requested_frame_count(0), render_frame_count(0);
	__int64 e_period(0), min_eperiod(0), temp_vali(0), v_dir(0), h_dir(0), local_time_0(0), load_timemark(0), current_timemark(-1);
	
	I_64 pool_index[2*MAX_POOL_COUNT];

	unsigned int silent_channel[MAX_CHANNEL_COUNT];	
	unsigned int result(0), buffer_size(0), buffer_padding(0), last_source_frame(0), current_source_frame(-1), type_float(0), channel_mask(0), restart_flag(1), chan_dur(0), src_mask(0), mix_sign(0), i_val(0), i_val1(0);


	__declspec(align(16)) unsigned int last_sample[MAX_CHANNEL_COUNT*256];
	volatile float mix_coffs[20];

	mix_coffs[17] = 0.0f;
	mix_coffs[18] = 1.0f;

	
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // THREAD_PRIORITY_ABOVE_NORMAL THREAD_PRIORITY_TIME_CRITICAL

	Memory::Chain::RegisterThread(pool_index);

	System::MemoryFill_SSE3(last_sample, MAX_CHANNEL_COUNT*256*sizeof(float), 0, 0);

	if (CoInitializeEx(0, COINIT_MULTITHREADED) == S_OK)
	if (HANDLE block_obj = CreateEvent(0, 0, 0, 0)) {
		if (CoCreateInstance(CLSID_MMDeviceEnumerator, 0, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&i_numerator) == S_OK) {
			if (i_numerator->GetDefaultAudioEndpoint(eRender, eMultimedia, &a_device) == S_OK) {

				if (a_device->Activate(IID_IAudioClient, CLSCTX_ALL, 0, (void **)&a_client) == S_OK) {
					a_client->GetDevicePeriod(&e_period, &min_eperiod);
					a_client->GetMixFormat((WAVEFORMATEX **)&mix_format);

					if (mix_format) {
						

						if (mix_format->Format.wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
							System::MemoryCopy(&src_a_format, mix_format, sizeof(WAVEFORMATEXTENSIBLE));
							
							channel_mask = mix_format->dwChannelMask;
							if (mix_format->SubFormat == KSDATAFORMAT_SUBTYPE_IEEE_FLOAT) {
								type_float = 1;
							} else {
								if (mix_format->SubFormat == KSDATAFORMAT_SUBTYPE_PCM) {
									type_float = 2;
								}
							}

						} else {
							System::MemoryCopy(&src_a_format, mix_format, sizeof(WAVEFORMATEX));

							channel_mask = (1 << mix_format->Format.nChannels) - 1;
							if (mix_format->Format.wFormatTag == WAVE_FORMAT_PCM) {
								type_float = 2;								
							}
						}

						CoTaskMemFree(mix_format);



						if (type_float == 1)
						if (a_client->Initialize(AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_RATEADJUST, render_buffer_size, 0, &src_a_format.Format, 0) == S_OK) {

							if ((a_client->GetService(IID_IAudioRenderClient, (void **)&r_client) == S_OK) && (a_client->GetService(IID_IAudioClockAdjustment, (void **)&a_adjust) == S_OK)) {
								a_client->GetBufferSize(&buffer_size);
								
								SetEvent(stream_cfg->ini_block);

								for (;stream_cfg->terminator;) {
									WaitForSingleObject(block_obj, 23);

									if (stream_cfg->view_on & 1) {
										if (I_64 * vb_cfg = reinterpret_cast<I_64 *>(view_buf.Acquire())) {
											if (vb_cfg[15]) {
												vb_cfg[15] = 0;
												
												if (loop_counter++ < 320) {
													CalculateVS(vb_cfg);
												} else {
													ShakeVS(vb_cfg);
												}

												vb_cfg[15] = 1;
											}

											view_buf.Release();
										}
									}

									if ((int)stream_cfg->flush_sign < 0) {
										continue;
									}

									a_client->GetCurrentPadding(&buffer_padding);									
									current_timemark = stream_cfg->current_timemark;


									if ((stream_cfg->reset_flag == 0) && (buffer_padding)) {
										if (current_timemark >= 0) {
											current_timemark += System::GetUCount(local_time_0, 0);
										}

									} else {
										if (stream_cfg->reset_flag) last_source_frame = stream_cfg->drop_frame + 1;
										stream_cfg->reset_flag = 0;
										
										if (buffer_padding) {
											a_client->Stop();
											a_client->Reset();
																											
											restart_flag = 1;
										}
										
										local_time_0 = System::GetTime();

										load_timemark = stream_cfg->load_timemark;
										if (current_timemark >= 0) {
											load_timemark += SYNC_DELTA_VAL;
										}
									}

									
									
									current_source_frame = InterlockedExchange((long *)&stream_cfg->flush_sign, stream_cfg->time_shift);
									stream_cfg->time_shift = -1;


									last_source_frame &= (MAX_FRAME_COUNT - 1);
									if (current_source_frame < last_source_frame) current_source_frame += MAX_FRAME_COUNT;
									
																		
									for (unsigned int i(0);i<MAX_CHANNEL_COUNT;i++) silent_channel[i] = 1;

																	
									requested_frame_count = 0;

									if (wave_ptr = reinterpret_cast<float *>(stream_cfg->frame_buff.Acquire())) {


										requested_frame_count = 0;
										for (;last_source_frame <= current_source_frame;last_source_frame++) {

											frame_ptr = wave_ptr + (last_source_frame & (MAX_FRAME_COUNT - 1))*stream_cfg->frame_size;
											frame_cfg = reinterpret_cast<FrameCfg *>(frame_ptr);

											if (frame_cfg->valid_sign == 0) continue;

											frame_ptr += stream_cfg->sdata_offset;

											frame_cfg->target_frequency = src_a_format.Format.nSamplesPerSec;
											frame_cfg->target_chan_count = src_a_format.Format.nChannels;
											frame_cfg->target_bit_depth = src_a_format.Format.wBitsPerSample;

											frame_cfg->volume_val = stream_cfg->volume_val;
													
											frame_cfg->tch_idx = 0;

											src_mask = 0;
											for (unsigned int i(0);i<frame_cfg->source_chan_count;i++) {
												src_mask |= frame_cfg->chan_type[i];
											}

											mix_sign = ((src_mask & channel_mask) ^ src_mask);

											if (current_timemark < load_timemark) {
													
												if (frame_cfg->source_frequency != src_a_format.Format.nSamplesPerSec) {
												// it is global, so probably wait until buffer is played completly before change
													if (a_adjust->SetSampleRate(frame_cfg->source_frequency) == S_OK) src_a_format.Format.nSamplesPerSec = frame_cfg->source_frequency;
												}


												if (load_timemark > (current_timemark + 3*SYNC_DELTA_VAL)) {
													requested_frame_count = src_a_format.Format.nSamplesPerSec;
													requested_frame_count *= (load_timemark - current_timemark - 2*SYNC_DELTA_VAL);
													requested_frame_count /= 10000;

													if (r_client->GetBuffer(requested_frame_count, (unsigned char **)&pcm_pointer) == S_OK) {
														r_client->ReleaseBuffer(requested_frame_count, AUDCLNT_BUFFERFLAGS_SILENT);
															
														load_timemark = current_timemark + 2*SYNC_DELTA_VAL;
													}
												}



												requested_frame_count = frame_cfg->window_count*frame_cfg->window_sample_count;

												if (frame_cfg->source_frequency == src_a_format.Format.nSamplesPerSec) {
													a_client->GetCurrentPadding(&buffer_padding);
													if (requested_frame_count > (buffer_size - buffer_padding)) {
														result = 'fuck';
													}


													if (r_client->GetBuffer(requested_frame_count, (unsigned char **)&pcm_pointer) == S_OK) {
		
														if (mix_sign) {
															System::MemoryFill_SSE3(pcm_pointer, requested_frame_count*src_a_format.Format.nBlockAlign, 0, 0);
															
															for (unsigned int channel_selector(1);channel_selector < channel_mask;channel_selector<<=1) {
																if ((channel_selector & channel_mask) == 0) continue;

															// configure matrix
																i_val = System::BitIndexHigh_32(channel_selector);
																for (unsigned int j(0);j<16;j++) {
																	mix_coffs[j] = dmix_matrix[i_val][j];
																}

																mix_coffs[19] = mix_coffs[17];
																i_val = (mix_sign | channel_selector) ^ src_mask;
																i_val1 = src_mask;

																for (unsigned int j(0);i_val1;i_val>>=1, i_val1>>=1, j++) {
																	if (i_val & 1) {
																		mix_coffs[j] = mix_coffs[17];
																	} else {
																		mix_coffs[19] += mix_coffs[j]*mix_coffs[j];
																	}
																}

																mix_coffs[19] = mix_coffs[18]/Calc::FloatSqrt(mix_coffs[19]);
																
																for (unsigned int j(0);j<16;j++) mix_coffs[j] *= mix_coffs[19];
																
																i_val = (mix_sign | channel_selector);

																for (unsigned int ch_i(0);ch_i<frame_cfg->source_chan_count;ch_i++) {
																	if (frame_cfg->chan_type[ch_i] & i_val) {
																		i_val1 = System::BitIndexHigh_32(frame_cfg->chan_type[ch_i]);
																		if (mix_coffs[i_val1]) result = MixUnpackF(pcm_pointer, frame_ptr + frame_cfg->channel_offset*ch_i, &frame_cfg->source_frequency, mix_coffs[i_val1]);
																	}
																}

																silent_channel[frame_cfg->tch_idx++] = 0;																
															}

														} else {
															for (unsigned int channel_selector(1);channel_selector < channel_mask;channel_selector <<= 1) {
																if ((channel_mask & channel_selector) == 0) continue;

																for (unsigned int ch_i(0);ch_i<frame_cfg->source_chan_count;ch_i++) {
																	if (frame_cfg->chan_type[ch_i] == channel_selector) {
																		result = UnpackChannelF(pcm_pointer, frame_ptr + frame_cfg->channel_offset*ch_i, &frame_cfg->source_frequency);
										
																		silent_channel[frame_cfg->tch_idx] = 0;
																		break;
																	}
																}

																frame_cfg->tch_idx++;
															}
														}
															
														r_client->ReleaseBuffer(requested_frame_count, 0);														
													}
												} else { // do old fasion
													if (frame_cfg->source_frequency < src_a_format.Format.nSamplesPerSec) {

														// recalculate requested
														requested_frame_count = frame_cfg->valid_sign*frame_cfg->target_frequency;
														requested_frame_count /= 10000;
															
														a_client->GetCurrentPadding(&buffer_padding);
															
														if (requested_frame_count > (buffer_size - buffer_padding)) {
															result = 'fuck';
														}

														buffer_padding = requested_frame_count + render_frame_count;
														if (r_client->GetBuffer(requested_frame_count, (unsigned char **)&pcm_pointer) == S_OK) {
/*
																if (render_frame_count) {
																	System::MemoryCopy(pcm_pointer, last_sample + MAX_CHANNEL_COUNT*16, render_frame_count*frame_cfg->target_chan_count*sizeof(float));
																	pcm_pointer += render_frame_count*frame_cfg->target_chan_count;
																}
*/
															render_frame_count = 0;
															chan_dur = -1;
		
															for (unsigned int channel_selector(1);channel_selector < channel_mask;channel_selector <<= 1) {
																if ((channel_mask & channel_selector) == 0) continue;

																for (unsigned int ch_i(0);ch_i<frame_cfg->source_chan_count;ch_i++) {

																	if (frame_cfg->chan_type[ch_i] == channel_selector) {																			
																		result = IUnpackChannelF(pcm_pointer, frame_ptr + frame_cfg->channel_offset*ch_i, &frame_cfg->source_frequency, last_sample + frame_cfg->tch_idx*16);

																		if (result < chan_dur) chan_dur = result;
									
																		silent_channel[frame_cfg->tch_idx] = 0;

																		break;
																	}
																}

																frame_cfg->tch_idx++;
															}

															r_client->ReleaseBuffer(requested_frame_count, 0);														
														}
													}
												}


											} else {
												load_timemark += frame_cfg->valid_sign;

											}

											frame_cfg->valid_sign = 0;
										}


												
												
										if (restart_flag) a_client->Start();
										restart_flag = 0;

										stream_cfg->frame_buff.Release();
									}

									if (requested_frame_count == 0)
										if (current_source_frame >= last_source_frame) last_source_frame = current_source_frame + 1;

								}

								a_client->Stop();								

								a_adjust->Release();
								r_client->Release();
							}							
						}						
					}

					a_client->Release();
				}

				a_device->Release();
			}

			i_numerator->Release();
		}

		CloseHandle(block_obj);
	}

	CoUninitialize();

	Memory::Chain::UnregisterThread();

	SetEvent(stream_cfg->ini_block);

	return result;
}



UI_64 Audio::DefaultRenderer::Initialize() {
	UI_64 result(0);
	double d_vals[2];
	
	unsigned int r_val(0x00000007), g_val(0x00007F00), b_val(0xFFFF0000);

	AAC::Initialize();
	MP3::Initialize();
	AC3::Initialize();
	DTS::Initialize();
	

	ls_tab[0] = ls_tab[1] = ls_tab[2] = 0.0f; ls_tab[3] = 1.0f;
	ls_tab[4] = ls_tab[5] = ls_tab[6] = ls_tab[7] = 0.0f;

	for (unsigned int i(1);i<1024;i++) {
		d_vals[0] = 0.0;
		d_vals[1] = (424971.84583579188411814940805841/(3072.0 + i)/(3072.0 + i))*Calc::RealSin(180.0*(3072.0 + i)/1024.0)*Calc::RealSin(180.0*(3072.0 + i)/4096.0);
		ls_tab[i*8 + 0] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(2048.0 + i)/(2048.0 + i))*Calc::RealSin(180.0*(2048.0 + i)/1024.0)*Calc::RealSin(180.0*(2048.0 + i)/4096.0);
		ls_tab[i*8 + 1] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(1024.0 + i)/(1024.0 + i))*Calc::RealSin(180.0*(1024.0 + i)/1024.0)*Calc::RealSin(180.0*(1024.0 + i)/4096.0);
		ls_tab[i*8 + 2] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(i)/(i))*Calc::RealSin(180.0*(i)/1024.0)*Calc::RealSin(180.0*(i)/4096.0);
		ls_tab[i*8 + 3] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(1024.0 - i)/(1024.0 - i))*Calc::RealSin(180.0*(1024.0 - i)/1024.0)*Calc::RealSin(180.0*(1024.0 - i)/4096.0);
		ls_tab[i*8 + 4] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(2048.0 - i)/(2048.0 - i))*Calc::RealSin(180.0*(2048.0 - i)/1024.0)*Calc::RealSin(180.0*(2048.0 - i)/4096.0);
		ls_tab[i*8 + 5] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(3072.0 - i)/(3072.0 - i))*Calc::RealSin(180.0*(3072.0 - i)/1024.0)*Calc::RealSin(180.0*(3072.0 - i)/4096.0);
		ls_tab[i*8 + 6] = d_vals[1];
		d_vals[0] += d_vals[1];

		d_vals[1] = (424971.84583579188411814940805841/(4096.0 - i)/(4096.0 - i))*Calc::RealSin(180.0*(4096.0 - i)/1024.0)*Calc::RealSin(180.0*(4096.0 - i)/4096.0);
		ls_tab[i*8 + 7] = d_vals[1];
		d_vals[0] += d_vals[1];

	}

/*
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i] = 0x80FF8000 | (i<<24); // bg
	}
	for (unsigned int i(128);i<192;i++) {
		vs_color_table[i] = 0xFFFF0000 | (i<<8); // bg
	}
	for (unsigned int i(0);i<64;i++) {
		vs_color_table[i + 192] = 0xFF00C000 | ((255 - i)<<16); // bg
	}

	for (unsigned int i(0);i<64;i++) {
		vs_color_table[256 + i] = 0xFFC00000 | ((192 + i)<<8); // g
	}
	for (unsigned int i(0);i<256;i++) {
		vs_color_table[256 + 64 + i] = 0xFFC0FF00 | (i); // gr
	}
	for (unsigned int i(0);i<192;i++) {
		vs_color_table[512 + 64 + i] = 0xFFC000FF | ((192-i)<<16) | ((255 - i) <<8);
	}
	for (unsigned int i(0);i<256;i++) {
		vs_color_table[768 + i] = 0xFF002000 | (255 - i);
	}
*/

/*
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i] = 0x80FF8000 | (i<<24); // bg
	}
	for (unsigned int i(128);i<256;i++) {
		vs_color_table[i] = 0xFFFF0000 | (i<<8); // bg
	}
	for (unsigned int i(0);i<256;i++) {
		vs_color_table[i + 256] = 0xFF00FF00 | ((255 - i)<<16); // bg
	}
	for (unsigned int i(0);i<256;i++) {
		vs_color_table[i + 512] = 0xFF00FF00 | (i); // g
	}
	for (unsigned int i(0);i<256;i++) {
		vs_color_table[768 + i] = 0xFF0000FF | ((255 - i)<<8); // gr
	}

	for (unsigned int i(1024);i<2048;i++) vs_color_table[i] = 0xFF000000;
*/

/*
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i] = 0x80FF8000 | (i<<24);
	}
*/
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i] = 0xFF000000 | ((((255*(128+i))>>8) + ((16*(128-i))>>7)) << 16) | ((((128*(128+i))>>8) + ((16*(128-i))>>7)) << 8) | ((16*(128-i))>>7); // 0x80FF8000 | (i<<24);
	}

	for (unsigned int i(128);i<256;i++) {
		vs_color_table[i] = 0xFFFF0000 | (i<<8);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i + 256] = 0xFF01FF00 | ((128 - i)<<17);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[i + 384] = 0xFF00FF01 | (i << 1);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[512 + i] = 0xFF0001FF | ((128 - i)<<9);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[640 + i] = 0xFF0000FF | (i << 16);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[768 + i] = 0xFF000000 | ((128 - i)<<16) | (255 - i);
	}
	for (unsigned int i(0);i<128;i++) {
		vs_color_table[896 + i] = 0x00000080 | ((255 - i)<<24);
	}



	for (unsigned int i(1024);i<2048;i++) vs_color_table[i] = 0xFF000000;


	vs_color_table[0] = 0xFF000000;







	
	g_val = OpenGL::Core::special_x_size;

	r_val = (OpenGL::Core::special_x_size << 4)/10;
	r_val += 15;
	r_val &= ~15;
	

	if (view_buf.New(view_tid, r_val*g_val*4*sizeof(I_64) + 16*sizeof(I_64), SFSco::large_mem_mgr) != -1) {// run length, i_count, temp, x, y
		if (I_64 * v_ptr = reinterpret_cast<I_64 *>(view_buf.Acquire())) {
			v_ptr[0] = 0; // selector
			v_ptr[1] = r_val*g_val;
			v_ptr[2] = r_val;
			v_ptr[3] = g_val;


			reinterpret_cast<double *>(v_ptr)[4] = -0.7505; // offset x -2.5
			reinterpret_cast<double *>(v_ptr)[5] = 0.05; // offset y -1.0

			reinterpret_cast<double *>(v_ptr)[6] = reinterpret_cast<double *>(v_ptr)[7] = 0.0015/g_val;
				
			v_ptr[8] = v_ptr[9] = 0x0FFFFFFFF; // mm

			reinterpret_cast<unsigned int *>(v_ptr + 10)[0] = 0; reinterpret_cast<unsigned int *>(v_ptr + 10)[1] = -1;
			
			v_ptr[11] = 0;
			v_ptr[15] = 1;

			view_buf.Release();
		}
	}

/*
	if (I_64 * vb_cfg = reinterpret_cast<I_64 *>(view_buf.Acquire())) {	
		System::MemoryFill_SSE3(vb_cfg + 16, vb_cfg[1]*4*sizeof(UI_64), 0, 0);
		
		view_buf.Release();
	}
*/

	return 0;
}


UI_64 Audio::DefaultRenderer::Finalize() {
	
	return 0;
}


// =======================================================================================================================================================================================
unsigned int Audio::GenCosTable(float * cos_table, unsigned int N_VAL) {
	unsigned int result(0), win_count(0), temp_val(0), capa_val(0);

	capa_val = N_VAL;
	
	for (unsigned int i(0);i<(capa_val>>4);i++) {
		cos_table[result + 8*i] = 2.0f*Calc::RealCos(180.0*(8.0*i + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 1] = 2.0f*Calc::RealCos(180.0*(8.0*i + 2.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 2] = 2.0f*Calc::RealCos(180.0*(8.0*i + 4.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 3] = 2.0f*Calc::RealCos(180.0*(8.0*i + 6.0 + 1.0)/2.0/capa_val);


		cos_table[result + 8*i + 4] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 8.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 5] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 6.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 6] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 4.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 7] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 2.0 + 1.0)/2.0/capa_val);
	}

	capa_val >>= 1;
	result += capa_val;

	for (unsigned int i(0);i<(capa_val>>4);i++) {
		cos_table[result + 8*i] = 2.0f*Calc::RealCos(180.0*(16.0*i + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 1] = 2.0f*Calc::RealCos(180.0*(16.0*i + 2.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 2] = 2.0f*Calc::RealCos(180.0*(16.0*i + 4.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 3] = 2.0f*Calc::RealCos(180.0*(16.0*i + 6.0 + 1.0)/2.0/capa_val);


		cos_table[result + 8*i + 4] = 2.0f*Calc::RealCos(180.0*(16.0*i + 8.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 5] = 2.0f*Calc::RealCos(180.0*(16.0*i + 10.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 6] = 2.0f*Calc::RealCos(180.0*(16.0*i + 12.0 + 1.0)/2.0/capa_val);
		cos_table[result + 8*i + 7] = 2.0f*Calc::RealCos(180.0*(16.0*i + 14.0 + 1.0)/2.0/capa_val);
	}

	capa_val >>= 1;
	result += capa_val;

	for (;capa_val>8;capa_val>>=1, result += capa_val) {		

		for (unsigned int i(0);i<(capa_val>>4);i++) {
			cos_table[result + 8*i] = 2.0f*Calc::RealCos(180.0*(8.0*i + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 1] = 2.0f*Calc::RealCos(180.0*(8.0*i + 2.0 + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 2] = 2.0f*Calc::RealCos(180.0*(8.0*i + 4.0 + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 3] = 2.0f*Calc::RealCos(180.0*(8.0*i + 6.0 + 1.0)/2.0/capa_val);


			cos_table[result + 8*i + 4] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 8.0 + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 5] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 6.0 + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 6] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 4.0 + 1.0)/2.0/capa_val);
			cos_table[result + 8*i + 7] = 2.0f*Calc::RealCos(180.0*(capa_val - 8.0*i - 2.0 + 1.0)/2.0/capa_val);
		}
	}

	cos_table[result + 0] = 2.0f*Calc::RealCos(180.0*(2.0*0.0 + 1.0)/2.0/8.0);
	cos_table[result + 1] = 2.0f*Calc::RealCos(180.0*(2.0*1.0 + 1.0)/2.0/8.0);
	cos_table[result + 2] = 2.0f*Calc::RealCos(180.0*(2.0*2.0 + 1.0)/2.0/8.0);
	cos_table[result + 3] = 2.0f*Calc::RealCos(180.0*(2.0*3.0 + 1.0)/2.0/8.0);


	cos_table[result + 4] = 2.0f*Calc::RealCos(180.0*(2.0*0.0 + 1.0)/2.0/4.0);
	cos_table[result + 5] = 1.0f;
	cos_table[result + 6] = 2.0f*Calc::RealCos(180.0*(2.0*1.0 + 1.0)/2.0/4.0);
	cos_table[result + 7] = 1.0f;

	cos_table[result + 8] = Calc::RealCos(180.0*(1.0)/2.0/2.0);
	cos_table[result + 9] = 1.0f;
	cos_table[result + 10] = Calc::RealCos(180.0*(1.0)/2.0/2.0);
	cos_table[result + 11] = 1.0f;



	result += 12;

	for (unsigned int n_v(4);n_v<=(N_VAL>>1);n_v<<=1) {
		win_count = 2;

		reinterpret_cast<unsigned int *>(cos_table)[result] = 0;
		reinterpret_cast<unsigned int *>(cos_table)[result + 1] = (n_v>>1);
		
		for (unsigned int o_idx(n_v>>2);o_idx;o_idx>>=1) {
			for (unsigned int i(0);i<win_count;i++) {
				reinterpret_cast<unsigned int *>(cos_table)[result + win_count + i] = reinterpret_cast<unsigned int *>(cos_table)[result + i] + o_idx;
			}
			win_count <<= 1;
		}

		for (unsigned int i(0);i<n_v;i++) {
			unsigned int temp_val = reinterpret_cast<unsigned int *>(cos_table)[result + i];
			reinterpret_cast<unsigned int *>(cos_table)[result + i] = ((((temp_val & 0xFFFFFFFC) << 1) | (temp_val & 3)) << 2);
		}

		result += n_v;
	}

	return result;
}





