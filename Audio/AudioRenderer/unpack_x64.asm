
; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


EXTERN ?ls_tab@DefaultRenderer@Audio@@0PAMA:DWORD

.CONST
ALIGN 16

float_1		DWORD	3F800000h, 3F800000h, 3F800000h, 3F800000h
float_m1	DWORD	0BF800000h, 0BF800000h, 0BF800000h, 0BF800000h



.CODE

ALIGN 16
?ClearChannel@DefaultRenderer@Audio@@CAXPEAXIII@Z	PROC
	LEA rcx, [rcx + r8*4]
	
	SHL r9, 2

	XOR rax, rax
align 16
	clear_loop:
		MOV [rcx], eax
		LEA rcx, [rcx + r9]


	DEC edx
	JNZ clear_loop

	RET
?ClearChannel@DefaultRenderer@Audio@@CAXPEAXIII@Z	ENDP


ALIGN 16
?MixUnpackF@DefaultRenderer@Audio@@CAIPEAMPEBMPEBIM@Z	PROC

	MOV r10d, [r8 + 12] ; target chan count
	SHL r10, 2

	MOV eax, [r8 + 16]
	LEA rcx, [rcx + rax*4]
	
	XOR rax, rax

	MOV r11d, [r8 + 32] ; window sample count
	SHR r11d, 4

	MOV r9d, [r8 + 36] ; window count
	
	MOVSS xmm7, DWORD PTR [r8 + 40] ; volume
	MULPS xmm7, xmm3
	SHUFPS xmm7, xmm7, 0

	MOVAPS xmm8, XMMWORD PTR [float_1]
	MOVAPS xmm9, XMMWORD PTR [float_m1]

align 16
	window_loop:
		MOVAPS xmm0, [rdx]
		MULPS xmm0, xmm7
		MAXPS xmm0, xmm9
		MINPS xmm0, xmm8
		

		MOVAPS xmm1, [rdx + 16]
		MULPS xmm1, xmm7
		MAXPS xmm1, xmm9
		MINPS xmm1, xmm8
		

		MOVAPS xmm2, [rdx + 32]
		MULPS xmm2, xmm7
		MAXPS xmm2, xmm9
		MINPS xmm2, xmm8
		

		MOVAPS xmm3, [rdx + 48]
		MULPS xmm3, xmm7
		MAXPS xmm3, xmm9
		MINPS xmm3, xmm8
		

		
		ADDSS xmm0, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		ADDSS xmm0, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		ADDSS xmm0, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		ADDSS xmm0, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
	

		ADDSS xmm1, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		ADDSS xmm1, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		ADDSS xmm1, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		ADDSS xmm1, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]


		ADDSS xmm2, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		ADDSS xmm2, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		ADDSS xmm2, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		ADDSS xmm2, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]


		ADDSS xmm3, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		ADDSS xmm3, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		ADDSS xmm3, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		ADDSS xmm3, DWORD PTR [rcx]
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]








		LEA rdx, [rdx + 64]
		LEA rax, [rax + 16]


	DEC r11d
	JNZ window_loop
		MOV r11d, [r8 + 32]		; window sample count
		LEA rdx, [rdx + r11*4]
				
		SHR r11d, 4
		
	DEC r9d
	JNZ window_loop



	RET
?MixUnpackF@DefaultRenderer@Audio@@CAIPEAMPEBMPEBIM@Z	ENDP


ALIGN 16
?UnpackChannelF@DefaultRenderer@Audio@@CAIPEAXPEBMPEBI@Z	PROC

	MOV r10d, [r8 + 12] ; target chan count
	SHL r10, 2

	MOV eax, [r8 + 16]
	LEA rcx, [rcx + rax*4]
	
	XOR rax, rax

	MOV r11d, [r8 + 32] ; window sample count
	SHR r11d, 4

	MOV r9d, [r8 + 36] ; window count
	
	MOVSS xmm7, DWORD PTR [r8 + 40] ; volume
	SHUFPS xmm7, xmm7, 0

	MOVAPS xmm8, XMMWORD PTR [float_1]
	MOVAPS xmm9, XMMWORD PTR [float_m1]

align 16
	window_loop:
		MOVAPS xmm0, [rdx]
		MULPS xmm0, xmm7
		MAXPS xmm0, xmm9
		MINPS xmm0, xmm8
		

		MOVAPS xmm1, [rdx + 16]
		MULPS xmm1, xmm7
		MAXPS xmm1, xmm9
		MINPS xmm1, xmm8
		

		MOVAPS xmm2, [rdx + 32]
		MULPS xmm2, xmm7
		MAXPS xmm2, xmm9
		MINPS xmm2, xmm8
		

		MOVAPS xmm3, [rdx + 48]
		MULPS xmm3, xmm7
		MAXPS xmm3, xmm9
		MINPS xmm3, xmm8
		

		
		
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
		SHUFPS xmm0, xmm0, 39h
		MOVSS DWORD PTR [rcx], xmm0
		LEA rcx, [rcx  + r10]
	

		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]
		SHUFPS xmm1, xmm1, 39h
		MOVSS DWORD PTR [rcx], xmm1
		LEA rcx, [rcx  + r10]


		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]
		SHUFPS xmm2, xmm2, 39h
		MOVSS DWORD PTR [rcx], xmm2
		LEA rcx, [rcx  + r10]


		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]
		SHUFPS xmm3, xmm3, 39h
		MOVSS DWORD PTR [rcx], xmm3
		LEA rcx, [rcx  + r10]








		LEA rdx, [rdx + 64]
		LEA rax, [rax + 16]


	DEC r11d
	JNZ window_loop
		MOV r11d, [r8 + 32]		; window sample count
		LEA rdx, [rdx + r11*4]
				
		SHR r11d, 4
		
	DEC r9d
	JNZ window_loop



	RET
?UnpackChannelF@DefaultRenderer@Audio@@CAIPEAXPEBMPEBI@Z	ENDP



ALIGN 16
?IUnpackChannelF@DefaultRenderer@Audio@@CAIPEAXPEBMPEBIPEAI@Z	PROC
	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	LEA r15, [?ls_tab@DefaultRenderer@Audio@@0PAMA]

	MOV rbx, r9
	MOV r10, rdx

	MOV eax, [r8 + 16]
	LEA rcx, [rcx + rax*4]

	XOR eax, eax
	

	MOV edx, [r8] ; source frequency
	DIV DWORD PTR [r8 + 4] ; target frequency

	MOV r12, rax
		
	XOR rax, rax	
	MOV rdx, r10


	MOV r10d, [r8 + 12] ; target chan count
	SHL r10, 2


	MOV r11d, [r8 + 32] ; window sample count
	SHR r11d, 2
	
	MOV r9d, [r8 + 36] ; window count
	

	MOVSS xmm13, DWORD PTR [r8 + 40]
	SHUFPS xmm13, xmm13, 0

	MOVAPS xmm14, XMMWORD PTR [float_1]
	MOVAPS xmm15, XMMWORD PTR [float_m1]


; load history	
	MOV r13d, [rbx]
	

	MOVAPS xmm0, [rbx + 16]
	MOVAPS xmm1, [rbx + 32]

align 16
	window_loop:				
		MOVAPS xmm12, [rdx]
		MULPS xmm12, xmm13

		MINPS xmm12, xmm14
		MAXPS xmm12, xmm15

		LEA rdx, [rdx + 16]


	align 16
		gap_0:
			MOV r14d, r13d
			SHR r14d, 22
			SHL r14d, 5

			MOVAPS xmm8, [r15 + r14]
			MOVAPS xmm9, [r15 + r14 + 16]


			MULPS xmm8, xmm0
			MULPS xmm9, xmm1

			ADDPS xmm8, xmm9
			HADDPS xmm8, xmm8
			HADDPS xmm8, xmm8
			
			MOVSS DWORD PTR [rcx], xmm8
			LEA rcx, [rcx + r10]

			INC rax
		ADD r13d, r12d
		JNC gap_0

		MOVSS xmm0, xmm1
		SHUFPS xmm0, xmm0, 39h
		MOVSS xmm1, xmm12
		SHUFPS xmm1, xmm1, 39h

		SHUFPS xmm12, xmm12, 39h
		


	align 16
		gap_1:
			MOV r14d, r13d
			SHR r14d, 22
			SHL r14d, 5

			MOVAPS xmm8, [r15 + r14]
			MOVAPS xmm9, [r15 + r14 + 16]


			MULPS xmm8, xmm0
			MULPS xmm9, xmm1

			ADDPS xmm8, xmm9
			HADDPS xmm8, xmm8
			HADDPS xmm8, xmm8

			MOVSS DWORD PTR [rcx], xmm8
			LEA rcx, [rcx + r10]

			INC rax
		ADD r13d, r12d
		JNC gap_1

		MOVSS xmm0, xmm1
		SHUFPS xmm0, xmm0, 39h
		MOVSS xmm1, xmm12
		SHUFPS xmm1, xmm1, 39h

		SHUFPS xmm12, xmm12, 39h


	align 16
		gap_2:
			MOV r14d, r13d
			SHR r14d, 22
			SHL r14d, 5

			MOVAPS xmm8, [r15 + r14]
			MOVAPS xmm9, [r15 + r14 + 16]

			MULPS xmm8, xmm0
			MULPS xmm9, xmm1

			ADDPS xmm8, xmm9
			HADDPS xmm8, xmm8
			HADDPS xmm8, xmm8

			MOVSS DWORD PTR [rcx], xmm8
			LEA rcx, [rcx + r10]

			INC rax
		ADD r13d, r12d
		JNC gap_2

		MOVSS xmm0, xmm1
		SHUFPS xmm0, xmm0, 39h
		MOVSS xmm1, xmm12
		SHUFPS xmm1, xmm1, 39h

		SHUFPS xmm12, xmm12, 39h


	align 16
		gap_3:
			MOV r14d, r13d
			SHR r14d, 22
			SHL r14d, 5

			MOVAPS xmm8, [r15 + r14]
			MOVAPS xmm9, [r15 + r14 + 16]
			

			MULPS xmm8, xmm0
			MULPS xmm9, xmm1

			ADDPS xmm8, xmm9
			HADDPS xmm8, xmm8
			HADDPS xmm8, xmm8

			MOVSS DWORD PTR [rcx], xmm8
			LEA rcx, [rcx + r10]

			INC rax
		ADD r13d, r12d
		JNC gap_3

		MOVSS xmm0, xmm1
		SHUFPS xmm0, xmm0, 39h
		MOVSS xmm1, xmm12
		SHUFPS xmm1, xmm1, 39h


	DEC r11d
	JNZ window_loop
	

	
		MOV r11d, [r8 + 32]		; window count
		LEA rdx, [rdx + r11*4]

		SHR r11d, 2		
		
	DEC r9d
	JNZ window_loop

	MOV [rbx], r13d
	
	MOVAPS [rbx + 16], xmm0
	MOVAPS [rbx + 32], xmm1

	POP r15
	POP r14
	POP r13
	POP r12

	POP rbx
	
	RET
?IUnpackChannelF@DefaultRenderer@Audio@@CAIPEAXPEBMPEBIPEAI@Z	ENDP


END


