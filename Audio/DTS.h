/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "Audio\AudioRenderer.h"


#define DTS_CHANNEL_SIZE		4*32*33

#define DTS_CM_FRONT_CENTER		0x00000001
#define DTS_CM_FRONT_LEFT		0x00000002
#define DTS_CM_FRONT_RIGHT		0x00000004
#define DTS_CM_LOW_FREQUENCY	0x00000020
#define DTS_CM_REAR_CENTER		0x00000040
#define DTS_CM_REAR_LEFT		0x00000080
#define DTS_CM_REAR_RIGHT		0x00000100
#define DTS_CM_SIDE_LEFT		0x00000200
#define DTS_CM_SIDE_RIGHT		0x00000400


namespace Audio {


	class DTS: public Codec {
		private:
			static const unsigned int sampling_frequencies[16];
			static const unsigned int ll_sampling_frequencies[16];
			static const unsigned int bps_range[8];

			static const unsigned int scale_adjustment[4];

			static const int hts_cfg[];
			static const int hti_cfg[][4];

			static const int sbook_cfg[][4];

			static const int scale_huffman_tab[];
			static const int sample_huffman_tab[];

			static const unsigned int scale_fac7[];
			static const unsigned int scale_fac6[];
			static const unsigned int scale_facJ[];

			static const unsigned int q_step_size[2][32];

			static const unsigned int range_tab[][2];
			static const char hf_VQ[1024][32];

			static const unsigned int speaker_counts[];

			static const unsigned int lfe_val_tab[];

			static const unsigned int dts2win_map[];

			static const float dmix_tab[];

			static const unsigned int mix_chan_count[];
			
			static __declspec(align(16)) const unsigned int adpcm_tab[][4];
			static __declspec(align(16)) const int ll_adpcm_tab[];

			static __declspec(align(16)) const unsigned int lfe_interpol[2][512];
			static __declspec(align(16)) const unsigned int qmf_interpol[2][512];
			
			static __declspec(align(16)) float synth_cos[640];

			unsigned int hd_brand;
			
			struct FrameBSHeader {
				unsigned int frame_size, frame_version, frame_type, short_block_sample_count, ext_audio_id, dialog_norm;

				unsigned int sf_idx, pcm_idx, br_idx, sampling_frequency, arr_mode, block32_count, full_chan_count;

				unsigned int chan_type[MAX_CHANNEL_COUNT];
				unsigned char center_present, lfe_present, surround_mode, ed_range_flag, et_stamp_flag, aux_data_flag, hdcd_flag, ext_audio_flag, async_wi_flag, predictor_flag, interpolator_switch, front_encoding, surround_encoding, crc_present;

			} bs_hdr;


			struct FrameConfig {
				unsigned int subframe_count, active_chan_count, subsub_count, partial_s_count, range_val;


				struct ChanDesc {
					unsigned char activ_subband_count; // 33
					unsigned char hfvq_subband_start; // 32
					unsigned char joint_intensity_idx; // 3b
					unsigned char transient_mode_cb; // 2b

					unsigned char scale_factor_cb; // 3b
					unsigned char bit_allocation_cb; // 3b
					unsigned char joint_ii_cb;
					unsigned char uc_reserved;

					unsigned int pvq_index[32];
					unsigned int joint_scale_factor[32];


					unsigned char quantization_index_cb[32];
					unsigned char scale_factor_adjustment[32];

					unsigned char prediction_mode[32];
					unsigned char allocation_index[32];
					unsigned char transition_mode[32];


					
				} chan_desc[MAX_CHANNEL_COUNT];

			} frame_cfg;




			struct SubStreamHeader {
				unsigned int asset_size[8];
				unsigned int h_size, x_size, a_size, duration_code;
				unsigned int sf_idx;

				struct MixMeta {
					unsigned short mix_channel_mask[4];
					unsigned char chan_count[4];
					unsigned char adjustment_level, config_count;
					
				} mm_data;

				struct AudioPresentation {
					unsigned char asset_mask[8];
					unsigned char mask, bc_core_present, bc_index, bc_asset_index;
				} audio_p[8];

				unsigned char index, audio_presentation_count, assets_count, static_field_flag, mix_md_flag, xs_bits;

				
			} ss_hdr;

			struct AssetDescriptor {
				struct Speakers {
					unsigned int remap_mask[3];
					unsigned char remap_codes[3][32];
					unsigned short layout_mask;
					unsigned char count, remap_count, code_count;

				} speakers[8];

				struct MixMeta {
					struct ConfigSet {
						unsigned char map_mask[2][256];
						unsigned char mix_coffs[2][256][2];
						unsigned char scale_code[2];
						
					} c_set[4];

					unsigned char post_gain, dyr_compression, dyr_comp_limit, custom_dyr_code;
					unsigned char external_flag;

				} m_meta;


				unsigned int size, language, bit_depth, max_sf_idx, tot_chan_count, activity_mask, col_count;
				unsigned int core_xmask, core_xsize, xbr_size, xxch_size, x96_size, lbr_size, xll_size, aux_size;
				unsigned char _type, representation_type, coding_mode, core_sync_dist, lbr_sync_dist;
				unsigned char dyr_code, dnorm_code, dyr_mix_code, aux_codec_id, stream_id;


				unsigned char stereo_flag, ch7_flag, one2one_flag, mix_present_flag, remap_set_count;

			} asset[8];

			struct LLConfig {
				struct SubSetHdr {

					struct MixCfg {
						float coffs[64];
						unsigned int channel_mask[4], speaker_count[4], speaker_mask[4];						
						unsigned char embed_flag, _type, map_coff_bits, speaker_config_count;

					} d_mix;

					struct BandCfg {
						unsigned char refl_coffs[256];						
						unsigned char adaptive_predictor_order[16];
						unsigned char fixed_predictor_order[16];
						unsigned char lsb_abit[16];
						char decor_alpha[8];

						unsigned int lsb_present;
						unsigned char decorelation_enabled;
						unsigned char max_ap_order;
					} band_cfg[4];

					unsigned int sf_idx, sampling_frequency, channel_config, residual_ce, map_offset;					

					unsigned char chan_count, bit_resolution, bit_width, bits4abit, interpolation_multiplier, replacement_cs;
					unsigned char skip_decode_flag, primary_chan_flag, hierarchical_flag, xband_count;

				} ss_hdr[16];


				struct BandInfo {					
					unsigned int set_size[256][16];
					unsigned int segment_size[256];
					unsigned int size;
					
				} b_info[4];
				
				struct CodingInfo {
					unsigned short sample_part_0;

					unsigned char rice_flag;
					unsigned char aux_abit, abit_0, abit;					

				} c_info[16];

				unsigned int chan_map[MAX_CHANNEL_COUNT];
				unsigned int size, segment_count, bits4addr, samples_per_segment, tot_chan_count, x_chan_count;				
				unsigned char chan_set_count, bits4ssize, bits4mask, crc_config, representation_type, channel_position_mask, fixed_lsb_split, fband_count, lsb_scalable_flag, seg_type;

				
			} ll_frame;

			class DecoderState: public DecoderCfg {
				public:
					__forceinline int GetRiceVal() {
						int result(0);

						for (;(NextBit() == 0) && (src_size > 0);result++);

						return result;
					}

					__forceinline int GetSignedVal(int bit_count) {
						int result = GetNumber(bit_count);

						if (result & 1) {
							result = -(result >> 1) - 1;
						} else {
							result >>= 1;
						}

						return result;
					};
										

					__forceinline int GetHuffmaVal(const int * cb_ptr, int cb_idx) {
						unsigned int code_val(0);

						int c_count(0);
						
						if (src_size > 0) {
							code_val = NextBit();

							for (;cb_idx>=0;) {															
								c_count = cb_ptr[0]; cb_ptr += 2;

								for (int i(0);i<c_count;i++, cb_ptr+=2) {
									if (code_val == cb_ptr[0]) {
										return cb_ptr[1];
									}
								}
							
								cb_idx -= c_count;

								code_val <<= 1;
								code_val |= NextBit();
							}
						}

						return 0;
					};


			} decoder_cfg;



			static void GetVector(int *, int, int);
			static void ScaleChannel(float *, unsigned int);
			static void InverseADPCM(float *, UI_64, unsigned int, unsigned int);
			static void InterpolateLFE(float *, UI_64, unsigned int, unsigned int);
			static void InterpolateQMF(float *, UI_64, UI_64, const unsigned int *);
			static void Normalize(float *, const unsigned int &, unsigned int);
			static void StereoAdjust(float *, float *, unsigned int);
			
			static void LLOverride(int *, unsigned int, unsigned int);
			static void LLConvertAdd(int *, unsigned int, unsigned int);
			
			

			UI_64 CoreDecode(I_64 &);
			UI_64 SubDecode(I_64 &);

			void GetBitStreamHeader();
			void GetCoreAudioHeader();
			
			void GetSideInfo();
			void GetSubFrame();

			void GetAssetDescriptor();
			
			void CalculateLLBand(unsigned int);
			void GetLLMSB(const LLConfig::SubSetHdr * sub_hdr, unsigned int);
			void GetLLSegState(unsigned int, unsigned int, const LLConfig::SubSetHdr *);
			UI_64 GetLLCSHeader(LLConfig::SubSetHdr &, unsigned int, unsigned int);
			UI_64 GetLossless(unsigned int);
			UI_64 GetLowBitrate(unsigned int);

			virtual UI_64 FilterFrame(unsigned int *);
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &);

			virtual unsigned int GetSatCount(unsigned int *);

			virtual unsigned int GetSatInfoCount() { return 3; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &);

		public:
			static const UI_64 frame_buf_tid = 961749023;


			DTS();
			virtual ~DTS();




			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
						
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
			virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);


			static UI_64 Initialize();
			static UI_64 Finalize();

	};


}

/*


*/

