/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\MP3.h"
#include "System\SysUtils.h"
#include "Audio\AAC.h"

#pragma warning(disable:4244)

__forceinline void Audio::MP3::SetDataQuad(float *& d_ptr, unsigned int tab_idx, short & b_count, int s_flag) {
	short h_idx(0);


	if (tab_idx) {
		h_idx = decoder_cfg.GetNumber(4) ^ 0x000F;
		b_count -= 4;
	} else {
		h_idx = decoder_cfg.GetHuffmaVal(4, b_count);
		h_idx = Huffman_Tab[h_idx*4 + 1];
	}
	
	reinterpret_cast<int *>(d_ptr)[0] = (h_idx >> 3) & 1;

	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}

	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_flag & 0xFFFF];

	d_ptr++;

	reinterpret_cast<int *>(d_ptr)[0] = (h_idx >> 2) & 1;
	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}

	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_flag & 0xFFFF];

	d_ptr++;



	if (s_flag & 0xF0000000) {
		d_ptr += 4;
	}


	reinterpret_cast<int *>(d_ptr)[0] = (h_idx >> 1) & 1;
	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}

	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_flag & 0xFFFF];

	d_ptr++;

	reinterpret_cast<int *>(d_ptr)[0] = h_idx & 1;
	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}

	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_flag & 0xFFFF];

	d_ptr++;

}


__forceinline void Audio::MP3::SetDataPair(float *& d_ptr, unsigned int tab_idx, short & b_count, int s_fac) {
	short h_idx = decoder_cfg.GetHuffmaVal(tab_idx, b_count);

	reinterpret_cast<int *>(d_ptr)[0] = Huffman_Tab[h_idx*4 + 1];


	if (hct_cfg[tab_idx*4 + 2]) { // esc
		if (reinterpret_cast<int *>(d_ptr)[0] == 15) {
			reinterpret_cast<int *>(d_ptr)[0] += decoder_cfg.GetNumber(hct_cfg[tab_idx*4 + 2]);

			b_count -= hct_cfg[tab_idx*4 + 2];
		}
	}

	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}
		
	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_fac];


	d_ptr++;
	

	reinterpret_cast<int *>(d_ptr)[0] = Huffman_Tab[h_idx*4 + 2];

	if (hct_cfg[tab_idx*4 + 2]) { // esc
		if (reinterpret_cast<int *>(d_ptr)[0] == 15) {
			reinterpret_cast<int *>(d_ptr)[0] += decoder_cfg.GetNumber(hct_cfg[tab_idx*4 + 2]);
			b_count -= hct_cfg[tab_idx*4 + 2];
		}
	}

	if (reinterpret_cast<int *>(d_ptr)[0]) {
		reinterpret_cast<int *>(d_ptr)[0] = AAC::de_quant[reinterpret_cast<int *>(d_ptr)[0]];
		if (decoder_cfg.NextBit()) reinterpret_cast<int *>(d_ptr)[0] |= 0x80000000;
		b_count--;
	}

	reinterpret_cast<int *>(d_ptr)[4] = AAC::scale_factor[s_fac];


	d_ptr++;

}






void Audio::MP3::GetGranule(unsigned int chan_idx, unsigned int g_idx) {
	float * d_ptr(decoder_cfg.spectral_data + chan_idx*MP3_CHANNEL_SIZE + g_idx*MP3_GRANULE_SIZE*2 - 4);
	int t_val(0), sfb_idx(0), x_val(0), j_3(0), w_idx(0);

	if (decoder_cfg.active_cfg[chan_idx].block_type[g_idx] == 2) { // short type			
		if (decoder_cfg.active_cfg[chan_idx].switch_point[g_idx]) {

			if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]) {
				for (int i(0);i<8;i++) { // long					
					decoder_cfg.active_cfg[chan_idx].sf_idc[i] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
					decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] -= sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0];
				}

				decoder_cfg.active_cfg[chan_idx].sf_idc[8 ] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);

				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]);
						
				decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] -= 9*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0];
			} else {
				if (g_idx == 1) {
					for (unsigned int i(0);i<8;i++) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i] = 0;
					}
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 ] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 1] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 2] = 0;

					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3 + 1] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3 + 2] = 0;
				
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2 + 1] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 3*2 + 2] = 0;
				}
			}

			if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]) {
				for (int i(6);i<12;i++) {
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]);
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]);
					decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]);

					decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] -= 3*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1];
				}

				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 9*3] = 0;
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 9*3 + 1] = 0;
				decoder_cfg.active_cfg[chan_idx].sf_idc[8 + 9*3 + 2] = 0;

			} else {
				if (g_idx == 1) {
					for (int i(6);i<13;i++) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3 + 1] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[8 + (i-3)*3 + 2] = 0;
					}
				}				
			}



			
			sfb_idx = 0;
			t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);
			
			if (decoder_cfg.active_cfg[chan_idx].table_select[g_idx][0]) {
				for (int i(0);i<36;i+=2) {
					if ((i & 3) == 0) d_ptr += 4;

					if (i >= sfb_offset_long[sf_idx][sfb_idx+1][0]) {
						sfb_idx++;
						t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);
					}

					SetDataPair(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][0], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], t_val);				
				}
			} else {
				d_ptr += 36*2;
			}


			sfb_idx = 3;
			j_3 = 36;
			w_idx = 0;
			t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);

			if (decoder_cfg.active_cfg[chan_idx].table_select[g_idx][1]) {
				for (int i(36);i<decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];i+=2) { // until big end
					if ((i & 3) == 0) d_ptr += 4;

					if ((i - j_3) >= sfb_offset_short[sf_idx][sfb_idx][1]) {
						j_3 = i;
						w_idx++;
						if (w_idx > 2) {
							w_idx = 0;
							sfb_idx++;
						}
						t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);					
					}

					SetDataPair(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][1], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], t_val);
				}
			} else {
				t_val = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] - 36;
				if (t_val & 3) d_ptr += 4;
				d_ptr += ((t_val & 0xFFFFFFFC) << 1) | (t_val & 3);

				for (;decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] >= sfb_offset_short[sf_idx][sfb_idx][0];sfb_idx++);
				--sfb_idx;
				w_idx = 0;
				t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);
			}

			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = 3;
			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = 36; // long end
			
		} else {
			if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]) {
				if (g_idx == 0) {
					for (int i(0);i<6;i++) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0]);

						decoder_cfg.active_cfg[chan_idx].part2_3l[0] -= 3*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0];
					}
				} else {
					for (int i(0);i<6;i++) {						
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0]);

						decoder_cfg.active_cfg[chan_idx].part2_3l[1] -= 3*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0];						
					}
				}
			} else {
				if (g_idx == 1) {
					for (int i(0);i<6;i++) {						
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = 0;						
					}
				}
			}

			if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]) {
				if (g_idx == 0) {
					for (int i(6);i<12;i++) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1]);

						decoder_cfg.active_cfg[chan_idx].part2_3l[0] -= 3*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1];
					}
				} else {
					for (int i(6);i<12;i++) {					
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1]);
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1]);

						decoder_cfg.active_cfg[chan_idx].part2_3l[1] -= 3*sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1];
					}

					decoder_cfg.active_cfg[chan_idx].sf_idc[12*3] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[12*3 + 1] = 0;
					decoder_cfg.active_cfg[chan_idx].sf_idc[12*3 + 2] = 0;

				}
			} else {
				if (g_idx == 1) {
					for (int i(6);i<13;i++) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 1] = 0;
						decoder_cfg.active_cfg[chan_idx].sf_idc[i*3 + 2] = 0;
					}
				}
			}
						

			// do short huffman
			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = sfb_offset_short[sf_idx][decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1]][0];
			if (decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] > decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]) decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];
			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = sfb_offset_short[sf_idx][decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2]][0];
			if (decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] > decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]) decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];

			sfb_idx = 0;			
			j_3 = 0;
			w_idx = 0;
			t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);

			for (int i(0);i<3;i++) {
				if (decoder_cfg.active_cfg[chan_idx].table_select[g_idx][i]) {
					for (int j(decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i]);j<decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1];j+=2) {					
						if ((j&3) == 0) d_ptr += 4;

						if ((j - j_3) >= sfb_offset_short[sf_idx][sfb_idx][1]) {
							j_3 = j;
							w_idx++;
							if (w_idx > 2) {
								w_idx = 0;
								sfb_idx++;
							}
							t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);						
						}
						SetDataPair(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][i], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], t_val);
					}
				} else {
					t_val = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1] - decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i];
					
					if (((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i] & 3) == 0) && (t_val & 3)) d_ptr += 4;
					d_ptr += ((t_val & 0xFFFFFFFC) << 1) | (t_val & 3);

					for (;decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1] >= sfb_offset_short[sf_idx][sfb_idx][0];sfb_idx++);
					--sfb_idx;
					w_idx = 0;
					t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);
				}
			}
			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = 0;
			decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = 0; // long end
		}

		x_val = ((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] & 3)<<28);
		for (;decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx]>0;decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]+=4) {
			if ((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] + 4) > sfb_offset_short[sf_idx][13][0]) break;
			if ((x_val & 0xF0000000) == 0) d_ptr += 4;

			if ((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] - j_3) >= sfb_offset_short[sf_idx][sfb_idx][1]) {
				j_3 = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];
				w_idx++;
				if (w_idx > 2) {
					w_idx = 0;
					sfb_idx++;
					if (sfb_idx > 12) break;
				}

				t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[3*sfb_idx + w_idx]<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]) + decoder_cfg.active_cfg[chan_idx].subb_gain[g_idx][w_idx]);				
			}

			SetDataQuad(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][3], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], x_val | t_val);
		}


		if (decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] > 0) {
			decoder_cfg.SkipBits(decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx]);
			decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] = 0;
		}

	} else { // long type
		if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][0]) {
			if (g_idx == 0) {
				for (int i(0);i<11;i++) {
					decoder_cfg.active_cfg[chan_idx].sf_idc[i] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0]);
					decoder_cfg.active_cfg[chan_idx].part2_3l[0] -= sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][0];
				}
			} else {
				for (int i(0);i<11;i++) {
					if (decoder_cfg.active_cfg[chan_idx].sfb_common[i] == 0) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0]);
						decoder_cfg.active_cfg[chan_idx].part2_3l[1] -= sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][0];
					}
				}
			}
		} else {
			if (g_idx == 1) {
				for (int i(0);i<11;i++) {
					if (decoder_cfg.active_cfg[chan_idx].sfb_common[i] == 0) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i] = 0;
					}
				}
			}
		}

		if (sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[g_idx]][1]) {
			if (g_idx == 0) {
				for (int i(11);i<21;i++) {
					decoder_cfg.active_cfg[chan_idx].sf_idc[i] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1]);
					decoder_cfg.active_cfg[chan_idx].part2_3l[0] -= sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[0]][1];
				}
			} else {
				for (int i(11);i<21;i++) {
					if (decoder_cfg.active_cfg[chan_idx].sfb_common[i] == 0) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i] = decoder_cfg.GetNumber(sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1]);
						decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] -= sf_III_size[decoder_cfg.active_cfg[chan_idx].sf_compress[1]][1];
					}
				}

				decoder_cfg.active_cfg[chan_idx].sf_idc[21] = 0;
			}
		} else {
			if (g_idx == 1) {
				for (int i(11);i<22;i++) {
					if (decoder_cfg.active_cfg[chan_idx].sfb_common[i] == 0) {
						decoder_cfg.active_cfg[chan_idx].sf_idc[i] = 0;
					}
				}				
			}
		}

		

		// do long huffman		
		decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = sfb_offset_long[sf_idx][decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1]][0];
		if (decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] > decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]) decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];
		decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = sfb_offset_long[sf_idx][decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2]][0];
		if (decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] > decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]) decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];

		sfb_idx = 0;
		t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);

		for (int i(0);i<3;i++) {
			if (decoder_cfg.active_cfg[chan_idx].table_select[g_idx][i]) {
				for (int j(decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i]);j<decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1];j+=2) {
					if ((j&3) == 0) d_ptr += 4;

					if (j >= sfb_offset_long[sf_idx][sfb_idx+1][0]) {
						++sfb_idx;
						t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);					
					}

					SetDataPair(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][i], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], t_val);
				}
			} else {				
				t_val = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1] - decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i];
				
				if (((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i] & 3) == 0) && (t_val & 3)) d_ptr += 4;
				d_ptr += ((t_val & 0xFFFFFFFC) << 1) | (t_val & 3);

				for (;decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][i+1] >= sfb_offset_long[sf_idx][sfb_idx][0];sfb_idx++);
				--sfb_idx;
				t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);
			}
		}

		x_val = ((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] & 3)<<28);

		for (;decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx]>0;decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3]+=4) {
			if ((decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] + 4) > sfb_offset_long[sf_idx][22][0]) break;

			if ((x_val & 0xF0000000) == 0) d_ptr += 4;

			if (decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3] >= sfb_offset_long[sf_idx][sfb_idx+1][0]) {				
				if (++sfb_idx > 21) break;
				t_val = decoder_cfg.active_cfg[chan_idx].global_gain[g_idx] - ((decoder_cfg.active_cfg[chan_idx].sf_idc[sfb_idx] + (decoder_cfg.active_cfg[chan_idx].pre_flag[g_idx] & pre_tab[sfb_idx]))<<decoder_cfg.active_cfg[chan_idx].sf_scale[g_idx]);
				
			}
	
			SetDataQuad(d_ptr, decoder_cfg.active_cfg[chan_idx].table_select[g_idx][3], decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx], x_val | t_val);
		}

		decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][1] = 12;
		decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][2] = decoder_cfg.active_cfg[chan_idx].region_addr[g_idx][3];

		if (decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] > 0) {

			decoder_cfg.SkipBits(decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx]);
			decoder_cfg.active_cfg[chan_idx].part2_3l[g_idx] = 0;
		}
	}
}

UI_64 Audio::MP3::AudioData_III() {
	UI_64 x_val(0), m_size(0), result(0), m_end(0);
	
	m_end = decoder_cfg.GetNumber(9);

	x_val = frame_size - 4; // capacity
	if (protection_id == 0) x_val -= 2;

	decoder_cfg.frame_cfg->window_sample_count = MP3_GRANULE_SIZE;
	decoder_cfg.frame_cfg->window_count = 2;
	decoder_cfg.frame_cfg->window_offset = MP3_GRANULE_SIZE;


	if (c_mode == 3) { // single chan
		decoder_cfg.frame_cfg->source_chan_count = 1;
		decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;


		decoder_cfg.GetNumber(5); // private

		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[0] = decoder_cfg.current_cfg[0].sfb_common[1] = decoder_cfg.current_cfg[0].sfb_common[2] = decoder_cfg.current_cfg[0].sfb_common[3] = decoder_cfg.current_cfg[0].sfb_common[4] = decoder_cfg.current_cfg[0].sfb_common[5] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[6] = decoder_cfg.current_cfg[0].sfb_common[7] = decoder_cfg.current_cfg[0].sfb_common[8] = decoder_cfg.current_cfg[0].sfb_common[9] = decoder_cfg.current_cfg[0].sfb_common[10] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[11] = decoder_cfg.current_cfg[0].sfb_common[12] = decoder_cfg.current_cfg[0].sfb_common[13] = decoder_cfg.current_cfg[0].sfb_common[14] = decoder_cfg.current_cfg[0].sfb_common[15] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[16] = decoder_cfg.current_cfg[0].sfb_common[17] = decoder_cfg.current_cfg[0].sfb_common[18] = decoder_cfg.current_cfg[0].sfb_common[19] = decoder_cfg.current_cfg[0].sfb_common[20] = 1;
		}


		decoder_cfg.current_cfg[0].part2_3l[0] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[0].region_addr[0][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[0].global_gain[0] = decoder_cfg.GetNumber(8) - 10;

		decoder_cfg.current_cfg[0].sf_compress[0] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[0].block_split_flag[0] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].block_type[0] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[0].block_type[1] == 2) {
				decoder_cfg.current_cfg[0].region_addr[0][1] = 3;
				decoder_cfg.current_cfg[0].region_addr[0][2] = 3;
			} else {
				decoder_cfg.current_cfg[0].region_addr[0][1] = 8;
				decoder_cfg.current_cfg[0].region_addr[0][2] = 8;
			}			
			
			decoder_cfg.current_cfg[0].switch_point[0] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[0].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][1] = decoder_cfg.current_cfg[0].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].subb_gain[0][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[0][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[0][2] = (decoder_cfg.GetNumber(3)<<3);

		} else {
			decoder_cfg.current_cfg[0].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].region_addr[0][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[0].region_addr[0][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[0].region_addr[0][3]) {
				decoder_cfg.current_cfg[0].region_addr[0][1] += (decoder_cfg.current_cfg[0].region_addr[0][1] > 0);
				decoder_cfg.current_cfg[0].region_addr[0][2] += decoder_cfg.current_cfg[0].region_addr[0][1] + (decoder_cfg.current_cfg[0].region_addr[0][2]>0);
			}
		}
		

		decoder_cfg.current_cfg[0].pre_flag[0] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[0].sf_scale[0] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[0].table_select[0][3] = decoder_cfg.NextBit();


		decoder_cfg.current_cfg[0].part2_3l[1] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[0].region_addr[1][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[0].global_gain[1] = decoder_cfg.GetNumber(8) - 10;


		decoder_cfg.current_cfg[0].sf_compress[1] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[0].block_split_flag[1] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].block_type[1] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[0].block_type[1] == 2) {
				decoder_cfg.current_cfg[0].region_addr[1][1] = 3;
				decoder_cfg.current_cfg[0].region_addr[1][2] = 3;
			} else {
				decoder_cfg.current_cfg[0].region_addr[1][1] = 8;
				decoder_cfg.current_cfg[0].region_addr[1][2] = 8;
			}			

			decoder_cfg.current_cfg[0].switch_point[1] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[0].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][1] = decoder_cfg.current_cfg[0].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].subb_gain[1][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[1][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[1][2] = (decoder_cfg.GetNumber(3)<<3);



		} else {
			decoder_cfg.current_cfg[0].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].region_addr[1][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[0].region_addr[1][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[0].region_addr[1][3]) {
				decoder_cfg.current_cfg[0].region_addr[1][1] += 1;
				decoder_cfg.current_cfg[0].region_addr[1][2] += decoder_cfg.current_cfg[0].region_addr[1][1] + 1;
			}
		}


		decoder_cfg.current_cfg[0].pre_flag[1] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[0].sf_scale[1] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[0].table_select[1][3] = decoder_cfg.NextBit();


// main
		decoder_cfg.current_cfg[0].byte_align = packet_offset - m_end;

		x_val -= 17; // main size
		System::MemoryCopy(decoder_cfg.packet_ptr + packet_offset, decoder_cfg.src_ptr, x_val);
		
		m_size = decoder_cfg.current_cfg[0].part2_3l[0] + decoder_cfg.current_cfg[0].part2_3l[1];
		
		if (decoder_cfg.active_cfg[0].byte_align != -1) {
			active_byte = decoder_cfg.active_cfg[0].byte_align;
			active_bit = 0;
		}

		if (m_size) {
			packet_offset += x_val;
			x_val = packet_offset - active_byte;

			if (m_size <= (x_val<<3)) {
				// set decoder pointer			
				decoder_cfg.SetSource(decoder_cfg.packet_ptr + active_byte, x_val);
				if (active_bit) decoder_cfg.GetNumber(active_bit);

				GetGranule(0, 0);
				GetGranule(0, 1);

				if (decoder_cfg.active_cfg[0].part2_3l[0] | decoder_cfg.active_cfg[0].part2_3l[1]) {

					Rescale(decoder_cfg.spectral_data, 18*2);

					if (decoder_cfg.active_cfg[0].block_type[0] == 2) {
						Reorder(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][1]);
					}
					if (decoder_cfg.active_cfg[0].block_type[1] == 2) {
						Reorder(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[0].region_addr[1][1]);
					}


					AliasReduction(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][3], decoder_cfg.active_cfg[0].region_addr[1][3]);

					if (decoder_cfg.active_cfg[0].region_addr[0][2]) {
						IMDCT_36(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][2]);					
						FormatLongWindow(decoder_cfg.spectral_data, (decoder_cfg.active_cfg[0].block_type[0]!=2)?window_table[decoder_cfg.active_cfg[0].block_type[0]]:window_table[0], decoder_cfg.active_cfg[0].region_addr[0][2], previous_frame_delta + 3*MP3_GRANULE_SIZE);
					}
					if ((decoder_cfg.active_cfg[0].block_type[0] == 2) && (decoder_cfg.active_cfg[0].region_addr[0][2] <= 36) && (decoder_cfg.active_cfg[0].region_addr[0][3] > decoder_cfg.active_cfg[0].region_addr[0][2])) {
						IMDCT_12(decoder_cfg.spectral_data + decoder_cfg.active_cfg[0].region_addr[0][2]*2, decoder_cfg.active_cfg[0].region_addr[0][3] - decoder_cfg.active_cfg[0].region_addr[0][2]);
						FormatShortWindow(decoder_cfg.spectral_data + decoder_cfg.active_cfg[0].region_addr[0][2]*2, window_table[2], decoder_cfg.active_cfg[0].region_addr[0][3] - decoder_cfg.active_cfg[0].region_addr[0][2], previous_frame_delta + 3*MP3_GRANULE_SIZE);
					}

					if (decoder_cfg.active_cfg[0].region_addr[1][2]) {
						IMDCT_36(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[0].region_addr[1][2]);
						FormatLongWindow(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, (decoder_cfg.active_cfg[0].block_type[1]!=2)?window_table[decoder_cfg.active_cfg[0].block_type[1]]:window_table[0], decoder_cfg.active_cfg[0].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}
					if ((decoder_cfg.active_cfg[0].block_type[1] == 2) && (decoder_cfg.active_cfg[0].region_addr[1][2] <= 36) && (decoder_cfg.active_cfg[0].region_addr[1][3] > decoder_cfg.active_cfg[0].region_addr[1][2])) {
						IMDCT_12(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[0].region_addr[1][2]*2, decoder_cfg.active_cfg[0].region_addr[1][3] - decoder_cfg.active_cfg[0].region_addr[1][2]);
						FormatShortWindow(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[0].region_addr[1][2]*2, window_table[2], decoder_cfg.active_cfg[0].region_addr[1][3] - decoder_cfg.active_cfg[0].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}

					Synthesize(decoder_cfg.spectral_data, previous_frame_delta);
				} else {
					System::MemoryFill_SSE3(decoder_cfg.spectral_data, MP3_CHANNEL_SIZE*sizeof(float), 0, 0);
				}



				active_byte = reinterpret_cast<UI_64>(decoder_cfg.src_ptr) - reinterpret_cast<UI_64>(decoder_cfg.packet_ptr);
				active_bit = 8 - decoder_cfg.bit_mask[3];

				if ((active_byte + 16384) > MP3_MAX_PACKET*8192) {
					packet_offset -= active_byte;
					System::MemoryCopy(decoder_cfg.packet_ptr, decoder_cfg.src_ptr, packet_offset);
					active_byte = 0;
				}
		
			
				if (++active_packet >= MP3_MAX_PACKET) active_packet = 0;

			} else {
				// return something			

			}
		}
	} else {
		decoder_cfg.frame_cfg->source_chan_count = 2;

		decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
		decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

		decoder_cfg.GetNumber(3); // private




		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[0] = decoder_cfg.current_cfg[0].sfb_common[1] = decoder_cfg.current_cfg[0].sfb_common[2] = decoder_cfg.current_cfg[0].sfb_common[3] = decoder_cfg.current_cfg[0].sfb_common[4] = decoder_cfg.current_cfg[0].sfb_common[5] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[6] = decoder_cfg.current_cfg[0].sfb_common[7] = decoder_cfg.current_cfg[0].sfb_common[8] = decoder_cfg.current_cfg[0].sfb_common[9] = decoder_cfg.current_cfg[0].sfb_common[10] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[11] = decoder_cfg.current_cfg[0].sfb_common[12] = decoder_cfg.current_cfg[0].sfb_common[13] = decoder_cfg.current_cfg[0].sfb_common[14] = decoder_cfg.current_cfg[0].sfb_common[15] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].sfb_common[16] = decoder_cfg.current_cfg[0].sfb_common[17] = decoder_cfg.current_cfg[0].sfb_common[18] = decoder_cfg.current_cfg[0].sfb_common[19] = decoder_cfg.current_cfg[0].sfb_common[20] = 1;
		}


		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].sfb_common[0] = decoder_cfg.current_cfg[1].sfb_common[1] = decoder_cfg.current_cfg[1].sfb_common[2] = decoder_cfg.current_cfg[1].sfb_common[3] = decoder_cfg.current_cfg[1].sfb_common[4] = decoder_cfg.current_cfg[1].sfb_common[5] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].sfb_common[6] = decoder_cfg.current_cfg[1].sfb_common[7] = decoder_cfg.current_cfg[1].sfb_common[8] = decoder_cfg.current_cfg[1].sfb_common[9] = decoder_cfg.current_cfg[1].sfb_common[10] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].sfb_common[11] = decoder_cfg.current_cfg[1].sfb_common[12] = decoder_cfg.current_cfg[1].sfb_common[13] = decoder_cfg.current_cfg[1].sfb_common[14] = decoder_cfg.current_cfg[1].sfb_common[15] = 1;
		}
		if (decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].sfb_common[16] = decoder_cfg.current_cfg[1].sfb_common[17] = decoder_cfg.current_cfg[1].sfb_common[18] = decoder_cfg.current_cfg[1].sfb_common[19] = decoder_cfg.current_cfg[1].sfb_common[20] = 1;
		}

// granule 0
		decoder_cfg.current_cfg[0].part2_3l[0] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[0].region_addr[0][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[0].global_gain[0] = decoder_cfg.GetNumber(8) - 10;
		if (xc_mode & 2) decoder_cfg.current_cfg[0].global_gain[0] -= 2;


		decoder_cfg.current_cfg[0].sf_compress[0] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[0].block_split_flag[0] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].block_type[0] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[0].block_type[0] == 2) {
				decoder_cfg.current_cfg[0].region_addr[0][1] = 3;
				decoder_cfg.current_cfg[0].region_addr[0][2] = 3;
			} else {
				decoder_cfg.current_cfg[0].region_addr[0][1] = 8;
				decoder_cfg.current_cfg[0].region_addr[0][2] = 8;
			}
			
			
			decoder_cfg.current_cfg[0].switch_point[0] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[0].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][1] = decoder_cfg.current_cfg[0].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].subb_gain[0][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[0][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[0][2] = (decoder_cfg.GetNumber(3)<<3);

		} else {
			decoder_cfg.current_cfg[0].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].region_addr[0][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[0].region_addr[0][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[0].region_addr[0][3]) {
				decoder_cfg.current_cfg[0].region_addr[0][1] += 1;
				decoder_cfg.current_cfg[0].region_addr[0][2] += decoder_cfg.current_cfg[0].region_addr[0][1] + 1;
			}

		}
		

		decoder_cfg.current_cfg[0].pre_flag[0] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[0].sf_scale[0] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[0].table_select[0][3] = decoder_cfg.NextBit();



		decoder_cfg.current_cfg[1].part2_3l[0] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[1].region_addr[0][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[1].global_gain[0] = decoder_cfg.GetNumber(8) - 10;
		if (xc_mode & 2) decoder_cfg.current_cfg[1].global_gain[0] -= 2;


		decoder_cfg.current_cfg[1].sf_compress[0] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[1].block_split_flag[0] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].block_type[0] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[1].block_type[0] == 2) {
				decoder_cfg.current_cfg[1].region_addr[0][1] = 3;
				decoder_cfg.current_cfg[1].region_addr[0][2] = 3;
			} else {
				decoder_cfg.current_cfg[1].region_addr[0][1] = 8;
				decoder_cfg.current_cfg[1].region_addr[0][2] = 8;
			}			
			
			decoder_cfg.current_cfg[1].switch_point[0] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[1].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[0][1] = decoder_cfg.current_cfg[1].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[1].subb_gain[0][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[1].subb_gain[0][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[1].subb_gain[0][2] = (decoder_cfg.GetNumber(3)<<3);

		} else {
			decoder_cfg.current_cfg[1].table_select[0][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[0][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[0][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[1].region_addr[0][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[1].region_addr[0][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[1].region_addr[0][3]) {
				decoder_cfg.current_cfg[1].region_addr[0][1] += 1;
				decoder_cfg.current_cfg[1].region_addr[0][2] += decoder_cfg.current_cfg[1].region_addr[0][1] + 1;
			}
		}


		decoder_cfg.current_cfg[1].pre_flag[0] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[1].sf_scale[0] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[1].table_select[0][3] = decoder_cfg.NextBit();


// granule 1
		decoder_cfg.current_cfg[0].part2_3l[1] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[0].region_addr[1][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[0].global_gain[1] = decoder_cfg.GetNumber(8) - 10;
		if (xc_mode & 2) decoder_cfg.current_cfg[0].global_gain[1] -= 2;


		decoder_cfg.current_cfg[0].sf_compress[1] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[0].block_split_flag[1] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[0].block_type[1] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[0].block_type[1] == 2) {
				decoder_cfg.current_cfg[0].region_addr[1][1] = 3;
				decoder_cfg.current_cfg[0].region_addr[1][2] = 3;
			} else {
				decoder_cfg.current_cfg[0].region_addr[1][1] = 8;
				decoder_cfg.current_cfg[0].region_addr[1][2] = 8;
			}
			

			decoder_cfg.current_cfg[0].switch_point[1] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[0].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][1] = decoder_cfg.current_cfg[0].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].subb_gain[1][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[1][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[0].subb_gain[1][2] = (decoder_cfg.GetNumber(3)<<3);



		} else {
			decoder_cfg.current_cfg[0].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[0].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[0].region_addr[1][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[0].region_addr[1][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[0].region_addr[1][3]) {
				decoder_cfg.current_cfg[0].region_addr[1][1] += 1;
				decoder_cfg.current_cfg[0].region_addr[1][2] += decoder_cfg.current_cfg[0].region_addr[1][1] + 1;
			}
		}


		decoder_cfg.current_cfg[0].pre_flag[1] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[0].sf_scale[1] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[0].table_select[1][3] = decoder_cfg.NextBit();


		decoder_cfg.current_cfg[1].part2_3l[1] = decoder_cfg.GetNumber(12);
		decoder_cfg.current_cfg[1].region_addr[1][3] = (decoder_cfg.GetNumber(9)<<1);
		decoder_cfg.current_cfg[1].global_gain[1] = decoder_cfg.GetNumber(8) - 10;
		if (xc_mode & 2) decoder_cfg.current_cfg[1].global_gain[1] -= 2;


		decoder_cfg.current_cfg[1].sf_compress[1] = decoder_cfg.GetNumber(4);
		
		if (decoder_cfg.current_cfg[1].block_split_flag[1] = decoder_cfg.NextBit()) {
			decoder_cfg.current_cfg[1].block_type[1] = decoder_cfg.GetNumber(2);
			if (decoder_cfg.current_cfg[1].block_type[1] == 2) {
				decoder_cfg.current_cfg[1].region_addr[1][1] = 3;
				decoder_cfg.current_cfg[1].region_addr[1][2] = 3;
			} else {
				decoder_cfg.current_cfg[1].region_addr[1][1] = 8;
				decoder_cfg.current_cfg[1].region_addr[1][2] = 8;
			}
			

			decoder_cfg.current_cfg[1].switch_point[1] = decoder_cfg.NextBit();

			decoder_cfg.current_cfg[1].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[1][1] = decoder_cfg.current_cfg[1].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[1].subb_gain[1][0] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[1].subb_gain[1][1] = (decoder_cfg.GetNumber(3)<<3);
			decoder_cfg.current_cfg[1].subb_gain[1][2] = (decoder_cfg.GetNumber(3)<<3);



		} else {
			decoder_cfg.current_cfg[1].table_select[1][0] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[1][1] = decoder_cfg.GetNumber(5);
			decoder_cfg.current_cfg[1].table_select[1][2] = decoder_cfg.GetNumber(5);

			decoder_cfg.current_cfg[1].region_addr[1][1] = decoder_cfg.GetNumber(4);
			decoder_cfg.current_cfg[1].region_addr[1][2] = decoder_cfg.GetNumber(3);

			if (decoder_cfg.current_cfg[1].region_addr[1][3]) {
				decoder_cfg.current_cfg[1].region_addr[1][1] += 1;
				decoder_cfg.current_cfg[1].region_addr[1][2] += decoder_cfg.current_cfg[1].region_addr[1][1] + 1;
			}
		}


		decoder_cfg.current_cfg[1].pre_flag[1] = -decoder_cfg.NextBit();
		decoder_cfg.current_cfg[1].sf_scale[1] = decoder_cfg.NextBit() + 1;
		decoder_cfg.current_cfg[1].table_select[1][3] = decoder_cfg.NextBit();



// main		
		decoder_cfg.current_cfg[0].byte_align = packet_offset - m_end;
		
		x_val -= 32; // main size
		System::MemoryCopy(decoder_cfg.packet_ptr + packet_offset, decoder_cfg.src_ptr, x_val);
		

		m_size = decoder_cfg.active_cfg[0].part2_3l[0] + decoder_cfg.active_cfg[0].part2_3l[1] + decoder_cfg.active_cfg[1].part2_3l[0] + decoder_cfg.active_cfg[1].part2_3l[1];

		if (decoder_cfg.active_cfg[0].byte_align != -1) {
			active_byte = decoder_cfg.active_cfg[0].byte_align;
			active_bit = 0;
		}
			

		if (m_size) {
			packet_offset += x_val;
			x_val = packet_offset - active_byte;

			if (m_size <= ((x_val)<<3)) {
				// set decoder pointer
				decoder_cfg.SetSource(decoder_cfg.packet_ptr + active_byte, x_val);
				if (active_bit) decoder_cfg.GetNumber(active_bit);

			
				GetGranule(0, 0);
				GetGranule(1, 0);

				GetGranule(0, 1);
				GetGranule(1, 1);
						
				if (((decoder_cfg.active_cfg[0].part2_3l[0] | decoder_cfg.active_cfg[0].part2_3l[1] | decoder_cfg.active_cfg[1].part2_3l[0] | decoder_cfg.active_cfg[1].part2_3l[1]) == 0)) {

					Rescale(decoder_cfg.spectral_data, 18*2);
					Rescale(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, 18*2);

					if (decoder_cfg.active_cfg[0].block_type[0] == 2) {
						Reorder(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][1]);
					}
					if (decoder_cfg.active_cfg[0].block_type[1] == 2) {
						Reorder(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[0].region_addr[1][1]);
					}
					if (decoder_cfg.active_cfg[1].block_type[0] == 2) {
						Reorder(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, decoder_cfg.active_cfg[1].region_addr[0][1]);
					}
					if (decoder_cfg.active_cfg[1].block_type[1] == 2) {
						Reorder(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[1].region_addr[1][1]);
					}

					if (xc_mode & 2) { // MS
						MSAdjust(decoder_cfg.spectral_data);

					}

					if (xc_mode & 1) { // intensity
						IntesityAdjust(decoder_cfg.spectral_data);

					}
				
					if (xc_mode) {
						if (decoder_cfg.active_cfg[0].region_addr[0][2] > decoder_cfg.active_cfg[1].region_addr[0][2]) decoder_cfg.active_cfg[1].region_addr[0][2] = decoder_cfg.active_cfg[0].region_addr[0][2];
						else decoder_cfg.active_cfg[0].region_addr[0][2] = decoder_cfg.active_cfg[1].region_addr[0][2];
						if (decoder_cfg.active_cfg[0].region_addr[1][2] > decoder_cfg.active_cfg[1].region_addr[1][2]) decoder_cfg.active_cfg[1].region_addr[1][2] = decoder_cfg.active_cfg[0].region_addr[1][2];
						else decoder_cfg.active_cfg[0].region_addr[1][2] = decoder_cfg.active_cfg[1].region_addr[1][2];

						if (decoder_cfg.active_cfg[0].region_addr[0][3] > decoder_cfg.active_cfg[1].region_addr[0][3]) decoder_cfg.active_cfg[1].region_addr[0][3] = decoder_cfg.active_cfg[0].region_addr[0][3];
						else decoder_cfg.active_cfg[0].region_addr[0][3] = decoder_cfg.active_cfg[1].region_addr[0][3];
						if (decoder_cfg.active_cfg[0].region_addr[1][3] > decoder_cfg.active_cfg[1].region_addr[1][3]) decoder_cfg.active_cfg[1].region_addr[1][3] = decoder_cfg.active_cfg[0].region_addr[1][3];
						else decoder_cfg.active_cfg[0].region_addr[1][3] = decoder_cfg.active_cfg[1].region_addr[1][3];

					}



					AliasReduction(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][3], decoder_cfg.active_cfg[0].region_addr[1][3]);
					AliasReduction(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, decoder_cfg.active_cfg[1].region_addr[0][3], decoder_cfg.active_cfg[1].region_addr[1][3]);



					if (decoder_cfg.active_cfg[0].region_addr[0][2]) {
						IMDCT_36(decoder_cfg.spectral_data, decoder_cfg.active_cfg[0].region_addr[0][2]);
						FormatLongWindow(decoder_cfg.spectral_data, (decoder_cfg.active_cfg[0].block_type[0]!=2)?window_table[decoder_cfg.active_cfg[0].block_type[0]]:window_table[0], decoder_cfg.active_cfg[0].region_addr[0][2], previous_frame_delta + MP3_GRANULE_SIZE*3);
					}
					if ((decoder_cfg.active_cfg[0].block_type[0] == 2) && (decoder_cfg.active_cfg[0].region_addr[0][2] <= 36) && (decoder_cfg.active_cfg[0].region_addr[0][3] > decoder_cfg.active_cfg[0].region_addr[0][2])) {
						IMDCT_12(decoder_cfg.spectral_data + decoder_cfg.active_cfg[0].region_addr[0][2]*2, decoder_cfg.active_cfg[0].region_addr[0][3] - decoder_cfg.active_cfg[0].region_addr[0][2]);
						FormatShortWindow(decoder_cfg.spectral_data + decoder_cfg.active_cfg[0].region_addr[0][2]*2, window_table[2], decoder_cfg.active_cfg[0].region_addr[0][3] - decoder_cfg.active_cfg[0].region_addr[0][2], previous_frame_delta + MP3_GRANULE_SIZE*3);
					}


					if (decoder_cfg.active_cfg[0].region_addr[1][2]) {
						IMDCT_36(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[0].region_addr[1][2]);
						FormatLongWindow(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2, (decoder_cfg.active_cfg[0].block_type[1]!=2)?window_table[decoder_cfg.active_cfg[0].block_type[1]]:window_table[0], decoder_cfg.active_cfg[0].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}
					if ((decoder_cfg.active_cfg[0].block_type[1] == 2) && (decoder_cfg.active_cfg[0].region_addr[1][2] <= 36) && (decoder_cfg.active_cfg[0].region_addr[1][3] > decoder_cfg.active_cfg[0].region_addr[1][2])) {
						IMDCT_12(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[0].region_addr[1][2]*2, decoder_cfg.active_cfg[0].region_addr[1][3] - decoder_cfg.active_cfg[0].region_addr[1][2]);
						FormatShortWindow(decoder_cfg.spectral_data + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[0].region_addr[1][2]*2, window_table[2], decoder_cfg.active_cfg[0].region_addr[1][3] - decoder_cfg.active_cfg[0].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}

					Synthesize(decoder_cfg.spectral_data, previous_frame_delta);



					if (decoder_cfg.active_cfg[1].region_addr[0][2]) {
						IMDCT_36(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, decoder_cfg.active_cfg[1].region_addr[0][2]);
						FormatLongWindow(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, (decoder_cfg.active_cfg[1].block_type[0]!=2)?window_table[decoder_cfg.active_cfg[1].block_type[0]]:window_table[0], decoder_cfg.active_cfg[1].region_addr[0][2], previous_frame_delta + MP3_GRANULE_SIZE*3);
					}
					if ((decoder_cfg.active_cfg[1].block_type[0] == 2) &&(decoder_cfg.active_cfg[1].region_addr[0][2] <= 36) && (decoder_cfg.active_cfg[1].region_addr[0][3] > decoder_cfg.active_cfg[1].region_addr[0][2])) {
						IMDCT_12(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + decoder_cfg.active_cfg[1].region_addr[0][2]*2, decoder_cfg.active_cfg[1].region_addr[0][3] - decoder_cfg.active_cfg[1].region_addr[0][2]);
						FormatShortWindow(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + decoder_cfg.active_cfg[1].region_addr[0][2]*2, window_table[2], decoder_cfg.active_cfg[1].region_addr[0][3] - decoder_cfg.active_cfg[1].region_addr[0][2], previous_frame_delta + MP3_GRANULE_SIZE*3);
					}

					if (decoder_cfg.active_cfg[1].region_addr[1][2]) {
						IMDCT_36(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + MP3_GRANULE_SIZE*2, decoder_cfg.active_cfg[1].region_addr[1][2]);
						FormatLongWindow(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + MP3_GRANULE_SIZE*2, (decoder_cfg.active_cfg[1].block_type[1]!=2)?window_table[decoder_cfg.active_cfg[1].block_type[1]]:window_table[0], decoder_cfg.active_cfg[1].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}
					if ((decoder_cfg.active_cfg[1].block_type[1] == 2) && (decoder_cfg.active_cfg[1].region_addr[1][2] <= 36) && (decoder_cfg.active_cfg[1].region_addr[1][3] > decoder_cfg.active_cfg[1].region_addr[1][2])) {
						IMDCT_12(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[1].region_addr[1][2]*2, decoder_cfg.active_cfg[1].region_addr[1][3] - decoder_cfg.active_cfg[1].region_addr[1][2]);
						FormatShortWindow(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE + MP3_GRANULE_SIZE*2 + decoder_cfg.active_cfg[1].region_addr[1][2]*2, window_table[2], decoder_cfg.active_cfg[1].region_addr[1][3] - decoder_cfg.active_cfg[1].region_addr[1][2], -(UI_64)MP3_GRANULE_SIZE);
					}				
								
					Synthesize(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, previous_frame_delta);

				} else {
					System::MemoryFill_SSE3(decoder_cfg.spectral_data, 2*MP3_CHANNEL_SIZE*sizeof(float), 0, 0);

				}


				active_byte = reinterpret_cast<UI_64>(decoder_cfg.src_ptr) - reinterpret_cast<UI_64>(decoder_cfg.packet_ptr);
				active_bit = 8 - decoder_cfg.bit_mask[3];

				if ((active_byte + 16384) > MP3_MAX_PACKET*8192) {
					packet_offset -= active_byte;
					System::MemoryCopy(decoder_cfg.packet_ptr, decoder_cfg.src_ptr, packet_offset);
					active_byte = 0;
				}		
			
				if (++active_packet >= MP3_MAX_PACKET) active_packet = 0;


			} else {
				// probably error, return something

			}
		}
	}

	
	return result;

}


void Audio::MP3::Reorder(float * sp_ptr, int start_band) {
	int * xyz_ptr(0), * abc_ptr(0), a(0), b(0), c(0), x(0), y(0), z(0);


	x = sfb_offset_short[sf_idx][start_band][0];
	a = 0;
	

	xyz_ptr = reinterpret_cast<int *>(sp_ptr);
	abc_ptr = xyz_ptr + 4;



	for (int j_6(0);;a++, j_6++) {

		if (a >= sfb_offset_short[sf_idx][start_band][1]) {			
			++start_band;
			a = 0;
		}

		if (j_6 >= 6) {
			j_6 = 0;
			x += 12;
		}

		if (x >= MP3_GRANULE_SIZE) break;

		c = sfb_offset_short[sf_idx][start_band][0] + a;
		z = x++;
		b = ((c & 0xFFFFFFFC)<<1) | (c&3);
		y = ((z & 0xFFFFFFFC)<<1) | (z&3);		
		xyz_ptr[y] = abc_ptr[b];

		c += sfb_offset_short[sf_idx][start_band][1];
		z += 6;		
		b = ((c & 0xFFFFFFFC)<<1) | (c&3);
		y = ((z & 0xFFFFFFFC)<<1) | (z&3);		
		xyz_ptr[y] = abc_ptr[b];

		c += sfb_offset_short[sf_idx][start_band][1];
		z += 6;		
		b = ((c & 0xFFFFFFFC)<<1) | (c&3);
		y = ((z & 0xFFFFFFFC)<<1) | (z&3);
		xyz_ptr[y] = abc_ptr[b];

		

	}
}


// ===============================================================================================================================================
const short Audio::MP3::pre_tab[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 2, 0};

__declspec(align(16)) const float Audio::MP3::alias_cofs[] = { // cs, ca, ca, cs
	0.999993f, -0.00369997f,
	0.999899f, -0.0141986f,
	0.999161f, -0.0409656f,
	0.995518f, -0.0945742f,
	0.983315f, -0.181913f,
	0.949629f, -0.313377f,
	0.881742f, -0.471732f,
	0.857493f, -0.514496f	
};

const unsigned char Audio::MP3::sf_III_size[16][2] = {
	0, 0,
	0, 1,
	0, 2,
	0, 3,
	3, 0,
	1, 1,
	1, 2,
	1, 3,
	2, 1,
	2, 2,
	2, 3,
	3, 1,
	3, 2,
	3, 3,
	4, 2,
	4, 3
};


const unsigned short Audio::MP3::sfb_offset_long[4][24][2] = {
	{
		{0, 4},
		{4, 4},
		{8, 4},
		{12, 4},

		{16, 4},
		{20, 4},
		{24, 6},
		{30, 6},

		{36, 8},
		{44, 8},
		{52, 10},
		{62, 12},

		{74, 16},
		{90, 20},
		{110, 24},
		{134, 28},

		{162, 34},
		{196, 42},
		{238, 50},
		{288, 54},

		{342, 76},
		{418, 158},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},

	},

	{
		{0, 4},
		{4, 4},
		{8, 4},
		{12, 4},

		{16, 4},
		{20, 4},
		{24, 4},
		{30, 6},

		{36, 6},
		{42, 8},
		{50, 10},
		{60, 12},

		{72, 16},
		{88, 18},
		{106, 22},
		{128, 28},

		{156, 34},
		{190, 40},
		{230, 46},
		{276, 54},

		{330, 54},
		{384, 192},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},

	},

	{
		{0, 4},
		{4, 4},
		{8, 4},
		{12, 4},

		{16, 4},
		{20, 4},
		{24, 6},
		{30, 6},

		{36, 8},
		{44, 10},
		{54, 12},
		{66, 16},

		{82, 20},
		{102, 24},
		{126, 30},
		{156, 38},

		{194, 46},
		{240, 56},
		{296, 68},
		{364, 84},

		{448, 102},
		{550, 26},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},

	},

	{
		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

	}

};

const unsigned short Audio::MP3::sfb_offset_short[4][16][2] = {
	{
		{0, 4},
		{12, 4},
		{24, 4},
		{36, 4},

		{48, 6},
		{66, 8},
		{90, 10},
		{120, 12},

		{156, 14},
		{198, 18},
		{252, 22},
		{318, 30},
		
		{408, 56},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},
		{0, 0}
	},

	{
		{0, 4},
		{12, 4},
		{24, 4},
		{36, 4},

		{48, 6},
		{66, 6},
		{84, 10},
		{114, 12},

		{150, 14},
		{192, 16},
		{240, 20},
		{300, 26},

		{378, 66},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},
		{0, 0}

	},

	{
		{0, 4},
		{12, 4},
		{24, 4},
		{36, 4},

		{48, 6},
		{66, 8},
		{90, 12},
		{126, 16},

		{174, 20},
		{234, 26},
		{312, 34},
		{414, 42},

		{540, 12},
		{MP3_GRANULE_SIZE, 0},
		{0, 0},
		{0, 0}
	},

	{
		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0},

		{0, 0},
		{0, 0},
		{0, 0},
		{0, 0}
	}

};

const unsigned int Audio::MP3::hct_cfg[] = {
		0, 0, 0, 0,
		0, 4, 0, 0,
		7, 9, 0, 0,
		22, 9, 0, 0,
		37, 16, 0, 0,
		37+22, 16, 0, 0,
		61+22, 16, 0, 0,
		84+22, 36, 0, 0,
		130+22, 36, 0, 0,
		177+22, 36, 0, 0,
		222+22, 64, 0, 0,
		297+22, 64, 0, 0,
		372+22, 64, 0, 0,
		446+22, 256, 0, 0,
		0, 0, 0, 0,
		721+22, 256, 0, 0,

		990+22, 256, 1, 0,
		990+22, 256, 2, 0,
		990+22, 256, 3, 0,
		990+22, 256, 4, 0,
		990+22, 256, 6, 0,
		990+22, 256, 8, 0,
		990+22, 256, 10, 0,
		990+22, 256, 13, 0,

		1263+22, 256, 4, 0,
		1263+22, 256, 5, 0,
		1263+22, 256, 6, 0,
		1263+22, 256, 7, 0,
		1263+22, 256, 8, 0,
		1263+22, 256, 9, 0,
		1263+22, 256, 11, 0,
		1263+22, 256, 13, 0

};

const short Audio::MP3::Huffman_Tab[] = {
	1, 1, 0, 0,
		1, 0, 0, 0,
	1, 2, 0, 0,	
		1, 1, 0, 0,
	2, 3, 0, 0,
		1, 0, 1, 0,
		0, 1, 1, 0,


	1, 1, 0, 0,
		1, 0, 0, 0,
	0, 2, 0, 0,
	3, 3, 0, 0,
		2, 0, 1, 0,
		3, 1, 0, 0,
		1, 1, 1, 0,
	0, 4, 0, 0,
	3, 5, 0, 0,
		1, 1, 2, 0,
		3, 2, 0, 0,
		2, 2, 1, 0,
	2, 6, 0, 0,
		1, 0, 2, 0,
		0, 2, 2, 0,
	
		
	0, 1, 0, 0,
	3, 2, 0, 0,
		3, 0, 0, 0,
		2, 0, 1, 0,
		1, 1, 1, 0,
	1, 3, 0, 0,
		1, 1, 0, 0,	
	0, 4, 0, 0,
	3, 5, 0, 0,
		1, 1, 2, 0,
		3, 2, 0, 0,
		2, 2, 1, 0,
	2, 6, 0, 0,
		1, 0, 2, 0,
		0, 2, 2, 0,
	
		
	1, 1, 0, 0,
		1, 0, 0, 0,
	0, 2, 0, 0,
	0, 3, 0, 0,
	4, 4, 0, 0,
		5, 1, 0, 0,
		4, 2, 0, 0,
		6, 4, 0, 0,
		7, 8, 0, 0,
	5, 5, 0, 0,
		5, 3, 0, 0,
		4, 6, 0, 0,
		3, 9, 0, 0,
		6, 10, 0, 0,
		7, 12, 0, 0,
	6, 6, 0, 0,
		5, 5, 0, 0,
		4, 7, 0, 0,
		0, 11, 0, 0,
		2, 13, 0, 0,
		3, 14, 0, 0,
		1, 15, 0, 0,



	1, 1, 0, 0,
		1, 0, 0, 0,	
	0, 2, 0, 0,
	3, 3, 0, 0,
		2, 0, 1, 0,
		3, 1, 0, 0,
		1, 1, 1, 0,	
	0, 4, 0, 0,
	0, 5, 0, 0,
	5, 6, 0, 0,
		6, 0, 2, 0,
		4, 1, 2, 0,
		7, 2, 0, 0,
		5, 2, 1, 0,
		1, 3, 1, 0,
	5, 7, 0, 0,
		5, 0, 3, 0,
		4, 1, 3, 0,
		7, 2, 2, 0,
		6, 3, 0, 0,
		1, 3, 2, 0,
	2, 8, 0, 0,
		1, 2, 3, 0,
		0, 3, 3, 0,
	
		
	0, 1, 0, 0,
	1, 2, 0, 0,
		2, 1, 1, 0,
	3, 3, 0, 0,
		7, 0, 0, 0,
		3, 0, 1, 0,
		6, 1, 0, 0,
	3, 4, 0, 0,
		3, 1, 2, 0,
		5, 2, 0, 0,
		4, 2, 1, 0,
	4, 5, 0, 0,
		5, 0, 2, 0,
		2, 1, 3, 0,
		4, 2, 2, 0,
		3, 3, 1, 0,
	3, 6, 0, 0,
		1, 2, 3, 0,
		3, 3, 0, 0,
		2, 3, 2, 0,
	2, 7, 0, 0,
		1, 0, 3, 0,
		0, 3, 3, 0,
	


	1, 1, 0, 0,	
		1, 0, 0, 0,	
	0, 2, 0, 0,
	2, 3, 0, 0,
		2, 0, 1, 0,
		3, 1, 0, 0,
	1, 4, 0, 0,
		3, 1, 1, 0,
	1, 5, 0, 0,
		4, 2, 1, 0,
	3, 6, 0, 0,
		10, 0, 2, 0,
		7, 1, 2, 0,
		11, 2, 0, 0,
	7, 7, 0, 0,
		10, 1, 3, 0,
		5, 1, 4, 0,
		13, 2, 2, 0,
		12, 3, 0, 0,
		11, 3, 1, 0,
		7, 4, 0, 0,
		6, 4, 1, 0,
	9, 8, 0, 0,
		19, 0, 3, 0,
		16, 0, 4, 0,
		3, 1, 5, 0,
		17, 2, 3, 0,
		8, 2, 4, 0,
		18, 3, 2, 0,
		9, 4, 2, 0,
		6, 5, 0, 0,
		4, 5, 1, 0,
	8, 9, 0, 0,
		10, 0, 5, 0,
		4, 2, 5, 0,
		15, 3, 3, 0,
		11, 3, 4, 0,
		2, 3, 5, 0,
		14, 4, 3, 0,
		3, 4, 4, 0,
		5, 5, 2, 0,
	4, 10, 0, 0,
		1, 4, 5, 0,
		3, 5, 3, 0,
		2, 5, 4, 0,
		0, 5, 5, 0,
	
		
	0, 1, 0, 0,
	2, 2, 0, 0,
		3, 0, 0, 0,
		1, 1, 1, 0,
	2, 3, 0, 0,
		4, 0, 1, 0,
		5, 1, 0, 0,
	2, 4, 0, 0,
		2, 1, 2, 0,
		3, 2, 1, 0,	
	0, 5, 0, 0,
	3, 6, 0, 0,
		6, 0, 2, 0,
		7, 2, 0, 0,
		5, 2, 2, 0,
	1, 7, 0, 0,
		5, 4, 1, 0,
	13, 8, 0, 0,
		18, 0, 3, 0,
		12, 0, 4, 0,
		16, 1, 3, 0,
		9, 1, 4, 0,
		3, 1, 5, 0,
		14, 2, 3, 0,
		7, 2, 4, 0,
		19, 3, 0, 0,
		17, 3, 1, 0,
		15, 3, 2, 0,
		13, 4, 0, 0,
		8, 4, 2, 0,
		4, 5, 1, 0,
	8, 9, 0, 0,
		5, 0, 5, 0,
		3, 2, 5, 0,
		13, 3, 3, 0,
		10, 3, 4, 0,
		11, 4, 3, 0,
		12, 5, 0, 0,
		4, 5, 2, 0,
		1, 5, 3, 0,
	3, 10, 0, 0,
		4, 3, 5, 0,
		5, 4, 4, 0,
		1, 4, 5, 0,
	2, 11, 0, 0,
		1, 5, 4, 0,
		0, 5, 5, 0,
	
		
	0, 1, 0, 0,
	0, 2, 0, 0,
	4, 3, 0, 0,
		7, 0, 0, 0,
		5, 0, 1, 0,
		6, 1, 0, 0,
		4, 1, 1, 0,
	3, 4, 0, 0,
		5, 1, 2, 0,
		7, 2, 0, 0,
		6, 2, 1, 0,
	4, 5, 0, 0,
		9, 0, 2, 0,
		5, 1, 3, 0,
		8, 2, 2, 0,
		6, 3, 1, 0,
	6, 6, 0, 0,
		14, 0, 3, 0,
		6, 1, 4, 0,
		8, 2, 3, 0,
		15, 3, 0, 0,
		9, 3, 2, 0,
		7, 4, 1, 0,
	7, 7, 0, 0,
		8, 2, 4, 0,
		10, 3, 3, 0,
		5, 3, 4, 0,
		11, 4, 0, 0,
		9, 4, 2, 0,
		6, 4, 3, 0,
		4, 5, 1, 0,
	8, 8, 0, 0,
		15, 0, 4, 0,
		7, 1, 5, 0,
		5, 2, 5, 0,
		1, 3, 5, 0,
		4, 4, 4, 0,
		14, 5, 0, 0,
		6, 5, 2, 0,
		2, 5, 3, 0,
	4, 9, 0, 0,
		7, 0, 5, 0,
		1, 4, 5, 0,
		6, 5, 4, 0,
		0, 5, 5, 0,
	
	
	1, 1, 0, 0,
		1, 0, 0, 0,	
	0, 2, 0, 0,
	2, 3, 0, 0,
		2, 0, 1, 0,
		3, 1, 0, 0,
	1, 4, 0, 0,
		3, 1, 1, 0,	
	0, 5, 0, 0,
	4, 6, 0, 0,
		10, 0, 2, 0,
		8, 1, 2, 0,
		11, 2, 0, 0,
		9, 2, 1, 0,
	4, 7, 0, 0,
		12, 1, 3, 0,
		15, 2, 2, 0,
		14, 3, 0, 0,
		13, 3, 1, 0,
	11, 8, 0, 0,
		23, 0, 3, 0,
		18, 1, 4, 0,
		12, 1, 6, 0,
		7, 1, 7, 0,
		21, 2, 3, 0,
		22, 3, 2, 0,
		20, 4, 0, 0,
		19, 4, 1, 0,
		14, 6, 0, 0,
		13, 6, 1, 0,
		8, 7, 1, 0,
	15, 9, 0, 0,
		35, 0, 4, 0,
		30, 0, 5, 0,
		12, 0, 6, 0,
		21, 1, 5, 0,
		32, 2, 4, 0,
		19, 2, 6, 0,
		6, 2, 7, 0,
		34, 3, 3, 0,
		18, 3, 6, 0,
		33, 4, 2, 0,
		31, 5, 0, 0,
		22, 5, 1, 0,
		10, 6, 2, 0,
		9, 7, 0, 0,
		7, 7, 2, 0,
	18, 10, 0, 0,
		17, 0, 7, 0,
		40, 2, 5, 0,
		46, 3, 4, 0,
		23, 3, 5, 0,
		7, 3, 7, 0,
		47, 4, 3, 0,
		27, 4, 4, 0,
		22, 4, 5, 0,
		9, 4, 6, 0,
		3, 4, 7, 0,
		41, 5, 2, 0,
		26, 5, 3, 0,
		5, 5, 6, 0,
		11, 6, 3, 0,
		16, 6, 4, 0,
		6, 6, 5, 0,
		8, 7, 3, 0,
		4, 7, 4, 0,
	8, 11, 0, 0,
		21, 5, 4, 0,
		20, 5, 5, 0,
		3, 5, 7, 0,
		5, 6, 6, 0,
		1, 6, 7, 0,
		4, 7, 5, 0,
		2, 7, 6, 0,
		0, 7, 7, 0,
	
		
	0, 1, 0, 0,
	1, 2, 0, 0,
		3, 0, 0, 0,
	3, 3, 0, 0,
		4, 0, 1, 0,
		5, 1, 0, 0,
		3, 1, 1, 0,
	1, 4, 0, 0,
		4, 1, 2, 0,
	3, 5, 0, 0,
		10, 0, 2, 0,
		11, 2, 0, 0,
		7, 2, 1, 0,
	3, 6, 0, 0,
		10, 1, 3, 0,
		13, 2, 2, 0,
		11, 3, 1, 0,
	8, 7, 0, 0,
		24, 0, 3, 0,
		11, 1, 6, 0,
		18, 2, 3, 0,
		25, 3, 0, 0,
		19, 3, 2, 0,
		12, 6, 1, 0,
		9, 6, 2, 0,
		4, 7, 1, 0,
	19, 8, 0, 0,
		34, 0, 4, 0,
		21, 0, 6, 0,
		32, 1, 4, 0,
		17, 1, 5, 0,
		10, 1, 7, 0,
		30, 2, 4, 0,
		20, 2, 6, 0,
		5, 2, 7, 0,
		27, 3, 4, 0,
		12, 3, 6, 0,
		35, 4, 0, 0,
		33, 4, 1, 0,
		31, 4, 2, 0,
		28, 5, 0, 0,
		26, 5, 1, 0,
		14, 6, 0, 0,
		13, 6, 3, 0,
		11, 7, 0, 0,
		6, 7, 2, 0,
	11, 9, 0, 0,
		33, 0, 5, 0,
		15, 0, 7, 0,
		31, 2, 5, 0,
		59, 3, 3, 0,
		5, 3, 7, 0,
		58, 4, 3, 0,
		30, 4, 4, 0,
		7, 4, 6, 0,
		32, 5, 2, 0,
		14, 6, 4, 0,
		6, 7, 3, 0,
	13, 10, 0, 0,
		18, 3, 5, 0,
		16, 4, 5, 0,
		5, 4, 7, 0,
		19, 5, 3, 0,
		17, 5, 4, 0,
		8, 5, 6, 0,
		9, 6, 5, 0,
		4, 6, 6, 0,
		1, 6, 7, 0,
		6, 7, 4, 0,
		3, 7, 5, 0,
		2, 7, 6, 0,
		0, 7, 7, 0,
	2, 11, 0, 0,
		15, 5, 5, 0,
		14, 5, 7, 0,
	
		
	0, 1, 0, 0,
	0, 2, 0, 0,
	3, 3, 0, 0,
		6, 0, 1, 0,
		7, 1, 0, 0,
		5, 1, 1, 0,
	3, 4, 0, 0,
		9, 0, 0, 0,
		6, 1, 2, 0,
		7, 2, 1, 0,
	5, 5, 0, 0,
		16, 0, 2, 0,
		9, 1, 3, 0,
		17, 2, 0, 0,
		11, 2, 2, 0,
		10, 3, 1, 0,
	5, 6, 0, 0,
		14, 2, 3, 0,
		17, 3, 0, 0,
		15, 3, 2, 0,
		12, 3, 3, 0,
		13, 4, 1, 0,
	12, 7, 0, 0,
		33, 0, 3, 0,
		23, 1, 4, 0,
		16, 1, 5, 0,
		21, 2, 4, 0,
		10, 2, 6, 0,
		18, 3, 4, 0,
		32, 4, 0, 0,
		22, 4, 2, 0,
		19, 4, 3, 0,
		17, 5, 1, 0,
		12, 6, 1, 0,
		11, 6, 2, 0,
	21, 8, 0, 0,
		41, 0, 4, 0,
		26, 1, 6, 0,
		11, 1, 7, 0,
		30, 2, 5, 0,
		7, 2, 7, 0,
		28, 3, 5, 0,
		14, 3, 6, 0,
		5, 3, 7, 0,
		18, 4, 4, 0,
		16, 4, 5, 0,
		9, 4, 6, 0,
		40, 5, 0, 0,
		31, 5, 2, 0,
		29, 5, 3, 0,
		17, 5, 4, 0,
		4, 5, 6, 0,
		27, 6, 0, 0,
		15, 6, 3, 0,
		10, 6, 4, 0,
		12, 7, 1, 0,
		8, 7, 2, 0,
	13, 9, 0, 0,
		39, 0, 5, 0,
		38, 0, 6, 0,
		26, 0, 7, 0,
		5, 4, 7, 0,
		13, 5, 5, 0,
		2, 5, 7, 0,
		7, 6, 5, 0,
		4, 6, 6, 0,
		27, 7, 0, 0,
		12, 7, 3, 0,
		6, 7, 4, 0,
		3, 7, 5, 0,
		1, 7, 6, 0,
	2, 10, 0, 0,
		1, 6, 7, 0,
		0, 7, 7, 0,
	
	
	1, 1, 0, 0,
		1, 0, 0, 0,	
	0, 2, 0, 0,
	1, 3, 0, 0,
		3, 1, 0, 0,
	2, 4, 0, 0,
		5, 0, 1, 0,
		4, 1, 1, 0,	
	0, 5, 0, 0,
	4, 6, 0, 0,
		14, 0, 2, 0,
		12, 1, 2, 0,
		15, 2, 0, 0,
		13, 2, 1, 0,
	6, 7, 0, 0,
		21, 0, 3, 0,
		19, 1, 3, 0,
		23, 2, 2, 0,
		22, 3, 0, 0,
		20, 3, 1, 0,
		16, 4, 1, 0,
	8, 8, 0, 0,
		34, 0, 4, 0,
		31, 1, 4, 0,
		26, 1, 5, 0,
		36, 2, 3, 0,
		37, 3, 2, 0,
		35, 4, 0, 0,
		27, 5, 1, 0,
		20, 8, 1, 0,
	22, 9, 0, 0,
		51, 0, 5, 0,
		46, 0, 6, 0,
		42, 0, 8, 0,
		44, 1, 6, 0,
		33, 1, 7, 0,
		31, 1, 8, 0,
		24, 1, 9, 0,
		59, 2, 4, 0,
		49, 2, 5, 0,
		29, 2, 8, 0,
		61, 3, 3, 0,
		56, 3, 4, 0,
		60, 4, 2, 0,
		57, 4, 3, 0,
		58, 5, 0, 0,
		50, 5, 2, 0,
		47, 6, 0, 0,
		45, 6, 1, 0,
		34, 7, 1, 0,
		43, 8, 0, 0,
		30, 8, 2, 0,
		25, 9, 1, 0,
	32, 10, 0, 0,
		71, 0, 7, 0,
		52, 0, 9, 0,
		32, 1, 10, 0,
		24, 1, 11, 0,
		77, 2, 6, 0,
		65, 2, 7, 0,
		40, 2, 9, 0,
		30, 2, 10, 0,
		79, 3, 5, 0,
		73, 3, 6, 0,
		64, 3, 7, 0,
		43, 3, 8, 0,
		97, 4, 4, 0,
		75, 4, 5, 0,
		54, 4, 8, 0,
		96, 5, 3, 0,
		76, 5, 4, 0,
		70, 5, 5, 0,
		78, 6, 2, 0,
		74, 6, 3, 0,
		72, 7, 0, 0,
		56, 7, 2, 0,
		44, 8, 3, 0,
		55, 8, 4, 0,
		53, 9, 0, 0,
		41, 9, 2, 0,
		37, 9, 3, 0,
		35, 10, 0, 0,
		33, 10, 1, 0,
		31, 10, 2, 0,
		25, 11, 1, 0,
		23, 11, 2, 0,
	39, 11, 0, 0,
		68, 0, 10, 0,
		52, 0, 11, 0,
		31, 1, 12, 0,
		40, 2, 11, 0,
		27, 2, 12, 0,
		76, 3, 9, 0,
		56, 3, 10, 0,
		37, 3, 11, 0,
		26, 3, 12, 0,
		114, 4, 6, 0,
		91, 4, 7, 0,
		73, 4, 9, 0,
		55, 4, 10, 0,
		93, 5, 6, 0,
		84, 5, 7, 0,
		77, 5, 8, 0,
		58, 5, 9, 0,
		29, 5, 11, 0,
		115, 6, 4, 0,
		94, 6, 5, 0,
		90, 6, 6, 0,
		79, 6, 7, 0,
		69, 6, 8, 0,
		95, 7, 3, 0,
		92, 7, 4, 0,
		85, 7, 5, 0,
		78, 8, 5, 0,
		72, 8, 6, 0,
		44, 9, 4, 0,
		59, 9, 5, 0,
		54, 9, 6, 0,
		57, 10, 3, 0,
		42, 10, 4, 0,
		53, 11, 0, 0,
		38, 11, 3, 0,
		34, 12, 0, 0,
		32, 12, 1, 0,
		28, 12, 2, 0,
		21, 13, 1, 0,
	38, 12, 0, 0,
		67, 0, 12, 0,
		44, 0, 13, 0,
		35, 1, 13, 0,
		22, 1, 14, 0,
		14, 1, 15, 0,
		33, 2, 13, 0,
		31, 3, 13, 0,
		41, 4, 11, 0,
		48, 4, 12, 0,
		79, 5, 10, 0,
		83, 6, 9, 0,
		71, 6, 10, 0,
		50, 6, 11, 0,
		91, 7, 6, 0,
		90, 7, 7, 0,
		86, 7, 8, 0,
		73, 7, 9, 0,
		87, 8, 7, 0,
		78, 8, 8, 0,
		61, 8, 9, 0,
		46, 8, 10, 0,
		66, 9, 8, 0,
		82, 10, 5, 0,
		72, 10, 6, 0,
		47, 10, 8, 0,
		70, 11, 4, 0,
		60, 11, 5, 0,
		51, 11, 6, 0,
		36, 11, 7, 0,
		39, 12, 3, 0,
		49, 12, 4, 0,
		30, 12, 6, 0,
		45, 13, 0, 0,
		34, 13, 2, 0,
		23, 14, 1, 0,
		20, 14, 2, 0,
		16, 15, 0, 0,
		15, 15, 1, 0,
	36, 13, 0, 0,
		43, 0, 14, 0,
		19, 0, 15, 0,
		42, 2, 14, 0,
		16, 2, 15, 0,
		25, 3, 14, 0,
		14, 3, 15, 0,
		53, 4, 13, 0,
		23, 4, 14, 0,
		74, 5, 12, 0,
		49, 5, 13, 0,
		59, 6, 12, 0,
		38, 6, 13, 0,
		77, 7, 10, 0,
		65, 7, 11, 0,
		51, 7, 12, 0,
		54, 8, 11, 0,
		37, 8, 12, 0,
		81, 9, 7, 0,
		76, 9, 9, 0,
		57, 9, 10, 0,
		80, 10, 7, 0,
		58, 10, 9, 0,
		21, 10, 11, 0,
		55, 11, 8, 0,
		26, 11, 9, 0,
		34, 11, 10, 0,
		75, 12, 5, 0,
		52, 12, 7, 0,
		64, 13, 3, 0,
		56, 13, 4, 0,
		50, 13, 5, 0,
		48, 14, 0, 0,
		39, 14, 3, 0,
		36, 14, 4, 0,
		35, 14, 5, 0,
		17, 15, 2, 0,
	26, 14, 0, 0,
		24, 4, 15, 0,
		41, 5, 14, 0,
		17, 5, 15, 0,
		36, 6, 14, 0,
		15, 6, 15, 0,
		44, 7, 13, 0,
		30, 8, 13, 0,
		54, 9, 11, 0,
		37, 9, 12, 0,
		18, 9, 13, 0,
		55, 10, 10, 0,
		22, 10, 12, 0,
		23, 11, 11, 0,
		48, 12, 8, 0,
		40, 12, 9, 0,
		49, 13, 6, 0,
		45, 13, 7, 0,
		31, 13, 8, 0,
		19, 13, 9, 0,
		12, 13,	10, 0,
		21, 14, 7, 0,
		16, 14, 8, 0,
		27, 15, 3, 0,
		25, 15, 4, 0,
		20, 15, 5, 0,
		11, 15, 7, 0,
	19, 15, 0, 0,
		20, 8, 14, 0,
		16, 8, 15, 0,
		11, 9, 15, 0,
		26, 10, 13, 0,
		27, 11, 12, 0,
		14, 11, 13, 0,
		9, 11, 14, 0,
		52, 12, 10, 0,
		28, 12, 11, 0,
		18, 12, 12, 0,
		15, 13, 11, 0,
		7, 13, 13, 0,
		53, 14, 6, 0,
		13, 14, 10, 0,
		10, 14, 11, 0,
		6, 14, 12, 0,
		29, 15, 6, 0,
		17, 15, 8, 0,
		12, 15, 9, 0,
	16, 16, 0, 0,
		43, 7, 14, 0,
		42, 7, 15, 0,
		39, 9, 14, 0,
		38, 10, 14, 0,
		7, 11, 15, 0,
		17, 12, 13, 0,
		9, 12, 14, 0,
		5, 12, 15, 0,
		10, 13, 12, 0,
		6, 13, 14, 0,
		3, 13, 15, 0,
		4, 14, 14, 0,
		2, 14, 15, 0,
		16, 15, 10, 0,
		8, 15, 11, 0,
		1, 15, 15, 0,
	3, 17, 0, 0,
		22, 10, 15, 0,
		23, 14, 9, 0,
		1, 14, 13, 0,
	1, 18, 0, 0,
		1, 15, 13, 0,
	2, 19, 0, 0,
		1, 15, 12, 0,
		0, 15, 14, 0,
	
		

	0, 1, 0, 0,
	0, 2, 0, 0,
	2, 3, 0, 0,
		7, 0, 0, 0,
		5, 1, 1, 0,
	2, 4, 0, 0,
		12, 0, 1, 0,
		13, 1, 0, 0,
	5, 5, 0, 0,
		18, 0, 2, 0,
		16, 1, 2, 0,
		19, 2, 0, 0,
		17, 2, 1, 0,
		15, 2, 2, 0,
	6, 6, 0, 0,
		27, 1, 3, 0,
		24, 2, 3, 0,
		29, 3, 0, 0,
		28, 3, 1, 0,
		25, 3, 2, 0,
		22, 4, 1, 0,
	14, 7, 0, 0,
		53, 0, 3, 0,
		47, 0, 4, 0,
		46, 1, 4, 0,
		36, 1, 5, 0,
		41, 2, 4, 0,
		34, 2, 5, 0,
		43, 3, 3, 0,
		39, 3, 4, 0,
		52, 4, 0, 0,
		42, 4, 2, 0,
		40, 4, 3, 0,
		37, 5, 1, 0,
		35, 5, 2, 0,
		32, 6, 1, 0,
	23, 8, 0, 0,
		76, 0, 5, 0,
		61, 1, 6, 0,
		51, 1, 7, 0,
		42, 1, 8, 0,
		59, 2, 6, 0,
		48, 2, 7, 0,
		40, 2, 8, 0,
		63, 3, 5, 0,
		55, 3, 6, 0,
		67, 4, 4, 0,
		57, 4, 5, 0,
		77, 5, 0, 0,
		66, 5, 3, 0,
		58, 5, 4, 0,
		52, 5, 5, 0,
		60, 6, 2, 0,
		56, 6, 3, 0,
		50, 6, 4, 0,
		53, 7, 1, 0,
		49, 7, 2, 0,
		43, 8, 1, 0,
		41, 8, 2, 0,
		34, 9, 1, 0,
	45, 9, 0, 0,
		124, 0, 6, 0,
		108, 0, 7, 0,
		89, 0, 8, 0,
		70, 1, 9, 0,
		52, 1, 10, 0,
		64, 2, 9, 0,
		50, 2, 10, 0,
		93, 3, 7, 0,
		76, 3, 8, 0,
		59, 3, 9, 0,
		95, 4, 6, 0,
		79, 4, 7, 0,
		72, 4, 8, 0,
		57, 4, 9, 0,
		91, 5, 6, 0,
		74, 5, 7, 0,
		62, 5, 8, 0,
		48, 5, 9, 0,
		125, 6, 0, 0,
		92, 6, 5, 0,
		78, 6, 6, 0,
		65, 6, 7, 0,
		55, 6, 8, 0,
		109, 7, 0, 0,
		94, 7, 3, 0,
		88, 7, 4, 0,
		75, 7, 5, 0,
		66, 7, 6, 0,
		90, 8, 0, 0,
		77, 8, 3, 0,
		73, 8, 4, 0,
		63, 8, 5, 0,
		56, 8, 6, 0,
		71, 9, 0, 0,
		67, 9, 2, 0,
		60, 9, 3, 0,
		58, 9, 4, 0,
		49, 9, 5, 0,
		53, 10, 1, 0,
		51, 10, 2, 0,
		47, 10, 3, 0,
		42, 11, 1, 0,
		40, 11, 2, 0,
		37, 11, 3, 0,
		30, 12, 2, 0,
	49, 10, 0, 0,
		123, 0, 9, 0,
		108, 0, 10, 0,
		83, 1, 11, 0,
		65, 1, 12, 0,
		41, 1, 13, 0,
		78, 2, 11, 0,
		62, 2, 12, 0,
		93, 3, 10, 0,
		72, 3, 11, 0,
		54, 3, 12, 0,
		89, 4, 10, 0,
		69, 4, 11, 0,
		49, 4, 12, 0,
		79, 5, 10, 0,
		63, 5, 11, 0,
		87, 6, 9, 0,
		71, 6, 10, 0,
		51, 6, 11, 0,
		122, 7, 7, 0,
		91, 7, 8, 0,
		73, 7, 9, 0,
		56, 7, 10, 0,
		42, 7, 11, 0,
		92, 8, 7, 0,
		77, 8, 8, 0,
		66, 8, 9, 0,
		47, 8, 10, 0,
		88, 9, 6, 0,
		76, 9, 7, 0,
		67, 9, 8, 0,
		109, 10, 0, 0,
		90, 10, 4, 0,
		82, 10, 5, 0,
		58, 10, 6, 0,
		57, 10, 7, 0,
		48, 10, 8, 0,
		86, 11, 0, 0,
		70, 11, 4, 0,
		64, 11, 5, 0,
		52, 11, 6, 0,
		43, 11, 7, 0,
		68, 12, 1, 0,
		55, 12, 3, 0,
		50, 12, 4, 0,
		46, 12, 5, 0,
		44, 13, 1, 0,
		39, 13, 2, 0,
		38, 13, 3, 0,
		34, 13, 4, 0,
	61, 11, 0, 0,
		119, 0, 11, 0,
		107, 0, 12, 0,
		81, 0, 13, 0,
		59, 1, 14, 0,
		36, 1, 15, 0,
		80, 2, 13, 0,
		56, 2, 14, 0,
		33, 2, 15, 0,
		75, 3, 13, 0,
		50, 3, 14, 0,
		29, 3, 15, 0,
		66, 4, 13, 0,
		46, 4, 14, 0,
		27, 4, 15, 0,
		90, 5, 12, 0,
		62, 5, 13, 0,
		40, 5, 14, 0,
		73, 6, 12, 0,
		51, 6, 13, 0,
		64, 7, 12, 0,
		44, 7, 13, 0,
		21, 7, 14, 0,
		67, 8, 11, 0,
		48, 8, 12, 0,
		106, 9, 9, 0,
		71, 9, 10, 0,
		54, 9, 11, 0,
		38, 9, 12, 0,
		72, 10, 9, 0,
		57, 10, 10, 0,
		41, 10, 11, 0,
		23, 10, 12, 0,
		70, 11, 8, 0,
		55, 11, 9, 0,
		42, 11, 10, 0,
		25, 11, 11, 0,
		118, 12, 0, 0,
		74, 12, 6, 0,
		65, 12,	7, 0,
		49, 12, 8, 0,
		39, 12, 9, 0,
		24, 12, 10, 0,
		16, 12, 11, 0,
		91, 13, 0, 0,
		63, 13, 5, 0,
		52, 13, 6, 0,
		45, 13, 7, 0,
		31, 13, 8, 0,
		60, 14, 1, 0,
		58, 14, 2, 0,
		53, 14, 3, 0,
		47, 14, 4, 0,
		43, 14, 5, 0,
		32, 14, 6, 0,
		22, 14, 7, 0,
		37, 15, 1, 0,
		34, 15, 2, 0,
		30, 15, 3, 0,
		28, 15, 4, 0,
		20, 15, 5, 0,
		17, 15, 6, 0,
	35, 12, 0, 0,
		122, 0, 14, 0,
		38, 5, 15, 0,
		70, 6, 14, 0,
		30, 6, 15, 0,
		25, 7, 15, 0,
		53, 8, 13, 0,
		36, 8, 14, 0,
		20, 8, 15, 0,
		39, 9, 13, 0,
		23, 9, 14, 0,
		15, 9, 15, 0,
		27, 10, 13, 0,
		9, 10, 15, 0,
		29, 11, 12, 0,
		18, 11, 13, 0,
		11, 11, 14, 0,
		22, 12, 12, 0,
		13, 12, 13, 0,
		52, 13, 9, 0,
		28, 13, 10, 0,
		19, 13, 11, 0,
		14, 13, 12, 0,
		8, 13, 13, 0,
		123, 14, 0, 0,
		37, 14, 8, 0,
		24, 14, 9, 0,
		17, 14, 10, 0,
		12, 14, 11, 0,
		2, 14, 14, 0,
		71, 15, 0, 0,
		26, 15, 7, 0,
		21, 15, 8, 0,
		16, 15, 9, 0,
		10, 15, 10, 0,
		6, 15, 11, 0,
	14, 13, 0, 0,
		63, 0, 15, 0,
		62, 10, 14, 0,
		11, 11, 15, 0,
		14, 12, 14, 0,
		7, 12, 15, 0,
		9, 13, 14, 0,
		3, 13, 15, 0,
		15, 14, 12, 0,
		10, 14, 13, 0,
		1, 14, 15, 0,
		8, 15, 12, 0,
		6, 15, 13, 0,
		2, 15, 14, 0,
		0, 15, 15, 0,
	
	
	
	1, 1, 0, 0,
		1, 0, 0, 0,	
	0, 2, 0, 0,
	1, 3, 0, 0,
		3, 1, 0, 0,
	2, 4, 0, 0,
		5, 0, 1, 0,
		4, 1, 1, 0,	
	0, 5, 0, 0,
	4, 6, 0, 0,
		14, 0, 2, 0,
		12, 1, 2, 0,
		15, 2, 0, 0,
		13, 2, 1, 0,
	3, 7, 0, 0,
		20, 1, 3, 0,
		23, 2, 2, 0,
		21, 3, 1, 0,
	11, 8, 0, 0,
		44, 0, 3, 0,
		35, 1, 4, 0,
		9, 1, 15, 0,
		38, 2, 3, 0,
		45, 3, 0, 0,
		39, 3, 2, 0,
		36, 4, 1, 0,
		30, 5, 1, 0,
		10, 15, 1, 0,
		7, 15, 2, 0,
		3, 15, 15, 0,
	24, 9, 0, 0,
		74, 0, 4, 0,
		63, 0, 5, 0,
		17, 0, 15, 0,
		62, 1, 5, 0,
		53, 1, 6, 0,
		47, 1, 7, 0,
		67, 2, 4, 0,
		58, 2, 5, 0,
		16, 2, 15, 0,
		69, 3, 3, 0,
		64, 3, 4, 0,
		75, 4, 0, 0,
		68, 4, 2, 0,
		65, 4, 3, 0,
		9, 4, 15, 0,
		66, 5, 0, 0,
		59, 5, 2, 0,
		56, 5, 3, 0,
		54, 6, 1, 0,
		52, 6, 2, 0,
		48, 7, 1, 0,
		12, 15, 0, 0,
		11, 15, 3, 0,
		10, 15, 4, 0,
	34, 10, 0, 0,
		110, 0, 6, 0,
		93, 0, 7, 0,
		83, 1, 8, 0,
		75, 1, 9, 0,
		68, 1, 10, 0,
		103, 2, 6, 0,
		90, 2, 7, 0,
		72, 2, 9, 0,
		114, 3, 5, 0,
		99, 3, 6, 0,
		87, 3, 7, 0,
		26, 3, 15, 0,
		115, 4, 4, 0,
		101, 4, 5, 0,
		102, 5, 4, 0,
		16, 5, 15, 0,
		111, 6, 0, 0,
		100, 6, 3, 0,
		10, 6, 15, 0,
		98, 7, 0, 0,
		91, 7, 2, 0,
		88, 7, 3, 0,
		8, 7, 15, 0,
		85, 8, 0, 0,
		84, 8, 1, 0,
		81, 8, 2, 0,
		7, 8, 15, 0,
		76, 9, 1, 0,
		73, 9, 2, 0,
		67, 10, 2, 0,
		4, 10, 15, 0,
		17, 15, 5, 0,
		11, 15, 6, 0,
		9, 15, 7, 0,
	49, 11, 0, 0,
		172, 0, 8, 0,
		149, 0, 9, 0,
		138, 0, 10, 0,
		119, 1, 11, 0,
		107, 1, 13, 0,
		161, 2, 8, 0,
		127, 2, 10, 0,
		117, 2, 11, 0,
		110, 2, 12, 0,
		158, 3, 8, 0,
		140, 3, 9, 0,
		179, 4, 6, 0,
		164, 4, 7, 0,
		155, 4, 8, 0,
		185, 5, 5, 0,
		173, 5, 6, 0,
		142, 5, 8, 0,
		184, 6, 4, 0,
		178, 6, 5, 0,
		160, 6, 6, 0,
		133, 6, 7, 0,
		165, 7, 4, 0,
		157, 7, 5, 0,
		148, 7, 6, 0,
		159, 8, 3, 0,
		156, 8, 4, 0,
		143, 8, 5, 0,
		154, 9, 0, 0,
		141, 9, 3, 0,
		131, 9, 4, 0,
		11, 9, 15, 0,
		139, 10, 0, 0,
		129, 10, 1, 0,
		125, 10, 3, 0,
		120, 11, 1, 0,
		118, 11, 2, 0,
		115, 11, 3, 0,
		6, 11, 15, 0,
		4, 12, 15, 0,
		2, 13, 15, 0,
		102, 14, 2, 0,
		0, 14, 15, 0,
		13, 15, 8, 0,
		12, 15, 9, 0,
		10, 15, 10, 0,
		7, 15, 11, 0,
		5, 15, 12, 0,
		3, 15, 13, 0,
		1, 15, 14, 0,
	42, 12, 0, 0,
		242, 0, 11, 0,
		225, 0, 12, 0,
		195, 0, 13, 0,
		201, 1, 12, 0,
		207, 1, 14, 0,
		209, 2, 13, 0,
		206, 2, 14, 0,
		252, 3, 10, 0,
		212, 3, 11, 0,
		199, 3, 12, 0,
		264, 4, 9, 0,
		246, 4, 10, 0,
		226, 4, 11, 0,
		265, 5, 7, 0,
		253, 5, 9, 0,
		232, 5, 10, 0,
		257, 6, 8, 0,
		244, 6, 9, 0,
		228, 6, 10, 0,
		217, 6, 11, 0,
		261, 7, 7, 0,
		248, 7, 8, 0,
		260, 8, 6, 0,
		249, 8, 7, 0,
		256, 9, 5, 0,
		245, 9, 6, 0,
		247, 10, 4, 0,
		233, 10, 5, 0,
		229, 10, 6, 0,
		219, 10, 7, 0,
		243, 11, 0, 0,
		227, 11, 4, 0,
		223, 11, 5, 0,
		202, 12, 0, 0,
		224, 12, 1, 0,
		222, 12, 2, 0,
		218, 12, 3, 0,
		216, 12, 4, 0,
		211, 13, 1, 0,
		210, 13, 2, 0,
		208, 13, 3, 0,
		187, 14, 3, 0,
	37, 13, 0, 0,
		376, 0, 14, 0,
		387, 3, 13, 0,
		365, 3, 14, 0,
		395, 4, 12, 0,
		382, 4, 13, 0,
		362, 4, 14, 0,
		400, 5, 11, 0,
		388, 5, 12, 0,
		378, 5, 13, 0,
		385, 6, 12, 0,
		366, 6, 13, 0,
		407, 7, 9, 0,
		397, 7, 10, 0,
		372, 7, 11, 0,
		380, 7, 12, 0,
		427, 8, 8, 0,
		401, 8, 9, 0,
		392, 8, 10, 0,
		383, 8, 11, 0,
		426, 9, 7, 0,
		406, 9, 8, 0,
		394, 9, 9, 0,
		384, 9, 10, 0,
		359, 9, 12, 0,
		352, 9, 14, 0,
		393, 10, 8, 0,
		396, 11, 6, 0,
		223, 11, 13, 0,
		389, 12, 5, 0,
		386, 12, 6, 0,
		381, 12, 7, 0,
		364, 12, 8, 0,
		370, 13, 4, 0,
		379, 13, 5, 0,
		377, 14, 0, 0,
		369, 14, 1, 0,
		358, 14, 6, 0,
	31, 14, 0, 0,
		445, 5, 14, 0,
		715, 6, 14, 0,
		727, 8, 12, 0,
		713, 8, 13, 0,
		708, 8, 14, 0,
		735, 9, 11, 0,
		710, 9, 13, 0,
		743, 10, 9, 0,
		737, 10, 10, 0,
		720, 10, 11, 0,
		439, 10, 14, 0,
		746, 11, 7, 0,
		742, 11, 8, 0,
		736, 11, 9, 0,
		721, 11, 10, 0,
		712, 11, 11, 0,
		706, 11, 12, 0,
		436, 11, 14, 0,
		443, 12, 10, 0,
		707, 12, 11, 0,
		440, 12, 12, 0,
		437, 12, 13, 0,
		747, 13, 0, 0,
		734, 13, 6, 0,
		723, 13, 7, 0,
		714, 13, 8, 0,
		726, 14, 4, 0,
		722, 14, 5, 0,
		711, 14, 7, 0,
		709, 14, 8, 0,
		434, 14, 14, 0,
	12, 15, 0, 0,
		889, 7, 13, 0,
		884, 7, 14, 0,
		885, 10, 12, 0,
		882, 10, 13, 0,
		888, 12, 9, 0,
		883, 13, 10, 0,
		877, 13, 11, 0,
		876, 13, 12, 0,
		865, 13, 14, 0,
		866, 14, 9, 0,
		871, 14, 11, 0,
		870, 14, 13, 0,
	3, 16, 0, 0,
		1728, 12, 14, 0,
		1735, 13, 9, 0,
		1734, 14, 10, 0,
	2, 17, 0, 0,
		3459, 13, 13, 0,
		3458, 14, 12, 0,
	
		







	0, 1, 0, 0,
	0, 2, 0, 0,
	0, 3, 0, 0,
	5, 4, 0, 0,
		15, 0, 0, 0,
		13, 0, 1, 0,
		14, 1, 0, 0,
		12, 1, 1, 0,
		3, 15, 15, 0,
	2, 5, 0, 0,
		21, 1, 2, 0,
		22, 2, 1, 0,
	5, 6, 0, 0,
		46, 0, 2, 0,
		38, 1, 3, 0,
		47, 2, 0, 0,
		41, 2, 2, 0,
		39, 3, 1, 0,
	25, 7, 0, 0,
		80, 0, 3, 0,
		71, 1, 4, 0,
		74, 2, 3, 0,
		68, 2, 4, 0,
		18, 2, 15, 0,
		81, 3, 0, 0,
		75, 3, 2, 0,
		70, 3, 3, 0,
		16, 3, 15, 0,
		72, 4, 1, 0,
		69, 4, 2, 0,
		14, 4, 15, 0,
		66, 5, 1, 0,
		12, 5, 15, 0,
		10, 6, 15, 0,
		20, 15, 1, 0,
		19, 15, 2, 0,
		17, 15, 3, 0,
		15, 15, 4, 0,
		13, 15, 5, 0,
		11, 15, 6, 0,
		9, 15, 7, 0,
		7, 15, 8, 0,
		6, 15, 9, 0,
		4, 15, 10, 0,
	38, 8, 0, 0,
		146, 0, 4, 0,
		130, 1, 5, 0,
		122, 1, 6, 0,
		42, 1, 15, 0,
		128, 2, 5, 0,
		120, 2, 6, 0,
		134, 3, 4, 0,
		125, 3, 5, 0,
		116, 3, 6, 0,
		147, 4, 0, 0,
		135, 4, 3, 0,
		127, 4, 4, 0,
		118, 4, 5, 0,
		112, 4, 6, 0,
		129, 5, 2, 0,
		126, 5, 3, 0,
		119, 5, 4, 0,
		114, 5, 5, 0,
		123, 6, 1, 0,
		121, 6, 2, 0,
		117, 6, 3, 0,
		113, 6, 4, 0,
		115, 7, 1, 0,
		111, 7, 2, 0,
		109, 7, 3, 0,
		17, 7, 15, 0,
		16, 8, 15, 0,
		11, 9, 15, 0,
		10, 10, 15, 0,
		6, 11, 15, 0,
		4, 12, 15, 0,
		2, 13, 15, 0,
		0, 14, 15, 0,
		43, 15, 0, 0,
		7, 15, 11, 0,
		5, 15, 12, 0,
		3, 15, 13, 0,
		1, 15, 14, 0,
	52, 9, 0, 0,
		262, 0, 5, 0,
		248, 0, 6, 0,
		88, 0, 15, 0,
		216, 1, 7, 0,
		209, 1, 8, 0,
		198, 1, 9, 0,
		221, 2, 7, 0,
		207, 2, 8, 0,
		194, 2, 9, 0,
		182, 2, 10, 0,
		220, 3, 7, 0,
		204, 3, 8, 0,
		190, 3, 9, 0,
		178, 3, 10, 0,
		210, 4, 7, 0,
		200, 4, 8, 0,
		188, 4, 9, 0,
		263, 5, 0, 0,
		214, 5, 6, 0,
		202, 5, 7, 0,
		192, 5, 8, 0,
		180, 5, 9, 0,
		249, 6, 0, 0,
		215, 6, 5, 0,
		206, 6, 6, 0,
		195, 6, 7, 0,
		185, 6, 8, 0,
		211, 7, 4, 0,
		203, 7, 5, 0,
		196, 7, 6, 0,
		187, 7, 7, 0,
		212, 8, 1, 0,
		208, 8, 2, 0,
		205, 8, 3, 0,
		201, 8, 4, 0,
		193, 8, 5, 0,
		186, 8, 6, 0,
		177, 8, 7, 0,
		169, 8, 8, 0,
		199, 9, 1, 0,
		197, 9, 2, 0,
		191, 9, 3, 0,
		189, 9, 4, 0,
		181, 9, 5, 0,
		174, 9, 6, 0,
		184, 10, 1, 0,
		183, 10, 2, 0,
		179, 10, 3, 0,
		175, 10, 4, 0,
		171, 11, 2, 0,
		168, 11, 3, 0,
		164, 11, 4, 0,
	80, 10, 0, 0,
		434, 0, 7, 0,
		426, 0, 8, 0,
		327, 1, 10, 0,
		345, 1, 11, 0,
		319, 1, 12, 0,
		297, 1, 13, 0,
		279, 1, 14, 0,
		340, 2, 11, 0,
		315, 2, 12, 0,
		295, 2, 13, 0,
		325, 3, 11, 0,
		311, 3, 12, 0,
		293, 3, 13, 0,
		271, 3, 14, 0,
		352, 4, 10, 0,
		323, 4, 11, 0,
		306, 4, 12, 0,
		285, 4, 13, 0,
		341, 5, 10, 0,
		317, 5, 11, 0,
		301, 5, 12, 0,
		281, 5, 13, 0,
		262, 5, 14, 0,
		347, 6, 9, 0,
		330, 6, 10, 0,
		308, 6, 11, 0,
		291, 6, 12, 0,
		272, 6, 13, 0,
		435, 7, 0, 0,
		353, 7, 8, 0,
		332, 7, 9, 0,
		313, 7, 10, 0,
		298, 7, 11, 0,
		283, 7, 12, 0,
		427, 8, 0, 0,
		320, 8, 9, 0,
		303, 8, 10, 0,
		286, 8, 11, 0,
		268, 8, 12, 0,
		335, 9, 0, 0,
		333, 9, 7, 0,
		321, 9, 8, 0,
		305, 9, 9, 0,
		289, 9, 10, 0,
		275, 9, 11, 0,
		344, 10, 5, 0,
		331, 10, 6, 0,
		314, 10, 7, 0,
		304, 10, 8, 0,
		290, 10, 9, 0,
		277, 10, 10, 0,
		346, 11, 1, 0,
		318, 11, 5, 0,
		309, 11, 6, 0,
		299, 11, 7, 0,
		287, 11, 8, 0,
		276, 11, 9, 0,
		263, 11, 10, 0,
		322, 12, 1, 0,
		316, 12, 2, 0,
		312, 12, 3, 0,
		307, 12, 4, 0,
		302, 12, 5, 0,
		292, 12, 6, 0,
		284, 12, 7, 0,
		269, 12, 8, 0,
		261, 12, 9, 0,
		300, 13, 1, 0,
		296, 13, 2, 0,
		294, 13, 3, 0,
		288, 13, 4, 0,
		282, 13, 5, 0,
		273, 13, 6, 0,
		266, 13, 7, 0,
		280, 14, 1, 0,
		278, 14, 2, 0,
		274, 14, 3, 0,
		267, 14, 4, 0,
		264, 14, 5, 0,
		259, 14, 6, 0,
	47, 11, 0, 0,
		669, 0, 9, 0,
		653, 0, 10, 0,
		649, 0, 11, 0,
		621, 0, 12, 0,
		517, 0, 13, 0,
		541, 2, 14, 0,
		540, 4, 14, 0,
		520, 6, 14, 0,
		531, 7, 13, 0,
		381, 7, 14, 0,
		514, 8, 13, 0,
		377, 8, 14, 0,
		521, 9, 12, 0,
		379, 9, 13, 0,
		371, 9, 14, 0,
		668, 10, 0, 0,
		530, 10, 11, 0,
		383, 10, 12, 0,
		373, 10, 13, 0,
		366, 10, 14, 0,
		652, 11, 0, 0,
		513, 11, 11, 0,
		375, 11, 12, 0,
		368, 11, 13, 0,
		362, 11, 14, 0,
		648, 12, 0, 0,
		512, 12, 10, 0,
		376, 12, 11, 0,
		370, 12, 12, 0,
		364, 12, 13, 0,
		359, 12, 14, 0,
		620, 13, 0, 0,
		515, 13, 8, 0,
		380, 13, 9, 0,
		374, 13, 10, 0,
		369, 13, 11, 0,
		365, 13, 12, 0,
		361, 13, 13, 0,
		357, 13, 14, 0,
		382, 14, 7, 0,
		378, 14, 8, 0,
		372, 14, 9, 0,
		367, 14, 10, 0,
		363, 14, 11, 0,
		360, 14, 12, 0,
		358, 14, 13, 0,
		356, 14, 14, 0,
	2, 12, 0, 0,
		1032, 0, 14, 0,
		1033, 14, 0, 0	
		
};


