/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\MP3.h"

void Audio::MP3::Synthesize_I_II(float * d_ptr, UI_64 p_delta, unsigned int band_count, UI_64 delta_2) {
	for (unsigned int i(0);i<band_count;i++) {
		IDCT_II(d_ptr + i*64, cos_table_32, 64, 0, 0x3F800000);
	}
	
	Synthesize(d_ptr, p_delta, band_count, delta_2);

}

void Audio::MP3::GetSample_I(unsigned int s, unsigned int sb, unsigned int c_idx) {	
	int a_val(0);
	float * d_ptr = decoder_cfg.spectral_data;
	if (c_idx & 1) d_ptr += MP3_CHANNEL_SIZE;
		
	d_ptr += ((s<<6) + sb);

	a_val = decoder_cfg.GetNumber(decoder_cfg.current_cfg[c_idx].allocation_idc[sb] + 1) + 1;
	a_val -= (1 << decoder_cfg.current_cfg[c_idx].allocation_idc[sb]);

	d_ptr[0] = a_val;		
	d_ptr[0] *= layer_I_denom[decoder_cfg.current_cfg[c_idx].allocation_idc[sb]]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[sb]];

	if (c_idx & 2) {
		d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
	}
}

UI_64 Audio::MP3::AudioData_I() {
	unsigned int i_bound(0);
	
	decoder_cfg.frame_cfg->window_sample_count = 384;
	decoder_cfg.frame_cfg->window_count = 1;
	decoder_cfg.frame_cfg->window_offset = 384;


	switch (c_mode) {
		case 3:  // single channel
			decoder_cfg.frame_cfg->source_chan_count = 1;
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;


			for (unsigned int sb(0);sb<32;sb++) {
				decoder_cfg.current_cfg[0].allocation_idc[sb] = decoder_cfg.GetNumber(4);
			}

			for (unsigned int sb(0);sb<32;sb++) {
				if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
					decoder_cfg.current_cfg[0].sf_idc[sb] = decoder_cfg.GetNumber(6);
				}
			}

			for (unsigned int s(0);s<12;s++) {
				for (unsigned int sb(0);sb<32;sb++) {
					if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
						GetSample_I(s, sb, 0);
					}
				}
			}

		break;
		case 0: case 2: // stereo or dual
			decoder_cfg.frame_cfg->source_chan_count = 2;

			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
				
			for (unsigned int i(0);i<32;i++) {
				decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.GetNumber(4);
				decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(4);
			}

			for (unsigned int i(0);i<32;i++) {
				if (decoder_cfg.current_cfg[0].allocation_idc[i]) {
					decoder_cfg.current_cfg[0].sf_idc[i] = decoder_cfg.GetNumber(6);
				}
				if (decoder_cfg.current_cfg[1].allocation_idc[i]) {
					decoder_cfg.current_cfg[1].sf_idc[i] = decoder_cfg.GetNumber(6);
				}
			}

			for (unsigned int s(0);s<12;s++) {
				for (unsigned int sb(0);sb<32;sb++) {
					if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
						GetSample_I(s, sb, 0);
					}

					if (decoder_cfg.current_cfg[1].allocation_idc[sb]) {
						GetSample_I(s, sb, 1);
					}
				}
			}
		break;
		case 1: // intensity stereo
			decoder_cfg.frame_cfg->source_chan_count = 2;

			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			i_bound = ((xc_mode + 1) << 2);

			for (unsigned int i(0);i<i_bound;i++) {
				decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.GetNumber(4);
				decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(4);
			}
			for (unsigned int i(i_bound);i<32;i++) {
				decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(4);
			}

			for (unsigned int i(0);i<32;i++) {
				if (decoder_cfg.current_cfg[0].allocation_idc[i]) {
					decoder_cfg.current_cfg[0].sf_idc[i] = decoder_cfg.GetNumber(6);
				}
				if (decoder_cfg.current_cfg[1].allocation_idc[i]) {
					decoder_cfg.current_cfg[1].sf_idc[i] = decoder_cfg.GetNumber(6);
				}
			}

			for (unsigned int s(0);s<12;s++) {
				for (unsigned int sb(0);sb<i_bound;sb++) {
					if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
						GetSample_I(s, sb, 0);
					}

					if (decoder_cfg.current_cfg[1].allocation_idc[sb]) {
						GetSample_I(s, sb, 1);
					}
				}

				for (unsigned int sb(i_bound);sb<32;sb++) {
					if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
						GetSample_I(s, sb, 2);
					}
				}
			}

		break;
	}

	Synthesize_I_II(decoder_cfg.spectral_data, previous_frame_delta, 12, pfd_2);
	if (c_mode != 3) {
		Synthesize_I_II(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, previous_frame_delta, 12, pfd_2);
	}

	return 0;
}

// =================================================================================================================================================================================================================================================================

void Audio::MP3::GetTriplet_II(const unsigned char (* q_table)[20], unsigned int gr_sb, unsigned int c_idx) {

	unsigned int d_index(gr_sb >> 16), a_index(0);
	int b_val(0);
	float * d_ptr = decoder_cfg.spectral_data;
	if (c_idx & 1) d_ptr += MP3_CHANNEL_SIZE;

	gr_sb &= 0x0000FFFF;
	
	a_index = q_table[gr_sb][decoder_cfg.current_cfg[c_idx & 1].allocation_idc[gr_sb]];
		
	d_ptr += ((d_index*3)<<6) + gr_sb;

	gr_sb *= 3;
	gr_sb += (d_index >> 2);
	
	if (layer_II_mappings[a_index][0]) { // mod_lav
		b_val = decoder_cfg.GetNumber(layer_II_mappings[a_index][1]) + layer_II_mappings[a_index][0] - 1;
	
		d_ptr[0] = mod_lav_II[b_val][0];
		d_ptr[0] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];
		
					
		d_ptr[64] = mod_lav_II[b_val][1];		
		d_ptr[64] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];
		

		d_ptr[128] = mod_lav_II[b_val][2];		
		d_ptr[128] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];


	} else {
						
		b_val = decoder_cfg.GetNumber(layer_II_mappings[a_index][1]) + 1;
		b_val -= (1 << (layer_II_mappings[a_index][1] - 1));		
		
		d_ptr[0] = b_val;
		d_ptr[0] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];
		

		b_val = decoder_cfg.GetNumber(layer_II_mappings[a_index][1]) + 1;
		b_val -= (1 << (layer_II_mappings[a_index][1] - 1));		

		d_ptr[64] = b_val;
		d_ptr[64] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];
				

		b_val = decoder_cfg.GetNumber(layer_II_mappings[a_index][1]) + 1;
		b_val -= (1 << (layer_II_mappings[a_index][1] - 1));

		d_ptr[128] = b_val;
		d_ptr[128] *= layer_II_denom[a_index]*scale_fac_I_II[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb]];
		
	}


	if (c_idx & 2) {
		d_ptr[MP3_CHANNEL_SIZE + 0] = d_ptr[0];
		d_ptr[MP3_CHANNEL_SIZE + 64] = d_ptr[64];
		d_ptr[MP3_CHANNEL_SIZE + 128] = d_ptr[128];
	}


}

void Audio::MP3::GetSingle_II() {	
	const unsigned char (* q_table)[20](layer_II_quantization[map_II_selector[sf_idx][brate_idx][0]]);


	for (unsigned int i(0);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
	}

	for (unsigned int i(0);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[i]) {
			decoder_cfg.current_cfg[0].sf_idc[3*i] = decoder_cfg.GetNumber(2);
		}
	}

		
	for (unsigned int j(0);j<map_II_selector[sf_idx][brate_idx][1];j++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[j]) {
			switch (decoder_cfg.current_cfg[0].sf_idc[3*j]) {
				case 0:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 1:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 2:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 3:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
			}
		}
	}
		
	for (unsigned int gr(0);gr<0xC0000;gr+=0x10000) {
		for (unsigned int sb(0);sb<map_II_selector[sf_idx][brate_idx][1];sb++) {
			if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 0);
			}
		}
	}
}

void Audio::MP3::GetDual_II() {
	const unsigned char (* q_table)[20](layer_II_quantization[map_II_selector[sf_idx][brate_idx][0]]);

	for (unsigned int i(0);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
		decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
	}

	for (unsigned int i(0);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[i]) {
			decoder_cfg.current_cfg[0].sf_idc[3*i] = decoder_cfg.GetNumber(2);
		}

		if (decoder_cfg.current_cfg[1].allocation_idc[i]) {
			decoder_cfg.current_cfg[1].sf_idc[3*i] = decoder_cfg.GetNumber(2);
		}
	}


	for (unsigned int j(0);j<map_II_selector[sf_idx][brate_idx][1];j++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[j]) {
			switch (decoder_cfg.current_cfg[0].sf_idc[3*j]) {
				case 0:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 1:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 2:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 3:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
			}
		}

		if (decoder_cfg.current_cfg[1].allocation_idc[j]) {
			switch (decoder_cfg.current_cfg[1].sf_idc[3*j]) {
				case 0:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 1:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 2:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 3:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
			}
		}
	}

	for (unsigned int gr(0);gr<0xC0000;gr+=0x10000) {
		for (unsigned int sb(0);sb<map_II_selector[sf_idx][brate_idx][1];sb++) {
			if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 0);
			}

			if (decoder_cfg.current_cfg[1].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 1);
			}
		}
	}
}

void Audio::MP3::GetStereo_II() {
	const unsigned char (* q_table)[20](layer_II_quantization[map_II_selector[sf_idx][brate_idx][0]]);
	
	unsigned int x_bound = ((xc_mode + 1) << 2);


	for (unsigned int i(0);i<x_bound;i++) {
		decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
		decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
	}
	for (unsigned int i(x_bound);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		decoder_cfg.current_cfg[0].allocation_idc[i] = decoder_cfg.current_cfg[1].allocation_idc[i] = decoder_cfg.GetNumber(q_table[i][0]);
	}

	for (unsigned int i(0);i<map_II_selector[sf_idx][brate_idx][1];i++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[i]) {
			decoder_cfg.current_cfg[0].sf_idc[3*i] = decoder_cfg.GetNumber(2);
		}

		if (decoder_cfg.current_cfg[1].allocation_idc[i]) {
			decoder_cfg.current_cfg[1].sf_idc[3*i] = decoder_cfg.GetNumber(2);
		}
	}


	for (unsigned int j(0);j<map_II_selector[sf_idx][brate_idx][1];j++) {
		if (decoder_cfg.current_cfg[0].allocation_idc[j]) {
			switch (decoder_cfg.current_cfg[0].sf_idc[3*j]) {
				case 0:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 1:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 2:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 3:
					decoder_cfg.current_cfg[0].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[0].sf_idc[3*j+1] = decoder_cfg.current_cfg[0].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
			}
		}

		if (decoder_cfg.current_cfg[1].allocation_idc[j]) {
			switch (decoder_cfg.current_cfg[1].sf_idc[3*j]) {
				case 0:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 1:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 2:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
				case 3:
					decoder_cfg.current_cfg[1].sf_idc[3*j] = decoder_cfg.GetNumber(6);
					decoder_cfg.current_cfg[1].sf_idc[3*j+1] = decoder_cfg.current_cfg[1].sf_idc[3*j+2] = decoder_cfg.GetNumber(6);
				break;
			}
		}
	}


	for (unsigned int gr(0);gr<0xC0000;gr+=0x10000) {
		for (unsigned int sb(0);sb<x_bound;sb++) {
			if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 0);
			}

			if (decoder_cfg.current_cfg[1].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 1);
			}
		}


		for (unsigned int sb(x_bound);sb<map_II_selector[sf_idx][brate_idx][1];sb++) {
			if (decoder_cfg.current_cfg[0].allocation_idc[sb]) {
				GetTriplet_II(q_table, gr | sb, 2);
			}
		}
	}
}

UI_64 Audio::MP3::AudioData_II() {	
	decoder_cfg.frame_cfg->window_sample_count = MP3_GRANULE_SIZE*2;
	decoder_cfg.frame_cfg->window_count = 1;
	decoder_cfg.frame_cfg->window_offset = MP3_GRANULE_SIZE*2;


	switch (c_mode) {
		case 3: // single channel
			decoder_cfg.frame_cfg->source_chan_count = 1;
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;

			GetSingle_II();
		break;
		case 0: case 2:// stereo or dual
			decoder_cfg.frame_cfg->source_chan_count = 2;

			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			GetDual_II();
		break;
		case 1: // intensity stereo
			decoder_cfg.frame_cfg->source_chan_count = 2;

			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			GetStereo_II();
		break;
	}

	Synthesize_I_II(decoder_cfg.spectral_data, previous_frame_delta, 36, 0);
	if (c_mode != 3) {
		Synthesize_I_II(decoder_cfg.spectral_data + MP3_CHANNEL_SIZE, previous_frame_delta, 36, 0);
	}

	return 0;
}

// ========================================================================================================================================================================


const float Audio::MP3::layer_I_denom[] = {1.0f, 2.0f/3.0f, 2.0f/7.0f, 2.0f/15.0f, 2.0f/31.0f, 2.0f/63.0f, 2.0f/127.0f, 2.0f/255.0f, 2.0f/511.0f, 2.0f/1023.0f, 2.0f/2047.0f, 2.0f/4095.0f, 2.0f/8191.0f, 2.0f/16383.0f, 2.0f/32767.0f, 2.0f/65535.0f};
const float Audio::MP3::layer_II_denom[] = {2.0f/3.0f, 2.0f/5.0f, 2.0f/7.0f, 2.0f/9.0f, 2.0f/15.0f, 2.0f/31.0f, 2.0f/63.0f, 2.0f/127.0f, 2.0f/255.0f, 2.0f/511.0f, 2.0f/1023.0f, 2.0f/2047.0f, 2.0f/4095.0f, 2.0f/8191.0f, 2.0f/16383.0f, 2.0f/32767.0f, 2.0f/65535.0f};

const float Audio::MP3::scale_fac_I_II[] = {
	2.0000000000e+000f, 1.5874010324e+000f, 1.2599210739e+000f, 1.0000000000e+000f, 7.9370051622e-001f, 6.2996053696e-001f, 5.0000000000e-001f, 3.9685025811e-001f,
	3.1498026848e-001f, 2.5000000000e-001f, 1.9842512906e-001f, 1.5749013424e-001f, 1.2500000000e-001f, 9.9212564528e-002f, 7.8745067120e-002f, 6.2500000000e-002f,
	4.9606282264e-002f, 3.9372533560e-002f, 3.1250000000e-002f, 2.4803141132e-002f, 1.9686266780e-002f, 1.5625000000e-002f, 1.2401570566e-002f, 9.8431333899e-003f,
	7.8125000000e-003f, 6.2007852830e-003f, 4.9215666950e-003f, 3.9062500000e-003f, 3.1003926415e-003f, 2.4607833475e-003f, 1.9531250000e-003f, 1.5501963207e-003f,
	1.2303916737e-003f, 9.7656250000e-004f, 7.7509816037e-004f, 6.1519583687e-004f, 4.8828125000e-004f, 3.8754908019e-004f, 3.0759791844e-004f, 2.4414062500e-004f,
	1.9377454009e-004f, 1.5379895922e-004f, 1.2207031250e-004f, 9.6887270047e-005f, 7.6899479609e-005f, 6.1035156250e-005f, 4.8443635023e-005f, 3.8449739804e-005f,
	3.0517578125e-005f, 2.4221817512e-005f, 1.9224869902e-005f, 1.5258789063e-005f, 1.2110908756e-005f, 9.6124349511e-006f, 7.6293945313e-006f, 6.0554543779e-006f,
	4.8062174756e-006f, 3.8146972656e-006f, 3.0277271890e-006f, 2.4031087378e-006f, 1.9073486328e-006f, 1.5138635945e-006f, 1.2015543689e-006f, 9.5367431641e-007f

};

const unsigned int Audio::MP3::layer_II_mappings[][2] = {
	{1, 5},
	{33, 7},
	{0, 3},
	{161, 10},
	{0, 4},
	{0, 5},
	{0, 6},
	{0, 7},
	{0, 8},
	{0, 9},
	{0, 10},
	{0, 11},
	{0, 12},
	{0, 13},
	{0, 14},
	{0, 15},
	{0, 16}

};

const unsigned int Audio::MP3::map_II_selector[4][16][2] = {
	{{-1, -1}, {2, 8}, {2, 8}, {0, 27}, {0, 27}, {0, 27}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}},
	{{-1, -1}, {2, 8}, {2, 8}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}, {0, 27}},
	{{-1, -1}, {3, 12}, {3, 12}, {0, 27}, {0, 27}, {0, 27}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}, {1, 30}},

};

const unsigned char Audio::MP3::layer_II_quantization[4][32][20] = {
{
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16}
},

{
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{3, 0, 1, 2, 3, 4, 5, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16},
	{2, 0, 1, 16}

},


{
	{4, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{4, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7}
},



{
	{4, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{4, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7},
	{3, 0, 1, 3, 4, 5, 6, 7}
}
};

const char Audio::MP3::mod_lav_II[][4] =
	{
		-1,	-1,	-1,	0,
		0,	-1,	-1,	0,
		1,	-1,	-1,	0,
		-1,	0,	-1,	0,
		0,	0,	-1,	0,
		1,	0,	-1,	0,
		-1,	1,	-1,	0,
		0,	1,	-1,	0,
		1,	1,	-1,	0,
		-1,	-1,	0,	0,
		0,	-1,	0,	0,
		1,	-1,	0,	0,
		-1,	0,	0,	0,
		0,	0,	0,	0,
		1,	0,	0,	0,
		-1,	1,	0,	0,
		0,	1,	0,	0,
		1,	1,	0,	0,
		-1,	-1,	1,	0,
		0,	-1,	1,	0,
		1,	-1,	1,	0,
		-1,	0,	1,	0,
		0,	0,	1,	0,
		1,	0,	1,	0,
		-1,	1,	1,	0,
		0,	1,	1,	0,
		1,	1,	1,	0,
		-1,	-1,	-1,	0,
		0,	-1,	-1,	0,
		1,	-1,	-1,	0,
		-1,	0,	-1,	0,
		0,	0,	-1,	0,
		-2,	-2,	-2,	0,
		-1,	-2,	-2,	0,
		0,	-2,	-2,	0,
		1,	-2,	-2,	0,
		2,	-2,	-2,	0,
		-2,	-1,	-2,	0,
		-1,	-1,	-2,	0,
		0,	-1,	-2,	0,
		1,	-1,	-2,	0,
		2,	-1,	-2,	0,
		-2,	0,	-2,	0,
		-1,	0,	-2,	0,
		0,	0,	-2,	0,
		1,	0,	-2,	0,
		2,	0,	-2,	0,
		-2,	1,	-2,	0,
		-1,	1,	-2,	0,
		0,	1,	-2,	0,
		1,	1,	-2,	0,
		2,	1,	-2,	0,
		-2,	2,	-2,	0,
		-1,	2,	-2,	0,
		0,	2,	-2,	0,
		1,	2,	-2,	0,
		2,	2,	-2,	0,
		-2,	-2,	-1,	0,
		-1,	-2,	-1,	0,
		0,	-2,	-1,	0,
		1,	-2,	-1,	0,
		2,	-2,	-1,	0,
		-2,	-1,	-1,	0,
		-1,	-1,	-1,	0,
		0,	-1,	-1,	0,
		1,	-1,	-1,	0,
		2,	-1,	-1,	0,
		-2,	0,	-1,	0,
		-1,	0,	-1,	0,
		0,	0,	-1,	0,
		1,	0,	-1,	0,
		2,	0,	-1,	0,
		-2,	1,	-1,	0,
		-1,	1,	-1,	0,
		0,	1,	-1,	0,
		1,	1,	-1,	0,
		2,	1,	-1,	0,
		-2,	2,	-1,	0,
		-1,	2,	-1,	0,
		0,	2,	-1,	0,
		1,	2,	-1,	0,
		2,	2,	-1,	0,
		-2,	-2,	0,	0,
		-1,	-2,	0,	0,
		0,	-2,	0,	0,
		1,	-2,	0,	0,
		2,	-2,	0,	0,
		-2,	-1,	0,	0,
		-1,	-1,	0,	0,
		0,	-1,	0,	0,
		1,	-1,	0,	0,
		2,	-1,	0,	0,
		-2,	0,	0,	0,
		-1,	0,	0,	0,
		0,	0,	0,	0,
		1,	0,	0,	0,
		2,	0,	0,	0,
		-2,	1,	0,	0,
		-1,	1,	0,	0,
		0,	1,	0,	0,
		1,	1,	0,	0,
		2,	1,	0,	0,
		-2,	2,	0,	0,
		-1,	2,	0,	0,
		0,	2,	0,	0,
		1,	2,	0,	0,
		2,	2,	0,	0,
		-2,	-2,	1,	0,
		-1,	-2,	1,	0,
		0,	-2,	1,	0,
		1,	-2,	1,	0,
		2,	-2,	1,	0,
		-2,	-1,	1,	0,
		-1,	-1,	1,	0,
		0,	-1,	1,	0,
		1,	-1,	1,	0,
		2,	-1,	1,	0,
		-2,	0,	1,	0,
		-1,	0,	1,	0,
		0,	0,	1,	0,
		1,	0,	1,	0,
		2,	0,	1,	0,
		-2,	1,	1,	0,
		-1,	1,	1,	0,
		0,	1,	1,	0,
		1,	1,	1,	0,
		2,	1,	1,	0,
		-2,	2,	1,	0,
		-1,	2,	1,	0,
		0,	2,	1,	0,
		1,	2,	1,	0,
		2,	2,	1,	0,
		-2,	-2,	2,	0,
		-1,	-2,	2,	0,
		0,	-2,	2,	0,
		1,	-2,	2,	0,
		2,	-2,	2,	0,
		-2,	-1,	2,	0,
		-1,	-1,	2,	0,
		0,	-1,	2,	0,
		1,	-1,	2,	0,
		2,	-1,	2,	0,
		-2,	0,	2,	0,
		-1,	0,	2,	0,
		0,	0,	2,	0,
		1,	0,	2,	0,
		2,	0,	2,	0,
		-2,	1,	2,	0,
		-1,	1,	2,	0,
		0,	1,	2,	0,
		1,	1,	2,	0,
		2,	1,	2,	0,
		-2,	2,	2,	0,
		-1,	2,	2,	0,
		0,	2,	2,	0,
		1,	2,	2,	0,
		2,	2,	2,	0,
		-2,	-2,	-2,	0,
		-1,	-2,	-2,	0,
		0,	-2,	-2,	0,
		-4,	-4,	-4,	0,
		-3,	-4,	-4,	0,
		-2,	-4,	-4,	0,
		-1,	-4,	-4,	0,
		0,	-4,	-4,	0,
		1,	-4,	-4,	0,
		2,	-4,	-4,	0,
		3,	-4,	-4,	0,
		4,	-4,	-4,	0,
		-4,	-3,	-4,	0,
		-3,	-3,	-4,	0,
		-2,	-3,	-4,	0,
		-1,	-3,	-4,	0,
		0,	-3,	-4,	0,
		1,	-3,	-4,	0,
		2,	-3,	-4,	0,
		3,	-3,	-4,	0,
		4,	-3,	-4,	0,
		-4,	-2,	-4,	0,
		-3,	-2,	-4,	0,
		-2,	-2,	-4,	0,
		-1,	-2,	-4,	0,
		0,	-2,	-4,	0,
		1,	-2,	-4,	0,
		2,	-2,	-4,	0,
		3,	-2,	-4,	0,
		4,	-2,	-4,	0,
		-4,	-1,	-4,	0,
		-3,	-1,	-4,	0,
		-2,	-1,	-4,	0,
		-1,	-1,	-4,	0,
		0,	-1,	-4,	0,
		1,	-1,	-4,	0,
		2,	-1,	-4,	0,
		3,	-1,	-4,	0,
		4,	-1,	-4,	0,
		-4,	0,	-4,	0,
		-3,	0,	-4,	0,
		-2,	0,	-4,	0,
		-1,	0,	-4,	0,
		0,	0,	-4,	0,
		1,	0,	-4,	0,
		2,	0,	-4,	0,
		3,	0,	-4,	0,
		4,	0,	-4,	0,
		-4,	1,	-4,	0,
		-3,	1,	-4,	0,
		-2,	1,	-4,	0,
		-1,	1,	-4,	0,
		0,	1,	-4,	0,
		1,	1,	-4,	0,
		2,	1,	-4,	0,
		3,	1,	-4,	0,
		4,	1,	-4,	0,
		-4,	2,	-4,	0,
		-3,	2,	-4,	0,
		-2,	2,	-4,	0,
		-1,	2,	-4,	0,
		0,	2,	-4,	0,
		1,	2,	-4,	0,
		2,	2,	-4,	0,
		3,	2,	-4,	0,
		4,	2,	-4,	0,
		-4,	3,	-4,	0,
		-3,	3,	-4,	0,
		-2,	3,	-4,	0,
		-1,	3,	-4,	0,
		0,	3,	-4,	0,
		1,	3,	-4,	0,
		2,	3,	-4,	0,
		3,	3,	-4,	0,
		4,	3,	-4,	0,
		-4,	4,	-4,	0,
		-3,	4,	-4,	0,
		-2,	4,	-4,	0,
		-1,	4,	-4,	0,
		0,	4,	-4,	0,
		1,	4,	-4,	0,
		2,	4,	-4,	0,
		3,	4,	-4,	0,
		4,	4,	-4,	0,
		-4,	-4,	-3,	0,
		-3,	-4,	-3,	0,
		-2,	-4,	-3,	0,
		-1,	-4,	-3,	0,
		0,	-4,	-3,	0,
		1,	-4,	-3,	0,
		2,	-4,	-3,	0,
		3,	-4,	-3,	0,
		4,	-4,	-3,	0,
		-4,	-3,	-3,	0,
		-3,	-3,	-3,	0,
		-2,	-3,	-3,	0,
		-1,	-3,	-3,	0,
		0,	-3,	-3,	0,
		1,	-3,	-3,	0,
		2,	-3,	-3,	0,
		3,	-3,	-3,	0,
		4,	-3,	-3,	0,
		-4,	-2,	-3,	0,
		-3,	-2,	-3,	0,
		-2,	-2,	-3,	0,
		-1,	-2,	-3,	0,
		0,	-2,	-3,	0,
		1,	-2,	-3,	0,
		2,	-2,	-3,	0,
		3,	-2,	-3,	0,
		4,	-2,	-3,	0,
		-4,	-1,	-3,	0,
		-3,	-1,	-3,	0,
		-2,	-1,	-3,	0,
		-1,	-1,	-3,	0,
		0,	-1,	-3,	0,
		1,	-1,	-3,	0,
		2,	-1,	-3,	0,
		3,	-1,	-3,	0,
		4,	-1,	-3,	0,
		-4,	0,	-3,	0,
		-3,	0,	-3,	0,
		-2,	0,	-3,	0,
		-1,	0,	-3,	0,
		0,	0,	-3,	0,
		1,	0,	-3,	0,
		2,	0,	-3,	0,
		3,	0,	-3,	0,
		4,	0,	-3,	0,
		-4,	1,	-3,	0,
		-3,	1,	-3,	0,
		-2,	1,	-3,	0,
		-1,	1,	-3,	0,
		0,	1,	-3,	0,
		1,	1,	-3,	0,
		2,	1,	-3,	0,
		3,	1,	-3,	0,
		4,	1,	-3,	0,
		-4,	2,	-3,	0,
		-3,	2,	-3,	0,
		-2,	2,	-3,	0,
		-1,	2,	-3,	0,
		0,	2,	-3,	0,
		1,	2,	-3,	0,
		2,	2,	-3,	0,
		3,	2,	-3,	0,
		4,	2,	-3,	0,
		-4,	3,	-3,	0,
		-3,	3,	-3,	0,
		-2,	3,	-3,	0,
		-1,	3,	-3,	0,
		0,	3,	-3,	0,
		1,	3,	-3,	0,
		2,	3,	-3,	0,
		3,	3,	-3,	0,
		4,	3,	-3,	0,
		-4,	4,	-3,	0,
		-3,	4,	-3,	0,
		-2,	4,	-3,	0,
		-1,	4,	-3,	0,
		0,	4,	-3,	0,
		1,	4,	-3,	0,
		2,	4,	-3,	0,
		3,	4,	-3,	0,
		4,	4,	-3,	0,
		-4,	-4,	-2,	0,
		-3,	-4,	-2,	0,
		-2,	-4,	-2,	0,
		-1,	-4,	-2,	0,
		0,	-4,	-2,	0,
		1,	-4,	-2,	0,
		2,	-4,	-2,	0,
		3,	-4,	-2,	0,
		4,	-4,	-2,	0,
		-4,	-3,	-2,	0,
		-3,	-3,	-2,	0,
		-2,	-3,	-2,	0,
		-1,	-3,	-2,	0,
		0,	-3,	-2,	0,
		1,	-3,	-2,	0,
		2,	-3,	-2,	0,
		3,	-3,	-2,	0,
		4,	-3,	-2,	0,
		-4,	-2,	-2,	0,
		-3,	-2,	-2,	0,
		-2,	-2,	-2,	0,
		-1,	-2,	-2,	0,
		0,	-2,	-2,	0,
		1,	-2,	-2,	0,
		2,	-2,	-2,	0,
		3,	-2,	-2,	0,
		4,	-2,	-2,	0,
		-4,	-1,	-2,	0,
		-3,	-1,	-2,	0,
		-2,	-1,	-2,	0,
		-1,	-1,	-2,	0,
		0,	-1,	-2,	0,
		1,	-1,	-2,	0,
		2,	-1,	-2,	0,
		3,	-1,	-2,	0,
		4,	-1,	-2,	0,
		-4,	0,	-2,	0,
		-3,	0,	-2,	0,
		-2,	0,	-2,	0,
		-1,	0,	-2,	0,
		0,	0,	-2,	0,
		1,	0,	-2,	0,
		2,	0,	-2,	0,
		3,	0,	-2,	0,
		4,	0,	-2,	0,
		-4,	1,	-2,	0,
		-3,	1,	-2,	0,
		-2,	1,	-2,	0,
		-1,	1,	-2,	0,
		0,	1,	-2,	0,
		1,	1,	-2,	0,
		2,	1,	-2,	0,
		3,	1,	-2,	0,
		4,	1,	-2,	0,
		-4,	2,	-2,	0,
		-3,	2,	-2,	0,
		-2,	2,	-2,	0,
		-1,	2,	-2,	0,
		0,	2,	-2,	0,
		1,	2,	-2,	0,
		2,	2,	-2,	0,
		3,	2,	-2,	0,
		4,	2,	-2,	0,
		-4,	3,	-2,	0,
		-3,	3,	-2,	0,
		-2,	3,	-2,	0,
		-1,	3,	-2,	0,
		0,	3,	-2,	0,
		1,	3,	-2,	0,
		2,	3,	-2,	0,
		3,	3,	-2,	0,
		4,	3,	-2,	0,
		-4,	4,	-2,	0,
		-3,	4,	-2,	0,
		-2,	4,	-2,	0,
		-1,	4,	-2,	0,
		0,	4,	-2,	0,
		1,	4,	-2,	0,
		2,	4,	-2,	0,
		3,	4,	-2,	0,
		4,	4,	-2,	0,
		-4,	-4,	-1,	0,
		-3,	-4,	-1,	0,
		-2,	-4,	-1,	0,
		-1,	-4,	-1,	0,
		0,	-4,	-1,	0,
		1,	-4,	-1,	0,
		2,	-4,	-1,	0,
		3,	-4,	-1,	0,
		4,	-4,	-1,	0,
		-4,	-3,	-1,	0,
		-3,	-3,	-1,	0,
		-2,	-3,	-1,	0,
		-1,	-3,	-1,	0,
		0,	-3,	-1,	0,
		1,	-3,	-1,	0,
		2,	-3,	-1,	0,
		3,	-3,	-1,	0,
		4,	-3,	-1,	0,
		-4,	-2,	-1,	0,
		-3,	-2,	-1,	0,
		-2,	-2,	-1,	0,
		-1,	-2,	-1,	0,
		0,	-2,	-1,	0,
		1,	-2,	-1,	0,
		2,	-2,	-1,	0,
		3,	-2,	-1,	0,
		4,	-2,	-1,	0,
		-4,	-1,	-1,	0,
		-3,	-1,	-1,	0,
		-2,	-1,	-1,	0,
		-1,	-1,	-1,	0,
		0,	-1,	-1,	0,
		1,	-1,	-1,	0,
		2,	-1,	-1,	0,
		3,	-1,	-1,	0,
		4,	-1,	-1,	0,
		-4,	0,	-1,	0,
		-3,	0,	-1,	0,
		-2,	0,	-1,	0,
		-1,	0,	-1,	0,
		0,	0,	-1,	0,
		1,	0,	-1,	0,
		2,	0,	-1,	0,
		3,	0,	-1,	0,
		4,	0,	-1,	0,
		-4,	1,	-1,	0,
		-3,	1,	-1,	0,
		-2,	1,	-1,	0,
		-1,	1,	-1,	0,
		0,	1,	-1,	0,
		1,	1,	-1,	0,
		2,	1,	-1,	0,
		3,	1,	-1,	0,
		4,	1,	-1,	0,
		-4,	2,	-1,	0,
		-3,	2,	-1,	0,
		-2,	2,	-1,	0,
		-1,	2,	-1,	0,
		0,	2,	-1,	0,
		1,	2,	-1,	0,
		2,	2,	-1,	0,
		3,	2,	-1,	0,
		4,	2,	-1,	0,
		-4,	3,	-1,	0,
		-3,	3,	-1,	0,
		-2,	3,	-1,	0,
		-1,	3,	-1,	0,
		0,	3,	-1,	0,
		1,	3,	-1,	0,
		2,	3,	-1,	0,
		3,	3,	-1,	0,
		4,	3,	-1,	0,
		-4,	4,	-1,	0,
		-3,	4,	-1,	0,
		-2,	4,	-1,	0,
		-1,	4,	-1,	0,
		0,	4,	-1,	0,
		1,	4,	-1,	0,
		2,	4,	-1,	0,
		3,	4,	-1,	0,
		4,	4,	-1,	0,
		-4,	-4,	0,	0,
		-3,	-4,	0,	0,
		-2,	-4,	0,	0,
		-1,	-4,	0,	0,
		0,	-4,	0,	0,
		1,	-4,	0,	0,
		2,	-4,	0,	0,
		3,	-4,	0,	0,
		4,	-4,	0,	0,
		-4,	-3,	0,	0,
		-3,	-3,	0,	0,
		-2,	-3,	0,	0,
		-1,	-3,	0,	0,
		0,	-3,	0,	0,
		1,	-3,	0,	0,
		2,	-3,	0,	0,
		3,	-3,	0,	0,
		4,	-3,	0,	0,
		-4,	-2,	0,	0,
		-3,	-2,	0,	0,
		-2,	-2,	0,	0,
		-1,	-2,	0,	0,
		0,	-2,	0,	0,
		1,	-2,	0,	0,
		2,	-2,	0,	0,
		3,	-2,	0,	0,
		4,	-2,	0,	0,
		-4,	-1,	0,	0,
		-3,	-1,	0,	0,
		-2,	-1,	0,	0,
		-1,	-1,	0,	0,
		0,	-1,	0,	0,
		1,	-1,	0,	0,
		2,	-1,	0,	0,
		3,	-1,	0,	0,
		4,	-1,	0,	0,
		-4,	0,	0,	0,
		-3,	0,	0,	0,
		-2,	0,	0,	0,
		-1,	0,	0,	0,
		0,	0,	0,	0,
		1,	0,	0,	0,
		2,	0,	0,	0,
		3,	0,	0,	0,
		4,	0,	0,	0,
		-4,	1,	0,	0,
		-3,	1,	0,	0,
		-2,	1,	0,	0,
		-1,	1,	0,	0,
		0,	1,	0,	0,
		1,	1,	0,	0,
		2,	1,	0,	0,
		3,	1,	0,	0,
		4,	1,	0,	0,
		-4,	2,	0,	0,
		-3,	2,	0,	0,
		-2,	2,	0,	0,
		-1,	2,	0,	0,
		0,	2,	0,	0,
		1,	2,	0,	0,
		2,	2,	0,	0,
		3,	2,	0,	0,
		4,	2,	0,	0,
		-4,	3,	0,	0,
		-3,	3,	0,	0,
		-2,	3,	0,	0,
		-1,	3,	0,	0,
		0,	3,	0,	0,
		1,	3,	0,	0,
		2,	3,	0,	0,
		3,	3,	0,	0,
		4,	3,	0,	0,
		-4,	4,	0,	0,
		-3,	4,	0,	0,
		-2,	4,	0,	0,
		-1,	4,	0,	0,
		0,	4,	0,	0,
		1,	4,	0,	0,
		2,	4,	0,	0,
		3,	4,	0,	0,
		4,	4,	0,	0,
		-4,	-4,	1,	0,
		-3,	-4,	1,	0,
		-2,	-4,	1,	0,
		-1,	-4,	1,	0,
		0,	-4,	1,	0,
		1,	-4,	1,	0,
		2,	-4,	1,	0,
		3,	-4,	1,	0,
		4,	-4,	1,	0,
		-4,	-3,	1,	0,
		-3,	-3,	1,	0,
		-2,	-3,	1,	0,
		-1,	-3,	1,	0,
		0,	-3,	1,	0,
		1,	-3,	1,	0,
		2,	-3,	1,	0,
		3,	-3,	1,	0,
		4,	-3,	1,	0,
		-4,	-2,	1,	0,
		-3,	-2,	1,	0,
		-2,	-2,	1,	0,
		-1,	-2,	1,	0,
		0,	-2,	1,	0,
		1,	-2,	1,	0,
		2,	-2,	1,	0,
		3,	-2,	1,	0,
		4,	-2,	1,	0,
		-4,	-1,	1,	0,
		-3,	-1,	1,	0,
		-2,	-1,	1,	0,
		-1,	-1,	1,	0,
		0,	-1,	1,	0,
		1,	-1,	1,	0,
		2,	-1,	1,	0,
		3,	-1,	1,	0,
		4,	-1,	1,	0,
		-4,	0,	1,	0,
		-3,	0,	1,	0,
		-2,	0,	1,	0,
		-1,	0,	1,	0,
		0,	0,	1,	0,
		1,	0,	1,	0,
		2,	0,	1,	0,
		3,	0,	1,	0,
		4,	0,	1,	0,
		-4,	1,	1,	0,
		-3,	1,	1,	0,
		-2,	1,	1,	0,
		-1,	1,	1,	0,
		0,	1,	1,	0,
		1,	1,	1,	0,
		2,	1,	1,	0,
		3,	1,	1,	0,
		4,	1,	1,	0,
		-4,	2,	1,	0,
		-3,	2,	1,	0,
		-2,	2,	1,	0,
		-1,	2,	1,	0,
		0,	2,	1,	0,
		1,	2,	1,	0,
		2,	2,	1,	0,
		3,	2,	1,	0,
		4,	2,	1,	0,
		-4,	3,	1,	0,
		-3,	3,	1,	0,
		-2,	3,	1,	0,
		-1,	3,	1,	0,
		0,	3,	1,	0,
		1,	3,	1,	0,
		2,	3,	1,	0,
		3,	3,	1,	0,
		4,	3,	1,	0,
		-4,	4,	1,	0,
		-3,	4,	1,	0,
		-2,	4,	1,	0,
		-1,	4,	1,	0,
		0,	4,	1,	0,
		1,	4,	1,	0,
		2,	4,	1,	0,
		3,	4,	1,	0,
		4,	4,	1,	0,
		-4,	-4,	2,	0,
		-3,	-4,	2,	0,
		-2,	-4,	2,	0,
		-1,	-4,	2,	0,
		0,	-4,	2,	0,
		1,	-4,	2,	0,
		2,	-4,	2,	0,
		3,	-4,	2,	0,
		4,	-4,	2,	0,
		-4,	-3,	2,	0,
		-3,	-3,	2,	0,
		-2,	-3,	2,	0,
		-1,	-3,	2,	0,
		0,	-3,	2,	0,
		1,	-3,	2,	0,
		2,	-3,	2,	0,
		3,	-3,	2,	0,
		4,	-3,	2,	0,
		-4,	-2,	2,	0,
		-3,	-2,	2,	0,
		-2,	-2,	2,	0,
		-1,	-2,	2,	0,
		0,	-2,	2,	0,
		1,	-2,	2,	0,
		2,	-2,	2,	0,
		3,	-2,	2,	0,
		4,	-2,	2,	0,
		-4,	-1,	2,	0,
		-3,	-1,	2,	0,
		-2,	-1,	2,	0,
		-1,	-1,	2,	0,
		0,	-1,	2,	0,
		1,	-1,	2,	0,
		2,	-1,	2,	0,
		3,	-1,	2,	0,
		4,	-1,	2,	0,
		-4,	0,	2,	0,
		-3,	0,	2,	0,
		-2,	0,	2,	0,
		-1,	0,	2,	0,
		0,	0,	2,	0,
		1,	0,	2,	0,
		2,	0,	2,	0,
		3,	0,	2,	0,
		4,	0,	2,	0,
		-4,	1,	2,	0,
		-3,	1,	2,	0,
		-2,	1,	2,	0,
		-1,	1,	2,	0,
		0,	1,	2,	0,
		1,	1,	2,	0,
		2,	1,	2,	0,
		3,	1,	2,	0,
		4,	1,	2,	0,
		-4,	2,	2,	0,
		-3,	2,	2,	0,
		-2,	2,	2,	0,
		-1,	2,	2,	0,
		0,	2,	2,	0,
		1,	2,	2,	0,
		2,	2,	2,	0,
		3,	2,	2,	0,
		4,	2,	2,	0,
		-4,	3,	2,	0,
		-3,	3,	2,	0,
		-2,	3,	2,	0,
		-1,	3,	2,	0,
		0,	3,	2,	0,
		1,	3,	2,	0,
		2,	3,	2,	0,
		3,	3,	2,	0,
		4,	3,	2,	0,
		-4,	4,	2,	0,
		-3,	4,	2,	0,
		-2,	4,	2,	0,
		-1,	4,	2,	0,
		0,	4,	2,	0,
		1,	4,	2,	0,
		2,	4,	2,	0,
		3,	4,	2,	0,
		4,	4,	2,	0,
		-4,	-4,	3,	0,
		-3,	-4,	3,	0,
		-2,	-4,	3,	0,
		-1,	-4,	3,	0,
		0,	-4,	3,	0,
		1,	-4,	3,	0,
		2,	-4,	3,	0,
		3,	-4,	3,	0,
		4,	-4,	3,	0,
		-4,	-3,	3,	0,
		-3,	-3,	3,	0,
		-2,	-3,	3,	0,
		-1,	-3,	3,	0,
		0,	-3,	3,	0,
		1,	-3,	3,	0,
		2,	-3,	3,	0,
		3,	-3,	3,	0,
		4,	-3,	3,	0,
		-4,	-2,	3,	0,
		-3,	-2,	3,	0,
		-2,	-2,	3,	0,
		-1,	-2,	3,	0,
		0,	-2,	3,	0,
		1,	-2,	3,	0,
		2,	-2,	3,	0,
		3,	-2,	3,	0,
		4,	-2,	3,	0,
		-4,	-1,	3,	0,
		-3,	-1,	3,	0,
		-2,	-1,	3,	0,
		-1,	-1,	3,	0,
		0,	-1,	3,	0,
		1,	-1,	3,	0,
		2,	-1,	3,	0,
		3,	-1,	3,	0,
		4,	-1,	3,	0,
		-4,	0,	3,	0,
		-3,	0,	3,	0,
		-2,	0,	3,	0,
		-1,	0,	3,	0,
		0,	0,	3,	0,
		1,	0,	3,	0,
		2,	0,	3,	0,
		3,	0,	3,	0,
		4,	0,	3,	0,
		-4,	1,	3,	0,
		-3,	1,	3,	0,
		-2,	1,	3,	0,
		-1,	1,	3,	0,
		0,	1,	3,	0,
		1,	1,	3,	0,
		2,	1,	3,	0,
		3,	1,	3,	0,
		4,	1,	3,	0,
		-4,	2,	3,	0,
		-3,	2,	3,	0,
		-2,	2,	3,	0,
		-1,	2,	3,	0,
		0,	2,	3,	0,
		1,	2,	3,	0,
		2,	2,	3,	0,
		3,	2,	3,	0,
		4,	2,	3,	0,
		-4,	3,	3,	0,
		-3,	3,	3,	0,
		-2,	3,	3,	0,
		-1,	3,	3,	0,
		0,	3,	3,	0,
		1,	3,	3,	0,
		2,	3,	3,	0,
		3,	3,	3,	0,
		4,	3,	3,	0,
		-4,	4,	3,	0,
		-3,	4,	3,	0,
		-2,	4,	3,	0,
		-1,	4,	3,	0,
		0,	4,	3,	0,
		1,	4,	3,	0,
		2,	4,	3,	0,
		3,	4,	3,	0,
		4,	4,	3,	0,
		-4,	-4,	4,	0,
		-3,	-4,	4,	0,
		-2,	-4,	4,	0,
		-1,	-4,	4,	0,
		0,	-4,	4,	0,
		1,	-4,	4,	0,
		2,	-4,	4,	0,
		3,	-4,	4,	0,
		4,	-4,	4,	0,
		-4,	-3,	4,	0,
		-3,	-3,	4,	0,
		-2,	-3,	4,	0,
		-1,	-3,	4,	0,
		0,	-3,	4,	0,
		1,	-3,	4,	0,
		2,	-3,	4,	0,
		3,	-3,	4,	0,
		4,	-3,	4,	0,
		-4,	-2,	4,	0,
		-3,	-2,	4,	0,
		-2,	-2,	4,	0,
		-1,	-2,	4,	0,
		0,	-2,	4,	0,
		1,	-2,	4,	0,
		2,	-2,	4,	0,
		3,	-2,	4,	0,
		4,	-2,	4,	0,
		-4,	-1,	4,	0,
		-3,	-1,	4,	0,
		-2,	-1,	4,	0,
		-1,	-1,	4,	0,
		0,	-1,	4,	0,
		1,	-1,	4,	0,
		2,	-1,	4,	0,
		3,	-1,	4,	0,
		4,	-1,	4,	0,
		-4,	0,	4,	0,
		-3,	0,	4,	0,
		-2,	0,	4,	0,
		-1,	0,	4,	0,
		0,	0,	4,	0,
		1,	0,	4,	0,
		2,	0,	4,	0,
		3,	0,	4,	0,
		4,	0,	4,	0,
		-4,	1,	4,	0,
		-3,	1,	4,	0,
		-2,	1,	4,	0,
		-1,	1,	4,	0,
		0,	1,	4,	0,
		1,	1,	4,	0,
		2,	1,	4,	0,
		3,	1,	4,	0,
		4,	1,	4,	0,
		-4,	2,	4,	0,
		-3,	2,	4,	0,
		-2,	2,	4,	0,
		-1,	2,	4,	0,
		0,	2,	4,	0,
		1,	2,	4,	0,
		2,	2,	4,	0,
		3,	2,	4,	0,
		4,	2,	4,	0,
		-4,	3,	4,	0,
		-3,	3,	4,	0,
		-2,	3,	4,	0,
		-1,	3,	4,	0,
		0,	3,	4,	0,
		1,	3,	4,	0,
		2,	3,	4,	0,
		3,	3,	4,	0,
		4,	3,	4,	0,
		-4,	4,	4,	0,
		-3,	4,	4,	0,
		-2,	4,	4,	0,
		-1,	4,	4,	0,
		0,	4,	4,	0,
		1,	4,	4,	0,
		2,	4,	4,	0,
		3,	4,	4,	0,
		4,	4,	4,	0,
		-4,	-4,	-4,	0,
		-3,	-4,	-4,	0,
		-2,	-4,	-4,	0,
		-1,	-4,	-4,	0,
		0,	-4,	-4,	0,
		1,	-4,	-4,	0,
		2,	-4,	-4,	0,
		3,	-4,	-4,	0,
		4,	-4,	-4,	0,
		-4,	-3,	-4,	0,
		-3,	-3,	-4,	0,
		-2,	-3,	-4,	0,
		-1,	-3,	-4,	0,
		0,	-3,	-4,	0,
		1,	-3,	-4,	0,
		2,	-3,	-4,	0,
		3,	-3,	-4,	0,
		4,	-3,	-4,	0,
		-4,	-2,	-4,	0,
		-3,	-2,	-4,	0,
		-2,	-2,	-4,	0,
		-1,	-2,	-4,	0,
		0,	-2,	-4,	0,
		1,	-2,	-4,	0,
		2,	-2,	-4,	0,
		3,	-2,	-4,	0,
		4,	-2,	-4,	0,
		-4,	-1,	-4,	0,
		-3,	-1,	-4,	0,
		-2,	-1,	-4,	0,
		-1,	-1,	-4,	0,
		0,	-1,	-4,	0,
		1,	-1,	-4,	0,
		2,	-1,	-4,	0,
		3,	-1,	-4,	0,
		4,	-1,	-4,	0,
		-4,	0,	-4,	0,
		-3,	0,	-4,	0,
		-2,	0,	-4,	0,
		-1,	0,	-4,	0,
		0,	0,	-4,	0,
		1,	0,	-4,	0,
		2,	0,	-4,	0,
		3,	0,	-4,	0,
		4,	0,	-4,	0,
		-4,	1,	-4,	0,
		-3,	1,	-4,	0,
		-2,	1,	-4,	0,
		-1,	1,	-4,	0,
		0,	1,	-4,	0,
		1,	1,	-4,	0,
		2,	1,	-4,	0,
		3,	1,	-4,	0,
		4,	1,	-4,	0,
		-4,	2,	-4,	0,
		-3,	2,	-4,	0,
		-2,	2,	-4,	0,
		-1,	2,	-4,	0,
		0,	2,	-4,	0,
		1,	2,	-4,	0,
		2,	2,	-4,	0,
		3,	2,	-4,	0,
		4,	2,	-4,	0,
		-4,	3,	-4,	0,
		-3,	3,	-4,	0,
		-2,	3,	-4,	0,
		-1,	3,	-4,	0,
		0,	3,	-4,	0,
		1,	3,	-4,	0,
		2,	3,	-4,	0,
		3,	3,	-4,	0,
		4,	3,	-4,	0,
		-4,	4,	-4,	0,
		-3,	4,	-4,	0,
		-2,	4,	-4,	0,
		-1,	4,	-4,	0,
		0,	4,	-4,	0,
		1,	4,	-4,	0,
		2,	4,	-4,	0,
		3,	4,	-4,	0,
		4,	4,	-4,	0,
		-4,	-4,	-3,	0,
		-3,	-4,	-3,	0,
		-2,	-4,	-3,	0,
		-1,	-4,	-3,	0,
		0,	-4,	-3,	0,
		1,	-4,	-3,	0,
		2,	-4,	-3,	0,
		3,	-4,	-3,	0,
		4,	-4,	-3,	0,
		-4,	-3,	-3,	0,
		-3,	-3,	-3,	0,
		-2,	-3,	-3,	0,
		-1,	-3,	-3,	0,
		0,	-3,	-3,	0,
		1,	-3,	-3,	0,
		2,	-3,	-3,	0,
		3,	-3,	-3,	0,
		4,	-3,	-3,	0,
		-4,	-2,	-3,	0,
		-3,	-2,	-3,	0,
		-2,	-2,	-3,	0,
		-1,	-2,	-3,	0,
		0,	-2,	-3,	0,
		1,	-2,	-3,	0,
		2,	-2,	-3,	0,
		3,	-2,	-3,	0,
		4,	-2,	-3,	0,
		-4,	-1,	-3,	0,
		-3,	-1,	-3,	0,
		-2,	-1,	-3,	0,
		-1,	-1,	-3,	0,
		0,	-1,	-3,	0,
		1,	-1,	-3,	0,
		2,	-1,	-3,	0,
		3,	-1,	-3,	0,
		4,	-1,	-3,	0,
		-4,	0,	-3,	0,
		-3,	0,	-3,	0,
		-2,	0,	-3,	0,
		-1,	0,	-3,	0,
		0,	0,	-3,	0,
		1,	0,	-3,	0,
		2,	0,	-3,	0,
		3,	0,	-3,	0,
		4,	0,	-3,	0,
		-4,	1,	-3,	0,
		-3,	1,	-3,	0,
		-2,	1,	-3,	0,
		-1,	1,	-3,	0,
		0,	1,	-3,	0,
		1,	1,	-3,	0,
		2,	1,	-3,	0,
		3,	1,	-3,	0,
		4,	1,	-3,	0,
		-4,	2,	-3,	0,
		-3,	2,	-3,	0,
		-2,	2,	-3,	0,
		-1,	2,	-3,	0,
		0,	2,	-3,	0,
		1,	2,	-3,	0,
		2,	2,	-3,	0,
		3,	2,	-3,	0,
		4,	2,	-3,	0,
		-4,	3,	-3,	0,
		-3,	3,	-3,	0,
		-2,	3,	-3,	0,
		-1,	3,	-3,	0,
		0,	3,	-3,	0,
		1,	3,	-3,	0,
		2,	3,	-3,	0,
		3,	3,	-3,	0,
		4,	3,	-3,	0,
		-4,	4,	-3,	0,
		-3,	4,	-3,	0,
		-2,	4,	-3,	0,
		-1,	4,	-3,	0,
		0,	4,	-3,	0,
		1,	4,	-3,	0,
		2,	4,	-3,	0,
		3,	4,	-3,	0,
		4,	4,	-3,	0,
		-4,	-4,	-2,	0,
		-3,	-4,	-2,	0,
		-2,	-4,	-2,	0,
		-1,	-4,	-2,	0,
		0,	-4,	-2,	0,
		1,	-4,	-2,	0,
		2,	-4,	-2,	0,
		3,	-4,	-2,	0,
		4,	-4,	-2,	0,
		-4,	-3,	-2,	0,
		-3,	-3,	-2,	0,
		-2,	-3,	-2,	0,
		-1,	-3,	-2,	0,
		0,	-3,	-2,	0,
		1,	-3,	-2,	0,
		2,	-3,	-2,	0,
		3,	-3,	-2,	0,
		4,	-3,	-2,	0,
		-4,	-2,	-2,	0,
		-3,	-2,	-2,	0,
		-2,	-2,	-2,	0,
		-1,	-2,	-2,	0,
		0,	-2,	-2,	0,
		1,	-2,	-2,	0,
		2,	-2,	-2,	0,
		3,	-2,	-2,	0,
		4,	-2,	-2,	0,
		-4,	-1,	-2,	0,
		-3,	-1,	-2,	0,
		-2,	-1,	-2,	0,
		-1,	-1,	-2,	0,
		0,	-1,	-2,	0,
		1,	-1,	-2,	0,
		2,	-1,	-2,	0,
		3,	-1,	-2,	0,
		4,	-1,	-2,	0,
		-4,	0,	-2,	0,
		-3,	0,	-2,	0,
		-2,	0,	-2,	0,
		-1,	0,	-2,	0,
		0,	0,	-2,	0,
		1,	0,	-2,	0,
		2,	0,	-2,	0,
		3,	0,	-2,	0,
		4,	0,	-2,	0,
		-4,	1,	-2,	0,
		-3,	1,	-2,	0,
		-2,	1,	-2,	0,
		-1,	1,	-2,	0,
		0,	1,	-2,	0,
		1,	1,	-2,	0,
		2,	1,	-2,	0,
		3,	1,	-2,	0,
		4,	1,	-2,	0,
		-4,	2,	-2,	0,
		-3,	2,	-2,	0,
		-2,	2,	-2,	0,
		-1,	2,	-2,	0,
		0,	2,	-2,	0,
		1,	2,	-2,	0,
		2,	2,	-2,	0,
		3,	2,	-2,	0,
		4,	2,	-2,	0,
		-4,	3,	-2,	0,
		-3,	3,	-2,	0,
		-2,	3,	-2,	0,
		-1,	3,	-2,	0,
		0,	3,	-2,	0,
		1,	3,	-2,	0,
		2,	3,	-2,	0,
		3,	3,	-2,	0,
		4,	3,	-2,	0,
		-4,	4,	-2,	0,
		-3,	4,	-2,	0,
		-2,	4,	-2,	0,
		-1,	4,	-2,	0,
		0,	4,	-2,	0,
		1,	4,	-2,	0,
		2,	4,	-2,	0,
		3,	4,	-2,	0,
		4,	4,	-2,	0,
		-4,	-4,	-1,	0,
		-3,	-4,	-1,	0,
		-2,	-4,	-1,	0,
		-1,	-4,	-1,	0,
		0,	-4,	-1,	0,
		1,	-4,	-1,	0,
		2,	-4,	-1,	0,
		3,	-4,	-1,	0,
		4,	-4,	-1,	0,
		-4,	-3,	-1,	0,
		-3,	-3,	-1,	0,
		-2,	-3,	-1,	0,
		-1,	-3,	-1,	0,
		0,	-3,	-1,	0,
		1,	-3,	-1,	0,
		2,	-3,	-1,	0,
		3,	-3,	-1,	0,
		4,	-3,	-1,	0,
		-4,	-2,	-1,	0,
		-3,	-2,	-1,	0,
		-2,	-2,	-1,	0,
		-1,	-2,	-1,	0,
		0,	-2,	-1,	0,
		1,	-2,	-1,	0,
		2,	-2,	-1,	0,
		3,	-2,	-1,	0,
		4,	-2,	-1,	0,
		-4,	-1,	-1,	0,
		-3,	-1,	-1,	0,
		-2,	-1,	-1,	0,
		-1,	-1,	-1,	0,
		0,	-1,	-1,	0,
		1,	-1,	-1,	0,
		2,	-1,	-1,	0,
		3,	-1,	-1,	0,
		4,	-1,	-1,	0,
		-4,	0,	-1,	0,
		-3,	0,	-1,	0,
		-2,	0,	-1,	0,
		-1,	0,	-1,	0,
		0,	0,	-1,	0,
		1,	0,	-1,	0,
		2,	0,	-1,	0,
		3,	0,	-1,	0,
		4,	0,	-1,	0,
		-4,	1,	-1,	0,
		-3,	1,	-1,	0,
		-2,	1,	-1,	0,
		-1,	1,	-1,	0,
		0,	1,	-1,	0,
		1,	1,	-1,	0,
		2,	1,	-1,	0,
	};


/*


void Audio::MP3::GetTriplet_II(const unsigned char (* q_table)[20], unsigned int gr_sb, unsigned int c_idx) {
	static const unsigned int d_incr[] = {5, 1, 1, 1};	
	unsigned int b_val(0), d_index(gr_sb >> 16), a_index(0);
	int * d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data), scale_val;
	if (c_idx & 1) d_ptr += MP3_CHANNEL_SIZE;

	gr_sb &= 0x0000FFFF;
	
	a_index = q_table[gr_sb][decoder_cfg.current_cfg[c_idx & 1].allocation_idc[gr_sb]];
	gr_sb *= 3;

	reinterpret_cast<float *>(&scale_val)[0] = reinterpret_cast<const float *>(layer_II_mappings[a_index])[0]*reinterpret_cast<const float *>(scale_fac_I_II)[decoder_cfg.current_cfg[c_idx & 1].sf_idc[gr_sb + (d_index >> 2)]];

	d_index = ((gr_sb<<2) + d_index)*3;	
	d_index = ((d_index & 0xFFFFFFFC)<<1) | (d_index & 3);
	
	d_ptr += d_index++;
	d_index &= 3;
		

	if (layer_II_mappings[a_index][1]) {
		b_val = decoder_cfg.GetNumber(layer_II_mappings[a_index][2]) + layer_II_mappings[a_index][1] - 1;
	
		d_ptr[0] = mod_lav_II[b_val][0];
		d_ptr[0] += layer_II_mappings[a_index][3]; // + D
		d_ptr[4] = scale_val;
		
		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];			
		}

		d_ptr += d_incr[d_index++];
			
		d_ptr[0] = mod_lav_II[b_val][1];
		d_ptr[0] += layer_II_mappings[a_index][3];
		d_ptr[4] = scale_val;
		
		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];			
		}

		d_ptr += d_incr[d_index & 3];

		d_ptr[0] = mod_lav_II[b_val][2];
		d_ptr[0] += layer_II_mappings[a_index][3]; 
		d_ptr[4] = scale_val;

		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];			
		}


	} else {
		b_val = (32 - layer_II_mappings[a_index][2]);
						
		d_ptr[0] = ((decoder_cfg.GetNumber(layer_II_mappings[a_index][2]) << b_val) ^ 0x80000000);
		d_ptr[0] >>= b_val;
		d_ptr[0] += layer_II_mappings[a_index][3];
		d_ptr[4] = scale_val;
		
		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];
		}

		d_ptr += d_incr[d_index++];

		d_ptr[0] = ((decoder_cfg.GetNumber(layer_II_mappings[a_index][2]) << b_val) ^ 0x80000000);
		d_ptr[0] >>= b_val;
		d_ptr[0] += layer_II_mappings[a_index][3];
		d_ptr[4] = scale_val;
				
		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];
		}

		d_ptr += d_incr[d_index & 3];

		d_ptr[0] = ((decoder_cfg.GetNumber(layer_II_mappings[a_index][2]) << b_val) ^ 0x80000000);
		d_ptr[0] >>= b_val;
		d_ptr[0] += layer_II_mappings[a_index][3];
		d_ptr[4] = scale_val;
		
		if (c_idx & 2) {
			d_ptr[MP3_CHANNEL_SIZE] = d_ptr[0];
			d_ptr[MP3_CHANNEL_SIZE + 4] = d_ptr[4];
		}

	}

}

*/

//const unsigned int Audio::MP3::layer_I_denom[] = {0x3F800000, 0x3EAAAAAA, 0x3E124924, 0x3D888888, 0x3D042108, 0x3C820820, 0x3C010204, 0x3B808080, 0x3B004020, 0x3A802008, 0x3A001002, 0x39800800, 0x39000400, 0x38800200, 0x38000100, 0x37800080};

/*
const unsigned int Audio::MP3::scale_fac_I_II[] = {
	0x40000000, 0x3FCB2FF5, 0x3FA14518, 0x3F800000, 0x3F4B2FF5, 0x3F214518, 0x3F000000, 0x3ECB2FF5,
	0x3EA14518, 0x3E800000, 0x3E4B2FF5, 0x3E214518, 0x3E000000, 0x3DCB2FF5, 0x3DA14518, 0x3D800000,
	0x3D4B2FF5, 0x3D214518, 0x3D000000, 0x3CCB2FF5, 0x3CA14518, 0x3C800000, 0x3C4B2FF5, 0x3C214518,
	0x3C000000, 0x3BCB2FF5, 0x3BA14518, 0x3B800000, 0x3B4B2FF5, 0x3B214518, 0x3B000000, 0x3ACB2FF5,
	0x3AA14518, 0x3A800000, 0x3A4B2FF5, 0x3A214518, 0x3A000000, 0x39CB2FF5, 0x39A14518, 0x39800000,
	0x394B2FF5, 0x39214518, 0x39000000, 0x38CB2FF5, 0x38A14518, 0x38800000, 0x384B2FF5, 0x38214518,
	0x38000000, 0x37CB2FF5, 0x37A14518, 0x37800000, 0x374B2FF5, 0x37214518, 0x37000000, 0x36CB2FF5,
	0x36A14518, 0x36800000, 0x364B2FF5, 0x36214518, 0x36000000, 0x35CB2FF5, 0x35A14518, 0x35800000
};
*/


