
; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


EXTERN	?alias_cofs@MP3@Audio@@0QBMB:DWORD
EXTERN	?cos_table_12@MP3@Audio@@0PAMA:DWORD
EXTERN	?cos_table_36@MP3@Audio@@0PAMA:DWORD
EXTERN	?synth_cos@MP3@Audio@@0PAMA:DWORD
EXTERN	?synth_window@MP3@Audio@@0QBIB:DWORD



.CONST
ALIGN 16
sign_flag1	DWORD	00000000h, 00000000h, 00000000h, 80000000h
sign_flag2	DWORD	80000000h, 80000000h, 00000000h, 00000000h
phase_flip	DWORD	00000000h, 80000000h, 00000000h, 80000000h

.CODE



ALIGN 16
?MSAdjust@MP3@Audio@@CAXPEAM@Z	PROC	

	LEA r11, [rcx + 4608] ; g1

	MOV eax, 16

align 16
	g0_loop:
		MOVAPS xmm0, [rcx]	
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm4, [rcx + 64]
		MOVAPS xmm6, [rcx + 96]

		MOVAPS xmm1, xmm0
		MOVAPS xmm3, xmm2
		MOVAPS xmm5, xmm4
		MOVAPS xmm7, xmm6


		ADDPS xmm0, [rcx + 18432]
		SUBPS xmm1, [rcx + 18432]

		ADDPS xmm2, [rcx + 18432 + 32]
		SUBPS xmm3, [rcx + 18432 + 32]

		ADDPS xmm4, [rcx + 18432 + 64]
		SUBPS xmm5, [rcx + 18432 + 64]

		ADDPS xmm6, [rcx + 18432 + 96]
		SUBPS xmm7, [rcx + 18432 + 96]


		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm0
		MOVAPS [rcx + 32], xmm2
		MOVAPS [rcx + 48], xmm2
		MOVAPS [rcx + 64], xmm4
		MOVAPS [rcx + 80], xmm4
		MOVAPS [rcx + 96], xmm6
		MOVAPS [rcx + 112], xmm6

		MOVAPS [rcx + 18432], xmm1
		MOVAPS [rcx + 18432 + 16], xmm1
		MOVAPS [rcx + 18432 + 32], xmm3
		MOVAPS [rcx + 18432 + 48], xmm3
		MOVAPS [rcx + 18432 + 64], xmm5
		MOVAPS [rcx + 18432 + 80], xmm5
		MOVAPS [rcx + 18432 + 96], xmm7
		MOVAPS [rcx + 18432 + 112], xmm7


	ADD rcx, 128

		MOVAPS xmm0, [rcx]	
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm4, [rcx + 64]
		MOVAPS xmm6, [rcx + 96]
		MOVAPS xmm8, [rcx + 128]


		MOVAPS xmm1, xmm0
		MOVAPS xmm3, xmm2
		MOVAPS xmm5, xmm4
		MOVAPS xmm7, xmm6
		MOVAPS xmm9, xmm8


		ADDPS xmm0, [rcx + 18432]
		SUBPS xmm1, [rcx + 18432]

		ADDPS xmm2, [rcx + 18432 + 32]
		SUBPS xmm3, [rcx + 18432 + 32]

		ADDPS xmm4, [rcx + 18432 + 64]
		SUBPS xmm5, [rcx + 18432 + 64]

		ADDPS xmm6, [rcx + 18432 + 96]
		SUBPS xmm7, [rcx + 18432 + 96]

		ADDPS xmm8, [rcx + 18432 + 128]
		SUBPS xmm9, [rcx + 18432 + 128]

		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm0
		MOVAPS [rcx + 32], xmm2
		MOVAPS [rcx + 48], xmm2
		MOVAPS [rcx + 64], xmm4
		MOVAPS [rcx + 80], xmm4
		MOVAPS [rcx + 96], xmm6
		MOVAPS [rcx + 112], xmm6
		MOVAPS [rcx + 128], xmm8
		MOVAPS [rcx + 144], xmm8

		MOVAPS [rcx + 18432], xmm1
		MOVAPS [rcx + 18432 + 16], xmm1
		MOVAPS [rcx + 18432 + 32], xmm3
		MOVAPS [rcx + 18432 + 48], xmm3
		MOVAPS [rcx + 18432 + 64], xmm5
		MOVAPS [rcx + 18432 + 80], xmm5
		MOVAPS [rcx + 18432 + 96], xmm7
		MOVAPS [rcx + 18432 + 112], xmm7
		MOVAPS [rcx + 18432 + 128], xmm9
		MOVAPS [rcx + 18432 + 144], xmm9



		ADD rcx, 160
	DEC eax
	JNZ g0_loop


	MOV eax, 16

align 16
	g1_loop:
		MOVAPS xmm0, [r11]	
		MOVAPS xmm2, [r11 + 32]
		MOVAPS xmm4, [r11 + 64]
		MOVAPS xmm6, [r11 + 96]

		MOVAPS xmm1, xmm0
		MOVAPS xmm3, xmm2
		MOVAPS xmm5, xmm4
		MOVAPS xmm7, xmm6


		ADDPS xmm0, [r11 + 18432]
		SUBPS xmm1, [r11 + 18432]

		ADDPS xmm2, [r11 + 18432 + 32]
		SUBPS xmm3, [r11 + 18432 + 32]

		ADDPS xmm4, [r11 + 18432 + 64]
		SUBPS xmm5, [r11 + 18432 + 64]

		ADDPS xmm6, [r11 + 18432 + 96]
		SUBPS xmm7, [r11 + 18432 + 96]


		MOVAPS [r11], xmm0
		MOVAPS [r11 + 16], xmm0
		MOVAPS [r11 + 32], xmm2
		MOVAPS [r11 + 48], xmm2
		MOVAPS [r11 + 64], xmm4
		MOVAPS [r11 + 80], xmm4
		MOVAPS [r11 + 96], xmm6
		MOVAPS [r11 + 112], xmm6

		MOVAPS [r11 + 18432], xmm1
		MOVAPS [r11 + 18432 + 16], xmm1
		MOVAPS [r11 + 18432 + 32], xmm3
		MOVAPS [r11 + 18432 + 48], xmm3
		MOVAPS [r11 + 18432 + 64], xmm5
		MOVAPS [r11 + 18432 + 80], xmm5
		MOVAPS [r11 + 18432 + 96], xmm7
		MOVAPS [r11 + 18432 + 112], xmm7


	ADD r11, 128

		MOVAPS xmm0, [r11]	
		MOVAPS xmm2, [r11 + 32]
		MOVAPS xmm4, [r11 + 64]
		MOVAPS xmm6, [r11 + 96]
		MOVAPS xmm8, [r11 + 128]


		MOVAPS xmm1, xmm0
		MOVAPS xmm3, xmm2
		MOVAPS xmm5, xmm4
		MOVAPS xmm7, xmm6
		MOVAPS xmm9, xmm8


		ADDPS xmm0, [r11 + 18432]
		SUBPS xmm1, [r11 + 18432]

		ADDPS xmm2, [r11 + 18432 + 32]
		SUBPS xmm3, [r11 + 18432 + 32]

		ADDPS xmm4, [r11 + 18432 + 64]
		SUBPS xmm5, [r11 + 18432 + 64]

		ADDPS xmm6, [r11 + 18432 + 96]
		SUBPS xmm7, [r11 + 18432 + 96]

		ADDPS xmm8, [r11 + 18432 + 128]
		SUBPS xmm9, [r11 + 18432 + 128]

		MOVAPS [r11], xmm0
		MOVAPS [r11 + 16], xmm0
		MOVAPS [r11 + 32], xmm2
		MOVAPS [r11 + 48], xmm2
		MOVAPS [r11 + 64], xmm4
		MOVAPS [r11 + 80], xmm4
		MOVAPS [r11 + 96], xmm6
		MOVAPS [r11 + 112], xmm6
		MOVAPS [r11 + 128], xmm8
		MOVAPS [r11 + 144], xmm8

		MOVAPS [r11 + 18432], xmm1
		MOVAPS [r11 + 18432 + 16], xmm1
		MOVAPS [r11 + 18432 + 32], xmm3
		MOVAPS [r11 + 18432 + 48], xmm3
		MOVAPS [r11 + 18432 + 64], xmm5
		MOVAPS [r11 + 18432 + 80], xmm5
		MOVAPS [r11 + 18432 + 96], xmm7
		MOVAPS [r11 + 18432 + 112], xmm7
		MOVAPS [r11 + 18432 + 128], xmm9
		MOVAPS [r11 + 18432 + 144], xmm9



		ADD r11, 160
	DEC eax
	JNZ g1_loop


	RET
?MSAdjust@MP3@Audio@@CAXPEAM@Z	ENDP


ALIGN 16
?IntesityAdjust@MP3@Audio@@CAXPEAM@Z	PROC


	RET
?IntesityAdjust@MP3@Audio@@CAXPEAM@Z	ENDP

ALIGN 16
?AliasReduction@MP3@Audio@@CAXPEAMHH@Z	PROC
	LEA rax, [?alias_cofs@MP3@Audio@@0QBMB]
		
	
	MOVAPS xmm12, [rax]
	MOVAPS xmm13, [rax + 16]
	MOVAPS xmm14, [rax + 32]
	MOVAPS xmm15, [rax + 48]

	LEA r11, [rcx + 4608]

	MOV eax, edx
	ADD eax, 17
	XOR edx, edx
	MOV r10d, 18
	DIV r10d

	MOV ah, al
	SHR al, 1
	JZ last_g0

align 16
	g0_loop:
	
		MOVLPS xmm0, QWORD PTR [rcx + 72]
		MOVLPS xmm1, QWORD PTR [rcx + 96]
		MOVLPS xmm2, QWORD PTR [rcx + 104]
		MOVLPS xmm3, QWORD PTR [rcx + 128]

		UNPCKLPS xmm0, xmm0
		UNPCKLPS xmm1, xmm1
		UNPCKLPS xmm2, xmm2
		UNPCKLPS xmm3, xmm3

		MULPS xmm0, xmm12
		MULPS xmm1, xmm13
		MULPS xmm2, xmm14
		MULPS xmm3, xmm15

		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		MOVLPS xmm4, QWORD PTR [rcx + 136]
		MOVLPS xmm5, QWORD PTR [rcx + 160]
		MOVLPS xmm6, QWORD PTR [rcx + 168]
		MOVLPS xmm7, QWORD PTR [rcx + 192]
	

		SHUFPS xmm4, xmm4, 05h
		SHUFPS xmm5, xmm5, 05h
		SHUFPS xmm6, xmm6, 05h
		SHUFPS xmm7, xmm7, 05h
	
		MULPS xmm4, xmm15
		MULPS xmm5, xmm14
		MULPS xmm6, xmm13
		MULPS xmm7, xmm12


		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		ADDSUBPS xmm0, xmm7
		ADDSUBPS xmm1, xmm6
		ADDSUBPS xmm2, xmm5
		ADDSUBPS xmm3, xmm4


		SHUFPS xmm0, xmm0, 78h
		SHUFPS xmm1, xmm1, 78h
		SHUFPS xmm2, xmm2, 78h
		SHUFPS xmm3, xmm3, 78h





		MOVLPS xmm4, QWORD PTR [rcx + 224]
		MOVLPS xmm5, QWORD PTR [rcx + 232]
		MOVLPS xmm6, QWORD PTR [rcx + 256]
		MOVLPS xmm7, QWORD PTR [rcx + 264]

		UNPCKLPS xmm4, xmm4
		UNPCKLPS xmm5, xmm5
		UNPCKLPS xmm6, xmm6
		UNPCKLPS xmm7, xmm7

		MULPS xmm4, xmm12
		MULPS xmm5, xmm13
		MULPS xmm6, xmm14
		MULPS xmm7, xmm15

		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		MOVLPS xmm8, QWORD PTR [rcx + 288]
		MOVLPS xmm9, QWORD PTR [rcx + 296]
		MOVLPS xmm10, QWORD PTR [rcx + 320]
		MOVLPS xmm11, QWORD PTR [rcx + 328]
	

		SHUFPS xmm8, xmm8, 05h
		SHUFPS xmm9, xmm9, 05h
		SHUFPS xmm10, xmm10, 05h
		SHUFPS xmm11, xmm11, 05h
	
		MULPS xmm8, xmm15
		MULPS xmm9, xmm14
		MULPS xmm10, xmm13
		MULPS xmm11, xmm12


		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		ADDSUBPS xmm4, xmm11
		ADDSUBPS xmm5, xmm10
		ADDSUBPS xmm6, xmm9
		ADDSUBPS xmm7, xmm8

		SHUFPS xmm4, xmm4, 78h
		SHUFPS xmm5, xmm5, 78h
		SHUFPS xmm6, xmm6, 78h
		SHUFPS xmm7, xmm7, 78h

;		offload
		
		MOVLPS QWORD PTR [rcx + 72], xmm0
		MOVLPS QWORD PTR [rcx + 96], xmm1
		MOVLPS QWORD PTR [rcx + 104], xmm2
		MOVLPS QWORD PTR [rcx + 128], xmm3


		MOVHPS QWORD PTR [rcx + 136], xmm3
		MOVHPS QWORD PTR [rcx + 160], xmm2
		MOVHPS QWORD PTR [rcx + 168], xmm1
		MOVHPS QWORD PTR [rcx + 192], xmm0


		MOVLPS QWORD PTR [rcx + 224], xmm4
		MOVLPS QWORD PTR [rcx + 232], xmm5
		MOVLPS QWORD PTR [rcx + 256], xmm6
		MOVLPS QWORD PTR [rcx + 264], xmm7

		MOVHPS QWORD PTR [rcx + 288], xmm7
		MOVHPS QWORD PTR [rcx + 296], xmm6
		MOVHPS QWORD PTR [rcx + 320], xmm5
		MOVHPS QWORD PTR [rcx + 328], xmm4



		ADD rcx, 288

	DEC al
	JNZ g0_loop
align 16
	last_g0:
		AND ah, 1
		JZ no_g0

			MOVLPS xmm0, QWORD PTR [rcx + 72]
			MOVLPS xmm1, QWORD PTR [rcx + 96]
			MOVLPS xmm2, QWORD PTR [rcx + 104]
			MOVLPS xmm3, QWORD PTR [rcx + 128]

			UNPCKLPS xmm0, xmm0
			UNPCKLPS xmm1, xmm1
			UNPCKLPS xmm2, xmm2
			UNPCKLPS xmm3, xmm3

			MULPS xmm0, xmm12
			MULPS xmm1, xmm13
			MULPS xmm2, xmm14
			MULPS xmm3, xmm15

			SHUFPS xmm12, xmm12, 0B1h
			SHUFPS xmm13, xmm13, 0B1h
			SHUFPS xmm14, xmm14, 0B1h
			SHUFPS xmm15, xmm15, 0B1h


			MOVLPS xmm4, QWORD PTR [rcx + 136]
			MOVLPS xmm5, QWORD PTR [rcx + 160]
			MOVLPS xmm6, QWORD PTR [rcx + 168]
			MOVLPS xmm7, QWORD PTR [rcx + 192]
	

			SHUFPS xmm4, xmm4, 05h
			SHUFPS xmm5, xmm5, 05h
			SHUFPS xmm6, xmm6, 05h
			SHUFPS xmm7, xmm7, 05h
	
			MULPS xmm4, xmm15
			MULPS xmm5, xmm14
			MULPS xmm6, xmm13
			MULPS xmm7, xmm12


			SHUFPS xmm12, xmm12, 0B1h
			SHUFPS xmm13, xmm13, 0B1h
			SHUFPS xmm14, xmm14, 0B1h
			SHUFPS xmm15, xmm15, 0B1h


			ADDSUBPS xmm0, xmm7
			ADDSUBPS xmm1, xmm6
			ADDSUBPS xmm2, xmm5
			ADDSUBPS xmm3, xmm4


			SHUFPS xmm0, xmm0, 78h
			SHUFPS xmm1, xmm1, 78h
			SHUFPS xmm2, xmm2, 78h
			SHUFPS xmm3, xmm3, 78h
	

			MOVLPS QWORD PTR [rcx + 72], xmm0
			MOVLPS QWORD PTR [rcx + 96], xmm1
			MOVLPS QWORD PTR [rcx + 104], xmm2
			MOVLPS QWORD PTR [rcx + 128], xmm3


			MOVHPS QWORD PTR [rcx + 136], xmm3
			MOVHPS QWORD PTR [rcx + 160], xmm2
			MOVHPS QWORD PTR [rcx + 168], xmm1
			MOVHPS QWORD PTR [rcx + 192], xmm0

	
	
	

align 16
	no_g0:
		XOR edx, edx
		MOV eax, r8d
		ADD eax, 17
		MOV r10d, 18
		DIV r10d


	MOV ah, al


	SHR al, 1
	JZ last_g1

align 16
	g1_loop:
	
		MOVLPS xmm0, QWORD PTR [r11 + 72]
		MOVLPS xmm1, QWORD PTR [r11 + 96]
		MOVLPS xmm2, QWORD PTR [r11 + 104]
		MOVLPS xmm3, QWORD PTR [r11 + 128]

		UNPCKLPS xmm0, xmm0
		UNPCKLPS xmm1, xmm1
		UNPCKLPS xmm2, xmm2
		UNPCKLPS xmm3, xmm3

		MULPS xmm0, xmm12
		MULPS xmm1, xmm13
		MULPS xmm2, xmm14
		MULPS xmm3, xmm15

		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		MOVLPS xmm4, QWORD PTR [r11 + 136]
		MOVLPS xmm5, QWORD PTR [r11 + 160]
		MOVLPS xmm6, QWORD PTR [r11 + 168]
		MOVLPS xmm7, QWORD PTR [r11 + 192]
	

		SHUFPS xmm4, xmm4, 05h
		SHUFPS xmm5, xmm5, 05h
		SHUFPS xmm6, xmm6, 05h
		SHUFPS xmm7, xmm7, 05h
	
		MULPS xmm4, xmm15
		MULPS xmm5, xmm14
		MULPS xmm6, xmm13
		MULPS xmm7, xmm12


		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		ADDSUBPS xmm0, xmm7
		ADDSUBPS xmm1, xmm6
		ADDSUBPS xmm2, xmm5
		ADDSUBPS xmm3, xmm4


		SHUFPS xmm0, xmm0, 78h
		SHUFPS xmm1, xmm1, 78h
		SHUFPS xmm2, xmm2, 78h
		SHUFPS xmm3, xmm3, 78h





		MOVLPS xmm4, QWORD PTR [r11 + 224]
		MOVLPS xmm5, QWORD PTR [r11 + 232]
		MOVLPS xmm6, QWORD PTR [r11 + 256]
		MOVLPS xmm7, QWORD PTR [r11 + 264]

		UNPCKLPS xmm4, xmm4
		UNPCKLPS xmm5, xmm5
		UNPCKLPS xmm6, xmm6
		UNPCKLPS xmm7, xmm7

		MULPS xmm4, xmm12
		MULPS xmm5, xmm13
		MULPS xmm6, xmm14
		MULPS xmm7, xmm15

		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		MOVLPS xmm8, QWORD PTR [r11 + 288]
		MOVLPS xmm9, QWORD PTR [r11 + 296]
		MOVLPS xmm10, QWORD PTR [r11 + 320]
		MOVLPS xmm11, QWORD PTR [r11 + 328]
	

		SHUFPS xmm8, xmm8, 05h
		SHUFPS xmm9, xmm9, 05h
		SHUFPS xmm10, xmm10, 05h
		SHUFPS xmm11, xmm11, 05h
	
		MULPS xmm8, xmm15
		MULPS xmm9, xmm14
		MULPS xmm10, xmm13
		MULPS xmm11, xmm12


		SHUFPS xmm12, xmm12, 0B1h
		SHUFPS xmm13, xmm13, 0B1h
		SHUFPS xmm14, xmm14, 0B1h
		SHUFPS xmm15, xmm15, 0B1h


		ADDSUBPS xmm4, xmm11
		ADDSUBPS xmm5, xmm10
		ADDSUBPS xmm6, xmm9
		ADDSUBPS xmm7, xmm8

		SHUFPS xmm4, xmm4, 78h
		SHUFPS xmm5, xmm5, 78h
		SHUFPS xmm6, xmm6, 78h
		SHUFPS xmm7, xmm7, 78h

;		offload
		
		MOVLPS QWORD PTR [r11 + 72], xmm0
		MOVLPS QWORD PTR [r11 + 96], xmm1
		MOVLPS QWORD PTR [r11 + 104], xmm2
		MOVLPS QWORD PTR [r11 + 128], xmm3


		MOVHPS QWORD PTR [r11 + 136], xmm3
		MOVHPS QWORD PTR [r11 + 160], xmm2
		MOVHPS QWORD PTR [r11 + 168], xmm1
		MOVHPS QWORD PTR [r11 + 192], xmm0


		MOVLPS QWORD PTR [r11 + 224], xmm4
		MOVLPS QWORD PTR [r11 + 232], xmm5
		MOVLPS QWORD PTR [r11 + 256], xmm6
		MOVLPS QWORD PTR [r11 + 264], xmm7

		MOVHPS QWORD PTR [r11 + 288], xmm7
		MOVHPS QWORD PTR [r11 + 296], xmm6
		MOVHPS QWORD PTR [r11 + 320], xmm5
		MOVHPS QWORD PTR [r11 + 328], xmm4



		ADD r11, 288

	DEC al
	JNZ g1_loop
align 16
	last_g1:
		AND ah, 1
		JZ no_g1

			MOVLPS xmm0, QWORD PTR [r11 + 72]
			MOVLPS xmm1, QWORD PTR [r11 + 96]
			MOVLPS xmm2, QWORD PTR [r11 + 104]
			MOVLPS xmm3, QWORD PTR [r11 + 128]

			UNPCKLPS xmm0, xmm0
			UNPCKLPS xmm1, xmm1
			UNPCKLPS xmm2, xmm2
			UNPCKLPS xmm3, xmm3

			MULPS xmm0, xmm12
			MULPS xmm1, xmm13
			MULPS xmm2, xmm14
			MULPS xmm3, xmm15

			SHUFPS xmm12, xmm12, 0B1h
			SHUFPS xmm13, xmm13, 0B1h
			SHUFPS xmm14, xmm14, 0B1h
			SHUFPS xmm15, xmm15, 0B1h


			MOVLPS xmm4, QWORD PTR [r11 + 136]
			MOVLPS xmm5, QWORD PTR [r11 + 160]
			MOVLPS xmm6, QWORD PTR [r11 + 168]
			MOVLPS xmm7, QWORD PTR [r11 + 192]
	

			SHUFPS xmm4, xmm4, 05h
			SHUFPS xmm5, xmm5, 05h
			SHUFPS xmm6, xmm6, 05h
			SHUFPS xmm7, xmm7, 05h
	
			MULPS xmm4, xmm15
			MULPS xmm5, xmm14
			MULPS xmm6, xmm13
			MULPS xmm7, xmm12


			SHUFPS xmm12, xmm12, 0B1h
			SHUFPS xmm13, xmm13, 0B1h
			SHUFPS xmm14, xmm14, 0B1h
			SHUFPS xmm15, xmm15, 0B1h


			ADDSUBPS xmm0, xmm7
			ADDSUBPS xmm1, xmm6
			ADDSUBPS xmm2, xmm5
			ADDSUBPS xmm3, xmm4


			SHUFPS xmm0, xmm0, 78h
			SHUFPS xmm1, xmm1, 78h
			SHUFPS xmm2, xmm2, 78h
			SHUFPS xmm3, xmm3, 78h
	

			MOVLPS QWORD PTR [r11 + 72], xmm0
			MOVLPS QWORD PTR [r11 + 96], xmm1
			MOVLPS QWORD PTR [r11 + 104], xmm2
			MOVLPS QWORD PTR [r11 + 128], xmm3


			MOVHPS QWORD PTR [r11 + 136], xmm3
			MOVHPS QWORD PTR [r11 + 160], xmm2
			MOVHPS QWORD PTR [r11 + 168], xmm1
			MOVHPS QWORD PTR [r11 + 192], xmm0

align 16
	no_g1:

	RET
?AliasReduction@MP3@Audio@@CAXPEAMHH@Z	ENDP


ALIGN 16
?IMDCT_12@MP3@Audio@@CAXPEAMH@Z	PROC	
			
	LEA r11, [?cos_table_12@MP3@Audio@@0PAMA]
	
	MOV eax, edx
	ADD eax, 11
	XOR edx, edx
	MOV r10d, 12
	DIV r10d

	MOVAPS xmm14, XMMWORD PTR [sign_flag1]
	MOVAPS xmm15, XMMWORD PTR [sign_flag2]

align 16
	idct_loop:

		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 32]
		MOVAPS xmm2, [rcx + 64]


		MOVAPS xmm3, [r11]
		MOVAPS xmm4, [r11 + 16]
		MOVAPS xmm5, [r11 + 32]

		MULPS xmm3, xmm0
		MULPS xmm4, xmm1
		MULPS xmm5, xmm2

		MOVAPS xmm6, [r11 + 48]
		MOVAPS xmm7, [r11 + 64]
		MOVAPS xmm8, [r11 + 80]

		MULPS xmm6, xmm0
		MULPS xmm7, xmm1
		MULPS xmm8, xmm2

		HADDPS xmm3, xmm4
		HADDPS xmm5, xmm6
		HADDPS xmm7, xmm8

		MOVHLPS xmm4, xmm3
		SHUFPS xmm3, xmm5, 0E4h
		SHUFPS xmm5, xmm7, 0E4h
		UNPCKLPS xmm4, xmm7

		HADDPS xmm3, xmm5
		ADDPS xmm3, xmm4




		MOVAPS xmm4, [r11 + 96]
		MOVAPS xmm5, [r11 + 112]
		MOVAPS xmm6, [r11 + 128]

		MULPS xmm4, xmm0
		MULPS xmm5, xmm1
		MULPS xmm6, xmm2

		MOVAPS xmm7, [r11 + 144]
		MOVAPS xmm8, [r11 + 160]
		MOVAPS xmm9, [r11 + 176]

		MULPS xmm7, xmm0
		MULPS xmm8, xmm1
		MULPS xmm9, xmm2

		HADDPS xmm4, xmm5
		HADDPS xmm6, xmm7
		HADDPS xmm8, xmm9

		MOVHLPS xmm5, xmm4
		SHUFPS xmm4, xmm6, 0E4h
		SHUFPS xmm6, xmm8, 0E4h
		UNPCKLPS xmm5, xmm8

		HADDPS xmm4, xmm6
		ADDPS xmm4, xmm5





		MOVAPS xmm5, [r11 + 192]
		MOVAPS xmm6, [r11 + 208]
		MOVAPS xmm7, [r11 + 224]

		MULPS xmm5, xmm0
		MULPS xmm6, xmm1
		MULPS xmm7, xmm2

		MOVAPS xmm8, [r11 + 240]
		MOVAPS xmm9, [r11 + 256]
		MOVAPS xmm10, [r11 + 272]

		MULPS xmm8, xmm0
		MULPS xmm9, xmm1
		MULPS xmm10, xmm2

		HADDPS xmm5, xmm6
		HADDPS xmm7, xmm8
		HADDPS xmm9, xmm10

		MOVHLPS xmm6, xmm5
		SHUFPS xmm5, xmm7, 0E4h
		SHUFPS xmm7, xmm9, 0E4h
		UNPCKLPS xmm6, xmm9

		HADDPS xmm5, xmm7
		ADDPS xmm5, xmm6



		MOVAPS xmm0, xmm3
		MOVAPS xmm1, xmm3
		MOVAPS xmm2, xmm4


		SHUFPS xmm0, xmm4, 04h
		XORPS xmm0, xmm14
		SHUFPS xmm1, xmm5, 41h
		XORPS xmm1, xmm15
		SHUFPS xmm2, xmm5, 15h

		MOVAPS xmm7, xmm5
		MOVAPS xmm5, xmm4		
		MOVAPS xmm4, xmm3

		SHUFPS xmm3, xmm5, 0AEh
		XORPS xmm3, xmm14
		SHUFPS xmm4, xmm7, 0EBh
		XORPS xmm4, xmm15
		SHUFPS xmm5, xmm7, 0BFh



		MOVAPS [rcx + 9216], xmm0
		MOVAPS [rcx + 9216 + 16], xmm1
		MOVAPS [rcx + 9216 + 32], xmm2
		MOVAPS [rcx + 9216 + 48], xmm3
		MOVAPS [rcx + 9216 + 64], xmm4
		MOVAPS [rcx + 9216 + 80], xmm5
						
		ADD rcx, 96
	DEC eax
	JNZ idct_loop



	RET
?IMDCT_12@MP3@Audio@@CAXPEAMH@Z	ENDP


ALIGN 16
?IMDCT_36@MP3@Audio@@CAXPEAMH@Z	PROC	

	LEA r11, [?cos_table_36@MP3@Audio@@0PAMA]

	MOV eax, edx ; long end
	ADD eax, 35
	XOR edx, edx
	MOV r10d, 36
	DIV r10d

	MOVAPS xmm15, XMMWORD PTR [sign_flag1]
	SHUFPS xmm15, xmm15, 0FCh

align 16
	idct_loop:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 32]
		MOVAPS xmm2, [rcx + 64]
		MOVAPS xmm3, [rcx + 96]
		MULPS xmm0, [r11]
		MULPS xmm1, [r11 + 16]
		MULPS xmm2, [r11 + 32]
		MULPS xmm3, [r11 + 48]
		ADDPS xmm0, xmm1
		ADDPS xmm2, xmm3
		ADDPS xmm0, xmm2

		MOVAPS xmm1, [rcx + 128]
		MULPS xmm1, [r11 + 64]
		
		MOVAPS xmm2, [rcx + 160]
		MOVAPS xmm3, [rcx + 192]
		MOVAPS xmm4, [rcx + 224]
		MOVAPS xmm5, [rcx + 256]
		MULPS xmm2, [r11 + 80]
		MULPS xmm3, [r11 + 96]
		MULPS xmm4, [r11 + 112]
		MULPS xmm5, [r11 + 128]
		ADDPS xmm2, xmm3
		ADDPS xmm4, xmm5
		ADDPS xmm2, xmm4

		HADDPS xmm0, xmm2
		ADDPS xmm0, xmm1

		MOVAPS xmm1, [rcx]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 64]
		MOVAPS xmm4, [rcx + 96]
		MULPS xmm1, [r11 + 144]
		MULPS xmm2, [r11 + 16 + 144]
		MULPS xmm3, [r11 + 32 + 144]
		MULPS xmm4, [r11 + 48 + 144]
		ADDPS xmm1, xmm2
		ADDPS xmm3, xmm4
		ADDPS xmm1, xmm3

		MOVAPS xmm2, [rcx + 128]
		MULPS xmm2, [r11 + 64 + 144]
		
		MOVAPS xmm3, [rcx + 160]
		MOVAPS xmm4, [rcx + 192]
		MOVAPS xmm5, [rcx + 224]
		MOVAPS xmm6, [rcx + 256]
		MULPS xmm3, [r11 + 80 + 144]
		MULPS xmm4, [r11 + 96 + 144]
		MULPS xmm5, [r11 + 112 + 144]
		MULPS xmm6, [r11 + 128 + 144]
		ADDPS xmm3, xmm4
		ADDPS xmm5, xmm6
		ADDPS xmm3, xmm5

		HADDPS xmm1, xmm3
		ADDPS xmm1, xmm2

		HADDPS xmm0, xmm1

	LEA r11, [r11 + 288]

		MOVAPS xmm1, [rcx]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 64]
		MOVAPS xmm4, [rcx + 96]
		MULPS xmm1, [r11]
		MULPS xmm2, [r11 + 16]
		MULPS xmm3, [r11 + 32]
		MULPS xmm4, [r11 + 48]
		ADDPS xmm1, xmm2
		ADDPS xmm3, xmm4
		ADDPS xmm1, xmm3

		MOVAPS xmm2, [rcx + 128]
		MULPS xmm2, [r11 + 64]
		
		MOVAPS xmm3, [rcx + 160]
		MOVAPS xmm4, [rcx + 192]
		MOVAPS xmm5, [rcx + 224]
		MOVAPS xmm6, [rcx + 256]
		MULPS xmm3, [r11 + 80]
		MULPS xmm4, [r11 + 96]
		MULPS xmm5, [r11 + 112]
		MULPS xmm6, [r11 + 128]
		ADDPS xmm3, xmm4
		ADDPS xmm5, xmm6
		ADDPS xmm3, xmm5

		HADDPS xmm1, xmm3
		ADDPS xmm1, xmm2

		MOVAPS xmm2, [rcx]
		MOVAPS xmm3, [rcx + 32]
		MOVAPS xmm4, [rcx + 64]
		MOVAPS xmm5, [rcx + 96]
		MULPS xmm2, [r11 + 144]
		MULPS xmm3, [r11 + 16 + 144]
		MULPS xmm4, [r11 + 32 + 144]
		MULPS xmm5, [r11 + 48 + 144]
		ADDPS xmm2, xmm3
		ADDPS xmm4, xmm5
		ADDPS xmm2, xmm4

		MOVAPS xmm3, [rcx + 128]
		MULPS xmm3, [r11 + 64 + 144]
		
		MOVAPS xmm4, [rcx + 160]
		MOVAPS xmm5, [rcx + 192]
		MOVAPS xmm6, [rcx + 224]
		MOVAPS xmm7, [rcx + 256]
		MULPS xmm4, [r11 + 80 + 144]
		MULPS xmm5, [r11 + 96 + 144]
		MULPS xmm6, [r11 + 112 + 144]
		MULPS xmm7, [r11 + 128 + 144]
		ADDPS xmm4, xmm5
		ADDPS xmm6, xmm7
		ADDPS xmm4, xmm6

		HADDPS xmm2, xmm4
		ADDPS xmm2, xmm3

		HADDPS xmm1, xmm2


	LEA r11, [r11 + 288]

		MOVAPS xmm2, [rcx]
		MOVAPS xmm3, [rcx + 32]
		MOVAPS xmm4, [rcx + 64]
		MOVAPS xmm5, [rcx + 96]
		MULPS xmm2, [r11]
		MULPS xmm3, [r11 + 16]
		MULPS xmm4, [r11 + 32]
		MULPS xmm5, [r11 + 48]
		ADDPS xmm2, xmm3
		ADDPS xmm4, xmm5
		ADDPS xmm2, xmm4

		MOVAPS xmm3, [rcx + 128]
		MULPS xmm3, [r11 + 64]
		
		MOVAPS xmm4, [rcx + 160]
		MOVAPS xmm5, [rcx + 192]
		MOVAPS xmm6, [rcx + 224]
		MOVAPS xmm7, [rcx + 256]
		MULPS xmm4, [r11 + 80]
		MULPS xmm5, [r11 + 96]
		MULPS xmm6, [r11 + 112]
		MULPS xmm7, [r11 + 128]
		ADDPS xmm4, xmm5
		ADDPS xmm6, xmm7
		ADDPS xmm4, xmm6

		HADDPS xmm2, xmm4
		ADDPS xmm2, xmm3

		MOVAPS xmm3, [rcx]
		MOVAPS xmm4, [rcx + 32]
		MOVAPS xmm5, [rcx + 64]
		MOVAPS xmm6, [rcx + 96]
		MULPS xmm3, [r11 + 144]
		MULPS xmm4, [r11 + 16 + 144]
		MULPS xmm5, [r11 + 32 + 144]
		MULPS xmm6, [r11 + 48 + 144]
		ADDPS xmm3, xmm4
		ADDPS xmm5, xmm6
		ADDPS xmm3, xmm5

		MOVAPS xmm4, [rcx + 128]
		MULPS xmm4, [r11 + 64 + 144]
		
		MOVAPS xmm5, [rcx + 160]
		MOVAPS xmm6, [rcx + 192]
		MOVAPS xmm7, [rcx + 224]
		MOVAPS xmm8, [rcx + 256]
		MULPS xmm5, [r11 + 80 + 144]
		MULPS xmm6, [r11 + 96 + 144]
		MULPS xmm7, [r11 + 112 + 144]
		MULPS xmm8, [r11 + 128 + 144]
		ADDPS xmm5, xmm6
		ADDPS xmm7, xmm8
		ADDPS xmm5, xmm7

		HADDPS xmm3, xmm5
		ADDPS xmm3, xmm4

		HADDPS xmm2, xmm3



	LEA r11, [r11 + 288]

		MOVAPS xmm3, [rcx]
		MOVAPS xmm4, [rcx + 32]
		MOVAPS xmm5, [rcx + 64]
		MOVAPS xmm6, [rcx + 96]
		MULPS xmm3, [r11]
		MULPS xmm4, [r11 + 16]
		MULPS xmm5, [r11 + 32]
		MULPS xmm6, [r11 + 48]
		ADDPS xmm3, xmm4
		ADDPS xmm5, xmm6
		ADDPS xmm3, xmm5

		MOVAPS xmm4, [rcx + 128]
		MULPS xmm4, [r11 + 64]
		
		MOVAPS xmm5, [rcx + 160]
		MOVAPS xmm6, [rcx + 192]
		MOVAPS xmm7, [rcx + 224]
		MOVAPS xmm8, [rcx + 256]
		MULPS xmm5, [r11 + 80]
		MULPS xmm6, [r11 + 96]
		MULPS xmm7, [r11 + 112]
		MULPS xmm8, [r11 + 128]
		ADDPS xmm5, xmm6
		ADDPS xmm7, xmm8
		ADDPS xmm5, xmm7

		HADDPS xmm3, xmm5
		ADDPS xmm3, xmm4

		MOVAPS xmm4, [rcx]
		MOVAPS xmm5, [rcx + 32]
		MOVAPS xmm6, [rcx + 64]
		MOVAPS xmm7, [rcx + 96]
		MULPS xmm4, [r11 + 144]
		MULPS xmm5, [r11 + 16 + 144]
		MULPS xmm6, [r11 + 32 + 144]
		MULPS xmm7, [r11 + 48 + 144]
		ADDPS xmm4, xmm5
		ADDPS xmm6, xmm7
		ADDPS xmm4, xmm6

		MOVAPS xmm5, [rcx + 128]
		MULPS xmm5, [r11 + 64 + 144]
		
		MOVAPS xmm6, [rcx + 160]
		MOVAPS xmm7, [rcx + 192]
		MOVAPS xmm8, [rcx + 224]
		MOVAPS xmm9, [rcx + 256]
		MULPS xmm6, [r11 + 80 + 144]
		MULPS xmm7, [r11 + 96 + 144]
		MULPS xmm8, [r11 + 112 + 144]
		MULPS xmm9, [r11 + 128 + 144]
		ADDPS xmm6, xmm7
		ADDPS xmm8, xmm9
		ADDPS xmm6, xmm8

		HADDPS xmm4, xmm6
		ADDPS xmm4, xmm5

		HADDPS xmm3, xmm4



	LEA r11, [r11 + 288]

		MOVAPS xmm4, [rcx]
		MOVAPS xmm5, [rcx + 32]
		MOVAPS xmm6, [rcx + 64]
		MOVAPS xmm7, [rcx + 96]
		MULPS xmm4, [r11]
		MULPS xmm5, [r11 + 16]
		MULPS xmm6, [r11 + 32]
		MULPS xmm7, [r11 + 48]
		ADDPS xmm4, xmm5
		ADDPS xmm6, xmm7
		ADDPS xmm4, xmm6

		MOVAPS xmm5, [rcx + 128]
		MULPS xmm5, [r11 + 64]
		
		MOVAPS xmm6, [rcx + 160]
		MOVAPS xmm7, [rcx + 192]
		MOVAPS xmm8, [rcx + 224]
		MOVAPS xmm9, [rcx + 256]
		MULPS xmm6, [r11 + 80]
		MULPS xmm7, [r11 + 96]
		MULPS xmm8, [r11 + 112]
		MULPS xmm9, [r11 + 128]
		ADDPS xmm6, xmm7
		ADDPS xmm8, xmm9
		ADDPS xmm6, xmm8

		HADDPS xmm4, xmm6
		ADDPS xmm4, xmm5

		MOVAPS xmm5, [rcx]
		MOVAPS xmm6, [rcx + 32]
		MOVAPS xmm7, [rcx + 64]
		MOVAPS xmm8, [rcx + 96]
		MULPS xmm5, [r11 + 144]
		MULPS xmm6, [r11 + 16 + 144]
		MULPS xmm7, [r11 + 32 + 144]
		MULPS xmm8, [r11 + 48 + 144]
		ADDPS xmm5, xmm6
		ADDPS xmm7, xmm8
		ADDPS xmm5, xmm7

		MOVAPS xmm6, [rcx + 128]
		MULPS xmm6, [r11 + 64 + 144]
		
		MOVAPS xmm7, [rcx + 160]
		MOVAPS xmm8, [rcx + 192]
		MOVAPS xmm9, [rcx + 224]
		MOVAPS xmm10, [rcx + 256]
		MULPS xmm7, [r11 + 80 + 144]
		MULPS xmm8, [r11 + 96 + 144]
		MULPS xmm9, [r11 + 112 + 144]
		MULPS xmm10, [r11 + 128 + 144]
		ADDPS xmm7, xmm8
		ADDPS xmm9, xmm10
		ADDPS xmm7, xmm9

		HADDPS xmm5, xmm7
		ADDPS xmm5, xmm6

		HADDPS xmm4, xmm5






	LEA r11, [r11 + 288]

		MOVAPS xmm5, [rcx]
		MOVAPS xmm6, [rcx + 32]
		MOVAPS xmm7, [rcx + 64]
		MOVAPS xmm8, [rcx + 96]
		MULPS xmm5, [r11]
		MULPS xmm6, [r11 + 16]
		MULPS xmm7, [r11 + 32]
		MULPS xmm8, [r11 + 48]
		ADDPS xmm5, xmm6
		ADDPS xmm7, xmm8
		ADDPS xmm5, xmm7

		MOVAPS xmm6, [rcx + 128]
		MULPS xmm6, [r11 + 64]
		
		MOVAPS xmm7, [rcx + 160]
		MOVAPS xmm8, [rcx + 192]
		MOVAPS xmm9, [rcx + 224]
		MOVAPS xmm10, [rcx + 256]
		MULPS xmm7, [r11 + 80]
		MULPS xmm8, [r11 + 96]
		MULPS xmm9, [r11 + 112]
		MULPS xmm10, [r11 + 128]
		ADDPS xmm7, xmm8
		ADDPS xmm9, xmm10
		ADDPS xmm7, xmm9

		HADDPS xmm5, xmm7
		ADDPS xmm5, xmm6

		MOVAPS xmm6, [rcx]
		MOVAPS xmm7, [rcx + 32]
		MOVAPS xmm8, [rcx + 64]
		MOVAPS xmm9, [rcx + 96]
		MULPS xmm6, [r11 + 144]
		MULPS xmm7, [r11 + 16 + 144]
		MULPS xmm8, [r11 + 32 + 144]
		MULPS xmm9, [r11 + 48 + 144]
		ADDPS xmm6, xmm7
		ADDPS xmm8, xmm9
		ADDPS xmm6, xmm8

		MOVAPS xmm7, [rcx + 128]
		MULPS xmm7, [r11 + 64 + 144]
		
		MOVAPS xmm8, [rcx + 160]
		MOVAPS xmm9, [rcx + 192]
		MOVAPS xmm10, [rcx + 224]
		MOVAPS xmm11, [rcx + 256]
		MULPS xmm8, [r11 + 80 + 144]
		MULPS xmm9, [r11 + 96 + 144]
		MULPS xmm10, [r11 + 112 + 144]
		MULPS xmm11, [r11 + 128 + 144]
		ADDPS xmm8, xmm9
		ADDPS xmm10, xmm11
		ADDPS xmm8, xmm10

		HADDPS xmm6, xmm8
		ADDPS xmm6, xmm7

		HADDPS xmm5, xmm6




	LEA r11, [r11 + 288]

		MOVAPS xmm6, [rcx]
		MOVAPS xmm7, [rcx + 32]
		MOVAPS xmm8, [rcx + 64]
		MOVAPS xmm9, [rcx + 96]
		MULPS xmm6, [r11]
		MULPS xmm7, [r11 + 16]
		MULPS xmm8, [r11 + 32]
		MULPS xmm9, [r11 + 48]
		ADDPS xmm6, xmm7
		ADDPS xmm8, xmm9
		ADDPS xmm6, xmm8

		MOVAPS xmm7, [rcx + 128]
		MULPS xmm7, [r11 + 64]
		
		MOVAPS xmm8, [rcx + 160]
		MOVAPS xmm9, [rcx + 192]
		MOVAPS xmm10, [rcx + 224]
		MOVAPS xmm11, [rcx + 256]
		MULPS xmm8, [r11 + 80]
		MULPS xmm9, [r11 + 96]
		MULPS xmm10, [r11 + 112]
		MULPS xmm11, [r11 + 128]
		ADDPS xmm8, xmm9
		ADDPS xmm10, xmm11
		ADDPS xmm8, xmm10

		HADDPS xmm6, xmm8
		ADDPS xmm6, xmm7

		MOVAPS xmm7, [rcx]
		MOVAPS xmm8, [rcx + 32]
		MOVAPS xmm9, [rcx + 64]
		MOVAPS xmm10, [rcx + 96]
		MULPS xmm7, [r11 + 144]
		MULPS xmm8, [r11 + 16 + 144]
		MULPS xmm9, [r11 + 32 + 144]
		MULPS xmm10, [r11 + 48 + 144]
		ADDPS xmm7, xmm8
		ADDPS xmm9, xmm10
		ADDPS xmm7, xmm9

		MOVAPS xmm8, [rcx + 128]
		MULPS xmm8, [r11 + 64 + 144]
		
		MOVAPS xmm9, [rcx + 160]
		MOVAPS xmm10, [rcx + 192]
		MOVAPS xmm11, [rcx + 224]
		MOVAPS xmm12, [rcx + 256]
		MULPS xmm9, [r11 + 80 + 144]
		MULPS xmm10, [r11 + 96 + 144]
		MULPS xmm11, [r11 + 112 + 144]
		MULPS xmm12, [r11 + 128 + 144]
		ADDPS xmm9, xmm10
		ADDPS xmm11, xmm12
		ADDPS xmm9, xmm11

		HADDPS xmm7, xmm9
		ADDPS xmm7, xmm8

		HADDPS xmm6, xmm7




	LEA r11, [r11 + 288]

		MOVAPS xmm7, [rcx]
		MOVAPS xmm8, [rcx + 32]
		MOVAPS xmm9, [rcx + 64]
		MOVAPS xmm10, [rcx + 96]
		MULPS xmm7, [r11]
		MULPS xmm8, [r11 + 16]
		MULPS xmm9, [r11 + 32]
		MULPS xmm10, [r11 + 48]
		ADDPS xmm7, xmm8
		ADDPS xmm9, xmm10
		ADDPS xmm7, xmm9

		MOVAPS xmm8, [rcx + 128]
		MULPS xmm8, [r11 + 64]
		
		MOVAPS xmm9, [rcx + 160]
		MOVAPS xmm10, [rcx + 192]
		MOVAPS xmm11, [rcx + 224]
		MOVAPS xmm12, [rcx + 256]
		MULPS xmm9, [r11 + 80]
		MULPS xmm10, [r11 + 96]
		MULPS xmm11, [r11 + 112]
		MULPS xmm12, [r11 + 128]
		ADDPS xmm9, xmm10
		ADDPS xmm11, xmm12
		ADDPS xmm9, xmm11

		HADDPS xmm7, xmm9
		ADDPS xmm7, xmm8

		MOVAPS xmm8, [rcx]
		MOVAPS xmm9, [rcx + 32]
		MOVAPS xmm10, [rcx + 64]
		MOVAPS xmm11, [rcx + 96]
		MULPS xmm8, [r11 + 144]
		MULPS xmm9, [r11 + 16 + 144]
		MULPS xmm10, [r11 + 32 + 144]
		MULPS xmm11, [r11 + 48 + 144]
		ADDPS xmm8, xmm9
		ADDPS xmm10, xmm11
		ADDPS xmm8, xmm10

		MOVAPS xmm9, [rcx + 128]
		MULPS xmm9, [r11 + 64 + 144]
		
		MOVAPS xmm10, [rcx + 160]
		MOVAPS xmm11, [rcx + 192]
		MOVAPS xmm12, [rcx + 224]
		MOVAPS xmm13, [rcx + 256]
		MULPS xmm10, [r11 + 80 + 144]
		MULPS xmm11, [r11 + 96 + 144]
		MULPS xmm12, [r11 + 112 + 144]
		MULPS xmm13, [r11 + 128 + 144]
		ADDPS xmm10, xmm11
		ADDPS xmm12, xmm13
		ADDPS xmm10, xmm12

		HADDPS xmm8, xmm10
		ADDPS xmm8, xmm9

		HADDPS xmm7, xmm8




	LEA r11, [r11 + 288]

		MOVAPS xmm8, [rcx]
		MOVAPS xmm9, [rcx + 32]
		MOVAPS xmm10, [rcx + 64]
		MOVAPS xmm11, [rcx + 96]
		MULPS xmm8, [r11]
		MULPS xmm9, [r11 + 16]
		MULPS xmm10, [r11 + 32]
		MULPS xmm11, [r11 + 48]
		ADDPS xmm8, xmm9
		ADDPS xmm10, xmm11
		ADDPS xmm8, xmm10

		MOVAPS xmm9, [rcx + 128]
		MULPS xmm9, [r11 + 64]
		
		MOVAPS xmm10, [rcx + 160]
		MOVAPS xmm11, [rcx + 192]
		MOVAPS xmm12, [rcx + 224]
		MOVAPS xmm13, [rcx + 256]
		MULPS xmm10, [r11 + 80]
		MULPS xmm11, [r11 + 96]
		MULPS xmm12, [r11 + 112]
		MULPS xmm13, [r11 + 128]
		ADDPS xmm10, xmm11
		ADDPS xmm12, xmm13
		ADDPS xmm10, xmm12

		HADDPS xmm8, xmm10
		ADDPS xmm8, xmm9

		MOVAPS xmm9, [rcx]
		MOVAPS xmm10, [rcx + 32]
		MOVAPS xmm11, [rcx + 64]
		MOVAPS xmm12, [rcx + 96]
		MULPS xmm9, [r11 + 144]
		MULPS xmm10, [r11 + 16 + 144]
		MULPS xmm11, [r11 + 32 + 144]
		MULPS xmm12, [r11 + 48 + 144]
		ADDPS xmm9, xmm10
		ADDPS xmm11, xmm12
		ADDPS xmm9, xmm11

		MOVAPS xmm10, [rcx + 128]
		MULPS xmm10, [r11 + 64 + 144]
		
		MOVAPS xmm11, [rcx + 160]
		MOVAPS xmm12, [rcx + 192]
		MOVAPS xmm13, [rcx + 224]
		MOVAPS xmm14, [rcx + 256]
		MULPS xmm11, [r11 + 80 + 144]
		MULPS xmm12, [r11 + 96 + 144]
		MULPS xmm13, [r11 + 112 + 144]
		MULPS xmm14, [r11 + 128 + 144]
		ADDPS xmm11, xmm12
		ADDPS xmm13, xmm14
		ADDPS xmm11, xmm13

		HADDPS xmm9, xmm11
		ADDPS xmm9, xmm10

		HADDPS xmm8, xmm9


LEA r11, [r11 - 2304]

		MOVAPS xmm14, xmm15		
		SHUFPS xmm14, xmm14, 0FFh

; offload		

		MOVAPS xmm9, xmm0
		SHUFPS xmm9, xmm1, 88h ; 0 1 2 3
		MOVAPS xmm10, xmm2
		SHUFPS xmm10, xmm3, 88h ; 4 5 6 7
					
		
		MOVAPS xmm11, xmm4
		SHUFPS xmm11, xmm1, 0
		SHUFPS xmm11, xmm3, 20h
		XORPS xmm11, xmm15 ; 8 -8 -7 -6
		MOVAPS xmm12, xmm2
		SHUFPS xmm12, xmm1, 22h
		XORPS xmm12, xmm14 ; -5 -4 -3 -2

		MOVAPS xmm13, xmm0
		XORPS xmm13, xmm14
		SHUFPS xmm13, xmm5, 82h ; -1 0 9 10

		MOVAPS [rcx + 9216], xmm9
		MOVAPS [rcx + 9216 + 16], xmm10
		MOVAPS [rcx + 9216 + 32], xmm11
		MOVAPS [rcx + 9216 + 48], xmm12
		MOVAPS [rcx + 9216 + 64], xmm13


		MOVAPS xmm9, xmm6
		SHUFPS xmm9, xmm7, 88h ; 11 12 13 14
		MOVAPS xmm10, xmm8
		SHUFPS xmm10, xmm4, 0A8h


		MOVAPS xmm11, xmm8
		SHUFPS xmm11, xmm7, 22h
		MOVAPS xmm12, xmm6
		SHUFPS xmm12, xmm5, 22h

		MOVAPS [rcx + 9216 + 80], xmm9
		MOVAPS [rcx + 9216 + 96], xmm10
		MOVAPS [rcx + 9216 + 112], xmm11
		MOVAPS [rcx + 9216 + 128], xmm12

	

		MOVAPS xmm9, xmm0
		SHUFPS xmm9, xmm1, 0DDh ; 0 1 2 3
		MOVAPS xmm10, xmm2
		SHUFPS xmm10, xmm3, 0DDh ; 4 5 6 7
					
		
		MOVAPS xmm11, xmm4
		SHUFPS xmm11, xmm1, 55h
		SHUFPS xmm11, xmm3, 70h
		XORPS xmm11, xmm15 ; 8 -8 -7 -6
		MOVAPS xmm12, xmm2
		SHUFPS xmm12, xmm1, 77h
		XORPS xmm12, xmm14 ; -5 -4 -3 -2

		MOVAPS xmm13, xmm0
		XORPS xmm13, xmm14
		SHUFPS xmm13, xmm5, 0D7h ; -1 0 9 10

		MOVAPS [rcx + 9216 + 144], xmm9
		MOVAPS [rcx + 9216 + 144 + 16], xmm10
		MOVAPS [rcx + 9216 + 144 + 32], xmm11
		MOVAPS [rcx + 9216 + 144 + 48], xmm12
		MOVAPS [rcx + 9216 + 144 + 64], xmm13


		MOVAPS xmm9, xmm6
		SHUFPS xmm9, xmm7, 0DDh ; 11 12 13 14
		MOVAPS xmm10, xmm8
		SHUFPS xmm10, xmm4, 0FDh

		MOVAPS xmm11, xmm8
		SHUFPS xmm11, xmm7, 77h
		MOVAPS xmm12, xmm6
		SHUFPS xmm12, xmm5, 77h

		MOVAPS [rcx + 9216 + 144 + 80], xmm9
		MOVAPS [rcx + 9216 + 144 + 96], xmm10
		MOVAPS [rcx + 9216 + 144 + 112], xmm11
		MOVAPS [rcx + 9216 + 144 + 128], xmm12

	

		ADD rcx, 288
	DEC eax
	JNZ idct_loop


	RET
?IMDCT_36@MP3@Audio@@CAXPEAMH@Z	ENDP

ALIGN 16
?FormatLongWindow@MP3@Audio@@CAXPEAMPEBMH_K@Z	PROC
	LEA rcx, [rcx + 9216]
	LEA r11, [r9*4]
	MOV r9, rcx

	MOVAPS xmm0, [rdx]
	MOVAPS xmm1, [rdx + 16]
	MOVAPS xmm2, [rdx + 32]
	MOVAPS xmm3, [rdx + 48]
	MOVAPS xmm4, [rdx + 64]
	MOVAPS xmm5, [rdx + 80]
	MOVAPS xmm6, [rdx + 96]
	MOVAPS xmm7, [rdx + 112]
	MOVAPS xmm8, [rdx + 128]
		

	MOV eax, r8d
	ADD eax, 17
	XOR edx, edx
	MOV r10d, 18
	DIV r10d

	MOV r8d, 32
	SUB r8d, eax

	LEA r10, [rcx - 9216]

align 16
	window_loop:
		XORPS xmm15, xmm15

		MOVUPS xmm11, [r9 + r11]
		MOVUPS xmm12, [r9 + r11 + 16]
		MOVUPS xmm13, [r9 + r11 + 32]
		MOVUPS xmm14, [r9 + r11 + 48]
		MOVLPS xmm15, QWORD PTR [r9 + r11 + 64]


		MOVAPS xmm9, [rcx]
		MULPS xmm9, xmm0
		ADDPS xmm9, xmm11
		MOVUPS [r9], xmm9

		MOVAPS xmm9, [rcx + 16]
		MULPS xmm9, xmm1
		ADDPS xmm9, xmm12
		MOVUPS [r9 + 16], xmm9

		MOVAPS xmm9, [rcx + 32]
		MULPS xmm9, xmm2
		ADDPS xmm9, xmm13
		MOVUPS [r9 + 32], xmm9

		MOVAPS xmm9, [rcx + 48]
		MULPS xmm9, xmm3
		ADDPS xmm9, xmm14
		MOVUPS [r9 + 48], xmm9

			
		MOVAPS xmm11, [rcx + 64]
		MULPS xmm11, xmm4
		ADDPS xmm11, xmm15
		MOVLPS QWORD PTR [r9 + 64], xmm11

	

		MOVAPS xmm12, [rcx + 80]
		MULPS xmm12, xmm5
		MOVAPS xmm13, [rcx + 96]
		MULPS xmm13, xmm6
		MOVAPS xmm14, [rcx + 112]
		MULPS xmm14, xmm7
		MOVAPS xmm15, [rcx + 128]
		MULPS xmm15, xmm8
		

		SHUFPS xmm11, xmm12, 4Eh
		SHUFPS xmm12, xmm13, 4Eh
		SHUFPS xmm13, xmm14, 4Eh
		SHUFPS xmm14, xmm15, 4Eh
		

		MOVUPS [r9 - 9216], xmm11
		MOVUPS [r9 - 9216 + 16], xmm12
		MOVUPS [r9 - 9216 + 32], xmm13
		MOVUPS [r9 - 9216 + 48], xmm14
		MOVHPS QWORD PTR [r9 - 9216 + 64], xmm15


	ADD rcx, 144
	ADD r9, 72

	DEC eax
	JNZ window_loop


	MOV eax, 9
	AND r8d, r8d
	JZ copy_loop

		XORPS xmm0, xmm0

	align 16
		zero_loop:
			MOVUPS xmm11, [r9 + r11]
			MOVUPS xmm12, [r9 + r11 + 16]
			MOVUPS xmm13, [r9 + r11 + 32]
			MOVUPS xmm14, [r9 + r11 + 48]
			MOVLPS xmm15, QWORD PTR [r9 + r11 + 64]


			MOVUPS [r9 - 9216], xmm0
			MOVUPS [r9 - 9216 + 16], xmm0
			MOVUPS [r9 - 9216 + 32], xmm0
			MOVUPS [r9 - 9216 + 48], xmm0
			MOVLPS QWORD PTR [r9 - 9216 + 64], xmm0

			MOVUPS [r9], xmm11
			MOVUPS [r9 + 16], xmm12
			MOVUPS [r9 + 32], xmm13
			MOVUPS [r9 + 48], xmm14
			MOVLPS QWORD PTR [r9 + 64], xmm15

			ADD r9, 72
		DEC r8d
		JNZ zero_loop
		
	
align 16
	copy_loop:
		MOVAPS xmm0, [r10]
		MOVAPS xmm1, [r10 + 16]
		MOVAPS xmm2, [r10 + 32]
		MOVAPS xmm3, [r10 + 48]
		MOVAPS xmm4, [r10 + 64]
		MOVAPS xmm5, [r10 + 80]
		MOVAPS xmm6, [r10 + 96]
		MOVAPS xmm7, [r10 + 112]
		MOVAPS xmm8, [r10 + 128]
		MOVAPS xmm9, [r10 + 144]
		MOVAPS xmm10, [r10 + 160]
		MOVAPS xmm11, [r10 + 176]
		MOVAPS xmm12, [r10 + 192]
		MOVAPS xmm13, [r10 + 208]
		MOVAPS xmm14, [r10 + 224]
		MOVAPS xmm15, [r10 + 240]
		
		MOVAPS [r10 + 9216 + 2304], xmm0
		MOVAPS [r10 + 9216 + 2304 + 16], xmm1
		MOVAPS [r10 + 9216 + 2304 + 32], xmm2
		MOVAPS [r10 + 9216 + 2304 + 48], xmm3
		MOVAPS [r10 + 9216 + 2304 + 64], xmm4
		MOVAPS [r10 + 9216 + 2304 + 80], xmm5
		MOVAPS [r10 + 9216 + 2304 + 96], xmm6
		MOVAPS [r10 + 9216 + 2304 + 112], xmm7
		MOVAPS [r10 + 9216 + 2304 + 128], xmm8
		MOVAPS [r10 + 9216 + 2304 + 144], xmm9
		MOVAPS [r10 + 9216 + 2304 + 160], xmm10
		MOVAPS [r10 + 9216 + 2304 + 176], xmm11
		MOVAPS [r10 + 9216 + 2304 + 192], xmm12
		MOVAPS [r10 + 9216 + 2304 + 208], xmm13
		MOVAPS [r10 + 9216 + 2304 + 224], xmm14
		MOVAPS [r10 + 9216 + 2304 + 240], xmm15

		ADD r10, 256
	DEC eax
	JNZ copy_loop



	RET
?FormatLongWindow@MP3@Audio@@CAXPEAMPEBMH_K@Z	ENDP

ALIGN 16
?FormatShortWindow@MP3@Audio@@CAXPEAMPEBMH_K@Z	PROC
	LEA rcx, [rcx + 9216]
	LEA r11, [r9*4]
	MOV r9, rcx

	MOVAPS xmm0, [rdx]
	MOVAPS xmm1, [rdx + 16]
	MOVAPS xmm2, [rdx + 32]
	
	XORPS xmm14, xmm14
		
	MOV eax, r8d
	ADD eax, 17
	XOR edx, edx
	MOV r10d, 18
	DIV r10d

	MOV r8d, 32
	SUB r8d, eax
	LEA r10, [rcx - 9216]

align 16
	window_loop:
		MOVUPS xmm11, [r9 + r11]
		MOVUPS xmm12, [r9 + r11 + 16]
		MOVUPS xmm13, [r9 + r11 + 32]

		MOVAPS xmm3, [rcx]
		MOVUPS [r9], xmm11
		
		MULPS xmm3, xmm0

		XORPS xmm11, xmm11
		MOVLHPS xmm11, xmm3
		ADDPS xmm12, xmm11
		MOVAPS xmm11, xmm13

		MOVAPS xmm4, [rcx + 16]
		MULPS xmm4, xmm1
		MOVAPS xmm5, [rcx + 32]
		MULPS xmm5, xmm2

		MOVAPS xmm6, [rcx + 48]
		MULPS xmm6, xmm0
		MOVAPS xmm7, [rcx + 64]
		MULPS xmm7, xmm1
		MOVAPS xmm8, [rcx + 80]
		MULPS xmm8, xmm2

		MOVAPS xmm9, [rcx + 96]
		MULPS xmm9, xmm0
		MOVAPS xmm10, [rcx + 112]
		MULPS xmm10, xmm1
		MOVAPS xmm13, [rcx + 128]
		MULPS xmm13, xmm2

	ADD rcx, 144
		
		MOVUPS [r9 + 16], xmm12

		SHUFPS xmm3, xmm4, 4Eh
		ADDPS xmm3, xmm11
		MOVUPS [r9 + 32], xmm3

		SHUFPS xmm4, xmm5, 4Eh
		ADDPS xmm4, xmm6		
		MOVUPS [r9 + 48], xmm4

		SHUFPS xmm5, xmm9, 4Eh
		ADDPS xmm5, xmm7
		MOVLPS QWORD PTR [r9 + 64], xmm5

		SHUFPS xmm9, xmm10, 4Eh
		ADDPS xmm9, xmm8

		MOVHLPS xmm11, xmm5
		MOVLHPS xmm11, xmm9

		MOVHLPS xmm12, xmm9
		SHUFPS xmm12, xmm10, 0E4h
				
		MOVUPS [r9 - 9216], xmm11
		MOVUPS [r9 - 9216 + 16], xmm12
		MOVUPS [r9 - 9216 + 32], xmm13
		MOVUPS [r9 - 9216 + 48], xmm14
		MOVLPS QWORD PTR [r9 - 9216 + 64], xmm14

	ADD r9, 72

	DEC eax
	JNZ window_loop

	MOV eax, 9

	AND r8d, r8d
	JZ copy_loop


	align 16
		zero_loop:
			MOVUPS xmm11, [r9 + r11]
			MOVUPS xmm12, [r9 + r11 + 16]
			MOVUPS xmm13, [r9 + r11 + 32]

			MOVUPS [r9 - 9216], xmm14
			MOVUPS [r9 - 9216 + 16], xmm14
			MOVUPS [r9 - 9216 + 32], xmm14
			MOVUPS [r9 - 9216 + 48], xmm14
			MOVLPS QWORD PTR [r9 - 9216 + 64], xmm14

			MOVUPS [r9], xmm11
			MOVUPS [r9 + 16], xmm12
			MOVUPS [r9 + 32], xmm13
			MOVUPS [r9 + 48], xmm14
			MOVLPS QWORD PTR [r9 + 64], xmm14
			
			ADD r9, 72
		DEC r8d
		JNZ zero_loop
		

	
align 16
	copy_loop:
		MOVAPS xmm0, [r10]
		MOVAPS xmm1, [r10 + 16]
		MOVAPS xmm2, [r10 + 32]
		MOVAPS xmm3, [r10 + 48]
		MOVAPS xmm4, [r10 + 64]
		MOVAPS xmm5, [r10 + 80]
		MOVAPS xmm6, [r10 + 96]
		MOVAPS xmm7, [r10 + 112]
		MOVAPS xmm8, [r10 + 128]
		MOVAPS xmm9, [r10 + 144]
		MOVAPS xmm10, [r10 + 160]
		MOVAPS xmm11, [r10 + 176]
		MOVAPS xmm12, [r10 + 192]
		MOVAPS xmm13, [r10 + 208]
		MOVAPS xmm14, [r10 + 224]
		MOVAPS xmm15, [r10 + 240]
		
		MOVAPS [r10 + 9216 + 2304], xmm0
		MOVAPS [r10 + 9216 + 2304 + 16], xmm1
		MOVAPS [r10 + 9216 + 2304 + 32], xmm2
		MOVAPS [r10 + 9216 + 2304 + 48], xmm3
		MOVAPS [r10 + 9216 + 2304 + 64], xmm4
		MOVAPS [r10 + 9216 + 2304 + 80], xmm5
		MOVAPS [r10 + 9216 + 2304 + 96], xmm6
		MOVAPS [r10 + 9216 + 2304 + 112], xmm7
		MOVAPS [r10 + 9216 + 2304 + 128], xmm8
		MOVAPS [r10 + 9216 + 2304 + 144], xmm9
		MOVAPS [r10 + 9216 + 2304 + 160], xmm10
		MOVAPS [r10 + 9216 + 2304 + 176], xmm11
		MOVAPS [r10 + 9216 + 2304 + 192], xmm12
		MOVAPS [r10 + 9216 + 2304 + 208], xmm13
		MOVAPS [r10 + 9216 + 2304 + 224], xmm14
		MOVAPS [r10 + 9216 + 2304 + 240], xmm15

		ADD r10, 256
	DEC eax
	JNZ copy_loop
	

	RET
?FormatShortWindow@MP3@Audio@@CAXPEAMPEBMH_K@Z	ENDP


ALIGN 16
?Synthesize@MP3@Audio@@CAXPEAM_K@Z	PROC
	MOV r11, rcx
	LEA r8, [rcx + 9216]


	XORPS xmm15, xmm15

	LEA r9, [?synth_cos@MP3@Audio@@0PAMA]
	

	MOV al, 18
align 16
	v_loop_0:
		; deinterleave, 8 registers
		MOVSS xmm0, DWORD PTR [r8]
		MOVSS xmm1, DWORD PTR [r8 + 18*4]
		MOVSS xmm2, DWORD PTR [r8 + 36*4]
		MOVSS xmm3, DWORD PTR [r8 + 54*4]
		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2
		XORPS xmm0, xmm15

		MOVSS xmm1, DWORD PTR [r8 + 72*4]
		MOVSS xmm2, DWORD PTR [r8 + 90*4]
		MOVSS xmm3, DWORD PTR [r8 + 108*4]
		MOVSS xmm4, DWORD PTR [r8 + 126*4]
		UNPCKLPS xmm1, xmm2
		UNPCKLPS xmm3, xmm4
		MOVLHPS xmm1, xmm3
		XORPS xmm1, xmm15

		MOVSS xmm2, DWORD PTR [r8 + 144*4]
		MOVSS xmm3, DWORD PTR [r8 + 162*4]
		MOVSS xmm4, DWORD PTR [r8 + 180*4]
		MOVSS xmm5, DWORD PTR [r8 + 198*4]
		UNPCKLPS xmm2, xmm3
		UNPCKLPS xmm4, xmm5
		MOVLHPS xmm2, xmm4
		XORPS xmm2, xmm15

		MOVSS xmm3, DWORD PTR [r8 + 216*4]
		MOVSS xmm4, DWORD PTR [r8 + 234*4]
		MOVSS xmm5, DWORD PTR [r8 + 252*4]
		MOVSS xmm6, DWORD PTR [r8 + 270*4]
		UNPCKLPS xmm3, xmm4
		UNPCKLPS xmm5, xmm6
		MOVLHPS xmm3, xmm5
		XORPS xmm3, xmm15

		MOVSS xmm4, DWORD PTR [r8 + 288*4]
		MOVSS xmm5, DWORD PTR [r8 + 306*4]
		MOVSS xmm6, DWORD PTR [r8 + 324*4]
		MOVSS xmm7, DWORD PTR [r8 + 342*4]
		UNPCKLPS xmm4, xmm5
		UNPCKLPS xmm6, xmm7
		MOVLHPS xmm4, xmm6
		XORPS xmm4, xmm15

		MOVSS xmm5, DWORD PTR [r8 + 360*4]
		MOVSS xmm6, DWORD PTR [r8 + 378*4]
		MOVSS xmm7, DWORD PTR [r8 + 396*4]
		MOVSS xmm8, DWORD PTR [r8 + 414*4]
		UNPCKLPS xmm5, xmm6
		UNPCKLPS xmm7, xmm8
		MOVLHPS xmm5, xmm7
		XORPS xmm5, xmm15

		MOVSS xmm6, DWORD PTR [r8 + 432*4]
		MOVSS xmm7, DWORD PTR [r8 + 450*4]
		MOVSS xmm8, DWORD PTR [r8 + 468*4]
		MOVSS xmm9, DWORD PTR [r8 + 486*4]
		UNPCKLPS xmm6, xmm7
		UNPCKLPS xmm8, xmm9
		MOVLHPS xmm6, xmm8
		XORPS xmm6, xmm15

		MOVSS xmm7, DWORD PTR [r8 + 504*4]
		MOVSS xmm8, DWORD PTR [r8 + 522*4]
		MOVSS xmm9, DWORD PTR [r8 + 540*4]
		MOVSS xmm10, DWORD PTR [r8 + 558*4]
		UNPCKLPS xmm7, xmm8
		UNPCKLPS xmm9, xmm10
		MOVLHPS xmm7, xmm9
		XORPS xmm7, xmm15


		MOV r10, r9

		MOV ah, 16
	align 16
		vdct_loop_0:

			MOVAPS xmm8, [r10]
			MOVAPS xmm9, [r10 + 16]
			MOVAPS xmm10, [r10 + 32]
			MOVAPS xmm11, [r10 + 48]

			MULPS xmm8, xmm0
			MULPS xmm9, xmm1
			MULPS xmm10, xmm2
			MULPS xmm11, xmm3

			ADDPS xmm8, xmm9
			ADDPS xmm10, xmm11
			ADDPS xmm8, xmm10

			MOVAPS xmm9, [r10 + 64]
			MOVAPS xmm10, [r10 + 80]
			MOVAPS xmm11, [r10 + 96]
			MOVAPS xmm12, [r10 + 112]

			MULPS xmm9, xmm4
			MULPS xmm10, xmm5
			MULPS xmm11, xmm6
			MULPS xmm12, xmm7

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			ADDPS xmm8, xmm9

		ADD r10, 128

			MOVAPS xmm9, [r10]
			MOVAPS xmm10, [r10 + 16]
			MOVAPS xmm11, [r10 + 32]
			MOVAPS xmm12, [r10 + 48]

			MULPS xmm9, xmm0
			MULPS xmm10, xmm1
			MULPS xmm11, xmm2
			MULPS xmm12, xmm3

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			MOVAPS xmm10, [r10 + 64]
			MOVAPS xmm11, [r10 + 80]
			MOVAPS xmm12, [r10 + 96]
			MOVAPS xmm13, [r10 + 112]

			MULPS xmm10, xmm4
			MULPS xmm11, xmm5
			MULPS xmm12, xmm6
			MULPS xmm13, xmm7

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			ADDPS xmm9, xmm10

			HADDPS xmm8, xmm9

		ADD r10, 128

			MOVAPS xmm9, [r10]
			MOVAPS xmm10, [r10 + 16]
			MOVAPS xmm11, [r10 + 32]
			MOVAPS xmm12, [r10 + 48]

			MULPS xmm9, xmm0
			MULPS xmm10, xmm1
			MULPS xmm11, xmm2
			MULPS xmm12, xmm3

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			MOVAPS xmm10, [r10 + 64]
			MOVAPS xmm11, [r10 + 80]
			MOVAPS xmm12, [r10 + 96]
			MOVAPS xmm13, [r10 + 112]

			MULPS xmm10, xmm4
			MULPS xmm11, xmm5
			MULPS xmm12, xmm6
			MULPS xmm13, xmm7

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			ADDPS xmm9, xmm10

		ADD r10, 128

			MOVAPS xmm10, [r10]
			MOVAPS xmm11, [r10 + 16]
			MOVAPS xmm12, [r10 + 32]
			MOVAPS xmm13, [r10 + 48]

			MULPS xmm10, xmm0
			MULPS xmm11, xmm1
			MULPS xmm12, xmm2
			MULPS xmm13, xmm3

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			MOVAPS xmm11, [r10 + 64]
			MOVAPS xmm12, [r10 + 80]
			MOVAPS xmm13, [r10 + 96]
			MOVAPS xmm14, [r10 + 112]

			MULPS xmm11, xmm4
			MULPS xmm12, xmm5
			MULPS xmm13, xmm6
			MULPS xmm14, xmm7

			ADDPS xmm11, xmm12
			ADDPS xmm13, xmm14
			ADDPS xmm11, xmm13

			ADDPS xmm10, xmm11

			HADDPS xmm9, xmm10

		ADD r10, 128


			HADDPS xmm8, xmm9



			MOVNTPS [r11], xmm8

			ADD r11, 16

		DEC ah
		JNZ vdct_loop_0


		XORPS xmm15, XMMWORD PTR [phase_flip]


		ADD r8, 4
		
	DEC al
	JNZ v_loop_0



	LEA r8, [rcx + 9216 + 4608]

	MOV al, 18
align 16
	v_loop_1:
		; deinterleave, 8 registers
		MOVSS xmm0, DWORD PTR [r8]
		MOVSS xmm1, DWORD PTR [r8 + 18*4]
		MOVSS xmm2, DWORD PTR [r8 + 36*4]
		MOVSS xmm3, DWORD PTR [r8 + 54*4]
		UNPCKLPS xmm0, xmm1
		UNPCKLPS xmm2, xmm3
		MOVLHPS xmm0, xmm2
		XORPS xmm0, xmm15

		MOVSS xmm1, DWORD PTR [r8 + 72*4]
		MOVSS xmm2, DWORD PTR [r8 + 90*4]
		MOVSS xmm3, DWORD PTR [r8 + 108*4]
		MOVSS xmm4, DWORD PTR [r8 + 126*4]
		UNPCKLPS xmm1, xmm2
		UNPCKLPS xmm3, xmm4
		MOVLHPS xmm1, xmm3
		XORPS xmm1, xmm15

		MOVSS xmm2, DWORD PTR [r8 + 144*4]
		MOVSS xmm3, DWORD PTR [r8 + 162*4]
		MOVSS xmm4, DWORD PTR [r8 + 180*4]
		MOVSS xmm5, DWORD PTR [r8 + 198*4]
		UNPCKLPS xmm2, xmm3
		UNPCKLPS xmm4, xmm5
		MOVLHPS xmm2, xmm4
		XORPS xmm2, xmm15

		MOVSS xmm3, DWORD PTR [r8 + 216*4]
		MOVSS xmm4, DWORD PTR [r8 + 234*4]
		MOVSS xmm5, DWORD PTR [r8 + 252*4]
		MOVSS xmm6, DWORD PTR [r8 + 270*4]
		UNPCKLPS xmm3, xmm4
		UNPCKLPS xmm5, xmm6
		MOVLHPS xmm3, xmm5
		XORPS xmm3, xmm15

		MOVSS xmm4, DWORD PTR [r8 + 288*4]
		MOVSS xmm5, DWORD PTR [r8 + 306*4]
		MOVSS xmm6, DWORD PTR [r8 + 324*4]
		MOVSS xmm7, DWORD PTR [r8 + 342*4]
		UNPCKLPS xmm4, xmm5
		UNPCKLPS xmm6, xmm7
		MOVLHPS xmm4, xmm6
		XORPS xmm4, xmm15

		MOVSS xmm5, DWORD PTR [r8 + 360*4]
		MOVSS xmm6, DWORD PTR [r8 + 378*4]
		MOVSS xmm7, DWORD PTR [r8 + 396*4]
		MOVSS xmm8, DWORD PTR [r8 + 414*4]
		UNPCKLPS xmm5, xmm6
		UNPCKLPS xmm7, xmm8
		MOVLHPS xmm5, xmm7
		XORPS xmm5, xmm15

		MOVSS xmm6, DWORD PTR [r8 + 432*4]
		MOVSS xmm7, DWORD PTR [r8 + 450*4]
		MOVSS xmm8, DWORD PTR [r8 + 468*4]
		MOVSS xmm9, DWORD PTR [r8 + 486*4]
		UNPCKLPS xmm6, xmm7
		UNPCKLPS xmm8, xmm9
		MOVLHPS xmm6, xmm8
		XORPS xmm6, xmm15

		MOVSS xmm7, DWORD PTR [r8 + 504*4]
		MOVSS xmm8, DWORD PTR [r8 + 522*4]
		MOVSS xmm9, DWORD PTR [r8 + 540*4]
		MOVSS xmm10, DWORD PTR [r8 + 558*4]
		UNPCKLPS xmm7, xmm8
		UNPCKLPS xmm9, xmm10
		MOVLHPS xmm7, xmm9
		XORPS xmm7, xmm15


		MOV r10, r9

		MOV ah, 16
	align 16
		vdct_loop_1:

			MOVAPS xmm8, [r10]
			MOVAPS xmm9, [r10 + 16]
			MOVAPS xmm10, [r10 + 32]
			MOVAPS xmm11, [r10 + 48]

			MULPS xmm8, xmm0
			MULPS xmm9, xmm1
			MULPS xmm10, xmm2
			MULPS xmm11, xmm3

			ADDPS xmm8, xmm9
			ADDPS xmm10, xmm11
			ADDPS xmm8, xmm10

			MOVAPS xmm9, [r10 + 64]
			MOVAPS xmm10, [r10 + 80]
			MOVAPS xmm11, [r10 + 96]
			MOVAPS xmm12, [r10 + 112]

			MULPS xmm9, xmm4
			MULPS xmm10, xmm5
			MULPS xmm11, xmm6
			MULPS xmm12, xmm7

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			ADDPS xmm8, xmm9

		ADD r10, 128

			MOVAPS xmm9, [r10]
			MOVAPS xmm10, [r10 + 16]
			MOVAPS xmm11, [r10 + 32]
			MOVAPS xmm12, [r10 + 48]

			MULPS xmm9, xmm0
			MULPS xmm10, xmm1
			MULPS xmm11, xmm2
			MULPS xmm12, xmm3

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			MOVAPS xmm10, [r10 + 64]
			MOVAPS xmm11, [r10 + 80]
			MOVAPS xmm12, [r10 + 96]
			MOVAPS xmm13, [r10 + 112]

			MULPS xmm10, xmm4
			MULPS xmm11, xmm5
			MULPS xmm12, xmm6
			MULPS xmm13, xmm7

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			ADDPS xmm9, xmm10

			HADDPS xmm8, xmm9

		ADD r10, 128

			MOVAPS xmm9, [r10]
			MOVAPS xmm10, [r10 + 16]
			MOVAPS xmm11, [r10 + 32]
			MOVAPS xmm12, [r10 + 48]

			MULPS xmm9, xmm0
			MULPS xmm10, xmm1
			MULPS xmm11, xmm2
			MULPS xmm12, xmm3

			ADDPS xmm9, xmm10
			ADDPS xmm11, xmm12
			ADDPS xmm9, xmm11

			MOVAPS xmm10, [r10 + 64]
			MOVAPS xmm11, [r10 + 80]
			MOVAPS xmm12, [r10 + 96]
			MOVAPS xmm13, [r10 + 112]

			MULPS xmm10, xmm4
			MULPS xmm11, xmm5
			MULPS xmm12, xmm6
			MULPS xmm13, xmm7

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			ADDPS xmm9, xmm10

		ADD r10, 128

			MOVAPS xmm10, [r10]
			MOVAPS xmm11, [r10 + 16]
			MOVAPS xmm12, [r10 + 32]
			MOVAPS xmm13, [r10 + 48]

			MULPS xmm10, xmm0
			MULPS xmm11, xmm1
			MULPS xmm12, xmm2
			MULPS xmm13, xmm3

			ADDPS xmm10, xmm11
			ADDPS xmm12, xmm13
			ADDPS xmm10, xmm12

			MOVAPS xmm11, [r10 + 64]
			MOVAPS xmm12, [r10 + 80]
			MOVAPS xmm13, [r10 + 96]
			MOVAPS xmm14, [r10 + 112]

			MULPS xmm11, xmm4
			MULPS xmm12, xmm5
			MULPS xmm13, xmm6
			MULPS xmm14, xmm7

			ADDPS xmm11, xmm12
			ADDPS xmm13, xmm14
			ADDPS xmm11, xmm13

			ADDPS xmm10, xmm11

			HADDPS xmm9, xmm10

		ADD r10, 128


			HADDPS xmm8, xmm9



			MOVNTPS [r11], xmm8

			ADD r11, 16

		DEC ah
		JNZ vdct_loop_1


		XORPS xmm15, XMMWORD PTR [phase_flip]


		ADD r8, 4
		
	DEC al
	JNZ v_loop_1
		


; first 16 require previous frame
; last 20 - current frame only


	LEA r9, [?synth_window@MP3@Audio@@0QBIB]
	LEA r11, [rcx + 9216] ; output pointer
	LEA rdx, [r11 + rdx*4] ; previous frame pointer


	MOV eax, 1
	
align 16
	swin_loop_0:

		XORPS xmm0, xmm0
		XORPS xmm1, xmm1
		XORPS xmm2, xmm2
		XORPS xmm3, xmm3
		XORPS xmm4, xmm4
		XORPS xmm5, xmm5
		XORPS xmm6, xmm6
		XORPS xmm7, xmm7
		
		MOV ah, al		
		MOV r10, rcx
		MOV r8d, 8

	align 16
		ssamp_loop_0:	

			MOVAPS xmm8, [r10]
			MOVAPS xmm9, [r10 + 16]
			MOVAPS xmm10, [r10 + 32]
			MOVAPS xmm11, [r10 + 48]
			MOVAPS xmm12, [r10 + 64]
			MOVAPS xmm13, [r10 + 80]
			MOVAPS xmm14, [r10 + 96]
			MOVAPS xmm15, [r10 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			


			DEC ah
			CMOVZ r10, rdx
			LEA r10, [r10 - 64*4]


			MOVAPS xmm8, [r10 + 128]
			MOVAPS xmm9, [r10 + 128 + 16]
			MOVAPS xmm10, [r10 + 128 + 32]
			MOVAPS xmm11, [r10 + 128 + 48]
			MOVAPS xmm12, [r10 + 128 + 64]
			MOVAPS xmm13, [r10 + 128 + 80]
			MOVAPS xmm14, [r10 + 128 + 96]
			MOVAPS xmm15, [r10 + 128 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			


			DEC ah
			CMOVZ r10, rdx
			LEA r10, [r10 - 64*4]

		DEC r8d
		JNZ ssamp_loop_0

		MOVNTPS [r11], xmm0
		MOVNTPS [r11 + 16], xmm1
		MOVNTPS [r11 + 32], xmm2
		MOVNTPS [r11 + 48], xmm3
		MOVNTPS [r11 + 64], xmm4
		MOVNTPS [r11 + 80], xmm5
		MOVNTPS [r11 + 96], xmm6
		MOVNTPS [r11 + 112], xmm7


		LEA r9, [r9 - 2048]
		LEA r11, [r11 + 128]
		LEA rcx, [rcx + 64*4]
	INC al
	CMP al, 18
	JBE swin_loop_0



	LEA r11, [rcx + 9216]
	DEC al
	

align 16
	swin_loop_1:

		XORPS xmm0, xmm0
		XORPS xmm1, xmm1
		XORPS xmm2, xmm2
		XORPS xmm3, xmm3
		XORPS xmm4, xmm4
		XORPS xmm5, xmm5
		XORPS xmm6, xmm6
		XORPS xmm7, xmm7
		
		MOV r8d, 8
		MOV r10, rcx

	align 16
		ssamp_loop_1:	

			MOVAPS xmm8, [r10]
			MOVAPS xmm9, [r10 + 16]
			MOVAPS xmm10, [r10 + 32]
			MOVAPS xmm11, [r10 + 48]
			MOVAPS xmm12, [r10 + 64]
			MOVAPS xmm13, [r10 + 80]
			MOVAPS xmm14, [r10 + 96]
			MOVAPS xmm15, [r10 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			
			LEA r10, [r10 - 64*4]


			MOVAPS xmm8, [r10 + 128]
			MOVAPS xmm9, [r10 + 128 + 16]
			MOVAPS xmm10, [r10 + 128 + 32]
			MOVAPS xmm11, [r10 + 128 + 48]
			MOVAPS xmm12, [r10 + 128 + 64]
			MOVAPS xmm13, [r10 + 128 + 80]
			MOVAPS xmm14, [r10 + 128 + 96]
			MOVAPS xmm15, [r10 + 128 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			

			LEA r10, [r10 - 64*4]

		DEC r8d
		JNZ ssamp_loop_1

		MOVNTPS [r11], xmm0
		MOVNTPS [r11 + 16], xmm1
		MOVNTPS [r11 + 32], xmm2
		MOVNTPS [r11 + 48], xmm3
		MOVNTPS [r11 + 64], xmm4
		MOVNTPS [r11 + 80], xmm5
		MOVNTPS [r11 + 96], xmm6
		MOVNTPS [r11 + 112], xmm7


		LEA r9, [r9 - 2048]
		LEA r11, [r11 + 128]
		LEA rcx, [rcx + 64*4]

	DEC al
	JNZ swin_loop_1




	RET
?Synthesize@MP3@Audio@@CAXPEAM_K@Z	ENDP



ALIGN 16
?Synthesize@MP3@Audio@@CAXPEAM_KI1@Z	PROC
	PUSH rbx
	PUSH rsi
	PUSH rdi

	MOV ebx, r8d
	MOV bh, bl	
	

	LEA r11, [rcx + 9216] ; output pointer
	
	LEA rsi, [rcx + rdx*4] ; previous frame pointer
	SHL r8, 8
	LEA rsi, [rsi + r8]
	
	MOV rdi, rsi

	MOV rdx, r9
	LEA r9, [?synth_window@MP3@Audio@@0QBIB]

	MOV eax, 1
	
align 16
	win_loop:

		XORPS xmm0, xmm0
		XORPS xmm1, xmm1
		XORPS xmm2, xmm2
		XORPS xmm3, xmm3
		XORPS xmm4, xmm4
		XORPS xmm5, xmm5
		XORPS xmm6, xmm6
		XORPS xmm7, xmm7
		
		MOV ah, al		
		MOV r10, rcx
		MOV r8d, 8

		MOV rsi, rdi

	align 16
		sam_loop:

			MOVAPS xmm8, [r10]
			MOVAPS xmm9, [r10 + 16]
			MOVAPS xmm10, [r10 + 32]
			MOVAPS xmm11, [r10 + 48]
			MOVAPS xmm12, [r10 + 64]
			MOVAPS xmm13, [r10 + 80]
			MOVAPS xmm14, [r10 + 96]
			MOVAPS xmm15, [r10 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			


			DEC ah
			JNZ skip_adjust_0
				MOV r10, rsi
				LEA rsi, [rsi + rdx*4]
				MOV ah, bh
			skip_adjust_0:
			

			LEA r10, [r10 - 64*4]


			MOVAPS xmm8, [r10 + 128]
			MOVAPS xmm9, [r10 + 128 + 16]
			MOVAPS xmm10, [r10 + 128 + 32]
			MOVAPS xmm11, [r10 + 128 + 48]
			MOVAPS xmm12, [r10 + 128 + 64]
			MOVAPS xmm13, [r10 + 128 + 80]
			MOVAPS xmm14, [r10 + 128 + 96]
			MOVAPS xmm15, [r10 + 128 + 112]


			MULPS xmm8, [r9]
			ADDPS xmm0, xmm8
			MULPS xmm9, [r9 + 16]
			ADDPS xmm1, xmm9
			MULPS xmm10, [r9 + 32]
			ADDPS xmm2, xmm10
			MULPS xmm11, [r9 + 48]
			ADDPS xmm3, xmm11
			MULPS xmm12, [r9 + 64]
			ADDPS xmm4, xmm12
			MULPS xmm13, [r9 + 80]
			ADDPS xmm5, xmm13
			MULPS xmm14, [r9 + 96]
			ADDPS xmm6, xmm14
			MULPS xmm15, [r9 + 112]
			ADDPS xmm7, xmm15

			ADD r9, 128
			


			DEC ah
			JNZ skip_adjust_1
				MOV r10, rsi
				LEA rsi, [rsi + rdx*4]
				MOV ah, bh
			skip_adjust_1:

			LEA r10, [r10 - 64*4]

		DEC r8d
		JNZ sam_loop

		MOVNTPS [r11], xmm0
		MOVNTPS [r11 + 16], xmm1
		MOVNTPS [r11 + 32], xmm2
		MOVNTPS [r11 + 48], xmm3
		MOVNTPS [r11 + 64], xmm4
		MOVNTPS [r11 + 80], xmm5
		MOVNTPS [r11 + 96], xmm6
		MOVNTPS [r11 + 112], xmm7


		LEA r9, [r9 - 2048]
		LEA r11, [r11 + 128]
		LEA rcx, [rcx + 64*4]
	INC al
	CMP al, bl
	JBE win_loop


	POP rdi
	POP rsi
	POP rbx

	RET
?Synthesize@MP3@Audio@@CAXPEAM_KI1@Z	ENDP


END


