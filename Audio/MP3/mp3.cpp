/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\MP3.h"
#include "System\SysUtils.h"

#include "Math\CalcSpecial.h"


#pragma warning(disable:4244)


__declspec(align(16)) float Audio::MP3::cos_table_12[72];
__declspec(align(16)) float Audio::MP3::cos_table_36[648];

__declspec(align(16)) float Audio::MP3::cos_table_32[144];

__declspec(align(16)) float Audio::MP3::window_table[4][36];

__declspec(align(16)) float Audio::MP3::synth_cos[2048];

void Audio::MP3::ChannelConfig::Clear() {	
	System::MemoryFill(this, sizeof(ChannelConfig), 0);
	byte_align = -1;
}

Audio::MP3::MP3() : frame_size(0), xframe_size(0), pfd_2(0), current_packet(0), active_packet(0), packet_offset(0), layer_type(0), protection_id(0), brate_idx(0), sf_idx(0), sampling_frequency(0), padding(0), c_mode(0), xc_mode(0), active_byte(0), active_bit(0), temp_offset2(0) {

	stream_cfg.sdata_offset = DefaultRenderer::frame_cfg_size + MP3_CHANNEL_SIZE/2;
	stream_cfg.frame_size = DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE;

}


Audio::MP3::~MP3() {
	packet_buf.Destroy();
	
}

UI_64 Audio::MP3::Initialize() {
	unsigned int k(0);
	float * c_ptr(0);

	for (int i(0);i<3;i++) {
		for (int j(0);j<6;j++) {
			cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*i + 6.0 + 1.0)*(2.0*j + 1.0));
		}
		for (int j(0);j<6;j++) {
			cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*i + 6.0 + 1.0)*(2.0*j + 1.0));
		}
	}		
	for (int j(0);j<6;j++) {
		cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*8.0 + 6.0 + 1.0)*(2.0*j + 1.0));
	}
	for (int j(0);j<6;j++) {
		cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*8.0 + 6.0 + 1.0)*(2.0*j + 1.0));
	}
	for (int i(6);i<8;i++) {
		for (int j(0);j<6;j++) {
			cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*i + 6.0 + 1.0)*(2.0*j + 1.0));
		}
		for (int j(0);j<6;j++) {
			cos_table_12[k++] = Calc::RealCos(180.0/2.0/12.0*(2.0*i + 6.0 + 1.0)*(2.0*j + 1.0));
		}
	}

	k = 0;

	for (int i(0);i<9;i++) {
		for (int j(0);j<18;j++) {
			cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*i + 18.0 + 1.0)*(2.0*j + 1.0));
		}
		for (int j(0);j<18;j++) {
			cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*i + 18.0 + 1.0)*(2.0*j + 1.0));
		}
	}		
	for (int j(0);j<18;j++) {
		cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*26.0 + 18.0 + 1.0)*(2.0*j + 1.0));
	}
	for (int j(0);j<18;j++) {
		cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*26.0 + 18.0 + 1.0)*(2.0*j + 1.0));
	}
	for (int i(18);i<26;i++) {
		for (int j(0);j<18;j++) {
			cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*i + 18.0 + 1.0)*(2.0*j + 1.0));
		}
		for (int j(0);j<18;j++) {
			cos_table_36[k++] = Calc::RealCos(180.0/2.0/36.0*(2.0*i + 18.0 + 1.0)*(2.0*j + 1.0));
		}
	}

// gen cos 32
	c_ptr = cos_table_32;

	for (unsigned int N(16);N>4;N >>= 1) {
		for (unsigned int i(0);i<(N/8);i++) {
			c_ptr[0] = 2.0*Calc::RealCos(45.0/N*(8.0*i + 0.0 + 1.0));
			c_ptr[1] = 2.0*Calc::RealCos(45.0/N*(8.0*i + 2.0 + 1.0));
			c_ptr[2] = 2.0*Calc::RealCos(45.0/N*(8.0*i + 4.0 + 1.0));
			c_ptr[3] = 2.0*Calc::RealCos(45.0/N*(8.0*i + 6.0 + 1.0));

			c_ptr[4] = 2.0*Calc::RealCos(45.0/N*(2.0*N - 8.0*i - 8.0 + 1.0));
			c_ptr[5] = 2.0*Calc::RealCos(45.0/N*(2.0*N - 8.0*i - 6.0 + 1.0));
			c_ptr[6] = 2.0*Calc::RealCos(45.0/N*(2.0*N - 8.0*i - 4.0 + 1.0));
			c_ptr[7] = 2.0*Calc::RealCos(45.0/N*(2.0*N - 8.0*i - 2.0 + 1.0));
			
			c_ptr += 8;
		}
	}

	c_ptr[0] = 2.0f*Calc::RealCos(45.0/4.0*(2.0*0.0 + 1.0));
	c_ptr[1] = 2.0f*Calc::RealCos(45.0/4.0*(2.0*1.0 + 1.0));
	c_ptr[2] = 2.0f*Calc::RealCos(45.0/4.0*(2.0*2.0 + 1.0));
	c_ptr[3] = 2.0f*Calc::RealCos(45.0/4.0*(2.0*3.0 + 1.0));

	c_ptr[4] = 2.0f*Calc::RealCos(45.0/2.0*(2.0*0.0 + 1.0));
	c_ptr[5] = 1.0f;
	c_ptr[6] = 2.0f*Calc::RealCos(45.0/2.0*(2.0*1.0 + 1.0));
	c_ptr[7] = 1.0f;

	c_ptr[8] = Calc::RealCos(45.0);
	c_ptr[9] = 1.0f;
	c_ptr[10] = Calc::RealCos(45.0);
	c_ptr[11] = 1.0f;


	c_ptr += 12;

	for (unsigned int n_v(4);n_v<=32;n_v<<=1) {
		k = 2;

		reinterpret_cast<unsigned int *>(c_ptr)[0] = 0;
		reinterpret_cast<unsigned int *>(c_ptr)[1] = (n_v>>1);
		
		for (unsigned int o_idx(n_v>>2);o_idx;o_idx>>=1) {
			for (unsigned int i(0);i<k;i++) {
				reinterpret_cast<unsigned int *>(c_ptr)[k + i] = reinterpret_cast<unsigned int *>(c_ptr)[i] + o_idx;
			}
			k <<= 1;
		}

		for (unsigned int i(0);i<n_v;i++) {			
			reinterpret_cast<unsigned int *>(c_ptr)[i] <<= 2;
		}

		c_ptr += n_v;
	}


// synth
	for (unsigned int i(0);i<64;i++) {
		for (unsigned int j(0);j<32;j++) {
			synth_cos[i*32 + j] = Calc::RealCos(180.0/64.0*(16.0 + i)*(2.0*j + 1.0));
		}
	}

// set windows

	for (unsigned int i(0);i<36;i++) {
		window_table[0][i] = Calc::RealSin(180.0/36.0*(0.5 + i));
	}

	for (unsigned int i(0);i<18;i++) {
		window_table[1][i] = Calc::RealSin(180.0/36.0*(0.5 + i));
	}
	for (unsigned int i(18);i<24;i++) {
		window_table[1][i] = 1.0f;
	}
	for (unsigned int i(6);i<12;i++) {
		window_table[1][i + 18] = Calc::RealSin(180.0/12.0*(0.5 + i));
	}
	for (unsigned int i(30);i<36;i++) {
		window_table[1][i] = 0.0f;
	}

	for (unsigned int j(0);j<3;j++) {
		for (unsigned int i(0);i<12;i++) {
			window_table[2][j*12 + i] = Calc::RealSin(180.0/12.0*(0.5 + i));
		}
	}

	for (unsigned int i(0);i<6;i++) {
		window_table[3][i] = 0.0f;
	}
	for (unsigned int i(0);i<6;i++) {
		window_table[3][i + 6] = Calc::RealSin(180.0/12.0*(0.5 + i));
	}
	for (unsigned int i(12);i<18;i++) {
		window_table[3][i] = 1.0f;
	}
	for (unsigned int i(18);i<36;i++) {
		window_table[3][i] = Calc::RealSin(180.0/36.0*(0.5 + i));
	}

	return 0;
}

UI_64 Audio::MP3::Finalize() {

	return 0;
}

UI_64 Audio::MP3::Unpack(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(RESULT_DECODE_COMPLETE);

	for (;(data_length & (FILE_BUFFER_SIZE*2 - 1)) > 0;) {
		result = DecodeUnit(data_ptr, data_length, time_mark);

		if (data_length >= frame_size) {
			data_length -= frame_size;
			data_ptr += frame_size;
		} else {

		}
	}

	return result;
}

UI_64 Audio::MP3::DecodeUnit(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0), x_val(0);

	if ((time_mark == -1) || (time_mark == -3)) {
		temp_offset2 = 0;
		current_packet = 0;
		active_packet = 0;
		
		time_mark++;
	}

	if (stream_cfg.set_complete) {
		if (data_length < (xframe_size<<2)) {
			if (unsigned char * t_ptr = reinterpret_cast<unsigned char *>(packet_buf.Acquire())) {

				System::MemoryCopy(t_ptr + MP3_MAX_PACKET*2*ccfg_size, data_ptr, data_length);
				temp_offset2 = data_length;
				packet_buf.Release();
			}

			result = data_length;
			result <<= 32;
			return result | RESULT_STREAM_BREAK;
		}

		if (temp_offset2) {
			if (unsigned char * t_ptr = reinterpret_cast<unsigned char *>(packet_buf.Acquire())) {

				System::MemoryCopy(t_ptr + MP3_MAX_PACKET*2*ccfg_size + temp_offset2, data_ptr, (xframe_size<<2) - temp_offset2);
				data_length = (xframe_size<<2);
				data_ptr = t_ptr + MP3_MAX_PACKET*2*ccfg_size;
			}
		}
	}

	if (data_ptr)
	if ((data_ptr[0] == 0xFF) && ((data_ptr[1] & 0xF0) == 0xF0)) {		
		decoder_cfg.SetSource(data_ptr+1, (data_length & (FILE_BUFFER_SIZE*2 - 1))- 1);
		
		decoder_cfg.GetNumber(4);

// header ===============================================================
		decoder_cfg.NextBit(); // ID
		layer_type = decoder_cfg.GetNumber(2);
		protection_id = decoder_cfg.NextBit();

		brate_idx = decoder_cfg.GetNumber(4);
		sf_idx = decoder_cfg.GetNumber(2);
		sampling_frequency = sampling_frequencies[sf_idx];

		padding = decoder_cfg.NextBit(); // padding

		decoder_cfg.NextBit(); // private

		c_mode = decoder_cfg.GetNumber(2);
		xc_mode = decoder_cfg.GetNumber(2);

		decoder_cfg.NextBit(); // copyright

		decoder_cfg.NextBit(); // original

		decoder_cfg.GetNumber(2); // emphasis


		frame_size = fsize_table[layer_type][sf_idx][brate_idx] + padding;
		if (layer_type == 3) frame_size <<= 2;
		
		if (!packet_buf) {
			packet_buf.New(0, MP3_MAX_PACKET*(2*ccfg_size + 8192));
						
		}

		if (time_mark == -2) {
			result = frame_size;
			if (xframe_size < frame_size) xframe_size = frame_size;

			if (temp_offset2) {				
				if (result > temp_offset2) {
					result -= temp_offset2;
					temp_offset2 = 0;
				} else {
					temp_offset2 -= result;

					System::MemoryCopy(const_cast<unsigned char *>(data_ptr), data_ptr + result, temp_offset2);
						
					result = 0;
				}

				packet_buf.Release();
			}				

			if (layer_type != 3) time_mark = time_delta + 11520000;
			else time_mark = time_delta + 3840000;

			time_delta = time_mark % sampling_frequency;

			time_mark /= sampling_frequency;
				
			result <<= 32;
			result |= RESULT_CODEC_SPECIAL;
			return result;
		}

// crc ======================================================================
		if (protection_id == 0)	x_val = decoder_cfg.GetNumber(16);
		
// audio data ===============================================================
		if (!stream_cfg.frame_buff) {
			stream_cfg.frame_buff.New(frame_buf_tid, MAX_FRAME_COUNT*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE)*sizeof(float), SFSco::large_mem_mgr);

		}


		if (decoder_cfg.spectral_data = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {			
			previous_frame_delta = stream_cfg.current_frame++;
			stream_cfg.current_frame &= (MAX_FRAME_COUNT - 1);
			
			pfd_2 = previous_frame_delta - 1;
			pfd_2 &= (MAX_FRAME_COUNT - 1);
			if (previous_frame_delta < stream_cfg.current_frame) previous_frame_delta = -1;
			if (pfd_2 < previous_frame_delta) pfd_2 = -1;

			previous_frame_delta *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);
			pfd_2 *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);

			decoder_cfg.spectral_data += stream_cfg.current_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);
			decoder_cfg.frame_cfg = reinterpret_cast<DefaultRenderer::FrameCfg *>(decoder_cfg.spectral_data);
			decoder_cfg.spectral_data += DefaultRenderer::frame_cfg_size;
			
			System::MemoryFill_SSE3(decoder_cfg.frame_cfg, (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE)*sizeof(float), 0, 0);

			decoder_cfg.frame_cfg->time_mark = time_mark;
			decoder_cfg.frame_cfg->source_frequency = sampling_frequency;
			decoder_cfg.frame_cfg->channel_offset = MP3_CHANNEL_SIZE;

			if (decoder_cfg.packet_ptr = reinterpret_cast<unsigned char *>(packet_buf.Acquire())) {
				
				decoder_cfg.current_cfg = reinterpret_cast<ChannelConfig *>(decoder_cfg.packet_ptr) + current_packet*2;
				decoder_cfg.active_cfg = reinterpret_cast<ChannelConfig *>(decoder_cfg.packet_ptr) + active_packet*2;

				if (++current_packet >= MP3_MAX_PACKET) current_packet = 0;

				decoder_cfg.packet_ptr += MP3_MAX_PACKET*2*ccfg_size + 8192;

				decoder_cfg.current_cfg[0].Clear();
				decoder_cfg.current_cfg[1].Clear();

				switch (layer_type) {
					case 1:					
						result = AudioData_III();
					break;
					case 2:					
						result = AudioData_II();
					break;
					case 3:					
						result = AudioData_I();
					break;
				}


				packet_buf.Release();

				if (stream_cfg.ini_block == 0) {
					if (stream_cfg.ini_block = CreateEvent(0, 0, 0, 0)) {
						mixer_thread = CreateThread(0, 0, Audio::DefaultRenderer::MixerLoop, &stream_cfg, 0, 0);
						if (stream_cfg.set_complete & COMPLETE_CONTAINER) WaitForSingleObject(stream_cfg.ini_block, INFINITE);
					}
				}

				if (stream_cfg.flush_sign == -1) stream_cfg.flush_sign = -2;
			}

			stream_cfg.frame_buff.Release();
		}
/*
// ancillary data ===================================================================
		if (layer_type & 0x02) {
			// bit_rate can be used to determine next sync location
			decoder_cfg.SyncSearch();
		}
*/
	} else {
		result = 'mp3e';
	}
	
	if (result != 'mp3e') {
		result <<= 32;
		if (temp_offset2 == 0) {		
			if (result == 0) {
				result = frame_size;
				result <<= 32;
				result |= RESULT_DECODE_COMPLETE;
			}
		} else {
			if (result == 0) {
				result = frame_size;
				if (result > temp_offset2) {
					result -= temp_offset2;
					temp_offset2 = 0;
				} else {
					temp_offset2 -= result;

					System::MemoryCopy(const_cast<unsigned char *>(data_ptr), data_ptr + result, temp_offset2);
						
					result = 0;

				}
				result <<= 32;
				result |= RESULT_DECODE_COMPLETE;
			}

			packet_buf.Release();
		}

		if (decoder_cfg.frame_cfg) { // time_mark = 11520000/sampling_frequency;
			decoder_cfg.frame_cfg->valid_sign = time_delta + (decoder_cfg.frame_cfg->window_count*decoder_cfg.frame_cfg->window_sample_count*10000);
			time_delta = (decoder_cfg.frame_cfg->valid_sign % decoder_cfg.frame_cfg->source_frequency);
			decoder_cfg.frame_cfg->valid_sign /= decoder_cfg.frame_cfg->source_frequency;
			

			time_mark = decoder_cfg.frame_cfg->valid_sign;
		}
	} else {
		result <<= 32;
	}


	return result;
}


	

void Audio::MP3::FilterPreview(unsigned int *, OpenGL::DecodeRecord &) {

}

void Audio::MP3::InitViewBuf() {
	time_delta = 0;
	_src_desc->source_frame++;

	stream_cfg.view_on = stream_cfg.view_on_default;
}

UI_64 Audio::MP3::FilterFrame(unsigned int * pi_ptr) {
	UI_64 result(0);
	
	if (stream_cfg.view_on & 1) {
		_src_desc->source_frame++;
		if (I_64 * vb_cfg = reinterpret_cast<I_64 *>(DefaultRenderer::view_buf.Acquire())) {
			if (reinterpret_cast<unsigned int *>(vb_cfg)[0] != reinterpret_cast<unsigned int *>(vb_cfg)[1]) {
				DefaultRenderer::VSColorMap(pi_ptr, vb_cfg);				
			}

			DefaultRenderer::view_buf.Release();
		}
	}
		
	return result;
}


UI_64 Audio::MP3::FilterSatFrame(float * vector_ptr, OpenGL::DecodeRecord & de_rec) {
	float inc_val(0.0f);
	UI_64 result(0), last_frame(de_rec.target_time & 0xFFFFFFFF), last_adjust(de_rec.target_time >> 32), new_adjust(0), f_off(0);

	if (last_frame == stream_cfg.current_frame) return 0;

	if (de_rec.start_time == 0) last_frame = -1;

	if (float * f_ptr = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {

		result = -System::GetUCount(de_rec.start_time, 0);
		de_rec.start_time = System::GetTime();

		if (last_frame != -1) {
			result *= reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			result /= 10000;

			last_adjust = MP3_GRANULE_SIZE*2 - last_adjust;

			if (result >= last_adjust) {
				result -= last_adjust;
				last_adjust = result % (MP3_GRANULE_SIZE*2);

				last_frame++;
			} else {
				last_adjust = (MP3_GRANULE_SIZE*2) - last_adjust + result;
			}

			last_frame += (result/(MP3_GRANULE_SIZE*2));
			last_frame &= (MAX_FRAME_COUNT - 1);

		} else {
			result = 8192;
			last_adjust = 128;

			last_frame = 7;
		}


		if (last_frame > stream_cfg.current_frame) last_frame = stream_cfg.current_frame;

		de_rec.target_time = last_adjust;
		de_rec.target_time <<= 32;
		de_rec.target_time |= last_frame;


		result = de_rec.width - last_adjust;		
		new_adjust = (result % (MP3_GRANULE_SIZE*2));
		result = (result/(MP3_GRANULE_SIZE*2));

		
		if (new_adjust) {
			f_off = (last_frame - result - 1) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);
			f_off += de_rec.pic_id*MP3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MP3_CHANNEL_SIZE/2);

			if (new_adjust > MP3_GRANULE_SIZE) {
				for (unsigned int si(MP3_GRANULE_SIZE*2 - new_adjust);si<MP3_GRANULE_SIZE;si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

				f_off += 2*MP3_GRANULE_SIZE;

				for (unsigned int si(0);si<MP3_GRANULE_SIZE;si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

			} else {
				f_off += 2*MP3_GRANULE_SIZE;


				for (unsigned int si(MP3_GRANULE_SIZE - new_adjust);si<MP3_GRANULE_SIZE;si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}
			}
		}	
		


		for (;result > 0;result--) {
			f_off = (last_frame - result) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);
			f_off += de_rec.pic_id*MP3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MP3_CHANNEL_SIZE/2);
			
			for (unsigned int si(0);si<MP3_GRANULE_SIZE;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}

			f_off += 2*MP3_GRANULE_SIZE;

			for (unsigned int si(0);si<MP3_GRANULE_SIZE;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}

		}

		if (last_adjust) {
			f_off = last_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);
			f_off += de_rec.pic_id*MP3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MP3_CHANNEL_SIZE/2);			
			
			if (last_adjust > MP3_GRANULE_SIZE) {
				for (unsigned int si(0);si<MP3_GRANULE_SIZE;si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

				last_adjust -= MP3_GRANULE_SIZE;
				f_off += MP3_GRANULE_SIZE*2;
			}
						
			for (unsigned int si(0);si<last_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
			
		}	

		stream_cfg.frame_buff.Release();

		result = 0x00010000;		
	}

	// maybe normalize vector


	return result;
}



void Audio::MP3::JumpOn() {


}			

UI_64 Audio::MP3::UnitTest(const float *) {
	return 0;
}
			
UI_64 Audio::MP3::CfgOverride(UI_64, UI_64) {
	UI_64 result(0);

	return result;
}





unsigned int Audio::MP3::GetSatCount(unsigned int * sat_desc) {
	UI_64 f_off(0);
	unsigned int result(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);

		f_ptr += f_off;
		
		result = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
		for (unsigned int i(0);i<8;i++) sat_desc[i] = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->chan_type[i];

		stream_cfg.frame_buff.Release();
	}


	return result;
}


void Audio::MP3::GetSatInfo(unsigned int i_id, OpenGL::StringsBoard::InfoLine & i_line) {
	UI_64 f_off(0);


	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*MP3_CHANNEL_SIZE);

		f_ptr += f_off;
		
		switch (i_id) {
			case 0:
				i_line.cap[0] = L'M'; i_line.cap[1] = L'P'; i_line.cap[2] = L'4' - layer_type; i_line.cap[3] = 0;
				i_line.cap_size = 3;
				i_line.cap_flags = 0;
			break;
			case 1:
				i_line.cap[0] = L'S'; i_line.cap[1] = L'F'; i_line.cap[2] = L':'; i_line.cap[3] = L'H'; i_line.cap[4] = L'z'; i_line.cap[5] = 0;
				i_line.cap_size = 0x00020003;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeDouble | OpenGL::StringsBoard::InfoTypeFormatted | 1;

				reinterpret_cast<double &>(i_line.val) = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			break;
			case 2:
				i_line.cap[0] = L'c'; i_line.cap[1] = L'h'; i_line.cap[2] = L'a'; i_line.cap[3] = L'n'; i_line.cap[4] = L'n'; i_line.cap[5] = L'e'; i_line.cap[6] = L'l'; i_line.cap[7] = L's'; i_line.cap[8] = 0;
				i_line.cap_size = 8;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeINT64 | OpenGL::StringsBoard::InfoDataPrefix;

				i_line.val = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
			break;
		}

		stream_cfg.frame_buff.Release();
	}
}


// ========================================================================================================================================================

const unsigned int Audio::MP3::sampling_frequencies[4] = {44100, 48000, 32000, 0};

const unsigned int Audio::MP3::fsize_table[4][4][16] = {
	{
		{0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
		{0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
		{0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
		{0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0}
	},

	{
		{0,	104,	130,	156,	182,	208,	261,	313,	365,	417,	522,	626,	731,	835,	1044,	0},
		{0,	96,	120,	144,	168,	192,	240,	288,	336,	384,	480,	576,	672,	768,	960,	0},
		{0,	144,	180,	216,	252,	288,	360,	432,	504,	576,	720,	864,	1008,	1152,	1440,	0}
	},
	{
		{0,	104,	156,	182,	208,	261,	313,	365,	417,	522,	626,	731,	835,	1044,	1253,	0},
		{0,	96,	144,	168,	192,	240,	288,	336,	384,	480,	576,	672,	768,	960,	1152,	0},
		{0,	144,	216,	252,	288,	360,	432,	504,	576,	720,	864,	1008,	1152,	1440,	1728,	0}
	},
	{
		{0,	8,	17,	26, 34,	43,	52,	60,	69,	78,	87,	95,	104,	113,	121,	0},
		{0,	8,	16,	24,	32,	40,	48,	56,	64,	72,	80,	88,	96,	104,	112,	0},
		{0,	12,	24,	36,	48,	60,	72,	84,	96,	108,	120,	132,	144,	156,	168,	0}
	}

};


__declspec(align(16)) const unsigned int Audio::MP3::synth_window[512] = {
	0x00000000, 0xb7800000, 0xb7800000, 0xb7800000, 0xb7800000, 0xb7800000, 0xb7800000, 0xb8000000, 
	0xb8000000, 0xb8000000, 0xb8000000, 0xb8400000, 0xb8400000, 0xb8800000, 0xb8800000, 0xb8a00000, 
	0xb8a00000, 0xb8c00000, 0xb8e00000, 0xb8e00000, 0xb9000000, 0xb9100000, 0xb9200000, 0xb9300000, 
	0xb9500000, 0xb9600000, 0xb9800000, 0xb9880000, 0xb9980000, 0xb9a80000, 0xb9c00000, 0xb9d00000, 
	0xb9e80000, 0xb9f80000, 0xba0c0000, 0xba180000, 0xba240000, 0xba340000, 0xba440000, 0xba540000, 
	0xba680000, 0xba7c0000, 0xba880000, 0xba920000, 0xba9e0000, 0xbaaa0000, 0xbab60000, 0xbac20000, 
	0xbad00000, 0xbade0000, 0xbaea0000, 0xbafa0000, 0xbb040000, 0xbb0b0000, 0xbb130000, 0xbb1a0000, 
	0xbb210000, 0xbb290000, 0xbb300000, 0xbb370000, 0xbb3e0000, 0xbb440000, 0xbb4a0000, 0xbb500000, 

	0x3b550000, 0x3b5a0000, 0x3b5e0000, 0x3b610000, 0x3b630000, 0x3b640000, 0x3b640000, 0x3b630000, 
	0x3b600000, 0x3b5d0000, 0x3b570000, 0x3b500000, 0x3b480000, 0x3b3d0000, 0x3b310000, 0x3b230000, 
	0x3b120000, 0x3afe0000, 0x3ad40000, 0x3aa60000, 0x3a640000, 0x39e80000, 0xb8000000, 0xba100000, 
	0xba900000, 0xbade0000, 0xbb190000, 0xbb450000, 0xbb740000, 0xbb930000, 0xbbad8000, 0xbbc88000, 
	0xbbe58000, 0xbc01c000, 0xbc114000, 0xbc214000, 0xbc31c000, 0xbc42c000, 0xbc540000, 0xbc65c000, 
	0xbc77c000, 0xbc850000, 0xbc8e2000, 0xbc974000, 0xbca06000, 0xbca98000, 0xbcb28000, 0xbcbb4000, 
	0xbcc3e000, 0xbccc4000, 0xbcd44000, 0xbcdbe000, 0xbce32000, 0xbce9c000, 0xbcefe000, 0xbcf54000, 
	0xbcfa2000, 0xbcfe0000, 0xbd009000, 0xbd01b000, 0xbd025000, 0xbd027000, 0xbd020000, 0xbd00f000, 

	0x3cfea000, 0x3cfa0000, 0x3cf40000, 0x3ceca000, 0x3ce3c000, 0x3cd96000, 0x3ccd8000, 0x3cbfe000, 
	0x3cb0c000, 0x3ca00000, 0x3c8d6000, 0x3c728000, 0x3c468000, 0x3c174000, 0x3bc90000, 0x3b390000, 
	0xba340000, 0xbb900000, 0xbc084000, 0xbc4b8000, 0xbc88e000, 0xbcad8000, 0xbcd38000, 0xbcfac000, 
	0xbd11a000, 0xbd267000, 0xbd3bc000, 0xbd517000, 0xbd679000, 0xbd7df000, 0xbd8a4800, 0xbd95a000, 
	0xbda10800, 0xbdac6800, 0xbdb7b800, 0xbdc2e800, 0xbdcde800, 0xbdd8b800, 0xbde33800, 0xbded6800, 
	0xbdf73000, 0xbe004400, 0xbe04ac00, 0xbe08cc00, 0xbe0c9800, 0xbe100c00, 0xbe132000, 0xbe15c400, 
	0xbe17fc00, 0xbe19b800, 0xbe1af000, 0xbe1b9c00, 0xbe1bb800, 0xbe1b3c00, 0xbe1a1c00, 0xbe185800, 
	0xbe15e000, 0xbe12b400, 0xbe0ecc00, 0xbe0a2000, 0xbe04b000, 0xbdfce000, 0xbdeec000, 0xbddef000, 

	0x3dcd7000, 0x3dba3800, 0x3da54000, 0x3d8e8800, 0x3d6c0000, 0x3d377000, 0x3cfea000, 0x3c874000, 
	0x3a8c0000, 0xbc798000, 0xbd04a000, 0xbd4e4000, 0xbd8da800, 0xbdb5d000, 0xbddf9000, 0xbe057000, 
	0xbe1bdc00, 0xbe32fc00, 0xbe4ad000, 0xbe635000, 0xbe7c6c00, 0xbe8b0e00, 0xbe982c00, 0xbea58a00, 
	0xbeb32200, 0xbec0ec00, 0xbecee400, 0xbedd0200, 0xbeeb4000, 0xbef99600, 0xbf03ff00, 0xbf0b3800, 
	0xbf127100, 0xbf19a800, 0xbf20d800, 0xbf27fe00, 0xbf2f1500, 0xbf361900, 0xbf3d0600, 0xbf43d900, 
	0xbf4a8d00, 0xbf511e00, 0xbf578a00, 0xbf5dca00, 0xbf63dd00, 0xbf69be00, 0xbf6f6900, 0xbf74dc00, 
	0xbf7a1300, 0xbf7f0a00, 0xbf81df00, 0xbf841680, 0xbf862a00, 0xbf881780, 0xbf89df00, 0xbf8b7e00, 
	0xbf8cf480, 0xbf8e4180, 0xbf8f6380, 0xbf905a00, 0xbf912480, 0xbf91c300, 0xbf923400, 0xbf927800, 


	0x3f928f00, 0x3f927800, 0x3f923400, 0x3f91c300, 0x3f912480, 0x3f905a00, 0x3f8f6380, 0x3f8e4180, 
	0x3f8cf480, 0x3f8b7e00, 0x3f89df00, 0x3f881780, 0x3f862a00, 0x3f841680, 0x3f81df00, 0x3f7f0a00, 
	0x3f7a1300, 0x3f74dc00, 0x3f6f6900, 0x3f69be00, 0x3f63dd00, 0x3f5dca00, 0x3f578a00, 0x3f511e00, 
	0x3f4a8d00, 0x3f43d900, 0x3f3d0600, 0x3f361900, 0x3f2f1500, 0x3f27fe00, 0x3f20d800, 0x3f19a800, 
	0x3f127100, 0x3f0b3800, 0x3f03ff00, 0x3ef99600, 0x3eeb4000, 0x3edd0200, 0x3ecee400, 0x3ec0ec00, 
	0x3eb32200, 0x3ea58a00, 0x3e982c00, 0x3e8b0e00, 0x3e7c6c00, 0x3e635000, 0x3e4ad000, 0x3e32fc00, 
	0x3e1bdc00, 0x3e057000, 0x3ddf9000, 0x3db5d000, 0x3d8da800, 0x3d4e4000, 0x3d04a000, 0x3c798000, 
	0xba8c0000, 0xbc874000, 0xbcfea000, 0xbd377000, 0xbd6c0000, 0xbd8e8800, 0xbda54000, 0xbdba3800, 

	0x3dcd7000, 0x3ddef000, 0x3deec000, 0x3dfce000, 0x3e04b000, 0x3e0a2000, 0x3e0ecc00, 0x3e12b400, 
	0x3e15e000, 0x3e185800, 0x3e1a1c00, 0x3e1b3c00, 0x3e1bb800, 0x3e1b9c00, 0x3e1af000, 0x3e19b800, 
	0x3e17fc00, 0x3e15c400, 0x3e132000, 0x3e100c00, 0x3e0c9800, 0x3e08cc00, 0x3e04ac00, 0x3e004400, 
	0x3df73000, 0x3ded6800, 0x3de33800, 0x3dd8b800, 0x3dcde800, 0x3dc2e800, 0x3db7b800, 0x3dac6800, 
	0x3da10800, 0x3d95a000, 0x3d8a4800, 0x3d7df000, 0x3d679000, 0x3d517000, 0x3d3bc000, 0x3d267000, 
	0x3d11a000, 0x3cfac000, 0x3cd38000, 0x3cad8000, 0x3c88e000, 0x3c4b8000, 0x3c084000, 0x3b900000, 
	0x3a340000, 0xbb390000, 0xbbc90000, 0xbc174000, 0xbc468000, 0xbc728000, 0xbc8d6000, 0xbca00000, 
	0xbcb0c000, 0xbcbfe000, 0xbccd8000, 0xbcd96000, 0xbce3c000, 0xbceca000, 0xbcf40000, 0xbcfa0000, 

	0x3cfea000, 0x3d00f000, 0x3d020000, 0x3d027000, 0x3d025000, 0x3d01b000, 0x3d009000, 0x3cfe0000, 
	0x3cfa2000, 0x3cf54000, 0x3cefe000, 0x3ce9c000, 0x3ce32000, 0x3cdbe000, 0x3cd44000, 0x3ccc4000, 
	0x3cc3e000, 0x3cbb4000, 0x3cb28000, 0x3ca98000, 0x3ca06000, 0x3c974000, 0x3c8e2000, 0x3c850000, 
	0x3c77c000, 0x3c65c000, 0x3c540000, 0x3c42c000, 0x3c31c000, 0x3c214000, 0x3c114000, 0x3c01c000, 
	0x3be58000, 0x3bc88000, 0x3bad8000, 0x3b930000, 0x3b740000, 0x3b450000, 0x3b190000, 0x3ade0000, 
	0x3a900000, 0x3a100000, 0x38000000, 0xb9e80000, 0xba640000, 0xbaa60000, 0xbad40000, 0xbafe0000, 
	0xbb120000, 0xbb230000, 0xbb310000, 0xbb3d0000, 0xbb480000, 0xbb500000, 0xbb570000, 0xbb5d0000, 
	0xbb600000, 0xbb630000, 0xbb640000, 0xbb640000, 0xbb630000, 0xbb610000, 0xbb5e0000, 0xbb5a0000, 

	0x3b550000, 0x3b500000, 0x3b4a0000, 0x3b440000, 0x3b3e0000, 0x3b370000, 0x3b300000, 0x3b290000, 
	0x3b210000, 0x3b1a0000, 0x3b130000, 0x3b0b0000, 0x3b040000, 0x3afa0000, 0x3aea0000, 0x3ade0000, 
	0x3ad00000, 0x3ac20000, 0x3ab60000, 0x3aaa0000, 0x3a9e0000, 0x3a920000, 0x3a880000, 0x3a7c0000, 
	0x3a680000, 0x3a540000, 0x3a440000, 0x3a340000, 0x3a240000, 0x3a180000, 0x3a0c0000, 0x39f80000, 
	0x39e80000, 0x39d00000, 0x39c00000, 0x39a80000, 0x39980000, 0x39880000, 0x39800000, 0x39600000, 
	0x39500000, 0x39300000, 0x39200000, 0x39100000, 0x39000000, 0x38e00000, 0x38e00000, 0x38c00000, 
	0x38a00000, 0x38a00000, 0x38800000, 0x38800000, 0x38400000, 0x38400000, 0x38000000, 0x38000000, 
	0x38000000, 0x38000000, 0x37800000, 0x37800000, 0x37800000, 0x37800000, 0x37800000, 0x37800000
};
