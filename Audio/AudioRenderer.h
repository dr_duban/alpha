/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#pragma warning(disable:4244)

#include <Windows.h>
#include <Ks.h>
#include <KsMedia.h>

#include "CoreLibrary\SFSCore.h"
#include "Media\CodedSource.h"

#include "Compression\Pack.h"

#define	MAX_FRAME_COUNT		128
#define MAX_CHANNEL_COUNT	8


namespace Audio {	

	void IDCT_IV(float * data, const float * cos_table, UI_64 window_size_2, float * data_out, UI_64 scale_factor);
	void IDCT_II(float * data, const float * cos_table, UI_64 window_size_2, float * data_out, UI_64 scale_factor);

	void FormatWindow(float * data, UI_64 prev_frame_offset, const float * win_tab, unsigned int sample_count);
	void Rescale(float * data, unsigned int sample_count);	
	void AddChannel(float *, const float *, unsigned int, float);
	void Deinterleave(float *, unsigned int);


	unsigned int GenCosTable(float *, unsigned int);
	
	class DefaultRenderer {
		public:
			struct FrameCfg {
				I_64 time_mark;

				unsigned int chan_type[MAX_CHANNEL_COUNT];

				
				unsigned int source_frequency; // 0
				unsigned int target_frequency; // 4
				unsigned int source_chan_count; // 8
				unsigned int target_chan_count; // 12

				unsigned int tch_idx; // 16
				unsigned int target_bit_depth; // 20
				unsigned int channel_offset; // 24				
				unsigned int window_offset; // 28

				unsigned int window_sample_count; // 32
				unsigned int window_count; // 36
				unsigned int volume_val; // 40
				int valid_sign; // 44
				

			};

			struct StreamData {
				static unsigned int view_on_default;
				unsigned int sdata_offset, current_frame, frame_size, terminator, flush_sign, time_shift, reset_flag, drop_frame, volume_val, set_complete, view_on;
				
				I_64 current_timemark, load_timemark;

				void * ini_block;
				SFSco::Object frame_buff;

				StreamData() : sdata_offset(0), current_frame(MAX_FRAME_COUNT - 1), frame_size(0), terminator(-1), flush_sign(-1), reset_flag(-1), drop_frame(MAX_FRAME_COUNT - 1), volume_val(0), time_shift(-1), ini_block(0), current_timemark(0), load_timemark(0), set_complete(0), view_on(0) {};
				~StreamData() { if (ini_block) CloseHandle(ini_block);};
			};

			static const unsigned int frame_cfg_size = (((sizeof(FrameCfg) + 15) & (~15)) >> 2);
			static SFSco::Object view_buf;
			static const UI_64 view_tid = 982451501;
		private:
			static UI_64 loop_counter;

			static __declspec(align(16)) float ls_tab[8192];
			static __declspec(align(16)) unsigned int vs_color_table[2048];

			static const float dmix_matrix[16][16];

			static unsigned int UnpackChannelF(void *, const float *, const unsigned int *);
			static unsigned int IUnpackChannelF(void *, const float *, const unsigned int *, unsigned int *);			

			static unsigned int MixUnpackF(float *, const float *, const unsigned int *, float);

			static void ClearChannel(void *, unsigned int, unsigned int, unsigned int);

			static void CalculateVS(I_64 *);
			static void ShakeVS(I_64 *);

		public:





			static const UI_64 volume_set[];
						
			static DWORD __stdcall MixerLoop(void *);
			static void VSColorMap(unsigned int *, I_64 *);

			static UI_64 Initialize();
			static UI_64 Finalize();

	};



	class DecoderCfg : public Pack::StreamReader {
		public:
			DefaultRenderer::FrameCfg * frame_cfg;					
			float * spectral_data;

			DecoderCfg() : frame_cfg(0), spectral_data(0) {};
	};


	class Codec: public Media::Codec {
		protected:
			DefaultRenderer::StreamData stream_cfg;
		
			HANDLE mixer_thread;
			UI_64 previous_frame_delta, time_delta;

			Codec() : mixer_thread(0), previous_frame_delta(0), time_delta(0) {

				reinterpret_cast<float *>(&stream_cfg.volume_val)[0] = reinterpret_cast<double *>(&SFSco::program_cfg.audio_default_volume)[0];
			}

			virtual ~Codec() {
				if (mixer_thread) {
					stream_cfg.terminator = 0;

					WaitForSingleObject(mixer_thread, INFINITE);
					CloseHandle(mixer_thread);
				}


				stream_cfg.frame_buff.Destroy();
//				stream_cfg.frame_buff.Wait();

			}

			virtual void SetVolume(const unsigned int * a_val) {
				stream_cfg.volume_val = a_val[0];
			};

			virtual void SetComplete(unsigned int cf) {
				stream_cfg.set_complete |= cf;
			};

			virtual void Flush(I_64 current_m, I_64 m_start) {
				stream_cfg.current_timemark = current_m;
				stream_cfg.load_timemark = m_start;

				if (stream_cfg.flush_sign == -2) {
					stream_cfg.flush_sign = stream_cfg.current_frame;
					stream_cfg.time_shift = -1;
				} else {
					stream_cfg.time_shift = stream_cfg.current_frame;
				}
			};

			virtual void Reset() {
				stream_cfg.reset_flag = 1;
				stream_cfg.drop_frame = stream_cfg.current_frame;
			};


	};

}

/*

*/
