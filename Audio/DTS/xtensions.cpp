/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/



#include "Audio\DTS.h"

#include "System\SysUtils.h"
#include "Securitat\Crypter.h"

UI_64 Audio::DTS::GetLLCSHeader(LLConfig::SubSetHdr & sub_hdr, unsigned int asset_idx, unsigned int scalable_flag) {
	DecoderState temp_reader(decoder_cfg);
	unsigned int h_size(0), x_val(0), x_val1(0), band_idx(0), original_chan_order[MAX_CHANNEL_COUNT];
	float t_sfa(0);

	temp_reader = decoder_cfg;

	h_size = decoder_cfg.GetNumber(10) + 1;

	sub_hdr.chan_count = decoder_cfg.GetNumber(4) + 1;
	sub_hdr.residual_ce = decoder_cfg.GetNumber(sub_hdr.chan_count);

	sub_hdr.bit_resolution = decoder_cfg.GetNumber(5);
	sub_hdr.bit_width = decoder_cfg.GetNumber(5);

	if (sub_hdr.bit_width > 16) {
		sub_hdr.bits4abit = 5;
	} else {
		if (sub_hdr.bit_width > 8) {
			sub_hdr.bits4abit = 4;
		} else {
			sub_hdr.bits4abit = 3;
		}

		if (ll_frame.chan_set_count > 1) {
			sub_hdr.bits4abit++;
		}
	}


	sub_hdr.sf_idx = decoder_cfg.GetNumber(4);

	sub_hdr.sampling_frequency = ll_sampling_frequencies[sub_hdr.sf_idx];

	sub_hdr.interpolation_multiplier = decoder_cfg.GetNumber(2);
	sub_hdr.replacement_cs = decoder_cfg.GetNumber(2);

	if (sub_hdr.replacement_cs > 0) {		
		sub_hdr.skip_decode_flag = decoder_cfg.NextBit() ^ 1;
	}

	if (asset[asset_idx].one2one_flag) {
		sub_hdr.primary_chan_flag = decoder_cfg.NextBit();

		if (x_val = decoder_cfg.NextBit()) { // downmix coeff embedded
			sub_hdr.d_mix.embed_flag = decoder_cfg.NextBit();

			if (sub_hdr.primary_chan_flag) {
				sub_hdr.d_mix._type = decoder_cfg.GetNumber(3);
			}
		}

		sub_hdr.hierarchical_flag = decoder_cfg.NextBit();
		
		if (x_val) {
			if (sub_hdr.primary_chan_flag) {				
				x_val = mix_chan_count[sub_hdr.d_mix._type];
								
				for (unsigned int i(0);i<x_val;i++) {
					for (unsigned int j(0);j<sub_hdr.chan_count;j++) {
						sub_hdr.d_mix.coffs[i*sub_hdr.chan_count + j] = dmix_tab[decoder_cfg.GetNumber(9)]; // downmix coeffs
					}
				}

			} else {				
				x_val = asset[asset_idx].col_count;

				for (unsigned int i(0);i<x_val;i++) {
					t_sfa = dmix_tab[decoder_cfg.GetNumber(9) + 512]; // inv scale

					for (unsigned int j(0);j<sub_hdr.chan_count;j++) {
						sub_hdr.d_mix.coffs[i*sub_hdr.chan_count + j] = t_sfa*dmix_tab[decoder_cfg.GetNumber(9)]; // downmix coeffs
					}
				}
			}

			
//			decoder_cfg.SkipBits(x_val*9);
		}

		if (decoder_cfg.NextBit()) {
			sub_hdr.channel_config = decoder_cfg.GetNumber(ll_frame.bits4mask); // channel mask (see page 75)
			sub_hdr.map_offset = ll_frame.tot_chan_count;

			x_val = sub_hdr.channel_config;

			for (unsigned int chan_id(0), j(0);x_val && (ll_frame.tot_chan_count<MAX_CHANNEL_COUNT);x_val >>= 1, chan_id++) {
				if (x_val & 1) {
					for (j=0;j<decoder_cfg.frame_cfg->source_chan_count;j++) {
						if (decoder_cfg.frame_cfg->chan_type[j] == dts2win_map[chan_id]) {
							ll_frame.chan_map[ll_frame.tot_chan_count++] = j;
							break;
						}
					}

					if (j >= decoder_cfg.frame_cfg->source_chan_count) {
						ll_frame.chan_map[ll_frame.tot_chan_count++] = decoder_cfg.frame_cfg->source_chan_count + ll_frame.x_chan_count;
						decoder_cfg.frame_cfg->chan_type[decoder_cfg.frame_cfg->source_chan_count + ll_frame.x_chan_count++] = dts2win_map[chan_id];
					}
				}
			}

		} else {
			for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
				decoder_cfg.GetNumber(9); // radius delta
				decoder_cfg.GetNumber(9); // theta
				decoder_cfg.GetNumber(7); // phi
			}
		}

		
	} else {
		sub_hdr.primary_chan_flag = 1;
		sub_hdr.hierarchical_flag = 1;

		if (decoder_cfg.NextBit()) { // mapping coeff present
			sub_hdr.d_mix.map_coff_bits = 6 + (decoder_cfg.GetNumber(3)<<1);
			sub_hdr.d_mix.speaker_config_count = decoder_cfg.GetNumber(2) + 1;

			for (unsigned int i(0);i<sub_hdr.d_mix.speaker_config_count;i++) {
				sub_hdr.d_mix.channel_mask[i] = decoder_cfg.GetNumber(sub_hdr.chan_count);
				sub_hdr.d_mix.speaker_count[i] = decoder_cfg.GetNumber(6) + 1;

				if (x_val = decoder_cfg.NextBit()) {
					sub_hdr.d_mix.speaker_mask[i] = decoder_cfg.GetNumber(ll_frame.bits4mask);
				}

				for (unsigned int j(0);j<sub_hdr.d_mix.speaker_count[i];j++) {
					if (!x_val) {
						decoder_cfg.GetNumber(9); // radius delta
						decoder_cfg.GetNumber(9); // theta
						decoder_cfg.GetNumber(7); // phi
					}

					x_val1 = sub_hdr.d_mix.channel_mask[i];
					for (unsigned int s(0);s<sub_hdr.chan_count;s++) {
						if (x_val1 & 1) {
							decoder_cfg.GetNumber(sub_hdr.d_mix.map_coff_bits); // speacker map coeffs
						}
						x_val1 >>= 1;

					}
				}
			}
		}
	}

	asset[asset_idx].col_count += sub_hdr.chan_count;

	if (sub_hdr.sampling_frequency > 96000) {
		sub_hdr.xband_count = 2 + (decoder_cfg.NextBit() << 1);
	} else {
		sub_hdr.xband_count = 1;
	}

	if (sub_hdr.xband_count > ll_frame.fband_count) ll_frame.fband_count = sub_hdr.xband_count;

	for (x_val1 = 0;(1<<x_val1)<sub_hdr.chan_count;x_val1++);

	for (unsigned int band_idx(0);band_idx < sub_hdr.xband_count;band_idx++) {

		sub_hdr.band_cfg[band_idx].decorelation_enabled = decoder_cfg.NextBit();

		if (sub_hdr.band_cfg[band_idx].decorelation_enabled) {
			if (x_val1) {
				for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
					original_chan_order[i] = ll_frame.chan_map[sub_hdr.map_offset + decoder_cfg.GetNumber(x_val1)];
				}

				for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
					ll_frame.chan_map[sub_hdr.map_offset + i] = original_chan_order[i];
				}

			}

			for (unsigned int i(0);i<(sub_hdr.chan_count>>1);i++) {
				if (decoder_cfg.NextBit()) { //; chPFlag
					sub_hdr.band_cfg[band_idx].decor_alpha[i] = decoder_cfg.GetSignedVal(7); // pairwise coeff
				}
			}
		}


		for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
			sub_hdr.band_cfg[band_idx].adaptive_predictor_order[i] = decoder_cfg.GetNumber(4); // adaptpredorder
			if (sub_hdr.band_cfg[band_idx].adaptive_predictor_order[i] > sub_hdr.band_cfg[band_idx].max_ap_order) sub_hdr.band_cfg[band_idx].max_ap_order = sub_hdr.band_cfg[band_idx].adaptive_predictor_order[i];
		}

		for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
			if (sub_hdr.band_cfg[band_idx].adaptive_predictor_order[i] == 0) {
				sub_hdr.band_cfg[band_idx].fixed_predictor_order[i] = decoder_cfg.GetNumber(2);
			}
		}

		for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
			for (unsigned int j(0);j<sub_hdr.band_cfg[band_idx].adaptive_predictor_order[i];j++) {
				sub_hdr.band_cfg[band_idx].refl_coffs[j] = decoder_cfg.GetNumber(8); // refl coeffs
			}
		}

		if (band_idx) {
			if (sub_hdr.d_mix.embed_flag) {
				decoder_cfg.NextBit(); // enbdmixinextband
			}

			scalable_flag = decoder_cfg.NextBit();
			
		}

		if (scalable_flag) {
			sub_hdr.band_cfg[band_idx].lsb_present = decoder_cfg.GetNumber(ll_frame.bits4ssize); // lsb section size

			for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
				sub_hdr.band_cfg[band_idx].lsb_abit[i] = decoder_cfg.GetNumber(4); // scalable lsb
			}
		}

		if (ll_frame.fixed_lsb_split) {
			for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
				sub_hdr.band_cfg[band_idx].lsb_abit[i] = ll_frame.fixed_lsb_split;
			}
		}

		if (band_idx) scalable_flag = decoder_cfg.NextBit();
	
		if (scalable_flag) {
			for (unsigned int i(0);i<sub_hdr.chan_count;i++) {
				decoder_cfg.GetNumber(4); // bit width adjust
			}
		}
	}


	decoder_cfg = temp_reader;
	decoder_cfg.Skip(h_size - 2);
	decoder_cfg.GetNumber(16); // CRC

	return 0;
}

void Audio::DTS::GetLLSegState(unsigned int band_idx, unsigned int seg_idx, const LLConfig::SubSetHdr * sub_hdr) {
	unsigned int x_val(0);

	if (ll_frame.seg_type = decoder_cfg.NextBit()) {
		x_val = 1;
	} else { // distict coding paramenter for each channel
		x_val = sub_hdr->chan_count;
	}


	for (unsigned int p(0);p<x_val;p++) {
		ll_frame.c_info[p].rice_flag = decoder_cfg.NextBit();

		if ((ll_frame.seg_type == 0) && ll_frame.c_info[p].rice_flag) {
			if (decoder_cfg.NextBit()) {
				ll_frame.c_info[p].aux_abit = decoder_cfg.GetNumber(sub_hdr->bits4abit) + 1;
			} else {
				ll_frame.c_info[p].aux_abit = 0; // no hybrid
			}
		}
	}


	for (unsigned int p(0);p<x_val;p++) {
		if (seg_idx == 0) {
		// coding parameter for part 1
			ll_frame.c_info[p].abit_0 = decoder_cfg.GetNumber(sub_hdr->bits4abit);

			if (ll_frame.c_info[p].rice_flag == 0) {
				if (ll_frame.c_info[p].abit_0) ll_frame.c_info[p].abit_0++;
			}

			if (ll_frame.seg_type == 0) {
				ll_frame.c_info[p].sample_part_0 = sub_hdr->band_cfg[band_idx].adaptive_predictor_order[p];
			} else {
				ll_frame.c_info[p].sample_part_0 = sub_hdr->band_cfg[band_idx].max_ap_order;
			}
		} else {
			ll_frame.c_info[p].abit_0 = 0;
			ll_frame.c_info[p].sample_part_0 = 0;
		}

	// coding paramenter for part 2 of segment
		ll_frame.c_info[p].abit = decoder_cfg.GetNumber(sub_hdr->bits4abit);
		if (ll_frame.c_info[p].rice_flag == 0) {
			if (ll_frame.c_info[p].abit) ll_frame.c_info[p].abit++;
		}
	}

	if (ll_frame.seg_type) {
		for (unsigned int i(1);i<sub_hdr->chan_count;i++) {
			ll_frame.c_info[i] = ll_frame.c_info[0];
		}
	}

}

void Audio::DTS::GetLLMSB(const LLConfig::SubSetHdr * sub_hdr, unsigned int se_idx) {
	int * d_ptr(0);
	unsigned int x_val(0), x_count(0);

	for (unsigned int ci(0);ci<sub_hdr->chan_count;ci++) {
		d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data) + se_idx*ll_frame.samples_per_segment;

		if ((ci + sub_hdr->map_offset) < MAX_CHANNEL_COUNT) {
			d_ptr += (ll_frame.chan_map[ci + sub_hdr->map_offset]*DTS_CHANNEL_SIZE + 3072);
		}

		if (ll_frame.c_info[ci].rice_flag == 0) { // linear mode
			if (ll_frame.c_info[ci].abit_0) {
				for (unsigned int n(0);n<ll_frame.c_info[ci].sample_part_0;n++) {
					d_ptr[n] = decoder_cfg.GetSignedVal(ll_frame.c_info[ci].abit_0);
				}
			}


			if (ll_frame.c_info[ci].abit) {
				for (unsigned int n(ll_frame.c_info[ci].sample_part_0);n<ll_frame.samples_per_segment;n++) {
					d_ptr[n] = decoder_cfg.GetSignedVal(ll_frame.c_info[ci].abit);
				}
			}

		} else { // rice code			
			for (unsigned int n(0);n<ll_frame.c_info[ci].sample_part_0;n++) {
				d_ptr[n] = decoder_cfg.GetRiceVal();					
			
				if (ll_frame.c_info[ci].abit_0) {
					d_ptr[n] <<= ll_frame.c_info[ci].abit_0;
					d_ptr[n] |= decoder_cfg.GetNumber(ll_frame.c_info[ci].abit_0);
				}
				
				if (d_ptr[n] & 1) {
					d_ptr[n] = -(d_ptr[n] >> 1) - 1;
				} else {
					d_ptr[n] >>= 1;
				}				
			}
		

			if (ll_frame.c_info[ci].aux_abit) {
/*
	The 'Hybrid' RICE coding is employed to guarantee an upper bound (nBits4SamplLoci +
	m_pancAuxABIT[nParmIndex]) on the length of the unary part of RICE codes

	don't know what it means but top address is equal to ll_frame.samples_per_segment, so i need to read at least log2(ll_frame.samples_per_segment)
*/
								
				x_count = decoder_cfg.GetNumber(ll_frame.bits4addr);

				for (int i(0);i<x_count;i++) {
				// location of isolated max samples
					d_ptr[decoder_cfg.GetNumber(ll_frame.bits4addr)] = 0x80000000;
				}
			}

	// unpack all residuals in one channel (part 2)
			for (unsigned int n(ll_frame.c_info[ci].sample_part_0);n<ll_frame.samples_per_segment;n++) {
				if (d_ptr[n] != 0x80000000) {
					d_ptr[n] = decoder_cfg.GetRiceVal();
					if (ll_frame.c_info[ci].abit) {
						d_ptr[n] <<= ll_frame.c_info[ci].abit;
						d_ptr[n] |= decoder_cfg.GetNumber(ll_frame.c_info[ci].abit);
					}
				} else {
					d_ptr[n] = decoder_cfg.GetNumber(ll_frame.c_info[ci].aux_abit);
				}

				if (d_ptr[n] & 1) {
					d_ptr[n] = -(d_ptr[n] >> 1) - 1;
				} else {
					d_ptr[n] >>= 1;
				}				
			}
		}
	}
}

void Audio::DTS::CalculateLLBand(unsigned int band_idx) {
	__int64 c_vals[2][256], i_val(0);
	int * d_ptr(0), * s_ptr(0), fixed_vals[4];
	float * dst_ptr(0), * src_ptr(0);
	LLConfig::SubSetHdr * subs_hdr(0);
	
	unsigned int frm_size = ll_frame.segment_count*ll_frame.samples_per_segment, temp_c(0);

	
	
	for (unsigned int set_idx(0);set_idx<ll_frame.chan_set_count;set_idx++) {
		subs_hdr = ll_frame.ss_hdr + set_idx;

		for (unsigned int ci(0);ci<subs_hdr->chan_count;ci++) {
			d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data);
						
			if ((ci + subs_hdr->map_offset) < MAX_CHANNEL_COUNT) {
				d_ptr += (ll_frame.chan_map[ci + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 3072);
			}

		// fixed coefficient prediction
			if (subs_hdr->band_cfg[band_idx].fixed_predictor_order[ci]) {
				
				fixed_vals[0] = fixed_vals[1] = fixed_vals[2] = fixed_vals[3] = 0;

				for (unsigned int n(0);n<frm_size;n++) {
					fixed_vals[1] += d_ptr[n];
					for (unsigned int nord(2);nord <= subs_hdr->band_cfg[band_idx].fixed_predictor_order[ci];nord++) {
						fixed_vals[nord] += fixed_vals[nord - 1];
					}

					d_ptr[n] = fixed_vals[subs_hdr->band_cfg[band_idx].fixed_predictor_order[ci]];

				}

			}

		// adaptive coffs prediction
			if (subs_hdr->band_cfg[band_idx].adaptive_predictor_order[ci]) {					
				
				for (unsigned int n(0);n<subs_hdr->band_cfg[band_idx].adaptive_predictor_order[ci];n++) {
					c_vals[temp_c ^ 1][n] = ll_adpcm_tab[subs_hdr->band_cfg[band_idx].refl_coffs[n]];

					for (unsigned int m(0);m<n;m++) {
						c_vals[temp_c ^ 1][m] = c_vals[temp_c][m] + ((c_vals[temp_c][n - m - 1]*c_vals[temp_c ^ 1][n] + 0x8000) >> 16);
					}						

					temp_c ^= 1;
				}

						
				for (unsigned int n(subs_hdr->band_cfg[band_idx].adaptive_predictor_order[ci]);n<frm_size;n++) {
					i_val = 0;
					for (unsigned int m(0);m<subs_hdr->band_cfg[band_idx].adaptive_predictor_order[ci];m++) {
						i_val += c_vals[temp_c][m]*d_ptr[n - m - 1];
					}
					i_val += 0x8000;
					i_val >>= 16;

					if (i_val > 8388607) i_val = 8388607;					
					if (i_val < -8388608) i_val = -8388608;
					
					d_ptr[n] += i_val;				
				}
			}
		}				

	// pairwise decorellation (done on a channel pair)
		for (unsigned int ci(0);ci<(subs_hdr->chan_count>>1);ci++) {
			if (subs_hdr->band_cfg[band_idx].decor_alpha[ci]) {
				d_ptr = s_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data);

				if ((2*(ci + 1) + subs_hdr->map_offset) <= MAX_CHANNEL_COUNT) {
					s_ptr += (ll_frame.chan_map[2*ci + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 3072);
					d_ptr += (ll_frame.chan_map[2*ci + 1 + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 3072);
				}

				for (unsigned int n(0);n<frm_size;n++) {
					d_ptr[n] += ((s_ptr[n]*subs_hdr->band_cfg[band_idx].decor_alpha[ci] + 32) >> 6); // thoughts: abs alpha has width of 6bits... assuming alpha value lies in the range of [-1, 1] ...
				}							
			}
		}
					
// convert to float
		d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data);

		temp_c = subs_hdr->residual_ce;
		for (unsigned int ci(0);ci<subs_hdr->chan_count;ci++, (temp_c>>=1)) {
			if ((ci + subs_hdr->map_offset) < MAX_CHANNEL_COUNT) {
				if (temp_c & 1) {
					LLOverride(d_ptr + (ll_frame.chan_map[ci + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 2048), frm_size, 0x3F800000 - ((int)subs_hdr->bit_resolution << 23));
				} else {
					LLConvertAdd(d_ptr + (ll_frame.chan_map[ci + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 2048), frm_size, 0x3F800000 - ((int)subs_hdr->bit_resolution << 23));
				}
			}
		}

		if (subs_hdr->primary_chan_flag == 0)
		if (subs_hdr->d_mix.embed_flag && (subs_hdr->replacement_cs == 0)) { // reverse the downmix
			if (subs_hdr->hierarchical_flag) { // hierarchical downmix						
				
				for (unsigned int i(0);i<set_idx;i++) {
					temp_c = 0;
					for (unsigned int j(0);j<ll_frame.ss_hdr[i].chan_count;j++) {
						// channels of prev subsets, add scaled chans from currrent set
						if ((j + ll_frame.ss_hdr[i].map_offset) < MAX_CHANNEL_COUNT) {
							dst_ptr = decoder_cfg.spectral_data + (ll_frame.chan_map[j + ll_frame.ss_hdr[i].map_offset]*DTS_CHANNEL_SIZE + 2048);

							for (unsigned int k(0);k<subs_hdr->chan_count;k++) {
								if ((k + subs_hdr->map_offset) < MAX_CHANNEL_COUNT) {
									src_ptr = decoder_cfg.spectral_data + (ll_frame.chan_map[k + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 2048);

									if (subs_hdr->d_mix.coffs[temp_c + k]) AddChannel(dst_ptr, src_ptr, frm_size, subs_hdr->d_mix.coffs[temp_c + k]);
								}
							}
						}
						temp_c += subs_hdr->chan_count;
					}
				}

			} else { // parallel downmix
				
				for (unsigned int i(0);i<ll_frame.ss_hdr[0].chan_count;i++) {
					// channels of first subset, add scaled chans from currrent set
					if ((i + ll_frame.ss_hdr[0].map_offset) < MAX_CHANNEL_COUNT) {
						dst_ptr = decoder_cfg.spectral_data + (ll_frame.chan_map[i + ll_frame.ss_hdr[i].map_offset]*DTS_CHANNEL_SIZE + 2048);
					
						for (unsigned int j(0);j<subs_hdr->chan_count;j++) {
							if ((j + subs_hdr->map_offset) < MAX_CHANNEL_COUNT) {
								src_ptr = decoder_cfg.spectral_data + (ll_frame.chan_map[j + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 2048);

								if (subs_hdr->d_mix.coffs[temp_c + j]) AddChannel(dst_ptr, src_ptr, frm_size, subs_hdr->d_mix.coffs[temp_c + j]);
							}
						}
					}
					temp_c += subs_hdr->chan_count;
				}
			}
		}
	}
}

UI_64 Audio::DTS::GetLossless(unsigned int asset_idx) {
	
	DecoderState temp_reader(decoder_cfg);
	LLConfig::SubSetHdr * subs_hdr(0);
	unsigned int h_val(0);
	__int64 i_val(0);
	
	unsigned char temp_c(0);

	

// common header ==========================================================================
	decoder_cfg.GetNumber(4); // version

	h_val = decoder_cfg.GetNumber(8) + 1;

	ll_frame.size = decoder_cfg.GetNumber(decoder_cfg.GetNumber(5) + 1) + 1;
	ll_frame.chan_set_count = decoder_cfg.GetNumber(4) + 1;

	ll_frame.segment_count = (1 << decoder_cfg.GetNumber(4));
	
// it will be stupid if number of samples in xtensions would exceed the number of samples in core
	ll_frame.bits4addr = decoder_cfg.GetNumber(4);
	ll_frame.samples_per_segment = (1 << ll_frame.bits4addr);

	ll_frame.bits4ssize = decoder_cfg.GetNumber(5) + 1;

	ll_frame.crc_config = decoder_cfg.GetNumber(2);

	ll_frame.lsb_scalable_flag = decoder_cfg.NextBit();

	ll_frame.bits4mask = decoder_cfg.GetNumber(5) + 1;

	if (ll_frame.lsb_scalable_flag) ll_frame.fixed_lsb_split = decoder_cfg.GetNumber(4);

	decoder_cfg = temp_reader;
	decoder_cfg.Skip(h_val - 6);
	decoder_cfg.GetNumber(16); // CRC

	if (ll_frame.segment_count <= 256) { // no support of ultra fucking segmentation
// sub header channel set ================================================================
		for (unsigned int i(0);i<ll_frame.chan_set_count;i++) {
			GetLLCSHeader(ll_frame.ss_hdr[i], asset_idx, ll_frame.lsb_scalable_flag);
		}

// navigation index table ==================================================================================
/*
		for (unsigned int band_idx(0);band_idx<ll_frame.fband_count;band_idx++) {
			ll_frame.b_info[band_idx].size = 0;

			for (unsigned int seg_idx(0);seg_idx<ll_frame.segment_count;seg_idx++) {			
				ll_frame.b_info[band_idx].segment_size[seg_idx] = 0; // reference says nya decoder_cfg.GetNumber(ll_frame.bits4ssize), and it looks like nya 0  WTF??????????????????????????????

				for (unsigned int set_idx(0);set_idx<ll_frame.chan_set_count;set_idx++) {
					if (ll_frame.ss_hdr[set_idx].xband_count > band_idx) {
						ll_frame.b_info[band_idx].set_size[seg_idx][set_idx] = decoder_cfg.GetNumber(ll_frame.bits4ssize) + 1;
						ll_frame.b_info[band_idx].segment_size[s] += ll_frame.b_info[band_idx].set_size[s][c];
					}
				}

				ll_frame.b_info[band_idx].size += ll_frame.b_info[band_idx].segment_size[s];
			}
		}

		decoder_cfg.ByteAlign();

		just ignore this NAVI section

/*/
		
		for (unsigned short crc_val(-1);(crc_val != ((unsigned short)decoder_cfg.src_ptr[0] | ((unsigned short)decoder_cfg.src_ptr[1]<<8))) && (decoder_cfg.src_size > 2);) {
			temp_c = decoder_cfg.NextByte();
			Crypt::CRC_16(crc_val, &temp_c, 1, 3);
		}
//*/
		decoder_cfg.GetNumber(16); // crc

//		temp_reader = decoder_cfg;

// currently no support of sampling frequencies above 96k, so, only frequency band 0 should be processed
		for (unsigned int band_idx(0);band_idx<1;band_idx++) { // all bands syntax

			for (unsigned int seg_idx(0);seg_idx<ll_frame.segment_count;seg_idx++) {				
				/*
					differernt segment types

					segment 0 subdevided in 2 parts: PCM & ADPCM

					After the RICE coding flags and the corresponding coding parameters (ABIT) have been unpacked, the remainder of
					the data is unpacked. This is described in clauses 8.4.1.2 and 8.4.1.4 of the present document.
				*/

				for (unsigned int set_idx(0);set_idx<ll_frame.chan_set_count;set_idx++) {
					subs_hdr = ll_frame.ss_hdr + set_idx;

					if (subs_hdr->xband_count <= band_idx) continue;

					if ((seg_idx == 0) || (decoder_cfg.NextBit() == 0)) { // new seg state
						GetLLSegState(band_idx, seg_idx, subs_hdr);
					}


					GetLLMSB(subs_hdr, seg_idx);
												
					if (ll_frame.crc_config) {
						if (band_idx == 0) {
							decoder_cfg.ByteAlign();
							decoder_cfg.GetNumber(16); // CRC
						}
					}

				// decimator history for bands 1 & 3
					if (seg_idx == 0) {
						if (band_idx & 1) {
							decoder_cfg.GetNumber(5); // bits4hist always 32
							for (unsigned int i(0);i<subs_hdr->chan_count;i++) {
								for (unsigned int j(0);j<7;j++) {
									decoder_cfg.GetNumber(32);									
								}
							}
						}
					}


				// lsb residual
					if (subs_hdr->band_cfg[band_idx].lsb_present) {
						decoder_cfg.ByteAlign();

						for (unsigned int ci(0);ci<subs_hdr->chan_count;ci++) {
							// do channel adjust
							if (subs_hdr->band_cfg[band_idx].lsb_abit[ci]) {
								int * d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data) + seg_idx*ll_frame.samples_per_segment;

								if ((ci + subs_hdr->map_offset) < MAX_CHANNEL_COUNT) {
									d_ptr += (ll_frame.chan_map[ci + subs_hdr->map_offset]*DTS_CHANNEL_SIZE + 3072);
								}

								for (unsigned int n(0);n<ll_frame.samples_per_segment;n++) {
									d_ptr[n] <<= subs_hdr->band_cfg[band_idx].lsb_abit[ci];
									d_ptr[n] |= decoder_cfg.GetNumber(subs_hdr->band_cfg[band_idx].lsb_abit[ci]);
								}
							}
						}

						if (ll_frame.crc_config > 1) {
							if (band_idx == 0) {
								decoder_cfg.ByteAlign();
								decoder_cfg.GetNumber(16); // CRC
							}
						}
					}

					decoder_cfg.ByteAlign();

//					temp_reader.Skip(ll_frame.b_info[band_idx].set_size[seg_idx][set_idx]);
//					decoder_cfg = temp_reader;					


				} // set loop
			


			} // segment loop

			
			if (ll_frame.crc_config == 3) {
				if (band_idx) {
					decoder_cfg.GetNumber(16);
				}
			}


// all calc is done on per band basis
			CalculateLLBand(band_idx);


		} // band loop
		

		decoder_cfg.frame_cfg->source_chan_count = ll_frame.tot_chan_count;

		return RESULT_DECODE_COMPLETE;
	} else {
		return 0;
	}	
}


UI_64 Audio::DTS::GetLowBitrate(unsigned int asset_idx) {
	

	return 0;
}


// =========================================================================================================================================================================================

const unsigned int Audio::DTS::ll_sampling_frequencies[] = {
	8000, 16000, 32000, 64000,
	128000, 22050, 44100, 88200,
	176400, 352800, 12000, 24000,
	48000, 96000, 192000, 384000

};

const unsigned int Audio::DTS::dts2win_map[] = {
	SPEAKER_FRONT_CENTER,
	SPEAKER_FRONT_LEFT,
	SPEAKER_FRONT_RIGHT,
	-2, // -110
	-2, // 110
	SPEAKER_LOW_FREQUENCY,
	SPEAKER_BACK_CENTER,
	SPEAKER_BACK_LEFT,
	SPEAKER_BACK_RIGHT,
	SPEAKER_SIDE_LEFT,
	SPEAKER_SIDE_RIGHT,
	-2,
	-2, 
	-2,
	-2,
	-2
};


