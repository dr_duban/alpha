/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Audio\DTS.h"

#include "System\SysUtils.h"

void Audio::DTS::GetSideInfo() {
	int scale_fac_ptr(0);
	unsigned int scale_val(0), step_size(0), scale_adjust(0), * channel_ptr(0);

	
	for (unsigned int i(0);i<MAX_CHANNEL_COUNT;i++) {
		for (unsigned int j(0);j<32;j++) frame_cfg.chan_desc[i].pvq_index[j] = 0; 
		for (unsigned int j(0);j<32;j++) frame_cfg.chan_desc[i].joint_scale_factor[j] = 0; 

		for (unsigned int j(0);j<32;j++) frame_cfg.chan_desc[i].prediction_mode[j] = 0; 
		for (unsigned int j(0);j<32;j++) frame_cfg.chan_desc[i].allocation_index[j] = 0; 
		for (unsigned int j(0);j<32;j++) frame_cfg.chan_desc[i].transition_mode[j] = 0; 
	}

	
	frame_cfg.subsub_count = decoder_cfg.GetNumber(2) + 1;
	frame_cfg.partial_s_count = decoder_cfg.GetNumber(3);

	decoder_cfg.frame_cfg->window_sample_count = frame_cfg.subsub_count*8*32;

	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		for (unsigned char j(0);j<frame_cfg.chan_desc[i].activ_subband_count;j++) {
			frame_cfg.chan_desc[i].prediction_mode[j] = decoder_cfg.NextBit();
		}
	}



	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		for (unsigned char j(0);j<frame_cfg.chan_desc[i].activ_subband_count;j++) {
			if (frame_cfg.chan_desc[i].prediction_mode[j]) { // ADPCM coded low frequency
				frame_cfg.chan_desc[i].pvq_index[j] = decoder_cfg.GetNumber(12);
			}
		}
	}



	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		switch (frame_cfg.chan_desc[i].bit_allocation_cb) {
			case 7: // invalid

			break;
			case 6: // 5-bit
				for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) {
					frame_cfg.chan_desc[i].allocation_index[j] = decoder_cfg.GetNumber(5);
				}
			break;
			case 5: // 4-bit
				for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) {
					frame_cfg.chan_desc[i].allocation_index[j] = decoder_cfg.GetNumber(4);
				}
			break;
			default: // huffman coded
				for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) {
					frame_cfg.chan_desc[i].allocation_index[j] = decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*frame_cfg.chan_desc[i].bit_allocation_cb]<<1), hts_cfg[4*frame_cfg.chan_desc[i].bit_allocation_cb + 1]);
				}

		}
	}



	if (frame_cfg.subsub_count > 1)
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) { // 
			if (frame_cfg.chan_desc[i].allocation_index[j]) {
				frame_cfg.chan_desc[i].transition_mode[j] = decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*(frame_cfg.chan_desc[i].transient_mode_cb + 5)]<<1), hts_cfg[4*(frame_cfg.chan_desc[i].transient_mode_cb + 5) + 1]);

			}
		}
	}


	

	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		channel_ptr = reinterpret_cast<unsigned int *>(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE + 4);

		for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) {
			if (frame_cfg.chan_desc[i].allocation_index[j]) {
				switch (frame_cfg.chan_desc[i].scale_factor_cb) {
					case 7: // invalid

					break;
					case 6: // 7-bit
						scale_fac_ptr = decoder_cfg.GetNumber(7);
						scale_val = scale_fac7[scale_fac_ptr];
					break;
					case 5: // 6-bit
						scale_fac_ptr = decoder_cfg.GetNumber(6);
						scale_val = scale_fac6[scale_fac_ptr];
					break;
					default:
						scale_fac_ptr += decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9)]<<1), hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9) + 1]);
						scale_val = scale_fac6[scale_fac_ptr];

				}								

				step_size = q_step_size[bs_hdr.br_idx == 0x1F][frame_cfg.chan_desc[i].allocation_index[j]];
				scale_adjust = scale_adjustment[frame_cfg.chan_desc[i].scale_factor_adjustment[frame_cfg.chan_desc[i].allocation_index[j]]];

				if (frame_cfg.chan_desc[i].transition_mode[j]) {
					for (unsigned int t(0);t<frame_cfg.chan_desc[i].transition_mode[j];t++) {
						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;

						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;
					}

					switch (frame_cfg.chan_desc[i].scale_factor_cb) {
						case 7: // invalid

						break;
						case 6: // 7-bit
							scale_fac_ptr = decoder_cfg.GetNumber(7);
							scale_val = scale_fac7[scale_fac_ptr];
						break;
						case 5: // 6-bit
							scale_fac_ptr = decoder_cfg.GetNumber(6);
							scale_val = scale_fac6[scale_fac_ptr];
						break;
						default:
							scale_fac_ptr += decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9)]<<1), hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9) + 1]);
							scale_val = scale_fac6[scale_fac_ptr];

					}

					for (unsigned int t(frame_cfg.chan_desc[i].transition_mode[j]);t<frame_cfg.subsub_count;t++) {
						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;

						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;
					}

				} else {
					for (unsigned int t(0);t<frame_cfg.subsub_count;t++) {
						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;

						channel_ptr[0] = channel_ptr[1] = scale_val;
						channel_ptr[2] = step_size;
						channel_ptr[3] = scale_adjust;

						channel_ptr += 8;
					}

				}
			} else {
				channel_ptr += 2*frame_cfg.subsub_count*8;
			}
		}





		for (unsigned int j(frame_cfg.chan_desc[i].hfvq_subband_start);j<frame_cfg.chan_desc[i].activ_subband_count;j++) {
			switch (frame_cfg.chan_desc[i].scale_factor_cb) {
				case 7: // invalid

				break;
				case 6: // 7-bit
					scale_fac_ptr = decoder_cfg.GetNumber(7);
					scale_val = scale_fac7[scale_fac_ptr];
				break;
				case 5: // 6-bit
					scale_fac_ptr = decoder_cfg.GetNumber(6);
					scale_val = scale_fac6[scale_fac_ptr];
				break;
				default:
					scale_fac_ptr += decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9)]<<1), hts_cfg[4*(frame_cfg.chan_desc[i].scale_factor_cb + 9) + 1]);
					scale_val = scale_fac6[scale_fac_ptr];

			}

			step_size = 0x3D800000; // why ????????????????? motherfuckers
			scale_adjust = 0x3F800000;

			for (unsigned int t(0);t<frame_cfg.subsub_count;t++) {
				channel_ptr[0] = channel_ptr[1] = scale_val;
				channel_ptr[2] = step_size;
				channel_ptr[3] = scale_adjust;

				channel_ptr += 8;

				channel_ptr[0] = channel_ptr[1] = scale_val;
				channel_ptr[2] = step_size;
				channel_ptr[3] = scale_adjust;

				channel_ptr += 8;
			}
		}
	}





	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		if (frame_cfg.chan_desc[i].joint_intensity_idx) {
			frame_cfg.chan_desc[i].joint_ii_cb = decoder_cfg.GetNumber(3);
		}
	}


	scale_fac_ptr = 0;
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		if (frame_cfg.chan_desc[i].joint_intensity_idx) {			
			switch (frame_cfg.chan_desc[i].joint_ii_cb) {
				case 7: // invalid

				break;
				case 6: // 7-bit
					for (unsigned int j(frame_cfg.chan_desc[i].activ_subband_count);j<frame_cfg.chan_desc[frame_cfg.chan_desc[i].joint_intensity_idx - 1].activ_subband_count;j++) {
						frame_cfg.chan_desc[i].joint_scale_factor[j] = scale_facJ[frame_cfg.chan_desc[i].joint_scale_factor[j] = decoder_cfg.GetNumber(7) + 64];
					}
				break;
				case 5: // 6-bit
					for (unsigned int j(frame_cfg.chan_desc[i].activ_subband_count);j<frame_cfg.chan_desc[frame_cfg.chan_desc[i].joint_intensity_idx - 1].activ_subband_count;j++) {
						frame_cfg.chan_desc[i].joint_scale_factor[j] = scale_facJ[decoder_cfg.GetNumber(6) + 64];
					}
				break;
				default:
					for (unsigned int j(frame_cfg.chan_desc[i].activ_subband_count);j<frame_cfg.chan_desc[frame_cfg.chan_desc[i].joint_intensity_idx - 1].activ_subband_count;j++) {
						frame_cfg.chan_desc[i].joint_scale_factor[j] = scale_facJ[decoder_cfg.GetHuffmaVal(scale_huffman_tab + (hts_cfg[4*(frame_cfg.chan_desc[i].joint_ii_cb + 9)]<<1), hts_cfg[4*(frame_cfg.chan_desc[i].joint_ii_cb + 9) + 1]) + 64];

					}


			}
		}
	}

	frame_cfg.range_val = bps_range[7]; // bs_hdr.pcm_idx motherfuckers
	if (bs_hdr.ed_range_flag) {
		frame_cfg.range_val += range_tab[decoder_cfg.GetNumber(8)][1] - 0x3F800000;
	}

	if (bs_hdr.crc_present) {
		decoder_cfg.GetNumber(16);
	}
}


void Audio::DTS::GetSubFrame() {
	unsigned int sam_ptr(0), q_cb(0);
	int * channel_ptr(0), vec_val(0);
	
	
	GetSideInfo();
	
// hf vq
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		channel_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE + frame_cfg.chan_desc[i].hfvq_subband_start*2*8*frame_cfg.subsub_count);

		for (unsigned int j(frame_cfg.chan_desc[i].hfvq_subband_start);j<frame_cfg.chan_desc[i].activ_subband_count;j++) {
			sam_ptr = decoder_cfg.GetNumber(10);

			for (unsigned int t(0);t<frame_cfg.subsub_count;t++) {
				channel_ptr[0] = hf_VQ[sam_ptr][t*8 + 0];
				channel_ptr[1] = hf_VQ[sam_ptr][t*8 + 1];
				channel_ptr[2] = hf_VQ[sam_ptr][t*8 + 2];
				channel_ptr[3] = hf_VQ[sam_ptr][t*8 + 3];

				channel_ptr += 8;
				
				channel_ptr[0] = hf_VQ[sam_ptr][t*8 + 4];
				channel_ptr[1] = hf_VQ[sam_ptr][t*8 + 5];
				channel_ptr[2] = hf_VQ[sam_ptr][t*8 + 6];
				channel_ptr[3] = hf_VQ[sam_ptr][t*8 + 7];

				channel_ptr += 8;
			}
		}
	}

	if (bs_hdr.lfe_present) {
		channel_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + bs_hdr.full_chan_count*DTS_CHANNEL_SIZE);

		for (unsigned int i(0);i<2*bs_hdr.lfe_present*frame_cfg.subsub_count;i++) {
			channel_ptr[i + 4] = lfe_val_tab[decoder_cfg.GetNumber(8)];
		}

		channel_ptr[0] = channel_ptr[1] = channel_ptr[2] = channel_ptr[3] = scale_fac7[decoder_cfg.GetNumber(8) + 128];

	}



	for (unsigned int s(0);s<frame_cfg.subsub_count;s++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			channel_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE + s*8*2);

			for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++, channel_ptr += frame_cfg.subsub_count*2*8) {

				if (frame_cfg.chan_desc[i].allocation_index[j]) {
					if (sam_ptr = sbook_cfg[frame_cfg.chan_desc[i].allocation_index[j]][0]) {			
						q_cb = frame_cfg.chan_desc[i].quantization_index_cb[frame_cfg.chan_desc[i].allocation_index[j]];

						if (sbook_cfg[frame_cfg.chan_desc[i].allocation_index[j]][1] > q_cb) {
							q_cb += sam_ptr;

							channel_ptr[0] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[1] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[2] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[3] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							
						

							channel_ptr[8] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[9] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[10] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							channel_ptr[11] = decoder_cfg.GetHuffmaVal(sample_huffman_tab + (hti_cfg[q_cb][0] << 1), hti_cfg[q_cb][1]);
							

						} else {
							if (hti_cfg[sam_ptr][2]) {
								// vector
								GetVector(channel_ptr, decoder_cfg.GetNumber(hti_cfg[sam_ptr][2]), hti_cfg[sam_ptr][1]);
								GetVector(channel_ptr + 8, decoder_cfg.GetNumber(hti_cfg[sam_ptr][2]), hti_cfg[sam_ptr][1]);

							} else {
								sam_ptr = -2;
							}
						}
						
					} else {
						if (sbook_cfg[frame_cfg.chan_desc[i].allocation_index[j]][3]) {
							sam_ptr = -2;
						} else {
							sam_ptr = 'fuck';
						}
					}

					if (sam_ptr == -2) {
						// NFE
						sam_ptr = (35 - frame_cfg.chan_desc[i].allocation_index[j]);
						vec_val = frame_cfg.chan_desc[i].allocation_index[j] - 3;

						channel_ptr[0] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[0] >>= sam_ptr;
						channel_ptr[1] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[1] >>= sam_ptr;
						channel_ptr[2] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[2] >>= sam_ptr;
						channel_ptr[3] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[3] >>= sam_ptr;


						channel_ptr[8] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[8] >>= sam_ptr;
						channel_ptr[9] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[9] >>= sam_ptr;
						channel_ptr[10] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[10] >>= sam_ptr;
						channel_ptr[11] = decoder_cfg.GetNumber(vec_val) << sam_ptr;
						channel_ptr[11] >>= sam_ptr;


					}
				} else {
					sam_ptr = 'fuck';
				}
			}
		}

		if ((s == (frame_cfg.subsub_count - 1)) || (bs_hdr.async_wi_flag)) {
			if (decoder_cfg.GetNumber(16) ^ 0x0FFFF) {
				sam_ptr = 'fuck';
				break;
			}
		}
	}
}


// ===========================================================================================================================================================================================================
const unsigned int Audio::DTS::scale_adjustment[4] = { 0x3F800000, 0x3F900000, 0x3FA00000, 0x3FB80000 };

const unsigned int Audio::DTS::bps_range[] = {0x38000000, 0x38000000, 0x3600000, 0x36000000, 0, 0x34000000, 0x34000000, 0x37AAAAAA};

const unsigned int Audio::DTS::scale_fac6[] = {
	0x3F800000, 0x40000000, 0x40000000, 0x40400000, 0x40400000, 0x40800000, 0x40C00000, 0x40E00000, 0x41200000, 0x41400000, 0x41800000, 0x41A00000, 0x41D00000, 0x42080000, 0x42300000, 0x42600000,
	0x42900000, 0x42BA0000, 0x42F00000, 0x431B0000, 0x43480000, 0x43808000, 0x43A58000, 0x43D58000, 0x44098000, 0x44310000, 0x44640000, 0x4492E000, 0x44BD4000, 0x44F3C000, 0x451D0000, 0x454A4000,
	0x45824800, 0x45A7D000, 0x45D83000, 0x460B4400, 0x46336800, 0x46671C00, 0x4694DE00, 0x46BFC600, 0x46F70E00, 0x471F2200, 0x474D0100, 0x47840C00, 0x47AA1C00, 0x47DB2500, 0x480D2800, 0x4835D840,
	0x486A42C0, 0x4896E4C0, 0x48C26360, 0x48FA6BA0, 0x49214D50, 0x494FCC20, 0x4985D8F0, 0x49AC6DD0, 0x4A0F1494, 0x4A3852C0, 0x4A6D744C, 0x4A98F340, 0x4AC509BC, 0x4AFDD58C, 0x00000000, 0x00000000
};

const unsigned int Audio::DTS::scale_fac7[] = {
	0x3F800000, 0x3F800000, 0x40000000, 0x40000000, 0x40000000, 0x40000000, 0x40400000, 0x40400000, 0x40400000, 0x40800000, 0x40800000, 0x40A00000, 0x40C00000, 0x40E00000, 0x40E00000, 0x41000000,
	0x41200000, 0x41300000, 0x41400000, 0x41600000, 0x41800000, 0x41900000, 0x41A00000, 0x41B80000, 0x41D00000, 0x41F00000, 0x42080000, 0x42180000, 0x42300000, 0x42480000, 0x42600000, 0x42800000,
	0x42900000, 0x42A40000, 0x42BA0000, 0x42D40000, 0x42F00000, 0x43080000, 0x431B0000, 0x43300000, 0x43480000, 0x43620000, 0x43808000, 0x43920000, 0x43A58000, 0x43BC0000, 0x43D58000, 0x43F20000,
	0x44098000, 0x441C0000, 0x44310000, 0x44490000, 0x44640000, 0x44816000, 0x4492E000, 0x44A6C000, 0x44BD4000, 0x44D6C000, 0x44F3C000, 0x450A5000, 0x451D0000, 0x45323000, 0x454A4000, 0x45659000,
	0x45824800, 0x4593E000, 0x45A7D000, 0x45BE7800, 0x45D83000, 0x45F56000, 0x460B4400, 0x461E1000, 0x46336800, 0x464BA000, 0x46671C00, 0x46832800, 0x4694DE00, 0x46A8F600, 0x46BFC600, 0x46D9AA00,
	0x46F70E00, 0x470C3400, 0x471F2200, 0x47349E00, 0x474D0100, 0x4768AE00, 0x47840C00, 0x4795E000, 0x47AA1C00, 0x47C11380, 0x47DB2500, 0x47F8BB00, 0x480D2800, 0x482036C0, 0x4835D840, 0x484E6540,
	0x486A42C0, 0x4884F1C0, 0x4896E4C0, 0x48AB4400, 0x48C26360, 0x48DCA200, 0x48FA6BA0, 0x490E1D70, 0x49214D50, 0x49371460, 0x494FCC20, 0x496BDA30, 0x4985D8F0, 0x4997EB18, 0x49AC6DD0, 0x49C3B568,
	0x49DE21A8, 0x49FC1F20, 0x4A0F1494, 0x4A2265D4, 0x4A3852C0, 0x4A513578, 0x4A6D744C, 0x4A86C1B0, 0x4A98F340, 0x4AAD99A2, 0x4AC509BC, 0x4ADFA3EA, 0x4AFDD58C, 0x00000000, 0x00000000, 0x00000000,


	0x3D0F5C29, 0x3D0F5C29, 0x3D8F5C29, 0x3D8F5C29, 0x3D8F5C29, 0x3D8F5C29, 0x3DD70A3E, 0x3DD70A3E, 0x3DD70A3E, 0x3E0F5C29, 0x3E0F5C29, 0x3E333333, 0x3E570A3E, 0x3E7AE148, 0x3E7AE148, 0x3E8F5C29,
	0x3EB33333, 0x3EC51EB8, 0x3ED70A3E, 0x3EFAE148, 0x3F0F5C29, 0x3F2147AE, 0x3F333333, 0x3F4E147B, 0x3F68F5C3, 0x3F866666, 0x3F9851EC, 0x3FAA3D71, 0x3FC51EB8, 0x3FE00000, 0x3FFAE148, 0x400F5C29,
	0x402147AE, 0x4037AE15, 0x405051EC, 0x406D70A4, 0x40866666, 0x409851EC, 0x40AD999A, 0x40C51EB8, 0x40E00000, 0x40FD1EB8, 0x410FEB85, 0x4123851F, 0x41395C29, 0x41528F5C, 0x416F1EB8, 0x4187851F,
	0x419A0000, 0x41AEB852, 0x41C63D71, 0x41E11EB8, 0x41FF5C29, 0x4210E666, 0x42248000, 0x423AC28F, 0x4253F5C3, 0x4270851F, 0x42888000, 0x429AE8F6, 0x42AFD70A, 0x42C791EC, 0x42E2851F, 0x43008E15,
	0x4311EA3D, 0x43259EB8, 0x433BF333, 0x43555333, 0x43722148, 0x438968F6, 0x439BFA3D, 0x43B107AE, 0x43C8EF5C, 0x43E40F5C, 0x44016BD7, 0x4412E51F, 0x4426BB33, 0x443D3C7B, 0x4456C948, 0x4473C8A4,
	0x448A59C3, 0x449D070A, 0x44B23A8F, 0x44CA4A8F, 0x44E59AB8, 0x45024CF6, 0x4513E47B, 0x4527DC29, 0x453E85C3, 0x45583ECD, 0x4575711F, 0x458B4A00, 0x459E1852, 0x45B37085, 0x45CBAA85, 0x45E729B8,
	0x46032F9F, 0x4614E5CD, 0x46290033, 0x463FD148, 0x4659B6FB, 0x46771BD7, 0x468C3C45, 0x469F2B36, 0x46B4A883, 0x46CD0C95, 0x46E8BBA9, 0x470413C9, 0x4715E8BB, 0x472A2606, 0x47411ED5, 0x475B3193,
	0x4778C989, 0x478D3026, 0x47A04002, 0x47B5E2B0, 0x47CE7129, 0x47EA505E, 0x4804F972, 0x4816ED69, 0x482B4DE1, 0x48426EA1, 0x485CAEBE, 0x487A7A25, 0x488E25AB, 0x00000000, 0x00000000, 0x00000000
};

const unsigned int Audio::DTS::scale_facJ[] = {
	0x3CCD8559, 0x3CDA1A93, 0x3CE6AFCD, 0x3CF45176, 0x3D017FC7, 0x3D0919F0, 0x3D113A50, 0x3D19E0E7, 0x3D230DB7, 0x3D2CC0BE, 0x3D36F9FD, 0x3D41B973, 0x3D4D423E, 0x3D59945B, 0x3D666CB1, 0x3D740E5A,
	0x3D813CAC, 0x3D88D6D4, 0x3D90F734, 0x3D999DCB, 0x3DA2A90D, 0x3DAC5C14, 0x3DB69553, 0x3DC154CA, 0x3DCCBC06, 0x3DD8EC96, 0x3DE5C4EB, 0x3DF36695, 0x3E00E8C9, 0x3E0893B8, 0x3E10A351, 0x3E193922,
	0x3E224463, 0x3E2BE6A3, 0x3E361FE2, 0x3E40DF59, 0x3E4C575C, 0x3E58665E, 0x3E653EB4, 0x3E72CF96, 0x3E809D49, 0x3E883772, 0x3E904F6E, 0x3E98DCDB, 0x3EA1E81D, 0x3EAB81F9, 0x3EB5AA71, 0x3EC069E8,
	0x3ECBD124, 0x3ED7E88A, 0x3EE4B018, 0x3EF240FB, 0x3F004D98, 0x3F07E7C0, 0x3F0FF759, 0x3F187C63, 0x3F2187A5, 0x3F2B191E, 0x3F353D64, 0x3F3FF8A9, 0x3F4B5782, 0x3F576684, 0x3F6429E1, 0x3F71AE2E,
	0x3F800000, 0x3F8795D5, 0x3F8F9F56, 0x3F98201D, 0x3FA124F2, 0x3FAAB021, 0x3FB4CE1C, 0x3FBF8573, 0x3FCADE01, 0x3FD6E243, 0x3FE39F56, 0x3FF11B71, 0x3FFF64AE, 0x40074318, 0x400F4730, 0x4017C4DB,
	0x4020C27A, 0x402A4990, 0x4034602D, 0x403F10F5, 0x404A6320, 0x40566057, 0x4063150E, 0x407089A0, 0x407EC9B0, 0x4086F156, 0x408EF030, 0x4097689D, 0x40A0617C, 0x40A9E22E, 0x40B3F30E, 0x40BE9C8D,
	0x40C9E7FF, 0x40D5DEE8, 0x40E28B2F, 0x40EFF751, 0x40FE2F83, 0x41069F8A, 0x410E99A4, 0x41170CDD, 0x41200000, 0x41297AE1, 0x413385F0, 0x413E28F6, 0x41496D5D, 0x41555CFB, 0x416201A3, 0x416F65FE,
	0x417D9581, 0x41864DD3, 0x418E432D, 0x4196B127, 0x419F9EED, 0x41A91446, 0x41B318FC, 0x41BDB5A8, 0x41C8F34D, 0x41D4DB8C, 0x41E1786C, 0x41EED495, 0x41FCFB7F, 0x4205FC6A, 0x420DECDA, 0x421655B5,
	0x421F3E28
};

const unsigned int Audio::DTS::q_step_size[2][32] = {
	{0x00000000, 0x3FCCCCCD, 0x3F800000, 0x3F4CCCCD, 0x3F170A3D, 0x3F000000, 0x3ED70A3D, 0x3EAE147B, 0x3E428F5C, 0x3DE147AE, 0x3D75C28F, 0x3D0F5C29, 0x3C9BA5E3, 0x3C343958, 0x3BD4FDF4, 0x3B83126F, 0x3B23D70A, 0x3AB78034, 0x3A51B717, 0x39EBEDFA, 0x399D4952, 0x39324207, 0x38A7C5AC, 0x3827C5AC, 0x37A7C5AC, 0x3727C5AC, 0x36A7C5AC, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
	{0x00000000, 0x3F800000, 0x3F000000, 0x3EA8F5C0, 0x3E800000, 0x3E29FBE0, 0x3E000000, 0x3DA9FBE0, 0x3D800000, 0x3D000000, 0x3C7F9700, 0x3C010200, 0x3B808400, 0x3B004000, 0x3A802000, 0x3A001000, 0x39800000, 0x39000000, 0x38800000, 0x38000000, 0x37800000, 0x36700000, 0x36800000, 0x35600000, 0x35800000, 0x35000000, 0x34800000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}
};

const unsigned int Audio::DTS::range_tab[][2] = {

	{0x39D3C7E9, 0x3CD42C3D}, {0x39D9F7AE, 0x3CD9E83E}, {0x39E05515, 0x3CE075F7}, {0x39E6E176, 0x3CE703B0}, {0x39ED9F80, 0x3CED9168}, {0x39F4908B, 0x3CF4F0D8}, {0x39FBB497, 0x3CFB7E91}, {0x3A018729, 0x3D016F00}, {0x3A054EDF, 0x3D051EB8}, {0x3A093370, 0x3D09374C}, {0x3A0D3588, 0x3D0D4FDF}, {0x3A115527, 0x3D116873}, {0x3A1592FA, 0x3D158106}, {0x3A19F1AE, 0x3D1A0275}, {0x3A1E6FED, 0x3D1E83E4}, {0x3A231066, 0x3D230553}, 
	{0x3A27D3C4, 0x3D27EF9E}, {0x3A2CBA08, 0x3D2CD9E8}, {0x3A31C534, 0x3D31C433}, {0x3A36F5F6, 0x3D371759}, {0x3A3C4DA4, 0x3D3C6A7F}, {0x3A41CD95, 0x3D41BDA5}, {0x3A477677, 0x3D4779A7}, {0x3A4D499F, 0x3D4D35A8}, {0x3A5347BB, 0x3D535A86}, {0x3A597379, 0x3D597F63}, {0x3A5FCCDA, 0x3D5FA440}, {0x3A6655E0, 0x3D6631F9}, {0x3A6D0FE3, 0x3D6D288D}, {0x3A73FB90, 0x3D741F21}, {0x3A7B1B95, 0x3D7B15B5}, {0x3A81384F, 0x3D813A93}, 
	{0x3A84FE57, 0x3D84EA4B}, {0x3A88E08F, 0x3D88CE70}, {0x3A8CDFA2, 0x3D8CE704}, {0x3A90FCE8, 0x3D90FF97}, {0x3A9538B7, 0x3D954C98}, {0x3A999410, 0x3D99999A}, {0x3A9E0FF6, 0x3D9E1B09}, {0x3AA2ADBF, 0x3DA29C78}, {0x3AA76DC3, 0x3DA786C2}, {0x3AAC5157, 0x3DAC3C9F}, {0x3AB1597F, 0x3DB15B57}, {0x3AB6873B, 0x3DB67A10}, {0x3ABBDB8E, 0x3DBBCD36}, {0x3AC15825, 0x3DC154CA}, {0x3AC6FD55, 0x3DC710CB}, {0x3ACCCCCD, 0x3DCCCCCD}, 
	{0x3AD2C7E4, 0x3DD2BD3C}, {0x3AD8EF45, 0x3DD8E219}, {0x3ADF44F4, 0x3DDF3B64}, {0x3AE5C9F4, 0x3DE5C91D}, {0x3AEC7FF0, 0x3DEC8B44}, {0x3AF36796, 0x3DF381D8}, {0x3AFA833E, 0x3DFA786C}, {0x3B00E9F5, 0x3E00EBEE}, {0x3B04ADA4, 0x3E04B5DD}, {0x3B088D83, 0x3E089A02}, {0x3B0C8A3D, 0x3E0C7E28}, {0x3B10A4D3, 0x3E10B0F2}, {0x3B14DE1E, 0x3E14E3BD}, {0x3B1936F3, 0x3E1930BE}, {0x3B1DB055, 0x3E1DB22D}, {0x3B224B19, 0x3E224DD3}, 
	{0x3B270842, 0x3E2703B0}, {0x3B2BE8D2, 0x3E2BEDFA}, {0x3B30EDF4, 0x3E30F27C}, {0x3B361880, 0x3E361134}, {0x3B3B69CE, 0x3E3B645A}, {0x3B40E2B4, 0x3E40EBEE}, {0x3B468489, 0x3E468DB9}, {0x3B4C50A6, 0x3E4C49BA}, {0x3B5247E1, 0x3E525461}, {0x3B586BE7, 0x3E585F07}, {0x3B5EBDBB, 0x3E5EB852}, {0x3B653EB4, 0x3E65460B}, {0x3B6BF07F, 0x3E6BEDFA}, {0x3B72D41D, 0x3E72CA58}, {0x3B79EB69, 0x3E79F55A}, {0x3B809BC7, 0x3E809D49}, 
	{0x3B845D32, 0x3E845A1D}, {0x3B883AA2, 0x3E883E42}, {0x3B8C3502, 0x3E8C2F83}, {0x3B904D15, 0x3E904817}, {0x3B9483C6, 0x3E9487FD}, {0x3B98DA01, 0x3E98D4FE}, {0x3B9D509E, 0x3E9D566D}, {0x3BA1E89E, 0x3EA1E4F7}, {0x3BA6A2EC, 0x3EA6A7F0}, {0x3BAB808C, 0x3EAB851F}, {0x3BB082AA, 0x3EB07C85}, {0x3BB5AA1B, 0x3EB5A858}, {0x3BBAF824, 0x3EBAFB7F}, {0x3BC06DC4, 0x3EC068DC}, {0x3BC60C3E, 0x3EC60AA6}, {0x3BCBD4AA, 0x3ECBD3C3}, 
	{0x3BD1C85F, 0x3ED1C433}, {0x3BD7E89F, 0x3ED7E910}, {0x3BDE3697, 0x3EDE353F}, {0x3BE4B3B4, 0x3EE4B5DD}, {0x3BEB614D, 0x3EEB5DCC}, {0x3BF240E5, 0x3EF24745}, {0x3BF953D4, 0x3EF95810}, {0x3C004DCE, 0x3F004EA5}, {0x3C040CEA, 0x3F040B78}, {0x3C07E80C, 0x3F07E910}, {0x3C0BDFFD, 0x3F0BE0DF}, {0x3C0FF596, 0x3F0FF2E5}, {0x3C1429C3, 0x3F142C3D}, {0x3C187D4F, 0x3F187FCC}, {0x3C1CF13D, 0x3F1CF41F}, {0x3C21866D, 0x3F218937}, 
	{0x3C263DE2, 0x3F263F14}, {0x3C2B1892, 0x3F2B15B5}, {0x3C301795, 0x3F301A37}, {0x3C353BEC, 0x3F3538EF}, {0x3C3A86BA, 0x3F3A8588}, {0x3C3FF914, 0x3F3FF972}, {0x3C45941E, 0x3F4594AF}, {0x3C4B591A, 0x3F4B573F}, {0x3C514933, 0x3F5147AE}, {0x3C5765AD, 0x3F5765FE}, {0x3C5DAFD4, 0x3F5DB22D}, {0x3C64290A, 0x3F642C3D}, {0x3C6AD29C, 0x3F6AD42C}, {0x3C71ADF8, 0x3F71B08A}, {0x3C78BCA0, 0x3F78BAC7}, {0x3C800000, 0x3F800000}, 
	{0x3C83BCD9, 0x3F83BCD3}, {0x3C8795A1, 0x3F879724}, {0x3C8B8B29, 0x3F8B8BAC}, {0x3C8F9E4E, 0x3F8F9DB2}, {0x3C93CFE6, 0x3F93D07D}, {0x3C9820D9, 0x3F9820C5}, {0x3C9C920C, 0x3F9C91D1}, {0x3CA12478, 0x3FA123A3}, {0x3CA5D90D, 0x3FA5D97F}, {0x3CAAB0D3, 0x3FAAB021}, {0x3CAFACCC, 0x3FAFAE14}, {0x3CB4CE08, 0x3FB4CCCD}, {0x3CBA159B, 0x3FBA161E}, {0x3CBF84A5, 0x3FBF837B}, {0x3CC51C4F, 0x3FC51B71}, {0x3CCADDCA, 0x3FCADE01}, 
	{0x3CD0CA48, 0x3FD0CB29}, {0x3CD6E30B, 0x3FD6E2EB}, {0x3CDD2966, 0x3FDD288D}, {0x3CE39EAB, 0x3FE39F56}, {0x3CEA4431, 0x3FEA43FE}, {0x3CF11B6B, 0x3FF119CE}, {0x3CF825C6, 0x3FF82752}, {0x3CFF64BF, 0x3FFF65FE}, {0x3D036CF5, 0x40036C8B}, {0x3D074369, 0x40074396}, {0x3D0B368A, 0x400B367A}, {0x3D0F4736, 0x400F46DC}, {0x3D137642, 0x40137660}, {0x3D17C495, 0x4017C505}, {0x3D1C331A, 0x401C32CA}, {0x3D20C2C1, 0x4020C2F8}, 
	{0x3D25747C, 0x402573EB}, {0x3D2A4952, 0x402A48E9}, {0x3D2F4245, 0x402F41F2}, {0x3D346062, 0x403460AA}, {0x3D39A4C2, 0x4039A512}, {0x3D3F1081, 0x403F10CB}, {0x3D44A4C8, 0x4044A57A}, {0x3D4A62C2, 0x404A6320}, {0x3D504BA8, 0x40504B5E}, {0x3D5660BD, 0x4056617C}, {0x3D5CA349, 0x405CA3D7}, {0x3D6314A0, 0x40631412}, {0x3D69B622, 0x4069B574}, {0x3D708932, 0x407089A0}, {0x3D778F49, 0x40778EF3}, {0x3D7EC9E2, 0x407ECA58}, 
	{0x3D831D41, 0x40831D15}, {0x3D86F161, 0x4086F1AA}, {0x3D8AE21E, 0x408AE219}, {0x3D8EF052, 0x408EF007}, {0x3D931CD5, 0x40931D15}, {0x3D97688D, 0x40976873}, {0x3D9BD462, 0x409BD495}, {0x3DA06143, 0x40A0617C}, {0x3DA51026, 0x40A50FF9}, {0x3DA9E20D, 0x40A9E1B1}, {0x3DAED7FD, 0x40AED845}, {0x3DB3F300, 0x40B3F2E5}, {0x3DB9342F, 0x40B93405}, {0x3DBE9CA5, 0x40BE9C78}, {0x3DC42D88, 0x40C42DE0}, {0x3DC9E807, 0x40C9E83E}, 
	{0x3DCFCD58, 0x40CFCD36}, {0x3DD5DEBC, 0x40D5DE6A}, {0x3DDC1D7C, 0x40DC1D7E}, {0x3DE28AEB, 0x40E28B44}, {0x3DE92867, 0x40E9288D}, {0x3DEFF755, 0x40EFF6FD}, {0x3DF6F929, 0x40F6F909}, {0x3DFE2F5F, 0x40FE2F83}, {0x3E02CDBE, 0x4102CD9F}, {0x3E069F8B, 0x41069FBE}, {0x3E0A8DE6, 0x410A8DB9}, {0x3E0E99A3, 0x410E999A}, {0x3E12C39F, 0x4112C3CA}, {0x3E170CBC, 0x41170CB3}, {0x3E1B75E2, 0x411B75F7}, {0x3E200000, 0x41200000}, 
	{0x3E24AC0E, 0x4124AC08}, {0x3E297B08, 0x41297AE1}, {0x3E2E6DF5, 0x412E6DC6}, {0x3E3385E0, 0x413385F0}, {0x3E38C3DF, 0x4138C3CA}, {0x3E3E290D, 0x413E28F6}, {0x3E43B690, 0x4143B6AE}, {0x3E496D96, 0x41496DC6}, {0x3E4F4F54, 0x414F4F76}, {0x3E555D09, 0x41555CFB}, {0x3E5B9801, 0x415B97F6}, {0x3E62018A, 0x416201A3}, {0x3E689B02, 0x41689AD4}, {0x3E6F65D0, 0x416F65FE}, {0x3E766364, 0x41766388}, {0x3E7D953A, 0x417D9518}, 
	{0x3E827E6C, 0x41827E5D}, {0x3E864DE8, 0x41864DD3}, {0x3E8A39E0, 0x418A39F5}, {0x3E8E432A, 0x418E432D}, {0x3E926A9F, 0x41926AB3}, {0x3E96B122, 0x4196B127}, {0x3E9B179B, 0x419B178D}, {0x3E9F9EF9, 0x419F9EED}, {0x3EA44831, 0x41A4484B}, {0x3EA91441, 0x41A91446}, {0x3EAE042E, 0x41AE0419}, {0x3EB31902, 0x41B318FC}, {0x3EB853D3, 0x41B853C3}, {0x3EBDB5BC, 0x41BDB5A8}, {0x3EC33FE1, 0x41C33FE6}, {0x3EC8F36F, 0x41C8F382}, 
	{0x3ECED19C, 0x41CED183}, {0x3ED4DBA6, 0x41D4DBC0}, {0x3EDB12D6, 0x41DB12D7}, {0x3EE1787B, 0x41E1786C}, {0x3EE80DF4, 0x41E80DED}, {0x3EEED4A3, 0x41EED495}, {0x3EF5CDFA, 0x41F5CE07}, {0x3EFCFB72, 0x41FCFB7F}, {0x3F022F49, 0x42022F4F}, {0x3F05FC76, 0x4205FC6A}, {0x3F09E60D, 0x4209E618}, {0x3F0DECE4, 0x420DECDA}, {0x3F1211D5, 0x421211D1}, {0x3F1655C0, 0x421655B5}, {0x3F1AB98E, 0x421AB98C}, {0x3F1F3E2D, 0x421F3E28}

};


const unsigned int Audio::DTS::lfe_val_tab[] = {
	0x00000000, 0x3F800000, 0x40000000, 0x40400000, 0x40800000, 0x40A00000, 0x40C00000, 0x40E00000, 0x41000000, 0x41100000, 0x41200000, 0x41300000, 0x41400000, 0x41500000, 0x41600000, 0x41700000,
	0x41800000, 0x41880000, 0x41900000, 0x41980000, 0x41A00000, 0x41A80000, 0x41B00000, 0x41B80000, 0x41C00000, 0x41C80000, 0x41D00000, 0x41D80000, 0x41E00000, 0x41E80000, 0x41F00000, 0x41F80000,
	0x42000000, 0x42040000, 0x42080000, 0x420C0000, 0x42100000, 0x42140000, 0x42180000, 0x421C0000, 0x42200000, 0x42240000, 0x42280000, 0x422C0000, 0x42300000, 0x42340000, 0x42380000, 0x423C0000,
	0x42400000, 0x42440000, 0x42480000, 0x424C0000, 0x42500000, 0x42540000, 0x42580000, 0x425C0000, 0x42600000, 0x42640000, 0x42680000, 0x426C0000, 0x42700000, 0x42740000, 0x42780000, 0x427C0000,
	0x42800000, 0x42820000, 0x42840000, 0x42860000, 0x42880000, 0x428A0000, 0x428C0000, 0x428E0000, 0x42900000, 0x42920000, 0x42940000, 0x42960000, 0x42980000, 0x429A0000, 0x429C0000, 0x429E0000,
	0x42A00000, 0x42A20000, 0x42A40000, 0x42A60000, 0x42A80000, 0x42AA0000, 0x42AC0000, 0x42AE0000, 0x42B00000, 0x42B20000, 0x42B40000, 0x42B60000, 0x42B80000, 0x42BA0000, 0x42BC0000, 0x42BE0000,
	0x42C00000, 0x42C20000, 0x42C40000, 0x42C60000, 0x42C80000, 0x42CA0000, 0x42CC0000, 0x42CE0000, 0x42D00000, 0x42D20000, 0x42D40000, 0x42D60000, 0x42D80000, 0x42DA0000, 0x42DC0000, 0x42DE0000,
	0x42E00000, 0x42E20000, 0x42E40000, 0x42E60000, 0x42E80000, 0x42EA0000, 0x42EC0000, 0x42EE0000, 0x42F00000, 0x42F20000, 0x42F40000, 0x42F60000, 0x42F80000, 0x42FA0000, 0x42FC0000, 0x42FE0000,
	0xC3000000, 0xC2FE0000, 0xC2FC0000, 0xC2FA0000, 0xC2F80000, 0xC2F60000, 0xC2F40000, 0xC2F20000, 0xC2F00000, 0xC2EE0000, 0xC2EC0000, 0xC2EA0000, 0xC2E80000, 0xC2E60000, 0xC2E40000, 0xC2E20000,
	0xC2E00000, 0xC2DE0000, 0xC2DC0000, 0xC2DA0000, 0xC2D80000, 0xC2D60000, 0xC2D40000, 0xC2D20000, 0xC2D00000, 0xC2CE0000, 0xC2CC0000, 0xC2CA0000, 0xC2C80000, 0xC2C60000, 0xC2C40000, 0xC2C20000,
	0xC2C00000, 0xC2BE0000, 0xC2BC0000, 0xC2BA0000, 0xC2B80000, 0xC2B60000, 0xC2B40000, 0xC2B20000, 0xC2B00000, 0xC2AE0000, 0xC2AC0000, 0xC2AA0000, 0xC2A80000, 0xC2A60000, 0xC2A40000, 0xC2A20000,
	0xC2A00000, 0xC29E0000, 0xC29C0000, 0xC29A0000, 0xC2980000, 0xC2960000, 0xC2940000, 0xC2920000, 0xC2900000, 0xC28E0000, 0xC28C0000, 0xC28A0000, 0xC2880000, 0xC2860000, 0xC2840000, 0xC2820000,
	0xC2800000, 0xC27C0000, 0xC2780000, 0xC2740000, 0xC2700000, 0xC26C0000, 0xC2680000, 0xC2640000, 0xC2600000, 0xC25C0000, 0xC2580000, 0xC2540000, 0xC2500000, 0xC24C0000, 0xC2480000, 0xC2440000,
	0xC2400000, 0xC23C0000, 0xC2380000, 0xC2340000, 0xC2300000, 0xC22C0000, 0xC2280000, 0xC2240000, 0xC2200000, 0xC21C0000, 0xC2180000, 0xC2140000, 0xC2100000, 0xC20C0000, 0xC2080000, 0xC2040000,
	0xC2000000, 0xC1F80000, 0xC1F00000, 0xC1E80000, 0xC1E00000, 0xC1D80000, 0xC1D00000, 0xC1C80000, 0xC1C00000, 0xC1B80000, 0xC1B00000, 0xC1A80000, 0xC1A00000, 0xC1980000, 0xC1900000, 0xC1880000,
	0xC1800000, 0xC1700000, 0xC1600000, 0xC1500000, 0xC1400000, 0xC1300000, 0xC1200000, 0xC1100000, 0xC1000000, 0xC0E00000, 0xC0C00000, 0xC0A00000, 0xC0800000, 0xC0400000, 0xC0000000, 0xBF800000 

};


