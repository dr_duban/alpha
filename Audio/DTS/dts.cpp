/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\DTS.h"

#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"


__declspec(align(16)) float Audio::DTS::synth_cos[640];


#define CORE_SUBSTREAM_CORE	0x0001
#define BCCORE_XXCH			0x0002
#define BCCORE_x96			0x0004
#define BCCORE_XCH			0x0008
#define EXSUBSTREAM_CORE	0x0010
#define EXSUBSTREAM_XBR		0x0020
#define EXSUBSTREAM_XXCH	0x0040
#define EXSUBSTREAM_X96		0x0080
#define EXSUBSTREAM_LBR		0x0100
#define EXSUBSTREAM_XLL		0x0200
#define EXSUBSTREAM_RE1		0x0400
#define EXSUBSTREAM_RE2		0x0800

Audio::DTS::DTS() : hd_brand(0) {

	stream_cfg.sdata_offset = DefaultRenderer::frame_cfg_size + 2048;
	stream_cfg.frame_size = DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE;

}


Audio::DTS::~DTS() {

}

UI_64 Audio::DTS::Unpack(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0);


	for (result = DecodeUnit(data_ptr, data_length, time_mark);result == RESULT_DECODE_COMPLETE;result = DecodeUnit(data_ptr, data_length, time_mark)) {
		if (data_length > bs_hdr.frame_size) {
			data_length -= bs_hdr.frame_size;
			data_ptr += bs_hdr.frame_size;
		} else {
			break;
		}
	}

	return result;
}


void Audio::DTS::GetBitStreamHeader() {

	bs_hdr.frame_type = decoder_cfg.NextBit();
	bs_hdr.short_block_sample_count = decoder_cfg.GetNumber(5) + 1;
	bs_hdr.crc_present = decoder_cfg.NextBit(); // crc_present

	bs_hdr.block32_count = decoder_cfg.GetNumber(7) + 1; // this multiplied by 32 gives total number of samples in current frame

	bs_hdr.frame_size = decoder_cfg.GetNumber(14) + 1;


	switch (bs_hdr.arr_mode = decoder_cfg.GetNumber(6)) {
		case 0:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_CENTER;

			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 1;			
		break;
		case 1: // dual mono
		case 2: // stereo
		case 3: // (L+R) (L-R)
		case 4: // LT RT
			bs_hdr.chan_type[0] = SPEAKER_FRONT_LEFT;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_RIGHT;

			bs_hdr.center_present = -1;
			bs_hdr.full_chan_count = 2;

			bs_hdr.surround_mode = bs_hdr.arr_mode;
		break;

		case 5:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_CENTER;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_LEFT;					
			bs_hdr.chan_type[2] = SPEAKER_FRONT_RIGHT;

			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 3;
		break;
		case 6:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_LEFT;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[2] = SPEAKER_BACK_CENTER;

			bs_hdr.center_present = -1;
			bs_hdr.full_chan_count = 3;
		break;
		case 7:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_CENTER;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_LEFT;					
			bs_hdr.chan_type[2] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[3] = SPEAKER_BACK_CENTER;

			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 4;
		break;

		case 8:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_LEFT;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[2] = SPEAKER_SIDE_LEFT;
			bs_hdr.chan_type[3] = SPEAKER_SIDE_RIGHT;

			bs_hdr.center_present = -1;
			bs_hdr.full_chan_count = 4;
		break;
		case 9:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_CENTER;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_LEFT;					
			bs_hdr.chan_type[2] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[3] = SPEAKER_SIDE_LEFT;
			bs_hdr.chan_type[4] = SPEAKER_SIDE_RIGHT;

			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 5;
		break;
		case 10: case 12:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_LEFT;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[2] = SPEAKER_SIDE_LEFT;
			bs_hdr.chan_type[3] = SPEAKER_SIDE_RIGHT;
			bs_hdr.chan_type[4] = SPEAKER_BACK_LEFT;
			bs_hdr.chan_type[5] = SPEAKER_BACK_RIGHT;
					
			bs_hdr.center_present = -1;
			bs_hdr.full_chan_count = 6;
		break;
		case 11:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_CENTER;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_LEFT;					
			bs_hdr.chan_type[2] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[3] = SPEAKER_BACK_LEFT;
			bs_hdr.chan_type[4] = SPEAKER_BACK_RIGHT;
			bs_hdr.chan_type[5] = SPEAKER_BACK_CENTER;
					
			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 6;

		break;
		case 13:
			bs_hdr.chan_type[0] = SPEAKER_FRONT_LEFT;
			bs_hdr.chan_type[1] = SPEAKER_FRONT_CENTER;
			bs_hdr.chan_type[2] = SPEAKER_FRONT_RIGHT;
			bs_hdr.chan_type[3] = SPEAKER_SIDE_LEFT;
			bs_hdr.chan_type[4] = SPEAKER_SIDE_RIGHT;
			bs_hdr.chan_type[5] = SPEAKER_BACK_LEFT;
			bs_hdr.chan_type[6] = SPEAKER_BACK_RIGHT;
					
			bs_hdr.center_present = 1;
			bs_hdr.full_chan_count = 7;

		break;
		// no support of 8 channels
		case 14:

		break;
		case 15:

		break;
	}


	bs_hdr.sf_idx = decoder_cfg.GetNumber(4);
	bs_hdr.sampling_frequency = sampling_frequencies[bs_hdr.sf_idx];

	bs_hdr.br_idx = decoder_cfg.GetNumber(5);

	decoder_cfg.NextBit(); // reserved

	bs_hdr.ed_range_flag = decoder_cfg.NextBit();
	bs_hdr.et_stamp_flag = decoder_cfg.NextBit();

	bs_hdr.aux_data_flag = decoder_cfg.NextBit();

	bs_hdr.hdcd_flag = decoder_cfg.NextBit();

	bs_hdr.ext_audio_id = decoder_cfg.GetNumber(3);
	bs_hdr.ext_audio_flag = decoder_cfg.NextBit();

	bs_hdr.async_wi_flag = decoder_cfg.NextBit();

	if (bs_hdr.lfe_present = decoder_cfg.GetNumber(2)) {
		bs_hdr.chan_type[bs_hdr.full_chan_count] = SPEAKER_LOW_FREQUENCY;
	}

	bs_hdr.predictor_flag = decoder_cfg.NextBit();

	if (bs_hdr.crc_present) {
		decoder_cfg.GetNumber(16); // CRC
	}

	bs_hdr.interpolator_switch = decoder_cfg.NextBit();

	bs_hdr.frame_version = decoder_cfg.GetNumber(4);

	decoder_cfg.GetNumber(2); // CHIST

	bs_hdr.pcm_idx = decoder_cfg.GetNumber(3);

	bs_hdr.front_encoding = decoder_cfg.NextBit();
	bs_hdr.surround_encoding = decoder_cfg.NextBit();
	
	bs_hdr.dialog_norm = decoder_cfg.GetNumber(4);


}

void Audio::DTS::GetCoreAudioHeader() {
	frame_cfg.subframe_count = decoder_cfg.GetNumber(4) + 1;
	frame_cfg.active_chan_count = decoder_cfg.GetNumber(3) + 1;

	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].activ_subband_count = decoder_cfg.GetNumber(5) + 2;
	}
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].hfvq_subband_start = decoder_cfg.GetNumber(5) + 1;
	}
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].joint_intensity_idx = decoder_cfg.GetNumber(3);
	}
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].transient_mode_cb = decoder_cfg.GetNumber(2);
	}
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].scale_factor_cb = decoder_cfg.GetNumber(3);
	}
	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].bit_allocation_cb = decoder_cfg.GetNumber(3);
	}



	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		frame_cfg.chan_desc[i].quantization_index_cb[1] = decoder_cfg.NextBit();		
	}
	for (unsigned int j(2);j<=5;j++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			frame_cfg.chan_desc[i].quantization_index_cb[j] = decoder_cfg.GetNumber(2);			
		}
	}
	for (unsigned int j(6);j<=10;j++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			frame_cfg.chan_desc[i].quantization_index_cb[j] = decoder_cfg.GetNumber(3);			
		}
	}
	for (unsigned int j(11);j<=26;j++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			frame_cfg.chan_desc[i].quantization_index_cb[j] = 0;			
		}
	}

	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		for (unsigned int j(0);j<32;j++) {
			frame_cfg.chan_desc[i].scale_factor_adjustment[j] = 0;
		}
	}


	for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
		if (frame_cfg.chan_desc[i].quantization_index_cb[1] == 0) {
			frame_cfg.chan_desc[i].scale_factor_adjustment[1] = decoder_cfg.GetNumber(2);
		}
	}
	for (unsigned int j(2);j<=5;j++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			if (frame_cfg.chan_desc[i].quantization_index_cb[j] < 3) {
				frame_cfg.chan_desc[i].scale_factor_adjustment[j] = decoder_cfg.GetNumber(2);
			}
		}
	}
	for (unsigned int j(6);j<=10;j++) {
		for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
			if (frame_cfg.chan_desc[i].quantization_index_cb[j] < 7) {
				frame_cfg.chan_desc[i].scale_factor_adjustment[j] = decoder_cfg.GetNumber(2);
			}
		}
	}
	
	if (bs_hdr.crc_present) {
		decoder_cfg.GetNumber(16);
	}
}


UI_64 Audio::DTS::DecodeUnit(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0);
	int sync_word(0);
	
	if (!stream_cfg.frame_buff) {
		stream_cfg.frame_buff.New(frame_buf_tid, MAX_FRAME_COUNT*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE)*sizeof(float), SFSco::large_mem_mgr);
	}


	if (data_ptr) decoder_cfg.SetSource(data_ptr, data_length);

	if (decoder_cfg.src_ptr) {
		sync_word = decoder_cfg.GetNumber(32);
//		sync_word <<= 16;
//		sync_word |= decoder_cfg.GetNumber(16);

		switch (sync_word) {
			case 0x02b09261: // substream core
				hd_brand = 1;
			case 0x7FFE8001: // Core
				result = CoreDecode(time_mark);
			break;
/*
			case 0x5A5A5A5A: // XCH

				hd_brand = 1;
			break;
			case 0x47004A03: // XXCH

				hd_brand = 1;
			break;
			case 0x1D95F262: // X96

				hd_brand = 1;
			break;
			case 0x655E315E: // XBR

				hd_brand = 1;
			break;
			case 0x0A801921: // LBR

				hd_brand = 1;
			break;
			case 0x41A29547: // XLL

				hd_brand = 1;
			break;
*/
			case 0x64582025: // substream
				result = SubDecode(time_mark);
				hd_brand = 1;
			break;

		}


	}



	if (result & RESULT_DECODE_COMPLETE) {
		if (stream_cfg.ini_block == 0) {
			if (stream_cfg.ini_block = CreateEvent(0, 0, 0, 0)) {
				mixer_thread = CreateThread(0, 0, Audio::DefaultRenderer::MixerLoop, &stream_cfg, 0, 0);	
				if (stream_cfg.set_complete) WaitForSingleObject(stream_cfg.ini_block, INFINITE);
			}
		}

		if (stream_cfg.flush_sign == -1) stream_cfg.flush_sign = -2;
	} else {
		if (stream_cfg.current_frame) stream_cfg.current_frame--;
		else stream_cfg.current_frame = MAX_FRAME_COUNT - 1;
	}



	if (stream_cfg.flush_sign == -1) stream_cfg.flush_sign = -2;

	return result;
}

UI_64 Audio::DTS::CoreDecode(I_64 & time_mark) {
	UI_64 offsa(0), result(0), sa_count(0);
	int f_size(0), l_chan(0), r_chan(0);
						
	GetBitStreamHeader();

	GetCoreAudioHeader();		

	for (unsigned int s(0);s<frame_cfg.subframe_count;s++) {
		if (decoder_cfg.src_size == 0) break;		
					
		if (decoder_cfg.spectral_data = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {
			previous_frame_delta = stream_cfg.current_frame++;
			
			stream_cfg.current_frame &= (MAX_FRAME_COUNT - 1);
					
			if (previous_frame_delta < stream_cfg.current_frame) previous_frame_delta = -1;
			previous_frame_delta *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);

			if (bs_hdr.predictor_flag) offsa = previous_frame_delta;

			decoder_cfg.spectral_data += stream_cfg.current_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);
			decoder_cfg.frame_cfg = reinterpret_cast<DefaultRenderer::FrameCfg *>(decoder_cfg.spectral_data);
			decoder_cfg.spectral_data += DefaultRenderer::frame_cfg_size;

					
			System::MemoryFill_SSE3(decoder_cfg.frame_cfg, (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE)*sizeof(float), 0, 0);

			decoder_cfg.frame_cfg->time_mark = time_mark;		
			decoder_cfg.frame_cfg->channel_offset = DTS_CHANNEL_SIZE;
			

			decoder_cfg.frame_cfg->source_frequency = bs_hdr.sampling_frequency;
			decoder_cfg.frame_cfg->source_chan_count = bs_hdr.full_chan_count + (bs_hdr.lfe_present>0);
	
			decoder_cfg.frame_cfg->window_sample_count = 32*32;
			decoder_cfg.frame_cfg->window_count = 1;

			for (unsigned int i(0);i<decoder_cfg.frame_cfg->source_chan_count;i++) {
				decoder_cfg.frame_cfg->chan_type[i] = bs_hdr.chan_type[i];
			}

			GetSubFrame();


			for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
				ScaleChannel(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE, decoder_cfg.frame_cfg->window_sample_count >> 5);

				for (unsigned int j(0);j<frame_cfg.chan_desc[i].hfvq_subband_start;j++) {
					if (frame_cfg.chan_desc[i].prediction_mode[j]) {
						InverseADPCM(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE, offsa, frame_cfg.chan_desc[i].pvq_index[j], (j << 16) | frame_cfg.subsub_count);
					} else {
						decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + 4*1024 + (j<<2) + 0] = decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + ((j+1)<<((frame_cfg.subsub_count>>1) + 4)) - 8];
						decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + 4*1024 + (j<<2) + 1] = decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + ((j+1)<<((frame_cfg.subsub_count>>1) + 4)) - 7];
						decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + 4*1024 + (j<<2) + 2] = decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + ((j+1)<<((frame_cfg.subsub_count>>1) + 4)) - 6];
						decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + 4*1024 + (j<<2) + 3] = decoder_cfg.spectral_data[i*DTS_CHANNEL_SIZE + ((j+1)<<((frame_cfg.subsub_count>>1) + 4)) - 5];
					}
				}
			}

			// if joint


			// if summ diff
			if ((bs_hdr.front_encoding) || (bs_hdr.arr_mode == 3)) {
				for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
					if (bs_hdr.chan_type[i] == SPEAKER_FRONT_LEFT) l_chan = i;
					if (bs_hdr.chan_type[i] == SPEAKER_FRONT_RIGHT) r_chan = i;
				}

				StereoAdjust(decoder_cfg.spectral_data + l_chan*DTS_CHANNEL_SIZE, decoder_cfg.spectral_data + r_chan*DTS_CHANNEL_SIZE, decoder_cfg.frame_cfg->window_sample_count);

			}

			if (bs_hdr.surround_encoding) {
				for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
					if (bs_hdr.chan_type[i] == SPEAKER_SIDE_LEFT) l_chan = i;
					if (bs_hdr.chan_type[i] == SPEAKER_SIDE_RIGHT) r_chan = i;
				}

				StereoAdjust(decoder_cfg.spectral_data + l_chan*DTS_CHANNEL_SIZE, decoder_cfg.spectral_data + r_chan*DTS_CHANNEL_SIZE, decoder_cfg.frame_cfg->window_sample_count);

			}

			sa_count = reinterpret_cast<Audio::DefaultRenderer::FrameCfg *>(reinterpret_cast<UI_64>(decoder_cfg.frame_cfg) + (previous_frame_delta<<2))->window_sample_count;
			if ((sa_count == 0) || (sa_count > 1024)) sa_count = 1024;			
			sa_count <<= 32;
			sa_count |= frame_cfg.subsub_count;

			for (unsigned int i(0);i<frame_cfg.active_chan_count;i++) {
				InterpolateQMF(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE, previous_frame_delta, sa_count, qmf_interpol[bs_hdr.interpolator_switch]);
				Normalize(decoder_cfg.spectral_data + i*DTS_CHANNEL_SIZE, frame_cfg.range_val, decoder_cfg.frame_cfg->window_sample_count);
			}

			if (bs_hdr.lfe_present) {
				InterpolateLFE(decoder_cfg.spectral_data + frame_cfg.active_chan_count*DTS_CHANNEL_SIZE, previous_frame_delta, bs_hdr.lfe_present, frame_cfg.subsub_count);
				Normalize(decoder_cfg.spectral_data + frame_cfg.active_chan_count*DTS_CHANNEL_SIZE, 0x34000000, decoder_cfg.frame_cfg->window_sample_count); // frame_cfg.range_val motherfuckers
			}



			




			decoder_cfg.frame_cfg->valid_sign = time_delta + (decoder_cfg.frame_cfg->window_count*decoder_cfg.frame_cfg->window_sample_count*10000);
			time_delta = (decoder_cfg.frame_cfg->valid_sign % decoder_cfg.frame_cfg->source_frequency);
			decoder_cfg.frame_cfg->valid_sign /= decoder_cfg.frame_cfg->source_frequency;
			

			time_mark = decoder_cfg.frame_cfg->valid_sign;

			stream_cfg.frame_buff.Release();			

			result = RESULT_DECODE_COMPLETE;
		}
	}


	if (decoder_cfg.src_size) {
	// optional info
		if (bs_hdr.et_stamp_flag) {
			decoder_cfg.GetNumber(32);			
		}

		f_size = 0;
		if (bs_hdr.aux_data_flag) {
			f_size = decoder_cfg.GetNumber(6);
		}

		decoder_cfg.ByteAlign(); // 32-bit boundary align
		if (f_size) { // aux data
			decoder_cfg.Skip(f_size);
		}

		if ((bs_hdr.crc_present) && (bs_hdr.ed_range_flag)) {
			decoder_cfg.GetNumber(16);
		}


	// extended audio
		if (bs_hdr.ext_audio_flag) {
			switch (bs_hdr.ext_audio_id) {
				case 0: // XCH

				break;
				case 2: // x96

				break;
				case 6: // XXCH

				break;

			}
		}
	}


	return result;

}

void Audio::DTS::GetAssetDescriptor() {
	DecoderState temp_reader(decoder_cfg);
	unsigned int x_val(0), a_idx(0), c_vals[4];

	x_val = decoder_cfg.GetNumber(9) + 1;
	a_idx = decoder_cfg.GetNumber(3);

	asset[a_idx].size = x_val;

	if (ss_hdr.static_field_flag) {
		if (decoder_cfg.NextBit()) {
			if (decoder_cfg.NextBit()) asset[a_idx]._type = decoder_cfg.GetNumber(4);
		}

		if (decoder_cfg.NextBit()) {
			asset[a_idx].language = decoder_cfg.GetNumber(24);
		}

		if (decoder_cfg.NextBit()) { // text info
			x_val = decoder_cfg.GetNumber(10) + 1;

			decoder_cfg.Skip(x_val);
		}

		asset[a_idx].bit_depth = decoder_cfg.GetNumber(5) + 1;
		asset[a_idx].max_sf_idx = decoder_cfg.GetNumber(4);
		asset[a_idx].tot_chan_count = decoder_cfg.GetNumber(8) + 1;

		if (asset[a_idx].one2one_flag = decoder_cfg.NextBit()) { // one 2 one map
			if (asset[a_idx].tot_chan_count > 2) {
				asset[a_idx].stereo_flag = decoder_cfg.NextBit();
			}

			if (asset[a_idx].tot_chan_count > 6) {
				asset[a_idx].ch7_flag = decoder_cfg.NextBit();
			}

			if (decoder_cfg.NextBit()) { // mask enabled
				x_val = ((decoder_cfg.GetNumber(2) + 1) << 2);

				asset[a_idx].activity_mask = decoder_cfg.GetNumber(x_val);		

				asset[a_idx].remap_set_count = decoder_cfg.GetNumber(3);
				
				for (unsigned int i(0);i<asset[a_idx].remap_set_count;i++) {
					asset[a_idx].speakers[i].layout_mask = decoder_cfg.GetNumber(x_val);
				}

				for (unsigned int i(0);i<asset[a_idx].remap_set_count;i++) {
					asset[a_idx].speakers[i].count = speaker_counts[System::BitIndexHigh_16(asset[a_idx].speakers[i].layout_mask)];
					asset[a_idx].speakers[i].remap_count = decoder_cfg.GetNumber(5) + 1;

					for (unsigned int j(0);j<asset[a_idx].speakers[i].count;j++) {
						x_val = asset[a_idx].speakers[i].remap_mask[j] = decoder_cfg.GetNumber(asset[a_idx].speakers[i].remap_count);
										
						for (;x_val;x_val >>= 1) {
							if (x_val & 1) {
								asset[a_idx].speakers[i].remap_codes[j][asset[i].speakers[j].code_count++] = decoder_cfg.GetNumber(5);
							}
						}
					}
				}
			}
		} else {

			asset[a_idx].representation_type = decoder_cfg.GetNumber(3);
		}
	}

// dynamic metadata ==========================================

	if (x_val = decoder_cfg.NextBit()) { // dynamic range flag
		asset[a_idx].dyr_code = decoder_cfg.GetNumber(8);
	}

	if (decoder_cfg.NextBit()) {
		asset[a_idx].dnorm_code = decoder_cfg.GetNumber(5);
	}

	if (x_val && asset[a_idx].stereo_flag) {
		asset[a_idx].dyr_mix_code = decoder_cfg.GetNumber(8);
	}

	asset[a_idx].mix_present_flag = 0;
	if (ss_hdr.mix_md_flag) {
		asset[a_idx].mix_present_flag = decoder_cfg.NextBit();
	}

	if (x_val) { // mix metadata present
		asset[a_idx].m_meta.external_flag = decoder_cfg.NextBit();

		asset[a_idx].m_meta.post_gain = decoder_cfg.GetNumber(6);
		asset[a_idx].m_meta.dyr_compression = decoder_cfg.GetNumber(2);

		if (asset[a_idx].m_meta.dyr_compression == 3) {
			asset[a_idx].m_meta.custom_dyr_code = decoder_cfg.GetNumber(8);
		} else {
			asset[a_idx].m_meta.dyr_comp_limit = decoder_cfg.GetNumber(3);
		}

		x_val = decoder_cfg.NextBit();
		
		for (unsigned int i(0);i<ss_hdr.mm_data.config_count;i++) {
			if (x_val) {
				for (unsigned int j(0);j<ss_hdr.mm_data.chan_count[i];j++) {
					asset[a_idx].m_meta.c_set[i].scale_code[j] = decoder_cfg.GetNumber(6);
				}
			} else {
				asset[a_idx].m_meta.c_set[i].scale_code[0] = asset[a_idx].m_meta.c_set[i].scale_code[1] = decoder_cfg.GetNumber(6);
			}
		}

		x_val = 1;
		c_vals[0] = asset[a_idx].tot_chan_count;

		if (asset[a_idx].ch7_flag) {
			c_vals[1] = 6;
			x_val = 2;
		}

		if (asset[a_idx].stereo_flag) {
			c_vals[x_val++] = 2;			
		}

		for (unsigned int i(0);i<ss_hdr.mm_data.config_count;i++) {
			for (unsigned int j(0);j<x_val;j++) {
				for (unsigned int c(0);c<c_vals[j];c++) {
					asset[a_idx].m_meta.c_set[i].map_mask[j][c] = decoder_cfg.GetNumber(ss_hdr.mm_data.chan_count[i]);

					if (asset[a_idx].m_meta.c_set[i].map_mask[j][c] & 1) {
						asset[a_idx].m_meta.c_set[i].mix_coffs[j][c][0] = decoder_cfg.GetNumber(6);
					}
					if (asset[a_idx].m_meta.c_set[i].map_mask[j][c] & 2) {
						asset[a_idx].m_meta.c_set[i].mix_coffs[j][c][1] = decoder_cfg.GetNumber(6);
					}
				}
			}
		}
	}

// navigation data ============================================
	switch (asset[a_idx].coding_mode = decoder_cfg.GetNumber(2)) {
		case 0: // multiple
			asset[a_idx].core_xmask = decoder_cfg.GetNumber(12);

			if (asset[a_idx].core_xmask & EXSUBSTREAM_CORE) {
				asset[a_idx].core_xsize = decoder_cfg.GetNumber(14) + 1;

				if (decoder_cfg.NextBit()) {
					asset[a_idx].core_sync_dist = (1<<decoder_cfg.GetNumber(2));
				}
			}

			if (asset[a_idx].core_xmask & EXSUBSTREAM_XBR) {
				asset[a_idx].xbr_size = decoder_cfg.GetNumber(14) + 1;
			}
			if (asset[a_idx].core_xmask & EXSUBSTREAM_XXCH) {
				asset[a_idx].xxch_size = decoder_cfg.GetNumber(14) + 1;
			}
			if (asset[a_idx].core_xmask & EXSUBSTREAM_X96) {
				asset[a_idx].x96_size = decoder_cfg.GetNumber(12) + 1;
			}
			if (asset[a_idx].core_xmask & EXSUBSTREAM_LBR) {
				asset[a_idx].lbr_size = decoder_cfg.GetNumber(14) + 1;

				if (decoder_cfg.NextBit()) {
					asset[a_idx].lbr_sync_dist = (1 << decoder_cfg.GetNumber(2));
				}
			}

			if (asset[a_idx].core_xmask & EXSUBSTREAM_XLL) {
				asset[a_idx].xll_size = decoder_cfg.GetNumber(ss_hdr.xs_bits) + 1;

				if (decoder_cfg.NextBit()) {
					decoder_cfg.GetNumber(4); // peak smoothing buf size
								
					decoder_cfg.GetNumber(decoder_cfg.GetNumber(5) + 1); // coding delay
					decoder_cfg.GetNumber(ss_hdr.xs_bits); // sync offset
				}
			}

			if (asset[a_idx].core_xmask & EXSUBSTREAM_RE1) {
				decoder_cfg.GetNumber(16);
			}

			if (asset[a_idx].core_xmask & EXSUBSTREAM_RE2) {
				decoder_cfg.GetNumber(16);
			}

		break;
		case 1: // loss-less
			asset[a_idx].xll_size = decoder_cfg.GetNumber(ss_hdr.xs_bits) + 1;

			if (decoder_cfg.NextBit()) {
				decoder_cfg.GetNumber(4); // peak smoothing buf size
								
				decoder_cfg.GetNumber(decoder_cfg.GetNumber(5) + 1); // coding delay
				decoder_cfg.GetNumber(ss_hdr.xs_bits); // sync offset
			}

		break;
		case 2: // low bitrate
			asset[a_idx].lbr_size = decoder_cfg.GetNumber(14) + 1;

			if (decoder_cfg.NextBit()) {
				asset[a_idx].lbr_sync_dist = (1 << decoder_cfg.GetNumber(2));
			}

		break;
		case 3: // auxilary
			asset[a_idx].aux_size = decoder_cfg.GetNumber(14) + 1;
			asset[a_idx].aux_codec_id = decoder_cfg.GetNumber(8);

			if (decoder_cfg.NextBit()) {
				decoder_cfg.GetNumber(3); // sync dist
			}
		break;
	}

	if (((asset[a_idx].coding_mode == 0) && (asset[a_idx].core_xmask & EXSUBSTREAM_XLL)) || (asset[a_idx].coding_mode == 1)) {
		asset[a_idx].stream_id = decoder_cfg.GetNumber(3);
	}

	if (asset[a_idx].one2one_flag && ss_hdr.mix_md_flag && !asset[a_idx].mix_present_flag) {
		if (decoder_cfg.NextBit()) {
			x_val = decoder_cfg.NextBit();

			for (unsigned int i(0);i<ss_hdr.mm_data.config_count;i++) {
				if (x_val) {
					for (unsigned int c(0);c<ss_hdr.mm_data.chan_count[i];c++) {
						asset[a_idx].m_meta.c_set[i].scale_code[c] = decoder_cfg.GetNumber(6);
					}
				} else {
					asset[a_idx].m_meta.c_set[i].scale_code[0] = asset[a_idx].m_meta.c_set[i].scale_code[1] = decoder_cfg.GetNumber(6);
				}
			}
		}
	}


/*
	what ever it is just skip it for now


	decoder_cfg.NextBit(); // asset in secondary decoder

	if (decoder_cfg.NextBit()) {
		decoder_cfg.GetNumber(4); // version ver2

	}
*/
		

	decoder_cfg = temp_reader;
	decoder_cfg.Skip(asset[a_idx].size);
}

UI_64 Audio::DTS::SubDecode(I_64 & time_mark) {
	UI_64 result(0);

	DecoderState temp_reader(decoder_cfg);
	unsigned int sync_word(0);
	unsigned char c_val(0);


	if (decoder_cfg.spectral_data = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {
		previous_frame_delta = ((stream_cfg.current_frame - 1) & (MAX_FRAME_COUNT - 1));
			
		if (previous_frame_delta < stream_cfg.current_frame) previous_frame_delta = -1;
		previous_frame_delta *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);

		decoder_cfg.spectral_data += stream_cfg.current_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);
		decoder_cfg.frame_cfg = reinterpret_cast<DefaultRenderer::FrameCfg *>(decoder_cfg.spectral_data);
		decoder_cfg.spectral_data += DefaultRenderer::frame_cfg_size;

		System::MemoryFill(&ss_hdr, sizeof(SubStreamHeader), 0);
		System::MemoryFill(asset, 8*sizeof(AssetDescriptor), 0);
		System::MemoryFill(&ll_frame, sizeof(LLConfig), 0);

		decoder_cfg.GetNumber(8); // user defined

		ss_hdr.index = decoder_cfg.GetNumber(2);

		if (decoder_cfg.NextBit()) { // long header
			ss_hdr.h_size = decoder_cfg.GetNumber(12) + 1;		
			ss_hdr.xs_bits = 20;
		} else {
			ss_hdr.h_size = decoder_cfg.GetNumber(8) + 1;		
			ss_hdr.xs_bits = 16;
		}

		ss_hdr.x_size = decoder_cfg.GetNumber(ss_hdr.xs_bits) + 1;

		if (ss_hdr.static_field_flag = decoder_cfg.NextBit()) { // static bit
			ss_hdr.sf_idx = decoder_cfg.GetNumber(2);
			ss_hdr.duration_code = ((decoder_cfg.GetNumber(3) + 1)<<9);

			if (decoder_cfg.NextBit()) {
				decoder_cfg.Skip(4); // timestamp
				decoder_cfg.GetNumber(4);			
			}

			ss_hdr.audio_presentation_count = decoder_cfg.GetNumber(3) + 1;
			ss_hdr.assets_count = decoder_cfg.GetNumber(3) + 1;

			for (unsigned int i(0);i<ss_hdr.audio_presentation_count;i++) {
				ss_hdr.audio_p[i].mask = decoder_cfg.GetNumber(ss_hdr.index + 1);
			}

			for (unsigned int i(0);i<ss_hdr.audio_presentation_count;i++) {
				c_val = 1;
				for (unsigned int j(0);j<ss_hdr.assets_count;j++) {
					if (ss_hdr.audio_p[i].mask & c_val) {
						ss_hdr.audio_p[i].asset_mask[j] = decoder_cfg.GetNumber(8);
					}

					c_val <<= 1;
				}
			}

			if (ss_hdr.mix_md_flag = decoder_cfg.NextBit()) { // mix metadata
				ss_hdr.mm_data.adjustment_level = decoder_cfg.GetNumber(2);
				c_val = ((decoder_cfg.GetNumber(2) + 1) << 2);
				ss_hdr.mm_data.config_count = decoder_cfg.GetNumber(2) + 1;

				for (unsigned char i(0);i<ss_hdr.mm_data.config_count;i++) {
					ss_hdr.mm_data.mix_channel_mask[i] = decoder_cfg.GetNumber(c_val);
					ss_hdr.mm_data.chan_count[i] = speaker_counts[System::BitIndexHigh_16(ss_hdr.mm_data.mix_channel_mask[i])];
				}
			}
		} else {
			ss_hdr.audio_presentation_count = 1;
			ss_hdr.assets_count = 1;
		}

		for (unsigned int i(0);i<ss_hdr.assets_count;i++) {
			ss_hdr.asset_size[i] = decoder_cfg.GetNumber(ss_hdr.xs_bits) + 1;
		}

		for (unsigned int i(0);i<ss_hdr.assets_count;i++) {
			GetAssetDescriptor();
		}

		for (unsigned int i(0);i<ss_hdr.audio_presentation_count;i++) {
			ss_hdr.audio_p[i].bc_core_present = decoder_cfg.NextBit();
		}

		for (unsigned int i(0);i<ss_hdr.audio_presentation_count;i++) {
			if (ss_hdr.audio_p[i].bc_core_present) {
				ss_hdr.audio_p[i].bc_index = decoder_cfg.GetNumber(2);
				ss_hdr.audio_p[i].bc_asset_index = decoder_cfg.GetNumber(3);
			}
		}


	// skip whatever up to crc
	
		decoder_cfg = temp_reader;
		decoder_cfg.Skip(ss_hdr.h_size - 6);

		decoder_cfg.GetNumber(16); // CRC


// assets
// each asset is like separate program, so my major interest is asset 0, I guess
// ignore non 0 assets for now, there must be a way to select an asset of interest


#ifdef _DEBUG
	if (stream_cfg.current_frame == 0x0039) {
		c_val = 0;
	}
#endif

		for (unsigned int a(0);a<1;a++) { // ss_hdr.assets_count


			for (unsigned int c(asset[a].core_xmask & 0x000FF0);c;c>>=1) {
				if ((c & 1) == 0) continue;

				sync_word = decoder_cfg.GetNumber(32);
//				sync_word <<= 16;
//				sync_word |= decoder_cfg.GetNumber(16);

				switch (sync_word) {
					
					case 0x02b09261: // substream core
				//		result = CoreDecode(time_mark);
					break;

					case 0x5A5A5A5A: // XCH
						
					break;
					case 0x47004A03: // XXCH
		
					break;
					case 0x1D95F262: // X96
						
					break;
					case 0x655E315E: // XBR
						
					break;
					case 0x0A801921: // LBR
						result = GetLowBitrate(a);
	
					break;
					case 0x41A29547: // XLL
						result = GetLossless(a);
	
					break;

				}
			}
		}

		stream_cfg.frame_buff.Release();
	}
		
	return result;
}


UI_64 Audio::DTS::FilterFrame(unsigned int *) {
	UI_64 result(0);


	return result;
}


UI_64 Audio::DTS::FilterSatFrame(float * vector_ptr, OpenGL::DecodeRecord & de_rec) {
	float inc_val(0.0f);
	UI_64 result(0), last_frame(de_rec.target_time & 0xFFFFFFFF), last_adjust(de_rec.target_time >> 32), new_adjust(0), f_off(0), fs_count(0);

	if (last_frame == stream_cfg.current_frame) return 0;

	if (de_rec.start_time == 0) last_frame = -1;

	if (float * f_ptr = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {
		fs_count = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->window_sample_count;
		if (fs_count == 0) fs_count = 1024;

		result = -System::GetUCount(de_rec.start_time, 0);
		de_rec.start_time = System::GetTime();

		if (last_frame != -1) {
			result *= reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			result /= 10000;

			last_adjust = fs_count - last_adjust;

			if (result >= last_adjust) {
				result -= last_adjust;
				last_adjust = result % fs_count;

				last_frame++;
			} else {
				last_adjust = fs_count - last_adjust + result;
			}

			last_frame += (result/fs_count);
			last_frame &= (MAX_FRAME_COUNT - 1);

		} else {
			result = 8192;
			last_adjust = 0;

			last_frame = 8;
		}


		if (last_frame > stream_cfg.current_frame) last_frame = stream_cfg.current_frame;

		de_rec.target_time = last_adjust;
		de_rec.target_time <<= 32;
		de_rec.target_time |= last_frame;


		result = de_rec.width - last_adjust;		
		new_adjust = (result % fs_count);
		result = (result/fs_count);

		
		if (new_adjust) {
			f_off = (last_frame - result - 1) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*DTS_CHANNEL_SIZE + fs_count - new_adjust;


			for (unsigned int si(0);si<new_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}	
		


		for (;result > 0;result--) {
			f_off = (last_frame - result)*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*DTS_CHANNEL_SIZE;
			
			for (unsigned int si(0);si<fs_count;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}

		if (last_adjust) {
			f_off = last_frame*(DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);
			f_off += (DefaultRenderer::frame_cfg_size + 2048);
			f_off += de_rec.pic_id*DTS_CHANNEL_SIZE;
			
			for (unsigned int si(0);si<last_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
		}	

		stream_cfg.frame_buff.Release();

		result = 0x00010000;
	}

	return result;
}

	
void Audio::DTS::JumpOn() {


}

UI_64 Audio::DTS::UnitTest(const float *) {
	return 0;
}
			
UI_64 Audio::DTS::CfgOverride(UI_64, UI_64) {
	UI_64 result(0);

	return result;
}

unsigned int Audio::DTS::GetSatCount(unsigned int * sat_desc) {
	UI_64 f_off(0);
	unsigned int result(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);

		f_ptr += f_off;
		
		result = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
		for (unsigned int i(0);i<8;i++) sat_desc[i] = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->chan_type[i];

		stream_cfg.frame_buff.Release();
	}


	return result;
}

void Audio::DTS::GetSatInfo(unsigned int i_id, OpenGL::StringsBoard::InfoLine & i_line) {
	UI_64 f_off(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*DTS_CHANNEL_SIZE);

		f_ptr += f_off;
		
		switch (i_id) {
			case 0:
				if (hd_brand) {
					i_line.cap[0] = L'D'; i_line.cap[1] = L'T'; i_line.cap[2] = L'S'; i_line.cap[3] = L'-'; i_line.cap[4] = L'H'; i_line.cap[5] = L'D'; i_line.cap[6] = 0;
					i_line.cap_size = 6;
				} else {
					i_line.cap[0] = L'D'; i_line.cap[1] = L'T'; i_line.cap[2] = L'S'; i_line.cap[3] = 0;
					i_line.cap_size = 3;
				}

				i_line.cap_flags = 0;
			break;
			case 1:
				i_line.cap[0] = L'S'; i_line.cap[1] = L'F'; i_line.cap[2] = L':'; i_line.cap[3] = L'H'; i_line.cap[4] = L'z'; i_line.cap[5] = 0;
				i_line.cap_size = 0x00020003;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeDouble | OpenGL::StringsBoard::InfoTypeFormatted | 1;

				reinterpret_cast<double &>(i_line.val) = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			break;
			case 2:
				i_line.cap[0] = L'c'; i_line.cap[1] = L'h'; i_line.cap[2] = L'a'; i_line.cap[3] = L'n'; i_line.cap[4] = L'n'; i_line.cap[5] = L'e'; i_line.cap[6] = L'l'; i_line.cap[7] = L's'; i_line.cap[8] = 0;
				i_line.cap_size = 8;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeINT64 | OpenGL::StringsBoard::InfoDataPrefix;

				i_line.val = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
			break;
		}

		stream_cfg.frame_buff.Release();
	}


}




UI_64 Audio::DTS::Initialize() {
	int conter(0);

	for (int i(0);i<4;i++) {
		for (int j(0);j<16;j++) {
			synth_cos[conter++] = Calc::RealCos(180.0/64.0*(2*j + 1)*(2*(4*i + 0) + 1));
			synth_cos[conter++] = Calc::RealCos(180.0/64.0*(2*j + 1)*(2*(4*i + 1) + 1));
			synth_cos[conter++] = Calc::RealCos(180.0/64.0*(2*j + 1)*(2*(4*i + 2) + 1));
			synth_cos[conter++] = Calc::RealCos(180.0/64.0*(2*j + 1)*(2*(4*i + 3) + 1));

			synth_cos[conter++] = Calc::RealCos(180.0/32.0*(2*j + 1)*(4*i + 0));
			synth_cos[conter++] = Calc::RealCos(180.0/32.0*(2*j + 1)*(4*i + 1));
			synth_cos[conter++] = Calc::RealCos(180.0/32.0*(2*j + 1)*(4*i + 2));
			synth_cos[conter++] = Calc::RealCos(180.0/32.0*(2*j + 1)*(4*i + 3));

		}
	}



	for (int i(0);i<4;i++) {
		synth_cos[conter++] = 0.25/2.0/Calc::RealCos(180.0/128.0*(2*(4*i + 0) + 1));
		synth_cos[conter++] = 0.25/2.0/Calc::RealCos(180.0/128.0*(2*(4*i + 1) + 1));
		synth_cos[conter++] = 0.25/2.0/Calc::RealCos(180.0/128.0*(2*(4*i + 2) + 1));
		synth_cos[conter++] = 0.25/2.0/Calc::RealCos(180.0/128.0*(2*(4*i + 3) + 1));

		synth_cos[conter++] = -0.25/2.0/Calc::RealSin(180.0/128.0*(2*(4*i + 0) + 1));
		synth_cos[conter++] = -0.25/2.0/Calc::RealSin(180.0/128.0*(2*(4*i + 1) + 1));
		synth_cos[conter++] = -0.25/2.0/Calc::RealSin(180.0/128.0*(2*(4*i + 2) + 1));
		synth_cos[conter++] = -0.25/2.0/Calc::RealSin(180.0/128.0*(2*(4*i + 3) + 1));
	}

	return 0;
}

UI_64 Audio::DTS::Finalize() {


	return 0;
}

// ==========================================================================================================================================================================================================

const unsigned int Audio::DTS::sampling_frequencies[] = {
	0, 8000, 16000, 32000,
	0, 0, 11025, 22050,
	44100, 0, 0, 12000,
	24000, 48000, 0, 0
};



__declspec(align(16)) const unsigned int Audio::DTS::lfe_interpol[2][512] = {
	{	
		0x3E1EA598, 0x3F2F9FF4, 0x3E229886, 0x3A0B60D3, 0x3E1AC168, 0x3F2F9853, 0x3E269A21, 0x392B8887, 0x3E16EC07, 0x3F2F8914, 0x3E2AAA54, 0x3945F4E1, 0x3E132580, 0x3F2F7237, 0x3E2EC90B, 0x3962F276, 0x3E0F6DDE, 0x3F2F53BE, 0x3E32F62C, 0x39815400, 0x3E0BC52C, 0x3F2F2DAF, 0x3E37319C, 0x39928966, 0x3E082B6F, 0x3F2F000D, 0x3E3B7B3E, 0x39A54037, 0x3E04A0AD, 0x3F2ECADE, 0x3E3FD2F1, 0x39B996E5,
		0x3E0124E5, 0x3F2E8E29, 0x3E443895, 0x39CFCCC8, 0x3DFB7035, 0x3F2E49F3, 0x3E48AC02, 0x39E7EDB5, 0x3DF4B495, 0x3F2DFE46, 0x3E4D2D17, 0x3A00FC99, 0x3DEE16E7, 0x3F2DAB2B, 0x3E51BBA6, 0x3A0EED3C, 0x3DE79718, 0x3F2D50AC, 0x3E565784, 0x3A1E034E, 0x3DE13520, 0x3F2CEED4, 0x3E5B0084, 0x3A2E80DF, 0x3DDAF0EC, 0x3F2C85AC, 0x3E5FB673, 0x3A3FD60D, 0x3DD4CA63, 0x3F2C1544, 0x3E64791F, 0x3A52AC5A,
		0x3DCEC16C, 0x3F2B9DA8, 0x3E694851, 0x3A66C8D9, 0x3DC8D5ED, 0x3F2B1EE6, 0x3E6E23D2, 0x3A7C53D9, 0x3DC307C1, 0x3F2A990D, 0x3E730B69, 0x3A89B097, 0x3DBD56CC, 0x3F2A0C2D, 0x3E77FED5, 0x3A95FCE1, 0x3DB7C2DC, 0x3F297857, 0x3E7CFDD6, 0x3AA31BEB, 0x3DB24BD2, 0x3F28DD9D, 0x3E810417, 0x3AB11767, 0x3DACF177, 0x3F283C10, 0x3E838ECB, 0x3ABFFF6F, 0x3DA7B3A5, 0x3F2793C2, 0x3E861EE4, 0x3ACFD744,
		0x3DA29223, 0x3F26E4CA, 0x3E88B43C, 0x3AE0A8EC, 0x3D9D8CBD, 0x3F262F39, 0x3E8B4EAD, 0x3AF28393, 0x3D98A338, 0x3F257327, 0x3E8DEE11, 0x3B02BDB6, 0x3D93D560, 0x3F24B0A8, 0x3E90923F, 0x3B0CC30D, 0x3D8F22F1, 0x3F23E7D4, 0x3E933B0E, 0x3B176415, 0x3D8A8BAF, 0x3F2318C1, 0x3E95E853, 0x3B229E20, 0x3D860F55, 0x3F224388, 0x3E9899E1, 0x3B2E7D8B, 0x3D81ADA1, 0x3F216841, 0x3E9B4F8C, 0x3B3B0684,
		0x3D7ACC9A, 0x3F208706, 0x3E9E0925, 0x3B48415B, 0x3D727220, 0x3F1F9FEF, 0x3EA0C67F, 0x3B5632EE, 0x3D6A4B40, 0x3F1EB318, 0x3EA38766, 0x3B64E4E7, 0x3D625760, 0x3F1DC09D, 0x3EA64BAA, 0x3B745DFF, 0x3D5A95E6, 0x3F1CC898, 0x3EA91319, 0x3B82532D, 0x3D53063B, 0x3F1BCB25, 0x3EABDD80, 0x3B8AE165, 0x3D4BA7B0, 0x3F1AC863, 0x3EAEAAA8, 0x3B93DF5B, 0x3D4479B0, 0x3F19C06B, 0x3EB17A5D, 0x3B9D5081,
		0x3D3D7B87, 0x3F18B35E, 0x3EB44C69, 0x3BA73819, 0x3D36AC96, 0x3F17A15A, 0x3EB72092, 0x3BB19BB9, 0x3D300C27, 0x3F168A7B, 0x3EB9F6A0, 0x3BBC7E5F, 0x3D299996, 0x3F156EE2, 0x3EBCCE5D, 0x3BC7E4F8, 0x3D235423, 0x3F144EAE, 0x3EBFA78A, 0x3BD3D46D, 0x3D1D3B31, 0x3F1329FD, 0x3EC281EE, 0x3BE0508D, 0x3D174DF6, 0x3F1200F3, 0x3EC55D4D, 0x3BED5E66, 0x3D118BD1, 0x3F10D3AD, 0x3EC83969, 0x3BFB0221,
		0x3D0BF3F2, 0x3F0FA24D, 0x3ECB1607, 0x3C04A0CF, 0x3D0685BF, 0x3F0E6CF4, 0x3ECDF2E6, 0x3C0C1055, 0x3D014065, 0x3F0D33C2, 0x3ED0CFC7, 0x3C13D246, 0x3CF84677, 0x3F0BF6D9, 0x3ED3AC6D, 0x3C1BE904, 0x3CEE5AF9, 0x3F0AB65C, 0x3ED68895, 0x3C245765, 0x3CE4BCF0, 0x3F09726D, 0x3ED96400, 0x3C2D1F4E, 0x3CDB6AC7, 0x3F082B2D, 0x3EDC3E68, 0x3C36443D, 0x3CD26332, 0x3F06E0BE, 0x3EDF1791, 0x3C3FC7C6,
		0x3CC9A48C, 0x3F059343, 0x3EE1EF34, 0x3C49AD5D, 0x3CC12D7B, 0x3F0442DF, 0x3EE4C512, 0x3C53F735, 0x3CB8FC74, 0x3F02EFB5, 0x3EE798E3, 0x3C5EA867, 0x3CB1101B, 0x3F0199E6, 0x3EEA6A64, 0x3C69C336, 0x3CA966E5, 0x3F004196, 0x3EED3954, 0x3C754ACA, 0x3CA1FF58, 0x3EFDCDCF, 0x3EF0056C, 0x3C80A0DA, 0x3C9AD817, 0x3EFB13F9, 0x3EF2CE66, 0x3C86D580, 0x3C93EFAE, 0x3EF855F3, 0x3EF59401, 0x3C8D448F,
		0x3C8D448F, 0x3EF59401, 0x3EF855F3, 0x3C93EFAE, 0x3C86D580, 0x3EF2CE66, 0x3EFB13F9, 0x3C9AD817, 0x3C80A0DA, 0x3EF0056C, 0x3EFDCDCF, 0x3CA1FF58, 0x3C754ACA, 0x3EED3954, 0x3F004196, 0x3CA966E5, 0x3C69C336, 0x3EEA6A64, 0x3F0199E6, 0x3CB1101B, 0x3C5EA867, 0x3EE798E3, 0x3F02EFB5, 0x3CB8FC74, 0x3C53F735, 0x3EE4C512, 0x3F0442DF, 0x3CC12D7B, 0x3C49AD5D, 0x3EE1EF34, 0x3F059343, 0x3CC9A48C,
		0x3C3FC7C6, 0x3EDF1791, 0x3F06E0BE, 0x3CD26332, 0x3C36443D, 0x3EDC3E68, 0x3F082B2D, 0x3CDB6AC7, 0x3C2D1F4E, 0x3ED96400, 0x3F09726D, 0x3CE4BCF0, 0x3C245765, 0x3ED68895, 0x3F0AB65C, 0x3CEE5AF9, 0x3C1BE904, 0x3ED3AC6D, 0x3F0BF6D9, 0x3CF84677, 0x3C13D246, 0x3ED0CFC7, 0x3F0D33C2, 0x3D014065, 0x3C0C1055, 0x3ECDF2E6, 0x3F0E6CF4, 0x3D0685BF, 0x3C04A0CF, 0x3ECB1607, 0x3F0FA24D, 0x3D0BF3F2,
		0x3BFB0221, 0x3EC83969, 0x3F10D3AD, 0x3D118BD1, 0x3BED5E66, 0x3EC55D4D, 0x3F1200F3, 0x3D174DF6, 0x3BE0508D, 0x3EC281EE, 0x3F1329FD, 0x3D1D3B31, 0x3BD3D46D, 0x3EBFA78A, 0x3F144EAE, 0x3D235423, 0x3BC7E4F8, 0x3EBCCE5D, 0x3F156EE2, 0x3D299996, 0x3BBC7E5F, 0x3EB9F6A0, 0x3F168A7B, 0x3D300C27, 0x3BB19BB9, 0x3EB72092, 0x3F17A15A, 0x3D36AC96, 0x3BA73819, 0x3EB44C69, 0x3F18B35E, 0x3D3D7B87,
		0x3B9D5081, 0x3EB17A5D, 0x3F19C06B, 0x3D4479B0, 0x3B93DF5B, 0x3EAEAAA8, 0x3F1AC863, 0x3D4BA7B0, 0x3B8AE165, 0x3EABDD80, 0x3F1BCB25, 0x3D53063B, 0x3B82532D, 0x3EA91319, 0x3F1CC898, 0x3D5A95E6, 0x3B745DFF, 0x3EA64BAA, 0x3F1DC09D, 0x3D625760, 0x3B64E4E7, 0x3EA38766, 0x3F1EB318, 0x3D6A4B40, 0x3B5632EE, 0x3EA0C67F, 0x3F1F9FEF, 0x3D727220, 0x3B48415B, 0x3E9E0925, 0x3F208706, 0x3D7ACC9A,
		0x3B3B0684, 0x3E9B4F8C, 0x3F216841, 0x3D81ADA1, 0x3B2E7D8B, 0x3E9899E1, 0x3F224388, 0x3D860F55, 0x3B229E20, 0x3E95E853, 0x3F2318C1, 0x3D8A8BAF, 0x3B176415, 0x3E933B0E, 0x3F23E7D4, 0x3D8F22F1, 0x3B0CC30D, 0x3E90923F, 0x3F24B0A8, 0x3D93D560, 0x3B02BDB6, 0x3E8DEE11, 0x3F257327, 0x3D98A338, 0x3AF28393, 0x3E8B4EAD, 0x3F262F39, 0x3D9D8CBD, 0x3AE0A8EC, 0x3E88B43C, 0x3F26E4CA, 0x3DA29223,
		0x3ACFD744, 0x3E861EE4, 0x3F2793C2, 0x3DA7B3A5, 0x3ABFFF6F, 0x3E838ECB, 0x3F283C10, 0x3DACF177, 0x3AB11767, 0x3E810417, 0x3F28DD9D, 0x3DB24BD2, 0x3AA31BEB, 0x3E7CFDD6, 0x3F297857, 0x3DB7C2DC, 0x3A95FCE1, 0x3E77FED5, 0x3F2A0C2D, 0x3DBD56CC, 0x3A89B097, 0x3E730B69, 0x3F2A990D, 0x3DC307C1, 0x3A7C53D9, 0x3E6E23D2, 0x3F2B1EE6, 0x3DC8D5ED, 0x3A66C8D9, 0x3E694851, 0x3F2B9DA8, 0x3DCEC16C,
		0x3A52AC5A, 0x3E64791F, 0x3F2C1544, 0x3DD4CA63, 0x3A3FD60D, 0x3E5FB673, 0x3F2C85AC, 0x3DDAF0EC, 0x3A2E80DF, 0x3E5B0084, 0x3F2CEED4, 0x3DE13520, 0x3A1E034E, 0x3E565784, 0x3F2D50AC, 0x3DE79718, 0x3A0EED3C, 0x3E51BBA6, 0x3F2DAB2B, 0x3DEE16E7, 0x3A00FC99, 0x3E4D2D17, 0x3F2DFE46, 0x3DF4B495, 0x39E7EDB5, 0x3E48AC02, 0x3F2E49F3, 0x3DFB7035, 0x39CFCCC8, 0x3E443895, 0x3F2E8E29, 0x3E0124E5,
		0x39B996E5, 0x3E3FD2F1, 0x3F2ECADE, 0x3E04A0AD, 0x39A54037, 0x3E3B7B3E, 0x3F2F000D, 0x3E082B6F, 0x39928966, 0x3E37319C, 0x3F2F2DAF, 0x3E0BC52C, 0x39815400, 0x3E32F62C, 0x3F2F53BE, 0x3E0F6DDE, 0x3962F276, 0x3E2EC90B, 0x3F2F7237, 0x3E132580, 0x3945F4E1, 0x3E2AAA54, 0x3F2F8914, 0x3E16EC07, 0x392B8887, 0x3E269A21, 0x3F2F9853, 0x3E1AC168, 0x3A0B60D3, 0x3E229886, 0x3F2F9FF4, 0x3E1EA598
	},

	{	
		0x3C0D44A3, 0x3D9EA5AD, 0x3E759422, 0x3EAFA00C, 0x3E785615, 0x3DA2989B, 0x3C13EFC2, 0x398B60E7, 0x3C06D593, 0x3D9AC17D, 0x3E72CE88, 0x3EAF986B, 0x3E7B141B, 0x3DA69A37, 0x3C1AD82C, 0x38AB88A2, 0x3C00A0EB, 0x3D96EC1B, 0x3E70058C, 0x3EAF892C, 0x3E7DCDF0, 0x3DAAAA6B, 0x3C21FF6F, 0x38C5F4FC, 0x3BF54AE9, 0x3D932593, 0x3E6D3974, 0x3EAF724E, 0x3E8041A7, 0x3DAEC922, 0x3C2966FC, 0x38E2F293,
		0x3BE9C357, 0x3D8F6DF2, 0x3E6A6A85, 0x3EAF53D6, 0x3E8199F8, 0x3DB2F643, 0x3C311033, 0x39015410, 0x3BDEA886, 0x3D8BC540, 0x3E679902, 0x3EAF2DC7, 0x3E82EFC6, 0x3DB731B3, 0x3C38FC8E, 0x39128979, 0x3BD3F751, 0x3D882B82, 0x3E64C530, 0x3EAF0025, 0x3E8442F1, 0x3DBB7B56, 0x3C412D93, 0x3925404D, 0x3BC9AD78, 0x3D84A0BD, 0x3E61EF53, 0x3EAECAF6, 0x3E859355, 0x3DBFD30B, 0x3C49A4A6, 0x393996FE,
		0x3BBFC7E0, 0x3D8124F7, 0x3E5F17B0, 0x3EAE8E40, 0x3E86E0CF, 0x3DC438AF, 0x3C52634F, 0x394FCCE4, 0x3BB64456, 0x3D7B7058, 0x3E5C3E87, 0x3EAE4A0B, 0x3E882B3F, 0x3DC8AC1E, 0x3C5B6AE6, 0x3967EDD3, 0x3BAD1F66, 0x3D74B4B7, 0x3E59641C, 0x3EADFE5E, 0x3E89727F, 0x3DCD2D33, 0x3C64BD0E, 0x3980FCAA, 0x3BA4577B, 0x3D6E1705, 0x3E5688B3, 0x3EADAB43, 0x3E8AB670, 0x3DD1BBC3, 0x3C6E5B1A, 0x398EED4F,
		0x3B9BE919, 0x3D679737, 0x3E53AC8A, 0x3EAD50C4, 0x3E8BF6ED, 0x3DD657A1, 0x3C784699, 0x399E0363, 0x3B93D25A, 0x3D61353F, 0x3E50CFE5, 0x3EACEEEB, 0x3E8D33D5, 0x3DDB00A1, 0x3C814078, 0x39AE80F8, 0x3B8C1069, 0x3D5AF109, 0x3E4DF303, 0x3EAC85C3, 0x3E8E6D06, 0x3DDFB691, 0x3C8685D1, 0x39BFD626, 0x3B84A0E2, 0x3D54CA80, 0x3E4B1622, 0x3EAC155B, 0x3E8FA260, 0x3DE4793D, 0x3C8BF404, 0x39D2AC76,
		0x3B7B0245, 0x3D4EC188, 0x3E483985, 0x3EAB9DC0, 0x3E90D3C2, 0x3DE94870, 0x3C918BE5, 0x39E6C8F9, 0x3B6D5E87, 0x3D48D608, 0x3E455D67, 0x3EAB1EFD, 0x3E920107, 0x3DEE23F2, 0x3C974E09, 0x39FC53FD, 0x3B6050AB, 0x3D4307DC, 0x3E428209, 0x3EAA9924, 0x3E932A12, 0x3DF30B89, 0x3C9D3B47, 0x3A09B0A9, 0x3B53D489, 0x3D3D56E4, 0x3E3FA7A5, 0x3EAA0C45, 0x3E944EC2, 0x3DF7FEF6, 0x3CA3543A, 0x3A15FCF5,
		0x3B47E513, 0x3D37C2F4, 0x3E3CCE76, 0x3EA9786F, 0x3E956EF6, 0x3DFCFDFB, 0x3CA999AD, 0x3A231C02, 0x3B3C7E78, 0x3D324BE9, 0x3E39F6BA, 0x3EA8DDB3, 0x3E968A90, 0x3E01042A, 0x3CB00C3E, 0x3A31177F, 0x3B319BD2, 0x3D2CF18E, 0x3E3720AA, 0x3EA83C26, 0x3E97A16E, 0x3E038EDE, 0x3CB6ACAF, 0x3A3FFF88, 0x3B273831, 0x3D27B3BB, 0x3E344C81, 0x3EA793DA, 0x3E98B374, 0x3E061EF6, 0x3CBD7BA0, 0x3A4FD760,
		0x3B1D5097, 0x3D229239, 0x3E317A75, 0x3EA6E4E1, 0x3E99C07F, 0x3E08B44E, 0x3CC479CB, 0x3A60A90A, 0x3B13DF6F, 0x3D1D8CD2, 0x3E2EAAC0, 0x3EA62F50, 0x3E9AC877, 0x3E0B4EC0, 0x3CCBA7CC, 0x3A7283B3, 0x3B0AE178, 0x3D18A34D, 0x3E2BDD98, 0x3EA5733E, 0x3E9BCB3B, 0x3E0DEE25, 0x3CD30658, 0x3A82BDC7, 0x3B02533E, 0x3D13D575, 0x3E291331, 0x3EA4B0BF, 0x3E9CC8AD, 0x3E109254, 0x3CDA9604, 0x3A8CC31F,
		0x3AF45E21, 0x3D0F2305, 0x3E264BC1, 0x3EA3E7EA, 0x3E9DC0B3, 0x3E133B22, 0x3CE2577D, 0x3A976429, 0x3AE4E507, 0x3D0A8BC2, 0x3E23877C, 0x3EA318D7, 0x3E9EB32F, 0x3E15E868, 0x3CEA4B5F, 0x3AA29E35, 0x3AD6330A, 0x3D060F68, 0x3E20C694, 0x3EA2439E, 0x3E9FA004, 0x3E1899F6, 0x3CF27241, 0x3AAE7DA3, 0x3AC84176, 0x3D01ADB4, 0x3E1E093B, 0x3EA16857, 0x3EA0871C, 0x3E1B4FA2, 0x3CFACCBD, 0x3ABB069D,
		0x3ABB069D, 0x3CFACCBD, 0x3E1B4FA2, 0x3EA0871C, 0x3EA16857, 0x3E1E093B, 0x3D01ADB4, 0x3AC84176, 0x3AAE7DA3, 0x3CF27241, 0x3E1899F6, 0x3E9FA004, 0x3EA2439E, 0x3E20C694, 0x3D060F68, 0x3AD6330A, 0x3AA29E35, 0x3CEA4B5F, 0x3E15E868, 0x3E9EB32F, 0x3EA318D7, 0x3E23877C, 0x3D0A8BC2, 0x3AE4E507, 0x3A976429, 0x3CE2577D, 0x3E133B22, 0x3E9DC0B3, 0x3EA3E7EA, 0x3E264BC1, 0x3D0F2305, 0x3AF45E21,
		0x3A8CC31F, 0x3CDA9604, 0x3E109254, 0x3E9CC8AD, 0x3EA4B0BF, 0x3E291331, 0x3D13D575, 0x3B02533E, 0x3A82BDC7, 0x3CD30658, 0x3E0DEE25, 0x3E9BCB3B, 0x3EA5733E, 0x3E2BDD98, 0x3D18A34D, 0x3B0AE178, 0x3A7283B3, 0x3CCBA7CC, 0x3E0B4EC0, 0x3E9AC877, 0x3EA62F50, 0x3E2EAAC0, 0x3D1D8CD2, 0x3B13DF6F, 0x3A60A90A, 0x3CC479CB, 0x3E08B44E, 0x3E99C07F, 0x3EA6E4E1, 0x3E317A75, 0x3D229239, 0x3B1D5097,
		0x3A4FD760, 0x3CBD7BA0, 0x3E061EF6, 0x3E98B374, 0x3EA793DA, 0x3E344C81, 0x3D27B3BB, 0x3B273831, 0x3A3FFF88, 0x3CB6ACAF, 0x3E038EDE, 0x3E97A16E, 0x3EA83C26, 0x3E3720AA, 0x3D2CF18E, 0x3B319BD2, 0x3A31177F, 0x3CB00C3E, 0x3E01042A, 0x3E968A90, 0x3EA8DDB3, 0x3E39F6BA, 0x3D324BE9, 0x3B3C7E78, 0x3A231C02, 0x3CA999AD, 0x3DFCFDFB, 0x3E956EF6, 0x3EA9786F, 0x3E3CCE76, 0x3D37C2F4, 0x3B47E513,
		0x3A15FCF5, 0x3CA3543A, 0x3DF7FEF6, 0x3E944EC2, 0x3EAA0C45, 0x3E3FA7A5, 0x3D3D56E4, 0x3B53D489, 0x3A09B0A9, 0x3C9D3B47, 0x3DF30B89, 0x3E932A12, 0x3EAA9924, 0x3E428209, 0x3D4307DC, 0x3B6050AB, 0x39FC53FD, 0x3C974E09, 0x3DEE23F2, 0x3E920107, 0x3EAB1EFD, 0x3E455D67, 0x3D48D608, 0x3B6D5E87, 0x39E6C8F9, 0x3C918BE5, 0x3DE94870, 0x3E90D3C2, 0x3EAB9DC0, 0x3E483985, 0x3D4EC188, 0x3B7B0245,
		0x39D2AC76, 0x3C8BF404, 0x3DE4793D, 0x3E8FA260, 0x3EAC155B, 0x3E4B1622, 0x3D54CA80, 0x3B84A0E2, 0x39BFD626, 0x3C8685D1, 0x3DDFB691, 0x3E8E6D06, 0x3EAC85C3, 0x3E4DF303, 0x3D5AF109, 0x3B8C1069, 0x39AE80F8, 0x3C814078, 0x3DDB00A1, 0x3E8D33D5, 0x3EACEEEB, 0x3E50CFE5, 0x3D61353F, 0x3B93D25A, 0x399E0363, 0x3C784699, 0x3DD657A1, 0x3E8BF6ED, 0x3EAD50C4, 0x3E53AC8A, 0x3D679737, 0x3B9BE919,
		0x398EED4F, 0x3C6E5B1A, 0x3DD1BBC3, 0x3E8AB670, 0x3EADAB43, 0x3E5688B3, 0x3D6E1705, 0x3BA4577B, 0x3980FCAA, 0x3C64BD0E, 0x3DCD2D33, 0x3E89727F, 0x3EADFE5E, 0x3E59641C, 0x3D74B4B7, 0x3BAD1F66, 0x3967EDD3, 0x3C5B6AE6, 0x3DC8AC1E, 0x3E882B3F, 0x3EAE4A0B, 0x3E5C3E87, 0x3D7B7058, 0x3BB64456, 0x394FCCE4, 0x3C52634F, 0x3DC438AF, 0x3E86E0CF, 0x3EAE8E40, 0x3E5F17B0, 0x3D8124F7, 0x3BBFC7E0,
		0x393996FE, 0x3C49A4A6, 0x3DBFD30B, 0x3E859355, 0x3EAECAF6, 0x3E61EF53, 0x3D84A0BD, 0x3BC9AD78, 0x3925404D, 0x3C412D93, 0x3DBB7B56, 0x3E8442F1, 0x3EAF0025, 0x3E64C530, 0x3D882B82, 0x3BD3F751, 0x39128979, 0x3C38FC8E, 0x3DB731B3, 0x3E82EFC6, 0x3EAF2DC7, 0x3E679902, 0x3D8BC540, 0x3BDEA886, 0x39015410, 0x3C311033, 0x3DB2F643, 0x3E8199F8, 0x3EAF53D6, 0x3E6A6A85, 0x3D8F6DF2, 0x3BE9C357,
		0x38E2F293, 0x3C2966FC, 0x3DAEC922, 0x3E8041A7, 0x3EAF724E, 0x3E6D3974, 0x3D932593, 0x3BF54AE9, 0x38C5F4FC, 0x3C21FF6F, 0x3DAAAA6B, 0x3E7DCDF0, 0x3EAF892C, 0x3E70058C, 0x3D96EC1B, 0x3C00A0EB, 0x38AB88A2, 0x3C1AD82C, 0x3DA69A37, 0x3E7B141B, 0x3EAF986B, 0x3E72CE88, 0x3D9AC17D, 0x3C06D593, 0x398B60E7, 0x3C13EFC2, 0x3DA2989B, 0x3E785615, 0x3EAFA00C, 0x3E759422, 0x3D9EA5AD, 0x3C0D44A3
	}

};

__declspec(align(16)) const unsigned int Audio::DTS::qmf_interpol[2][512] = {
	{
		0xB415454D, 0xB435DD22, 0xB45A0AD7, 0xB481139A, 0x385795E0, 0x385BC3E8, 0x385F32D9, 0x3861CB00, 0x39FC6B06, 0x39F721ED, 0x39F075F8, 0x39E85949, 0x3AC40E79, 0x3AAFF57E, 0x3A9A1B50, 0x3A827C5B, 0x3C928A76, 0x3C925D0E, 0x3C92025C, 0x3C917A9B, 0x3AD66AFD, 0x3AE710EB, 0x3AF60738, 0x3B01AAF1, 0x3A002FF8, 0x3A018806, 0x3A0245A1, 0x3A027100, 0x3852BF87, 0x384D56B2, 0x3847702D, 0x38411FB2,
		0xB4974C45, 0xB4AFEA79, 0xB4CB313D, 0xB4E96C78, 0x386373DA, 0x38641425, 0x386391FC, 0x3861D2F2, 0x39DEBEE3, 0x39D39AC8, 0x39C6E21C, 0x39B88B39, 0x3A522C90, 0x3A1BCFFD, 0x39C3C6C5, 0x3911A733, 0x3C90C623, 0x3C8FE563, 0x3C8ED8F5, 0x3C8DA182, 0x3B0782EF, 0x3B0C9087, 0x3B10D91A, 0x3B14626F, 0x3A02123D, 0x3A013274, 0x39FFB3F3, 0x39FC22F6, 0x383A77E4, 0x38338A46, 0x382C6738, 0x38251DF6,
		0xB5057939, 0xB518126A, 0xB52CB859, 0xB543A7D6, 0x385EBC2F, 0x385A3296, 0x38541AE7, 0x384C59E9, 0x39A88DCF, 0x3996E2FC, 0x3983856E, 0x395CE2F0, 0xB8E4B83A, 0xB9C22D3A, 0xBA2909B1, 0xBA746233, 0x3C8C3FD2, 0x3C8AB4C9, 0x3C890163, 0x3C8726B8, 0x3B1732AB, 0x3B19504C, 0x3B1AC21D, 0x3B1B8F31, 0x39F7C34F, 0x39F2A646, 0x39ECDD07, 0x39E67880, 0x381DBC95, 0x3816500C, 0x380EE42C, 0x380783B3,
		0xB55D243C, 0xB57977E0, 0xB58C7A2D, 0xB59DF966, 0x3842D493, 0x38377039, 0x382A12C0, 0x381AA2CF, 0x392F4A59, 0x38FC81E5, 0x389394F7, 0x378F7639, 0xBAA1879D, 0xBACA7E8B, 0xBAF50A9A, 0xBB108F8F, 0x3C8525F3, 0x3C83005C, 0x3C80B74E, 0x3C7C9870, 0x3B1BBED8, 0x3B1B589D, 0x3B1A6432, 0x3B18E975, 0x39DF8957, 0x39D81FDD, 0x39D04BFA, 0x39C81D21, 0x38003846, 0x37F21509, 0x37E40414, 0x37D64AF4,
		0x35F983EC, 0x35DEF504, 0x35C70089, 0x35B16A19, 0xB78CAB73, 0xB7BDECDF, 0xB7EA564A, 0xB8090801, 0x396E405D, 0x3929750B, 0x38CF876F, 0x38251871, 0x3B6FCBE2, 0x3B56FDE7, 0x3B3ED3ED, 0x3B275702, 0xBC66D5E6, 0xBC6C9C9A, 0xBC722C32, 0xBC77813E, 0xBB0E5FDE, 0xBB11A372, 0xBB1480FE, 0xBB16F05E, 0xB9A4F6AC, 0xB9AE0188, 0xB9B6E9CC, 0xB9BFA245, 0xB7A39FA8, 0xB7AF9836, 0xB7BC0C2D, 0xB7C8F518,
		0x364181A6, 0x362DB1ED, 0x361BBAC3, 0x360B76B8, 0x3756C531, 0x36960273, 0xB65894B5, 0xB72CDAF3, 0x3A077181, 0x39E6FA7A, 0x39C0531B, 0x399B039D, 0x3BAC5017, 0x3B9ED4C4, 0x3B91962B, 0x3B849A12, 0xBC4DDC25, 0xBC545AF8, 0xBC5AB165, 0xBC60DB9D, 0xBAFBEEB9, 0xBB028224, 0xBB06C723, 0xBB0ABE65, 0xB9806248, 0xB98980BA, 0xB992AABA, 0xB99BD5C6, 0xB771BF1F, 0xB782C5A5, 0xB78D335D, 0xB798277F,
		0x36932A6A, 0x3684C845, 0x366F480B, 0x365750E8, 0x385C7443, 0x382EB373, 0x3803A766, 0x37B6A3E3, 0x3A5C7860, 0x3A468CFD, 0x3A3107F1, 0x3A1BF9B5, 0x3BE41FE0, 0x3BD5F0A9, 0x3BC7E478, 0x3BBA0217, 0xBC329ADA, 0xBC39942A, 0xBC4074CD, 0xBC4738C8, 0xBAD35AB5, 0xBADE045D, 0xBAE85F7D, 0xBAF25D51, 0xB93A29E8, 0xB94B5508, 0xB95CDCEE, 0xB96EB257, 0xB72D135B, 0xB73CB0AD, 0xB74D542F, 0xB75F0298,
		0x36DACCDE, 0x36C69BC6, 0x36B3FE94, 0x36A2DFFA, 0x38D6EB1A, 0x38BAD2C1, 0x389FFCA1, 0x38867211, 0x3A9B579E, 0x3A8FF451, 0x3A849CEB, 0x3A72B855, 0x3C0ED606, 0x3C079C53, 0x3C006595, 0x3BF26B1C, 0xBC160ED7, 0xBC1D42E2, 0xBC246E38, 0xBC2B8CDF, 0xBAA6BB80, 0xBAB2140F, 0xBABD54D9, 0xBAC870E3, 0xB8F439F4, 0xB9095890, 0xB9191FEF, 0xB9296966, 0xB6F0A6B5, 0xB7041EB3, 0xB710D201, 0xB71E762F,
		0x36F0A6B5, 0x37041EB3, 0x3710D201, 0x371E762F, 0x38F439F4, 0x39095890, 0x39191FEF, 0x39296966, 0x3AA6BB80, 0x3AB2140F, 0x3ABD54D9, 0x3AC870E3, 0x3C160ED7, 0x3C1D42E2, 0x3C246E38, 0x3C2B8CDF, 0xBC0ED606, 0xBC079C53, 0xBC006595, 0xBBF26B1C, 0xBA9B579E, 0xBA8FF451, 0xBA849CEB, 0xBA72B855, 0xB8D6EB1A, 0xB8BAD2C1, 0xB89FFCA1, 0xB8867211, 0xB6DACCDE, 0xB6C69BC6, 0xB6B3FE94, 0xB6A2DFFA,
		0x372D135B, 0x373CB0AD, 0x374D542F, 0x375F0298, 0x393A29E8, 0x394B5508, 0x395CDCEE, 0x396EB257, 0x3AD35AB5, 0x3ADE045D, 0x3AE85F7D, 0x3AF25D51, 0x3C329ADA, 0x3C39942A, 0x3C4074CD, 0x3C4738C8, 0xBBE41FE0, 0xBBD5F0A9, 0xBBC7E478, 0xBBBA0217, 0xBA5C7860, 0xBA468CFD, 0xBA3107F1, 0xBA1BF9B5, 0xB85C7443, 0xB82EB373, 0xB803A766, 0xB7B6A3E3, 0xB6932A6A, 0xB684C845, 0xB66F480B, 0xB65750E8,
		0x3771BF1F, 0x3782C5A5, 0x378D335D, 0x3798277F, 0x39806248, 0x398980BA, 0x3992AABA, 0x399BD5C6, 0x3AFBEEB9, 0x3B028224, 0x3B06C723, 0x3B0ABE65, 0x3C4DDC25, 0x3C545AF8, 0x3C5AB165, 0x3C60DB9D, 0xBBAC5017, 0xBB9ED4C4, 0xBB91962B, 0xBB849A12, 0xBA077181, 0xB9E6FA7A, 0xB9C0531B, 0xB99B039D, 0xB756C531, 0xB6960273, 0x365894B5, 0x372CDAF3, 0xB64181A6, 0xB62DB1ED, 0xB61BBAC3, 0xB60B76B8,
		0x37A39FA8, 0x37AF9836, 0x37BC0C2D, 0x37C8F518, 0x39A4F6AC, 0x39AE0188, 0x39B6E9CC, 0x39BFA245, 0x3B0E5FDE, 0x3B11A372, 0x3B1480FE, 0x3B16F05E, 0x3C66D5E6, 0x3C6C9C9A, 0x3C722C32, 0x3C77813E, 0xBB6FCBE2, 0xBB56FDE7, 0xBB3ED3ED, 0xBB275702, 0xB96E405D, 0xB929750B, 0xB8CF876F, 0xB8251871, 0x378CAB73, 0x37BDECDF, 0x37EA564A, 0x38090801, 0xB5F983EC, 0xB5DEF504, 0xB5C70089, 0xB5B16A19,
		0x38003846, 0x37F21509, 0x37E40414, 0x37D64AF4, 0x39DF8957, 0x39D81FDD, 0x39D04BFA, 0x39C81D21, 0x3B1BBED8, 0x3B1B589D, 0x3B1A6432, 0x3B18E975, 0x3C8525F3, 0x3C83005C, 0x3C80B74E, 0x3C7C9870, 0xBAA1879D, 0xBACA7E8B, 0xBAF50A9A, 0xBB108F8F, 0x392F4A59, 0x38FC81E5, 0x389394F7, 0x378F7639, 0x3842D493, 0x38377039, 0x382A12C0, 0x381AA2CF, 0xB55D243C, 0xB57977E0, 0xB58C7A2D, 0xB59DF966,
		0x381DBC95, 0x3816500C, 0x380EE42C, 0x380783B3, 0x39F7C34F, 0x39F2A646, 0x39ECDD07, 0x39E67880, 0x3B1732AB, 0x3B19504C, 0x3B1AC21D, 0x3B1B8F31, 0x3C8C3FD2, 0x3C8AB4C9, 0x3C890163, 0x3C8726B8, 0xB8E4B83A, 0xB9C22D3A, 0xBA2909B1, 0xBA746233, 0x39A88DCF, 0x3996E2FC, 0x3983856E, 0x395CE2F0, 0x385EBC2F, 0x385A3296, 0x38541AE7, 0x384C59E9, 0xB5057939, 0xB518126A, 0xB52CB859, 0xB543A7D6,
		0x383A77E4, 0x38338A46, 0x382C6738, 0x38251DF6, 0x3A02123D, 0x3A013274, 0x39FFB3F3, 0x39FC22F6, 0x3B0782EF, 0x3B0C9087, 0x3B10D91A, 0x3B14626F, 0x3C90C623, 0x3C8FE563, 0x3C8ED8F5, 0x3C8DA182, 0x3A522C90, 0x3A1BCFFD, 0x39C3C6C5, 0x3911A733, 0x39DEBEE3, 0x39D39AC8, 0x39C6E21C, 0x39B88B39, 0x386373DA, 0x38641425, 0x386391FC, 0x3861D2F2, 0xB4974C45, 0xB4AFEA79, 0xB4CB313D, 0xB4E96C78,
		0x3852BF87, 0x384D56B2, 0x3847702D, 0x38411FB2, 0x3A002FF8, 0x3A018806, 0x3A0245A1, 0x3A027100, 0x3AD66AFD, 0x3AE710EB, 0x3AF60738, 0x3B01AAF1, 0x3C928A76, 0x3C925D0E, 0x3C92025C, 0x3C917A9B, 0x3AC40E79, 0x3AAFF57E, 0x3A9A1B50, 0x3A827C5B, 0x39FC6B06, 0x39F721ED, 0x39F075F8, 0x39E85949, 0x385795E0, 0x385BC3E8, 0x385F32D9, 0x3861CB00, 0xB415454D, 0xB435DD22, 0xB45A0AD7, 0xB481139A
	},

	{
		0x2EFAB22A, 0x2E9CFB8C, 0xB20F99DD, 0xB2D943D2, 0x33B58CD1, 0x338B2704, 0x35A5F2ED, 0x3626708B, 0x37A74CA8, 0x3794D2DD, 0x383D36FD, 0x385ECA1D, 0x3ACA01FA, 0x3AC1CC77, 0x3AB6B773, 0x3AAA9B3C, 0x3C9B014C, 0x3C9AC5D0, 0x3C9A4E0A, 0x3C999BE1, 0x3AD0B8E1, 0x3AD60556, 0x3AD8E0D6, 0x3ADB01AD, 0x3787166B, 0x377EB13C, 0x342D4DE8, 0xB6F0E547, 0x332201EC, 0x330A9DB6, 0xB3116093, 0xB281BA81,
		0xB3C411CC, 0xB494CC08, 0xB51A3F3A, 0xB4C756E5, 0x3677F738, 0x367B216D, 0x365AF368, 0x36FAE16F, 0x3880DE97, 0x3893E8F7, 0x38A77064, 0x38B44F33, 0x3A9C98BF, 0x3A8C96C6, 0x3A751596, 0x3A4CBE94, 0x3C98AF5A, 0x3C978910, 0x3C962A17, 0x3C949389, 0x3ADBBF61, 0x3ADB3E38, 0x3AD98C04, 0x3AD6BF5C, 0xB76949DE, 0xB7A61034, 0xB7D1807C, 0xB7C03C36, 0xB374C900, 0xB4A50163, 0xB5328803, 0xB652AE6E,
		0xB4EFE9D6, 0xB518EFB4, 0xB5291FF4, 0xB533419F, 0x371C641C, 0x373C3759, 0x375D0B28, 0x377C9F5D, 0x38C3E92F, 0x38D2470C, 0x38DF4ECE, 0x38EAD904, 0x3A202552, 0x39DE5E31, 0x396730C7, 0xB473FA8A, 0x3C92C6D3, 0x3C90C56A, 0x3C8E9100, 0x3C8C2B6C, 0x3AD2F5BD, 0x3ACE4522, 0x3AC8C2DE, 0x3AC28821, 0xB7C58938, 0xB7C08592, 0xB7B2D76E, 0xB79DD896, 0xB68C7FDC, 0xB6A944DD, 0xB6C63F22, 0xB6DE76E1,
		0xB535BF99, 0xB5313795, 0xB52642CB, 0xB515F3B5, 0x378CA6E9, 0x3798A6A4, 0x37A18C04, 0x37A6B6DC, 0x38F481DF, 0x38FC14E3, 0x3900A755, 0x3901BF9C, 0xB979A256, 0xBA01435B, 0xBA489EAB, 0xBA8A386C, 0x3C8996AD, 0x3C86D4E5, 0x3C83E857, 0x3C80D36D, 0x3ABBA878, 0x3AB43B4D, 0x3AAC55CF, 0x3AA40805, 0xB782F9AA, 0xB747DE5D, 0xB704CFC0, 0xB67F409E, 0xB6F0E9A9, 0xB6FCC925, 0xB700D43D, 0xB6FF7F18,
		0xB58D49F2, 0xB5801BA0, 0xB5601B2F, 0xB53CD667, 0x380F1770, 0x380D419D, 0x3808807F, 0x38010D4F, 0xB8E62619, 0xB8F1B438, 0xB8F8A386, 0xB8FBC0DE, 0x3B1BD689, 0x3B048DD8, 0x3ADCA4F6, 0x3AB25373, 0xBC663F1B, 0xBC6D782D, 0xBC747598, 0xBC7B31D5, 0xBA800525, 0xBA891973, 0xBA921800, 0xBA9AF0EE, 0x383E2D14, 0x384B4A7E, 0x3855244B, 0x385BA063, 0x369AF34A, 0x36AED839, 0x36BF270D, 0x36CAE26D,
		0xB583D736, 0xB596EEFC, 0xB59991BB, 0xB5964D0E, 0x37F00D8F, 0x380266AC, 0x3809972E, 0x380DDF04, 0xB88120DF, 0xB8A37212, 0xB8BF793D, 0xB8D58A2F, 0x3B816CC7, 0x3B67B0FB, 0x3B4D712D, 0x3B34252C, 0xBC477980, 0xBC4F6758, 0xBC5730D9, 0xBC5ED00F, 0xBA380D96, 0xBA49BC48, 0xBA5BB365, 0xBA6DD4E3, 0x37DE0F51, 0x38060DCD, 0x381B1C2E, 0x382E0858, 0x35FFAB86, 0x362B83DE, 0x365A53E3, 0x3684A934,
		0xB4AA085A, 0xB562F2BF, 0xB5D1818D, 0xB56F723A, 0x378391D3, 0x379497FC, 0x379CDF27, 0x37D7B591, 0x3817FFF5, 0x36DA891C, 0xB7A89D9F, 0xB830C297, 0x3BBBC6BC, 0x3BACA3BC, 0x3B9DD8F3, 0x3B8F6F61, 0xBC26CE49, 0xBC2F11AA, 0xBC374897, 0xBC3F6D4E, 0xB9EB5256, 0xBA057752, 0xBA15E04B, 0xBA26BAE1, 0x368B7F29, 0x36F2F170, 0x3734219F, 0x37AF4978, 0x328D2269, 0x33D85E88, 0x3483712C, 0x35AF6487,
		0x30DD8393, 0x303B82AF, 0xB3322763, 0xB3DCCB17, 0x3585E05C, 0x3518405D, 0x36E45B63, 0x373D52E9, 0x3939DF75, 0x39098666, 0x38DCA685, 0x3890AB24, 0x3BFB11E5, 0x3BEAE0D3, 0x3BDAF424, 0x3BCB3A31, 0xBC05B50A, 0xBC0DF00E, 0xBC1639C9, 0xBC1E8471, 0xB969F36D, 0xB98C74BA, 0xB9AFD819, 0xB9CCF4AE, 0xB5C4D1DC, 0xB5F7CA15, 0x33A6B680, 0x35F2E14C, 0xB13759A6, 0xB16818D3, 0x31EA5CE1, 0x317F4F91,
		0x313759A6, 0x316818D3, 0xB1EA5CE1, 0xB17F4F91, 0x35C4D1DC, 0x35F7CA15, 0xB3A6B680, 0xB5F2E14C, 0x3969F36D, 0x398C74BA, 0x39AFD819, 0x39CCF4AE, 0x3C05B50A, 0x3C0DF00E, 0x3C1639C9, 0x3C1E8471, 0xBBFB11E5, 0xBBEAE0D3, 0xBBDAF424, 0xBBCB3A31, 0xB939DF75, 0xB9098666, 0xB8DCA685, 0xB890AB24, 0xB585E05C, 0xB518405D, 0xB6E45B63, 0xB73D52E9, 0xB0DD8393, 0xB03B82AF, 0x33322763, 0x33DCCB17,
		0xB28D2269, 0xB3D85E88, 0xB483712C, 0xB5AF6487, 0xB68B7F29, 0xB6F2F170, 0xB734219F, 0xB7AF4978, 0x39EB5256, 0x3A057752, 0x3A15E04B, 0x3A26BAE1, 0x3C26CE49, 0x3C2F11AA, 0x3C374897, 0x3C3F6D4E, 0xBBBBC6BC, 0xBBACA3BC, 0xBB9DD8F3, 0xBB8F6F61, 0xB817FFF5, 0xB6DA891C, 0x37A89D9F, 0x3830C297, 0xB78391D3, 0xB79497FC, 0xB79CDF27, 0xB7D7B591, 0x34AA085A, 0x3562F2BF, 0x35D1818D, 0x356F723A,
		0xB5FFAB86, 0xB62B83DE, 0xB65A53E3, 0xB684A934, 0xB7DE0F51, 0xB8060DCD, 0xB81B1C2E, 0xB82E0858, 0x3A380D96, 0x3A49BC48, 0x3A5BB365, 0x3A6DD4E3, 0x3C477980, 0x3C4F6758, 0x3C5730D9, 0x3C5ED00F, 0xBB816CC7, 0xBB67B0FB, 0xBB4D712D, 0xBB34252C, 0x388120DF, 0x38A37212, 0x38BF793D, 0x38D58A2F, 0xB7F00D8F, 0xB80266AC, 0xB809972E, 0xB80DDF04, 0x3583D736, 0x3596EEFC, 0x359991BB, 0x35964D0E,
		0xB69AF34A, 0xB6AED839, 0xB6BF270D, 0xB6CAE26D, 0xB83E2D14, 0xB84B4A7E, 0xB855244B, 0xB85BA063, 0x3A800525, 0x3A891973, 0x3A921800, 0x3A9AF0EE, 0x3C663F1B, 0x3C6D782D, 0x3C747598, 0x3C7B31D5, 0xBB1BD689, 0xBB048DD8, 0xBADCA4F6, 0xBAB25373, 0x38E62619, 0x38F1B438, 0x38F8A386, 0x38FBC0DE, 0xB80F1770, 0xB80D419D, 0xB808807F, 0xB8010D4F, 0x358D49F2, 0x35801BA0, 0x35601B2F, 0x353CD667,
		0xB6F0E9A9, 0xB6FCC925, 0xB700D43D, 0xB6FF7F18, 0xB782F9AA, 0xB747DE5D, 0xB704CFC0, 0xB67F409E, 0x3ABBA878, 0x3AB43B4D, 0x3AAC55CF, 0x3AA40805, 0x3C8996AD, 0x3C86D4E5, 0x3C83E857, 0x3C80D36D, 0xB979A256, 0xBA01435B, 0xBA489EAB, 0xBA8A386C, 0x38F481DF, 0x38FC14E3, 0x3900A755, 0x3901BF9C, 0x378CA6E9, 0x3798A6A4, 0x37A18C04, 0x37A6B6DC, 0xB535BF99, 0xB5313795, 0xB52642CB, 0xB515F3B5,
		0xB68C7FDC, 0xB6A944DD, 0xB6C63F22, 0xB6DE76E1, 0xB7C58938, 0xB7C08592, 0xB7B2D76E, 0xB79DD896, 0x3AD2F5BD, 0x3ACE4522, 0x3AC8C2DE, 0x3AC28821, 0x3C92C6D3, 0x3C90C56A, 0x3C8E9100, 0x3C8C2B6C, 0x3A202552, 0x39DE5E31, 0x396730C7, 0xB473FA8A, 0x38C3E92F, 0x38D2470C, 0x38DF4ECE, 0x38EAD904, 0x371C641C, 0x373C3759, 0x375D0B28, 0x377C9F5D, 0xB4EFE9D6, 0xB518EFB4, 0xB5291FF4, 0xB533419F,
		0xB374C900, 0xB4A50163, 0xB5328803, 0xB652AE6E, 0xB76949DE, 0xB7A61034, 0xB7D1807C, 0xB7C03C36, 0x3ADBBF61, 0x3ADB3E38, 0x3AD98C04, 0x3AD6BF5C, 0x3C98AF5A, 0x3C978910, 0x3C962A17, 0x3C949389, 0x3A9C98BF, 0x3A8C96C6, 0x3A751596, 0x3A4CBE94, 0x3880DE97, 0x3893E8F7, 0x38A77064, 0x38B44F33, 0x3677F738, 0x367B216D, 0x365AF368, 0x36FAE16F, 0xB3C411CC, 0xB494CC08, 0xB51A3F3A, 0xB4C756E5,
		0x332201EC, 0x330A9DB6, 0xB3116093, 0xB281BA81, 0x3787166B, 0x377EB13C, 0x342D4DE8, 0xB6F0E547, 0x3AD0B8E1, 0x3AD60556, 0x3AD8E0D6, 0x3ADB01AD, 0x3C9B014C, 0x3C9AC5D0, 0x3C9A4E0A, 0x3C999BE1, 0x3ACA01FA, 0x3AC1CC77, 0x3AB6B773, 0x3AAA9B3C, 0x37A74CA8, 0x3794D2DD, 0x383D36FD, 0x385ECA1D, 0x33B58CD1, 0x338B2704, 0x35A5F2ED, 0x3626708B, 0x2EFAB22A, 0x2E9CFB8C, 0xB20F99DD, 0xB2D943D2
	}

};

const unsigned int Audio::DTS::speaker_counts[] = { 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 2, 1, 2, 1, 2 };




