; safe-fail audio codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE

EXTERN ?adpcm_tab@DTS@Audio@@0QAY03$$CBIA:DWORD
EXTERN ?lfe_interpol@DTS@Audio@@0QAY0CAA@$$CBIA:DWORD
EXTERN ?synth_cos@DTS@Audio@@0PAMA:DWORD


.CONST
ALIGN 16

int_4	DWORD	4, 4, 4, 4


.CODE

ALIGN 16
?GetVector@DTS@Audio@@CAXPEAHHH@Z	PROC
	MOV eax, edx
	
	MOV r9d, r8d
	DEC r9d
	SHR r9d, 1

	XOR edx, edx
	DIV r8d
	SUB edx, r9d
	MOV [rcx], edx

	XOR edx, edx
	DIV r8d
	SUB edx, r9d
	MOV [rcx + 4], edx

	XOR edx, edx
	DIV r8d
	SUB edx, r9d
	MOV [rcx + 8], edx

	XOR edx, edx
	DIV r8d
	SUB edx, r9d
	MOV [rcx + 12], edx	

	RET
?GetVector@DTS@Audio@@CAXPEAHHH@Z	ENDP


ALIGN 16
?ScaleChannel@DTS@Audio@@CAXPEAMI@Z	PROC
	
	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]
	

	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]


	CVTDQ2PS xmm0, [rcx]
	MOVAPS xmm1, [rcx + 16]
	MOVAPS xmm2, xmm1
	MOVAPS xmm3, xmm1
	SHUFPS xmm1, xmm1, 0
	SHUFPS xmm2, xmm2, 0AAh
	SHUFPS xmm3, xmm3, 0FFh	
	MULPS xmm1, xmm2
	MULPS xmm0, xmm3
	MULPS xmm0, xmm1
	MOVAPS [rcx], xmm0
	LEA rcx, [rcx + 32]



	DEC edx
	JNZ ?ScaleChannel@DTS@Audio@@CAXPEAMI@Z


	RET
?ScaleChannel@DTS@Audio@@CAXPEAMI@Z	ENDP


ALIGN 16
?InverseADPCM@DTS@Audio@@CAXPEAM_KII@Z	PROC

	LEA r11, [?adpcm_tab@DTS@Audio@@0QAY03$$CBIA]
	MOV r10, rcx

	MOV ecx, r9d
	SHR r9d, 16

	AND ecx, 0FFh
	MOV ch, cl
	SHR cl, 1
	ADD cl, 6

	MOV eax, r9d
	SHL r9d, 4
	LEA r9, [r10 + r9 + 4096*4]
	
	SHL rax, cl
	LEA r10, [r10 + rax]
	
	SHL r8, 4


	MOVAPS xmm2, [r9 + rdx*4] ; history points
	MOVAPS xmm1, [r11 + r8] ; adpcm coeff

	MOVAPS xmm8, xmm1
	MOVAPS xmm9, xmm1
	MOVAPS xmm10, xmm1

	SHUFPS xmm8, xmm8, 0FFh
	SHUFPS xmm9, xmm9, 0AAh
	SHUFPS xmm10, xmm10, 055h


	SHR ecx, 8


	align 16
		ss_loop:


			MOVAPS xmm0, xmm2
			MOVAPS xmm3, xmm2
			MULPS xmm2, xmm1 ; x0
	
			MOVAPS xmm11, [r10]
	
			MOVSS xmm3, xmm11
			SHUFPS xmm3, xmm3, 39h			
			MOVAPS xmm4, xmm3
			MULPS xmm3, xmm1
	
			MOVAPS xmm5, xmm2
			MULPS xmm5, xmm8

			ADDPS xmm3, xmm5 ; x1



			SHUFPS xmm11, xmm11, 39h
			MOVSS xmm4, xmm11
			SHUFPS xmm4, xmm4, 39h
			MOVAPS xmm5, xmm4
			MULPS xmm4, xmm1
	
			MOVAPS xmm6, xmm2
			MOVAPS xmm7, xmm3

			MULPS xmm6, xmm9
			MULPS xmm7, xmm8

			ADDPS xmm4, xmm6
			ADDPS xmm4, xmm7 ; x2


			SHUFPS xmm11, xmm11, 39h
			MOVSS xmm5, xmm11
			SHUFPS xmm5, xmm5, 39h
			MULPS xmm5, xmm1


			MOVAPS xmm6, xmm2
			MOVAPS xmm7, xmm3
			MOVAPS xmm0, xmm4

			MULPS xmm6, xmm10
			MULPS xmm7, xmm9
			MULPS xmm0, xmm8


			ADDPS xmm0, xmm5
			ADDPS xmm6, xmm7
			ADDPS xmm0, xmm6 ; x3



			HADDPS xmm2, xmm3
			HADDPS xmm4, xmm0
			HADDPS xmm2, xmm4

			SHUFPS xmm11, xmm11, 4Eh
			ADDPS xmm2, xmm11
			MOVAPS [r10], xmm2

			

			LEA r10, [r10 + 32]





			MOVAPS xmm0, xmm2			
			MOVAPS xmm3, xmm2
			MULPS xmm2, xmm1 ; x0
	
			MOVAPS xmm11, [r10]
	
			MOVSS xmm3, xmm11
			SHUFPS xmm3, xmm3, 39h
			MOVAPS xmm4, xmm3
			MULPS xmm3, xmm1
	
			MOVAPS xmm5, xmm2
			MULPS xmm5, xmm8

			ADDPS xmm3, xmm5 ; x1



			SHUFPS xmm11, xmm11, 39h
			MOVSS xmm4, xmm11
			SHUFPS xmm4, xmm4, 39h
			MOVAPS xmm5, xmm4
			MULPS xmm4, xmm1

			MOVAPS xmm6, xmm2
			MOVAPS xmm7, xmm3

			MULPS xmm6, xmm9
			MULPS xmm7, xmm8

			ADDPS xmm4, xmm6
			ADDPS xmm4, xmm7 ; x2


			SHUFPS xmm11, xmm11, 39h
			MOVSS xmm5, xmm11
			SHUFPS xmm5, xmm5, 39h
			MULPS xmm5, xmm1


			MOVAPS xmm6, xmm2
			MOVAPS xmm7, xmm3
			MOVAPS xmm0, xmm4

			MULPS xmm6, xmm10
			MULPS xmm7, xmm9
			MULPS xmm0, xmm8


			ADDPS xmm0, xmm5
			ADDPS xmm6, xmm7
			ADDPS xmm0, xmm6 ; xmm3



			HADDPS xmm2, xmm3
			HADDPS xmm4, xmm0
			HADDPS xmm2, xmm4

			SHUFPS xmm11, xmm11, 4Eh
			ADDPS xmm2, xmm11
			MOVAPS [r10], xmm2


			LEA r10, [r10 + 32]


		DEC ecx
		JNZ ss_loop

		MOVNTPS [r9], xmm2
	
	RET
?InverseADPCM@DTS@Audio@@CAXPEAM_KII@Z	ENDP



ALIGN 16
?InterpolateLFE@DTS@Audio@@CAXPEAM_KII@Z	PROC
	LEA r11, [rcx + 1024*2*4]
	LEA r10, [?lfe_interpol@DTS@Audio@@0QAY0CAA@$$CBIA]
		
	LEA rax, [rcx + 1024*4 - 16]

	MOVAPS xmm0, [rcx] ; scale fac

	
	SHR r8d, 1
	JNZ lfe_64
		MOVAPS xmm1, [rcx + rdx*4 + 1024*4] ; history

		MOV r8, r10
		MOV edx, 0408h

		

		SHR r9d, 1
		JNZ loop_128
			MOV edx, 0208h
		
	align 16
		loop_128:
			LEA rcx, [rcx + 16]
			MOVAPS xmm2, [rcx]
			MULPS xmm2, xmm0
			SHL r9, 4
			MOVNTPS [rax + r9], xmm2
			SHR r9, 4

		align 16
			loop_128_xsam:
				MOVSS xmm1, xmm2
				SHUFPS xmm1, xmm1, 39h
				
				SHUFPS xmm2, xmm2, 39h

		align 16
			loop_128ip_8:

				MOVAPS xmm4, [r10]
				MOVAPS xmm5, [r10 + 16]
				MOVAPS xmm6, [r10 + 32]
				MOVAPS xmm7, [r10 + 48]
				MULPS xmm4, xmm1
				MULPS xmm5, xmm1
				MULPS xmm6, xmm1
				MULPS xmm7, xmm1
				HADDPS xmm4, xmm5
				HADDPS xmm6, xmm7
				HADDPS xmm4, xmm6



				MOVAPS xmm5, [r10 + 64]
				MOVAPS xmm6, [r10 + 80]
				MOVAPS xmm7, [r10 + 96]
				MOVAPS xmm8, [r10 + 112]
				MULPS xmm5, xmm1
				MULPS xmm6, xmm1
				MULPS xmm7, xmm1
				MULPS xmm8, xmm1
				HADDPS xmm5, xmm6
				HADDPS xmm7, xmm8
				HADDPS xmm5, xmm7



				MOVAPS xmm6, [r10 + 128]
				MOVAPS xmm7, [r10 + 144]
				MOVAPS xmm8, [r10 + 160]
				MOVAPS xmm9, [r10 + 176]
				MULPS xmm6, xmm1
				MULPS xmm7, xmm1
				MULPS xmm8, xmm1
				MULPS xmm9, xmm1
				HADDPS xmm6, xmm7
				HADDPS xmm8, xmm9
				HADDPS xmm6, xmm8



				MOVAPS xmm7, [r10 + 192]
				MOVAPS xmm8, [r10 + 208]
				MOVAPS xmm9, [r10 + 224]
				MOVAPS xmm3, [r10 + 240]
				MULPS xmm7, xmm1
				MULPS xmm8, xmm1
				MULPS xmm9, xmm1
				MULPS xmm3, xmm1
				HADDPS xmm7, xmm8
				HADDPS xmm9, xmm3
				HADDPS xmm7, xmm9


				MOVAPS [r11], xmm4
				MOVAPS [r11 + 16], xmm5
				MOVAPS [r11 + 32], xmm6
				MOVAPS [r11 + 48], xmm7

				LEA r11, [r11 + 64]
				LEA r10, [r10 + 256]
			DEC dl
			JNZ loop_128ip_8

			MOV dl, 8
			MOV r10, r8

			DEC dh
			JNZ loop_128_xsam

			MOV edx, 0408h
			

		DEC r9d
		JG loop_128


		
		RET

align 16
	lfe_64:		; 4, 8, 16 samples
		LEA r10, [r10 + 2048]

		MOVAPS xmm2, [rcx + rdx*4+ 1024] ; history
		MOVAPS xmm1, [rcx + rdx*4 + 1024 + 16] ; history

		MOV r8, r10
		MOV edx, 0404h
		
	align 16
		loop_64:
			LEA rcx, [rcx + 16]
			MOVAPS xmm3, [rcx]
			MULPS xmm3, xmm0
			SHL r9, 4
			MOVNTPS [rax + r9], xmm3
			SHR r9, 4

		align 16
			loop_64_4sam:				
				MOVSS xmm1, xmm2
				SHUFPS xmm1, xmm1, 39h	
	
				MOVSS xmm2, xmm3
				SHUFPS xmm2, xmm2, 39h

				SHUFPS xmm3, xmm3, 39h

		align 16
			loop_64ip_4:

				MOVAPS xmm4, [r10]
				MOVAPS xmm8, [r10 + 16]
				MOVAPS xmm5, [r10 + 32]
				MOVAPS xmm9, [r10 + 48]
				MULPS xmm4, xmm1
				MULPS xmm8, xmm2
				MULPS xmm5, xmm1
				MULPS xmm9, xmm2
				MOVAPS xmm6, [r10 + 64]
				MOVAPS xmm10, [r10 + 80]
				MOVAPS xmm7, [r10 + 96]
				MOVAPS xmm11, [r10 + 112]
				MULPS xmm6, xmm1
				MULPS xmm10, xmm2
				MULPS xmm7, xmm1
				MULPS xmm11, xmm2
				ADDPS xmm4, xmm8
				ADDPS xmm5, xmm9
				ADDPS xmm6, xmm10
				ADDPS xmm7, xmm11
				HADDPS xmm4, xmm5
				HADDPS xmm6, xmm7
				HADDPS xmm4, xmm6


				MOVAPS xmm5, [r10 + 128]
				MOVAPS xmm9, [r10 + 144]
				MOVAPS xmm6, [r10 + 160]
				MOVAPS xmm10, [r10 + 176]
				MULPS xmm5, xmm1
				MULPS xmm9, xmm2
				MULPS xmm6, xmm1
				MULPS xmm10, xmm2
				MOVAPS xmm7, [r10 + 192]
				MOVAPS xmm11, [r10 + 208]
				MOVAPS xmm8, [r10 + 224]
				MOVAPS xmm12, [r10 + 240]
				MULPS xmm7, xmm1
				MULPS xmm11, xmm2
				MULPS xmm8, xmm1
				MULPS xmm12, xmm2
				ADDPS xmm5, xmm9
				ADDPS xmm6, xmm10
				ADDPS xmm7, xmm11
				ADDPS xmm8, xmm12
				HADDPS xmm5, xmm6
				HADDPS xmm7, xmm8
				HADDPS xmm5, xmm7



				MOVAPS xmm6, [r10 + 256]
				MOVAPS xmm10, [r10 + 272]
				MOVAPS xmm7, [r10 + 288]
				MOVAPS xmm11, [r10 + 304]
				MULPS xmm6, xmm1
				MULPS xmm10, xmm2
				MULPS xmm7, xmm1
				MULPS xmm11, xmm2
				MOVAPS xmm8, [r10 + 320]
				MOVAPS xmm12, [r10 + 336]
				MOVAPS xmm9, [r10 + 352]
				MOVAPS xmm13, [r10 + 368]
				MULPS xmm8, xmm1
				MULPS xmm12, xmm2
				MULPS xmm9, xmm1
				MULPS xmm13, xmm2
				ADDPS xmm6, xmm10
				ADDPS xmm7, xmm11
				ADDPS xmm8, xmm12
				ADDPS xmm9, xmm13
				HADDPS xmm6, xmm7
				HADDPS xmm8, xmm9
				HADDPS xmm6, xmm8



				MOVAPS xmm7, [r10 + 384]
				MOVAPS xmm11, [r10 + 400]
				MOVAPS xmm8, [r10 + 416]
				MOVAPS xmm12, [r10 + 432]
				MULPS xmm7, xmm1
				MULPS xmm11, xmm2
				MULPS xmm8, xmm1
				MULPS xmm12, xmm2
				MOVAPS xmm9, [r10 + 448]
				MOVAPS xmm13, [r10 + 464]
				MOVAPS xmm10, [r10 + 480]
				MOVAPS xmm14, [r10 + 496]
				MULPS xmm9, xmm1
				MULPS xmm13, xmm2
				MULPS xmm10, xmm1
				MULPS xmm14, xmm2
				ADDPS xmm7, xmm11
				ADDPS xmm8, xmm12
				ADDPS xmm9, xmm13
				ADDPS xmm10, xmm14
				HADDPS xmm7, xmm8
				HADDPS xmm9, xmm10
				HADDPS xmm7, xmm9




				MOVAPS [r11], xmm4
				MOVAPS [r11 + 16], xmm5
				MOVAPS [r11 + 32], xmm6
				MOVAPS [r11 + 48], xmm7

				LEA r11, [r11 + 64]
				LEA r10, [r10 + 512]
			DEC dl
			JNZ loop_64ip_4

			MOV dl, 4
			MOV r10, r8

			DEC dh
			JNZ loop_64_4sam

			MOV edx, 0404h
			

		DEC r9d
		JNZ loop_64
		

	RET
?InterpolateLFE@DTS@Audio@@CAXPEAM_KII@Z	ENDP



ALIGN 16
?InterpolateQMF@DTS@Audio@@CAXPEAM_K1PEBI@Z	PROC
	PUSH rbp

	PUSH rbx

	PUSH rdi
	PUSH rsi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	LEA r11, [?synth_cos@DTS@Audio@@0PAMA]
	MOV r12, r11

	MOV r13, rcx
	MOV rsi, rcx
	LEA rdi, [rcx + 2*1024*4] ; rax ptr

	MOV rbp, r8
	SHR rbp, 32

	MOV eax, r8d
	SHL eax, 1 ; band counter
	SHL r8d, 6 ; band offset



align 16
	cos_loop:

		XORPS xmm10, xmm10
	
		MOV ebx, 0410h

	align 16
		pack_4_loop:

			MOVAPS xmm0, [rcx]
			MOVAPS xmm1, [rcx + r8]
			LEA rcx, [rcx + r8*2]
			MOVAPS xmm2, [rcx]
			MOVAPS xmm3, [rcx + r8]
			LEA rcx, [rcx + r8*2]
			MOVAPS xmm4, [rcx]
			MOVAPS xmm5, [rcx + r8]
			LEA rcx, [rcx + r8*2]
			MOVAPS xmm6, [rcx]
			MOVAPS xmm7, [rcx + r8]
			LEA rcx, [rcx + r8*2]


			MOVAPS xmm11, xmm7

			ADDPS xmm7, xmm6
			ADDPS xmm6, xmm5
			ADDPS xmm5, xmm4
			ADDPS xmm4, xmm3
			ADDPS xmm3, xmm2
			ADDPS xmm2, xmm1
			ADDPS xmm1, xmm0
			ADDPS xmm0, xmm10

			MOVAPS xmm10, xmm11
						

		align 16
			ab_4_loop:
				MOVAPS xmm8, [rdi + 1024*4]
				MOVAPS xmm9, [rdi + 1024*4 + 16]

				MOVAPS xmm12, [r11]
				LEA r11, [r11 + 16]
				MOVAPS xmm13, xmm12
				MOVAPS xmm14, xmm12
				MOVAPS xmm15, xmm12

				SHUFPS xmm12, xmm12, 0
				SHUFPS xmm13, xmm13, 55h
				SHUFPS xmm14, xmm14, 0AAh
				SHUFPS xmm15, xmm15, 0FFh

				MULPS xmm12, xmm1
				MULPS xmm13, xmm3
				MULPS xmm14, xmm5
				MULPS xmm15, xmm7

				ADDPS xmm12, xmm13
				ADDPS xmm14, xmm15
				ADDPS xmm12, xmm14

				ADDPS xmm8, xmm12
				

				MOVAPS xmm12, [r11]
				LEA r11, [r11 + 16]
				MOVAPS xmm13, xmm12
				MOVAPS xmm14, xmm12
				MOVAPS xmm15, xmm12

				SHUFPS xmm12, xmm12, 0
				SHUFPS xmm13, xmm13, 55h
				SHUFPS xmm14, xmm14, 0AAh
				SHUFPS xmm15, xmm15, 0FFh

				MULPS xmm12, xmm0
				MULPS xmm13, xmm2
				MULPS xmm14, xmm4
				MULPS xmm15, xmm6

				ADDPS xmm12, xmm13
				ADDPS xmm14, xmm15
				ADDPS xmm12, xmm14
								
				ADDPS xmm9, xmm12


				MOVAPS [rdi + 1024*4], xmm8 ; A
				MOVAPS [rdi + 1024*4 + 16], xmm9 ; B

				LEA rdi, [rdi + 32]
				
			DEC bl
			JNZ ab_4_loop

			MOV bl, 16
			LEA rdi, [rdi - 32*16]
			
		DEC bh
		JNZ pack_4_loop

		; ab 4*16
		

		MOV bl, 4
	align 16
		raX_loop:
			MOVAPS xmm0, [rdi + 1024*4]
			MOVAPS xmm1, xmm0
			ADDPS xmm0, [rdi + 1024*4 + 16] ; A + B
			SUBPS xmm1, [rdi + 1024*4 + 16] ; A - B
			MOVAPS xmm2, [rdi + 1024*4 + 32]
			MOVAPS xmm3, xmm2
			ADDPS xmm2, [rdi + 1024*4 + 48]
			SUBPS xmm3, [rdi + 1024*4 + 48]
			MOVAPS xmm4, [rdi + 1024*4 + 64]
			MOVAPS xmm5, xmm4
			ADDPS xmm4, [rdi + 1024*4 + 80]
			SUBPS xmm5, [rdi + 1024*4 + 80]
			MOVAPS xmm6, [rdi + 1024*4 + 96]
			MOVAPS xmm7, xmm6
			ADDPS xmm6, [rdi + 1024*4 + 112]
			SUBPS xmm7, [rdi + 1024*4 + 112]


			MOVAPS xmm12, [r11]
			LEA r11, [r11 + 16]
			MOVAPS xmm13, xmm12
			MOVAPS xmm14, xmm12
			MOVAPS xmm15, xmm12
			SHUFPS xmm12, xmm12, 0
			SHUFPS xmm13, xmm13, 55h
			SHUFPS xmm14, xmm14, 0AAh
			SHUFPS xmm15, xmm15, 0FFh

			MULPS xmm0, xmm12
			MULPS xmm2, xmm13
			MULPS xmm4, xmm14
			MULPS xmm6, xmm15


			MOVAPS xmm12, [r11]
			LEA r11, [r11 + 16]
			MOVAPS xmm13, xmm12
			MOVAPS xmm14, xmm12
			MOVAPS xmm15, xmm12
			SHUFPS xmm12, xmm12, 0
			SHUFPS xmm13, xmm13, 55h
			SHUFPS xmm14, xmm14, 0AAh
			SHUFPS xmm15, xmm15, 0FFh

			MULPS xmm1, xmm12
			MULPS xmm3, xmm13
			MULPS xmm5, xmm14
			MULPS xmm7, xmm15


			MOVAPS xmm8, xmm0
			SUBPS xmm0, xmm1
			ADDPS xmm8, xmm1
			MOVAPS xmm9, xmm2
			SUBPS xmm2, xmm3
			ADDPS xmm9, xmm3
			MOVAPS xmm10, xmm4
			SUBPS xmm4, xmm5
			ADDPS xmm10, xmm5
			MOVAPS xmm11, xmm6
			SUBPS xmm6, xmm7
			ADDPS xmm11, xmm7



; transpose
			MOVAPS xmm1, xmm0
			UNPCKLPS xmm0, xmm2 ; 1 5 2 6
			UNPCKHPS xmm1, xmm2 ; 3 7 4 8
			MOVAPS xmm3, xmm4
			UNPCKLPS xmm3, xmm6 ; 9 13 10 14
			UNPCKHPS xmm4, xmm6 ; 11 15 12 16
			
			MOVAPS xmm2, xmm0
			UNPCKLPD xmm0, xmm3
			UNPCKHPD xmm2, xmm3
			MOVAPS xmm3, xmm1
			UNPCKLPD xmm1, xmm4
			UNPCKHPD xmm3, xmm4

			MOVAPS [rdi + 1024*4], xmm0
			MOVAPS [rdi + 1024*4 + 16], xmm2
			MOVAPS [rdi + 1024*4 + 32], xmm1
			MOVAPS [rdi + 1024*4 + 48], xmm3


			MOVAPS xmm0, xmm8
			UNPCKLPS xmm0, xmm9 ; 1 5 2 6
			UNPCKHPS xmm8, xmm9 ; 3 7 4 8
			MOVAPS xmm2, xmm10
			UNPCKLPS xmm2, xmm11 ; 9 13 10 14
			UNPCKHPS xmm10, xmm11 ; 11 15 12 16
			
			MOVAPS xmm1, xmm0
			UNPCKLPD xmm0, xmm2
			UNPCKHPD xmm1, xmm2
			MOVAPS xmm2, xmm8
			UNPCKLPD xmm2, xmm10
			UNPCKHPD xmm8, xmm10


			MOVAPS [rdi + 1024*4 + 64], xmm0
			MOVAPS [rdi + 1024*4 + 80], xmm1
			MOVAPS [rdi + 1024*4 + 96], xmm2
			MOVAPS [rdi + 1024*4 + 112], xmm8

			LEA rdi, [rdi + 128]

		DEC bl
		JNZ raX_loop

; shuffle 

		MOVAPS xmm0, [rdi + 1024*4 - 512 + 16]
		MOVAPS xmm1, [rdi + 1024*4 - 512 + 32]
		MOVAPS xmm2, [rdi + 1024*4 - 512 + 48]

		MOVAPS xmm3, [rdi + 1024*4 - 512 + 80]
		MOVAPS xmm4, [rdi + 1024*4 - 512 + 96]
		MOVAPS xmm5, [rdi + 1024*4 - 512 + 112]

		MOVAPS xmm6, [rdi + 1024*4 - 512 + 128]
		MOVAPS [rdi + 1024*4 - 512 + 128], xmm0
		MOVAPS xmm7, [rdi + 1024*4 - 512 + 160]
		MOVAPS xmm8, [rdi + 1024*4 - 512 + 176]
		MOVAPS xmm9, [rdi + 1024*4 - 512 + 192]
		MOVAPS [rdi + 1024*4 - 512 + 192], xmm3

		MOVAPS xmm0, [rdi + 1024*4 - 512 + 224]
		MOVAPS xmm3, [rdi + 1024*4 - 512 + 240]

		MOVAPS xmm10, [rdi + 1024*4 - 512 + 256]
		MOVAPS [rdi + 1024*4 - 512 + 256], xmm1
		MOVAPS xmm11, [rdi + 1024*4 - 512 + 272]
		MOVAPS [rdi + 1024*4 - 512 + 272], xmm7
		MOVAPS xmm12, [rdi + 1024*4 - 512 + 304]

		MOVAPS xmm1, [rdi + 1024*4 - 512 + 320]
		MOVAPS [rdi + 1024*4 - 512 + 320], xmm4
		MOVAPS xmm7, [rdi + 1024*4 - 512 + 336]
		MOVAPS [rdi + 1024*4 - 512 + 336], xmm0
		MOVAPS xmm13, [rdi + 1024*4 - 512 + 368]
		
		MOVAPS xmm4, [rdi + 1024*4 - 512 + 384]
		MOVAPS [rdi + 1024*4 - 512 + 384], xmm2
		MOVAPS xmm0, [rdi + 1024*4 - 512 + 400]
		MOVAPS [rdi + 1024*4 - 512 + 400], xmm8
		MOVAPS xmm14, [rdi + 1024*4 - 512 + 416]
		MOVAPS [rdi + 1024*4 - 512 + 416], xmm12

		MOVAPS xmm2, [rdi + 1024*4 - 512 + 448]
		MOVAPS [rdi + 1024*4 - 512 + 448], xmm5
		MOVAPS xmm8, [rdi + 1024*4 - 512 + 464]
		MOVAPS [rdi + 1024*4 - 512 + 464], xmm3
		MOVAPS xmm12, [rdi + 1024*4 - 512 + 480]
		MOVAPS [rdi + 1024*4 - 512 + 480], xmm13


		MOVAPS [rdi + 1024*4 - 512 + 16], xmm6
		MOVAPS [rdi + 1024*4 - 512 + 32], xmm10
		MOVAPS [rdi + 1024*4 - 512 + 48], xmm4

		MOVAPS [rdi + 1024*4 - 512 + 80], xmm9
		MOVAPS [rdi + 1024*4 - 512 + 96], xmm1
		MOVAPS [rdi + 1024*4 - 512 + 112], xmm2

		MOVAPS [rdi + 1024*4 - 512 + 160], xmm11
		MOVAPS [rdi + 1024*4 - 512 + 176], xmm0
		MOVAPS [rdi + 1024*4 - 512 + 224], xmm7
		MOVAPS [rdi + 1024*4 - 512 + 240], xmm8

		MOVAPS [rdi + 1024*4 - 512 + 304], xmm14
		MOVAPS [rdi + 1024*4 - 512 + 368], xmm12



		MOV r11, r12

		LEA rsi, [rsi + 32]
		MOV rcx, rsi

	DEC eax
	JNZ cos_loop



	





	LEA rsi, [r13 + 3*1024*4]
	LEA rdi, [r13 + 2*1024*4]



	LEA rax, [rdx + rbp]
	LEA r13, [rsi + rax*4 - 32*4]
	
	

	
	

; copy previous
	
	MOVAPS xmm0, [rdi + rax*4]
	MOVAPS xmm1, [rdi + rax*4 + 16]
	MOVAPS xmm2, [rdi + rax*4 + 32]
	MOVAPS xmm3, [rdi + rax*4 + 48]
	MOVAPS xmm4, [rdi + rax*4 + 64]
	MOVAPS xmm5, [rdi + rax*4 + 80]
	MOVAPS xmm6, [rdi + rax*4 + 96]
	MOVAPS xmm7, [rdi + rax*4 + 112]

	MOVAPS [rdi], xmm0
	MOVAPS [rdi + 16], xmm1
	MOVAPS [rdi + 32], xmm2
	MOVAPS [rdi + 48], xmm3
	MOVAPS [rdi + 64], xmm4
	MOVAPS [rdi + 80], xmm5
	MOVAPS [rdi + 96], xmm6
	MOVAPS [rdi + 112], xmm7




	MOV r12, r9
	MOV r11, rsi
	MOV r10, rsi
	SUB r13, r10
	MOV rbx, r10


	SHR r8d, 3 ; loop counter

	MOV eax, 4
	

align 16
	filter_loop_1:

		MOV rsi, r11
		MOVAPS xmm0, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
		
		MOVAPS xmm1, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
			
		MOVAPS xmm2, [rsi]
		
		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm3, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm4, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm5, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm6, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm7, [rsi]



		MULPS xmm0, [r9]
		MULPS xmm1, [r9 + 16]
		ADDPS xmm0, xmm1
		MULPS xmm2, [r9 + 32]
		MULPS xmm3, [r9 + 48]
		ADDPS xmm2, xmm3
		MULPS xmm4, [r9 + 64]
		MULPS xmm5, [r9 + 80]
		ADDPS xmm4, xmm5
		MULPS xmm6, [r9 + 96]
		MULPS xmm7, [r9 + 112]
		ADDPS xmm6, xmm7

		ADDPS xmm0, xmm2
		ADDPS xmm4, xmm6
		ADDPS xmm0, xmm4
	

		ADDPS xmm0, [rdi]
		MOVAPS [rdi], xmm0

		MOV r10, rbx
		LEA rdi, [rdi + 16]
		LEA r9, [r9 + 128]
		LEA r11, [r11 + 16]
		LEA r13, [r13 + 16]

	DEC eax
	JNZ filter_loop_1
	MOV eax, 4


	LEA r11, [r11 - 16]
	LEA r13, [r13 - 16]

	
align 16
	filter_loop_2:
		MOV rsi, r11
		MOVAPS xmm0, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
		
		MOVAPS xmm1, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
			
		MOVAPS xmm2, [rsi]
		
		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm3, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm4, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm5, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm6, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm7, [rsi]



		MULPS xmm0, [r9]
		MULPS xmm1, [r9 + 16]
		ADDPS xmm0, xmm1
		MULPS xmm2, [r9 + 32]
		MULPS xmm3, [r9 + 48]
		ADDPS xmm2, xmm3
		MULPS xmm4, [r9 + 64]
		MULPS xmm5, [r9 + 80]
		ADDPS xmm4, xmm5
		MULPS xmm6, [r9 + 96]
		MULPS xmm7, [r9 + 112]
		ADDPS xmm6, xmm7

		ADDPS xmm0, xmm2
		ADDPS xmm4, xmm6
		ADDPS xmm0, xmm4
	
		SHUFPS xmm0, xmm0, 1Bh
		ADDPS xmm0, [rdi]
		MOVAPS [rdi], xmm0

		MOV r10, rbx
		LEA rdi, [rdi + 16]
		LEA r9, [r9 + 128]
		LEA r11, [r11 - 16]
		LEA r13, [r13 - 16]

	DEC eax
	JNZ filter_loop_2
	MOV eax, 4




	LEA r11, [r11 + 16*5]
	LEA r13, [r13 + 16*5]
		


align 16
	filter_high_1:
		MOV rsi, r11
		MOVAPS xmm0, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
		
		MOVAPS xmm1, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
			
		MOVAPS xmm2, [rsi]
		
		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm3, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm4, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm5, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm6, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm7, [rsi]



		MULPS xmm0, [r9]
		MULPS xmm1, [r9 + 16]
		ADDPS xmm0, xmm1
		MULPS xmm2, [r9 + 32]
		MULPS xmm3, [r9 + 48]
		ADDPS xmm2, xmm3
		MULPS xmm4, [r9 + 64]
		MULPS xmm5, [r9 + 80]
		ADDPS xmm4, xmm5
		MULPS xmm6, [r9 + 96]
		MULPS xmm7, [r9 + 112]
		ADDPS xmm6, xmm7

		ADDPS xmm0, xmm2
		ADDPS xmm4, xmm6
		ADDPS xmm0, xmm4
	

		ADDPS xmm0, [rdi]
		MOVAPS [rdi], xmm0

		MOV r10, rbx
		LEA rdi, [rdi + 16]
		LEA r9, [r9 + 128]
		LEA r11, [r11 + 16]
		LEA r13, [r13 + 16]

	DEC eax
	JNZ filter_high_1
	MOV eax, 4


	LEA r11, [r11 - 16]
	LEA r13, [r13 - 16]
	

align 16
	filter_high_2:
		MOV rsi, r11
		MOVAPS xmm0, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
		
		MOVAPS xmm1, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]
			
		MOVAPS xmm2, [rsi]
		
		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm3, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm4, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm5, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm6, [rsi]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		LEA rsi, [rsi - 32*4]
		LEA r15, [r13 + r10]
		MOV r14, rsi
		SUB r14, r10		
		SAR r14, 63
		NOT r14
		AND rsi, r14
		NOT r14
		AND r15, r14
		AND r14, rdx
		OR rsi, r15
		LEA r10, [r10 + r14*4]

		MOVAPS xmm7, [rsi]



		MULPS xmm0, [r9]
		MULPS xmm1, [r9 + 16]
		ADDPS xmm0, xmm1
		MULPS xmm2, [r9 + 32]
		MULPS xmm3, [r9 + 48]
		ADDPS xmm2, xmm3
		MULPS xmm4, [r9 + 64]
		MULPS xmm5, [r9 + 80]
		ADDPS xmm4, xmm5
		MULPS xmm6, [r9 + 96]
		MULPS xmm7, [r9 + 112]
		ADDPS xmm6, xmm7

		ADDPS xmm0, xmm2
		ADDPS xmm4, xmm6
		ADDPS xmm0, xmm4
	
		SHUFPS xmm0, xmm0, 1Bh

		CMP r8d, 1
		JZ hi_offload
			ADDPS xmm0, [rdi]
		hi_offload:

		MOVAPS [rdi], xmm0

		MOV r10, rbx
		LEA rdi, [rdi + 16]
		LEA r9, [r9 + 128]
		LEA r11, [r11 - 16]
		LEA r13, [r13 - 16]

	DEC eax
	JNZ filter_high_2
	MOV eax, 4



	LEA r11, [r11 + 16*5]
	LEA r13, [r13 - 3*16]
	LEA rdi, [rdi - 32*4]

	MOV r9, r12

	DEC r8d
	JNZ filter_loop_1




	POP r15
	POP r14
	POP r13
	POP r12

	POP rsi
	POP rdi

	POP rbx

	POP rbp

	RET
?InterpolateQMF@DTS@Audio@@CAXPEAM_K1PEBI@Z	ENDP


ALIGN 16
?StereoAdjust@DTS@Audio@@CAXPEAM0I@Z	PROC
	SUB rdx, rcx
	SHR r8d, 4


align 16
	calc_loop:

		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]

		MOVAPS xmm4, xmm0
		MOVAPS xmm5, xmm1
		MOVAPS xmm6, xmm2
		MOVAPS xmm6, xmm3

		MOVAPS xmm8, [rcx + rdx]
		MOVAPS xmm9, [rcx + rdx + 16]
		MOVAPS xmm10, [rcx + rdx + 32]
		MOVAPS xmm11, [rcx + rdx + 48]


		ADDPS xmm0, xmm8
		SUBPS xmm4, xmm8
		ADDPS xmm1, xmm9
		SUBPS xmm5, xmm9
		ADDPS xmm2, xmm10
		SUBPS xmm6, xmm10
		ADDPS xmm3, xmm11
		SUBPS xmm7, xmm11


		MOVAPS [rcx], xmm0
		MOVAPS [rcx + 16], xmm1
		MOVAPS [rcx + 32], xmm2
		MOVAPS [rcx + 48], xmm3

		MOVAPS [rcx + rdx], xmm4
		MOVAPS [rcx + rdx + 16], xmm5
		MOVAPS [rcx + rdx + 32], xmm6
		MOVAPS [rcx + rdx + 48], xmm7

		LEA rcx, [rcx + 64]

	DEC r8d
	JNZ calc_loop

	RET
?StereoAdjust@DTS@Audio@@CAXPEAM0I@Z	ENDP


ALIGN 16
?Normalize@DTS@Audio@@CAXPEAMAEBII@Z	PROC
	MOVSS xmm8, DWORD PTR [rdx]
	SHUFPS xmm8, xmm8, 0
	
	LEA rcx, [rcx + 2*1024*4]
	
	SHR r8d, 5
	
align 16
	scale_loop:
	
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx + 16]
		MOVAPS xmm2, [rcx + 32]
		MOVAPS xmm3, [rcx + 48]
		MOVAPS xmm4, [rcx + 64]
		MOVAPS xmm5, [rcx + 80]
		MOVAPS xmm6, [rcx + 96]
		MOVAPS xmm7, [rcx + 112]

		MULPS xmm0, xmm8
		MULPS xmm1, xmm8
		MULPS xmm2, xmm8
		MULPS xmm3, xmm8
		MULPS xmm4, xmm8
		MULPS xmm5, xmm8
		MULPS xmm6, xmm8
		MULPS xmm7, xmm8

		MOVNTPS [rcx], xmm0
		MOVNTPS [rcx + 16], xmm1
		MOVNTPS [rcx + 32], xmm2
		MOVNTPS [rcx + 48], xmm3
		MOVNTPS [rcx + 64], xmm4
		MOVNTPS [rcx + 80], xmm5
		MOVNTPS [rcx + 96], xmm6
		MOVNTPS [rcx + 112], xmm7


		LEA rcx, [rcx + 128]

	DEC r8d
	JNZ scale_loop

	SFENCE

	RET
?Normalize@DTS@Audio@@CAXPEAMAEBII@Z	ENDP


ALIGN 16
?LLOverride@DTS@Audio@@CAXPEAHII@Z	PROC
	MOVD xmm4, r8d
	SHR edx, 4
	

	SHUFPS xmm4, xmm4, 0

align 16
	calc_loop:
		CVTDQ2PS xmm0, [rcx + 4096]
		CVTDQ2PS xmm1, [rcx + 4096 + 16]
		CVTDQ2PS xmm2, [rcx + 4096 + 32]
		CVTDQ2PS xmm3, [rcx + 4096 + 48]

		MULPS xmm0, xmm4
		MULPS xmm1, xmm4
		MULPS xmm2, xmm4
		MULPS xmm3, xmm4

		MOVNTPS [rcx], xmm0
		MOVNTPS [rcx + 16], xmm1
		MOVNTPS [rcx + 32], xmm2
		MOVNTPS [rcx + 48], xmm3

		LEA rcx, [rcx + 64]
	DEC edx
	JNZ calc_loop

	SFENCE

	RET
?LLOverride@DTS@Audio@@CAXPEAHII@Z	ENDP


ALIGN 16
?LLConvertAdd@DTS@Audio@@CAXPEAHII@Z	PROC
	MOVD xmm4, r8d
	SHR edx, 4
	

	SHUFPS xmm4, xmm4, 0

align 16
	calc_loop:
		CVTDQ2PS xmm0, [rcx + 4096]
		CVTDQ2PS xmm1, [rcx + 4096 + 16]
		CVTDQ2PS xmm2, [rcx + 4096 + 32]
		CVTDQ2PS xmm3, [rcx + 4096 + 48]

		MULPS xmm0, xmm4
		MULPS xmm1, xmm4
		MULPS xmm2, xmm4
		MULPS xmm3, xmm4

		ADDPS xmm0, [rcx]
		ADDPS xmm1, [rcx + 16]
		ADDPS xmm2, [rcx + 32]
		ADDPS xmm4, [rcx + 48]

		MOVNTPS [rcx], xmm0
		MOVNTPS [rcx + 16], xmm1
		MOVNTPS [rcx + 32], xmm2
		MOVNTPS [rcx + 48], xmm3

		LEA rcx, [rcx + 64]
	DEC edx
	JNZ calc_loop

	SFENCE

	RET
?LLConvertAdd@DTS@Audio@@CAXPEAHII@Z	ENDP




END

