/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "AudioRenderer.h"

#define MP3_GRANULE_SIZE		576
#define MP3_CHANNEL_SIZE		4*2*MP3_GRANULE_SIZE
#define MP3_MAX_PACKET			8

/*
	sorry, but intensity stereo is ignored
*/

namespace Audio {
	class MP3: public Codec {
		private:
			static const unsigned int sampling_frequencies[4];
			
			static const float layer_I_denom[];
			static const float layer_II_denom[];
			static const float scale_fac_I_II[];

			static const char mod_lav_II[][4];
			static const unsigned int layer_II_mappings[][2];
			static const unsigned char layer_II_quantization[4][32][20];
			static const unsigned int map_II_selector[4][16][2];
			
			

			static const short pre_tab[];
			static const unsigned int hct_cfg[];
			static const short Huffman_Tab[];


			static const unsigned short sfb_offset_long[4][24][2];
			static const unsigned short sfb_offset_short[4][16][2];

			static const unsigned int fsize_table[4][4][16];
			static const unsigned char sf_III_size[16][2];

			static __declspec(align(16)) const unsigned int synth_window[512];			
			static __declspec(align(16)) const float alias_cofs[];
			static __declspec(align(16)) float cos_table_12[72];
			static __declspec(align(16)) float cos_table_32[144];
			static __declspec(align(16)) float cos_table_36[648];
			static __declspec(align(16)) float synth_cos[2048];

			static __declspec(align(16)) float window_table[4][36];
			
			static void MSAdjust(float *);
			static void IntesityAdjust(float *);
			static void AliasReduction(float *, int, int);
			
			static void IMDCT_36(float *, int);
			static void IMDCT_12(float *, int);
			static void FormatLongWindow(float *, const float *, int, UI_64);
			static void FormatShortWindow(float *, const float *, int, UI_64);



			static void Synthesize(float *, UI_64);
			static void Synthesize(float *, UI_64, unsigned int, UI_64);
			static void Synthesize_I_II(float *, UI_64, unsigned int, UI_64);


			struct ChannelConfig {
				UI_64 byte_align;

				short part2_3l[2];				
				short global_gain[2];
				short subb_gain[2][4];
				short region_addr[2][4];
				
				short pre_flag[2];

				unsigned char sf_compress[2];
				unsigned char block_split_flag[2];
				unsigned char block_type[2];
				unsigned char switch_point[2];
				unsigned char table_select[2][4];
								
				unsigned char sf_scale[2];
				
				unsigned char sfb_common[24];
				unsigned char allocation_idc[32];
				unsigned char sf_idc[96];



				void Clear();
			};
			

			class DecoderState: public DecoderCfg {
				public:
					unsigned char * packet_ptr;
					ChannelConfig * active_cfg, * current_cfg;


					DecoderState() : packet_ptr(0), active_cfg(0), current_cfg(0) {

					};

					__forceinline void SyncSearch() {						
						if (src_size > 0) {
							if (bit_mask[3] < 8) {
								src_ptr++;
								src_size--;
							}

							for (;src_size > 0;src_ptr++, src_size--) {
								if (src_ptr[0] == 0xFF) {
									if ((src_size>1) && ((src_ptr[1] & 0xF0) == 0xF0)) {										
										break;
									}
								}
							}
						}
					};
									
					__forceinline short GetHuffmaVal(short cb_idx, short & b_count) {
						const short * cb_ptr(Huffman_Tab + (hct_cfg[cb_idx*4]<<2));
						unsigned int code_val(0);
						short result(hct_cfg[4*cb_idx]), c_count(0);
						
						if (src_size > 0) {
							code_val = NextBit();
							b_count--;

							for (cb_idx = result + hct_cfg[4*cb_idx + 1];result < cb_idx;cb_idx++) {
								result++;								
							
								c_count = cb_ptr[0]; cb_ptr += 4;
								for (short i(0);i<c_count;i++, cb_ptr+=4) {
									if (code_val == cb_ptr[0]) {
										return (result + i);
									}
								}
							
								result += c_count;

								code_val <<= 1;
								code_val |= NextBit();

								b_count--;
							}
						}

						return 0;
					};
				
			} decoder_cfg;


			static const unsigned int ccfg_size = ((sizeof(ChannelConfig) + 15) & 0xFFFFFFF0);

			UI_64 active_packet, active_byte, active_bit;
			UI_64 current_packet, packet_offset, pfd_2;

			unsigned int layer_type, protection_id, brate_idx, sf_idx, sampling_frequency, padding, c_mode, xc_mode, frame_size, temp_offset2, xframe_size;

			SFSco::Object packet_buf;

			__forceinline void SetDataPair(float *&, unsigned int, short &, int);
			__forceinline void SetDataQuad(float *&, unsigned int, short &, int);

			void GetTriplet_II(const unsigned char (*)[20], unsigned int, unsigned int);
			void GetSingle_II();
			void GetDual_II();
			void GetStereo_II();

			void GetGranule(unsigned int, unsigned int);

			void GetSample_I(unsigned int, unsigned int, unsigned int);
			UI_64 AudioData_I();
			UI_64 AudioData_II();
			UI_64 AudioData_III();

			void Reorder(float *, int);

			virtual void AudioViewOn() {
				stream_cfg.view_on_default |= 1;
				stream_cfg.view_on |= ((stream_cfg.view_on >> 1) & 1);
				_src_desc->source_frame++;
			};

			virtual void AudioViewOff() {
				stream_cfg.view_on_default &= 0xFFFFFFFE;
				stream_cfg.view_on &= 0xFFFFFFFE;
			};

			virtual void InitViewBuf();

			virtual UI_64 FilterFrame(unsigned int *);
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &);

			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &);
			virtual unsigned int GetSatCount(unsigned int *);

			virtual unsigned int GetSatInfoCount() { return 3; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &);

		public:
			static const UI_64 frame_buf_tid = 961748969;
			MP3();
			virtual ~MP3();

			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
						
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
			virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);


			static UI_64 Initialize();
			static UI_64 Finalize();

	};



}
