/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "Audio\AudioRenderer.h"


#define ONLY_LONG_SEQUENCE		0
#define LONG_START_SEQUENCE		1
#define EIGHT_SHORT_SEQUENCE	2
#define LONG_STOP_SEQUENCE		3


#define MAX_SECTION_COUNT	64
#define MAX_APROGRAM_COUNT	16

#define MAX_CODE_LENGTH		24
#define MAX_CODE_BOOK		16

#define MAX_GROUP_COUNT		8

#define MAX_TNS_CCOUNT		20

#define AAC_CHANNEL_SIZE		4096


namespace Audio {
	class AAC: public Codec {
		private:

			static const unsigned int sampling_frequencies[16];
			static const unsigned int num_swb_short_window[16];
			static const unsigned int num_swb_long_window[16];
			static const unsigned short pred_sfb_max[16];

			static const short tns_max_sfb[16][2];
			static const short tns_max_sfb_ssr[16][2];

			static const unsigned short swb_offset_long[16][52];
			static const unsigned short swb_offset_short[16][16];

			static const unsigned short swb_offset_shortlong[2][8][40];
			static const unsigned char codebook_cfg[MAX_CODE_BOOK];
			static const unsigned int Huffman_LC[MAX_CODE_BOOK][MAX_CODE_LENGTH][2]; // length, top_code
			
			static const char mod_lav[MAX_CODE_BOOK][320][4];
			
			static const unsigned int KBD_1024[2048];
			static const unsigned int KBD_128[256];
			static const unsigned int SIN_1024[2048];
			static const unsigned int SIN_128[256];

			static __declspec(align(16)) float cos_table_0[5120];
			static __declspec(align(16)) float window_table[4][4][2048]; // 2 sequencies, 4 shape combinations per sequence
						
			static int tns_sin[2][16];

						
			static void StereoAdjustAll(float *, unsigned int);
			static void TNSFilterUp(float *, int, const float *);
			static void TNSFilterDown(float *, int, const float *);

			static void Format8Window(float *, UI_64, const float *);



			unsigned int access_unit_type, audio_object_type, channel_idc, x_aot;
			unsigned int sampling_frequency, sf_idx, x_sampling_frequency, xsf_idx;
			unsigned int core_coder_delay, layer_number, num_of_sub_frame, layer_length, aac_resilience_flags;
			unsigned int ep_config, direct_mapping_flag, frame_lines, current_program, max_tns, previous_shape[MAX_CHANNEL_COUNT];

			unsigned int sync_xtype;

			const unsigned short * sfb_offsets;
		

			struct ChannelConfig {
				struct ICS {
					int line_count, scale_fac, sfb_count;
					short global_gain, ltp_lag;				

					unsigned short window_sequence, num_windows, num_window_groups, window_group_size[8];
					unsigned short window_shape;

					unsigned short max_sfb, ms_mask;
					unsigned short scale_factor_grouping;

					unsigned short predictor_reset_group_number;

					unsigned short ltp_coef;				
					unsigned short ltp_long_used[40];
					unsigned short prediction_used[64];
					
					unsigned char ms_used[MAX_GROUP_COUNT][MAX_SECTION_COUNT];

			// temporal noise
					struct NoiseConfig {
						unsigned short filter_count;

						struct Filter {
							unsigned short length;
							unsigned short order;
							unsigned short direction_flag;
						
							float coefs[MAX_TNS_CCOUNT];

						} filter[4];

					} tns_cfg[8];

				// gain control
					unsigned short max_band;
					struct GControl {
						unsigned short number;

						short level[8];
						short location[8];
					} gain_control[4][8];

				} ics[2];

			//section part
				struct SectionGroup {
					unsigned short section_count;

					struct SectionEntry {
						unsigned short code_book_idx;
						unsigned short length;
						unsigned short start;
					} section[MAX_SECTION_COUNT];
				} group[MAX_GROUP_COUNT];

			// scalefactor part
				short gain[MAX_GROUP_COUNT][MAX_SECTION_COUNT];

								

				void ICSClear();
				void GroupClear();
			} channel_cfg;

			struct CouplingState {
				unsigned char element_count;
				unsigned char gain_element_list_count;

				unsigned char target_tag[MAX_CHANNEL_COUNT];
				unsigned char lr_state[MAX_CHANNEL_COUNT];


				unsigned char domain, _reserved;
				unsigned char gain_element_sign, gain_element_scale;

				short common_gain[MAX_CHANNEL_COUNT];
				short gain[MAX_CHANNEL_COUNT][MAX_GROUP_COUNT][MAX_SECTION_COUNT];

			} coupling_cfg;

			DecoderCfg decoder_cfg;

			struct ProgramConfig {			
				unsigned int o_type;

				unsigned int load_runner[4];

				unsigned int sampling_frequency;
				unsigned int center_present;
				unsigned int chan_count;
				
				unsigned int num_channel_elements[5]; // front, side, back, lfe, coupling

				unsigned int num_assoc_data_elements;
				
				unsigned int mono_mixdown_element_number, stereo_mixdown_element_number;

				unsigned int matrix_mixdown_idx, pseudo_surround_flag;

				struct Element {
					unsigned char is_cpe;
					unsigned char tag_select;
				} elements[6][16];

				void Clear();

			} program_cfg[MAX_APROGRAM_COUNT];


			UI_64 GenericRaw();
			UI_64 ERRaw();
			UI_64 ScalableRaw();

			void GetProgramConfig();		
					
			void GetChannel(unsigned int, unsigned int);
			void GetCouplingChannel();
			void SpectralData(unsigned int);
			void StereoAdjust();
			void Restore(unsigned int);
			void IntensityAdjust(unsigned int);
			void TNSAdjust(unsigned int);

			__forceinline void SectionData(unsigned int);
			__forceinline void PulseData(unsigned int);
			__forceinline void TNSData(unsigned int);
			__forceinline void GainControlData(unsigned int);
			__forceinline void ScaleFactorData(unsigned int);
			
			__forceinline void ICS_Info(unsigned int, unsigned int);
			__forceinline void LTP_Data(unsigned int);



			virtual unsigned int GetSatCount(unsigned int *);

			virtual unsigned int GetSatInfoCount() { return 3; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &);

			virtual UI_64 FilterFrame(unsigned int *);
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &);

		public:
			static const UI_64 frame_buf_tid = 961748951;

			static int scale_factor[512];
			static int de_quant[8192];

			static UI_64 Initialize();
			static UI_64 Finalize();

			AAC(unsigned int);
			virtual ~AAC();

			
			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
			
			
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
			virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);

	};



}

/*
					__forceinline unsigned char NextBit() {
						unsigned char result(0);

						if (src_size >= 0) {
							result = 1 + (((bit_mask[0] & bit_mask[1]) - 1) >> 7);

							if ((bit_mask[0] >>= 1) == 0) {
								bit_mask[0] = 0x00000080;

								src_ptr++;
								src_size--;

								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];

								bit_mask[3] = 9;
							}

							bit_mask[3]--;
						}

						return result;
					};

					__forceinline unsigned int GetNumber(int bit_count) {
						unsigned int result(0);

						if (src_size >= 0) {
							for (;bit_count > bit_mask[3];bit_mask[3] += 8) {
								src_ptr++;
								src_size--;

								bit_mask[0] <<= 8;
								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];

							}

							result = (bit_mask[0] << 1) - 1;
							result &= bit_mask[1];

							
							bit_mask[3] -= bit_count;

							result >>= bit_mask[3];

							

							if ((bit_mask[0] >>= bit_count) == 0) {
								bit_mask[0] = 0x00000080;

								src_ptr++;
								src_size--;

								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];

								bit_mask[3] = 8;
							}
						}

						return result;
					};


					__forceinline unsigned char NextByte() {
						unsigned char result(0);

						if (src_size >= 0) {
							result = src_ptr[0];

							src_ptr++;
							src_size--;

							bit_mask[0] = 0x00000080;

							bit_mask[1] <<= 8;
							bit_mask[1] |= src_ptr[0];

							bit_mask[3] = 8;
														
						}

						return result;
					};

					__forceinline void Skip(unsigned int s_val) {
						if (src_size > s_val) {
							src_ptr += s_val;
							src_size -= s_val;

							bit_mask[1] = src_ptr[0];
						} else {
							src_size = -1;
						}
					};

					__forceinline void ByteAlign() {
						if (src_size >= 0) {
							if (bit_mask[0] ^ 0x00000080) {
								bit_mask[0] = 0x00000080;

								src_ptr++;
								src_size--;

								bit_mask[0] = 0x00000080;

								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];
								
								bit_mask[3] = 8;
							}
						}
					};

					__forceinline void SetSource(const unsigned char * sptr, UI_64 sval) {						
						src_ptr = sptr;
						bit_mask[0] = 0x00000080;
						bit_mask[1] = sptr[0];
						bit_mask[2] = 0;
						bit_mask[3] = 8;
						src_size = sval;
					};

					__forceinline bool IsSource(unsigned int bco) {
						return ((bco + 8) <= ((src_size<<3) + bit_mask[3]));

					}


*/