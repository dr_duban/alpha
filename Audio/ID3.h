/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Media\Source.h"
#include "Media\CodedSource.h"

#include "Data\Stream.h"


namespace Audio {
	class ID3: public Media::Program {
		protected:

			static DWORD __stdcall ReceiverLoop(void *);	
			
			static Memory::Allocator * id3_pool;
			
			
			UI_64 temp_offset, sample_top, sample_runner, jump_val;
			unsigned int version, frame_tag, frame_size, frame_flags, audio_offset;
				
			Media::CodedSource * _sequence;

			UI_64 duration, sample_count, tt_offset, b_position;			
			unsigned int id, _type, fb_count;
			
			HANDLE reciever_thread;
				
			Data::Stream source;

			unsigned int terminator;

			UI_64 ParseBlock(const unsigned char *&, UI_64 &);

			ID3(unsigned int);
			virtual ~ID3();

//			virtual void NVFlush();
			virtual Media::Source * GetVSource(UI_64 &);
			virtual Media::Source * GetASource(UI_64 &);

		public:			
			static const UI_64 _tid = 961749037;
			static const UI_64 sample_table_tid = 92416190071;

			static UI_64 Initialize();
			static UI_64 Finalize();

			static Program * SetSource(const Data::Stream &, unsigned int p_flag);
			static bool IsID3(Data::Stream &);
			static bool IsMP3(Data::Stream &);

			virtual UI_64 Pack(const void *, UI_64);
			virtual UI_64 GetSpan();


	};

}

