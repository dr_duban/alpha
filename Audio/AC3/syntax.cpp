/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\AC3.h"

#include "System\SysUtils.h"

void Audio::AC3::CopyCouplingVals(unsigned int c_idx) {
	int * chan_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + c_idx*AC3_CHANNEL_SIZE);
	const int * coupl_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + (MAX_CHANNEL_COUNT - 1)*AC3_CHANNEL_SIZE);

	unsigned int i_idx(coupling.range[0] + 1);

	chan_ptr += ((i_idx & 0xFFFFFFFC) << 1) + (i_idx & 3);
	coupl_ptr += ((i_idx & 0xFFFFFFFC) << 1) + (i_idx & 3);

	for (unsigned int i(0);i<coupling.band_counter;i++) {

		for (unsigned j(0);j<coupling.band_size[i];j++) {

			if (channels[c_idx].dither_flag == 0) {
				chan_ptr[0] = coupl_ptr[0];
			} else {
				if (coupl_ptr[0] != 0.0) chan_ptr[0] = coupl_ptr[0];
				else {
					if (coupl_ptr[4]) {
						if (reinterpret_cast<float *>(chan_ptr)[0] = (int)System::Gen32()) {
							chan_ptr[0] -= 0x10000000;
						}						
					}
				}
			}

			if (channels[c_idx].cpl.coords[i][1]) {
				chan_ptr[4] = i_float_tab[channels[c_idx].cpl.coords[i][1]] - ((coupl_ptr[4] + (channels[c_idx].cpl.master_coord + channels[c_idx].cpl.coords[i][0])) << 23);
			}

			chan_ptr += data_increment[++i_idx & 3];
			coupl_ptr += data_increment[i_idx & 3];
		}
	}
}

void Audio::AC3::GetMantissas(unsigned int ch_idx, unsigned int i_idx, unsigned int i_end) {
	int * mask_ptr(decoder_cfg.exponent_data + ch_idx*EXPONENT_BLOCK_SIZE + 256), x_val(17);
	int * d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + ch_idx*AC3_CHANNEL_SIZE);

	i_end += i_idx;

	d_ptr += ((i_idx & 0xFFFFFFFC) << 1) + (i_idx & 3);	
	
	for (;i_idx <= i_end;d_ptr += data_increment[++i_idx & 3]) {
		x_val =  mask_ptr[i_idx*2 + 1];

		switch (mask_ptr[i_idx*2]) {
			case 1:
				if (ma_vals[0 + 4] > 2) {
					ma_vals[0 + 4] = 0;

					ma_vals[1 + 4] = decoder_cfg.GetNumber(5);
				}

				d_ptr[0] = symmetric_mantab[1][pack_9[ma_vals[1 + 4]][ma_vals[0 + 4]++]];
				
			break;
			case 2:
				if (ma_vals[0 + 8] > 2) {
					ma_vals[0 + 8] = 0;

					ma_vals[1 + 8] = decoder_cfg.GetNumber(7);
				}

				d_ptr[0] = symmetric_mantab[2][pack_25[ma_vals[1 + 8]][ma_vals[0 + 8]++]];
			break;
			case 4:
				if (ma_vals[0 + 12] > 1) {
					ma_vals[0 + 12] = 0;

					ma_vals[1 + 12] = decoder_cfg.GetNumber(7);
				}

				d_ptr[0] = symmetric_mantab[4][pack_11[ma_vals[1 + 12]][ma_vals[0 + 12]++]];
			break;


			case 0:
				d_ptr[0] = 0.0f;				
			break;
			case 3:				
				d_ptr[0] = symmetric_mantab[3][decoder_cfg.GetNumber(3)];
			break;
			case 5:
				d_ptr[0] = symmetric_mantab[5][decoder_cfg.GetNumber(4)];
			break;
			default:
				ma_vals[0] = (mask_ptr[i_idx*2]<<1);
				ma_vals[1] = (decoder_cfg.GetNumber(bit_map[ma_vals[0]]) << bit_map[ma_vals[0] + 1]);
				
				reinterpret_cast<float *>(d_ptr)[0] = (ma_vals[1] >> bit_map[ma_vals[0] + 1]); // sign extension
				x_val += (bit_map[ma_vals[0]] - 1);

		}

		if (ch_idx != (MAX_CHANNEL_COUNT -1)) {
			d_ptr[4] = scale_tab[x_val];
		} else {
			d_ptr[4] = x_val;
		}
	}
}

void Audio::AC3::CalculateBAI(int * msk_ptr, short snr_offset, unsigned int i_idx, unsigned int i_end) {
	int * bnd_psd(msk_ptr - 128);
	unsigned int band_idx(-1);

	if (i_idx) i_end += i_idx++;

	msk_ptr += i_idx*2;

	for (;i_idx <= i_end;i_idx++, msk_ptr += 2) {
		if (band_idx != mask_tab[i_idx]) {
			band_idx = mask_tab[i_idx];
			
			bnd_psd[band_idx] -= (snr_offset + bai.masking_floor);
			bnd_psd[band_idx] &= ~(bnd_psd[band_idx] >> 31);
			bnd_psd[band_idx] &= 0x1FE0;
			bnd_psd[band_idx] += bai.masking_floor;

			msk_ptr[0] = ((3072 - (msk_ptr[1]<<7) - bnd_psd[band_idx]) >> 5);
			msk_ptr[0] &= ~(msk_ptr[0] >> 31);
			msk_ptr[0] = ((msk_ptr[0] | ((63 - msk_ptr[0]) >> 31)) & 63);			
			msk_ptr[0] = bap_tab[msk_ptr[0]];

		} else {
			msk_ptr[0] = ((3072 - (msk_ptr[1]<<7) - bnd_psd[band_idx]) >> 5);
			msk_ptr[0] &= ~(msk_ptr[0] >> 31);
			msk_ptr[0] = ((msk_ptr[0] | ((63 - msk_ptr[0]) >> 31)) & 63);			
			msk_ptr[0] = bap_tab[msk_ptr[0]];
		}		
	}

}


void Audio::AC3::CalculateMaskingCurve(int * bnd_psd, MaskRecord & msk, int i_idx, int i_end) {	
	int lc_val(0), zombie_val(0), temp_val(0), temp_val2(0);

	bnd_psd -= 128;

	for (unsigned int i(0);i<64;i++) {
		bnd_psd[i] = bnd_psd[i + 64];
	}

	if (i_idx == 0) {
		i_end = mask_tab[i_end] + 1;

		LowComp(lc_val, bnd_psd + 0, 0);
		zombie_val = bnd_psd[0] - msk.fast_gain - lc_val;

		temp_val = bai.dB_per_bit - bnd_psd[0];
		if (temp_val > 0) zombie_val += (temp_val >> 2);

		if (zombie_val > hearing_threshold_tab[sf_idx][0]) bnd_psd[0] = zombie_val;
		else bnd_psd[0] = hearing_threshold_tab[sf_idx][0];


		LowComp(lc_val, bnd_psd + 1, 1);
		zombie_val = bnd_psd[1] - msk.fast_gain - lc_val;

		temp_val = bai.dB_per_bit - bnd_psd[1];
		if (temp_val > 0) zombie_val += (temp_val >> 2);

		if (zombie_val > hearing_threshold_tab[sf_idx][1]) bnd_psd[1] = zombie_val;
		else bnd_psd[1] = hearing_threshold_tab[sf_idx][1];


		for (i_idx = 2;i_idx < 7;i_idx++) {
			if ((i_end != 7) || (i_idx != 6)) {
				LowComp(lc_val, bnd_psd + i_idx, i_idx);
				temp_val2 = bnd_psd[i_idx] - bnd_psd[i_idx + 1];
			}

			msk.fast_leak = bnd_psd[i_idx] - msk.fast_gain;
			msk.slow_leak = bnd_psd[i_idx] - bai.slow_gain;

			zombie_val = msk.fast_leak - lc_val;

			temp_val = bai.dB_per_bit - bnd_psd[i_idx];
			if (temp_val > 0) zombie_val += (temp_val >> 2);					

			if (zombie_val > hearing_threshold_tab[sf_idx][i_idx]) bnd_psd[i_idx] = zombie_val;
			else bnd_psd[i_idx] = hearing_threshold_tab[sf_idx][i_idx];

			if ((i_end != 7) || (i_idx != 6)) {
				if (temp_val2 <= 0) {
					i_idx++;
					break;
				}
			}

		}

		temp_val2 = 22;
		if (i_end < 22) temp_val2 = i_end;

		for (;i_idx < temp_val2;i_idx++) {
			if ((i_end != 7) || (i_idx != 6)) {
				LowComp(lc_val, bnd_psd + i_idx, i_idx);
			}
			msk.fast_leak -= bai.fast_decay;
			temp_val = (bnd_psd[i_idx] - msk.fast_gain);
			if (msk.fast_leak < temp_val) msk.fast_leak = temp_val;
			msk.slow_leak -= bai.slow_decay;
			temp_val = (bnd_psd[i_idx] - bai.slow_gain);
			if (msk.slow_leak < temp_val) msk.slow_leak = temp_val;

			zombie_val = msk.fast_leak - lc_val;
			if (zombie_val < msk.slow_leak) zombie_val = msk.slow_leak;

			temp_val = bai.dB_per_bit - bnd_psd[i_idx];
			if (temp_val > 0) zombie_val += (temp_val >> 2);

			if (zombie_val > hearing_threshold_tab[sf_idx][i_idx]) bnd_psd[i_idx] = zombie_val;
			else bnd_psd[i_idx] = hearing_threshold_tab[sf_idx][i_idx];

		}

	} else {
		i_end = mask_tab[i_idx + i_end] + 1;
		i_idx = mask_tab[i_idx + 1];		
	}

	for (;i_idx < i_end;i_idx++) {
		msk.fast_leak -= bai.fast_decay;
		temp_val = (bnd_psd[i_idx] - msk.fast_gain);
		if (msk.fast_leak < temp_val) msk.fast_leak = temp_val;
		msk.slow_leak -= bai.slow_decay;
		temp_val = (bnd_psd[i_idx] - bai.slow_gain);
		if (msk.slow_leak < temp_val) msk.slow_leak = temp_val;

		zombie_val = msk.fast_leak;
		if (zombie_val < msk.slow_leak) zombie_val = msk.slow_leak;

		temp_val = bai.dB_per_bit - bnd_psd[i_idx];
		if (temp_val > 0) zombie_val += (temp_val >> 2);

		if (zombie_val > hearing_threshold_tab[sf_idx][i_idx]) bnd_psd[i_idx] = zombie_val;
		else bnd_psd[i_idx] = hearing_threshold_tab[sf_idx][i_idx];
	}
}

void Audio::AC3::GetExponents(int * sd_ptr, unsigned int g_count, unsigned int e_type, unsigned int e_runner) {	
	int pack_val[4], band_id(-1);
	int * bnd_psd(sd_ptr - 64);

	pack_val[0] = decoder_cfg.GetNumber(4);
	
	if (e_runner) {
		sd_ptr += e_runner*2;

		pack_val[0] <<= 1;
	} else {
		band_id = 0;
		sd_ptr[0] = 3072 - (pack_val[0] << 7);
		bnd_psd[band_id] = sd_ptr[0];

		sd_ptr[1] = pack_val[0];
	}

	sd_ptr += 2;
		
	switch (e_type) {
		case 1:
			for (unsigned int j(0);j<g_count;j++) {
				pack_val[3] = decoder_cfg.GetNumber(7);

				pack_val[1] = pack_25[pack_val[3]][0];
				pack_val[2] = pack_25[pack_val[3]][1];
				pack_val[3] = pack_25[pack_val[3]][2];

				pack_val[1] += (pack_val[0] - 2);
				pack_val[2] += (pack_val[1] - 2);
				pack_val[3] += (pack_val[2] - 2);

				pack_val[0] = pack_val[3];
				
				sd_ptr[0] = 3072 - (pack_val[1] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				
				sd_ptr[1] = pack_val[1];
		
				sd_ptr += 2;
				sd_ptr[0] = 3072 - (pack_val[2] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[2];

				sd_ptr += 2;
				sd_ptr[0] = 3072 - (pack_val[3] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}

				sd_ptr[1] = pack_val[3];

				sd_ptr += 2;
			}
		break;
		case 2:
			for (unsigned int j(0);j<g_count;j++) {
				pack_val[3] = decoder_cfg.GetNumber(7);

				pack_val[1] = pack_25[pack_val[3]][0];
				pack_val[2] = pack_25[pack_val[3]][1];
				pack_val[3] = pack_25[pack_val[3]][2];

				pack_val[1] += (pack_val[0] - 2);
				pack_val[2] += (pack_val[1] - 2);
				pack_val[3] += (pack_val[2] - 2);

				pack_val[0] = pack_val[3];

				sd_ptr[0] = 3072 - (pack_val[1] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[1];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr += 4;
				sd_ptr[0] = 3072 - (pack_val[2] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[2];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr += 4;
		
				sd_ptr[0] = 3072 - (pack_val[3] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[3];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr += 4;

			}

		break;
		case 3:
			for (unsigned int j(0);j<g_count;j++) {
				pack_val[3] = decoder_cfg.GetNumber(7);

				pack_val[1] = pack_25[pack_val[3]][0];
				pack_val[2] = pack_25[pack_val[3]][1];
				pack_val[3] = pack_25[pack_val[3]][2];

				pack_val[1] += (pack_val[0] - 2);
				pack_val[2] += (pack_val[1] - 2);
				pack_val[3] += (pack_val[2] - 2);

				pack_val[0] = pack_val[3];

			
				sd_ptr[0] = 3072 - (pack_val[1] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[1];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr[4] = sd_ptr[2];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[4];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[4]);
				}
				sd_ptr[5] = sd_ptr[3];

				sd_ptr[6] = sd_ptr[4];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[6];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[6]);
				}
				sd_ptr[7] = sd_ptr[5];

				sd_ptr += 8;


				sd_ptr[0] = 3072 - (pack_val[2] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[2];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr[4] = sd_ptr[2];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[4];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[4]);
				}
				sd_ptr[5] = sd_ptr[3];

				sd_ptr[6] = sd_ptr[4];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[6];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[6]);
				}
				sd_ptr[7] = sd_ptr[5];

				sd_ptr += 8;


				sd_ptr[0] = 3072 - (pack_val[3] << 7);
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[0];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[0]);
				}
				sd_ptr[1] = pack_val[3];

				sd_ptr[2] = sd_ptr[0];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[2];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[2]);
				}
				sd_ptr[3] = sd_ptr[1];

				sd_ptr[4] = sd_ptr[2];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[4];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[4]);
				}
				sd_ptr[5] = sd_ptr[3];

				sd_ptr[6] = sd_ptr[4];
				if (mask_tab[++e_runner] != band_id) {
					band_id = mask_tab[e_runner];
					bnd_psd[band_id] = sd_ptr[6];
				} else {
					LogAdd(bnd_psd[band_id], sd_ptr[6]);
				}
				sd_ptr[7] = sd_ptr[5];

				sd_ptr += 8;
			}
		break;
	}
}


void Audio::AC3::GetAudioBlock() {
	int * mask_ptr(0), d_vals[16] = {0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0};
	MaskRecord ma_re;

	int x_val(0);

	
	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		channels[i].switch_flag[0] = decoder_cfg.NextBit();		
	}
	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		channels[i].dither_flag = decoder_cfg.NextBit();
	}

	if (decoder_cfg.NextBit()) {
		range_gain[0] = decoder_cfg.GetNumber(8);
	}

	if (bsi.coding_mode == 0) {
		if (decoder_cfg.NextBit()) {
			range_gain[1] = decoder_cfg.GetNumber(8);
		}
	}

// coupling strategy info
	if (decoder_cfg.NextBit()) {
		if (coupling.in_use_flag = decoder_cfg.NextBit()) {
			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				channels[i].cpl.in_use = decoder_cfg.NextBit();
			}

			if (bsi.coding_mode == 2) {
				coupling.phase_flags_in_use = decoder_cfg.NextBit();
			}

			coupling.range[0] = decoder_cfg.GetNumber(4); // 7.24
			coupling.range[1] = decoder_cfg.GetNumber(4) - coupling.range[0] + 3;
									
			coupling.band_counter = 0;
			coupling.band_size[coupling.band_counter] = 12;

			for (unsigned char i(1);i<coupling.range[1];i++) {
				if (decoder_cfg.NextBit()) {
					coupling.band_size[coupling.band_counter] += 12;
				} else {					
					coupling.band_size[++coupling.band_counter] = 12;
				}
			}

			coupling.band_counter++;

			coupling.range[0] *= 12;
			coupling.range[0] += 36;

			coupling.range[1] *= 12;
		}
	}

	x_val = 0;

	if (coupling.in_use_flag) {		

		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			x_val <<= 1;
			if (channels[i].cpl.in_use) {
				if (decoder_cfg.NextBit()) {
					x_val |= 1;

					channels[i].cpl.master_coord = 3*decoder_cfg.GetNumber(2) - 3 + 5;

					for (unsigned int j(0);j<coupling.band_counter;j++) {
						channels[i].cpl.coords[j][0] = decoder_cfg.GetNumber(4); // exponent
						channels[i].cpl.coords[j][1] = (decoder_cfg.GetNumber(4) | (((channels[i].cpl.coords[i][0] + 1) & 16) ^ 16)); // mantissa
						
						channels[i].cpl.coords[j][0] -= (((channels[i].cpl.coords[j][0] + 1) & 16) >> 4);
					}
				}
			}
		}
	}

	if (bsi.coding_mode == 2) {
		if ((coupling.phase_flags_in_use) && (x_val)) {
			for (unsigned int i(0);i<coupling.band_counter;i++) {
				coupling.pahse_flags[i] = decoder_cfg.NextBit();
			}

		}

		if (decoder_cfg.NextBit()) { // rematrixing
			x_val = 4;

			if (coupling.in_use_flag) {
				if (coupling.range[0] <= 2) {
					if (coupling.range[0] == 0) x_val = 2;
					else x_val = 3;
				}
			}

			for (unsigned int i(0);i<x_val;i++) {
				coupling.rematrix_flag[i] = decoder_cfg.NextBit();
			}
		}
	}


	if (coupling.in_use_flag) {
		if (coupling.exponent_strategy = decoder_cfg.GetNumber(2)) {
			coupling.group_count = coupling.range[1] / 3;
			coupling.group_count >>= (coupling.exponent_strategy - 1);
		}
	}

	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		channels[i].exponent_strategy = decoder_cfg.GetNumber(2);
	}

	if (bsi.lfe_present) {
		channels[bsi.full_chan_count].exponent_strategy = decoder_cfg.NextBit();
		channels[bsi.full_chan_count].group_count = 2;
		
		channels[bsi.full_chan_count].range[0] = 0;
		channels[bsi.full_chan_count].range[1] = 2*3;
	}

	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		if (channels[i].exponent_strategy) {
			channels[i].range[0] = 0;

			if (channels[i].cpl.in_use == 0) {
				channels[i].range[1] = decoder_cfg.GetNumber(6) + 12 + 12; // channels[i].bandwidth_code
				
			} else {
				channels[i].range[1] = coupling.range[0]/3;

			}
						
			x_val = (channels[i].exponent_strategy - 1);
			channels[i].group_count = ((channels[i].range[1] + ((1 << x_val) - 1)) >> x_val);
			
			channels[i].range[1] *= 3;
		}
	}

	if (coupling.in_use_flag) {
		if (coupling.exponent_strategy) {
			GetExponents(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, coupling.group_count, coupling.exponent_strategy, coupling.range[0]);
		}
	}

	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		if (channels[i].exponent_strategy) {
			GetExponents(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, channels[i].group_count, channels[i].exponent_strategy, 0);
			channels[i].gain_range = decoder_cfg.GetNumber(2);
		}
	}

	if (bsi.lfe_present) {
		if (channels[bsi.full_chan_count].exponent_strategy) {
			GetExponents(decoder_cfg.exponent_data + bsi.full_chan_count*EXPONENT_BLOCK_SIZE + 256, channels[bsi.full_chan_count].group_count, channels[bsi.full_chan_count].exponent_strategy, 0);
		}
	}

// parametric bit allocation info =================================================================================================
	if (decoder_cfg.NextBit()) { // bit allocation
		bai.slow_decay = 0x000F + (decoder_cfg.GetNumber(2)<<1);
		bai.fast_decay = 0x003F + (decoder_cfg.GetNumber(2)*0x0014);
		
		x_val = decoder_cfg.GetNumber(2);
		bai.slow_gain = 0x0540 - (x_val*0x0068) + ((x_val & 2)<<2);

		if (bai.dB_per_bit = decoder_cfg.GetNumber(2)) {
			bai.dB_per_bit *= 0x0200;
			bai.dB_per_bit += 0x0500;
		}

		bai.masking_floor = floor_table[decoder_cfg.GetNumber(3)];
	}

	if (decoder_cfg.NextBit()) { // SNR offsets
		x_val = decoder_cfg.GetNumber(6); // coarse_snr_offset
		x_val -= 15;
		x_val <<= 4;

		if (coupling.in_use_flag) {
			coupling.msk.snr_offset = ((x_val + decoder_cfg.GetNumber(4)) << 2);
			coupling.msk.fast_gain = 0x0080 + (decoder_cfg.GetNumber(3)<<7);
		}

		for (int i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
			channels[i].msk.snr_offset = ((x_val + decoder_cfg.GetNumber(4)) << 2);
			channels[i].msk.fast_gain = 0x0080 + (decoder_cfg.GetNumber(3) << 7);
		}
	}

	if (coupling.in_use_flag) {
		if (decoder_cfg.NextBit()) {
			coupling.msk.fast_leak = (decoder_cfg.GetNumber(3) << 8) + 768;
			coupling.msk.slow_leak = (decoder_cfg.GetNumber(3) << 8) + 768;

		}
				
		ma_re = coupling.msk;
		CalculateMaskingCurve(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, ma_re, coupling.range[0], coupling.range[1]);
	}

	for (int i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
		ma_re = channels[i].msk;
		CalculateMaskingCurve(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, ma_re, 0, channels[i].range[1]);
	}


// delta bit allocation =============================================================================
	if (decoder_cfg.NextBit()) {
		if (coupling.in_use_flag) {
			
			d_vals[9] = decoder_cfg.GetNumber(2);

			if (d_vals[9] == 3) {
				// mute state

			}
		}

		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			d_vals[i] = decoder_cfg.GetNumber(2);
		}

		if ((coupling.in_use_flag) && (d_vals[9] == 1)) {
			mask_ptr = decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 128;
			d_vals[10] = decoder_cfg.GetNumber(3) + 1;
			d_vals[11] = 0;

			for (int j(0);j<d_vals[10];j++) {
				d_vals[11] += decoder_cfg.GetNumber(5);
				d_vals[12] = decoder_cfg.GetNumber(4);
				d_vals[13] = decoder_cfg.GetNumber(3);

				d_vals[13] -= 4;
				d_vals[13] = d_vals[13] - ~(d_vals[13] >> 31);
				d_vals[13] <<= 7;

				for (int k(0);k<d_vals[12];k++) {
					mask_ptr[d_vals[11]++] += d_vals[13];
				}
			}
		}

		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			if (d_vals[i] == 1) {
				mask_ptr = decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 128;
				
				d_vals[10] = decoder_cfg.GetNumber(3) + 1;
				d_vals[11] = 0;

				for (int j(0);j<d_vals[10];j++) {
					d_vals[11] += decoder_cfg.GetNumber(5);
					d_vals[12] = decoder_cfg.GetNumber(4);
					d_vals[13] = decoder_cfg.GetNumber(3);

					d_vals[13] -= 4;
					d_vals[13] = d_vals[13] - ~(d_vals[13] >> 31);
					d_vals[13] <<= 7;

					for (int k(0);k<d_vals[12];k++) {
						mask_ptr[d_vals[11]++] += d_vals[13];
					}
				}		
			}
		}
	}


// skip =============================================================================================
	if (decoder_cfg.NextBit()) {
		decoder_cfg.Skip(decoder_cfg.GetNumber(9));
	}

// get mantissa values ==============================================================================
	x_val = 0;

	ma_vals[0] = 3; ma_vals[1] = 0; ma_vals[2] = 0; ma_vals[3] = 0;
	ma_vals[4] = 3; ma_vals[5] = 0; ma_vals[6] = 0; ma_vals[7] = 0;
	ma_vals[8] = 3; ma_vals[9] = 0; ma_vals[10] = 0; ma_vals[11] = 0;
	ma_vals[12] = 3; ma_vals[13] = 0; ma_vals[14] = 0; ma_vals[15] = 0;


	for (int i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
		if (channels[i].range[1]) {
			CalculateBAI(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, channels[i].msk.snr_offset, 0, channels[i].range[1]);
			GetMantissas(i, 0, channels[i].range[1]);
		
			if (channels[i].cpl.in_use) {
				if (coupling.in_use_flag && (x_val == 0)) {
					x_val = 1;

					CalculateBAI(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, coupling.msk.snr_offset, coupling.range[0], coupling.range[1]);
					GetMantissas(MAX_CHANNEL_COUNT - 1, coupling.range[0] + 1, coupling.range[1]);
				}
				
				CopyCouplingVals(i);

			}
		}
	}
}


UI_64 Audio::AC3::GetFrame() {
	UI_64 result(0);
	unsigned int x_val(0);


	System::MemoryFill(channels, MAX_CHANNEL_COUNT*sizeof(ChanCfg), 0);
	System::MemoryFill(&coupling, sizeof(CouplingInfo), 0);
	bai.Clear();
	
// syncinfo ==============================================================================================
		
	x_val = decoder_cfg.GetNumber(16); // crc1

	sf_idx = decoder_cfg.GetNumber(2);
	sampling_frequency = sampling_frequencies[sf_idx];

	result = (frame_size_table[sf_idx][decoder_cfg.GetNumber(6)]<<1); // size code


// bsi ====================================================================================================
	bsi._id = decoder_cfg.GetNumber(5);
	bsi._mode = decoder_cfg.GetNumber(3);

	switch (bsi.coding_mode = decoder_cfg.GetNumber(3)) {
		case 0:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			bsi.center_present = -1;
			bsi.full_chan_count = 2;			
		break;
		case 1:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;

			bsi.center_present = 1;
			bsi.full_chan_count = 1;			
		break;
		case 2:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			bsi.full_chan_count = 2;
			bsi.surround_mode = decoder_cfg.GetNumber(2);
		break;
		case 3:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;

			bsi.center_present = 1;
			bsi.full_chan_count = 3;

			bsi.center_level = decoder_cfg.GetNumber(2);
		break;
		case 4:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_CENTER;

			bsi.full_chan_count = 3;

			bsi.surround_level = decoder_cfg.GetNumber(2);
		break;
		case 5:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_CENTER;

			bsi.center_present = 1;
			bsi.full_chan_count = 4;

			bsi.center_level = decoder_cfg.GetNumber(2);
			bsi.surround_level = decoder_cfg.GetNumber(2);
		break;
		case 6:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_LEFT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_RIGHT;

			bsi.full_chan_count = 4;

			bsi.surround_level = decoder_cfg.GetNumber(2);
		break;
		case 7:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
			decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;

			bsi.center_present = 1;
			bsi.full_chan_count = 5;

			bsi.center_level = decoder_cfg.GetNumber(2);
			bsi.surround_level = decoder_cfg.GetNumber(2);
		break;

	}

	if (bsi.lfe_present = decoder_cfg.NextBit()) {
		decoder_cfg.frame_cfg->chan_type[bsi.full_chan_count] = SPEAKER_LOW_FREQUENCY;
	}


	bsi.normalization[0] = decoder_cfg.GetNumber(5);

	if (decoder_cfg.NextBit()) {
		bsi.compression_gain[0] = decoder_cfg.GetNumber(8);
	}

	if (decoder_cfg.NextBit()) {
		bsi.language[0] = decoder_cfg.GetNumber(8);
	}

	if (decoder_cfg.NextBit()) {
		bsi.mixing_level[0] = decoder_cfg.GetNumber(5);
		
		decoder_cfg.GetNumber(2); // room type
	}

	if (bsi.coding_mode == 0) {
		bsi.normalization[1] = decoder_cfg.GetNumber(5);

		if (decoder_cfg.NextBit()) {
			bsi.compression_gain[1] = decoder_cfg.GetNumber(8);
		}

		if (decoder_cfg.NextBit()) {
			bsi.language[1] = decoder_cfg.GetNumber(8);
		}

		if (decoder_cfg.NextBit()) {
			bsi.mixing_level[1] = decoder_cfg.GetNumber(5);
			decoder_cfg.GetNumber(2); // room type
		}
	}

	decoder_cfg.NextBit(); // copyright

	decoder_cfg.NextBit(); // original

	if (decoder_cfg.NextBit()) {
		decoder_cfg.GetNumber(14); // some info 1 (dependes on bsi._id)
	}

	if (decoder_cfg.NextBit()) {
		decoder_cfg.GetNumber(14); // some info 2
	}

	if (decoder_cfg.NextBit()) { // additional info
		x_val = decoder_cfg.GetNumber(6);
		decoder_cfg.Skip(x_val + 1);		
	}

	
	decoder_cfg.frame_cfg->source_chan_count = bsi.full_chan_count + bsi.lfe_present;
	decoder_cfg.frame_cfg->source_frequency = sampling_frequency;
	decoder_cfg.frame_cfg->window_sample_count = 256;
	decoder_cfg.frame_cfg->window_count = 6;


// ===================================================================================================
				
	GetAudioBlock();
	TransformBlock(0);

	previous_frame_delta = -((UI_64)AUDIO_BLOCK_SIZE);

	decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;
	GetAudioBlock();
	TransformBlock(0);
	decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;
	GetAudioBlock();
	TransformBlock(0);
	decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;
	GetAudioBlock();
	TransformBlock(0);
	decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;
	GetAudioBlock();
	TransformBlock(0);
	decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;
	GetAudioBlock();
	TransformBlock(0);

	decoder_cfg.spectral_data -= AUDIO_BLOCK_SIZE*5;






	if (decoder_cfg.src_size > 2) {
// auxdata ==========================================================================================
		decoder_cfg.Skip(decoder_cfg.src_size - 2);




// crc2 =======================================================================================
		x_val = decoder_cfg.NextByte();
		x_val <<= 8;
		x_val |= decoder_cfg.NextByte();

	}



	return result;
}


