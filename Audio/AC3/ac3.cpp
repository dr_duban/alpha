/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\AC3.h"



#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"

#pragma warning(disable:4244)


__declspec(align(16)) float Audio::AC3::cos_table_0[2048];

float Audio::AC3::large_step_size[2][16];
float Audio::AC3::small_step_size[3][16];

int Audio::AC3::scale_tab[128];

char Audio::AC3::pack_11[128][2];
char Audio::AC3::pack_25[128][4];
char Audio::AC3::pack_9[32][4];

UI_64 Audio::AC3::Initialize() {
	float t_val(0);
	
	for (unsigned int i(0);i<128;i++) {
		scale_tab[i] = 0x3F800000 - (i << 23);		
	}

	for (unsigned int i(0);i<128;i++) {
		pack_11[i][0] = i / 11;
		pack_11[i][1] = i % 11;
	}

	for (unsigned int i(0);i<128;i++) {
		pack_25[i][0] = i / 25;
		pack_25[i][1] = i % 25;
		pack_25[i][2] = (pack_25[i][1] % 5);
		pack_25[i][1] /= 5;
	}

	for (unsigned int i(0);i<32;i++) {
		pack_9[i][0] = i / 9;
		pack_9[i][1] = i % 9;
		pack_9[i][2] = (pack_9[i][1] % 3);
		pack_9[i][1] /= 3;
	}
	


	GenCosTable(cos_table_0 + GenCosTable(cos_table_0, 512), 256);


	t_val = 2.0f;
	large_step_size[0][0] = 2.0f;
	for (unsigned int i(1);i<16;i++) {
		large_step_size[0][i] = 1.0f/(t_val - 1.0f);
		t_val *= 2.0f;
	}

	t_val = 4.0f;	
	for (unsigned int i(0);i<16;i++) {
		large_step_size[1][i] = 3.0f/(t_val - 2.0f);
		t_val *= 2.0f;
	}

	
	t_val = 2.0f;	
	for (unsigned int i(0);i<16;i++) {
		small_step_size[0][i] = 2.0f/(t_val - 1.0f);
		t_val += 2.0f;
	}

	t_val = 2.0f;	
	for (unsigned int i(0);i<16;i++) {
		small_step_size[1][i] = 1.0f/t_val;
		t_val *= 2.0f;
	}

	t_val = 4.0f;	
	for (unsigned int i(0);i<16;i++) {
		small_step_size[2][i] = 1.0f/t_val;
		t_val *= 2.0f;
	}
	return 0;
}

UI_64 Audio::AC3::Finalize() {

	return 0;
}



Audio::AC3::AC3(unsigned int es) : enhanced_syntax(es), sf_idx(0), sampling_frequency(0) {
	stream_cfg.sdata_offset = DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*EXPONENT_BLOCK_SIZE + (AC3_CHANNEL_SIZE/2);
	stream_cfg.frame_size = DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE;
}


Audio::AC3::~AC3() {

}


UI_64 Audio::AC3::Unpack(const unsigned char *, UI_64, I_64 &) {
	UI_64 result(RESULT_DECODE_COMPLETE);

	return result;
}

UI_64 Audio::AC3::DecodeUnit(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(RESULT_DECODE_COMPLETE), f_size(0);

	if (data_ptr) decoder_cfg.SetSource(data_ptr, data_length);

	if (decoder_cfg.src_ptr) {
		if (((decoder_cfg.src_ptr[0] == 0x0B) && (decoder_cfg.src_ptr[1] == 0x77)) || (stream_cfg.set_complete & COMPLETE_MKV_FUCK)) { // sync word

			if (!stream_cfg.frame_buff) {
				stream_cfg.frame_buff.New(frame_buf_tid, MAX_FRAME_COUNT*(DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE)*sizeof(float), SFSco::large_mem_mgr);
			}

			if (decoder_cfg.exponent_data = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {
				
				previous_frame_delta = stream_cfg.current_frame++;
				stream_cfg.current_frame &= (MAX_FRAME_COUNT - 1);

				if (previous_frame_delta < stream_cfg.current_frame) previous_frame_delta = -1;				

				decoder_cfg.exponent_data += stream_cfg.current_frame*(DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);
				decoder_cfg.frame_cfg = reinterpret_cast<DefaultRenderer::FrameCfg *>(decoder_cfg.exponent_data);
				

				decoder_cfg.exponent_data += DefaultRenderer::frame_cfg_size;
				decoder_cfg.spectral_data = reinterpret_cast<float *>(decoder_cfg.exponent_data + MAX_CHANNEL_COUNT*EXPONENT_BLOCK_SIZE);

				System::MemoryFill_SSE3(decoder_cfg.frame_cfg, (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE)*sizeof(float), 0, 0);

				
				previous_frame_delta *= (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);
				f_size = reinterpret_cast<DefaultRenderer::FrameCfg *>(reinterpret_cast<float *>(decoder_cfg.frame_cfg) + previous_frame_delta)->window_count;
				if ((f_size > 0) && (f_size < 6)) {
					f_size--;
				} else {
					f_size = 5;
				}

				previous_frame_delta += f_size*AUDIO_BLOCK_SIZE;


				
				decoder_cfg.frame_cfg->time_mark = time_mark;
				decoder_cfg.frame_cfg->channel_offset = AC3_CHANNEL_SIZE;

#ifdef _DEBUG
if (time_mark >= 0x0000AA00) {
	f_size = 0;

}
#endif
				if ((stream_cfg.set_complete & COMPLETE_MKV_FUCK) == 0) decoder_cfg.Skip(2);

				if (enhanced_syntax) {
					f_size = GetEFrame();
				} else {
					f_size = GetFrame();
				}

				
				decoder_cfg.frame_cfg->valid_sign = time_delta + decoder_cfg.frame_cfg->window_count*decoder_cfg.frame_cfg->window_sample_count*10000;
				time_delta = decoder_cfg.frame_cfg->valid_sign % decoder_cfg.frame_cfg->source_frequency;
				decoder_cfg.frame_cfg->valid_sign /= decoder_cfg.frame_cfg->source_frequency;

				time_mark = decoder_cfg.frame_cfg->valid_sign;

				data_ptr += f_size;
				data_length -= f_size;


				stream_cfg.frame_buff.Release();

				if (stream_cfg.ini_block == 0) {
					if (stream_cfg.ini_block = CreateEvent(0, 0, 0, 0)) {
						mixer_thread = CreateThread(0, 0, Audio::DefaultRenderer::MixerLoop, &stream_cfg, 0, 0);
						if (stream_cfg.set_complete & COMPLETE_CONTAINER) WaitForSingleObject(stream_cfg.ini_block, INFINITE);
					}	
				}
			}
		}
	}


	if (stream_cfg.flush_sign == -1) stream_cfg.flush_sign = -2;

	return result;
}

/*
	layout reconsideration

	frame cfg
	exponent data


	channel 0 6 windows
	channel 1 6 windows
	....


*/


void Audio::AC3::TransformBlock(unsigned int blk_idx) {
	UI_64 scale_fac(0xC0000000); // 0xBF800000

	for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
		Rescale(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE, 8);

		if (channels[i].xc.spectral_extension) {
			SynthetizeSE(i);
		}


		if (channels[i].switch_flag[blk_idx] == 0) {
			IDCT_IV(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE, cos_table_0, 256, decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE + (AC3_CHANNEL_SIZE/2), scale_fac);
		} else {
			Deinterleave(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE, 128);

			IDCT_IV(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE, cos_table_0 + 1024, 128, decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE + (AC3_CHANNEL_SIZE/2), scale_fac | 0x100000000);
			IDCT_IV(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE + 256, cos_table_0 + 1024, 128, decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE + (AC3_CHANNEL_SIZE/2) + 256, scale_fac | 0x200000000);
		}

		FormatWindow(decoder_cfg.spectral_data + i*AC3_CHANNEL_SIZE + (AC3_CHANNEL_SIZE/2), previous_frame_delta, win_table, 256);
	}

}


UI_64 Audio::AC3::FilterFrame(unsigned int *) {
	UI_64 result(0);

	return result;
}

	
void Audio::AC3::JumpOn() {


}

UI_64 Audio::AC3::UnitTest(const float *) {
	return 0;
}
			
UI_64 Audio::AC3::CfgOverride(UI_64, UI_64) {
	UI_64 result(0);

	return result;
}

unsigned int Audio::AC3::GetSatCount(unsigned int * sat_desc) {
	UI_64 f_off(0);
	unsigned int result(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);

		f_ptr += f_off;
		
		result = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
		for (unsigned int i(0);i<8;i++) sat_desc[i] = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->chan_type[i];

		stream_cfg.frame_buff.Release();
	}


	return result;
}

void Audio::AC3::GetSatInfo(unsigned int i_id, OpenGL::StringsBoard::InfoLine & i_line) {
	UI_64 f_off(0);

	if (int * f_ptr = reinterpret_cast<int *>(stream_cfg.frame_buff.Acquire())) {		
		f_off = stream_cfg.current_frame & (MAX_FRAME_COUNT - 1);
		f_off *= (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);

		f_ptr += f_off;
		
		switch (i_id) {
			case 0:
				if (enhanced_syntax) {
					i_line.cap[0] = L'E'; i_line.cap[1] = L'-'; i_line.cap[2] = L'A'; i_line.cap[3] = L'C'; i_line.cap[4] = L'3'; i_line.cap[5] = 0;
					i_line.cap_size = 5;
				} else {
					i_line.cap[0] = L'A'; i_line.cap[1] = L'C'; i_line.cap[2] = L'3'; i_line.cap[3] = 0;
					i_line.cap_size = 3;
				}
				i_line.cap_flags = 0;
			break;
			case 1:
				i_line.cap[0] = L'S'; i_line.cap[1] = L'F'; i_line.cap[2] = L':'; i_line.cap[3] = L'H'; i_line.cap[4] = L'z'; i_line.cap[5] = 0;
				i_line.cap_size = 0x00020003;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeDouble | OpenGL::StringsBoard::InfoTypeFormatted | 1;

				reinterpret_cast<double &>(i_line.val) = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			break;
			case 2:
				i_line.cap[0] = L'c'; i_line.cap[1] = L'h'; i_line.cap[2] = L'a'; i_line.cap[3] = L'n'; i_line.cap[4] = L'n'; i_line.cap[5] = L'e'; i_line.cap[6] = L'l'; i_line.cap[7] = L's'; i_line.cap[8] = 0;
				i_line.cap_size = 8;
				i_line.cap_flags = OpenGL::StringsBoard::InfoTypeINT64 | OpenGL::StringsBoard::InfoDataPrefix;

				i_line.val = reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_chan_count;
			break;
		}

		stream_cfg.frame_buff.Release();
	}
}


UI_64 Audio::AC3::FilterSatFrame(float * vector_ptr, OpenGL::DecodeRecord & de_rec) {
	float inc_val(0.0f);
	UI_64 result(0), last_frame(de_rec.target_time & 0xFFFFFFFF), last_adjust(de_rec.target_time >> 32), new_adjust(0), f_off(0), temp_val(0);

	if (last_frame == stream_cfg.current_frame) return 0;

	if (de_rec.start_time == 0) last_frame = -1;

	if (float * f_ptr = reinterpret_cast<float *>(stream_cfg.frame_buff.Acquire())) {

		result = -System::GetUCount(de_rec.start_time, 0);
		de_rec.start_time = System::GetTime();

		if (last_frame != -1) {
			result *= reinterpret_cast<DefaultRenderer::FrameCfg *>(f_ptr)->source_frequency;
			result /= 10000;

			last_adjust = AUDIO_BLOCK_SIZE*3 - last_adjust;

			if (result >= last_adjust) {
				result -= last_adjust;
				last_adjust = result % (AUDIO_BLOCK_SIZE*3);

				last_frame++;
			} else {
				last_adjust = (AUDIO_BLOCK_SIZE*3) - last_adjust + result;
			}

			last_frame += (result/(AUDIO_BLOCK_SIZE*3));
			last_frame &= (MAX_FRAME_COUNT - 1);

		} else {
			result = 8192;
			last_adjust = 512;

			last_frame = 5;
		}


		if (last_frame > stream_cfg.current_frame) last_frame = stream_cfg.current_frame;

		de_rec.target_time = last_adjust;
		de_rec.target_time <<= 32;
		de_rec.target_time |= last_frame;


		result = de_rec.width - last_adjust;		
		new_adjust = (result % (AUDIO_BLOCK_SIZE*3));
		result = (result/(AUDIO_BLOCK_SIZE*3));

		
		if (new_adjust) {
			f_off = (last_frame - result - 1) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);
			f_off += de_rec.pic_id*AC3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*EXPONENT_BLOCK_SIZE + (AC3_CHANNEL_SIZE/2));

			if (new_adjust > (AUDIO_BLOCK_SIZE/2)) {
				temp_val = (new_adjust + (AUDIO_BLOCK_SIZE/2) - 1)/(AUDIO_BLOCK_SIZE/2);

				f_off += (6 - temp_val)*AUDIO_BLOCK_SIZE;

				for (unsigned int si(temp_val*(AUDIO_BLOCK_SIZE/2) - new_adjust);si<(AUDIO_BLOCK_SIZE/2);si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

				f_off += AUDIO_BLOCK_SIZE;
				for (unsigned int wi(7 - temp_val);wi<6;wi++) {
					for (unsigned int si(0);si<(AUDIO_BLOCK_SIZE/2);si++) {
						vector_ptr[0] = inc_val;
						vector_ptr[1] = f_ptr[f_off + si];
						vector_ptr[2] = 0.0f;
						vector_ptr[3] = 1.0f;

						inc_val += 1.0f;
						vector_ptr += 4;
					}

				}

			} else {
				f_off += 4*AUDIO_BLOCK_SIZE + AUDIO_BLOCK_SIZE;
				
				for (unsigned int si((AUDIO_BLOCK_SIZE/2) - new_adjust);si<(AUDIO_BLOCK_SIZE/2);si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}
			}
		}	
		


		for (;result > 0;result--) {
			f_off = (last_frame - result) & (MAX_FRAME_COUNT - 1);
			f_off *= (DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);
			f_off += de_rec.pic_id*AC3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*EXPONENT_BLOCK_SIZE + (AC3_CHANNEL_SIZE/2));
			
			for (unsigned int wi(0);wi<6;wi++) {
				for (unsigned int si(0);si<(AUDIO_BLOCK_SIZE/2);si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

				f_off += AUDIO_BLOCK_SIZE;
			}
		}

		if (last_adjust) {
			f_off = last_frame*(DefaultRenderer::frame_cfg_size + AC3_FRAME_SIZE);
			f_off += de_rec.pic_id*AC3_CHANNEL_SIZE;
			f_off += (DefaultRenderer::frame_cfg_size + MAX_CHANNEL_COUNT*EXPONENT_BLOCK_SIZE + (AC3_CHANNEL_SIZE/2));
			
			result = last_adjust/(AUDIO_BLOCK_SIZE/2);

			for (unsigned int wi(0);wi<result;wi++) {
				for (unsigned int si(0);si<(AUDIO_BLOCK_SIZE/2);si++) {
					vector_ptr[0] = inc_val;
					vector_ptr[1] = f_ptr[f_off + si];
					vector_ptr[2] = 0.0f;
					vector_ptr[3] = 1.0f;

					inc_val += 1.0f;
					vector_ptr += 4;
				}

				last_adjust -= (AUDIO_BLOCK_SIZE/2);
				f_off += AUDIO_BLOCK_SIZE;
			}
						
			for (unsigned int si(0);si<last_adjust;si++) {
				vector_ptr[0] = inc_val;
				vector_ptr[1] = f_ptr[f_off + si];
				vector_ptr[2] = 0.0f;
				vector_ptr[3] = 1.0f;

				inc_val += 1.0f;
				vector_ptr += 4;
			}
			
		}	

		stream_cfg.frame_buff.Release();

		result = 0x00010000;
	}

	// maybe normalize vector


	return result;
}



// ======================================================================================================================================================================================
const unsigned int Audio::AC3::data_increment[4] = {5, 1, 1, 1};

const unsigned int Audio::AC3::bit_map[32] = {0, 32, 5, 27, 7, 25, 3, 29,		7, 25, 4, 28, 5, 27, 6, 26,		7, 25, 8, 24, 9, 23, 10, 22,	11, 21, 12, 20, 14, 18, 16, 16};

const unsigned int Audio::AC3::sampling_frequencies[16] = {
	48000, 44100, 32000, 0,
	24000, 22050, 16000, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,

};

const unsigned int Audio::AC3::frame_size_table[4][64] = {
	{
		64, 64, 80, 80, 96, 96, 112, 112,		128, 128, 160, 160, 192, 192, 224, 224,		256, 256, 320, 320, 384, 384, 448, 448,		512, 512, 640, 640, 768, 768, 896, 896,
		1024, 1024, 1152, 1152, 1280, 1280, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,			0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0
	},

	{
		69, 70, 87, 88, 104, 105, 121, 122,		139, 140, 174, 175, 208, 209, 243, 244,		278, 279, 348, 349, 417, 418, 487, 488,		557, 558, 696, 697, 835, 836, 975, 976,
		1114, 1115, 1253, 1254, 1393, 1394, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0
	},

	{
		96, 96, 120, 120, 144, 144, 168, 168,		192, 192, 240, 240, 288, 288, 336, 336,		384, 384, 480, 480, 576, 576, 672, 672,		768, 768, 960, 960, 1152, 1152, 1344, 1344,
		1536, 1536, 1728, 1728, 1920, 1920, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0

	},

	{
		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0,		0, 0, 0, 0, 0, 0, 0, 0
	}
};

const short Audio::AC3::floor_table[8] = {0x02F0, 0x02B0, 0x0270, 0x0230, 0x01F0, 0x0170, 0x00F0, -2048};

const unsigned char Audio::AC3::mask_tab[256] = {
	0,
	1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,
	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,
	25,	26,	27,	28,	28,	28,	29,	29,	29,	30,	30,	30,
	31,	31,	31,	32,	32,	32,	33,	33,	33,	34,	34,	34,
	35,	35,	35,	35,	35,	35,	36,	36,	36,	36,	36,	36,
	37,	37,	37,	37,	37,	37,	38,	38,	38,	38,	38,	38,
	39,	39,	39,	39,	39,	39,	40,	40,	40,	40,	40,	40,
	41,	41,	41,	41,	41,	41,	41,	41,	41,	41,	41,	41,
	42,	42,	42,	42,	42,	42,	42,	42,	42,	42,	42,	42,
	43,	43,	43,	43,	43,	43,	43,	43,	43,	43,	43,	43,
	44,	44,	44,	44,	44,	44,	44,	44,	44,	44,	44,	44,
	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,
	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,	45,
	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,
	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,	46,
	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,
	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,	47,
	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,
	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,	48,
	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,
	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,	49,
	0,	0,	0

};

const char Audio::AC3::la_tab[256] = {
	0x40, 0x3f, 0x3e, 0x3d, 0x3c, 0x3b, 0x3a, 0x39, 0x38, 0x37, 0x36, 0x35, 0x34, 0x34, 0x33, 0x32,
	0x31, 0x30, 0x2f, 0x2f, 0x2e, 0x2d, 0x2c, 0x2c, 0x2b, 0x2a, 0x29, 0x29, 0x28, 0x27, 0x26, 0x26,
	0x25, 0x24, 0x24, 0x23, 0x23, 0x22, 0x21, 0x21, 0x20, 0x20, 0x1f, 0x1e, 0x1e, 0x1d, 0x1d, 0x1c,
	0x1c, 0x1b, 0x1b, 0x1a, 0x1a, 0x19, 0x19, 0x18, 0x18, 0x17, 0x17, 0x16, 0x16, 0x15, 0x15, 0x15,
	0x14, 0x14, 0x13, 0x13, 0x13, 0x12, 0x12, 0x12, 0x11, 0x11, 0x11, 0x10, 0x10, 0x10, 0x0f, 0x0f,
	0x0f, 0x0e, 0x0e, 0x0e, 0x0d, 0x0d, 0x0d, 0x0d, 0x0c, 0x0c, 0x0c, 0x0c, 0x0b, 0x0b, 0x0b, 0x0b,
	0x0a, 0x0a, 0x0a, 0x0a, 0x0a, 0x09, 0x09, 0x09, 0x09, 0x09, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
	0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x05, 0x05,
	0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
	0x04, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x02,
	0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
	0x02, 0x02, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

};


const short Audio::AC3::hearing_threshold_tab[4][50] = {
	{ 0x04D0, 0x04D0, 0x0440, 0x0400, 0x03E0, 0x03C0, 0x03B0, 0x03B0,		0x03A0, 0x03A0, 0x03A0, 0x03A0, 0x03A0, 0x0390, 0x0390, 0x0390,		0x0380, 0x0380, 0x0370, 0x0370, 0x0360, 0x0360, 0x0350, 0x0350,		0x0340, 0x0340, 0x0330, 0x0320, 0x0310, 0x0300, 0x02F0, 0x02F0,		0x02F0, 0x02F0, 0x0300, 0x0310, 0x0340, 0x0390, 0x03E0, 0x0420,		0x0460, 0x0490, 0x04A0, 0x0460, 0x0440, 0x0440, 0x0520, 0x0800, 0x0840, 0x0840 },
	{ 0x04F0, 0x04F0, 0x0460, 0x0410, 0x03E0, 0x03D0, 0x03C0, 0x03B0,		0x03B0, 0x03A0, 0x03A0, 0x03A0, 0x03A0, 0x03A0, 0x0390, 0x0390,		0x0390, 0x0380, 0x0380, 0x0380, 0x0370, 0x0370, 0x0360, 0x0360,		0x0350, 0x0350, 0x0340, 0x0340, 0x0320, 0x0310, 0x0300, 0x02F0,		0x02F0, 0x02F0, 0x02F0, 0x0300, 0x0320, 0x0350, 0x0390, 0x03E0,		0x0420, 0x0450, 0x04A0, 0x0490, 0x0460, 0x0440, 0x0480, 0x0630, 0x0840, 0x0840 },
	{ 0x0580, 0x0580, 0x04B0, 0x0450, 0x0420, 0x03F0, 0x03E0, 0x03D0,		0x03C0, 0x03B0, 0x03B0, 0x03B0, 0x03A0, 0x03A0, 0x03A0, 0x03A0,		0x03A0, 0x03A0, 0x03A0, 0x03A0, 0x0390, 0x0390, 0x0390, 0x0390,		0x0380, 0x0380, 0x0380, 0x0370, 0x0360, 0x0350, 0x0340, 0x0330,		0x0320, 0x0310, 0x0300, 0x02F0, 0x02F0, 0x02F0, 0x0300, 0x0310,		0x0330, 0x0350, 0x03C0, 0x0410, 0x0470, 0x04A0, 0x0460, 0x0440, 0x0450, 0x04E0 },
	{ 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

};

const unsigned int Audio::AC3::bap_tab[64] = {
	0, 1, 1, 1, 1, 1, 2, 2,
	3, 3, 3, 4, 4, 5, 5, 6,
	6, 6, 6, 7, 7, 7, 7, 8,
	8, 8, 8, 9, 9, 9, 9, 10,
	10, 10, 10, 11, 11, 11, 11, 12,
	12, 12, 12, 13, 13, 13, 13, 14,
	14, 14, 14, 14, 14, 14, 14, 15,
	15, 15, 15, 15, 15, 15, 15, 15

};


__declspec(align(16)) const float Audio::AC3::win_table[512] = {
	0.00014f, 0.00024f, 0.00037f, 0.00051f, 0.00067f, 0.00086f, 0.00107f, 0.00130f, 0.00157f, 0.00187f, 0.00220f, 0.00256f, 0.00297f, 0.00341f, 0.00390f, 0.00443f, 
	0.00501f, 0.00564f, 0.00632f, 0.00706f, 0.00785f, 0.00871f, 0.00962f, 0.01061f, 0.01166f, 0.01279f, 0.01399f, 0.01526f, 0.01662f, 0.01806f, 0.01959f, 0.02121f, 
	0.02292f, 0.02472f, 0.02662f, 0.02863f, 0.03073f, 0.03294f, 0.03527f, 0.03770f, 0.04025f, 0.04292f, 0.04571f, 0.04862f, 0.05165f, 0.05481f, 0.05810f, 0.06153f, 
	0.06508f, 0.06878f, 0.07261f, 0.07658f, 0.08069f, 0.08495f, 0.08935f, 0.09389f, 0.09859f, 0.10343f, 0.10842f, 0.11356f, 0.11885f, 0.12429f, 0.12988f, 0.13563f, 
	0.14152f, 0.14757f, 0.15376f, 0.16011f, 0.16661f, 0.17325f, 0.18005f, 0.18699f, 0.19407f, 0.20130f, 0.20867f, 0.21618f, 0.22382f, 0.23161f, 0.23952f, 0.24757f, 
	0.25574f, 0.26404f, 0.27246f, 0.28100f, 0.28965f, 0.29841f, 0.30729f, 0.31626f, 0.32533f, 0.33450f, 0.34376f, 0.35311f, 0.36253f, 0.37204f, 0.38161f, 0.39126f, 
	0.40096f, 0.41072f, 0.42054f, 0.43040f, 0.44030f, 0.45023f, 0.46020f, 0.47019f, 0.48020f, 0.49022f, 0.50025f, 0.51028f, 0.52031f, 0.53033f, 0.54033f, 0.55031f, 
	0.56026f, 0.57019f, 0.58007f, 0.58991f, 0.59970f, 0.60944f, 0.61912f, 0.62873f, 0.63827f, 0.64774f, 0.65713f, 0.66643f, 0.67564f, 0.68476f, 0.69377f, 0.70269f, 
	0.71150f, 0.72019f, 0.72877f, 0.73723f, 0.74557f, 0.75378f, 0.76186f, 0.76981f, 0.77762f, 0.78530f, 0.79283f, 0.80022f, 0.80747f, 0.81457f, 0.82151f, 0.82831f, 
	0.83496f, 0.84145f, 0.84779f, 0.85398f, 0.86001f, 0.86588f, 0.87160f, 0.87716f, 0.88257f, 0.88782f, 0.89291f, 0.89785f, 0.90264f, 0.90728f, 0.91176f, 0.91610f, 
	0.92028f, 0.92432f, 0.92822f, 0.93197f, 0.93558f, 0.93906f, 0.94240f, 0.94560f, 0.94867f, 0.95162f, 0.95444f, 0.95713f, 0.95971f, 0.96217f, 0.96451f, 0.96674f, 
	0.96887f, 0.97089f, 0.97281f, 0.97463f, 0.97635f, 0.97799f, 0.97953f, 0.98099f, 0.98236f, 0.98366f, 0.98488f, 0.98602f, 0.98710f, 0.98811f, 0.98905f, 0.98994f, 
	0.99076f, 0.99153f, 0.99225f, 0.99291f, 0.99353f, 0.99411f, 0.99464f, 0.99513f, 0.99558f, 0.99600f, 0.99639f, 0.99674f, 0.99706f, 0.99736f, 0.99763f, 0.99788f, 
	0.99811f, 0.99831f, 0.99850f, 0.99867f, 0.99882f, 0.99895f, 0.99908f, 0.99919f, 0.99929f, 0.99938f, 0.99946f, 0.99953f, 0.99959f, 0.99965f, 0.99969f, 0.99974f, 
	0.99978f, 0.99981f, 0.99984f, 0.99986f, 0.99988f, 0.99990f, 0.99992f, 0.99993f, 0.99994f, 0.99995f, 0.99996f, 0.99997f, 0.99998f, 0.99998f, 0.99998f, 0.99999f, 
	0.99999f, 0.99999f, 0.99999f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 

	1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 1.00000f, 0.99999f, 0.99999f, 0.99999f, 
	0.99999f, 0.99998f, 0.99998f, 0.99998f, 0.99997f, 0.99996f, 0.99995f, 0.99994f, 0.99993f, 0.99992f, 0.99990f, 0.99988f, 0.99986f, 0.99984f, 0.99981f, 0.99978f, 
	0.99974f, 0.99969f, 0.99965f, 0.99959f, 0.99953f, 0.99946f, 0.99938f, 0.99929f, 0.99919f, 0.99908f, 0.99895f, 0.99882f, 0.99867f, 0.99850f, 0.99831f, 0.99811f, 
	0.99788f, 0.99763f, 0.99736f, 0.99706f, 0.99674f, 0.99639f, 0.99600f, 0.99558f, 0.99513f, 0.99464f, 0.99411f, 0.99353f, 0.99291f, 0.99225f, 0.99153f, 0.99076f, 
	0.98994f, 0.98905f, 0.98811f, 0.98710f, 0.98602f, 0.98488f, 0.98366f, 0.98236f, 0.98099f, 0.97953f, 0.97799f, 0.97635f, 0.97463f, 0.97281f, 0.97089f, 0.96887f, 
	0.96674f, 0.96451f, 0.96217f, 0.95971f, 0.95713f, 0.95444f, 0.95162f, 0.94867f, 0.94560f, 0.94240f, 0.93906f, 0.93558f, 0.93197f, 0.92822f, 0.92432f, 0.92028f, 
	0.91610f, 0.91176f, 0.90728f, 0.90264f, 0.89785f, 0.89291f, 0.88782f, 0.88257f, 0.87716f, 0.87160f, 0.86588f, 0.86001f, 0.85398f, 0.84779f, 0.84145f, 0.83496f, 
	0.82831f, 0.82151f, 0.81457f, 0.80747f, 0.80022f, 0.79283f, 0.78530f, 0.77762f, 0.76981f, 0.76186f, 0.75378f, 0.74557f, 0.73723f, 0.72877f, 0.72019f, 0.71150f, 
	0.70269f, 0.69377f, 0.68476f, 0.67564f, 0.66643f, 0.65713f, 0.64774f, 0.63827f, 0.62873f, 0.61912f, 0.60944f, 0.59970f, 0.58991f, 0.58007f, 0.57019f, 0.56026f, 
	0.55031f, 0.54033f, 0.53033f, 0.52031f, 0.51028f, 0.50025f, 0.49022f, 0.48020f, 0.47019f, 0.46020f, 0.45023f, 0.44030f, 0.43040f, 0.42054f, 0.41072f, 0.40096f, 
	0.39126f, 0.38161f, 0.37204f, 0.36253f, 0.35311f, 0.34376f, 0.33450f, 0.32533f, 0.31626f, 0.30729f, 0.29841f, 0.28965f, 0.28100f, 0.27246f, 0.26404f, 0.25574f, 
	0.24757f, 0.23952f, 0.23161f, 0.22382f, 0.21618f, 0.20867f, 0.20130f, 0.19407f, 0.18699f, 0.18005f, 0.17325f, 0.16661f, 0.16011f, 0.15376f, 0.14757f, 0.14152f, 
	0.13563f, 0.12988f, 0.12429f, 0.11885f, 0.11356f, 0.10842f, 0.10343f, 0.09859f, 0.09389f, 0.08935f, 0.08495f, 0.08069f, 0.07658f, 0.07261f, 0.06878f, 0.06508f, 
	0.06153f, 0.05810f, 0.05481f, 0.05165f, 0.04862f, 0.04571f, 0.04292f, 0.04025f, 0.03770f, 0.03527f, 0.03294f, 0.03073f, 0.02863f, 0.02662f, 0.02472f, 0.02292f, 
	0.02121f, 0.01959f, 0.01806f, 0.01662f, 0.01526f, 0.01399f, 0.01279f, 0.01166f, 0.01061f, 0.00962f, 0.00871f, 0.00785f, 0.00706f, 0.00632f, 0.00564f, 0.00501f, 
	0.00443f, 0.00390f, 0.00341f, 0.00297f, 0.00256f, 0.00220f, 0.00187f, 0.00157f, 0.00130f, 0.00107f, 0.00086f, 0.00067f, 0.00051f, 0.00037f, 0.00024f, 0.00014f

};

// extended =======================================================================================================================================================================================

const unsigned int Audio::AC3::hebap_tab[64] = {
	0, 1, 2, 3, 4, 5, 6, 7,
	8, 8, 8, 8, 9, 9, 9, 10,
	10, 10, 10, 11, 11, 11, 11, 12,
	12, 12, 12, 13, 13, 13, 13, 14,
	
	14, 14, 14, 15, 15, 15, 15, 16,
	16, 16, 16, 17, 17, 17, 17, 18,
	18, 18, 18, 18, 18, 18, 18, 19,
	19, 19, 19, 19, 19, 19, 19, 19

};

const unsigned int Audio::AC3::ebit_map[40] = {0, 32, 2, 30, 3, 29, 4, 28,		5, 27, 7, 25, 8, 24, 9, 23,		3, 29, 4, 28, 5, 27, 6, 26,	7, 25, 8, 24, 9, 23, 10, 22,	11, 21, 12, 20, 14, 18, 16, 16};

const unsigned char Audio::AC3::ah_gain_map5[32][4] = {
	{0, 0, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 2, 0},

	{0, 1, 0, 0},
	{0, 1, 1, 0},
	{0, 1, 2, 0},

	{0, 2, 0, 0},
	{0, 2, 1, 0},
	{0, 2, 2, 0},


	{1, 0, 0, 0},
	{1, 0, 1, 0},
	{1, 0, 2, 0},

	{1, 1, 0, 0},
	{1, 1, 1, 0},
	{1, 1, 2, 0},

	{1, 2, 0, 0},
	{1, 2, 1, 0},
	{1, 2, 2, 0},


	{2, 0, 0, 0},
	{2, 0, 1, 0},
	{2, 0, 2, 0},

	{2, 1, 0, 0},
	{2, 1, 1, 0},
	{2, 1, 2, 0},

	{2, 2, 0, 0},
	{2, 2, 1, 0},
	{2, 2, 2, 0},

	{3, 0, 0, 0},
	{3, 0, 1, 0},
	{3, 0, 2, 0},

	{3, 1, 0, 0},

};

const int Audio::AC3::he_remap_a[][3] = {
	{4681, -10923, -4681},
	{2185, -14043, -6554},
	{1057, -15292, -7399},
	{520, -15855, -7802},
	{258, -16124, -7998},
	{129, -16255, -8096},
	{64, -16320, -8144},
	{32, -16352, -8168},
	{16, -16368, -8180},
	{8, 0, 0},
	{2, 0, 0},
	{0, 0, 0}
};

const int Audio::AC3::he_remap_b[][2] = {
	{-5461, -1170},
	{-11703, -4915},
	{-14199, -6606},
	{-15327, -7412},
	{-15864, -7805},
	{-16126, -7999},
	{-16255, -8096},
	{-16320, -8144},
	{-16352, -8168},
	{0, 0},
	{0, 0},
	{0, 0}
};

const unsigned char Audio::AC3::exp_str_map[32][6] = {
	{1, 0, 0, 0, 0, 0},
	{1, 0, 0, 0, 0, 3},
	{1, 0, 0, 0, 2, 0},
	{1, 0, 0, 0, 3, 3},
	{2, 0, 0, 2, 0, 0},
	{2, 0, 0, 2, 0, 3},
	{2, 0, 0, 3, 2, 0},
	{2, 0, 0, 3, 3, 3},
	{2, 0, 1, 0, 0, 0},
	{2, 0, 2, 0, 0, 3},
	{2, 0, 2, 0, 2, 0},
	{2, 0, 2, 0, 3, 3},
	{2, 0, 3, 2, 0, 0},
	{2, 0, 3, 2, 0, 3},
	{2, 0, 3, 3, 2, 0},
	{2, 0, 3, 3, 3, 3},
	{3, 1, 0, 0, 0, 0},
	{3, 1, 0, 0, 0, 3},
	{3, 2, 0, 0, 2, 0},
	{3, 2, 0, 0, 3, 3},
	{3, 2, 0, 2, 0, 0},
	{3, 2, 0, 2, 0, 3},
	{3, 2, 0, 3, 2, 0},
	{3, 2, 0, 3, 3, 3},
	{3, 3, 1, 0, 0, 0},
	{3, 3, 2, 0, 0, 3},
	{3, 3, 2, 0, 2, 0},
	{3, 3, 2, 0, 3, 3},
	{3, 3, 3, 2, 0, 0},
	{3, 3, 3, 2, 0, 3},
	{3, 3, 3, 3, 2, 0},
	{3, 3, 3, 3, 3, 3}

};

const unsigned char Audio::AC3::default_cpl_bs[] = { 0, 0, 0, 0, 0, 0, 0, 0,	1, 0, 1, 1, 0, 1, 1, 1, 1, 1};
const unsigned char Audio::AC3::default_xse_bs[] = { 0, 0, 0, 0, 0, 0, 0, 0,	1, 0, 1, 0, 1, 0, 1, 0, 1};
const unsigned char Audio::AC3::default_ecpl_bs[] = {0, 0, 0, 0, 0, 0, 0, 0,	0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1};


const int Audio::AC3::i_float_tab[] = {
	0x00000000, 0x3F800000, 0x40000000, 0x40400000, 0x40800000, 0x40A00000, 0x40C00000, 0x40E00000,
	0x41000000, 0x41100000, 0x41200000, 0x41300000, 0x41400000, 0x41500000, 0x41600000, 0x41700000,
	0x41800000, 0x41880000, 0x41900000, 0x41980000, 0x41A00000, 0x41A80000, 0x41B00000, 0x41B80000,
	0x41C00000, 0x41C80000, 0x41D00000, 0x41D80000,	0x41E00000, 0x41E80000, 0x41F00000, 0x41F80000
};

const float Audio::AC3::border_filter_coffs[][3] = {
	{0.954841604f, 0.911722489f, 0.870550563f},
	{0.911722489f, 0.831237896f, 0.757858283f},
	{0.870550563f, 0.757858283f, 0.659753955f},
	{0.831237896f, 0.690956440f, 0.574349177f},
	{0.793700526f, 0.629960525f, 0.500000000f},
	{0.757858283f, 0.574349177f, 0.435275282f},
	{0.723634619f, 0.523647061f, 0.378929142f},
	{0.690956440f, 0.477420802f, 0.329876978f},
	{0.659753955f, 0.435275282f, 0.287174589f},
	{0.629960525f, 0.396850263f, 0.250000000f},
	{0.601512518f, 0.361817309f, 0.217637641f},
	{0.574349177f, 0.329876978f, 0.189464571f},
	{0.548412490f, 0.300756259f, 0.164938489f},
	{0.523647061f, 0.274206245f, 0.143587294f},
	{0.500000000f, 0.250000000f, 0.125000000f},
	{0.477420802f, 0.227930622f, 0.108818820f},
	{0.455861244f, 0.207809474f, 0.094732285f},
	{0.435275282f, 0.189464571f, 0.082469244f},
	{0.415618948f, 0.172739110f, 0.071793647f},
	{0.396850263f, 0.157490131f, 0.062500000f},
	{0.378929142f, 0.143587294f, 0.054409410f},
	{0.361817309f, 0.130911765f, 0.047366143f},
	{0.345478220f, 0.119355200f, 0.041234622f},
	{0.329876978f, 0.108818820f, 0.035896824f},
	{0.314980262f, 0.099212566f, 0.031250000f},
	{0.300756259f, 0.090454327f, 0.027204705f},
	{0.287174589f, 0.082469244f, 0.023683071f},
	{0.274206245f, 0.075189065f, 0.020617311f},
	{0.261823531f, 0.068551561f, 0.017948412f},
	{0.250000000f, 0.062500000f, 0.015625000f},
	{0.238710401f, 0.056982656f, 0.013602353f},
	{0.227930622f, 0.051952369f, 0.011841536f}

};

const int Audio::AC3::symmetric_mantab[6][16] = {
	{	0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000	},
	{	0xBF2AAAAA, 0x00000000, 0x3F2AAAAA, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000	},
	{	0xBF4CCCCC, 0xBECCCCCC, 0x00000000, 0x3ECCCCCC,	0x3F4CCCCC, 0x00000000, 0x00000000, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000	},
	{	0xBF5B6DB6, 0xBF124924, 0xBE924924, 0x00000000,	0x3E924924, 0x3F124924, 0x3F5B6DB6, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000,	0x00000000, 0x00000000, 0x00000000, 0x00000000	},
	{	0xBF68BA2E, 0xBF3A2E8B, 0xBF0BA2E8, 0xBEBA2E8B, 0xBE3A2E8B, 0x00000000, 0x3E3A2E8B, 0x3EBA2E8B, 0x3F0BA2E8, 0x3F3A2E8B, 0x3F68BA2E, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000	},
	{	0xBF6EEEEE, 0xBF4CCCCC, 0xBF2AAAAA, 0xBF088888,	0xBECCCCCC, 0xBE888888, 0xBE088888, 0x00000000,	0x3E088888, 0x3E888888, 0x3ECCCCCC, 0x3F088888,	0x3F2AAAAA, 0x3F4CCCCC, 0x3F6EEEEE, 0x00000000	}
};
