/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Audio\AC3.h"
#include "System\SysUtils.h"

#include "Math\CalcSpecial.h"

void Audio::AC3::SynthetizeSE(unsigned int c_idx) {
	float t_vals[8];
	float * src_ptr = decoder_cfg.spectral_data + c_idx*AC3_CHANNEL_SIZE;
	float * dst_ptr = src_ptr, * src_ref(0);

	int period = e_frame.se_range[0] - e_frame.se_start_copy_freq, s_idx(0), d_idx(0), p_idx(0);

	s_idx = e_frame.se_start_copy_freq + 1;
	src_ptr += ((s_idx & 0xFFFFFFFC) << 1) + (s_idx & 3);
	src_ref = src_ptr;

	d_idx = e_frame.se_range[0] + 1;
	dst_ptr += ((d_idx & 0xFFFFFFFC) << 1) + (d_idx & 3);
	

	for (unsigned short bi(0);bi<e_frame.band_counter;bi++) {
		channels_se[c_idx].rms_e[bi] = 0.0f;

		for (unsigned int i_idx(0);i_idx < e_frame.band_size[bi];i_idx++) {
			dst_ptr[0] = src_ptr[0];			
			if (dst_ptr[4] = (int)System::Gen32()) {
				reinterpret_cast<int *>(dst_ptr)[4] -= 0x10000000;
			}

			channels_se[c_idx].rms_e[bi] += dst_ptr[0]*dst_ptr[0];

			src_ptr += data_increment[++s_idx & 3];
			dst_ptr += data_increment[++d_idx & 3];

			if (++p_idx >= period) {
				p_idx = 0;	
				src_ptr = src_ref;
			}
		}

		channels_se[c_idx].rms_e[bi] /= e_frame.band_size[bi];		
	}

	Calc::Float4NSqrt(channels_se[c_idx].rms_e, 5);
	
	// apply border filter
	if (channels[c_idx].xc.border_filter) {
		for (int i(0);;i++) {		
			s_idx = period*i;
			if (s_idx >= e_frame.se_range[1]) break;

			s_idx += e_frame.se_range[0] - 1;

			src_ptr = decoder_cfg.spectral_data + c_idx*AC3_CHANNEL_SIZE; + ((s_idx & 0xFFFFFFFC) << 1) + (s_idx & 3);

			src_ptr[0] *= border_filter_coffs[channels[c_idx].xc.attenuation_code][0];

			src_ptr += data_increment[++s_idx & 3];
			src_ptr[0] *= border_filter_coffs[channels[c_idx].xc.attenuation_code][1];

			src_ptr += data_increment[++s_idx & 3];
			src_ptr[0] *= border_filter_coffs[channels[c_idx].xc.attenuation_code][2];

			src_ptr += data_increment[++s_idx & 3];
			src_ptr[0] *= border_filter_coffs[channels[c_idx].xc.attenuation_code][1];

			src_ptr += data_increment[++s_idx & 3];
			src_ptr[0] *= border_filter_coffs[channels[c_idx].xc.attenuation_code][0];
		}

	}	

	// do blending
	
	dst_ptr = decoder_cfg.spectral_data + c_idx*AC3_CHANNEL_SIZE;

	d_idx = e_frame.se_range[0] + 1;
	dst_ptr += ((d_idx & 0xFFFFFFFC) << 1) + (d_idx & 3);
	

	for (unsigned short bi(0);bi<e_frame.band_counter;bi++) {
		t_vals[0] = channels_se[c_idx].rms_e[bi]*channels_se[c_idx].blend[bi][0];

		for (unsigned int i_idx(0);i_idx < e_frame.band_size[bi];i_idx++) {
			dst_ptr[0] = dst_ptr[0]*channels_se[c_idx].blend[bi][1] + dst_ptr[4]*t_vals[0];
			dst_ptr[0] *= channels_se[c_idx].spxco[bi];

			dst_ptr[4] = dst_ptr[0];

			dst_ptr += data_increment[++d_idx & 3];
		}		
	}
}

UI_64 Audio::AC3::GetEFrame() { // extended syntax
	UI_64 result(0);
	
	System::MemoryFill(&e_frame, sizeof(EFrame), 0);
	System::MemoryFill(channels, MAX_CHANNEL_COUNT*sizeof(ChanCfg), 0);
	System::MemoryFill(&coupling, sizeof(CouplingInfo), 0);
	bai.Clear();

	range_gain[0] = range_gain[1] = 0;



// ============================================================================================
	result = GetEBSI();

	GetEFrameHdr();

	GetEAudioBlock(0);
	TransformBlock(0);

	previous_frame_delta = -((UI_64)AUDIO_BLOCK_SIZE);


	for (unsigned int i(1);i<bsi.xi.block_count;i++) {
		decoder_cfg.spectral_data += AUDIO_BLOCK_SIZE;

		GetEAudioBlock(i);
		TransformBlock(i);
	}



// ==========================================================================================
	if (decoder_cfg.src_size > 2) {

	// auxdata
	

	// e check
	}

	return result;
}

void Audio::AC3::GetBS(short * range, unsigned short * band_size, unsigned short & band_counter, const unsigned char * ddef_bs) {

	if (decoder_cfg.NextBit()) {
		band_counter = 0;
		band_size[band_counter] = 12;

		for (short i(range[0] + 1);i<range[1];i++) {
			if (decoder_cfg.NextBit()) {
				band_size[band_counter] += 12;
			} else {
				band_size[++band_counter] = 12;
			}
		}

		band_counter++;
	} else {
		if (ddef_bs) {

			band_counter = 0;
			band_size[band_counter] = 12;

			for (short i(range[0] + 1);i<range[1];i++) {
				if (ddef_bs[i]) {
					band_size[band_counter] += 12;
				} else {
					band_size[++band_counter] = 12;
				}
			}

			band_counter++;
		}
	}
}

void Audio::AC3::GetEAudioBlock(unsigned int blk_idx) {
	float t_vals[4];
	int * mask_ptr(0), d_vals[64] = {0, 0, 0, 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0};
	unsigned int x_val(1), x_val2(0);
	MaskRecord ma_re;

	if (e_frame.block_switch_flag) {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].switch_flag[blk_idx] = decoder_cfg.NextBit();
	}

	if (e_frame.dither_flag) {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].dither_flag = decoder_cfg.NextBit();
	} else {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].dither_flag = 1;
	}

	if (decoder_cfg.NextBit()) {
		range_gain[0] = decoder_cfg.GetNumber(8);
	}

	if (bsi.coding_mode == 0) {
		if (decoder_cfg.NextBit()) range_gain[1] = decoder_cfg.GetNumber(8);
	}

// spectral extension ========================================================================================================================
	if (blk_idx) {
		x_val = decoder_cfg.NextBit();
	} else {
		x_val = 1;
	}

	if (x_val) {
		if (e_frame.se_in_use = decoder_cfg.NextBit()) {

			if (bsi.coding_mode == 1) {
				channels[0].xc.spectral_extension = 1;
			} else {
				for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].xc.spectral_extension = decoder_cfg.NextBit();
			}

			e_frame.se_start_copy_freq = decoder_cfg.GetNumber(2)*12 + 24;
			e_frame.se_range[0] = decoder_cfg.GetNumber(3);
			e_frame.se_range[1] = decoder_cfg.GetNumber(3);

			if (e_frame.se_range[0] < 6) e_frame.se_range[0] += 2;
			else e_frame.se_range[0] = (e_frame.se_range[0]<<1) - 3;
			if (e_frame.se_range[1] < 3) e_frame.se_range[1] += 5;
			else e_frame.se_range[1] = (e_frame.se_range[1]<<1) + 3;

			GetBS(e_frame.se_range, e_frame.band_size, e_frame.band_counter, (blk_idx)?0:default_xse_bs);
			
			e_frame.se_range[0] *= 12;
			e_frame.se_range[0] += 24;
			
			e_frame.se_range[1] = 0;
			for (unsigned short i(0);i<e_frame.band_counter;i++) e_frame.se_range[1] += e_frame.band_size[i];

			
		} else {
			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				channels[i].xc.spectral_extension = 0;
				channels[i].xc.first_s_coords = 1;
			}
		}
	}

	if (e_frame.se_in_use) {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			if (channels[i].xc.spectral_extension) {
				if (channels[i].xc.first_s_coords) {
					channels[i].xc.first_s_coords = 0;
					x_val2 = 1;						
				} else {
					x_val2 = decoder_cfg.NextBit();
				}

				if (x_val2) {
					if (reinterpret_cast<int *>(t_vals)[0] = i_float_tab[decoder_cfg.GetNumber(5)]) { // channels[i].xc.spectral_blend
						reinterpret_cast<int *>(t_vals)[0] -= 0x02800000;
					}
					
					t_vals[1] = e_frame.se_range[0];
					t_vals[2] = (e_frame.se_range[0] + e_frame.se_range[1]);

					channels[i].xc.master_coord = 3*decoder_cfg.GetNumber(2) + 2 - 5;


					for (unsigned int j(0);j<e_frame.band_counter;j++) {
						channels[i].se_coords[j][0] = decoder_cfg.GetNumber(4);
						channels[i].se_coords[j][1] = decoder_cfg.GetNumber(2);

						if (channels[i].se_coords[j][0] < 15) {
							++channels[i].se_coords[j][0];
							channels[i].se_coords[j][1] |= 4;
						}
						
						if (channels[i].se_coords[j][1]) {
							reinterpret_cast<int *>(channels_se[i].spxco)[j] = i_float_tab[channels[i].se_coords[j][1]] - ((channels[i].se_coords[j][0] + channels[i].xc.master_coord) << 23);
						} else {
							channels_se[i].spxco[j] = 0;
						}

						channels_se[i].blend[j][0] = t_vals[1];
						t_vals[3] = e_frame.band_size[j];
						t_vals[1] += t_vals[3];
						channels_se[i].blend[j][0] += 0.5f*t_vals[3];
						channels_se[i].blend[j][0] /= t_vals[2];
						channels_se[i].blend[j][0] -= t_vals[0];

						if (channels_se[i].blend[j][0] < 0.0f) channels_se[i].blend[j][0] = 0.0f;
						if (channels_se[i].blend[j][0] > 1.0f) channels_se[i].blend[j][0] = 1.0f;

						channels_se[i].blend[j][1] = 1.0f - channels_se[i].blend[j][0];

						Calc::Float2Sqrt(channels_se[i].blend[j]);						
						if (reinterpret_cast<int *>(channels_se[i].blend[j])[0]) reinterpret_cast<int *>(channels_se[i].blend[j])[0] -= 0x10000000;
					}

				}

			} else {
				channels[i].xc.first_s_coords = 1;
			}

		}
	}

// coupling =========================================================================================================
	GetECoupling(blk_idx);



// exponents ========================================================================================================
	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		if (e_frame.blocks[blk_idx].exp_str[i]) {
			channels[i].range[0] = 0;

			if (channels[i].cpl.in_use | channels[i].xc.spectral_extension) {
				if (channels[i].cpl.in_use) {
					channels[i].range[1] = coupling.range[0] / 3;
				} else {
					if (channels[i].xc.spectral_extension) {
						channels[i].range[1] = e_frame.se_range[0] / 3;
					}
				}

			} else {
				channels[i].range[1] = decoder_cfg.GetNumber(6) + 12 + 12;
				
			}
						
			x_val = (e_frame.blocks[blk_idx].exp_str[i] - 1);
			channels[i].group_count = ((channels[i].range[1] + ((1 << x_val) - 1)) >> x_val);

			channels[i].range[1] *= 3;
		}
	}

	if (e_frame.blocks[blk_idx].coupling_in_use) {
		if (e_frame.blocks[blk_idx].coupling_strategy) {
			coupling.group_count = coupling.range[1] / 3;
			coupling.group_count >>= (e_frame.blocks[blk_idx].coupling_strategy - 1);
			GetExponents(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, coupling.group_count, e_frame.blocks[blk_idx].coupling_strategy, coupling.range[0]);
		}
	}

	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		if (e_frame.blocks[blk_idx].exp_str[i]) {
			GetExponents(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, channels[i].group_count, e_frame.blocks[blk_idx].exp_str[i], 0);
			channels[i].gain_range = decoder_cfg.GetNumber(2);
		}
	}

	if (bsi.lfe_present) {
		if (e_frame.blocks[blk_idx].lfe_strategy) {
			channels[bsi.full_chan_count].group_count = 2;
	
			channels[bsi.full_chan_count].range[0] = 0;
			channels[bsi.full_chan_count].range[1] = 2*3;

			GetExponents(decoder_cfg.exponent_data + bsi.full_chan_count*EXPONENT_BLOCK_SIZE + 256, channels[bsi.full_chan_count].group_count, e_frame.blocks[blk_idx].lfe_strategy, 0);
		}
	}


// parametric bit allocation info =================================================================================================
	if (e_frame.ba_model_flag) {
		if (decoder_cfg.NextBit()) { // bit allocation
			bai.slow_decay = 0x000F + (decoder_cfg.GetNumber(2)<<1);
			bai.fast_decay = 0x003F + (decoder_cfg.GetNumber(2)*0x0014);
		
			x_val = decoder_cfg.GetNumber(2);
			bai.slow_gain = 0x0540 - (x_val*0x0068) + ((x_val & 2)<<2);

			if (bai.dB_per_bit = decoder_cfg.GetNumber(2)) {
				bai.dB_per_bit *= 0x0200;
				bai.dB_per_bit += 0x0500;
			}

			bai.masking_floor = floor_table[decoder_cfg.GetNumber(3)];
		}
	} else {
		bai.slow_decay = 0x000F + (2<<1);
		bai.fast_decay = 0x003F + 0x0014;
		bai.slow_gain = 0x0540 - 0x0068;
		bai.dB_per_bit = 2*0x0200 + 0x0500;
		bai.masking_floor = floor_table[7];
	}


	if (e_frame.offset_strategy == 0) {
		x_val = e_frame.snr_offset[0];
		x_val -= 15;
		x_val <<= 4;

		x_val += e_frame.snr_offset[1];
		x_val <<= 2;

		if (e_frame.blocks[blk_idx].coupling_in_use) {
			coupling.msk.snr_offset = x_val;
		}

		for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
			channels[i].msk.snr_offset = x_val;
		}

	} else {
		if (blk_idx) x_val2 = decoder_cfg.NextBit();
		else x_val2 = 1;

		if (x_val2) {
			x_val = decoder_cfg.GetNumber(6);
			x_val -= 15;
			x_val <<= 4;

			switch (e_frame.offset_strategy) {
				case 1:
					x_val += decoder_cfg.GetNumber(4);
					x_val <<= 2;

					coupling.msk.snr_offset = x_val;	

					for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
						channels[i].msk.snr_offset = x_val;
					}

				break;
				case 2:

					if (e_frame.blocks[blk_idx].coupling_in_use) {
						coupling.msk.snr_offset = ((x_val + decoder_cfg.GetNumber(4)) << 2);
					}

					for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
						channels[i].msk.snr_offset = ((x_val + decoder_cfg.GetNumber(4)) << 2);
					}

				break;
			}
		}
	}


	if (e_frame.fast_gain_flag) {
		x_val2 = decoder_cfg.NextBit();;
	} else {
		x_val2 = 0;
	}


	if (x_val2) {
		if (e_frame.blocks[blk_idx].coupling_in_use) {
			coupling.msk.fast_gain = 0x0080 + (decoder_cfg.GetNumber(3)<<7);
		}

		for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
			channels[i].msk.fast_gain = 0x0080 + (decoder_cfg.GetNumber(3)<<7);
		}

	} else {
		if (e_frame.blocks[blk_idx].coupling_in_use) {
			coupling.msk.fast_gain = 0x0080 + (4<<7);
		}

		for (unsigned short i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
			channels[i].msk.fast_gain = 0x0080 + (4<<7);
		}
	}

	if (bsi.xi._type == 0) {
		if (decoder_cfg.NextBit()) e_frame.coverter_offset = decoder_cfg.GetNumber(10);
	}



	if (e_frame.blocks[blk_idx].coupling_in_use) {		
		if (e_frame.first_cpl_leak) {
			e_frame.first_cpl_leak = 0;
			x_val2 = 1;
		} else {
			x_val2 = decoder_cfg.NextBit();
		}

		if (x_val2) {
			coupling.msk.fast_leak = (decoder_cfg.GetNumber(3) << 8) + 768;
			coupling.msk.slow_leak = (decoder_cfg.GetNumber(3) << 8) + 768;
		}

		ma_re = coupling.msk;
		CalculateMaskingCurve(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, ma_re, coupling.range[0], coupling.range[1]);
	}

	for (int i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
		ma_re = channels[i].msk;
		CalculateMaskingCurve(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, ma_re, 0, channels[i].range[1]);
	}

// delta bit allocation =============================================================================
	if (e_frame.dba_flag) {
		if (decoder_cfg.NextBit()) {
			if (e_frame.blocks[blk_idx].coupling_in_use) {			
				d_vals[9] = decoder_cfg.GetNumber(2);

				if (d_vals[9] == 3) {
					// mute state

				}
			}

			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				d_vals[i] = decoder_cfg.GetNumber(2);
			}



			if ((e_frame.blocks[blk_idx].coupling_in_use) && (d_vals[9] == 1)) {
				mask_ptr = decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 128;
				d_vals[10] = decoder_cfg.GetNumber(3) + 1;
				d_vals[11] = 0;

				for (int j(0);j<d_vals[10];j++) {
					d_vals[11] += decoder_cfg.GetNumber(5);
					d_vals[12] = decoder_cfg.GetNumber(4);
					d_vals[13] = decoder_cfg.GetNumber(3);

					d_vals[13] -= 4;
					d_vals[13] = d_vals[13] - ~(d_vals[13] >> 31);
					d_vals[13] <<= 7;

					for (int k(0);k<d_vals[12];k++) {
						mask_ptr[d_vals[11]++] += d_vals[13];
					}
				}
			}

			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				switch (d_vals[i]) {
					case 1: // new info
						mask_ptr = decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 128;
				
						d_vals[10] = decoder_cfg.GetNumber(3); //  + 1;					
					// collect delta
						for (int j(0);j<=d_vals[10];j++) {
							d_vals[j*4 + 16] = decoder_cfg.GetNumber(5);
							d_vals[j*4 + 17] = decoder_cfg.GetNumber(4);
							d_vals[j*4 + 18] = decoder_cfg.GetNumber(3);

							d_vals[j*4 + 18] -= 4;
							d_vals[j*4 + 18] = d_vals[j*4 + 18] - ~(d_vals[j*4 + 18] >> 31);
							d_vals[j*4 + 18] <<= 7;

						}
					case 0: // previous applies
						d_vals[11] = 0;

						for (int j(0);j<=d_vals[10];j++) {
							d_vals[11] += d_vals[j*4 + 16];

							for (int k(0);k<d_vals[j*4 + 17];k++) {
								mask_ptr[d_vals[11]++] += d_vals[j*4 + 18];
							}
						}

					break;
					// case 2: no delta to apply
				}
			}
		}
	}

// skip =============================================================================================
	if (e_frame.skip_field_flag) {
		if (decoder_cfg.NextBit()) {
			decoder_cfg.Skip(decoder_cfg.GetNumber(9));
		}
	}

// get mantissa values ==============================================================================
	x_val = coupling._reserved;

	ma_vals[0] = 3; ma_vals[1] = 0; ma_vals[2] = 0; ma_vals[3] = 0;
	ma_vals[4] = 3; ma_vals[5] = 0; ma_vals[6] = 0; ma_vals[7] = 0;
	ma_vals[8] = 3; ma_vals[9] = 0; ma_vals[10] = 0; ma_vals[11] = 0;
	ma_vals[12] = 3; ma_vals[13] = 0; ma_vals[14] = 0; ma_vals[15] = 0;


	for (int i(0);i<(bsi.full_chan_count + bsi.lfe_present);i++) {
		if (channels[i].range[1]) {
			if (channels[i].xc.hybrid_in_use) {
				if (channels[i].xc.hybrid_in_use == 1) {
					channels[i].xc.hybrid_in_use = 2;

					CalculateEBAI(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, channels[i].msk.snr_offset, 0, channels[i].range[1]);
					GetEMantissas(i, 0, channels[i].range[1]);
				}
			} else {
				CalculateBAI(decoder_cfg.exponent_data + i*EXPONENT_BLOCK_SIZE + 256, channels[i].msk.snr_offset, 0, channels[i].range[1]);
				GetMantissas(i, 0, channels[i].range[1]);
				
			}
			

			if (channels[i].cpl.in_use) {
				if (e_frame.blocks[blk_idx].coupling_in_use && (x_val == 0)) {
					x_val = 1;

					if (e_frame.cpl_hybrid_in_use) {
						CalculateEBAI(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, coupling.msk.snr_offset, coupling.range[0], coupling.range[1]);
						GetEMantissas(MAX_CHANNEL_COUNT - 1, coupling.range[0] + 1, coupling.range[1]);

						coupling._reserved = 1;
					} else {
						CalculateBAI(decoder_cfg.exponent_data + (MAX_CHANNEL_COUNT - 1)*EXPONENT_BLOCK_SIZE + 256, coupling.msk.snr_offset, coupling.range[0], coupling.range[1]);
						GetMantissas(MAX_CHANNEL_COUNT - 1, coupling.range[0] + 1, coupling.range[1]);
					}
				}
				
				CopyCouplingVals(i);
			}
		}
	}
}

void Audio::AC3::GetEMantissas(unsigned int ch_idx, unsigned int i_idx, unsigned int i_end) {	
	int * mask_ptr(decoder_cfg.exponent_data + ch_idx*EXPONENT_BLOCK_SIZE + 256), endbap(17);
	int * d_ptr = reinterpret_cast<int *>(decoder_cfg.spectral_data + ch_idx*AC3_CHANNEL_SIZE);

	int pre_ma[6];
	float fre_ma[6];

	int x_val(0), denominator(0);
	unsigned int gain_val(0);


	unsigned int aq_mode = decoder_cfg.GetNumber(2);
	if (aq_mode < 2) endbap = 12;
	

	i_end += i_idx;

	d_ptr += ((i_idx & 0xFFFFFFFC) << 1) + (i_idx & 3);


	for (unsigned int i(i_idx);i<=i_end;i++) {
		if (mask_ptr[i*2] > 7) {
			if (mask_ptr[i*2] < endbap) aq_gain[i] = 0x0000C000;
			else aq_gain[i] = 0x00008000;
			
		} else {
			aq_gain[i] = 0;

		}

	}

	switch (aq_mode) {
		case 1: case 2:
			for (unsigned int i(i_idx);i<=i_end;i++) {
				if (aq_gain[i] & 0x00004000) {
					if (decoder_cfg.NextBit()) aq_gain[i] |= aq_mode;
				}
			}
		break;
		case 3:
			x_val = 3;
			for (unsigned int i(i_idx);i<=i_end;i++) {
				if (aq_gain[i] & 0x00004000) {
					if (x_val > 2) {
						x_val = 0;
						gain_val = decoder_cfg.GetNumber(5);
					}

					aq_gain[i] |= ah_gain_map5[gain_val][x_val++];
				}
			}
		break;

	}


	for (;i_idx <= i_end;d_ptr += data_increment[++i_idx & 3]) {
		ma_vals[0] = (mask_ptr[i_idx*2]<<1);

		denominator = mask_ptr[i_idx*2 + 1] + 15;

		if (ch_idx != (MAX_CHANNEL_COUNT - 1)) {
			denominator = scale_tab[denominator];
		}

		if (ebit_map[ma_vals[0]]) {			

			if (aq_gain[i_idx]) {
				x_val = mask_ptr[i_idx*2] - 8;			
				gain_val = aq_gain[i_idx] & 3;

				for (unsigned int j(0);j<6;j++) {
					if (ebit_map[ma_vals[0]] > gain_val) {
						ma_vals[1] = (decoder_cfg.GetNumber(ebit_map[ma_vals[0]] - gain_val) << (ebit_map[ma_vals[0] + 1] + gain_val));
						endbap = 1;

						if (ma_vals[1] == 0x80000000) {
							if (gain_val) { // large one
					
								ma_vals[1] = (decoder_cfg.GetNumber(ebit_map[ma_vals[0]] - 2 + gain_val) << (ebit_map[ma_vals[0] + 1] + 2 - gain_val));
								ma_vals[1] >>= (ebit_map[ma_vals[0] + 1] + 2 - gain_val);
					
								pre_ma[j] = (he_remap_a[x_val][gain_val]*ma_vals[1]) + (ma_vals[1] << 15);

								if (ma_vals[1] >= 0) {
									pre_ma[j] += (32768 >> gain_val);
								} else {
									pre_ma[j] += he_remap_b[x_val][gain_val-1];
								}								

								endbap = 0;
							}
						}

						if (endbap) {
							ma_vals[1] >>= (ebit_map[ma_vals[0] + 1] + gain_val);

							pre_ma[j] = (ma_vals[1] << 15);
							
							if (gain_val == 0) {
								pre_ma[j] += (he_remap_a[x_val][0]*ma_vals[1]);
							}
						}

						pre_ma[j] >>= (ebit_map[ma_vals[0]] - 1);

					} else {
						pre_ma[j] = 0;
					}					
				}


				fre_ma[0] = 0.3660254037f*(pre_ma[1] + pre_ma[5]); // SQRT2*cos(5pi/12)
				fre_ma[1] = fre_ma[0];
				fre_ma[0] += (pre_ma[1] + pre_ma[3]); // c0
				fre_ma[1] += (pre_ma[5] - pre_ma[3]); // c2

				fre_ma[5] = 1.4142135623f*pre_ma[4]; // SQRT2

				fre_ma[2] = pre_ma[0] + (0.5f*fre_ma[5]);
				fre_ma[3] = fre_ma[2];
				fre_ma[4] = 1.2247448713f*pre_ma[2]; // SQRT2*cos(pi/6)		

				fre_ma[2] += fre_ma[4]; // d0
				fre_ma[3] -= fre_ma[4]; // d2
							
				fre_ma[4] = pre_ma[1] - pre_ma[3] - pre_ma[5]; // c1
				fre_ma[5] = pre_ma[0] - fre_ma[5]; // d1
			

				reinterpret_cast<float *>(d_ptr)[0*AUDIO_BLOCK_SIZE] = (fre_ma[2] + fre_ma[0]);
				reinterpret_cast<float *>(d_ptr)[1*AUDIO_BLOCK_SIZE] = (fre_ma[5] + fre_ma[4]);
				reinterpret_cast<float *>(d_ptr)[2*AUDIO_BLOCK_SIZE] = (fre_ma[3] + fre_ma[1]);
				reinterpret_cast<float *>(d_ptr)[3*AUDIO_BLOCK_SIZE] = (fre_ma[3] - fre_ma[1]);
				reinterpret_cast<float *>(d_ptr)[4*AUDIO_BLOCK_SIZE] = (fre_ma[5] - fre_ma[4]);
				reinterpret_cast<float *>(d_ptr)[5*AUDIO_BLOCK_SIZE] = (fre_ma[2] - fre_ma[0]);

			} else { // VQ vals
			
				ma_vals[1] = decoder_cfg.GetNumber(ebit_map[ma_vals[0]]); // index into VQ
				ma_vals[1] += (1 << ebit_map[ma_vals[0]]) - 4;

				d_ptr[0*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][0];
				d_ptr[1*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][1];
				d_ptr[2*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][2];
				d_ptr[3*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][3];
				d_ptr[4*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][4];
				d_ptr[5*AUDIO_BLOCK_SIZE] = VQ_table[ma_vals[1]][5];

			}

		}

		d_ptr[0*AUDIO_BLOCK_SIZE + 4] = denominator;
		d_ptr[1*AUDIO_BLOCK_SIZE + 4] = denominator;
		d_ptr[2*AUDIO_BLOCK_SIZE + 4] = denominator;
		d_ptr[3*AUDIO_BLOCK_SIZE + 4] = denominator;
		d_ptr[4*AUDIO_BLOCK_SIZE + 4] = denominator;
		d_ptr[5*AUDIO_BLOCK_SIZE + 4] = denominator;

	}
}

void Audio::AC3::CalculateEBAI(int * msk_ptr, short snr_offset, unsigned int i_idx, unsigned int i_end) {
	int * bnd_psd(msk_ptr - 128);
	unsigned int band_idx(-1);

	if (i_idx) i_end += i_idx++;

	msk_ptr += i_idx*2;

	for (;i_idx <= i_end;i_idx++, msk_ptr += 2) {
		if (band_idx != mask_tab[i_idx]) {
			band_idx = mask_tab[i_idx];
			
			bnd_psd[band_idx] -= (snr_offset + bai.masking_floor);
			bnd_psd[band_idx] &= ~(bnd_psd[band_idx] >> 31);
			bnd_psd[band_idx] &= 0x1FE0;
			bnd_psd[band_idx] += bai.masking_floor;

			msk_ptr[0] = ((3072 - (msk_ptr[1]<<7) - bnd_psd[band_idx]) >> 5);
			msk_ptr[0] &= ~(msk_ptr[0] >> 31);
			msk_ptr[0] = ((msk_ptr[0] | ((63 - msk_ptr[0]) >> 31)) & 63);			
			msk_ptr[0] = hebap_tab[msk_ptr[0]];

		} else {
			msk_ptr[0] = ((3072 - (msk_ptr[1]<<7) - bnd_psd[band_idx]) >> 5);
			msk_ptr[0] &= ~(msk_ptr[0] >> 31);
			msk_ptr[0] = ((msk_ptr[0] | ((63 - msk_ptr[0]) >> 31)) & 63);			
			msk_ptr[0] = hebap_tab[msk_ptr[0]];
		}		
	}

}


void Audio::AC3::GetECoupling(unsigned int blk_idx) {
	unsigned int x_val(0), x_val1(0);
	int x_val2(0);

	if (e_frame.blocks[blk_idx].cpl_e_flag) {
		if (e_frame.blocks[blk_idx].coupling_in_use) {
			e_frame.x_coupling_flag = decoder_cfg.NextBit();

			if (bsi.coding_mode == 2) {
				channels[0].cpl.in_use = 1;
				channels[1].cpl.in_use = 1;

			} else {
				for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].cpl.in_use = decoder_cfg.NextBit();

			}


			if (e_frame.x_coupling_flag == 0) {
				if (bsi.coding_mode == 2) {
					coupling.phase_flags_in_use = decoder_cfg.NextBit();
				}

				coupling.range[0] = decoder_cfg.GetNumber(4);

				if (e_frame.se_in_use) {
					coupling.range[1] = e_frame.se_range[0]/12 - 3;					
				} else {
					coupling.range[1] = decoder_cfg.GetNumber(4) + 3;
				}
								
				GetBS(coupling.range, coupling.band_size, coupling.band_counter, (blk_idx)?0:default_cpl_bs);
				
				coupling.range[0] *= 12;
				coupling.range[0] += 36;
				
				coupling.range[1] = 0;
				for (unsigned short i(0);i<coupling.band_counter;i++) coupling.range[1] += coupling.band_size[i];

				
			} else { // enhanced coupling in use, may I will into it one more time
				coupling.range[0] = decoder_cfg.GetNumber(4);
				if (coupling.range[0] < 3) coupling.range[0] <<= 1;
				else {
					if (coupling.range[0] < 13) {
						coupling.range[0] += 2;
					} else {
						coupling.range[0] <<= 1;
						coupling.range[0] -= 10;
					}
				}

				if (e_frame.se_in_use) {
					coupling.range[1] = (e_frame.se_range[0] - 24)/12;
					if (coupling.range[1] < 6) {
						coupling.range[1] += 5;
					} else {
						coupling.range[1] <<= 1;
					}

				} else {
					coupling.range[1] = decoder_cfg.GetNumber(4) + 7;
				}

				

				if (x_val = decoder_cfg.NextBit()) {
					coupling.band_counter = 0;

					if (coupling.range[0] < 8) {
						if (coupling.range[1] > 8) x_val2 = 8;
						else x_val2 = coupling.range[1];

						for (short i(coupling.range[0]+1);i<x_val2;i++) {
							if (i < 4) {
								coupling.band_size[coupling.band_counter++] = 6;
							} else {
								coupling.band_size[coupling.band_counter++] = 12;
							}
						}
												
					} else {
						x_val2 = coupling.range[0] + 1;
						coupling.band_size[coupling.band_counter] = 12;
					}
					

					for (int i(x_val2);i<coupling.range[1];i++) {
						if (decoder_cfg.NextBit()) {
							coupling.band_size[coupling.band_counter] += 12;
						} else {					
							coupling.band_size[++coupling.band_counter] = 12;
						}
					}

					coupling.band_counter++;
				} else {
					if (blk_idx == 0) {
						x_val = 1;
						coupling.band_counter = 0;					

						if (coupling.range[0] < 8) {
							if (coupling.range[1] > 8) x_val2 = 8;
							else x_val2 = coupling.range[1];

							for (short i(coupling.range[0]);i<x_val2;i++) {
								if (i < 4) {
									coupling.band_size[coupling.band_counter++] = 6;
								} else {
									coupling.band_size[coupling.band_counter++] = 12;
								}
							}
												
						} else {
							x_val2 = coupling.range[0] + 1;
							coupling.band_size[coupling.band_counter] = 12;
						}
					

						for (int i(x_val2);i<coupling.range[1];i++) {
							if (default_ecpl_bs[i]) {
								coupling.band_size[coupling.band_counter] += 12;
							} else {					
								coupling.band_size[++coupling.band_counter] = 12;
							}
						}

						coupling.band_counter++;
					}
				}

				if (x_val) {
					if (coupling.range[0] < 4) {
						coupling.range[0] *= 6;
						coupling.range[0] += 12;
					} else {
						coupling.range[0] -= 4;
						coupling.range[0] *= 12;
						coupling.range[0] += 36;

					}

					coupling.range[1] = 0;
					for (unsigned short i(0);i<coupling.band_counter;i++) coupling.range[1] += coupling.band_size[i];
				}

			}

		} else {
			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				channels[i].cpl.in_use = 0;
				channels[i].cpl.first_coords = 1;
			}

			e_frame.first_cpl_leak = 1;
			coupling.phase_flags_in_use = 0;
			e_frame.x_coupling_flag = 0;
		}
	}

	if (e_frame.blocks[blk_idx].coupling_in_use) {
		if (e_frame.x_coupling_flag == 0) {
			x_val1 = 0;

			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				if (channels[i].cpl.in_use) {
					if (channels[i].cpl.first_coords) {
						channels[i].cpl.first_coords = 0;
						x_val2 = 1;
					} else {
						x_val2 = decoder_cfg.NextBit();
					}

					if (x_val2) {						
						x_val1 = 1;

						channels[i].cpl.master_coord = 3*decoder_cfg.GetNumber(2) - 3 + 5; // decoder_cfg.GetNumber(2);

						for (unsigned int j(0);j<coupling.band_counter;j++) {
							channels[i].cpl.coords[j][0] = decoder_cfg.GetNumber(4); // exponent
							channels[i].cpl.coords[j][1] = (decoder_cfg.GetNumber(4) | (((channels[i].cpl.coords[i][0] + 1) & 16) ^ 16)) - 1; // mantissa
						
							channels[i].cpl.coords[j][0] -= (((channels[i].cpl.coords[j][0] + 1) & 16) >> 4);
						}

					}

				} else {
					channels[i].cpl.first_coords = 1;
				}

			}

			if ((bsi.coding_mode == 2) && (coupling.phase_flags_in_use) && (x_val1)) {
				for (unsigned int i(0);i<coupling.band_counter;i++) {
					coupling.pahse_flags[i] = decoder_cfg.NextBit();
				}
			}


		} else { // enhanced coupling
			coupling.first_chin = -1;

			x_val = decoder_cfg.NextBit();

			for (unsigned int i(0);i<bsi.full_chan_count;i++) {
				if (channels[i].cpl.in_use) {
					if (coupling.first_chin == (unsigned char)-1) coupling.first_chin = i;
										
					if (channels[i].cpl.first_coords) {
						channels[i].cpl.first_coords = 0;
						
						x_val1 = 1;

						if (i > coupling.first_chin) x_val2 = 1;
						else x_val2 = 0;

					} else {
						x_val1 = decoder_cfg.NextBit();
						if (i > coupling.first_chin) x_val2 = decoder_cfg.NextBit();
						else x_val2 = 0;
					}

					if (x_val1) {
						for (unsigned int j(0);j<coupling.band_counter;j++) channels[i].cpl.scaling[j] = decoder_cfg.GetNumber(5);
					}

					if (x_val2) {
						for (unsigned int j(0);j<coupling.band_counter;j++) {
							channels[i].cpl.coords[j][0] = decoder_cfg.GetNumber(6);
							channels[i].cpl.coords[j][1] = decoder_cfg.GetNumber(3);
						}

					}

					if (i > coupling.first_chin) {
						channels[i].cpl.transient = decoder_cfg.NextBit();
					}

				} else {
					channels[i].cpl.first_coords = 1;
				}
			}
		}
	}



// rematrixing
	if (bsi.coding_mode == 2) {
		if (blk_idx) x_val = decoder_cfg.NextBit();
		else x_val = 1;

		if (x_val) {
			x_val2 = 4;
			if (e_frame.blocks[blk_idx].coupling_in_use) {
				if (coupling.range[0] <= 60) {
					if (coupling.range[0] == 36) x_val2 = 2;
					else x_val2 = 3;
				}
			}

			for (unsigned int i(0);i<x_val2;i++) coupling.rematrix_flag[i] = decoder_cfg.NextBit();
		}

	}
}


void Audio::AC3::GetEFrameHdr() {
	unsigned x_val(0), x_val2(0);

	if (bsi.xi.block_count == 6) {
		e_frame.exponent_s_flag = decoder_cfg.NextBit();
		e_frame.hybrid_t_flag = decoder_cfg.NextBit();
	} else {
		e_frame.exponent_s_flag = 1;		
	}

	e_frame.offset_strategy = decoder_cfg.GetNumber(2);
	e_frame.transient_p_flag = decoder_cfg.NextBit();
	e_frame.block_switch_flag = decoder_cfg.NextBit();
	e_frame.dither_flag = decoder_cfg.NextBit();
	e_frame.ba_model_flag = decoder_cfg.NextBit();
	e_frame.fast_gain_flag = decoder_cfg.NextBit();
	e_frame.dba_flag = decoder_cfg.NextBit();
	e_frame.skip_field_flag = decoder_cfg.NextBit();
	e_frame.attenuation_flag = decoder_cfg.NextBit();

	if (bsi.coding_mode > 1) {
		e_frame.blocks[0].cpl_e_flag = 1;
		e_frame.blocks[0].coupling_in_use = decoder_cfg.NextBit();
		for (unsigned int i(1);i<bsi.xi.block_count;i++) {
			if (e_frame.blocks[i].cpl_e_flag = decoder_cfg.NextBit()) e_frame.blocks[i].coupling_in_use = decoder_cfg.NextBit();
			else e_frame.blocks[i].coupling_in_use = e_frame.blocks[i-1].coupling_in_use;
		}
	}

	if (e_frame.exponent_s_flag) {
		for (unsigned int i(0);i<bsi.xi.block_count;i++) {
			if (e_frame.blocks[i].coupling_in_use) {
				e_frame.blocks[i].coupling_strategy = decoder_cfg.GetNumber(2);			
			}
			for (unsigned int j(0);j<bsi.full_chan_count;j++) e_frame.blocks[i].exp_str[j] = decoder_cfg.GetNumber(2);
		}

	} else {
		x_val = 0;
		for (unsigned int i(0);i<bsi.xi.block_count;i++) x_val += e_frame.blocks[i].coupling_in_use;
		if ((bsi.coding_mode > 1) && (x_val)) {
			x_val = decoder_cfg.GetNumber(5);

			e_frame.blocks[0].coupling_strategy = exp_str_map[x_val][0];
			e_frame.blocks[1].coupling_strategy = exp_str_map[x_val][1];
			e_frame.blocks[2].coupling_strategy = exp_str_map[x_val][2];
			e_frame.blocks[3].coupling_strategy = exp_str_map[x_val][3];
			e_frame.blocks[4].coupling_strategy = exp_str_map[x_val][4];
			e_frame.blocks[5].coupling_strategy = exp_str_map[x_val][5];


		}
		
		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			x_val = decoder_cfg.GetNumber(5);
			
			e_frame.blocks[0].exp_str[i] = exp_str_map[x_val][0];
			e_frame.blocks[1].exp_str[i] = exp_str_map[x_val][1];
			e_frame.blocks[2].exp_str[i] = exp_str_map[x_val][2];
			e_frame.blocks[3].exp_str[i] = exp_str_map[x_val][3];
			e_frame.blocks[4].exp_str[i] = exp_str_map[x_val][4];
			e_frame.blocks[5].exp_str[i] = exp_str_map[x_val][5];
		}
	}

	if (bsi.lfe_present) {
		for (unsigned int i(0);i<bsi.xi.block_count;i++) e_frame.blocks[i].lfe_strategy = decoder_cfg.NextBit();
	}

	if (bsi.xi._type == 0) {		
		if (bsi.xi.block_count != 6) x_val = decoder_cfg.NextBit();
		else x_val = 1;

		if (x_val) {
			for (unsigned int i(0);i<bsi.full_chan_count;i++) channels[i].xc.converter_strategy = decoder_cfg.GetNumber(5);
		}

	}

	if (e_frame.hybrid_t_flag) {
		x_val = 0; x_val2 = 0;
		for (unsigned int i(0);i<bsi.xi.block_count;i++) {
			if ((e_frame.blocks[i].cpl_e_flag) || (e_frame.blocks[i].coupling_strategy)) x_val++;
			if (e_frame.blocks[i].coupling_in_use) x_val2++;
		}

		if ((x_val2 == 6) && (x_val == 1)) e_frame.cpl_hybrid_in_use = decoder_cfg.NextBit();

		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			x_val2 = 0;
			for (unsigned int j(0);j<bsi.xi.block_count;j++) if (e_frame.blocks[j].exp_str[i]) x_val2++;

			if (x_val2 == 1) channels[i].xc.hybrid_in_use = decoder_cfg.NextBit();
		}

		if (bsi.lfe_present) {
			x_val2 = 0;
			for (unsigned int i(0);i<bsi.xi.block_count;i++) if (e_frame.blocks[i].lfe_strategy) x_val2++;

			if (x_val2 == 1) channels[bsi.full_chan_count].xc.hybrid_in_use = decoder_cfg.NextBit();
		}
	}

	if (e_frame.offset_strategy == 0) {
		e_frame.snr_offset[0] = decoder_cfg.GetNumber(6);
		e_frame.snr_offset[1] = decoder_cfg.GetNumber(4);
	}

	if (e_frame.transient_p_flag) {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			if (decoder_cfg.NextBit()) {
				channels[i].xc.tp_location = decoder_cfg.GetNumber(10);
				channels[i].xc.tp_length = decoder_cfg.GetNumber(8);
			}
		}
	}

	if (e_frame.attenuation_flag) {
		for (unsigned int i(0);i<bsi.full_chan_count;i++) {
			if (channels[i].xc.border_filter = decoder_cfg.NextBit()) {
				channels[i].xc.attenuation_code = decoder_cfg.GetNumber(5);
			}
		}
	}

	x_val = 0;
	if (bsi.xi.block_count > 1) x_val = decoder_cfg.NextBit();

	if (x_val) {
		for (unsigned int i(1);i<bsi.xi.block_count;i++) {
			e_frame.blocks[i].start_info = decoder_cfg.GetNumber(bsi.xi.sinfo_bits);
		}
	}


	for (unsigned int i(0);i<bsi.full_chan_count;i++) {
		channels[i].xc.first_s_coords = 1;
		channels[i].cpl.first_coords = 1;
	}
	e_frame.first_cpl_leak = 1;

}

UI_64 Audio::AC3::GetEBSI() {
	UI_64 result(0);
	unsigned int x_val(0);

	bsi.xi._type = decoder_cfg.GetNumber(2);
	bsi.xi.sub_id = decoder_cfg.GetNumber(3);

	bsi.xi.sinfo_bits = decoder_cfg.GetNumber(11) + 1;
	result = (bsi.xi.sinfo_bits<<1);

	x_val = System::BitIndexHigh_16(bsi.xi.sinfo_bits);
	if (bsi.xi.sinfo_bits & ((1 < x_val) - 1)) x_val++;
	bsi.xi.sinfo_bits = x_val + 4;

	sf_idx = decoder_cfg.GetNumber(2);
	if (sf_idx == 3) {
		sf_idx = 4 + decoder_cfg.GetNumber(2);
		bsi.xi.block_count = 3;
	} else {
		bsi.xi.block_count = decoder_cfg.GetNumber(2);		
	}

	if (bsi.xi.block_count == 3) {
		bsi.xi.block_count = 6;
	} else {
		bsi.xi.block_count++;
	}

	sampling_frequency = sampling_frequencies[sf_idx];

	bsi.coding_mode = decoder_cfg.GetNumber(3);
	bsi.lfe_present = decoder_cfg.NextBit();

	bsi._id = decoder_cfg.GetNumber(5);
	if (bsi._id != 16) {
		x_val = 'fuck';
	}
	
	bsi.normalization[0] = decoder_cfg.GetNumber(5);

	if (decoder_cfg.NextBit()) {
		bsi.compression_gain[0] = decoder_cfg.GetNumber(8);
	}

	if (bsi.coding_mode == 0) {
		bsi.normalization[1] = decoder_cfg.GetNumber(5);

		if (decoder_cfg.NextBit()) {
			bsi.compression_gain[1] = decoder_cfg.GetNumber(8);
		}
	}

	if (bsi.xi._type == 1) {
		if (decoder_cfg.NextBit()) {
			bsi.chan_map = decoder_cfg.GetNumber(16);

		}
	}

	if (decoder_cfg.NextBit()) { // mix metadata
		if (bsi.coding_mode > 2) {
			bsi.xi.downmix_mode = decoder_cfg.GetNumber(2);
		
			if (bsi.coding_mode & 1) {
				bsi.xi.center_mix_level[0] = decoder_cfg.GetNumber(3);
				bsi.xi.surround_mix_level[0] = decoder_cfg.GetNumber(3);
			}
		
			if (bsi.coding_mode & 4) {
				bsi.xi.center_mix_level[1] = decoder_cfg.GetNumber(3);
				bsi.xi.surround_mix_level[1] = decoder_cfg.GetNumber(3);
			}
		}

		if (bsi.lfe_present) {
			if (decoder_cfg.NextBit()) {
				bsi.xi.lfe_mix_code = decoder_cfg.GetNumber(5);
			}
		}

		if (bsi.xi._type == 0) {
			if (decoder_cfg.NextBit()) {
				bsi.xi.program_sfac[0] = decoder_cfg.GetNumber(6);
			}

			if (bsi.coding_mode == 0) {
				if (decoder_cfg.NextBit()) {
					bsi.xi.program_sfac[1] = decoder_cfg.GetNumber(6);
				}
			}

			if (decoder_cfg.NextBit()) {
				bsi.xi.xprogram_sfac = decoder_cfg.GetNumber(6);
			}

			switch (decoder_cfg.GetNumber(2)) {
				case 1:
					decoder_cfg.GetNumber(5);
				break;
				case 2:
					decoder_cfg.GetNumber(12);
				break;
				case 3:
					x_val = decoder_cfg.GetNumber(5) + 2;
					decoder_cfg.Skip(x_val);
				break;
			}

			if (bsi.coding_mode < 2) {
				if (decoder_cfg.NextBit()) {
					bsi.xi.pan_info[0] = decoder_cfg.GetNumber(14);
				}

				if (bsi.coding_mode == 0) {
					if (decoder_cfg.NextBit()) {
						bsi.xi.pan_info[1] = decoder_cfg.GetNumber(14);
					}
				}

			}


			if (decoder_cfg.NextBit()) {
				if (bsi.xi.block_count == 1) {
					bsi.xi.mix_cfg[0] = decoder_cfg.GetNumber(5);
				} else {
					for (unsigned int i(0);i<bsi.xi.block_count;i++) {
						if (decoder_cfg.NextBit()) {
							bsi.xi.mix_cfg[i] = decoder_cfg.GetNumber(5);
						}
					}
				}

			}

		}
	}

	if (decoder_cfg.NextBit()) { // informational metadata
		bsi._mode = decoder_cfg.GetNumber(3);

		decoder_cfg.NextBit(); // copyright
		decoder_cfg.NextBit(); // origbs

		if (bsi.coding_mode == 2) {
			bsi.xi.surround_mode[0] = decoder_cfg.GetNumber(2);
			bsi.xi.headphone_mode = decoder_cfg.GetNumber(2);

		}
		if (bsi.coding_mode >= 6) {
			bsi.xi.surround_mode[1] = decoder_cfg.GetNumber(2);
		}

		if (decoder_cfg.NextBit()) {
			bsi.xi.mix_level[0] = decoder_cfg.GetNumber(5);
			bsi.xi.room_type[0] = decoder_cfg.GetNumber(2);
			bsi.xi.ad_converter_type[0] = decoder_cfg.NextBit();
		}

		if (bsi.coding_mode == 0) {
			if (decoder_cfg.NextBit()) {
				bsi.xi.mix_level[1] = decoder_cfg.GetNumber(5);
				bsi.xi.room_type[1] = decoder_cfg.GetNumber(2);
				bsi.xi.ad_converter_type[1] = decoder_cfg.NextBit();				
			}
		}

		if (bsi.xi.sub_id < 3) {
			if (decoder_cfg.NextBit()) {
				sampling_frequency <<= 1;

			}
		}
	}

	if ((bsi.xi._type == 0) && (bsi.xi.block_count < 6)) {
		decoder_cfg.NextBit(); // convsync
	}

	if (bsi.xi._type == 2) {
		if (bsi.xi.block_count == 6) x_val = 1;
		else x_val = decoder_cfg.NextBit();

		if (x_val) {
			result = (frame_size_table[sf_idx][decoder_cfg.GetNumber(6)]<<1);
		}
	}

	if (decoder_cfg.NextBit()) {
		x_val = decoder_cfg.GetNumber(6) + 1;
		decoder_cfg.Skip(x_val);
	}


	switch (bsi.coding_mode) {
		case 0:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			bsi.center_present = -1;
			bsi.full_chan_count = 2;			
		break;
		case 1:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_CENTER;

			bsi.center_present = 1;
			bsi.full_chan_count = 1;			
		break;
		case 2:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;

			bsi.full_chan_count = 2;
		break;
		case 3:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;

			bsi.center_present = 1;
			bsi.full_chan_count = 3;
		break;
		case 4:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_CENTER;

			bsi.full_chan_count = 3;

		break;
		case 5:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_CENTER;

			bsi.center_present = 1;
			bsi.full_chan_count = 4;

		break;
		case 6:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_BACK_LEFT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_RIGHT;

			bsi.full_chan_count = 4;

		break;
		case 7:
			decoder_cfg.frame_cfg->chan_type[0] = SPEAKER_FRONT_LEFT;
			decoder_cfg.frame_cfg->chan_type[1] = SPEAKER_FRONT_CENTER;
			decoder_cfg.frame_cfg->chan_type[2] = SPEAKER_FRONT_RIGHT;
			decoder_cfg.frame_cfg->chan_type[3] = SPEAKER_BACK_LEFT;
			decoder_cfg.frame_cfg->chan_type[4] = SPEAKER_BACK_RIGHT;

			bsi.center_present = 1;
			bsi.full_chan_count = 5;

		break;

	}

	if (bsi.lfe_present) {
		decoder_cfg.frame_cfg->chan_type[bsi.full_chan_count] = SPEAKER_LOW_FREQUENCY;
	}

	decoder_cfg.frame_cfg->source_chan_count = bsi.full_chan_count + bsi.lfe_present;
	decoder_cfg.frame_cfg->source_frequency = sampling_frequency;
	decoder_cfg.frame_cfg->window_sample_count = 256;
	decoder_cfg.frame_cfg->window_count = bsi.xi.block_count;


	return result;
}


