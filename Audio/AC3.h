/*
** safe-fail audio codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "Audio\AudioRenderer.h"



#define EXPONENT_BLOCK_SIZE		768
#define AUDIO_BLOCK_SIZE		256*2
#define	AC3_CHANNEL_SIZE		6*AUDIO_BLOCK_SIZE*2
#define AC3_FRAME_SIZE				MAX_CHANNEL_COUNT*(EXPONENT_BLOCK_SIZE + AC3_CHANNEL_SIZE)

namespace Audio {
	class AC3: public Codec {
		private:
			static __declspec(align(16)) float cos_table_0[2048];
			static __declspec(align(16)) const float win_table[512];

			static const int VQ_table[][6];

			static const unsigned int data_increment[4];
			static const unsigned int sampling_frequencies[16];
			static const unsigned int frame_size_table[4][64];

			static const unsigned int bap_tab[64];
			static const unsigned int hebap_tab[64];
			static const unsigned char mask_tab[256];
			static const char la_tab[256];

			static const int symmetric_mantab[6][16];
			

			static const int i_float_tab[];

			static const int he_remap_a[][3];
			static const int he_remap_b[][2];

			static const short hearing_threshold_tab[4][50];

			static const unsigned int bit_map[32];
			static const unsigned int ebit_map[40];

			static const unsigned char exp_str_map[32][6];

			static const unsigned char ah_gain_map5[32][4];

			static const unsigned char default_cpl_bs[];
			static const unsigned char default_xse_bs[];
			static const unsigned char default_ecpl_bs[];

			static const short floor_table[8];

			static const float border_filter_coffs[][3];

			static float large_step_size[2][16];
			static float small_step_size[3][16];
			
			static int scale_tab[128];

			static char pack_25[128][4];
			static char pack_11[128][2];
			static char pack_9[32][4];
						


			static __forceinline void LogAdd(int & a, int b) {
				a -= b;

				if (a >= 0) {
					b += a;
				} else {
					a = -a;
				}

				a >>= 1;
				a = (a | ((255 - a) >> 31)) & 255; //if (a > 255) a = 255;

				a = b + la_tab[a];
			};

			static __forceinline void LowComp(int & lc_val, const int * psd, int idx) {
				if (idx < 7) {
					if ((psd[0] + 256) == psd[1]) {
						lc_val = 384;
					} else {
						if (psd[0] > psd[1]) {
							lc_val -= 64;
							lc_val &= ~(lc_val >> 31);							
						}
					}


				} else {
					if (idx < 20) {
						if ((psd[0] + 256) == psd[1]) {
							lc_val = 320;
						} else {
							if (psd[0] > psd[1]) {
								lc_val -= 64;
								lc_val &= ~(lc_val >> 31);

							}
						}

					} else {
						lc_val -= 128;
						lc_val &= ~(lc_val >> 31);						
					}
				}
			};
						
			
			int ma_vals[16];
			unsigned int aq_gain[256];

			unsigned int enhanced_syntax; // current_frame
			unsigned int sampling_frequency, sf_idx;
						
			
			class DecoderState: public DecoderCfg {
				public:
					int * exponent_data;

					DecoderState() : exponent_data(0) {

					};

			} decoder_cfg;

			struct StreamInfo {
				unsigned short _id;
				unsigned short _mode;
				
				unsigned short full_chan_count, center_present, lfe_present;
				unsigned short coding_mode;
				unsigned short center_level, surround_level;
				unsigned short surround_mode;
				unsigned short chan_map;
				 
				unsigned short normalization[2];
				unsigned short compression_gain[2];
				unsigned short language[2];

				unsigned short mixing_level[2];
				

				struct XInfo {
					unsigned short pan_info[2];

					unsigned short sinfo_bits;

					unsigned char mix_cfg[6];
					unsigned char center_mix_level[2];
					unsigned char surround_mix_level[2];
					unsigned char program_sfac[2];
					unsigned char mix_level[2];
					unsigned char room_type[2];
					unsigned char ad_converter_type[2];
					unsigned char surround_mode[2];

					unsigned char downmix_mode;					
					unsigned char xprogram_sfac;
					unsigned char lfe_mix_code;					
					unsigned char headphone_mode;				

					unsigned char x_bsi;
					unsigned char e_info;

					unsigned char _type;
					unsigned char sub_id;
					unsigned char block_count;					

				} xi;


			} bsi;

			struct EFrame {
				short se_start_copy_freq, se_range[2], coverter_offset;
				unsigned short band_size[20], band_counter;

				struct BlockCfg {
					unsigned char exp_str[MAX_CHANNEL_COUNT];					
					unsigned int start_info;

					unsigned char coupling_strategy;
					unsigned char coupling_in_use;
					unsigned char lfe_strategy;
					unsigned char cpl_e_flag;

				} blocks[6];
								

				unsigned char snr_offset[2];
								
				unsigned char offset_strategy;
				
				unsigned char cpl_hybrid_in_use, first_cpl_leak;
				unsigned char exponent_s_flag, hybrid_t_flag, transient_p_flag, block_switch_flag, dither_flag, ba_model_flag, fast_gain_flag, dba_flag, skip_field_flag, attenuation_flag, se_band_flag, se_in_use, x_coupling_flag;

			} e_frame;

			struct MaskRecord {
				short snr_offset;
				short fast_gain;
				short fast_leak;
				short slow_leak;

				MaskRecord & operator =(const MaskRecord & ref_mr) {
					snr_offset = ref_mr.snr_offset;
					fast_gain = ref_mr.fast_gain;
					fast_leak = ref_mr.fast_leak;
					slow_leak = ref_mr.slow_leak;

					return *this;
				}
			};


			struct ChanCfg {
				unsigned char se_coords[20][2];
				short range[2];
				MaskRecord msk;

				struct CouplingInfo {
					unsigned char coords[20][2];
					unsigned char scaling[20];
					
					unsigned char in_use;
					unsigned char first_coords;
					unsigned char master_coord;
					unsigned char transient;

				} cpl;

				struct EChan {
					unsigned short tp_location;
					unsigned short tp_length;

					unsigned char converter_strategy;
					unsigned char hybrid_in_use;
					unsigned char attenuation_code;
					unsigned char spectral_extension;

					unsigned char first_s_coords;					
					unsigned char spectral_blend;
					unsigned char master_coord;
					unsigned char border_filter;

				} xc;
				
				unsigned char switch_flag[6];
				unsigned char dither_flag;

				unsigned char exponent_strategy;
				unsigned char group_count;

				unsigned char gain_range;

			} channels[MAX_CHANNEL_COUNT];


			struct ChanCfgSE {
				float blend[20][2];
				float spxco[20];
				float rms_e[20];

			} channels_se[MAX_CHANNEL_COUNT];

			struct CouplingInfo {
				unsigned short band_size[20];
				unsigned char pahse_flags[20];
					
				unsigned short mantissa_cfg[2];
				short range[2];

				MaskRecord msk;

				
				unsigned short in_use_flag, phase_flags_in_use, group_count, band_counter;
										
				unsigned short exponent_strategy;
				unsigned short absolute_exponent;

				unsigned char rematrix_flag[4];

				unsigned char first_chin;
				unsigned char _reserved;

					
			} coupling;


			struct BitAllocation {
				short slow_decay;
				short fast_decay;
				short slow_gain;
				short dB_per_bit;
				short masking_floor;

				void Clear() {
					slow_decay = 0;
					fast_decay = 0;
					slow_gain = 0;
					dB_per_bit = 0;
					masking_floor = 0;
				};

			} bai;				

			unsigned short range_gain[2];


			void GetBS(short *, unsigned short *, unsigned short &, const unsigned char *);

			UI_64 GetFrame();

			void GetEFrameHdr();
			UI_64 GetEBSI();
			UI_64 GetEFrame();

			void GetExponents(int *, unsigned int, unsigned int, unsigned int);
			void CalculateMaskingCurve(int *, MaskRecord &, int, int);
			void CalculateBAI(int *, short, unsigned int, unsigned int);		
			void GetMantissas(unsigned int, unsigned int, unsigned int);			
			void CopyCouplingVals(unsigned int);


			void GetAudioBlock();
			void TransformBlock(unsigned int);

			void CalculateEBAI(int *, short, unsigned int, unsigned int);
			void GetEMantissas(unsigned int, unsigned int, unsigned int);			
			void GetECoupling(unsigned int);
			void GetEAudioBlock(unsigned int);
			void SynthetizeSE(unsigned int);

			virtual unsigned int GetSatCount(unsigned int *);

			virtual unsigned int GetSatInfoCount() { return 3; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &);

			virtual UI_64 FilterFrame(unsigned int *);
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &);
			
		public:
			static const UI_64 frame_buf_tid = 961748947;
			static UI_64 Initialize();
			static UI_64 Finalize();

			AC3(unsigned int);
			virtual ~AC3();

			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
			
			
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
			virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);

	};

}
