/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\Capture.h"

#include <Windows.h>
#include <DShow.h>
#include "streams.h"

#include <wmcodecdsp.h>

#include "System\SysUtils.h"
#include "CoreLibrary\SFSCore.h"



struct MediaFormat {
	AM_MEDIA_TYPE * mType;	
	unsigned int width, height;
	wchar_t _string[32];
};

class FrameProcessor;

struct DShowSet {	
	IGraphBuilder * _ds_graph;
	ICaptureGraphBuilder2 * _dsg_builder;
	IPin * _o_pin;
	IBaseFilter * _capture_filter, * _capture_sink; // , * _still_sink;
	IMediaControl * _g_control;
	
	FrameProcessor * _processor;

	MediaFormat _iformats[64];
		
	unsigned int _format_runner, _current_format, _state, ui_reserved;

};

void FreeMediaType(AM_MEDIA_TYPE * mt) {
    if (mt->cbFormat != 0) {
        CoTaskMemFree((PVOID)mt->pbFormat);
        mt->cbFormat = 0;
        mt->pbFormat = 0;
    }

    if (mt->pUnk != NULL) {
        // pUnk should not be used.
        mt->pUnk->Release();
        mt->pUnk = 0;
    }
}



// null renderer ========================================================================================================


class dsNullRenderer: public CBaseVideoRenderer {
	public:
		static const GUID CLSID;

		dsNullRenderer(HRESULT * res) : CBaseVideoRenderer(CLSID, L"Null Renderer", 0, res) {};

		virtual ~dsNullRenderer() {};

		virtual HRESULT DoRenderSample(IMediaSample *) {
			return S_OK;
		};

		virtual HRESULT CheckMediaType(const CMediaType * mtype) {
			if (mtype->majortype == MEDIATYPE_Video) return S_OK;
			return VFW_E_TYPE_NOT_ACCEPTED;
		};

};

const GUID dsNullRenderer::CLSID = {0x38836841, 0x695B, 0x43C5, 0x9A, 0x27, 0xFE, 0xE5, 0x11, 0xA2, 0x57, 0xE1};

// frame processor =========================================================================================================

class FrameProcessor: public CBaseVideoRenderer {
	private:		
		unsigned int source_quality;
		unsigned int source_id;
	public:
		static const GUID CLSID;

		FrameProcessor(HRESULT * res) : CBaseVideoRenderer(CLSID, L"Frame Processor", 0, res), source_id(-1), source_quality(0) {};

		void SetSID(unsigned int iso) {
			source_id = iso;
		}

		virtual ~FrameProcessor() {};

		void Restart() {
			
		};

		void SetQuality(unsigned int qlevel) {
			switch (qlevel) {
				case 1:
					source_quality = 0x00040004;
				break;
				case 2:
					source_quality = 0x00080008;
				break;
				case 3:
					source_quality = 0x000C000C;
				break;
				case 4:
					source_quality = 0x00100010;
				break;
				case 5:
					source_quality = 0x00200020;
				break;
				default:
					source_quality = 0;
			}
		}

		unsigned int GetFrameRate() {
			return 0; // (frame_count*1000 / (GetTickCount() - start_mark));
		}

		virtual HRESULT CheckMediaType(const CMediaType * mtype) {
			if (mtype->majortype == MEDIATYPE_Video) return S_OK;
			return VFW_E_TYPE_NOT_ACCEPTED;
		};


		virtual HRESULT DoRenderSample(IMediaSample * msample) {

			HRESULT result(0);
			unsigned char * buffer(0);
			


			if (Video::CaptureSource * source_obj = dynamic_cast<Video::CaptureSource *>(Media::Source::GetSource(source_id)))
			if (source_obj->FrameBufferReady()) {

	
			//		ds_cfg = reinterpret_cast<DShowSet *>(source_cfg->_reserved);
			//		cmety = ds_cfg->_iformats[ds_cfg->_current_format].mType;

				if (unsigned int bsize = msample->GetActualDataLength()) {
					if (bsize <= source_obj->frame_buf_size)
					if (!msample->GetPointer(&buffer)) {
						InterlockedXor((long *)&source_obj->in_pin, (source_obj->out_pin ^ 3));

						if (unsigned char * fbpo = reinterpret_cast<unsigned char *>(source_obj->frame_buf[source_obj->in_pin].Acquire())) {
							System::MemoryCopy_SSE3(fbpo, buffer, bsize);
							source_obj->frame_buf[source_obj->in_pin].Release();

							InterlockedIncrement(&source_obj->_descriptor.source_frame);
						}
					}
				}							
				source_obj->_descriptor._state_flags ^= FRAME_BUFFER_UPDATE;
			}

			return 0;
		}



};

const GUID FrameProcessor::CLSID = {0xCA1C1C1F, 0x2DCF, 0x46BE, 0x80, 0xC4, 0x58, 0x13, 0x14, 0xB3, 0xB8, 0xCD};

// capture source ======================================================================================================

#define MEDIA_FORMAT_CHANGE		0x01000000



unsigned int Video::CaptureSource::source_runner = 0;

//unsigned int Video::CaptureSource::source_map[MAX_SOURCE_COUNT];

Video::CaptureSource::CaptureSource() : frame_buf_size(0), out_pin(2), in_pin(1) {
	System::MemoryFill(_reserved, 768*sizeof(UI_64), 0);
	
}

Video::CaptureSource::~CaptureSource() {
	DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(_reserved);

	for (unsigned int mi(0);mi<ds_cfg->_format_runner;mi++) FreeMediaType(ds_cfg->_iformats[mi].mType);

	ds_cfg->_format_runner = 0;

	if (ds_cfg->_g_control) {
		ds_cfg->_g_control->Stop();
		ds_cfg->_g_control->Release();
		ds_cfg->_g_control = 0;
	}

	if (ds_cfg->_o_pin) {
		ds_cfg->_o_pin->Release();
		ds_cfg->_o_pin = 0;
	}

	if (ds_cfg->_dsg_builder) {
		ds_cfg->_dsg_builder->Release();
		ds_cfg->_dsg_builder = 0;
	}

	if (ds_cfg->_ds_graph) {
		ds_cfg->_ds_graph->Release();
		ds_cfg->_ds_graph = 0;
	}

	if (ds_cfg->_capture_filter) {
		ds_cfg->_capture_filter->Release();
		ds_cfg->_capture_filter = 0;
	}

	if (ds_cfg->_capture_sink) {
		ds_cfg->_capture_sink->Release();
		ds_cfg->_capture_sink = 0;

	}
	
	frame_buf[0].Destroy();
	frame_buf[1].Destroy();
	frame_buf[2].Destroy();
}

UI_64 Video::CaptureSource::FilterFrame(unsigned int * pipo) {
	UI_64 result(1);
	GUID media_type;

	GetSourceMediaType(media_type);

	InterlockedXor((long *)&out_pin, (in_pin ^ 3));

	if (unsigned char * fpo = reinterpret_cast<unsigned char *>(frame_buf[out_pin].Acquire())) {

		if (media_type == MEDIASUBTYPE_YUY2) {
			SFSco::YUY22RGBA(pipo, fpo, &_descriptor.width);							
		} else {
			if (media_type == MEDIASUBTYPE_RGB24) {
				SFSco::BGR2RGBA(pipo, fpo, &_descriptor.width);
			} else {
				if (media_type == MEDIASUBTYPE_I420) {
					SFSco::I4202RGBA(pipo, fpo, &_descriptor.width);
				}
			}
		}

		frame_buf[out_pin].Release();
		result = 0;
	}

	return result;
}

bool Video::CaptureSource::FrameBufferReady() {
	if ((_descriptor._state_flags & FRAME_ACTION_MASK) == 0) {
		_descriptor._state_flags |= FRAME_BUFFER_UPDATE;
		return true;
	}
	return false;
}




void Video::CaptureSource::SetSourceQuality(unsigned int indi, unsigned int qlevel) {
	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(indi))) {
		reinterpret_cast<DShowSet *>(source_obj->_reserved)->_processor->SetQuality(qlevel);
	}
}

UI_64 Video::CaptureSource::Initialize() {
	UI_64 result(0);
	HRESULT hresult(0);

	VIDEOINFOHEADER * vihe(0);
	SFSco::Object tobj;
	ICreateDevEnum * pDevEnum(0);
	IEnumMoniker * pEnum(0);
	IMoniker * pMoniker(0);
	IPropertyBag * pPropBag(0);
	IEnumMediaTypes * mediT(0);
	VARIANT desc, name, path;
	DShowSet * ds_cfg(0);
	
	UI_64 blint(0);
	unsigned max_w(0), max_h(0), i_val(0);
	


	if (CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pDevEnum)) == S_OK) {
        
		if (pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnum, 0) == S_OK) {
			VariantInit(&desc); VariantInit(&name); VariantInit(&path);

			while (pEnum->Next(1, &pMoniker, 0) == S_OK) {				
//				source_obj.Blank();
				if (CaptureSource * source_obj = new CaptureSource()) {
					max_w = 0; max_h = 0;

					if (result = pMoniker->BindToStorage(0, 0, IID_PPV_ARGS(&pPropBag))) {
						pMoniker->Release();
						continue;  
					}

					if (pPropBag->Read(L"Description", &desc, 0)) desc.bstrVal = 0;
					if (pPropBag->Read(L"FriendlyName", &name, 0)) name.bstrVal = 0;
					if (pPropBag->Read(L"DevicePath", &path, 0)) path.bstrVal = 0;
								
					pPropBag->Release();
					pPropBag = 0;
										
					source_obj->_descriptor.SetStrings(name.bstrVal, path.bstrVal, desc.bstrVal);
									
					ds_cfg = reinterpret_cast<DShowSet *>(source_obj->_reserved);

					result = CoCreateInstance(CLSID_CaptureGraphBuilder2, 0, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&ds_cfg->_dsg_builder);
					if (!result) result = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&ds_cfg->_ds_graph);
					if (!result) result = ds_cfg->_dsg_builder->SetFiltergraph(ds_cfg->_ds_graph);
					if (!result) result = ds_cfg->_ds_graph->QueryInterface(IID_IMediaControl, (void **)&ds_cfg->_g_control);

					if (!result) {

						result = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&ds_cfg->_capture_filter);
						if (!result) result = ds_cfg->_ds_graph->AddFilter(ds_cfg->_capture_filter, L"Capture Filter");

						if (!result) {
							if (ds_cfg->_processor = new FrameProcessor((HRESULT *)&result)) {
								if (!result) result = ds_cfg->_processor->QueryInterface(IID_IBaseFilter, (void **)&ds_cfg->_capture_sink);
								if (!result) result = ds_cfg->_ds_graph->AddFilter(ds_cfg->_capture_sink, L"Capture Sink");
							}
								
								
							if (!result) {									
								result = ds_cfg->_dsg_builder->FindPin(ds_cfg->_capture_filter, PINDIR_OUTPUT, &PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, 0, 0, &ds_cfg->_o_pin);
								if (!result) {
									result = ds_cfg->_o_pin->EnumMediaTypes(&mediT);
						
									ds_cfg->_format_runner = 0;									
									ds_cfg->_current_format = -1;

									if (!result)
									while (!mediT->Next(1, &ds_cfg->_iformats[ds_cfg->_format_runner].mType, 0)) {
										if ((ds_cfg->_iformats[ds_cfg->_format_runner].mType->subtype == MEDIASUBTYPE_YUY2) || (ds_cfg->_iformats[ds_cfg->_format_runner].mType->subtype == MEDIASUBTYPE_RGB24) || (ds_cfg->_iformats[ds_cfg->_format_runner].mType->subtype == MEDIASUBTYPE_I420))
										if ((ds_cfg->_iformats[ds_cfg->_format_runner].mType->formattype == FORMAT_VideoInfo) && (ds_cfg->_iformats[ds_cfg->_format_runner].mType->cbFormat >= sizeof(VIDEOINFOHEADER)) && (ds_cfg->_iformats[ds_cfg->_format_runner].mType->pbFormat)) {
											if (ds_cfg->_format_runner < 63) {												
														
												vihe = reinterpret_cast<VIDEOINFOHEADER *>(ds_cfg->_iformats[ds_cfg->_format_runner].mType->pbFormat);
										
												if (((vihe->bmiHeader.biWidth & 7) == 0) && ((vihe->bmiHeader.biHeight & 7) == 0)) {
											
													ds_cfg->_iformats[ds_cfg->_format_runner].width = vihe->bmiHeader.biWidth; // .restr, 26, L"%d x %d x ", , );
													ds_cfg->_iformats[ds_cfg->_format_runner].height = vihe->bmiHeader.biHeight;

													if (vihe->bmiHeader.biWidth > max_w) max_w = vihe->bmiHeader.biWidth;
													if (vihe->bmiHeader.biHeight > max_h) max_h = vihe->bmiHeader.biHeight;

													i_val = Strings::IntToCStr(reinterpret_cast<char *>(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1), ds_cfg->_iformats[ds_cfg->_format_runner].width, 0);
													reinterpret_cast<char *>(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1)[i_val++] = ' ';
													reinterpret_cast<char *>(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1)[i_val++] = 'x';
													reinterpret_cast<char *>(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1)[i_val++] = ' ';
													i_val += Strings::IntToCStr(reinterpret_cast<char *>(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1) + i_val, ds_cfg->_iformats[ds_cfg->_format_runner].height, 0);

													Strings::A2W(ds_cfg->_iformats[ds_cfg->_format_runner]._string + 1);

													ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L' ';
													ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L':';
													ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L' ';

													if (vihe->bmiHeader.biCompression) {
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = vihe->bmiHeader.biCompression & 0xFF;
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = (vihe->bmiHeader.biCompression >> 8) & 0xFF;
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = (vihe->bmiHeader.biCompression >> 16) & 0xFF;
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = (vihe->bmiHeader.biCompression >> 24) & 0xFF;

													} else {
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L'R';
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L'G';
														ds_cfg->_iformats[ds_cfg->_format_runner]._string[i_val++ + 1] = L'B';
													}
													
													ds_cfg->_iformats[ds_cfg->_format_runner]._string[0] = i_val;

													ds_cfg->_format_runner++;

												}
											} else {
												FreeMediaType(ds_cfg->_iformats[ds_cfg->_format_runner].mType);
											}
										} else {
											FreeMediaType(ds_cfg->_iformats[ds_cfg->_format_runner].mType);
										}
											
									}

									mediT->Release();
									mediT = 0;


									if (!result) {
										IAMStreamConfig * pStreamCfg(0);
/*												
										for (unsigned int mm(0);mm<ds_cfg->_format_runner;mm++) {
											vihe = reinterpret_cast<VIDEOINFOHEADER *>(ds_cfg->_iformats[mm].mType->pbFormat);
																				
											if ((vihe->bmiHeader.biHeight == 720) && (vihe->bmiHeader.biWidth == 1280)) {
												ds_cfg->_current_format = mm;
												break;
											}

											if ((vihe->bmiHeader.biHeight == 720) && (vihe->bmiHeader.biWidth == 960)) {
												ds_cfg->_current_format = mm;
												break;
											}

										}
										if (ds_cfg->_current_format == -1) 
*/
										ds_cfg->_current_format = 0;											
																					
										if (!result) result = ds_cfg->_o_pin->QueryInterface(IID_IAMStreamConfig, (void **)&pStreamCfg);
	
					
										if (!result) result = pStreamCfg->SetFormat(ds_cfg->_iformats[ds_cfg->_current_format].mType);
							
										if (!result) {
											vihe = reinterpret_cast<VIDEOINFOHEADER *>(ds_cfg->_iformats[ds_cfg->_current_format].mType->pbFormat);

											source_obj->_descriptor.width = vihe->bmiHeader.biWidth;
											source_obj->_descriptor.height = vihe->bmiHeader.biHeight;

											source_obj->_descriptor.c_width = source_obj->_descriptor.width + 15; source_obj->_descriptor.c_width &= ~15;
											source_obj->_descriptor.c_height = source_obj->_descriptor.height + 15; source_obj->_descriptor.c_height &= ~15;

											source_obj->_descriptor.pixel_count = vihe->bmiHeader.biWidth;
											source_obj->_descriptor.pixel_count *= vihe->bmiHeader.biHeight;
											
											result = ds_cfg->_dsg_builder->RenderStream(&PIN_CATEGORY_CAPTURE, 0, ds_cfg->_capture_filter, 0, ds_cfg->_capture_sink);
										}


										if (pStreamCfg) pStreamCfg->Release();
									}
								}
							}
						}
					}

					max_w += 15; max_w &= ~15;
					max_h += 15; max_h &= ~15;
										
					if (!result) {
						source_obj->frame_buf_size = 3*(max_w*max_h);

						result = source_obj->frame_buf[0].New(0, source_obj->frame_buf_size, SFSco::xl_mem_mgr);
						result |= source_obj->frame_buf[1].New(0, source_obj->frame_buf_size, SFSco::xl_mem_mgr);
						result |= source_obj->frame_buf[2].New(0, source_obj->frame_buf_size, SFSco::xl_mem_mgr);

						result = (result == -1);
						source_obj->_descriptor._state_flags |= SOURCE_TYPE_VIDEO;
					}

					VariantClear(&desc); VariantClear(&path); VariantClear(&name);
				
					pMoniker->Release();
					pMoniker = 0;
				

					if (!result) {
						reinterpret_cast<DShowSet *>(source_obj->_reserved)->_processor->SetSID(source_obj->GetID());

						source_runner++;

					} else {
						delete source_obj;
				
					}
				}
			}
						
			pEnum->Release();
			
		} else {
			result = VFW_E_NOT_FOUND;
		}
		        
		pDevEnum->Release();
	}
		

	return result;


}

Media::Source * Video::CaptureSource::TakeAShot(unsigned int s_id, SFSco::Object & pic_buf) {
	Media::Source * result(0);
	UI_64 pic_count(0);
	
	// resize pic buf
	if (HANDLE delay_block = CreateEvent(0, 0, 0, 0)) {
		if (CaptureSource * src_ptr = reinterpret_cast<CaptureSource *>(GetSource(s_id))) {
			src_ptr->On();

			pic_count = src_ptr->_descriptor.c_width;
			pic_count *= src_ptr->_descriptor.c_height;
			if (src_ptr->_descriptor.precision != 1) pic_count <<= 1;

			if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(pic_buf.Acquire())) {
				if (pi_ptr[0] >= pic_count) pic_count = 0;
				pic_buf.Release();
			}

			if (pic_count) {
				if (pic_buf.Resize((pic_count+4)*sizeof(unsigned int)) != -1) {
					if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(pic_buf.Acquire())) {
						pi_ptr[0] = pic_count;
						pic_buf.Release();
					}

					pic_count = 0;
				}
			}

			if (pic_count == 0) {
				for (unsigned int i(0);i<100;i++) {
					if (src_ptr->FrameAvailable()) break;
					WaitForSingleObject(delay_block, 10);
				}

				src_ptr->Off();

				if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pic_buf.Acquire())) {
					src_ptr->FilterFrame(pipo + 4);

					pic_buf.Release();

					result = src_ptr;
				}
			} else {
				src_ptr->Off();
			}
		}
		CloseHandle(delay_block);
	}

	return result;
}

unsigned int Video::CaptureSource::On() {
	unsigned int result(-1);
		
	if (InterlockedIncrement(&on_count) == 1) { // System::LockedInc(
		DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(_reserved);
		Reset();
		ds_cfg->_processor->Restart();
		ds_cfg->_state = 1;
		result = ds_cfg->_g_control->Run();

		__super::On(); //_descriptor._state_flags &= (~SOURCE_NO_RENDER);
	}

	return result;
}

unsigned int Video::CaptureSource::Off() {
	unsigned int result(-1);

	if (InterlockedDecrement(&on_count) == 0) { //System::LockedDec(
		DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(_reserved);
		ds_cfg->_state = 0;
		ds_cfg->_g_control->Stop();
		result = ds_cfg->_processor->GetFrameRate();

		__super::Off(); // _descriptor._state_flags |= SOURCE_NO_RENDER;
	}

	return result;
}



unsigned int Video::CaptureSource::GetMediaFormatCount(unsigned int indi) {
	unsigned int result(-1);
	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(indi))) {
		result =  reinterpret_cast<DShowSet *>(source_obj->_reserved)->_format_runner;
	}

	return result;
}
/*
void Video::CaptureSource::SelectMediaFormat(unsigned int indi, unsigned int resi) {
	unsigned int result(-1);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(indi))) {
		DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(source_obj->_reserved);

		if (resi < ds_cfg->_format_runner) {
			ds_cfg->_selected_format = resi;

			source_obj->_descriptor._state_flags = SOURCE_SERVICE | MEDIA_FORMAT_CHANGE;
		}
	}
}
*/

unsigned int Video::CaptureSource::GetSourceCount() {
	return source_runner;
}


UI_64 Video::CaptureSource::SelectMediaFormat(unsigned int s_idx, unsigned int f_idx) {
	HRESULT result(0);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(s_idx))) {
		DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(source_obj->_reserved);

		if ((f_idx != ds_cfg->_current_format) && (f_idx < ds_cfg->_format_runner)) {

			if (ds_cfg->_g_control) {
				if (ds_cfg->_state) ds_cfg->_g_control->Stop();
				ds_cfg->_g_control->Release();
				ds_cfg->_g_control = 0;
								
				source_obj->_descriptor._state_flags |= SOURCE_NO_RENDER;
			}

			if (ds_cfg->_o_pin) {
				ds_cfg->_o_pin->Release();
				ds_cfg->_o_pin = 0;
			}

			if (ds_cfg->_dsg_builder) {
				ds_cfg->_dsg_builder->Release();
				ds_cfg->_dsg_builder = 0;
			}

			if (ds_cfg->_ds_graph) {
				ds_cfg->_ds_graph->Release();		
				ds_cfg->_ds_graph = 0;
			}

			result = CoCreateInstance(CLSID_CaptureGraphBuilder2, 0, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&ds_cfg->_dsg_builder);
			if (!result) result = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&ds_cfg->_ds_graph);

			if (!result) result = ds_cfg->_dsg_builder->SetFiltergraph(ds_cfg->_ds_graph);
			if (!result) result = ds_cfg->_ds_graph->QueryInterface(IID_IMediaControl, (void **)&ds_cfg->_g_control);

			if (!result) {
				result = ds_cfg->_ds_graph->AddFilter(ds_cfg->_capture_filter, L"Capture Filter");

				if (!result) {
					result = ds_cfg->_ds_graph->AddFilter(ds_cfg->_capture_sink, L"Capture Sink");

					if (!result) {
						result = ds_cfg->_ds_graph->AddFilter(ds_cfg->_capture_sink, L"Frame Processor");
								
						if (!result) {
							IAMStreamConfig * pStreamCfg(0);

							result = ds_cfg->_dsg_builder->FindPin(ds_cfg->_capture_filter, PINDIR_OUTPUT, &PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, 0, 0, &ds_cfg->_o_pin);

							if (!result) result = ds_cfg->_o_pin->QueryInterface(IID_IAMStreamConfig, (void **)&pStreamCfg);

							if (!result) result = pStreamCfg->SetFormat(ds_cfg->_iformats[f_idx].mType);
							
							if (!result) {
								ds_cfg->_current_format = f_idx;

								VIDEOINFOHEADER * vihe = reinterpret_cast<VIDEOINFOHEADER *>(ds_cfg->_iformats[ds_cfg->_current_format].mType->pbFormat);

								source_obj->_descriptor.width = vihe->bmiHeader.biWidth;
								source_obj->_descriptor.height = vihe->bmiHeader.biHeight;

								source_obj->_descriptor.c_width = source_obj->_descriptor.width + 15; source_obj->_descriptor.c_width &= ~15;
								source_obj->_descriptor.c_height = source_obj->_descriptor.height + 15; source_obj->_descriptor.c_height &= ~15;

								source_obj->_descriptor.pixel_count = vihe->bmiHeader.biWidth;
								source_obj->_descriptor.pixel_count *= vihe->bmiHeader.biHeight;
				
								result = ds_cfg->_dsg_builder->RenderStream(&PIN_CATEGORY_CAPTURE, 0, ds_cfg->_capture_filter, 0, ds_cfg->_capture_sink);

							}
					
							if (pStreamCfg) pStreamCfg->Release();
							
						}
					}
				}				
			}

			if (!result) {
				source_obj->on_count--;
				source_obj->On();
			}
		} else {
			result = ds_cfg->_current_format;
		}
	}

	return result;
}




void Video::CaptureSource::GetSourceMediaType(GUID & stype) {
	DShowSet * ds_cfg = reinterpret_cast<DShowSet *>(_reserved);
	stype = ds_cfg->_iformats[ds_cfg->_current_format].mType->subtype;

}

const wchar_t * Video::CaptureSource::GetMediaFormatString(unsigned int s_idx, unsigned int m_idx) {
	const wchar_t * result(0);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(s_idx))) {
		if (m_idx < reinterpret_cast<DShowSet *>(source_obj->_reserved)->_format_runner)
			result = reinterpret_cast<DShowSet *>(source_obj->_reserved)->_iformats[m_idx]._string;

	}

	return result;

}

const wchar_t * Video::CaptureSource::GetCurrentFormatString(unsigned int s_idx) {
	const wchar_t * result(0);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(s_idx))) {
		result = reinterpret_cast<DShowSet *>(source_obj->_reserved)->_iformats[reinterpret_cast<DShowSet *>(source_obj->_reserved)->_current_format]._string;
	}

	return result;

}

unsigned int Video::CaptureSource::GetCurrentFormatID(unsigned int s_idx) {
	unsigned int result(-1);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(s_idx))) {
		result = reinterpret_cast<DShowSet *>(source_obj->_reserved)->_current_format;
	}

	return result;

}


/*
unsigned int Video::CaptureSource::FormatCurrentColorString(unsigned int s_idx, wchar_t * s_out) {
	unsigned int i_val(0);

	if (CaptureSource * source_obj = dynamic_cast<CaptureSource *>(GetSource(s_idx))) {
		i_val = wcslen(reinterpret_cast<DShowSet *>(source_obj->_reserved)->_iformats[reinterpret_cast<DShowSet *>(source_obj->_reserved)->_current_format].color);
		System::MemoryCopy(s_out, reinterpret_cast<DShowSet *>(source_obj->_reserved)->_iformats[reinterpret_cast<DShowSet *>(source_obj->_reserved)->_current_format].color, i_val*sizeof(wchar_t));
	}

	return i_val;

}
*/
