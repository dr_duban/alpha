/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once


#include "System\Memory.h"

#include "Data\Stream.h"
#include "Media\Source.h"

#include "CodedSource.h"


namespace Video {

	class RIFF: public Media::Program {
		protected:
			static DWORD __stdcall ReceiverLoop(void *);	
			
			static Memory::Allocator * riff_pool;
			
			
			UI_64 chunk_size, temp_offset, time_scale;
			unsigned int chunk_type, rili_form, parse_step, op_size, stream_runner, stream_top;
				

			struct StreamEntry {
				Media::CodedSource * _sequence;
				UI_64 time_delta, time_position;
				unsigned int id, _type;
				
			};


			SFSco::Object riff_obj, temp_buf; // , code_buf;
			HANDLE reciever_thread;
				
			Data::Stream source;

			unsigned int terminator;

			
			RIFF(unsigned int p_flag);
			virtual ~RIFF();

			virtual Media::Source * GetVSource(OpenGL::DecodeRecord &);

			UI_64 ParseBlock(const unsigned char *&, UI_64);
			UI_64 ParseChunk(const unsigned int *, UI_64);


		public:
			static const UI_64 _tid = 36559;

			static UI_64 Initialize();
			static UI_64 Finalize();

			static Program * SetSource(const Data::Stream &, unsigned int previw_flag);
			
			static bool IsAVI(Data::Stream &);
			static bool IsWAV(Data::Stream &);
			
			virtual UI_64 Pack(const void *, UI_64);


	};



}


