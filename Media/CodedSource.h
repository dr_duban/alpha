/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Media\Source.h"

#define COMPLETE_CONTAINER	0x00000001
#define COMPLETE_MKV_FUCK	0x80000000

namespace Media {

	class Codec {
		friend class CodedSource;
		protected:
			static const unsigned int min_buffer_frames = 16;
			
			unsigned int codec_id, frame_list_count;
			Source::Descriptor * _src_desc;
		
			
			unsigned int GetId() const {
				return codec_id;
			};

			Codec() : codec_id(SFSco::VideoType::Undefined), _src_desc(0), frame_list_count(0) {

			};

			virtual ~Codec() {
				
			};

			virtual void InitViewBuf() {};

			virtual void SetVolume(const unsigned int *) {};
			virtual void Flush(I_64, I_64) {};
			virtual void Reset() {};
			virtual unsigned int GetCT() { return 0; };
			virtual UI_64 SetCT(UI_64) {return -1;};

			virtual void SetRenderSkip(__int64) {};
			virtual void SetComplete(unsigned int) {};

			virtual void JumpOn() = 0;
			
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &) = 0;
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &) = 0;
			
			virtual unsigned int GetSatCount(unsigned int *) { return 0; };
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &) { return 0; };
			virtual UI_64 FilterFrame(unsigned int *) { return 0; };
			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &) {};

			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int) = 0;
			
			virtual UI_64 UnitTest(const float *) = 0;
			virtual UI_64 CfgOverride(UI_64, UI_64) = 0;

			virtual unsigned int GetSatInfoCount() { return 0; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &) {};

			virtual void AudioViewOn() {};
			virtual void AudioViewOff() {};


			
	};



	class CodedSource : public Source {			

		protected:
			Codec * _codec;

			unsigned int terminator;

			CodedSource();
						
			virtual unsigned int GetCT();
			virtual UI_64 SetCT(UI_64);

			virtual UI_64 Service();
			
			virtual unsigned int GetSatCount(unsigned int *);
			virtual const wchar_t * GetSatStr(unsigned int);

			virtual unsigned int GetSatInfoCount();
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &);

			virtual UI_64 FilterFrame(unsigned int *);
			virtual UI_64 FilterSatFrame(float *, OpenGL::DecodeRecord &);
			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &);
			
			
			virtual UI_64 Test();

			virtual void AudioViewOn();
			virtual void AudioViewOff();
						
		public:
			static CodedSource * Create(unsigned int);
			
			void SetComplete(unsigned int);
			void SetRenderSkip(__int64);

			void Flush(I_64, I_64);
			void Reset();
			void SetVolume(const float *);
			void JumpOn();
			
			UI_64 Decode(const unsigned char *, UI_64, I_64 &);
			UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);

			void SetSpecialDescriptor(UI_64);
			void InitViewBuf();

			virtual ~CodedSource();			



	};

	extern "C" UI_64 Codec_GetNALSize(const unsigned char *, UI_64);

}


