/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\Source.h"
#include "Media\Capture.h"

#include "System\SysUtils.h"

#include "Data\Files.h"

#include "Media\h222.h"
#include "Media\MKV.h"
#include "Media\MP4.h"
//#include "Media\RIFF.h"
#include "Audio\ID3.h"

#include "Media\H264.h"

SFSco::IOpenGL * Media::Program::main_ogl = 0;
HANDLE Media::Program::ini_block = 0;
unsigned int Media::Program::terminator = 1;

I_64 Media::Program::sp_frequency = 0;


Media::Program::Program(unsigned int p_flag) : start_mark(0), stop_mark(0), search_mark(0), search_mode(0), search_idx(0), sync_start(-1), current_time(0) {
		
	program_block[0] = CreateEvent(0, 0, 0, 0);
	program_block[1] = 0;


	autotrigger[0] = 0;
	if (p_flag) {
		program_block[1] = CreateEvent(0, 0, 0, 0);
		autotrigger[1] = 0x53290D01;
	} else {		
		autotrigger[1] = -1;
	}
		
	
	pts[0] = pts[1] = 0;

	audio.flags = AUDIO_SOURCE_EXCLUSIVE;
	video.flags = VIDEO_SOURCE_EXCLUSIVE;
}


Media::Program::~Program() {

	for (unsigned int i(0);i<video.count;i++) { // capture only
		if (video.src_list[i]) {
			Source::RemoveDREntry((UI_64)video.src_list[i]);
		}
	}

	for (unsigned int i(0);i<audio.count;i++) { // capture only
		if (audio.src_list[i]) {
			Source::RemoveDREntry((UI_64)audio.src_list[i]);
		}
	}


	if (program_block[0]) CloseHandle(program_block[0]);
	if (program_block[1]) CloseHandle(program_block[1]);
}

UI_64 Media::Program::Initialize() {
	wchar_t q_name[] = {19, L'v',L'i', L'd', L'e', L'o', L' ', L'p', L'r', L'e', L'v', L'i', L'e', L'w', L' ', L'q', L'u', L'e', L'u', L'e'};
	wchar_t d_name[] = {18, L'v',L'i', L'd', L'e', L'o', L' ', L'd', L'e', L'c', L'o', L'd', L'e', L' ', L'q', L'u', L'e', L'u', L'e'};

	UI_64 result = Video::H264::Initialize();
		
	result |= Video::H222::Initialize();
	result |= Video::MKV::Initialize();
	result |= Video::MP4::Initialize();
	
//	result |= Video::RIFF::Initialize();

	QueryPerformanceFrequency((LARGE_INTEGER *)&sp_frequency);
	
//	dq_runner[0] = dq_runner[1] = 0;
//	dq_top[0] = dq_top[1] = 0;


	if (!result) {
		if (ini_block = CreateEvent(0, 0, 0, 0)) {
			result = decoding_queue.Init(sizeof(OpenGL::DecodeRecord), Source::decoding_queue_capacity, d_name);
/*
			result = decoding_queue[0].New(video_de_queue_tid, Source::decoding_queue_capacity*sizeof(OpenGL::DecodeRecord));
			result |= decoding_queue[1].New(video_de_queue_tid, Source::decoding_queue_capacity*sizeof(OpenGL::DecodeRecord));
*/
			if (result != -1) {				
//				dq_top[0] = dq_top[1] = Source::decoding_queue_capacity;
				
				result = prev_queue.Init(sizeof(OpenGL::DecodeRecord), Source::decoding_queue_capacity, q_name);
				
				if (!result) {
					prev_thread = CreateThread(0, 0x20000, PreviewLoop, 0, 0, 0);
					if (!prev_thread) result = GetLastError();

					if (!result) {
						if (decoder_thread = CreateThread(0, 0, DecoderLoop, 0, 0, 0)) {
							WaitForSingleObject(ini_block, INFINITE);
						} else {
							result = GetLastError();
						}
					}
				}
			}
		} else {
			result = GetLastError();
		}
	}

	return result;
}

UI_64 Media::Program::Finalize() {
	UI_64 result(0);

	terminator = 0;

	if (prev_thread) {
		WaitForSingleObject(prev_thread, INFINITE);
		CloseHandle(prev_thread);

		prev_thread = 0;
	}


	if (decoder_thread) {
		WaitForSingleObject(decoder_thread, INFINITE);
		CloseHandle(decoder_thread);

		decoder_thread = 0;
	}



	if (ini_block) CloseHandle(ini_block);
	ini_block = 0;

//	Video::RIFF::Finalize();
	Video::MP4::Finalize();
	Video::MKV::Finalize();
	Video::H222::Finalize();

	Video::H264::Finalize();

	main_ogl = 0;

	return result;
}

Media::Program * Media::Program::FromFile(SFSco::Object & fn_obj, unsigned int preview_flag) {
	Program * result(0);

	UI_64 * offset_table(0);	
	Data::File t_stream;

	if (wchar_t * fname_ptr = reinterpret_cast<wchar_t *>(fn_obj.Acquire())) {
		Data::File::Open(t_stream, fname_ptr);
		fn_obj.Release();		
	}

	if (t_stream) {
		if (t_stream.NoError()) {
			if (Video::MP4::IsMP4(t_stream)) {
				result = Video::MP4::SetSource(t_stream, preview_flag);

			} else {
				if (Video::MKV::IsMKV(t_stream)) {
					result = Video::MKV::SetSource(t_stream, preview_flag);
				
				} else {
					if (Video::H222::IsT222(t_stream)) {
						result = Video::H222::SetSource(t_stream, preview_flag);

					} else {
						if (Audio::ID3::IsMP3(t_stream)) {
							result = Audio::ID3::SetSource(t_stream, preview_flag);

						}
					}
				}
			}
		}

		if (!result) {
			t_stream.Destroy();
		}
	}

	return result;
}

Media::Program * Media::Program::FromFile(OpenGL::Element::Header * el_hdr, OpenGL::DecodeRecord & de_rec) {
	Program * result(0);

	UI_64 * offset_table(0);	
	Data::File t_stream;

	if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Acquire())) {
		offset_table = reinterpret_cast<UI_64 *>(pl_ptr + reinterpret_cast<UI_64 *>(pl_ptr)[0]);
		if (de_rec.fname_id <= offset_table[0]) {
			reinterpret_cast<UI_64 *>(pl_ptr)[1] = de_rec.fname_id;

			if (de_rec.name_text_id != -1) dynamic_cast<OpenGL::Core *>(el_hdr->_context)->RewriteText(de_rec.name_text_id, pl_ptr + offset_table[de_rec.fname_id] + 1, pl_ptr[offset_table[de_rec.fname_id]]);
			
			System::MemoryCopy(pl_ptr + 8 + pl_ptr[8] + 1, pl_ptr + offset_table[de_rec.fname_id] + 1, pl_ptr[offset_table[de_rec.fname_id]]*sizeof(wchar_t));
			pl_ptr[8 + pl_ptr[8] + 1 + pl_ptr[offset_table[de_rec.fname_id]]] = L'\0';

			Data::File::Open(t_stream, pl_ptr + 9);
		}

		reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Release();
	}

	if (t_stream) {
		if (t_stream.NoError()) {
			if (Video::MP4::IsMP4(t_stream)) {
				result = Video::MP4::SetSource(t_stream, de_rec.flags & OpenGL::DecodeRecord::FlagPreview);

			} else {
				if (Video::MKV::IsMKV(t_stream)) {
					result = Video::MKV::SetSource(t_stream, de_rec.flags & OpenGL::DecodeRecord::FlagPreview);

				} else {
					if (Video::H222::IsT222(t_stream)) {
						result = Video::H222::SetSource(t_stream, de_rec.flags & OpenGL::DecodeRecord::FlagPreview);
					} else {
						if (Audio::ID3::IsMP3(t_stream)) {
							result = Audio::ID3::SetSource(t_stream, de_rec.flags & OpenGL::DecodeRecord::FlagPreview);

						}				
					}
				}
			}
		}

		if (!result) {
			t_stream.Destroy();
		}
	}

	return result;
}


UI_64 Media::Program::AddVSource(Source * src_ptr) { // for capture only
	if (video.count < 32) {
		video.src_list[video.count++] = src_ptr;
	}

	return 0;
}

UI_64 Media::Program::EnableSource(OpenGL::DecodeRecord & source_record) {
	UI_64 p_result(source_record.pic_id), a_id(0);
	Source * a_source(0);
	
	source_record._reserved[6] = 0;
	
	if (UI_64 result = (UI_64)reinterpret_cast<Program *>(source_record.program)->SelectVSource(source_record.pic_id)) {

		a_source = reinterpret_cast<Program *>(source_record.program)->GetASource(a_id);

		source_record.start_time = 0;
		source_record.target_time = reinterpret_cast<Program *>(source_record.program)->stop_mark - reinterpret_cast<Program *>(source_record.program)->start_mark;


		if (!ref_req.element_obj) {
			ref_req.element_obj = source_record.element_obj;

			ref_req.start_time = source_record.target_time;
			ref_req.target_time = 0;
		}

			

		source_record.program = result;

		if (source_record.flags & OpenGL::DecodeRecord::FlagCapture) {
			source_record.pic_id = p_result;
			Video::CaptureSource::SelectMediaFormat(source_record.fname_id, source_record.pic_id);
		}	

		if (source_record.program) {

			source_record.width = reinterpret_cast<Source* >(source_record.program)->_descriptor.width;
			source_record.height = reinterpret_cast<Source* >(source_record.program)->_descriptor.height;
			source_record.c_width = reinterpret_cast<Source* >(source_record.program)->_descriptor.c_width;
			source_record.c_height = reinterpret_cast<Source* >(source_record.program)->_descriptor.c_height;

			if (reinterpret_cast<Source* >(source_record.program)->_descriptor.precision > 1) source_record.precision = 0x10000000;
			else source_record.precision = 0;

			source_record._reserved[6] = reinterpret_cast<Source *>(source_record.program)->_descriptor.c_width;
			source_record._reserved[6] *= reinterpret_cast<Source *>(source_record.program)->_descriptor.c_height;
			if (reinterpret_cast<Source *>(source_record.program)->_descriptor.precision != 1) source_record._reserved[6] <<= 1;


			System::MemoryCopy(&pending_req, &source_record, sizeof(OpenGL::DecodeRecord));
			pending_req.program = 0;

			if (OpenGL::Core::SetDecodeView(source_record, Source::TestFunc)) { // change element_obj to corresponding icon element
				Source::AddDREntry(source_record);
			} else {
				Source::UpdateDREntry(source_record);				
			}

			if (a_source) {
				source_record.program = reinterpret_cast<UI_64>(a_source);
				source_record.flags = OpenGL::DecodeRecord::FlagReset;
				source_record._reserved[0] = 2;
				source_record.element_obj = pending_req.element_obj;

				Source::AddDREntry(source_record);
			}

//			if (a_source) a_source->SetSatEntries(pending_req.element_obj);
		}

	} else {
		source_record.flags &= OpenGL::DecodeRecord::FlagRefresh;
		System::MemoryCopy(&pending_req, &source_record, sizeof(OpenGL::DecodeRecord));
	}

	return source_record._reserved[6];
}

void Media::Source::ResetSatRecord(const OpenGL::Element & a_plane) {
	OpenGL::DecodeRecord so_rec;

	so_rec.program = reinterpret_cast<UI_64>(this);
	so_rec.flags = OpenGL::DecodeRecord::FlagReset;
	so_rec._reserved[0] = 2;
	so_rec.element_obj = a_plane;

	AddDREntry(so_rec);

}

void Media::Program::Start(unsigned int sval) {
	if (sync_start == -2) {		
		sync_start = -1;
	}
	
	if (autotrigger[0] = sval)
		if (program_block[0]) SetEvent(program_block[0]);
}

void Media::Program::Stop() {
	autotrigger[0] = 0;
	sync_start = -2;
	if (program_block[0]) SetEvent(program_block[0]);
}

void Media::Program::Jump(I_64 dest_mark) {
	// set some control flags
	search_mark = dest_mark;
	search_mode = 1;
	// set event as vell
	if (program_block[0]) SetEvent(program_block[0]);
}

void Media::Program::Volume(I_64 dest_mark) {
	// set some control flags
	search_mark = dest_mark;
	search_mode = 11;
	// set event as vell
//	if (program_block[0]) SetEvent(program_block[0]);
}

void Media::Program::ResetPTS() {
	pts[0] = pts[1] = 0;
}

UI_64 Media::Program::GetSpan() {
	start_mark = stop_mark = -1;

	return 1;
}

UI_64 Media::Program::PreviewShot(OpenGL::DecodeRecord & de_rec, SFSco::Object & pi_obj) {
	UI_64 result(0), color_scheme(0);
	Source * src_ptr(0);
	
	if (program_block[1]) {
		if (WaitForSingleObject(program_block[1], INFINITE) == 0) {
			src_ptr = GetVSource(result);
			if (!src_ptr) src_ptr = GetASource(result);

			if (src_ptr) {
				de_rec._reserved[6] = 0;

				src_ptr->InitDRB(de_rec);

				if (de_rec._reserved[6]) {
					result = -1;

					if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(pi_obj.Acquire())) {
						if (de_rec._reserved[6] > pi_ptr[0]) result = de_rec._reserved[6];
						else result = 0;

						pi_obj.Release();

						if (result) {
							if (pi_obj.Resize((result + 4)*sizeof(unsigned int)) != -1) {
								if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(pi_obj.Acquire())) {
									pi_ptr[0] = result;
									result = 0;
									
									pi_obj.Release();
								}
							}
						}
					}

					if (result) {
						result = 0;
					} else {
					

						if (unsigned int * pi_ptr = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
							src_ptr->FilterPreview(pi_ptr + 4, de_rec);
					
							pi_obj.Release();
						}

						result = 1;
					}
				}
			}
		}
	}

	return result;
}


I_64 Media::Program::SetPTS(I_64 p_val) {

// send a feedback

	ref_req.flags = OpenGL::DecodeRecord::FlagProgramInfo;
	ref_req.target_time = p_val - start_mark;
	Decode(ref_req);
	


	if (autotrigger[0]) {
		if (autotrigger[1] != -1) p_val = autotrigger[1];
			
		if (pts[0]) {
			p_val = System::GetUCount(pts[0], p_val - pts[1]);

			if (p_val > 100) {				
				WaitForSingleObject(program_block[0], p_val/10);														
			}
			
		} else {
			pts[0] = System::GetTime();
			pts[1] = p_val;
		}

	} else {
		p_val = 0;
		pts[0] = pts[1] = 0;

		WaitForSingleObject(program_block[0], INFINITE);

	}

	return p_val;
}

/*
I_64 Media::Program::SetPTS(I_64 p_val) {
	I_64 d_val(0);

// send a feedback

	ref_req.flags = OpenGL::DecodeRecord::FlagProgramInfo;
	ref_req.target_time = p_val - start_mark;
	Decode(ref_req);
	




	p_val -= pts[0];

	if (autotrigger[0]) {
		if (autotrigger[1] != -1) p_val = autotrigger[1];

			
		if (pts[0] != pts[1]) {
			if (p_val > 0) {
				d_val = System::GetUCount(pts[1], p_val);

				if (d_val >= 50) {
					WaitForSingleObject(program_block[0], d_val/10);
				}			
										
			}
			pts[0] -= System::GetUCount(pts[1], p_val);
		}

		pts[0] += p_val;
		pts[1] = System::GetTime();

	} else {
		p_val = 0;
		pts[0] = pts[1] = 0;

		WaitForSingleObject(program_block[0], INFINITE);

	}

	return p_val;
}


*/

void Media::Program::SetSTP() {
	I_64 d_val(0);
	if (program_block[1]) {
		if (autotrigger[1]) {
			d_val = stop_mark - start_mark;

			search_mark = (d_val*(autotrigger[1] & 0x007F))/100;
			search_mode = 1;

			autotrigger[1] >>= 8;

		} else {
			SetEvent(program_block[1]);
			WaitForSingleObject(program_block[0], INFINITE);
		}
	}
}

void Media::Program::Terminate() {
	if (program_block[1]) SetEvent(program_block[1]);
	if (program_block[0]) SetEvent(program_block[0]);
}



Media::Source * Media::Program::GetVSource(UI_64 & p_id) {
	if (p_id >= video.count) p_id = 0;
	return video.src_list[p_id];
}

Media::Source * Media::Program::GetASource(UI_64 & p_id) {
	if (p_id >= audio.count) p_id = 0;
	return audio.src_list[p_id];
}

Media::Source * Media::Program::SelectVSource(UI_64 & p_id) {
	Source * result = GetVSource(p_id);

	if ((!result) && (video.count == 0)) {
		result = GetASource(p_id);
	}

//		current_source = src_rec.pic_id;
	

	return result;
}


unsigned int Media::Program::GetCurrentCT(UI_64 s_id) {
	unsigned int result(-1);

	if (Source * src = GetVSource(s_id)) {
		result = src->GetCT();

	}

	return result;
}

UI_64 Media::Program::SetCurrentCT(UI_64 ct_val, UI_64 s_id) {
	UI_64 result(-1);
	if (Source * src = GetVSource(s_id)) {
		result = src->SetCT(ct_val);

	}

	return result;
}

void Media::Program::AudioViewOn() {
	UI_64 result(0);
	if (Source * src = GetASource(result)) {
		src->AudioViewOn();
	}

}

void Media::Program::AudioViewOff() {
	UI_64 result(0);
	if (Source * src = GetASource(result)) {
		src->AudioViewOff();
	}

}


unsigned int Media::Program::GetAudioSourceCount() {
	return audio.count;
}

unsigned int Media::Program::GetVideoSourceCount() {
	return video.count;
}

unsigned int Media::Program::EnableAudioSource(unsigned int, const OpenGL::Element &) {

	return 0;
}

unsigned int Media::Program::DisableAudioSource(unsigned int) {

	return 0;
}

unsigned int Media::Program::IsAudioActive(unsigned int) {

	return 0;
}

unsigned int Media::Program::GetAudioLNG(unsigned int) {

	return 0;
}



