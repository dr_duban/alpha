/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\Source.h"

#include "System\SysUtils.h"

//#include "Media\SFV.h"
#include "Media\h222.h"
#include "Media\H264.h"

#include "Pics\JPEG.h"

unsigned int Media::Drain::terminator = 1;
unsigned int Media::Drain::_service = 0;

HANDLE Media::Drain::encoder_thread = 0;
HANDLE Media::Drain::wait_obj = 0;

SFSco::List<Media::Drain::DrainCfg> Media::Drain::drain_list;
SFSco::List<Media::Drain::SourceCfg> Media::Drain::source_list;


UI_64 Media::Drain::Initialize() {
	UI_64 result(0);

	if (wait_obj = CreateEvent(0, 0, 0, 0)) {
		if (encoder_thread = CreateThread(0, 0, EncoderLoop, 0, 0, 0)) {


		} else {
			result = GetLastError();
		}

	} else {
		result = GetLastError();
	}

	return result;
}


UI_64 Media::Drain::Finalize() {

	terminator = 0;
	if (wait_obj) {		
		if (encoder_thread) {
			SetEvent(wait_obj);
			WaitForSingleObject(encoder_thread, INFINITE);
			CloseHandle(encoder_thread);
		}

		encoder_thread = 0;

		CloseHandle(wait_obj);
	}

	drain_list.Destroy();
	source_list.Destroy();

	return 0;
}

DWORD __stdcall Media::Drain::EncoderLoop(void *) {
	DWORD result(0);

	unsigned int si_count(0);
	SourceCfg scfg;
	DrainCfg dcfg;

	for (;terminator;) {
		WaitForSingleObject(wait_obj, 160);
/*
		if (si_count = source_list.GetCount()) {
			for (unsigned int i(0);i<si_count;i++) {
				if (scfg = source_list[i]) {

					if (Source * src_obj = Source::source_list[scfg._id ^ 0x60000000]) {
						if (src_obj->_state_flags & 2) {
							if (scfg._status == 0) {
								dcfg = drain_list[scfg.drain_id];
																	
								if (unsigned int * pipo = reinterpret_cast<unsigned int *>(src_obj->GetPicturePointer())) {
									if (unsigned short * amps = reinterpret_cast<unsigned short *>(scfg.frame_buf.Acquire())) {
										scfg._status = 1;
										source_list.Set(i, scfg);
	
										if (scfg._reserved[11] & 1) { // crop


										}

										if (scfg._reserved[11] & 2) { // resize


										}

										scfg._reserved[20] = (scfg.f_switch & scfg.s_offset); // current
										scfg._reserved[21] = ((scfg._reserved[20] ^ scfg.s_offset) << 1); // previous

										SFSco::MemoryFill_SSE(amps + scfg._reserved[20], 64*sizeof(UI_64), 0x7FFF7FFF7FFF7FFF, 0x7FFF7FFF7FFF7FFF);
										SFSco::MemoryFill_SSE(amps + scfg._reserved[20] + 256, 17*64*sizeof(UI_64), 0, 0);

										scfg._reserved[20] <<= 1;

										switch (scfg.codec_id) {
											case SFSco::VideoType::H262:
											case SFSco::VideoType::SFV:										

												SFSco::JPG_FDCT(amps+(scfg._reserved[20]>>1), pipo, scfg._reserved, 1);

											break;

											case SFSco::VideoType::H264:
											case SFSco::VideoType::H265:

											break;
										}

										src_obj->ReleasePicturePointer();
										src_obj->_state_flags ^= 2;
											

										switch (scfg._reserved[9]) { // chroma subsampling
											case 20:
												Pics::JPEG::DCT_20(amps+(scfg._reserved[20]>>1), scfg._reserved);
											break;
											case 22:
												Pics::JPEG::DCT_22(amps+(scfg._reserved[20]>>1), scfg._reserved);
											break;
											case 40:
												Pics::JPEG::DCT_40(amps+(scfg._reserved[20]>>1), scfg._reserved);
											break;
										}
	
										scfg.frame_buf.Release();
										scfg._status = 2;
										source_list.Set(i, scfg);

										SetEvent(dcfg.t_block);
									} else {
										src_obj->ReleasePicturePointer();									
									}

								}
																
							}
						}
					}
				}
			}
		}
*/
	}

	return result;
}

// ==========================================================================================================================================================================================================================

DWORD __stdcall Media::Drain::DrainLoop(void * dinp) {
	UI_64 fc_size(0);
	DWORD result(0);
	unsigned int dindex((UI_64)dinp), sid(0), fslot(0);
	

	DrainCfg dcfg;
	SourceCfg scfg;


	dcfg.terminator = 1;

	for (;dcfg.terminator;) {
		dcfg = drain_list[dindex];
		if (!dcfg) break;
		WaitForSingleObject(dcfg.t_block, 203);

/*
		if (dcfg.container == 0) {
			switch (dcfg.container_type) {
				case SFSco::ContainerType::H222_PS:
//					dcfg.container = new H222();
				break;
				case SFSco::ContainerType::H222_TS:
//					dcfg.container = new H222();
				break;
				case SFSco::ContainerType::MKV:

				break;
			}

			drain_list.Set(dindex, dcfg);
		}
*/
	
/*
		for (unsigned int i(0);i<dcfg.program_count;i++) {
			if (dcfg.program_set[i].video_source != -1) {
				if (scfg = source_list[dcfg.program_set[i].video_source]) {
					if (scfg._status == 2) {
						if (scfg.codec == 0) {
							switch (scfg.codec_id) {
								case SFSco::VideoType::H264:
						//			scfg.codec = new H264();
								break;
								case SFSco::VideoType::H265:

								break;
								case SFSco::VideoType::H262:

								break;
								case SFSco::VideoType::SFV:
									scfg.codec = new SFV();
								break;

							}
						}



						if (scfg.codec) {
							if (unsigned short * ampo = reinterpret_cast<unsigned short *>(scfg.frame_buf.Acquire())) {
								fc_size = scfg.codec->Pack(ampo, scfg._reserved, scfg.q_val);

//								dcfg.container->Pack();
								// copy and format ampo+previous to container

								scfg.frame_buf.Release();
							}

						}

						scfg._status = 0;
						scfg.f_switch = ~scfg.f_switch;
						source_list.Set(dcfg.program_set[i].video_source, scfg);
					}
				}
			}

			for (unsigned j(0);j<dcfg.program_set[i].audio_count;j++) {
				if (scfg = source_list[dcfg.program_set[i].audio_source[j]]) {
					if (scfg._status == 2) {
						if (scfg.codec == 0) {


						}














						scfg._status = 0;
						scfg.f_switch = ~scfg.f_switch;
						source_list.Set(dcfg.program_set[i].audio_source[j], scfg);
					}
				}
			}
		}

*/
	}


/*
	if (dcfg) {
		for (unsigned int i(0);i<dcfg.program_count;i++) {
			if (dcfg.program_set[i].video_source != -1) {
				if (scfg = source_list[dcfg.program_set[i].video_source]) {
					if (scfg.codec) delete scfg.codec;
					scfg.codec = 0;
					scfg._status = 0;
					scfg.f_switch = 0;
					source_list.Set(dcfg.program_set[i].video_source, scfg);
					
				}
			}

			for (unsigned j(0);j<dcfg.program_set[i].audio_count;j++) {
				if (scfg = source_list[dcfg.program_set[i].audio_source[j]]) {
					if (scfg.codec) delete scfg.codec;
					scfg.codec = 0;
					scfg._status = 0;
					scfg.f_switch = 0;
					source_list.Set(dcfg.program_set[i].audio_source[j], scfg);					
				}
			}
		}
		
		if (dcfg.container) delete dcfg.container;
		dcfg.container = 0;
		drain_list.Set(dindex, dcfg);

	}

*/

	return result;
}

unsigned int Media::Drain::Create(unsigned int cnt_type) {
	UI_64 result(-1);
	DrainCfg dcf;

	dcf.container_type = cnt_type;
	dcf.terminator = 1;

	if (dcf.t_block = CreateEvent(0, 0, 0, 0)) {
		result = drain_list.Add(dcf);

		if (dcf._thread = CreateThread(0, 0, DrainLoop, (void *)result, 0, 0)) {
			drain_list.Set(result, dcf);
		} else {
			drain_list.Remove(result);
			CloseHandle(dcf.t_block);
		}
	}
	

	return result;
}

unsigned int Media::Drain::AttachSource(unsigned int drain_id, SFSco::IOpenGL::ScreenSet & sset, unsigned int program_id) {
	UI_64 result(-1);
	UI_64 ebsize(0);

	SourceCfg scfg;
	unsigned int sid(0);

	if (source_list.FindIt(&sset.i_id, 4) != -1) return -1;
/*
	if (DrainCfg dcfg = drain_list[drain_id]) {		
		if (program_id == -1) {			
			program_id = dcfg.program_count++;
			if (dcfg.program_count > MAX_PROGRAM_COUNT) return -1;

		}

		if (program_id >= dcfg.program_count) return -1;

		switch (sset.i_id & 0xF0000000) {
			case 0x60000000: // video
				if (dcfg.program_set[program_id].video_source == -1) {
				
					if (Source * src_obj = Source::source_list[sset.i_id ^ 0x60000000]) {
						src_obj->drain_id = drain_id;
						// original frame size
						scfg._reserved[12] = src_obj->width;
						scfg._reserved[13] = src_obj->height;
						scfg._reserved[14] = src_obj->c_width;
						scfg._reserved[15] = src_obj->c_height;
													
						if (sset.screen_height || sset.screen_width) { // crop
							scfg._reserved[11] = 1;


						}

						if (sset.full_height || sset.full_width) { // resize
							scfg._reserved[11] |= 2;


						}
							
						if (scfg._reserved[11] == 0) {
							scfg._reserved[0] = src_obj->width;
							scfg._reserved[1] = src_obj->height;
						}

						scfg._reserved[2] = (scfg._reserved[0]+15) & 0xFFFFFFF0;
						scfg._reserved[3] = (scfg._reserved[1]+15) & 0xFFFFFFF0;

							
						scfg._reserved[4] = 18*64*sizeof(UI_64); // stat nums (coded bit stream offset)
						scfg._reserved[8] = scfg._reserved[2]*scfg._reserved[3]; // canvas size
						scfg._reserved[16] = scfg._reserved[5] = scfg._reserved[4] + (scfg._reserved[8]<<2) + (scfg._reserved[8] << 1); // Y offset
						scfg._reserved[17] = scfg._reserved[6] = scfg._reserved[5] + (scfg._reserved[8]<<1); // Cb offset
						scfg._reserved[18] = scfg._reserved[7] = scfg._reserved[6] + (scfg._reserved[8]<<1); // Cr offset
						scfg._reserved[10] = scfg._reserved[0]*scfg._reserved[1]; // pixel count

						scfg._reserved[9] = sset.cmp_count;

						ebsize = (scfg._reserved[8] << 1) + scfg._reserved[8] + (scfg._reserved[4]>>2); // 2 * 3 components * sizeof(short) + stats

						scfg._reserved[19] = scfg._reserved[8] = scfg._reserved[7] + (scfg._reserved[8]<<1); // end offset


						if (!scfg.frame_buf) {
							if (ebsize > 0x30000) {
								result = scfg.frame_buf.New(0, (ebsize<<3), SFSco::large_mem_mgr);
							} else {
								result = scfg.frame_buf.New(0, (ebsize<<3));
							}
						} else {
							scfg.frame_buf.Resize(ebsize<<3);
						}

						if (result != -1) {
							if (void * buf_ptr = scfg.frame_buf.Acquire()) {
								MemoryFill_SSE3(buf_ptr, (ebsize<<3), 0, 0);
								scfg.frame_buf.Release();
							}
						} else {
							return -1;
						}

					} else {
						return -1;
					}
				}

			break;
			case 0x50000000:
				if (dcfg.program_set[program_id].audio_count < 31) {
					

				
				} else {
					return -1;
				}

			break;
			default:
				return -1;

		}
		

		scfg._id = sset.i_id;
		scfg.drain_id = drain_id;
		scfg.program_id = program_id;
	
		scfg.codec_id = sset.i_type;
		scfg.q_val = sset.q_factor;
		
		scfg.s_offset = (ebsize<<1);

		sid = source_list.Add(scfg);
			
		if (sid != -1) {
			switch (sset.i_id & 0xF0000000) {
				case 0x60000000:
					dcfg.program_set[program_id].video_source = sid;
				break;
				case 0x50000000:
					dcfg.program_set[program_id].audio_source[dcfg.program_set[program_id].audio_count++] = sid;
					
				break;
			}
		
			drain_list.Set(drain_id, dcfg);
		}
		
	} else {
		return -1;
	}
*/
	return program_id;
}

unsigned int Media::Drain::DetachSource(unsigned int source_id) {
	unsigned int result = source_list.FindIt(&source_id, 4);
	SourceCfg scfg;
	DrainCfg dcfg;
/*
	if (result != -1) {
		scfg = source_list.Remove(result);
		scfg.frame_buf.Destroy();

		dcfg = drain_list[scfg.drain_id];

		if (dcfg.program_set[scfg.program_id].video_source == result) {
			if (Source * src_obj = Source::source_list[source_id ^ 0x60000000]) {
				src_obj->drain_id = -1;

			}

			dcfg.program_set[scfg.program_id].video_source = -1;

		} else {
			for (unsigned int i(0);i<dcfg.program_set[scfg.program_id].audio_count;i++) {
				if (dcfg.program_set[scfg.program_id].audio_source[i] == result) {
					dcfg.program_set[scfg.program_id].audio_count--;

					for (unsigned int j(i);j<dcfg.program_set[scfg.program_id].audio_count;j++) {
						dcfg.program_set[scfg.program_id].audio_source[j] = dcfg.program_set[scfg.program_id].audio_source[j+1];
					}

					dcfg.program_set[scfg.program_id].audio_source[dcfg.program_set[scfg.program_id].audio_count] = -1;

					break;
				}
			}			
		}

		drain_list.Set(scfg.drain_id, dcfg);
	}
*/
	return result;
}


