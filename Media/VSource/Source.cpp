/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\Capture.h"

#include "System\SysUtils.h"


#define SOURCE_LIST_CAPACITY	128
#define DLNG_COUNT				9


unsigned int Media::Source::source_runner = 0;
unsigned int Media::Source::source_top = 0;
SFSco::Object Media::Source::source_list, Media::Source::picture_buffer;



UI_64 Media::Source::PicBufferSize(UI_64 c_size) {
	UI_64  result(0);

	if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(picture_buffer.Acquire())) {
		if (c_size > pi_ptr[0]) result = c_size;

		picture_buffer.Release();
	}

	if (result) {
		if (picture_buffer.Resize((result + 4)*sizeof(unsigned int)) != -1) {
			if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(picture_buffer.Acquire())) {
				pi_ptr[0] = result;
				result = 0;
				picture_buffer.Release();
			}
		}
	}

	return result;
}



Media::Source::Descriptor::Descriptor() {
	System::MemoryFill(this, sizeof(Descriptor), 0);
	lang_id = -1;
	precision = 1;
}

void Media::Source::Descriptor::SetStrings(const wchar_t * nm, const wchar_t * pt, const wchar_t * ds) { // Properties * source_cfg
	UI_64 result(0);
	
	if (nm) {
		name_length = wcslen(nm);
		if (name_length >= MAX_PATH) name_length = MAX_PATH-1;
		System::MemoryCopy(_name, nm, name_length*sizeof(wchar_t));
	}

	if (pt) {
		path_length = wcslen(pt);
		if (path_length > MAX_PATH) path_length = MAX_PATH;
		System::MemoryCopy(_devPath, pt, path_length*sizeof(wchar_t));
	}

	if (ds) {
		result = wcslen(ds);
		result = (result>=(MAX_PATH-1))?MAX_PATH:(result+1);
		System::MemoryCopy(_description, ds, result*sizeof(wchar_t));
	}

};


unsigned int Media::Source::GetName(unsigned int s_id, wchar_t * o_name) {
	unsigned int result(0);
	if (s_id < source_runner) {
		if (Source ** sl_ptr = reinterpret_cast<Source **>(source_list.Acquire())) {
			System::MemoryCopy(o_name, sl_ptr[s_id]->_descriptor._name, sl_ptr[s_id]->_descriptor.name_length*sizeof(wchar_t));
			result = sl_ptr[s_id]->_descriptor.name_length;

			source_list.Release();
		}
	}

	return result;
}

void Media::Source::InitDRB(OpenGL::DecodeRecord & d_rec) {
	if (_descriptor.path_length == -1) {
		d_rec.target_time = reinterpret_cast<UI_64 *>(_descriptor._description)[0];
		d_rec._reserved[4] = -1;
	} else {
		d_rec._reserved[4] = 0;
	}

	d_rec.width = _descriptor.width;
	d_rec.height = _descriptor.height;
	d_rec.c_width = _descriptor.c_width;
	d_rec.c_height = _descriptor.c_height;
	
	d_rec.precision = _descriptor.precision;

	d_rec._reserved[6] = _descriptor.c_width*_descriptor.c_height;
	if (d_rec.precision != 1) d_rec._reserved[6] <<= 1;
}

Media::Source::Source() : _list_id(-1), render_frame(0), code_frame(0), render_id(-1), drain_id(-1), on_count(0) {
	test_vector[0] = test_vector[1] = test_vector[2] = test_vector[3] = 0;

	if (source_runner >= source_top) {
		if (source_list.Expand(SOURCE_LIST_CAPACITY*sizeof(Source *)) != -1) {
			source_top += SOURCE_LIST_CAPACITY;
		}
	}

	if (source_runner < source_top) {
		if (Source ** sl_ptr = reinterpret_cast<Source **>(source_list.Acquire())) {
			for (unsigned int i(0);i<source_runner;i++) {
				if (sl_ptr[i] == 0) {
					sl_ptr[i] = this;
					_list_id = i;
					break;
				}
			}

			if (_list_id == -1) {
				_list_id = System::LockedInc_32(&source_runner);
				sl_ptr[_list_id] = this;
			}

			source_list.Release();
		}
	}
}

Media::Source::~Source()  {

	if (_list_id < source_runner) {
		if (void ** sl_ptr = reinterpret_cast<void **>(source_list.Acquire())) {		
			sl_ptr[_list_id] = 0;

			source_list.Release();
		}
	}

//	if (_descriptor.frame_buf) _descriptor.frame_buf.Destroy();
}


unsigned int Media::Source::GetID() {
	return _list_id;
}

unsigned int Media::Source::IsVideo() {
	return _descriptor._state_flags & SOURCE_TYPE_VIDEO;
}

unsigned int Media::Source::IsAudio() {
	return _descriptor._state_flags & SOURCE_TYPE_AUDIO;
}

unsigned int Media::Source::IsSubs() {
	return _descriptor._state_flags & SOURCE_TYPE_SUBS;
}

unsigned int Media::Source::GetLNGID() {
	return _descriptor.lang_id;
}


Media::Source * Media::Source::GetSource(unsigned int sid) {
	Source * result(0);

	if (sid < source_runner) {
		if (Source ** sl_ptr = reinterpret_cast<Source **>(source_list.Acquire())) {			
			result = sl_ptr[sid];

			source_list.Release();
		}

	}

	return result;
}


UI_64 Media::Source::Initialize(SFSco::IOpenGL ** oglm) {
	UI_64 result(-1);
	SFSco::Object tempo;

	if (!oglm) return -52;
	Program::main_ogl = *oglm;


	result = picture_buffer.New(0, (1024*1024 + 4)*sizeof(unsigned int), SFSco::xl_mem_mgr);

	if (result != -1) {
		if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(picture_buffer.Acquire())) {
			pi_ptr[0] = 1048576;
			pi_ptr[1] = 0;
			picture_buffer.Release();
		}

		result = 0;
	}

	if (!result)
	if (source_list.New(0, SOURCE_LIST_CAPACITY*sizeof(Source *)) != -1) {
		source_top = SOURCE_LIST_CAPACITY;
		source_runner = 0;
		
		if (render_queue.New(render_q_tid, render_queue_size*sizeof(OpenGL::DecodeRecord)) != -1) {
			render_top = render_queue_size;
			render_runner = 0;

			result = Program::Initialize();

		}
	}

	return result;
}


UI_64 Media::Source::Finalize() {
	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].program) {
				if (de_rec[i].flags & OpenGL::DecodeRecord::FlagProgram) {
					delete reinterpret_cast<Program *>(de_rec[i].program);
					de_rec[i].program = 0;
				}				
			}
		}

		render_queue.Release();

		render_runner = 0;
	}



	Program::Finalize();

	if (Source ** src_ptr = reinterpret_cast<Source **>(source_list.Acquire())) {
		for (unsigned int i(0);i<source_runner;i++) {
			if (src_ptr[i]) delete src_ptr[i];
			src_ptr[i] = 0;
		}
		source_list.Release();
		source_runner = 0;
	}
	
	source_list.Destroy();
	source_top = 0;

/*
	Drain::Finalize();

	control
	
*/
	


	return 0;
}



UI_64 Media::Source::Service() {
	return 0;
}

void Media::Source::Reset() {
	_descriptor.source_frame = render_frame = code_frame = 0;
}

unsigned int Media::Source::On() {
	_descriptor._state_flags &= ~SOURCE_NO_RENDER;
	return 0;
}

unsigned int Media::Source::Off() {
	_descriptor._state_flags |= SOURCE_NO_RENDER;

	return 0;
}

unsigned int Media::Source::TestFlag(unsigned int ft) {
	return ((_descriptor._state_flags & ft) ^ ft);	
}


UI_64 Media::Source::Pack(const void *, UI_64) {

	return 0;
}

UI_64 Media::Source::Test() {
	return 0;
}

UI_64 Media::Source::TestFunc(OpenGL::Element & ref_el) {
	UI_64 result(-5);
	float m_val(1.5f);

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int j(0);j<render_runner;j++) {
			if ((de_rec[j].element_obj == ref_el) && (de_rec[j].program)) {
				ref_el.GetPtrPos(reinterpret_cast<Source *>(de_rec[j].program)->test_vector);
				
				switch (de_rec[j].flags & OpenGL::DecodeRecord::SizeMask) {
					case OpenGL::DecodeRecord::SizeMinimize15:
						reinterpret_cast<Source *>(de_rec[j].program)->test_vector[0] *= m_val;
						reinterpret_cast<Source *>(de_rec[j].program)->test_vector[1] *= m_val;
					break;
					case OpenGL::DecodeRecord::SizeMinimize:
						m_val = de_rec[j].precision & 0x0FFFFFFF;
						reinterpret_cast<Source *>(de_rec[j].program)->test_vector[0] *= m_val;
						reinterpret_cast<Source *>(de_rec[j].program)->test_vector[1] *= m_val;
					break;
					
				}

				result = reinterpret_cast<Source *>(de_rec[j].program)->Test();
				break;
			}
		}

		render_queue.Release();
	}

	return result;
}

UI_64 Media::Source::Refresh(OpenGL::DecodeRecord & source_record) {
	OpenGL::Element pic_el(source_record.element_obj);

	source_record.width = _descriptor.width;
	source_record.height = _descriptor.height;
	source_record.c_width = _descriptor.c_width;
	source_record.c_height = _descriptor.c_height;

	if (_descriptor.precision > 1) source_record.precision = 0x10000000;
	else source_record.precision = 0;

	source_record._reserved[6] = _descriptor.c_width;
	source_record._reserved[6] *= _descriptor.c_height;

	if (_descriptor.precision != 1) source_record._reserved[6] <<= 1;

	source_record.flags |= OpenGL::DecodeRecord::FlagRefresh;

	source_record.element_obj.Up().Up();
	if (OpenGL::Core::SetDecodeView(source_record) == 0) { // change element_obj to corresponding icon element
		source_record.flags &= ~(OpenGL::DecodeRecord::FlagSize); // 		source_record.flags &= ~(OpenGL::DecodeRecord::FlagRefresh | OpenGL::DecodeRecord::FlagSize);

		source_record.element_obj = pic_el;
		Source::UpdateDREntry(source_record);
	}

	return source_record._reserved[6];

}

unsigned int Media::Source::GetCT() {
	return 0;
}

UI_64 Media::Source::SetCT(UI_64) {
	return -1;
}

unsigned int Media::Source::GetCTCount() {
	return 10;
}

const wchar_t * Media::Source::GetCTString(unsigned int s_idx) {
	const wchar_t * result(0);

	if (s_idx < 10) {
		result = ct_strings[s_idx];

	}

	return result;
}

unsigned int Media::Source::GetLNGIDCount() {
	return DLNG_COUNT;
}

const wchar_t * Media::Source::GetLNGIDString(unsigned int lid) {
	const wchar_t * result(0);

	if (lid < DLNG_COUNT) {
		result = lang_list[40 + lid].name;
	}


	return result;
}


const wchar_t * Media::Source::GetLNGString(unsigned int lid, unsigned int ldx) {
	const wchar_t * result(0);
	
	for (unsigned int i(0);i<49;i++) {
		if (lid == lang_list[i].id) {
			result = lang_list[i].name;
			break;
		}
	}

	if (result == 0) {
		if (ldx < 40) {
			result = lang_list[ldx].name;
		}
	}

	return result;
}

unsigned int Media::Source::TestLNG(unsigned int l_id) {
	unsigned int result(0);
	
	if (l_id == lang_list[40 + SFSco::program_cfg.media_program_language].id) {
		result = SFSco::program_cfg.media_program_language + 1;	
	}	

	return result;
}

const Media::Source::LLEntry Media::Source::lang_list[] = {
	{0, {4, L'A', L' ', L'0', L'0'}},
	{1, {4, L'A', L' ', L'0', L'1'}},
	{2, {4, L'A', L' ', L'0', L'2'}},
	{3, {4, L'A', L' ', L'0', L'3'}},
	{4, {4, L'A', L' ', L'0', L'4'}},
	{5, {4, L'A', L' ', L'0', L'5'}},
	{6, {4, L'A', L' ', L'0', L'6'}},
	{7, {4, L'A', L' ', L'0', L'7'}},
	{8, {4, L'A', L' ', L'0', L'8'}},
	{9, {4, L'A', L' ', L'0', L'9'}},

	{10, {4, L'A', L' ', L'1', L'0'}},
	{11, {4, L'A', L' ', L'1', L'1'}},
	{12, {4, L'A', L' ', L'1', L'2'}},
	{13, {4, L'A', L' ', L'1', L'3'}},
	{14, {4, L'A', L' ', L'1', L'4'}},
	{15, {4, L'A', L' ', L'1', L'5'}},
	{16, {4, L'A', L' ', L'1', L'6'}},
	{17, {4, L'A', L' ', L'1', L'7'}},
	{18, {4, L'A', L' ', L'1', L'8'}},
	{19, {4, L'A', L' ', L'1', L'9'}},

	{20, {4, L'A', L' ', L'2', L'0'}},
	{21, {4, L'A', L' ', L'2', L'1'}},
	{22, {4, L'A', L' ', L'2', L'2'}},
	{23, {4, L'A', L' ', L'2', L'3'}},
	{24, {4, L'A', L' ', L'2', L'4'}},
	{25, {4, L'A', L' ', L'2', L'5'}},
	{26, {4, L'A', L' ', L'2', L'6'}},
	{27, {4, L'A', L' ', L'2', L'7'}},
	{28, {4, L'A', L' ', L'2', L'8'}},
	{29, {4, L'A', L' ', L'2', L'9'}},

	{30, {4, L'A', L' ', L'3', L'0'}},
	{31, {4, L'A', L' ', L'3', L'1'}},
	{32, {4, L'A', L' ', L'3', L'2'}},
	{33, {4, L'A', L' ', L'3', L'3'}},
	{34, {4, L'A', L' ', L'3', L'4'}},
	{35, {4, L'A', L' ', L'3', L'5'}},
	{36, {4, L'A', L' ', L'3', L'6'}},
	{37, {4, L'A', L' ', L'3', L'7'}},
	{38, {4, L'A', L' ', L'3', L'8'}},
	{39, {4, L'A', L' ', L'3', L'9'}},



	{'deu', {3, L'D', L'E', L'U'}},
	{'eng', {3, L'E', L'N', L'G'}},
	{'fra', {3, L'F', L'R', L'A'}},
	{'ita', {3, L'I', L'T', L'A'}},

	{'jpn', {3, L'J', L'P', L'N'}},
	{'rus', {3, L'R', L'U', L'S'}},
	{'spa', {3, L'S', L'P', L'A'}},
	{'ukr', {3, L'U', L'K', L'R'}},

	{'zho', {3, L'C', L'H', L'N'}}
};


const wchar_t Media::Source::ct_strings[][8] = {
	{1, L'I', 0},
	{3, L'7', L'0', L'9', L'\0'},
	{1, L'Y', L'\0'},
	{2, L'C', L'b', L'\0'},

	{4, L'T', L' ', L'4', L'7', L'\0'},
	{3, L'6', L'0', L'1', L'\0'},
	{2, L'C', L'r', L'\0'},
	{5, L'2', L'4', L'0', L' ', L'M', L'\0'},

	{5, L'Y',L'G',L'c',L'G',L'o',L'\0'},
	{4, L'2', L'0', L'2', L'0'}


};





