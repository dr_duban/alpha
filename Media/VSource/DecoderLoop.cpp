/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\Source.h"
#include "Media\Capture.h"

#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"

#include "OpenGL\Core\OGL_WGL.h"


#include "System\SysUtils.h"



HANDLE Media::Program::decoder_thread = 0;
HANDLE Media::Program::prev_thread = 0;

// unsigned int Media::Program::current_de_queue = 0;

// unsigned int Media::Program::dq_runner[2];
// unsigned int Media::Program::dq_top[2];


// SFSco::Object Media::Program::decoding_queue[2];
Control::SimpleQueue Media::Program::decoding_queue;
Control::SimpleQueue Media::Program::prev_queue;



UI_64 Media::Program::Decode(OpenGL::DecodeRecord & id_entry) {
	UI_64 result(0);
	unsigned int d_queue(0), q_pos(0);

	if (id_entry.flags & OpenGL::DecodeRecord::FlagPreview) {
		if (id_entry.flags & 1) {
			prev_queue.Clear();
		}
		
		prev_queue.Push(&id_entry);
		

		result = 1;
	} else {
		if ((id_entry.flags & OpenGL::DecodeRecord::RecordFlags) == OpenGL::DecodeRecord::RecordFlags) {
			if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
				for (unsigned int j(0);j<Source::render_runner;j++) {
					if ((de_rec[j].element_obj == id_entry.element_obj) && (de_rec[j].program)) {
						de_rec[j].flags |= (id_entry.flags & (~OpenGL::DecodeRecord::RecordFlags));
						result = 1;
						break;
					}
				}
				Source::render_queue.Release();
			}
		} else {
			decoding_queue.Push(&id_entry);
/*
			unsigned int d_queue(current_de_queue & 1);
			unsigned int q_pos(System::LockedInc(dq_runner + d_queue));

			if (q_pos >= dq_top[d_queue]) { // resize
				if (decoding_queue[d_queue].Expand(Source::decoding_queue_capacity*sizeof(OpenGL::DecodeRecord)) != -1) {
					dq_top[d_queue] += Source::decoding_queue_capacity;

				}
			}

			if (q_pos < dq_top[d_queue])
			if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
				System::MemoryCopy_SSE3(d_record + q_pos, &id_entry, sizeof(OpenGL::DecodeRecord));

				decoding_queue[d_queue].Release();
			}

*/
			result = 1;
		}
	}

	return result;
}



DWORD __stdcall Media::Program::DecoderLoop(void * context) {
	DWORD result(0);
	wchar_t temp_str[512];

	I_64 pool_index[2*MAX_POOL_COUNT];

	UI_64 ni_available(0), list_increment(1), t_val(0), ii(0);
	unsigned int _wao(0), i_val(0), slide_show(-1);
	

	OpenGL::DecodeRecord current_record;
	OpenGL::APPlane d_plane;

	Memory::Chain::RegisterThread(pool_index);

	CoInitializeEx(0, COINIT_MULTITHREADED);

	Video::CaptureSource::Initialize();

	OpenGL::Core::GetHeaderString(18, temp_str);
	if (HWND w_handle = CreateWindowEx(0, temp_str + 1, 0, WS_POPUP, 0, 0, 16, 16, 0, 0, 0, 0)) { // L"{E8523660-E691-4C6C-85CC-9C598A07F187}"
		if (HDC w_hdc = GetDC(w_handle)) {				
			int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3, WGL_CONTEXT_MINOR_VERSION_ARB, 2, WGL_CONTEXT_PROFILE_MASK_ARB,  WGL_CONTEXT_CORE_PROFILE_BIT_ARB, WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0, 0};

			if (SetPixelFormat(w_hdc, OpenGL::Core::GetPFI(), OpenGL::Core::GetPFD())) {
				if (HGLRC gl_rc = WGL::CreateContextAttribs(w_hdc, 0, attribs)) {
					if (main_ogl->ShareRC(gl_rc) == 0) {
						if (WGL::MakeContextCurrent(w_hdc, w_hdc, gl_rc)) {
							

							OpenGL::glDisable(GL_DEPTH_TEST);
							OpenGL::glDisable(GL_CULL_FACE);
							OpenGL::glDisable(GL_STENCIL_TEST);
							OpenGL::glDisable(GL_SCISSOR_TEST);									
							OpenGL::glDisable(GL_BLEND);
																																				
							OpenGL::GenVertexArrays(1, &_wao);
							OpenGL::BindVertexArray(_wao);
									
							OpenGL::ClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
									
							OpenGL::glPixelStorei(GL_PACK_ALIGNMENT, 4);
							OpenGL::glPixelStorei(GL_PACK_ROW_LENGTH, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_ROWS, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
								
							OpenGL::ActiveTexture(GL_TEXTURE0);


							SetEvent(ini_block);

							if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {
								for (;terminator;) {
									WaitForSingleObject(ebreaker, 41); // if ((dq_runner[0] | dq_runner[1]) == 0) 
											
//									d_queue = (System::LockedInc(&current_de_queue) & 1);

									if (ii = decoding_queue.Pop(&current_record)) {
										if (SFSco::program_cfg.media_repeat) slide_show = -2;
										else if (slide_show == -2) slide_show = -1;

										for (;;ii = decoding_queue.Pop(&current_record)) {
											result = -1;

											if (current_record.element_obj)
											if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
												for (unsigned int j(0);j<Source::render_runner;j++) {
													if ((de_rec[j].element_obj == current_record.element_obj) && (de_rec[j].program)) {
														current_record.program = de_rec[j].program;

														current_record.flags |= (de_rec[j].flags & (~OpenGL::DecodeRecord::SizeMask));
														current_record.fname_id = de_rec[j].fname_id;
														current_record.name_text_id = de_rec[j].name_text_id;

														result = j;
														break;
													}
												}

												Source::render_queue.Release();
											}

											if (result != -1) {
												if (current_record.flags & OpenGL::DecodeRecord::ProgramOptions) {
													list_increment = 0;
													ni_available = 0;

													switch (current_record.flags & OpenGL::DecodeRecord::ProgramOptions) {
														case OpenGL::DecodeRecord::FlagJump:
															reinterpret_cast<Program *>(current_record.program)->Jump(current_record.target_time);

														break;
														case OpenGL::DecodeRecord::FlagMasterVolume:
															// set level for all audio
															reinterpret_cast<Program *>(current_record.program)->Volume(current_record.target_time);

														break;
														case OpenGL::DecodeRecord::FlagDestroy:
															// clean all related sceen_objs
															delete reinterpret_cast<Program *>(current_record.program);
															reinterpret_cast<OpenGL::APPlane &>(current_record.element_obj).Destroy();

															current_record.program = 0;
														break;
														case OpenGL::DecodeRecord::FlagStart:
															reinterpret_cast<Program *>(current_record.program)->Start();

														break;
														case OpenGL::DecodeRecord::FlagStop:
															reinterpret_cast<Program *>(current_record.program)->Stop();
															
														break;

														case OpenGL::DecodeRecord::FlagSourceOn:
															current_record.flags &= ~OpenGL::DecodeRecord::FlagProgram;
															reinterpret_cast<Program *>(current_record.program)->EnableSource(current_record);

														break;
														case OpenGL::DecodeRecord::FlagSourceOff:
															if ((current_record.flags & OpenGL::DecodeRecord::FlagProgram) == 0) {
																			
																Source::RemoveDREntry(current_record.program); // disable the source

																OpenGL::Core::ClearDecodeView(current_record.element_obj); // destroy

															}

														break;
														case OpenGL::DecodeRecord::FlagProgramInfo:
															// updat evideo position slider
															current_record.element_obj.SetXTra(current_record.target_time);
														break;
/*
														case OpenGL::DecodeRecord::FlagSourceSelect:
															if (current_record.pic_id != -1) {


															}
														break;												
*/																






														case OpenGL::DecodeRecord::FlagPlayAll:
															// enter slide show
															reinterpret_cast<OpenGL::APPlane &>(current_record.element_obj).RootScreen();
															
															if (slide_show != result) {
																slide_show = result;
															} else {
																slide_show = -1;
															}

														break;

														case OpenGL::DecodeRecord::FlagKill:


														case OpenGL::DecodeRecord::FlagSSNext:															
															if (current_record.pic_id == -1) {
																if (slide_show == -1) break;
															} else {
																current_record.fname_id = current_record.pic_id;
															}

															current_record.flags = OpenGL::DecodeRecord::FlagNextTrack;
															
														case OpenGL::DecodeRecord::FlagNextTrack:
															list_increment = 2;
			
														case OpenGL::DecodeRecord::FlagPrevTrack:
															current_record.fname_id += --list_increment;
														

														

															ni_available++;
															if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
																if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Acquire())) {
																	UI_64 * offset_table = reinterpret_cast<UI_64 *>(pl_ptr + reinterpret_cast<UI_64 *>(pl_ptr)[0]);
															
																	for (;(current_record.fname_id >= 1) && (current_record.fname_id <= offset_table[0]);current_record.fname_id += list_increment) {
																		if ((pl_ptr[offset_table[current_record.fname_id]])) {
																			ni_available = 5;

																			break;
																		}
																	}

																	if (ni_available == 2) {
																		for (;(current_record.fname_id >= 1) && (current_record.fname_id <= offset_table[0]);current_record.fname_id -= list_increment) {
																			if ((pl_ptr[offset_table[current_record.fname_id]])) {
																				ni_available = 5;

																				break;
																			}
																		}
																	}
																	reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Release();
																}																																

																current_record.element_obj.Release();
															}														


														// destroy all screens exept one
															if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
																if (ni_available == 5) {
																	t_val = 0;
																			
																	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(de_rec[result].element_obj.Acquire())) {
																		t_val = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_count;

																		de_rec[result].element_obj.Release();
																	}

																		
																	for (unsigned int mm(1);mm<t_val;mm++) {
																		current_record.element_obj = de_rec[result].element_obj;
																		current_record.element_obj.Down().Left().Down();
																		OpenGL::Core::ClearDecodeView(current_record.element_obj, false);
																	}


																	
																	current_record.element_obj = de_rec[result].element_obj;
																	de_rec[result].fname_id = current_record.fname_id;																
																}

																Source::render_queue.Release();
															}
														break;
														
													}


													if (current_record.flags &= (OpenGL::DecodeRecord::FlagNextTrack | OpenGL::DecodeRecord::FlagPrevTrack)) {
														current_record.flags |= OpenGL::DecodeRecord::FlagFile;
/*
														if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
															for (unsigned int ii(i+1);ii<dq_runner[d_queue];ii++) {
																if (d_record[ii].element_obj == current_record.element_obj) d_record[ii].element_obj.Blank();
															}
															decoding_queue[d_queue].Release();
														}
*/
														switch (ni_available) {
															case 5:																	
																t_val = 0;

																if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
																	if (t_val = (UI_64)FromFile(el_hdr, current_record)) {
																		reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->deco_rec.program = t_val;
																	}

																	current_record.element_obj.Release();
																}


																if (t_val) {
																	delete reinterpret_cast<Program *>(current_record.program);
																	current_record.program = t_val;
																	
																	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
																		de_rec[result].program = current_record.program;																	
																		
																		Source::render_queue.Release();
																	}

																	reinterpret_cast<Program *>(current_record.program)->Start(SFSco::program_cfg.media_start_on_load);																
			
																	current_record.pic_id = 0;
																	current_record.flags |= OpenGL::DecodeRecord::FlagRefresh;
																	reinterpret_cast<Program *>(current_record.program)->EnableSource(current_record); // placing first source on render queue must be a part of start method
																	
																} else {
																	// destroy app plane, set some error

																}
														
															break;
															case 1: // terminator
																if (slide_show != -1) {
																	// exit slide show
																	reinterpret_cast<OpenGL::APPlane &>(current_record.element_obj).RootScreen();

																	slide_show = -1;
																}

															break;
															case 2: // no more files
																
															break;
														}
														
													}




													if (current_record.program == 0) {
														if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
															de_rec[result].program = 0;

															Source::render_queue.Release();
														}
													}
												} else {
													switch (current_record.flags & OpenGL::DecodeRecord::SourceOptions) {
														case OpenGL::DecodeRecord::FlagSize:
															if (current_record.flags & OpenGL::DecodeRecord::SizeMask) { // feedback for slider
																current_record.element_obj.SetXTra(current_record._reserved[4]);
															} else {
																reinterpret_cast<Source *>(current_record.program)->Refresh(current_record);
															}
														break;
														default:
															if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
																InterlockedOr((long *)&de_rec[result].flags, (current_record.flags & OpenGL::DecodeRecord::SourceOptions));
																Source::render_queue.Release();
															}
													}
												}
											} else {

												current_record.program = 0;

												if (current_record.flags & OpenGL::DecodeRecord::FlagCapture) {
													if (current_record.program = (UI_64)new Program(0)) {
														reinterpret_cast<Program *>(current_record.program)->AddVSource(Source::GetSource(current_record.fname_id));

														if (current_record.name_text_id != -1)
														if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
															if (i_val = Source::GetName(current_record.fname_id, temp_str))
															reinterpret_cast<OpenGL::Core *>(el_hdr->_context)->RewriteText(current_record.name_text_id, temp_str, i_val);

															current_record.element_obj.Release();
														}
													}

												} else {
													if (current_record.flags & OpenGL::DecodeRecord::FlagFile) {
														if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
															current_record.program = (UI_64)FromFile(el_hdr, current_record);
															reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->deco_rec.program = current_record.program;

															current_record.element_obj.Release();
														}
													}
												}


												if (current_record.program) {
													reinterpret_cast<Program *>(current_record.program)->Start(SFSco::program_cfg.media_start_on_load);

													current_record.flags |= OpenGL::DecodeRecord::FlagProgram;
													Source::AddDREntry(current_record);
			
													current_record.flags ^= OpenGL::DecodeRecord::FlagProgram;

													reinterpret_cast<Program *>(current_record.program)->EnableSource(current_record); // placing first source on render queue must be a part of start method

													if (current_record.program == 0) {
														if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
															el_hdr->_context->Update();

															current_record.element_obj.Release();
														}
													} else {
														// destroy and error
													}
												} else {
													// destroy app plane, set some error

												}
											}

											if (ii == 2) break;
										}

						//				dq_runner[d_queue] = 0;
									}
								}

								CloseHandle(ebreaker);
							} else {
								result = GetLastError();
							}

							OpenGL::DeleteVertexArrays(1, &_wao);												
							WGL::MakeContextCurrent(w_hdc, w_hdc, 0);
								
						} else {
							result = GetLastError();
						}
					} else {
						result = GetLastError();
					}
						
					wglDeleteContext(gl_rc);
				} else {
					result = GetLastError();
				}
			}
			ReleaseDC(w_handle, w_hdc);
		} else {
			result = GetLastError();
		}
			
		DestroyWindow(w_handle);

	} else {
		result = GetLastError();
	}


	CoUninitialize();

	Memory::Chain::UnregisterThread();

	if (ini_block) SetEvent(ini_block);

	return result;
}



DWORD __stdcall Media::Program::PreviewLoop(void * context) {
	DWORD result(0);
	wchar_t temp_str[512];

	I_64 pool_index[2*MAX_POOL_COUNT];

	unsigned int _wao(0), i_val(0);
	UI_64 ii(0);
	
	SFSco::Object fname_obj, small_size, picture_buf, blank_buf;
		
	OpenGL::DecodeRecord current_record;

	Memory::Chain::RegisterThread(pool_index);

	CoInitializeEx(0, COINIT_MULTITHREADED);

	OpenGL::Core::GetHeaderString(18, temp_str);
	if (HWND w_handle = CreateWindowEx(0, temp_str + 1, 0, WS_POPUP, 0, 0, 16, 16, 0, 0, 0, 0)) { // L"{E8523660-E691-4C6C-85CC-9C598A07F187}"
		if (HDC w_hdc = GetDC(w_handle)) {				
			int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3, WGL_CONTEXT_MINOR_VERSION_ARB, 2, WGL_CONTEXT_PROFILE_MASK_ARB,  WGL_CONTEXT_CORE_PROFILE_BIT_ARB, WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0, 0};

			if (SetPixelFormat(w_hdc, OpenGL::Core::GetPFI(), OpenGL::Core::GetPFD())) {
				if (HGLRC gl_rc = WGL::CreateContextAttribs(w_hdc, 0, attribs)) {
					if (main_ogl->ShareRC(gl_rc) == 0) {
						if (WGL::MakeContextCurrent(w_hdc, w_hdc, gl_rc)) {
							

							OpenGL::glDisable(GL_DEPTH_TEST);
							OpenGL::glDisable(GL_CULL_FACE);
							OpenGL::glDisable(GL_STENCIL_TEST);
							OpenGL::glDisable(GL_SCISSOR_TEST);									
							OpenGL::glDisable(GL_BLEND);
																																				
							OpenGL::GenVertexArrays(1, &_wao);
							OpenGL::BindVertexArray(_wao);
									
							OpenGL::ClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
									
							OpenGL::glPixelStorei(GL_PACK_ALIGNMENT, 4);
							OpenGL::glPixelStorei(GL_PACK_ROW_LENGTH, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_ROWS, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
								
							OpenGL::ActiveTexture(GL_TEXTURE0);




							if (small_size.New(small_buf_tid, 1024*1024*sizeof(unsigned int), SFSco::large_mem_mgr) != -1) {
								if (picture_buf.New(pic_buf_tid, (1024*1024 + 4)*sizeof(unsigned int), SFSco::large_mem_mgr) != -1) {
									if (UI_64 * pi_ptr = reinterpret_cast<UI_64 *>(picture_buf.Acquire())) {
										pi_ptr[0] = 1048576;
										pi_ptr[1] = 0;

										picture_buf.Release();
									}									

									if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {
										for (;terminator;) {
											WaitForSingleObject(ebreaker, 61);
											
											if (ii = prev_queue.Pull(&current_record)) {
												for (;;ii = prev_queue.Pull(&current_record)) {													

													current_record.program = 0;

													if (current_record.flags & OpenGL::DecodeRecord::FlagCapture) {
														// get a shot
														if (current_record.program = (UI_64)Video::CaptureSource::TakeAShot(current_record.fname_id, picture_buf)) {
// here it gets really interesting
															current_record.width = reinterpret_cast<Source *>(current_record.program)->_descriptor.width;
															current_record.height = reinterpret_cast<Source *>(current_record.program)->_descriptor.height;
															current_record.c_width = reinterpret_cast<Source *>(current_record.program)->_descriptor.c_width;
															current_record.c_height = reinterpret_cast<Source *>(current_record.program)->_descriptor.c_height;
															
															if (reinterpret_cast<Source *>(current_record.program)->_descriptor.precision > 1) current_record.precision = 0x10000000;
															else current_record.precision = 0;
														}

													} else {
														
														if (current_record.flags & OpenGL::DecodeRecord::FlagFile) {
															
															fname_obj.Set(SFSco::mem_mgr, current_record.fname_id);

															if (current_record.program = (UI_64)FromFile(fname_obj, 1)) {
																current_record._reserved[5] = 0;

																if (reinterpret_cast<Program *>(current_record.program)->PreviewShot(current_record, picture_buf)) {
																	delete reinterpret_cast<Program *>(current_record.program);

																} else {
																	delete reinterpret_cast<Program *>(current_record.program);
																	current_record.program = 0;
																}
															}
														}
													}

												

													if (current_record.program) OpenGL::Core::SetDecodePreview(current_record, picture_buf, small_size, 16);
													else OpenGL::Core::SetDecodePreview(current_record, blank_buf, small_size, 16);
												

													if (ii == 2) break;
												}										
											}
										}

										CloseHandle(ebreaker);
									} else {
										result = GetLastError();
									}
									picture_buf.Destroy();
								} else {
									result = 22222;
								}
								small_size.Destroy();
							} else {
								result = 11111;
							}


							OpenGL::DeleteVertexArrays(1, &_wao);												
							WGL::MakeContextCurrent(w_hdc, w_hdc, 0);
								
						} else {
							result = GetLastError();
						}
					} else {
						result = GetLastError();
					}
						
					wglDeleteContext(gl_rc);
				} else {
					result = GetLastError();
				}
			}
			ReleaseDC(w_handle, w_hdc);
		} else {
			result = GetLastError();
		}
			
		DestroyWindow(w_handle);

	} else {
		result = GetLastError();
	}


	CoUninitialize();

	Memory::Chain::UnregisterThread();

	return result;
}

