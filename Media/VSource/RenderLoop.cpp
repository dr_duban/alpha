/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\Source.h"


#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"

#include "OpenGL\Core\OGL_WGL.h"


#include "System\SysUtils.h"

#include "Media\Capture.h"
#include "Pics\Image.h"

// Source renderer ===========================================================================================================================================================================================================


unsigned int Media::Source::render_runner = 0;
unsigned int Media::Source::render_top = 0;

SFSco::Object Media::Source::render_queue;


bool Media::Source::FrameAvailable() {

	if (render_frame != _descriptor.source_frame) {
		render_frame = _descriptor.source_frame;

		return true;
	}

	return false;
}

UI_64 Media::Source::AddDREntry(OpenGL::DecodeRecord & new_rec) {
	UI_64 result(-1);

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].program == 0) {
				result = i;
				break;
			}
		}

		render_queue.Release();
	}

	if (result == -1) {
		if (render_runner >= render_top) {			
			if (render_queue.Expand(render_queue_size*sizeof(OpenGL::DecodeRecord)) != -1) {
				render_top += render_queue_size;
			}
		}

		if (render_runner < render_top) result = render_runner++;
	}

	if (result != -1) {		
		if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
			System::MemoryCopy(de_rec + result, &new_rec, sizeof(OpenGL::DecodeRecord));

			render_queue.Release();
		}

		if ((new_rec.flags & OpenGL::DecodeRecord::FlagProgram) == 0) {
			reinterpret_cast<Source* >(new_rec.program)->On();
		}
	}

	return result;
}

UI_64 Media::Source::UpdateDREntry(OpenGL::DecodeRecord & new_rec) {
	UI_64 result(-1);

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].element_obj == new_rec.element_obj) {
				de_rec[i].program = new_rec.program;
			

				de_rec[i].width = new_rec.width;
				de_rec[i].height = new_rec.height;
				de_rec[i].c_width = new_rec.c_width;
				de_rec[i].c_height = new_rec.c_height;

				de_rec[i].pic_id = new_rec.pic_id;				

				de_rec[i].start_time = new_rec.start_time;
				de_rec[i].target_time = new_rec.target_time;
				de_rec[i]._reserved[0] = new_rec._reserved[0];

				de_rec[i]._reserved[4] = new_rec._reserved[4];				
				de_rec[i]._reserved[6] = new_rec._reserved[6];

				de_rec[i].flags = new_rec.flags & OpenGL::DecodeRecord::RecordFlags;

				break;
			}
		}

		render_queue.Release();
	}

	if ((new_rec.flags & (OpenGL::DecodeRecord::FlagProgram | OpenGL::DecodeRecord::FlagRefresh)) == OpenGL::DecodeRecord::FlagRefresh) {
		reinterpret_cast<Source* >(new_rec.program)->On();
	}


	return result;
}

UI_64 Media::Source::SetSatEntries(const OpenGL::Element & a_el) {
	__declspec(align(16)) OpenGL::StringsBoard::InfoLine i_line;

	unsigned int c_count(0), ic_count(0), sat_desc[8];
	OpenGL::DecodeRecord sat_de;
	OpenGL::APPlane a_plane;
	

	a_plane(a_el);
	sat_de.program = (UI_64)this;

	
	sat_de._reserved[0] = 1;
	
	
	if (c_count = GetSatCount(sat_desc)) {
		a_plane.HideCPBoards();

		ic_count = GetSatInfoCount();
		for (unsigned int i(0);i<ic_count;i++) {
			GetSatInfo(i, i_line);
			a_plane.SetICPText(i, i_line);
		}

		for (unsigned int i(0);i<c_count;i++) {
			a_plane.GetCPBoard(sat_de.element_obj, i, GetSatStr(sat_desc[i]));
			if (sat_de.element_obj) {
				sat_de.element_obj.Show();

				sat_de.pic_id = i;
			
				AddDREntry(sat_de);
			}
		}
	}

	return c_count;
}

void Media::Source::ResetSat() {
	// search render queue for this and remove those records

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(Source::render_queue.Acquire())) {
		for (unsigned int j(0);j<Source::render_runner;j++) {
			if (de_rec[j].program == (UI_64)this) {
				de_rec[j].element_obj.Hide();

				de_rec[j].program = 0;				
				
				break;
			}
		}

		Source::render_queue.Release();
	}
}



UI_64 Media::Source::RemoveDREntry(UI_64 s_id) {
	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].program == s_id) {
				if ((de_rec[i].flags & OpenGL::DecodeRecord::FlagProgram) == 0) {
					reinterpret_cast<Source *>(de_rec[i].program)->Off();
				}

				de_rec[i].program = 0;
			}
		}

		render_queue.Release();
	}


	return 0;
}



UI_64 Media::Source::TexUpdate(OpenGL::Core * core_ptr) {
	UI_64 result(0), rup(0);

	unsigned int si_count(render_runner);
	Control::Nucleus ime_buf;
	
	OpenGL::DecodeRecord current_record;
	OpenGL::AttributeArray a_arr;
	
	OpenGL::ActiveTexture(GL_TEXTURE0 + OpenGL::Core::video_unit);

	for (unsigned int si(0);si<si_count;si++) {
		current_record.program = 0;

		if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
			if (((de_rec[si].flags & OpenGL::DecodeRecord::FlagProgram) == 0) && (de_rec[si].program)) {
				System::MemoryCopy(&current_record, de_rec + si, sizeof(OpenGL::DecodeRecord));
			}
			
			render_queue.Release();
		}

		if (current_record.program)
		switch (current_record._reserved[0]) {
			case 1:
				current_record.element_obj.Down().Right();
				current_record.width = 0;

				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
					current_record.width = el_hdr->vertex_count;
					
					current_record.element_obj.Release();
				}

				if (current_record.width) {

					current_record.element_obj.InitAttributeArray(OpenGL::AttributeArray::Vertex, a_arr);
				
					if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(a_arr.Acquire())) {
						if (rup |= reinterpret_cast<Source *>(current_record.program)->FilterSatFrame(reinterpret_cast<float *>(a_hdr + 1), current_record)) {
							if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
								de_rec[si].start_time = current_record.start_time;
								de_rec[si].target_time = current_record.target_time;
			
								render_queue.Release();
							}

							if (core_ptr) core_ptr->ResetElement(current_record.element_obj);
						}



						a_arr.Release();
					}
				}
				
			break;
			case 2: // set sat
				if (reinterpret_cast<Source *>(current_record.program)->SetSatEntries(current_record.element_obj)) {
					if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
						de_rec[si].program = 0;
			
						render_queue.Release();
					}
				}

			break;
			default:
				if (current_record._reserved[6])
				if (reinterpret_cast<Source *>(current_record.program)->FrameAvailable()) {
					result = PicBufferSize(current_record._reserved[6]);

					if (!result) {					
						if (unsigned int * pi_ptr = reinterpret_cast<unsigned int *>(picture_buffer.Acquire())) {
							current_record._reserved[5] = reinterpret_cast<Source *>(current_record.program)->FilterFrame(pi_ptr+4);
						
							picture_buffer.Release();
						}
					
						if (current_record._reserved[5] <= OpenGL::Core::color_matrix_count) {
							switch (current_record.flags & OpenGL::DecodeRecord::SourceOptions) {
								case OpenGL::DecodeRecord::FlagTakeAShot:
								// copy picture but & some identities to image encoding queue
									ime_buf(picture_buffer).Clone(0, 16);							

									// color convert
									if (unsigned int * s_pi = reinterpret_cast<unsigned int *>(ime_buf.Acquire())) {
										Pics::Image_Convert2RGBA(s_pi, current_record._reserved[5]-1, current_record.c_width, current_record.c_height);

										ime_buf.Release();
									}

									Pics::Image::shot_set.full_width = current_record.c_width;
									Pics::Image::shot_set.full_height = current_record.c_height;
									Pics::Image::shot_set.offset_x = 0;
									Pics::Image::shot_set.offset_y = 0;
									Pics::Image::shot_set.screen_width = current_record.width;
									Pics::Image::shot_set.screen_height = current_record.height;

									Pics::Image::shot_set.i_flags = SFSco::IOpenGL::ScreenSet::FlagDataDestroy;

								// get source name

									Pics::Image::Encode(ime_buf, Pics::Image::shot_set);

								break;

							}
												
							rup |= core_ptr->SetDecodeFrame(current_record, picture_buffer, current_record._reserved[5] | 0x00100000);

							current_record.flags &= OpenGL::DecodeRecord::RecordFlags;
					
							if (rup & 1) {
								current_record.flags |= OpenGL::DecodeRecord::FlagSize;
								Program::Decode(current_record);

								current_record.flags ^= OpenGL::DecodeRecord::FlagSize;
							}
						}
					}

					if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
						if (rup & 1) {
							rup ^= 1;
						
							System::MemoryCopy(de_rec + si, &current_record, sizeof(OpenGL::DecodeRecord));

						} else {

							de_rec[si].flags = current_record.flags;
						}

						render_queue.Release();										
					}

				} else {
					if (current_record.flags & OpenGL::DecodeRecord::FlagRefresh) {
						current_record.flags &= ~OpenGL::DecodeRecord::FlagRefresh;
					
						rup |= core_ptr->SetDecodeFrame(current_record, picture_buffer, current_record._reserved[5] | 0x00100000);
					
						current_record.flags &= OpenGL::DecodeRecord::RecordFlags;

						if (rup & 1) {
							current_record.flags |= OpenGL::DecodeRecord::FlagSize;
							Program::Decode(current_record);

							current_record.flags ^= OpenGL::DecodeRecord::FlagSize;
						}				
						

						if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
							if (rup & 1) {
								rup ^= 1;
						
								System::MemoryCopy(de_rec + si, &current_record, sizeof(OpenGL::DecodeRecord));
							} else {

								de_rec[si].flags = current_record.flags;
							}

							render_queue.Release();										
						}
					}
				}
		}
	}									
	


	return rup;
}

