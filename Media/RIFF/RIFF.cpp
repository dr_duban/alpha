/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\RIFF.h"
#include "System\SysUtils.h"


Memory::Allocator * Video::RIFF::riff_pool = 0;

Video::RIFF::RIFF(unsigned int pflag) : Program(pflag), terminator(1), reciever_thread(0), chunk_type(0), chunk_size(0), rili_form(0), temp_offset(0), parse_step(0), op_size(0), time_scale(1), stream_runner(0), stream_top(0) {
	
}

Video::RIFF::~RIFF() {
	terminator = 0;
	if (reciever_thread) {
		SetEvent(program_block[0]);
		if (reciever_thread) {
			WaitForSingleObject(reciever_thread, INFINITE);
			CloseHandle(reciever_thread);
		}
	}

	
	temp_buf.Destroy();	
	riff_obj.Destroy();

//	if (source) delete source;
	source.Destroy();

}



UI_64 Video::RIFF::Initialize() {
	wchar_t pool_name[] = {9, L'r',L'i', L'f', L'f', L' ', L'p', L'o', L'o', L'l'};
	UI_64 result(0);

	if (riff_pool) return -1;

	if (riff_pool = new Memory::Allocator(0)) {
		result = riff_pool->Init(new Memory::Buffer(), 0x0100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;


	return result;
}

UI_64 Video::RIFF::Finalize() {
	if (riff_pool) delete riff_pool;


	return 0;
}

bool Video::RIFF::IsAVI(Data::Stream & src) {
	UI_64 b_size(1024);
	bool result(false);
	
	if (unsigned int * src_ptr = reinterpret_cast<unsigned int *>(src.MoveLock(b_size, (UI_64)0))) {
		b_size = src_ptr[1];
		if ((src_ptr[0] == 'FFIR') && (src_ptr[2] == ' IVA')) {
			result = true;
		}		
	}

	return result;
}

Media::Source * Video::RIFF::GetVSource(OpenGL::DecodeRecord & src_rec) {
	Media::Source * result(0);
	unsigned int s_val(0);

	if (src_rec.pic_id >= video.count) src_rec.pic_id = 0;
	s_val = src_rec.pic_id;

	if (StreamEntry * st_hdr = reinterpret_cast<StreamEntry *>(riff_obj.Acquire())) {
		for (unsigned int j(0);j<video.count;j++) {
			if (st_hdr[j]._sequence) {
				if (st_hdr[j]._sequence->IsVideo())
				if (s_val-- == 0) {
					result = st_hdr[j]._sequence;
					break;
				}
			}
		}
		riff_obj.Release();
	}

	return result;
}

Media::Program * Video::RIFF::SetSource(const Data::Stream & src, unsigned int preview_flag) {
	RIFF * result(0);
	UI_64 eval(0);
		
	if (result = new RIFF(preview_flag)) {
		eval = result->riff_obj.New(_tid, 32*sizeof(StreamEntry), riff_pool);

		eval |= result->temp_buf.New(0, 131072);

		if (eval != -1) {
			if (StreamEntry * s_hdr = reinterpret_cast<StreamEntry *>(result->riff_obj.Acquire())) {
//				System::MemoryFill(s_hdr, 32*sizeof(StreamEntry), 0);
				
				result->riff_obj.Release();

				result->stream_top = 32;
				result->source = src;

				eval = 0;
				if (result->program_block[0]) {
					if (result->reciever_thread = CreateThread(0, 0, ReceiverLoop, result, 0, 0)) {

					} else {
						eval = GetLastError();
					}
				}
			}
		}
		
		if (eval) {
			delete result;
			result = 0;
		}
	}


	return result;
}


DWORD __stdcall Video::RIFF::ReceiverLoop(void * cntx) {
	UI_64 result(0), block_size(0), b_val(0), t_val(0);

	RIFF * riff_ptr = reinterpret_cast<RIFF *>(cntx);
	const unsigned char * block_ptr(0);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // THREAD_PRIORITY_TIME_CRITICAL
		

	for (;riff_ptr->terminator;) {

		block_size = FILE_BUFFER_SIZE;
		if (block_ptr = riff_ptr->source.IncLock(block_size)) {
			riff_ptr->ParseBlock(block_ptr, block_size);
		}
	}

	if (StreamEntry * s_ptr = reinterpret_cast<StreamEntry *>(riff_ptr->riff_obj.Acquire())) {
		for (;riff_ptr->stream_runner--;) {
			if (s_ptr[riff_ptr->stream_runner]._sequence) delete s_ptr[riff_ptr->stream_runner]._sequence;
		}
		
		riff_ptr->riff_obj.Release();
	}


	return result;
}



UI_64 Video::RIFF::ParseBlock(const unsigned char *& block_ptr, UI_64 block_size) {
	UI_64 r64_val[4] = {0, 0, 0, 0};

	switch (parse_step) {
		case 1: goto chunk_type_op;
		case 2: goto chunk_size_op;
				
		case 3: goto riff_form_op;
		case 4: goto chunk_parse_op;
	}




	for (;block_size && terminator;) {

		if (block_ptr[0] == 0) {
			block_ptr++;
			block_size--;
		}

		parse_step = 1;
		chunk_type = 0;
		op_size = 4;
chunk_type_op:
		for (;(block_size) && (op_size);op_size--, block_ptr++) {
			chunk_type <<= 8;
			chunk_type |= block_ptr[0];

			block_size--;
		}
		if (block_size == 0) break;


		parse_step = 2;
		chunk_size = 0;
		op_size = 4;

chunk_size_op:
		for (;(block_size) && (op_size);op_size--, block_ptr++) {			
			chunk_size |= (unsigned int)block_ptr[0] << ((4 - op_size) << 3);
			
			block_size--;
		}
		if (block_size == 0) break;
		
		


		if ((chunk_type == 'RIFF') || (chunk_type == 'LIST')) {
			parse_step = 3;

			rili_form = 0;
			op_size = 4;
			
riff_form_op:
			for (;(block_size) && (op_size);op_size--, block_ptr++) {	
				rili_form <<= 8;
				rili_form |= block_ptr[0];
			
				block_size--;
			}
			if (block_size == 0) break;

			chunk_size = 0;
		}

		parse_step = 4;

chunk_parse_op:
		if ((chunk_size) && (chunk_type)) {

			if (chunk_type != 'idx1') {
				if ((chunk_size - temp_offset) <= block_size) {
					if (temp_offset) {
						if (unsigned char * tc_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
							System::MemoryCopy_SSE3(tc_ptr + temp_offset, block_ptr, (chunk_size - temp_offset));

							ParseChunk(reinterpret_cast<const unsigned int *>(tc_ptr), chunk_size);

							temp_buf.Release();						
						}

						block_ptr += (chunk_size - temp_offset);
						block_size -= (chunk_size - temp_offset);

					} else {

						ParseChunk(reinterpret_cast<const unsigned int *>(block_ptr), chunk_size);

						block_ptr += chunk_size;
						block_size -= chunk_size;
					}

					chunk_size = 0;
				} else {
					if (temp_offset) {
						if (unsigned char * tc_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
							System::MemoryCopy_SSE3(tc_ptr + temp_offset, block_ptr, block_size);

							temp_buf.Release();

							temp_offset += block_size;
						}

					} else {
						if (temp_buf.Resize(chunk_size + 32) != -1) {
							if (void * t_ptr = temp_buf.Acquire()) {
								System::MemoryCopy(t_ptr, block_ptr, block_size);

								temp_buf.Release();

								temp_offset = block_size;
							}
						} else {
					// error
						}
					}
				}
			
			} else {
				// error
			}
						
		} else {
			if (chunk_size <= block_size) {
				block_ptr += chunk_size;
				block_size -= chunk_size;				

				chunk_size = 0;
			} else {
				chunk_size -= block_size;
			}

		}

		if (chunk_size) break;

		parse_step = 0;
		temp_offset = 0;
	}



	return 0;
}

UI_64 Video::RIFF::ParseChunk(const unsigned int * ck_ptr, UI_64 ck_size) {
	UI_64 result(0);
	I_64 t_val(0);
	unsigned int stream_idx(0);

	switch (rili_form) {
		case 'hdrl': // header list
			switch (chunk_type) {
				case 'avih':


				break;

			}


		break;
		case 'strl': // stream list
			switch (chunk_type) {
				case 'strh': // stream header
					

					if (stream_runner < stream_top)
					if (StreamEntry * stream_ptr = reinterpret_cast<StreamEntry *>(riff_obj.Acquire())) {
						stream_ptr[stream_runner]._sequence = 0;
						stream_ptr[stream_runner].time_delta = 10000;
						stream_ptr[stream_runner].time_delta *= ck_ptr[5];
						stream_ptr[stream_runner].time_delta /= ck_ptr[6];
						stream_ptr[stream_runner].time_position = stream_ptr[stream_runner].time_delta*ck_ptr[7];




						switch (ck_ptr[0]) {
							case 'sdiv': // video stream
								switch (ck_ptr[1]) {
									case '462H': case '1CVA': case 'CVAD': case '4PML': case '3QVS': case '462X':
										if (stream_ptr[stream_runner]._sequence = Media::CodedSource::Create(0x1B)) {
											if (++video.count == 1) stream_ptr[stream_runner]._sequence->On();

											start_mark = 0;
											stop_mark = 10000;
											stop_mark *= ck_ptr[8];
											stop_mark *= ck_ptr[5];
											stop_mark /= ck_ptr[6];

										}
									break;
									case '562H': case 'CVEH':

									break;							
								}
							break;
							case 'sdua': // audio stream

							break;
							case 'stxt': // test stream

							break;
						}

						riff_obj.Release();


						if (++stream_runner >= stream_top) {
							if (riff_obj.Expand(32*sizeof(StreamEntry)) != -1) {
								stream_top += 32;
							}
						}
					}




				break;
				case 'strf': // stream format: BITMAPINFO / WAVEFORMATEX

				break;

			}

		break;
		case 'movi': case 'rec ':
			stream_idx = ((chunk_type & 0x0F000000) >> 20) | ((chunk_type & 0x000F0000) >> 16);
			if (stream_idx < stream_runner) {
				if (StreamEntry * s_ptr = reinterpret_cast<StreamEntry *>(riff_obj.Acquire())) {
					if (s_ptr[stream_idx]._sequence) {
						t_val = s_ptr[stream_idx].time_position;
						s_ptr[stream_idx].time_position += s_ptr[stream_idx].time_delta;

						result = s_ptr[stream_idx]._sequence->Decode(reinterpret_cast<const unsigned char *>(ck_ptr), ck_size, t_val);

										
						if (result >= RESULT_DECODE_COMPLETE) {
							if (result & RESULT_DECODE_COMPLETE) {
								if (s_ptr[stream_idx]._sequence->IsVideo()) {
									if (pending_req.program) {
										if (GetVSource(pending_req)) {
											pending_req.flags = OpenGL::DecodeRecord::FlagSourceOn;
											Program::Decode(pending_req);

											pending_req.program = 0;
										}
									}

			//						NVFlush();
									SetPTS(t_val);
								}

							} else {
								result >>= 32;
/*					
								case -3: // end of source
								case -4: // codec is not set
								case -6: // slice header is missing
								case -91: case -92: // damaged bit stream
								case -94: // slice parsing error
*/
							}

						} else {
							// test for pending source select, send decode record on success

							if (pending_req.program) {
								if (GetVSource(pending_req)) {
									pending_req.flags = OpenGL::DecodeRecord::FlagSourceOn;
									Program::Decode(pending_req);

									pending_req.program = 0;
								}
							}
						}						
					}

					riff_obj.Release();
				}
			}

		break;

		

	}

	return result;
}

