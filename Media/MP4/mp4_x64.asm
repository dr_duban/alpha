
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
MP4_AdjustOffsets	PROC	
	XOR r11, r11
	XOR r10, r10
	NOT r10

	
	LEA rax, [rdx - 1]

	MOV r8, rdx
	MOV r9, rcx

ALIGN 16
	calc_loop:
		CMP r10, [rcx+16]
		JNZ reset
			ADD [rcx+16], r11
			ADD r11, [rcx+24]

		JMP next_se
		reset:
			MOV r10, [rcx+16]
			MOV r11, [rcx+24]

		next_se:

		ADD rcx, 64
	DEC rdx
	JNZ calc_loop

	
		
	LEA rdx, [r8 - 2]
	MOV rcx, r9
	
	MOV QWORD PTR [rcx + 32], 1
	MOV QWORD PTR [rcx + 40], 1

	MOV QWORD PTR [rcx + 32 + 64], 0
	MOV QWORD PTR [rcx + 40 + 64], 0


	MOV r8, rcx					; beginning
	LEA r9, [rcx + 64]
	MOV r10, r9
	MOV r11, 2

	LEA rcx, [rcx + 64 + 64]

	
align 16
	sort_loop:
		MOV rax, [rcx + 16] ; new offset val
		

		CMP rax, [r10 + 16]
		JB search_to_the_beginning
	align 16
		search_to_the_end:
			CMP r10, r9
			JZ search_complete
				MOV r10, [r10 + 40]
				LEA r10, [r10*8]
				LEA r10, [r8 + r10*8]

				CMP rax, [r10 + 16]
				JA search_to_the_end

				MOV r10, [r10 + 32]
				LEA r10, [r10*8]
				LEA r10, [r8 + r10*8]

			JMP search_complete

	align 16
		search_to_the_beginning:
			MOV r10, [r10 + 32]
			LEA r10, [r10*8]
			LEA r10, [r8 + r10*8]

			CMP r10, r9
			JZ search_complete
				CMP rax, [r10 + 16]
				JB search_to_the_beginning

	align 16
		search_complete:

			CMP r10, r9
			JNZ no_bend				
				CMP rax, [r9 + 16]
				JB no_bend
					MOV r9, rcx
			no_bend:



			MOV rax, [r10 + 40]
			MOV [r10 + 40], r11
			MOV [rcx + 40], rax

			LEA rax, [rax*8]
			LEA rax, [r8 + rax*8]

			MOV r10, [rax + 32]
			MOV [rax + 32], r11

			MOV [rcx + 32], r10
			


			MOV r10, rcx

					
		ADD rcx, 64
		INC r11
	DEC rdx
	JNZ sort_loop

	MOV rax, [r9 + 40]

	RET
MP4_AdjustOffsets	ENDP



END
