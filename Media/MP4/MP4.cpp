/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\MP4.h"
#include "System\SysUtils.h"

#include "Media\Source.h"

Memory::Allocator * Video::MP4::mp4_pool = 0;

Video::MP4::MP4(unsigned int pflag) : Program(pflag), terminator(1), reciever_thread(0), box_type(0), box_size(0), temp_offset(0), parse_step(0), op_size(0), time_scale(1), sample_runner(0), sample_top(0), track_runner(0), track_top(0), start_idx(0), stop_idx(0), current_idx(0), current_track(-1), jump_val(0), avc_size(4), no_video(1), frame_counter(0), temp_vid(-1) {
	
}

Video::MP4::~MP4() {
	terminator = 0;
	if (reciever_thread) {
		SetEvent(program_block[0]);
		if (reciever_thread) {
			WaitForSingleObject(reciever_thread, INFINITE);
			CloseHandle(reciever_thread);
		}
	}

	
	temp_buf.Destroy();
	time_table.Destroy();
	mp4_obj.Destroy();

//	if (source) delete source;
	source.Destroy();

}



UI_64 Video::MP4::Initialize() {
	wchar_t pool_name[] = {8, L'm',L'p', L'4', L' ', L'p', L'o', L'o', L'l'};
	UI_64 result(0);

	if (mp4_pool) return -1;

	if (mp4_pool = new Memory::Allocator(0)) {
		result = mp4_pool->Init(new Memory::Buffer(), 0x0100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;


	return result;
}

UI_64 Video::MP4::Finalize() {
	if (mp4_pool) delete mp4_pool;


	return 0;
}

bool Video::MP4::IsMP4(Data::Stream & src) {
	UI_64 b_size(1024);
	bool result(false);
	
	if (unsigned int * src_ptr = reinterpret_cast<unsigned int *>(src.MoveLock(b_size, (UI_64)0))) {
		b_size = src_ptr[0];
		if (src_ptr[1] == 'pytf') {
			result = true;
		}		
	}

	return result;
}

void Video::MP4::NVFlush(TrackEntry * tk_ptr) {
	for (unsigned int j(0);j<track_runner;j++) {
		if (tk_ptr[j]._sequence) {
			if (!tk_ptr[j]._sequence->IsVideo()) {

				tk_ptr[j]._sequence->Flush(current_time, sync_start);

			}
		}
	}
	sync_start = -1;
}

void Video::MP4::NVReset() {
	if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (!tk_hdr[j]._sequence->IsVideo()) {
					tk_hdr[j]._sequence->Reset();
				}
			}
		}
		mp4_obj.Release();
	}
	
	sync_start = -1;
	
}

unsigned int Video::MP4::EnableAudioSource(unsigned int a_sel, OpenGL::Element & a_plane) {	

	if (a_sel < audio.count) {
		if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {
						if ((audio.flags & AUDIO_SOURCE_EXCLUSIVE) && (a_sel != 0)) {
							tk_hdr[j]._sequence->ResetSat();
							tk_hdr[j]._sequence->Off();
							
						}
						if (a_sel-- == 0) {
							tk_hdr[j]._sequence->On();
							tk_hdr[j]._sequence->ResetSatRecord(a_plane);
						}
					}
				}
			}
			mp4_obj.Release();
		}
	}

	return a_sel;
}

unsigned int Video::MP4::IsAudioActive(unsigned int a_sel) {
	unsigned int result(0);

	if (a_sel < audio.count) {
		if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {						
						if (a_sel-- == 0) {
							result = tk_hdr[j]._sequence->TestFlag(SOURCE_NO_RENDER);
							break;
						}
					}
				}
			}
			mp4_obj.Release();
		}
	}

	return result;
}

unsigned int Video::MP4::GetAudioLNG(unsigned int a_sel) {
	unsigned int result(a_sel);

	if (a_sel < audio.count) {
		if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {
						if (a_sel-- == 0) {
							if (tk_hdr[j].lang_id) result = tk_hdr[j].lang_id;
							break;
						}
					}
				}
			}
			mp4_obj.Release();
		}
	}

	return result;
}

unsigned int Video::MP4::DisableAudioSource(unsigned int a_sel) {
	


	return a_sel;
}



void Video::MP4::SetAudioVolume(float a_val) {
	if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsAudio()) {
					tk_hdr[j]._sequence->SetVolume(&a_val);
				}
			}
		}
		mp4_obj.Release();
	}
}


Media::Source * Video::MP4::GetVSource(UI_64 & p_id) {
	Media::Source * result(0);
	unsigned int s_val(0);

	if (p_id >= video.count) p_id = 0;
	s_val = p_id;

	if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsVideo())
				if (s_val-- == 0) {
					result = tk_hdr[j]._sequence;
					break;
				}
			}
		}
		mp4_obj.Release();
	}

	return result;
}

Media::Source * Video::MP4::GetASource(UI_64 & p_id) {
	Media::Source * result(0);
	unsigned int s_val(0);

	if (p_id >= video.count) p_id = 0;
	s_val = p_id;

	if (TrackEntry * tk_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsAudio())
				if (s_val-- == 0) {
					result = tk_hdr[j]._sequence;
					break;
				}
			}
		}
		mp4_obj.Release();
	}

	return result;
}


Media::Program * Video::MP4::SetSource(const Data::Stream & src, unsigned int preview_flag) {
	MP4 * result(0);
	UI_64 eval(0);
		
	if (result = new MP4(preview_flag)) {
		eval = result->mp4_obj.New(_tid, 32*sizeof(TrackEntry), mp4_pool);
		eval |= result->time_table.New(sample_table_tid, 8192*sizeof(SampleEntry), mp4_pool);
		eval |= result->temp_buf.New(0, 131072);

		if (eval != -1) {
			result->sample_runner = 0;
			result->sample_top = 8192;

			if (TrackEntry * thdr = reinterpret_cast<TrackEntry *>(result->mp4_obj.Acquire())) {
//				System::MemoryFill(thdr, 32*sizeof(TrackEntry), 0);

				result->mp4_obj.Release();

				result->track_top = 32;
				result->source = src;

				eval = 0;
				if (result->program_block[0]) {
					if (result->reciever_thread = CreateThread(0, 0, ReceiverLoop, result, 0, 0)) {

					} else {
						eval = GetLastError();
					}
				}
			}
		}
		
		if (eval) {
			delete result;
			result = 0;
		}
	}


	return result;
}

DWORD __stdcall Video::MP4::ReceiverLoop(void * cntx) {
	UI_64 result(0), block_size(0), b_val(0);
	I_64 t_val(0), scan_limit(0);

	I_64 pool_index[2*MAX_POOL_COUNT];

	MP4 * mp4_ptr = reinterpret_cast<MP4 *>(cntx);
	const unsigned char * block_ptr(0);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // THREAD_PRIORITY_TIME_CRITICAL THREAD_PRIORITY_ABOVE_NORMAL
	
	Memory::Chain::RegisterThread(pool_index);
//	mp4_ptr->source->Reset();
	

	for (;mp4_ptr->terminator;) {
		if (scan_limit) {
			if ((mp4_ptr->frame_counter > scan_limit) && (mp4_ptr->no_video)) {
				mp4_ptr->terminator = 0;				
				break;
			}
		} else {
			if (mp4_ptr->sample_runner) {
				scan_limit = mp4_ptr->sample_runner/20;
				if (scan_limit > 500) scan_limit = 500;

			}
		}

		
		if (mp4_ptr->terminator == 1) {
			block_size = FILE_BUFFER_SIZE;

			if (mp4_ptr->jump_val) {
				mp4_ptr->jump_val += FILE_BUFFER_SIZE;				

				mp4_ptr->source.JumpLock(block_size, mp4_ptr->jump_val);
				mp4_ptr->jump_val = 0;

			}
			// headers mode
			
			if (block_ptr = mp4_ptr->source.IncLock(block_size)) {
				if (mp4_ptr->source.GetStatus() & STREAM_REPEAT_FLAG) {
					mp4_ptr->current_idx = mp4_ptr->start_idx;
					mp4_ptr->NVReset();

					mp4_ptr->search_mode = 2;
				}
			}
			

			if (block_ptr) mp4_ptr->ParseBlock(block_ptr, block_size);
		} else {
				// presentation mode from start_idx till stop_idx

			switch (mp4_ptr->search_mode) {
				case 1:
					mp4_ptr->current_idx = mp4_ptr->start_idx;

					if (SampleEntry * se_ptr = reinterpret_cast<SampleEntry *>(mp4_ptr->time_table.Acquire())) {

						for (unsigned int i(0);i<mp4_ptr->sample_runner;i++) {
							if (mp4_ptr->search_mark < se_ptr[i].presentation_time) {							
								mp4_ptr->current_idx = i;
							
								break;
							}
						}

						mp4_ptr->time_table.Release();
					}					
				
					if (mp4_ptr->program_block[1] == 0) {
						mp4_ptr->NVReset();
					}

					mp4_ptr->search_idx++;
					mp4_ptr->search_mode = 2;
				break;
				case 11: // volume
					mp4_ptr->SetAudioVolume(reinterpret_cast<double *>(&mp4_ptr->search_mark)[0]);
					mp4_ptr->search_mode = 0;
				break;
			}

/*
			if (mp4_ptr->current_idx == mp4_ptr->start_idx) {
				mp4_ptr->pts[0] = mp4_ptr->pts[1] = 0;
			}
*/


			if (TrackEntry * tk_ptr = reinterpret_cast<TrackEntry *>(mp4_ptr->mp4_obj.Acquire())) {
				if (SampleEntry * se_ptr = reinterpret_cast<SampleEntry *>(mp4_ptr->time_table.Acquire())) {
					// temp_offset
					if (tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence) {
						t_val = se_ptr[mp4_ptr->current_idx].presentation_time;
						block_size = se_ptr[mp4_ptr->current_idx].data_size;
						b_val = 0;

						if (block_ptr = mp4_ptr->source.MoveLock(block_size, se_ptr[mp4_ptr->current_idx].data_offset)) {							
							result = 0;

							if (tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->IsVideo()) {
								if (mp4_ptr->search_mode) {
									mp4_ptr->search_mode = 0;

									mp4_ptr->pts[0] = mp4_ptr->pts[1] = 0;
									
									tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->JumpOn();
								}

								if (mp4_ptr->audio.count == 0) mp4_ptr->sync_start = 0;

								for (;block_size > b_val;) {
									block_size -= b_val;
									block_ptr += b_val;

									b_val = 0;
									switch (mp4_ptr->avc_size) {
										case 4:
											b_val |= block_ptr[0]; b_val <<= 8; block_ptr++; block_size--;
										case 3:
											b_val |= block_ptr[0]; b_val <<= 8; block_ptr++; block_size--;
										case 2:
											b_val |= block_ptr[0]; b_val <<= 8; block_ptr++; block_size--;
										case 1:
											b_val |= block_ptr[0]; block_ptr++; block_size--;
										break;
									}
									
									result = tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->DecodeUnit(block_ptr, b_val, t_val);									
								}

								if (result >= RESULT_DECODE_COMPLETE) {
									if (result & RESULT_DECODE_COMPLETE) {
										
										if (mp4_ptr->program_block[1]) {
											mp4_ptr->SetSTP();

										} else {

											if (se_ptr[mp4_ptr->current_idx].track_idx == mp4_ptr->temp_vid) mp4_ptr->current_time = t_val;
												

											if (mp4_ptr->pending_req.program) {
												if (mp4_ptr->GetVSource(mp4_ptr->pending_req.pic_id)) {
													mp4_ptr->pending_req.flags |= OpenGL::DecodeRecord::FlagSourceOn;
													Decode(mp4_ptr->pending_req);

													mp4_ptr->pending_req.program = 0;
												}
											}

											if (mp4_ptr->sync_start >= 0) {
												mp4_ptr->NVFlush(tk_ptr);
											}
									
											if (mp4_ptr->SetPTS(t_val) < 0) {
												tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->SetRenderSkip(1);
											}

										}
									} else {
										result >>= 32;
/*					
										case -3: // end of source
										case -4: // codec is not set
										case -6: // slice header is missing
										case -91: case -92: // damaged bit stream
										case -94: // slice parsing error
*/
									}

								}

							} else {
								if (!mp4_ptr->search_mode) {

									if (tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->IsAudio()) {
										if (mp4_ptr->sync_start == -1) mp4_ptr->sync_start = t_val;
									}

									result = tk_ptr[se_ptr[mp4_ptr->current_idx].track_idx]._sequence->DecodeUnit(block_ptr, block_size, t_val);

								}
							}							
						}						
					}

					
					if (se_ptr[mp4_ptr->current_idx].next_e == mp4_ptr->start_idx) {
// FlagSSNext
						if (mp4_ptr->terminator > 1)
						if (mp4_ptr->pending_req.element_obj) {
							mp4_ptr->pending_req.flags = OpenGL::DecodeRecord::FlagSSNext;
							mp4_ptr->pending_req.pic_id = -1;
							Decode(mp4_ptr->pending_req);

						}

						mp4_ptr->NVReset();

						mp4_ptr->search_mode = 2;
					}

					mp4_ptr->current_idx = se_ptr[mp4_ptr->current_idx].next_e;

					mp4_ptr->time_table.Release();
				}

				mp4_ptr->mp4_obj.Release();
			}

		}

	}


	mp4_ptr->Terminate();


	if (TrackEntry * tk_ptr = reinterpret_cast<TrackEntry *>(mp4_ptr->mp4_obj.Acquire())) {
		for (;mp4_ptr->track_runner--;) {
			if (tk_ptr[mp4_ptr->track_runner]._sequence) delete tk_ptr[mp4_ptr->track_runner]._sequence;
		}
		
		mp4_ptr->mp4_obj.Release();
	}

	Memory::Chain::UnregisterThread();

	return result;
}


UI_64 Video::MP4::ParseBlock(const unsigned char *& block_ptr, UI_64 & block_size) {
	UI_64 r64_val[4] = {0, 0, 0, 0};

	switch (parse_step) {
		case 1: goto box_size_op;
		case 2: goto box_type_op;

		case 3:	goto box_xsize_op;		
		case 4: goto box_parse_op;
	}




	for (;block_size && (terminator == 1);) {

		parse_step = 1;
		box_size = 0;
		op_size = 4;
box_size_op:
		for (;(block_size) && (op_size);op_size--, block_ptr++) {
			box_size <<= 8;
			box_size |= block_ptr[0];					
			
			block_size--;
		}
		if (block_size == 0) break;


		parse_step = 2;
		box_type = 0;
		op_size = 4;

box_type_op:
		for (;(block_size) && (op_size);op_size--, block_ptr++) {
			box_type <<= 8;
			box_type |= block_ptr[0];

			block_size--;
		}
		if (block_size == 0) break;


		parse_step = 3;

		switch (box_size) {
			case 0: // file size
				box_size = -1; // block_size;
			break;
			case 1:
				box_size = 0;
				op_size = 8;
box_xsize_op:
				for (;(block_size) && (op_size);op_size--, block_ptr++) {
					box_size <<= 8;
					box_size |= block_ptr[0];					
			
					block_size--;
				}

				if (block_size) box_size -= 8;
			break;
		
		}
		if (block_size == 0) break;

		box_size -= 8;
		parse_step = 4;

box_parse_op:
		if ((box_size) && (box_type)) {
			if ((box_type == 'moov') || (box_type == 'trak') || (box_type == 'stbl') || (box_type == 'dinf') || (box_type == 'minf') || (box_type == 'mdia')) { // containerts
				box_size = 0;
			}


			if (box_type == 'mdat') {
				if (sample_runner) { // media data
// format time tasbles
					if (SampleEntry * se_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {					

						start_idx = MP4_AdjustOffsets(se_ptr, sample_runner, sizeof(SampleEntry));
						stop_idx = se_ptr[start_idx].prev_e;

						start_mark = se_ptr[start_idx].presentation_time;
						stop_mark = se_ptr[stop_idx].presentation_time;

						time_table.Release();						
					}

					current_idx = start_idx;
					
					if (terminator) terminator = 2;
				} else {
					jump_val = box_size - block_size;
					parse_step = 0;
				}

				box_size = 0;
			}




			if (box_size)
			if ((box_size - temp_offset) <= block_size) {
				if (temp_offset) {
					if (unsigned char * tc_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
						System::MemoryCopy_SSE3(tc_ptr + temp_offset, block_ptr, (box_size - temp_offset));

						ParseBox(tc_ptr, box_size);

						temp_buf.Release();						
					}

					block_ptr += (box_size - temp_offset);
					block_size -= (box_size - temp_offset);

				} else {

					ParseBox(block_ptr, box_size);

					block_ptr += box_size;
					block_size -= box_size;
				}

				box_size = 0;
			} else {
				if (temp_offset) {
					if (unsigned char * tc_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
						System::MemoryCopy_SSE3(tc_ptr + temp_offset, block_ptr, block_size);

						temp_buf.Release();

						temp_offset += block_size;
					}

				} else {
					if (temp_buf.Resize(box_size + 32) != -1) {
						if (void * t_ptr = temp_buf.Acquire()) {
							System::MemoryCopy(t_ptr, block_ptr, block_size);

							temp_buf.Release();

							temp_offset = block_size;
						}
					} else {
					// error
					}
				}

			}
			
		} else {
			// error
		}

		if (box_size | jump_val) break;

		parse_step = 0;
		temp_offset = 0;
	}



	return 0;
}

UI_64 Video::MP4::ParseBox(const unsigned char * bx_ptr, UI_64 bx_size) {
//	SFSco::Object t_obj;
	SampleEntry * sample_ptr(0);

	I_64 r64_val[4];
	int i_val[8];

	switch (box_type) {
// containers
//		case 'moov': // all metadata
//		case 'mvex': // x moovie
//		case 'trak': // track or stream
//		case 'stbl':

//		case 'edts': // edit list
//		case 'mdia': // media info in trak
//		case 'minf': // media information
//		case 'dinf': // data information box		
//		case 'xml':  // XML
//		case 'bxml': // binary XML
			
//			box_size = 0;
//		break;
// moov ======================================================================================================	
		case 'mvhd':
		case 'mdhd':

			time_scale = 0;

			if (bx_ptr[0] == 1) { // version check
				for (unsigned int i(0);i<4;i++) {
					time_scale <<= 8;
					time_scale |= bx_ptr[20+i];
				}
				i_val[0] = 32;
			} else {
				for (unsigned int i(0);i<4;i++) {
					time_scale <<= 8;
					time_scale |= bx_ptr[12+i];
				}
				i_val[0] = 20;
			}

			if (current_track < track_runner) {
				if (TrackEntry * t_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					i_val[1] = bx_ptr[i_val[0]];
					i_val[1] <<= 8; i_val[1] |= bx_ptr[i_val[0] + 1];


					t_hdr[current_track].lang_id = (i_val[1] & 0x1F);
					t_hdr[current_track].lang_id <<= 8; t_hdr[current_track].lang_id |= ((i_val[1] >> 5) & 0x1F);
					t_hdr[current_track].lang_id <<= 8; t_hdr[current_track].lang_id |= ((i_val[1] >> 10) & 0x1F);

					t_hdr[current_track].lang_id |= 0x00606060;
					
					if (t_hdr[current_track]._sequence)
					if (t_hdr[current_track]._sequence->IsAudio() && Media::Source::TestLNG(t_hdr[current_track].lang_id)) {
						for (unsigned int i(0);i<track_runner;i++) {
							if (t_hdr[i]._sequence)
							if (t_hdr[i]._sequence->IsAudio()) {
								t_hdr[i]._sequence->Off();

								break;
							}
						}

						t_hdr[current_track]._sequence->On();

					}

					mp4_obj.Release();

				}
			}
		break;


/*
		case 'mehd': // moovie xheader

		break;
*/		
// trac ===============================================================================
		case 'tkhd': // track header
			current_track = -1;
/*			
			if ((bx_ptr[3] & 1) == 0) { // not enabled

				break;
			}
*/
			i_val[0] = 0;
			if (bx_ptr[0] == 1) { // version check
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[20+i];
				}
			} else {
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[12+i];
				}
			}

			
			if (TrackEntry * t_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
				for (unsigned int i(0);i<track_runner;i++) {
					if (t_hdr[i].id == i_val[0]) {
						current_track = i;
						break;
					}
				}

				mp4_obj.Release();
			}

			if (current_track == -1) {
				if (track_runner < track_top) {
					current_track = track_runner++;
				} else {
					if (mp4_obj.Expand(32*sizeof(TrackEntry)) != -1) {
						if (TrackEntry * t_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
							System::MemoryFill(t_hdr + track_top, 32*sizeof(TrackEntry), 0);

							mp4_obj.Release();
						}

						track_top += 32;

						current_track = track_runner++;
					}
				}

				if (current_track != -1) {
					if (TrackEntry * t_hdr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
						t_hdr[current_track].id = i_val[0];

						t_hdr[current_track].duration = 0;
						t_hdr[current_track].sample_count = 0;							
						t_hdr[current_track].tt_offset = sample_runner;

						if (bx_ptr[0] == 1) {
							for (unsigned int i(0);i<8;i++) {
								t_hdr[current_track].duration <<= 8;
								t_hdr[current_track].duration |= bx_ptr[28+i];
							}
						} else {
							for (unsigned int i(0);i<4;i++) {
								t_hdr[current_track].duration <<= 8;
								t_hdr[current_track].duration |= bx_ptr[20+i];
							}
						}

						t_hdr[current_track].duration *= 10000;
						t_hdr[current_track].duration /= time_scale;

						mp4_obj.Release();
					}
				}
			}

			
		break;
/*
		case 'tref': // track reference (reference to another track)

		break;
		case 'trex': // track x-default

		break;
*/

// media ===============================================================
/*
		case 'hdlr': // media handle type

		break;
*/
// media info ===============================================
/*
		case 'vmhd': // video media header

		break;
		case 'smhd': // sound media header

		break;
		case 'hmhd': // hint media header

		break;
		case 'nmhd': // null media header

		break;
		case 'dref': // data reference box, used with url and urn
			i_val = 0;
			for (unsigned int i(0);i<4;i++) {
				i_val <<= 8;
				i_val |= block_ptr[4+i];
			}

			if (i_val) {
				block_ptr += 8;
				block_size -= 8;

				// version flags data_entry

			}

						
		break;


		case 'dpnd': // MPEG-4 dependency

		break;
		case 'ipir': // IPI declarations for referenced track

		break;
		case 'mpod': // reference as included track

		break;
		case 'sync': // reference as sync

		break;

		case 'url':

		break;
		case 'urn':

		break;
*/
// sample table box=============================
		case 'stsd': // sample descriptor
			if (current_track != -1) {
				bx_ptr += 4; bx_size -= 4;

			// sequence obj count per track
				i_val[0] = bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[0] <<= 8; i_val[0] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[0] <<= 8; i_val[0] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[0] <<= 8; i_val[0] |= bx_ptr[0]; bx_ptr++; bx_size--;

				if (TrackEntry * track_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					for (unsigned int i(0);i<1;i++) { //ui_val[0], in my design only one _sequence object is allowed per track
					// size
						i_val[1] = bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
						
						i_val[1] -= 8;

					// type
						i_val[2] = bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
						i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
						

												
					// ref_idx
						i_val[3] = bx_ptr[7];
						i_val[3] <<= 8; i_val[3] |= bx_ptr[8];

						bx_ptr += 8;
						bx_size -= 8;

						if (track_ptr[current_track]._sequence) {
							delete track_ptr[current_track]._sequence;
							track_ptr[current_track]._sequence = 0;
						}


						switch (i_val[2]) {
							case 'avc1': case 'avc2': case 'avc3': case 'avc4': // h264								
								if (track_ptr[current_track]._sequence = Media::CodedSource::Create(0x1B)) {								
									no_video = 0;

									if (++video.count == 1) {
										track_ptr[current_track]._sequence->On();
										temp_vid = current_track;
									}

									if (program_block[1]) track_ptr[current_track]._sequence->SetRenderSkip(-1);

									bx_ptr += 70;
									bx_size -= 70;


									i_val[4] = bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
						
									i_val[4] -= 8;

							// type
									i_val[5] = bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;

									if (i_val[5] == 'avcC') { // h264 config
										bx_ptr += 4;
										bx_size -= 4;

										avc_size = (bx_ptr[0] & 3) + 1;

										bx_ptr++;
										bx_size--;


										i_val[5] = bx_ptr[0] & 0x1F; bx_ptr++; bx_size--;
										r64_val[0] = 0;

										for (;i_val[5];i_val[5]--) {
											i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
											i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

											track_ptr[current_track]._sequence->DecodeUnit(bx_ptr, i_val[6], r64_val[0]);

											bx_ptr += i_val[6];
											bx_size -= i_val[6];
										}


										i_val[5] = bx_ptr[0]; bx_ptr++; bx_size--;
										
										for (;i_val[5];i_val[5]--) {
											i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
											i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

											track_ptr[current_track]._sequence->DecodeUnit(bx_ptr, i_val[6], r64_val[0]);

											bx_ptr += i_val[6];
											bx_size -= i_val[6];
										}

									}									
								}



							break;
							case 'avcp': // h264 params
								bx_ptr += 70;
								bx_size -= 70;

								bx_ptr += 4;
								bx_size -= 4;

								avc_size = (bx_ptr[0] & 3) + 1;

								bx_ptr++;
								bx_size--;


								i_val[5] = bx_ptr[0] & 0x1F; bx_ptr++; bx_size--;
								r64_val[0] = 0;

								for (;i_val[5];i_val[5]--) {
									i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

									track_ptr[current_track]._sequence->DecodeUnit(bx_ptr, i_val[6], r64_val[0]);

									bx_ptr += i_val[6];
									bx_size -= i_val[6];
								}


								i_val[5] = bx_ptr[0]; bx_ptr++; bx_size--;
										
								for (;i_val[5];i_val[5]--) {
									i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
									i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

									track_ptr[current_track]._sequence->DecodeUnit(bx_ptr, i_val[6], r64_val[0]);

									bx_ptr += i_val[6];
									bx_size -= i_val[6];
								}

							break;
							case 'hvc1': case 'hev1': // h265

/*
							'hvcC': // h265 configuration
							
							

*/

							break;
							case 'm4ds': // mpeg-4 descriptors

							break;



							case 's263': // h263

							break;

							case 'mp4a': // mpeg-4 audio
/*
								if (track_ptr[current_track]._sequence = Media::CodedSource::Create(0x0F)) {
									source_count++;
*/
								bx_ptr += 20;
								bx_size -= 20;


								i_val[4] = bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
						
								i_val[4] -= 8;

							// type
								i_val[5] = bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;


								// ES_Descriptor
								if (i_val[5] == 'esds') {
									bx_ptr += 4;
									bx_size -= 4;

									if (bx_ptr[0] == 3) { // ES_DescrTag
										for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????

										bx_ptr += 2; bx_size -= 2;

										i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
										i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

										i_val[7] = bx_ptr[0]; bx_ptr++; bx_size--;
										if (i_val[7] & 0x80) {
											// stream Dependence

											bx_ptr += 2; bx_size -= 2;
										}

										if (i_val[7] & 0x40) {
											// url
											bx_size -= bx_ptr[0];
											bx_ptr += bx_ptr[0];
										}

										if (i_val[7] & 0x20) {
											// ocr id

											bx_ptr += 2; bx_size -= 2;
										}

										// DecoderConfigDescriptor
										if (bx_ptr[0] == 4) {
											for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????

											bx_ptr += 2; bx_size -= 2;

											i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
											switch (i_val[6]) { // objectTypeIndication
												case 0x40: // 14496-3 audio subpart 1, subclose 1.6
													
													track_ptr[current_track]._sequence = Media::CodedSource::Create(0x0F);
														
												break;

												case 0x66: // 13818-7 main
												case 0x67: // 13818-7 low complexity
												case 0x68: // 13818-7 scalable
													// decoder specific info - adif_header(); access unit - raw_data_block()
													track_ptr[current_track]._sequence = Media::CodedSource::Create(0x11);

												break;
												case 0x69: // 13818-3 audio
													// decoder specific info - empty; access unit - frame()

												break;

												case 0x6B: // 11172-3 audio
													// decoder specific info - empty; access unit - frame()
													track_ptr[current_track]._sequence = Media::CodedSource::Create(3);
												break;

											}

											if (track_ptr[current_track]._sequence) {
												if (program_block[1] == 0)
												if (++audio.count == 1) track_ptr[current_track]._sequence->On();
												else {
													if (Media::Source::TestLNG(track_ptr[current_track].lang_id)) {
														for (unsigned int i(0);i<track_runner;i++) {
															if (track_ptr[i]._sequence)
															if (track_ptr[i]._sequence->IsAudio()) {
																track_ptr[i]._sequence->Off();

																break;
															}
														}

														track_ptr[current_track]._sequence->On();
													}

												}
											}

											i_val[7] = bx_ptr[0]; bx_ptr++; bx_size--;

											bx_ptr += 11; bx_size -= 11;

											if (bx_ptr[0] == 5) { // decoder specific info
												for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????

												bx_ptr += 1; bx_size -= 1;

												if (track_ptr[current_track]._sequence) {
													r64_val[0] = bx_ptr[0];
													r64_val[1] = -1;
													track_ptr[current_track]._sequence->DecodeUnit(++bx_ptr, r64_val[0], r64_val[1]);

													bx_ptr += r64_val[0];
													bx_size -= (r64_val[0] + 1);
												}
											}
										}
									}
								}
							
							break;
							case 'mp4v': // mpeg-4 visual

								bx_ptr += 70;
								bx_size -= 70;


								i_val[4] = bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
						
								i_val[4] -= 8;

							// type
								i_val[5] = bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;
								i_val[5] <<= 8; i_val[5] |= bx_ptr[0]; bx_ptr++; bx_size--;


								// ES_Descriptor
								if (i_val[5] == 'esds') {
									bx_ptr += 4;
									bx_size -= 4;

									if (bx_ptr[0] == 3) { // ES_DescrTag
										for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????
										bx_ptr += 2; bx_size -= 2;

										i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
										i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

										i_val[7] = bx_ptr[0]; bx_ptr++; bx_size--;
										if (i_val[7] & 0x80) {
											// stream Dependence

											bx_ptr += 2; bx_size -= 2;
										}

										if (i_val[7] & 0x40) {
											// url
											bx_size -= bx_ptr[0];
											bx_ptr += bx_ptr[0];
										}

										if (i_val[7] & 0x20) {
											// ocr id

											bx_ptr += 2; bx_size -= 2;
										}

										// DecoderConfigDescriptor
										if (bx_ptr[0] == 4) {
											for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????
											bx_ptr += 2; bx_size -= 2;

											i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
											switch (i_val[6]) { // objectTypeIndication
												case 0x20: // 14496-2 visual

												break;
												case 0x21: // H264

												break;
												case 0x22: // H264 config

												break;

												case 0x60: // 13818-2 simple
												case 0x61: // 13818-2 main
												case 0x62: // 13818-2 SNR
												case 0x63: // 13818-2 spatial
												case 0x64: // 13818-2 high
												case 0x65: // 13818-2 422

												break;

												case 0x6A: // 11172-2 visual

												break;
												case 0x6C: // 10918-1 visual (JPEG picture)

												break;
												case 0x6E: // 15444-1 visual (JPEG2000)

												break;

											}

											i_val[7] = bx_ptr[0]; bx_ptr++; bx_size--;

											bx_ptr += 11; bx_size -= 11;

											if (bx_ptr[0] == 5) { // decoder specific info
												for (;bx_ptr[1] == 0x80;bx_ptr++, bx_size--); // what a fuck is this??????????
												bx_ptr += 1; bx_size -= 1;

											}

										}

									}

								}


							break;
							case 'mp4s': // mpeg-4 system

							break;
							case 'm2ts': // mpeg-2 transport stream

							break;


							case 'dtsc': case 'dtse': case 'dtsh': case 'dtsl': case 'dtsx':
								CreateAudio(track_ptr, 0x86);
							break;

							case 'ac-3':
								CreateAudio(track_ptr, 0x81);
							break;
							case 'ec-3':
								CreateAudio(track_ptr, 0x87);
							break;
							case 'ac-4':

							break;
							case 'dts+':

							break;
							case 'dts-':

							break;
							case 'g719':

							break;
							case 'g726':

							break;
						}


						bx_ptr += i_val[1];
						bx_size -= i_val[1];

					}

					mp4_obj.Release();
				}

			}
		break;
		case 'stts': // decoding time2sample			

			if (current_track != -1) {
				i_val[0] = 0;
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}


				r64_val[0] = 0;
			
				bx_ptr += 8;
				bx_size -= 8;

				r64_val[0] = 0;
				r64_val[1] = sample_runner;
				r64_val[2] = 0;
				
				if (sample_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) sample_ptr += r64_val[1];


				for (;i_val[0] && sample_ptr;i_val[0]--) {
					i_val[1] = bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;

					i_val[2] = bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
					i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;

					r64_val[3] = i_val[2];

					r64_val[3] *= 10000;
					r64_val[3] /= time_scale;

					for (;i_val[1];i_val[1]--, r64_val[0]++) {
						sample_ptr[r64_val[0]].presentation_time = sample_ptr[r64_val[0]].decoding_time = r64_val[2];
						sample_ptr[r64_val[0]].next_e = sample_ptr[r64_val[0]].prev_e = 0; // r64_val[0] + r64_val[1] - 1;
						sample_ptr[r64_val[0]].data_size = sample_ptr[r64_val[0]].data_offset = 0;
						sample_ptr[r64_val[0]].track_idx = current_track;

						r64_val[2] += r64_val[3];

						if (++sample_runner >= sample_top) {
							time_table.Release();
							sample_ptr = 0;

							if (time_table.Expand(8192*sizeof(SampleEntry)) != -1) {
								sample_top += 8192;
								if (sample_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) sample_ptr += r64_val[1];
							}
						}
					}
				}

				if (sample_ptr) time_table.Release();

				if (TrackEntry * track_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					track_ptr[current_track].sample_count = r64_val[0];					

					mp4_obj.Release();
				}
			}

		break;
		case 'ctts': // composition time2sample
			if (current_track != -1) {
				i_val[0] = 0;
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}

				
			

				bx_ptr += 8;
				bx_size -= 8;
				
				if (TrackEntry * track_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					if (sample_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {

						sample_ptr += track_ptr[current_track].tt_offset;

						r64_val[0] = 0;

						for (;i_val[0];i_val[0]--) {
							i_val[1] = bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[1] <<= 8; i_val[1] |= bx_ptr[0]; bx_ptr++; bx_size--;

							i_val[2] = bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;


							r64_val[3] = i_val[2];

							r64_val[3] *= 10000;
							r64_val[3] /= time_scale;

							for (;i_val[1] && (r64_val[0] < track_ptr[current_track].sample_count);i_val[1]--) {
								sample_ptr[r64_val[0]++].presentation_time += r64_val[3];

							}
						}
				
						time_table.Release();
					}
					mp4_obj.Release();
				}
			}

		break;
		case 'stsc': // sample to chunk
			if (current_track != -1) {

				i_val[0] = 0; // entry_count_size
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}
								

				bx_ptr += 8;
				bx_size -= 8;

				i_val[5] = 0;


// first chunk
				i_val[6] = bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[6] <<= 8; i_val[6] |= bx_ptr[0]; bx_ptr++; bx_size--;

// samples per chunk
				i_val[3] = bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;

// samples desc idx
				i_val[4] = bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
				i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;


				if (TrackEntry * tr_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					if (SampleEntry * sm_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {
						sm_ptr += tr_ptr[current_track].tt_offset;

						for (i_val[0]--;i_val[0];i_val[0]--) {
// first chunk
							i_val[2] = bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[2] <<= 8; i_val[2] |= bx_ptr[0]; bx_ptr++; bx_size--;


							if (i_val[4] == 1) {
								i_val[7] = i_val[6] - 1;

								i_val[6] = i_val[2] - i_val[6];								

								if ((i_val[6]*i_val[3] + i_val[5]) > tr_ptr[current_track].sample_count) break;

								for (;i_val[6];i_val[6]--, i_val[7]++) {
									for (i_val[1]=0;i_val[1]<i_val[3];i_val[1]++) {
										sm_ptr[i_val[5]++].data_offset = i_val[7];
									}									
								}

								i_val[6] = i_val[2];
							}							


// samples per chunk
							i_val[3] = bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[3] <<= 8; i_val[3] |= bx_ptr[0]; bx_ptr++; bx_size--;

// samples desc idx
							i_val[4] = bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;
							i_val[4] <<= 8; i_val[4] |= bx_ptr[0]; bx_ptr++; bx_size--;

						}


						
						i_val[7] = i_val[6] - 1;

						i_val[6] = (tr_ptr[current_track].sample_count- i_val[5])/i_val[3];

						for (;i_val[6];i_val[6]--, i_val[7]++) {
							for (i_val[1]=0;i_val[1]<i_val[3];i_val[1]++) {
								sm_ptr[i_val[5]++].data_offset = i_val[7];
							}
						}

						time_table.Release();
					}
					mp4_obj.Release();
				}
			}

		break;
		case 'stco': // chunk offset
			if (current_track != -1) {
				i_val[0] = 0; // sample_count
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}

				bx_ptr += 8;
				bx_size -= 8;


				if (TrackEntry * tr_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {					
					if (SampleEntry * sm_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {
						sm_ptr += tr_ptr[current_track].tt_offset;

						for (i_val[1] = 0;i_val[1] < tr_ptr[current_track].sample_count; i_val[1]++) {
							if (sm_ptr[i_val[1]].data_offset < i_val[0]) {
								sm_ptr[i_val[1]].data_offset = reinterpret_cast<const unsigned int *>(bx_ptr)[sm_ptr[i_val[1]].data_offset];
								System::BSWAP_32(reinterpret_cast<unsigned int &>(sm_ptr[i_val[1]].data_offset));
							}
						}

						time_table.Release();
					}
					mp4_obj.Release();
				}
			}

		break;
		case 'co64': // chunk offset 64
			if (current_track != -1) {
				i_val[0] = 0; // sample_count
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}

				bx_ptr += 8;
				bx_size -= 8;


				if (TrackEntry * tr_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {					
					if (SampleEntry * sm_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {
						sm_ptr += tr_ptr[current_track].tt_offset;

						for (i_val[1] = 0;i_val[1] < tr_ptr[current_track].sample_count; i_val[1]++) {
							if (sm_ptr[i_val[1]].data_offset < i_val[0]) {
								sm_ptr[i_val[1]].data_offset = reinterpret_cast<const UI_64 *>(bx_ptr)[sm_ptr[i_val[1]].data_offset];
								System::BSWAP_64(sm_ptr[i_val[1]].data_offset);
							}
						}

						time_table.Release();
					}
					mp4_obj.Release();
				}
			}

		break;


		case 'stsz': // sample sizes
			if (current_track != -1) {

				i_val[0] = 0; // sample_size
				for (unsigned int i(0);i<4;i++) {
					i_val[0] <<= 8;
					i_val[0] |= bx_ptr[4+i];
				}

				i_val[1] = 0; // sample_count
				for (unsigned int i(0);i<4;i++) {
					i_val[1] <<= 8;
					i_val[1] |= bx_ptr[8+i];
				}

				bx_ptr += 12;
				bx_size -= 12;


				if (TrackEntry * track_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					if (i_val[1] > track_ptr[current_track].sample_count) i_val[1] = track_ptr[current_track].sample_count;
					
					if (SampleEntry * s_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {
						s_ptr += track_ptr[current_track].tt_offset;

						if (i_val[0]) { // flat
							for (i_val[2] = 0;i_val[2]<i_val[1];i_val[2]++) {
								s_ptr[i_val[2]].data_size = i_val[0];
							}

						} else {
							for (i_val[2] = 0;i_val[2]<i_val[1];i_val[2]++) {
								s_ptr[i_val[2]].data_size = bx_ptr[0]; bx_ptr++; bx_size--;
								s_ptr[i_val[2]].data_size <<= 8; s_ptr[i_val[2]].data_size |= bx_ptr[0]; bx_ptr++; bx_size--;
								s_ptr[i_val[2]].data_size <<= 8; s_ptr[i_val[2]].data_size |= bx_ptr[0]; bx_ptr++; bx_size--;
								s_ptr[i_val[2]].data_size <<= 8; s_ptr[i_val[2]].data_size |= bx_ptr[0]; bx_ptr++; bx_size--;
							}
						}

						time_table.Release();
					}
					mp4_obj.Release();
				}
			}


		break;
		case 'stz2': // compact sample sizes
			if (current_track != -1) {

				i_val[0] = bx_ptr[7]; // field size

				i_val[1] = 0; // sample_count
				for (unsigned int i(0);i<4;i++) {
					i_val[1] <<= 8;
					i_val[1] |= bx_ptr[8+i];
				}

				bx_ptr += 12;
				bx_size -= 12;


				if (TrackEntry * track_ptr = reinterpret_cast<TrackEntry *>(mp4_obj.Acquire())) {
					if (i_val[1] > track_ptr[current_track].sample_count) i_val[1] = track_ptr[current_track].sample_count;
					
					if (SampleEntry * s_ptr = reinterpret_cast<SampleEntry *>(time_table.Acquire())) {
						s_ptr += track_ptr[current_track].tt_offset;

						switch (i_val[0]) { // flat
							case 4:
								i_val[1] &= (~1);

								for (i_val[2] = 0;i_val[2]<i_val[1];i_val[2]+=2) {
									s_ptr[i_val[2]].data_size = (bx_ptr[0] >> 4);
									s_ptr[i_val[2]+1].data_size = (bx_ptr[0] & 0x0F);
								
									bx_ptr++; bx_size--;								
								}

							break;
							case 8:
								for (i_val[2] = 0;i_val[2]<i_val[1];i_val[2]++) {
									s_ptr[i_val[2]].data_size = bx_ptr[0]; bx_ptr++; bx_size--;								
								}
							break;
							case 16:
								for (i_val[2] = 0;i_val[2]<i_val[1];i_val[2]++) {
									s_ptr[i_val[2]].data_size = bx_ptr[0]; bx_ptr++; bx_size--;
									s_ptr[i_val[2]].data_size <<= 8; s_ptr[i_val[2]].data_size |= bx_ptr[0]; bx_ptr++; bx_size--;
								}

							break;
						}

						time_table.Release();
					}
					mp4_obj.Release();
				}
			}

		break;


/*
		case 'stss': // sync sample table (random access point)

		break;
		case 'stsh': // shadow sync sample table

		break;
		case 'padb': // sample padding bits

		break;
		case 'stdp': // sample degradation priority

		break;
		case 'sbgp': // sample2group

		break;
		case 'sgpd': // sample group description

		break;
		case 'subs': // sub-sample information

		break;

		
			







// IPMP control box ===================================================================
		case 'ipmc':

		break;



// moovie fragment ===================================================================================
		case 'moof':

		break;
		case 'mfhd': // movie fragment header

		break;

 // track fragment ====================================================================
		case 'traf':

		break;
		case 'tfhd': // track fragment header

		break;
		case 'trun': // case track fragment run

		break;
		case 'sdtp': // indepenedent and disposable samples

		break;

		
		
 // moovie fragment random access =====================================================================
		case 'mfra':

		break;
		case 'tfra': // track fragment random access

		break;
		case 'mfro': // moovie fragment random access offset

		break;


// meta data =========================================================================================
		case 'meta':

		break;
		case 'iloc': // item location

		break;
		case 'ipro': // item protection

		break;
		case 'sinf': // protection scheme info box

		break;
		case 'frma': // original format box

		break;
		case 'imif': // IPMP format box

		break;
		case 'schm': // scheme type box

		break;
		case 'schi': // sceme information box

		break;


		case 'iinf': // item information

		break;
		case 'pitm': // primary item reference

		break;






		case 'elst': // an edit list

		break;

		case 'pdin': // progressive download info

		break;

		case 'iods': // object descriptor box

		break;
*/

	}
		

	return 0;
}



void Video::MP4::CreateAudio(TrackEntry * track_ptr, unsigned int cid) {
	if (track_ptr[current_track]._sequence = Media::CodedSource::Create(cid)) {
		if (program_block[1] == 0)
		if (++audio.count == 1) track_ptr[current_track]._sequence->On();
		else {
			if (Media::Source::TestLNG(track_ptr[current_track].lang_id)) {
				for (unsigned int i(0);i<track_runner;i++) {
					if (track_ptr[i]._sequence)
					if (track_ptr[i]._sequence->IsAudio()) {
						track_ptr[i]._sequence->Off();

						break;
					}
				}
				track_ptr[current_track]._sequence->On();
			}
		}
	}
}

/*


												case 0xA5: // ac3
													track_ptr[current_track]._sequence = Media::CodedSource::Create(0x81);
												break;
												case 0xA6: // eac3
													track_ptr[current_track]._sequence = Media::CodedSource::Create(0x87);
												break;

												case 0xA9:
												case 0xAA:
												case 0xAB:
												case 0xAC: // dts
													track_ptr[current_track]._sequence = Media::CodedSource::Create(0x86);
												break;



*/


