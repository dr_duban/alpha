#include "Media\H262.h"



Video::H262::H262() {
	
}

Video::H262::~H262() {
	
}


UI_64 Video::H262::Initialize() {
	UI_64 result(0);

	return result;
}

UI_64 Video::H262::Finalize() {

	return 0;
}

UI_64 Video::H262::Unpack(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0);
	I_64 ref_mark(0), x_mark(time_mark);

	unsigned char temp_vals[8];

	for (;data_length>8;) {
		temp_vals[0] = data_ptr[0] | data_ptr[1];

		if (temp_vals[0] == 0) {
			for (;((temp_vals[0] |= data_ptr[2]) == 0);) {
				data_ptr++;
				data_length--;
			}

			if (temp_vals[0] == 1) { // access unit delimiter				
				data_ptr += 3; data_length -= 3;
				
				if (UI_64 ssize = GetNALSize(data_ptr, data_length)) {					
					if (ssize > 1) {
						ref_mark = time_mark;

						result = DecodeUnit(data_ptr, ssize, ref_mark);
						if (result & RESULT_DECODE_COMPLETE) {
							x_mark = ref_mark;
							if (decode_skip_count < 0) break; // -1
						}
					}

					data_ptr += ssize;
					data_length -= ssize;									

					if (result > 0x01000000) break;
					
				} else {
					// error					
				}

			} else {
				// error
				result = 'aud1';
				result <<= 32;
				break;
			}


		} else {
			result = 'aud0';
			result <<= 32;
			// error
			break;
		}
	}

	if (x_mark != time_mark) {
		time_mark = x_mark;
		result |= RESULT_DECODE_COMPLETE;
	}

	return result;

	return result;
}

UI_64 Video::H262::DecodeUnit(const unsigned char * u_ptr, UI_64 u_length, I_64 & time_mark) {
	UI_64 result(-3);
	unsigned char temp_vals[8];

	unsigned char start_code(u_ptr[0]);

	result <<= 32;


	decoder_cfg.SetSource(u_ptr+1, u_length-1);

	decoder_cfg.flag_set = 0;

	switch (start_code)	{
		case 0x00: // picture start

		break;
		case 0xB0: // reserved

		break;
		case 0xB1: // reserved

		break;
		case 0xB2: // user data

		break;
		case 0xB3: // sequence header

		break;
		case 0xB4: // sequence error

		break;
		case 0xB5: // extension

		break;
		case 0xB6: // reserved

		break;
		case 0xB7: // sequence end

		break;
		case 0xB8: // group

		break;
		default:
			if (start_code > 0xB8) {
				// system

			} else {
				// slice

			}

	}


	return result;
}

void Video::H262::JumpOn() {
	
}

UI_64 Video::H262::FilterFrame(unsigned int *) {
	UI_64 result(0);


	return result;
}

void Video::H262::FilterPreview(unsigned int *, OpenGL::DecodeRecord &) {
	

}


