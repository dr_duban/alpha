/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Media\CodedSource.h"




namespace Video {

	class H262: public Media::Codec {
		protected:
			class DecoderState {


			} decoder_cfg;

			int decode_skip_count;
			
			virtual void SetRenderSkip(__int64);
		public:
			static const UI_64 frame_buf_tid = 961749247;
			static const unsigned short zz_4x4[2][16], zz_8x8[2][64];
			
			static UI_64 Initialize();
			static UI_64 Finalize();

			H262();
			virtual ~H262();

			virtual unsigned int GetCT();
			virtual UI_64 SetCT(UI_64);

			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 FilterFrame(unsigned int *);
			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &);
			
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
	//		virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);


	};


}

