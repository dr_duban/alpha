/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include <Windows.h>

#include "System\Memory.h"

#include "Data\Stream.h"

#include "CodedSource.h"



namespace Video {

	class MKV: public Media::Program { // multiple coded sources
		protected:
			static const char codec_id_strings[][32];
			static const unsigned int stream_type_map[256];

			static DWORD __stdcall ReceiverLoop(void *);	

			static Memory::Allocator * mkv_pool;
							

			struct EBMLHeader {
				unsigned int start_code;
				unsigned int version, read_version, max_id_length, max_elsize;
				unsigned __int64 doc_type;
				unsigned int doc_type_version, doc_type_read_version;
			} ebml;

			struct TrackHeader {
				Media::CodedSource * _sequence;
				
				I_64 default_duration; // , local_time, sync_time, sync_dist; //default_field_duration, track_overlay, codec_delay, seek_prerol;
				unsigned int id, codec_id, attachment_id, lang_id;

				char codec_string[32];
				
			};

			struct Attachment {
				SFSco::Object data;

				char file_name[256];
				char mime_type[256];
				
			};

			struct BlockHeader {
				UI_64 time_code, time_offset, refeernce_time_stamp, reference_duration;

				unsigned int track_id;				

				union {
					struct {
						bool discardable: 1;	

						unsigned char lacing: 2;
						
						bool invisible: 1;						

						bool : 1;
						bool : 1;
						bool : 1;

						bool key_frame: 1;
					
					};

					unsigned int _oval;
				} flags;

			} current_block;
						
			SFSco::Object mkv_obj, att_obj, temp_buf; // , code_buf;
			HANDLE reciever_thread;
				
			Data::Stream source;			
			UI_64 action_size, temp_offset, temp_val, time_scale, block_counter;
			unsigned int action_code, parse_step, track_runner, track_top, att_runner, att_top, terminator, avc_size, no_video, temp_vid, fuck_mode_on;
			int la_size[256];

			UI_64 GetElBytes(const unsigned char *, void *, UI_64, UI_64);
			UI_64 ParseBlock(const unsigned char *, UI_64);
			UI_64 ParseAction(const unsigned char *, UI_64);
			
			void SetAudioVolume(float);

			void NVFlush(TrackHeader *);
			void NVReset();

			MKV(unsigned int preview_flag);
			virtual ~MKV();

		//	virtual void GetSpan();
			virtual Media::Source * GetVSource(UI_64 &);
			virtual Media::Source * GetASource(UI_64 &);

//			virtual UI_64 PreviewShot(OpenGL::DecodeRecord &, SFSco::Object & pi_obj);

		public:
			static const UI_64 _tid = 961749097;

			static UI_64 Initialize();
			static UI_64 Finalize();

			static Program * SetSource(const Data::Stream &, unsigned int previw_flag);
			static bool IsMKV(Data::Stream &);

			virtual UI_64 Pack(const void *, UI_64);

						
			virtual unsigned int EnableAudioSource(unsigned int, const OpenGL::Element &);
			virtual unsigned int DisableAudioSource(unsigned int);
			virtual unsigned int IsAudioActive(unsigned int);
			virtual unsigned int GetAudioLNG(unsigned int);
	};


}

