/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include <Windows.h>

#include "CoreLibrary\SFSCore.h"
#include "OpenGL\OpenGL.h"

#define MAX_PROGRAM_COUNT 32

#define SOURCE_TYPE_VIDEO		0x00010000
#define SOURCE_TYPE_AUDIO		0x00020000
#define SOURCE_TYPE_SUBS		0x00040000

#define SOURCE_NO_RENDER		0x00100000

#define FRAME_RENDER			0x00000002
#define FRAME_BUFFER_UPDATE		0x00000004
#define FRAME_ACTION_MASK		0x000000FF

#define SOURCE_START			0x10000000
#define SOURCE_STOP				0x20000000
#define SOURCE_SERVICE			0x40000000
#define SOURCE_TERMINATE		0x80000000

#define SOURCE_CONTROL_OP_MASK	0xF0000000
#define SOURCE_CONTROLINFO_MASK	0x0F000000


#define RESULT_NO_RENDER		512
#define	RESULT_DECODE_COMPLETE	256
#define RESULT_STREAM_BREAK		128
#define RESULT_CODEC_SPECIAL	64
#define RESULT_BUFFER_SET		32
#define RESULT_HEADER_COMPLETE	16

#define	FILE_BUFFER_SIZE	0x00100000



#define AUDIO_SOURCE_EXCLUSIVE		0x00000001
#define VIDEO_SOURCE_EXCLUSIVE		0x00000002


namespace Media {
	
	class Source {
		friend class Program;
		friend class Drain;
		public:
			struct Descriptor {
				wchar_t _name[MAX_PATH];
				wchar_t _description[MAX_PATH];
				wchar_t _devPath[MAX_PATH];

				unsigned int width, height, c_width, c_height;
				unsigned int source_frame, _state_flags, name_length, path_length;
				unsigned int pixel_count, precision, lang_id, ui_reserved;

				Descriptor();

				void SetStrings(const wchar_t * nm, const wchar_t * pt, const wchar_t * ds);

			};
			
		private:
			static const UI_64 render_q_tid = 961749121;
			static const unsigned int decoding_queue_capacity = 32;
			static const unsigned int render_queue_size = 128;

			static const wchar_t ct_strings[][8];

			static SFSco::Object source_list, render_queue, picture_buffer;
			static unsigned int source_runner, source_top, render_runner, render_top;
						
			unsigned int _list_id;

			static UI_64 PicBufferSize(UI_64);
		protected:
			
			Descriptor _descriptor;
			unsigned int render_frame, code_frame, render_id, drain_id, on_count;
			__declspec(align(16)) float test_vector[4];

			bool FrameAvailable();

			void Reset();
			
			Source();
			virtual ~Source();

			virtual UI_64 Service();

			virtual unsigned int GetCT();
			virtual UI_64 SetCT(UI_64);

			virtual unsigned int GetSatCount(unsigned int *) { return 0; };
			virtual const wchar_t * GetSatStr(unsigned int) { return 0; };
			virtual unsigned int GetSatInfoCount() { return 0; };
			virtual void GetSatInfo(unsigned int, OpenGL::StringsBoard::InfoLine &) {};

			virtual UI_64 FilterFrame(unsigned int *) { return 0; };
			virtual UI_64 FilterSatFrame(float * out_ptr, OpenGL::DecodeRecord &) { return 0; };
			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &) {};

			virtual UI_64 Pack(const void *, UI_64);
			virtual UI_64 Test();
						
						
			static Source * GetSource(unsigned int);

			virtual void AudioViewOn() {};
			virtual void AudioViewOff() {};
						
			UI_64 SetSatEntries(const OpenGL::Element &);

			static UI_64 AddDREntry(OpenGL::DecodeRecord &);
			static UI_64 UpdateDREntry(OpenGL::DecodeRecord &);
			static UI_64 RemoveDREntry(UI_64);
			
			static UI_64 TestFunc(OpenGL::Element &);

		public:
			
			struct LLEntry {
				unsigned int id;
				wchar_t name[6];
			};

			static const LLEntry lang_list[];
			
			static const wchar_t * GetLNGString(unsigned int, unsigned int);
			static const wchar_t * GetLNGIDString(unsigned int);
			static unsigned int GetLNGIDCount();

			static const wchar_t * GetCTString(unsigned int);
			static unsigned int GetCTCount();
			static unsigned int TestLNG(unsigned int);

			static unsigned int GetName(unsigned int s_id, wchar_t *);

			static UI_64 Initialize(SFSco::IOpenGL **);
			static UI_64 Finalize();

			static void NextUnit(unsigned int);

			static UI_64 TexUpdate(OpenGL::Core *);

			UI_64 Refresh(OpenGL::DecodeRecord &);

			unsigned int GetID();
					
			unsigned int IsVideo();
			unsigned int IsAudio();
			unsigned int IsSubs();

			virtual unsigned int On();
			virtual unsigned int Off();
			unsigned int TestFlag(unsigned int);
			unsigned int GetLNGID();
			
			void InitDRB(OpenGL::DecodeRecord &);

			void ResetSatRecord(const OpenGL::Element &);
			void ResetSat();			

	};

	class Drain {
		friend class Source;
		protected:
			struct DrainCfg {
				HANDLE _thread, t_block;
				Source * container;
				unsigned int out_stream[4];
				unsigned int terminator;
				unsigned int container_type, _reserved;
				
				unsigned int program_count;

				struct Program {
					unsigned int video_source;

					unsigned int audio_count;

					unsigned int audio_source[31];

					
				} program_set[MAX_PROGRAM_COUNT];


				void Blank() {
					_thread = 0; t_block = 0;
					container = 0;
					out_stream[0] = out_stream[1] = out_stream[2] = out_stream[3] = -1;

					container_type = 0;
					terminator = 0;
					_reserved = 0;
					program_count = 0;

					for (unsigned int i(0);i<MAX_PROGRAM_COUNT;i++) {
						program_set[i].video_source = -1;

						program_set[i].audio_count = 0;

						for (unsigned int j(0);j<31;j++) {
							program_set[i].audio_source[j] = -1;

						}						
					}

				};

				DrainCfg() {
					Blank();
				};

				DrainCfg(const DrainCfg & ref_d) {
					*this = ref_d;
				};

				DrainCfg & operator=(const DrainCfg & ref_d) {
					_thread = ref_d._thread;
					t_block = ref_d.t_block;
					container = ref_d.container;
					
					out_stream[0] = ref_d.out_stream[0];
					out_stream[1] = ref_d.out_stream[1];
					out_stream[2] = ref_d.out_stream[2];
					out_stream[3] = ref_d.out_stream[3];

					container_type = ref_d.container_type;
					terminator = ref_d.terminator;
					_reserved = ref_d._reserved;
					program_count = ref_d.program_count;

					SFSco::MemoryCopy(program_set, ref_d.program_set, MAX_PROGRAM_COUNT*sizeof(Program));

					return *this;
				};

				operator bool() {
					return (terminator>0);
				};

				void Destroy() {
					if (t_block) {
						if (_thread) {
							terminator = 0;
							SetEvent(t_block);
							WaitForSingleObject(_thread, INFINITE);
							CloseHandle(_thread);
						}

						CloseHandle(t_block);
					}

					if (container) delete container;
					// for program count, remove sources from source list

					program_count = 0;
				};
			};

			struct SourceCfg {
				unsigned int _id, _status;
				unsigned int drain_id, program_id;

				unsigned int codec_id;
				unsigned int q_val;

//				Codec * codec;
				SFSco::Object frame_buf;

				UI_64 s_offset;
				UI_64 f_switch;

				UI_64 _reserved[32];

				void Blank() {
					_id = -1; _status = 0;

					drain_id = program_id = -1;

					codec_id = 0; q_val = 0;
	//				codec = 0;

					s_offset = 0;

					f_switch = 0;
					frame_buf.Blank();

					for (unsigned int i(0);i<32;i++) _reserved[i] = 0;			

				};

				SourceCfg() {
					Blank();
				};

				SourceCfg(const SourceCfg & ref_s) {
					*this = ref_s;
				};

				SourceCfg & operator=(const SourceCfg & ref_s) {
					_id = ref_s._id; _status = ref_s._status;

					drain_id = ref_s.drain_id; program_id = ref_s.program_id;

					codec_id = ref_s.codec_id;
					q_val = ref_s.q_val;

					s_offset = ref_s.s_offset;

					f_switch = ref_s.f_switch;
					frame_buf = ref_s.frame_buf;

					for (unsigned int i(0);i<32;i++) _reserved[i] = ref_s._reserved[i];

					return *this;
				};

				operator bool() {
					return (_id != -1);
				};

				void Destroy() {
					// remove from drain list

					
//					if (codec) delete codec;
					_id = -1;
				};
			};

			static HANDLE encoder_thread, wait_obj;
			
			static unsigned int _service;
			static unsigned int terminator;
			
			static DWORD __stdcall EncoderLoop(void *);
			static DWORD __stdcall DrainLoop(void *);

			static SFSco::List<DrainCfg> drain_list;
			static SFSco::List<SourceCfg> source_list;
			
						

		public:

		// source attach
			static unsigned int AttachSource(unsigned int drain_id, SFSco::IOpenGL::ScreenSet &, unsigned int program_id);
			static unsigned int DetachSource(unsigned int source_id);

			static unsigned int Create(unsigned int container_type);


			static UI_64 BindStream(unsigned int drain_id);
			static UI_64 UnbindStream(unsigned int drain_id);

			static UI_64 Initialize();
			static UI_64 Finalize();


	};


	class Program {
		friend class Source;
		private:
			static const UI_64 video_de_queue_tid = 961749157;
			static I_64 sp_frequency;
			static unsigned int terminator; // , current_de_queue, dq_runner[2], dq_top[2];
			static HANDLE decoder_thread, prev_thread, ini_block;
//			static SFSco::Object decoding_queue[2];
			static Control::SimpleQueue prev_queue, decoding_queue;
			
			static DWORD __stdcall DecoderLoop(void *);
			static DWORD __stdcall PreviewLoop(void *);
			static DWORD __stdcall EncoderLoop(void *);

		protected:
			static SFSco::IOpenGL * main_ogl;
			HANDLE program_block[2];			
			I_64 start_mark, stop_mark, search_mark, current_time, sync_start;

			I_64 pts[2];

			struct SourceRec {
				unsigned int count;
				unsigned int flags;
				Source * src_list[32];

				SourceRec() : count(0), flags(0) { for (unsigned int i(0);i<32;i++) src_list[i] = 0;};
			} audio, video;
			
			unsigned int autotrigger[2];
			OpenGL::DecodeRecord pending_req, ref_req;
			unsigned int search_mode, search_idx;


			UI_64 EnableSource(OpenGL::DecodeRecord &);
		
			virtual UI_64 AddVSource(Source *);			
			virtual UI_64 GetSpan();

			virtual UI_64 PreviewShot(OpenGL::DecodeRecord &, SFSco::Object & pi_obj);


//			void SetTSO(unsigned int);
//			void ResetTSO();

			void ResetPTS();

			void Start(unsigned int = 1);
			void Stop();
			void Jump(I_64);
			void Volume(I_64);

//			virtual unsigned int GetSourceCount();

			virtual Source * GetVSource(UI_64 &);
			virtual Source * GetASource(UI_64 &);

			Source * SelectVSource(UI_64 &);

			Program(unsigned int preview_flag);
			virtual ~Program();

//			virtual void NVFlush(void *);
//			virtual void NVReset();
			
			I_64 SetPTS(I_64);
			void SetSTP();
			void Terminate();

			static Program * FromFile(SFSco::Object &, unsigned int preview_flag);
			static Program * FromFile(OpenGL::Element::Header *, OpenGL::DecodeRecord &);

			static UI_64 Initialize();
			static UI_64 Finalize();

			
		public:
			static const UI_64 small_buf_tid = 961748987;
			static const UI_64 pic_buf_tid = 961748993;
						
			static UI_64 Decode(OpenGL::DecodeRecord &);

			unsigned int GetCurrentCT(UI_64 = -1);
			UI_64 SetCurrentCT(UI_64, UI_64 = -1);

			unsigned int GetAudioSourceCount();
			unsigned int GetVideoSourceCount();

			void AudioViewOn();
			void AudioViewOff();


			virtual unsigned int EnableAudioSource(unsigned int, const OpenGL::Element &);
			virtual unsigned int DisableAudioSource(unsigned int);
			virtual unsigned int IsAudioActive(unsigned int);
			virtual unsigned int GetAudioLNG(unsigned int);


	};

}




