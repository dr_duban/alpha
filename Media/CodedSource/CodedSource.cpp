/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\CodedSource.h"

#include "Media\H264.h"

#include "Audio\AAC.h"
#include "Audio\AC3.h"
#include "Audio\MP3.h"
#include "Audio\DTS.h"


#include "System\SysUtils.h"



Media::CodedSource::CodedSource() : _codec(0), terminator(1) {
	
}


Media::CodedSource::~CodedSource() {
	RemoveDREntry((UI_64)this);

	if (_codec) delete _codec;	
}

UI_64 Media::CodedSource::Service() {

	return 0;
}


UI_64 Media::CodedSource::Test() {
	UI_64 result(-1);
	if (_codec) result = _codec->UnitTest(test_vector);

	return result;
}



void Media::CodedSource::JumpOn() {
	if (_codec) _codec->JumpOn();

}

UI_64 Media::CodedSource::Decode(const unsigned char * src_ptr, UI_64 src_size, I_64 & time_mark) {
	UI_64 result(RESULT_NO_RENDER);

	if ((_descriptor._state_flags & SOURCE_NO_RENDER) == 0) {
		result = -4;
		result <<= 32;
		if (_codec) result = _codec->Unpack(src_ptr, src_size, time_mark);
	}

	return result;
}

UI_64 Media::CodedSource::DecodeUnit(const unsigned char * src_ptr, UI_64 src_size, I_64 & time_mark) {
	UI_64 result(RESULT_NO_RENDER);

	if (((_descriptor._state_flags & SOURCE_NO_RENDER) == 0) || (time_mark < 0)) {
		result = -4;
		result <<= 32;

		if (_codec) result = _codec->DecodeUnit(src_ptr, src_size, time_mark);
	}

	return result;
}

UI_64 Media::CodedSource::FilterFrame(unsigned int * pipo) {
	if (_codec) return _codec->FilterFrame(pipo);
	else return -44;	
}

UI_64 Media::CodedSource::FilterSatFrame(float * o_ptr, OpenGL::DecodeRecord & d_rec) {
	if (_codec) return _codec->FilterSatFrame(o_ptr, d_rec);
	else return 0;	
}


unsigned int Media::CodedSource::GetSatCount(unsigned int * s_desc) {
	if (_codec) return _codec->GetSatCount(s_desc);
	else return 0;
}

unsigned int Media::CodedSource::GetSatInfoCount() {
	if (_codec) return _codec->GetSatInfoCount();
	else return 0;
}

void Media::CodedSource::GetSatInfo(unsigned int iid, OpenGL::StringsBoard::InfoLine & i_line) {
	if (_codec) _codec->GetSatInfo(iid, i_line);
}

const wchar_t * Media::CodedSource::GetSatStr(unsigned int c_id) {
	static const wchar_t short_cha_names[16*4] = {
		2, L'F', L'L', 0,
		2, L'F', L'R', 0,
		2, L'F', L'C', 0,

		3, L'L', L'F', L'E',

		2, L'B', L'L', 0,
		2, L'B', L'R', 0,
		2, L'B', L'C', 0,

		2, L'S', L'L', 0,
		2, L'S', L'R', 0
	};


	switch (c_id) {
		case 0x0001:
			return short_cha_names;
		
		case 0x0002:
			return short_cha_names + 4;
		
		case 0x0004:
			return short_cha_names + 8;
		
		case 0x0008:
			return short_cha_names + 12;
		
		case 0x0010:
			return short_cha_names + 16;
		
		case 0x0020:
			return short_cha_names + 20;
		
		case 0x0100:
			return short_cha_names + 24;
		
		case 0x0200:
			return short_cha_names + 28;
		
		case 0x0400:
			return short_cha_names + 32;
		
		default:
			return 0;


	}
}


void Media::CodedSource::FilterPreview(unsigned int * pipo, OpenGL::DecodeRecord & de_rec) {
	if (_codec) _codec->FilterPreview(pipo, de_rec);	
}


void Media::CodedSource::SetRenderSkip(__int64 s_f) {
	if (_codec)	_codec->SetRenderSkip(s_f);
}

void Media::CodedSource::Flush(I_64 cm, I_64 lm) {
	if (_codec)	_codec->Flush(cm, lm);
}

void Media::CodedSource::Reset() {
	if (_codec)	_codec->Reset();
}

void Media::CodedSource::SetVolume(const float * a_val) {
	if (_codec)	_codec->SetVolume(reinterpret_cast<const unsigned int *>(a_val));
}

Media::CodedSource * Media::CodedSource::Create(unsigned int ctype) {

	CodedSource * result(0);

	if (result = new CodedSource()) {
		result->_descriptor._state_flags |= SOURCE_NO_RENDER;

		switch (ctype) {
			case 0x01: // 11172-2 video

			break;
			case 0x02: // H.262 video or 11172-2 constrained parameter video stream

			break;
			case 0x03: // 11172-3 audio
				if (result->_codec = new Audio::MP3()) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}

			break;
			case 0x04: // 13818-3 audio

			break;
			case 0x05: // H.222.0 private sections

			break;
			case 0x06: // H.222.0 provate data

			break;
			case 0x07: // 13522 MHEG

			break;
			case 0x08: // H.222.0 annex A DSM-CC

			break;
			case 0x09: // H.222.1
				
			break;
			case 0x0A: // 13818-6 type A

			break;
			case 0x0B: // 13818-6 type B

			break;
			case 0x0C: // 13818-6 type C

			break;
			case 0x0D: // 13818-6 type D

			break;
			case 0x0E: // 13818-1 auxiliary

			break;
			case 0x0F: // 13818-7 audio with ADTS (AAC)			
				if (result->_codec = new Audio::AAC(0)) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}
			break;
			case 0x10: // 14496-2 visual

			break;
			case 0x11: // 14496-3 audio with LATM (AAC)
				if (result->_codec = new Audio::AAC(1)) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}

			break;
			case 0x12: // 14496-1 SL or FlexMux in PES

			break;
			case 0x13: // 14496-1 SL or FlexMux in 14496_sections

			break;
			case 0x14: // 13818-6 sync dowload protocol

			break;
			case 0x15: // metadata in PES

			break;
			case 0x16: // meatadata in metadata_sections

			break;
			case 0x17: // metadata 13818-6 Data Carousel

			break;
			case 0x18: // meatadata 13818-6 Object Carousel

			break;
			case 0x19: // meatadata 13818-6 sync dowload

			break;
			case 0x1A: // IPMP

			break;
			case 0x1B: // H.264
				if (result->_codec = new Video::H264()) {
					result->_descriptor._state_flags |= SOURCE_TYPE_VIDEO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}
			break;

			case 0x24: // H.265
				
			break;
			case 0x42: // chinese video standard (AVS)

			break;
			case 0x81: // AC3
				if (result->_codec = new Audio::AC3(0)) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}

			break;
			case 0x83: // LPCM


			break;
			case 0x82: case 0x86: case 0x8A:// DTS
				if (result->_codec = new Audio::DTS()) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}
			break;
			case 0x87: // E-AC3
				if (result->_codec = new Audio::AC3(1)) {
					result->_descriptor._state_flags |= SOURCE_TYPE_AUDIO; // | SOURCE_NO_RENDER;
					result->_codec->_src_desc = &result->_descriptor;
				}

			break;

		}

		
				
	}

	

	if (result->_codec) {

/*
		if (result->_descriptor._state_flags & SOURCE_TYPE_AUDIO) {
			


			} else {
				delete result;
				result = 0;

			}

		}
*/
	} else {
		delete result;
		result = 0;
	}

	return result;

}

void Media::CodedSource::SetComplete(unsigned int cf) {
	if (_codec) _codec->SetComplete(cf);
}

unsigned int Media::CodedSource::GetCT() {
	unsigned int result(0);

	if (_codec) {
		result = _codec->GetCT();
	}

	return result;
}

UI_64 Media::CodedSource::SetCT(UI_64 xv) {
	UI_64 result(-1);
	
	if (_codec) {
		if (xv > 9) xv = 1;
		result = _codec->SetCT(xv);
	}

	return result;
}

void Media::CodedSource::SetSpecialDescriptor(UI_64 tm) {

	_descriptor.height = OpenGL::Core::special_x_size;
	_descriptor.c_height = OpenGL::Core::special_x_size;

	_descriptor.width = (OpenGL::Core::special_x_size << 4)/10;

	_descriptor.width += 15;
	_descriptor.width &= ~15;
	_descriptor.c_width = _descriptor.width;

	_descriptor.path_length = -1;
	reinterpret_cast<UI_64 *>(_descriptor._description)[0] = tm/10000;

}

void Media::CodedSource::AudioViewOn() {
	if (_codec) _codec->AudioViewOn();
}

void Media::CodedSource::AudioViewOff() {
	if (_codec) _codec->AudioViewOff();
}


void Media::CodedSource::InitViewBuf() {
	if (_codec) _codec->InitViewBuf();
}


/*
				switch (str_hdr->_type) {
					case 0x01: // 11172-2 video

					break;
					case 0x02: // H.262 video or 11172-2 constrained parameter video stream

					break;
					case 0x03: // 11172-3 audio

					break;
					case 0x04: // 13818-3 audio

					break;
					case 0x05: // H.222.0 private sections

					break;
					case 0x06: // H.222.0 provate data

					break;
					case 0x07: // 13522 MHEG

					break;
					case 0x08: // H.222.0 annex A DSM-CC

					break;
					case 0x09: // H.222.1

					break;
					case 0x0A: // 13818-6 type A

					break;
					case 0x0B: // 13818-6 type B

					break;
					case 0x0C: // 13818-6 type C

					break;
					case 0x0D: // 13818-6 type D

					break;
					case 0x0E: // 13818-1 auxiliary

					break;
					case 0x0F: // 13818-7 audio with ADTS

					break;
					case 0x10: // 14496-2 visual

					break;
					case 0x11: // 14496-3 audio with LATM

					break;
					case 0x12: // 14496-1 SL or FlexMux in PES

					break;
					case 0x13: // 14496-1 SL or FlexMux in 14496_sections

					break;
					case 0x14: // 13818-6 sync dowload protocol

					break;
					case 0x15: // metadata in PES

					break;
					case 0x16: // meatadata in metadata_sections

					break;
					case 0x17: // metadata 13818-6 Data Carousel

					break;
					case 0x18: // meatadata 13818-6 Object Carousel

					break;
					case 0x19: // meatadata 13818-6 sync dowload

					break;
					case 0x1A: // IPMP

					break;
					case 0x1B: // H.264
						str_hdr->_codec->Unpack();
					break;

					case 0x24: // H.265

					break;
					case 0x42: // chinese video standard

					break;
				}


*/


// lang codes


