
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE





.CODE

ALIGN 16
Codec_GetNALSize	PROC
	
	MOV r8, 1
	XOR r9, r9

	MOV rax, rcx
	DEC rax
	INC rdx

align 16
	search_start_marker:
		INC rax

		DEC rdx
		JZ done

			SHL r8d, 8
			MOV r8b, [rax]

			MOV r9d, r8d
			AND r9d, 0FFFFFFh
			XOR r9d, 1
			JNZ search_start_marker

			XOR r8d, 1
			JNZ done
				DEC rax
align 16
	done:
		AND rdx, rdx
		JZ no_sub
			SUB rax, 2
		no_sub:
		SUB	rax, rcx

	RET
Codec_GetNALSize	ENDP




END