/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Media\Source.h"

#define MAX_SOURCE_COUNT	32

class FrameProcessor;

namespace Video {

	class CaptureSource: public Media::Source {
		friend class FrameProcessor;
		private:
			static unsigned int source_runner;
						
			UI_64 _reserved[768];

			UI_64 frame_buf_size;
			unsigned int out_pin, in_pin;
			SFSco::Object frame_buf[4];


			void GetSourceMediaType(GUID &);

			bool FrameBufferReady();
		protected:
			CaptureSource();
			virtual ~CaptureSource();

			virtual unsigned int On();
			virtual unsigned int Off();

			virtual UI_64 FilterFrame(unsigned int *);

		public:
			static UI_64 Initialize();
			
			static UI_64 SelectMediaFormat(unsigned int, unsigned int);
			static void SetSourceQuality(unsigned int, unsigned int);

			static unsigned int GetSourceCount();
			static const wchar_t * GetName(unsigned int);

			static const wchar_t * GetCurrentFormatString(unsigned int);
			static const wchar_t * GetMediaFormatString(unsigned int, unsigned int);
			static unsigned int GetMediaFormatCount(unsigned int);
			static unsigned int GetCurrentFormatID(unsigned int);

			static Source * TakeAShot(unsigned int, SFSco::Object &);

			

//			static unsigned int FormatCurrentSizeString(unsigned int, wchar_t *);
//			static unsigned int FormatCurrentColorString(unsigned int, wchar_t *);
//		static void PrintFormatsOut();
			

	};




}

