/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include <Windows.h>

#include "System\Memory.h"

#include "Data\Stream.h"
#include "Media\Source.h"

#include "CodedSource.h"


namespace Video {

	class MP4: public Media::Program {
		public:
			struct SampleEntry {
				I_64 decoding_time, presentation_time;

				UI_64 data_offset, data_size;
				UI_64 prev_e, next_e;
				unsigned int track_idx, ui_reserved;
				UI_64 ui64_reserved;
			};

		protected:

			static DWORD __stdcall ReceiverLoop(void *);	
			
			static Memory::Allocator * mp4_pool;
			
			
			I_64 box_size, temp_offset, time_scale, sample_top, sample_runner, start_idx, stop_idx, current_idx, jump_val, frame_counter;
			unsigned int box_type, parse_step, op_size, track_runner, current_track, track_top, avc_size, no_video, temp_vid;
				
			struct TrackEntry {
				Media::CodedSource * _sequence;

				I_64 duration, sample_count, tt_offset; //, local_time, sync_time, sync_dist;
				unsigned int id, _type, lang_id, ref_track;
				
			};


						
			SFSco::Object mp4_obj, time_table, temp_buf;
			HANDLE reciever_thread;
				
			Data::Stream source;

			unsigned int terminator;

			UI_64 ParseBlock(const unsigned char *&, UI_64 &);
			UI_64 ParseBox(const unsigned char *, UI_64);

			void SetAudioVolume(float);

			void CreateAudio(TrackEntry *, unsigned int);

			void NVFlush(TrackEntry *);
			void NVReset();

			MP4(unsigned int p_flag);
			virtual ~MP4();



			virtual Media::Source * GetVSource(UI_64 &);
			virtual Media::Source * GetASource(UI_64 &);
//			virtual UI_64 PreviewShot(OpenGL::DecodeRecord &, SFSco::Object & pi_obj);

		public:
			static const UI_64 _tid = 961749043;
			static const UI_64 sample_table_tid = 32416190071;

			static UI_64 Initialize();
			static UI_64 Finalize();

			static Program * SetSource(const Data::Stream &, unsigned int p_flag);
			static bool IsMP4(Data::Stream &);

			virtual UI_64 Pack(const void *, UI_64);

			unsigned int GetAudioSourceCount();
			unsigned int GetVideoSourceCount();

			virtual unsigned int EnableAudioSource(unsigned int, OpenGL::Element &);
			virtual unsigned int DisableAudioSource(unsigned int);
			virtual unsigned int IsAudioActive(unsigned int);
			virtual unsigned int GetAudioLNG(unsigned int);


	};

	extern "C" UI_64 MP4_AdjustOffsets(MP4::SampleEntry *, UI_64, UI_64);

}

