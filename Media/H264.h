/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Media\CodedSource.h"

#define		FLAG_SECOND_FIELD	0x00000001

#define		FLAG_FIELD_PAIR		0x00000004
#define		FLAG_AFRO_FRAME		0x00000008


#define		FLAG_AFRO_REF		0x00008000
#define		FLAG_FIELD_REF		0x00004000
#define		FLAG_FRAME_REF		0x00002000
#define		FLAG_PAIR_REF		0x00001000

#define		MASK_LONG_IDX		0x00FF0000
#define		FLAG_BUFFER_PIC		0x00000100
#define		FLAG_ACTIVE_REF		0x00000200
#define		FLAG_LONG_REF		0x00000400
#define		FLAG_OUT_REF		0x00000800
#define		FLAG_REMOVED_REF	0x00008000

#define		FLAG_SET_LONG_REF	0x01000000
#define		FLAG_REMOVE_REF		0x80000000



#define		FLAG_PARTITION_A	0x00000020
#define		FLAG_PARTITION_B	0x00000040
#define		FLAG_PARTITION_C	0x00000080
#define		MASK_PARTITIONS		0x000000E0



#define		FLAG_MB_INVALID		0x80000000
#define		FLAG_MB_TYPE_I		0x00000020
#define		FLAG_MB_TYPE_B		0x00000040
#define		FLAG_MB_TYPE_P		0x00000080
#define		FLAG_MB_TYPE_S		0x00000100
#define		FLAG_MB_TYPE_0		0x00000200
#define		FLAG_MB_TYPE_I_PCM	0x00000400
#define		FLAG_MB_TYPE_SKIP	0x00000800


#define		MASK_MB_TYPE_ID		0x0000001F

#define		FLAG_MB_8x8_SIZE	0x00001000
#define		FLAG_MB_T_BYPASS	0x00002000
#define		FLAG_MB_DEBLOCK		0x00004000
#define		FLAG_MB_AVAILABLE	0x00008000

#define		FLAG_MB_LUMA_0		0x00010000
#define		FLAG_MB_LUMA_1		0x00020000
#define		FLAG_MB_LUMA_2		0x00040000
#define		FLAG_MB_LUMA_3		0x00080000
#define		MASK_MB_LUMA		0x000F0000

#define		FLAG_MB_CHROMA_DC	0x00100000
#define		FLAG_MB_CHROMA_AC	0x00200000


#define		FLAG_MB_PART_V		0x10000000
#define		FLAG_MB_PART_H		0x20000000



#define		MASK_SUB_TYPE		0x000F

#define		VAL_SUB_PMODE_L0	0x0100
#define		VAL_SUB_PMODE_L1	0x0200
#define		VAL_SUB_PMODE_BI	0x0300

#define		FLAG_SUB_PART_V		0x1000
#define		FLAG_SUB_PART_H		0x2000

#define		MAX_REF_IDX			128

namespace Video {
	extern "C" {
		unsigned int H264_DS_GolombUnsigned(void *);		
		unsigned int H264_DS_GetNumber(void *, unsigned int);
		unsigned char H264_DS_GetZeroRun(void *, unsigned char);

		void H264_CABAC_InitCtxVars(void *, short, unsigned char);


		void H264_PackPreview(unsigned int *, const unsigned int *, const unsigned int *, const OpenGL::DecodeRecord &);

		void H264_Convert2Picture_0(unsigned int *, const short *);
		void H264_Convert2Picture_1(unsigned int *, const short *);
		void H264_Convert2Picture_2(unsigned int *, const short *);

		void H264_Deblock_0(short *);
		void H264_Deblock_1(short *);
		void H264_Deblock_2(short *);

		void H264_NormalizeScalingLists(void *, void *);

		void H264_DCTransform0(short *, unsigned int);
		void H264_DCTransform1(short *, unsigned int);
		void H264_DCTransformChroma1(short *, unsigned int);
		void H264_DCTransformChroma2(short *, unsigned int);

		void H264_SamplesI0Transform4x4(short *, unsigned int, short[][16], unsigned int);
		void H264_SamplesI1Transform4x4(short *, unsigned int, short[][16], unsigned int);
		void H264_SamplesI0Transform8x8(short *, unsigned int, short[][64], unsigned int = 0);
		void H264_SamplesI1Transform8x8(short *, unsigned int, short[][64], unsigned int = 0);

		void H264_BypassAdjustH4x4(short *, unsigned int);
		void H264_BypassAdjustV4x4(short *, unsigned int);
		void H264_BypassAdjustH4x4a(short *, unsigned int, unsigned int);
		void H264_BypassAdjustV4x4a(short *, unsigned int, unsigned int);
		void H264_BypassAdjustH8x8(short *, unsigned int);
		void H264_BypassAdjustV8x8(short *, unsigned int);
		void H264_Scan0Reverse8x8(short *);
		void H264_Scan1Reverse8x8(short *);
		void H264_Scan0Reverse4x4(short *, unsigned int);
		void H264_Scan1Reverse4x4(short *, unsigned int);


		void H264_DefaultLumaUpdate(short *, const short *);
		void H264_WeightedLumaUpdate(short *, void *, const short *); // InterPredSet &
		void H264_DefaultChromaUpdate(short *);
		void H264_WeightedChromaUpdate(short *, void *); // InterPredSet &

		void H264_InterpolateLuma(void *, const short *, const short *, const short *); // InterPredSet &
		void H264_InterpolateChroma(void *, const short *, const short *, UI_64); // InterPredSet &

		void H264_Interpolate_G();
		void H264_Interpolate_d();
		void H264_Interpolate_h();
		void H264_Interpolate_n();
		void H264_Interpolate_a();
		void H264_Interpolate_e();
		void H264_Interpolate_i();
		void H264_Interpolate_p();
		void H264_Interpolate_b();
		void H264_Interpolate_f();
		void H264_Interpolate_j();
		void H264_Interpolate_q();
		void H264_Interpolate_c();
		void H264_Interpolate_gg();
		void H264_Interpolate_k();
		void H264_Interpolate_r();

		void H264_InterClear();


		void H264_SamplesShake4x4(short *);
		void H264_VerticalIPred4x4(short *, const short *);
		void H264_HorizontalIPred4x4(short *, const short *);
		void H264_DCIPred4x4(short *, const short *);
		void H264_DownLeftIPred4x4(short *, const short *);
		void H264_DownRightIPred4x4(short *, const short *);
		void H264_VRightIPred4x4(short *, const short *);
		void H264_VLeftIPred4x4(short *, const short *);
		void H264_HDownIPred4x4(short *, const short *);
		void H264_HUpIPred4x4(short *, const short *);

		


		void H264_SamplesShake8x8(short *);
		unsigned int H264_ReferenceFilter8x8(short *, const short *, const short *, const short *);
		void H264_VerticalIPred8x8(short *, const short *);
		void H264_HorizontalIPred8x8(short *, const short *);
		void H264_DCIPred8x8(short *, const short *);
		void H264_DownLeftIPred8x8(short *, const short *);
		void H264_DownRightIPred8x8(short *, const short *);
		void H264_VRightIPred8x8(short *, const short *);
		void H264_VLeftIPred8x8(short *, const short *);
		void H264_HDownIPred8x8(short *, const short *);
		void H264_HUpIPred8x8(short *, const short *);

		
		
		
		
		void H264_ChromaShake1(short *);
		void H264_ChromaShake2(short *);

		void H264_VerticalIPred16(short *, const short *);
		void H264_HorizontalIPred16(short *, const short *);
		void H264_VerticalIChroma(short *, const short *, unsigned int);
		void H264_HorizontalIChroma(short *, const short *, unsigned int);

		void H264_DCIPred16(short *, const short *, const short *);
		void H264_DCIChroma(short *, const short *, const short *, unsigned int);
		void H264_LevelShift16(short *, unsigned int);

		void H264_PlaneIPred16(short *, const short *, const short *, short);
		void H264_PlaneIChroma(short *, const short *, const short *, unsigned int);


	}

	class H264: public Media::Codec {
		protected:

			struct NAL_Unit {
				union Header {
					struct {
						unsigned char _type: 5;
						unsigned char ref_idc: 2;
						unsigned char : 1;
					};
					unsigned char _bval;
				} unit;

				unsigned char primary_pic_type;

				struct SVCHeader {
					unsigned int difference_of_base_pic_nums;
					unsigned int long_term_base_pic;

					unsigned char no_inter_layer_pred_flag;
					unsigned char priority_id;
					unsigned char idr_flag;

					unsigned char dependency_id;
					unsigned char quality_id;
					unsigned char temporal_id;

					unsigned char use_ref_base_pic_flag;
					unsigned char discardable_flag;
					unsigned char ouput_flag;
					
					unsigned char store_ref_base_pic_flag;
					unsigned char adaptive_ref_base_pic_marking_mode_flag;
				} SVC;

				struct MVCHeader {
					unsigned short view_id;

					unsigned char non_idr_flag;
					unsigned char priority_id;					
					unsigned char temporal_id;
					unsigned char anchor_pic_flag;
					unsigned char inter_view_flag;
				} MVC;

			} NAL;
			
			

			struct VUI {				
				unsigned int matrix_coefficients;
				unsigned int luma_d, chroma_d;
				unsigned int video_full_range_flag;
				unsigned int width, height, c_width, c_height;

				UI_64 second_field_offset;

				unsigned int num_units_tick;
				unsigned int time_scale;
				unsigned int max_bytes_per_pic_denom;
				unsigned int max_num_reorder_frames;
				unsigned int max_decoded_frame_buffering;


				SFSco::Object svc_buf, mvc_buf;


				unsigned short sar_width, sar_height;

				unsigned char aspect_ratio_idc;

				unsigned char oversacn_appropriate_flag;

				unsigned char video_format;
				
				unsigned char colour_primaries;
				unsigned char transfer_characteristics;
				

				unsigned char chroma_loc_type_top;
				unsigned char chroma_loc_type_bottom;

				unsigned char fixed_frame_rate_flag;


				unsigned char nal_hrd_present_flag;
				unsigned char vcl_hrd_present_flag;
				unsigned char low_delay_hrd_flag;
				
				unsigned char pic_struct_present_flag;

				unsigned char motion_vectors_over_pic_flag;
				unsigned char max_bits_per_mb_denom;
				unsigned char max_mv_length_bits[2];				

			} vui_params;

			struct HRD {
				struct CodedPicBuf {
					unsigned int bit_rate;
					unsigned int cpb_size;
					unsigned int cbr_flag;

				} cpb_cfg[32];

				unsigned char cpb_cnt;
				unsigned char bit_rate_scale;
				unsigned char cpb_size_scale;
				
				unsigned char initial_cpb_removal_delay_length;
				unsigned char cpb_removal_delay_length;
				unsigned char dpb_output_delay_length;
				unsigned char time_offset_length;

			} nal_hrd, vcl_hrd;

			struct SequencePSet {
				short intra_scaling_list_4x4[3][16];
				short intra_scaling_list_8x8[3][64];

				short inter_scaling_list_4x4[3][16];
				short inter_scaling_list_8x8[3][64];
								
				UI_64 pic_width_mbs, pic_height_mbs;
				UI_64 pic_height_mapunits;

				UI_64 frame_crop_left;
				UI_64 frame_crop_right;
				UI_64 frame_crop_top;
				UI_64 frame_crop_bottom;

				unsigned int max_num_ref_frames, max_pic_order_count_lsb;

				int non_ref_pic_offset, expectedDeltaPP;
				int top2bottom_field_offset;
				int ref_frame_offset[256];


				unsigned char scaling_matrix_present;
				unsigned char profile_idc;

				union {
					struct {
						bool : 1;
						bool : 1;
						bool set0: 1;
						bool set1: 1;
						bool set2: 1;
						bool set3: 1;
						bool set4: 1;
						bool set5: 1;
					};
					unsigned char _bval;
				} constraint_flags;

				unsigned char level_idc;
				unsigned char valid;

				unsigned char chroma_format_idc;
				unsigned char separate_planes_flag;
				unsigned char cmp_depth[4];				
				unsigned char chroma_width;
				unsigned char chroma_height;

								
				unsigned char qpprime_y_zero_transform_bypass_flag;

				unsigned char frame_num_gaps_flag;
				unsigned char frame_mbs_only_flag;
				unsigned char mb_adaptive_frame_field_flag;
				unsigned char direct_8x8_inference_flag;
				

				unsigned char max_frame_num_bits;
				unsigned char pic_order_cnt_type;
				unsigned char max_pic_order_cnt_bits;
				unsigned char delta_pic_order_zero_flag;
				unsigned char num_ref_frames_in_pic_order;


				struct Extension {
//					unsigned char set_id;
					unsigned char aux_format_idc;
					unsigned char bit_depth_aux8;
					unsigned char alpha_opaque;
					unsigned char alpha_transparent;

					unsigned char alpha_incr_flag;
					unsigned char additional_ext_flag;
					unsigned char additional_ext2_flag;
					unsigned char additional_ext2_data_flag;

				} extension;

				struct SVCX {
					int scaled_ref_layer_left_offset;
					int scaled_ref_layer_top_offset;
					int scaled_ref_layer_right_offset;
					int scaled_ref_layer_bottom_offset;

					unsigned char inter_layer_deblocking_flag;
					unsigned char extended_spacial_capability_idc;
					unsigned char chroma_phase_x;
					unsigned char chroma_phase_y;
					unsigned char ref_layer_chroma_phase_x;
					unsigned char ref_layer_chroma_phase_y;

					unsigned char tcoeff_level_prediction_flag;
					unsigned char adaptive_tcoeff_level_prediction_flag;
					unsigned char slice_header_restriction_flag;
				} SVC;

			};
			
			static const UI_64 seq_psize = (sizeof(SequencePSet)+31)&(~31);

			struct PicturePSet {
				short intra_scaling_list_4x4[2][3][6][16];
				short intra_scaling_list_8x8[2][3][6][64];

				short inter_scaling_list_4x4[2][3][6][16];
				short inter_scaling_list_8x8[2][3][6][64];

				struct SliceGproup {
					unsigned int run_length;
					unsigned int top_left;
					unsigned int bottom_right;
					unsigned int id;
				} slice_groups[8];

				unsigned int slice_group_change_rate;
				unsigned int size_map_units;

				unsigned char valid;
				unsigned char sp_set_id;

				unsigned char entropy_coding_mode_flag;
				unsigned char bottom_field_pic_order_flag;

				unsigned char num_slice_groups;
				unsigned char slice_group_map_type;

				unsigned char slice_group_change_direction_flag;

				unsigned char num_ref_idx[2];

				unsigned char weighted_pred;
				unsigned char weighted_bipred;

				short init_qp;
				short init_qs;
				char chroma_qpinx_offset[4];

				unsigned char deblocking_filter_control_flag;
				unsigned char constrained_intra_pred_flag;
				unsigned char redundant_pic_cnt_flag;

				unsigned char trasnform_8x8_mode_flag;

			};

			static const UI_64 pic_psize = (sizeof(PicturePSet)+31)&(~31);

			static __declspec(align(16)) const short default_intra_scaling_4x4[16];
			static __declspec(align(16)) const short default_intra_scaling_8x8[64];
			static __declspec(align(16)) const short default_inter_scaling_4x4[16];			
			static __declspec(align(16)) const short default_inter_scaling_8x8[64];

			static __declspec(align(16)) const short norm_adjust_4x4[2][6][16];
			static __declspec(align(16)) const short norm_adjust_8x8[2][6][64];

			static __declspec(align(16)) const unsigned char deblocker_cfg[52][8];

			struct IDRec {
				unsigned int frame_num;
				int field_ocount[2];
				unsigned int idr_flag, idr_pic_id, pic_set_id, ref_idc, field_pic_flag, bottom_field_flag;

				IDRec() { Clear(); };

				void Clear() {
					frame_num = -1;
					field_ocount[0] = -1;
					field_ocount[1] = -1;

					idr_flag = 0;
					idr_pic_id = 0;
					pic_set_id = -1;
					ref_idc = 0;
					field_pic_flag = 0;
					bottom_field_flag = 0;

				};

				IDRec & operator =(const IDRec & r_rec) {
					frame_num = r_rec.frame_num;
					field_ocount[0] = r_rec.field_ocount[0];
					field_ocount[1] = r_rec.field_ocount[1];

					idr_flag = r_rec.idr_flag;
					idr_pic_id = r_rec.idr_pic_id;
					pic_set_id = r_rec.pic_set_id;
					ref_idc = r_rec.ref_idc;
					field_pic_flag = r_rec.field_pic_flag;
					bottom_field_flag = r_rec.bottom_field_flag;

					return *this;
				};

				unsigned int NEQ(const IDRec & r_rec) const {
					unsigned int result(0);

					result = frame_num ^ r_rec.frame_num;
					result |= field_ocount[0] ^ r_rec.field_ocount[0];
					result |= field_ocount[1] ^ r_rec.field_ocount[1];
					result |= idr_flag ^ r_rec.idr_flag;
					result |= idr_pic_id ^ r_rec.idr_pic_id;
					result |= pic_set_id ^ r_rec.pic_set_id;
					result |= ref_idc ^ r_rec.ref_idc;
					result |= field_pic_flag ^ r_rec.field_pic_flag;
					result |= bottom_field_flag ^ r_rec.bottom_field_flag;

					return result;
				};
			};
			
			struct RefPicture {
				SFSco::Object buffer;
				UI_64 frame_size;
				I_64 time_stamp;

				unsigned int _reserved;

				IDRec id_rec;

				RefPicture() :_reserved(0), frame_size(0), time_stamp(0) {};

				RefPicture & operator =(const RefPicture & ref_p) {
					buffer = ref_p.buffer;
					frame_size = ref_p.frame_size;
					time_stamp = ref_p.time_stamp;

					_reserved = ref_p._reserved;

					id_rec = ref_p.id_rec;


					return *this;
				};

				void Destroy() {
					buffer.Destroy();
					
					_reserved = 0;
					frame_size = 0;
					time_stamp = 0;

					id_rec.Clear();
				};

			} pic_list[MAX_REF_IDX];

			I_64 time_table[MAX_REF_IDX][2];

			struct Slice {
				struct WeightTable {
					short c_weight;
					short c_offset;
				} weight_table[32][2][3];

				PicturePSet * pic_set;
				SequencePSet * seq_set;

				UI_64 first_mb, max_pic_num;
				IDRec id_rec;
							
				unsigned int mb_skip_run;
				unsigned int gray_val[2];

				unsigned char weight_denom_bits[4];
				
				unsigned char _type;
				unsigned char weight_type;

				unsigned char num_ref_idx_active[2];
					
				short qpy;
				short qsy;

				short alpha_c0_offset;
				short beta_offset;

				unsigned char colour_plane_id;
				unsigned char direct_spatial_mv_pred_flag;

				unsigned char sp_for_switch_flag;
				unsigned char disable_deblocking_filter_idc;

				unsigned char group_charge_cycle;				
				unsigned char redundant_pic_cnt;
			

				void Clear();

				Slice() {Clear();};
			} current_slice;

			HANDLE stop_hnd;
			unsigned int pp_count, sp_count, out_pin;
			int decode_skip_count;
			SFSco::Object picture_params, sequence_params;

			struct RefSet {
				unsigned short ref_id, ref_idx;
				short mvd[4][2];

				RefSet() : ref_id(0), ref_idx(-1) {
					for (unsigned int i(0);i<4;i++) {
						mvd[i][0] = mvd[i][1] = 0;		
					}
				};
			};


			struct MacroBlockSet {
				unsigned int flag_type;
				unsigned short coded_block[4];

				short qp_delta, qp[3];

				unsigned char par_val, _reserved;
			
				unsigned char field_decoding_flag;
				unsigned char intra_chroma_pred_mode;				
				
				char intra_pred_mode[16];

				struct SubSet {
					unsigned short type_flag;					
					unsigned short ref_idx[2];

					short mvd[2][4][2];

					SubSet & operator =(const SubSet & ref_ss) {
						type_flag = ref_ss.type_flag;
						
						ref_idx[0] = ref_ss.ref_idx[0];
						ref_idx[1] = ref_ss.ref_idx[1];

						mvd[0][0][0] = ref_ss.mvd[0][0][0];
						mvd[0][0][1] = ref_ss.mvd[0][0][1];
						mvd[0][1][0] = ref_ss.mvd[0][1][0];
						mvd[0][1][1] = ref_ss.mvd[0][1][1];
						mvd[0][2][0] = ref_ss.mvd[0][2][0];
						mvd[0][2][1] = ref_ss.mvd[0][2][1];
						mvd[0][3][0] = ref_ss.mvd[0][3][0];
						mvd[0][3][1] = ref_ss.mvd[0][3][1];

						mvd[1][0][0] = ref_ss.mvd[1][0][0];
						mvd[1][0][1] = ref_ss.mvd[1][0][1];
						mvd[1][1][0] = ref_ss.mvd[1][1][0];
						mvd[1][1][1] = ref_ss.mvd[1][1][1];
						mvd[1][2][0] = ref_ss.mvd[1][2][0];
						mvd[1][2][1] = ref_ss.mvd[1][2][1];
						mvd[1][3][0] = ref_ss.mvd[1][3][0];
						mvd[1][3][1] = ref_ss.mvd[1][3][1];


						return *this;
					};

				} sub[4];
								
				unsigned char coeff_token[3][16];
				short x_tent[8];

			};
						

			class DecoderState {
				public:
					const unsigned char * src_ptr;
					unsigned int bit_mask[2];
					__int64 src_size;

					

				public:					
					static const unsigned int cbp_map12[2][48];
					static const unsigned int cbp_map03[2][16];
										

					MacroBlockSet * mb_x;
					const MacroBlockSet * mb_a, * mb_b, * mb_p, * mb_c, * mb_d, * mb_u;

					short * frame_ptr;

					UI_64 pline_offset, current_mb_offset;

					UI_64 nal_counter;
					
					__int64 current_mb_ptr, line_counter, line_ptr, slice_width, flag_set, current_frame_ptr;
					unsigned int prevPOCMsb, prevPOCLsb, prev_ref_num, reset_op, frameNumOffset, list_count[2];
					unsigned short ref_pic_list[2][64];

					unsigned char above_flag, labs_selector;

					unsigned char chroma_array_type, jump_flag, color_transform_override, deblock_override;

					__declspec(align(16)) short clip_vals[8][8];

					__forceinline unsigned char NextBit() {
						unsigned char result(0);

						if (src_size > 0) {
							result = 1 + (((bit_mask[0] & bit_mask[1]) - 1) >> 7);

							if ((bit_mask[0] >>= 1) == 0) {
								bit_mask[0] = 0x00000080;

								src_ptr++;
								src_size--;

								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];

								if ((bit_mask[1] & 0x00FFFFFF) == 3) {
									if (src_size > 0) {
										src_ptr++;
										src_size--;

										bit_mask[1] <<= 8;
										bit_mask[1] |= src_ptr[0];

									}
								}
							}
						}

						return result;
					};



					__forceinline unsigned char NextByte() {
						unsigned char result(0);

						if (src_size > 0) {
							result = src_ptr[0];

							src_ptr++;
							src_size--;

							bit_mask[0] = 0x00000080;

							bit_mask[1] <<= 8;
							bit_mask[1] |= src_ptr[0];
														
							if ((bit_mask[1] & 0x00FFFFFF) == 3) {
								if (src_size > 0) {
									src_ptr++;
									src_size--;

									bit_mask[1] <<= 8;
									bit_mask[1] |= src_ptr[0];
								}
							}
						}

						return result;
					};

					__forceinline void ByteAlign() {
						if (src_size > 0) {
							if (bit_mask[0] ^ 0x00000080) {
								bit_mask[0] = 0x00000080;

								src_ptr++;
								src_size--;

								bit_mask[0] = 0x00000080;

								bit_mask[1] <<= 8;
								bit_mask[1] |= src_ptr[0];
															
								if ((bit_mask[1] & 0x00FFFFFF) == 3) {
									if (src_size > 0) {
										src_ptr++;
										src_size--;

										bit_mask[1] <<= 8;
										bit_mask[1] |= src_ptr[0];
									}
								}
							}
						}
					};


					__forceinline unsigned char IsMoreRBSP() {
						if (src_size > 1) return 1;
						if (src_size == 1) return ((src_ptr[0] ^ bit_mask[0]) & (bit_mask[0]-1));
						return 0;
					};					

					__forceinline void SetSource(const unsigned char * sptr, UI_64 sval) {						
						src_ptr = sptr;
						bit_mask[0] = 0x00000080;
						bit_mask[1] = 0x0000FF00 | sptr[0];						
						src_size = sval;
					};

					__forceinline bool IsSource(short crv = 0) {
						if (src_size > 0) return 1;
						if (crv >= 256) return 1;
						return 0;
					};

					__forceinline void MapRL0() {
						if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].ref_idx[0] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].type_flag = ref_pic_list[0][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].ref_idx[0]];
						else {
							if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].ref_idx[1] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].type_flag = ref_pic_list[1][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[0].ref_idx[1]];
						}

						if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].ref_idx[0] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].type_flag = ref_pic_list[0][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].ref_idx[0]];
						else {
							if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].ref_idx[1] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].type_flag = ref_pic_list[1][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[1].ref_idx[1]];
						}

						if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].ref_idx[0] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].type_flag = ref_pic_list[0][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].ref_idx[0]];
						else {
							if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].ref_idx[1] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].type_flag = ref_pic_list[1][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[2].ref_idx[1]];
						}

						if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].ref_idx[0] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].type_flag = ref_pic_list[0][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].ref_idx[0]];
						else {
							if (reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].ref_idx[1] != 0xFFFF) reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].type_flag = ref_pic_list[1][reinterpret_cast<MacroBlockSet::SubSet *>(mb_x->x_tent)[3].ref_idx[1]];
						}

					};

					int GolombSigned();


					DecoderState();

			} decoder_cfg;
			
			static const unsigned int up_map[16], left_map[16], ur_map[16], ul_map[16];
			static const unsigned int up_idx[16], left_idx[16], ur_idx[16], ul_idx[16];
			static const unsigned int dc_idx_16[16];

			static const unsigned int col_map_idx[4][4];
			static const unsigned int col_remap[4][16];

			static int Clip3(int, int, int);

			__declspec(align(16)) struct CABAC {
				struct TypeBin {
					unsigned char lengths[8];
					unsigned char codes[32];
					unsigned char vals[32];
				};

				static __declspec(align(16)) const short m_n[4][2048];

				static const short codi_range_lps[64][4];
				static const unsigned char transition_table[64][2];
				static const unsigned char scoefflag_inc_8x8[2][64];
				static const unsigned char lscoefflag_inc_8x8[64];
				static const TypeBin i_type, b_type;
				static const unsigned int type_bit_switch[8][2];

				static const unsigned int sig_flag_chroma_dc_offset[8];

				static const unsigned int si_coeff_offset[2][16];
				static const unsigned int last_si_coeff_offset[2][16];

				static const unsigned int coeff_abs_offset[16];
				static const unsigned int coded_block_flag_offset[16];

				static unsigned int abs_lev_block_offset[16];
				
				static const unsigned char coeff_abs_idx_inc[4][16];
				
				static const unsigned short up_cbf[16], left_cbf[16];

				static unsigned int * bin_run_tab;

				unsigned char context_vars[1024]; // [2];
				short codi_range, codi_offset;
				
				__forceinline void RenormD(DecoderState & d_cfg) {
					for (;(codi_range < 256) && (d_cfg.src_size > 0);) {
						codi_range <<= 1;
						codi_offset <<= 1;

						codi_offset |= 1 + (((d_cfg.bit_mask[0] & d_cfg.bit_mask[1]) - 1) >> 7);
	
						if ((d_cfg.bit_mask[0] >>= 1) == 0) {
							d_cfg.bit_mask[0] = 0x00000080;

							d_cfg.src_ptr++;
							d_cfg.src_size--;

							d_cfg.bit_mask[1] <<= 8;
							d_cfg.bit_mask[1] |= d_cfg.src_ptr[0];

							if ((d_cfg.bit_mask[1] & 0x00FFFFFF) == 3) {
								if (d_cfg.src_size > 0) {
									d_cfg.src_ptr++;
									d_cfg.src_size--;

									d_cfg.bit_mask[1] <<= 8;
									d_cfg.bit_mask[1] |= d_cfg.src_ptr[0];

								} else {
									break;
								}
							}
						}
					}
				};

				__forceinline short GetBinVal(unsigned int cidx, DecoderState & d_cfg) {
					short cir_lps(0);
					short result(0);

					cir_lps = codi_range_lps[context_vars[cidx] & 0x3F][(codi_range>>6) & 3];
					codi_range -= cir_lps;

					result = context_vars[cidx] & 0x40;
					if (codi_offset < codi_range) {						
						context_vars[cidx] = result | transition_table[context_vars[cidx] & 0x3F][1];
					} else {
						result ^= 0x40;
						if ((context_vars[cidx] & 0x3F) == 0) {
							context_vars[cidx] &= 0x3F;
							context_vars[cidx] |= result;
						}

						codi_offset -= codi_range;
						codi_range = cir_lps;		


						context_vars[cidx] = (context_vars[cidx] & 0x40) | transition_table[context_vars[cidx] & 0x3F][0];
					}

					RenormD(d_cfg);

					return (result >> 6);
				};


				__forceinline short GetBinVal(DecoderState & d_cfg) {
					short result(0);
					codi_offset <<= 1;

					codi_offset |= d_cfg.NextBit();

					if (codi_offset >= codi_range) {
						result = 1;
						codi_offset -= codi_range;
					}

					return result;
				};

				__forceinline unsigned short GetBinVal276(DecoderState & d_cfg) {
					codi_range -= 2;

					if (codi_offset >= codi_range) return 1;
	
					RenormD(d_cfg);	
					return 0;
				};

				__forceinline void Reset(DecoderState & d_cfg) {
					codi_range = 510;
					codi_offset = H264_DS_GetNumber(&d_cfg, 9);
				};

				

				unsigned int GetMBTypeI(DecoderState &);
				unsigned int GetMBTypeSI(DecoderState &);
				unsigned int GetMBTypeB(DecoderState &);
				unsigned int GetMBTypeP(DecoderState &);
				
				unsigned int GetCodedBlockPattern(DecoderState &);

				__forceinline unsigned short GetMBSkipFlag(DecoderState &);
				__forceinline unsigned char GetSubMBPType(DecoderState &);
				__forceinline unsigned char GetSubMBBType(DecoderState &);

				void GetMVD(short *, DecoderState &, unsigned int);
								
				__forceinline unsigned char GetRefIdx(DecoderState &, unsigned int, unsigned int);

				__forceinline char GetMBQPDelta(DecoderState &);
				__forceinline unsigned char GetIntraChromaPredMode(DecoderState &);
				__forceinline unsigned short GetPrevIntraPredModeFlag(DecoderState &);
				__forceinline char GetRemIntraMode(DecoderState &);
				__forceinline unsigned char GetMBFieldDecodingFlag(DecoderState &);

				__forceinline unsigned short GetCoeffSignFlag(DecoderState &);

				__forceinline unsigned short GetCodedBlockFlag8x8(DecoderState &, unsigned short, unsigned short);
				__forceinline unsigned short GetSignificantCoeffFlag8x8(DecoderState &, unsigned int, unsigned short);
				__forceinline unsigned short GetLastSignificantCoeffFlag8x8(DecoderState &, unsigned int, unsigned short);

				__forceinline unsigned short GetCodedBlockFlag(DecoderState &, unsigned short, unsigned short);
				__forceinline unsigned short GetSignificantCoeffFlag(DecoderState &, unsigned int, unsigned short);
				__forceinline unsigned short GetLastSignificantCoeffFlag(DecoderState &, unsigned int, unsigned short);
				__forceinline short GetCoeffAbsLevel(DecoderState &, unsigned int);

				__forceinline unsigned short GetCodedBlockFlagDC(DecoderState &, unsigned short, unsigned short);

				__forceinline unsigned short GetSignificantCoeffFlagChromaDC(DecoderState &, unsigned int);
				__forceinline unsigned short GetLastSignificantCoeffFlagChromaDC(DecoderState &, unsigned int);

				__forceinline unsigned short GetCodedBlockFlagChroma1AC(DecoderState &, unsigned short, unsigned short);
				__forceinline unsigned short GetCodedBlockFlagChroma2AC(DecoderState &, unsigned short, unsigned short);

				__forceinline unsigned short GetEndOfSliceFlag(DecoderState &);

				__forceinline unsigned short GetTransformSize8x8Flag(DecoderState &);


			} cabac;

			struct CAVLC {
				static __declspec(align(16)) const unsigned char token_category_tab[6][16][2];
				static __declspec(align(16)) const unsigned char token_val_tab[6][64];
				static __declspec(align(16)) const unsigned char zeros_map[10][8];

				static unsigned char GetRefIdx(DecoderState &, unsigned int);

				static unsigned char GetCoeffToken0(DecoderState &);
				static unsigned char GetCoeffToken1(DecoderState &);
				static unsigned char GetCoeffToken2(DecoderState &);
				static unsigned char GetCoeffToken3(DecoderState &);
				static unsigned char GetCoeffToken4(DecoderState &);
				static unsigned char GetCoeffToken5(DecoderState &);
				
				static unsigned char GetTotalZeros(DecoderState &, unsigned int);
				static unsigned char GetTotalZerosDC1(DecoderState &, unsigned int);
				static unsigned char GetTotalZerosDC2(DecoderState &, unsigned int);

				static unsigned char GetRunBefore(DecoderState &, unsigned int);

				
				void GetCoeffLevels(DecoderState &, short *, int);

				void GetCoeffLevelsDC(DecoderState &, short *, int);
				void GetCoeffLevelsDC1(DecoderState &, short *, int);
				void GetCoeffLevelsDC2(DecoderState &, short *, int);

			} cavlc;
			
			static const short qpc_map[22];
			static const unsigned int bmb_part_mode[24];
			static const unsigned short bmb_pred_mode[24][4];

			static const unsigned short bsubtype_pred_mode[16];
			static const unsigned short psubtype_pred_mode[4];


			__forceinline unsigned int Filter8x8(const short *, unsigned int, unsigned int);
			__forceinline unsigned int Filter4x4(const short *, unsigned int, unsigned int);
			
			typedef void (* pIPred)(short *, const short *);

			static pIPred i_pred_4x4[9];

			static pIPred i_pred_8x8[9];


			static short FormatMbaffVal(short, unsigned char);
			static unsigned short FormatMbaffVal(unsigned short, unsigned char);
		
			typedef void (* pInterpolate)();

			struct InterPredSet {				
				unsigned int map_flags;
				short mvd[2];
				UI_64 l_offset;
				int w0, o0, w1, o1, logWD, _reserved;

				pInterpolate func;
			};

			struct InterPredCfg {
				const MacroBlockSet * mbi;
				unsigned int part_idx, sub_idx;
			};
			
			void FormatICfg(InterPredCfg *, unsigned int p_idx, unsigned int c_idx);

			static pInterpolate ifuncs[4][4];


			UI_64 GetPicParameters();
			UI_64 GetSeqParameters();
			UI_64 GetPicScaling(PicturePSet *, unsigned char, unsigned char);
			UI_64 GetSeqScaling(SequencePSet *);

			UI_64 GetVUIParameters();

			UI_64 GetSVCExtension(SequencePSet *);
			UI_64 GetSVCVUI(SequencePSet *);
			UI_64 GetMVCExtension(SequencePSet *);
			UI_64 GetMVCVUI(SequencePSet *);
			UI_64 GetMVCDExtension(SequencePSet *);

			UI_64 GetHRDParameters(HRD &);

			UI_64 GetSliceLayer(I_64 &);
//			UI_64 GetSlicePartitionA();
//			UI_64 GetSlicePartitionB();

			UI_64 GetSliceHeader(I_64 &);
			UI_64 SliceCleanUp();
			void InitializeRefPicListB();
			void InitializeRefPicListP();

			UI_64 GetWeightTable();
			UI_64 GetRefPicListModification();
			UI_64 GetRefPicListMVC();
			unsigned int GetDecRefPicMarking();
			void ApplyMarking(I_64 &);

			UI_64 GetSliceDataE();
			UI_64 GetSliceData();

			UI_64 GetMBLayerIE();
			UI_64 GetMBLayerBE();
			UI_64 GetMBLayerPE();
			UI_64 GetMBLayerSIE();

			UI_64 GetMBLayerI();
			UI_64 GetMBLayerB();
			UI_64 GetMBLayerP();
			UI_64 GetMBLayerSI();
						
			void SetQPs();
			
			UI_64 Restore();

			UI_64 ResidualE();				

			UI_64 Residual8x8E(short *, unsigned short, unsigned int);
			UI_64 Residual4x4E(short *, unsigned short, unsigned int);

			UI_64 ResidualDCE(short *, unsigned short, unsigned int);
			UI_64 ResidualChromaDCE(short *, unsigned short, unsigned int);
			UI_64 ResidualChroma1ACE(short *, unsigned short, unsigned int);
			UI_64 ResidualChroma2ACE(short *, unsigned short, unsigned int);


			void MBPredE();
			void MBPredIE();
			void SMBPPredE();
			void SMBBPredE();
			void BSubPredCalc(MacroBlockSet::SubSet &, const InterPredCfg *, unsigned int ip_idx, unsigned int list_idx, unsigned int sub_idx);

			UI_64 MBPred();
			UI_64 MBPredI();
			UI_64 SMBPPred();
			UI_64 SMBBPred();


			UI_64 Residual();

			UI_64 Residual8x8(short *, unsigned short, unsigned int);
			UI_64 Residual4x4(short *, unsigned short, unsigned int);

			UI_64 ResidualDC(short *, unsigned short);
			UI_64 ResidualChroma1DC(short *, unsigned short);
			UI_64 ResidualChroma2DC(short *, unsigned short);
			UI_64 ResidualChroma1AC(short *, unsigned short, unsigned int);
			UI_64 ResidualChroma2AC(short *, unsigned short, unsigned int);


			void SetMBDeblock();		

			void IncMbAddr();

			void RestoreSamples4x4(short *, unsigned int);
			void RestoreSamples8x8(short *, unsigned int);
			void RestoreChroma1(short *, unsigned int);
			void RestoreChroma2(short *, unsigned int);

			void InterDecode(unsigned short *, unsigned int, unsigned int);


			unsigned int SkipPredB();			

			unsigned int SkipPredP();

			void MVPredP();
			void MVPredB();

			void MVPredSubB();
			void MVPredSubP();
			void MVMedianPred(MacroBlockSet::SubSet &, const InterPredCfg *, unsigned int list_idx, unsigned int xps_idx);
			
			void MVPredB8x8(unsigned int);

			void SetInterWeights(InterPredSet &, unsigned short, unsigned short, unsigned int);

			int InterPicDist(unsigned int, unsigned int, unsigned int = FLAG_SECOND_FIELD);

			void SamplesCopy();

			void InterPredLuma(short *, unsigned int);			
			void InterPredChroma1(short *, unsigned int);
			void InterPredChroma2(short *, unsigned int);
			
			virtual void SetRenderSkip(__int64);
		public:
			static const UI_64 frame_buf_tid = 961748941;
			static const unsigned short zz_4x4[2][16], zz_8x8[2][64];
			
			static UI_64 Initialize();
			static UI_64 Finalize();

			H264();
			virtual ~H264();

			virtual unsigned int GetCT();
			virtual UI_64 SetCT(UI_64);

			virtual void JumpOn();
			virtual UI_64 Unpack(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 DecodeUnit(const unsigned char *, UI_64, I_64 &);
			virtual UI_64 FilterFrame(unsigned int *);
			virtual void FilterPreview(unsigned int *, OpenGL::DecodeRecord &);
			
			virtual UI_64 Pack(unsigned short *, UI_64 *, unsigned int);
			virtual UI_64 UnitTest(const float *);
			virtual UI_64 CfgOverride(UI_64, UI_64);
			
	};



}




