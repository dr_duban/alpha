/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once
#include <Windows.h>

#include "CoreLibrary\SFSCore.h"
#include "System\Memory.h"
#include "Data\Stream.h"

#include "CodedSource.h"

namespace Video {
	
	class H222: public Media::Program {
		protected:
			static const unsigned char sync_byte = 0x47;
			static const unsigned char stuff_byte = 0xFF;

			static const unsigned char program_terminate_code = 0xB9;
			static const unsigned char pes_header_code = 0xBA;			
			static const unsigned char system_header_code = 0xBB;
			static const unsigned char pes_directory_code = 0xFF;
// descriptors ==================
			 // tag: 2
			struct VideoStreamDescriptor {
				union {
					struct {								
						bool still_pic: 1;
						bool constrained: 1;
						bool MPEG1: 1;
						unsigned char rate_code: 4;
						bool multiple_rate: 1;
					};
					unsigned char _bval;

				} frame_flags;

				unsigned char MPEG1_profile;

				union {
					struct {
						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool frate_ext: 1;
						unsigned char chroma_format: 2;

					};

					unsigned char _bval;

				} MPEG1_flags;
			};

			// tag 3
			union AudioStreamDescriptor {
				struct {
					bool : 1;
					bool : 1;
					bool : 1;
					bool var_rate: 1;

					unsigned char layer: 2;
					bool ID: 1;
					bool free_format: 1;
				};

				unsigned char _bval;
			};

			// tag 4
			union HierarchyDescriptor {
				struct {					
					unsigned char _type: 4;
					unsigned char : 4;
					
					unsigned char layer_index: 6;
					unsigned char : 2;

					unsigned char embedded_layer_index: 6;
					unsigned char : 2;

					unsigned char channel: 6;
					unsigned char : 2;

				};

				unsigned char _bval[4];

			};

			// tag 5
			struct RegistrationDescriptor { // identification for private decriptors


			};

			// tag 6 : data alignment type | single byte

			// tag 7
			struct TargetBackgroudGridDescriptor {
				unsigned short width;
				unsigned short height;
				unsigned char aspect_ratio;
			};

			// tag 8
			struct VideoWindowDescriptor {
				unsigned short width;
				unsigned short height;
				unsigned char priority;

			};

			// tag 9 : CA descriptor


			// tag 10 : ISO 639 language


			// tag 11 : system clock
			union SystemClockDescriptor {
				struct {
					unsigned char clock_accuracy_integer: 6;
					bool : 1;
					bool external_clock_indicator: 1;

					bool : 1;
					bool : 1;
					bool : 1;
					bool : 1;

					bool : 1;
					unsigned char clock_accuracy_exponent: 3;

				};
				unsigned char _bval[2];

			};

			// tag 12 : multiplex buffer utilization
			struct MuBuUtDescriptor {
				unsigned short LTW_offaset_low, LTW_offset_high;

			};

			// tag 13 : copyright


			// tag 14 : maximum bitrate | single short val


			// tag 15 : privata data indicator | single integer


			// tag 16 : smoothing buffer
			struct SmothingBufferDescriptor {
				unsigned short leak_rate, size;

			};


			// tag 17 : STD (13818-2)
			union STDDscriptor {
				struct {
					bool leak_valid: 1;
					bool : 1;
					bool : 1;
					bool : 1;

					bool : 1;
					bool : 1;
					bool : 1;
					bool : 1;
				};
				unsigned char _bval;
			};

			// tag 18 : IBP
			struct IBPDescriptor {
				unsigned short max_gop_length;
				unsigned char identical_gop, closed_gop; // bool
			};


			// tag 19-26 : 13818-6


			// tag 27 : MPEG-4 video | single byte (14496-2)


			// tag 28 : MPEG-4 audio | single byte (14496-3)


			// tag 29 : IOD (14496-1)


			// tag 30 : SL | single short (14496-1)


			// tag 31 : FlexMux (14496-1)


			// tag 32 : external ES_ID | single short (14496-1)


			// tag 33 : MuxCode (14496-1)


			// tag 34 : FmxBufferSize (14496-1)


			// tag 35 : multiplexbuffer
			struct MultiplexBufferDescriptor {
				unsigned int mb_size, leak_rate;
			};

			// tag 36 : content labeling
			struct ContentLabelingDescriptor {
				unsigned int metadata_application_format_id, metadata_format_id;
				unsigned short metadata_application_format;
				unsigned char metadata_format, metadata_service_id;
				unsigned short ts_location, ts_id, program_number;
			};

			// tag 37 : metadataaa pointer


			// tag 38 : metadata


			// tag 39 : metadata STD
			struct MetadataSTDDescriptor {
				unsigned int input_leak_rate;
				unsigned int buffer_size;
				unsigned int output_leak_rate;
			};

			// tag 40 : AVC video
			struct AVCVideoDescriptor {
				unsigned char profile_id, level_id;

				union {
					struct {
						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool constraint_set2: 1;
						bool constraint_set1: 1;
						bool constraint_set0: 1;


						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool : 1;
						bool pic_24h: 1;
						bool still: 1;

					};
					unsigned char _bval[2];
				} flags;

			};

			// tag 41 : IPMP


			// tag 42 : AVC timing
			struct AVCHRDDescriptor {
				unsigned int N_90k, K_90k, num_units;

				union {
					struct {
						bool picand_time_info: 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool : 1;
						bool : 1;
						bool hrd_managment: 1;


						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool pic_2_display: 1;
						bool temporal_poc: 1;
						bool fixed_frame_rate: 1;
					};
					unsigned char _bval[2];
				} flags;
			};

			// tag 43 : MPEG-2 AAC audio
			struct MPEG2AACDescriptor {
				unsigned char profile;
				unsigned char channel_configuration;
				unsigned char additional_info;
			};

			// tag 44 : FlexMuxTiming
			struct FlexMuxTimingDescriptor {
				unsigned int FCR_resolution;
				unsigned short FCR_ES_ID;
				unsigned char FCR_length, fmx_rate_length;

			};

			// tag 45 : text stream header


			// tag 46 : audio extension stream header


			// tag 47 : video auxillary stream


			// tag 48 : video scalable stream header


			// tag 49 : video multistream header


			// tag 50 : video 15444-3

			
			// tag 51 : video multi operation point


			// tag 52 : video 3D header


			// tag 53 : program info 3D


			// tag 54 : video info 3D




// transport stream =====================================================
			struct TransportPacket {
				union Service {
					struct {
						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool priority: 1;
						bool start_indicator: 1;
						bool error: 1;
					};
					unsigned char _bval;

				} service_flags;

				union Control {
					struct {
						unsigned char continuity_counter: 4;
						unsigned char adaptation_field: 2;
						unsigned char scrambling: 2;
					};
					unsigned char _bval;
				} control_flags;

				union AdaptationFlags {
					struct {
						bool adaptation_field_extension: 1;
						bool transport_private_data: 1;
						bool splicing_point: 1;
						bool OPCR: 1;

						bool PCR: 1;
						bool elementary_stream_priority: 1;
						bool random_access: 1;
						bool discontinuity: 1;
											
					};

					unsigned char _bval;
				} adaptation_flags;

				union XtentionFlags {
					struct {
						bool x2: 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool pSTD_buffer: 1;
						bool sequence_counter: 1;
						bool pack_header_field: 1;
						bool private_data: 1;

					};
					unsigned char _bval;
				};

				unsigned short id;
				UI_64 l_size;

				TransportPacket() : id(0x1FFF), l_size(0) {
					service_flags._bval = 0;
					control_flags._bval = 0;
					adaptation_flags._bval = 0;
				};

			} current_packet;
		

			union PES_flags {
				struct {
					bool original: 1;
					bool copyright: 1;
					bool data_alignment: 1;
					bool priority: 1;

					unsigned char scrambling_control: 2;
					unsigned char marker: 2;

					bool extension: 1;
					bool CRC: 1;
					bool additional_copyright: 1;
					bool DSM_trick_mode: 1;

					bool ES_rate: 1;
					bool ESCR: 1;
					unsigned char PTS_DTS: 2;
				};

				unsigned char _bvsl[2];
			};

			struct TableConfig {				
				union PATcfg {
					struct {
						bool current_next_id: 1;
						unsigned char version: 5;
						unsigned char: 2;
					};
					unsigned char _bval;
				} tab_cfg;

				unsigned char current_section;
				unsigned char section_count;
			};

			struct PIDvector {
				UI_64 pid_val;
				SFSco::Object pid_obj;
			};

			struct AdaptationControl {
				unsigned char splice_countdown;
				unsigned char splice_type;

				unsigned char ltw_valid_flag; // bool

				UI_64 legal_time_window_offset;
				UI_64 piecewise_rate;
				unsigned __int64 decoding_time_stamp_nau;

			};


			struct TransportHeader {
				UI_64 pid_runner, pid_top;
			
				AdaptationControl adaptation_cfg;
			};


// program stream ===========================================================
			struct PESHeader {
				unsigned __int64 system_clock;
				unsigned int mux_rate;
			};

			struct SysteHeader {
				unsigned int rate_bound;
				union {
					struct {
						bool CSPS: 1;
						bool fixed: 1;
						unsigned char audio_bound: 6;


						unsigned char video_bound: 5;
						bool : 1;
						bool video_lock: 1;
						bool audio_lock: 1;



						bool : 1;
						bool : 1;
						bool : 1;
						bool : 1;

						bool : 1;
						bool : 1;
						bool : 1;
						bool rate_restriction: 1;

					};
					unsigned char _bval[3];
				} flags;

			};



// =========================================








			struct ProgramHeader { // trasport stream descritor
				unsigned short number, list_length;				
				unsigned short program_map_pid, pcr_pid;

				unsigned __int64 clock_frequency;// pcr_base*300 + pcr_ext
				unsigned __int64 original_clock_frequency;// pcr_base*300 + pcr_ext

				unsigned short pid_list[128];
			};


			struct StreamHeader {
				unsigned char id, _type;
				unsigned short PID[2];


				struct ExtensionSet {
					union ISO11172 {
						struct {
							unsigned char packet_sequence_counter: 7;
							bool : 1;
							
							unsigned char stuff_length: 6;
							bool MPEG_id: 1;
							bool : 1;
							
						};
						unsigned char _bvsl[2];
					} iso_11172;
										
					UI_64 pstd_buffer_size;
				} extension;

				struct TrickSet {
					unsigned char type;
					unsigned char field_id;
					
					unsigned char field_repeat_count;
					unsigned char frequency_truncation;
					
					unsigned char intra_slice_refresh; // bool
					unsigned char _reserved;
				} trick;

				PESHeader pes_header;
				
				I_64 presentation_time_stamp, decoding_time_stamp, elementary_stream_clock_reference; // , local_time, sync_time, sync_dist;

				UI_64 elementary_stream_rate, packet_length, temp_offset, payload_length, temp_size;
				unsigned int last_search_state, lang_id;
				unsigned short previous_packet_CRC;

				Media::CodedSource * _sequence;
				// Codec * _codec;
			};

			

			static const UI_64 ts_header_offset = (sizeof(TransportHeader)+15) & (~(UI_64)15);
			static const UI_64 s_header_offset = (sizeof(StreamHeader)+15) & (~(UI_64)15);
			static const UI_64 p_header_offset = (sizeof(ProgramHeader)+15) & (~(UI_64)15);

			static Memory::Allocator * ts_pool;
						
			SFSco::Object ts_obj, temp_buf, code_buf;
			HANDLE reciever_thread;
				
			Data::Stream source;			
			UI_64 program_count, terminator, temp_size, temp_offset, payload_length, packet_counter, no_video, temp_vid, ts_type;


			// collection of buffers
			static DWORD __stdcall ReceiverLoop(void *);	

			UI_64 GetAF(const unsigned char *);

			UI_64 DecodePacket(const unsigned char *);
			
			UI_64 GetStreamData(const unsigned char *, SFSco::Object &);
			UI_64 ExtractStreamData(StreamHeader *, const unsigned char *);
			UI_64 DataSwitch(StreamHeader *, const unsigned char *);

			UI_64 GetTableSection(const unsigned char *, SFSco::Object &);
			UI_64 ExtractProgramData(const unsigned char *, SFSco::Object &);

			UI_64 AddProgram(const unsigned char *);
			UI_64 DeleteProgram(UI_64);

			UI_64 AddStream(const unsigned char *, SFSco::Object &);
			UI_64 DeleteStream();

			UI_64 ExpandMapTable(UI_64 = 1);
			UI_64 AddMapVector(unsigned short, const SFSco::Object &);

			void SetAudioVolume(float);

			void NVFlush();
			void NVReset();

			H222(unsigned int = 0);
			virtual ~H222();
			

			virtual Media::Source * GetVSource(UI_64 &);
			virtual Media::Source * GetASource(UI_64 &);

			virtual UI_64 GetSpan();
									
		public:
			static const UI_64 _tid = 961749067;
			static const UI_64 program_tid = 961749079;
			static const UI_64 stream_tid = 961749091;
		//	static const UI_64 table_tid = 6549;

// two types: ransport stream and program stream

			UI_64 CreateProgram(); // attach source to the program

			static UI_64 Initialize();
			static UI_64 Finalize();

			static Program * SetSource(const Data::Stream &, unsigned int preview_flag);
			static bool IsT222(Data::Stream &);

			virtual UI_64 Pack(const void *, UI_64);
						
			virtual unsigned int EnableAudioSource(unsigned int, const OpenGL::Element &);
			virtual unsigned int DisableAudioSource(unsigned int);
			virtual unsigned int IsAudioActive(unsigned int);
			virtual unsigned int GetAudioLNG(unsigned int);

	};


}

