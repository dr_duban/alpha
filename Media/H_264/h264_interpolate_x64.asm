
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
ALIGN 16
tap_6	WORD	1, -5, 20, 20, -5, 1, 0, 0



.CODE

ALIGN 16
H264_DefaultLumaUpdate	PROC
	PCMPEQW xmm10, xmm10

	PADDW xmm14, xmm12
	PADDW xmm15, xmm13
		
	PSUBW xmm14, xmm10
	PSUBW xmm15, xmm10

	PSRAW xmm14, 1
	PSRAW xmm15, 1

	PADDW xmm14, [rcx]
	PADDW xmm15, [rcx+16]


	MOVDQA [rcx], xmm14
	MOVDQA [rcx+16], xmm15


	RET
H264_DefaultLumaUpdate	ENDP


ALIGN 16
H264_WeightedLumaUpdate	PROC
	MOV rax, rcx

	PCMPEQD xmm7, xmm7

	MOVDQA xmm0, xmm14
	MOVDQA xmm1, xmm14
	PUNPCKLWD xmm0, xmm12
	PUNPCKHWD xmm1, xmm12
	MOVDQA xmm2, xmm15
	MOVDQA xmm3, xmm15
	PUNPCKLWD xmm2, xmm13
	PUNPCKHWD xmm3, xmm13

	
	MOVDQU xmm8, [rdx+16]
	PSHUFD xmm9, xmm8, 55h ; o0
	PSHUFD xmm10, xmm8, 0AAh ; w1
	PSHUFD xmm11, xmm8, 0FFh ; o1
	PSHUFD xmm8, xmm8, 0 ; w0
	
	MOV ecx, [rdx+32]
	
	MOV edx, 1
	SHL edx, cl
	MOVD xmm5, edx
	PSHUFD xmm5, xmm5, 0

	
	PUNPCKLWD xmm8, xmm10
	PSHUFD xmm8, xmm8, 0

	PADDD xmm9, xmm11
	PSUBD xmm9, xmm7
	PSRAD xmm9, 1

	INC ecx

	PMADDWD xmm0, xmm8
	PMADDWD xmm1, xmm8
	PADDD xmm0, xmm5
	PADDD xmm1, xmm5
	PMADDWD xmm2, xmm8
	PMADDWD xmm3, xmm8
	PADDD xmm2, xmm5
	PADDD xmm3, xmm5

	MOVD xmm12, ecx

	PSRAD xmm0, xmm12
	PSRAD xmm1, xmm12
	PSRAD xmm2, xmm12
	PSRAD xmm3, xmm12

	PADDD xmm0, xmm9
	PADDD xmm1, xmm9
	PADDD xmm2, xmm9
	PADDD xmm3, xmm9

	PACKSSDW xmm0, xmm1
	PACKSSDW xmm2, xmm3

	PADDW xmm0, [rax]
	PADDW xmm2, [rax+16]


	MOVDQA [rax], xmm0
	MOVDQA [rax+16], xmm2


	RET
H264_WeightedLumaUpdate	ENDP


ALIGN 16
H264_DefaultChromaUpdate	PROC

	PCMPEQW xmm7, xmm7

	PADDW xmm10, xmm8		
	PSUBW xmm10, xmm7
	PSRAW xmm10, 1
		
		
	MOVD rax, xmm10

	ADD [rcx], ax	
	SHR rax, 16
	ADD [rcx+2], ax
	SHR rax, 16
	ADD [rcx+8], ax
	SHR rax, 16
	ADD [rcx+10], ax
	

	RET
H264_DefaultChromaUpdate	ENDP


ALIGN 16
H264_WeightedChromaUpdate	PROC
	MOV rax, rcx	

	PCMPEQD xmm7, xmm7
	PUNPCKLWD xmm10, xmm8

	MOVD xmm6, DWORD PTR [rdx]
	PSHUFLW xmm6, xmm6, 0
	PUNPCKLQDQ xmm6, xmm6

	PADDW xmm10, xmm6

	MOVDQU xmm0, [rdx+16]
	PSHUFD xmm1, xmm0, 55h ; o0
	PSHUFD xmm2, xmm0, 0AAh ; w1
	PSHUFD xmm3, xmm0, 0FFh ; o1
	PSHUFD xmm0, xmm0, 0 ; w0
	
	MOV ecx, [rdx+32]
	
	MOV edx, 1
	SHL edx, cl
	MOVD xmm5, edx
	PSHUFD xmm5, xmm5, 0
	
	PUNPCKLWD xmm0, xmm2
	PSHUFD xmm0, xmm0, 0

	PADDD xmm1, xmm3
	PSUBD xmm1, xmm7
	PSRAD xmm1, 1

	INC ecx

	MOVD xmm4, ecx

	PMADDWD xmm10, xmm0
	PADDD xmm10, xmm5
	PSRAD xmm10, xmm4
	PADDD xmm10, xmm1

	PACKSSDW xmm10, xmm10
	PSUBW xmm10, xmm6

	MOVD rcx, xmm10

	ADD [rax], cx
	SHR rcx, 16
	ADD [rax+2], cx
	SHR rcx, 16
	ADD [rax+8], cx
	SHR rcx, 16
	ADD [rax+10], cx


	RET
H264_WeightedChromaUpdate	ENDP





ALIGN 16
H264_InterpolateChroma	PROC
	XCHG r8, rcx
	MOV rax, rcx
	NEG rax
	OR rcx, rax
	SAR rcx, 32

	MOV r10, 0800000008h
	MOVD xmm7, r10	
	MOVD xmm6, r9 ; xy

	PSHUFD xmm5, xmm7, 0
	PSLLD xmm5, 2 ; 32

	PSUBD xmm7, xmm6 ; 8x, 8y
	PUNPCKLQDQ xmm7, xmm6
	PSHUFD xmm6, xmm7, 088h ; 8x x 8x x
	PSHUFD xmm7, xmm7, 0F5h ; 8y 8y y y

	PACKSSDW xmm7, xmm7
	PACKSSDW xmm6, xmm6

	PMULLW xmm6, xmm7 ; 8x8y x8y y8x xy
		
	MOV eax, [r8]



		
	TEST eax, 0F0h
	JNZ border_block

		TEST eax, 2
		JZ no_right_line
			TEST eax, 8
			JNZ bottom_right
			; right line

				MOVZX r9, WORD PTR [r8+4]
				MOVZX r10, WORD PTR [r8+6]

				MOV cl, 8
				SUB cl, r9b

				SHL r10, 4
				ADD rdx, r10			

				LEA rax, [rdx + r9*2]
				LEA rdx, [rdx + 2048]


				PINSRW xmm0, WORD PTR [rax], 0
				PINSRW xmm1, WORD PTR [rax+16], 0
				PINSRW xmm2, WORD PTR [rax+32], 0

				ADD rax, 2
				DEC cl
				CMOVZ rax, rdx

				PINSRW xmm0, WORD PTR [rax], 1
				PINSRW xmm1, WORD PTR [rax+16], 1
				PINSRW xmm2, WORD PTR [rax+32], 1

				ADD rax, 2
				DEC cl
				CMOVZ rax, rdx

				PINSRW xmm0, WORD PTR [rax], 2
				PINSRW xmm1, WORD PTR [rax+16], 2
				PINSRW xmm2, WORD PTR [rax+32], 2
				
			JMP interpolate_proc
		align 16
			bottom_right:
				MOVZX r9, WORD PTR [r8+4]
				MOVZX r10, WORD PTR [r8+6]

				MOV cl, 8
				SUB cl, r9b

				SHL r10, 4
				ADD rdx, r10			

				LEA rax, [rdx + r9*2]
				LEA rdx, [rdx + 2048]

				MOV r11d, [r8+36]

				MOV r8, [r8+8]
				SUB r8, r10
				
				XOR r9, r9				
				

				SUB r10, r11
				NEG r10				
								
				CMOVNC r10, r8
				RCL r9, 1
				XOR r9, 1
				SHL r9, 4
				MOV r11, r8
				ADD r11, r9


				PINSRW xmm0, WORD PTR [rax], 0
				PINSRW xmm1, WORD PTR [rax+r10], 0
				PINSRW xmm2, WORD PTR [rax+r11], 0

				ADD rax, 2
				DEC cl
				CMOVZ rax, rdx

				PINSRW xmm0, WORD PTR [rax], 1
				PINSRW xmm1, WORD PTR [rax+r10], 1
				PINSRW xmm2, WORD PTR [rax+r11], 1

				ADD rax, 2
				DEC cl
				CMOVZ rax, rdx

				PINSRW xmm0, WORD PTR [rax], 2
				PINSRW xmm1, WORD PTR [rax+r10], 2
				PINSRW xmm2, WORD PTR [rax+r11], 2


			JMP interpolate_proc

	align 16
		no_right_line:
			TEST eax, 8
			JZ inner_block
			; bottom line
				
				MOVZX r9, WORD PTR [r8+4]
				MOVZX r10, WORD PTR [r8+6]

				MOV cx, [r8+36]
				ADD cx, 16
				SHR cx, 4				
				SUB cx, r10w

				SHL r10, 4

				LEA rdx, [rdx + r9*2]
				LEA rax, [rdx + r10]
				ADD rdx, [r8+8]


				MOVD xmm0, QWORD PTR [rax]
				ADD rax, 16

				DEC cl
				CMOVZ rax, rdx

				MOVD xmm1, QWORD PTR [rax]
				ADD rax, 16

				DEC cl
				CMOVZ rax, rdx

				MOVD xmm2, QWORD PTR [rax]



			JMP interpolate_proc
		align 16
			inner_block:
				MOVZX r9, WORD PTR [r8+4]
				MOVZX r10, WORD PTR [r8+6]

				SHL r10, 4
				LEA rdx, [rdx + r9*2]
				ADD rdx, r10

				MOVD xmm0, QWORD PTR [rdx]
				MOVD xmm1, QWORD PTR [rdx+16]
				MOVD xmm2, QWORD PTR [rdx+32]

			
				JMP interpolate_proc












align 16
	border_block:
		TEST eax, 10h
		JZ no_left_border

		; left-top cornder
		; left bottom corner
		; left border bottom line
		; left border

			TEST eax, 40h
			JZ no_top_left_corner
				XOR r10, r10
				MOV r11, 16
				CMP WORD PTR [r8+6], 1
				CMOVG r11, r10

				JMP left_border_calc
		align 16
			no_top_left_corner:
				TEST eax, 80h
				JZ left_border
					MOVZX rax, WORD PTR [r8+6]
																	
					SHL rax, 4
					ADD rdx, rax
					
					MOV r10, 16
					XOR r11, r11
					
					CMP eax, [r8+36]
					CMOVGE r10, r11
	
				JMP left_border_calc

			align 16
				left_border:
					TEST eax, 08h
					JZ no_lb_bottom_line
						MOVZX rax, WORD PTR [r8+6]								
																		
						SHL rax, 4
						ADD rdx, rax
												
						CMP eax, [r8+36]
						JGE lb_next_line
							MOV r10, 16

							MOV r11, [r8+8]
							SUB r11, rax

							JMP left_border_calc
						lb_next_line:
							MOV r10, [r8+8]
							SUB r10, rax

							LEA r11, [r10+16]

						JMP left_border_calc
				align 16
					no_lb_bottom_line:
						MOVZX rax, WORD PTR [r8+6]
						SHL rax, 4
						ADD rdx, rax

						MOV r10, 16
						MOV r11, 32
											
					align 16
						left_border_calc:

							MOV cx, [r8+4]

							XOR rax, rax
							MOV r9, 2

							
							PINSRW xmm0, WORD PTR [rdx], 0
							PINSRW xmm1, WORD PTR [rdx+r10], 0
							PINSRW xmm2, WORD PTR [rdx+r11], 0

							ADD rdx, rax
							DEC cl
							CMOVZ rax, r9

							PINSRW xmm0, WORD PTR [rdx], 1
							PINSRW xmm1, WORD PTR [rdx+r10], 1
							PINSRW xmm2, WORD PTR [rdx+r11], 1
														
							ADD rdx, rax

							PINSRW xmm0, WORD PTR [rdx], 2
							PINSRW xmm1, WORD PTR [rdx+r10], 2
							PINSRW xmm2, WORD PTR [rdx+r11], 2
							

							JMP interpolate_proc





	align 16
		no_left_border:
			TEST eax, 40h
			JZ no_top_border
				TEST eax, 20h
				JZ no_top_right_corner
					XOR r10, r10
					MOV r11, 16
					CMP WORD PTR [r8+6], 1
					CMOVA r11, r10

					JMP right_border_calc
		align 16
			no_top_right_corner:

				TEST eax, 02h
				JZ no_tb_right_line
					MOVZX r10, WORD PTR [r8+4]
					MOV cl, 8
					SUB cl, r10b

					LEA rax, [rdx + 2048]
					LEA rdx, [rdx + r10*2]

					XOR r10, r10
					MOV r11, 16

					CMP WORD PTR [r8+6], 1
					CMOVA r11, r10

					PINSRW xmm0, WORD PTR [rdx], 0
					PINSRW xmm1, WORD PTR [rdx+r10], 0
					PINSRW xmm2, WORD PTR [rdx+r11], 0
					ADD rdx, 2

					DEC cl
					CMOVZ rdx, rax

					PINSRW xmm0, WORD PTR [rdx], 1
					PINSRW xmm1, WORD PTR [rdx+r10], 1
					PINSRW xmm2, WORD PTR [rdx+r11], 1
					ADD rdx, 2

					DEC cl
					CMOVZ rdx, rax

					PINSRW xmm0, WORD PTR [rdx], 2
					PINSRW xmm1, WORD PTR [rdx+r10], 2
					PINSRW xmm2, WORD PTR [rdx+r11], 2


					JMP interpolate_proc

				align 16
					no_tb_right_line:
						MOVZX rax, WORD PTR [r8+4]
						LEA rdx, [rdx + rax*2]

						MOV cx, [r8+6]

						XOR r10, r10
						MOV r11, 16


						MOVD xmm0, QWORD PTR [rdx]

						DEC cl
						CMOVS r10, r11
						ADD rdx, r10

						MOVD xmm1, QWORD PTR [rdx]

						DEC cl
						CMOVS r10, r11
						ADD rdx, r10

						MOVD xmm2, QWORD PTR [rdx]



						JMP interpolate_proc










	align 16
		no_top_border:

			TEST eax, 20h
			JZ no_right_border
		
				TEST eax, 80h
				JZ right_border
				; bottom-right corner

					MOVZX rax, WORD PTR [r8+6]

					SHL rax, 4
					ADD rdx, rax

					
					MOV r10, 16
					XOR r11, r11
					CMP eax, [r8+36]
					CMOVAE r10, r11
								
				
				JMP right_border_calc

			align 16
				right_border:
					TEST eax, 08h
					JZ no_rb_bottom_line
						MOVZX rax, WORD PTR [r8+6]
																		
						SHL rax, 4
						ADD rdx, rax
												
						CMP eax, [r8+36]
						JAE rb_next_line
							MOV r10, 16

							MOV r11, [r8+8]
							SUB r11, rax

							JMP right_border_calc
						rb_next_line:
							MOV r10, [r8+8]
							SUB r10, rax

							LEA r11, [r10+16]

						JMP right_border_calc
				align 16
					no_rb_bottom_line:
						MOVZX rax, WORD PTR [r8+6]
						SHL rax, 4
						ADD rdx, rax

						MOV r10, 16
						MOV r11, 32
											
					align 16
						right_border_calc:
							MOVZX rax, WORD PTR [r8+4]
							LEA rdx, [rdx+rax*2]

							MOV cx, 8
							SUB cx, ax


							MOV rax, 2
							XOR r9, r9

						
							PINSRW xmm0, WORD PTR [rdx], 0
							PINSRW xmm1, WORD PTR [rdx+r10], 0
							PINSRW xmm2, WORD PTR [rdx+r11], 0
																					
							DEC cl
							CMOVZ rax, r9
							ADD rdx, rax

							PINSRW xmm0, WORD PTR [rdx], 1
							PINSRW xmm1, WORD PTR [rdx+r10], 1
							PINSRW xmm2, WORD PTR [rdx+r11], 1
														
							DEC cl
							CMOVZ rax, r9
							ADD rdx, rax

							PINSRW xmm0, WORD PTR [rdx], 2
							PINSRW xmm1, WORD PTR [rdx+r10], 2
							PINSRW xmm2, WORD PTR [rdx+r11], 2
							

							JMP interpolate_proc






	align 16
		no_right_border: ; bottom border
			TEST eax, 02h
			JZ no_bb_right_line
				MOVZX r10, WORD PTR [r8+4]
				MOVZX r11, WORD PTR [r8+6]

				SHL r11, 4
				ADD rdx, r11

				LEA rax, [rdx + 2048]
				LEA rdx, [rdx + r10*2]

				MOV cx, 8
				SUB cx, r10w
				
				MOVZX r10, WORD PTR [r8+36]
				SUB r10, r11
				XOR r11, r11


				PINSRW xmm0, WORD PTR [rdx], 0
				PINSRW xmm1, WORD PTR [rdx+r10], 0
				PINSRW xmm2, WORD PTR [rdx+r11], 0
				ADD rdx, 2

				DEC cx
				CMOVZ rdx, rax

				PINSRW xmm0, WORD PTR [rdx], 1
				PINSRW xmm1, WORD PTR [rdx+r10], 1
				PINSRW xmm2, WORD PTR [rdx+r11], 1
				ADD rdx, 2

				DEC cx
				CMOVZ rdx, rax

				PINSRW xmm0, WORD PTR [rdx], 2
				PINSRW xmm1, WORD PTR [rdx+r10], 2
				PINSRW xmm2, WORD PTR [rdx+r11], 2
				
			
				JMP interpolate_proc

		align 16
			no_bb_right_line:
				MOVZX r9, WORD PTR [r8+4]
				MOVZX r10, WORD PTR [r8+6]

				SHL r10, 4

				LEA rdx, [rdx + r9*2]
				ADD rdx, r10

				MOVZX rax, WORD PTR [r8+36]
				SUB rax, r10
				XOR r9, r9

				MOV cx, ax
				SHR cx, 4

				MOVD xmm0, QWORD PTR [rdx]
				
				DEC cx
				CMOVS rax, r9
				ADD rdx, rax

				MOVD xmm1, QWORD PTR [rdx]
				
				DEC cx
				CMOVS rax, r9
				ADD rdx, rax

				MOVD xmm2, QWORD PTR [rdx]


							
			
				

	align 16
		interpolate_proc:
			
			MOVDQA xmm8, xmm0
			PUNPCKLDQ xmm8, xmm1
			PSRLDQ xmm0, 2

			MOVDQA xmm9, xmm1
			PUNPCKLDQ xmm9, xmm2
			PSRLDQ xmm1, 2

			PSRLDQ xmm2, 2

			PUNPCKLDQ xmm0, xmm1
			PUNPCKLDQ xmm1, xmm2
						
			PUNPCKLQDQ xmm8, xmm0
			PUNPCKLQDQ xmm9, xmm1

			PMADDWD xmm8, xmm6
			PMADDWD xmm9, xmm6

			PHADDD xmm8, xmm9
			PADDD xmm8, xmm5
			PSRAD xmm8, 6

			PACKSSDW xmm8, xmm8

			SHR rcx, 32
			JNZ ref0_isset
				MOVDQA xmm10, xmm8				

			ref0_isset:


	RET
H264_InterpolateChroma	ENDP







ALIGN 16
H264_InterpolateLuma	PROC

	XCHG r8, rcx
	MOV rax, rcx
	NEG rax
	OR rcx, rax
	SAR rcx, 32



	MOV eax, [r8]
		
	TEST eax, 0F0h
	JNZ border_block

			TEST eax, 01h
			JZ no_left_line
				TEST eax, 04h
				JZ no_left_top_node
					; left-top node
								
					
					
					MOVZX rax, WORD PTR [r8+6] ; y
					MOV r10b, 2
					SUB r10b, al
					
					ADD rax, 14
					SHL rax, 5
					ADD rax, rdx
					SUB rax, [r8+8]
			
					JMP left_node_build
			align 16
				no_left_top_node:
					TEST eax, 08h
					JZ no_bottom_left_node
						; bottom-left node							

							MOVZX rax, WORD PTR [r8+6] ; y
							
							SUB rax, 2
							MOV r10b, 16
							SUB r10b, al

							SHL rax, 5
							ADD rax, rdx ; current

							ADD rdx, [r8+8] ; next
						
							JMP left_node_build

						align 16
							no_bottom_left_node:								
								MOVZX rax, WORD PTR [r8+6] ; y
							
								SUB rax, 2

								SHL rax, 5
								LEA rax, [rax + rdx] ; current
								MOV rdx, rax

								XOR r10, r10
					align 16
						left_node_build:
							MOVZX r11, WORD PTR [r8+4] ; x

							MOV cl, r10b
							MOV ch, cl

							MOV r10, rax
														

							MOVDQA xmm0, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm1, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm2, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm3, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm4, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm5, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm6, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm7, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm8, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx


							SUB r10, 2048
							SUB rdx, 2048



							MOV rax, r10
							MOV cl, ch
							
							PSLLDQ xmm0, 2
							PINSRW xmm0, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm1, 2
							PINSRW xmm1, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm2, 2
							PINSRW xmm2, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm3, 2
							PINSRW xmm3, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm4, 2
							PINSRW xmm4, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm5, 2
							PINSRW xmm5, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm6, 2
							PINSRW xmm6, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm7, 2
							PINSRW xmm7, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							PSLLDQ xmm8, 2
							PINSRW xmm8, WORD PTR [rax + 30], 0
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx


							
							AND r11d, r11d
							JNZ no_left_node_xadjust
								MOV rax, r10
								MOV cl, ch
							
								PSLLDQ xmm0, 2
								PINSRW xmm0, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm1, 2
								PINSRW xmm1, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm2, 2
								PINSRW xmm2, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm3, 2
								PINSRW xmm3, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm4, 2
								PINSRW xmm4, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm5, 2
								PINSRW xmm5, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm6, 2
								PINSRW xmm6, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm7, 2
								PINSRW xmm7, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								PSLLDQ xmm8, 2
								PINSRW xmm8, WORD PTR [rax + 28], 0
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx



							no_left_node_xadjust:
												
							LEA rax, [r10 + r11*2 + 2048 + 12]
							LEA rdx, [rdx + r11*2 + 2048 + 12]

							MOV cl, ch

							MOV r9w, [rax]
							ROR r9, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r9w, [rax]
							ROR r9, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r9w, [rax]
							ROR r9, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r9w, [rax]
							ROR r9, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx


							MOV r10w, [rax]
							ROR r10, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r10w, [rax]
							ROR r10, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r10w, [rax]
							ROR r10, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOV r10w, [rax]
							ROR r10, 16
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx


							MOVZX r11, WORD PTR [rax]

						JMP QWORD PTR [r8+40]



		align 16
			no_left_line:
				TEST eax, 02h
				JZ no_right_line
					TEST eax, 04h
					JZ no_top_right_node
						; top-right					

							MOVZX rax, WORD PTR [r8+6] ; y
							MOV r11b, 2
							SUB r11b, al

							ADD rax, 14
							SHL rax, 5

							ADD rax, rdx
							SUB rax, [r8+8] ; prev
							
							

						JMP right_node_build
				align 16
					no_top_right_node:
						TEST eax, 08h
						JZ no_bottom_right_node
							; bottom-right


							MOVZX rax, WORD PTR [r8+6] ; y

							SUB rax, 2

							MOV r11b, 16
							SUB r11b, al

							SHL rax, 5

							ADD rax, rdx ; current
							ADD rdx, [r8+8] ; next
						
							JMP right_node_build

						align 16
							no_bottom_right_node:
								
								MOVZX rax, WORD PTR [r8+6] ; y

								SUB rax, 2
								SHL rax, 5															

								LEA rax, [rax+rdx] ; current
								MOV rdx, rax

								XOR r11, r11


					align 16
						right_node_build:
							MOVZX r10, WORD PTR [r8+4] ; x
							ADD r10b, 6


							MOV cl, r11b
							MOV ch, cl

							MOV r11, rax
														

							MOVDQA xmm0, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm1, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm2, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm3, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm4, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm5, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm6, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm7, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm8, [rax+16]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							

							ADD r11, 2048
							ADD rdx, 2048
							

							AND r10b, 0Fh
							JZ line_most_right														
								
						align 16
								right_node_adjust_loop:
									MOV rax, r11
									MOV cl, ch

									PSRLDQ xmm0, 2
									PINSRW xmm0, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm1, 2
									PINSRW xmm1, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm2, 2
									PINSRW xmm2, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm3, 2
									PINSRW xmm3, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm4, 2
									PINSRW xmm4, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm5, 2
									PINSRW xmm5, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm6, 2
									PINSRW xmm6, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm7, 2
									PINSRW xmm7, WORD PTR [rax], 7
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									PSRLDQ xmm8, 2
									PINSRW xmm8, WORD PTR [rax], 7

									ADD r11, 2
									ADD rdx, 2

								DEC r10b
								JNZ right_node_adjust_loop

						align 16
							line_most_right:
															
								MOV rax, r11
								MOV cl, ch

								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx


								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx

								MOVZX r11, WORD PTR [rax]


				
							JMP QWORD PTR [r8+40]











				align 16
					no_right_line:
						TEST eax, 04h
						JZ no_top_line
							; top
											
							MOVZX r10, WORD PTR [r8+4]
							SUB r10, 2
							LEA rdx, [rdx+r10*2]
														
							MOVZX rax, WORD PTR [r8+6] ; y
							MOV r11b, 2
							SUB r11b, al
													

							ADD rax, 14
							SHL rax, 5
							ADD rax, rdx
							SUB rax, [r8+8]
							
							JMP horizontal_line_build
					align 16
						no_top_line:
							TEST eax, 08h
							JZ inside_part
								; bottom

									MOVZX r10, WORD PTR [r8+4]
									SUB r10, 2
									LEA rdx, [rdx+r10*2]
														
									MOVZX rax, WORD PTR [r8+6]
									SUB rax, 2
									MOV r11b, 16
									SUB r11b, al

									SHL rax, 5
									ADD rax, rdx													

									ADD rdx, [r8+8]
									


							align 16
								horizontal_line_build:
									MOV cl, r11b
									MOV ch, cl

									MOV r11, rax
																		

									MOVDQU xmm0, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm1, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm2, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm3, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm4, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm5, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm6, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm7, [rax]
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOVDQU xmm8, [rax]


									MOV rax, r11
									MOV cl, ch

									MOV r9w, [rax+16]
									ROR r9, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r9w, [rax+16]
									ROR r9, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r9w, [rax+16]
									ROR r9, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r9w, [rax+16]
									ROR r9, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx


									MOV r10w, [rax+16]
									ROR r10, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r10w, [rax+16]
									ROR r10, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r10w, [rax+16]
									ROR r10, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx
									MOV r10w, [rax+16]
									ROR r10, 16
									ADD rax, 32
									DEC cl
									CMOVZ rax, rdx

									MOVZX r11, WORD PTR [rax+16]


								JMP QWORD PTR [r8+40]
				align 16
					inside_part:
						MOVZX r10, WORD PTR [r8+4]
						SUB r10, 2
						MOVZX rax, WORD PTR [r8+6]
						SUB rax, 2

						SHL rax, 5
						ADD rdx, rax
						LEA rdx, [rdx+r10*2]

						MOVDQU xmm0, [rdx]
						MOVDQU xmm1, [rdx+32]
						MOVDQU xmm2, [rdx+64]
						MOVDQU xmm3, [rdx+96]
						MOVDQU xmm4, [rdx+128]
						MOVDQU xmm5, [rdx+160]
						MOVDQU xmm6, [rdx+192]
						MOVDQU xmm7, [rdx+224]
						MOVDQU xmm8, [rdx+256]

						MOV r9w, [rdx+16]
						ROR r9, 16
						MOV r9w, [rdx+48]
						ROR r9, 16
						MOV r9w, [rdx+80]
						ROR r9, 16
						MOV r9w, [rdx+112]
						ROR r9, 16

						MOV r10w, [rdx+144]
						ROR r10, 16
						MOV r10w, [rdx+176]
						ROR r10, 16
						MOV r10w, [rdx+208]
						ROR r10, 16
						MOV r10w, [rdx+240]
						ROR r10, 16

						MOVZX r11, WORD PTR [rdx+272]

					JMP QWORD PTR [r8+40]


	















align 16
border_block:

	TEST eax, 010h
	JZ no_left_border
		TEST eax, 040h
		JZ no_left_top_corner
			; left-top corner
				
			MOV r11w, 2
			SUB r11w, [r8+6]

			XOR rax, rax
			MOV r9, 32
			

			JMP left_corner_build

	align 16
		no_left_top_corner:
			TEST eax, 080h
			JZ no_bottom_left_corner
				; bottom-left corner
										
					MOVZX rax, WORD PTR [r8+6] ; y
					SUB rax, 2

					CMP ax, 15
					JBE no_out_of_border
						MOV ax, 15
												
					no_out_of_border:		
					
					MOV r11w, 15
					SUB r11w, ax

					SHL rax, 5
					ADD rdx, rax ; current
										
					MOV rax, 32
					XOR r9, r9
							
					TEST r11w, r11w
					CMOVZ rax, r9

			align 16
				left_corner_build:
					MOV r10w, 2
					SUB r10w, [r8+4] ; x


					MOV cl, r11b
					MOV ch, cl

					MOV r11, rax
					
					MOVDQA xmm0, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9					
					MOVDQA xmm1, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm2, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm3, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm4, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm5, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm6, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm7, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQA xmm8, [rdx]
					DEC cl

					CMP r10b, 7
					JBE left_corner_adjust
						MOV r10b, 7

				align 16
					left_corner_adjust:
						PSLLDQ xmm0, 2
						PSHUFLW xmm0, xmm0, 0E5h
						PSLLDQ xmm1, 2
						PSHUFLW xmm1, xmm1, 0E5h
						PSLLDQ xmm2, 2
						PSHUFLW xmm2, xmm2, 0E5h
						PSLLDQ xmm3, 2
						PSHUFLW xmm3, xmm3, 0E5h
						PSLLDQ xmm4, 2
						PSHUFLW xmm4, xmm4, 0E5h
						PSLLDQ xmm5, 2
						PSHUFLW xmm5, xmm5, 0E5h
						PSLLDQ xmm6, 2
						PSHUFLW xmm6, xmm6, 0E5h
						PSLLDQ xmm7, 2
						PSHUFLW xmm7, xmm7, 0E5h
						PSLLDQ xmm8, 2
						PSHUFLW xmm8, xmm8, 0E5h
						
						SUB rdx, 2	
					DEC r10b
					JNZ left_corner_adjust
																		
						LEA rdx, [rdx + 16]

						ROR rcx, 16
						MOV cx, [rdx]
						ROL rcx, 16
						
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r10w, [rdx]
						ROL r10, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r10w, [rdx]
						ROL r10, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r10w, [rdx]
						ROL r10, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r10w, [rdx]
						ROL r10, 16

						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r9w, [rdx]
						ROL r9, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r9w, [rdx]
						ROL r9, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r9w, [rdx]
						ROL r9, 16
						INC cl
						CMOVZ rax, r11
						SUB rdx, rax
						MOV r9w, [rdx]
						ROL r9, 16



						ROR rcx, 16
						MOVZX r11, cx
						ROL rcx, 16

					
			JMP QWORD PTR [r8+40]
	
		align 16
			no_bottom_left_corner:				
				TEST eax, 04h ; top line
				JZ no_lb_top_line
					MOVSX rax, WORD PTR [r8+6]
					MOV cl, 2
					SUB cl, al
					MOV ch, cl

					ADD rax, 14
					SHL rax, 5

					SUB rax, [r8+8]
					ADD rax, rdx

					JMP lebo_calc
			align 16
				no_lb_top_line:
					TEST eax, 08h
					JZ left_border
						MOVSX rax, WORD PTR [r8+6]
						MOV cl, 18
						SUB cl, al						
						MOV ch, cl

						SUB rax, 2
						SHL rax, 5

						ADD rax, rdx

						ADD rdx, [r8+8]


						JMP lebo_calc

				align 16
					left_border:
						XOR cx, cx											
						
						MOVSX rax, WORD PTR [r8+6]
						SUB rax, 2
						SHL rax, 5

						ADD rax, rdx

						
					align 16
						lebo_calc:
							MOV r10w, 2
							SUB r10w, [r8+4]

							MOV r11, rax

							MOVDQA xmm0, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							
							MOVDQA xmm1, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm2, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm3, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm4, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm5, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm6, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm7, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx

							MOVDQA xmm8, [rax]							
							DEC cl
							
							XOR rax, rax

							CMP r10b, 7
							JBE lebo_adjust
								MOV r10b, 7

						align 16
							lebo_adjust:
								PSLLDQ xmm0, 2
								PSHUFLW xmm0, xmm0, 0E5h
								PSLLDQ xmm1, 2
								PSHUFLW xmm1, xmm1, 0E5h
								PSLLDQ xmm2, 2
								PSHUFLW xmm2, xmm2, 0E5h
								PSLLDQ xmm3, 2
								PSHUFLW xmm3, xmm3, 0E5h
								PSLLDQ xmm4, 2
								PSHUFLW xmm4, xmm4, 0E5h
								PSLLDQ xmm5, 2
								PSHUFLW xmm5, xmm5, 0E5h
								PSLLDQ xmm6, 2
								PSHUFLW xmm6, xmm6, 0E5h
								PSLLDQ xmm7, 2
								PSHUFLW xmm7, xmm7, 0E5h
								PSLLDQ xmm8, 2
								PSHUFLW xmm8, xmm8, 0E5h

								
								SUB rax, 2
							DEC r10b
							JNZ lebo_adjust

								LEA rdx, [rdx + rax + 16]
								LEA rax, [r11 + rax + 16]

								SHR cx, 8

								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r9w, [rax]
								ROR r9, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx

								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx
								MOV r10w, [rax]
								ROR r10, 16
								ADD rax, 32
								DEC cl
								CMOVZ rax, rdx

								MOVSX r11, WORD PTR [rax]

				JMP QWORD PTR [r8+40]


align 16
	no_left_border:
		TEST eax, 020h
		JZ no_right_border
			TEST eax, 040h
			JZ no_top_right_corner
				; top-right corner
													
					MOV r11w, 2										
					SUB r11w, [r8+6]

					XOR rax, rax
					MOV r9, 32

					ADD rdx, 16

				JMP right_corner_build
		align 16
			no_top_right_corner:
				TEST eax, 080h
				JZ no_bottom_right_corner
					; bottom-right corner


						MOVZX rax, WORD PTR [r8+6]
						SUB rax, 2

						CMP ax, 15
						JBE no_out_of_border3
							MOV ax, 15
						no_out_of_border3:

						MOV r11w, 15
						SUB r11w, ax
												
						SHL rax, 5
						LEA rdx, [rdx+rax+16]
						
						MOV rax, 32
						XOR r9, r9
						
						TEST r11w, r11w
						CMOVZ rax, r9
							
				align 16
					right_corner_build:
						MOVZX r10, WORD PTR [r8+4] ; x
						ADD r10b, 6

						MOV cl, r11b
						MOV r11, rax


						MOVDQA xmm0, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm1, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm2, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm3, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm4, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm5, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm6, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm7, [rdx]
						ADD rdx, rax
						DEC cl
						CMOVZ rax, r9
						MOVDQA xmm8, [rdx]
						DEC cl

						AND r10, 0Fh
						JZ corner_most_right
							CMP r10b, 7
							JBE right_corner_adjust_loop
								MOV r10b, 7

					align 16
						right_corner_adjust_loop:
							PSRLDQ xmm0, 2
							PSHUFHW xmm0, xmm0, 0A4h
							PSRLDQ xmm1, 2
							PSHUFHW xmm1, xmm1, 0A4h
							PSRLDQ xmm2, 2
							PSHUFHW xmm2, xmm2, 0A4h
							PSRLDQ xmm3, 2
							PSHUFHW xmm3, xmm3, 0A4h
							PSRLDQ xmm4, 2
							PSHUFHW xmm4, xmm4, 0A4h
							PSRLDQ xmm5, 2
							PSHUFHW xmm5, xmm5, 0A4h
							PSRLDQ xmm6, 2
							PSHUFHW xmm6, xmm6, 0A4h
							PSRLDQ xmm7, 2
							PSHUFHW xmm7, xmm7, 0A4h
							PSRLDQ xmm8, 2
							PSHUFHW xmm8, xmm8, 0A4h


						DEC r10b
						JNZ right_corner_adjust_loop

					align 16
						corner_most_right:
							ROR rcx, 16
							MOV cx, [rdx+14]
							ROL rcx, 16
														
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r10w, [rdx+14]
							ROL r10, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r10w, [rdx+14]
							ROL r10, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r10w, [rdx+14]
							ROL r10, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r10w, [rdx+14]
							ROL r10, 16

							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r9w, [rdx+14]
							ROL r9, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r9w, [rdx+14]
							ROL r9, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r9w, [rdx+14]
							ROL r9, 16
							INC cl
							CMOVZ rax, r11
							ADD rdx, rax
							MOV r9w, [rdx+14]
							ROL r9, 16


							ROR rcx, 16
							MOVZX r11, cx
							ROL rcx, 16


				JMP QWORD PTR [r8+40]
			align 16
				no_bottom_right_corner:
					TEST eax, 04h ; top line
					JZ no_rb_top_line
						MOVZX rax, WORD PTR [r8+6]
						MOV cl, 2
						SUB cl, al
						MOV ch, cl

						ADD rax, 14
						SHL rax, 5

						SUB rax, [r8+8]
						LEA rax, [rax+rdx+16]						

						JMP ribo_calc
				align 16
					no_rb_top_line:
						TEST eax, 08h
						JZ right_border
							MOVZX rax, WORD PTR [r8+6]
							SUB rax, 2

							MOV cl, 16
							SUB cl, al	
							MOV ch, cl
														
							SHL rax, 5

							LEA rax, [rdx+rax+16]
							ADD rdx, [r8+8]
							

							JMP ribo_calc

					align 16
						right_border:
							XOR cx, cx	
														
							MOVZX rax, WORD PTR [r8+6]
							SUB rax, 2
							SHL rax, 5

							LEA rax, [rdx+rax+16]

					align 16
						ribo_calc:
							MOVZX r10, WORD PTR [r8+4]
							ADD r10b, 6

							LEA rdx, [rdx+16]
							MOV r11, rax


							MOVDQA xmm0, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm1, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm2, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm3, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm4, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm5, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm6, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm7, [rax]
							ADD rax, 32
							DEC cl
							CMOVZ rax, rdx
							MOVDQA xmm8, [rax]


					AND r10, 0Fh
					JZ border_most_right
						CMP r10b, 7
						JBE adjust_rborder_loop
							MOV r10b, 7
				align 16
					adjust_rborder_loop:
						PSRLDQ xmm0, 2
						PSHUFHW xmm0, xmm0, 0A4h
						PSRLDQ xmm1, 2
						PSHUFHW xmm1, xmm1, 0A4h
						PSRLDQ xmm2, 2
						PSHUFHW xmm2, xmm2, 0A4h
						PSRLDQ xmm3, 2
						PSHUFHW xmm3, xmm3, 0A4h
						PSRLDQ xmm4, 2
						PSHUFHW xmm4, xmm4, 0A4h
						PSRLDQ xmm5, 2
						PSHUFHW xmm5, xmm5, 0A4h
						PSRLDQ xmm6, 2
						PSHUFHW xmm6, xmm6, 0A4h
						PSRLDQ xmm7, 2
						PSHUFHW xmm7, xmm7, 0A4h
						PSRLDQ xmm8, 2
						PSHUFHW xmm8, xmm8, 0A4h
							
					DEC	r10b
					JNZ adjust_rborder_loop
			align 16
				border_most_right:
					LEA rax, [r11+14]
					LEA rdx, [rdx+14]

					SHR cx, 8
					
					MOV r9w, [rax]
					ROR r9, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r9w, [rax]
					ROR r9, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r9w, [rax]
					ROR r9, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r9w, [rax]
					ROR r9, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx

					MOV r10w, [rax]
					ROR r10, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r10w, [rax]
					ROR r10, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r10w, [rax]
					ROR r10, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx
					MOV r10w, [rax]
					ROR r10, 16
					ADD rax, 32
					DEC cl
					CMOVZ rax, rdx

					MOVZX r11, WORD PTR [rax]

				JMP QWORD PTR [r8+40]
		align 16
			no_right_border:
				TEST eax, 03h
				JNZ h_border_split

				TEST eax, 040h
				JZ no_top_border
					; top-border
					
					MOV cx, 2									
					SUB cx, [r8+6]
							
					XOR rax, rax
					MOV r9, 32

					JMP horizontal_border_build


			align 16
				no_top_border:
					; bottom border
											
					MOVZX rax, WORD PTR [r8+6]
					SUB rax, 2

					CMP ax, 15
					JBE no_out_of_border2
						MOV ax, 15
					no_out_of_border2:
					
					MOV cx, 15
					SUB cx, ax

					SHL rax, 5
					LEA rdx, [rdx+rax]

					MOV rax, 32
					XOR r9, r9

					TEST cx, cx
					CMOVZ rax, r9


			align 16
				horizontal_border_build:
					MOVZX r10, WORD PTR [r8+4]
					SUB r10, 2
					LEA rdx, [rdx+r10*2]

					
					MOV r11, rax

					MOV ch, cl
					
					MOVDQU xmm0, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm1, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm2, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm3, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm4, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm5, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm6, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm7, [rdx]
					ADD rdx, rax
					DEC cl
					CMOVZ rax, r9
					MOVDQU xmm8, [rdx]
					DEC cl


					ROR rcx, 16
					MOV cx, [rdx+16]
					ROL rcx, 16


					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r10w, [rdx+16]
					ROL r10, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r10w, [rdx+16]
					ROL r10, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r10w, [rdx+16]
					ROL r10, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r10w, [rdx+16]
					ROL r10, 16

					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r9w, [rdx+16]
					ROL r9, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r9w, [rdx+16]
					ROL r9, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r9w, [rdx+16]
					ROL r9, 16
					INC cl
					CMOVZ rax, r11
					SUB rdx, rax
					MOV r9w, [rdx+16]
					ROL r9, 16

					ROR rcx, 16
					MOVZX r11, cx
					ROL rcx, 16



				JMP QWORD PTR [r8+40]


		align 16
			h_border_split:
				TEST eax, 1
				JZ no_hb_left_line
					TEST eax, 040h
					JZ no_hb_top
						MOV cx, 2
						SUB cx, [r8+6]

						XOR r9, r9
						MOV r11, 32

					JMP top_line_calc
				align 16
					no_hb_top:
						MOVZX r11, WORD PTR [r8+6]
						SUB r11w, 2

						CMP r11w, 15
						JBE no_out_of_border5
							MOV r11w, 15
						no_out_of_border5:

						MOV cx, 15
						SUB cx, r11w
						MOV ch, cl

						SHL r11, 5
						LEA rdx, [rdx+r11]

						MOV r9, 32
						XOR r11, r11

						TEST cx, cx
						CMOVZ r9, r11

					align 16
						top_line_calc:
							ROR rcx, 16
							MOV cx, [r8+4]
							ROL rcx, 16
							
							MOV r10, r9

							MOVDQA xmm0, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm1, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm2, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm3, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm4, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm5, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm6, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm7, [rdx]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm8, [rdx]
							DEC cl




							MOV rax, rdx
							MOV ch, cl

							PSLLDQ xmm8, 2
							PINSRW xmm8, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm7, 2
							PINSRW xmm7, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm6, 2
							PINSRW xmm6, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm5, 2
							PINSRW xmm5, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm4, 2
							PINSRW xmm4, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm3, 2
							PINSRW xmm3, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm2, 2
							PINSRW xmm2, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm1, 2
							PINSRW xmm1, WORD PTR [rdx-2048+14], 0
							INC cl
							CMOVZ r9, r10
							SUB rdx, r9
							PSLLDQ xmm0, 2
							PINSRW xmm0, WORD PTR [rdx-2048+14], 0
							INC cl

							
							TEST rcx, 030000h
							JNZ no_hbl_adjust
								MOV cl, ch
								MOV rdx, rax

								AND cl, cl
								CMOVS r9, r11

								PSLLDQ xmm8, 2
								PINSRW xmm8, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm7, 2
								PINSRW xmm7, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm6, 2
								PINSRW xmm6, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm5, 2
								PINSRW xmm5, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm4, 2
								PINSRW xmm4, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm3, 2
								PINSRW xmm3, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm2, 2
								PINSRW xmm2, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm1, 2
								PINSRW xmm1, WORD PTR [rdx-2048+12], 0
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSLLDQ xmm0, 2
								PINSRW xmm0, WORD PTR [rdx-2048+12], 0
								INC cl

								SUB rdx, 2
							no_hbl_adjust:

								LEA rdx, [rdx + 14]
								MOV rax, r9								

								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								

								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11

								MOVSX r11, WORD PTR [rdx]

							JMP QWORD PTR [r8+40]







			align 16
				no_hb_left_line:
					TEST eax, 040h
					JZ no_hb_bottom
						MOV cx, 2
						SUB cx, [r8+6]
						
						XOR r9, r9
						MOV r11, 32

					JMP bottom_line_calc
				align 16
					no_hb_bottom:
						MOVZX r9, WORD PTR [r8+6]
						SUB r9, 2

						CMP r9w, 15
						JBE no_out_of_border4
							MOV r9w, 15
						no_out_of_border4:

						MOV cx, 15
						SUB cx, r9w

						SHL r9, 5
						ADD rdx, r9

						MOV r9, 32
						XOR r11, r11

						TEST cx, cx
						CMOVZ r9, r11

					align 16
						bottom_line_calc:
							MOVZX rax, WORD PTR [r8+4]
							ADD ax, 6
							
							MOV r10, r9

							MOVDQA xmm0, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm1, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm2, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm3, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm4, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm5, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm6, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm7, [rdx+16]
							ADD rdx, r9
							DEC cl
							CMOVZ r9, r11
							MOVDQA xmm8, [rdx+16]
							DEC cl

							ADD rdx, 2048

							AND ax, 0Fh
							JZ no_hbr_adjust
								MOV ch, cl

								ROR rcx, 16
								MOV cx, ax

								MOV rax, rdx

						align 16
							hbr_adjust:
								ROL rcx, 16

								MOV cl, ch

								MOV rdx, rax
								
								AND cl, cl
								CMOVS r9, r11
							

								PSRLDQ xmm8, 2
								PINSRW xmm8, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm7, 2
								PINSRW xmm7, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm6, 2
								PINSRW xmm6, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm5, 2
								PINSRW xmm5, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm4, 2
								PINSRW xmm4, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm3, 2
								PINSRW xmm3, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm2, 2
								PINSRW xmm2, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm1, 2
								PINSRW xmm1, WORD PTR [rdx], 7
								INC cl
								CMOVZ r9, r10
								SUB rdx, r9
								PSRLDQ xmm8, 2
								PINSRW xmm8, WORD PTR [rdx], 7
								INC cl

								ADD rax, 2

								ROR rcx, 16
								
							DEC cl
							JNZ hbr_adjust
							
								ROL rcx, 16
								ADD rdx, 2
								
						align 16
							no_hbr_adjust:
								MOV rax, r9

								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r9w, [rdx]
								ROR r9, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11


								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11
								MOV r10w, [rdx]
								ROR r10, 16
								ADD rdx, rax
								DEC cl
								CMOVZ rax, r11

								MOVSX r11, WORD PTR [rdx]

				JMP QWORD PTR [r8+40]


	RET
H264_InterpolateLuma	ENDP


ALIGN 16
H264_Interpolate_G	PROC


	PSRLDQ xmm2, 4
	PSRLDQ xmm3, 4
	PSRLDQ xmm4, 4
	PSRLDQ xmm5, 4
	
	PUNPCKLQDQ xmm2, xmm3
	PUNPCKLQDQ xmm4, xmm5

	MOVDQA xmm12, xmm2
	MOVDQA xmm13, xmm4	
	
	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:
	
	
	RET
H264_Interpolate_G	ENDP



ALIGN 16
H264_Interpolate_n	PROC

	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 4
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 4
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 4
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 4
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 4
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 4
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 4
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 4
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 4
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6
	MOVDQA xmm12, xmm1

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm12, xmm0
	PSLLD xmm0, 2
	PSUBD xmm12, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSRAD xmm12, 5



	PSUBD xmm4, xmm10
	PADDD xmm12, xmm4
	PSRAD xmm12, 1
	PADDD xmm2, xmm7
	MOVDQA xmm11, xmm2

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm11, xmm0
	PSLLD xmm0, 2
	PSUBD xmm11, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSRAD xmm11, 5





	PSUBD xmm5, xmm10
	PADDD xmm11, xmm5
	PSRAD xmm11, 1
	PADDD xmm3, xmm8
	MOVDQA xmm13, xmm3

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm13, xmm0
	PSLLD xmm0, 2
	PSUBD xmm13, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSRAD xmm13, 5




	PSUBD xmm6, xmm10
	PADDD xmm13, xmm6
	PSRAD xmm13, 1
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PSUBD xmm7, xmm10
	PADDD xmm4, xmm7
	PSRAD xmm4, 1

	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm4


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:

	

	RET
H264_Interpolate_n	ENDP



ALIGN 16
H264_Interpolate_d	PROC
	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 4
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 4
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 4
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 4
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 4
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 4
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 4
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 4
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 4
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6
	MOVDQA xmm12, xmm1

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm12, xmm0
	PSLLD xmm0, 2
	PSUBD xmm12, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSRAD xmm12, 5



	PADDD xmm12, xmm3
	PSRAD xmm12, 1


	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7
	MOVDQA xmm11, xmm2

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm11, xmm0
	PSLLD xmm0, 2
	PSUBD xmm11, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSRAD xmm11, 5


	PADDD xmm11, xmm4
	PSRAD xmm11, 1




	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8
	MOVDQA xmm13, xmm3

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm13, xmm0
	PSLLD xmm0, 2
	PSUBD xmm13, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSRAD xmm13, 5


	PADDD xmm13, xmm5
	PSRAD xmm13, 1



	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5


	PADDD xmm4, xmm6
	PSRAD xmm4, 1

	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm4


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:

	
	RET
H264_Interpolate_d	ENDP

ALIGN 16
H264_Interpolate_h	PROC
	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 4
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 4
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 4
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 4
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 4
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 4
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 4
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 4
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 4
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6
	MOVDQA xmm12, xmm1

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm12, xmm0
	PSLLD xmm0, 2
	PSUBD xmm12, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSLLD xmm0, 2
	PADDD xmm12, xmm0
	PSRAD xmm12, 5



	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7
	MOVDQA xmm11, xmm2

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm11, xmm0
	PSLLD xmm0, 2
	PSUBD xmm11, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSLLD xmm0, 2
	PADDD xmm11, xmm0
	PSRAD xmm11, 5



	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8
	MOVDQA xmm13, xmm3

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm13, xmm0
	PSLLD xmm0, 2
	PSUBD xmm13, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSLLD xmm0, 2
	PADDD xmm13, xmm0
	PSRAD xmm13, 5




	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm4



	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_h	ENDP



ALIGN 16
H264_Interpolate_b	PROC
	MOVDQA xmm9, XMMWORD PTR [tap_6]
	PCMPEQD xmm10, xmm10
	PSRLDQ xmm10, 10




	SHR r9, 32

	MOVDQA xmm12, xmm2
	MOVDQA xmm1, xmm2
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	SHR r9, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2
	PSRLDQ xmm8, 4

	PSUBW xmm12, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10
	
	PMADDWD xmm12, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm12, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm12, xmm7
	PSRAD xmm12, 5



	MOVDQA xmm0, xmm3
	MOVDQA xmm1, xmm3
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm0, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm0, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm0, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm0, xmm7
	PSRAD xmm0, 5



	MOVDQA xmm13, xmm4
	MOVDQA xmm1, xmm4
	PSRLDQ xmm1, 2
	PINSRW xmm1, r10d, 7
	SHR r10, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm13, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm13, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm13, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm13, xmm7
	PSRAD xmm13, 5



	MOVDQA xmm1, xmm5
	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7	
	MOVDQA xmm7, xmm11
	MOVDQA xmm8, xmm11
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm1, xmm10
	PSUBW xmm11, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm1, xmm9
	PMADDWD xmm11, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm1, xmm11
	PHADDD xmm7, xmm8
	PHADDD xmm1, xmm7	
	PSRAD xmm1, 5



	PACKSSDW xmm12, xmm0
	PACKSSDW xmm13, xmm1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_b	ENDP



ALIGN 16
H264_Interpolate_a	PROC
	MOVDQA xmm9, XMMWORD PTR [tap_6]
	PCMPEQD xmm10, xmm10
	PSRLDQ xmm10, 10




	SHR r9, 32

	MOVDQA xmm12, xmm2
	MOVDQA xmm1, xmm2
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	SHR r9, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2
	PSRLDQ xmm8, 4

	PSUBW xmm12, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10
	
	PMADDWD xmm12, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm12, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm12, xmm7
	PSRAD xmm12, 5



	MOVDQA xmm0, xmm3
	MOVDQA xmm1, xmm3
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm0, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm0, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm0, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm0, xmm7
	PSRAD xmm0, 5



	MOVDQA xmm13, xmm4
	MOVDQA xmm1, xmm4
	PSRLDQ xmm1, 2
	PINSRW xmm1, r10d, 7
	SHR r10, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm13, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm13, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm13, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm13, xmm7
	PSRAD xmm13, 5



	MOVDQA xmm1, xmm5
	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7	
	MOVDQA xmm7, xmm11
	MOVDQA xmm8, xmm11
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm1, xmm10
	PSUBW xmm11, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm1, xmm9
	PMADDWD xmm11, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm1, xmm11
	PHADDD xmm7, xmm8
	PHADDD xmm1, xmm7	
	PSRAD xmm1, 5



	PACKSSDW xmm12, xmm0
	PACKSSDW xmm13, xmm1

	PSRLDQ xmm2, 4
	PSRLDQ xmm3, 4
	PSRLDQ xmm4, 4
	PSRLDQ xmm5, 4

	PUNPCKLQDQ xmm2, xmm3
	PUNPCKLQDQ xmm4, xmm5

	PADDW xmm12, xmm2
	PADDW xmm13, xmm4

	PCMPEQW xmm10, xmm10

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1

	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_a	ENDP



ALIGN 16
H264_Interpolate_c	PROC
	MOVDQA xmm9, XMMWORD PTR [tap_6]
	PCMPEQD xmm10, xmm10
	PSRLDQ xmm10, 10




	SHR r9, 32

	MOVDQA xmm12, xmm2
	MOVDQA xmm1, xmm2
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	SHR r9, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2
	PSRLDQ xmm8, 4

	PSUBW xmm12, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10
	
	PMADDWD xmm12, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm12, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm12, xmm7
	PSRAD xmm12, 5



	MOVDQA xmm0, xmm3
	MOVDQA xmm1, xmm3
	PSRLDQ xmm1, 2
	PINSRW xmm1, r9d, 7
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm0, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm0, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm0, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm0, xmm7
	PSRAD xmm0, 5



	MOVDQA xmm13, xmm4
	MOVDQA xmm1, xmm4
	PSRLDQ xmm1, 2
	PINSRW xmm1, r10d, 7
	SHR r10, 16
	MOVDQA xmm7, xmm1
	MOVDQA xmm8, xmm1
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm13, xmm10
	PSUBW xmm1, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm13, xmm9
	PMADDWD xmm1, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm13, xmm1
	PHADDD xmm7, xmm8
	PHADDD xmm13, xmm7
	PSRAD xmm13, 5



	MOVDQA xmm1, xmm5
	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7	
	MOVDQA xmm7, xmm11
	MOVDQA xmm8, xmm11
	PSRLDQ xmm7, 2	
	PSRLDQ xmm8, 4

	PSUBW xmm1, xmm10
	PSUBW xmm11, xmm10
	PSUBW xmm7, xmm10
	PSUBW xmm8, xmm10

	PMADDWD xmm1, xmm9
	PMADDWD xmm11, xmm9
	PMADDWD xmm7, xmm9
	PMADDWD xmm8, xmm9
	PHADDD xmm1, xmm11
	PHADDD xmm7, xmm8
	PHADDD xmm1, xmm7	
	PSRAD xmm1, 5



	PACKSSDW xmm12, xmm0
	PACKSSDW xmm13, xmm1

	PSRLDQ xmm2, 6
	PSRLDQ xmm3, 6
	PSRLDQ xmm4, 6
	PSRLDQ xmm5, 6

	PUNPCKLQDQ xmm2, xmm3
	PUNPCKLQDQ xmm4, xmm5

	PADDW xmm12, xmm2
	PADDW xmm13, xmm4

	PCMPEQW xmm10, xmm10

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:
	

	RET
H264_Interpolate_c	ENDP


ALIGN 16
H264_Interpolate_p	PROC
	SHR r9, 48

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm12, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm12, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm12, xmm10
	PSRAD xmm12, 5





	MOVDQA xmm11, xmm4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm4
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5


	PACKSSDW xmm12, xmm11




	MOVDQA xmm13, xmm5
	PMADDWD xmm13, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm13, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm13, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm13, xmm10
	PSRAD xmm13, 5





	MOVDQA xmm11, xmm6
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm6
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5


	PACKSSDW xmm13, xmm11









	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 4
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 4
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 4
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 4
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 4
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 4
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 4
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 4
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 4
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm1, xmm0
	PSLLD xmm0, 2
	PSUBD xmm1, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSRAD xmm1, 5



	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm2, xmm0
	PSLLD xmm0, 2
	PSUBD xmm2, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSRAD xmm2, 5





	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm3, xmm0
	PSLLD xmm0, 2
	PSUBD xmm3, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSRAD xmm3, 5




	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PACKSSDW xmm1, xmm2
	PACKSSDW xmm3, xmm4


	PADDW xmm12, xmm1
	PADDW xmm13, xmm3

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_p	ENDP






ALIGN 16
H264_Interpolate_e	PROC
	SHR r9, 32

	MOVDQA xmm12, xmm2
	PMADDWD xmm12, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm12, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm12, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm12, xmm10
	PSRAD xmm12, 5





	MOVDQA xmm11, xmm3
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm3
	PSRLDQ xmm10, 2
	PINSRW xmm10, r9d, 7

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm12, xmm11




	MOVDQA xmm13, xmm4
	PMADDWD xmm13, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm13, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm13, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm13, xmm10
	PSRAD xmm13, 5





	MOVDQA xmm11, xmm5
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm5
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm13, xmm11









	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 4
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 4
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 4
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 4
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 4
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 4
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 4
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 4
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 4
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm1, xmm0
	PSLLD xmm0, 2
	PSUBD xmm1, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSRAD xmm1, 5


	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm2, xmm0
	PSLLD xmm0, 2
	PSUBD xmm2, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSRAD xmm2, 5




	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm3, xmm0
	PSLLD xmm0, 2
	PSUBD xmm3, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSRAD xmm3, 5




	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PACKSSDW xmm1, xmm2
	PACKSSDW xmm3, xmm4


	PADDW xmm12, xmm1
	PADDW xmm13, xmm3

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:


	

	RET
H264_Interpolate_e	ENDP


ALIGN 16
H264_Interpolate_gg	PROC
	SHR r9, 32

	MOVDQA xmm12, xmm2
	PMADDWD xmm12, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm12, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm12, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm12, xmm10
	PSRAD xmm12, 5





	MOVDQA xmm11, xmm3
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm3
	PSRLDQ xmm10, 2
	PINSRW xmm10, r9d, 7


	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm12, xmm11




	MOVDQA xmm13, xmm4
	PMADDWD xmm13, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm13, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm13, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm13, xmm10
	PSRAD xmm13, 5





	MOVDQA xmm11, xmm5
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm5
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm13, xmm11






	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 6
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 6
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 6
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 6
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 6
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 6
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 6
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 6
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 6
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm1, xmm0
	PSLLD xmm0, 2
	PSUBD xmm1, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSRAD xmm1, 5



	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm2, xmm0
	PSLLD xmm0, 2
	PSUBD xmm2, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSRAD xmm2, 5





	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm3, xmm0
	PSLLD xmm0, 2
	PSUBD xmm3, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSRAD xmm3, 5




	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PACKSSDW xmm1, xmm2
	PACKSSDW xmm3, xmm4


	PADDW xmm12, xmm1
	PADDW xmm13, xmm3

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_gg	ENDP


ALIGN 16
H264_Interpolate_r	PROC
	SHR r9, 48

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm12, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm12, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm12, xmm10
	PSRAD xmm12, 5





	MOVDQA xmm11, xmm4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm4
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm12, xmm11




	MOVDQA xmm13, xmm5
	PMADDWD xmm13, XMMWORD PTR [tap_6]
	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm11
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm13, xmm9

	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm11, 4
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm11

	PHADDD xmm13, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm13, xmm10
	PSRAD xmm13, 5




	MOVDQA xmm11, xmm6
	PMADDWD xmm11, XMMWORD PTR [tap_6]
	MOVDQA xmm10, xmm6
	PSRLDQ xmm10, 2
	PINSRW xmm10, r10d, 7

	SHR r10, 16

	MOVDQA xmm9, xmm10
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PHADDD xmm11, xmm9

	MOVDQA xmm9, xmm10
	PSRLDQ xmm9, 2
	PMADDWD xmm9, XMMWORD PTR [tap_6]

	PSRLDQ xmm10, 4
	PMADDWD xmm10, XMMWORD PTR [tap_6]
	PHADDD xmm9, xmm10
	
	PHADDD xmm11, xmm9
	PCMPEQD xmm10, xmm10
	PSLLD xmm10, 4
	PSUBD xmm11, xmm10
	PSRAD xmm11, 5

	PACKSSDW xmm13, xmm11









	PCMPEQD xmm10, xmm10
	
	PXOR xmm9, xmm9
	PSRLDQ xmm8, 6
	PUNPCKLWD xmm9, xmm8
	PSRAD xmm9, 16

	PXOR xmm8, xmm8
	PSRLDQ xmm7, 6
	PUNPCKLWD xmm8, xmm7
	PSRAD xmm8, 16

	PXOR xmm7, xmm7
	PSRLDQ xmm6, 6
	PUNPCKLWD xmm7, xmm6
	PSRAD xmm7, 16

	PXOR xmm6, xmm6
	PSRLDQ xmm5, 6
	PUNPCKLWD xmm6, xmm5
	PSRAD xmm6, 16

	PXOR xmm5, xmm5
	PSRLDQ xmm4, 6
	PUNPCKLWD xmm5, xmm4
	PSRAD xmm5, 16

	PXOR xmm4, xmm4
	PSRLDQ xmm3, 6
	PUNPCKLWD xmm4, xmm3
	PSRAD xmm4, 16

	PXOR xmm3, xmm3
	PSRLDQ xmm2, 6
	PUNPCKLWD xmm3, xmm2
	PSRAD xmm3, 16

	PXOR xmm2, xmm2
	PSRLDQ xmm1, 6
	PUNPCKLWD xmm2, xmm1
	PSRAD xmm2, 16

	PXOR xmm1, xmm1
	PSRLDQ xmm0, 6
	PUNPCKLWD xmm1, xmm0
	PSRAD xmm1, 16






	PSUBD xmm1, xmm10
	PSUBD xmm2, xmm10


	PSUBD xmm3, xmm10
	PADDD xmm1, xmm6

	MOVDQA xmm0, xmm2
	PADDD xmm0, xmm5
	PSUBD xmm1, xmm0
	PSLLD xmm0, 2
	PSUBD xmm1, xmm0

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm4
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSLLD xmm0, 2
	PADDD xmm1, xmm0
	PSRAD xmm1, 5



	PSUBD xmm4, xmm10
	PADDD xmm2, xmm7

	MOVDQA xmm0, xmm3
	PADDD xmm0, xmm6
	PSUBD xmm2, xmm0
	PSLLD xmm0, 2
	PSUBD xmm2, xmm0

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm5
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSLLD xmm0, 2
	PADDD xmm2, xmm0
	PSRAD xmm2, 5




	PSUBD xmm5, xmm10
	PADDD xmm3, xmm8

	MOVDQA xmm0, xmm4
	PADDD xmm0, xmm7
	PSUBD xmm3, xmm0
	PSLLD xmm0, 2
	PSUBD xmm3, xmm0

	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm6
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSLLD xmm0, 2
	PADDD xmm3, xmm0
	PSRAD xmm3, 5




	PSUBD xmm6, xmm10
	PADDD xmm4, xmm9
	
	MOVDQA xmm0, xmm5
	PADDD xmm0, xmm8
	PSUBD xmm4, xmm0
	PSLLD xmm0, 2
	PSUBD xmm4, xmm0

	MOVDQA xmm0, xmm6
	PADDD xmm0, xmm7
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSLLD xmm0, 2
	PADDD xmm4, xmm0
	PSRAD xmm4, 5



	PACKSSDW xmm1, xmm2
	PACKSSDW xmm3, xmm4


	PADDW xmm12, xmm1
	PADDW xmm13, xmm3

	PSUBW xmm12, xmm10
	PSUBW xmm13, xmm10

	PSRAW xmm12, 1
	PSRAW xmm13, 1


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:


	RET
H264_Interpolate_r	ENDP




ALIGN 16
H264_Interpolate_j	PROC

	MOVDQA xmm10, XMMWORD PTR [tap_6]	
	PCMPEQD xmm12, xmm12
	PSLLD xmm12, 4

	MOVDQA xmm11, xmm0
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm0, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm0, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm0, xmm9
	PSUBD xmm0, xmm12


	MOVDQA xmm11, xmm1
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm1, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm1, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm1, xmm9
	PSUBD xmm1, xmm12


	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm2, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm2, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm2, xmm9
	PSUBD xmm2, xmm12


	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm3, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm3, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm3, xmm9
	PSUBD xmm3, xmm12


	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm4, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm4, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm4, xmm9
	PSUBD xmm4, xmm12


	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm5, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm5, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm5, xmm9
	PSUBD xmm5, xmm12


	MOVDQA xmm11, xmm6
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm6, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm6, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm6, xmm9
	PSUBD xmm6, xmm12


	MOVDQA xmm11, xmm7
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm7, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm7, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm7, xmm9
	PSUBD xmm7, xmm12


	MOVDQA xmm11, xmm8
	PSRLDQ xmm11, 2
	PINSRW xmm11, r11d, 7	
	PMADDWD xmm8, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm8, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm8, xmm9
	PSUBD xmm8, xmm12






	
	


	PADDD xmm0, xmm5
	MOVDQA xmm12, xmm0
	MOVDQA xmm9, xmm1
	PADDD xmm9, xmm4
	PSUBD xmm12, xmm9
	PSLLD xmm9, 2
	PSUBD xmm12, xmm9
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm3
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSRAD xmm12, 10


	PADDD xmm1, xmm6
	MOVDQA xmm11, xmm1
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm5
	PSUBD xmm11, xmm9
	PSLLD xmm9, 2
	PSUBD xmm11, xmm9
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm4
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSRAD xmm11, 10


	PADDD xmm2, xmm7
	MOVDQA xmm13, xmm2
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm6
	PSUBD xmm13, xmm9
	PSLLD xmm9, 2
	PSUBD xmm13, xmm9
	MOVDQA xmm9, xmm4
	PADDD xmm9, xmm5
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSRAD xmm13, 10


	PADDD xmm3, xmm8
	PADDD xmm4, xmm7
	PSUBD xmm3, xmm4
	PSLLD xmm4, 2
	PSUBD xmm3, xmm4
	MOVDQA xmm9, xmm5
	PADDD xmm9, xmm6
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSRAD xmm3, 10



	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm3



	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:




	RET
H264_Interpolate_j	ENDP


ALIGN 16
H264_Interpolate_f	PROC

	MOVDQA xmm10, XMMWORD PTR [tap_6]	
	PCMPEQD xmm12, xmm12
	PSLLD xmm12, 4

	MOVDQA xmm11, xmm0
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm0, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm0, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm0, xmm9
	PSUBD xmm0, xmm12


	MOVDQA xmm11, xmm1
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm1, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm1, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm1, xmm9
	PSUBD xmm1, xmm12


	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm2, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm2, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm2, xmm9
	PSUBD xmm2, xmm12


	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm3, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm3, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm3, xmm9
	PSUBD xmm3, xmm12


	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm4, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm4, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm4, xmm9
	PSUBD xmm4, xmm12


	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm5, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm5, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm5, xmm9
	PSUBD xmm5, xmm12


	MOVDQA xmm11, xmm6
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm6, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm6, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm6, xmm9
	PSUBD xmm6, xmm12


	MOVDQA xmm11, xmm7
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm7, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm7, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm7, xmm9
	PSUBD xmm7, xmm12


	MOVDQA xmm11, xmm8
	PSRLDQ xmm11, 2
	PINSRW xmm11, r11d, 7	
	PMADDWD xmm8, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm8, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm8, xmm9
	PSUBD xmm8, xmm12







	PCMPEQD xmm10, xmm10
	
	


	PADDD xmm0, xmm5
	MOVDQA xmm12, xmm0
	MOVDQA xmm9, xmm1
	PADDD xmm9, xmm4
	PSUBD xmm12, xmm9
	PSLLD xmm9, 2
	PSUBD xmm12, xmm9
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm3
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSRAD xmm12, 10
	MOVDQA xmm0, xmm2
	PSRAD xmm0, 5


	PADDD xmm12, xmm0
	PSUBD xmm12, xmm10
	PSRAD xmm12, 1



	PADDD xmm1, xmm6
	MOVDQA xmm11, xmm1
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm5
	PSUBD xmm11, xmm9
	PSLLD xmm9, 2
	PSUBD xmm11, xmm9
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm4
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSRAD xmm11, 10
	MOVDQA xmm0, xmm3
	PSRAD xmm0, 5


	PADDD xmm11, xmm0
	PSUBD xmm11, xmm10
	PSRAD xmm11, 1


	PADDD xmm2, xmm7
	MOVDQA xmm13, xmm2
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm6
	PSUBD xmm13, xmm9
	PSLLD xmm9, 2
	PSUBD xmm13, xmm9
	MOVDQA xmm9, xmm4
	PADDD xmm9, xmm5
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSRAD xmm13, 10
	MOVDQA xmm0, xmm4
	PSRAD xmm0, 5


	PADDD xmm13, xmm0
	PSUBD xmm13, xmm10
	PSRAD xmm13, 1


	PADDD xmm3, xmm8
	PADDD xmm4, xmm7
	PSUBD xmm3, xmm4
	PSLLD xmm4, 2
	PSUBD xmm3, xmm4
	MOVDQA xmm9, xmm5
	PADDD xmm9, xmm6
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSRAD xmm3, 10
	MOVDQA xmm0, xmm5
	PSRAD xmm0, 5


	PADDD xmm3, xmm0
	PSUBD xmm3, xmm10
	PSRAD xmm3, 1



	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm3



	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:



	RET
H264_Interpolate_f	ENDP


ALIGN 16
H264_Interpolate_q	PROC

	MOVDQA xmm10, XMMWORD PTR [tap_6]	
	PCMPEQD xmm12, xmm12
	PSLLD xmm12, 4

	MOVDQA xmm11, xmm0
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm0, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm0, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm0, xmm9
	PSUBD xmm0, xmm12


	MOVDQA xmm11, xmm1
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm1, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm1, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm1, xmm9
	PSUBD xmm1, xmm12


	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm2, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm2, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm2, xmm9
	PSUBD xmm2, xmm12


	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm3, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm3, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm3, xmm9
	PSUBD xmm3, xmm12


	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm4, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm4, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm4, xmm9
	PSUBD xmm4, xmm12


	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm5, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm5, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm5, xmm9
	PSUBD xmm5, xmm12


	MOVDQA xmm11, xmm6
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm6, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm6, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm6, xmm9
	PSUBD xmm6, xmm12


	MOVDQA xmm11, xmm7
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm7, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm7, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm7, xmm9
	PSUBD xmm7, xmm12


	MOVDQA xmm11, xmm8
	PSRLDQ xmm11, 2
	PINSRW xmm11, r11d, 7	
	PMADDWD xmm8, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm8, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm8, xmm9
	PSUBD xmm8, xmm12







	PCMPEQD xmm10, xmm10
	
	


	PADDD xmm0, xmm5
	MOVDQA xmm12, xmm0
	MOVDQA xmm9, xmm1
	PADDD xmm9, xmm4
	PSUBD xmm12, xmm9
	PSLLD xmm9, 2
	PSUBD xmm12, xmm9
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm3
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSRAD xmm12, 10
	MOVDQA xmm0, xmm3
	PSRAD xmm0, 5


	PADDD xmm12, xmm0
	PSUBD xmm12, xmm10
	PSRAD xmm12, 1



	PADDD xmm1, xmm6
	MOVDQA xmm11, xmm1
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm5
	PSUBD xmm11, xmm9
	PSLLD xmm9, 2
	PSUBD xmm11, xmm9
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm4
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSRAD xmm11, 10
	MOVDQA xmm0, xmm4
	PSRAD xmm0, 5


	PADDD xmm11, xmm0
	PSUBD xmm11, xmm10
	PSRAD xmm11, 1


	PADDD xmm2, xmm7
	MOVDQA xmm13, xmm2
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm6
	PSUBD xmm13, xmm9
	PSLLD xmm9, 2
	PSUBD xmm13, xmm9
	MOVDQA xmm9, xmm4
	PADDD xmm9, xmm5
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSRAD xmm13, 10
	MOVDQA xmm0, xmm5
	PSRAD xmm0, 5


	PADDD xmm13, xmm0
	PSUBD xmm13, xmm10
	PSRAD xmm13, 1


	PADDD xmm3, xmm8
	PADDD xmm4, xmm7
	PSUBD xmm3, xmm4
	PSLLD xmm4, 2
	PSUBD xmm3, xmm4
	MOVDQA xmm9, xmm5
	PADDD xmm9, xmm6
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSRAD xmm3, 10
	MOVDQA xmm0, xmm6
	PSRAD xmm0, 5


	PADDD xmm3, xmm0
	PSUBD xmm3, xmm10
	PSRAD xmm3, 1



	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm3



	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:


	RET
H264_Interpolate_q	ENDP



ALIGN 16
H264_Interpolate_i	PROC
	
	MOVDQA xmm9, xmm1
	MOVDQA xmm1, xmm0
	PUNPCKLWD xmm0, xmm9 ; (0 8) (1 9) (2 10) (3 11)
	PUNPCKHWD xmm1, xmm9 ; (4 12) (5 13) (6 14) (7 15)

	MOVDQA xmm9, xmm3
	MOVDQA xmm3, xmm2
	PUNPCKLWD xmm2, xmm9 ; (16 24) (17 25) (18 26) (19 27)
	PUNPCKHWD xmm3, xmm9 ; (20 28) (21 29) (22 30) (23 31)

	MOVDQA xmm9, xmm5
	MOVDQA xmm5, xmm4
	PUNPCKLWD xmm4, xmm9 ; (32 40) (33 41) (34 42) (35 43)
	PUNPCKHWD xmm5, xmm9 ; (36 44) (37 45) (38 46) (39 47)

	MOVDQA xmm9, xmm7
	MOVDQA xmm7, xmm6
	PUNPCKLWD xmm6, xmm9 ; (48 56) (49 57) (50 58) (51 59)
	PUNPCKHWD xmm7, xmm9 ; (52 60) (53 61) (54 62) (55 63)



	MOVDQA xmm9, xmm2
	MOVDQA xmm2, xmm0
	PUNPCKLDQ xmm0, xmm9 ; (0 8 16 24) (1 9 17 25)
	PUNPCKHDQ xmm2, xmm9 ; (2 10 18 26) (3 11 19 27)
	
	MOVDQA xmm9, xmm3
	MOVDQA xmm3, xmm1
	PUNPCKLDQ xmm1, xmm9 ; (4 12 20 28) (5 13 21 29)
	PUNPCKHDQ xmm3, xmm9 ; (6 14 22 30) (7 15 23 31)

	MOVDQA xmm9, xmm6
	MOVDQA xmm6, xmm4
	PUNPCKLDQ xmm4, xmm9 ; (32 40 48 56) (33 41 49 57)
	PUNPCKHDQ xmm6, xmm9 ; (34 42 50 58) (35 43 51 59)

	MOVDQA xmm9, xmm7
	MOVDQA xmm7, xmm5
	PUNPCKLDQ xmm5, xmm9 ; (36 44 52 60) (37 45 53 61)
	PUNPCKHDQ xmm7, xmm9 ; (38 46 54 62) (39 47 55 63)



	MOVDQA xmm9, xmm1
	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm4 ; 0 8 16 24 32 40 48 56
	PUNPCKHQDQ xmm1, xmm4 ; 1 9 17 25 33 41 49 57

	MOVDQA xmm10, xmm3
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm2, xmm6 ; 2 10 18 26 34 42 50 58
	PUNPCKHQDQ xmm3, xmm6 ; 3 11 19 27 35 43 51 59

	MOVDQA xmm6, xmm5
	MOVDQA xmm4, xmm9
	MOVDQA xmm5, xmm9
	PUNPCKLQDQ xmm4, xmm6 ; 4 12 20 28 36 44 52 60
	PUNPCKHQDQ xmm5, xmm6 ; 5 13 21 29 37 45 53 61

	MOVDQA xmm9, xmm7
	MOVDQA xmm6, xmm10
	MOVDQA xmm7, xmm10
	PUNPCKLQDQ xmm6, xmm9 ; 6 14 22 30 38 46 54 62
	PUNPCKHQDQ xmm7, xmm9 ; 7 15 23 31 39 47 55 63

	MOVD xmm9, r9
	MOVD xmm10, r10
	PSLLDQ xmm10, 8
	MOVD r9, xmm8
	PSRLDQ xmm8, 8
	MOVD r10, xmm8

	MOVDQA xmm8, xmm9
	POR xmm8, xmm10

; transpose complete





	MOVDQA xmm10, XMMWORD PTR [tap_6]	
	PCMPEQD xmm12, xmm12
	PSLLD xmm12, 4

	MOVDQA xmm11, xmm0
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm0, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm0, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm0, xmm9
	PSUBD xmm0, xmm12


	MOVDQA xmm11, xmm1
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm1, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm1, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm1, xmm9
	PSUBD xmm1, xmm12


	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm2, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm2, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm2, xmm9
	PSUBD xmm2, xmm12


	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm3, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm3, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm3, xmm9
	PSUBD xmm3, xmm12


	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm4, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm4, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm4, xmm9
	PSUBD xmm4, xmm12


	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm5, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm5, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm5, xmm9
	PSUBD xmm5, xmm12


	MOVDQA xmm11, xmm6
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm6, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm6, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm6, xmm9
	PSUBD xmm6, xmm12


	MOVDQA xmm11, xmm7
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm7, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm7, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm7, xmm9
	PSUBD xmm7, xmm12


	MOVDQA xmm11, xmm8
	PSRLDQ xmm11, 2
	PINSRW xmm11, r11d, 7	
	PMADDWD xmm8, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm8, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm8, xmm9
	PSUBD xmm8, xmm12







	PCMPEQD xmm10, xmm10
	
	


	PADDD xmm0, xmm5
	MOVDQA xmm12, xmm0
	MOVDQA xmm9, xmm1
	PADDD xmm9, xmm4
	PSUBD xmm12, xmm9
	PSLLD xmm9, 2
	PSUBD xmm12, xmm9
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm3
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSRAD xmm12, 10
	MOVDQA xmm0, xmm2
	PSRAD xmm0, 5


	PADDD xmm12, xmm0
	PSUBD xmm12, xmm10
	PSRAD xmm12, 1



	PADDD xmm1, xmm6
	MOVDQA xmm11, xmm1
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm5
	PSUBD xmm11, xmm9
	PSLLD xmm9, 2
	PSUBD xmm11, xmm9
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm4
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSRAD xmm11, 10
	MOVDQA xmm0, xmm3
	PSRAD xmm0, 5


	PADDD xmm11, xmm0
	PSUBD xmm11, xmm10
	PSRAD xmm11, 1


	PADDD xmm2, xmm7
	MOVDQA xmm13, xmm2
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm6
	PSUBD xmm13, xmm9
	PSLLD xmm9, 2
	PSUBD xmm13, xmm9
	MOVDQA xmm9, xmm4
	PADDD xmm9, xmm5
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSRAD xmm13, 10
	MOVDQA xmm0, xmm4
	PSRAD xmm0, 5


	PADDD xmm13, xmm0
	PSUBD xmm13, xmm10
	PSRAD xmm13, 1


	PADDD xmm3, xmm8
	PADDD xmm4, xmm7
	PSUBD xmm3, xmm4
	PSLLD xmm4, 2
	PSUBD xmm3, xmm4
	MOVDQA xmm9, xmm5
	PADDD xmm9, xmm6
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSRAD xmm3, 10
	MOVDQA xmm0, xmm5
	PSRAD xmm0, 5


	PADDD xmm3, xmm0
	PSUBD xmm3, xmm10
	PSRAD xmm3, 1




	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm3


; traspose back
	MOVDQA xmm0, xmm12
	PUNPCKLWD xmm12, xmm13
	PUNPCKHWD xmm0, xmm13

	MOVDQA xmm13, xmm12
	PUNPCKLWD xmm12, xmm0
	PUNPCKHWD xmm13, xmm0


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:




	RET
H264_Interpolate_i	ENDP


ALIGN 16
H264_Interpolate_k	PROC
	
	MOVDQA xmm9, xmm1
	MOVDQA xmm1, xmm0
	PUNPCKLWD xmm0, xmm9 ; (0 8) (1 9) (2 10) (3 11)
	PUNPCKHWD xmm1, xmm9 ; (4 12) (5 13) (6 14) (7 15)

	MOVDQA xmm9, xmm3
	MOVDQA xmm3, xmm2
	PUNPCKLWD xmm2, xmm9 ; (16 24) (17 25) (18 26) (19 27)
	PUNPCKHWD xmm3, xmm9 ; (20 28) (21 29) (22 30) (23 31)

	MOVDQA xmm9, xmm5
	MOVDQA xmm5, xmm4
	PUNPCKLWD xmm4, xmm9 ; (32 40) (33 41) (34 42) (35 43)
	PUNPCKHWD xmm5, xmm9 ; (36 44) (37 45) (38 46) (39 47)

	MOVDQA xmm9, xmm7
	MOVDQA xmm7, xmm6
	PUNPCKLWD xmm6, xmm9 ; (48 56) (49 57) (50 58) (51 59)
	PUNPCKHWD xmm7, xmm9 ; (52 60) (53 61) (54 62) (55 63)



	MOVDQA xmm9, xmm2
	MOVDQA xmm2, xmm0
	PUNPCKLDQ xmm0, xmm9 ; (0 8 16 24) (1 9 17 25)
	PUNPCKHDQ xmm2, xmm9 ; (2 10 18 26) (3 11 19 27)
	
	MOVDQA xmm9, xmm3
	MOVDQA xmm3, xmm1
	PUNPCKLDQ xmm1, xmm9 ; (4 12 20 28) (5 13 21 29)
	PUNPCKHDQ xmm3, xmm9 ; (6 14 22 30) (7 15 23 31)

	MOVDQA xmm9, xmm6
	MOVDQA xmm6, xmm4
	PUNPCKLDQ xmm4, xmm9 ; (32 40 48 56) (33 41 49 57)
	PUNPCKHDQ xmm6, xmm9 ; (34 42 50 58) (35 43 51 59)

	MOVDQA xmm9, xmm7
	MOVDQA xmm7, xmm5
	PUNPCKLDQ xmm5, xmm9 ; (36 44 52 60) (37 45 53 61)
	PUNPCKHDQ xmm7, xmm9 ; (38 46 54 62) (39 47 55 63)



	MOVDQA xmm9, xmm1
	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm4 ; 0 8 16 24 32 40 48 56
	PUNPCKHQDQ xmm1, xmm4 ; 1 9 17 25 33 41 49 57

	MOVDQA xmm10, xmm3
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm2, xmm6 ; 2 10 18 26 34 42 50 58
	PUNPCKHQDQ xmm3, xmm6 ; 3 11 19 27 35 43 51 59

	MOVDQA xmm6, xmm5
	MOVDQA xmm4, xmm9
	MOVDQA xmm5, xmm9
	PUNPCKLQDQ xmm4, xmm6 ; 4 12 20 28 36 44 52 60
	PUNPCKHQDQ xmm5, xmm6 ; 5 13 21 29 37 45 53 61

	MOVDQA xmm9, xmm7
	MOVDQA xmm6, xmm10
	MOVDQA xmm7, xmm10
	PUNPCKLQDQ xmm6, xmm9 ; 6 14 22 30 38 46 54 62
	PUNPCKHQDQ xmm7, xmm9 ; 7 15 23 31 39 47 55 63

	MOVD xmm9, r9
	MOVD xmm10, r10
	PSLLDQ xmm10, 8
	MOVD r9, xmm8
	PSRLDQ xmm8, 8
	MOVD r10, xmm8

	MOVDQA xmm8, xmm9
	POR xmm8, xmm10

; transpose complete


	MOVDQA xmm10, XMMWORD PTR [tap_6]	
	PCMPEQD xmm12, xmm12
	PSLLD xmm12, 4

	MOVDQA xmm11, xmm0
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm0, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm0, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm0, xmm9
	PSUBD xmm0, xmm12 ; h1_-2 + 16


	MOVDQA xmm11, xmm1
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm1, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm1, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm1, xmm9
	PSUBD xmm1, xmm12 ; h1_-1 + 16


	MOVDQA xmm11, xmm2
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm2, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm2, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm2, xmm9
	PSUBD xmm2, xmm12 ; h1_0 + 16


	MOVDQA xmm11, xmm3
	PSRLDQ xmm11, 2
	PINSRW xmm11, r9d, 7
	SHR r9, 16
	PMADDWD xmm3, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm3, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm3, xmm9
	PSUBD xmm3, xmm12 ; h1_1 + 16


	MOVDQA xmm11, xmm4
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm4, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm4, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm4, xmm9
	PSUBD xmm4, xmm12 ; h1_2 + 16


	MOVDQA xmm11, xmm5
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm5, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm5, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm5, xmm9
	PSUBD xmm5, xmm12 ; h1_3 + 16


	MOVDQA xmm11, xmm6
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm6, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm6, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm6, xmm9
	PSUBD xmm6, xmm12 ; h1_4 + 16


	MOVDQA xmm11, xmm7
	PSRLDQ xmm11, 2
	PINSRW xmm11, r10d, 7
	SHR r10, 16
	PMADDWD xmm7, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm7, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm7, xmm9
	PSUBD xmm7, xmm12 ; h1_5 + 16


	MOVDQA xmm11, xmm8
	PSRLDQ xmm11, 2
	PINSRW xmm11, r11d, 7
	PMADDWD xmm8, xmm10
	MOVDQA xmm9, xmm11
	PMADDWD xmm9, xmm10
	PHADDD xmm8, xmm9
	MOVDQA xmm9, xmm11
	PSRLDQ xmm9, 2
	PMADDWD xmm9, xmm10
	PSRLDQ xmm11, 4
	PMADDWD xmm11, xmm10
	PHADDD xmm9, xmm11
	PHADDD xmm8, xmm9
	PSUBD xmm8, xmm12 ; h1_6 + 16







	PCMPEQD xmm10, xmm10
	
	


	PADDD xmm0, xmm5
	MOVDQA xmm12, xmm0
	MOVDQA xmm9, xmm1
	PADDD xmm9, xmm4
	PSUBD xmm12, xmm9
	PSLLD xmm9, 2
	PSUBD xmm12, xmm9
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm3
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSLLD xmm9, 2
	PADDD xmm12, xmm9
	PSRAD xmm12, 10
	MOVDQA xmm0, xmm3
	PSRAD xmm0, 5


	PADDD xmm12, xmm0
	PSUBD xmm12, xmm10
	PSRAD xmm12, 1



	PADDD xmm1, xmm6
	MOVDQA xmm11, xmm1
	MOVDQA xmm9, xmm2
	PADDD xmm9, xmm5
	PSUBD xmm11, xmm9
	PSLLD xmm9, 2
	PSUBD xmm11, xmm9
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm4
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSLLD xmm9, 2
	PADDD xmm11, xmm9
	PSRAD xmm11, 10
	MOVDQA xmm0, xmm4
	PSRAD xmm0, 5


	PADDD xmm11, xmm0
	PSUBD xmm11, xmm10
	PSRAD xmm11, 1


	PADDD xmm2, xmm7
	MOVDQA xmm13, xmm2
	MOVDQA xmm9, xmm3
	PADDD xmm9, xmm6
	PSUBD xmm13, xmm9
	PSLLD xmm9, 2
	PSUBD xmm13, xmm9
	MOVDQA xmm9, xmm4
	PADDD xmm9, xmm5
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSLLD xmm9, 2
	PADDD xmm13, xmm9
	PSRAD xmm13, 10
	MOVDQA xmm0, xmm5
	PSRAD xmm0, 5


	PADDD xmm13, xmm0
	PSUBD xmm13, xmm10
	PSRAD xmm13, 1


	PADDD xmm3, xmm8
	PADDD xmm4, xmm7
	PSUBD xmm3, xmm4
	PSLLD xmm4, 2
	PSUBD xmm3, xmm4
	MOVDQA xmm9, xmm5
	PADDD xmm9, xmm6
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSLLD xmm9, 2
	PADDD xmm3, xmm9
	PSRAD xmm3, 10
	MOVDQA xmm0, xmm6
	PSRAD xmm0, 5


	PADDD xmm3, xmm0
	PSUBD xmm3, xmm10
	PSRAD xmm3, 1



	PACKSSDW xmm12, xmm11
	PACKSSDW xmm13, xmm3







; traspose back
	MOVDQA xmm0, xmm12
	PUNPCKLWD xmm12, xmm13
	PUNPCKHWD xmm0, xmm13

	MOVDQA xmm13, xmm12
	PUNPCKLWD xmm12, xmm0
	PUNPCKHWD xmm13, xmm0


	SHR rcx, 32
	JNZ ref0_isset
		MOVDQA xmm14, xmm12
		MOVDQA xmm15, xmm13
	ref0_isset:




	RET
H264_Interpolate_k	ENDP



ALIGN 16
H264_InterClear	PROC
	PCMPEQW xmm12, xmm12
	PCMPEQW xmm13, xmm13
	PCMPEQW xmm14, xmm14
	PCMPEQW xmm15, xmm15


	RET
H264_InterClear	ENDP


END


