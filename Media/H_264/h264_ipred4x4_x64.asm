
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE


ALIGN 16
H264_SamplesShake4x4	PROC
; 01
	MOVDQA xmm0, [rcx]
	MOVDQA xmm2, [rcx+16]
	MOVDQA xmm4, [rcx+32]
	MOVDQA xmm5, [rcx+48]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	PUNPCKLQDQ xmm2, xmm5
	PUNPCKHQDQ xmm3, xmm5


; 23
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm6, [rcx+80]
	MOVDQA xmm8, [rcx+96]
	MOVDQA xmm9, [rcx+112]

	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6
	PUNPCKLQDQ xmm4, xmm8
	PUNPCKHQDQ xmm5, xmm8
	PUNPCKLQDQ xmm6, xmm9
	PUNPCKHQDQ xmm7, xmm9

; 45
	MOVDQA xmm8, [rcx+128]
	MOVDQA xmm10, [rcx+144]
	MOVDQA xmm12, [rcx+160]
	MOVDQA xmm13, [rcx+176]

	MOVDQA xmm9, xmm8
	MOVDQA xmm11, xmm10
	PUNPCKLQDQ xmm8, xmm12
	PUNPCKHQDQ xmm9, xmm12
	PUNPCKLQDQ xmm10, xmm13
	PUNPCKHQDQ xmm11, xmm13


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm8
	MOVDQA [rcx+32], xmm1
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm2
	MOVDQA [rcx+80], xmm10
	MOVDQA [rcx+96], xmm3	
	MOVDQA [rcx+112], xmm11



; 67

	MOVDQA xmm0, [rcx+192]
	MOVDQA xmm2, [rcx+208]
	MOVDQA xmm8, [rcx+224]
	MOVDQA xmm9, [rcx+240]
	
	
	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm8
	PUNPCKHQDQ xmm1, xmm8
	PUNPCKLQDQ xmm2, xmm9
	PUNPCKHQDQ xmm3, xmm9
	

	MOVDQA [rcx+128], xmm4
	MOVDQA [rcx+144], xmm0
	MOVDQA [rcx+160], xmm5
	MOVDQA [rcx+176], xmm1
	MOVDQA [rcx+192], xmm6
	MOVDQA [rcx+208], xmm2
	MOVDQA [rcx+224], xmm7	
	MOVDQA [rcx+240], xmm3






	ADD rcx, 256

; 89
	MOVDQA xmm0, [rcx]
	MOVDQA xmm2, [rcx+16]
	MOVDQA xmm4, [rcx+32]
	MOVDQA xmm5, [rcx+48]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	PUNPCKLQDQ xmm2, xmm5
	PUNPCKHQDQ xmm3, xmm5


; 1011
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm6, [rcx+80]
	MOVDQA xmm8, [rcx+96]
	MOVDQA xmm9, [rcx+112]

	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6
	PUNPCKLQDQ xmm4, xmm8
	PUNPCKHQDQ xmm5, xmm8
	PUNPCKLQDQ xmm6, xmm9
	PUNPCKHQDQ xmm7, xmm9

; 1213
	MOVDQA xmm8, [rcx+128]
	MOVDQA xmm10, [rcx+144]
	MOVDQA xmm12, [rcx+160]
	MOVDQA xmm13, [rcx+176]

	MOVDQA xmm9, xmm8
	MOVDQA xmm11, xmm10
	PUNPCKLQDQ xmm8, xmm12
	PUNPCKHQDQ xmm9, xmm12
	PUNPCKLQDQ xmm10, xmm13
	PUNPCKHQDQ xmm11, xmm13


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm8
	MOVDQA [rcx+32], xmm1
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm2
	MOVDQA [rcx+80], xmm10
	MOVDQA [rcx+96], xmm3	
	MOVDQA [rcx+112], xmm11



; 1415

	MOVDQA xmm0, [rcx+192]
	MOVDQA xmm2, [rcx+208]
	MOVDQA xmm8, [rcx+224]
	MOVDQA xmm9, [rcx+240]
	
	
	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm8
	PUNPCKHQDQ xmm1, xmm8
	PUNPCKLQDQ xmm2, xmm9
	PUNPCKHQDQ xmm3, xmm9
	

	MOVDQA [rcx+128], xmm4
	MOVDQA [rcx+144], xmm0
	MOVDQA [rcx+160], xmm5
	MOVDQA [rcx+176], xmm1
	MOVDQA [rcx+192], xmm6
	MOVDQA [rcx+208], xmm2
	MOVDQA [rcx+224], xmm7	
	MOVDQA [rcx+240], xmm3


	RET
H264_SamplesShake4x4	ENDP



ALIGN 16
H264_DCIPred4x4	PROC
	PCMPEQD xmm3, xmm3

	PXOR xmm0, xmm0
	PXOR xmm1, xmm1

	PUNPCKLWD xmm0, [rdx]
	PUNPCKLWD xmm1, [rdx+16]
	
	PSRAD xmm0, 16
	PSRAD xmm1, 16

	PSUBD xmm0, xmm3
	PADDD xmm0, xmm1

	PHADDD xmm0, xmm0
	PHADDD xmm0, xmm0

	PSRAD xmm0, 3

	PACKSSDW xmm0, xmm0
	MOVDQA xmm1, xmm0

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET

H264_DCIPred4x4	ENDP

ALIGN 16
H264_VerticalIPred4x4	PROC
	MOVDQA xmm0, [rdx]
	PUNPCKLQDQ xmm0, xmm0

	MOVDQA xmm1, xmm0

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_VerticalIPred4x4	ENDP

ALIGN 16
H264_HorizontalIPred4x4	PROC
	MOVDQA xmm0, [rdx+16]
	
	PUNPCKLWD xmm0, xmm0
	MOVDQA xmm1, xmm0

	PUNPCKLDQ xmm0, xmm0
	PUNPCKHDQ xmm1, xmm1

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_HorizontalIPred4x4	ENDP

ALIGN 16
H264_DownLeftIPred4x4	PROC
	MOVDQA xmm0, [rdx]

	PCMPEQD xmm3, xmm3

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0

	PSRLDQ xmm1, 2
	PSRLDQ xmm2, 4
	
	PSHUFHW xmm1, xmm1, 0A4h
	PSHUFHW xmm2, xmm2, 054h
	
	PSUBW xmm1, xmm3

	PSLLW xmm1, 1
	
	PADDW xmm0, xmm1
	PADDW xmm0, xmm2
	
	PSRAW xmm0, 2

	MOVDQA xmm1, xmm0
	PSRLDQ xmm1, 2

	PUNPCKLQDQ xmm0, xmm1

	MOVDQA xmm2, xmm1
	PSRLDQ xmm2, 4
	PSRLDQ xmm1, 2

	PUNPCKLQDQ xmm1, xmm2


	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_DownLeftIPred4x4	ENDP


ALIGN 16
H264_DownRightIPred4x4	PROC
	MOVDQA xmm0, [rdx]
	PSHUFLW xmm1, [rdx+16], 01Bh
		
	PCMPEQD xmm3, xmm3

	PSLLDQ xmm0, 2
	PINSRW xmm0, WORD PTR [rdx+30], 0
	
	PUNPCKLQDQ xmm1, xmm0 ; y 3210-1 x 012

	MOVDQA xmm2, xmm1
	PSRLDQ xmm2, 2
	PSRLDQ xmm0, 2

	PUNPCKLQDQ xmm2, xmm0 ; y210-1 x 0123

	MOVDQA xmm0, xmm2

	PSUBW xmm2, xmm3
	PSLLW xmm2, 1

	PSRLDQ xmm0, 2

	PADDW xmm0, xmm1
	PADDW xmm0, xmm2

	PSRAW xmm0, 2
	
	MOVDQA xmm2, xmm0
	MOVDQA xmm1, xmm0

	PSRLDQ xmm1, 2
	PUNPCKLQDQ xmm1, xmm0

	PSRLDQ xmm2, 4
	PSRLDQ xmm0, 6

	PUNPCKLQDQ xmm0, xmm2


	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_DownRightIPred4x4	ENDP

ALIGN 16
H264_VRightIPred4x4	PROC
	MOVDQA xmm0, [rdx]
	PSHUFLW xmm1, [rdx+16], 06h

	PINSRW xmm1, WORD PTR [rdx+30], 3

	PUNPCKLQDQ xmm1, xmm0

	PCMPEQD xmm3, xmm3

	MOVDQA xmm0, xmm1
	MOVDQA xmm2, xmm1

	PSUBW xmm1, xmm3

	PSRLDQ xmm1, 2
	PSRLDQ xmm2, 4


	PADDW xmm2, xmm0
	PADDW xmm0, xmm1

	PSLLW xmm1, 1
	PADDW xmm1, xmm2

	PSRAW xmm0, 1
	PSRAW xmm1, 2

	MOVD eax, xmm1

	PSRLDQ xmm0, 6
	PSRLDQ xmm1, 4
	
	PUNPCKLQDQ xmm0, xmm1

	MOVDQA xmm1, xmm0

	PSLLDQ xmm1, 2

	PINSRW xmm1, eax, 4
	SHR eax, 16
	PINSRW xmm1, eax, 0

	
	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_VRightIPred4x4	ENDP




ALIGN 16
H264_HDownIPred4x4	PROC

	PSHUFLW xmm1, [rdx], 06h
	MOVDQA xmm0, [rdx+16]
	PINSRW xmm1, WORD PTR [rdx + 30], 3
	
	PUNPCKLQDQ xmm1, xmm0

	PCMPEQD xmm3, xmm3

	MOVDQA xmm0, xmm1
	MOVDQA xmm2, xmm1
	
	PSUBW xmm1, xmm3

	PSRLDQ xmm1, 2
	PSRLDQ xmm2, 4

	PADDW xmm2, xmm0
	PADDW xmm0, xmm1 
	PSLLW xmm1, 1
	PADDW xmm1, xmm2

	PSRAW xmm0, 1
	PSRAW xmm1, 2

	MOVD eax, xmm1

	PSRLDQ xmm1, 4
	PSRLDQ xmm0, 6

	MOVDQA xmm2, xmm0
	MOVDQA xmm3, xmm1

	PSLLDQ xmm2, 2
	PSLLDQ xmm3, 2

	PINSRW xmm3, eax, 0
	SHR eax, 16
	PINSRW xmm2, eax, 0


	PUNPCKLWD xmm0, xmm1
	PUNPCKLWD xmm2, xmm3

	MOVDQA xmm1, xmm0
	PUNPCKLDQ xmm0, xmm2
	PUNPCKHDQ xmm1, xmm2

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_HDownIPred4x4	ENDP

ALIGN 16
H264_VLeftIPred4x4	PROC
	MOVDQA xmm0, [rdx]

	PCMPEQD xmm3, xmm3

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0
	PSUBW xmm1, xmm3	

	PSRLDQ xmm1, 2
	PSRLDQ xmm2, 4

	PADDW xmm2, xmm0
	PADDW xmm0, xmm1
	PSLLW xmm1, 1
	PADDW xmm1, xmm2

	PSRAW xmm1, 2
	PSRAW xmm0, 1

	MOVDQA xmm2, xmm0

	PUNPCKLQDQ xmm0, xmm1
	PSRLDQ xmm2, 2
	PSRLDQ xmm1, 2
	PUNPCKLQDQ xmm2, xmm1

	PADDW xmm0, [rcx]
	PADDW xmm2, [rcx+16]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm2
	
	RET
H264_VLeftIPred4x4	ENDP

ALIGN 16
H264_HUpIPred4x4	PROC
	MOVDQA xmm0, [rdx+16]

	PSHUFHW xmm0, xmm0, 0

	PCMPEQD xmm3, xmm3

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0

	PSUBW xmm1, xmm3
	PSRLDQ xmm1, 2
	PSRLDQ xmm2, 4

	PADDW xmm2, xmm0
	PADDW xmm0, xmm1
	PSLLW xmm1, 1
	PADDW xmm1, xmm2


	PSRAW xmm0, 1
	PSRAW xmm1, 2

	MOVDQA xmm2, xmm0
	PUNPCKLWD xmm0, xmm1

	PSRLDQ xmm2, 2
	PSRLDQ xmm1, 2

	PUNPCKLWD xmm2, xmm1

	MOVDQA xmm1, xmm0

	PUNPCKLDQ xmm0, xmm2
	PUNPCKHDQ xmm1, xmm2


	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1

	RET
H264_HUpIPred4x4	ENDP






END

