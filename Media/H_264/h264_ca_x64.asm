
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


EXTERN	?m_n@CABAC@H264@Video@@2QAY0IAA@$$CBFA:WORD

.CONST
ALIGN 16
word_one	WORD	1, 1, 1, 1, 1, 1, 1, 1
word_126	WORD	126, 126, 126, 126, 126, 126, 126, 126
word_64		WORD	64, 64, 64, 64, 64, 64, 64, 64
word_256	WORD	256, 256, 256, 256, 256, 256, 256, 256

.CODE



ALIGN 16
H264_DS_GetZeroRun	PROC
	XOR rax, rax
	MOV ah, dl

	MOV r8, [rcx] ; pointer
	MOV rdx, [rcx+8] ; mask	
	MOV r9, [rcx+16] ; size

	CMP r9, 0
	JLE exit

	MOV r10, rdx
	SHR r10, 32

align 16
	nextbit:
		INC al
		CMP al, ah
		JA noaa

		TEST dl, [r8]
		JNZ gotit
			ROR dl, 1
			JNC nextbit

				INC r8
				DEC r9
				JZ valready

				SHL r10d, 8
				MOV r10b, [r8]
				AND r10d, 0FFFFFFh

				MOV r11d, r10d
				XOR r11d, 3
				JNZ nextbit
					INC r8
					DEC r9
					JZ valready

					SHL r10d, 8
					MOV r10b, [r8]
				JMP nextbit
align 16
	gotit:		
		ROR dl, 1
		JNC noaa
			INC r8
			DEC r9
			JZ valready

			SHL r10d, 8
			MOV r10b, [r8]
			AND r10d, 0FFFFFFh

			MOV r11d, r10d
			XOR r11d, 3
			JNZ noaa
				INC r8
				DEC r9
				JZ valready

				SHL r10d, 8
				MOV r10b, [r8]
align 16
	noaa:		
		DEC al
		MOV ah, 0
		
align 16
	valready:
	
		SHL r10, 32
		MOV r10b, dl

		MOV [rcx], r8
		MOV [rcx+8], r10
		MOV [rcx+16], r9

exit:

	RET
H264_DS_GetZeroRun	ENDP





ALIGN 16
H264_DS_GetNumber	PROC
	XOR r10, r10
	MOV r9, rcx

	MOV r8, [r9] ; pointer
	MOV rax, [r9+8] ; mask	
	CMP QWORD PTR [r9+16], 0
	JLE exit

	MOV r10, rax
	SHR r10, 32

	BSF cx, ax
	INC cx

	SHL eax, 1
	DEC eax
	AND al, [r8]

	SUB cl, dl
	JNS noeload
align 16	
	bload:
		INC r8
		DEC QWORD PTR [r9+16]
		JZ nonext
			SHL r10d, 8
			MOV r10b, [r8]
			AND r10d, 0FFFFFFh

			MOV r11d, r10d
			XOR r11d, 3
			JZ bload

		SHL rax, 8
		MOV al, [r8]
	ADD cl, 8
	JS bload
align 16
	noeload:
		TEST cl, cl
		JNZ nonext
			INC r8
			DEC QWORD PTR [r9+16]
			JZ nonext

			SHL r10d, 8
			MOV r10b, [r8]
			AND r10d, 0FFFFFFh

			MOV r11d, r10d
			XOR r11d, 3
			JNZ nonext
				INC r8
				DEC QWORD PTR [r9+16]
				JZ nonext

				SHL r10d, 8
				MOV r10b, [r8]

	nonext:
		SHR rax, cl

		SHL r10, 32
		MOV r10b, 080h
		ROL r10b, cl

		MOV [r9], r8
		MOV [r9+8], r10 ; mask	

exit:

	RET
H264_DS_GetNumber	ENDP

ALIGN 16
H264_DS_GolombUnsigned	PROC
	XOR rax, rax
	XOR rdx, rdx
	MOV r9, rcx	

	MOV r8, [r9] ; pointer
	MOV rdx, [r9+8] ; mask	

	CMP QWORD PTR [r9+16], 0
	JLE exit

	MOV r10, rdx
	SHR r10, 32

align 16
	nextbit:
		INC al
		TEST dl, [r8]
		JNZ gotit
			ROR dl, 1
			JNC nextbit

				INC r8
				DEC QWORD PTR [r9+16]
				JZ valready

				SHL r10d, 8
				MOV r10b, [r8]
				AND r10d, 0FFFFFFh

				MOV r11d, r10d
				XOR r11d, 3
				JNZ nextbit
					INC r8
					DEC QWORD PTR [r9+16]
					JZ valready

					SHL r10d, 8
					MOV r10b, [r8]
				JMP nextbit
align 16
	gotit:		
		ROR dl, 1
		JNC noaa
			INC r8
			DEC QWORD PTR [r9+16]
			JZ valready

			SHL r10d, 8
			MOV r10b, [r8]
			AND r10d, 0FFFFFFh

			MOV r11d, r10d
			XOR r11d, 3
			JNZ noaa
				INC r8
				DEC QWORD PTR [r9+16]
				JZ valready

				SHL r10d, 8
				MOV r10b, [r8]
align 16
	noaa:		
		DEC al
		JZ valready


	align 16
		getnumber:
			BSF cx, dx
			INC cx

			SHL edx, 2
			DEC edx
			
			AND dl, [r8]

			SUB cl, al
			JNS noeload
		align 16	
			bload:
				INC r8
				DEC QWORD PTR [r9+16]
				JZ valready

					SHL r10d, 8
					MOV r10b, [r8]
					AND r10d, 0FFFFFFh

					MOV r11d, r10d
					XOR r11d, 3
					JZ bload

				SHL rdx, 8
				MOV dl, [r8]
			ADD cl, 8
			JS bload
	align 16
		noeload:
			TEST cl, cl
			JNZ nonext
				INC r8
				DEC QWORD PTR [r9+16]
				JZ valready

				SHL r10d, 8
				MOV r10b, [r8]
				AND r10d, 0FFFFFFh
				
				MOV r11d, r10d
				XOR r11d, 3
				JNZ nonext
					INC r8
					DEC QWORD PTR [r9+16]
					JZ valready

					SHL r10d, 8
					MOV r10b, [r8]
align 16
	nonext:
		SHR rdx, cl
		MOV rax, rdx
		DEC rax

		MOV dl, 080h
		ROL dl, cl
		
align 16
	valready:
	
		SHL r10, 32
		MOV r10b, dl

		MOV [r9], r8
		MOV [r9+8], r10

exit:

	RET
H264_DS_GolombUnsigned	ENDP



ALIGN 16
H264_CABAC_InitCtxVars	PROC
	MOV eax, edx
	SHL eax, 16
	OR eax, edx
	
	MOVD xmm0, eax
	PSHUFD xmm0, xmm0, 0

	MOVDQA xmm6, XMMWORD PTR [word_one]
	MOVDQA xmm7, XMMWORD PTR [word_126]
	MOVDQA xmm4, XMMWORD PTR [word_64]
	MOVDQA xmm8, XMMWORD PTR [word_256]
	
	PCMPEQD xmm5, xmm5

	LEA r11, [?m_n@CABAC@H264@Video@@2QAY0IAA@$$CBFA]

	SHL r8, 12
	ADD r11, r8 ; m_n pointer

	MOV r8, 32

align 16
	init_loop:

		MOVDQA xmm1, [r11]
		PMULLW xmm1, xmm0
		PSRAW xmm1, 4
		PADDW xmm1, [r11+16]

		MOVDQA xmm2, xmm1
		MOVDQA xmm3, xmm7
	
		PCMPGTW xmm3, xmm1 ; < 126
		PCMPGTW xmm2, xmm6 ; > 1

		PAND xmm1, xmm3
		PAND xmm1, xmm2

		PXOR xmm3, xmm5
		PXOR xmm2, xmm5

		PAND xmm3, xmm7	
		PAND xmm2, xmm6

		POR xmm1, xmm2
		POR xmm1, xmm3

		PSUBW xmm1, xmm4 ; 64
		PXOR xmm2, xmm2

		PCMPGTW xmm2, xmm1 ; state <= 63
		MOVDQA xmm3, xmm2
		PXOR xmm3, xmm5 ; state > 63

		PAND xmm3, xmm1

		PADDW xmm1, xmm6
		PAND xmm1, xmm2

		PXOR xmm2, xmm5
		PAND xmm2, xmm8
		
		PSRLW xmm2, 2
		POR xmm3, xmm2

		PSUBW xmm3, xmm1



		MOVDQA xmm1, [r11+32]
		PMULLW xmm1, xmm0
		PSRAW xmm1, 4
		PADDW xmm1, [r11+48]

		MOVDQA xmm2, xmm1
		MOVDQA xmm9, xmm7
	
		PCMPGTW xmm9, xmm1 ; < 126
		PCMPGTW xmm2, xmm6 ; > 1

		PAND xmm1, xmm9
		PAND xmm1, xmm2

		PXOR xmm9, xmm5
		PXOR xmm2, xmm5

		PAND xmm9, xmm7	
		PAND xmm2, xmm6

		POR xmm1, xmm2
		POR xmm1, xmm9

		PSUBW xmm1, xmm4 ; 64
		PXOR xmm2, xmm2

		PCMPGTW xmm2, xmm1 ; state <= 63
		MOVDQA xmm9, xmm2
		PXOR xmm9, xmm5 ; state > 63

		PAND xmm9, xmm1

		PADDW xmm1, xmm6
		PAND xmm1, xmm2

		PXOR xmm2, xmm5
		PAND xmm2, xmm8
		
		PSRLW xmm2, 2
		POR xmm9, xmm2

		PSUBW xmm9, xmm1



		MOVDQA xmm1, [r11+64]
		PMULLW xmm1, xmm0
		PSRAW xmm1, 4
		PADDW xmm1, [r11+80]

		MOVDQA xmm2, xmm1
		MOVDQA xmm10, xmm7
	
		PCMPGTW xmm10, xmm1 ; < 126
		PCMPGTW xmm2, xmm6 ; > 1

		PAND xmm1, xmm10
		PAND xmm1, xmm2

		PXOR xmm10, xmm5
		PXOR xmm2, xmm5

		PAND xmm10, xmm7	
		PAND xmm2, xmm6

		POR xmm1, xmm2
		POR xmm1, xmm10

		PSUBW xmm1, xmm4 ; 64
		PXOR xmm2, xmm2

		PCMPGTW xmm2, xmm1 ; state <= 63
		MOVDQA xmm10, xmm2
		PXOR xmm10, xmm5 ; state > 63

		PAND xmm10, xmm1

		PADDW xmm1, xmm6
		PAND xmm1, xmm2

		PXOR xmm2, xmm5
		PAND xmm2, xmm8
		
		PSRLW xmm2, 2
		POR xmm10, xmm2

		PSUBW xmm10, xmm1



		MOVDQA xmm1, [r11+96]
		PMULLW xmm1, xmm0
		PSRAW xmm1, 4
		PADDW xmm1, [r11+112]

		MOVDQA xmm2, xmm1
		MOVDQA xmm11, xmm7
	
		PCMPGTW xmm11, xmm1 ; < 126
		PCMPGTW xmm2, xmm6 ; > 1

		PAND xmm1, xmm11
		PAND xmm1, xmm2

		PXOR xmm11, xmm5
		PXOR xmm2, xmm5

		PAND xmm11, xmm7	
		PAND xmm2, xmm6

		POR xmm1, xmm2
		POR xmm1, xmm11

		PSUBW xmm1, xmm4 ; 64
		PXOR xmm2, xmm2

		PCMPGTW xmm2, xmm1 ; state <= 63
		MOVDQA xmm11, xmm2
		PXOR xmm11, xmm5 ; state > 63

		PAND xmm11, xmm1

		PADDW xmm1, xmm6
		PAND xmm1, xmm2

		PXOR xmm2, xmm5
		PAND xmm2, xmm8
		
		PSRLW xmm2, 2
		POR xmm11, xmm2

		PSUBW xmm11, xmm1


		PACKUSWB xmm3, xmm9
		PACKUSWB xmm10, xmm11

		MOVDQA [rcx], xmm3
		MOVDQA [rcx+16], xmm10

		ADD r11, 128
		ADD rcx, 32

	DEC r8
	JNZ init_loop


	RET
H264_CABAC_InitCtxVars	ENDP


END

