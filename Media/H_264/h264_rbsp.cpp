/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\H264.h"
#include "System\SysUtils.h"

#pragma warning(disable:4244)

UI_64 Video::H264::CfgOverride(UI_64, UI_64) {

	return 0;
}

void Video::H264::Slice::Clear() {
	System::MemoryFill(weight_table, 32*2*3*sizeof(WeightTable), 0);

	pic_set = 0;
	seq_set = 0;

	first_mb = 0;
	max_pic_num = 0;

	id_rec.Clear();

	mb_skip_run = 0;
	gray_val[0] = gray_val[1] = 0x00000080;

	_type = 0;
	weight_type = 0;
	colour_plane_id = 0;

	redundant_pic_cnt = 0;
	direct_spatial_mv_pred_flag = 1;

	num_ref_idx_active[0] = num_ref_idx_active[1] = 0;

	qpy = qsy = 0;

	sp_for_switch_flag = 0;

	disable_deblocking_filter_idc = 0;
	alpha_c0_offset = 0;
	beta_offset = 0;

	group_charge_cycle = 0;
	weight_denom_bits[0] = weight_denom_bits[1] = weight_denom_bits[2] = weight_denom_bits[3] = 0;

}

Video::H264::H264() : pp_count(0), sp_count(0), out_pin(0), stop_hnd(0), decode_skip_count(0) {
	SFSco::Object temp_obj;
	codec_id = SFSco::VideoType::H264;

	stop_hnd = CreateEvent(0, 0, 1, 0);

	System::MemoryFill(&nal_hrd, sizeof(HRD), 0);
	System::MemoryFill(&vcl_hrd, sizeof(HRD), 0);
		
	for (unsigned int i(0);i<MAX_REF_IDX;i++) {
		time_table[i][0] = -1; (UI_64 &)time_table[i][0] >>= 1;
		time_table[i][1] = -1;
	}

	System::MemoryFill(reinterpret_cast<unsigned char *>(&vui_params), sizeof(VUI), 0);
	System::MemoryCopy(&vui_params.svc_buf, &temp_obj, sizeof(SFSco::Object));
	System::MemoryCopy(&vui_params.mvc_buf, &temp_obj, sizeof(SFSco::Object));
	vui_params.video_format = 5;
	vui_params.colour_primaries = 2;
	
	vui_params.matrix_coefficients = 1;
	
	ifuncs[0][0] = H264_Interpolate_G;
	ifuncs[0][1] = H264_Interpolate_d;
	ifuncs[0][2] = H264_Interpolate_h;
	ifuncs[0][3] = H264_Interpolate_n;

	ifuncs[1][0] = H264_Interpolate_a;
	ifuncs[1][1] = H264_Interpolate_e;
	ifuncs[1][2] = H264_Interpolate_i;
	ifuncs[1][3] = H264_Interpolate_p;

	ifuncs[2][0] = H264_Interpolate_b;
	ifuncs[2][1] = H264_Interpolate_f;
	ifuncs[2][2] = H264_Interpolate_j;
	ifuncs[2][3] = H264_Interpolate_q;

	ifuncs[3][0] = H264_Interpolate_c;
	ifuncs[3][1] = H264_Interpolate_gg;
	ifuncs[3][2] = H264_Interpolate_k;
	ifuncs[3][3] = H264_Interpolate_r;


	i_pred_4x4[0] = H264_VerticalIPred4x4;
	i_pred_4x4[1] = H264_HorizontalIPred4x4;
	i_pred_4x4[2] = H264_DCIPred4x4;
	i_pred_4x4[3] = H264_DownLeftIPred4x4;
	i_pred_4x4[4] = H264_DownRightIPred4x4;
	i_pred_4x4[5] = H264_VRightIPred4x4;
	i_pred_4x4[6] = H264_HDownIPred4x4;
	i_pred_4x4[7] = H264_VLeftIPred4x4;
	i_pred_4x4[8] = H264_HUpIPred4x4;


	i_pred_8x8[0] = H264_VerticalIPred8x8;
	i_pred_8x8[1] = H264_HorizontalIPred8x8;
	i_pred_8x8[2] = H264_DCIPred8x8;
	i_pred_8x8[3] = H264_DownLeftIPred8x8;
	i_pred_8x8[4] = H264_DownRightIPred8x8;
	i_pred_8x8[5] = H264_VRightIPred8x8;
	i_pred_8x8[6] = H264_HDownIPred8x8;
	i_pred_8x8[7] = H264_VLeftIPred8x8;
	i_pred_8x8[8] = H264_HUpIPred8x8;

}

Video::H264::~H264() {
	
	if (stop_hnd) {
		WaitForSingleObject(stop_hnd, INFINITE);
		CloseHandle(stop_hnd);
	}

	for (unsigned int i(0);i<MAX_REF_IDX;i++) {
		pic_list[i].Destroy();
	}

	frame_list_count = 0;

	vui_params.svc_buf.Destroy();
	vui_params.mvc_buf.Destroy();
	picture_params.Destroy();
	sequence_params.Destroy();
}

Video::H264::DecoderState::DecoderState() {
	System::MemoryFill(this, sizeof(DecoderState), 0);
	
	bit_mask[0] = 0x00000080;
	bit_mask[1] = 0x000000FF;

	color_transform_override = 255;
	deblock_override = 1;
}


int Video::H264::DecoderState::GolombSigned() {
	int result(H264_DS_GolombUnsigned(this));
	
	if (result & 1) result = (result+1)>>1;
	else result = -(result>>1);

	return result;
}

int Video::H264::Clip3(int x, int y, int z) {	
	z = (((z-x) >> 31) & x) | ((~((z-x) >> 31)) & z);
	z = (((y-z) >> 31) & y) | ((~((y-z) >> 31)) & z);
	return z;
}


// =============================================================================================================================================
void Video::H264::JumpOn() {
	decoder_cfg.jump_flag = 1;
}

UI_64 Video::H264::DecodeUnit(const unsigned char * u_ptr, UI_64 u_length, I_64 & time_mark) {
	UI_64 result(-3);
	unsigned char temp_vals[8];

	unsigned char svc_ext(0);

	result <<= 32;


	System::MemoryFill(&NAL, sizeof(NAL_Unit), 0);
	NAL.unit._bval = u_ptr[0];
								
	decoder_cfg.nal_counter++;

	decoder_cfg.SetSource(u_ptr+1, u_length-1);

	decoder_cfg.flag_set = 0;

	
	svc_ext = 0;

	if ((NAL.unit._type == 14) || (NAL.unit._type == 20) || (NAL.unit._type == 21)) {					
												
		if (svc_ext = decoder_cfg.NextBit()) { // svc_extension
			NAL.SVC.idr_flag = decoder_cfg.NextBit();

			NAL.SVC.priority_id = H264_DS_GetNumber(&decoder_cfg, 6);

			NAL.SVC.no_inter_layer_pred_flag = decoder_cfg.NextBit();

			NAL.SVC.dependency_id = H264_DS_GetNumber(&decoder_cfg, 3);
			NAL.SVC.quality_id = H264_DS_GetNumber(&decoder_cfg, 4);
			NAL.SVC.temporal_id = H264_DS_GetNumber(&decoder_cfg, 3);

			NAL.SVC.use_ref_base_pic_flag = decoder_cfg.NextBit();

			NAL.SVC.discardable_flag = decoder_cfg.NextBit();
	
			NAL.SVC.ouput_flag = decoder_cfg.NextBit();
							

		} else { // mvc_extension
			NAL.MVC.non_idr_flag = decoder_cfg.NextBit();

			NAL.MVC.priority_id = H264_DS_GetNumber(&decoder_cfg, 6);
			NAL.MVC.view_id = H264_DS_GetNumber(&decoder_cfg, 10);
			NAL.MVC.temporal_id = H264_DS_GetNumber(&decoder_cfg, 3);

			NAL.MVC.anchor_pic_flag = decoder_cfg.NextBit();

			NAL.MVC.inter_view_flag = decoder_cfg.NextBit();

		}

		decoder_cfg.ByteAlign();
	}
					
					
	if (decoder_cfg.IsSource())
	switch (NAL.unit._type) {
		case 5: // slice of IDR picture
			decoder_cfg.reset_op = 1;
		case 1: // slice of non-IDR picture						
		case 19: // slice of auxiliary picture
			result = 0;
			if ((_src_desc->_state_flags & SOURCE_NO_RENDER) == 0) result = GetSliceLayer(time_mark);
			else result = RESULT_NO_RENDER;
		break;


		case 2: case 3: case 4: // not supported

/*
			decoder_cfg.flag_set |= FLAG_PARTITION_A; // partitioning |= 1;
			GetSlicePartitionA();

		 // slice daa partition B
			decoder_cfg.flag_set |= FLAG_PARTITION_B;
		 // slice daa partition C
			decoder_cfg.flag_set |= FLAG_PARTITION_C;
			GetSlicePartitionB();
*/
			
		break;
						

		case 6: // SEI (new access unit)
							
			temp_vals[1] = 0;

			for (temp_vals[3] = decoder_cfg.NextByte(); temp_vals[3] == 0xFF; temp_vals[3] = decoder_cfg.NextByte()) {
				temp_vals[1]--;
			}

			temp_vals[1] += temp_vals[3]; // type
			
			temp_vals[2] = 0;
			for (temp_vals[3] = decoder_cfg.NextByte(); temp_vals[3] == 0xFF; temp_vals[3] = decoder_cfg.NextByte()) {
				temp_vals[2]--;
			}

			temp_vals[2] += temp_vals[3]; // size

			// analize message


			result = 0;
		break;
		case 7: // sequence parameter set (new access unit)
			if (GetSeqParameters() < 32) result = decoder_cfg.IsMoreRBSP();
		break;
		case 8: // picture parameter set (new acess unit)
			if (GetPicParameters() < 256) result = decoder_cfg.IsMoreRBSP();
							
		break;
		case 9: // access unit delimiter
			NAL.primary_pic_type = H264_DS_GetNumber(&decoder_cfg, 3);
			result = 0;		
		break;
		case 10: // end of sequence

		break;
		case 11: // end of stream

		break;
		case 12: // filter data, just ignore it
							
		break;
		case 13: // sequence parameter set extension
			result = H264_DS_GolombUnsigned(&decoder_cfg);
			if (result < sp_count) {
				if (SequencePSet * sset = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) {
					sset = reinterpret_cast<SequencePSet *>(reinterpret_cast<char *>(sset) + result*seq_psize);

					if (sset->extension.aux_format_idc = H264_DS_GolombUnsigned(&decoder_cfg)) {
						sset->extension.bit_depth_aux8 = H264_DS_GolombUnsigned(&decoder_cfg);

						sset->extension.alpha_incr_flag = decoder_cfg.NextBit();

						sset->extension.alpha_opaque = H264_DS_GetNumber(&decoder_cfg, sset->extension.bit_depth_aux8+9);
						sset->extension.alpha_transparent = H264_DS_GetNumber(&decoder_cfg, sset->extension.bit_depth_aux8+9);
					}

					sset->extension.additional_ext_flag = decoder_cfg.NextBit();

					sequence_params.Release();
				}
			}
			result = 0;
		break;
		case 14: // prefix NAL unit
			if (svc_ext) {
				if (NAL.unit.ref_idc) {
					NAL.SVC.store_ref_base_pic_flag = decoder_cfg.NextBit();

					if ((NAL.SVC.use_ref_base_pic_flag || NAL.SVC.store_ref_base_pic_flag) && (NAL.SVC.idr_flag == 0)) {
						NAL.SVC.adaptive_ref_base_pic_marking_mode_flag = decoder_cfg.NextBit();

						temp_vals[5] = 1;
						if (NAL.SVC.adaptive_ref_base_pic_marking_mode_flag) {
							for (;temp_vals[5];){
								temp_vals[5] = H264_DS_GolombUnsigned(&decoder_cfg);

								switch (temp_vals[5]) {
									case 1:
										NAL.SVC.difference_of_base_pic_nums = H264_DS_GolombUnsigned(&decoder_cfg);
									break;
									case 2:
										NAL.SVC.long_term_base_pic = H264_DS_GolombUnsigned(&decoder_cfg);
									break;
								}
							}
						}
					}

				} else {
					
				}
			}
			result = 0;
		break;
		case 15: // subset sequence parameter
							
			result = GetSeqParameters();

			if (result < sp_count) {
				if (SequencePSet * sset = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) {
					sset = reinterpret_cast<SequencePSet *>(reinterpret_cast<char *>(sset) + result*seq_psize);

					switch (sset->profile_idc) {
						case 83: case 86:
							GetSVCExtension(sset);

							if (decoder_cfg.NextBit()) {
								GetSVCVUI(sset);
							}
						break;
						case 118: case 128:
							decoder_cfg.NextBit();

							GetMVCExtension(sset);

							if (decoder_cfg.NextBit()) {
								GetMVCVUI(sset);
							}

						break;
						case 138:
							decoder_cfg.NextBit();

							GetMVCDExtension(sset);
						break;
					}

					sequence_params.Release();

					result = 0;
				}				
			}
/*
			seq_params.extension.additional_ext2 = nal_ptr[0] & (1<<(-bit_position));
			if ((++bit_position &= 7) == 0) {
				nal_ptr++;
				result++;
			}

			if (seq_params.extension.additional_ext2) {
				seq_params.extension.additional_ext2_data = nal_ptr[0] & (1<<(-bit_position));
				if ((++bit_position &= 7) == 0) {
					nal_ptr++;
					result++;
				}
			}
*/
		break;
			
		case 20: // slice extension
			if (svc_ext) {


			} else {
				result = GetSliceLayer(time_mark);
			}
		break;
		case 21: // slice extension for depth view
			result = 0;
		break;
						
	}

	return result;
}

UI_64 Video::H264::Unpack(const unsigned char * data_ptr, UI_64 data_length, I_64 & time_mark) {
	UI_64 result(0);
	I_64 ref_mark(0), x_mark(time_mark);

	unsigned char temp_vals[8];

	for (;data_length>8;) {
		temp_vals[0] = data_ptr[0] | data_ptr[1];

		if (temp_vals[0] == 0) {
			for (;((temp_vals[0] |= data_ptr[2]) == 0);) {
				data_ptr++;
				data_length--;
			}

			if (temp_vals[0] == 1) { // access unit delimiter				
				data_ptr += 3; data_length -= 3;
				
				if (UI_64 ssize = Media::Codec_GetNALSize(data_ptr, data_length)) {					
					if (ssize > 1) {
						ref_mark = time_mark;

						result = DecodeUnit(data_ptr, ssize, ref_mark);
						if (result & RESULT_DECODE_COMPLETE) {
							x_mark = ref_mark;
							if (decode_skip_count < 0) break; // -1
						}
					}

					data_ptr += ssize;
					data_length -= ssize;									

					if (result > 0x01000000) break;
					
				} else {
					// error					
				}

			} else {
				// error
				result = 'aud1';
				result <<= 32;
				break;
			}


		} else {
			result = 'aud0';
			result <<= 32;
			// error
			break;
		}
	}

	if (x_mark != time_mark) {
		time_mark = x_mark;
		result |= RESULT_DECODE_COMPLETE;
	}

	return result;
}

UI_64 Video::H264::GetPicParameters() {
	UI_64 result(0), tsval(0);
	unsigned int top_bit(0);
	short next_scale(0);

	result = H264_DS_GolombUnsigned(&decoder_cfg);
	
	if (result < 256) {
		if (result >= pp_count) {
			tsval = (result - pp_count + 4) & (~3);
			if (picture_params.Expand(tsval*pic_psize) != -1) {
				pp_count += tsval;
			}
		}

		result |= 0x80000000;
		if (PicturePSet * pic_params = reinterpret_cast<PicturePSet *>(picture_params.Acquire())) {
			pic_params = reinterpret_cast<PicturePSet *>(reinterpret_cast<char *>(pic_params) + (result & 0x00FF)*pic_psize);

			System::MemoryFill(pic_params, pic_psize, 0);

			pic_params->sp_set_id = H264_DS_GolombUnsigned(&decoder_cfg);
			if (pic_params->sp_set_id < sp_count) {
				if (SequencePSet * seq_params = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) {
					seq_params = reinterpret_cast<SequencePSet *>(reinterpret_cast<char *>(seq_params)+pic_params->sp_set_id*seq_psize);

					if (seq_params->valid) {

						pic_params->valid = 1;
				
						pic_params->entropy_coding_mode_flag = decoder_cfg.NextBit();

						pic_params->bottom_field_pic_order_flag = decoder_cfg.NextBit();

						if (pic_params->num_slice_groups = H264_DS_GolombUnsigned(&decoder_cfg)) {
							pic_params->slice_group_map_type = H264_DS_GolombUnsigned(&decoder_cfg);
				
							switch (pic_params->slice_group_map_type = H264_DS_GolombUnsigned(&decoder_cfg)) {
								case 0:
									for (unsigned char i(0);i<=pic_params->num_slice_groups;i++) pic_params->slice_groups[i].run_length = H264_DS_GolombUnsigned(&decoder_cfg);
								break;

								case 2:
									for (unsigned char i(0);i<pic_params->num_slice_groups;i++) {
										pic_params->slice_groups[i].top_left = H264_DS_GolombUnsigned(&decoder_cfg);
										pic_params->slice_groups[i].bottom_right = H264_DS_GolombUnsigned(&decoder_cfg);
									}
								break;

								case 3:		
								case 4:
								case 5:
									pic_params->slice_group_change_direction_flag = decoder_cfg.NextBit();

									pic_params->slice_group_change_rate = H264_DS_GolombUnsigned(&decoder_cfg);
								break;

								case 6:
									pic_params->size_map_units = H264_DS_GolombUnsigned(&decoder_cfg);

									top_bit = pic_params->num_slice_groups+1;
									top_bit = System::BitIndexHigh_32(top_bit) + 1;

									for (unsigned char i(0);i<=pic_params->num_slice_groups;i++) {
										pic_params->slice_groups[i].id = H264_DS_GetNumber(&decoder_cfg, top_bit);
									}
								break;

							}
						}

						pic_params->num_ref_idx[0] = H264_DS_GolombUnsigned(&decoder_cfg);
						pic_params->num_ref_idx[1] = H264_DS_GolombUnsigned(&decoder_cfg);

						pic_params->weighted_pred = decoder_cfg.NextBit();

						pic_params->weighted_bipred = H264_DS_GetNumber(&decoder_cfg, 2);

						pic_params->init_qp = decoder_cfg.GolombSigned() + 26;
						pic_params->init_qs = decoder_cfg.GolombSigned() + 26;

						pic_params->chroma_qpinx_offset[1] = decoder_cfg.GolombSigned();


						pic_params->deblocking_filter_control_flag = decoder_cfg.NextBit();

						pic_params->constrained_intra_pred_flag = decoder_cfg.NextBit();
			
						pic_params->redundant_pic_cnt_flag = decoder_cfg.NextBit();

						// inialize scaling lists to seq set lists
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->intra_scaling_list_4x4[0][ij][0], seq_params->intra_scaling_list_4x4[ij], 16*sizeof(short));
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->intra_scaling_list_8x8[0][ij][0], seq_params->intra_scaling_list_8x8[ij], 64*sizeof(short));
						
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->inter_scaling_list_4x4[0][ij][0], seq_params->inter_scaling_list_4x4[ij], 16*sizeof(short));
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->inter_scaling_list_8x8[0][ij][0], seq_params->inter_scaling_list_8x8[ij], 64*sizeof(short));
						

						if (decoder_cfg.IsMoreRBSP()) {
							pic_params->trasnform_8x8_mode_flag = decoder_cfg.NextBit();

							GetPicScaling(pic_params, seq_params->scaling_matrix_present, seq_params->chroma_format_idc);

							pic_params->chroma_qpinx_offset[2] = decoder_cfg.GolombSigned();
						} else {
							pic_params->chroma_qpinx_offset[2] = pic_params->chroma_qpinx_offset[1];
						}


						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->intra_scaling_list_4x4[1][ij][0], pic_params->intra_scaling_list_4x4[0][ij][0], 16*sizeof(short));
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->intra_scaling_list_8x8[1][ij][0], pic_params->intra_scaling_list_8x8[0][ij][0], 64*sizeof(short));
						
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->inter_scaling_list_4x4[1][ij][0], pic_params->inter_scaling_list_4x4[0][ij][0], 16*sizeof(short));
						for (unsigned int ij(0);ij<3;ij++) System::MemoryCopy(pic_params->inter_scaling_list_8x8[1][ij][0], pic_params->inter_scaling_list_8x8[0][ij][0], 64*sizeof(short));


						H264_NormalizeScalingLists(pic_params->intra_scaling_list_4x4, pic_params->intra_scaling_list_8x8);
						H264_NormalizeScalingLists(pic_params->inter_scaling_list_4x4, pic_params->inter_scaling_list_8x8);

						result &= 0x00FF;
					}

					sequence_params.Release();
				}				
			}
			picture_params.Release();
		}
	}

	return result;
}

UI_64 Video::H264::GetSeqParameters() {
	UI_64 result(0), tsval(0);
	SFSco::Object temp_obj, mbset;
	unsigned int top_bit(0), tempb[3];


	tempb[0] = decoder_cfg.NextByte();

	tempb[1] = decoder_cfg.NextByte();

	tempb[2] = decoder_cfg.NextByte();

	result = H264_DS_GolombUnsigned(&decoder_cfg);

	if (result < 32) {
		if (result >= sp_count) {
			tsval = (result - sp_count + 4) & (~3);
			if (sequence_params.Expand(tsval*seq_psize) != -1) {
				sp_count += tsval;
			}
			tsval = 0;
		}

		if (SequencePSet * seq_params = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) {
			seq_params = reinterpret_cast<SequencePSet *>(reinterpret_cast<char *>(seq_params)+result*seq_psize);

			System::MemoryFill(seq_params, seq_psize, 0);

			seq_params->profile_idc = tempb[0];
			seq_params->constraint_flags._bval = tempb[1];
			seq_params->level_idc = tempb[2];
			seq_params->valid = 0;
			seq_params->chroma_format_idc = 1; // 4:2:0 Default
			seq_params->cmp_depth[3] = 1;

			if ((seq_params->profile_idc == 100) || (seq_params->profile_idc == 110) || (seq_params->profile_idc == 122) || (seq_params->profile_idc == 244) ||
				(seq_params->profile_idc == 44) || (seq_params->profile_idc == 83) || (seq_params->profile_idc == 86) ||
				(seq_params->profile_idc == 118) || (seq_params->profile_idc == 128) || (seq_params->profile_idc == 138)) {

					seq_params->chroma_format_idc = H264_DS_GolombUnsigned(&decoder_cfg);

					if (seq_params->chroma_format_idc == 3) {
					seq_params->separate_planes_flag = decoder_cfg.NextBit();

				}

				seq_params->cmp_depth[0] = H264_DS_GolombUnsigned(&decoder_cfg);
				seq_params->cmp_depth[1] = seq_params->cmp_depth[2] = H264_DS_GolombUnsigned(&decoder_cfg);				
				if ((seq_params->cmp_depth[0] > 0) || (seq_params->cmp_depth[1] > 0)) seq_params->cmp_depth[3] = 2;

				seq_params->qpprime_y_zero_transform_bypass_flag = decoder_cfg.NextBit();

				GetSeqScaling(seq_params);

			}

			if (seq_params->scaling_matrix_present == 0) {
			// flat
				System::MemoryFill(seq_params->intra_scaling_list_4x4[0], 3*16*sizeof(short), 0x00100010);
				System::MemoryFill(seq_params->intra_scaling_list_8x8[0], 3*64*sizeof(short), 0x00100010);

				System::MemoryFill(seq_params->inter_scaling_list_4x4[0], 3*16*sizeof(short), 0x00100010);
				System::MemoryFill(seq_params->inter_scaling_list_8x8[0], 3*64*sizeof(short), 0x00100010);

			}
	
			if (seq_params->separate_planes_flag == 0) {
				switch (seq_params->chroma_format_idc) {
					case 1:
						seq_params->chroma_width = seq_params->chroma_height = 1;
					break;
					case 2:
						seq_params->chroma_width = 1;
					break;
				}
			}

			seq_params->max_frame_num_bits = H264_DS_GolombUnsigned(&decoder_cfg);

			seq_params->pic_order_cnt_type = H264_DS_GolombUnsigned(&decoder_cfg);

			switch (seq_params->pic_order_cnt_type) {
				case 0:
					seq_params->max_pic_order_cnt_bits = H264_DS_GolombUnsigned(&decoder_cfg);
					seq_params->max_pic_order_count_lsb = (1 << (seq_params->max_pic_order_cnt_bits+4));
				break;
				case 1:
					seq_params->delta_pic_order_zero_flag = decoder_cfg.NextBit();
					
					seq_params->non_ref_pic_offset = decoder_cfg.GolombSigned();
					seq_params->top2bottom_field_offset = decoder_cfg.GolombSigned();

					seq_params->num_ref_frames_in_pic_order = H264_DS_GolombUnsigned(&decoder_cfg);
					seq_params->expectedDeltaPP = 0;
					for (unsigned char i(0);i<seq_params->num_ref_frames_in_pic_order;i++) {
						seq_params->ref_frame_offset[i] = decoder_cfg.GolombSigned();
						seq_params->expectedDeltaPP += seq_params->ref_frame_offset[i];
					}

				break;
			}

			seq_params->max_num_ref_frames = H264_DS_GolombUnsigned(&decoder_cfg);
			if (seq_params->max_num_ref_frames < 2) seq_params->max_num_ref_frames = 2;

			seq_params->frame_num_gaps_flag = decoder_cfg.NextBit();
	
			seq_params->pic_width_mbs = H264_DS_GolombUnsigned(&decoder_cfg) + 1;
			seq_params->pic_height_mapunits = H264_DS_GolombUnsigned(&decoder_cfg) + 1;
				
			seq_params->frame_mbs_only_flag = decoder_cfg.NextBit();

			if (seq_params->frame_mbs_only_flag) seq_params->pic_height_mbs = seq_params->pic_height_mapunits;
			else seq_params->pic_height_mbs = seq_params->pic_height_mapunits<<1;

			if (seq_params->frame_mbs_only_flag == 0) {
				seq_params->mb_adaptive_frame_field_flag = decoder_cfg.NextBit();
			}

			seq_params->direct_8x8_inference_flag = decoder_cfg.NextBit();

			decoder_cfg.chroma_array_type = 0;
			if (seq_params->separate_planes_flag == 0) decoder_cfg.chroma_array_type = seq_params->chroma_format_idc;	


			if (decoder_cfg.NextBit()) { // croping
				seq_params->frame_crop_left = H264_DS_GolombUnsigned(&decoder_cfg);
				seq_params->frame_crop_right = H264_DS_GolombUnsigned(&decoder_cfg);
				seq_params->frame_crop_top = H264_DS_GolombUnsigned(&decoder_cfg);
				seq_params->frame_crop_bottom = H264_DS_GolombUnsigned(&decoder_cfg);

				if (decoder_cfg.chroma_array_type) {
					seq_params->frame_crop_left <<= seq_params->chroma_width;
					seq_params->frame_crop_right <<= seq_params->chroma_width;
					seq_params->frame_crop_top <<= seq_params->chroma_height;
					seq_params->frame_crop_bottom <<= seq_params->chroma_height;

				}

				if (seq_params->frame_mbs_only_flag == 0) {
					seq_params->frame_crop_top <<= 1;
					seq_params->frame_crop_bottom <<= 1;
				}					

			}




			if (decoder_cfg.NextBit()) {
				System::MemoryFill(&vui_params, sizeof(VUI), 0);
				System::MemoryCopy(&vui_params.svc_buf, &temp_obj, sizeof(SFSco::Object));
				System::MemoryCopy(&vui_params.mvc_buf, &temp_obj, sizeof(SFSco::Object));
				vui_params.video_format = 5;
				vui_params.colour_primaries = 2;				
				vui_params.matrix_coefficients = 1;

				GetVUIParameters();

//				if (decoder_cfg.color_transform_override != 255) vui_params.matrix_coefficients = decoder_cfg.color_transform_override;
			}			

			seq_params->valid = 1;

			sequence_params.Release();

		}
	}


	return result;
}

unsigned int Video::H264::GetCT() {
	if (decoder_cfg.color_transform_override != 255) return decoder_cfg.color_transform_override;
	else return vui_params.matrix_coefficients;
}

UI_64 Video::H264::SetCT(UI_64 xv) {
	decoder_cfg.color_transform_override = xv; // vui_params.matrix_coefficients = 

	return xv;
}


UI_64 Video::H264::GetSeqScaling(SequencePSet * seq_params) {
	short next_scale(0);

	if (seq_params->scaling_matrix_present = decoder_cfg.NextBit()) {
		for (unsigned int i(0);i<3;i++) { // intra
			if (decoder_cfg.NextBit()) { // scaling list present
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<15;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {							
							seq_params->intra_scaling_list_4x4[i][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
							// fill out
							next_scale = seq_params->intra_scaling_list_4x4[i][j-1];
							for (;j<15;j++) seq_params->intra_scaling_list_4x4[i][j] = next_scale;
						}
					}
					seq_params->intra_scaling_list_4x4[i][15] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(seq_params->intra_scaling_list_4x4[i], default_intra_scaling_4x4, 16*sizeof(short));
				}
			} else {
				// fall-back A
				if (i == 0) System::MemoryCopy_SSE3(seq_params->intra_scaling_list_4x4[0], default_intra_scaling_4x4, 16*sizeof(short));
				else System::MemoryCopy_SSE3(seq_params->intra_scaling_list_4x4[i], seq_params->intra_scaling_list_4x4[i-1], 16*sizeof(short));
			}
		}

		for (unsigned int i(0);i<3;i++) { // inter
			if (decoder_cfg.NextBit()) { // scaling list present
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<15;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {
							seq_params->inter_scaling_list_4x4[i][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
							// fill out
							next_scale = seq_params->inter_scaling_list_4x4[i][j-1];
							for (;j<15;j++) seq_params->inter_scaling_list_4x4[i][j] = next_scale;
						}
					}
					seq_params->inter_scaling_list_4x4[i][15] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(seq_params->inter_scaling_list_4x4[i], default_inter_scaling_4x4, 16*sizeof(short));
				}
			} else {
				// fall-back A
				if (i == 0) System::MemoryCopy_SSE3(seq_params->inter_scaling_list_4x4[0], default_inter_scaling_4x4, 16*sizeof(short));
				else System::MemoryCopy_SSE3(seq_params->inter_scaling_list_4x4[i], seq_params->inter_scaling_list_4x4[i-1], 16*sizeof(short));

			}
		}



		if (decoder_cfg.NextBit()) {
			if (next_scale = 8 + decoder_cfg.GolombSigned()) {
				for (unsigned int j(0);j<63;j++) {
					next_scale += 256;
					if (next_scale &= 0x00FF) {
						seq_params->intra_scaling_list_8x8[0][j] = next_scale;
						next_scale += decoder_cfg.GolombSigned();
					} else {
					// fill out
						next_scale = seq_params->intra_scaling_list_8x8[0][j-1];
						for (;j<63;j++) seq_params->intra_scaling_list_8x8[0][j] = next_scale;										
					}
				}
				seq_params->intra_scaling_list_8x8[0][63] = next_scale;
			} else {
				// use default
				System::MemoryCopy_SSE3(seq_params->intra_scaling_list_8x8[0], default_intra_scaling_8x8, 64*sizeof(short));
			}
		} else {
				// fall-back A
			System::MemoryCopy_SSE3(seq_params->intra_scaling_list_8x8[0], default_intra_scaling_8x8, 64*sizeof(short));
		}


		if (decoder_cfg.NextBit()) {
			if (next_scale = 8 + decoder_cfg.GolombSigned()) {
				for (unsigned int j(0);j<63;j++) {
					next_scale += 256;
					if (next_scale &= 0x00FF) {
						seq_params->inter_scaling_list_8x8[0][j] = next_scale;
						next_scale += decoder_cfg.GolombSigned();
					} else {
					// fill out
						next_scale = seq_params->inter_scaling_list_8x8[0][j-1];
						for (;j<63;j++) seq_params->inter_scaling_list_8x8[0][j] = next_scale;										
					}
				}
				seq_params->inter_scaling_list_8x8[0][63] = next_scale;
			} else {
				// use default
				System::MemoryCopy_SSE3(seq_params->inter_scaling_list_8x8[0], default_inter_scaling_8x8, 64*sizeof(short));
			}
		} else {
				// fall-back A
			System::MemoryCopy_SSE3(seq_params->inter_scaling_list_8x8[0], default_inter_scaling_8x8, 64*sizeof(short));
		}


		if (seq_params->chroma_format_idc == 3) {
			for (unsigned int i(1);i<3;i++) {
				if (decoder_cfg.NextBit()) {
					if (next_scale = 8 + decoder_cfg.GolombSigned()) {
						for (unsigned int j(0);j<63;j++) {
							next_scale += 256;
							if (next_scale &= 0x00FF) {
								seq_params->intra_scaling_list_8x8[i][j] = next_scale;
								next_scale += decoder_cfg.GolombSigned();
							} else {
							// fill out
								next_scale = seq_params->intra_scaling_list_8x8[i][j-1];
								for (;j<63;j++) seq_params->intra_scaling_list_8x8[i][j] = next_scale;										
							}
						}
						seq_params->intra_scaling_list_8x8[i][63] = next_scale;
					} else {
						// use default
						System::MemoryCopy_SSE3(seq_params->intra_scaling_list_8x8[i], default_intra_scaling_8x8, 64*sizeof(short));
					}
				} else {
						// fall-back A
					System::MemoryCopy_SSE3(seq_params->intra_scaling_list_8x8[i], seq_params->intra_scaling_list_8x8[i-1], 64*sizeof(short));
				}


				if (decoder_cfg.NextBit()) {
					if (next_scale = 8 + decoder_cfg.GolombSigned()) {
						for (unsigned int j(0);j<63;j++) {
							next_scale += 256;
							if (next_scale &= 0x00FF) {
								seq_params->inter_scaling_list_8x8[i][j] = next_scale;
								next_scale += decoder_cfg.GolombSigned();
							} else {
							// fill out
								next_scale = seq_params->inter_scaling_list_8x8[i][j-1];
								for (;j<63;j++) seq_params->inter_scaling_list_8x8[i][j] = next_scale;										
							}
						}
						seq_params->inter_scaling_list_8x8[i][63] = next_scale;
					} else {
						// use default
						System::MemoryCopy_SSE3(seq_params->inter_scaling_list_8x8[i], default_inter_scaling_8x8, 64*sizeof(short));
					}
				} else {
					// fall-back A
					System::MemoryCopy_SSE3(seq_params->inter_scaling_list_8x8[i], seq_params->inter_scaling_list_8x8[i-1], 64*sizeof(short));
				}
			}
		}
	}

	return 0;
}


UI_64 Video::H264::GetPicScaling(PicturePSet * pic_params, unsigned char seq_smp, unsigned char cati) {
	short next_scale(0);

	if (decoder_cfg.NextBit()) {
		for (unsigned int i(0);i<3;i++) { // intra
			if (decoder_cfg.NextBit()) { // scaling list present
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<15;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {
							pic_params->intra_scaling_list_4x4[0][i][0][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
							// fill out
							next_scale = pic_params->intra_scaling_list_4x4[0][i][0][j-1];
							for (;j<15;j++) pic_params->intra_scaling_list_4x4[0][i][0][j] = next_scale;
						}
					}

					pic_params->intra_scaling_list_4x4[0][i][0][15] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(pic_params->intra_scaling_list_4x4[0][i][0], default_intra_scaling_4x4, 16*sizeof(short));
				}
			} else {
				if (i) {
					System::MemoryCopy_SSE3(pic_params->intra_scaling_list_4x4[0][i][0], pic_params->intra_scaling_list_4x4[0][i-1][0], 16*sizeof(short));
				} else {
					if (seq_smp == 0) { // fall-back A
						System::MemoryCopy_SSE3(pic_params->intra_scaling_list_4x4[0][i][0], default_intra_scaling_4x4, 16*sizeof(short));
					}
				}
			}
		}

		for (unsigned int i(0);i<3;i++) { // inter
			if (decoder_cfg.NextBit()) { // scaling list present
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<15;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {
							pic_params->inter_scaling_list_4x4[0][i][0][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
							// fill out
							next_scale = pic_params->inter_scaling_list_4x4[0][i][0][j-1];
							for (;j<15;j++) pic_params->inter_scaling_list_4x4[0][i][0][j] = next_scale;
						}
					}
					pic_params->inter_scaling_list_4x4[0][i][0][15] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(pic_params->inter_scaling_list_4x4[0][i][0], default_inter_scaling_4x4, 16*sizeof(short));
				}
			} else {
				if (i) {
					System::MemoryCopy_SSE3(pic_params->inter_scaling_list_4x4[0][i][0], pic_params->inter_scaling_list_4x4[0][i-1][0], 16*sizeof(short));
				} else {
					if (seq_smp == 0) {// fall-back A
						System::MemoryCopy_SSE3(pic_params->inter_scaling_list_4x4[0][i][0], default_inter_scaling_4x4, 16*sizeof(short));
					}
				}
			}
		}


		if (pic_params->trasnform_8x8_mode_flag) {

			if (decoder_cfg.NextBit()) {
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<63;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {
							pic_params->intra_scaling_list_8x8[0][0][0][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
						// fill out
							next_scale = pic_params->intra_scaling_list_8x8[0][0][0][j-1];
							for (;j<63;j++) pic_params->intra_scaling_list_8x8[0][0][0][j] = next_scale;										
						}
					}
					pic_params->intra_scaling_list_8x8[0][0][0][63] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(pic_params->intra_scaling_list_8x8[0][0][0], default_intra_scaling_8x8, 64*sizeof(short));
				}
			} else {
				if (seq_smp == 0) { // fall-back A
					System::MemoryCopy_SSE3(pic_params->intra_scaling_list_8x8[0][0][0], default_intra_scaling_8x8, 64*sizeof(short));
				}
			}


			if (decoder_cfg.NextBit()) {
				if (next_scale = 8 + decoder_cfg.GolombSigned()) {
					for (unsigned int j(0);j<63;j++) {
						next_scale += 256;
						if (next_scale &= 0x00FF) {
							pic_params->inter_scaling_list_8x8[0][0][0][j] = next_scale;
							next_scale += decoder_cfg.GolombSigned();
						} else {
						// fill out
							next_scale = pic_params->inter_scaling_list_8x8[0][0][0][j-1];
							for (;j<63;j++) pic_params->inter_scaling_list_8x8[0][0][0][j] = next_scale;										
						}
					}
					pic_params->inter_scaling_list_8x8[0][0][0][63] = next_scale;
				} else {
					// use default
					System::MemoryCopy_SSE3(pic_params->inter_scaling_list_8x8[0][0][0], default_inter_scaling_8x8, 64*sizeof(short));
				}
			} else {
				if (seq_smp == 0) { // fall-back A
					System::MemoryCopy_SSE3(pic_params->inter_scaling_list_8x8[0][0][0], default_inter_scaling_8x8, 64*sizeof(short));
				}
			}


			if (cati == 3) {
				for (unsigned int i(1);i<3;i++) {
					if (decoder_cfg.NextBit()) {
						if (next_scale = 8 + decoder_cfg.GolombSigned()) {
							for (unsigned int j(0);j<63;j++) {
								next_scale += 256;
								if (next_scale &= 0x00FF) {
									pic_params->intra_scaling_list_8x8[0][i][0][j] = next_scale;
									next_scale += decoder_cfg.GolombSigned();
								} else {
						// fill out
									next_scale = pic_params->intra_scaling_list_8x8[0][i][0][j-1];
									for (;j<63;j++) pic_params->intra_scaling_list_8x8[0][i][0][j] = next_scale;										
								}
							}
							pic_params->intra_scaling_list_8x8[0][i][0][63] = next_scale;
						} else {
							// use default
							System::MemoryCopy_SSE3(pic_params->intra_scaling_list_8x8[0][i][0], default_intra_scaling_8x8, 64*sizeof(short));
						}
					} else {
						System::MemoryCopy_SSE3(pic_params->intra_scaling_list_8x8[0][i][0], pic_params->intra_scaling_list_8x8[0][i-1][0], 64*sizeof(short));
					}


					if (decoder_cfg.NextBit()) {
						if (next_scale = 8 + decoder_cfg.GolombSigned()) {
							for (unsigned int j(0);j<63;j++) {
								next_scale += 256;
								if (next_scale &= 0x00FF) {
									pic_params->inter_scaling_list_8x8[0][i][0][j] = next_scale;
									next_scale += decoder_cfg.GolombSigned();
								} else {
								// fill out
									next_scale = pic_params->inter_scaling_list_8x8[0][i][0][j-1];
									for (;j<63;j++) pic_params->inter_scaling_list_8x8[0][i][0][j] = next_scale;										
								}
							}
							pic_params->inter_scaling_list_8x8[0][i][0][63] = next_scale;
						} else {
							// use default
							System::MemoryCopy_SSE3(pic_params->inter_scaling_list_8x8[0][i][0], default_inter_scaling_8x8, 64*sizeof(short));
						}
					} else {
						System::MemoryCopy_SSE3(pic_params->inter_scaling_list_8x8[0][i][0], pic_params->inter_scaling_list_8x8[0][i-1][0], 64*sizeof(short));
					}
				}
			}
		}
	}

	return 0;
}


UI_64 Video::H264::GetVUIParameters() {
//	UI_64 result(0);

	if (decoder_cfg.NextBit()) { // aspect ratio
		vui_params.aspect_ratio_idc = H264_DS_GetNumber(&decoder_cfg, 8);

		if (vui_params.aspect_ratio_idc == 0xFF) {
			vui_params.sar_width = H264_DS_GetNumber(&decoder_cfg, 16);

			vui_params.sar_height = H264_DS_GetNumber(&decoder_cfg, 16);
		}
	}

	if (decoder_cfg.NextBit()) { // overscan info present
		vui_params.oversacn_appropriate_flag = decoder_cfg.NextBit();
	}


	if (decoder_cfg.NextBit()) { // video signal info present
		vui_params.video_format = H264_DS_GetNumber(&decoder_cfg, 3);

		vui_params.video_full_range_flag = decoder_cfg.NextBit();


		if (decoder_cfg.NextBit()) { // colour desc
			vui_params.colour_primaries = H264_DS_GetNumber(&decoder_cfg, 8);

			vui_params.transfer_characteristics = H264_DS_GetNumber(&decoder_cfg, 8);

			vui_params.matrix_coefficients = H264_DS_GetNumber(&decoder_cfg, 8);
			if (vui_params.matrix_coefficients > 10) vui_params.matrix_coefficients = 1;
		}
	}

	if (decoder_cfg.NextBit()) { // chroma loc info present
		vui_params.chroma_loc_type_top = H264_DS_GolombUnsigned(&decoder_cfg);
		vui_params.chroma_loc_type_bottom = H264_DS_GolombUnsigned(&decoder_cfg);
	}


	if (decoder_cfg.NextBit()) { // timing info info
		vui_params.num_units_tick = H264_DS_GetNumber(&decoder_cfg, 32);

		vui_params.time_scale = H264_DS_GetNumber(&decoder_cfg, 32);

		vui_params.fixed_frame_rate_flag = decoder_cfg.NextBit();

	}

	vui_params.nal_hrd_present_flag = decoder_cfg.NextBit();

	if (vui_params.nal_hrd_present_flag) { // nal hrd
		GetHRDParameters(nal_hrd);

	}

	vui_params.vcl_hrd_present_flag = decoder_cfg.NextBit();

	if (vui_params.vcl_hrd_present_flag) { // vcl hrd
		GetHRDParameters(vcl_hrd);

	}


	if (vui_params.nal_hrd_present_flag || vui_params.vcl_hrd_present_flag) {
		vui_params.low_delay_hrd_flag = decoder_cfg.NextBit();
	}
	
	vui_params.pic_struct_present_flag = decoder_cfg.NextBit();

	if (decoder_cfg.NextBit()) { // bitstream restriction
		vui_params.motion_vectors_over_pic_flag = decoder_cfg.NextBit();

		vui_params.max_bytes_per_pic_denom = H264_DS_GolombUnsigned(&decoder_cfg);
		vui_params.max_bits_per_mb_denom = H264_DS_GolombUnsigned(&decoder_cfg);

		vui_params.max_mv_length_bits[0] = H264_DS_GolombUnsigned(&decoder_cfg); // horizonatal
		vui_params.max_mv_length_bits[1] = H264_DS_GolombUnsigned(&decoder_cfg); // vertical

		vui_params.max_num_reorder_frames = H264_DS_GolombUnsigned(&decoder_cfg);
		vui_params.max_decoded_frame_buffering = H264_DS_GolombUnsigned(&decoder_cfg);
	}

	return 0;
}

UI_64 Video::H264::GetHRDParameters(HRD & hrd) {
//	UI_64 result(0);

	hrd.cpb_cnt = H264_DS_GolombUnsigned(&decoder_cfg);
	hrd.bit_rate_scale = H264_DS_GetNumber(&decoder_cfg, 4);
	hrd.cpb_size_scale = H264_DS_GetNumber(&decoder_cfg, 4);

	for (unsigned char i(0);i<=hrd.cpb_cnt;i++) {
		hrd.cpb_cfg[i].bit_rate = H264_DS_GolombUnsigned(&decoder_cfg);
		hrd.cpb_cfg[i].cpb_size = H264_DS_GolombUnsigned(&decoder_cfg);

		hrd.cpb_cfg[i].cbr_flag = decoder_cfg.NextBit();
	}

	hrd.initial_cpb_removal_delay_length = H264_DS_GetNumber(&decoder_cfg, 5);
	hrd.cpb_removal_delay_length = H264_DS_GetNumber(&decoder_cfg, 5);
	hrd.dpb_output_delay_length = H264_DS_GetNumber(&decoder_cfg, 5);
	hrd.time_offset_length = H264_DS_GetNumber(&decoder_cfg, 5);

	return 0;
}



UI_64 Video::H264::GetSliceHeader(I_64 & time_mark) {
	UI_64 result(0);
	unsigned int POCMsb(0), absFrameNum(0), cy_count(0), pcy_count(0), zombie_val(0), mask_val(0), rf_ptr[4];

	
	current_slice.Clear();

	current_slice.id_rec.ref_idc = NAL.unit.ref_idc;

	current_slice.first_mb = H264_DS_GolombUnsigned(&decoder_cfg);
	current_slice._type = H264_DS_GolombUnsigned(&decoder_cfg);
	current_slice.id_rec.pic_set_id = H264_DS_GolombUnsigned(&decoder_cfg);
	current_slice.id_rec.frame_num = -1;

	decoder_cfg.clip_vals[0][0] = 1;
	decoder_cfg.clip_vals[0][1] = -5;
	decoder_cfg.clip_vals[0][2] = 20;
	decoder_cfg.clip_vals[0][3] = 20;
	decoder_cfg.clip_vals[0][4] = -5;
	decoder_cfg.clip_vals[0][5] = 1;
	decoder_cfg.clip_vals[0][6] = 0;
	decoder_cfg.clip_vals[0][7] = 0;




	for (unsigned int i(0);i<8;i++) decoder_cfg.clip_vals[1][i] = 255;
	for (unsigned int i(0);i<8;i++) decoder_cfg.clip_vals[2][i] = 255;

	if (decoder_cfg.jump_flag) {		
		if ((current_slice._type == 2) || (current_slice._type == 7)) {
			decoder_cfg.jump_flag = 0;
			
			if (decode_skip_count >= 0) { // -1
				decoder_cfg.current_frame_ptr = 0;
			
				frame_list_count = 0;
				decode_skip_count = 0;
							
				for (unsigned int i(0);i<MAX_REF_IDX;i++) {
					pic_list[i].Destroy();
				}

				for (unsigned int i(0);i<MAX_REF_IDX;i++) {
					time_table[i][0] = -1; (UI_64 &)time_table[i][0] >>= 1;
					time_table[i][1] = -1;
				}
			} else {
				zombie_val = 1;
			}
			
		} else {
			return 0;
		}
	}

	if ((decode_skip_count > 0) && (current_slice.id_rec.ref_idc == 0)) {
		decode_skip_count--;
		return 0;							
	}


	if (current_slice.id_rec.pic_set_id < pp_count) {
		if ((current_slice.seq_set = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) && (current_slice.pic_set = reinterpret_cast<PicturePSet *>(picture_params.Acquire()))) {
			current_slice.pic_set = reinterpret_cast<PicturePSet *>(reinterpret_cast<char *>(current_slice.pic_set) + current_slice.id_rec.pic_set_id*pic_psize);
			current_slice.seq_set = reinterpret_cast<SequencePSet *>(reinterpret_cast<char *>(current_slice.seq_set) + current_slice.pic_set->sp_set_id*seq_psize);

			if (current_slice.pic_set->valid && current_slice.seq_set->valid) {








// init stuff

				if (current_slice.seq_set->separate_planes_flag) {
					current_slice.colour_plane_id = H264_DS_GetNumber(&decoder_cfg, 2);		
				}

				current_slice.id_rec.frame_num = H264_DS_GetNumber(&decoder_cfg, current_slice.seq_set->max_frame_num_bits+4);		

				if (current_slice.seq_set->frame_mbs_only_flag == 0) {
					current_slice.id_rec.field_pic_flag = decoder_cfg.NextBit();

					if (current_slice.id_rec.field_pic_flag) {
						current_slice.id_rec.bottom_field_flag = decoder_cfg.NextBit();
//						decoder_cfg.flag_set |= FLAG_FIELD;
					} else {
						if (current_slice.seq_set->mb_adaptive_frame_field_flag) decoder_cfg.flag_set |= FLAG_AFRO_FRAME;
						else decoder_cfg.flag_set |= FLAG_FIELD_PAIR;
					}
				} else {
	//				decoder_cfg.flag_set |= FLAG_FRAME;
				}
		
				current_slice.max_pic_num = (1 << (current_slice.seq_set->max_frame_num_bits+4));
		
				if (current_slice.id_rec.field_pic_flag) {
					current_slice.max_pic_num <<= 1;
					current_slice.id_rec.frame_num <<= 1;
					current_slice.id_rec.frame_num |= current_slice.id_rec.bottom_field_flag;
				}

				if (current_slice.id_rec.frame_num < pic_list[decoder_cfg.current_frame_ptr].id_rec.frame_num) decoder_cfg.frameNumOffset += current_slice.max_pic_num;

				current_slice.max_pic_num--;




				decoder_cfg.flag_set &= (~FLAG_SECOND_FIELD);

				if ((!current_slice.seq_set->mb_adaptive_frame_field_flag) && (current_slice.id_rec.field_pic_flag)) {
					if (current_slice.id_rec.bottom_field_flag) {
						decoder_cfg.flag_set |= FLAG_SECOND_FIELD;					
					}
				}

				if ((pic_list[decoder_cfg.current_frame_ptr].id_rec.frame_num == current_slice.id_rec.frame_num) && (current_slice.first_mb == 0) && (current_slice.seq_set->frame_mbs_only_flag == 0)) {
					decoder_cfg.flag_set |= FLAG_SECOND_FIELD;
				}

// some numbers
				decoder_cfg.slice_width = current_slice.seq_set->pic_width_mbs;
				decoder_cfg.pline_offset = (current_slice.seq_set->pic_width_mbs+1)*4*256;

				vui_params.c_width = current_slice.seq_set->pic_width_mbs;
				vui_params.c_height = current_slice.seq_set->pic_height_mapunits << current_slice.id_rec.field_pic_flag; //<<((decoder_cfg.flag_set & FLAG_AFRO_FRAME)));









				switch (decoder_cfg.reset_op & 3) {
					case 1: // IDR
						current_slice.id_rec.idr_flag = 1;
						current_slice.id_rec.idr_pic_id = H264_DS_GolombUnsigned(&decoder_cfg);

						decoder_cfg.prevPOCMsb = decoder_cfg.prevPOCLsb = 0;
						decoder_cfg.frameNumOffset = 0;
					break;
					case 2: // memop 5
						switch (current_slice.seq_set->pic_order_cnt_type) {
							case 0:
								pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[0] -= pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];
								pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[1] -= pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];

								decoder_cfg.prevPOCMsb = 0;
								if (current_slice.id_rec.bottom_field_flag) {							
									decoder_cfg.prevPOCLsb = 0;
								} else {
									decoder_cfg.prevPOCLsb = pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[0];
								}

							break;
							case 1: case 2:
								decoder_cfg.frameNumOffset = 0;
							break;
						}

					break;

				}
				

// set order counts
				switch (current_slice.seq_set->pic_order_cnt_type) {
					case 0:
						POCMsb = decoder_cfg.prevPOCMsb;

						current_slice.id_rec.field_ocount[0] = H264_DS_GetNumber(&decoder_cfg, current_slice.seq_set->max_pic_order_cnt_bits+4);
												
						if ((current_slice.id_rec.field_ocount[0] < decoder_cfg.prevPOCLsb) && (decoder_cfg.prevPOCLsb >= (current_slice.id_rec.field_ocount[0] + (current_slice.seq_set->max_pic_order_count_lsb>>1)))) {
							POCMsb += current_slice.seq_set->max_pic_order_count_lsb;
						} else {
							if ((current_slice.id_rec.field_ocount[0] > decoder_cfg.prevPOCLsb) && (current_slice.id_rec.field_ocount[0] > (decoder_cfg.prevPOCLsb + (current_slice.seq_set->max_pic_order_count_lsb>>1)))) {
								POCMsb -= current_slice.seq_set->max_pic_order_count_lsb;
							}
						}

						decoder_cfg.prevPOCLsb = current_slice.id_rec.field_ocount[0];
						decoder_cfg.prevPOCMsb = POCMsb;

						current_slice.id_rec.field_ocount[0] += POCMsb;
						current_slice.id_rec.field_ocount[1] = current_slice.id_rec.field_ocount[0];

						if (current_slice.pic_set->bottom_field_pic_order_flag && (current_slice.id_rec.field_pic_flag == 0)) {
							current_slice.id_rec.field_ocount[1] += decoder_cfg.GolombSigned(); // current_slice.delta_pic_order_cnt_bottom
						}


						
						

					break;
					case 1:
						if (current_slice.seq_set->num_ref_frames_in_pic_order) {
							if (current_slice.id_rec.field_pic_flag) absFrameNum = decoder_cfg.frameNumOffset + (current_slice.id_rec.frame_num>>1);
							else absFrameNum = decoder_cfg.frameNumOffset + current_slice.id_rec.frame_num;

							if ((absFrameNum) && (current_slice.id_rec.ref_idc == 0)) absFrameNum--;

							if (absFrameNum) {
								absFrameNum--;
								cy_count = absFrameNum/current_slice.seq_set->num_ref_frames_in_pic_order;
								pcy_count = absFrameNum%current_slice.seq_set->num_ref_frames_in_pic_order;

								absFrameNum = cy_count*current_slice.seq_set->expectedDeltaPP;
								for (unsigned int i(0);i<pcy_count;i++) absFrameNum += current_slice.seq_set->ref_frame_offset[i];
							}							
						}

						if (current_slice.id_rec.ref_idc == 0) absFrameNum += current_slice.seq_set->non_ref_pic_offset;


						current_slice.id_rec.field_ocount[0] = absFrameNum;
						current_slice.id_rec.field_ocount[1] = current_slice.seq_set->top2bottom_field_offset;

						if (current_slice.seq_set->delta_pic_order_zero_flag == 0) {
							current_slice.id_rec.field_ocount[0] += decoder_cfg.GolombSigned(); // current_slice.delta_pic_order_cnt[0]
					
							if (current_slice.pic_set->bottom_field_pic_order_flag && (current_slice.id_rec.field_pic_flag == 0)) {
								current_slice.id_rec.field_ocount[1] += decoder_cfg.GolombSigned(); // current_slice.delta_pic_order_cnt[1]
							}
						}

						current_slice.id_rec.field_ocount[1] += current_slice.id_rec.field_ocount[0];

					break;
					case 2:
						if (decoder_cfg.reset_op & 1) {
							current_slice.id_rec.field_ocount[0] = current_slice.id_rec.field_ocount[1] = 0;
						} else {
							current_slice.id_rec.field_ocount[0] = current_slice.id_rec.frame_num + decoder_cfg.frameNumOffset;
							current_slice.id_rec.field_ocount[0] <<= 1;
					
							if (current_slice.id_rec.ref_idc == 0) {
								current_slice.id_rec.field_ocount[0]--;
							}

							current_slice.id_rec.field_ocount[1] = current_slice.id_rec.field_ocount[0];
						}

					break;
				}













				



// initialize new picture
				if (pic_list[decoder_cfg.current_frame_ptr].id_rec.NEQ(current_slice.id_rec) || (zombie_val)) {

					
					

					if (decode_skip_count >= 0) ApplyMarking(time_mark); // -1
					

					if (pic_list[decoder_cfg.current_frame_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC)) {
			// deblock
						if (decode_skip_count >= 0) // -1
						if (short * f_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {

							switch (decoder_cfg.chroma_array_type) {
								case 1:
									H264_Deblock_1(f_ptr);
								break;
								case 2:
									H264_Deblock_2(f_ptr);
								break;
								case 0:
									H264_Deblock_0(f_ptr);
								break;
							}

							pic_list[decoder_cfg.current_frame_ptr].buffer.Release();
						}
						
						decoder_cfg.current_frame_ptr++;
						decoder_cfg.current_frame_ptr &= (MAX_REF_IDX - 1);												
					}

					
					if (current_slice.id_rec.ref_idc) pic_list[decoder_cfg.current_frame_ptr]._reserved = FLAG_ACTIVE_REF;
					else pic_list[decoder_cfg.current_frame_ptr]._reserved = FLAG_BUFFER_PIC;
					

					pic_list[decoder_cfg.current_frame_ptr]._reserved |= (decoder_cfg.flag_set & FLAG_SECOND_FIELD);

					if ((current_slice.seq_set->mb_adaptive_frame_field_flag)) pic_list[decoder_cfg.current_frame_ptr]._reserved |= FLAG_AFRO_REF;
					else
						if (current_slice.id_rec.field_pic_flag) {	
							pic_list[decoder_cfg.current_frame_ptr]._reserved |= FLAG_FIELD_REF;

							if (current_slice.id_rec.bottom_field_flag) {
								time_mark++;
								pic_list[decoder_cfg.current_frame_ptr]._reserved |= FLAG_SECOND_FIELD;
							}
						} else {
							if (current_slice.seq_set->frame_mbs_only_flag) pic_list[decoder_cfg.current_frame_ptr]._reserved |= FLAG_FRAME_REF;
							else pic_list[decoder_cfg.current_frame_ptr]._reserved |= (FLAG_PAIR_REF | FLAG_FIELD_REF);
						}

					




					pic_list[decoder_cfg.current_frame_ptr].id_rec = current_slice.id_rec;
					
					if (time_mark == 0) time_mark = 1;
					pic_list[decoder_cfg.current_frame_ptr].time_stamp = time_mark;
					
					vui_params.second_field_offset = decoder_cfg.pline_offset*((vui_params.c_height+1)>>1);
					pic_list[decoder_cfg.current_frame_ptr].frame_size = (vui_params.second_field_offset<<2) + (decoder_cfg.pline_offset<<1);

					if (pic_list[decoder_cfg.current_frame_ptr].frame_size) {
						if (pic_list[decoder_cfg.current_frame_ptr].buffer) {
							result = pic_list[decoder_cfg.current_frame_ptr].buffer.Resize(pic_list[decoder_cfg.current_frame_ptr].frame_size);
						} else {
							result = pic_list[decoder_cfg.current_frame_ptr].buffer.New(frame_buf_tid, pic_list[decoder_cfg.current_frame_ptr].frame_size, SFSco::large_mem_mgr);
						}
					} else {
						result = -1; // error
					}
						
					if (result != -1) {
						for (unsigned int i(0);i<MAX_REF_IDX;i++) {
							if (time_table[i][0] > time_mark) {
								for (unsigned int j(MAX_REF_IDX-1);j>i;j--) {
									time_table[j][0] = time_table[j-1][0];
									time_table[j][1] = time_table[j-1][1];
								}

								time_table[i][0] = time_mark;
								time_table[i][1] = decoder_cfg.current_frame_ptr;

								break;
							}
						}

						frame_list_count++;

					}

					if (pic_list[decoder_cfg.current_frame_ptr]._reserved & (FLAG_FIELD_REF | FLAG_PAIR_REF)) {
						// get closest availble field of opposite polarity by order count
						cy_count = -1;
						pcy_count = (decoder_cfg.flag_set & FLAG_SECOND_FIELD) ^ FLAG_SECOND_FIELD;
						POCMsb = -1;

						for (unsigned int oi(0);oi<MAX_REF_IDX;oi++) {
							if (pic_list[oi]._reserved) {
								if ((pic_list[oi]._reserved & (FLAG_SECOND_FIELD | FLAG_REMOVED_REF)) == pcy_count) {
									if ((pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pcy_count ^ FLAG_SECOND_FIELD] - pic_list[oi].id_rec.field_ocount[pcy_count]) < POCMsb) {
										POCMsb = pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pcy_count ^ FLAG_SECOND_FIELD] - pic_list[oi].id_rec.field_ocount[pcy_count];
										cy_count = oi;
									}
								}
							}
						}


						if (cy_count != -1) {
							if (short * cf_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {
								if (short * pf_ptr = reinterpret_cast<short *>(pic_list[cy_count].buffer.Acquire())) {
								
									if (pcy_count) {
										cf_ptr += vui_params.second_field_offset;
										pf_ptr += vui_params.second_field_offset;
									}

									System::MemoryCopy_SSE3(cf_ptr, pf_ptr, vui_params.second_field_offset*sizeof(short));

									pic_list[cy_count].buffer.Release();
								}
								pic_list[decoder_cfg.current_frame_ptr].buffer.Release();
							}
						} else {
							if (short * cf_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {
								if (pcy_count) {
									cf_ptr += vui_params.second_field_offset;
								}

					//			System::MemoryFill_SSE3(cf_ptr, vui_params.second_field_offset*sizeof(short), 0, 0);

								pic_list[decoder_cfg.current_frame_ptr].buffer.Release();
							}
						}
					} else {
						vui_params.second_field_offset = 0;
					}
					
					if (_src_desc) {						
						_src_desc->c_width = (vui_params.c_width<<4);
						_src_desc->width = _src_desc->c_width - current_slice.seq_set->frame_crop_right;
					
						_src_desc->c_height = (vui_params.c_height<<4);
						_src_desc->height = _src_desc->c_height - current_slice.seq_set->frame_crop_bottom;

						_src_desc->precision = current_slice.seq_set->cmp_depth[3];


					}


					if (result != -1) {

						result = RESULT_HEADER_COMPLETE;
					
						absFrameNum = 0;
						cy_count = -1; pcy_count = -1;
						
						if (decode_skip_count >= 0) { // -1
							for (unsigned int pi(0);pi<MAX_REF_IDX;pi++) {
								if ((time_table[pi][0]) && (time_table[pi][1] != -1)) {
									pcy_count = pi;
									break;
								}
							}

							if (pcy_count != -1) {
								for (unsigned int i(0);i<pcy_count;i++) {
									pic_list[time_table[i][1]].Destroy();

								}
									
								frame_list_count -= pcy_count;

								for (unsigned int j(pcy_count);j<MAX_REF_IDX;j++) {
									time_table[j - pcy_count][0] = time_table[j][0]; 
									time_table[j - pcy_count][1] = time_table[j][1];
								}
							}
	
							if ((time_table[0][0]) && (time_table[0][1] != -1)) {
								cy_count = 0;
							}
						} else {
							cy_count = 0;
						}

						
						if (cy_count != -1) {
							if ((frame_list_count >= min_buffer_frames) || (decode_skip_count < 0)) { // -1

								time_mark = time_table[0][0];
								time_table[0][0] = 0;
								out_pin = time_table[0][1];

								pic_list[out_pin]._reserved |= FLAG_OUT_REF;
									
								if (_src_desc) InterlockedIncrement(&_src_desc->source_frame);
								result = RESULT_BUFFER_SET; // RESULT_DECODE_COMPLETE;

							} else {
								// keep on loading							
							}
						} else {
							// keep on loading
						}
					} else {
						result = 0;
					}
				} else {
					time_mark = pic_list[out_pin].time_stamp;
					if ((frame_list_count >= min_buffer_frames) || (decode_skip_count < 0)) { // -1
						result = RESULT_BUFFER_SET;
					} else {
						result = RESULT_HEADER_COMPLETE;
					}

				}






























	
				if (current_slice.pic_set->redundant_pic_cnt_flag) {
					current_slice.redundant_pic_cnt = H264_DS_GolombUnsigned(&decoder_cfg);
				}


// initialize picture lists
				switch (current_slice._type) {
					case 1: case 6: // B
						current_slice.direct_spatial_mv_pred_flag = decoder_cfg.NextBit();

						if (decoder_cfg.NextBit()) { // override flag
							current_slice.num_ref_idx_active[0] = H264_DS_GolombUnsigned(&decoder_cfg);
							current_slice.num_ref_idx_active[1] = H264_DS_GolombUnsigned(&decoder_cfg);
						} else {
							current_slice.num_ref_idx_active[0] = current_slice.pic_set->num_ref_idx[0];
							current_slice.num_ref_idx_active[1] = current_slice.pic_set->num_ref_idx[1];
						}

						InitializeRefPicListB();
						

					break;
					case 0: case 5: // P
					case 3: case 8: // SP

						if (decoder_cfg.NextBit()) { // override flag
							current_slice.num_ref_idx_active[0] = H264_DS_GolombUnsigned(&decoder_cfg);
						} else {
							current_slice.num_ref_idx_active[0] = current_slice.pic_set->num_ref_idx[0];
						}

						InitializeRefPicListP();
						

					break;
				}

// field list extra init
				if (current_slice.id_rec.field_pic_flag) {
					if (decoder_cfg.list_count[0] > 1) {
												
						pcy_count = decoder_cfg.flag_set & FLAG_SECOND_FIELD;
						cy_count = decoder_cfg.list_count[0];

						rf_ptr[0] = rf_ptr[1] = rf_ptr[2] = rf_ptr[3] = -1;

						for (unsigned int i(0);i<decoder_cfg.list_count[0];i++) {
							decoder_cfg.ref_pic_list[0][i + 32] = decoder_cfg.ref_pic_list[0][i];

							mask_val = pic_list[decoder_cfg.ref_pic_list[0][i + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_REMOVED_REF);
							if (mask_val & FLAG_LONG_REF) {
								if ((mask_val == (FLAG_LONG_REF | FLAG_ACTIVE_REF)) || (mask_val == (FLAG_LONG_REF | FLAG_BUFFER_PIC))) {
										
									if ((pic_list[decoder_cfg.ref_pic_list[0][i + 32]]._reserved & FLAG_SECOND_FIELD) == pcy_count) {
										if (rf_ptr[2] == -1) rf_ptr[2] = i;
									} else {
										if (rf_ptr[3] == -1) rf_ptr[3] = i;
									}
								
									cy_count--;
								}
							} else {
								if ((mask_val == FLAG_ACTIVE_REF) || (mask_val == FLAG_BUFFER_PIC)) {
									if ((pic_list[decoder_cfg.ref_pic_list[0][i + 32]]._reserved & FLAG_SECOND_FIELD) == pcy_count) {
										if (rf_ptr[0] == -1) rf_ptr[0] = i;
									} else {
										if (rf_ptr[1] == -1) rf_ptr[1] = i;
									}
								}
							}

							decoder_cfg.ref_pic_list[0][i] = 0;
						}



						for (absFrameNum = 0;absFrameNum < cy_count;) {
							if (rf_ptr[0] < decoder_cfg.list_count[0]) {
								decoder_cfg.ref_pic_list[0][absFrameNum++] = decoder_cfg.ref_pic_list[0][rf_ptr[0]+32];
								
								for (;++rf_ptr[0]<decoder_cfg.list_count[0];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[0][rf_ptr[0] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;

							if (rf_ptr[1] < decoder_cfg.list_count[0]) {
								decoder_cfg.ref_pic_list[0][absFrameNum++] = decoder_cfg.ref_pic_list[0][rf_ptr[1]+32];

								for (;++rf_ptr[1]<decoder_cfg.list_count[0];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[0][rf_ptr[1] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;
						}
					



						for (;absFrameNum < decoder_cfg.list_count[0];) {
							if (rf_ptr[2] < decoder_cfg.list_count[0]) {
								decoder_cfg.ref_pic_list[0][absFrameNum++] = decoder_cfg.ref_pic_list[0][rf_ptr[2]+32];
								
								for (;++rf_ptr[2]<decoder_cfg.list_count[0];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[0][rf_ptr[2] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | FLAG_LONG_REF | pcy_count)) || ((mask_val == (FLAG_BUFFER_PIC | FLAG_LONG_REF | pcy_count)))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;

							if (rf_ptr[3] < decoder_cfg.list_count[0]) {
								decoder_cfg.ref_pic_list[0][absFrameNum++] = decoder_cfg.ref_pic_list[0][rf_ptr[3]+32];

								for (;++rf_ptr[3]<decoder_cfg.list_count[0];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[0][rf_ptr[3] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | FLAG_LONG_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | FLAG_LONG_REF | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;
						}
					}



					if (decoder_cfg.list_count[1] > 1) {
												
						pcy_count = decoder_cfg.flag_set & FLAG_SECOND_FIELD;
						cy_count = decoder_cfg.list_count[1];

						rf_ptr[0] = rf_ptr[1] = rf_ptr[2] = rf_ptr[3] = -1;

						for (unsigned int i(0);i<decoder_cfg.list_count[1];i++) {
							decoder_cfg.ref_pic_list[1][i + 32] = decoder_cfg.ref_pic_list[1][i];

							mask_val = pic_list[decoder_cfg.ref_pic_list[1][i + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_REMOVED_REF);
							if (mask_val & FLAG_LONG_REF) {
								if ((mask_val == (FLAG_ACTIVE_REF | FLAG_LONG_REF)) || (mask_val == (FLAG_BUFFER_PIC | FLAG_LONG_REF))) {
																
									if ((pic_list[decoder_cfg.ref_pic_list[1][i + 32]]._reserved & FLAG_SECOND_FIELD) == pcy_count) {
										if (rf_ptr[2] == -1) rf_ptr[2] = i;
									} else {
										if (rf_ptr[3] == -1) rf_ptr[3] = i;
									}
								
									cy_count--;
								}
							} else {							
								if ((mask_val == FLAG_ACTIVE_REF) || (mask_val == FLAG_BUFFER_PIC)) {
									if ((pic_list[decoder_cfg.ref_pic_list[1][i + 32]]._reserved & FLAG_SECOND_FIELD) == pcy_count) {
										if (rf_ptr[0] == -1) rf_ptr[0] = i;
									} else {
										if (rf_ptr[1] == -1) rf_ptr[1] = i;
									}
								}
							}

							decoder_cfg.ref_pic_list[1][i] = 0;
						}



						for (absFrameNum = 0;absFrameNum < cy_count;) {
							if (rf_ptr[0] < decoder_cfg.list_count[1]) {
								decoder_cfg.ref_pic_list[1][absFrameNum++] = decoder_cfg.ref_pic_list[1][rf_ptr[0]+32];
								
								for (;++rf_ptr[0]<decoder_cfg.list_count[1];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[1][rf_ptr[0] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;

							if (rf_ptr[1] < decoder_cfg.list_count[1]) {
								decoder_cfg.ref_pic_list[1][absFrameNum++] = decoder_cfg.ref_pic_list[1][rf_ptr[1]+32];

								for (;++rf_ptr[1]<decoder_cfg.list_count[1];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[1][rf_ptr[1] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;
						}
					



						for (;absFrameNum < decoder_cfg.list_count[1];) {
							if (rf_ptr[2] < decoder_cfg.list_count[1]) {
								decoder_cfg.ref_pic_list[1][absFrameNum++] = decoder_cfg.ref_pic_list[1][rf_ptr[2]+32];
								
								for (;++rf_ptr[2]<decoder_cfg.list_count[1];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[1][rf_ptr[2] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | FLAG_LONG_REF | pcy_count)) && (mask_val == (FLAG_BUFFER_PIC | FLAG_LONG_REF | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;

							if (rf_ptr[3] < decoder_cfg.list_count[1]) {
								decoder_cfg.ref_pic_list[1][absFrameNum++] = decoder_cfg.ref_pic_list[1][rf_ptr[3]+32];

								for (;++rf_ptr[3]<decoder_cfg.list_count[1];) {
									mask_val = pic_list[decoder_cfg.ref_pic_list[1][rf_ptr[3] + 32]]._reserved & (FLAG_ACTIVE_REF | FLAG_BUFFER_PIC | FLAG_LONG_REF | FLAG_SECOND_FIELD | FLAG_REMOVED_REF);
									if ((mask_val == (FLAG_ACTIVE_REF | FLAG_LONG_REF | pcy_count)) || (mask_val == (FLAG_BUFFER_PIC | FLAG_LONG_REF | pcy_count))) {
										break;
									}
								}
							}

							pcy_count ^= FLAG_SECOND_FIELD;
						}
					}
				}



// modify lists

				if ((decoder_cfg.list_count[1] > 1) && (decoder_cfg.list_count[1] == decoder_cfg.list_count[0])) {
					cy_count = 1;

					for (unsigned int i(0);i<decoder_cfg.list_count[0];i++) {
						if (decoder_cfg.ref_pic_list[0][i] != decoder_cfg.ref_pic_list[1][i]) {
							cy_count = 0;
							break;
						}
					}

					if (cy_count) {
						cy_count = decoder_cfg.ref_pic_list[1][0];
						decoder_cfg.ref_pic_list[1][0] = decoder_cfg.ref_pic_list[1][1];
						decoder_cfg.ref_pic_list[1][1] = cy_count;
					}
				}


				
				if ((NAL.unit._type == 20) || (NAL.unit._type == 21)) {
					GetRefPicListMVC();
				} else {
					GetRefPicListModification();
				}





// initialize stuff				

				switch (current_slice._type) {
					case 1: case 6:
						current_slice.weight_type = current_slice.pic_set->weighted_bipred;
						if (current_slice.pic_set->weighted_bipred == 1) {
							GetWeightTable();
						}
					break;
					case 0: case 5:
					case 3: case 8:
						current_slice.weight_type = current_slice.pic_set->weighted_pred;
						if (current_slice.pic_set->weighted_pred) {
							GetWeightTable();
						}

					break;

				}
				



// mark pictures

				if (NAL.unit.ref_idc) {
					GetDecRefPicMarking();
					decoder_cfg.prev_ref_num = current_slice.id_rec.frame_num;
				}



				POCMsb = 3;

				switch (current_slice._type) {
					case 1: case 6: // B
					case 0: case 5: // P
					case 3: case 8: // SP
						if (current_slice.pic_set->entropy_coding_mode_flag) {
							POCMsb = H264_DS_GolombUnsigned(&decoder_cfg);
						}
					break;
				}

				current_slice.qpy = decoder_cfg.GolombSigned() + current_slice.pic_set->init_qp; // qp_delta
				if (current_slice.pic_set->entropy_coding_mode_flag) H264_CABAC_InitCtxVars(&cabac, Clip3(0, 51, current_slice.qpy), POCMsb & 3);

				switch (current_slice._type) {
					case 3: case 8: // SP
						current_slice.sp_for_switch_flag = decoder_cfg.NextBit();
							
					case 4: case 9: // SI
						current_slice.qsy = current_slice.pic_set->init_qs + decoder_cfg.GolombSigned();
					
					break;
				}

				if (current_slice.pic_set->deblocking_filter_control_flag) {
					current_slice.disable_deblocking_filter_idc = H264_DS_GolombUnsigned(&decoder_cfg);
						
					if (current_slice.disable_deblocking_filter_idc != 1) {
						current_slice.alpha_c0_offset = decoder_cfg.GolombSigned();
						current_slice.alpha_c0_offset <<= 1;
						current_slice.beta_offset = decoder_cfg.GolombSigned();
						current_slice.beta_offset <<= 1;
					}
				}

				if (current_slice.id_rec.field_pic_flag) { // disable for fields
					current_slice.disable_deblocking_filter_idc = -1;
				}

				if ((current_slice.pic_set->num_slice_groups>0) && (current_slice.pic_set->slice_group_map_type>2) && (current_slice.pic_set->slice_group_map_type<6)) {						
					current_slice.group_charge_cycle = H264_DS_GetNumber(&decoder_cfg, System::BitIndexHigh_32(current_slice.pic_set->size_map_units / current_slice.pic_set->slice_group_change_rate + 1) + 1);
				}

				






				current_slice.gray_val[0] = (0x0080 << current_slice.seq_set->cmp_depth[0]);
				current_slice.gray_val[1] = (0x0080 << current_slice.seq_set->cmp_depth[1]);

				decoder_cfg.clip_vals[1][0] = decoder_cfg.clip_vals[1][1] = decoder_cfg.clip_vals[1][2] = decoder_cfg.clip_vals[1][3] = decoder_cfg.clip_vals[1][4] = decoder_cfg.clip_vals[1][5] = decoder_cfg.clip_vals[1][6] = decoder_cfg.clip_vals[1][7] = (current_slice.gray_val[0]<<1) - 1;				
				

				vui_params.luma_d = current_slice.seq_set->cmp_depth[0];
				vui_params.chroma_d = current_slice.seq_set->cmp_depth[1];
			}
		}

	}

	if (vui_params.video_full_range_flag) {
		decoder_cfg.clip_vals[4][0] = decoder_cfg.clip_vals[4][1] = decoder_cfg.clip_vals[4][2] = decoder_cfg.clip_vals[4][3] = decoder_cfg.clip_vals[4][4] = decoder_cfg.clip_vals[4][5] = decoder_cfg.clip_vals[4][6] = decoder_cfg.clip_vals[4][7] = 0;
		decoder_cfg.clip_vals[5][0] = decoder_cfg.clip_vals[5][1] = decoder_cfg.clip_vals[5][2] = decoder_cfg.clip_vals[5][3] = decoder_cfg.clip_vals[5][4] = decoder_cfg.clip_vals[5][5] = decoder_cfg.clip_vals[5][6] = decoder_cfg.clip_vals[5][7] = (current_slice.gray_val[0] << 1) - 1;

		decoder_cfg.clip_vals[6][0] = decoder_cfg.clip_vals[6][1] = decoder_cfg.clip_vals[6][2] = decoder_cfg.clip_vals[6][3] = decoder_cfg.clip_vals[6][4] = decoder_cfg.clip_vals[6][5] = decoder_cfg.clip_vals[6][6] = decoder_cfg.clip_vals[6][7] = -current_slice.gray_val[1];
		decoder_cfg.clip_vals[7][0] = decoder_cfg.clip_vals[7][1] = decoder_cfg.clip_vals[7][2] = decoder_cfg.clip_vals[7][3] = decoder_cfg.clip_vals[7][4] = decoder_cfg.clip_vals[7][5] = decoder_cfg.clip_vals[7][6] = decoder_cfg.clip_vals[5][7] = current_slice.gray_val[1] - 1;

	} else {
		decoder_cfg.clip_vals[4][0] = decoder_cfg.clip_vals[4][1] = decoder_cfg.clip_vals[4][2] = decoder_cfg.clip_vals[4][3] = decoder_cfg.clip_vals[4][4] = decoder_cfg.clip_vals[4][5] = decoder_cfg.clip_vals[4][6] = decoder_cfg.clip_vals[4][7] = 16;
		decoder_cfg.clip_vals[5][0] = decoder_cfg.clip_vals[5][1] = decoder_cfg.clip_vals[5][2] = decoder_cfg.clip_vals[5][3] = decoder_cfg.clip_vals[5][4] = decoder_cfg.clip_vals[5][5] = decoder_cfg.clip_vals[5][6] = decoder_cfg.clip_vals[5][7] = 235;

		decoder_cfg.clip_vals[6][0] = decoder_cfg.clip_vals[6][1] = decoder_cfg.clip_vals[6][2] = decoder_cfg.clip_vals[6][3] = decoder_cfg.clip_vals[6][4] = decoder_cfg.clip_vals[6][5] = decoder_cfg.clip_vals[6][6] = decoder_cfg.clip_vals[6][7] = -112;
		decoder_cfg.clip_vals[7][0] = decoder_cfg.clip_vals[7][1] = decoder_cfg.clip_vals[7][2] = decoder_cfg.clip_vals[7][3] = decoder_cfg.clip_vals[7][4] = decoder_cfg.clip_vals[7][5] = decoder_cfg.clip_vals[7][6] = decoder_cfg.clip_vals[7][7] = 111;

	}


	decoder_cfg.reset_op &= 2;

	
	return result;
}

void Video::H264::SetRenderSkip(__int64 s_f) {
	decode_skip_count += s_f;
}

void Video::H264::InitializeRefPicListP() {
	unsigned int pic_ptr(0), ltp_num(0), lt_start(0), ltp_ptr(0);
	
	decoder_cfg.list_count[0] = decoder_cfg.list_count[1] = 0;
	System::MemoryFill(decoder_cfg.ref_pic_list, 128*sizeof(unsigned short), 0);



	for (pic_ptr = decoder_cfg.current_frame_ptr-1;(pic_ptr &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;pic_ptr--) {
		if ((pic_list[pic_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_LONG_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF) {
			decoder_cfg.ref_pic_list[0][decoder_cfg.list_count[0]++] = pic_ptr;
		}
	}

	lt_start = decoder_cfg.list_count[0];

	for (pic_ptr = decoder_cfg.current_frame_ptr-1;(pic_ptr &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;pic_ptr--) {
		if ((pic_list[pic_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_LONG_REF | FLAG_REMOVE_REF)) == (FLAG_ACTIVE_REF | FLAG_LONG_REF)) {
			ltp_num = pic_list[pic_ptr]._reserved & (MASK_LONG_IDX | FLAG_LONG_REF);

			ltp_ptr = decoder_cfg.list_count[0];
			for (unsigned int li(lt_start);li<decoder_cfg.list_count[0];li++) {
				if ((pic_list[li]._reserved & (MASK_LONG_IDX | FLAG_LONG_REF)) > ltp_num) {
					ltp_ptr = li;
					break;
				}
			}

			for (unsigned int li(decoder_cfg.list_count[0]++);li>ltp_ptr;li--) decoder_cfg.ref_pic_list[0][li] = decoder_cfg.ref_pic_list[0][li-1];
			decoder_cfg.ref_pic_list[0][ltp_ptr] = pic_ptr;
		}
	}
}


void Video::H264::InitializeRefPicListB() {
	unsigned int pic_ptr(0), ref_idx0(0), x_po(-1), ltp_num(0), lt_start(0), ltp_ptr(0);
		
	System::MemoryFill(decoder_cfg.ref_pic_list, 128*sizeof(unsigned short), 0);

		
	for (pic_ptr = decoder_cfg.current_frame_ptr-1;(pic_ptr &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;pic_ptr--) {
		if (((pic_list[pic_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_LONG_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF) && (pic_list[pic_ptr].id_rec.field_ocount[pic_list[pic_ptr]._reserved & FLAG_SECOND_FIELD] < pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD])) {
			
			x_po = ref_idx0;
			for (unsigned int i(0);i<ref_idx0;i++) {
				if (pic_list[pic_ptr].id_rec.field_ocount[pic_list[pic_ptr]._reserved & FLAG_SECOND_FIELD] > pic_list[decoder_cfg.ref_pic_list[0][i]].id_rec.field_ocount[pic_list[decoder_cfg.ref_pic_list[0][i]]._reserved & FLAG_SECOND_FIELD]) {
					x_po = i;
					break;
				}
			}
			
			for (unsigned int i(ref_idx0++);i>x_po;i--) decoder_cfg.ref_pic_list[0][i] = decoder_cfg.ref_pic_list[0][i-1];
			decoder_cfg.ref_pic_list[0][x_po] = pic_ptr;
		}
	}

	decoder_cfg.list_count[0] = ref_idx0;

	for (pic_ptr = decoder_cfg.current_frame_ptr-1;(pic_ptr &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;pic_ptr--) {
		if ((pic_list[pic_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_LONG_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF) {
			x_po = decoder_cfg.list_count[0];

			for (unsigned int i(0);i<ref_idx0;i++) {
				if (pic_ptr == decoder_cfg.ref_pic_list[0][i]) {
					x_po = -1;
					break;
				}
			}

			if (x_po != -1) {
				for (unsigned int i(ref_idx0);i<decoder_cfg.list_count[0];i++) {
					if (pic_list[pic_ptr].id_rec.field_ocount[pic_list[pic_ptr]._reserved & FLAG_SECOND_FIELD] < pic_list[decoder_cfg.ref_pic_list[0][i]].id_rec.field_ocount[pic_list[decoder_cfg.ref_pic_list[0][i]]._reserved & FLAG_SECOND_FIELD]) {
						x_po = i;
						break;
					}
				}

				for (unsigned int i(decoder_cfg.list_count[0]++);i>x_po;i--) decoder_cfg.ref_pic_list[0][i] = decoder_cfg.ref_pic_list[0][i-1];
				decoder_cfg.ref_pic_list[0][x_po] = pic_ptr;
			}
		}			
	}
	
// long refs list 0		
	lt_start = decoder_cfg.list_count[0];

	for (pic_ptr = decoder_cfg.current_frame_ptr-1;(pic_ptr &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;pic_ptr--) {
		if ((pic_list[pic_ptr]._reserved & (FLAG_ACTIVE_REF | FLAG_LONG_REF | FLAG_REMOVED_REF)) == (FLAG_ACTIVE_REF | FLAG_LONG_REF)) {
			ltp_num = pic_list[pic_ptr]._reserved & (MASK_LONG_IDX | FLAG_LONG_REF);

			ltp_ptr = decoder_cfg.list_count[0];
			for (unsigned int li(lt_start);li<decoder_cfg.list_count[0];li++) {
				if ((pic_list[li]._reserved & (MASK_LONG_IDX | FLAG_LONG_REF)) > ltp_num) {
					ltp_ptr = li;
					break;
				}
			}

			for (unsigned int li(decoder_cfg.list_count[0]++);li>ltp_ptr;li--) decoder_cfg.ref_pic_list[0][li] = decoder_cfg.ref_pic_list[0][li-1];
			decoder_cfg.ref_pic_list[0][ltp_ptr] = pic_ptr;
		}
	}







// list 1
	decoder_cfg.list_count[1] = decoder_cfg.list_count[0];

	for (unsigned int i1(lt_start);i1<decoder_cfg.list_count[1];i1++) decoder_cfg.ref_pic_list[1][i1] = decoder_cfg.ref_pic_list[0][i1];

	for (unsigned int i1(ref_idx0);i1<lt_start;i1++) decoder_cfg.ref_pic_list[1][i1 - ref_idx0] = decoder_cfg.ref_pic_list[0][i1];
	
	lt_start -= ref_idx0;
	
	for (unsigned int i1(0);i1<ref_idx0;i1++) decoder_cfg.ref_pic_list[1][i1 + lt_start] = decoder_cfg.ref_pic_list[0][i1];
	


}

UI_64 Video::H264::GetRefPicListModification() {
	unsigned int t_val(0), z_idx(0), o_val(0), num_val(0);
	unsigned char modification_of_pic_nums_idc(0);

	switch (current_slice._type) {
		case 1: case 6: // B
		case 0: case 5: // P
		case 3: case 8: // SP
						
			if (decoder_cfg.NextBit()) {
				num_val = current_slice.id_rec.frame_num;

				for (unsigned int ref_idx(0);modification_of_pic_nums_idc = (H264_DS_GolombUnsigned(&decoder_cfg) ^ 3);ref_idx++) {
					t_val = H264_DS_GolombUnsigned(&decoder_cfg) + 1;

					switch (modification_of_pic_nums_idc) {
						case 3: // sub							
							t_val = -t_val;
						case 2:
							num_val += t_val;
							num_val &= current_slice.max_pic_num;

							o_val = -1;

							for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;z_idx--) {
								if ((pic_list[z_idx].id_rec.frame_num == num_val) && ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF)) {
									o_val = z_idx;
									break;
								}
							}

							if (o_val != -1) {
								for (z_idx = (current_slice.num_ref_idx_active[0]+1);z_idx>ref_idx;z_idx--) {
									decoder_cfg.ref_pic_list[0][z_idx] = decoder_cfg.ref_pic_list[0][z_idx-1];
								}

								decoder_cfg.ref_pic_list[0][ref_idx] = o_val;

								for (z_idx = ref_idx;z_idx<=current_slice.num_ref_idx_active[0];z_idx++) {
									if (o_val == -1) {
										decoder_cfg.ref_pic_list[0][z_idx] = decoder_cfg.ref_pic_list[0][z_idx + 1];
									}

									if (decoder_cfg.ref_pic_list[0][z_idx+1] == o_val) o_val = -1;
								}
							}

						break;
						case 1: // long term pic num
							--t_val;
							t_val <<= 16;
							t_val |= FLAG_LONG_REF;
														

							for (unsigned int j(current_slice.num_ref_idx_active[0]);j>ref_idx;j--) {
								decoder_cfg.ref_pic_list[0][j] = decoder_cfg.ref_pic_list[0][j-1];
							}

							for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
								if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | MASK_LONG_IDX | FLAG_REMOVED_REF)) == t_val) {
									t_val = z_idx;
									break;
								}
							}

							if (t_val < MAX_REF_IDX) {
								decoder_cfg.ref_pic_list[0][ref_idx] = t_val;
							
								z_idx = decoder_cfg.ref_pic_list[0][current_slice.num_ref_idx_active[0]];

								o_val = 0;
								for (unsigned int j(ref_idx+1);j<current_slice.num_ref_idx_active[0];j++) {
									if (decoder_cfg.ref_pic_list[0][j] == t_val) o_val = 1;
								
									decoder_cfg.ref_pic_list[0][j] = decoder_cfg.ref_pic_list[0][j+o_val];
								}

								if (o_val) {
									decoder_cfg.ref_pic_list[0][current_slice.num_ref_idx_active[0]] = z_idx;
								} else {
									if (decoder_cfg.ref_pic_list[0][current_slice.num_ref_idx_active[0]] == t_val) decoder_cfg.ref_pic_list[0][current_slice.num_ref_idx_active[0]] = z_idx;
								}
							}

								
						break;

					}
				}
			}
		break;
	}

	if ((current_slice._type == 1) || (current_slice._type == 6)) {

		if (decoder_cfg.NextBit()) { // ref_pic_list_modification_flag_l1
			num_val = current_slice.id_rec.frame_num;

			for (unsigned int ref_idx(0);modification_of_pic_nums_idc = (H264_DS_GolombUnsigned(&decoder_cfg) ^ 3);ref_idx++) {
				t_val = H264_DS_GolombUnsigned(&decoder_cfg) + 1;

				switch (modification_of_pic_nums_idc) {
					case 3: // sub							
						t_val = -t_val;
					case 2:
						num_val += t_val;
						num_val &= current_slice.max_pic_num;

						o_val = -1;

						for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX - 1)) != decoder_cfg.current_frame_ptr;z_idx--) {
							if ((pic_list[z_idx].id_rec.frame_num == num_val) && ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF)) {
								o_val = z_idx;
								break;
							}
						}

						if (o_val != -1) {
							for (z_idx = (current_slice.num_ref_idx_active[1]+1);z_idx>ref_idx;z_idx--) {
								decoder_cfg.ref_pic_list[1][z_idx] = decoder_cfg.ref_pic_list[1][z_idx-1];
							}

							decoder_cfg.ref_pic_list[1][ref_idx] = o_val;

							for (z_idx = ref_idx;z_idx<=current_slice.num_ref_idx_active[1];z_idx++) {
								if (o_val == -1) {
									decoder_cfg.ref_pic_list[1][z_idx] = decoder_cfg.ref_pic_list[1][z_idx + 1];
								}

								if (decoder_cfg.ref_pic_list[1][z_idx+1] == o_val) o_val = -1;
							}
						}

					break;
					case 1: // long term
						--t_val;
						t_val <<= 16;
						t_val |= FLAG_LONG_REF;
						
						for (unsigned int j(current_slice.num_ref_idx_active[1]);j>ref_idx;j--) {
							decoder_cfg.ref_pic_list[1][j] = decoder_cfg.ref_pic_list[1][j-1];
						}

						for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
							if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | MASK_LONG_IDX | FLAG_REMOVED_REF)) == t_val) {
								t_val = z_idx;
								break;
							}
						}

						if (t_val < MAX_REF_IDX) {
							decoder_cfg.ref_pic_list[1][ref_idx] = t_val;
						
							z_idx = decoder_cfg.ref_pic_list[1][current_slice.num_ref_idx_active[1]];

							o_val = 0;
							for (unsigned int j(ref_idx+1);j<current_slice.num_ref_idx_active[1];j++) {
								if (decoder_cfg.ref_pic_list[1][j] == t_val) o_val = 1;
								
								decoder_cfg.ref_pic_list[1][j] = decoder_cfg.ref_pic_list[1][j+o_val];
							}

							if (o_val) {
								decoder_cfg.ref_pic_list[1][current_slice.num_ref_idx_active[1]] = z_idx;
							} else {
								if (decoder_cfg.ref_pic_list[1][current_slice.num_ref_idx_active[1]] == t_val) decoder_cfg.ref_pic_list[1][current_slice.num_ref_idx_active[1]] = z_idx;
							}
						}
								
					break;

				}
			}
		}
	}

	return 0;
}


unsigned int Video::H264::GetDecRefPicMarking() {
	unsigned int result(0);
	unsigned int ti_val(0), z_idx(0), o_val(0);

	unsigned char mem_op(1);


	if ((decoder_cfg.reset_op & 1) == 0) {
		if (decoder_cfg.NextBit()) { // adaptive_ref_pic_marking
			for (;mem_op = H264_DS_GolombUnsigned(&decoder_cfg);) {

				switch (mem_op) {
					case 2: // long term unused for reference
						o_val = H264_DS_GolombUnsigned(&decoder_cfg);
						o_val <<= 16;
						o_val |= FLAG_LONG_REF;

						for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
							if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | MASK_LONG_IDX | FLAG_REMOVED_REF)) == o_val) {
								pic_list[z_idx]._reserved |= FLAG_REMOVE_REF;

								break;
							}
						}
					break;

					case 3: // assign long term for short term
						ti_val = current_slice.id_rec.frame_num - H264_DS_GolombUnsigned(&decoder_cfg) - 1;
						ti_val &= current_slice.max_pic_num;

						o_val = H264_DS_GolombUnsigned(&decoder_cfg);

						if (o_val < 32) {
							for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
								if (pic_list[z_idx].id_rec.frame_num == ti_val) {
									pic_list[z_idx]._reserved |= (FLAG_SET_LONG_REF | (o_val<<16));

									break;
								}
							}
						}
					break;
					case 6: // asign long term to current picture						
						o_val = H264_DS_GolombUnsigned(&decoder_cfg);
						if (o_val < 32) {
							pic_list[decoder_cfg.current_frame_ptr]._reserved |= (FLAG_SET_LONG_REF | (o_val<<16));
						}
					break;



					case 1:	// short term unused for reference
						ti_val = current_slice.id_rec.frame_num - H264_DS_GolombUnsigned(&decoder_cfg) - 1;
						ti_val &= current_slice.max_pic_num;

						for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX -1)) != decoder_cfg.current_frame_ptr; z_idx--) {
							if (pic_list[z_idx].id_rec.frame_num == ti_val) {
								pic_list[z_idx]._reserved |= FLAG_REMOVE_REF;

								break;
							}
						}
						
					break;
					case 4: // mark all long term with idx greater than max as unused
						o_val = H264_DS_GolombUnsigned(&decoder_cfg);
						o_val <<= 16;
						o_val |= FLAG_LONG_REF;


						for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
							if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | MASK_LONG_IDX | FLAG_REMOVED_REF)) >= o_val) {
								pic_list[z_idx]._reserved |= FLAG_REMOVE_REF;

								break;
							}
						}

					break;
					case 5: // purge all
						decoder_cfg.reset_op = 2;					
					break;
				}
			}
		} else { // sliding window mode
			ti_val = 1;

			for (unsigned int j(0);j<32;j++) {
				if ((pic_list[j]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == (FLAG_LONG_REF | FLAG_ACTIVE_REF)) {
					ti_val++;
				}
			}
				
			o_val = -1;
			for (z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
				if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF) {
					if (ti_val >= current_slice.seq_set->max_num_ref_frames) {
						pic_list[z_idx]._reserved |= FLAG_REMOVE_REF;
						
					} else {
						if (current_slice.id_rec.field_pic_flag) {
							if (o_val != (pic_list[z_idx].id_rec.frame_num>>1)) {
								o_val = (pic_list[z_idx].id_rec.frame_num>>1);
								ti_val++;
							}
						} else {
							ti_val++;
						}
					}
				}
			}
		}
	} else { // idr
		ti_val = decoder_cfg.NextBit(); // no output

		if (decoder_cfg.NextBit()) { // long term
			pic_list[decoder_cfg.current_frame_ptr]._reserved |= FLAG_LONG_REF;
		}
				
	}
	

	return result;
}


void Video::H264::ApplyMarking(I_64 & time_mark) {
	unsigned int result(0), t_val(0), o_val(0);


	if (decoder_cfg.reset_op) {
		t_val = -1;
		result = -1;

		for (unsigned int i(0);i<MAX_REF_IDX;i++) {						
			if (pic_list[i]._reserved) {
				pic_list[i]._reserved |= FLAG_REMOVE_REF;

			}
		}
	} else {
		if (current_slice.id_rec.ref_idc) {
			result = ((current_slice.id_rec.frame_num - decoder_cfg.prev_ref_num) & current_slice.max_pic_num);
			if (current_slice.id_rec.field_pic_flag) result = ((result + 1) >> 1);
	
			if (result > 1) {
// gaps in id_rec.frame_num : page 126/127; nal_ref_idc not 0; not idr; field_pic_flag = 0; 
// page 132: gaps are marked before decoding of any slices of the current picture
					
				if (current_slice.id_rec.ref_idc == 0) --result;

				for (unsigned int j(0);j<32;j++) {
					if ((pic_list[j]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == (FLAG_LONG_REF | FLAG_ACTIVE_REF)) {
						result++;
					}
				}
				
				t_val = 0; o_val = -1;
				for	(unsigned int j = decoder_cfg.current_frame_ptr;t_val < MAX_REF_IDX;j--, t_val++) {
					j &= (MAX_REF_IDX - 1);
					
					if ((pic_list[j]._reserved & (FLAG_LONG_REF | FLAG_ACTIVE_REF | FLAG_REMOVED_REF)) == FLAG_ACTIVE_REF) {
						if (result >= current_slice.seq_set->max_num_ref_frames) {
							pic_list[j]._reserved |= FLAG_REMOVE_REF;
						} else {
							if (current_slice.id_rec.field_pic_flag) {
								if (o_val != (pic_list[j].id_rec.frame_num>>1)) {
									o_val = (pic_list[j].id_rec.frame_num>>1);
									result++;
								}
							} else {
								result++;
							}
						}
					}
				}

				t_val = 0;
			}
		}
	}

	t_val = 0;

	for (int i(MAX_REF_IDX-1);i>=0;i--) {
		if (time_table[i][1] != -1) {

			if (pic_list[time_table[i][1]]._reserved & FLAG_REMOVE_REF) {
				pic_list[time_table[i][1]]._reserved &= ~(FLAG_REMOVE_REF | FLAG_ACTIVE_REF);
				pic_list[time_table[i][1]]._reserved |= (FLAG_REMOVED_REF | FLAG_BUFFER_PIC);
			}


			if (++t_val >= min_buffer_frames) {
				if ((pic_list[time_table[i][1]]._reserved & (FLAG_REMOVED_REF | FLAG_OUT_REF)) == FLAG_REMOVED_REF) {
					pic_list[time_table[i][1]].Destroy();
					frame_list_count--;

					for (unsigned int j(i);j<(MAX_REF_IDX-1);j++) {
						time_table[j][0] = time_table[j+1][0];
						time_table[j][1] = time_table[j+1][1];
					}
				}
			}
		}
	}


	for (unsigned int i(0);i<MAX_REF_IDX;i++) {
		if (pic_list[i]._reserved & FLAG_SET_LONG_REF) {
			result = pic_list[i]._reserved & MASK_LONG_IDX;
			result |= FLAG_LONG_REF;

			for (unsigned int z_idx = (decoder_cfg.current_frame_ptr-1);(z_idx &= (MAX_REF_IDX-1)) != decoder_cfg.current_frame_ptr;z_idx--) {
				if ((pic_list[z_idx]._reserved & (FLAG_LONG_REF | MASK_LONG_IDX)) == result) {
					pic_list[z_idx].Destroy();
					frame_list_count--;

					break;
				}
			}

			pic_list[i]._reserved &= 0x00FFFFFF;
			pic_list[i]._reserved |= FLAG_LONG_REF;	
		}
	}
}

UI_64 Video::H264::GetWeightTable() {
//	UI_64 result(0);
	short wt_val[2] = {1, 1};

	current_slice.weight_denom_bits[0] = H264_DS_GolombUnsigned(&decoder_cfg);
	if (decoder_cfg.chroma_array_type) {
		current_slice.weight_denom_bits[1] = current_slice.weight_denom_bits[2] = H264_DS_GolombUnsigned(&decoder_cfg);
	}

	wt_val[0] <<= current_slice.weight_denom_bits[0];
	wt_val[1] <<= current_slice.weight_denom_bits[1];

	for (unsigned int i(0);i<=current_slice.num_ref_idx_active[0];i++) {

		if (decoder_cfg.NextBit()) {
			current_slice.weight_table[i][0][0].c_weight = decoder_cfg.GolombSigned();
			current_slice.weight_table[i][0][0].c_offset = decoder_cfg.GolombSigned();
		} else {
			current_slice.weight_table[i][0][0].c_weight = wt_val[0];
		}

		if (decoder_cfg.chroma_array_type) {

			if (decoder_cfg.NextBit()) {
				current_slice.weight_table[i][0][1].c_weight = decoder_cfg.GolombSigned();
				current_slice.weight_table[i][0][1].c_offset = decoder_cfg.GolombSigned();

				current_slice.weight_table[i][0][2].c_weight = decoder_cfg.GolombSigned();
				current_slice.weight_table[i][0][2].c_offset = decoder_cfg.GolombSigned();
			} else {
				current_slice.weight_table[i][0][1].c_weight = current_slice.weight_table[i][0][2].c_weight = wt_val[1];
			}

		}
	}

	if ((current_slice._type == 1) && (current_slice._type == 6)) {
		for (unsigned int i(0);i<=current_slice.num_ref_idx_active[1];i++) {

			if (decoder_cfg.NextBit()) {
				current_slice.weight_table[i][1][0].c_weight = decoder_cfg.GolombSigned();
				current_slice.weight_table[i][1][0].c_offset = decoder_cfg.GolombSigned();
			} else {
				current_slice.weight_table[i][1][0].c_weight = wt_val[0];
			}

			if (decoder_cfg.chroma_array_type) {

				if (decoder_cfg.NextBit()) {
					current_slice.weight_table[i][1][1].c_weight = decoder_cfg.GolombSigned();
					current_slice.weight_table[i][1][1].c_offset = decoder_cfg.GolombSigned();

					current_slice.weight_table[i][1][2].c_weight = decoder_cfg.GolombSigned();
					current_slice.weight_table[i][1][2].c_offset = decoder_cfg.GolombSigned();
				} else {
					current_slice.weight_table[i][1][1].c_weight = current_slice.weight_table[i][1][2].c_weight = wt_val[1];
				}
			}
		}
	}

	return 0;
}

UI_64 Video::H264::SliceCleanUp() {
	if (current_slice.pic_set) picture_params.Release();
	if (current_slice.seq_set) sequence_params.Release();
		
	current_slice.pic_set = 0;
	current_slice.seq_set = 0;
	
	return 0;
}

UI_64 Video::H264::GetSliceLayer(I_64 & time_mark) {
	UI_64 result(-6);
	result <<= 32;

	if (result = GetSliceHeader(time_mark)) {
		if (decoder_cfg.IsSource())
		if (current_slice.pic_set->entropy_coding_mode_flag) result |= GetSliceDataE();
		else result |= GetSliceData();

		if ((result & RESULT_BUFFER_SET) == 0) {
			result &= ~RESULT_DECODE_COMPLETE;
		}
	}

	SliceCleanUp();
	
	return result;
}
/*
UI_64 Video::H264::GetSlicePartitionA() {
//	System::MemoryFill(&current_slice, sizeof(Slice), 0);

//	if (SequencePSet * seq_p = reinterpret_cast<SequencePSet *>(sequence_params.Acquire())) {
//		if (PicturePSet * pic_p = reinterpret_cast<PicturePSet *>(picture_params.Acquire())) {

	if (GetSliceHeader()) {
		current_slice._id = H264_DS_GolombUnsigned(&decoder_cfg);

		if (decoder_cfg.IsSource())
		if (current_slice.pic_set->entropy_coding_mode_flag) GetSliceDataE();
		else GetSliceData();

	}

	return 0;
}

UI_64 Video::H264::GetSlicePartitionB() {
	
	current_slice._id = H264_DS_GolombUnsigned(&decoder_cfg);

	if (current_slice.seq_set && current_slice.pic_set) {
		if (current_slice.seq_set->separate_planes_flag) {
			current_slice.colour_plane_id = H264_DS_GetNumber(&decoder_cfg, 2);
		}

		if (current_slice.pic_set->redundant_pic_cnt_flag) {
			current_slice.redundant_pic_cnt = H264_DS_GolombUnsigned(&decoder_cfg);
		}

		if (decoder_cfg.IsSource())
		if (current_slice.pic_set->entropy_coding_mode_flag) GetSliceDataE();
		else GetSliceData();
	}

	return 0;
}

*/
// extensions ========

UI_64 Video::H264::GetSVCExtension(SequencePSet * seq_params) {
//	UI_64 result(0);

	seq_params->SVC.inter_layer_deblocking_flag = decoder_cfg.NextBit();

	seq_params->SVC.extended_spacial_capability_idc = H264_DS_GetNumber(&decoder_cfg, 2);

	if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
		seq_params->SVC.chroma_phase_x = decoder_cfg.NextBit();
	}

	if (decoder_cfg.chroma_array_type == 1) {
		seq_params->SVC.chroma_phase_y = H264_DS_GetNumber(&decoder_cfg, 2);
	}

	if (seq_params->SVC.extended_spacial_capability_idc == 1) {
		if (decoder_cfg.chroma_array_type) {
			seq_params->SVC.ref_layer_chroma_phase_x = decoder_cfg.NextBit();

			seq_params->SVC.ref_layer_chroma_phase_y = H264_DS_GetNumber(&decoder_cfg, 2);
		}

		seq_params->SVC.scaled_ref_layer_left_offset = decoder_cfg.GolombSigned();
		seq_params->SVC.scaled_ref_layer_top_offset = decoder_cfg.GolombSigned();
		seq_params->SVC.scaled_ref_layer_right_offset = decoder_cfg.GolombSigned();
		seq_params->SVC.scaled_ref_layer_bottom_offset = decoder_cfg.GolombSigned();
	}

	seq_params->SVC.tcoeff_level_prediction_flag = decoder_cfg.NextBit();

	if (seq_params->SVC.tcoeff_level_prediction_flag) {
		seq_params->SVC.adaptive_tcoeff_level_prediction_flag = decoder_cfg.NextBit();
	}

	seq_params->SVC.slice_header_restriction_flag = decoder_cfg.NextBit();


	return 0;
}

UI_64 Video::H264::GetSVCVUI(SequencePSet * seq_params) {


	return 0;
}
			
UI_64 Video::H264::GetMVCExtension(SequencePSet * seq_params) {


	return 0;
}
			
UI_64 Video::H264::GetMVCVUI(SequencePSet * seq_params) {


	return 0;
}

UI_64 Video::H264::GetMVCDExtension(SequencePSet * seq_params) {


	return 0;
}

UI_64 Video::H264::GetRefPicListMVC() {


	return 0;
}

