
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE



.CODE

ALIGN 16
H264_SamplesShake8x8	PROC

	MOVDQA xmm1, [rcx+16]
	MOVDQA xmm2, [rcx+32]
	MOVDQA xmm3, [rcx+48]
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm5, [rcx+80]
	MOVDQA xmm6, [rcx+96]
	MOVDQA xmm7, [rcx+112]
	MOVDQA xmm8, [rcx+128]
	MOVDQA xmm9, [rcx+144]
	MOVDQA xmm10, [rcx+160]
	MOVDQA xmm11, [rcx+176]
	MOVDQA xmm12, [rcx+192]
	MOVDQA xmm13, [rcx+208]
	MOVDQA xmm14, [rcx+224]
	MOVDQA xmm15, [rcx+240]


	MOVDQA [rcx+16], xmm8
	MOVDQA [rcx+32], xmm1
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm2
	MOVDQA [rcx+80], xmm10
	MOVDQA [rcx+96], xmm3
	MOVDQA [rcx+112], xmm11
	MOVDQA [rcx+128], xmm4
	MOVDQA [rcx+144], xmm12
	MOVDQA [rcx+160], xmm5
	MOVDQA [rcx+176], xmm13
	MOVDQA [rcx+192], xmm6
	MOVDQA [rcx+208], xmm14
	MOVDQA [rcx+224], xmm7
	MOVDQA [rcx+240], xmm15

	ADD rcx, 256

	MOVDQA xmm1, [rcx+16]
	MOVDQA xmm2, [rcx+32]
	MOVDQA xmm3, [rcx+48]
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm5, [rcx+80]
	MOVDQA xmm6, [rcx+96]
	MOVDQA xmm7, [rcx+112]
	MOVDQA xmm8, [rcx+128]
	MOVDQA xmm9, [rcx+144]
	MOVDQA xmm10, [rcx+160]
	MOVDQA xmm11, [rcx+176]
	MOVDQA xmm12, [rcx+192]
	MOVDQA xmm13, [rcx+208]
	MOVDQA xmm14, [rcx+224]
	MOVDQA xmm15, [rcx+240]


	MOVDQA [rcx+16], xmm8
	MOVDQA [rcx+32], xmm1
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm2
	MOVDQA [rcx+80], xmm10
	MOVDQA [rcx+96], xmm3
	MOVDQA [rcx+112], xmm11
	MOVDQA [rcx+128], xmm4
	MOVDQA [rcx+144], xmm12
	MOVDQA [rcx+160], xmm5
	MOVDQA [rcx+176], xmm13
	MOVDQA [rcx+192], xmm6
	MOVDQA [rcx+208], xmm14
	MOVDQA [rcx+224], xmm7
	MOVDQA [rcx+240], xmm15


	RET
H264_SamplesShake8x8	ENDP


ALIGN 16
H264_ReferenceFilter8x8	PROC
	MOV r11w, [rcx+48]
	SHL r11d, 16
	MOV r11w, [rcx+48]
	SHL r11, 16
	MOV r11w, [rcx+48]
	SHL r11, 16
	MOV r11w, [rcx+48]


	MOVSX r10, WORD PTR [rcx+50] ; mask
	MOVZX rax, WORD PTR [rcx+52] ; left scale
	
	AND r11, r10
	NOT r10d

	ROR r10, 32
	MOV r10w, r11w
	SHL r10w, 1
	ROR r10, 32
	
	PCMPEQW xmm7, xmm7

	AND r8, r8
	JZ no_up
		ROR r11, 32
		MOV r11w, [r8] ; x0
		AND r10w, r11w
		ROR r11, 32
		
		OR r11w, r10w
		
		MOVDQA xmm0, [r8]
		MOVDQA xmm1, xmm0
		
		PSHUFHW xmm1, xmm1, 0FFh
		PUNPCKHQDQ xmm1, xmm1
		
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm1

		PSRLDQ xmm4, 2
		PSHUFHW xmm4, xmm4, 0A4h

		AND r9, r9
		JZ no_xup
			PINSRW xmm4, WORD PTR [r9], 7
			MOVDQA xmm1, [r9]
			MOVDQA xmm3, xmm1
			MOVDQA xmm5, xmm1

			PSRLDQ xmm5, 2
			PSHUFHW xmm5, xmm5, 0A4h

			PSLLDQ xmm1, 2
			PINSRW xmm1, WORD PTR [r8+14], 0
		no_xup:


		PSLLDQ xmm0, 2
		PINSRW xmm0, r11d, 0

		PSUBW xmm2, xmm7
		PSUBW xmm3, xmm7

		PADDW xmm0, xmm2
		PADDW xmm0, xmm2
		PADDW xmm0, xmm4

		PADDW xmm1, xmm3
		PADDW xmm1, xmm3
		PADDW xmm1, xmm5

		PSRAW xmm0, 2
		PSRAW xmm1, 2

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1

		MOV r9d, 1

	no_up:

	SHR r10, 16
	SHR r11, 16



	AND rdx, rdx
	JZ no_left
		LEA rdx, [rdx + rax - 2]

		ROR r11, 32
		MOV r11w, [rdx]
		AND r10w, r11w
		ROR r11, 32
		
		OR r11w, r10w

		PINSRW xmm0, WORD PTR [rdx], 0
		PINSRW xmm0, WORD PTR [rdx+rax], 1
		LEA rdx, [rdx + rax*2]
		PINSRW xmm0, WORD PTR [rdx], 2
		PINSRW xmm0, WORD PTR [rdx+rax], 3
		LEA rdx, [rdx + rax*2]
		PINSRW xmm0, WORD PTR [rdx], 4
		PINSRW xmm0, WORD PTR [rdx+rax], 5
		LEA rdx, [rdx + rax*2]
		PINSRW xmm0, WORD PTR [rdx], 6
		PINSRW xmm0, WORD PTR [rdx+rax], 7


		MOVDQA xmm1, xmm0
		MOVDQA xmm2, xmm0

		PSLLDQ xmm0, 2
		PSRLDQ xmm2, 2
		PINSRW xmm0, r11d, 0
		PSHUFHW xmm2, xmm2, 0A4h

		PSUBW xmm1, xmm7		

		PADDW xmm0, xmm1
		PADDW xmm0, xmm1
		PADDW xmm0, xmm2				

		PSRAW xmm0, 2

		MOVDQA [rcx + 32], xmm0
		
		OR r9d, 2
	no_left:

	SHR r10, 16
	SHR r11, 16

	ADD r10w, r11w
	SHR r11, 16
	ADD r10w, r11w
	ADD r10w, 2
	SAR r10w, 2

	MOV [rcx+48], r10w

	MOV eax, r9d

	RET
H264_ReferenceFilter8x8	ENDP







ALIGN 16
H264_DCIPred8x8	PROC

	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3

	PUNPCKLWD xmm0, [rdx]
	PUNPCKHWD xmm1, [rdx]
	PUNPCKLWD xmm2, [rdx+32]
	PUNPCKHWD xmm3, [rdx+32]

	PSRAD xmm0, 16
	PSRAD xmm1, 16
	PSRAD xmm2, 16
	PSRAD xmm3, 16

	PADDD xmm0, xmm1
	PADDD xmm2, xmm3
	PCMPEQD xmm3, xmm3
	PSUBD xmm0, xmm3
	PSUBD xmm2, xmm3

	PADDD xmm0, xmm2		
	PHADDD xmm0, xmm0
	PHADDD xmm0, xmm0

	PSRAD xmm0, 4

	PACKSSDW xmm0, xmm0

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0
	MOVDQA xmm3, xmm0
	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm0
	MOVDQA xmm6, xmm0
	MOVDQA xmm7, xmm0

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_DCIPred8x8	ENDP







ALIGN 16
H264_VerticalIPred8x8	PROC
	MOVDQA xmm0, [rdx]	

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0
	MOVDQA xmm3, xmm0
	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm0
	MOVDQA xmm6, xmm0
	MOVDQA xmm7, xmm0


	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_VerticalIPred8x8	ENDP

ALIGN 16
H264_HorizontalIPred8x8	PROC
	MOVDQA xmm0, [rdx+32]

	MOVDQA xmm4, xmm0
	PUNPCKLWD xmm0, xmm0
	PUNPCKHWD xmm4, xmm4

	MOVDQA xmm2, xmm0
	MOVDQA xmm6, xmm4
	PUNPCKLDQ xmm0, xmm0
	PUNPCKHDQ xmm2, xmm2
	PUNPCKLDQ xmm4, xmm4
	PUNPCKHDQ xmm6, xmm6
	
	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6

	PUNPCKLQDQ xmm0, xmm0
	PUNPCKHQDQ xmm1, xmm1
	PUNPCKLQDQ xmm2, xmm2
	PUNPCKHQDQ xmm3, xmm3
	PUNPCKLQDQ xmm4, xmm4
	PUNPCKHQDQ xmm5, xmm5
	PUNPCKLQDQ xmm6, xmm6
	PUNPCKHQDQ xmm7, xmm7

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_HorizontalIPred8x8	ENDP


ALIGN 16
H264_DownLeftIPred8x8	PROC
	PCMPEQW xmm15, xmm15
	PSLLW xmm15, 1

	MOVDQA xmm0, [rdx]

	MOVDQA xmm1, xmm0
	PSRLDQ xmm1, 2
	PINSRW xmm1, WORD PTR [rdx+16], 7
	
	MOVDQA xmm2, xmm1
	PSRLDQ xmm2, 2
	PINSRW xmm2, WORD PTR [rdx+18], 7

	MOVDQA xmm3, xmm2
	PSRLDQ xmm3, 2
	PINSRW xmm3, WORD PTR [rdx+20], 7

	MOVDQA xmm4, xmm3
	PSRLDQ xmm4, 2
	PINSRW xmm4, WORD PTR [rdx+22], 7

	MOVDQA xmm5, xmm4
	PSRLDQ xmm5, 2
	PINSRW xmm5, WORD PTR [rdx+24], 7

	MOVDQA xmm6, xmm5
	PSRLDQ xmm6, 2
	PINSRW xmm6, WORD PTR [rdx+26], 7

	MOVDQA xmm7, xmm6
	PSRLDQ xmm7, 2
	PINSRW xmm7, WORD PTR [rdx+28], 7

	MOVDQA xmm8, xmm7
	PSRLDQ xmm8, 2
	PINSRW xmm8, WORD PTR [rdx+30], 7

	MOVDQA xmm9, xmm8
	PSRLDQ xmm9, 2
	PINSRW xmm9, WORD PTR [rdx+30], 7



	MOVDQA xmm10, xmm1
	PSLLW xmm10, 1
	PADDW xmm0, xmm10
	PADDW xmm0, xmm2
	PSUBW xmm0, xmm15
	PSRAW xmm0, 2
	PADDW xmm0, [rcx]

	MOVDQA xmm10, xmm2
	PSLLW xmm10, 1
	PADDW xmm1, xmm10
	PADDW xmm1, xmm3
	PSUBW xmm1, xmm15
	PSRAW xmm1, 2
	PADDW xmm1, [rcx+16]

	MOVDQA xmm10, xmm3
	PSLLW xmm10, 1
	PADDW xmm2, xmm10
	PADDW xmm2, xmm4
	PSUBW xmm2, xmm15
	PSRAW xmm2, 2
	PADDW xmm2, [rcx+32]

	MOVDQA xmm10, xmm4
	PSLLW xmm10, 1
	PADDW xmm3, xmm10
	PADDW xmm3, xmm5
	PSUBW xmm3, xmm15
	PSRAW xmm3, 2
	PADDW xmm3, [rcx+48]

	MOVDQA xmm10, xmm5
	PSLLW xmm10, 1
	PADDW xmm4, xmm10
	PADDW xmm4, xmm6
	PSUBW xmm4, xmm15
	PSRAW xmm4, 2
	PADDW xmm4, [rcx+64]

	MOVDQA xmm10, xmm6
	PSLLW xmm10, 1
	PADDW xmm5, xmm10
	PADDW xmm5, xmm7
	PSUBW xmm5, xmm15
	PSRAW xmm5, 2
	PADDW xmm5, [rcx+80]

	MOVDQA xmm10, xmm7
	PSLLW xmm10, 1
	PADDW xmm6, xmm10
	PADDW xmm6, xmm8
	PSUBW xmm6, xmm15
	PSRAW xmm6, 2
	PADDW xmm6, [rcx+96]
		
	PSLLW xmm8, 1
	PADDW xmm7, xmm8
	PADDW xmm7, xmm9
	PSUBW xmm7, xmm15
	PSRAW xmm7, 2
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7


	RET
H264_DownLeftIPred8x8	ENDP


ALIGN 16
H264_DownRightIPred8x8	PROC
	PCMPEQW xmm15, xmm15
	PSLLW xmm15, 1

	MOVDQA xmm0, [rdx]

	MOVDQA xmm1, xmm0
	PSLLDQ xmm1, 2
	PINSRW xmm1, WORD PTR [rdx+48], 0

	MOVDQA xmm2, xmm1
	PSLLDQ xmm2, 2
	PINSRW xmm2, WORD PTR [rdx+32], 0

	MOVDQA xmm3, xmm2
	PSLLDQ xmm3, 2
	PINSRW xmm3, WORD PTR [rdx+34], 0

	MOVDQA xmm4, xmm3
	PSLLDQ xmm4, 2
	PINSRW xmm4, WORD PTR [rdx+36], 0

	MOVDQA xmm5, xmm4
	PSLLDQ xmm5, 2
	PINSRW xmm5, WORD PTR [rdx+38], 0

	MOVDQA xmm6, xmm5
	PSLLDQ xmm6, 2
	PINSRW xmm6, WORD PTR [rdx+40], 0

	MOVDQA xmm7, xmm6
	PSLLDQ xmm7, 2
	PINSRW xmm7, WORD PTR [rdx+42], 0

	MOVDQA xmm8, xmm7
	PSLLDQ xmm8, 2
	PINSRW xmm8, WORD PTR [rdx+44], 0

	MOVDQA xmm9, xmm8
	PSLLDQ xmm9, 2
	PINSRW xmm9, WORD PTR [rdx+46], 0


	MOVDQA xmm10, xmm1
	PSLLW xmm10, 1
	PADDW xmm0, xmm10
	PADDW xmm0, xmm2
	PSUBW xmm0, xmm15
	PSRAW xmm0, 2
	PADDW xmm0, [rcx]

	MOVDQA xmm10, xmm2
	PSLLW xmm10, 1
	PADDW xmm1, xmm10
	PADDW xmm1, xmm3
	PSUBW xmm1, xmm15
	PSRAW xmm1, 2
	PADDW xmm1, [rcx+16]

	MOVDQA xmm10, xmm3
	PSLLW xmm10, 1
	PADDW xmm2, xmm10
	PADDW xmm2, xmm4
	PSUBW xmm2, xmm15
	PSRAW xmm2, 2
	PADDW xmm2, [rcx+32]

	MOVDQA xmm10, xmm4
	PSLLW xmm10, 1
	PADDW xmm3, xmm10
	PADDW xmm3, xmm5
	PSUBW xmm3, xmm15
	PSRAW xmm3, 2
	PADDW xmm3, [rcx+48]

	MOVDQA xmm10, xmm5
	PSLLW xmm10, 1
	PADDW xmm4, xmm10
	PADDW xmm4, xmm6
	PSUBW xmm4, xmm15
	PSRAW xmm4, 2
	PADDW xmm4, [rcx+64]

	MOVDQA xmm10, xmm6
	PSLLW xmm10, 1
	PADDW xmm5, xmm10
	PADDW xmm5, xmm7
	PSUBW xmm5, xmm15
	PSRAW xmm5, 2
	PADDW xmm5, [rcx+80]

	MOVDQA xmm10, xmm7
	PSLLW xmm10, 1
	PADDW xmm6, xmm10
	PADDW xmm6, xmm8
	PSUBW xmm6, xmm15
	PSRAW xmm6, 2
	PADDW xmm6, [rcx+96]
		
	PSLLW xmm8, 1
	PADDW xmm7, xmm8
	PADDW xmm7, xmm9
	PSUBW xmm7, xmm15
	PSRAW xmm7, 2
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_DownRightIPred8x8	ENDP


ALIGN 16
H264_VRightIPred8x8	PROC
	PCMPEQW xmm15, xmm15

	MOVDQA xmm2, [rdx]		; x
	MOVDQA xmm1, [rdx+32]	; y

	MOVDQA xmm3, xmm2
	MOVDQA xmm0, xmm2
	PSRLDQ xmm3, 2 ; 1234567
	PSLLDQ xmm0, 2	
	PINSRW xmm0, WORD PTR [rdx+48], 0
	
	PSUBW xmm2, xmm15

	PADDW xmm0, xmm2
	PADDW xmm2, xmm0
	PSRAW xmm0, 1				; 0 2 4 6 8 10 12 14
	
	PADDW xmm2, xmm3
	PSRAW xmm2, 2				; 1 3 5 7 9 11 13

	


	
	MOVDQA xmm4, xmm1
	PSLLDQ xmm4, 2
	PINSRW xmm4, WORD PTR [rdx+48], 0

	MOVDQA xmm5, xmm4
	PSLLDQ xmm5, 2
	PINSRW xmm5, WORD PTR [rdx], 0

	PSUBW xmm4, xmm15
	
	PADDW xmm1, xmm4
	PADDW xmm1, xmm4
	PADDW xmm1, xmm5
	PSRAW xmm1, 2				; -1 -2 -3 -4 -5 -6 -7

	

	PSRLDQ xmm15, 14
	
	MOVDQA xmm6, xmm0	; 0 2 4 6 8 10 12 14

	MOVDQA xmm7, xmm2
	PSLLDQ xmm7, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm7, xmm5		; -1 1 3 5 7 9 11 13	

	MOVDQA xmm8, xmm6
	PSLLDQ xmm8, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm8, xmm5		; -2 0 2 4 6 8 10 12

	MOVDQA xmm9, xmm7
	PSLLDQ xmm9, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm9, xmm5		; -3 -1 1 3 5 7 9 11

	MOVDQA xmm10, xmm8
	PSLLDQ xmm10, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm10, xmm5		; -4 -2 0 2 4 6 8 10

	MOVDQA xmm11, xmm9
	PSLLDQ xmm11, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm11, xmm5		; -5 -3 -1 1 3 5 7 9

	MOVDQA xmm12, xmm10
	PSLLDQ xmm12, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm12, xmm5		; -6 -4 -2 0 2 4 6 8

	MOVDQA xmm13, xmm11
	PSLLDQ xmm13, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm13, xmm5		; -7 -5 -3 -1 1 3 5 7


	PADDW xmm6, [rcx]
	PADDW xmm7, [rcx+16]
	PADDW xmm8, [rcx+32]
	PADDW xmm9, [rcx+48]
	PADDW xmm10, [rcx+64]
	PADDW xmm11, [rcx+80]
	PADDW xmm12, [rcx+96]
	PADDW xmm13, [rcx+112]

	MOVDQA [rcx], xmm6
	MOVDQA [rcx+16], xmm7
	MOVDQA [rcx+32], xmm8
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm10
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm13


	RET
H264_VRightIPred8x8	ENDP

ALIGN 16
H264_HDownIPred8x8	PROC
	PCMPEQW xmm15, xmm15

	MOVDQA xmm1, [rdx]
	MOVDQA xmm2, [rdx+32]

	MOVDQA xmm3, xmm2
	MOVDQA xmm0, xmm2
	PSRLDQ xmm3, 2 ; 1234567
	PSLLDQ xmm0, 2	
	PINSRW xmm0, WORD PTR [rdx+48], 0
	
	PSUBW xmm2, xmm15

	PADDW xmm0, xmm2
	PADDW xmm2, xmm0
	PSRAW xmm0, 1				; 0 2 4 6 8 10 12 14
	
	PADDW xmm2, xmm3
	PSRAW xmm2, 2				; 1 3 5 7 9 11 13

	


	
	MOVDQA xmm4, xmm1
	PSLLDQ xmm4, 2
	PINSRW xmm4, WORD PTR [rdx+48], 0

	MOVDQA xmm5, xmm4
	PSLLDQ xmm5, 2
	PINSRW xmm5, WORD PTR [rdx+32], 0

	PSUBW xmm4, xmm15	

	PADDW xmm1, xmm4
	PADDW xmm1, xmm4
	PADDW xmm1, xmm5
	PSRAW xmm1, 2				; -1 -2 -3 -4 -5 -6 -7

	

	PSRLDQ xmm15, 14
	
	MOVDQA xmm6, xmm0	; 0 2 4 6 8 10 12 14

	MOVDQA xmm7, xmm2
	PSLLDQ xmm7, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm7, xmm5		; -1 1 3 5 7 9 11 13	

	MOVDQA xmm8, xmm6
	PSLLDQ xmm8, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm8, xmm5		; -2 0 2 4 6 8 10 12

	MOVDQA xmm9, xmm7
	PSLLDQ xmm9, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm9, xmm5		; -3 -1 1 3 5 7 9 11

	MOVDQA xmm10, xmm8
	PSLLDQ xmm10, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm10, xmm5		; -4 -2 0 2 4 6 8 10

	MOVDQA xmm11, xmm9
	PSLLDQ xmm11, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm11, xmm5		; -5 -3 -1 1 3 5 7 9

	MOVDQA xmm12, xmm10
	PSLLDQ xmm12, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm12, xmm5		; -6 -4 -2 0 2 4 6 8

	MOVDQA xmm13, xmm11
	PSLLDQ xmm13, 2
	PSRLDQ xmm1, 2
	MOVDQA xmm5, xmm1
	PAND xmm5, xmm15
	POR xmm13, xmm5		; -7 -5 -3 -1 1 3 5 7


; transpose
	MOVDQA xmm0, xmm6
	PUNPCKLWD xmm0, xmm7 ; 0 8 1 9 2 10 3 11
	PUNPCKHWD xmm6, xmm7 ; 4 12 5 13 6 14 7 15

	MOVDQA xmm1, xmm8
	PUNPCKLWD xmm1, xmm9 ; 16 24 17 25 18 26 19 27
	PUNPCKHWD xmm8, xmm9 ; 20 28 21 29 22 30 23 31

	MOVDQA xmm2, xmm10
	PUNPCKLWD xmm2, xmm11 ; 32 40 33 41 34 42 35 43
	PUNPCKHWD xmm10, xmm11 ; 36 44 37 45 38 46 39 47

	MOVDQA xmm3, xmm12
	PUNPCKLWD xmm3, xmm13 ; 48 56 49 57 50 58 51 59
	PUNPCKHWD xmm12, xmm13 ; 52 60 53 61 54 62 55 63



	MOVDQA xmm7, xmm0
	PUNPCKLDQ xmm0, xmm1 ; 0 8 16 24 1 9 17 25
	PUNPCKHDQ xmm7, xmm1 ; 2 10 18 26

	MOVDQA xmm9, xmm6
	PUNPCKLDQ xmm6, xmm8 ; 4 12 20 28 5 13 21 29
	PUNPCKHDQ xmm9, xmm8 ; 6 14 22 30

	MOVDQA xmm11, xmm2
	PUNPCKLDQ xmm2, xmm3 ; 32 40 48 56 33 41 53 61
	PUNPCKHDQ xmm11, xmm3 ; 34 42 50 58

	MOVDQA xmm13, xmm10
	PUNPCKLDQ xmm10, xmm12 ; 36 44 52 60 37 45 53 61
	PUNPCKHDQ xmm13, xmm12 ; 38 46 54 62



	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm2 ; 0
	PUNPCKHQDQ xmm1, xmm2 ; 1

	MOVDQA xmm8, xmm7
	PUNPCKLQDQ xmm7, xmm11 ; 2
	PUNPCKHQDQ xmm8, xmm11 ; 3

	MOVDQA xmm3, xmm6
	PUNPCKLQDQ xmm3, xmm10 ; 4
	PUNPCKHQDQ xmm6, xmm10 ; 5

	MOVDQA xmm12, xmm9
	PUNPCKLQDQ xmm9, xmm13 ; 6
	PUNPCKHQDQ xmm12, xmm13 ; 7



	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm7, [rcx+32]
	PADDW xmm8, [rcx+48]
	PADDW xmm3, [rcx+64]
	PADDW xmm6, [rcx+80]
	PADDW xmm9, [rcx+96]
	PADDW xmm12, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm7
	MOVDQA [rcx+48], xmm8
	MOVDQA [rcx+64], xmm3
	MOVDQA [rcx+80], xmm6
	MOVDQA [rcx+96], xmm9
	MOVDQA [rcx+112], xmm12




	RET
H264_HDownIPred8x8	ENDP

ALIGN 16
H264_VLeftIPred8x8	PROC
	PCMPEQW xmm15, xmm15

	MOVDQA xmm0, [rdx]

	MOVDQA xmm1, xmm0
	PSRLDQ xmm1, 2
	PINSRW xmm1, WORD PTR [rdx+16], 7
	
	MOVDQA xmm2, xmm1
	PSRLDQ xmm2, 2
	PINSRW xmm2, WORD PTR [rdx+18], 7

	MOVDQA xmm3, xmm2
	PSRLDQ xmm3, 2
	PINSRW xmm3, WORD PTR [rdx+20], 7

	MOVDQA xmm4, xmm3
	PSRLDQ xmm4, 2
	PINSRW xmm4, WORD PTR [rdx+22], 7

	MOVDQA xmm5, xmm4
	PSRLDQ xmm5, 2
	PINSRW xmm5, WORD PTR [rdx+24], 7




		
	MOVDQA xmm10, xmm1
	

	PSUBW xmm1, xmm15
	PADDW xmm0, xmm1	; 0 + 1
	PADDW xmm1, xmm0
	PSRAW xmm0, 1

		
	PADDW xmm1, xmm2
	PSRAW xmm1, 2		; 0 + 2*1 + 2

	MOVDQA xmm11, xmm2
	
	PSUBW xmm2, xmm15
	PADDW xmm10, xmm2
	PADDD xmm2, xmm10
	
	PSRAW xmm10, 1		; 1 + 2

	PADDW xmm2, xmm3
	PSRAW xmm2, 2		; 1 + 2*2 + 3

	MOVDQA xmm12, xmm3
	
	PSUBW xmm3, xmm15
	PADDW xmm11, xmm3
	PADDW xmm3, xmm11
	PSRAW xmm11, 1		; 2 + 3

	PADDW xmm3, xmm4
	PSRAW xmm3, 2		; 2 + 2*3 + 4

	PSUBW xmm4, xmm15
	PADDW xmm12, xmm4
	PADDW xmm4, xmm12
	PSRAW xmm12, 1		; 3 + 4

	PADDW xmm4, xmm5
	PSRAW xmm4, 2		; 3 + 2*4 + 5


	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm10, [rcx+32]
	PADDW xmm2, [rcx+48]
	PADDW xmm11, [rcx+64]
	PADDW xmm3, [rcx+80]
	PADDW xmm12, [rcx+96]
	PADDW xmm4, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm10
	MOVDQA [rcx+48], xmm2
	MOVDQA [rcx+64], xmm11
	MOVDQA [rcx+80], xmm3
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm4
	


	RET
H264_VLeftIPred8x8	ENDP

ALIGN 16
H264_HUpIPred8x8	PROC
	PCMPEQW xmm15, xmm15
		
	MOVDQA xmm0, [rdx+32]

	MOVDQA xmm1, xmm0
	PSRLDQ xmm1, 2
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm2, xmm1
	PSRLDQ xmm2, 2
	PSHUFHW xmm2, xmm2, 0A4h
	
	PSUBW xmm1, xmm15

	PADDW xmm0, xmm1	
	PADDW xmm1, xmm0

	PADDW xmm1, xmm2
	PSRAW xmm0, 1			; 0 + 1
	PSRAW xmm1, 2			; 0 + 2*1 + 2

	
	
	MOVDQA xmm2, xmm0
	PUNPCKLWD xmm2, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm3, xmm0
	PUNPCKLWD xmm3, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm4, xmm0
	PUNPCKLWD xmm4, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm5, xmm0
	PUNPCKLWD xmm5, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm6, xmm0
	PUNPCKLWD xmm6, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	MOVDQA xmm7, xmm0
	PUNPCKLWD xmm7, xmm1
	PSRLDQ xmm0, 2
	PSRLDQ xmm1, 2
	PSHUFHW xmm0, xmm0, 0A4h
	PSHUFHW xmm1, xmm1, 0A4h

	
	PUNPCKLWD xmm0, xmm1	
	PSRLDQ xmm1, 2	
	PSHUFHW xmm1, xmm1, 0A4h




	PADDW xmm2, [rcx]
	PADDW xmm3, [rcx+16]
	PADDW xmm4, [rcx+32]
	PADDW xmm5, [rcx+48]
	PADDW xmm6, [rcx+64]
	PADDW xmm7, [rcx+80]
	PADDW xmm0, [rcx+96]
	PADDW xmm1, [rcx+112]

	MOVDQA [rcx], xmm2
	MOVDQA [rcx+16], xmm3
	MOVDQA [rcx+32], xmm4
	MOVDQA [rcx+48], xmm5
	MOVDQA [rcx+64], xmm6
	MOVDQA [rcx+80], xmm7
	MOVDQA [rcx+96], xmm0
	MOVDQA [rcx+112], xmm1




	RET
H264_HUpIPred8x8	ENDP




END
