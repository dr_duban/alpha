/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"


unsigned char Video::H264::CAVLC::GetRefIdx(DecoderState & d_cfg, unsigned int range) {
	if (d_cfg.flag_set & FLAG_AFRO_FRAME) range = (range<<1) + 1;

	if (range == 1) {
		return (d_cfg.NextBit() ^ 1);

	} else {
		return H264_DS_GolombUnsigned(&d_cfg);
	}

}

void Video::H264::CAVLC::GetCoeffLevels(DecoderState & d_cfg, short * levs, int token) {
	int lpref(0), lsufx(0), zcount((token>>5) & 3), coco(token>>8);
	unsigned int runvals[16], tval(0), slength(0);

	token &= 0x1F;

	runvals[0] = runvals[1] = runvals[2] = runvals[3] = runvals[4] = runvals[5] = runvals[6] = runvals[7] = runvals[8] = runvals[9] = runvals[10] = runvals[11] = runvals[12] = runvals[13] = runvals[14] = runvals[15] = 0;
		

	if (token) {
		if ((token > 10) && (zcount < 3)) {
			slength = 1;
		} else {
			slength = 0;
		}

		token = coco - token;
		for (int i(15);i>=token;i--) {
			if (i<zcount) {
				if (d_cfg.NextBit()) levs[i] = -1;
				else levs[i] = 1;
			} else {
				lpref = H264_DS_GetZeroRun(&d_cfg, 64);
								
				tval = slength;

				if (lpref < 15) {
					levs[i] = lpref << tval;
				} else {					
					levs[i] = 15 << tval;
					if (slength == 0) levs[i] += 15;
					tval = lpref - 3;

					if (lpref > 15) levs[i] += ((1<<tval) - 4096);					
				}

				if (lpref == 14) tval = 4;
								
				if (tval) levs[i] += H264_DS_GetNumber(&d_cfg, tval);
				
				if ((i == zcount) && (zcount <3)) levs[i] += 2;

				if (levs[i] & 1) {
					levs[i] = -levs[i] - 1;
				} else {
					levs[i] += 2;
				}

				levs[i] >>= 1;
				tval = (levs[i]<0)?-levs[i]:levs[i];
				tval <<= 1;

				if (slength == 0) slength = 1;
				
				if (tval > ((unsigned int)3<<slength)) slength++;
			}
		}

		tval = 0;
		if (token) tval = GetTotalZeros(d_cfg, coco-token);
		
		for (int i(15);i>token;i--) {
			if (tval) runvals[i] = GetRunBefore(d_cfg, tval);

			tval -= runvals[i];
		}

		runvals[token] = tval;

		tval = 0;
		for (unsigned int i(token);i<16;i++) {
			tval += runvals[i];
			if (tval >= i) break;

			levs[tval] = levs[i];
			levs[i] = 0;
			
			tval++;
		}

	}

}

void Video::H264::CAVLC::GetCoeffLevelsDC(DecoderState & d_cfg, short * levs, int token) {
	int lpref(0), lsufx(0), zcount((token>>5) & 3);
	unsigned int runvals[16], tval(0), slength(0);

	token &= 0x1F;

	runvals[0] = runvals[1] = runvals[2] = runvals[3] = runvals[4] = runvals[5] = runvals[6] = runvals[7] = runvals[8] = runvals[9] = runvals[10] = runvals[11] = runvals[12] = runvals[13] = runvals[14] = runvals[15] = 0;
		

	if (token) {
		if ((token > 10) && (zcount < 3)) {
			slength = 1;
		} else {
			slength = 0;
		}

		token = 16 - token;
		for (int i(15);i>=token;i--) {
			if (i<zcount) {
				if (d_cfg.NextBit()) levs[i] = -1;
				else levs[i] = 1;
			} else {
				lpref = H264_DS_GetZeroRun(&d_cfg, 64);
								
				tval = slength;

				if (lpref < 15) {
					levs[i] = lpref << tval;
				} else {					
					levs[i] = 15 << tval;
					if (slength == 0) levs[i] += 15;
					tval = lpref - 3;

					if (lpref > 15) levs[i] += ((1<<tval) - 4096);					
				}

				if (lpref == 14) tval = 4;
								
				if (tval) levs[i] += H264_DS_GetNumber(&d_cfg, tval);
				
				if ((i == zcount) && (zcount <3)) levs[i] += 2;

				if (levs[i] & 1) {
					levs[i] = -levs[i] - 1;
				} else {
					levs[i] += 2;
				}

				levs[i] >>= 1;
				tval = (levs[i]<0)?-levs[i]:levs[i];
				tval <<= 1;

				if (slength == 0) slength = 1;
				
				if (tval > ((unsigned int)3<<slength)) slength++;
			}
		}

		tval = 0;
		if (token) tval = GetTotalZeros(d_cfg, 16-token);
		
		for (int i(15);i>token;i--) {
			if (tval) runvals[i] = GetRunBefore(d_cfg, tval);

			tval -= runvals[i];
		}

		runvals[token] = tval;

		tval = 0;
		for (unsigned int i(token);i<16;i++) {
			tval += runvals[i];
			if (tval >= i) break;

			levs[tval] = levs[i];
			levs[i] = 0;
			
			tval++;
		}

	}

}

void Video::H264::CAVLC::GetCoeffLevelsDC1(DecoderState & d_cfg, short * levs, int token) {
	int lpref(0), lsufx(0), zcount((token>>5) & 3);
	unsigned int runvals[4], tval(0), slength(0);

	token &= 0x1F;

	runvals[0] = runvals[1] = runvals[2] = runvals[3] = 0;
		

	if (token) {
		if ((token > 10) && (zcount < 3)) {
			slength = 1;
		} else {
			slength = 0;
		}

		token = 4 - token;
		for (int i(3);i>=token;i--) {
			if (i<zcount) {
				if (d_cfg.NextBit()) levs[i] = -1;
				else levs[i] = 1;
			} else {
				lpref = H264_DS_GetZeroRun(&d_cfg, 64);
								
				tval = slength;

				if (lpref < 15) {
					levs[i] = lpref << tval;
				} else {					
					levs[i] = 15 << tval;
					if (slength == 0) levs[i] += 15;
					tval = lpref - 3;

					if (lpref > 15) levs[i] += ((1<<tval) - 4096);					
				}

				if (lpref == 14) tval = 4;
								
				if (tval) levs[i] += H264_DS_GetNumber(&d_cfg, tval);
				
				if ((i == zcount) && (zcount < 3)) levs[i] += 2;

				if (levs[i] & 1) {
					levs[i] = -levs[i] - 1;
				} else {
					levs[i] += 2;
				}

				levs[i] >>= 1;
				tval = (levs[i]<0)?-levs[i]:levs[i];
				tval <<= 1;

				if (slength == 0) slength = 1;
				
				if (tval > ((unsigned int)3<<slength)) slength++;
			}
		}

		tval = 0;
		if (token) tval = GetTotalZerosDC1(d_cfg, 4-token);
		
		for (int i(3);i>token;i--) {
			if (tval) runvals[i] = GetRunBefore(d_cfg, tval);

			tval -= runvals[i];
		}

		runvals[token] = tval;

		tval = 0;
		for (unsigned int i(token);i<4;i++) {
			tval += runvals[i];
			if (tval >= i) break;

			levs[tval] = levs[i];
			levs[i] = 0;
			
			tval++;
		}

	}

}


void Video::H264::CAVLC::GetCoeffLevelsDC2(DecoderState & d_cfg, short * levs, int token) {
	int lpref(0), lsufx(0), zcount((token>>5) & 3);
	unsigned int runvals[8], tval(0), slength(0);

	token &= 0x1F;

	runvals[0] = runvals[1] = runvals[2] = runvals[3] = runvals[4] = runvals[5] = runvals[6] = runvals[7] = 0;
		

	if (token) {
		if ((token > 10) && (zcount < 3)) {
			slength = 1;
		} else {
			slength = 0;
		}

		token = 8 - token;
		for (int i(7);i>=token;i--) {
			if (i<zcount) {
				if (d_cfg.NextBit()) levs[i] = -1;
				else levs[i] = 1;
			} else {
				lpref = H264_DS_GetZeroRun(&d_cfg, 64);
								
				tval = slength;

				if (lpref < 15) {
					levs[i] = lpref << tval;
				} else {					
					levs[i] = 15 << tval;
					if (slength == 0) levs[i] += 15;
					tval = lpref - 3;

					if (lpref > 15) levs[i] += ((1<<tval) - 4096);					
				}

				if (lpref == 14) tval = 4;
								
				if (tval) levs[i] += H264_DS_GetNumber(&d_cfg, tval);
				
				if ((i == zcount) && (zcount < 3)) levs[i] += 2;

				if (levs[i] & 1) {
					levs[i] = -levs[i] - 1;
				} else {
					levs[i] += 2;
				}

				levs[i] >>= 1;
				tval = (levs[i]<0)?-levs[i]:levs[i];
				tval <<= 1;

				if (slength == 0) slength = 1;
				
				if (tval > ((unsigned int)3<<slength)) slength++;
			}
		}

		tval = 0;
		if (token) tval = GetTotalZerosDC2(d_cfg, 8-token);
		
		for (int i(7);i>token;i--) {
			if (tval) runvals[i] = GetRunBefore(d_cfg, tval);

			tval -= runvals[i];
		}

		runvals[token] = tval;

		tval = 0;
		for (unsigned int i(token);i<8;i++) {
			tval += runvals[i];
			if (tval >= i) break;

			levs[tval] = levs[i];
			levs[i] = 0;
			
			tval++;
		}

	}

}

unsigned char Video::H264::CAVLC::GetCoeffToken0(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0);

	prefix = H264_DS_GetZeroRun(&d_cfg, 16);
	
	if (prefix<16)
	switch (token_category_tab[0][prefix][0]) {
		case 0:
			result = token_val_tab[0][token_category_tab[0][prefix][1]];
		break;
		case 1:
			if (d_cfg.NextBit()) {
				result = token_val_tab[0][token_category_tab[0][prefix][1]+2];
			} else {
				result = token_val_tab[0][token_category_tab[0][prefix][1]+d_cfg.NextBit()];
			}
		break;
		case 2:
			result = token_val_tab[0][token_category_tab[0][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
		break;
		case 3:
			result = token_val_tab[0][token_category_tab[0][prefix][1]+H264_DS_GetNumber(&d_cfg, 3)];
		break;
	}

	return result;
}

unsigned char Video::H264::CAVLC::GetCoeffToken1(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0);

	prefix = H264_DS_GetZeroRun(&d_cfg, 16);
	
	if (prefix<16)
	switch (token_category_tab[1][prefix][0]) {
		case 0:
			result = token_val_tab[1][token_category_tab[1][prefix][1]+d_cfg.NextBit()];
		break;
		case 1:
			if (d_cfg.NextBit()) {
				result = token_val_tab[1][token_category_tab[1][prefix][1]+2];
			} else {
				result = token_val_tab[1][token_category_tab[1][prefix][1]+d_cfg.NextBit()];
			}
		break;
		case 2:
			if (d_cfg.NextBit()) {
				result = token_val_tab[1][token_category_tab[1][prefix][1]+d_cfg.NextBit()+4];
			} else {
				result = token_val_tab[1][token_category_tab[1][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
			}
		break;
		case 3:
			result = token_val_tab[1][token_category_tab[1][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
		break;
		case 4:
			result = token_val_tab[1][token_category_tab[1][prefix][1]+H264_DS_GetNumber(&d_cfg, 3)];
		break;
		case 5:
			result = token_val_tab[1][token_category_tab[1][prefix][1]];
		break;
	}

	return result;
}

unsigned char Video::H264::CAVLC::GetCoeffToken2(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0);

	prefix = H264_DS_GetZeroRun(&d_cfg, 12);
	
	if (prefix<16)
	switch (token_category_tab[2][prefix][0]) {
		case 0:
			result = token_val_tab[2][token_category_tab[2][prefix][1]+H264_DS_GetNumber(&d_cfg, 3)];
		break;
		case 1:
			if (d_cfg.NextBit()) {
				if (d_cfg.NextBit()) {
					token_val_tab[2][token_category_tab[2][prefix][1]+6];
				} else {
					result = token_val_tab[2][token_category_tab[2][prefix][1]+d_cfg.NextBit()+4];
				}
			} else {
				result = token_val_tab[2][token_category_tab[2][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
			}
		break;
		case 2:
			result = token_val_tab[2][token_category_tab[2][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
		break;
		case 3:
			result = token_val_tab[2][token_category_tab[2][prefix][1]+d_cfg.NextBit()];
		break;
		case 4:
			result = token_val_tab[2][token_category_tab[2][prefix][1]];
		break;
	}

	return result;
}

unsigned char Video::H264::CAVLC::GetCoeffToken3(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0);

	prefix = H264_DS_GetNumber(&d_cfg, 4);
	
	switch (token_category_tab[3][prefix][0]) {
		case 0:
			result = H264_DS_GetNumber(&d_cfg, 2);
			if (result == 3) result = token_val_tab[3][token_category_tab[3][prefix][1]+2];
			else result = token_val_tab[3][token_category_tab[3][prefix][1]+result];
		break;
		case 1:
		case 2:
			result = token_val_tab[3][token_category_tab[3][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
		break;
	}

	return result;
}

unsigned char Video::H264::CAVLC::GetCoeffToken4(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0), coco(0);

	for (;result<9;result++) {
		prefix <<= 1;
		prefix |= d_cfg.NextBit();
	
		for (unsigned int j(0);j<token_category_tab[4][result][0];j++, coco++) {
			if (prefix == token_val_tab[4][2*coco]) {
				return token_val_tab[4][2*coco+1];
			}			
		}
	}

	return result;
}


unsigned char Video::H264::CAVLC::GetCoeffToken5(DecoderState & d_cfg) {
	unsigned char result(0), prefix(0);

	prefix = H264_DS_GetZeroRun(&d_cfg, 12);
	
	if (prefix<16)
	switch (token_category_tab[5][prefix][0]) {
		case 0:
			result = token_val_tab[5][token_category_tab[5][prefix][1]];
		break;
		case 1:
			result = token_val_tab[5][token_category_tab[5][prefix][1]+H264_DS_GetNumber(&d_cfg, 3)];
		break;
		case 2:
			result = token_val_tab[5][token_category_tab[5][prefix][1]+H264_DS_GetNumber(&d_cfg, 2)];
		break;
		case 3:
			if (H264_DS_GetNumber(&d_cfg, 2) == 3) {
				result = token_val_tab[5][token_category_tab[5][prefix][1]];
			}
		break;
	}

	return result;
}


unsigned char Video::H264::CAVLC::GetRunBefore(DecoderState & d_cfg, unsigned int zleft) {
	unsigned char result(0);

	switch (zleft) {
		case 1:
			result = d_cfg.NextBit() ^ 1;
		break;
		case 2:
			result = H264_DS_GetZeroRun(&d_cfg, 2);
		break;
		case 3:
			result = H264_DS_GetNumber(&d_cfg, 2) ^ 3;
		break;
		case 4:
			if (result = H264_DS_GetNumber(&d_cfg, 2)) {
				result ^= 3;
			} else {
				result = 4 - d_cfg.NextBit();
			}
			
		break;
		case 5:
			if (d_cfg.NextBit()) {
				result = d_cfg.NextBit() ^ 1;
			} else {
				result = 5 - H264_DS_GetNumber(&d_cfg, 2);

			}
		break;
		case 6:
			switch (result = H264_DS_GetNumber(&d_cfg, 2)) {
				case 3:
					result = 0;
				break;
				case 0:
					result += d_cfg.NextBit() + 1;
				break;
				default:
					result <<= 1;
					result += (2 - d_cfg.NextBit());
			}

		break;
		default:
			if (result = H264_DS_GetNumber(&d_cfg, 3)) {
				result ^= 7;
			} else {
				result = 7 + H264_DS_GetZeroRun(&d_cfg, 12);
			}

			
	}


	return result;
}

unsigned char Video::H264::CAVLC::GetTotalZeros(DecoderState & d_cfg, unsigned int tzvidx) {
	unsigned char result(0);

	switch (tzvidx) {
		case 1:
			if (result = H264_DS_GetZeroRun(&d_cfg, 12)) {
				result <<= 1;
				if (result < 16) result -= d_cfg.NextBit();
				else result--;
			}
		break;
		case 2:
			result = H264_DS_GetNumber(&d_cfg, 3);

			switch (result) {
				case 0:
					if (d_cfg.NextBit()) {
						result = 10 - d_cfg.NextBit();
					} else {
						result = 14 - H264_DS_GetNumber(&d_cfg, 2);
					}
				break;
				case 1:
					result = 8 - d_cfg.NextBit();
				break;
				case 2:
					result = 6 - d_cfg.NextBit();
				break;
				default:
					result ^= 7;

			}
		break;
		case 3:
			result = H264_DS_GetNumber(&d_cfg, 3);

			switch (result) {
				case 0:
					if (result = H264_DS_GetNumber(&d_cfg, 2)) {
						if (result == 1) {
							result = 12;
						} else {
							result = 12 - result;
						}
					} else {
						result = 13 - (d_cfg.NextBit() << 1);
					}

				break;
				case 1:
					if (d_cfg.NextBit()) result = 5;
					else result = 8;
				break;
				case 2:
					if (d_cfg.NextBit()) result = 0;
					else result = 4;
				break;
				default:
					result ^= 7;

					if (result < 3) result += 1;
					else result += 3;
			}

		break;
		case 4:
			result = zeros_map[1][H264_DS_GetNumber(&d_cfg, 3)];

			switch (result) {
				case 12:
					if (result = (H264_DS_GetNumber(&d_cfg, 2) ^ 3)) {
						result += 9;
					}
				break;
				case 9:
					result -= (d_cfg.NextBit() << 1);
				break;
				case 3:
					result -= d_cfg.NextBit();
				break;

			}
		break;
		case 5:
			result = zeros_map[1][H264_DS_GetNumber(&d_cfg, 3)];

			switch (result) {
				case 10:
					if (d_cfg.NextBit() == 0) {
						result = 11 - (d_cfg.NextBit() << 1);
					}
				break;
				case 8:
					if (d_cfg.NextBit()) result = 2;
				break;
				case 1:
					result -= d_cfg.NextBit();
				break;

			}
		break;
		case 6:
			result = zeros_map[2][H264_DS_GetNumber(&d_cfg, 3)];

			if (result == 0) {
				if (d_cfg.NextBit()) {
					result = 8;
				} else {
					if (d_cfg.NextBit()) {
						result = 1;
					} else {
						if (d_cfg.NextBit()) {
							result = 0;
						} else {
							result = 10;
						}
					}
				}
			}
		break;
		case 7:
			result = zeros_map[3][H264_DS_GetZeroRun(&d_cfg, 6)];
			switch (result) {
				case 5:
					if (d_cfg.NextBit() == 0) {
						result -= (d_cfg.NextBit()+2);
					}
				break;
				case 6:
					result -= (d_cfg.NextBit() << 1);
				break;
			}
		break;

		case 8:
			result = zeros_map[4][H264_DS_GetZeroRun(&d_cfg, 6)];
			switch (result) {
				case 6:
					result <<= d_cfg.NextBit();
				break;
				case 5:
					result -= d_cfg.NextBit();
				break;
			}
			
		break;
		case 9:
			result = zeros_map[5][H264_DS_GetZeroRun(&d_cfg, 6)];
			if (result == 4)
				result -= d_cfg.NextBit();
		break;
		case 10:
			result = zeros_map[6][H264_DS_GetZeroRun(&d_cfg, 5)];
			if (result == 4)
				result -= d_cfg.NextBit();
			
		break;
		case 11:
			result = zeros_map[7][H264_DS_GetZeroRun(&d_cfg, 4)];
			if (result == 3)
				result += (d_cfg.NextBit() << 1);
		break;
		case 12:
			result = zeros_map[8][H264_DS_GetZeroRun(&d_cfg, 4)];
		break;
		case 13:
			result = zeros_map[9][H264_DS_GetZeroRun(&d_cfg, 3)];
		break;
		case 14:
			result = 2 - H264_DS_GetZeroRun(&d_cfg, 2);
		break;
		case 15:
			result = d_cfg.NextBit();
		break;



	}

	return result;

}

unsigned char Video::H264::CAVLC::GetTotalZerosDC1(DecoderState & d_cfg, unsigned int tzvidx) {
	unsigned char result(0);

	if (tzvidx == 3) {
		result = d_cfg.NextBit() ^ 1;
	} else {
		result = H264_DS_GetZeroRun(&d_cfg, 4-tzvidx);
	}

	return result;
}

unsigned char Video::H264::CAVLC::GetTotalZerosDC2(DecoderState & d_cfg, unsigned int tzvidx) {
	unsigned char result(0);

	switch (tzvidx) {
		case 1:
			if (result = H264_DS_GetZeroRun(&d_cfg, 5))			
			switch (result) {
				case 1:
					result += d_cfg.NextBit();
				break;
				case 2:
					result += d_cfg.NextBit()+1;
				break;
				default:
					result += 2;

			}
		break;
		case 2:
			if (d_cfg.NextBit()) {
				result = 3 + H264_DS_GetNumber(&d_cfg, 2);
			} else {
				if (d_cfg.NextBit()) {
					result = 1;
				} else {
					result = d_cfg.NextBit() << 1;
				}
			}
		break;
		case 3:
			result = H264_DS_GetNumber(&d_cfg, 2);

			switch (result) {
				case 0:
					result = d_cfg.NextBit();
				break;
				case 1: case 2:
					result += 1;
				break;
				case 3:
					result = 4 + d_cfg.NextBit();
				break;
			}
		break;
		case 4:
			result = H264_DS_GetNumber(&d_cfg, 2);

			if (result == 3) {
				if (d_cfg.NextBit()) result = 4;
				else result = 0;
			} else {
				result++;
			}
		break;
		case 5:
			result = H264_DS_GetNumber(&d_cfg, 2);
		break;
		case 6:
			result = 2 - H264_DS_GetZeroRun(&d_cfg, 2);
		break;
		case 7:
			result = d_cfg.NextBit();
		break;

	}

	return result;
}

// ===================================================================================================================================================================================
const unsigned char Video::H264::CAVLC::token_category_tab[6][16][2] = { // category/byte offset
	{{0,0},	{0,1},	{0,2},	{1,4},	{1,7},	{2,10},	{2,14},		{2,18},	{2,22},	{3,30},	{3,38},	{3,46},	{3,54},	{2,26},	{0, 3}, {0,0}},
	{{0,0},	{1,2},	{2,5},	{3,17},	{3,21},	{3,25},	{3,29},		{4,37},	{4,45},	{4,53},	{2,11},	{3,33},	{5,61}, {0,0},	{0,0},	{0,0}},
	{{0,0},	{0,8},	{0,16},	{0,24},	{0,32},	{0,40},	{1, 48},	{2,55},	{3,59},	{4,61},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0}},
	{{0,0},	{1,3},	{2,6},	{2,10},	{2,14},	{2,18},	{2,22},		{2,26},	{2,30},	{2,34},	{2,38},	{2,42},	{2,46},	{2,50},	{2,54},	{2,58}},
	{{1,0},	{1,1},	{1,2},	{0,0},	{0,0},	{6,3}, {3,9},		{2,12},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0}},
	{{0,0},	{0,1},	{0,2},	{1,5},	{0,3},	{0,4},	{2,13},		{2,17},	{2,21},	{2,25},	{3,29},	{0,0},	{0,0},	{0,0},	{0,0},	{0,0}}
};



const unsigned char Video::H264::CAVLC::token_val_tab[6][64] = {
	0x00, 0x21, 0x42, 0x2D,	0x22, 0x01, 0x63, 0x65,		0x43, 0x64,	0x66, 0x44, 0x23, 0x02, 0x67, 0x45,		0x24, 0x03, 0x68, 0x46, 0x25, 0x04, 0x69, 0x47,		0x26, 0x05, 0x10, 0x50, 0x30, 0x0F, 0x08, 0x49,		0x28, 0x07, 0x6A, 0x48, 0x27, 0x06, 0x6C, 0x4B,		0x2A, 0x0A, 0x6B, 0x4A, 0x29, 0x09, 0x6E, 0x4D,		0x2C, 0x0C, 0x6D, 0x4C, 0x2B, 0x0B, 0x70, 0x4F,		0x2F, 0x0E, 0x6F, 0x4E, 0x2E, 0x0D, 0x00, 0x00,
	0x21, 0x00, 0x64, 0x63, 0x42, 0x66, 0x43, 0x23,		0x01, 0x65, 0x22, 0x2F, 0x0F, 0x4F, 0x2E, 0x4E,		0x0E, 0x67, 0x44, 0x24, 0x02, 0x68, 0x45, 0x25,		0x03, 0x05, 0x46, 0x26, 0x04, 0x69, 0x47, 0x27,		0x06, 0x70, 0x50, 0x30, 0x10, 0x6B, 0x49, 0x29,		0x08, 0x6A, 0x48, 0x28, 0x07, 0x0B, 0x4B, 0x2B,		0x0A, 0x6C, 0x4A, 0x2A, 0x09, 0x6E, 0x4D, 0x2D,		0x0D, 0x6D, 0x4C, 0x2C, 0x0C, 0x6F, 0x00, 0x00,
	0x67, 0x66, 0x65, 0x64, 0x63, 0x42, 0x21, 0x00,		0x25, 0x45, 0x24, 0x44, 0x23, 0x68, 0x43, 0x22,		0x03, 0x47, 0x27, 0x02, 0x69, 0x46, 0x26, 0x01,		0x07, 0x06, 0x49, 0x05, 0x6A, 0x48, 0x28, 0x04,		0x6C, 0x4B, 0x2A, 0x09, 0x6B, 0x4A, 0x29, 0x08,		0x0C, 0x4D, 0x2C, 0x0B, 0x6D, 0x4C, 0x2B, 0x0A,		0x2F, 0x0E, 0x6E, 0x4E, 0x2E, 0x0D, 0x2D, 0x30,		0x0F, 0x6F, 0x4F, 0x70, 0x50, 0x10, 0x00, 0x00,
	0x01, 0x21, 0x00, 0x02, 0x22, 0x42, 0x03, 0x23,		0x43, 0x63, 0x04, 0x24, 0x44, 0x64, 0x05, 0x25,		0x45, 0x65, 0x06, 0x26, 0x46, 0x66, 0x07, 0x27,		0x47, 0x67, 0x08, 0x28, 0x48, 0x68, 0x09, 0x29,		0x49, 0x69, 0x0A, 0x2A, 0x4A, 0x6A, 0x0B, 0x2B,		0x4B, 0x6B, 0x0C, 0x2C, 0x4C, 0x6C, 0x0D, 0x2D,		0x4D, 0x6D, 0x0E, 0x2E, 0x4E, 0x6E, 0x0F, 0x2F,		0x4F, 0x6F, 0x10, 0x30, 0x50, 0x70, 0x00, 0x00,
	0x01, 0x21, 0x01, 0x00, 0x01, 0x42, 0x02, 0x04,		0x03, 0x03, 0x04, 0x02, 0x05, 0x63, 0x06, 0x22,		0x07, 0x01, 0x00, 0x64, 0x02, 0x43, 0x03, 0x23,		0x02, 0x44, 0x03, 0x24, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x21, 0x42, 0x63, 0x64, 0x66, 0x65, 0x44,		0x43, 0x23, 0x22, 0x02, 0x01, 0x45, 0x24, 0x04,		0x03, 0x67, 0x46, 0x25, 0x05, 0x68, 0x47, 0x25,		0x05, 0x68, 0x47, 0x26, 0x06, 0x48, 0x28, 0x27,		0x07, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

};

const unsigned char Video::H264::CAVLC::zeros_map[10][8] = {
	12, 9, 3, 8, 6, 5, 4, 1,
	10, 8, 1, 7, 6, 5, 4, 3,
	0, 9, 7, 6, 5, 4, 3, 2,
	5, 6, 8, 7, 1, 0, 9, 0,
	5, 6, 7, 1, 2, 0, 8, 0,
	4, 6, 5, 2, 7, 0, 1, 0,
	4, 5, 2, 6, 0, 1, 0, 0,
	4, 3, 2, 1, 0, 0, 0, 0,
	3, 2, 4, 1, 0, 0, 0, 0,
	2, 3, 1, 0, 0, 0, 0, 0
};

/*
0prefix
	0		(0,0)
	1		(1,1)
	2		(2,2)
	14		(1,13)

	3		1(3,3), 00(1,2), 01(0,1)
	4		1(3,4), 00(3,5), 01(2,3)

	5		00(3,6), 01(2,4), 10(1,3), 11(0,2)
	6		00(3,7), 01(2,5), 10(1,4), 11(0,3)
	7		00(3,8), 01(2,6), 10(1,5), 11(0,4)
	8		00(3,9), 01(2,7), 10(1,6), 11(0,5)
	13		00(0,16), 01(2,16), 10(1,16), 11(0,15)

	9		000(0,8), 001(2,9), 010(1,8), 011(0,7), 100(3,10), 101(2,8), 110(1,7), 111(0,6)
	10		000(3,12), 001(2,11), 010(1,10), 011(0,10), 100(3,11), 101(2,10), 110(1,9), 111(0,9)
	11		000(3,14), 001(2,13), 010(1,12), 011(0,12), 100(3,13), 101(2,12), 110(1,11), 111(0,11)
	12		000(3,16), 001(2,15), 010(1,15), 011(0,14), 100(3,15), 101(2,14), 110(1,14), 111(0,13)

	
	

====================================================
0prefix
	0		0(1,1), 1(0,0)

	1		1(2,2), 00(3,4), 01(3,3)
	
	2		10(3,5), 11(1,2), 000(3,6), 001(2,3), 010(1,3), 011(0,1)
	10		10(2,14), 11(0,14), 000(1,15), 001(0,15), 010(2,15), 011(1,14)

	3		00(3,7), 01(2,4), 10(1,4), 11(0,2)
	4		00(3,8), 01(2,5), 10(1,5), 11(0,3)
	5		00(0,5), 01(2,6), 10(1,6), 11(0,4)
	6		00(3,9), 01(2,7), 10(1,7), 11(0,6)
	11		00(3,16), 01(2,16), 10(1,16), 11(0,16)

	7		000(3,11), 001(2,9), 010(1,9), 011(0,8), 100(3,10), 101(2,8), 110(1,8), 111(0,7)
	8		000(0,11), 001(2,11), 010(1,11), 011(0,10), 100(3,12), 101(2,10), 110(1,10), 111(0,9)
	9		000(3,14), 001(2,13), 010(1,13), 011(0,13), 100(3,13), 101(2,12), 110(1,12), 111(0,12)
	
	12		(3,15)

========================================================
0prefix
	0		000(3,7), 001(3,6), 010(3,5), 011(3,4), 100(3,3), 101(2,2), 110(1,1), 111(0,0)
	1		000(1,5), 001(2,5), 010(1,4), 011(2,4), 100(1,3), 101(3,8), 110(2,3), 111(1,2)
	2		000(0,3), 001(2,7), 010(1,7), 011(0,2), 100(3,9), 101(2,6), 110(1,6), 111(0,1)
	3		000(0,7), 001(0,6), 010(2,9), 011(0,5), 100(3,10), 101(2,8), 110(1,8), 111(0,4)
	4		000(3,12), 001(2,11), 010(1,10), 011(0,9), 100(3,11), 101(2,10), 110(1,9), 111(0,8)
	5		000(0,12), 001(2,13), 010(1,12), 011(0,11), 100(3,13), 101(2,12), 110(1,11), 111(0,10)
	
	6		11(1,13), 000(1,15), 001(0,14), 010(3,14), 011(2,14), 100(1,14), 101(0,13)
	
	7		00(1,16), 01(0,15), 10(3,15), 11(2,15)

	8		0(3,16), 1(2,16)
	
	9		(0,16)

=========================================================

fixed length code 4:2

	0000	00(0,1), 01(1,1), 11(0,0)

	0001	00(0,2), 01(1,2), 10(2,2)

	0010	00(0,3), 01(1,3), 10(2,3), 11(3,3)
	0011	00(0,4), 01(1,4), 10(2,4), 11(3,4)
	........


============================================================

length
	1		1(1,1)
	2		01(0,0)
	3		001(2,2)

	6		000010(0,4), 000011(0,3), 000100(0,2), 000101(3,3), 000110(1,2), 000111(0,1)
	7		0000000(3,4), 0000010(2,3), 0000011(1,3)
	8		00000010(2,4), 00000011(1,4)

================================================================
0prefix

	0		(0,0)
	1		(1,1)
	2		(2,2)
	4		(3,3)
	5		(3,4)

	3		000(3,6), 001(3,5), 010(2,4), 011(2,3), 100(1,3), 101(1,2), 110(0,2), 111(0,1)

	6		00(2,5), 01(1,4), 10(0,4), 11(0,3), 
	7		00(3,7), 01(2,6), 10(1,5), 11(0,5)
	8		00(3,8), 01(2,7), 10(1,6), 11(0,6)
	9		00(2,8), 01(1,8), 10(1,7), 11(0,7)

	10		11(0,8)

*/


