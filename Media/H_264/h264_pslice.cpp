/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"
#include "System\SysUtils.h"



void Video::H264::MVPredP() {
	InterPredCfg ip_cfg[4];
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);
	
	
	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
		case FLAG_MB_PART_V:
// part 0
			FormatICfg(ip_cfg, 0, 5);

			mvc[0].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
			
			if (decoder_cfg.mb_x->field_decoding_flag == ip_cfg[1].mbi->field_decoding_flag) {
				if (mvc[0].ref_idx[0] == reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0]) {
					mvc[0].mvd[0][0][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][0];
					mvc[0].mvd[0][0][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][1];
				} else {				
					MVMedianPred(mvc[0], ip_cfg, 0, 0);
				}
			} else {
				if (mvc[0].ref_idx[0] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0], ip_cfg[1].mbi->field_decoding_flag)) {
					mvc[0].mvd[0][0][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][0], ip_cfg[1].mbi->field_decoding_flag);
					mvc[0].mvd[0][0][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][1], ip_cfg[1].mbi->field_decoding_flag);
				} else {
					MVMedianPred(mvc[0], ip_cfg, 0, 0);
				}
			}

			mvc[0].mvd[0][0][0] += decoder_cfg.mb_x->sub[0].mvd[0][0][0];
			mvc[0].mvd[0][0][1] += decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			
			mvc[0].mvd[0][1][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][1][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][2][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][2][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][3][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][3][1] = mvc[0].mvd[0][0][1];

			mvc[1] = mvc[0];


// part 1
			FormatICfg(ip_cfg, 8, 13);

			mvc[2].ref_idx[0] = decoder_cfg.mb_x->sub[2].ref_idx[0];
			
			if (decoder_cfg.mb_x->field_decoding_flag == ip_cfg[0].mbi->field_decoding_flag) {
				if (mvc[2].ref_idx[0] == reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0]) {
					mvc[2].mvd[0][0][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][0];
					mvc[2].mvd[0][0][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][1];
				} else {
					MVMedianPred(mvc[2], ip_cfg, 0, 0);
				}
			} else {
				if (mvc[2].ref_idx[0] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0], ip_cfg[0].mbi->field_decoding_flag)) {
					mvc[2].mvd[0][0][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][0], ip_cfg[0].mbi->field_decoding_flag);
					mvc[2].mvd[0][0][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][1], ip_cfg[0].mbi->field_decoding_flag);
				} else {
					MVMedianPred(mvc[2], ip_cfg, 0, 0);
				}
			}

			mvc[2].mvd[0][0][0] += decoder_cfg.mb_x->sub[2].mvd[0][0][0];
			mvc[2].mvd[0][0][1] += decoder_cfg.mb_x->sub[2].mvd[0][0][1];

			mvc[2].mvd[0][1][0] = mvc[2].mvd[0][0][0];
			mvc[2].mvd[0][1][1] = mvc[2].mvd[0][0][1];
			mvc[2].mvd[0][2][0] = mvc[2].mvd[0][0][0];
			mvc[2].mvd[0][2][1] = mvc[2].mvd[0][0][1];
			mvc[2].mvd[0][3][0] = mvc[2].mvd[0][0][0];
			mvc[2].mvd[0][3][1] = mvc[2].mvd[0][0][1];

			mvc[3] = mvc[2];


		break;
		case FLAG_MB_PART_H:
// part 0
			FormatICfg(ip_cfg, 0, 1);

			mvc[0].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];

			if (decoder_cfg.mb_x->field_decoding_flag == ip_cfg[0].mbi->field_decoding_flag) {
				if (mvc[0].ref_idx[0] == reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0]) {
					mvc[0].mvd[0][0][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][0];
					mvc[0].mvd[0][0][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][1];
				} else {
					MVMedianPred(mvc[0], ip_cfg, 0, 0);
				}
			} else {
				if (mvc[0].ref_idx[0] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0], ip_cfg[0].mbi->field_decoding_flag)) {
					mvc[0].mvd[0][0][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][0], ip_cfg[0].mbi->field_decoding_flag);
					mvc[0].mvd[0][0][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][1], ip_cfg[0].mbi->field_decoding_flag);
				} else {
					MVMedianPred(mvc[0], ip_cfg, 0, 0);
				}
			}

			mvc[0].mvd[0][0][0] += decoder_cfg.mb_x->sub[0].mvd[0][0][0];
			mvc[0].mvd[0][0][1] += decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			

			mvc[0].mvd[0][1][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][1][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][2][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][2][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][3][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][3][1] = mvc[0].mvd[0][0][1];

			mvc[2] = mvc[0];

// part 1
			FormatICfg(ip_cfg, 4, 5);

			mvc[1].ref_idx[0] = decoder_cfg.mb_x->sub[1].ref_idx[0];

			if (decoder_cfg.mb_x->field_decoding_flag == ip_cfg[2].mbi->field_decoding_flag) {
				if (mvc[1].ref_idx[0] == reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0]) {
					mvc[1].mvd[0][0][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].mvd[0][ip_cfg[2].sub_idx][0];
					mvc[1].mvd[0][0][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].mvd[0][ip_cfg[2].sub_idx][1];
				} else {
					MVMedianPred(mvc[1], ip_cfg, 0, 0);
				}
			} else {
				if (mvc[1].ref_idx[0] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0], ip_cfg[2].mbi->field_decoding_flag)) {
					mvc[1].mvd[0][0][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].mvd[0][ip_cfg[2].sub_idx][0], ip_cfg[2].mbi->field_decoding_flag);
					mvc[1].mvd[0][0][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].mvd[0][ip_cfg[2].sub_idx][1], ip_cfg[2].mbi->field_decoding_flag);
				} else {
					MVMedianPred(mvc[1], ip_cfg, 0, 0);
				}
			}

			mvc[1].mvd[0][0][0] += decoder_cfg.mb_x->sub[1].mvd[0][0][0];
			mvc[1].mvd[0][0][1] += decoder_cfg.mb_x->sub[1].mvd[0][0][1];

			mvc[1].mvd[0][1][0] = mvc[1].mvd[0][0][0];
			mvc[1].mvd[0][1][1] = mvc[1].mvd[0][0][1];
			mvc[1].mvd[0][2][0] = mvc[1].mvd[0][0][0];
			mvc[1].mvd[0][2][1] = mvc[1].mvd[0][0][1];
			mvc[1].mvd[0][3][0] = mvc[1].mvd[0][0][0];
			mvc[1].mvd[0][3][1] = mvc[1].mvd[0][0][1];

			mvc[3] = mvc[1];

		break;
		case 0:
			mvc[0].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];

			FormatICfg(ip_cfg, 0, 5);		
			MVMedianPred(mvc[0], ip_cfg, 0, 0);

			mvc[0].mvd[0][0][0] += decoder_cfg.mb_x->sub[0].mvd[0][0][0];
			mvc[0].mvd[0][0][1] += decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			
			mvc[0].mvd[0][1][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][1][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][2][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][2][1] = mvc[0].mvd[0][0][1];
			mvc[0].mvd[0][3][0] = mvc[0].mvd[0][0][0];
			mvc[0].mvd[0][3][1] = mvc[0].mvd[0][0][1];

			mvc[3] = mvc[2] = mvc[1] = mvc[0];

		break;
		case (FLAG_MB_PART_V | FLAG_MB_PART_H): // P_8x8
			MVPredSubP();

		break;

	}
}


void Video::H264::MVPredSubP() {
	InterPredCfg ip_cfg[4];
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);

	for (unsigned int i(0);i<4;i++) {
		mvc[i].ref_idx[0] = decoder_cfg.mb_x->sub[i].ref_idx[0];

		switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
			case FLAG_SUB_PART_V:

				FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
				MVMedianPred(mvc[i], ip_cfg, 0, 0);
				mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
				mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				mvc[i].mvd[0][1][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][1][1] = mvc[i].mvd[0][0][1];

				FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 3);
				MVMedianPred(mvc[i], ip_cfg, 0, 2);
				mvc[i].mvd[0][2][0] += decoder_cfg.mb_x->sub[i].mvd[0][2][0];
				mvc[i].mvd[0][2][1] += decoder_cfg.mb_x->sub[i].mvd[0][2][1];
				mvc[i].mvd[0][3][0] = mvc[i].mvd[0][2][0];
				mvc[i].mvd[0][3][1] = mvc[i].mvd[0][2][1];

			break;
			case FLAG_SUB_PART_H:
				FormatICfg(ip_cfg, (i<<2), (i<<2));
				MVMedianPred(mvc[i], ip_cfg, 0, 0);
				mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
				mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];				
				mvc[i].mvd[0][2][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][2][1] = mvc[i].mvd[0][0][1];

				FormatICfg(ip_cfg, (i<<2)+1, (i<<2)+1);
				MVMedianPred(mvc[i], ip_cfg, 0, 1);
				mvc[i].mvd[0][1][0] += decoder_cfg.mb_x->sub[i].mvd[0][1][0];
				mvc[i].mvd[0][1][1] += decoder_cfg.mb_x->sub[i].mvd[0][1][1];
				mvc[i].mvd[0][3][0] = mvc[i].mvd[0][1][0];
				mvc[i].mvd[0][3][1] = mvc[i].mvd[0][1][1];

			break;
			case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
				FormatICfg(ip_cfg, (i<<2), (i<<2));
				MVMedianPred(mvc[i], ip_cfg, 0, 0);
				mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
				mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				FormatICfg(ip_cfg, (i<<2) + 1, (i<<2) + 1);
				MVMedianPred(mvc[i], ip_cfg, 0, 1);
				mvc[i].mvd[0][1][0] += decoder_cfg.mb_x->sub[i].mvd[0][1][0];
				mvc[i].mvd[0][1][1] += decoder_cfg.mb_x->sub[i].mvd[0][1][1];
				FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 2);
				MVMedianPred(mvc[i], ip_cfg, 0, 2);
				mvc[i].mvd[0][2][0] += decoder_cfg.mb_x->sub[i].mvd[0][2][0];
				mvc[i].mvd[0][2][1] += decoder_cfg.mb_x->sub[i].mvd[0][2][1];
				FormatICfg(ip_cfg, (i<<2) + 3, (i<<2) + 3);
				MVMedianPred(mvc[i], ip_cfg, 0, 3);
				mvc[i].mvd[0][3][0] += decoder_cfg.mb_x->sub[i].mvd[0][3][0];
				mvc[i].mvd[0][3][1] += decoder_cfg.mb_x->sub[i].mvd[0][3][1];

			break;
			case 0:
				FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
				MVMedianPred(mvc[i], ip_cfg, 0, 0);

				mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
				mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];

				mvc[i].mvd[0][1][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][1][1] = mvc[i].mvd[0][0][1];
				mvc[i].mvd[0][2][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][2][1] = mvc[i].mvd[0][0][1];
				mvc[i].mvd[0][3][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][3][1] = mvc[i].mvd[0][0][1];

			break;

		}
	}
}



unsigned int Video::H264::SkipPredP() {
	unsigned int result(0);
	InterPredCfg ip_cfg[4];
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);

	reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
	reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
	reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
	reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;

	FormatICfg(ip_cfg, 0, 5);		

	result = ((ip_cfg[0].mbi->flag_type & ip_cfg[1].mbi->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
	
	if ((reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0] | reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][0] | reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].mvd[0][ip_cfg[0].sub_idx][1]) == 0) result = 1;
	if ((reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0] | reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][0] | reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].mvd[0][ip_cfg[1].sub_idx][1]) == 0) result = 1;

	if (result == 0) {
		MVMedianPred(mvc[0], ip_cfg, 0, 0);

		mvc[0].mvd[0][1][0] = mvc[0].mvd[0][0][0];
		mvc[0].mvd[0][1][1] = mvc[0].mvd[0][0][1];
		mvc[0].mvd[0][2][0] = mvc[0].mvd[0][0][0];
		mvc[0].mvd[0][2][1] = mvc[0].mvd[0][0][1];
		mvc[0].mvd[0][3][0] = mvc[0].mvd[0][0][0];
		mvc[0].mvd[0][3][1] = mvc[0].mvd[0][0][1];
	
		mvc[3] = mvc[2] = mvc[1] = mvc[0];

	} else {
		if (current_slice.weight_type) result = 0;
	}

	return result;	
}

