
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
ALIGN 16


y_part	WORD	10773,	10773,	10773,	10773,	10773,	10773,	10773,	10773
cc_part	WORD	9070,	9070,	9070,	9070,	9070,	9070,	9070,	9070

pi_shuf	BYTE	0, 4, 1, 5, 2, 6, 3, 7, 8, 12, 9, 13, 10, 14, 11, 15

.CODE


ALIGN 16
H264_PackPreview	PROC
	MOV r10d, [r9 + 64 + 4] ; height
	MOV r11d, [r9 + 64 + 8] ; c_width

	MOV eax, r11d
	
	SHR eax, 4
	SHR r10d, 1

	MOVDQA xmm14, XMMWORD PTR [pi_shuf]
	PXOR xmm15, xmm15

align 16
	pack_loop:
			MOVDQA xmm0, [rdx]
			PSHUFB xmm0, xmm14
			MOVDQA xmm2, [rdx + 16]
			PSHUFB xmm2, xmm14
			MOVDQA xmm4, [rdx + 32]
			PSHUFB xmm4, xmm14
			MOVDQA xmm6, [rdx + 48]
			PSHUFB xmm6, xmm14

			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2
			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6

			PUNPCKLBW xmm0, xmm15
			PUNPCKHBW xmm1, xmm15
			PUNPCKLBW xmm2, xmm15
			PUNPCKHBW xmm3, xmm15
			PUNPCKLBW xmm4, xmm15
			PUNPCKHBW xmm5, xmm15
			PUNPCKLBW xmm6, xmm15
			PUNPCKHBW xmm7, xmm15

			MOVDQA xmm8, [rdx + r11*4]
			PSHUFB xmm8, xmm14
			MOVDQA xmm10, [rdx + r11*4 + 16]
			PSHUFB xmm10, xmm14
			MOVDQA xmm9, xmm8
			MOVDQA xmm11, xmm10

			PUNPCKLBW xmm8, xmm15
			PUNPCKHBW xmm9, xmm15
			PUNPCKLBW xmm10, xmm15
			PUNPCKHBW xmm11, xmm15


			PADDW xmm0, xmm8
			PADDW xmm1, xmm9
			PHADDW xmm0, xmm1
			PSRLW xmm0, 2
						
			PADDW xmm2, xmm10
			PADDW xmm3, xmm11
			PHADDW xmm2, xmm3
			PSRLW xmm2, 2

			MOVDQA xmm8, [rdx + r11*4 + 32]
			PSHUFB xmm8, xmm14
			MOVDQA xmm10, [rdx + r11*4 + 48]
			PSHUFB xmm10, xmm14
			MOVDQA xmm9, xmm8
			MOVDQA xmm11, xmm10

			PUNPCKLBW xmm8, xmm15
			PUNPCKHBW xmm9, xmm15
			PUNPCKLBW xmm10, xmm15
			PUNPCKHBW xmm11, xmm15

			PADDW xmm4, xmm8
			PADDW xmm5, xmm9
			PHADDW xmm4, xmm5
			PSRLW xmm4, 2
						
			PADDW xmm6, xmm10
			PADDW xmm7, xmm11
			PHADDW xmm6, xmm7
			PSRLW xmm6, 2
			

			PACKUSWB xmm0, xmm2
			PACKUSWB xmm4, xmm6

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx + 16], xmm4

			ADD rcx, 32
			ADD rdx, 64

		DEC eax
		JNZ pack_loop

		LEA rdx, [rdx + r11*4]

		
		MOV eax, r11d
		SHR eax, 4

	align 16
		pack_loop_1:
			MOVDQA xmm0, [r8]
			PSHUFB xmm0, xmm14
			MOVDQA xmm2, [r8 + 16]
			PSHUFB xmm2, xmm14
			MOVDQA xmm4, [r8 + 32]
			PSHUFB xmm4, xmm14
			MOVDQA xmm6, [r8 + 48]
			PSHUFB xmm6, xmm14

			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2
			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6

			PUNPCKLBW xmm0, xmm15
			PUNPCKHBW xmm1, xmm15
			PUNPCKLBW xmm2, xmm15
			PUNPCKHBW xmm3, xmm15
			PUNPCKLBW xmm4, xmm15
			PUNPCKHBW xmm5, xmm15
			PUNPCKLBW xmm6, xmm15
			PUNPCKHBW xmm7, xmm15

			MOVDQA xmm8, [r8 + r11*4]
			PSHUFB xmm8, xmm14
			MOVDQA xmm10, [r8 + r11*4 + 16]
			PSHUFB xmm10, xmm14
			MOVDQA xmm9, xmm8
			MOVDQA xmm11, xmm10

			PUNPCKLBW xmm8, xmm15
			PUNPCKHBW xmm9, xmm15
			PUNPCKLBW xmm10, xmm15
			PUNPCKHBW xmm11, xmm15

			PADDW xmm0, xmm8
			PADDW xmm1, xmm9
			PHADDW xmm0, xmm1
			PSRLW xmm0, 2
						
			PADDW xmm2, xmm10
			PADDW xmm3, xmm11
			PHADDW xmm2, xmm3
			PSRLW xmm2, 2
			

			MOVDQA xmm8, [r8 + r11*4 + 32]
			PSHUFB xmm8, xmm14
			MOVDQA xmm10, [r8 + r11*4 + 48]
			PSHUFB xmm10, xmm14
			MOVDQA xmm9, xmm8
			MOVDQA xmm11, xmm10

			PUNPCKLBW xmm8, xmm15
			PUNPCKHBW xmm9, xmm15
			PUNPCKLBW xmm10, xmm15
			PUNPCKHBW xmm11, xmm15

			PADDW xmm4, xmm8
			PADDW xmm5, xmm9
			PHADDW xmm4, xmm5
			PSRLW xmm4, 2
			
			PADDW xmm6, xmm10
			PADDW xmm7, xmm11
			PHADDW xmm6, xmm7
			PSRLW xmm6, 2

			PACKUSWB xmm0, xmm2
			PACKUSWB xmm4, xmm6

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx + 16], xmm4

			ADD rcx, 32
			ADD r8, 64

		DEC eax
		JNZ pack_loop_1

		LEA r8, [r8 + r11*4]

		MOV eax, r11d
		SHR eax, 4


	DEC r10d
	JNZ pack_loop

	SFENCE

	RET
H264_PackPreview	ENDP




ALIGN 16
H264_Convert2Picture_1	PROC	
	PUSH rbx
	PUSH r12

	PUSH r13
	PUSH r14
	PUSH r15

	PCMPEQW xmm9, xmm9

	PCMPEQW xmm10, xmm10
	PSLLW xmm10, 15
	MOVDQA xmm11, xmm10
	PSRLW xmm11, 8
	POR xmm10, xmm11
	
	
	PXOR xmm11, xmm11
	PXOR xmm13, xmm13
	PXOR xmm14, xmm14
	PXOR xmm15, xmm15


	MOVD xmm11, DWORD PTR [rdx + 4] ; luma depth
	MOVD xmm12, DWORD PTR [rdx + 8] ; chroma depth

	CMP DWORD PTR [rdx + 12], 0
	JNZ full_range
		MOV eax, 00100010h
		MOVD xmm13, eax
		PSHUFD xmm13, xmm13, 0

		MOVDQA xmm14, XMMWORD PTR [y_part]
		MOVDQA xmm15, XMMWORD PTR [cc_part]
	full_range:


	MOV r10d, [rdx+24] ; c_width
	MOV eax, [rdx+28] ; c_height

	MOV r15, [rdx + 32] ; second field offset
	SHL r15, 1


	MOV r8d, r10d
	MOV r9d, r10d
	SHL r10, 6 ; picture line offset
		

	LEA rdx, [rdx+2048+512]

	MOV r11, rcx	
	
	MOV r12, rdx
	LEA r13, [rdx + 16]
	MOV r14, 32

	MOV ebx, 0808h

	ADD r15, 32

	CMP r15, 64
	JB c_loop
		SUB r15, 32
		LEA r13, [rdx + r15]

		SHR eax, 1
		MOV ebx, 1010h
		MOV r14, 16

align 16
	c_loop:
				MOVDQA xmm0, [rdx] ; y
				PSUBW xmm0, xmm13
				MOVDQA xmm4, xmm0
				PMULHW xmm4, xmm14
				PADDW xmm0, xmm4
				PSRAW xmm0, xmm11

				MOVDQA xmm1, [rdx + 16] ; y
				PSUBW xmm1, xmm13
				MOVDQA xmm4, xmm1
				PMULHW xmm4, xmm14
				PADDW xmm1, xmm4
				PSRAW xmm1, xmm11

				PACKUSWB xmm0, xmm1
				
			; second line
				MOVDQA xmm2, [rdx + r15] ; y
				PSUBW xmm2, xmm13
				MOVDQA xmm4, xmm2
				PMULHW xmm4, xmm14
				PADDW xmm2, xmm4
				PSRAW xmm2, xmm11

				MOVDQA xmm3, [rdx + r15 + 16] ; y
				PSUBW xmm3, xmm13
				MOVDQA xmm4, xmm3
				PMULHW xmm4, xmm14
				PADDW xmm3, xmm4
				PSRAW xmm3, xmm11

				PACKUSWB xmm2, xmm3
				

				MOVDQA xmm4, [r12 + 512] ; cb
				MOVDQA xmm5, xmm4
				PMULHW xmm5, xmm15
				PADDW xmm4, xmm5
				PSRAW xmm4, xmm12

				PACKSSWB xmm4, xmm4
				PUNPCKLBW xmm4, xmm4
				PXOR xmm4, xmm10

				MOVDQA xmm6, [r12 + 1024] ; cr
				MOVDQA xmm5, xmm6
				PMULHW xmm5, xmm15
				PADDW xmm6, xmm5
				PSRAW xmm6, xmm12

				PACKSSWB xmm6, xmm6
				PUNPCKLBW xmm6, xmm6
				PXOR xmm6, xmm10


				LEA rdx, [rdx + r14*2]
				LEA r12, [r12 + r14]
				XCHG r12, r13

		
				MOVDQA xmm1, xmm0
				PUNPCKLBW xmm0, xmm4
				PUNPCKHBW xmm1, xmm4

				MOVDQA xmm3, xmm2
				PUNPCKLBW xmm2, xmm4
				PUNPCKHBW xmm3, xmm4


				MOVDQA xmm7, xmm6
				PUNPCKLBW xmm6, xmm9
				PUNPCKHBW xmm7, xmm9


				MOVDQA xmm4, xmm0
				MOVDQA xmm5, xmm1

				PUNPCKLWD xmm0, xmm6
				PUNPCKHWD xmm4, xmm6
				PUNPCKLWD xmm1, xmm7
				PUNPCKHWD xmm5, xmm7
				

				MOVNTDQ [r11], xmm0
				MOVNTDQ [r11+16], xmm4
				MOVNTDQ [r11+32], xmm1
				MOVNTDQ [r11+48], xmm5

				LEA r11, [r11 + r10]

				MOVDQA xmm4, xmm2
				MOVDQA xmm5, xmm3

				PUNPCKLWD xmm2, xmm6
				PUNPCKHWD xmm4, xmm6
				PUNPCKLWD xmm3, xmm7
				PUNPCKHWD xmm5, xmm7


				MOVNTDQ [r11], xmm2
				MOVNTDQ [r11+16], xmm4
				MOVNTDQ [r11+32], xmm3
				MOVNTDQ [r11+48], xmm5

				LEA r11, [r11 + r10]
				

			DEC bl
			JNZ c_loop
						

			ADD rcx, 64
			MOV r11, rcx

			MOV bl, bh
			LEA rdx, [rdx + 1536]
			LEA r12, [r12 + 1920]
			LEA r13, [r13 + 1920]

		DEC r9d
		JNZ c_loop
			
		MOV r9d, r8d

		SUB rcx, r10
		LEA rcx, [rcx + r10*8]
		LEA rcx, [rcx + r10*8]

		CMP r15, 64
		JB no_extra_rcx
			LEA rcx, [rcx + r10*8]
			LEA rcx, [rcx + r10*8]
		no_extra_rcx:

		MOV r11, rcx

		ADD rdx, 2048
		ADD r12, 2048
		ADD r13, 2048
	DEC eax
	JNZ c_loop

	SFENCE

	POP r15
	POP r14
	POP r13

	POP r12
	POP rbx


	RET
H264_Convert2Picture_1	ENDP



ALIGN 16
H264_Convert2Picture_0	PROC
	PUSH rbx

	PUSH r14
	PUSH r15

	PCMPEQW xmm9, xmm9

	PCMPEQW xmm10, xmm10
	PSLLW xmm10, 15
	MOVDQA xmm11, xmm10
	PSRLW xmm11, 8
	POR xmm10, xmm11

	PXOR xmm11, xmm11
	PXOR xmm13, xmm13
	PXOR xmm14, xmm14
	PXOR xmm15, xmm15


	MOVD xmm11, DWORD PTR [rdx + 4] ; luma depth
	MOVD xmm12, DWORD PTR [rdx + 8] ; chroma depth

	CMP DWORD PTR [rdx + 12], 0
	JNZ full_range
		MOV eax, 00100010h
		MOVD xmm13, eax
		PSHUFD xmm13, xmm13, 0

		MOVDQA xmm14, XMMWORD PTR [y_part]
		MOVDQA xmm15, XMMWORD PTR [cc_part]
	full_range:


	MOV r10d, [rdx + 24] ; c_width
	MOV eax, [rdx + 28] ; c_height
	MOV r15, [rdx + 32]
	SHL r15, 1

	MOV r8d, r10d
	MOV r9d, r10d
	SHL r10, 6 ; picture line offset
		
	LEA rdx, [rdx+2048+512]
	MOV r11, rcx	
	
	MOV ebx, 0808h
	MOV r14, 32

	ADD r15, 32
	CMP r15, 64
	JB c_loop
		SUB r15, 32

		MOV r14, 16

		MOV ebx, 1010h
		SHR eax, 1
align 16
	c_loop:
				MOVDQA xmm0, [rdx] ; y
				PSUBW xmm0, xmm13
				MOVDQA xmm7, xmm0
				PMULHW xmm7, xmm14
				PADDW xmm0, xmm7
				PSRAW xmm0, xmm11

				MOVDQA xmm1, [rdx + 16] ; y
				PSUBW xmm1, xmm13
				MOVDQA xmm7, xmm1
				PMULHW xmm7, xmm14
				PADDW xmm1, xmm7
				PSRAW xmm1, xmm11

				PACKUSWB xmm0, xmm1
		
		
		
				MOVDQA xmm1, [rdx + r15] ; y
				PSUBW xmm1, xmm13
				MOVDQA xmm7, xmm1
				PMULHW xmm7, xmm14
				PADDW xmm1, xmm7
				PSRAW xmm1, xmm11

				MOVDQA xmm2, [rdx + r15 + 16] ; y
				PSUBW xmm2, xmm13
				MOVDQA xmm7, xmm2
				PMULHW xmm7, xmm14
				PADDW xmm2, xmm7
				PSRAW xmm2, xmm11

				PACKUSWB xmm1, xmm2
				
				


				MOVDQA xmm2, [rdx + 512] ; cb
				MOVDQA xmm7, xmm2
				PMULHW xmm7, xmm15
				PADDW xmm2, xmm7
				PSRAW xmm2, xmm12

				MOVDQA xmm3, [rdx + 512 + 16] ; cb
				MOVDQA xmm7, xmm3
				PMULHW xmm7, xmm15
				PADDW xmm3, xmm7
				PSRAW xmm3, xmm12

				PACKSSWB xmm2, xmm3
				PXOR xmm2, xmm10

				MOVDQA xmm3, [rdx + r15 + 512] ; cb
				MOVDQA xmm7, xmm3
				PMULHW xmm7, xmm15
				PADDW xmm3, xmm7
				PSRAW xmm3, xmm12

				MOVDQA xmm4, [rdx + r15 + 512 + 16] ; cb
				MOVDQA xmm7, xmm4
				PMULHW xmm7, xmm15
				PADDW xmm4, xmm7
				PSRAW xmm4, xmm12

				PACKSSWB xmm3, xmm4
				PXOR xmm3, xmm10



				MOVDQA xmm4, [rdx + 1024] ; cr
				MOVDQA xmm7, xmm4
				PMULHW xmm7, xmm15
				PADDW xmm4, xmm7
				PSRAW xmm4, xmm12

				MOVDQA xmm5, [rdx + 1024 + 16] ; cr
				MOVDQA xmm7, xmm5
				PMULHW xmm7, xmm15
				PADDW xmm5, xmm7
				PSRAW xmm5, xmm12

				PACKSSWB xmm4, xmm5
				PXOR xmm4, xmm10


				MOVDQA xmm5, [rdx + r15 + 1024] ; cr
				MOVDQA xmm7, xmm5
				PMULHW xmm7, xmm15
				PADDW xmm5, xmm7
				PSRAW xmm5, xmm12

				MOVDQA xmm6, [rdx + r15 + 1024 + 16] ; cr
				MOVDQA xmm7, xmm6
				PMULHW xmm7, xmm15
				PADDW xmm6, xmm7
				PSRAW xmm6, xmm12

				PACKSSWB xmm5, xmm6
				PXOR xmm5, xmm10

				LEA rdx, [rdx + r14*2]





				MOVDQA xmm6, xmm0
				PUNPCKLBW xmm0, xmm2
				PUNPCKHBW xmm6, xmm2 ; ycb
				
				MOVDQA xmm2, xmm4
				PUNPCKLBW xmm2, xmm9
				PUNPCKHBW xmm4, xmm9 ; cra

				MOVDQA xmm8, xmm0
				MOVDQA xmm7, xmm6

				PUNPCKLWD xmm0, xmm2
				PUNPCKHWD xmm8, xmm2
				PUNPCKLWD xmm6, xmm4
				PUNPCKHWD xmm7, xmm4



				MOVNTDQ [r11], xmm0
				MOVNTDQ [r11+16], xmm8
				MOVNTDQ [r11+32], xmm6
				MOVNTDQ [r11+48], xmm7

				LEA r11, [r11 + r10]





				MOVDQA xmm6, xmm1
				PUNPCKLBW xmm1, xmm3
				PUNPCKHBW xmm6, xmm3 ; ycb
				
				MOVDQA xmm3, xmm5
				PUNPCKLBW xmm3, xmm9
				PUNPCKHBW xmm5, xmm9 ; cra

				MOVDQA xmm8, xmm1
				MOVDQA xmm7, xmm6

				PUNPCKLWD xmm1, xmm3
				PUNPCKHWD xmm8, xmm3
				PUNPCKLWD xmm6, xmm5
				PUNPCKHWD xmm7, xmm5


				MOVNTDQ [r11], xmm1
				MOVNTDQ [r11+16], xmm8
				MOVNTDQ [r11+32], xmm6
				MOVNTDQ [r11+48], xmm7

				LEA r11, [r11 + r10]




				


			DEC bl
			JNZ c_loop
						
			MOV bl, bh

			ADD rcx, 64
			MOV r11, rcx
						
			LEA rdx, [rdx + 1536]

		DEC r9d
		JNZ c_loop
			
		MOV r9d, r8d

		SUB rcx, r10
		LEA rcx, [rcx+r10*8]
		LEA rcx, [rcx+r10*8]

		CMP r15, 64
		JB no_extra_rcx
			LEA rcx, [rcx+r10*8]
			LEA rcx, [rcx+r10*8]

		no_extra_rcx:

		MOV r11, rcx

		ADD rdx, 2048
	DEC eax
	JNZ c_loop

	SFENCE

	POP r15
	POP r14

	POP rbx

	RET
H264_Convert2Picture_0	ENDP





ALIGN 16
H264_Convert2Picture_2	PROC
	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	PCMPEQW xmm9, xmm9
	
	PCMPEQW xmm10, xmm10
	PSLLW xmm10, 15
	MOVDQA xmm11, xmm10
	PSRLW xmm11, 8
	POR xmm10, xmm11


	PXOR xmm11, xmm11
	PXOR xmm13, xmm13
	PXOR xmm14, xmm14
	PXOR xmm15, xmm15


	MOVD xmm11, DWORD PTR [rdx + 4] ; luma depth
	MOVD xmm12, DWORD PTR [rdx + 8] ; chroma depth

	CMP DWORD PTR [rdx + 12], 0
	JNZ full_range
		MOV eax, 00100010h
		MOVD xmm13, eax
		PSHUFD xmm13, xmm13, 0

		MOVDQA xmm14, XMMWORD PTR [y_part]
		MOVDQA xmm15, XMMWORD PTR [cc_part]
	full_range:


	MOV r10d, [rdx+24] ; c_width
	MOV eax, [rdx+28] ; c_height
	MOV r15, [rdx + 32]
	SHL r15, 1

	MOV r8d, r10d
	MOV r9d, r10d
	SHL r10, 6 ; picture line offset
		
	LEA rdx, [rdx+2048+512]
	MOV r11, rcx
	
	MOV ebx, 0808h

	MOV r14, 32

	MOV r12, rdx
	LEA r13, [rdx + 16]

	ADD r15, 32
	CMP r15, 64
	JB c_loop
		SUB r15, 32
		LEA r13, [rdx + r15]

		SHR eax, 1
		MOV ebx, 1010h

		MOV r14, 16

align 16
	c_loop:
				MOVDQA xmm0, [rdx] ; y
				PSUBW xmm0, xmm13
				MOVDQA xmm4, xmm0
				PMULHW xmm4, xmm14
				PADDW xmm0, xmm4
				PSRAW xmm0, xmm11

				MOVDQA xmm1, [rdx + 16] ; y
				PSUBW xmm1, xmm13
				MOVDQA xmm4, xmm1
				PMULHW xmm4, xmm14
				PADDW xmm1, xmm4
				PSRAW xmm1, xmm11

				PACKUSWB xmm0, xmm1
				

				MOVDQA xmm2, [rdx + r15] ; y
				PSUBW xmm2, xmm13
				MOVDQA xmm4, xmm2
				PMULHW xmm4, xmm14
				PADDW xmm2, xmm4
				PSRAW xmm2, xmm11

				MOVDQA xmm3, [rdx + r15 + 16] ; y
				PSUBW xmm3, xmm13
				MOVDQA xmm4, xmm3
				PMULHW xmm4, xmm14
				PADDW xmm3, xmm4
				PSRAW xmm3, xmm11

				PACKUSWB xmm2, xmm3



				MOVDQA xmm4, [r12 + 512] ; cb
				MOVDQA xmm9, xmm4
				PMULHW xmm9, xmm15
				PADDW xmm4, xmm9
				PSRAW xmm4, xmm12

				MOVDQA xmm5, [r13 + 512] ; cb
				MOVDQA xmm9, xmm5
				PMULHW xmm9, xmm15
				PADDW xmm5, xmm9
				PSRAW xmm5, xmm12

				PACKUSWB xmm4, xmm5
				PXOR xmm4, xmm10

				MOVDQA xmm5, xmm4
				PUNPCKLBW xmm4, xmm4
				PUNPCKHBW xmm5, xmm5

				MOVDQA xmm6, [r12 + 1024] ; cb
				MOVDQA xmm9, xmm6
				PMULHW xmm9, xmm15
				PADDW xmm6, xmm9
				PSRAW xmm6, xmm12

				MOVDQA xmm7, [r13 + 1024] ; cb
				MOVDQA xmm9, xmm7
				PMULHW xmm9, xmm15
				PADDW xmm7, xmm9
				PSRAW xmm7, xmm12

				LEA rdx, [rdx + r14*2]
				LEA r12, [r12 + r14]
				LEA r13, [r13 + r14]
				

				PACKUSWB xmm6, xmm7
				PXOR xmm6, xmm10
				MOVDQA xmm7, xmm6
				PUNPCKLBW xmm6, xmm6
				PUNPCKHBW xmm7, xmm7






				MOVDQA xmm1, xmm0
				PUNPCKLBW xmm0, xmm4
				PUNPCKHBW xmm1, xmm4

				MOVDQA xmm3, xmm2
				PUNPCKLBW xmm2, xmm5
				PUNPCKHBW xmm3, xmm5


				MOVDQA xmm4, xmm6
				PUNPCKLBW xmm4, xmm9
				PUNPCKHBW xmm6, xmm9

				
				MOVDQA xmm5, xmm0
				MOVDQA xmm8, xmm1

				PUNPCKLWD xmm0, xmm4
				PUNPCKHWD xmm5, xmm4
				PUNPCKLWD xmm1, xmm6
				PUNPCKHWD xmm8, xmm6
				

				MOVNTDQ [r11], xmm0
				MOVNTDQ [r11+16], xmm5
				MOVNTDQ [r11+32], xmm1
				MOVNTDQ [r11+48], xmm8

				LEA r11, [r11 + r10]



				MOVDQA xmm4, xmm7
				PUNPCKLBW xmm4, xmm9
				PUNPCKHBW xmm7, xmm9

				MOVDQA xmm5, xmm2
				MOVDQA xmm8, xmm3

				PUNPCKLWD xmm2, xmm4
				PUNPCKHWD xmm5, xmm4
				PUNPCKLWD xmm3, xmm7
				PUNPCKHWD xmm8, xmm7



				MOVNTDQ [r11], xmm2
				MOVNTDQ [r11+16], xmm5
				MOVNTDQ [r11+32], xmm3
				MOVNTDQ [r11+48], xmm8

				LEA r11, [r11 + r10]
				


			DEC bl
			JNZ c_loop						

			ADD rcx, 64
			MOV r11, rcx

			MOV bl, bh

			LEA rdx, [rdx + 1536]
			LEA r12, [r12 + 1792]
			LEA r13, [r13 + 1792]

		DEC r9d
		JNZ c_loop
			
		MOV r9d, r8d

		SUB rcx, r10
		LEA rcx, [rcx+r10*8]
		LEA rcx, [rcx+r10*8]

		CMP r15, 64
		JB no_extra_rcx
			LEA rcx, [rcx+r10*8]
			LEA rcx, [rcx+r10*8]

		no_extra_rcx:

		MOV r11, rcx

		ADD rdx, 2048
		ADD r12, 2048
		ADD r13, 2048
	DEC eax
	JNZ c_loop

	SFENCE


	POP r15
	POP r14
	POP r13
	POP r12

	POP rbx


	RET
H264_Convert2Picture_2	ENDP







ALIGN 16
H264_Deblock_1	PROC

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	MOV r11, rcx

	MOV ecx, DWORD PTR [r11 + 4] ; luma depth
	SHL ecx, 16
	MOV cl, [r11 + 8] ; chroma depth

	ROR ecx, 16

	MOV r10d, [r11+24] ; c_width
	MOV eax, [r11+28] ; c_height
	

	CMP DWORD PTR [r11 + 40], 0
	JZ no_field_pic
		SHR eax, 1

		CMP DWORD PTR [r11 + 44], 0
		JZ no_field_pic
			MOV r8, [r11 + 32]
			LEA r11, [r11 + r8*2]

	no_field_pic:


	MOV r8d, r10d
	MOV r9d, r10d

	DEC r8d

	INC r9
	SHL r9, 11 ; picture mb_line offset
	NEG r9
	



align 16
	d_loop:
			MOV r15, -16
			LEA r11, [r11 + 2048]

		SUB r10d, 1
		JNC do_test

			MOV r10d, r8d
			LEA r11, [r11 + 2048]

	SUB eax, 1
	JZ done

align 16
	do_test:
		TEST DWORD PTR [r11], 04000h ; is deblock
		JZ d_loop
		

		align 16
			mb_luma_loop:
; vertical luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32]

				MOV r12, 16
				ADD r12, r15
				MOV r13, r12
				SHR r13, 2
				AND r12, 3
				SHL r12, 7
				LEA r12, [r12 + r13*8]

				LEA rsi, [r11 + r12 + 512]

				TEST r14d, 80h
				JNZ skip_luma_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					CMP r15, -4
					JL no_right_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 32]
					no_right_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta






				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 32]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 64]
					MOVD xmm3, QWORD PTR [rsi + 96]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 32]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 64]
					MOVD xmm7, QWORD PTR [rdi + 96]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_vb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm5
						PSUBW xmm10, xmm4
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_vb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm4

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm5
							PSUBW xmm11, xmm6
							PSUBW xmm11, xmm6
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13
							
							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm6, xmm11

				


						JMP luma_vb_offload
					align 16
						luma_vb_4:							
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm4
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm4
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm6

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm6
							MOVDQA xmm10, xmm7
							PADDW xmm10, xmm5
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm4
							PADDW xmm9, xmm5
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm5
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm12
							PSUBW xmm4, xmm13
							PSRAW xmm4, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm4, xmm15
							PAND xmm6, xmm15
							PAND xmm5, xmm15

							POR xmm4, xmm8
							POR xmm6, xmm9
							POR xmm5, xmm10



				align 16
					luma_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 32], xmm1
						MOVD QWORD PTR [rsi + 64], xmm2
						MOVD QWORD PTR [rsi + 96], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 32], xmm6
						MOVD QWORD PTR [rdi + 64], xmm5
						MOVD QWORD PTR [rdi + 96], xmm4

		align 16
			skip_luma_vb:
; horizontal luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32 + 1] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32 + 1]

				TEST r14d, 80h
				JNZ skip_luma_hb
					LEA rdi, [rsi - 128]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					TEST r15, 3
					JNZ no_top_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 2] ; top alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 3] ; top beta

						LEA rdi, [rdi + r9 + 128 + 384]
					no_top_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 32] ; p2
					MOVD xmm1, QWORD PTR [rdi + 64] ; p1
					MOVD xmm3, QWORD PTR [rdi + 96] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 32] ; q1
					MOVD xmm6, QWORD PTR [rsi + 64] ; q2
					MOVD xmm4, QWORD PTR [rsi + 96] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_hb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm6
						PSUBW xmm10, xmm7
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_hb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm7

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm6
							PSUBW xmm11, xmm5
							PSUBW xmm11, xmm5
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13

							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm5, xmm11

				


						JMP luma_hb_offload
					align 16
						luma_hb_4:
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm7
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm7
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm5

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm5
							MOVDQA xmm10, xmm4
							PADDW xmm10, xmm6
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm7
							PADDW xmm9, xmm6
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm6
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm12
							PSUBW xmm7, xmm13
							PSRAW xmm7, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm7, xmm15
							PAND xmm5, xmm15
							PAND xmm6, xmm15

							POR xmm7, xmm8
							POR xmm5, xmm9
							POR xmm6, xmm10


				align 16
					luma_hb_offload:
				;		MOVD QWORD PTR [rdi], xmm0
						MOVD QWORD PTR [rdi + 32], xmm2
						MOVD QWORD PTR [rdi + 64], xmm1
						MOVD QWORD PTR [rdi + 96], xmm3

						MOVD QWORD PTR [rsi], xmm7
						MOVD QWORD PTR [rsi + 32], xmm5
						MOVD QWORD PTR [rsi + 64], xmm6
				;		MOVD QWORD PTR [rsi + 96], xmm4




			align 16
				skip_luma_hb:



			INC r15
			JNZ mb_luma_loop




























; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 8] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 8]

				LEA rsi, [r11 + 1024]

				TEST r14d, 80h
				JNZ skip_chroma10_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma10_vb

						TEST r14d, 40h
						JNZ chroma10_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma10_vb_offload
					align 16
						chroma10_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma10_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma10_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 9] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 9]

				TEST r14d, 80h
				JNZ skip_chroma10_hb
					LEA rdi, [rsi + r9 + 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma10_hb

						TEST r14d, 40h
						JNZ chroma10_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma10_hb_offload
					align 16
						chroma10_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma10_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma10_hb:


; vertical chroma 11
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 12] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 12]

				LEA rsi, [r11 + 1024 + 64]

				TEST r14d, 80h
				JNZ skip_chroma11_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma11_vb

						TEST r14d, 40h
						JNZ chroma11_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma11_vb_offload
					align 16
						chroma11_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma11_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma11_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 13] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 13]

				TEST r14d, 80h
				JNZ skip_chroma11_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma11_hb

						TEST r14d, 40h
						JNZ chroma11_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma11_hb_offload
					align 16
						chroma11_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma11_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma11_hb:


; vertical chroma 12
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 24] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 24]

				LEA rsi, [r11 + 1024 + 8]

				TEST r14d, 80h
				JNZ skip_chroma12_vb

					LEA rdi, [rsi + 2048 - 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma12_vb

						TEST r14d, 40h
						JNZ chroma12_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma12_vb_offload
					align 16
						chroma12_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma12_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma12_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 25] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 25]

				TEST r14d, 80h
				JNZ skip_chroma12_hb
					LEA rdi, [rsi + r9 + 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma12_hb

						TEST r14d, 40h
						JNZ chroma12_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma12_hb_offload
					align 16
						chroma12_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma12_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma12_hb:


; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 28] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 28]

				LEA rsi, [r11 + 1024 + 64 + 8]

				TEST r14d, 80h
				JNZ skip_chroma13_vb

					LEA rdi, [rsi + 2048 - 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma13_vb

						TEST r14d, 40h
						JNZ chroma13_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma13_vb_offload
					align 16
						chroma13_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma13_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma13_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 32 + 29] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 32 + 29]

				TEST r14d, 80h
				JNZ skip_chroma13_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma13_hb

						TEST r14d, 40h
						JNZ chroma13_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma13_hb_offload
					align 16
						chroma13_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma13_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma13_hb:
































; vertical chroma 20
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 8] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 8]

				LEA rsi, [r11 + 1536]

				TEST r14d, 80h
				JNZ skip_chroma20_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma20_vb

						TEST r14d, 40h
						JNZ chroma20_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma20_vb_offload
					align 16
						chroma20_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma20_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma20_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 9] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 9]

				TEST r14d, 80h
				JNZ skip_chroma20_hb
					LEA rdi, [rsi + r9 + 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma20_hb

						TEST r14d, 40h
						JNZ chroma20_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma20_hb_offload
					align 16
						chroma20_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma20_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma20_hb:


; vertical chroma 11
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 12] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 12]

				LEA rsi, [r11 + 1536 + 64]

				TEST r14d, 80h
				JNZ skip_chroma21_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma21_vb

						TEST r14d, 40h
						JNZ chroma21_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma21_vb_offload
					align 16
						chroma21_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma21_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma21_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 13] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 13]

				TEST r14d, 80h
				JNZ skip_chroma21_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma21_hb

						TEST r14d, 40h
						JNZ chroma21_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma21_hb_offload
					align 16
						chroma21_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma21_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma21_hb:


; vertical chroma 12
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 24] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 24]

				LEA rsi, [r11 + 1536 + 8]

				TEST r14d, 80h
				JNZ skip_chroma22_vb

					LEA rdi, [rsi + 2048 - 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma22_vb

						TEST r14d, 40h
						JNZ chroma22_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma22_vb_offload
					align 16
						chroma22_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma22_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma22_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 25] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 25]

				TEST r14d, 80h
				JNZ skip_chroma22_hb
					LEA rdi, [rsi + r9 + 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma22_hb

						TEST r14d, 40h
						JNZ chroma22_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma22_hb_offload
					align 16
						chroma22_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma22_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma22_hb:


; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 28] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 28]

				LEA rsi, [r11 + 1536 + 64 + 8]

				TEST r14d, 80h
				JNZ skip_chroma23_vb

					LEA rdi, [rsi + 2048 - 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma23_vb

						TEST r14d, 40h
						JNZ chroma23_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma23_vb_offload
					align 16
						chroma23_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma23_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma23_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + 392 + 64 + 29] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + 392 + 64 + 29]

				TEST r14d, 80h
				JNZ skip_chroma23_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma23_hb

						TEST r14d, 40h
						JNZ chroma23_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma23_hb_offload
					align 16
						chroma23_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma23_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma23_hb:







	JMP d_loop
	
done:

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	RET
H264_Deblock_1	ENDP



































ALIGN 16
H264_Deblock_2	PROC

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	MOV r11, rcx

	MOV ecx, DWORD PTR [r11 + 4] ; luma depth
	SHL ecx, 16
	MOV cl, [r11 + 8] ; chroma depth

	ROR ecx, 16

	MOV r10d, [r11+24] ; c_width
	MOV eax, [r11+28] ; c_height
	
	CMP DWORD PTR [r11 + 40], 0
	JZ no_field_pic
		SHR eax, 1

		CMP DWORD PTR [r11 + 44], 0
		JZ no_field_pic
			MOV r8, [r11 + 32]
			LEA r11, [r11 + r8*2]
			
	no_field_pic:


	MOV r8d, r10d
	MOV r9d, r10d

	DEC r8d

	INC r9
	SHL r9, 11 ; picture mb_line offset
	NEG r9
	



align 16
	d_loop:
			MOV r15, -16
			LEA r11, [r11 + 2048]

		SUB r10d, 1
		JNC do_test

			MOV r10d, r8d
			LEA r11, [r11 + 2048]

	SUB eax, 1
	JZ done

align 16
	do_test:
		TEST DWORD PTR [r11], 04000h ; is deblock
		JZ d_loop
		

		align 16
			mb_luma_loop:
; vertical luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32]

				MOV r12, 16
				ADD r12, r15
				MOV r13, r12
				SHR r13, 2
				AND r12, 3
				SHL r12, 7
				LEA r12, [r12 + r13*8]

				LEA rsi, [r11 + r12 + 512]

				TEST r14d, 80h
				JNZ skip_luma_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					CMP r15, -4
					JL no_right_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 32]
					no_right_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta






				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 32]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 64]
					MOVD xmm3, QWORD PTR [rsi + 96]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 32]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 64]
					MOVD xmm7, QWORD PTR [rdi + 96]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_vb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm5
						PSUBW xmm10, xmm4
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_vb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm4

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm5
							PSUBW xmm11, xmm6
							PSUBW xmm11, xmm6
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13
							
							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm6, xmm11

				


						JMP luma_vb_offload
					align 16
						luma_vb_4:							
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm4
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm4
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm6

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm6
							MOVDQA xmm10, xmm7
							PADDW xmm10, xmm5
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm4
							PADDW xmm9, xmm5
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm5
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm12
							PSUBW xmm4, xmm13
							PSRAW xmm4, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm4, xmm15
							PAND xmm6, xmm15
							PAND xmm5, xmm15

							POR xmm4, xmm8
							POR xmm6, xmm9
							POR xmm5, xmm10



				align 16
					luma_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 32], xmm1
						MOVD QWORD PTR [rsi + 64], xmm2
						MOVD QWORD PTR [rsi + 96], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 32], xmm6
						MOVD QWORD PTR [rdi + 64], xmm5
						MOVD QWORD PTR [rdi + 96], xmm4

		align 16
			skip_luma_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32 + 1] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32 + 1]

				TEST r14d, 80h
				JNZ skip_luma_hb
					LEA rdi, [rsi - 128]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					TEST r15, 3
					JNZ no_top_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 2] ; top alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 3] ; top beta

						LEA rdi, [rdi + r9 + 128 + 384]
					no_top_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 32] ; p2
					MOVD xmm1, QWORD PTR [rdi + 64] ; p1
					MOVD xmm3, QWORD PTR [rdi + 96] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 32] ; q1
					MOVD xmm6, QWORD PTR [rsi + 64] ; q2
					MOVD xmm4, QWORD PTR [rsi + 96] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_hb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm6
						PSUBW xmm10, xmm7
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_hb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm7

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm6
							PSUBW xmm11, xmm5
							PSUBW xmm11, xmm5
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13

							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm5, xmm11

				


						JMP luma_hb_offload
					align 16
						luma_hb_4:
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm7
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm7
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm5

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm5
							MOVDQA xmm10, xmm4
							PADDW xmm10, xmm6
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm7
							PADDW xmm9, xmm6
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm6
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm12
							PSUBW xmm7, xmm13
							PSRAW xmm7, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm7, xmm15
							PAND xmm5, xmm15
							PAND xmm6, xmm15

							POR xmm7, xmm8
							POR xmm5, xmm9
							POR xmm6, xmm10


				align 16
					luma_hb_offload:
				;		MOVD QWORD PTR [rdi], xmm0
						MOVD QWORD PTR [rdi + 32], xmm2
						MOVD QWORD PTR [rdi + 64], xmm1
						MOVD QWORD PTR [rdi + 96], xmm3

						MOVD QWORD PTR [rsi], xmm7
						MOVD QWORD PTR [rsi + 32], xmm5
						MOVD QWORD PTR [rsi + 64], xmm6
				;		MOVD QWORD PTR [rsi + 96], xmm4




			align 16
				skip_luma_hb:



			INC r15
			JNZ mb_luma_loop






















			MOV r15, -4

		align 16
			mb_chroma1_loop:



; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 8] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 8]


				LEA rsi, [r11 + r15*4 + 1024 + 16]

				TEST r14d, 80h
				JNZ skip_chroma10_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c10
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c10:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma10_vb

						TEST r14d, 40h
						JNZ chroma10_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma10_vb_offload
					align 16
						chroma10_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma10_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma10_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 9] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 9]

				TEST r14d, 80h
				JNZ skip_chroma10_hb
					LEA rdi, [rsi + r9 + 192]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma10_hb

						TEST r14d, 40h
						JNZ chroma10_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma10_hb_offload
					align 16
						chroma10_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma10_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma10_hb:


; vertical chroma 11
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 10] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 10]

				LEA rsi, [r11 + r15*4 + 1024 + 64 + 16]

				TEST r14d, 80h
				JNZ skip_chroma11_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c11
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c11:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma11_vb

						TEST r14d, 40h
						JNZ chroma11_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma11_vb_offload
					align 16
						chroma11_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma11_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma11_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 11] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 11]

				TEST r14d, 80h
				JNZ skip_chroma11_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma11_hb

						TEST r14d, 40h
						JNZ chroma11_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma11_hb_offload
					align 16
						chroma11_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma11_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma11_hb:


; vertical chroma 12
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 12] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 12]

				LEA rsi, [r11 + r15*4 + 1024 + 128 + 16]

				TEST r14d, 80h
				JNZ skip_chroma12_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c12
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c12:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma12_vb

						TEST r14d, 40h
						JNZ chroma12_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma12_vb_offload
					align 16
						chroma12_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma12_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma12_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 13] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 13]

				TEST r14d, 80h
				JNZ skip_chroma12_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma12_hb

						TEST r14d, 40h
						JNZ chroma12_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma12_hb_offload
					align 16
						chroma12_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma12_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma12_hb:


; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 14] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 14]

				LEA rsi, [r11 + r15*4 + 1024 + 192 + 16]

				TEST r14d, 80h
				JNZ skip_chroma13_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c13
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c13:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma13_vb

						TEST r14d, 40h
						JNZ chroma13_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma13_vb_offload
					align 16
						chroma13_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma13_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma13_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 64 + 15] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 64 + 15]

				TEST r14d, 80h
				JNZ skip_chroma13_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma13_hb

						TEST r14d, 40h
						JNZ chroma13_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma13_hb_offload
					align 16
						chroma13_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma13_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma13_hb:


			ADD r15, 2
			JNZ mb_chroma1_loop




























			MOV r15, -4

		align 16
			mb_chroma2_loop:



; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 8] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 8]


				LEA rsi, [r11 + r15*4 + 1536 + 16]

				TEST r14d, 80h
				JNZ skip_chroma20_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c20
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c20:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma20_vb

						TEST r14d, 40h
						JNZ chroma20_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma20_vb_offload
					align 16
						chroma20_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma20_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma20_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 9] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 9]

				TEST r14d, 80h
				JNZ skip_chroma20_hb
					LEA rdi, [rsi + r9 + 192]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 2] ; top alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 3] ; top beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma20_hb

						TEST r14d, 40h
						JNZ chroma20_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma20_hb_offload
					align 16
						chroma20_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma20_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma20_hb:


; vertical chroma 11
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 10] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 10]

				LEA rsi, [r11 + r15*4 + 1536 + 64 + 16]

				TEST r14d, 80h
				JNZ skip_chroma21_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c21
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c21:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma21_vb

						TEST r14d, 40h
						JNZ chroma21_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma21_vb_offload
					align 16
						chroma21_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma21_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma21_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 11] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 11]

				TEST r14d, 80h
				JNZ skip_chroma21_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma21_hb

						TEST r14d, 40h
						JNZ chroma21_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma21_hb_offload
					align 16
						chroma21_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma21_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma21_hb:


; vertical chroma 12
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 12] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 12]

				LEA rsi, [r11 + r15*4 + 1536 + 128 + 16]

				TEST r14d, 80h
				JNZ skip_chroma22_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c22
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c22:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma22_vb

						TEST r14d, 40h
						JNZ chroma22_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma22_vb_offload
					align 16
						chroma22_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma22_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma22_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 13] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 13]

				TEST r14d, 80h
				JNZ skip_chroma22_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma22_hb

						TEST r14d, 40h
						JNZ chroma22_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma22_hb_offload
					align 16
						chroma22_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma22_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma22_hb:


; vertical chroma 10
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 14] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 14]

				LEA rsi, [r11 + r15*4 + 1536 + 192 + 16]

				TEST r14d, 80h
				JNZ skip_chroma23_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					CMP r15, -2
					JL no_right_c23
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 16]
					no_right_c23:
					
					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta
					
				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 16]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 32]
					MOVD xmm3, QWORD PTR [rsi + 48]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 16]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 32]
					MOVD xmm7, QWORD PTR [rdi + 48]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma23_vb

						TEST r14d, 40h
						JNZ chroma23_vb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tC


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta


						JMP chroma23_vb_offload
					align 16
						chroma23_vb_4:
							PCMPEQW xmm11, xmm11
							PSLLW xmm11, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm11
							PSRAW xmm3, 2

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm1
							PSUBW xmm4, xmm11
							PSRAW xmm4, 2

				align 16
					chroma23_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 16], xmm1
						MOVD QWORD PTR [rsi + 32], xmm2
						MOVD QWORD PTR [rsi + 48], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 16], xmm6
						MOVD QWORD PTR [rdi + 32], xmm5
						MOVD QWORD PTR [rdi + 48], xmm4

		align 16
			skip_chroma23_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*8 + 392 + 96 + 15] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*8 + 392 + 96 + 15]

				TEST r14d, 80h
				JNZ skip_chroma23_hb
					LEA rdi, [rsi - 64]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 16] ; p2
					MOVD xmm1, QWORD PTR [rdi + 32] ; p1
					MOVD xmm3, QWORD PTR [rdi + 48] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 16] ; q1
					MOVD xmm6, QWORD PTR [rsi + 32] ; q2
					MOVD xmm4, QWORD PTR [rsi + 48] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma23_hb

						TEST r14d, 40h
						JNZ chroma23_hb_4
							SHL r14d, cl
							INC r14d

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0


							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm13
							PSIGNW xmm13, xmm11
							PMAXSW xmm10, xmm13 ; delta

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta


						JMP chroma23_hb_offload
					align 16
						chroma23_hb_4:
							PCMPEQW xmm9, xmm9
							PSLLW xmm9, 1

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm9
							PSRAW xmm3, 2

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm1
							PSUBW xmm7, xmm9
							PSRAW xmm7, 2
						

				align 16
					chroma23_hb_offload:
;						MOVD QWORD PTR [rdi], xmm0
;						MOVD QWORD PTR [rdi + 16], xmm2
;						MOVD QWORD PTR [rdi + 32], xmm1
						MOVD QWORD PTR [rdi + 48], xmm3

						MOVD QWORD PTR [rsi], xmm7
;						MOVD QWORD PTR [rsi + 16], xmm5
;						MOVD QWORD PTR [rsi + 32], xmm6
;						MOVD QWORD PTR [rsi + 48], xmm4




			align 16
				skip_chroma23_hb:


			ADD r15, 2
			JNZ mb_chroma2_loop







	JMP d_loop
	
done:


	POP r15
	POP r14
	POP r13
	POP r12


	POP rdi
	POP rsi

	RET
H264_Deblock_2	ENDP








































ALIGN 16
H264_Deblock_0	PROC

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	MOV r11, rcx

	MOV ecx, DWORD PTR [r11 + 4] ; luma depth
	SHL ecx, 16
	MOV cl, [r11 + 8] ; chroma depth

	ROR ecx, 16

	MOV r10d, [r11+24] ; c_width
	MOV eax, [r11+28] ; c_height
	
	CMP DWORD PTR [r11 + 40], 0
	JZ no_field_pic
		SHR eax, 1

		CMP DWORD PTR [r11 + 44], 0
		JZ no_field_pic
			MOV r8, [r11 + 32]
			LEA r11, [r11 + r8*2]
			
	no_field_pic:


	MOV r8d, r10d
	MOV r9d, r10d

	DEC r8d

	INC r9
	SHL r9, 11 ; picture mb_line offset
	NEG r9
	



align 16
	d_loop:
			MOV r15, -16
			LEA r11, [r11 + 2048]

		SUB r10d, 1
		JNC do_test

			MOV r10d, r8d
			LEA r11, [r11 + 2048]

	SUB eax, 1
	JZ done

align 16
	do_test:
		TEST DWORD PTR [r11], 04000h ; is deblock
		JZ d_loop
		

		align 16
			mb_luma_loop:
; vertical luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32]

				MOV r12, 16
				ADD r12, r15
				MOV r13, r12
				SHR r13, 2
				AND r12, 3
				SHL r12, 7
				LEA r12, [r12 + r13*8]

				LEA rsi, [r11 + r12 + 512]

				TEST r14d, 80h
				JNZ skip_luma_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					CMP r15, -4
					JL no_right_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 32]
					no_right_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta






				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 32]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 64]
					MOVD xmm3, QWORD PTR [rsi + 96]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 32]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 64]
					MOVD xmm7, QWORD PTR [rdi + 96]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_vb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm5
						PSUBW xmm10, xmm4
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_vb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm4

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm5
							PSUBW xmm11, xmm6
							PSUBW xmm11, xmm6
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13
							
							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm6, xmm11

				


						JMP luma_vb_offload
					align 16
						luma_vb_4:							
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm4
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm4
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm6

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm6
							MOVDQA xmm10, xmm7
							PADDW xmm10, xmm5
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm4
							PADDW xmm9, xmm5
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm5
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm12
							PSUBW xmm4, xmm13
							PSRAW xmm4, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm4, xmm15
							PAND xmm6, xmm15
							PAND xmm5, xmm15

							POR xmm4, xmm8
							POR xmm6, xmm9
							POR xmm5, xmm10



				align 16
					luma_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 32], xmm1
						MOVD QWORD PTR [rsi + 64], xmm2
						MOVD QWORD PTR [rsi + 96], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 32], xmm6
						MOVD QWORD PTR [rdi + 64], xmm5
						MOVD QWORD PTR [rdi + 96], xmm4

		align 16
			skip_luma_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 32 + 1] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 32 + 1]

				TEST r14d, 80h
				JNZ skip_luma_hb
					LEA rdi, [rsi - 128]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 1] ; internal beta
					
					TEST r15, 3
					JNZ no_top_border
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 2] ; top alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 3] ; top beta

						LEA rdi, [rdi + r9 + 128 + 384]
					no_top_border:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 32] ; p2
					MOVD xmm1, QWORD PTR [rdi + 64] ; p1
					MOVD xmm3, QWORD PTR [rdi + 96] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 32] ; q1
					MOVD xmm6, QWORD PTR [rsi + 64] ; q2
					MOVD xmm4, QWORD PTR [rsi + 96] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_luma_hb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm6
						PSUBW xmm10, xmm7
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ luma_hb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm7

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm6
							PSUBW xmm11, xmm5
							PSUBW xmm11, xmm5
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13

							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm5, xmm11

				


						JMP luma_hb_offload
					align 16
						luma_hb_4:
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm7
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm7
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm5

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm5
							MOVDQA xmm10, xmm4
							PADDW xmm10, xmm6
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm7
							PADDW xmm9, xmm6
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm6
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm12
							PSUBW xmm7, xmm13
							PSRAW xmm7, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm7, xmm15
							PAND xmm5, xmm15
							PAND xmm6, xmm15

							POR xmm7, xmm8
							POR xmm5, xmm9
							POR xmm6, xmm10


				align 16
					luma_hb_offload:
				;		MOVD QWORD PTR [rdi], xmm0
						MOVD QWORD PTR [rdi + 32], xmm2
						MOVD QWORD PTR [rdi + 64], xmm1
						MOVD QWORD PTR [rdi + 96], xmm3

						MOVD QWORD PTR [rsi], xmm7
						MOVD QWORD PTR [rsi + 32], xmm5
						MOVD QWORD PTR [rsi + 64], xmm6
				;		MOVD QWORD PTR [rsi + 96], xmm4




			align 16
				skip_luma_hb:



			INC r15
			JNZ mb_luma_loop





























			MOV r15, -16

		align 16
			mb_chroma1_loop:
; vertical luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 64] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 64]

				MOV r12, 16
				ADD r12, r15
				MOV r13, r12
				SHR r13, 2
				AND r12, 3
				SHL r12, 7
				LEA r12, [r12 + r13*8]

				LEA rsi, [r11 + r12 + 1024]

				TEST r14d, 80h
				JNZ skip_chroma1_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					CMP r15, -4
					JL no_right_border_1
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 32]
					no_right_border_1:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta






				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 32]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 64]
					MOVD xmm3, QWORD PTR [rsi + 96]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 32]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 64]
					MOVD xmm7, QWORD PTR [rdi + 96]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma1_vb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm5
						PSUBW xmm10, xmm4
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ chroma1_vb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm4

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm5
							PSUBW xmm11, xmm6
							PSUBW xmm11, xmm6
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13
							
							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm6, xmm11

				


						JMP chroma1_vb_offload
					align 16
						chroma1_vb_4:							
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm4
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm4
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm6

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm6
							MOVDQA xmm10, xmm7
							PADDW xmm10, xmm5
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm4
							PADDW xmm9, xmm5
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm5
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm12
							PSUBW xmm4, xmm13
							PSRAW xmm4, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm4, xmm15
							PAND xmm6, xmm15
							PAND xmm5, xmm15

							POR xmm4, xmm8
							POR xmm6, xmm9
							POR xmm5, xmm10



				align 16
					chroma1_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 32], xmm1
						MOVD QWORD PTR [rsi + 64], xmm2
						MOVD QWORD PTR [rsi + 96], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 32], xmm6
						MOVD QWORD PTR [rdi + 64], xmm5
						MOVD QWORD PTR [rdi + 96], xmm4

		align 16
			skip_chroma1_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 64 + 1] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 64 + 1]

				TEST r14d, 80h
				JNZ skip_chroma1_hb
					LEA rdi, [rsi - 128]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 1] ; internal beta
					
					TEST r15, 3
					JNZ no_top_border_1
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 6 + 2] ; top alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 6 + 3] ; top beta

						LEA rdi, [rdi + r9 + 128 + 384]
					no_top_border_1:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 32] ; p2
					MOVD xmm1, QWORD PTR [rdi + 64] ; p1
					MOVD xmm3, QWORD PTR [rdi + 96] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 32] ; q1
					MOVD xmm6, QWORD PTR [rsi + 64] ; q2
					MOVD xmm4, QWORD PTR [rsi + 96] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma1_hb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm6
						PSUBW xmm10, xmm7
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ chroma1_hb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm7

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm6
							PSUBW xmm11, xmm5
							PSUBW xmm11, xmm5
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13

							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm5, xmm11

				


						JMP chroma1_hb_offload
					align 16
						chroma1_hb_4:
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm7
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm7
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm5

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm5
							MOVDQA xmm10, xmm4
							PADDW xmm10, xmm6
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm7
							PADDW xmm9, xmm6
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm6
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm12
							PSUBW xmm7, xmm13
							PSRAW xmm7, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm7, xmm15
							PAND xmm5, xmm15
							PAND xmm6, xmm15

							POR xmm7, xmm8
							POR xmm5, xmm9
							POR xmm6, xmm10


				align 16
					chroma1_hb_offload:
				;		MOVD QWORD PTR [rdi], xmm0
						MOVD QWORD PTR [rdi + 32], xmm2
						MOVD QWORD PTR [rdi + 64], xmm1
						MOVD QWORD PTR [rdi + 96], xmm3

						MOVD QWORD PTR [rsi], xmm7
						MOVD QWORD PTR [rsi + 32], xmm5
						MOVD QWORD PTR [rsi + 64], xmm6
				;		MOVD QWORD PTR [rsi + 96], xmm4




			align 16
				skip_chroma1_hb:



			INC r15
			JNZ mb_chroma1_loop

































			MOV r15, -16


		align 16
			mb_chroma2_loop:
; vertical luma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 96] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 96]

				MOV r12, 16
				ADD r12, r15
				MOV r13, r12
				SHR r13, 2
				AND r12, 3
				SHL r12, 7
				LEA r12, [r12 + r13*8]

				LEA rsi, [r11 + r12 + 1536]

				TEST r14d, 80h
				JNZ skip_chroma2_vb

					LEA rdi, [rsi + 8]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					CMP r15, -4
					JL no_right_border_2
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 4] ; right alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 5] ; right beta

						LEA rdi, [rdi + 2048 - 32]
					no_right_border_2:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha
										
					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta






				
					MOVD xmm0, QWORD PTR [rsi] ; p
					MOVD xmm3, QWORD PTR [rsi + 32]
					PUNPCKLWD xmm0, xmm3
					MOVD xmm2, QWORD PTR [rsi + 64]
					MOVD xmm3, QWORD PTR [rsi + 96]
					PUNPCKLWD xmm2, xmm3

					MOVDQA xmm1, xmm0
					PUNPCKLDQ xmm0, xmm2 ; p3		p2
					PUNPCKHDQ xmm1, xmm2 ; p1		p0

					MOVDQA xmm2, xmm0
					MOVDQA xmm3, xmm1

					PSRLDQ xmm2, 8 ; p2
					PSRLDQ xmm3, 8 ; p0


					MOVD xmm4, QWORD PTR [rdi] ; q
					MOVD xmm7, QWORD PTR [rdi + 32]
					PUNPCKLWD xmm4, xmm7
					MOVD xmm6, QWORD PTR [rdi + 64]
					MOVD xmm7, QWORD PTR [rdi + 96]
					PUNPCKLWD xmm6, xmm7

					MOVDQA xmm5, xmm4
					PUNPCKLDQ xmm4, xmm6 ; q0		q1
					PUNPCKHDQ xmm5, xmm6 ; q2		q3

					MOVDQA xmm6, xmm4
					MOVDQA xmm7, xmm5

					PSRLDQ xmm6, 8 ; q1
					PSRLDQ xmm7, 8 ; q3

			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm4 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm6
					PSUBW xmm10, xmm4 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma2_vb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm5
						PSUBW xmm10, xmm4
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ chroma2_vb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm4
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm6

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm4

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm4, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm5
							PSUBW xmm11, xmm6
							PSUBW xmm11, xmm6
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13
							
							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm6, xmm11

				


						JMP chroma2_vb_offload
					align 16
						chroma2_vb_4:							
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm4
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm4
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm6

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm6
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm6
							MOVDQA xmm10, xmm7
							PADDW xmm10, xmm5
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm4
							PADDW xmm9, xmm5
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm5
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm4, xmm6
							PADDW xmm4, xmm6
							PADDW xmm4, xmm12
							PSUBW xmm4, xmm13
							PSRAW xmm4, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm4, xmm15
							PAND xmm6, xmm15
							PAND xmm5, xmm15

							POR xmm4, xmm8
							POR xmm6, xmm9
							POR xmm5, xmm10



				align 16
					chroma2_vb_offload:
						PUNPCKLWD xmm1, xmm3 ; p10
						PUNPCKLWD xmm0, xmm2 ; p32

						MOVDQA xmm2, xmm0
						PUNPCKLDQ xmm0, xmm1
						PUNPCKHDQ xmm2, xmm1
					
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2
						PSRLDQ xmm1, 8
						PSRLDQ xmm3, 8



						PUNPCKLWD xmm4, xmm6 ; q01
						PUNPCKLWD xmm5, xmm7 ; q23

						MOVDQA xmm6, xmm4
						PUNPCKLDQ xmm6, xmm5
						PUNPCKHDQ xmm4, xmm5

						MOVDQA xmm7, xmm6
						MOVDQA xmm5, xmm4

						PSRLDQ xmm6, 8
						PSRLDQ xmm4, 8
	


						MOVD QWORD PTR [rsi], xmm0
						MOVD QWORD PTR [rsi + 32], xmm1
						MOVD QWORD PTR [rsi + 64], xmm2
						MOVD QWORD PTR [rsi + 96], xmm3

						MOVD QWORD PTR [rdi], xmm7
						MOVD QWORD PTR [rdi + 32], xmm6
						MOVD QWORD PTR [rdi + 64], xmm5
						MOVD QWORD PTR [rdi + 96], xmm4

		align 16
			skip_chroma2_vb:
; horizontal chroma
				MOVZX r14d, BYTE PTR [r11 + r15*2 + 392 + 96 + 1] ; t'co
				SHL r14d, 16
				MOV r14b, [r11 + r15*2 + 392 + 96 + 1]

				TEST r14d, 80h
				JNZ skip_chroma2_hb
					LEA rdi, [rsi - 128]

					MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 0] ; internal alpha
					MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 1] ; internal beta
					
					TEST r15, 3
					JNZ no_top_border_2
						MOVZX r12d, BYTE PTR [r11 + 392 + 96 + 12 + 2] ; top alpha
						MOVZX r13d, BYTE PTR [r11 + 392 + 96 + 12 + 3] ; top beta

						LEA rdi, [rdi + r9 + 128 + 384]
					no_top_border_2:

					SHL r12d, cl
					SHL r13d, cl

					MOVD xmm8, r12d
					PSHUFLW xmm8, xmm8, 0 ; alpha

					MOVD xmm9, r13d
					PSHUFLW xmm9, xmm9, 0 ; beta



					MOVD xmm0, QWORD PTR [rdi] ; p3
					MOVD xmm2, QWORD PTR [rdi + 32] ; p2
					MOVD xmm1, QWORD PTR [rdi + 64] ; p1
					MOVD xmm3, QWORD PTR [rdi + 96] ; p0


					MOVD xmm7, QWORD PTR [rsi] ; q0
					MOVD xmm5, QWORD PTR [rsi + 32] ; q1
					MOVD xmm6, QWORD PTR [rsi + 64] ; q2
					MOVD xmm4, QWORD PTR [rsi + 96] ; q3



			

					MOVDQA xmm10, xmm3
					PSUBW xmm10, xmm7 ; p0 - q0
					PABSW xmm10, xmm10				
					MOVDQA xmm15, xmm8
					PCMPGTW xmm15, xmm10 ; a > (p0 - q0)

					MOVDQA xmm10, xmm1
					PSUBW xmm10, xmm3 ; p1 - p0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (p1 - p0)

					PAND xmm15, xmm14

					MOVDQA xmm10, xmm5
					PSUBW xmm10, xmm7 ; q1 - q0
					PABSW xmm10, xmm10
					MOVDQA xmm14, xmm9
					PCMPGTW xmm14, xmm10 ; b > (q1 - q0)

					PAND xmm15, xmm14

					PMOVMSKB r12d, xmm15

					AND r12d, 0FFh
					JZ skip_chroma2_hb
						MOVDQA xmm14, xmm9
						MOVDQA xmm15, xmm9

						MOVDQA xmm10, xmm2
						PSUBW xmm10, xmm3
						PABSW xmm10, xmm10

						PCMPGTW xmm14, xmm10 ; p mask

						MOVDQA xmm10, xmm6
						PSUBW xmm10, xmm7
						PABSW xmm10, xmm10

						PCMPGTW xmm15, xmm10 ; q mask



						TEST r14d, 40h
						JNZ chroma2_hb_4
							SHL r14d, cl

							MOVD xmm13, r14d
							PSHUFD xmm13, xmm13, 0 ; tc0

							MOVDQA xmm12, xmm13
							PSUBW xmm12, xmm14
							PSUBW xmm12, xmm15 ; tC

							PCMPEQW xmm11, xmm11

							MOVDQA xmm10, xmm7
							PSUBW xmm10, xmm3
							PSUBW xmm10, xmm11
							PSLLW xmm10, 2

							PADDW xmm10, xmm1
							PSUBW xmm10, xmm5

							PSRAW xmm10, 3
							PMINSW xmm10, xmm12
							PSIGNW xmm12, xmm11
							PMAXSW xmm10, xmm12 ; delta

							PSIGNW xmm11, xmm11
							PADDW xmm11, xmm3
							PADDW xmm11, xmm7

							PSRAW xmm11, 1

							PADDW xmm3, xmm10 ; p0 + delta
							PSUBW xmm7, xmm10 ; q0 - delta

							MOVDQA xmm10, xmm11
							PADDW xmm10, xmm2
							PSUBW xmm10, xmm1
							PSUBW xmm10, xmm1
							PSRAW xmm10, 1

							PADDW xmm11, xmm6
							PSUBW xmm11, xmm5
							PSUBW xmm11, xmm5
							PSRAW xmm11, 1

							PCMPEQW xmm12, xmm12

							PMINSW xmm10, xmm13
							PMINSW xmm11, xmm13

							PSIGNW xmm13, xmm12

							PMAXSW xmm10, xmm13 ; p1 delta
							PMAXSW xmm11, xmm13 ; q1 delta

							PAND xmm10, xmm14
							PAND xmm11, xmm15

							PADDW xmm1, xmm10
							PADDW xmm5, xmm11

				


						JMP chroma2_hb_offload
					align 16
						chroma2_hb_4:
							PCMPEQW xmm13, xmm13
							PSLLW xmm13, 1

							PSRLW xmm8, 2						
							PSUBW xmm8, xmm13
							
							MOVDQA xmm10, xmm3
							PSUBW xmm10, xmm7
							PABSW xmm10, xmm10
							PCMPGTW xmm8, xmm10

							PAND xmm14, xmm8
							PAND xmm15, xmm8


							MOVDQA xmm11, xmm3 ; p0
							MOVDQA xmm12, xmm1 ; p1


							MOVDQA xmm9, xmm1
							MOVDQA xmm10, xmm0
							PADDW xmm10, xmm2
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2p3 + 2p2 + 2

							PADDW xmm9, xmm3
							PADDW xmm9, xmm2
							PADDW xmm9, xmm7
							PSUBW xmm9, xmm13 ; p2 + p1 + p0 + q0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm2
							PADDW xmm8, xmm5

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm14
							PAND xmm9, xmm14
							PAND xmm10, xmm14

							PADDW xmm3, xmm1
							PADDW xmm3, xmm1
							PADDW xmm3, xmm5
							PSUBW xmm3, xmm13
							PSRAW xmm3, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm14, xmm13

							PAND xmm3, xmm14
							PAND xmm1, xmm14
							PAND xmm2, xmm14

							POR xmm3, xmm8
							POR xmm1, xmm9
							POR xmm2, xmm10





							PSLLW xmm13, 1
							

							MOVDQA xmm9, xmm5
							MOVDQA xmm10, xmm4
							PADDW xmm10, xmm6
							PSLLW xmm10, 1
							PSUBW xmm10, xmm13 ; 2q3 + 2q2 + 2

							PADDW xmm9, xmm7
							PADDW xmm9, xmm6
							PADDW xmm9, xmm11
							PSUBW xmm9, xmm13 ; q2 + q1 + q0 + p0 + 2

							PADDW xmm10, xmm9

							MOVDQA xmm8, xmm9
							PSLLW xmm8, 1
							PSUBW xmm8, xmm6
							PADDW xmm8, xmm12

							PSRAW xmm8, 3
							PSRAW xmm9, 2
							PSRAW xmm10, 3
							
							PAND xmm8, xmm15
							PAND xmm9, xmm15
							PAND xmm10, xmm15

							PADDW xmm7, xmm5
							PADDW xmm7, xmm5
							PADDW xmm7, xmm12
							PSUBW xmm7, xmm13
							PSRAW xmm7, 2

							PCMPEQW xmm13, xmm13
							PXOR xmm15, xmm13

							PAND xmm7, xmm15
							PAND xmm5, xmm15
							PAND xmm6, xmm15

							POR xmm7, xmm8
							POR xmm5, xmm9
							POR xmm6, xmm10


				align 16
					chroma2_hb_offload:
				;		MOVD QWORD PTR [rdi], xmm0
						MOVD QWORD PTR [rdi + 32], xmm2
						MOVD QWORD PTR [rdi + 64], xmm1
						MOVD QWORD PTR [rdi + 96], xmm3

						MOVD QWORD PTR [rsi], xmm7
						MOVD QWORD PTR [rsi + 32], xmm5
						MOVD QWORD PTR [rsi + 64], xmm6
				;		MOVD QWORD PTR [rsi + 96], xmm4




			align 16
				skip_chroma2_hb:



			INC r15
			JNZ mb_chroma2_loop







	JMP d_loop
	
done:

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	RET
H264_Deblock_0	ENDP


END

