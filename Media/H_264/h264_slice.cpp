/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"
#include "System\SysUtils.h"

// never actually tested it, CABAC hit my roof hard enough

// non-entropy ====================================================================================================================================================================================================================================================

UI_64 Video::H264::GetSliceData() {
	UI_64 result(1);


	if (decoder_cfg.frame_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {
		if (decoder_cfg.flag_set & FLAG_SECOND_FIELD) { // 
			decoder_cfg.frame_ptr += vui_params.second_field_offset;
		}
		
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);

		decoder_cfg.line_counter = current_slice.first_mb / decoder_cfg.slice_width;
		decoder_cfg.line_ptr = current_slice.first_mb % decoder_cfg.slice_width;
		

		if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) {
			decoder_cfg.current_mb_offset = decoder_cfg.line_counter*(decoder_cfg.slice_width + 1)*4*256*2;
			if (current_slice.id_rec.bottom_field_flag) decoder_cfg.current_mb_offset += decoder_cfg.pline_offset;			
		} else {
			decoder_cfg.current_mb_offset = decoder_cfg.line_counter*(decoder_cfg.slice_width + 1)*4*256;
		}

		
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr + decoder_cfg.current_mb_offset, 4*256*sizeof(short), 0, 0);


		decoder_cfg.mb_u = decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr + decoder_cfg.current_mb_offset);
		
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


		decoder_cfg.current_mb_offset += decoder_cfg.line_ptr*4*256;

		decoder_cfg.frame_ptr += decoder_cfg.current_mb_offset+256;
		
		
		decoder_cfg.current_mb_ptr = -1;
		decoder_cfg.flag_set = 0;
		
		for (IncMbAddr();decoder_cfg.IsMoreRBSP();IncMbAddr()) {
			if (decoder_cfg.line_counter >= vui_params.c_height) {
				break;
			}
			

			switch (current_slice._type) {
				case 1: case 6: // B
					decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_B;
				case 0: case 5: // P
				case 3: case 8: // SP
					if (decoder_cfg.NextBit()) {
						decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_SKIP;
						decoder_cfg.mb_x->field_decoding_flag = current_slice.id_rec.field_pic_flag;

						if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_B) {
							result = SkipPredB();
						} else {
							result = SkipPredP();
						}

						decoder_cfg.MapRL0();

						if (result) {
							SamplesCopy();
						} else {
							InterPredLuma(decoder_cfg.frame_ptr, 0);
							H264_SamplesShake4x4(decoder_cfg.frame_ptr);

							switch (decoder_cfg.chroma_array_type) {
								case 1:
									InterPredChroma1(decoder_cfg.frame_ptr + 256, 1);
									H264_ChromaShake1(decoder_cfg.frame_ptr + 256);
									InterPredChroma1(decoder_cfg.frame_ptr + 512, 2);
									H264_ChromaShake1(decoder_cfg.frame_ptr + 512);
									
								break;
								case 2:
									InterPredChroma2(decoder_cfg.frame_ptr + 256, 1);
									H264_ChromaShake2(decoder_cfg.frame_ptr + 256);
									InterPredChroma2(decoder_cfg.frame_ptr, 2);
									H264_ChromaShake2(decoder_cfg.frame_ptr + 512);
								break;
								default:
									InterPredLuma(decoder_cfg.frame_ptr + 256, 1);
									H264_SamplesShake4x4(decoder_cfg.frame_ptr + 256);
									InterPredLuma(decoder_cfg.frame_ptr + 512, 2);
									H264_SamplesShake4x4(decoder_cfg.frame_ptr + 512);
							}
						}

						result = 2;
					}

					if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_B) == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_P;
				break;

			}


			if (result & 1) {
				if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) {
					if (((decoder_cfg.current_mb_ptr & 1)==0) || ((decoder_cfg.current_mb_ptr & 1) && (result & 2))) {
						decoder_cfg.mb_x->field_decoding_flag = decoder_cfg.NextBit();
					} else {
						if (decoder_cfg.mb_a) decoder_cfg.mb_x->field_decoding_flag = decoder_cfg.mb_a->field_decoding_flag;
						else if (decoder_cfg.mb_b) decoder_cfg.mb_x->field_decoding_flag = decoder_cfg.mb_b->field_decoding_flag;
					}
				} else {
					decoder_cfg.mb_x->field_decoding_flag = current_slice.id_rec.field_pic_flag;
				}

				if (decoder_cfg.flag_set & FLAG_FIELD_PAIR) decoder_cfg.mb_x->field_decoding_flag = 0x10;

				switch (current_slice._type) {
					case 3: case 8: // SP	
						decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_S;
					case 0: case 5: // P
						GetMBLayerP();
					break;
					case 1: case 6: // B
						GetMBLayerB();
					break;
					case 2: case 7: // I
						GetMBLayerI();
					break;
					case 4: case 9: // SI
						GetMBLayerSI();
					break;
				}

				result &= 1;
			}

			result &= 2;

			
			if ((decoder_cfg.flag_set & FLAG_AFRO_FRAME) && ((decoder_cfg.current_mb_ptr & 1) == 0)) {
				result |= 1;
			} else {
				if (decoder_cfg.NextBit() == 0) result |= 1;
			}

			if ((result & 1) == 0) break;
		}	

		if ((result & 1) == 0) {
			result = decoder_cfg.IsMoreRBSP();
		} else {
			result = -94;
			result <<= 32;
		}

		if (unsigned int * vp = reinterpret_cast<unsigned int *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {

			vui_params.num_units_tick = current_slice.id_rec.field_pic_flag;
			vui_params.time_scale = current_slice.id_rec.bottom_field_flag;

			System::MemoryCopy(vp, &vui_params, sizeof(VUI));

			pic_list[decoder_cfg.current_frame_ptr].buffer.Release();
		}

		pic_list[decoder_cfg.current_frame_ptr].buffer.Release();

	}

	return result;
}

UI_64 Video::H264::GetMBLayerP() {
	unsigned char no_sub_mb_part_size_lt8x8(1);
	unsigned int result = H264_DS_GolombUnsigned(&decoder_cfg);

 	if (result > 4) {
		result -= 5;
		decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I;

		if (result == 25) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I_PCM;
	}
	
	if (result == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_0;
	decoder_cfg.mb_x->flag_type |= (result | FLAG_MB_TYPE_P);



	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


		decoder_cfg.mb_x->sub[0].type_flag = decoder_cfg.mb_x->sub[1].type_flag = decoder_cfg.mb_x->sub[2].type_flag = decoder_cfg.mb_x->sub[3].type_flag = VAL_SUB_PMODE_L0;

		if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) < 3) {
			switch (decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) {
				case 1:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_PART_V;
				break;
				case 2:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_PART_H;
				break;

			}

			MBPred();

		} else { // P_88
			decoder_cfg.mb_x->flag_type |= (FLAG_MB_PART_V | FLAG_MB_PART_H);

			SMBPPred();

			for (unsigned char i(0);i<4;i++) {
				if (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) no_sub_mb_part_size_lt8x8 = 0;
			}
		}

		result = H264_DS_GolombUnsigned(&decoder_cfg);

		if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
			if (result < 48) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map12[1][result];
		} else {
			if (result < 16) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map03[1][result];
		}


		if ((decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) && (current_slice.pic_set->trasnform_8x8_mode_flag) && (no_sub_mb_part_size_lt8x8)) {
			if (decoder_cfg.NextBit()) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;

		}


		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {			
			Residual();
		} else {
			Restore();
		}

	} else {
		GetMBLayerI();
	}
	

	return 0;

}

UI_64 Video::H264::GetMBLayerB() {
	unsigned char no_sub_mb_part_size_lt8x8(1);

	unsigned int result = H264_DS_GolombUnsigned(&decoder_cfg);
	if (result > 22) {
		result -= 23;
		decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I;

		if (result == 25) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I_PCM;
	}
	
	if (result == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_0;

	decoder_cfg.mb_x->flag_type |= (result | FLAG_MB_TYPE_B);

	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		
		if ((decoder_cfg.mb_x->flag_type & (FLAG_MB_TYPE_0 | MASK_MB_TYPE_ID)) < 22) {
			decoder_cfg.mb_x->flag_type |= bmb_part_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID];
			decoder_cfg.mb_x->sub[0].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][0];
			decoder_cfg.mb_x->sub[1].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][1];
			decoder_cfg.mb_x->sub[2].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][2];
			decoder_cfg.mb_x->sub[3].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][3];
			

			MBPred();				
					
		} else { // B_88
			if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) == 22) {

				SMBBPred();

				for (unsigned char i(0);i<4;i++) {
					if ((decoder_cfg.mb_x->sub[i].type_flag & MASK_SUB_TYPE)) {
						if (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) no_sub_mb_part_size_lt8x8 = 0;
					} else {
						if (current_slice.seq_set->direct_8x8_inference_flag == 0) no_sub_mb_part_size_lt8x8 = 0;
					}
				}
			}			
		}

		result = H264_DS_GolombUnsigned(&decoder_cfg);

		if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
			if (result < 48) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map12[1][result];
		} else {
			if (result < 16) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map03[1][result];
		}
			
		if ((decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) && (current_slice.pic_set->trasnform_8x8_mode_flag) && (no_sub_mb_part_size_lt8x8) && ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) || (current_slice.seq_set->direct_8x8_inference_flag))) {
			if (decoder_cfg.NextBit()) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;

		}

		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
			Residual();
		}
	} else {
		// intra
		GetMBLayerI();
	}



	return 0;

}


UI_64 Video::H264::GetMBLayerI() {
	unsigned int ic(16>>(current_slice.seq_set->chroma_height + current_slice.seq_set->chroma_width)), ti(0), result(0);
	
	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		result = H264_DS_GolombUnsigned(&decoder_cfg);
		decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I;

		if (result == 25) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I_PCM;
		if (result == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_0;

	}


	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I_PCM) == 0) {
		if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_0) && (current_slice.pic_set->trasnform_8x8_mode_flag)) {
			if (decoder_cfg.NextBit()) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;
		}


		if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_0) {
			// mb_pred
			MBPredI();

			if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
				decoder_cfg.mb_x->intra_chroma_pred_mode = H264_DS_GolombUnsigned(&decoder_cfg);
			}

			result = H264_DS_GolombUnsigned(&decoder_cfg);

			if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
				if (result < 48) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map12[0][result];
			} else {
				if (result < 16) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map03[0][result];
			}

			if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
				Residual();
			} else {
				Restore();
			}

		} else {
			if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) > 12) decoder_cfg.mb_x->flag_type |= MASK_MB_LUMA;

			switch (((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID)-1) >> 2) {
				case 1: case 4:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_CHROMA_DC;
				break;
				case 2: case 5:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_CHROMA_AC;
				break;

			}

			if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
				decoder_cfg.mb_x->intra_chroma_pred_mode = H264_DS_GolombUnsigned(&decoder_cfg);
			}

			Residual();
		}

	} else { // I_PCM
		decoder_cfg.mb_x->qp[1] = current_slice.pic_set->chroma_qpinx_offset[1];
		decoder_cfg.mb_x->qp[2] = current_slice.pic_set->chroma_qpinx_offset[2];


		decoder_cfg.ByteAlign();

		result = current_slice.seq_set->cmp_depth[0]+8;

		for (unsigned int i(0);i<16;i++) {// pcm_luma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+1] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+2] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+3] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+4] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+5] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+6] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+7] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+8] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+9] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+10] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+11] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+12] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+13] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+14] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+15] = H264_DS_GetNumber(&decoder_cfg, result);

		}

		result = current_slice.seq_set->cmp_depth[1] + 8;

		for (unsigned int i(0);i<ic;i++) { // pcm_chroma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+1] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+2] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+3] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+256+4] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+5] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+6] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+7] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+256+8] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+9] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+10] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+11] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+256+12] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+13] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+14] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+256+15] = H264_DS_GetNumber(&decoder_cfg, result);
		}

		for (unsigned int i(0);i<ic;i++) { // pcm_chroma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+1] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+2] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+3] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+512+4] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+5] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+6] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+7] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+512+8] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+9] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+10] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+11] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+512+12] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+13] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+14] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+512+15] = H264_DS_GetNumber(&decoder_cfg, result);

		}

	}
	
	return 0;
}

UI_64 Video::H264::GetMBLayerSI() {

	unsigned int result = H264_DS_GolombUnsigned(&decoder_cfg);
	decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I;

	if (result > 0) {
		result -= 1;	

		if (result == 25) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_I_PCM;

	} else {
		decoder_cfg.mb_x->flag_type |= (result | FLAG_MB_TYPE_S);
	}
	
	if (result == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_0;

	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		MBPredI();

		if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
			decoder_cfg.mb_x->intra_chroma_pred_mode = H264_DS_GolombUnsigned(&decoder_cfg);
		}
		
		result = H264_DS_GolombUnsigned(&decoder_cfg);

		if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
			if (result < 48) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map12[1][result];
		} else {
			if (result < 16) decoder_cfg.mb_x->flag_type |= decoder_cfg.cbp_map03[1][result];
		}

		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
			Residual();
		} else {
			Restore();
		}
	} else {
		// intra
		GetMBLayerI();
	}
		
	return 0;

}

UI_64 Video::H264::MBPredI() {
	const MacroBlockSet * mbl(0), * mbu(0);
	unsigned char dc_pred_mode(0), val_a(0);

	if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // 8x8		
		dc_pred_mode = ((decoder_cfg.mb_a->flag_type & decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_a->flag_type & decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_a->intra_pred_mode[5] < decoder_cfg.mb_b->intra_pred_mode[10])?decoder_cfg.mb_a->intra_pred_mode[5]:decoder_cfg.mb_b->intra_pred_mode[10];

		if (decoder_cfg.NextBit()) {
			decoder_cfg.mb_x->intra_pred_mode[0] = decoder_cfg.mb_x->intra_pred_mode[1] = decoder_cfg.mb_x->intra_pred_mode[2] = decoder_cfg.mb_x->intra_pred_mode[3] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[0] = H264_DS_GetNumber(&decoder_cfg, 3) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[0] >= val_a) decoder_cfg.mb_x->intra_pred_mode[0]++;

			decoder_cfg.mb_x->intra_pred_mode[1] = decoder_cfg.mb_x->intra_pred_mode[2] = decoder_cfg.mb_x->intra_pred_mode[3] = decoder_cfg.mb_x->intra_pred_mode[0];
		}




		dc_pred_mode = ((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_x->intra_pred_mode[1] < decoder_cfg.mb_b->intra_pred_mode[14])?decoder_cfg.mb_x->intra_pred_mode[1]:decoder_cfg.mb_b->intra_pred_mode[14];

		if (decoder_cfg.NextBit()) {
			decoder_cfg.mb_x->intra_pred_mode[4] = decoder_cfg.mb_x->intra_pred_mode[5] = decoder_cfg.mb_x->intra_pred_mode[6] = decoder_cfg.mb_x->intra_pred_mode[7] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[4] = H264_DS_GetNumber(&decoder_cfg, 3) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[4] >= val_a) decoder_cfg.mb_x->intra_pred_mode[4]++;

			decoder_cfg.mb_x->intra_pred_mode[5] = decoder_cfg.mb_x->intra_pred_mode[6] = decoder_cfg.mb_x->intra_pred_mode[7] = decoder_cfg.mb_x->intra_pred_mode[4];
		}




		dc_pred_mode = ((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_a->intra_pred_mode[13] < decoder_cfg.mb_x->intra_pred_mode[2])?decoder_cfg.mb_a->intra_pred_mode[13]:decoder_cfg.mb_x->intra_pred_mode[2];

		if (decoder_cfg.NextBit()) {
			decoder_cfg.mb_x->intra_pred_mode[8] = decoder_cfg.mb_x->intra_pred_mode[9] = decoder_cfg.mb_x->intra_pred_mode[10] = decoder_cfg.mb_x->intra_pred_mode[11] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[8] = H264_DS_GetNumber(&decoder_cfg, 3) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[8] >= val_a) decoder_cfg.mb_x->intra_pred_mode[8]++;

			decoder_cfg.mb_x->intra_pred_mode[9] = decoder_cfg.mb_x->intra_pred_mode[10] = decoder_cfg.mb_x->intra_pred_mode[11] = decoder_cfg.mb_x->intra_pred_mode[8];
		}

		



		val_a = (decoder_cfg.mb_x->intra_pred_mode[9] < decoder_cfg.mb_x->intra_pred_mode[6])?decoder_cfg.mb_x->intra_pred_mode[9]:decoder_cfg.mb_x->intra_pred_mode[6];

		if (decoder_cfg.NextBit()) {
			decoder_cfg.mb_x->intra_pred_mode[12] = decoder_cfg.mb_x->intra_pred_mode[13] = decoder_cfg.mb_x->intra_pred_mode[14] = decoder_cfg.mb_x->intra_pred_mode[15] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[12] = H264_DS_GetNumber(&decoder_cfg, 3) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[12] >= val_a) decoder_cfg.mb_x->intra_pred_mode[12]++;

			decoder_cfg.mb_x->intra_pred_mode[13] = decoder_cfg.mb_x->intra_pred_mode[14] = decoder_cfg.mb_x->intra_pred_mode[15] = decoder_cfg.mb_x->intra_pred_mode[12];
		}
		
	} else {
		for (unsigned int bidx(0);bidx<16;bidx++) {
			mbl = decoder_cfg.mb_a;
			if (left_map[bidx]) mbl = decoder_cfg.mb_x;

			mbu = decoder_cfg.mb_b;
			if (up_map[bidx]) mbu = decoder_cfg.mb_x;
						
			dc_pred_mode = ((mbl->flag_type & mbu->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
			if (dc_pred_mode == 0) if ((mbl->flag_type & mbu->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

			val_a = 0;
			if (dc_pred_mode == 0) val_a = (mbl->intra_pred_mode[left_idx[bidx]] < mbu->intra_pred_mode[up_idx[bidx]])?mbl->intra_pred_mode[left_idx[bidx]]:mbu->intra_pred_mode[up_idx[bidx]];


			if (decoder_cfg.NextBit()) {
				decoder_cfg.mb_x->intra_pred_mode[bidx] = val_a;

			} else {
				decoder_cfg.mb_x->intra_pred_mode[bidx] = H264_DS_GetNumber(&decoder_cfg, 3) - 2;
				if (decoder_cfg.mb_x->intra_pred_mode[bidx] >= val_a) decoder_cfg.mb_x->intra_pred_mode[bidx]++;
			}
		}
	}
		

	return 0;
}


UI_64 Video::H264::MBPred() {

	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0)
			decoder_cfg.mb_x->sub[0].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
		
		switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
			case FLAG_MB_PART_V:
				decoder_cfg.mb_x->sub[1].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
				if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L0)
					decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[2].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
			break;
			case FLAG_MB_PART_H:
				decoder_cfg.mb_x->sub[2].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
				if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L0)
					decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[1].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
			break;
			case 0:
				decoder_cfg.mb_x->sub[1].ref_idx[0] = decoder_cfg.mb_x->sub[2].ref_idx[0] = decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
			break;
			
		}
	}

	if ((current_slice.num_ref_idx_active[1] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1)
			decoder_cfg.mb_x->sub[0].ref_idx[1] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[1]);
		
		switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {			
			case FLAG_MB_PART_V:
				decoder_cfg.mb_x->sub[1].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
				if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L1)
					decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[2].ref_idx[1] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[1]);
			break;
			case FLAG_MB_PART_H:
				decoder_cfg.mb_x->sub[2].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
				if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L1)
					decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[1].ref_idx[1] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[1]);
			break;
			case 0:
				decoder_cfg.mb_x->sub[1].ref_idx[1] = decoder_cfg.mb_x->sub[2].ref_idx[1] = decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
			break;
		}
	}

	if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0) {
		decoder_cfg.mb_x->sub[0].mvd[0][0][0] = decoder_cfg.GolombSigned();
		decoder_cfg.mb_x->sub[0].mvd[0][0][1] = decoder_cfg.GolombSigned();

		for (unsigned int i(1);i<4;i++) {
			decoder_cfg.mb_x->sub[0].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
			decoder_cfg.mb_x->sub[0].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
		}

	}

	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
		case FLAG_MB_PART_V:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L0) {
				decoder_cfg.mb_x->sub[2].mvd[0][0][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[2].mvd[0][0][1] = decoder_cfg.GolombSigned();

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[2].mvd[0][0][0];
					decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[2].mvd[0][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[2].mvd[0][0][0];
					decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[2].mvd[0][0][1];
				}

			}
		break;
		case FLAG_MB_PART_H:		
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}

			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L0) {
				decoder_cfg.mb_x->sub[1].mvd[0][0][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[1].mvd[0][0][1] = decoder_cfg.GolombSigned();

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[1].mvd[0][0][0];
					decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[1].mvd[0][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[1].mvd[0][0][0];
					decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[1].mvd[0][0][1];
				}
			}
		break;
		
	}


	if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1) {
		decoder_cfg.mb_x->sub[0].mvd[1][0][0] = decoder_cfg.GolombSigned();
		decoder_cfg.mb_x->sub[0].mvd[1][0][1] = decoder_cfg.GolombSigned();

		for (unsigned int i(1);i<4;i++) {
			decoder_cfg.mb_x->sub[0].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
			decoder_cfg.mb_x->sub[0].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
		}
	}

	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
		case FLAG_MB_PART_V:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L1) {
				decoder_cfg.mb_x->sub[2].mvd[1][0][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[2].mvd[1][0][1] = decoder_cfg.GolombSigned();

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[2].mvd[1][0][0];
					decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[2].mvd[1][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[2].mvd[1][0][0];
					decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[2].mvd[1][0][1];
				}


			}
		break;
		case FLAG_MB_PART_H:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}


			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L1) {
				decoder_cfg.mb_x->sub[1].mvd[1][0][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[1].mvd[1][0][1] = decoder_cfg.GolombSigned();


				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[1].mvd[1][0][0];
					decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[1].mvd[1][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[1].mvd[1][0][0];
					decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[1].mvd[1][0][1];
				}
			}
		break;
		case 0:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

		break;
	}

	return 0;
}



UI_64 Video::H264::SMBPPred() {
	
	decoder_cfg.mb_x->sub[0].type_flag = H264_DS_GolombUnsigned(&decoder_cfg);
	decoder_cfg.mb_x->sub[0].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[0].type_flag];
	decoder_cfg.mb_x->sub[1].type_flag = H264_DS_GolombUnsigned(&decoder_cfg);
	decoder_cfg.mb_x->sub[1].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[1].type_flag];
	decoder_cfg.mb_x->sub[2].type_flag = H264_DS_GolombUnsigned(&decoder_cfg);
	decoder_cfg.mb_x->sub[2].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[2].type_flag];
	decoder_cfg.mb_x->sub[3].type_flag = H264_DS_GolombUnsigned(&decoder_cfg);
	decoder_cfg.mb_x->sub[3].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[3].type_flag];
	


	if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) != 4)
	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		decoder_cfg.mb_x->sub[0].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
		decoder_cfg.mb_x->sub[1].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
		decoder_cfg.mb_x->sub[2].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
		decoder_cfg.mb_x->sub[3].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
	}

	for (unsigned int i(0);i<4;i++)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];

		break;
		case FLAG_SUB_PART_V:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][2][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][2][1];

		break;
		case FLAG_SUB_PART_H:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][1][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.GolombSigned();

		break;
	}

	return 0;
}

UI_64 Video::H264::SMBBPred() {

	for (unsigned int i(0);i<4;i++) {
		decoder_cfg.mb_x->sub[i].type_flag = H264_DS_GolombUnsigned(&decoder_cfg);
		decoder_cfg.mb_x->sub[i].type_flag |= bsubtype_pred_mode[decoder_cfg.mb_x->sub[i].type_flag];

	}
	

	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		for (unsigned int i(0);i<4;i++)
		if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L0)
			decoder_cfg.mb_x->sub[i].ref_idx[0] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[0]);
	}


	if ((current_slice.num_ref_idx_active[1] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		for (unsigned int i(0);i<4;i++)
		if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L1)
			decoder_cfg.mb_x->sub[i].ref_idx[1] = cavlc.GetRefIdx(decoder_cfg, current_slice.num_ref_idx_active[1]);
		
	}

	for (unsigned int i(0);i<4;i++)
	if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L0)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];

		break;
		case FLAG_SUB_PART_V:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][2][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][2][1];

		break;
		case FLAG_SUB_PART_H:
			decoder_cfg.mb_x->sub[i].mvd[0][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][1][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			for (unsigned int j(0);j<4;j++) {
				decoder_cfg.mb_x->sub[i].mvd[0][j][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[i].mvd[0][j][1] = decoder_cfg.GolombSigned();
			}

		break;
	}

	for (unsigned int i(0);i<4;i++)
	if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L1)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			decoder_cfg.mb_x->sub[i].mvd[1][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][0][1] = decoder_cfg.GolombSigned();

			decoder_cfg.mb_x->sub[i].mvd[1][1][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][1][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
			decoder_cfg.mb_x->sub[i].mvd[1][2][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][2][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];

		break;
		case FLAG_SUB_PART_V:
			decoder_cfg.mb_x->sub[i].mvd[1][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][1][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][1][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[1][2][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][2][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][2][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][2][1];

		break;
		case FLAG_SUB_PART_H:
			decoder_cfg.mb_x->sub[i].mvd[1][0][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][0][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][2][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][2][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
				
			decoder_cfg.mb_x->sub[i].mvd[1][1][0] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][1][1] = decoder_cfg.GolombSigned();
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][1][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			for (unsigned int j(0);j<4;j++) {
				decoder_cfg.mb_x->sub[i].mvd[1][j][0] = decoder_cfg.GolombSigned();
				decoder_cfg.mb_x->sub[i].mvd[1][j][1] = decoder_cfg.GolombSigned();
			}

		break;
	}

	return 0;
}

UI_64 Video::H264::Residual() {
	unsigned short i16flag(0x0002);

	SetQPs();

// residual luma

	if ((decoder_cfg.mb_x->flag_type & (FLAG_MB_TYPE_I | FLAG_MB_TYPE_0)) == FLAG_MB_TYPE_I) {
		ResidualDC(decoder_cfg.frame_ptr, 0); // luma Intra 16x16 DC, category 0
		i16flag = 0x0001;
	}
	
	if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 5
		if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8(decoder_cfg.frame_ptr, 0, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 5);
			
		RestoreSamples8x8(decoder_cfg.frame_ptr, 0);
	} else { // category 2
		if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4(decoder_cfg.frame_ptr, 0, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag);
			// sample construction
		RestoreSamples4x4(decoder_cfg.frame_ptr, 0);

	}


	switch (decoder_cfg.chroma_array_type) {
		case 1:
			if (decoder_cfg.mb_x->flag_type & (FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) { // Chroma DC, category 3
				ResidualChroma1DC(decoder_cfg.frame_ptr, 1);
				ResidualChroma1DC(decoder_cfg.frame_ptr, 2);
			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_CHROMA_AC) { // Chroma AC, category 4
				ResidualChroma1AC(decoder_cfg.frame_ptr, 1, 4);
				ResidualChroma1AC(decoder_cfg.frame_ptr, 2, 4);
			}

			RestoreChroma1(decoder_cfg.frame_ptr, 1);
			RestoreChroma1(decoder_cfg.frame_ptr, 2);
		break;
		case 2:
			if (decoder_cfg.mb_x->flag_type & (FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) { // Chroma DC, category 3
				ResidualChroma2DC(decoder_cfg.frame_ptr, 1);
				ResidualChroma2DC(decoder_cfg.frame_ptr, 2);
			}
		
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_CHROMA_AC) { // Chroma AC, category 4
				ResidualChroma2AC(decoder_cfg.frame_ptr, 1, 4);
				ResidualChroma2AC(decoder_cfg.frame_ptr, 2, 4);
			}

			RestoreChroma2(decoder_cfg.frame_ptr, 1);
			RestoreChroma2(decoder_cfg.frame_ptr, 2);
		break;
		case 3:			
		// residual Cb
			if (i16flag == 0x0001) {
				ResidualDC(decoder_cfg.frame_ptr, 1); // Cb Intra 16x16 DC, category 6

			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // caetgory 9
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8(decoder_cfg.frame_ptr, 1, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 9);
			
				RestoreSamples8x8(decoder_cfg.frame_ptr, 1);
			} else {
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4(decoder_cfg.frame_ptr, 1, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag | 6);

				RestoreSamples4x4(decoder_cfg.frame_ptr, 1);
			}


		// residual Cr
			if (i16flag == 0x0001) {
				ResidualDC(decoder_cfg.frame_ptr, 2); // Cr Intra 16x16 DC, category 10
			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 13
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8(decoder_cfg.frame_ptr, 2, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 13);
		
				RestoreSamples8x8(decoder_cfg.frame_ptr, 2);
			} else {
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4(decoder_cfg.frame_ptr, 2, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag | 10);

				RestoreSamples4x4(decoder_cfg.frame_ptr, 2);
			}


		break;
	}





	return 0;
}





UI_64 Video::H264::Residual8x8(short * co_ptr, unsigned short cmp_sel, unsigned int cat_idx) {
	UI_64 result(1);
	unsigned int pass_val((cat_idx & MASK_MB_LUMA) >> 15);

	const MacroBlockSet * mbl(decoder_cfg.mb_a), * mbu(decoder_cfg.mb_b);
	int nC(0);
	
	co_ptr += (cmp_sel<<8);


	for (unsigned int idx(0);idx<4;idx++, co_ptr += 64) {
		pass_val >>= 1;		
		if ((pass_val & 1) == 0) continue;






		if (left_map[(idx<<2)]) mbl = decoder_cfg.mb_x;
		if (up_map[(idx<<2)]) mbu = decoder_cfg.mb_x;	
		
		nC = 0;
		if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
		else {
			nC = mbl->coeff_token[cmp_sel][left_idx[(idx<<2)]] & 0x1F;
		}

		if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
		else {
			nC += mbu->coeff_token[cmp_sel][up_idx[(idx<<2)]] & 0x1F;
		}

		if ((mbl->flag_type & FLAG_MB_AVAILABLE) && (mbu->flag_type & FLAG_MB_AVAILABLE)) {
			nC++;
			nC >>= 1;
		}

		switch (nC) {
			case 0: case 1:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken0(decoder_cfg);
			break;
			case 2: case 3:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken1(decoder_cfg);
			break;
			case 4: case 5: case 6: case 7:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken2(decoder_cfg);
			break;
			default:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken3(decoder_cfg);
		}

		cavlc.GetCoeffLevels(decoder_cfg, co_ptr, ((idx<<2) & 0x3F00) | decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)]);
		
	}

	return 0;
}


UI_64 Video::H264::Residual4x4(short * co_ptr, unsigned short cmp_sel, unsigned int cat_idx) { // cat 1, 2, 7, 8, 11, 12
	UI_64 result(0);
	unsigned int pass_val((cat_idx & MASK_MB_LUMA) >> 15);
	const MacroBlockSet * mbl(decoder_cfg.mb_a), * mbu(decoder_cfg.mb_b);
	int nC(0);


	co_ptr += (cmp_sel<<8);

	
	
	for (unsigned int idx(0);idx<4;idx++, co_ptr += 64) {
		pass_val >>= 1;		
		if ((pass_val & 1) == 0) continue;






		if (left_map[(idx<<2)]) mbl = decoder_cfg.mb_x;
		if (up_map[(idx<<2)]) mbu = decoder_cfg.mb_x;	
		
		nC = 0;
		if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
		else {
			nC = mbl->coeff_token[cmp_sel][left_idx[(idx<<2)]] & 0x1F;
		}

		if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
		else {
			nC += mbu->coeff_token[cmp_sel][up_idx[(idx<<2)]] & 0x1F;
		}

		if ((mbl->flag_type & FLAG_MB_AVAILABLE) && (mbu->flag_type & FLAG_MB_AVAILABLE)) {
			nC++;
			nC >>= 1;
		}

		switch (nC) {
			case 0: case 1:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken0(decoder_cfg);
			break;
			case 2: case 3:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken1(decoder_cfg);
			break;
			case 4: case 5: case 6: case 7:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken2(decoder_cfg);
			break;
			default:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)] = cavlc.GetCoeffToken3(decoder_cfg);
		}

		cavlc.GetCoeffLevels(decoder_cfg, co_ptr, ((idx<<2) & 0x0F00) | decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2)]);









		if (left_map[(idx<<2) | 1]) mbl = decoder_cfg.mb_x;
		if (up_map[(idx<<2) | 1]) mbu = decoder_cfg.mb_x;	
		
		nC = 0;
		if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
		else {
			nC = mbl->coeff_token[cmp_sel][left_idx[(idx<<2) | 1]] & 0x1F;
		}

		if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
		else {
			nC += mbu->coeff_token[cmp_sel][up_idx[(idx<<2) | 1]] & 0x1F;
		}

		if ((mbl->flag_type & FLAG_MB_AVAILABLE) && (mbu->flag_type & FLAG_MB_AVAILABLE)) {
			nC++;
			nC >>= 1;
		}

		switch (nC) {
			case 0: case 1:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 1] = cavlc.GetCoeffToken0(decoder_cfg);
			break;
			case 2: case 3:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 1] = cavlc.GetCoeffToken1(decoder_cfg);
			break;
			case 4: case 5: case 6: case 7:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 1] = cavlc.GetCoeffToken2(decoder_cfg);
			break;
			default:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 1] = cavlc.GetCoeffToken3(decoder_cfg);
		}

		cavlc.GetCoeffLevels(decoder_cfg, co_ptr + 16, ((idx<<2) & 0x0F01) | decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 1]);





		if (left_map[(idx<<2) | 2]) mbl = decoder_cfg.mb_x;
		if (up_map[(idx<<2) | 2]) mbu = decoder_cfg.mb_x;	
		
		nC = 0;
		if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
		else {
			nC = mbl->coeff_token[cmp_sel][left_idx[(idx<<2) | 2]] & 0x1F;
		}

		if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
		else {
			nC += mbu->coeff_token[cmp_sel][up_idx[(idx<<2) | 2]] & 0x1F;
		}

		if ((mbl->flag_type & FLAG_MB_AVAILABLE) && (mbu->flag_type & FLAG_MB_AVAILABLE)) {
			nC++;
			nC >>= 1;
		}

		switch (nC) {
			case 0: case 1:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 2] = cavlc.GetCoeffToken0(decoder_cfg);
			break;
			case 2: case 3:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 2] = cavlc.GetCoeffToken1(decoder_cfg);
			break;
			case 4: case 5: case 6: case 7:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 2] = cavlc.GetCoeffToken2(decoder_cfg);
			break;
			default:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 2] = cavlc.GetCoeffToken3(decoder_cfg);
		}

		cavlc.GetCoeffLevels(decoder_cfg, co_ptr + 32, ((idx<<2) & 0x0F02) | decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 2]);




		if (left_map[(idx<<2) | 3]) mbl = decoder_cfg.mb_x;
		if (up_map[(idx<<2) | 3]) mbu = decoder_cfg.mb_x;	
		
		nC = 0;
		if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
		else {
			nC = mbl->coeff_token[cmp_sel][left_idx[(idx<<2) | 3]] & 0x1F;
		}

		if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
		else {
			nC += mbu->coeff_token[cmp_sel][up_idx[(idx<<2) | 3]] & 0x1F;
		}

		if ((mbl->flag_type & FLAG_MB_AVAILABLE) && (mbu->flag_type & FLAG_MB_AVAILABLE)) {
			nC++;
			nC >>= 1;
		}

		switch (nC) {
			case 0: case 1:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 3] = cavlc.GetCoeffToken0(decoder_cfg);
			break;
			case 2: case 3:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 3] = cavlc.GetCoeffToken1(decoder_cfg);
			break;
			case 4: case 5: case 6: case 7:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 3] = cavlc.GetCoeffToken2(decoder_cfg);
			break;
			default:
				decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 3] = cavlc.GetCoeffToken3(decoder_cfg);
		}

		cavlc.GetCoeffLevels(decoder_cfg, co_ptr + 48, ((idx<<2) & 0x0F03) | decoder_cfg.mb_x->coeff_token[cmp_sel][(idx<<2) | 3]);

	}

	return result;
}





















UI_64 Video::H264::ResidualDC(short * levs, unsigned short cmp_sel) {
	int nC(0);
	
	if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
	else {
		nC = decoder_cfg.mb_a->coeff_token[cmp_sel][5] & 0x1F;
	}

	if (decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
	else {
		nC += decoder_cfg.mb_b->coeff_token[cmp_sel][10] & 0x1F;
	}

	if ((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) && (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE)) {
		nC++;
		nC >>= 1;
	}


	switch (nC) {
		case 0: case 1:
			decoder_cfg.mb_x->coeff_token[cmp_sel][0] = cavlc.GetCoeffToken0(decoder_cfg);
		break;
		case 2: case 3:
			decoder_cfg.mb_x->coeff_token[cmp_sel][0] = cavlc.GetCoeffToken1(decoder_cfg);
		break;
		case 4: case 5: case 6: case 7:
			decoder_cfg.mb_x->coeff_token[cmp_sel][0] = cavlc.GetCoeffToken2(decoder_cfg);
		break;
		default:
			decoder_cfg.mb_x->coeff_token[cmp_sel][0] = cavlc.GetCoeffToken3(decoder_cfg);
	}
		
	
	cavlc.GetCoeffLevelsDC(decoder_cfg, levs, decoder_cfg.mb_x->coeff_token[cmp_sel][0]);

	return 0;
}

UI_64 Video::H264::ResidualChroma1DC(short * levs, unsigned short cmp_idx) {
	UI_64 result(0);

	decoder_cfg.mb_x->coeff_token[cmp_idx][0] = cavlc.GetCoeffToken4(decoder_cfg);
	
	cavlc.GetCoeffLevelsDC1(decoder_cfg, levs, decoder_cfg.mb_x->coeff_token[cmp_idx][0]);

	return result;
}

UI_64 Video::H264::ResidualChroma2DC(short * levs, unsigned short cmp_idx) {
	UI_64 result(0);

	decoder_cfg.mb_x->coeff_token[cmp_idx][0] = cavlc.GetCoeffToken5(decoder_cfg);
	
	cavlc.GetCoeffLevelsDC2(decoder_cfg, levs, decoder_cfg.mb_x->coeff_token[cmp_idx][0]);

	return result;
}

UI_64 Video::H264::ResidualChroma1AC(short * levs, unsigned short cmp_idx, unsigned int block_idx) {
	UI_64 result(1);
	int nC(0);

	switch (block_idx) {
		case 0:
			result = 0;
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][1] & 0x1F;
			}

			if (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
				else nC += decoder_cfg.mb_b->coeff_token[cmp_idx][2] & 0x1F;
			}
		break;
		case 1:
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][0] & 0x1F;

			if (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
				else nC += decoder_cfg.mb_b->coeff_token[cmp_idx][3] & 0x1F;
			}
		break;
		case 2:
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][3] & 0x1F;
			}

			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][0] & 0x1F;

		break;
		case 3:
			result = 2;
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][2] & 0x1F;
			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][1] & 0x1F;
		break;


	}

	if (result == 2) {
		nC++;
		nC >>= 1;
	}
	

	switch (nC) {
		case 0: case 1:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken0(decoder_cfg);
		break;
		case 2: case 3:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken1(decoder_cfg);
		break;
		case 4: case 5: case 6: case 7:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken2(decoder_cfg);
		break;
		default:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken3(decoder_cfg);
	}
		
	
	cavlc.GetCoeffLevels(decoder_cfg, levs, 0x0F00 | decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx]);

	return result;
}


UI_64 Video::H264::ResidualChroma2AC(short * levs, unsigned short cmp_idx, unsigned int block_idx) {
	UI_64 result(1);
	int nC(0);

	switch (block_idx & 0x0F) {
		case 0:
			result = 0;
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][1] & 0x1F;
			}

			if (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
				else nC += decoder_cfg.mb_b->coeff_token[cmp_idx][6] & 0x1F;
			}
		break;
		case 1:
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][0] & 0x1F;

			if (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) nC += 16;
				else nC += decoder_cfg.mb_b->coeff_token[cmp_idx][7] & 0x1F;
			}
		break;
		case 2:
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][3] & 0x1F;
			}

			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][0] & 0x1F;

		break;
		case 3:
			result = 2;
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][2] & 0x1F;
			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][1] & 0x1F;
		break;

		case 4:
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][5] & 0x1F;
			}

			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][2] & 0x1F;

		break;
		case 5:
			result = 2;
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][4] & 0x1F;
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][3] & 0x1F;
		break;
		case 6:
			if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
				result++;
				if (decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) nC = 16;
				else nC = decoder_cfg.mb_a->coeff_token[cmp_idx][7] & 0x1F;
			}

			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][4] & 0x1F;

		break;
		case 7:
			result = 2;
			nC = decoder_cfg.mb_x->coeff_token[cmp_idx][6] & 0x1F;
			nC += decoder_cfg.mb_x->coeff_token[cmp_idx][5] & 0x1F;
		break;

	}

	
	if (result == 2) {
		nC++;
		nC >>= 1;
	}	

	switch (nC) {
		case 0: case 1:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken0(decoder_cfg);
		break;
		case 2: case 3:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken1(decoder_cfg);
		break;
		case 4: case 5: case 6: case 7:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken2(decoder_cfg);
		break;
		default:
			decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx] = cavlc.GetCoeffToken3(decoder_cfg);
	}
	
	
	cavlc.GetCoeffLevels(decoder_cfg, levs, 0x0F00 | decoder_cfg.mb_x->coeff_token[cmp_idx][block_idx]);

	return result;
}
