/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"

#define MB_X_OFFSET	76

UI_64 Video::H264::FilterFrame(unsigned int * pipo) {
	UI_64 result('Fuck'), selector(out_pin & (MAX_REF_IDX - 1));
	
	if (stop_hnd) ResetEvent(stop_hnd);

	
	if (short * cf_ptr = reinterpret_cast<short *>(pic_list[selector].buffer.Acquire())) {				
		if ((reinterpret_cast<unsigned int *>(cf_ptr)[6]) && (reinterpret_cast<unsigned int *>(cf_ptr)[7])) {
			if (decoder_cfg.color_transform_override != 255) result = decoder_cfg.color_transform_override;
			else result = reinterpret_cast<VUI *>(cf_ptr)->matrix_coefficients;
			
			result++;
			result &= 0x000F;
			
			switch (decoder_cfg.chroma_array_type) {
				case 1:
					H264_Convert2Picture_1(pipo, cf_ptr);
				break;
				case 2:
					H264_Convert2Picture_2(pipo, cf_ptr);
				break;
				default:
					H264_Convert2Picture_0(pipo, cf_ptr);
			}		
		}
		
		pic_list[selector].buffer.Release();
		
	}

	if (stop_hnd) SetEvent(stop_hnd);

	return result;
}


void Video::H264::FilterPreview(unsigned int * pipo, OpenGL::DecodeRecord & de_rec) {	
	if (short * cf_0 = reinterpret_cast<short *>(pic_list[0].buffer.Acquire())) {				
		if ((reinterpret_cast<unsigned int *>(cf_0)[6]) && (reinterpret_cast<unsigned int *>(cf_0)[7])) {
			de_rec._reserved[5] = reinterpret_cast<VUI *>(cf_0)->matrix_coefficients + 1;
			de_rec._reserved[5] &= 0x000F;


			if (short * cf_1 = reinterpret_cast<short *>(pic_list[1].buffer.Acquire())) {
				switch (decoder_cfg.chroma_array_type) {
					case 1:
						H264_Convert2Picture_1(reinterpret_cast<unsigned int *>(cf_0), cf_1);
					break;
					case 2:
						H264_Convert2Picture_2(reinterpret_cast<unsigned int *>(cf_0), cf_1);
					break;
					default:
						H264_Convert2Picture_0(reinterpret_cast<unsigned int *>(cf_0), cf_1);
				}


				if (short * cf_2 = reinterpret_cast<short *>(pic_list[2].buffer.Acquire())) {
					switch (decoder_cfg.chroma_array_type) {
						case 1:
							H264_Convert2Picture_1(reinterpret_cast<unsigned int *>(cf_1), cf_2);
						break;
						case 2:
							H264_Convert2Picture_2(reinterpret_cast<unsigned int *>(cf_1), cf_2);
						break;
						default:
							H264_Convert2Picture_0(reinterpret_cast<unsigned int *>(cf_1), cf_2);
					}
		

					if (short * cf_3 = reinterpret_cast<short *>(pic_list[3].buffer.Acquire())) {
						switch (decoder_cfg.chroma_array_type) {
							case 1:
								H264_Convert2Picture_1(reinterpret_cast<unsigned int *>(cf_2), cf_3);
							break;
							case 2:
								H264_Convert2Picture_2(reinterpret_cast<unsigned int *>(cf_2), cf_3);
							break;
							default:
								H264_Convert2Picture_0(reinterpret_cast<unsigned int *>(cf_2), cf_3);
						}
		

						if (short * cf_4 = reinterpret_cast<short *>(pic_list[4].buffer.Acquire())) {
							switch (decoder_cfg.chroma_array_type) {
								case 1:
									H264_Convert2Picture_1(reinterpret_cast<unsigned int *>(cf_3), cf_4);
								break;
								case 2:
									H264_Convert2Picture_2(reinterpret_cast<unsigned int *>(cf_3), cf_4);
								break;
								default:
									H264_Convert2Picture_0(reinterpret_cast<unsigned int *>(cf_3), cf_4);
							}

							H264_PackPreview(pipo, reinterpret_cast<unsigned int *>(cf_0), reinterpret_cast<unsigned int *>(cf_1), de_rec);
							H264_PackPreview(pipo + (de_rec._reserved[6] >> 1), reinterpret_cast<unsigned int *>(cf_2), reinterpret_cast<unsigned int *>(cf_3), de_rec);
	
							pic_list[4].buffer.Release();
						}

						pic_list[3].buffer.Release();
					}

					pic_list[2].buffer.Release();
				}

				pic_list[1].buffer.Release();
			}

		}
		pic_list[0].buffer.Release();
	}
}


UI_64 Video::H264::UnitTest(const float * t_vec) {
	MacroBlockSet * mb_t(0);

	UI_64 result(0);
	UI_64 mb_selector[4] = {t_vec[0], t_vec[1], 0, 0};
	unsigned int selector(out_pin & (MAX_REF_IDX - 1));

	mb_selector[0] >>= 4;
	mb_selector[1] >>= 4;

	
	mb_selector[2] = (mb_selector[0] + 1)*4*256 + mb_selector[1]*decoder_cfg.pline_offset;
	mb_selector[3] = mb_selector[0] + mb_selector[1]*decoder_cfg.slice_width;

	if (short * cf_ptr = reinterpret_cast<short *>(pic_list[selector].buffer.Acquire())) {
		mb_t = reinterpret_cast<MacroBlockSet *>(cf_ptr + mb_selector[2]);
		
		pic_list[selector].buffer.Release();
	}


	return result;
}



void Video::H264::SetMBDeblock() {
	int result(0), result1(0), result2(0), idx_val[3][2];
/*
	32 bytes : t'co luma
	32 bytes : t'co chroma
	32 bytes : t'co chroma

	16 bytes :	(ab internal), (ab top), (ab right) luma
				(ab internal), (ab top), (ab right) chroma
				(ab internal), (ab top), (ab right) chroma
*/

	unsigned char * b_strength = reinterpret_cast<unsigned char *>(decoder_cfg.mb_x->x_tent + MB_X_OFFSET);

	reinterpret_cast<unsigned int *>(b_strength)[0] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[1] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[2] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[3] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[4] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[5] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[6] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[7] = 0x80808080;

	reinterpret_cast<unsigned int *>(b_strength)[8] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[9] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[10] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[11] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[12] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[13] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[14] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[15] = 0x80808080;

	reinterpret_cast<unsigned int *>(b_strength)[16] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[17] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[18] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[19] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[20] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[21] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[22] = 0x80808080;
	reinterpret_cast<unsigned int *>(b_strength)[23] = 0x80808080;

	reinterpret_cast<unsigned int *>(b_strength)[24] = 0;
	reinterpret_cast<unsigned int *>(b_strength)[25] = 0;
	reinterpret_cast<unsigned int *>(b_strength)[26] = 0;
	reinterpret_cast<unsigned int *>(b_strength)[27] = 0;
	reinterpret_cast<unsigned int *>(b_strength)[28] = 0;
	reinterpret_cast<unsigned int *>(b_strength)[29] = 0;


	if (current_slice.disable_deblocking_filter_idc == 0) {
		decoder_cfg.mb_x->flag_type |= FLAG_MB_DEBLOCK;


// vertical borders
		if (decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) {
			if ((decoder_cfg.mb_a->flag_type | decoder_cfg.mb_x->flag_type) & FLAG_MB_TYPE_I) {
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24] = reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26] = reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28] = reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30] = 0x40;

			} else {				
				result1 = decoder_cfg.mb_a->coded_block[0] & 0xA0A0;
				result1 >>= 5;
				result1 &= decoder_cfg.mb_x->coded_block[0];

				if (result1 & 0x0001) {
					reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1]) >> 2);

					if (result) {
						reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24] = 1;
					}
				}


				if (result1 & 0x0004) {
					reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[1].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][1]) >> 2);

					if (result) {
						reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26] = 1;
					}
				}


				if (result1 & 0x0100) {
					reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1]) >> 2);

					if (result) {
						reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28] = 1;
					}
				}

				if (result1 & 0x0400) {
					reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_a->x_tent)[3].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][1]) >> 2);

					if (result) {
						reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30] = 1;
					}
				}
			}

			idx_val[0][0] = idx_val[0][1] = ((decoder_cfg.mb_a->qp[0] + decoder_cfg.mb_x->qp[0] + 1) >> 1);
			idx_val[1][0] = idx_val[1][1] = ((decoder_cfg.mb_a->qp[1] + decoder_cfg.mb_x->qp[1] + 1) >> 1);
			idx_val[2][0] = idx_val[2][1] = ((decoder_cfg.mb_a->qp[2] + decoder_cfg.mb_x->qp[2] + 1) >> 1);

			idx_val[0][0] += current_slice.alpha_c0_offset; if (idx_val[0][0] > 51) idx_val[0][0] = 51;
			idx_val[0][1] += current_slice.beta_offset; if (idx_val[0][1] > 51) idx_val[0][1] = 51;
			idx_val[1][0] += current_slice.alpha_c0_offset; if (idx_val[1][0] > 51) idx_val[1][0] = 51;
			idx_val[1][1] += current_slice.beta_offset; if (idx_val[1][1] > 51) idx_val[1][1] = 51;
			idx_val[2][0] += current_slice.alpha_c0_offset; if (idx_val[2][0] > 51) idx_val[2][0] = 51;
			idx_val[2][1] += current_slice.beta_offset; if (idx_val[2][1] > 51) idx_val[2][1] = 51;
			
			// set right ab
			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 4] = deblocker_cfg[idx_val[0][0]][0]; // luma
			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 5] = deblocker_cfg[idx_val[0][1]][2];

			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 6 + 4] = deblocker_cfg[idx_val[1][0]][0]; // chroma 1
			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 6 + 5] = deblocker_cfg[idx_val[1][1]][2];

			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 12 + 4] = deblocker_cfg[idx_val[2][0]][0]; // chroma 2
			reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[96 + 12 + 5] = deblocker_cfg[idx_val[2][1]][2];


			if ((reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24] & 0xC0) == 0) {
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24 + 64] = deblocker_cfg[idx_val[2][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24 + 32] = deblocker_cfg[idx_val[1][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24] = deblocker_cfg[idx_val[0][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[24]];
			}

			if ((reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26] & 0xC0) == 0) {
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26 + 64] = deblocker_cfg[idx_val[2][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26 + 32] = deblocker_cfg[idx_val[1][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26] = deblocker_cfg[idx_val[0][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[26]];
			}

			if ((reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28] & 0xC0) == 0) {
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28 + 64] = deblocker_cfg[idx_val[2][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28 + 32] = deblocker_cfg[idx_val[1][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28] = deblocker_cfg[idx_val[0][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[28]];
			}

			if ((reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30] & 0xC0) == 0) {
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30 + 64] = deblocker_cfg[idx_val[2][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30 + 32] = deblocker_cfg[idx_val[1][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30]];
				reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30] = deblocker_cfg[idx_val[0][0]][4 + reinterpret_cast<unsigned char *>(const_cast<MacroBlockSet *>(decoder_cfg.mb_a)->x_tent + MB_X_OFFSET)[30]];
			}

		}


		

	// internal vertical borders
		if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) {
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
				b_strength[8] = b_strength[10] = b_strength[12] = b_strength[14] = 3;

			} else {
				if (decoder_cfg.mb_x->coded_block[0] & 0x00FF) {
					b_strength[8] = b_strength[10] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[8] = b_strength[10] = 1;
					}

				}

				if (decoder_cfg.mb_x->coded_block[0] & 0xFF00) {
					b_strength[12] = b_strength[14] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[8] = b_strength[10] = 1;
					}
				}
			}						

		} else {
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
				b_strength[0] = b_strength[2] = b_strength[4] = b_strength[6] = 3;
				b_strength[8] = b_strength[10] = b_strength[12] = b_strength[14] = 3;
				b_strength[16] = b_strength[18] = b_strength[20] = b_strength[22] = 3;
				
			} else {
				if (decoder_cfg.mb_x->coded_block[0] & 0x0003) {
					b_strength[0] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[0] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x000C) {
					b_strength[2] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[2] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0030) {
					b_strength[4] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[4] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x00C0) {
					b_strength[6] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[6] = 1;
					}
				}




				result1 = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0]);
				result1 |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1]);

				if (decoder_cfg.mb_x->coded_block[0] & 0x0012) {
					b_strength[8] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[8] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0048) {
					b_strength[10] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[10] = 1;
					}
				}

				result1 = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0]);
				result1 |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1]);


				if (decoder_cfg.mb_x->coded_block[0] & 0x0120) {
					b_strength[12] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[12] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0480) {
					b_strength[14] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[14] = 1;
					}
				}






				if (decoder_cfg.mb_x->coded_block[0] & 0x0300) {
					b_strength[16] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[16] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0C00) {
					b_strength[18] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[18] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x3000) {
					b_strength[20] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[20] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0xC000) {
					b_strength[22] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[22] = 1;
					}
				}
			}
		}


		idx_val[0][0] = idx_val[0][1] = decoder_cfg.mb_x->qp[0];
		idx_val[1][0] = idx_val[1][1] = decoder_cfg.mb_x->qp[1];
		idx_val[2][0] = idx_val[2][1] = decoder_cfg.mb_x->qp[2];
		
		idx_val[0][0] += current_slice.alpha_c0_offset; if (idx_val[0][0] > 51) idx_val[0][0] = 51;
		idx_val[0][1] += current_slice.beta_offset; if (idx_val[0][1] > 51) idx_val[0][1] = 51;
		idx_val[1][0] += current_slice.alpha_c0_offset; if (idx_val[1][0] > 51) idx_val[1][0] = 51;
		idx_val[1][1] += current_slice.beta_offset; if (idx_val[1][1] > 51) idx_val[1][1] = 51;
		idx_val[2][0] += current_slice.alpha_c0_offset; if (idx_val[2][0] > 51) idx_val[2][0] = 51;
		idx_val[2][1] += current_slice.beta_offset; if (idx_val[2][1] > 51) idx_val[2][1] = 51;

		b_strength[96 + 0] = deblocker_cfg[idx_val[0][0]][0]; // luma
		b_strength[96 + 1] = deblocker_cfg[idx_val[0][1]][2]; // luma
		b_strength[96 + 6 + 0] = deblocker_cfg[idx_val[1][0]][0]; // luma
		b_strength[96 + 6 + 1] = deblocker_cfg[idx_val[1][1]][2]; // luma
		b_strength[96 + 12 + 0] = deblocker_cfg[idx_val[2][0]][0]; // luma
		b_strength[96 + 12 + 1] = deblocker_cfg[idx_val[2][1]][2]; // luma


		if ((b_strength[0] & 0xC0) == 0) {
			b_strength[0 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[0]];
			b_strength[0 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[0]];
			b_strength[0] = deblocker_cfg[idx_val[0][0]][4 + b_strength[0]];
		}
		if ((b_strength[2] & 0xC0) == 0) {
			b_strength[2 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[2]];
			b_strength[2 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[2]];
			b_strength[2] = deblocker_cfg[idx_val[0][0]][4 + b_strength[2]];
		}
		if ((b_strength[4] & 0xC0) == 0) {
			b_strength[4 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[4]];
			b_strength[4 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[4]];
			b_strength[4] = deblocker_cfg[idx_val[0][0]][4 + b_strength[4]];
		}
		if ((b_strength[6] & 0xC0) == 0) {
			b_strength[6 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[6]];
			b_strength[6 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[6]];
			b_strength[6] = deblocker_cfg[idx_val[0][0]][4 + b_strength[6]];
		}


		if ((b_strength[8] & 0xC0) == 0) {
			b_strength[8 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[8]];
			b_strength[8 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[8]];
			b_strength[8] = deblocker_cfg[idx_val[0][0]][4 + b_strength[8]];
		}
		if ((b_strength[10] & 0xC0) == 0) {
			b_strength[10 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[10]];
			b_strength[10 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[10]];
			b_strength[10] = deblocker_cfg[idx_val[0][0]][4 + b_strength[10]];
		}
		if ((b_strength[12] & 0xC0) == 0) {
			b_strength[12 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[12]];
			b_strength[12 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[12]];
			b_strength[12] = deblocker_cfg[idx_val[0][0]][4 + b_strength[12]];
		}
		if ((b_strength[14] & 0xC0) == 0) {
			b_strength[14 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[14]];
			b_strength[14 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[14]];
			b_strength[14] = deblocker_cfg[idx_val[0][0]][4 + b_strength[14]];
		}


		if ((b_strength[16] & 0xC0) == 0) {
			b_strength[16 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[16]];
			b_strength[16 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[16]];
			b_strength[16] = deblocker_cfg[idx_val[0][0]][4 + b_strength[16]];
		}
		if ((b_strength[18] & 0xC0) == 0) {
			b_strength[18 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[18]];
			b_strength[18 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[18]];
			b_strength[18] = deblocker_cfg[idx_val[0][0]][4 + b_strength[18]];
		}
		if ((b_strength[20] & 0xC0) == 0) {
			b_strength[20 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[20]];
			b_strength[20 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[20]];
			b_strength[20] = deblocker_cfg[idx_val[0][0]][4 + b_strength[20]];
		}
		if ((b_strength[22] & 0xC0) == 0) {
			b_strength[22 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[22]];
			b_strength[22 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[22]];
			b_strength[22] = deblocker_cfg[idx_val[0][0]][4 + b_strength[22]];
		}






// horizontal borders
		if (decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) {
			if ((decoder_cfg.mb_b->flag_type | decoder_cfg.mb_x->flag_type) & FLAG_MB_TYPE_I) {
				b_strength[1] = b_strength[9] = b_strength[17] = b_strength[25] = 0x40;
			} else {				
				result1 = decoder_cfg.mb_b->coded_block[0] & 0xCC00;
				result1 >>= 10;
				result1 &= decoder_cfg.mb_x->coded_block[0];

				if (result1 & 0x0001) {
					b_strength[1] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[1] = 1;
					}
				}


				if (result1 & 0x0002) {
					b_strength[9] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[2].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[1].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[1].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[1].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[1].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[9] = 1;
					}
				}


				if (result1 & 0x0010) {
					b_strength[17] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[17] = 1;
					}
				}

				if (result1 & 0x0400) {
					b_strength[25] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_b->x_tent)[3].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[25] = 1;
					}
				}
			}

			idx_val[0][0] = idx_val[0][1] = ((decoder_cfg.mb_b->qp[0] + decoder_cfg.mb_x->qp[0] + 1) >> 1);
			idx_val[1][0] = idx_val[1][1] = ((decoder_cfg.mb_b->qp[1] + decoder_cfg.mb_x->qp[1] + 1) >> 1);
			idx_val[2][0] = idx_val[2][1] = ((decoder_cfg.mb_b->qp[2] + decoder_cfg.mb_x->qp[2] + 1) >> 1);

			idx_val[0][0] += current_slice.alpha_c0_offset; if (idx_val[0][0] > 51) idx_val[0][0] = 51;
			idx_val[0][1] += current_slice.beta_offset; if (idx_val[0][1] > 51) idx_val[0][1] = 51;
			idx_val[1][0] += current_slice.alpha_c0_offset; if (idx_val[1][0] > 51) idx_val[1][0] = 51;
			idx_val[1][1] += current_slice.beta_offset; if (idx_val[1][1] > 51) idx_val[1][1] = 51;
			idx_val[2][0] += current_slice.alpha_c0_offset; if (idx_val[2][0] > 51) idx_val[2][0] = 51;
			idx_val[2][1] += current_slice.beta_offset; if (idx_val[2][1] > 51) idx_val[2][1] = 51;
			
			// set right ab
			b_strength[96 + 2] = deblocker_cfg[idx_val[0][0]][0]; // luma
			b_strength[96 + 3] = deblocker_cfg[idx_val[0][1]][2];

			b_strength[96 + 6 + 2] = deblocker_cfg[idx_val[1][0]][0]; // chroma 1
			b_strength[96 + 6 + 3] = deblocker_cfg[idx_val[1][1]][2];

			b_strength[96 + 12 + 2] = deblocker_cfg[idx_val[2][0]][0]; // chroma 2
			b_strength[96 + 12 + 3] = deblocker_cfg[idx_val[2][1]][2];


			if ((b_strength[1] & 0xC0) == 0) {
				b_strength[1 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[1]];
				b_strength[1 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[1]];
				b_strength[1] = deblocker_cfg[idx_val[0][0]][4 + b_strength[1]];
			}
			if ((b_strength[9] & 0xC0) == 0) {
				b_strength[9 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[9]];
				b_strength[9 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[9]];
				b_strength[9] = deblocker_cfg[idx_val[0][0]][4 + b_strength[9]];
			}
			if ((b_strength[17] & 0xC0) == 0) {
				b_strength[17 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[17]];
				b_strength[17 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[17]];
				b_strength[17] = deblocker_cfg[idx_val[0][0]][4 + b_strength[17]];
			}
			if ((b_strength[25] & 0xC0) == 0) {
				b_strength[25 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[25]];
				b_strength[25 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[25]];
				b_strength[25] = deblocker_cfg[idx_val[0][0]][4 + b_strength[25]];
			}



		}

	// internal horizontal borders
		if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) {
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
				b_strength[5] = b_strength[13] = b_strength[21] = b_strength[29] = 3;

			} else {
				if (decoder_cfg.mb_x->coded_block[0] & 0x0F0F) {
					b_strength[5] = b_strength[13] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[5] = b_strength[13] = 1;
					}

				}

				if (decoder_cfg.mb_x->coded_block[0] & 0xF0F0) {
					b_strength[21] = b_strength[29] = 2;
				} else {
					result = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0]);
					result |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1]);

					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[21] = b_strength[29] = 1;
					}
				}
			}

		} else {
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
				b_strength[3] = b_strength[11] = b_strength[19] = b_strength[27] = 3;
				b_strength[5] = b_strength[13] = b_strength[21] = b_strength[29] = 3;
				b_strength[7] = b_strength[15] = b_strength[23] = b_strength[31] = 3;
				
			} else {
				if (decoder_cfg.mb_x->coded_block[0] & 0x0005) {
					b_strength[3] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[3] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x000A) {
					b_strength[11] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[11] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0050) {
					b_strength[19] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[19] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x00A0) {
					b_strength[27] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[27] = 1;
					}
				}




				result1 = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0]);
				result1 |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1]);

				if (decoder_cfg.mb_x->coded_block[0] & 0x0104) {
					b_strength[5] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[5] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0208) {
					b_strength[13] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[13] = 1;
					}
				}

				result1 = (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0]);
				result1 |= (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] ^ reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1]);

				if (decoder_cfg.mb_x->coded_block[0] & 0x1040) {
					b_strength[21] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][2][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1]) >> 2);

					if (result) {
						b_strength[21] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x2080) {
					b_strength[29] = 2;
				} else {
					result = result1 | ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[0][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].mvd[1][3][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][1]) >> 2);

					if (result) {
						b_strength[29] = 1;
					}
				}






				if (decoder_cfg.mb_x->coded_block[0] & 0x0500) {
					b_strength[7] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[7] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x0A00) {
					b_strength[15] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[15] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0x5000) {
					b_strength[23] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][2][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][0][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][2][1]) >> 2);

					if (result) {
						b_strength[23] = 1;
					}
				}

				if (decoder_cfg.mb_x->coded_block[0] & 0xA000) {
					b_strength[31] = 2;
				} else {
					result = ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[0][3][1]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][0] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][3][0]) >> 2);
					result |= ((reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][1][1] - reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].mvd[1][3][1]) >> 2);

					if (result) {
						b_strength[31] = 1;
					}
				}
			}
		}



		idx_val[0][0] = idx_val[0][1] = decoder_cfg.mb_x->qp[0];
		idx_val[1][0] = idx_val[1][1] = decoder_cfg.mb_x->qp[1];
		idx_val[2][0] = idx_val[2][1] = decoder_cfg.mb_x->qp[2];
		
		idx_val[0][0] += current_slice.alpha_c0_offset; if (idx_val[0][0] > 51) idx_val[0][0] = 51;
		idx_val[0][1] += current_slice.beta_offset; if (idx_val[0][1] > 51) idx_val[0][1] = 51;
		idx_val[1][0] += current_slice.alpha_c0_offset; if (idx_val[1][0] > 51) idx_val[1][0] = 51;
		idx_val[1][1] += current_slice.beta_offset; if (idx_val[1][1] > 51) idx_val[1][1] = 51;
		idx_val[2][0] += current_slice.alpha_c0_offset; if (idx_val[2][0] > 51) idx_val[2][0] = 51;
		idx_val[2][1] += current_slice.beta_offset; if (idx_val[2][1] > 51) idx_val[2][1] = 51;


		if ((b_strength[3] & 0xC0) == 0) {
			b_strength[3 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[3]];
			b_strength[3 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[3]];
			b_strength[3] = deblocker_cfg[idx_val[0][0]][4 + b_strength[3]];
		}
		if ((b_strength[11] & 0xC0) == 0) {
			b_strength[11 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[11]];
			b_strength[11 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[11]];
			b_strength[11] = deblocker_cfg[idx_val[0][0]][4 + b_strength[11]];
		}
		if ((b_strength[19] & 0xC0) == 0) {
			b_strength[19 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[19]];
			b_strength[19 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[19]];
			b_strength[19] = deblocker_cfg[idx_val[0][0]][4 + b_strength[19]];
		}
		if ((b_strength[27] & 0xC0) == 0) {
			b_strength[27 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[27]];
			b_strength[27 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[27]];
			b_strength[27] = deblocker_cfg[idx_val[0][0]][4 + b_strength[27]];
		}


		if ((b_strength[5] & 0xC0) == 0) {
			b_strength[5 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[5]];
			b_strength[5 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[5]];
			b_strength[5] = deblocker_cfg[idx_val[0][0]][4 + b_strength[5]];
		}
		if ((b_strength[13] & 0xC0) == 0) {
			b_strength[13 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[13]];
			b_strength[13 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[13]];
			b_strength[13] = deblocker_cfg[idx_val[0][0]][4 + b_strength[13]];
		}
		if ((b_strength[21] & 0xC0) == 0) {
			b_strength[21 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[21]];
			b_strength[21 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[21]];
			b_strength[21] = deblocker_cfg[idx_val[0][0]][4 + b_strength[21]];
		}
		if ((b_strength[29] & 0xC0) == 0) {
			b_strength[29 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[29]];
			b_strength[29 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[29]];
			b_strength[29] = deblocker_cfg[idx_val[0][0]][4 + b_strength[29]];
		}


		if ((b_strength[7] & 0xC0) == 0) {
			b_strength[7 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[7]];
			b_strength[7 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[7]];
			b_strength[7] = deblocker_cfg[idx_val[0][0]][4 + b_strength[7]];
		}
		if ((b_strength[15] & 0xC0) == 0) {
			b_strength[15 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[15]];
			b_strength[15 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[15]];
			b_strength[15] = deblocker_cfg[idx_val[0][0]][4 + b_strength[15]];
		}
		if ((b_strength[23] & 0xC0) == 0) {
			b_strength[23 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[23]];
			b_strength[23 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[23]];
			b_strength[23] = deblocker_cfg[idx_val[0][0]][4 + b_strength[23]];
		}
		if ((b_strength[31] & 0xC0) == 0) {
			b_strength[31 + 64] = deblocker_cfg[idx_val[2][0]][4 + b_strength[31]];
			b_strength[31 + 32] = deblocker_cfg[idx_val[1][0]][4 + b_strength[31]];
			b_strength[31] = deblocker_cfg[idx_val[0][0]][4 + b_strength[31]];
		}
	}
}


__declspec(align(16)) const unsigned char Video::H264::deblocker_cfg[52][8] = { // a', b', t'[0, 1, 2, 3]
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
// 4
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
// 8
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
// 12
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
	0, 0, 0, 0,			0, 0, 0, 0,
// 16
	4, 0, 2, 0,			0, 0, 0, 0,
	4, 0, 2, 0,			0, 0, 0, 1,
	5, 0, 2, 0,			0, 0, 0, 1,
	6, 0, 3, 0,			0, 0, 0, 1,
// 20
	7, 0, 3, 0,			0, 0, 1, 1,
	8, 0, 3, 0,			0, 0, 1, 1,
	9, 0, 3, 0,			0, 1, 1, 1,
	10, 0, 4, 0,		0, 1, 1, 1,
// 24
	12, 0, 4, 0,		0, 1, 1, 1,
	13, 0, 4, 0,		0, 1, 1, 1,
	15, 0, 6, 0,		0, 1, 1, 1,
	17, 0, 6, 0,		0, 1, 1, 2,
// 28
	20, 0, 7, 0,		0, 1, 1, 2,
	22, 0, 7, 0,		0, 1, 1, 2,
	25, 0, 8, 0,		0, 1, 1, 2,
	28, 0, 8, 0,		0, 1, 2, 3,
// 32
	32, 0, 9, 0,		0, 1, 2, 3,
	36, 0, 9, 0,		0, 2, 2, 3,
	40, 0, 10, 0,		0, 2, 2, 4,
	45, 0, 10, 0,		0, 2, 3, 4,
// 36
	50, 0, 11, 0,		0, 2, 3, 4,
	56, 0, 11, 0,		0, 3, 3, 5,
	63, 0, 12, 0,		0, 3, 4, 6,
	71, 0, 12, 0,		0, 3, 4, 6,
// 40
	80, 0, 13, 0,		0, 4, 5, 7,
	90, 0, 13, 0,		0, 4, 5, 8,
	101, 0, 14, 0,		0, 4, 6, 9,
	113, 0, 14, 0,		0, 5, 7, 10,
// 44
	127, 0, 15, 0,		0, 6, 8, 11,
	144, 0, 15, 0,		0, 6, 8, 13,
	162, 0, 16, 0,		0, 7, 10, 14,
	182, 0, 16, 0,		0, 8, 11, 16,
// 48
	203, 0, 17, 0,		0, 9, 12, 18,
	226, 0, 17, 0,		0, 10, 13, 20,
	255, 0, 18, 0,		0, 11, 15, 23,
	255, 0, 18, 0,		0, 13, 17, 25



};


/*



	

		PXOR xmm13, xmm13

		MOV r11, rcx ; destination

		MOV eax, r9d
		SHR eax, 8
		SHL eax, 9
		SUB rcx, rax		
		SHR eax, 4
		LEA rcx, [rcx+rax-96]


		CMP r9b, 16
		JNZ chroma_dc
			PSHUFD xmm0, xmm0, 0 ; 0000
								
			SHR edx, 16
			MOV eax, edx

			CMP edx, 6
			JAE DCTransform0
				SUB eax, 5
				NEG eax				

				XOR r10d, r10d
				BTS r10d, eax

				MOVD xmm13, r10d
				PSHUFD xmm13, xmm13, 0
			JMP DCTransform0

	align 16
		chroma_dc:
			ADD dl, 3
			CMP dl, 6
			JB no_adjust
				SUB dl, 6
				ADD edx, 010000h
			no_adjust:

			MOVZX eax, dl
			SHL eax, 5

			MOVZX eax, WORD PTR [r8 + rax] ; scale
			MOVD xmm0, eax
			PSHUFD xmm0, xmm0, 0

			
			SHR edx, 16
			MOV eax, edx
			MOVD xmm14, eax

			CMP edx, 6
			JAE trans_jmp
				SUB eax, 5
				NEG eax
			
				XOR r10d, r10d
				BTS r10d, eax

				MOVD xmm13, r10d
				PSHUFD xmm13, xmm13, 0

		trans_jmp:
			CMP r9b, 4
			JZ DCTransform4
			JMP DCTransform8
align 16
	no_dc:

*/

/*
1





	SHR r9d, 8
	JZ no_dc
		PXOR xmm13, xmm13
		MOV r11, rcx ; destination

		MOV eax, r9d
		SHR eax, 8
		SHL eax, 9
		SUB rcx, rax		
		SHR eax, 4
		LEA rcx, [rcx+rax-96]



		CMP r9b, 16
		JNZ chroma_dc
			PSHUFD xmm0, xmm0, 0 ; 0000
		
			SHR edx, 16
			MOV eax, edx

			CMP edx, 6
			JAE DCTransform1
				SUB eax, 5
				NEG eax

				XOR r10d, r10d
				BTS r10d, eax

				MOVD xmm13, r10d
				PSHUFD xmm13, xmm13, 0
			JMP DCTransform1

	align 16
		chroma_dc:
			ADD dl, 3
			CMP dl, 6
			JB no_adjust
				SUB dl, 6
				ADD edx, 010000h
			no_adjust:

			MOVZX eax, dl
			SHL eax, 5

			MOVZX eax, WORD PTR [r8 + rax] ; scale
			MOVD xmm0, eax
			PSHUFD xmm0, xmm0, 0

			
			SHR edx, 16
			MOV eax, edx
			MOVD xmm14, eax

			CMP edx, 6
			JAE trans_jmp
				SUB eax, 5
				NEG eax

				XOR r10d, r10d
				BTS r10d, eax

				MOVD xmm13, r10d
				PSHUFD xmm13, xmm13, 0
				

		trans_jmp:
			CMP r9b, 4
			JZ DCTransform4
			JMP DCTransform8
align 16
	no_dc:


*/


