
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


EXTERN	?norm_adjust_4x4@H264@Video@@1QAY15BA@$$CBFA:WORD
EXTERN	?norm_adjust_8x8@H264@Video@@1QAY15EA@$$CBFA:WORD



.CODE




ALIGN 16
H264_NormalizeScalingLists	PROC
	LEA r8, [?norm_adjust_4x4@H264@Video@@1QAY15BA@$$CBFA]





	MOV ax, 0200h

align 16
	ff_44:
		MOVDQA xmm0, [r8]
		MOVDQA xmm1, [r8+16]
		MOVDQA xmm2, [r8+32]
		MOVDQA xmm3, [r8+48]
		MOVDQA xmm4, [r8+64]
		MOVDQA xmm5, [r8+80]
		MOVDQA xmm6, [r8+96]
		MOVDQA xmm7, [r8+112]
		MOVDQA xmm8, [r8+128]
		MOVDQA xmm9, [r8+144]
		MOVDQA xmm10, [r8+160]
		MOVDQA xmm11, [r8+176]

		OR ax, 3

	align 16
		cmp_loop4:
			MOVDQA xmm12, [rcx]
			MOVDQA xmm13, [rcx+16]

			MOVDQA xmm14, xmm12
			MOVDQA xmm15, xmm13
			PMULLW xmm14, xmm0
			PMULLW xmm15, xmm1
			MOVDQA [rcx], xmm14
			MOVDQA [rcx+16], xmm15

			MOVDQA xmm14, xmm12
			MOVDQA xmm15, xmm13
			PMULLW xmm14, xmm2
			PMULLW xmm15, xmm3
			MOVDQA [rcx+32], xmm14
			MOVDQA [rcx+48], xmm15

			MOVDQA xmm14, xmm12
			MOVDQA xmm15, xmm13
			PMULLW xmm14, xmm4
			PMULLW xmm15, xmm5
			MOVDQA [rcx+64], xmm14
			MOVDQA [rcx+80], xmm15

			MOVDQA xmm14, xmm12
			MOVDQA xmm15, xmm13
			PMULLW xmm14, xmm6
			PMULLW xmm15, xmm7
			MOVDQA [rcx+96], xmm14
			MOVDQA [rcx+112], xmm15

			MOVDQA xmm14, xmm12
			MOVDQA xmm15, xmm13
			PMULLW xmm14, xmm8
			PMULLW xmm15, xmm9
			MOVDQA [rcx+128], xmm14
			MOVDQA [rcx+144], xmm15

			PMULLW xmm12, xmm10
			PMULLW xmm13, xmm11
			MOVDQA [rcx+160], xmm12
			MOVDQA [rcx+176], xmm13

			ADD rcx, 192
		DEC al
		JNZ cmp_loop4

		ADD r8, 192
	DEC ah
	JNZ ff_44


	LEA r9, [?norm_adjust_8x8@H264@Video@@1QAY15EA@$$CBFA]

	MOV eax, 020000h

align 16
	ff_88:
		OR eax, 0300h

	align 16
		cmp_loop8:
			MOV r8, r9
		
			MOVDQA xmm0, [rdx]
			MOVDQA xmm1, [rdx+16]
			MOVDQA xmm2, [rdx+32]
			MOVDQA xmm3, [rdx+48]
			MOVDQA xmm4, [rdx+64]
			MOVDQA xmm5, [rdx+80]
			MOVDQA xmm6, [rdx+96]
			MOVDQA xmm7, [rdx+112]
		
			OR eax, 6

		align 16
			idx_loop8:
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm1
				PMULLW xmm8, [r8]
				PMULLW xmm9, [r8+16]
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				PMULLW xmm10, [r8+32]
				PMULLW xmm11, [r8+48]
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMULLW xmm12, [r8+64]
				PMULLW xmm13, [r8+80]
				MOVDQA xmm14, xmm6
				MOVDQA xmm15, xmm7
				PMULLW xmm14, [r8+96]
				PMULLW xmm15, [r8+112]

				MOVDQA [rdx], xmm8
				MOVDQA [rdx+16], xmm9
				MOVDQA [rdx+32], xmm10
				MOVDQA [rdx+48], xmm11
				MOVDQA [rdx+64], xmm12
				MOVDQA [rdx+80], xmm13
				MOVDQA [rdx+96], xmm14
				MOVDQA [rdx+112], xmm15

				ADD r8, 128
				ADD rdx, 128
			DEC al
			JNZ idx_loop8
		
		DEC ah
		JNZ cmp_loop8

		ADD r9, 768
	SUB eax, 010000h
	JNZ ff_88

	RET
H264_NormalizeScalingLists	ENDP



ALIGN 16
H264_DCTransform0	PROC
	
	MOV r11, rcx

	SHL edx, 9

	SUB r11, rdx
	SHR edx, 4
	LEA r11, [r11+rdx-96]
	

	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3

	PUNPCKLWD xmm0, [r11]
	PUNPCKHWD xmm1, [r11]
	PUNPCKLWD xmm2, [r11+16]
	PUNPCKHWD xmm3, [r11+16]

	PSRAD xmm0, 16 ; 0 1 4 8
	PSRAD xmm1, 16 ; 5 2 3 6
	PSRAD xmm2, 16 ; 9 12 13 10
	PSRAD xmm3, 16 ; 7 11 14 15

; dezig

	PSHUFD xmm1, xmm1, 0C9h
	PSHUFD xmm2, xmm2, 09Ch

	MOVDQA xmm4, xmm0
	MOVDQA xmm7, xmm2

	PUNPCKLQDQ xmm4, xmm1 ; 0 1 2 3
	PUNPCKHQDQ xmm7, xmm3 ; 12 13 14 15

	PSRLDQ xmm0, 8 ; 4 8
	PSRLDQ xmm1, 8 ; 5 6
	PSLLDQ xmm2, 8 ; 9 10

	PUNPCKLDQ xmm0, xmm3 ; 4 7 8 11
	
	PUNPCKLQDQ xmm1, xmm0 ; 5 6 4 7
	PUNPCKHQDQ xmm2, xmm0 ; 9 10 8 11

	PSHUFD xmm1, xmm1, 0D2h ; 4 5 6 7
	PSHUFD xmm2, xmm2, 0D2h ; 8 9 10 11
	

; transform

	MOVDQA xmm0, xmm4
	MOVDQA xmm3, xmm7

	PADDD xmm0, xmm1 ; 0 + 4
	PADDD xmm3, xmm2 ; 8 + 12

	PSUBD xmm4, xmm1 ; 0 - 4
	PSUBD xmm7, xmm2 ; -(8 - 12)

	MOVDQA xmm1, xmm0
	PADDD xmm0, xmm3 ; (0 + 4) + (8 + 12)
	PSUBD xmm1, xmm3 ; (0 + 4) - (8 + 12)

	MOVDQA xmm5, xmm4
	PADDD xmm4, xmm7 ; (0 - 4) - (8 - 12)
	PSUBD xmm5, xmm7 ; (0 - 4) + (8 - 12)


; transponse
	MOVDQA xmm2, xmm0
	PUNPCKLDQ xmm0, xmm1 ; 0 4 1 5
	PUNPCKHDQ xmm2, xmm1 ; 2 6 3 7

	MOVDQA xmm6, xmm4
	PUNPCKLDQ xmm4, xmm5 ; 8 12 9 13
	PUNPCKHDQ xmm6, xmm5 ; 10 14 11 15

	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm2, xmm6
	PUNPCKHQDQ xmm3, xmm6


	MOVDQA xmm4, xmm0
	MOVDQA xmm7, xmm3

	PADDD xmm0, xmm1 ; 0 + 1
	PADDD xmm3, xmm2 ; 2 + 3

	PSUBD xmm4, xmm1 ; 0 - 1
	PSUBD xmm7, xmm2 ; -(2 - 3)

	MOVDQA xmm1, xmm0
	PADDD xmm0, xmm3 ; (0 + 1) + (2 + 3)
	PSUBD xmm1, xmm3 ; (0 + 1) - (2 + 3)

	MOVDQA xmm5, xmm4
	PADDD xmm4, xmm7 ; (0 - 1) - (2 - 3)
	PSUBD xmm5, xmm7 ; (0 - 1) + (2 - 3)





	PACKSSDW xmm0, xmm1
	PACKSSDW xmm4, xmm5

	MOVD r8, xmm0 ; 0 4 8 12
	MOVD r10, xmm4 ; 2 6 10 14

	PSRLDQ xmm0, 8
	PSRLDQ xmm4, 8

	MOVD r9, xmm0 ; 1 5 9 13
	MOVD r11, xmm4 ; 3 7 11 15


	MOV [rcx], r8w
	SHR r8, 16
	MOV [rcx+32], r9w
	SHR r9, 16

	MOV [rcx+64], r8w
	SHR r8, 16
	MOV [rcx+96], r9w
	SHR r9, 16

	MOV [rcx+128], r10w
	SHR r10, 16
	MOV [rcx+160], r11w
	SHR r11, 16

	MOV [rcx+192], r10w
	SHR r10, 16
	MOV [rcx+224], r11w
	SHR r11, 16


	MOV [rcx+256], r8w
	SHR r8, 16
	MOV [rcx+288], r9w
	SHR r9, 16

	MOV [rcx+320], r8w	
	MOV [rcx+352], r9w

	MOV [rcx+384], r10w
	SHR r10, 16
	MOV [rcx+416], r11w
	SHR r11, 16

	MOV [rcx+448], r10w
	MOV [rcx+480], r11w


	RET
H264_DCTransform0	ENDP




ALIGN 16
H264_DCTransform1	PROC
	
	MOV r11, rcx

	SHL edx, 9

	SUB r11, rdx
	SHR edx, 4
	LEA r11, [r11+rdx-96]
	

	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm4, xmm4

	PUNPCKLWD xmm1, [r11]
	PUNPCKHWD xmm2, [r11]
	PUNPCKLWD xmm0, [r11+16]
	PUNPCKHWD xmm4, [r11+16]

	PSRAD xmm1, 16 ; 0 4 1 8
	PSRAD xmm2, 16 ; 12 5 9 13
	PSRAD xmm0, 16 ; 2 6 10 14
	PSRAD xmm4, 16 ; 3 7 11 15

; dezig

	PSHUFD xmm1, xmm1, 0D8h ; 0 1 4 8
	PSHUFD xmm2, xmm2, 0C9h ; 5 9 12 13

	MOVDQA xmm3, xmm0
	PUNPCKLDQ xmm0, xmm4 ; 2 3 6 7
	PUNPCKHDQ xmm3, xmm4 ; 10 11 14 15

	MOVDQA xmm4, xmm1
	MOVDQA xmm7, xmm2
	
	PUNPCKLQDQ xmm4, xmm0 ; 0 1 2 3			; 4
	PUNPCKHQDQ xmm7, xmm3 ; 12 13 14 15		; 7


	PSRLDQ xmm1, 8
	PSRLDQ xmm0, 8
	PSLLDQ xmm3, 8
	PUNPCKLDQ xmm1, xmm2 ; 4 5 8 9
	MOVDQA xmm2, xmm1

	PUNPCKLQDQ xmm1, xmm0 ; 4 5 6 7			; 1
	PUNPCKHQDQ xmm2, xmm3 ; 8 9 10 11		; 2



; transform

	MOVDQA xmm0, xmm4
	MOVDQA xmm3, xmm7

	PADDD xmm0, xmm1 ; 0 + 4
	PADDD xmm3, xmm2 ; 8 + 12

	PSUBD xmm4, xmm1 ; 0 - 4
	PSUBD xmm7, xmm2 ; -(8 - 12)

	MOVDQA xmm1, xmm0
	PADDD xmm0, xmm3 ; (0 + 4) + (8 + 12)
	PSUBD xmm1, xmm3 ; (0 + 4) - (8 + 12)

	MOVDQA xmm5, xmm4
	PADDD xmm4, xmm7 ; (0 - 4) - (8 - 12)
	PSUBD xmm5, xmm7 ; (0 - 4) + (8 - 12)


; transponse
	MOVDQA xmm2, xmm0
	PUNPCKLDQ xmm0, xmm1 ; 0 4 1 5
	PUNPCKHDQ xmm2, xmm1 ; 2 6 3 7

	MOVDQA xmm6, xmm4
	PUNPCKLDQ xmm4, xmm5 ; 8 12 9 13
	PUNPCKHDQ xmm6, xmm5 ; 10 14 11 15

	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm2, xmm6
	PUNPCKHQDQ xmm3, xmm6


	MOVDQA xmm4, xmm0
	MOVDQA xmm7, xmm3

	PADDD xmm0, xmm1 ; 0 + 1
	PADDD xmm3, xmm2 ; 2 + 3

	PSUBD xmm4, xmm1 ; 0 - 1
	PSUBD xmm7, xmm2 ; -(2 - 3)

	MOVDQA xmm1, xmm0
	PADDD xmm0, xmm3 ; (0 + 1) + (2 + 3)
	PSUBD xmm1, xmm3 ; (0 + 1) - (2 + 3)

	MOVDQA xmm5, xmm4
	PADDD xmm4, xmm7 ; (0 - 1) - (2 - 3)
	PSUBD xmm5, xmm7 ; (0 - 1) + (2 - 3)





	PACKSSDW xmm0, xmm1
	PACKSSDW xmm4, xmm5

	MOVD r8, xmm0 ; 0 4 8 12
	MOVD r10, xmm4 ; 2 6 10 14

	PSRLDQ xmm0, 8
	PSRLDQ xmm4, 8

	MOVD r9, xmm0 ; 1 5 9 13
	MOVD r11, xmm4 ; 3 7 11 15


	MOV [rcx], r8w
	SHR r8, 16
	MOV [rcx+32], r9w
	SHR r9, 16

	MOV [rcx+64], r8w
	SHR r8, 16
	MOV [rcx+96], r9w
	SHR r9, 16

	MOV [rcx+128], r10w
	SHR r10, 16
	MOV [rcx+160], r11w
	SHR r11, 16

	MOV [rcx+192], r10w
	SHR r10, 16
	MOV [rcx+224], r11w
	SHR r11, 16


	MOV [rcx+256], r8w
	SHR r8, 16
	MOV [rcx+288], r9w
	SHR r9, 16

	MOV [rcx+320], r8w	
	MOV [rcx+352], r9w

	MOV [rcx+384], r10w
	SHR r10, 16
	MOV [rcx+416], r11w
	SHR r11, 16

	MOV [rcx+448], r10w
	MOV [rcx+480], r11w

	


	RET
H264_DCTransform1	ENDP



ALIGN 16
H264_DCTransformChroma1	PROC	
	
	MOV r11, rcx

	SHL edx, 9

	SUB r11, rdx		
	SHR edx, 4
	LEA r11, [r11+rdx-96]


	MOVD xmm0, QWORD PTR [r11]
	MOVDQA xmm1, xmm0

	PHADDW xmm0, xmm0 ; 0 + 1 | 2 + 3
	PHSUBW xmm1, xmm1 ; 0 - 1 | 2 - 3
	
	PUNPCKLDQ xmm0, xmm1
	MOVDQA xmm1, xmm0
	
	PHADDW xmm0, xmm0 ; (0+1)+(2+3) | (0-1)+(2-3)
	PHSUBW xmm1, xmm1 ; (0+1)-(2+3) | (0-1)-(2-3)

	PUNPCKLDQ xmm0, xmm1

	MOVD rdx, xmm0

	MOV [rcx], dx
	SHR rdx, 16
	MOV [rcx+32], dx
	SHR rdx, 16
	MOV [rcx+64], dx
	SHR rdx, 16
	MOV [rcx+96], dx

	RET
H264_DCTransformChroma1	ENDP

ALIGN 16
H264_DCTransformChroma2	PROC
	MOV r11, rcx

	SHL edx, 9

	SUB r11, rdx		
	SHR edx, 4
	LEA r11, [r11+rdx-96]


	MOVDQA xmm0, [r11]
	MOVDQA xmm1, xmm0

	PHADDW xmm0, xmm0 ; 0+1, 2+3, 4+5, 6+7
	PHSUBW xmm1, xmm1 ; 0-1, 2-3, 4-5, 6-7
	
	PUNPCKLQDQ xmm0, xmm1
	MOVDQA xmm1, xmm0
	
	PHADDW xmm0, xmm0 ; (0+1) + (2+3), (4+5) + (6+7), (0-1) + (2-3), (4-5) + (6-7)
	PHSUBW xmm1, xmm1 ; (0+1) - (2+3), (4+5) - (6+7), (0-1) - (2-3), (4-5) - (6-7)

	PUNPCKLQDQ xmm0, xmm1
	MOVDQA xmm1, xmm0

	PHADDW xmm0, xmm0 ; (0+1 + 2+3) + (4+5 + 6+7), (0-1 + 2-3) + (4-5 + 6-7), (0+1 - 2+3) + (4+5 - 6+7), (0-1 - 2-3) + (4-5 - 6-7)
	PHSUBW xmm1, xmm1 ; (0+1 + 2+3) - (4+5 + 6+7), (0-1 + 2-3) - (4-5 + 6-7), (0+1 - 2+3) - (4+5 - 6+7), (0-1 - 2-3) - (4-5 - 6-7)

	MOVD rax, xmm0
	MOVD rdx, xmm1


	MOV [rcx], ax
	SHR rax, 16
	MOV [rcx+32], ax
	SHR rax, 16
	MOV [rcx+64], dx
	SHR rdx, 16
	MOV [rcx+96], dx
	SHR rdx, 16

	MOV [rcx+128], ax
	SHR rax, 16
	MOV [rcx+160], ax

	MOV [rcx+192], dx
	SHR rdx, 16
	MOV [rcx+224], dx

	RET
H264_DCTransformChroma2	ENDP





ALIGN 16
H264_SamplesI0Transform4x4	PROC
	PXOR xmm15, xmm15	
	PXOR xmm13, xmm13
	PXOR xmm12, xmm12
	PXOR xmm11, xmm11
		

	MOV r11, rcx
	XOR rcx, rcx

	MOVZX eax, dl
	SHL eax, 5

	MOVDQA xmm0, [r8 + rax] ; scale matrix
	MOVDQA xmm2, [r8 + rax + 16]

	
	MOV eax, edx
	SHR eax, 16

	MOVD xmm14, eax ; qP/6
	
	CMP eax, 4
	JAE scale_start
		SUB eax, 3
		NEG eax
		XOR r10d, r10d
		BTS r10d, eax
		
		MOVD xmm13, r10d
		PSHUFD xmm13, xmm13, 0
		MOVDQA xmm12, xmm13
	scale_start:





	TEST r9w, 01000h
	JZ no_dc16
		MOV ecx, 2
		MOVD xmm11, ecx

		SHR edx, 16

		CMP edx, 6
		JAE no_dc2
			SUB edx, 5
			NEG edx
			XOR r10d, r10d
			BTS r10d, edx
		
			MOVD xmm1, r10d
			PSLLDQ xmm12, 4
		
			POR xmm12, xmm1		

		JMP no_dc2
align 16
	no_dc16:
		TEST r9w, 0100h
		JZ no_dc1
			MOV ecx, 1
			MOVD xmm11, ecx
			PSLLDQ xmm12, 4

			JMP no_dc2
	align 16
		no_dc1:
			TEST r9w, 0200h
			JZ no_dc2

				MOV ecx, 2

				ADD dl, 3
				CMP dl, 6
				JB do_nix
					SUB dl, 6
					ADD edx, 010000h

					DEC ecx
				do_nix:

				MOVD xmm11, ecx

				MOVZX eax, dl
				SHL eax, 5
				PINSRW xmm0, WORD PTR [r8+rax], 0

				SHR edx, 16

				CMP edx, 6
				JAE no_dc2
					SUB edx, 5
					NEG edx
					XOR r10d, r10d
					BTS r10d, edx
		
					PSLLDQ xmm12, 4
		
					MOVD xmm1, r10d
					POR xmm12, xmm1

align 16
	no_dc2:






	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PUNPCKLWD xmm2, xmm15
	PUNPCKHWD xmm3, xmm15
	


align 16
	transform_loop:

		MOVDQA xmm4, [r11]
		MOVDQA xmm6, [r11+16]

		MOVDQA xmm5, xmm4
		POR xmm5, xmm6
		PCMPEQD xmm5, xmm15
		PMOVMSKB eax, xmm5
		INC ax
		JZ skip_trans


		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6
		PUNPCKLWD xmm4, xmm15
		PUNPCKHWD xmm5, xmm15
		PUNPCKLWD xmm6, xmm15
		PUNPCKHWD xmm7, xmm15

		PMADDWD xmm4, xmm0
		PMADDWD xmm5, xmm1
		PMADDWD xmm6, xmm2
		PMADDWD xmm7, xmm3

		PADDD xmm4, xmm12
		PADDD xmm5, xmm13
		PADDD xmm6, xmm13
		PADDD xmm7, xmm13

		PSLLD xmm4, xmm14
		PSLLD xmm5, xmm14
		PSRAD xmm4, 4
		PSRAD xmm5, 4
		PSLLD xmm6, xmm14
		PSLLD xmm7, xmm14
		PSRAD xmm6, 4
		PSRAD xmm7, 4
		


		PCMPEQD xmm8, xmm8
		PSRLDQ xmm8, 12
		PAND xmm8, xmm4
		PXOR xmm4, xmm8
		PSRAD xmm8, xmm11
		POR xmm4, xmm8

; dezig

		PSHUFD xmm5, xmm5, 0C9h ; 2, 3, 5, 6
		PSHUFD xmm6, xmm6, 09Ch ; 9, 10, 12, 13

		MOVDQA xmm8, xmm4
		PUNPCKLQDQ xmm4, xmm5 ; 0, 1, 2, 3

		MOVDQA xmm9, xmm6
		PUNPCKHQDQ xmm9, xmm7 ; 12, 13, 14, 15

		PSRLDQ xmm8, 8
		PSLLDQ xmm7, 8

		POR xmm8, xmm7 ; 4, 8, 7, 11
		
		PSHUFD xmm8, xmm8, 08Dh ; 8, 11, 4, 7
		PUNPCKHDQ xmm5, xmm8 ; 5, 4, 6, 7
		PUNPCKLDQ xmm6, xmm8 ; 9, 8, 10, 11

		PSHUFD xmm5, xmm5, 0E1h ; 4 5 6 7
		PSHUFD xmm6, xmm6, 0E1h ; 8 9 10 11



; transpose
		MOVDQA xmm8, xmm6
		MOVDQA xmm6, xmm4		
		PUNPCKLDQ xmm4, xmm5 ; 0,4,1,5
		PUNPCKHDQ xmm6, xmm5 ; 2,6,3,7			
		MOVDQA xmm10, xmm8
		PUNPCKLDQ xmm8, xmm9 ; 8,12,9,13
		PUNPCKHDQ xmm10, xmm9 ; 10,14,11,15

		MOVDQA xmm5, xmm4		
		PUNPCKLQDQ xmm4, xmm8 ; 0 4 8 12
		PUNPCKHQDQ xmm5, xmm8 ; 1 5 9 13
		
		MOVDQA xmm7, xmm6
		PUNPCKLQDQ xmm6, xmm10 ; 2 6 10 14
		PUNPCKHQDQ xmm7, xmm10 ; 3 7 11 15





; residual transform

		MOVDQA xmm8, xmm4

		PADDD xmm4, xmm6 ; 0+2
		PSUBD xmm8, xmm6 ; 0-2
		MOVDQA xmm9, xmm8
		MOVDQA xmm10, xmm4

		PADDD xmm4, xmm5 ; 0+2+1
		PSUBD xmm10, xmm5 ; 0+2-1
		PSRAD xmm5, 1
		PADDD xmm8, xmm5 ; 0-2+1/2
		PSUBD xmm9, xmm5 ; 0-2-1/2

		PSUBD xmm8, xmm7 ; 0-2+1/2-3
		PADDD xmm9, xmm7 ; 0-2-1/2+3
		PSRAD xmm7, 1
		PADDD xmm4, xmm7 ; 0+2+1+3/2
		PSUBD xmm10, xmm7 ; 0+2-1-3/2


; transpose
		MOVDQA xmm6, xmm4
		PUNPCKLDQ xmm4, xmm8 ; 0,4,1,5
		PUNPCKHDQ xmm6, xmm8 ; 2,6,3,7			
		MOVDQA xmm8, xmm9
		PUNPCKLDQ xmm8, xmm10 ; 8,12,9,13
		PUNPCKHDQ xmm9, xmm10 ; 10,14,11,15

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6
		PUNPCKLQDQ xmm4, xmm8
		PUNPCKHQDQ xmm5, xmm8
		PUNPCKLQDQ xmm6, xmm9
		PUNPCKHQDQ xmm7, xmm9


		MOVDQA xmm8, xmm4

		PADDD xmm4, xmm6 ; 0+2
		PSUBD xmm8, xmm6 ; 0-2
		MOVDQA xmm9, xmm8
		MOVDQA xmm10, xmm4

		PADDD xmm4, xmm5 ; 0+2+1
		PSUBD xmm10, xmm5 ; 0+2-1
		PSRAD xmm5, 1
		PADDD xmm8, xmm5 ; 0-2+1/2
		PSUBD xmm9, xmm5 ; 0-2-1/2

		PSUBD xmm8, xmm7 ; 0-2+1/2-3
		PADDD xmm9, xmm7 ; 0-2-1/2+3
		PSRAD xmm7, 1
		PADDD xmm4, xmm7 ; 0+2+1+3/2
		PSUBD xmm10, xmm7 ; 0+2-1-3/2



		PCMPEQD xmm5, xmm5
		PSLLD xmm5, 5

		PSUBD xmm4, xmm5
		PSUBD xmm8, xmm5
		PSRAD xmm4, 6
		PSRAD xmm8, 6
		PSUBD xmm9, xmm5
		PSUBD xmm10, xmm5
		PSRAD xmm9, 6
		PSRAD xmm10, 6

		PACKSSDW xmm4, xmm8
		PACKSSDW xmm9, xmm10

		MOVDQA [r11], xmm4
		MOVDQA [r11+16], xmm9

align 16
skip_trans:

		ADD r11, 32


	DEC r9b
	JNZ transform_loop

	RET
H264_SamplesI0Transform4x4	ENDP




ALIGN 16
H264_SamplesI1Transform4x4	PROC
	PXOR xmm15, xmm15	
	PXOR xmm13, xmm13
	PXOR xmm12, xmm12
	PXOR xmm11, xmm11
		

	MOV r11, rcx
	XOR rcx, rcx

	MOVZX eax, dl
	SHL eax, 5

	MOVDQA xmm0, [r8 + rax] ; scale matrix
	MOVDQA xmm2, [r8 + rax + 16]

	
	MOV eax, edx
	SHR eax, 16

	MOVD xmm14, eax ; qP/6
	
	CMP eax, 4
	JAE scale_start
		SUB eax, 3
		NEG eax
		XOR r10d, r10d
		BTS r10d, eax
		
		MOVD xmm13, r10d
		PSHUFD xmm13, xmm13, 0
		MOVDQA xmm12, xmm13
	scale_start:





	TEST r9w, 01000h
	JZ no_dc16
		MOV ecx, 2
		MOVD xmm11, ecx

		SHR edx, 16

		CMP edx, 6
		JAE no_dc2
			SUB edx, 5
			NEG edx
			XOR r10d, r10d
			BTS r10d, edx
		
			MOVD xmm1, r10d
			PSLLDQ xmm12, 4
		
			POR xmm12, xmm1		

		JMP no_dc2
align 16
	no_dc16:
		TEST r9w, 0100h
		JZ no_dc1
			MOV ecx, 1
			MOVD xmm11, ecx
			PSLLDQ xmm12, 4

			JMP no_dc2
	align 16
		no_dc1:
			TEST r9w, 0200h
			JZ no_dc2

				MOV ecx, 2

				ADD dl, 3
				CMP dl, 6
				JB do_nix
					SUB dl, 6
					ADD edx, 010000h

					DEC ecx
				do_nix:

				MOVD xmm11, ecx

				MOVZX eax, dl
				SHL eax, 5
				PINSRW xmm0, WORD PTR [r8+rax], 0

				SHR edx, 16

				CMP edx, 6
				JAE no_dc2
					SUB edx, 5
					NEG edx
					XOR r10d, r10d
					BTS r10d, edx
		
					PSLLDQ xmm12, 4
		
					MOVD xmm1, r10d
					POR xmm12, xmm1

align 16
	no_dc2:






	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PUNPCKLWD xmm2, xmm15
	PUNPCKHWD xmm3, xmm15
	


align 16
	transform_loop:

		MOVDQA xmm4, [r11]
		MOVDQA xmm6, [r11+16]

		MOVDQA xmm5, xmm4
		POR xmm5, xmm6
		PCMPEQD xmm5, xmm15
		PMOVMSKB eax, xmm5
		INC ax
		JZ skip_trans


		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6
		PUNPCKLWD xmm4, xmm15
		PUNPCKHWD xmm5, xmm15
		PUNPCKLWD xmm6, xmm15
		PUNPCKHWD xmm7, xmm15

		PMADDWD xmm4, xmm0
		PMADDWD xmm5, xmm1
		PMADDWD xmm6, xmm2
		PMADDWD xmm7, xmm3

		PADDD xmm4, xmm12
		PADDD xmm5, xmm13
		PADDD xmm6, xmm13
		PADDD xmm7, xmm13

		PSLLD xmm4, xmm14
		PSLLD xmm5, xmm14
		PSRAD xmm4, 4
		PSRAD xmm5, 4
		PSLLD xmm6, xmm14
		PSLLD xmm7, xmm14
		PSRAD xmm6, 4
		PSRAD xmm7, 4
		

		PCMPEQD xmm8, xmm8
		PSRLDQ xmm8, 12
		PAND xmm8, xmm4
		PXOR xmm4, xmm8
		PSRAD xmm8, xmm11
		POR xmm4, xmm8

; dezig
		PCMPEQD xmm10, xmm10
		PSRLDQ xmm10, 4
		
		PSHUFD xmm4, xmm4, 0B4h ; 0 4 8 1
		MOVDQA xmm8, xmm4
		MOVDQA xmm9, xmm5
		PSRLDQ xmm8, 12 ; 1
		PSLLDQ xmm9, 12 ; 12


		PAND xmm4, xmm10
		PSLLDQ xmm10, 4
		PAND xmm5, xmm10

		POR xmm4, xmm9
		POR xmm5, xmm8


; residual transform

		MOVDQA xmm8, xmm4

		PADDD xmm4, xmm6 ; 0+2
		PSUBD xmm8, xmm6 ; 0-2
		MOVDQA xmm9, xmm8
		MOVDQA xmm10, xmm4

		PADDD xmm4, xmm5 ; 0+2+1
		PSUBD xmm10, xmm5 ; 0+2-1
		PSRAD xmm5, 1
		PADDD xmm8, xmm5 ; 0-2+1/2
		PSUBD xmm9, xmm5 ; 0-2-1/2

		PSUBD xmm8, xmm7 ; 0-2+1/2-3
		PADDD xmm9, xmm7 ; 0-2-1/2+3
		PSRAD xmm7, 1
		PADDD xmm4, xmm7 ; 0+2+1+3/2
		PSUBD xmm10, xmm7 ; 0+2-1-3/2


; transpose
		MOVDQA xmm6, xmm4
		PUNPCKLDQ xmm4, xmm8 ; 0,4,1,5
		PUNPCKHDQ xmm6, xmm8 ; 2,6,3,7			
		MOVDQA xmm8, xmm9
		PUNPCKLDQ xmm8, xmm10 ; 8,12,9,13
		PUNPCKHDQ xmm9, xmm10 ; 10,14,11,15

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6
		PUNPCKLQDQ xmm4, xmm8
		PUNPCKHQDQ xmm5, xmm8
		PUNPCKLQDQ xmm6, xmm9
		PUNPCKHQDQ xmm7, xmm9


		MOVDQA xmm8, xmm4

		PADDD xmm4, xmm6 ; 0+2
		PSUBD xmm8, xmm6 ; 0-2
		MOVDQA xmm9, xmm8
		MOVDQA xmm10, xmm4

		PADDD xmm4, xmm5 ; 0+2+1
		PSUBD xmm10, xmm5 ; 0+2-1
		PSRAD xmm5, 1
		PADDD xmm8, xmm5 ; 0-2+1/2
		PSUBD xmm9, xmm5 ; 0-2-1/2

		PSUBD xmm8, xmm7 ; 0-2+1/2-3
		PADDD xmm9, xmm7 ; 0-2-1/2+3
		PSRAD xmm7, 1
		PADDD xmm4, xmm7 ; 0+2+1+3/2
		PSUBD xmm10, xmm7 ; 0+2-1-3/2


; transpose back

		PCMPEQD xmm5, xmm5
		PSLLD xmm5, 5

		PSUBD xmm4, xmm5
		PSUBD xmm8, xmm5
		PSRAD xmm4, 6
		PSRAD xmm8, 6
		PSUBD xmm9, xmm5
		PSUBD xmm10, xmm5
		PSRAD xmm9, 6
		PSRAD xmm10, 6

		PACKSSDW xmm4, xmm8
		PACKSSDW xmm9, xmm10

		MOVDQA [r11], xmm4
		MOVDQA [r11+16], xmm9

align 16
skip_trans:

		ADD r11, 32


	DEC r9b
	JNZ transform_loop

	RET
H264_SamplesI1Transform4x4	ENDP



ALIGN 16
H264_SamplesI0Transform8x8	PROC

	PXOR xmm15, xmm15

	MOV r10d, 32
	MOVD xmm13, r10d
	PSHUFD xmm13, xmm13, 0		
	

	
	MOV eax, edx
	SHR eax, 16

	MOVD xmm14, eax ; qP/6
	

	MOVZX eax, dl
	SHL eax, 7

	MOVDQA xmm0, [rcx]	
	MOVDQA xmm2, [rcx+16]	
	MOVDQA xmm4, [rcx+32]	
	MOVDQA xmm6, [rcx+48]
	
	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6

	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PUNPCKLWD xmm2, xmm15
	PUNPCKHWD xmm3, xmm15
	PUNPCKLWD xmm4, xmm15
	PUNPCKHWD xmm5, xmm15
	PUNPCKLWD xmm6, xmm15
	PUNPCKHWD xmm7, xmm15

	MOVDQA xmm8, [r8+rax]
	MOVDQA xmm9, [r8+rax]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm0, xmm8
	PMADDWD xmm1, xmm9
	PSLLD xmm0, xmm14
	PSLLD xmm1, xmm14
	PADDD xmm0, xmm13
	PADDD xmm1, xmm13
	PSRAD xmm0, 6
	PSRAD xmm1, 6

	MOVDQA xmm8, [r8+rax+16]
	MOVDQA xmm9, [r8+rax+16]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm2, xmm8
	PMADDWD xmm3, xmm9
	PSLLD xmm2, xmm14
	PSLLD xmm3, xmm14
	PADDD xmm2, xmm13
	PADDD xmm3, xmm13
	PSRAD xmm2, 6
	PSRAD xmm3, 6

	MOVDQA xmm8, [r8+rax+32]
	MOVDQA xmm9, [r8+rax+32]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm4, xmm8
	PMADDWD xmm5, xmm9
	PSLLD xmm4, xmm14
	PSLLD xmm5, xmm14
	PADDD xmm4, xmm13
	PADDD xmm5, xmm13
	PSRAD xmm4, 6
	PSRAD xmm5, 6

	MOVDQA xmm8, [r8+rax+48]
	MOVDQA xmm9, [r8+rax+48]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm6, xmm8
	PMADDWD xmm7, xmm9
	PSLLD xmm6, xmm14
	PSLLD xmm7, xmm14
	PADDD xmm6, xmm13
	PADDD xmm7, xmm13
	PSRAD xmm6, 6
	PSRAD xmm7, 6




	MOVDQA xmm8, [rcx+64]
	MOVDQA xmm9, [rcx+80]
	MOVDQA xmm10, [rcx+96]
	MOVDQA xmm11, [rcx+112]


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7



	MOVDQA xmm4, xmm8
	MOVDQA xmm5, xmm9
	MOVDQA xmm6, xmm10
	MOVDQA xmm7, xmm11

	PUNPCKLWD xmm4, xmm15
	PUNPCKHWD xmm8, xmm15
	PUNPCKLWD xmm5, xmm15
	PUNPCKHWD xmm9, xmm15
	PUNPCKLWD xmm6, xmm15
	PUNPCKHWD xmm10, xmm15
	PUNPCKLWD xmm7, xmm15
	PUNPCKHWD xmm11, xmm15


	MOVDQA xmm0, [r8+rax+64]
	MOVDQA xmm1, [r8+rax+64]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm4, xmm0
	PMADDWD xmm8, xmm1
	PSLLD xmm4, xmm14
	PSLLD xmm8, xmm14
	PADDD xmm4, xmm13
	PADDD xmm8, xmm13
	PSRAD xmm4, 6
	PSRAD xmm8, 6

	MOVDQA xmm0, [r8+rax+80]
	MOVDQA xmm1, [r8+rax+80]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm5, xmm0
	PMADDWD xmm9, xmm1
	PSLLD xmm5, xmm14
	PSLLD xmm9, xmm14
	PADDD xmm5, xmm13
	PADDD xmm9, xmm13
	PSRAD xmm5, 6
	PSRAD xmm9, 6

	MOVDQA xmm0, [r8+rax+96]
	MOVDQA xmm1, [r8+rax+96]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm6, xmm0
	PMADDWD xmm10, xmm1
	PSLLD xmm6, xmm14
	PSLLD xmm10, xmm14
	PADDD xmm6, xmm13
	PADDD xmm10, xmm13
	PSRAD xmm6, 6
	PSRAD xmm10, 6

	MOVDQA xmm0, [r8+rax+112]
	MOVDQA xmm1, [r8+rax+112]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm7, xmm0
	PMADDWD xmm11, xmm1
	PSLLD xmm7, xmm14
	PSLLD xmm11, xmm14
	PADDD xmm7, xmm13
	PADDD xmm11, xmm13
	PSRAD xmm7, 6
	PSRAD xmm11, 6


; dezig	

	PSHUFD xmm0, [rcx], 78h
	PINSRW xmm0, WORD PTR [rcx+36], 6
	PINSRW xmm0, WORD PTR [rcx+38], 7 ; 0 8 16 24
	
	PSHUFD xmm1, [rcx+32], 0C0h
	PINSRW xmm1, WORD PTR [rcx+4], 0
	PINSRW xmm1, WORD PTR [rcx+6], 1
	PINSRW xmm1, WORD PTR [rcx+16], 2
	PINSRW xmm1, WORD PTR [rcx+18], 3 ; 1 9 17 25

	PSHUFD xmm2, [rcx+16], 0Dh
	PINSRW xmm2, WORD PTR [rcx+48], 4
	PINSRW xmm2, WORD PTR [rcx+50], 5
	PINSRW xmm2, WORD PTR [rcx+72], 6
	PINSRW xmm2, WORD PTR [rcx+74], 7 ; 2 10 18 26


	PINSRW xmm3, WORD PTR [rcx+24], 0
	PINSRW xmm3, WORD PTR [rcx+26], 1
	PINSRW xmm3, WORD PTR [rcx+52], 2
	PINSRW xmm3, WORD PTR [rcx+54], 3
	PINSRW xmm3, WORD PTR [rcx+68], 4
	PINSRW xmm3, WORD PTR [rcx+70], 5
	PINSRW xmm3, WORD PTR [rcx+96], 6
	PINSRW xmm3, WORD PTR [rcx+98], 7 ; 3 11 19 27


	PINSRW xmm12, WORD PTR [rcx+56], 0
	PINSRW xmm12, WORD PTR [rcx+58], 1
	PINSRW xmm12, WORD PTR [rcx+64], 2
	PINSRW xmm12, WORD PTR [rcx+66], 3
	PINSRW xmm12, WORD PTR [rcx+100], 4
	PINSRW xmm12, WORD PTR [rcx+102], 5
	PINSRW xmm12, WORD PTR [rcx+124], 6
	PINSRW xmm12, WORD PTR [rcx+126], 7 ; 4 12 20 28

	MOVDQA xmm13, xmm5
	MOVD r8, xmm5
	SHR r8, 32

	PSLLDQ xmm13, 12
	PINSRW xmm13, WORD PTR [rcx+60], 0
	PINSRW xmm13, WORD PTR [rcx+62], 1
	PINSRW xmm13, WORD PTR [rcx+104], 2
	PINSRW xmm13, WORD PTR [rcx+106], 3
	PINSRW xmm13, WORD PTR [rcx+120], 4
	PINSRW xmm13, WORD PTR [rcx+122], 5 ; 5 13 21 29

	PINSRW xmm14, WORD PTR [rcx+108], 0
	PINSRW xmm14, WORD PTR [rcx+110], 1
	PINSRW xmm14, WORD PTR [rcx+116], 2
	PINSRW xmm14, WORD PTR [rcx+118], 3
	PINSRW xmm14, r8d, 4
	SHR r8d, 16
	PINSRW xmm14, r8d, 5 ; 6 14 22


	MOVDQA xmm15, xmm5
	PSRLDQ xmm15, 4

	PINSRW xmm15, WORD PTR [rcx+112], 0
	PINSRW xmm15, WORD PTR [rcx+114], 1 ; 7 15 23

	MOV r8d, [rcx+40]

	MOVDQA [rcx], xmm4
	MOVDQA [rcx+16], xmm8
	MOVDQA [rcx+32], xmm6
	MOVDQA [rcx+48], xmm10

	
	MOVDQA xmm4, [rcx+80]
	PSLLDQ xmm4, 4

	PINSRW xmm4, r8d, 0
	SHR r8d, 16
	PINSRW xmm4, r8d, 1
	PINSRW xmm4, WORD PTR [rcx+12], 6
	PINSRW xmm4, WORD PTR [rcx+14], 7 ; 32 40 48 56




	PINSRW xmm5, WORD PTR [rcx+76], 0
	PINSRW xmm5, WORD PTR [rcx+78], 1
	PINSRW xmm5, WORD PTR [rcx+88], 2
	PINSRW xmm5, WORD PTR [rcx+90], 3
	PINSRW xmm5, WORD PTR [rcx+8], 4
	PINSRW xmm5, WORD PTR [rcx+10], 5
	PINSRW xmm5, WORD PTR [rcx+16], 6
	PINSRW xmm5, WORD PTR [rcx+18], 7 ; 33 41 49 57



	PSLLDQ xmm6, 12
	PINSRW xmm6, WORD PTR [rcx+92], 0
	PINSRW xmm6, WORD PTR [rcx+94], 1
	PINSRW xmm6, WORD PTR [rcx+4], 2
	PINSRW xmm6, WORD PTR [rcx+6], 3
	PINSRW xmm6, WORD PTR [rcx+20], 4
	PINSRW xmm6, WORD PTR [rcx+22], 5 ; 34 42 50 58



	MOVDQA [rcx+64], xmm9
	MOVDQA [rcx+80], xmm7

	PINSRW xmm15, WORD PTR [rcx+52], 6
	PINSRW xmm15, WORD PTR [rcx+54], 7 ; 7 15 23 31
	PINSRW xmm14, WORD PTR [rcx+64], 6
	PINSRW xmm14, WORD PTR [rcx+66], 7 ; 6 14 22 30


	PINSRW xmm7, WORD PTR [rcx], 0
	PINSRW xmm7, WORD PTR [rcx+2], 1
	PINSRW xmm7, WORD PTR [rcx+24], 2
	PINSRW xmm7, WORD PTR [rcx+26], 3
	PINSRW xmm7, WORD PTR [rcx+76], 4
	PINSRW xmm7, WORD PTR [rcx+78], 5
	PINSRW xmm7, WORD PTR [rcx+36], 6
	PINSRW xmm7, WORD PTR [rcx+38], 7 ; 35 43 51 59


	PINSRW xmm8, WORD PTR [rcx+28], 0
	PINSRW xmm8, WORD PTR [rcx+30], 1
	PINSRW xmm8, WORD PTR [rcx+72], 2
	PINSRW xmm8, WORD PTR [rcx+74], 3
	PINSRW xmm8, WORD PTR [rcx+40], 4
	PINSRW xmm8, WORD PTR [rcx+42], 5
	PINSRW xmm8, WORD PTR [rcx+84], 6
	PINSRW xmm8, WORD PTR [rcx+86], 7 ; 36 44 52 60


	PSHUFD xmm9, [rcx+80], 80h
	PINSRW xmm9, WORD PTR [rcx+68], 0
	PINSRW xmm9, WORD PTR [rcx+70], 1
	PINSRW xmm9, WORD PTR [rcx+44], 2
	PINSRW xmm9, WORD PTR [rcx+46], 3 ; 37 45 53 61

	PSHUFD xmm11, xmm11, 0D2h
	MOVD r8d, xmm11

	PINSRW xmm11, WORD PTR [rcx+56], 0
	PINSRW xmm11, WORD PTR [rcx+58], 1 ; 39 47 55 63

	PSHUFD xmm10, xmm10, 0Ch

	PINSRW xmm10, WORD PTR [rcx+92], 4
	PINSRW xmm10, WORD PTR [rcx+94], 5
	PINSRW xmm10, r8d, 6
	SHR r8d, 16
	PINSRW xmm10, r8d, 7 ; 38 46 54 62









; transform

	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm8

	MOVDQA xmm12, xmm0
	MOVDQA xmm8, xmm4

	PADDD xmm0, [rcx]									; e0
	PSUBD xmm12, [rcx]									
	PADDD xmm4, [rcx+16]								; e0
	PSUBD xmm8, [rcx+16]								

	MOVDQA [rcx], xmm12									; e2
	MOVDQA [rcx+16], xmm8


	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6

	PSRAD xmm12, 1
	PSRAD xmm8, 1

	PSUBD xmm12, xmm14
	PSUBD xmm8, xmm10

	PSRAD xmm14, 1
	PSRAD xmm10, 1

	PADDD xmm2, xmm14 ; e6
	PADDD xmm6, xmm10 ; e6


	MOVDQA [rcx+32], xmm2								; e6
	MOVDQA [rcx+48], xmm6								
	MOVDQA [rcx+64], xmm12								; e4
	MOVDQA [rcx+80], xmm8

	

	MOVDQA xmm12, xmm13 ; d5 copy
	MOVDQA xmm8, xmm9

	MOVDQA xmm2, xmm15 ; d7 copy
	MOVDQA xmm6, xmm11
	PSRAD xmm2, 1
	PSRAD xmm6, 1

	PSUBD xmm13, xmm3 ; d5 - d3
	PSUBD xmm9, xmm7 ; d5 - d3
	PSUBD xmm13, xmm15 ; d5 - d3 - d7
	PSUBD xmm9, xmm11 ; d5 - d3 - d7
	PSUBD xmm13, xmm2									; e1
	PSUBD xmm9, xmm6									; e1

	
	MOVDQA xmm2, xmm15
	MOVDQA xmm6, xmm11
	PSUBD xmm15, xmm3 ; d7 - d3
	PSUBD xmm11, xmm7 ; d7 - d3
	PADDD xmm15, xmm1 ; d7 - d3 + d1
	PADDD xmm11, xmm5 ; d7 - d3 + d1

	PADDD xmm2, xmm12 ; d7 + d5
	PADDD xmm6, xmm8 ; d7 + d5
	PSUBD xmm2, xmm1 ; d7 + d5 - d1
	PSUBD xmm6, xmm5 ; d7 + d5 - d1


	MOVDQA xmm14, xmm1
	MOVDQA xmm10, xmm5
	PSRAD xmm1, 1
	PSRAD xmm5, 1
	PADDD xmm1, xmm14
	PADDD xmm5, xmm10

	PADDD xmm1, xmm12
	PADDD xmm5, xmm8
	PADDD xmm1, xmm3
	PADDD xmm5, xmm7									; e7

	PSRAD xmm12, 1
	PSRAD xmm8, 1
	PSRAD xmm3, 1
	PSRAD xmm7, 1

	PADDD xmm2, xmm12									; e5
	PADDD xmm6, xmm8

	PSUBD xmm15, xmm3									; e3
	PSUBD xmm11, xmm7





	MOVDQA xmm12, xmm1
	MOVDQA xmm8, xmm5
	PSRAD xmm12, 2
	PSRAD xmm8, 2
	PADDD xmm12, xmm13								; f1
	PADDD xmm8, xmm9
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm1, xmm13								; f7
	PSUBD xmm5, xmm9


	MOVDQA xmm13, xmm15
	MOVDQA xmm9, xmm11
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm13, xmm2								; f5
	PSUBD xmm9, xmm6
	PSRAD xmm2, 2
	PSRAD xmm6, 2
	PADDD xmm15, xmm2								; f3
	PADDD xmm11, xmm6



	
	MOVDQA xmm2, xmm0
	MOVDQA xmm6, xmm4
	PADDD xmm0, [rcx+32]							; f0
	PSUBD xmm2, [rcx+32]							; f6
	PADDD xmm4, [rcx+48]	
	PSUBD xmm6, [rcx+48]


	MOVDQA xmm3, [rcx]
	MOVDQA xmm14, [rcx]
	MOVDQA xmm7, [rcx+16]
	MOVDQA xmm10, [rcx+16]

	PADDD xmm3, [rcx+64]							; f2
	PSUBD xmm14, [rcx+64]							; f4
	PADDD xmm7, [rcx+80]
	PSUBD xmm10, [rcx+80]

	MOVDQA [rcx], xmm1
	MOVDQA [rcx+16], xmm5
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm15
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm8


	MOVDQA xmm1, xmm0
	MOVDQA xmm5, xmm4
	PADDD xmm0, [rcx]						; g0
	PSUBD xmm1, [rcx]						; g7
	PADDD xmm4, [rcx+16]
	PSUBD xmm5, [rcx+16]

	MOVDQA xmm13, xmm3
	MOVDQA xmm9, xmm7	
	PADDD xmm3, [rcx+32]					; g1
	PSUBD xmm13, [rcx+32]					; g6
	PADDD xmm7, [rcx+48]
	PSUBD xmm9, [rcx+48]


	MOVDQA xmm15, xmm14
	MOVDQA xmm11, xmm10
	PADDD xmm14, [rcx+64]
	PSUBD xmm15, [rcx+64]
	PADDD xmm10, [rcx+80]					; g2
	PSUBD xmm11, [rcx+80]					; g5

	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6
	PADDD xmm2, [rcx+96]
	PSUBD xmm12, [rcx+96]
	PADDD xmm6, [rcx+112]					; g3
	PSUBD xmm8, [rcx+112]					; g4



; transpose
	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm15
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm1
	MOVDQA [rcx+64], xmm8
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm9
	MOVDQA [rcx+112], xmm5


	MOVDQA xmm5, xmm0
	PUNPCKLDQ xmm0, xmm3 ; 0 1 8 9
	PUNPCKHDQ xmm5, xmm3 ; 16 17 24 25
	MOVDQA xmm15, xmm14
	PUNPCKLDQ xmm14, xmm2 ; 2 3 10 11
	PUNPCKHDQ xmm15, xmm2 ; 18 19 26 27

	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm14 ; 0 1 2 3
	PUNPCKHQDQ xmm1, xmm14 ; 8 9 10 11
	MOVDQA xmm2, xmm5
	MOVDQA xmm3, xmm5
	PUNPCKLQDQ xmm2, xmm15 ; 16 17 18 19
	PUNPCKHQDQ xmm3, xmm15 ; 24 25 26 27


	MOVDQA xmm12, xmm4
	PUNPCKLDQ xmm12, xmm7 ; 32 33 40 41
	PUNPCKHDQ xmm4, xmm7 ; 48 49 56 57
	MOVDQA xmm11, xmm10
	PUNPCKLDQ xmm10, xmm6 ; 34 35 42 43
	PUNPCKHDQ xmm11, xmm6 ; 50 51 58 59

	MOVDQA xmm13, xmm12
	PUNPCKLQDQ xmm12, xmm10 ; 32 33 34 35
	PUNPCKHQDQ xmm13, xmm10 ; 40 41 42 43
	MOVDQA xmm14, xmm4
	MOVDQA xmm15, xmm4
	PUNPCKLQDQ xmm14, xmm11 ; 48 49 50 51
	PUNPCKHQDQ xmm15, xmm11 ; 56 57 58 59



	MOVDQA xmm4, [rcx]
	MOVDQA xmm6, [rcx]
	PUNPCKLDQ xmm4, [rcx+16] ; 4 5 12 13
	PUNPCKHDQ xmm6, [rcx+16] ; 20 21 28 29
	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6


	MOVDQA xmm10, [rcx+32]
	MOVDQA xmm11, [rcx+32]
	PUNPCKLDQ xmm10, [rcx+48]
	PUNPCKHDQ xmm11, [rcx+48]
	
	PUNPCKLQDQ xmm4, xmm10
	PUNPCKHQDQ xmm5, xmm10
	PUNPCKLQDQ xmm6, xmm11
	PUNPCKHQDQ xmm7, xmm11

	
	MOVDQA xmm8, [rcx+64]
	MOVDQA xmm10, [rcx+64]
	PUNPCKLDQ xmm8, [rcx+80]
	PUNPCKHDQ xmm10, [rcx+80]

	MOVDQA xmm9, [rcx+96]
	MOVDQA xmm11, [rcx+96]
	PUNPCKLDQ xmm9, [rcx+112]
	PUNPCKHDQ xmm11, [rcx+112]
	MOVDQA [rcx+96], xmm9
	MOVDQA [rcx+112], xmm11

	MOVDQA xmm9, xmm8
	MOVDQA xmm11, xmm10
	PUNPCKLQDQ xmm8, [rcx+96]
	PUNPCKHQDQ xmm9, [rcx+96]
	PUNPCKLQDQ xmm10, [rcx+112]
	PUNPCKHQDQ xmm11, [rcx+112]


; transform

	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm8

	MOVDQA xmm12, xmm0
	MOVDQA xmm8, xmm4

	PADDD xmm0, [rcx]									; e0
	PSUBD xmm12, [rcx]									
	PADDD xmm4, [rcx+16]								; e0
	PSUBD xmm8, [rcx+16]								

	MOVDQA [rcx], xmm12									; e2
	MOVDQA [rcx+16], xmm8


	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6

	PSRAD xmm12, 1
	PSRAD xmm8, 1

	PSUBD xmm12, xmm14
	PSUBD xmm8, xmm10

	PSRAD xmm14, 1
	PSRAD xmm10, 1

	PADDD xmm2, xmm14 ; e6
	PADDD xmm6, xmm10 ; e6


	MOVDQA [rcx+32], xmm2								; e6
	MOVDQA [rcx+48], xmm6								
	MOVDQA [rcx+64], xmm12								; e4
	MOVDQA [rcx+80], xmm8

	

	MOVDQA xmm12, xmm13 ; d5 copy
	MOVDQA xmm8, xmm9

	MOVDQA xmm2, xmm15 ; d7 copy
	MOVDQA xmm6, xmm11
	PSRAD xmm2, 1
	PSRAD xmm6, 1

	PSUBD xmm13, xmm3 ; d5 - d3
	PSUBD xmm9, xmm7 ; d5 - d3
	PSUBD xmm13, xmm15 ; d5 - d3 - d7
	PSUBD xmm9, xmm11 ; d5 - d3 - d7
	PSUBD xmm13, xmm2									; e1
	PSUBD xmm9, xmm6									; e1

	
	MOVDQA xmm2, xmm15
	MOVDQA xmm6, xmm11
	PSUBD xmm15, xmm3 ; d7 - d3
	PSUBD xmm11, xmm7 ; d7 - d3
	PADDD xmm15, xmm1 ; d7 - d3 + d1
	PADDD xmm11, xmm5 ; d7 - d3 + d1

	PADDD xmm2, xmm12 ; d7 + d5
	PADDD xmm6, xmm8 ; d7 + d5
	PSUBD xmm2, xmm1 ; d7 + d5 - d1
	PSUBD xmm6, xmm5 ; d7 + d5 - d1


	MOVDQA xmm14, xmm1
	MOVDQA xmm10, xmm5
	PSRAD xmm1, 1
	PSRAD xmm5, 1
	PADDD xmm1, xmm14
	PADDD xmm5, xmm10

	PADDD xmm1, xmm12
	PADDD xmm5, xmm8
	PADDD xmm1, xmm3
	PADDD xmm5, xmm7									; e7

	PSRAD xmm12, 1
	PSRAD xmm8, 1
	PSRAD xmm3, 1
	PSRAD xmm7, 1

	PADDD xmm2, xmm12									; e5
	PADDD xmm6, xmm8

	PSUBD xmm15, xmm3									; e3
	PSUBD xmm11, xmm7





	MOVDQA xmm12, xmm1
	MOVDQA xmm8, xmm5
	PSRAD xmm12, 2
	PSRAD xmm8, 2
	PADDD xmm12, xmm13								; f1
	PADDD xmm8, xmm9
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm1, xmm13								; f7
	PSUBD xmm5, xmm9


	MOVDQA xmm13, xmm15
	MOVDQA xmm9, xmm11
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm13, xmm2								; f5
	PSUBD xmm9, xmm6
	PSRAD xmm2, 2
	PSRAD xmm6, 2
	PADDD xmm15, xmm2								; f3
	PADDD xmm11, xmm6



	
	MOVDQA xmm2, xmm0
	MOVDQA xmm6, xmm4
	PADDD xmm0, [rcx+32]							; f0
	PSUBD xmm2, [rcx+32]							; f6
	PADDD xmm4, [rcx+48]	
	PSUBD xmm6, [rcx+48]


	MOVDQA xmm3, [rcx]
	MOVDQA xmm14, [rcx]
	MOVDQA xmm7, [rcx+16]
	MOVDQA xmm10, [rcx+16]

	PADDD xmm3, [rcx+64]							; f2
	PSUBD xmm14, [rcx+64]							; f4
	PADDD xmm7, [rcx+80]
	PSUBD xmm10, [rcx+80]

	MOVDQA [rcx], xmm1
	MOVDQA [rcx+16], xmm5
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm15
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm8


	MOVDQA xmm1, xmm0
	MOVDQA xmm5, xmm4
	PADDD xmm0, [rcx]						; g0
	PSUBD xmm1, [rcx]						; g7
	PADDD xmm4, [rcx+16]
	PSUBD xmm5, [rcx+16]

	MOVDQA xmm13, xmm3
	MOVDQA xmm9, xmm7	
	PADDD xmm3, [rcx+32]					; g1
	PSUBD xmm13, [rcx+32]					; g6
	PADDD xmm7, [rcx+48]
	PSUBD xmm9, [rcx+48]


	MOVDQA xmm15, xmm14
	MOVDQA xmm11, xmm10
	PADDD xmm14, [rcx+64]
	PSUBD xmm15, [rcx+64]
	PADDD xmm10, [rcx+80]					; g2
	PSUBD xmm11, [rcx+80]					; g5

	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6
	PADDD xmm2, [rcx+96]
	PSUBD xmm12, [rcx+96]
	PADDD xmm6, [rcx+112]					; g3
	PSUBD xmm8, [rcx+112]					; g4



; offload

	MOV eax, 32
	MOV [rcx], eax
	MOV [rcx+4], eax
	MOV [rcx+8], eax
	MOV [rcx+12], eax

	PADDD xmm0, [rcx]
	PADDD xmm4, [rcx]
	PSRAD xmm0, 6
	PSRAD xmm4, 6
	PACKSSDW xmm0, xmm4

	PADDD xmm3, [rcx]
	PADDD xmm7, [rcx]
	PSRAD xmm3, 6
	PSRAD xmm7, 6
	PACKSSDW xmm3, xmm7

	PADDD xmm14, [rcx]
	PADDD xmm10, [rcx]
	PSRAD xmm14, 6
	PSRAD xmm10, 6
	PACKSSDW xmm14, xmm10

	PADDD xmm2, [rcx]
	PADDD xmm6, [rcx]
	PSRAD xmm2, 6
	PSRAD xmm6, 6
	PACKSSDW xmm2, xmm6

	PADDD xmm12, [rcx]
	PADDD xmm8, [rcx]
	PSRAD xmm12, 6
	PSRAD xmm8, 6
	PACKSSDW xmm12, xmm8

	PADDD xmm15, [rcx]
	PADDD xmm11, [rcx]
	PSRAD xmm15, 6
	PSRAD xmm11, 6
	PACKSSDW xmm15, xmm11

	PADDD xmm13, [rcx]
	PADDD xmm9, [rcx]
	PSRAD xmm13, 6
	PSRAD xmm9, 6
	PACKSSDW xmm13, xmm9

	PADDD xmm1, [rcx]
	PADDD xmm5, [rcx]
	PSRAD xmm1, 6
	PSRAD xmm5, 6
	PACKSSDW xmm1, xmm5

	AND r9, r9
	JNZ convert_44
	
		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm3
		MOVDQA [rcx+32], xmm14
		MOVDQA [rcx+48], xmm2
		MOVDQA [rcx+64], xmm12
		MOVDQA [rcx+80], xmm15
		MOVDQA [rcx+96], xmm13
		MOVDQA [rcx+112], xmm1
		
		RET

align 16
	convert_44:
		MOVDQA xmm4, xmm0
		PUNPCKLQDQ xmm0, xmm3 ; 0
		PUNPCKHQDQ xmm4, xmm3 ; 1

		MOVDQA xmm5, xmm14
		PUNPCKLQDQ xmm5, xmm2 ; 0
		PUNPCKHQDQ xmm14, xmm2 ; 1
		
		MOVDQA xmm3, xmm12
		PUNPCKLQDQ xmm3, xmm15 ; 2
		PUNPCKHQDQ xmm12, xmm15 ; 3

		MOVDQA xmm2, xmm13
		PUNPCKLQDQ xmm2, xmm1 ; 2
		PUNPCKHQDQ xmm13, xmm1 ; 3

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm5

		MOVDQA [rcx+32], xmm4
		MOVDQA [rcx+48], xmm14

		MOVDQA [rcx+64], xmm3
		MOVDQA [rcx+80], xmm2

		MOVDQA [rcx+96], xmm12
		MOVDQA [rcx+112], xmm13


	RET
H264_SamplesI0Transform8x8	ENDP


ALIGN 16
H264_SamplesI1Transform8x8	PROC

	PXOR xmm15, xmm15
		
	MOV r10d, 32
	MOVD xmm13, r10d
	PSHUFD xmm13, xmm13, 0		

	
	MOV eax, edx
	SHR eax, 16

	MOVD xmm14, eax ; qP/6
	

	MOVZX eax, dl
	SHL eax, 7

	MOVDQA xmm0, [rcx]	
	MOVDQA xmm2, [rcx+16]	
	MOVDQA xmm4, [rcx+32]	
	MOVDQA xmm6, [rcx+48]
	
	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6

	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PUNPCKLWD xmm2, xmm15
	PUNPCKHWD xmm3, xmm15
	PUNPCKLWD xmm4, xmm15
	PUNPCKHWD xmm5, xmm15
	PUNPCKLWD xmm6, xmm15
	PUNPCKHWD xmm7, xmm15

	MOVDQA xmm8, [r8+rax]
	MOVDQA xmm9, [r8+rax]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm0, xmm8
	PMADDWD xmm1, xmm9
	PSLLD xmm0, xmm14
	PSLLD xmm1, xmm14
	PADDD xmm0, xmm13
	PADDD xmm1, xmm13
	PSRAD xmm0, 6
	PSRAD xmm1, 6

	MOVDQA xmm8, [r8+rax+16]
	MOVDQA xmm9, [r8+rax+16]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm2, xmm8
	PMADDWD xmm3, xmm9
	PSLLD xmm2, xmm14
	PSLLD xmm3, xmm14
	PADDD xmm2, xmm13
	PADDD xmm3, xmm13
	PSRAD xmm2, 6
	PSRAD xmm3, 6

	MOVDQA xmm8, [r8+rax+32]
	MOVDQA xmm9, [r8+rax+32]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm4, xmm8
	PMADDWD xmm5, xmm9
	PSLLD xmm4, xmm14
	PSLLD xmm5, xmm14
	PADDD xmm4, xmm13
	PADDD xmm5, xmm13
	PSRAD xmm4, 6
	PSRAD xmm5, 6

	MOVDQA xmm8, [r8+rax+48]
	MOVDQA xmm9, [r8+rax+48]
	PUNPCKLWD xmm8, xmm15
	PUNPCKHWD xmm9, xmm15
	PMADDWD xmm6, xmm8
	PMADDWD xmm7, xmm9
	PSLLD xmm6, xmm14
	PSLLD xmm7, xmm14
	PADDD xmm6, xmm13
	PADDD xmm7, xmm13
	PSRAD xmm6, 6
	PSRAD xmm7, 6




	MOVDQA xmm8, [rcx+64]
	MOVDQA xmm9, [rcx+80]
	MOVDQA xmm10, [rcx+96]
	MOVDQA xmm11, [rcx+112]


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7



	MOVDQA xmm4, xmm8
	MOVDQA xmm5, xmm9
	MOVDQA xmm6, xmm10
	MOVDQA xmm7, xmm11

	PUNPCKLWD xmm4, xmm15
	PUNPCKHWD xmm8, xmm15
	PUNPCKLWD xmm5, xmm15
	PUNPCKHWD xmm9, xmm15
	PUNPCKLWD xmm6, xmm15
	PUNPCKHWD xmm10, xmm15
	PUNPCKLWD xmm7, xmm15
	PUNPCKHWD xmm11, xmm15


	MOVDQA xmm0, [r8+rax+64]
	MOVDQA xmm1, [r8+rax+64]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm4, xmm0
	PMADDWD xmm8, xmm1
	PSLLD xmm4, xmm14
	PSLLD xmm8, xmm14
	PADDD xmm4, xmm13
	PADDD xmm8, xmm13
	PSRAD xmm4, 6
	PSRAD xmm8, 6

	MOVDQA xmm0, [r8+rax+80]
	MOVDQA xmm1, [r8+rax+80]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm5, xmm0
	PMADDWD xmm9, xmm1
	PSLLD xmm5, xmm14
	PSLLD xmm9, xmm14
	PADDD xmm5, xmm13
	PADDD xmm9, xmm13
	PSRAD xmm5, 6
	PSRAD xmm9, 6

	MOVDQA xmm0, [r8+rax+96]
	MOVDQA xmm1, [r8+rax+96]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm6, xmm0
	PMADDWD xmm10, xmm1
	PSLLD xmm6, xmm14
	PSLLD xmm10, xmm14
	PADDD xmm6, xmm13
	PADDD xmm10, xmm13
	PSRAD xmm6, 6
	PSRAD xmm10, 6

	MOVDQA xmm0, [r8+rax+112]
	MOVDQA xmm1, [r8+rax+112]
	PUNPCKLWD xmm0, xmm15
	PUNPCKHWD xmm1, xmm15
	PMADDWD xmm7, xmm0
	PMADDWD xmm11, xmm1
	PSLLD xmm7, xmm14
	PSLLD xmm11, xmm14
	PADDD xmm7, xmm13
	PADDD xmm11, xmm13
	PSRAD xmm7, 6
	PSRAD xmm11, 6


; dezig	

	MOVDQA xmm0, [rcx]
	PINSRW xmm0, WORD PTR [rcx+20], 6
	PINSRW xmm0, WORD PTR [rcx+22], 7			; 0 8 16 24
	
	PSHUFD xmm1, [rcx+16], 030h
	PINSRW xmm1, WORD PTR [rcx+12], 0
	PINSRW xmm1, WORD PTR [rcx+14], 1
	PINSRW xmm1, WORD PTR [rcx+36], 6
	PINSRW xmm1, WORD PTR [rcx+38], 7			; 1 9 17 25

	PINSRW xmm2, WORD PTR [rcx+32], 0
	PINSRW xmm2, WORD PTR [rcx+34], 1
	PINSRW xmm2, WORD PTR [rcx+56], 2
	PINSRW xmm2, WORD PTR [rcx+58], 3
	PINSRW xmm2, WORD PTR [rcx+64], 4
	PINSRW xmm2, WORD PTR [rcx+66], 5
	PINSRW xmm2, WORD PTR [rcx+80], 6
	PINSRW xmm2, WORD PTR [rcx+82], 7			; 2 10 18 26


	PSHUFD xmm3, [rcx+80], 34h
	PINSRW xmm3, WORD PTR [rcx+60], 0
	PINSRW xmm3, WORD PTR [rcx+62], 1
	PINSRW xmm3, WORD PTR [rcx+112], 6
	PINSRW xmm3, WORD PTR [rcx+114], 7			; 3 11 19 27


	PSHUFD xmm12, [rcx+112], 34h
	PINSRW xmm12, WORD PTR [rcx+88], 0
	PINSRW xmm12, WORD PTR [rcx+90], 1 ; 4 12 20

	
	PSHUFD xmm13, xmm8, 0D0h
	PSRLDQ xmm13, 4
	PINSRW xmm13, WORD PTR [rcx+120], 0
	PINSRW xmm13, WORD PTR [rcx+122], 1 ; 5 13 21


	MOVDQA [rcx], xmm8
	PINSRW xmm12, WORD PTR [rcx], 6
	PINSRW xmm12, WORD PTR [rcx+2], 7			; 4 12 20 28


	PSHUFD xmm14, xmm9, 90h
	PSRLDQ xmm14, 4
	PINSRW xmm14, WORD PTR [rcx+8], 0
	PINSRW xmm14, WORD PTR [rcx+10], 1 ; 6 14 22

	
	MOVDQA xmm15, xmm10
	PSLLDQ xmm15, 8
	PUNPCKHQDQ xmm15, xmm7						 ; 7 15 23 31


	MOVDQA xmm8, xmm5							; 36 44 52 60


	MOVDQA xmm5, xmm9
	PSLLDQ xmm5, 12
	POR xmm13, xmm5								; 5 13 21 2

	MOVDQA xmm5, xmm6
	PSLLDQ xmm6, 4
	PSRLDQ xmm9, 12
	POR xmm9, xmm6								; 37 45 53 61


	PSRLDQ xmm5, 12
	PSLLDQ xmm5, 12
	POR xmm14, xmm5								; 6 14 22 30

	PSRLDQ xmm10, 8
	PUNPCKLQDQ xmm10, xmm7						; 38 46 54 62

	MOVDQA xmm7, xmm4							; 35 43 51 59


												; xmm11 no change 39 47 55 63



	MOVDQA xmm4, [rcx+32]
	PSRLDQ xmm4, 4
	PINSRW xmm4, WORD PTR [rcx+24], 0
	PINSRW xmm4, WORD PTR [rcx+26], 1
	PINSRW xmm4, WORD PTR [rcx+48], 6
	PINSRW xmm4, WORD PTR [rcx+50], 7			; 32 40 48 56


	MOVDQA xmm5, [rcx+64]
	PINSRW xmm5, WORD PTR [rcx+52], 0
	PINSRW xmm5, WORD PTR [rcx+54], 1			; 33 41 49 57



	MOVDQA xmm6, [rcx+96]						; 34 42 50 58


; transform

	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm8

	MOVDQA xmm12, xmm0
	MOVDQA xmm8, xmm4

	PADDD xmm0, [rcx]									; e0
	PSUBD xmm12, [rcx]									
	PADDD xmm4, [rcx+16]								; e0
	PSUBD xmm8, [rcx+16]								

	MOVDQA [rcx], xmm12									; e2
	MOVDQA [rcx+16], xmm8


	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6

	PSRAD xmm12, 1
	PSRAD xmm8, 1

	PSUBD xmm12, xmm14
	PSUBD xmm8, xmm10

	PSRAD xmm14, 1
	PSRAD xmm10, 1

	PADDD xmm2, xmm14 ; e6
	PADDD xmm6, xmm10 ; e6


	MOVDQA [rcx+32], xmm2								; e6
	MOVDQA [rcx+48], xmm6								
	MOVDQA [rcx+64], xmm12								; e4
	MOVDQA [rcx+80], xmm8

	

	MOVDQA xmm12, xmm13 ; d5 copy
	MOVDQA xmm8, xmm9

	MOVDQA xmm2, xmm15 ; d7 copy
	MOVDQA xmm6, xmm11
	PSRAD xmm2, 1
	PSRAD xmm6, 1

	PSUBD xmm13, xmm3 ; d5 - d3
	PSUBD xmm9, xmm7 ; d5 - d3
	PSUBD xmm13, xmm15 ; d5 - d3 - d7
	PSUBD xmm9, xmm11 ; d5 - d3 - d7
	PSUBD xmm13, xmm2									; e1
	PSUBD xmm9, xmm6									; e1

	
	MOVDQA xmm2, xmm15
	MOVDQA xmm6, xmm11
	PSUBD xmm15, xmm3 ; d7 - d3
	PSUBD xmm11, xmm7 ; d7 - d3
	PADDD xmm15, xmm1 ; d7 - d3 + d1
	PADDD xmm11, xmm5 ; d7 - d3 + d1

	PADDD xmm2, xmm12 ; d7 + d5
	PADDD xmm6, xmm8 ; d7 + d5
	PSUBD xmm2, xmm1 ; d7 + d5 - d1
	PSUBD xmm6, xmm5 ; d7 + d5 - d1


	MOVDQA xmm14, xmm1
	MOVDQA xmm10, xmm5
	PSRAD xmm1, 1
	PSRAD xmm5, 1
	PADDD xmm1, xmm14
	PADDD xmm5, xmm10

	PADDD xmm1, xmm12
	PADDD xmm5, xmm8
	PADDD xmm1, xmm3
	PADDD xmm5, xmm7									; e7

	PSRAD xmm12, 1
	PSRAD xmm8, 1
	PSRAD xmm3, 1
	PSRAD xmm7, 1

	PADDD xmm2, xmm12									; e5
	PADDD xmm6, xmm8

	PSUBD xmm15, xmm3									; e3
	PSUBD xmm11, xmm7





	MOVDQA xmm12, xmm1
	MOVDQA xmm8, xmm5
	PSRAD xmm12, 2
	PSRAD xmm8, 2
	PADDD xmm12, xmm13								; f1
	PADDD xmm8, xmm9
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm1, xmm13								; f7
	PSUBD xmm5, xmm9


	MOVDQA xmm13, xmm15
	MOVDQA xmm9, xmm11
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm13, xmm2								; f5
	PSUBD xmm9, xmm6
	PSRAD xmm2, 2
	PSRAD xmm6, 2
	PADDD xmm15, xmm2								; f3
	PADDD xmm11, xmm6



	
	MOVDQA xmm2, xmm0
	MOVDQA xmm6, xmm4
	PADDD xmm0, [rcx+32]							; f0
	PSUBD xmm2, [rcx+32]							; f6
	PADDD xmm4, [rcx+48]	
	PSUBD xmm6, [rcx+48]


	MOVDQA xmm3, [rcx]
	MOVDQA xmm14, [rcx]
	MOVDQA xmm7, [rcx+16]
	MOVDQA xmm10, [rcx+16]

	PADDD xmm3, [rcx+64]							; f2
	PSUBD xmm14, [rcx+64]							; f4
	PADDD xmm7, [rcx+80]
	PSUBD xmm10, [rcx+80]

	MOVDQA [rcx], xmm1
	MOVDQA [rcx+16], xmm5
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm15
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm8


	MOVDQA xmm1, xmm0
	MOVDQA xmm5, xmm4
	PADDD xmm0, [rcx]						; g0
	PSUBD xmm1, [rcx]						; g7
	PADDD xmm4, [rcx+16]
	PSUBD xmm5, [rcx+16]

	MOVDQA xmm13, xmm3
	MOVDQA xmm9, xmm7	
	PADDD xmm3, [rcx+32]					; g1
	PSUBD xmm13, [rcx+32]					; g6
	PADDD xmm7, [rcx+48]
	PSUBD xmm9, [rcx+48]


	MOVDQA xmm15, xmm14
	MOVDQA xmm11, xmm10
	PADDD xmm14, [rcx+64]
	PSUBD xmm15, [rcx+64]
	PADDD xmm10, [rcx+80]					; g2
	PSUBD xmm11, [rcx+80]					; g5

	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6
	PADDD xmm2, [rcx+96]
	PSUBD xmm12, [rcx+96]
	PADDD xmm6, [rcx+112]					; g3
	PSUBD xmm8, [rcx+112]					; g4



; transpose
	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm15
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm1
	MOVDQA [rcx+64], xmm8
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm9
	MOVDQA [rcx+112], xmm5


	MOVDQA xmm5, xmm0
	PUNPCKLDQ xmm0, xmm3 ; 0 1 8 9
	PUNPCKHDQ xmm5, xmm3 ; 16 17 24 25
	MOVDQA xmm15, xmm14
	PUNPCKLDQ xmm14, xmm2 ; 2 3 10 11
	PUNPCKHDQ xmm15, xmm2 ; 18 19 26 27

	MOVDQA xmm1, xmm0
	PUNPCKLQDQ xmm0, xmm14 ; 0 1 2 3
	PUNPCKHQDQ xmm1, xmm14 ; 8 9 10 11
	MOVDQA xmm2, xmm5
	MOVDQA xmm3, xmm5
	PUNPCKLQDQ xmm2, xmm15 ; 16 17 18 19
	PUNPCKHQDQ xmm3, xmm15 ; 24 25 26 27


	MOVDQA xmm12, xmm4
	PUNPCKLDQ xmm12, xmm7 ; 32 33 40 41
	PUNPCKHDQ xmm4, xmm7 ; 48 49 56 57
	MOVDQA xmm11, xmm10
	PUNPCKLDQ xmm10, xmm6 ; 34 35 42 43
	PUNPCKHDQ xmm11, xmm6 ; 50 51 58 59

	MOVDQA xmm13, xmm12
	PUNPCKLQDQ xmm12, xmm10 ; 32 33 34 35
	PUNPCKHQDQ xmm13, xmm10 ; 40 41 42 43
	MOVDQA xmm14, xmm4
	MOVDQA xmm15, xmm4
	PUNPCKLQDQ xmm14, xmm11 ; 48 49 50 51
	PUNPCKHQDQ xmm15, xmm11 ; 56 57 58 59



	MOVDQA xmm4, [rcx]
	MOVDQA xmm6, [rcx]
	PUNPCKLDQ xmm4, [rcx+16] ; 4 5 12 13
	PUNPCKHDQ xmm6, [rcx+16] ; 20 21 28 29
	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6


	MOVDQA xmm10, [rcx+32]
	MOVDQA xmm11, [rcx+32]
	PUNPCKLDQ xmm10, [rcx+48]
	PUNPCKHDQ xmm11, [rcx+48]
	
	PUNPCKLQDQ xmm4, xmm10
	PUNPCKHQDQ xmm5, xmm10
	PUNPCKLQDQ xmm6, xmm11
	PUNPCKHQDQ xmm7, xmm11

	
	MOVDQA xmm8, [rcx+64]
	MOVDQA xmm10, [rcx+64]
	PUNPCKLDQ xmm8, [rcx+80]
	PUNPCKHDQ xmm10, [rcx+80]

	MOVDQA xmm9, [rcx+96]
	MOVDQA xmm11, [rcx+96]
	PUNPCKLDQ xmm9, [rcx+112]
	PUNPCKHDQ xmm11, [rcx+112]
	MOVDQA [rcx+96], xmm9
	MOVDQA [rcx+112], xmm11

	MOVDQA xmm9, xmm8
	MOVDQA xmm11, xmm10
	PUNPCKLQDQ xmm8, [rcx+96]
	PUNPCKHQDQ xmm9, [rcx+96]
	PUNPCKLQDQ xmm10, [rcx+112]
	PUNPCKHQDQ xmm11, [rcx+112]


; transform

	MOVDQA [rcx], xmm12
	MOVDQA [rcx+16], xmm8

	MOVDQA xmm12, xmm0
	MOVDQA xmm8, xmm4

	PADDD xmm0, [rcx]									; e0
	PSUBD xmm12, [rcx]									
	PADDD xmm4, [rcx+16]								; e0
	PSUBD xmm8, [rcx+16]								

	MOVDQA [rcx], xmm12									; e2
	MOVDQA [rcx+16], xmm8


	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6

	PSRAD xmm12, 1
	PSRAD xmm8, 1

	PSUBD xmm12, xmm14
	PSUBD xmm8, xmm10

	PSRAD xmm14, 1
	PSRAD xmm10, 1

	PADDD xmm2, xmm14 ; e6
	PADDD xmm6, xmm10 ; e6


	MOVDQA [rcx+32], xmm2								; e6
	MOVDQA [rcx+48], xmm6								
	MOVDQA [rcx+64], xmm12								; e4
	MOVDQA [rcx+80], xmm8

	

	MOVDQA xmm12, xmm13 ; d5 copy
	MOVDQA xmm8, xmm9

	MOVDQA xmm2, xmm15 ; d7 copy
	MOVDQA xmm6, xmm11
	PSRAD xmm2, 1
	PSRAD xmm6, 1

	PSUBD xmm13, xmm3 ; d5 - d3
	PSUBD xmm9, xmm7 ; d5 - d3
	PSUBD xmm13, xmm15 ; d5 - d3 - d7
	PSUBD xmm9, xmm11 ; d5 - d3 - d7
	PSUBD xmm13, xmm2									; e1
	PSUBD xmm9, xmm6									; e1

	
	MOVDQA xmm2, xmm15
	MOVDQA xmm6, xmm11
	PSUBD xmm15, xmm3 ; d7 - d3
	PSUBD xmm11, xmm7 ; d7 - d3
	PADDD xmm15, xmm1 ; d7 - d3 + d1
	PADDD xmm11, xmm5 ; d7 - d3 + d1

	PADDD xmm2, xmm12 ; d7 + d5
	PADDD xmm6, xmm8 ; d7 + d5
	PSUBD xmm2, xmm1 ; d7 + d5 - d1
	PSUBD xmm6, xmm5 ; d7 + d5 - d1


	MOVDQA xmm14, xmm1
	MOVDQA xmm10, xmm5
	PSRAD xmm1, 1
	PSRAD xmm5, 1
	PADDD xmm1, xmm14
	PADDD xmm5, xmm10

	PADDD xmm1, xmm12
	PADDD xmm5, xmm8
	PADDD xmm1, xmm3
	PADDD xmm5, xmm7									; e7

	PSRAD xmm12, 1
	PSRAD xmm8, 1
	PSRAD xmm3, 1
	PSRAD xmm7, 1

	PADDD xmm2, xmm12									; e5
	PADDD xmm6, xmm8

	PSUBD xmm15, xmm3									; e3
	PSUBD xmm11, xmm7





	MOVDQA xmm12, xmm1
	MOVDQA xmm8, xmm5
	PSRAD xmm12, 2
	PSRAD xmm8, 2
	PADDD xmm12, xmm13								; f1
	PADDD xmm8, xmm9
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm1, xmm13								; f7
	PSUBD xmm5, xmm9


	MOVDQA xmm13, xmm15
	MOVDQA xmm9, xmm11
	PSRAD xmm13, 2
	PSRAD xmm9, 2
	PSUBD xmm13, xmm2								; f5
	PSUBD xmm9, xmm6
	PSRAD xmm2, 2
	PSRAD xmm6, 2
	PADDD xmm15, xmm2								; f3
	PADDD xmm11, xmm6



	
	MOVDQA xmm2, xmm0
	MOVDQA xmm6, xmm4
	PADDD xmm0, [rcx+32]							; f0
	PSUBD xmm2, [rcx+32]							; f6
	PADDD xmm4, [rcx+48]	
	PSUBD xmm6, [rcx+48]


	MOVDQA xmm3, [rcx]
	MOVDQA xmm14, [rcx]
	MOVDQA xmm7, [rcx+16]
	MOVDQA xmm10, [rcx+16]

	PADDD xmm3, [rcx+64]							; f2
	PSUBD xmm14, [rcx+64]							; f4
	PADDD xmm7, [rcx+80]
	PSUBD xmm10, [rcx+80]

	MOVDQA [rcx], xmm1
	MOVDQA [rcx+16], xmm5
	MOVDQA [rcx+32], xmm13
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm15
	MOVDQA [rcx+80], xmm11
	MOVDQA [rcx+96], xmm12
	MOVDQA [rcx+112], xmm8


	MOVDQA xmm1, xmm0
	MOVDQA xmm5, xmm4
	PADDD xmm0, [rcx]						; g0
	PSUBD xmm1, [rcx]						; g7
	PADDD xmm4, [rcx+16]
	PSUBD xmm5, [rcx+16]

	MOVDQA xmm13, xmm3
	MOVDQA xmm9, xmm7	
	PADDD xmm3, [rcx+32]					; g1
	PSUBD xmm13, [rcx+32]					; g6
	PADDD xmm7, [rcx+48]
	PSUBD xmm9, [rcx+48]


	MOVDQA xmm15, xmm14
	MOVDQA xmm11, xmm10
	PADDD xmm14, [rcx+64]
	PSUBD xmm15, [rcx+64]
	PADDD xmm10, [rcx+80]					; g2
	PSUBD xmm11, [rcx+80]					; g5

	MOVDQA xmm12, xmm2
	MOVDQA xmm8, xmm6
	PADDD xmm2, [rcx+96]
	PSUBD xmm12, [rcx+96]
	PADDD xmm6, [rcx+112]					; g3
	PSUBD xmm8, [rcx+112]					; g4


; offload

	MOV eax, 32
	MOV [rcx], eax
	MOV [rcx+4], eax
	MOV [rcx+8], eax
	MOV [rcx+12], eax

	PADDD xmm0, [rcx]
	PADDD xmm4, [rcx]
	PSRAD xmm0, 6
	PSRAD xmm4, 6
	PACKSSDW xmm0, xmm4

	PADDD xmm3, [rcx]
	PADDD xmm7, [rcx]
	PSRAD xmm3, 6
	PSRAD xmm7, 6
	PACKSSDW xmm3, xmm7

	PADDD xmm14, [rcx]
	PADDD xmm10, [rcx]
	PSRAD xmm14, 6
	PSRAD xmm10, 6
	PACKSSDW xmm14, xmm10

	PADDD xmm2, [rcx]
	PADDD xmm6, [rcx]
	PSRAD xmm2, 6
	PSRAD xmm6, 6
	PACKSSDW xmm2, xmm6

	PADDD xmm12, [rcx]
	PADDD xmm8, [rcx]
	PSRAD xmm12, 6
	PSRAD xmm8, 6
	PACKSSDW xmm12, xmm8

	PADDD xmm15, [rcx]
	PADDD xmm11, [rcx]
	PSRAD xmm15, 6
	PSRAD xmm11, 6
	PACKSSDW xmm15, xmm11

	PADDD xmm13, [rcx]
	PADDD xmm9, [rcx]
	PSRAD xmm13, 6
	PSRAD xmm9, 6
	PACKSSDW xmm13, xmm9

	PADDD xmm1, [rcx]
	PADDD xmm5, [rcx]
	PSRAD xmm1, 6
	PSRAD xmm5, 6
	PACKSSDW xmm1, xmm5

	AND r9, r9
	JNZ convert_44
	
		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm3
		MOVDQA [rcx+32], xmm14
		MOVDQA [rcx+48], xmm2
		MOVDQA [rcx+64], xmm12
		MOVDQA [rcx+80], xmm15
		MOVDQA [rcx+96], xmm13
		MOVDQA [rcx+112], xmm1
		
		RET

align 16
	convert_44:
		MOVDQA xmm4, xmm0
		PUNPCKLQDQ xmm0, xmm3 ; 0
		PUNPCKHQDQ xmm4, xmm3 ; 1

		MOVDQA xmm5, xmm14
		PUNPCKLQDQ xmm5, xmm2 ; 0
		PUNPCKHQDQ xmm14, xmm2 ; 1
		
		MOVDQA xmm3, xmm12
		PUNPCKLQDQ xmm3, xmm15 ; 2
		PUNPCKHQDQ xmm12, xmm15 ; 3

		MOVDQA xmm2, xmm13
		PUNPCKLQDQ xmm2, xmm1 ; 2
		PUNPCKHQDQ xmm13, xmm1 ; 3

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm5

		MOVDQA [rcx+32], xmm4
		MOVDQA [rcx+48], xmm14

		MOVDQA [rcx+64], xmm3
		MOVDQA [rcx+80], xmm2

		MOVDQA [rcx+96], xmm12
		MOVDQA [rcx+112], xmm13


	RET
H264_SamplesI1Transform8x8	ENDP



ALIGN 16
H264_BypassAdjustH4x4	PROC
	AND edx, edx
	JNZ field_set
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+4], 1
		PINSRW xmm0, WORD PTR [rcx+6], 2
		PINSRW xmm0, WORD PTR [rcx+18], 3

		PINSRW xmm1, WORD PTR [rcx+2], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+16], 2
		PINSRW xmm1, WORD PTR [rcx+20], 3

		PINSRW xmm2, WORD PTR [rcx+10], 0
		PINSRW xmm2, WORD PTR [rcx+14], 1
		PINSRW xmm2, WORD PTR [rcx+22], 2
		PINSRW xmm2, WORD PTR [rcx+28], 3

		PINSRW xmm3, WORD PTR [rcx+12], 0
		PINSRW xmm3, WORD PTR [rcx+24], 1
		PINSRW xmm3, WORD PTR [rcx+26], 2
		PINSRW xmm3, WORD PTR [rcx+30], 3

	JMP byop
align 16
	field_set:
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+2], 1
		PINSRW xmm0, WORD PTR [rcx+6], 2
		PINSRW xmm0, WORD PTR [rcx+18], 3

		PINSRW xmm1, WORD PTR [rcx+4], 0
		PINSRW xmm1, WORD PTR [rcx+10], 1
		PINSRW xmm1, WORD PTR [rcx+12], 2
		PINSRW xmm1, WORD PTR [rcx+14], 3

		MOVD xmm2, QWORD PTR [rcx+16]
		MOVD xmm3, QWORD PTR [rcx+24]
		
align 16
	byop:
		
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2

		PUNPCKLWD xmm0, xmm1 ; 0 1 4 5 8 9 12 13
		PUNPCKLWD xmm2, xmm3 ; 2 3 6 7 10 11 14 15

		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2 ; 0 1 2 3 4 5 67
		PUNPCKHDQ xmm1, xmm2 ; 8 9 10 11 12 13

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1


	RET
H264_BypassAdjustH4x4	ENDP

ALIGN 16
H264_BypassAdjustV4x4	PROC
	AND edx, edx
	JNZ field_set
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+2], 1
		PINSRW xmm0, WORD PTR [rcx+10], 2
		PINSRW xmm0, WORD PTR [rcx+12], 3

		PINSRW xmm1, WORD PTR [rcx+4], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+14], 2
		PINSRW xmm1, WORD PTR [rcx+24], 3

		PINSRW xmm2, WORD PTR [rcx+6], 0
		PINSRW xmm2, WORD PTR [rcx+16], 1
		PINSRW xmm2, WORD PTR [rcx+22], 2
		PINSRW xmm2, WORD PTR [rcx+26], 3

		PINSRW xmm3, WORD PTR [rcx+18], 0
		PINSRW xmm3, WORD PTR [rcx+20], 1
		PINSRW xmm3, WORD PTR [rcx+28], 2
		PINSRW xmm3, WORD PTR [rcx+30], 3

	JMP byop
align 16
	field_set:
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+4], 1
		PINSRW xmm0, WORD PTR [rcx+16], 2
		PINSRW xmm0, WORD PTR [rcx+24], 3

		PINSRW xmm1, WORD PTR [rcx+2], 0
		PINSRW xmm1, WORD PTR [rcx+10], 1
		PINSRW xmm1, WORD PTR [rcx+18], 2
		PINSRW xmm1, WORD PTR [rcx+26], 3

		PINSRW xmm2, WORD PTR [rcx+6], 0
		PINSRW xmm2, WORD PTR [rcx+12], 1
		PINSRW xmm2, WORD PTR [rcx+20], 2
		PINSRW xmm2, WORD PTR [rcx+28], 3

		PINSRW xmm3, WORD PTR [rcx+8], 0
		PINSRW xmm3, WORD PTR [rcx+14], 1
		PINSRW xmm3, WORD PTR [rcx+22], 2
		PINSRW xmm3, WORD PTR [rcx+30], 3
		
align 16
	byop:
			
	PADDW xmm1, xmm0
	PADDW xmm2, xmm1
	PADDW xmm3, xmm2

	PUNPCKLQDQ xmm0, xmm1
	PUNPCKLQDQ xmm2, xmm3

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm2

	RET
H264_BypassAdjustV4x4	ENDP


ALIGN 16
FrameSetH44	PROC
	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+4], 1
	PINSRW xmm0, WORD PTR [rcx+6], 2
	PINSRW xmm0, WORD PTR [rcx+18], 3

	PINSRW xmm1, WORD PTR [rcx+2], 0
	PINSRW xmm1, WORD PTR [rcx+8], 1
	PINSRW xmm1, WORD PTR [rcx+16], 2
	PINSRW xmm1, WORD PTR [rcx+20], 3

	PINSRW xmm2, WORD PTR [rcx+10], 0
	PINSRW xmm2, WORD PTR [rcx+14], 1
	PINSRW xmm2, WORD PTR [rcx+22], 2
	PINSRW xmm2, WORD PTR [rcx+28], 3

	PINSRW xmm3, WORD PTR [rcx+12], 0
	PINSRW xmm3, WORD PTR [rcx+24], 1
	PINSRW xmm3, WORD PTR [rcx+26], 2
	PINSRW xmm3, WORD PTR [rcx+30], 3

	PINSRW xmm4, WORD PTR [rcx+32], 0
	PINSRW xmm4, WORD PTR [rcx+36], 1
	PINSRW xmm4, WORD PTR [rcx+38], 2
	PINSRW xmm4, WORD PTR [rcx+50], 3

	PINSRW xmm5, WORD PTR [rcx+34], 0
	PINSRW xmm5, WORD PTR [rcx+40], 1
	PINSRW xmm5, WORD PTR [rcx+48], 2
	PINSRW xmm5, WORD PTR [rcx+52], 3

	PINSRW xmm6, WORD PTR [rcx+42], 0
	PINSRW xmm6, WORD PTR [rcx+46], 1
	PINSRW xmm6, WORD PTR [rcx+54], 2
	PINSRW xmm6, WORD PTR [rcx+60], 3

	PINSRW xmm7, WORD PTR [rcx+44], 0
	PINSRW xmm7, WORD PTR [rcx+56], 1
	PINSRW xmm7, WORD PTR [rcx+58], 2
	PINSRW xmm7, WORD PTR [rcx+62], 3

	JMP r10
FrameSetH44	ENDP

ALIGN 16
FieldSetH44	PROC
	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+2], 1
	PINSRW xmm0, WORD PTR [rcx+6], 2
	PINSRW xmm0, WORD PTR [rcx+18], 3

	PINSRW xmm1, WORD PTR [rcx+4], 0
	PINSRW xmm1, WORD PTR [rcx+10], 1
	PINSRW xmm1, WORD PTR [rcx+12], 2
	PINSRW xmm1, WORD PTR [rcx+14], 3

	MOVD xmm2, QWORD PTR [rcx+16]
	MOVD xmm3, QWORD PTR [rcx+24]

	PINSRW xmm4, WORD PTR [rcx+32], 0
	PINSRW xmm4, WORD PTR [rcx+34], 1
	PINSRW xmm4, WORD PTR [rcx+38], 2
	PINSRW xmm4, WORD PTR [rcx+50], 3

	PINSRW xmm5, WORD PTR [rcx+36], 0
	PINSRW xmm5, WORD PTR [rcx+42], 1
	PINSRW xmm5, WORD PTR [rcx+44], 2
	PINSRW xmm5, WORD PTR [rcx+46], 3

	MOVD xmm6, QWORD PTR [rcx+48]
	MOVD xmm7, QWORD PTR [rcx+56]

	JMP r10
FieldSetH44	ENDP

ALIGN 16
H264_BypassAdjustH4x4a	PROC

	PXOR xmm8, xmm8
	PXOR xmm9, xmm9
	
	LEA r9, [FieldSetH44]
	LEA rax, [FrameSetH44]
	TEST r8d, r8d
	CMOVNZ rax, r9
			
	MOV r10, laba_1
	JMP rax
align 16
	laba_1:

	PADDW xmm0, xmm8
	PADDW xmm1, xmm0
	PADDW xmm2, xmm1
	PADDW xmm3, xmm2
	PADDW xmm4, xmm3
	PADDW xmm5, xmm4
	PADDW xmm6, xmm5
	PADDW xmm7, xmm6

	MOVDQA xmm8, xmm7

	PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
	PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
	PUNPCKLWD xmm4, xmm5
	PUNPCKLWD xmm6, xmm7

	MOVDQA xmm1, xmm0
	PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
	PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
	MOVDQA xmm3, xmm4
	PUNPCKLDQ xmm3, xmm6
	PUNPCKHDQ xmm4, xmm6


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm3
	MOVDQA [rcx+48], xmm4

	ADD rcx, 64

	MOV r10, laba_2
	JMP rax
align 16
	laba_2:
			
	PADDW xmm0, xmm9
	PADDW xmm1, xmm0
	PADDW xmm2, xmm1
	PADDW xmm3, xmm2
	PADDW xmm4, xmm3
	PADDW xmm5, xmm4
	PADDW xmm6, xmm5
	PADDW xmm7, xmm6

	MOVDQA xmm9, xmm7

	PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
	PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
	PUNPCKLWD xmm4, xmm5
	PUNPCKLWD xmm6, xmm7

	MOVDQA xmm1, xmm0
	PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
	PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
	MOVDQA xmm3, xmm4
	PUNPCKLDQ xmm3, xmm6
	PUNPCKHDQ xmm4, xmm6


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm3
	MOVDQA [rcx+48], xmm4
			
	ADD rcx, 64

			
	CMP dh, 2
	JZ no_bypass
	CMP dl, 2
	JZ bottom_set_h
	
	MOV r10, laba_3
	JMP rax
align 16
	laba_3:
			
		PADDW xmm0, xmm8
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm8, xmm7

		PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
		PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
		PUNPCKLWD xmm4, xmm5
		PUNPCKLWD xmm6, xmm7

		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
		PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
		MOVDQA xmm3, xmm4
		PUNPCKLDQ xmm3, xmm6
		PUNPCKHDQ xmm4, xmm6


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm3
		MOVDQA [rcx+48], xmm4

		ADD rcx, 64

	MOV r10, laba_4
	JMP rax
align 16
	laba_4:
			
		PADDW xmm0, xmm9
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm9, xmm7

		PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
		PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
		PUNPCKLWD xmm4, xmm5
		PUNPCKLWD xmm6, xmm7

		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
		PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
		MOVDQA xmm3, xmm4
		PUNPCKLDQ xmm3, xmm6
		PUNPCKHDQ xmm4, xmm6


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm3
		MOVDQA [rcx+48], xmm4
			
		ADD rcx, 64


align 16
	bottom_set_h:
		PXOR xmm8, xmm8
		PXOR xmm9, xmm9

	MOV r10, laba_5
	JMP rax
align 16
	laba_5:
			
		PADDW xmm0, xmm8
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm8, xmm7

		PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
		PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
		PUNPCKLWD xmm4, xmm5
		PUNPCKLWD xmm6, xmm7

		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
		PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
		MOVDQA xmm3, xmm4
		PUNPCKLDQ xmm3, xmm6
		PUNPCKHDQ xmm4, xmm6


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm3
		MOVDQA [rcx+48], xmm4

		ADD rcx, 64

	MOV r10, laba_6
	JMP rax
align 16
	laba_6:
			
		PADDW xmm0, xmm9
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm9, xmm7

		PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
		PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
		PUNPCKLWD xmm4, xmm5
		PUNPCKLWD xmm6, xmm7

		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
		PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
		MOVDQA xmm3, xmm4
		PUNPCKLDQ xmm3, xmm6
		PUNPCKHDQ xmm4, xmm6


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm3
		MOVDQA [rcx+48], xmm4
			
		ADD rcx, 64

			
		CMP dh, 2
		JZ no_bypass
		CMP dl, 2
		JZ no_bypass
	
	MOV r10, laba_7
	JMP rax
align 16
	laba_7:
			
			PADDW xmm0, xmm8
			PADDW xmm1, xmm0
			PADDW xmm2, xmm1
			PADDW xmm3, xmm2
			PADDW xmm4, xmm3
			PADDW xmm5, xmm4
			PADDW xmm6, xmm5
			PADDW xmm7, xmm6

			MOVDQA xmm8, xmm7

			PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
			PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
			PUNPCKLWD xmm4, xmm5
			PUNPCKLWD xmm6, xmm7

			MOVDQA xmm1, xmm0
			PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
			PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
			MOVDQA xmm3, xmm4
			PUNPCKLDQ xmm3, xmm6
			PUNPCKHDQ xmm4, xmm6


			MOVDQA [rcx], xmm0
			MOVDQA [rcx+16], xmm1
			MOVDQA [rcx+32], xmm3
			MOVDQA [rcx+48], xmm4

			ADD rcx, 64

	MOV r10, laba_8
	JMP rax
align 16
	laba_8:
			
			PADDW xmm0, xmm9
			PADDW xmm1, xmm0
			PADDW xmm2, xmm1
			PADDW xmm3, xmm2
			PADDW xmm4, xmm3
			PADDW xmm5, xmm4
			PADDW xmm6, xmm5
			PADDW xmm7, xmm6

			MOVDQA xmm9, xmm7

			PUNPCKLWD xmm0, xmm1 ; 0	4	1	5	2	6	3	7
			PUNPCKLWD xmm2, xmm3 ; 8	12	9	13	10	14	11	15
			PUNPCKLWD xmm4, xmm5
			PUNPCKLWD xmm6, xmm7

			MOVDQA xmm1, xmm0
			PUNPCKLDQ xmm0, xmm2 ; 0	4	8	1	5	9	13
			PUNPCKHDQ xmm1, xmm2 ; 2	6	10	14	3	7	11	15
			MOVDQA xmm3, xmm4
			PUNPCKLDQ xmm3, xmm6
			PUNPCKHDQ xmm4, xmm6


			MOVDQA [rcx], xmm0
			MOVDQA [rcx+16], xmm1
			MOVDQA [rcx+32], xmm3
			MOVDQA [rcx+48], xmm4
			
			ADD rcx, 64

align 16
	no_bypass:

	RET
H264_BypassAdjustH4x4a	ENDP

ALIGN 16
FrameSetV44	PROC
	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+2], 1
	PINSRW xmm0, WORD PTR [rcx+10], 2
	PINSRW xmm0, WORD PTR [rcx+12], 3

	PINSRW xmm1, WORD PTR [rcx+4], 0
	PINSRW xmm1, WORD PTR [rcx+8], 1
	PINSRW xmm1, WORD PTR [rcx+14], 2
	PINSRW xmm1, WORD PTR [rcx+24], 3

	PINSRW xmm2, WORD PTR [rcx+6], 0
	PINSRW xmm2, WORD PTR [rcx+16], 1
	PINSRW xmm2, WORD PTR [rcx+22], 2
	PINSRW xmm2, WORD PTR [rcx+26], 3

	PINSRW xmm3, WORD PTR [rcx+18], 0
	PINSRW xmm3, WORD PTR [rcx+20], 1
	PINSRW xmm3, WORD PTR [rcx+28], 2
	PINSRW xmm3, WORD PTR [rcx+30], 3


	PINSRW xmm4, WORD PTR [rcx+32], 0
	PINSRW xmm4, WORD PTR [rcx+34], 1
	PINSRW xmm4, WORD PTR [rcx+42], 2
	PINSRW xmm4, WORD PTR [rcx+44], 3

	PINSRW xmm5, WORD PTR [rcx+36], 0
	PINSRW xmm5, WORD PTR [rcx+40], 1
	PINSRW xmm5, WORD PTR [rcx+46], 2
	PINSRW xmm5, WORD PTR [rcx+56], 3

	PINSRW xmm6, WORD PTR [rcx+38], 0
	PINSRW xmm6, WORD PTR [rcx+48], 1
	PINSRW xmm6, WORD PTR [rcx+54], 2
	PINSRW xmm6, WORD PTR [rcx+58], 3

	PINSRW xmm7, WORD PTR [rcx+50], 0
	PINSRW xmm7, WORD PTR [rcx+52], 1
	PINSRW xmm7, WORD PTR [rcx+60], 2
	PINSRW xmm7, WORD PTR [rcx+62], 3

	JMP r10
FrameSetV44	ENDP

ALIGN 16
FieldSetV44	PROC
	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+4], 1
	PINSRW xmm0, WORD PTR [rcx+16], 2
	PINSRW xmm0, WORD PTR [rcx+24], 3

	PINSRW xmm1, WORD PTR [rcx+2], 0
	PINSRW xmm1, WORD PTR [rcx+10], 1
	PINSRW xmm1, WORD PTR [rcx+18], 2
	PINSRW xmm1, WORD PTR [rcx+26], 3

	PINSRW xmm2, WORD PTR [rcx+6], 0
	PINSRW xmm2, WORD PTR [rcx+12], 1
	PINSRW xmm2, WORD PTR [rcx+20], 2
	PINSRW xmm2, WORD PTR [rcx+28], 3

	PINSRW xmm3, WORD PTR [rcx+8], 0
	PINSRW xmm3, WORD PTR [rcx+14], 1
	PINSRW xmm3, WORD PTR [rcx+22], 2
	PINSRW xmm3, WORD PTR [rcx+30], 3


	PINSRW xmm4, WORD PTR [rcx+32], 0
	PINSRW xmm4, WORD PTR [rcx+36], 1
	PINSRW xmm4, WORD PTR [rcx+48], 2
	PINSRW xmm4, WORD PTR [rcx+56], 3

	PINSRW xmm5, WORD PTR [rcx+34], 0
	PINSRW xmm5, WORD PTR [rcx+42], 1
	PINSRW xmm5, WORD PTR [rcx+50], 2
	PINSRW xmm5, WORD PTR [rcx+58], 3

	PINSRW xmm6, WORD PTR [rcx+38], 0
	PINSRW xmm6, WORD PTR [rcx+44], 1
	PINSRW xmm6, WORD PTR [rcx+52], 2
	PINSRW xmm6, WORD PTR [rcx+60], 3

	PINSRW xmm7, WORD PTR [rcx+40], 0
	PINSRW xmm7, WORD PTR [rcx+46], 1
	PINSRW xmm7, WORD PTR [rcx+54], 2
	PINSRW xmm7, WORD PTR [rcx+62], 3

	JMP r10
FieldSetV44	ENDP


ALIGN 16
H264_BypassAdjustV4x4a	PROC
	PXOR xmm8, xmm8
	PXOR xmm9, xmm9
	PXOR xmm10, xmm10
	PXOR xmm11, xmm11
	
	LEA r9, [FieldSetV44]
	LEA rax, [FrameSetV44]
	TEST r8d, r8d
	CMOVNZ rax, r9

	MOV r10, laba_1
	JMP rax
align 16
	laba_1:

	PADDW xmm0, xmm8
	PADDW xmm1, xmm0
	PADDW xmm2, xmm1
	PADDW xmm3, xmm2

	MOVDQA xmm8, xmm3
	PUNPCKLQDQ xmm0, xmm1
	PUNPCKLQDQ xmm2, xmm3

	PADDW xmm4, xmm9
	PADDW xmm5, xmm4
	PADDW xmm6, xmm5
	PADDW xmm7, xmm6

	MOVDQA xmm9, xmm7
	PUNPCKLQDQ xmm4, xmm5
	PUNPCKLQDQ xmm6, xmm7


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm2
	MOVDQA [rcx+32], xmm4
	MOVDQA [rcx+64], xmm6

	ADD rcx, 64


	MOV r10, laba_2
	JMP rax
align 16
	laba_2:

	PADDW xmm0, xmm8
	PADDW xmm1, xmm0
	PADDW xmm2, xmm1
	PADDW xmm3, xmm2

	MOVDQA xmm8, xmm3
	PUNPCKLQDQ xmm0, xmm1
	PUNPCKLQDQ xmm2, xmm3

	PADDW xmm4, xmm9
	PADDW xmm5, xmm4
	PADDW xmm6, xmm5
	PADDW xmm7, xmm6

	MOVDQA xmm9, xmm7
	PUNPCKLQDQ xmm4, xmm5
	PUNPCKLQDQ xmm6, xmm7


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm2
	MOVDQA [rcx+32], xmm4
	MOVDQA [rcx+64], xmm6

	ADD rcx, 64

			
	CMP dh, 2
	JZ no_bypass
	CMP dl, 2
	JZ bottom_set

	MOV r10, laba_3
	JMP rax
align 16
	laba_3:

		PADDW xmm0, xmm10
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2

		MOVDQA xmm10, xmm3
		PUNPCKLQDQ xmm0, xmm1
		PUNPCKLQDQ xmm2, xmm3

		PADDW xmm4, xmm11
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm11, xmm7
		PUNPCKLQDQ xmm4, xmm5
		PUNPCKLQDQ xmm6, xmm7


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm2
		MOVDQA [rcx+32], xmm4
		MOVDQA [rcx+64], xmm6

		ADD rcx, 64


	MOV r10, laba_4
	JMP rax
align 16
	laba_4:

		PADDW xmm0, xmm10
		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2

		MOVDQA xmm10, xmm3
		PUNPCKLQDQ xmm0, xmm1
		PUNPCKLQDQ xmm2, xmm3

		PADDW xmm4, xmm11
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6

		MOVDQA xmm11, xmm7
		PUNPCKLQDQ xmm4, xmm5
		PUNPCKLQDQ xmm6, xmm7


		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm2
		MOVDQA [rcx+32], xmm4
		MOVDQA [rcx+64], xmm6

		ADD rcx, 64



	align 16
		bottom_set:

	MOV r10, laba_5
	JMP rax
align 16
	laba_5:

			PADDW xmm0, xmm8
			PADDW xmm1, xmm0
			PADDW xmm2, xmm1
			PADDW xmm3, xmm2

			MOVDQA xmm8, xmm3
			PUNPCKLQDQ xmm0, xmm1
			PUNPCKLQDQ xmm2, xmm3

			PADDW xmm4, xmm9
			PADDW xmm5, xmm4
			PADDW xmm6, xmm5
			PADDW xmm7, xmm6

			MOVDQA xmm9, xmm7
			PUNPCKLQDQ xmm4, xmm5
			PUNPCKLQDQ xmm6, xmm7


			MOVDQA [rcx], xmm0
			MOVDQA [rcx+16], xmm2
			MOVDQA [rcx+32], xmm4
			MOVDQA [rcx+64], xmm6

			ADD rcx, 64


	MOV r10, laba_6
	JMP rax
align 16
	laba_6:

			PADDW xmm0, xmm8
			PADDW xmm1, xmm0
			PADDW xmm2, xmm1
			PADDW xmm3, xmm2

			MOVDQA xmm8, xmm3
			PUNPCKLQDQ xmm0, xmm1
			PUNPCKLQDQ xmm2, xmm3

			PADDW xmm4, xmm9
			PADDW xmm5, xmm4
			PADDW xmm6, xmm5
			PADDW xmm7, xmm6

			MOVDQA xmm9, xmm7
			PUNPCKLQDQ xmm4, xmm5
			PUNPCKLQDQ xmm6, xmm7


			MOVDQA [rcx], xmm0
			MOVDQA [rcx+16], xmm2
			MOVDQA [rcx+32], xmm4
			MOVDQA [rcx+64], xmm6

			ADD rcx, 64

			
			CMP dh, 2
			JZ no_bypass
			CMP dl, 2
			JZ no_bypass

	MOV r10, laba_7
	JMP rax
align 16
	laba_7:

				PADDW xmm0, xmm10
				PADDW xmm1, xmm0
				PADDW xmm2, xmm1
				PADDW xmm3, xmm2

				MOVDQA xmm10, xmm3
				PUNPCKLQDQ xmm0, xmm1
				PUNPCKLQDQ xmm2, xmm3

				PADDW xmm4, xmm11
				PADDW xmm5, xmm4
				PADDW xmm6, xmm5
				PADDW xmm7, xmm6

				MOVDQA xmm11, xmm7
				PUNPCKLQDQ xmm4, xmm5
				PUNPCKLQDQ xmm6, xmm7


				MOVDQA [rcx], xmm0
				MOVDQA [rcx+16], xmm2
				MOVDQA [rcx+32], xmm4
				MOVDQA [rcx+64], xmm6

				ADD rcx, 64


	MOV r10, laba_8
	JMP rax
align 16
	laba_8:

				PADDW xmm0, xmm10
				PADDW xmm1, xmm0
				PADDW xmm2, xmm1
				PADDW xmm3, xmm2

				MOVDQA xmm10, xmm3
				PUNPCKLQDQ xmm0, xmm1
				PUNPCKLQDQ xmm2, xmm3

				PADDW xmm4, xmm11
				PADDW xmm5, xmm4
				PADDW xmm6, xmm5
				PADDW xmm7, xmm6

				MOVDQA xmm11, xmm7
				PUNPCKLQDQ xmm4, xmm5
				PUNPCKLQDQ xmm6, xmm7


				MOVDQA [rcx], xmm0
				MOVDQA [rcx+16], xmm2
				MOVDQA [rcx+32], xmm4
				MOVDQA [rcx+64], xmm6

				ADD rcx, 64
					

align 16
	no_bypass:

	RET
H264_BypassAdjustV4x4a	ENDP




ALIGN 16
H264_BypassAdjustH8x8	PROC
	AND edx, edx
	JNZ field_set
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+4], 1
		PINSRW xmm0, WORD PTR [rcx+6], 2
		PINSRW xmm0, WORD PTR [rcx+18], 3
		PINSRW xmm0, WORD PTR [rcx+20], 4
		PINSRW xmm0, WORD PTR [rcx+40], 5
		PINSRW xmm0, WORD PTR [rcx+42], 6
		PINSRW xmm0, WORD PTR [rcx+70], 7
		

		PINSRW xmm1, WORD PTR [rcx+2], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+16], 2
		PINSRW xmm1, WORD PTR [rcx+22], 3
		PINSRW xmm1, WORD PTR [rcx+38], 4
		PINSRW xmm1, WORD PTR [rcx+44], 5
		PINSRW xmm1, WORD PTR [rcx+68], 6
		PINSRW xmm1, WORD PTR [rcx+72], 7


		PINSRW xmm2, WORD PTR [rcx+10], 0
		PINSRW xmm2, WORD PTR [rcx+14], 1
		PINSRW xmm2, WORD PTR [rcx+24], 2
		PINSRW xmm2, WORD PTR [rcx+36], 3
		PINSRW xmm2, WORD PTR [rcx+46], 4
		PINSRW xmm2, WORD PTR [rcx+66], 5
		PINSRW xmm2, WORD PTR [rcx+74], 6
		PINSRW xmm2, WORD PTR [rcx+96], 7
		

		PINSRW xmm3, WORD PTR [rcx+12], 0
		PINSRW xmm3, WORD PTR [rcx+26], 1
		PINSRW xmm3, WORD PTR [rcx+34], 2
		PINSRW xmm3, WORD PTR [rcx+48], 3
		PINSRW xmm3, WORD PTR [rcx+64], 4
		PINSRW xmm3, WORD PTR [rcx+76], 5
		PINSRW xmm3, WORD PTR [rcx+94], 6
		PINSRW xmm3, WORD PTR [rcx+98], 7


		PINSRW xmm4, WORD PTR [rcx+28], 0
		PINSRW xmm4, WORD PTR [rcx+32], 1
		PINSRW xmm4, WORD PTR [rcx+50], 2
		PINSRW xmm4, WORD PTR [rcx+62], 3
		PINSRW xmm4, WORD PTR [rcx+78], 4
		PINSRW xmm4, WORD PTR [rcx+92], 5
		PINSRW xmm4, WORD PTR [rcx+100], 6
		PINSRW xmm4, WORD PTR [rcx+114], 7
		

		PINSRW xmm5, WORD PTR [rcx+30], 0
		PINSRW xmm5, WORD PTR [rcx+52], 1
		PINSRW xmm5, WORD PTR [rcx+60], 2
		PINSRW xmm5, WORD PTR [rcx+80], 3
		PINSRW xmm5, WORD PTR [rcx+90], 4
		PINSRW xmm5, WORD PTR [rcx+102], 5
		PINSRW xmm5, WORD PTR [rcx+112], 6
		PINSRW xmm5, WORD PTR [rcx+116], 7


		PINSRW xmm6, WORD PTR [rcx+54], 0
		PINSRW xmm6, WORD PTR [rcx+58], 1
		PINSRW xmm6, WORD PTR [rcx+82], 2
		PINSRW xmm6, WORD PTR [rcx+88], 3
		PINSRW xmm6, WORD PTR [rcx+104], 4
		PINSRW xmm6, WORD PTR [rcx+110], 5
		PINSRW xmm6, WORD PTR [rcx+118], 6
		PINSRW xmm6, WORD PTR [rcx+124], 7
		

		PINSRW xmm7, WORD PTR [rcx+56], 0
		PINSRW xmm7, WORD PTR [rcx+84], 1
		PINSRW xmm7, WORD PTR [rcx+86], 2
		PINSRW xmm7, WORD PTR [rcx+106], 3
		PINSRW xmm7, WORD PTR [rcx+108], 4
		PINSRW xmm7, WORD PTR [rcx+120], 5
		PINSRW xmm7, WORD PTR [rcx+122], 6
		PINSRW xmm7, WORD PTR [rcx+126], 7


	JMP byop
align 16
	field_set:
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+2], 1
		PINSRW xmm0, WORD PTR [rcx+4], 2
		PINSRW xmm0, WORD PTR [rcx+10], 3
		PINSRW xmm0, WORD PTR [rcx+12], 4
		PINSRW xmm0, WORD PTR [rcx+20], 5
		PINSRW xmm0, WORD PTR [rcx+22], 6
		PINSRW xmm0, WORD PTR [rcx+24], 7
		

		PINSRW xmm1, WORD PTR [rcx+6], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+14], 2
		PINSRW xmm1, WORD PTR [rcx+18], 3
		PINSRW xmm1, WORD PTR [rcx+26], 4
		PINSRW xmm1, WORD PTR [rcx+34], 5
		PINSRW xmm1, WORD PTR [rcx+36], 6
		PINSRW xmm1, WORD PTR [rcx+38], 7


		PINSRW xmm2, WORD PTR [rcx+16], 0
		PINSRW xmm2, WORD PTR [rcx+28], 1
		PINSRW xmm2, WORD PTR [rcx+32], 2
		PINSRW xmm2, WORD PTR [rcx+40], 3
		PINSRW xmm2, WORD PTR [rcx+48], 4
		PINSRW xmm2, WORD PTR [rcx+50], 5
		PINSRW xmm2, WORD PTR [rcx+52], 6
		PINSRW xmm2, WORD PTR [rcx+54], 7
		

		PINSRW xmm3, WORD PTR [rcx+30], 0
		PINSRW xmm3, WORD PTR [rcx+42], 1
		PINSRW xmm3, WORD PTR [rcx+46], 2
		PINSRW xmm3, WORD PTR [rcx+56], 3
		PINSRW xmm3, WORD PTR [rcx+64], 4
		PINSRW xmm3, WORD PTR [rcx+66], 5
		PINSRW xmm3, WORD PTR [rcx+68], 6
		PINSRW xmm3, WORD PTR [rcx+70], 7


		PINSRW xmm4, WORD PTR [rcx+44], 0
		PINSRW xmm4, WORD PTR [rcx+58], 1
		PINSRW xmm4, WORD PTR [rcx+62], 2
		PINSRW xmm4, WORD PTR [rcx+72], 3
		PINSRW xmm4, WORD PTR [rcx+80], 4
		PINSRW xmm4, WORD PTR [rcx+82], 5
		PINSRW xmm4, WORD PTR [rcx+84], 6
		PINSRW xmm4, WORD PTR [rcx+86], 7
		

		PINSRW xmm5, WORD PTR [rcx+60], 0
		PINSRW xmm5, WORD PTR [rcx+74], 1
		PINSRW xmm5, WORD PTR [rcx+78], 2
		PINSRW xmm5, WORD PTR [rcx+88], 3
		PINSRW xmm5, WORD PTR [rcx+94], 4
		PINSRW xmm5, WORD PTR [rcx+96], 5
		PINSRW xmm5, WORD PTR [rcx+98], 6
		PINSRW xmm5, WORD PTR [rcx+100], 7


		PINSRW xmm6, WORD PTR [rcx+76], 0
		PINSRW xmm6, WORD PTR [rcx+90], 1
		PINSRW xmm6, WORD PTR [rcx+92], 2
		PINSRW xmm6, WORD PTR [rcx+102], 3
		PINSRW xmm6, WORD PTR [rcx+108], 4
		PINSRW xmm6, WORD PTR [rcx+110], 5
		PINSRW xmm6, WORD PTR [rcx+112], 6
		PINSRW xmm6, WORD PTR [rcx+114], 7
		

		PINSRW xmm7, WORD PTR [rcx+104], 0
		PINSRW xmm7, WORD PTR [rcx+106], 1
		PINSRW xmm7, WORD PTR [rcx+116], 2
		PINSRW xmm7, WORD PTR [rcx+118], 3
		PINSRW xmm7, WORD PTR [rcx+120], 4
		PINSRW xmm7, WORD PTR [rcx+122], 5
		PINSRW xmm7, WORD PTR [rcx+124], 6
		PINSRW xmm7, WORD PTR [rcx+126], 7
		
align 16
	byop:

		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6


		MOVDQA xmm8, xmm0
		MOVDQA xmm9, xmm2

		PUNPCKLWD xmm0, xmm1 ; 0	8	1	9	2	10	3	11
		PUNPCKHWD xmm8, xmm1 ; 4	12	5	13	6	14	7	15
		PUNPCKLWD xmm2, xmm3 ; 16	24	17	25	18	26	19	27
		PUNPCKHWD xmm9, xmm3 ; 20	28	21	29	22	30	23	31

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm8

		PUNPCKLDQ xmm0, xmm2 ; 0	8	16	24	1	9	17	25
		PUNPCKHDQ xmm1, xmm2 ; 2	10	18	26	3	11	19	27
		PUNPCKLDQ xmm3, xmm9 ; 4	12	20	28	5	13	21	29
		PUNPCKHDQ xmm8, xmm9 ; 6	14	22	30	7	15	23	31
				
		MOVDQA xmm2, [rcx+64]
		MOVDQA xmm4, [rcx+80]
		MOVDQA xmm5, [rcx+96]
		MOVDQA xmm6, [rcx+112]
				
		MOVDQA xmm7, xmm2
		MOVDQA xmm9, xmm5

		PUNPCKLWD xmm2, xmm4
		PUNPCKHWD xmm7, xmm4
		PUNPCKLWD xmm5, xmm6
		PUNPCKHWD xmm9, xmm6



		MOVDQA xmm4, xmm0
		MOVDQA xmm6, xmm1

		PUNPCKLDQ xmm0, xmm2 ; 0	8	16	24	32	40	48	56
		PUNPCKHDQ xmm4, xmm2 ; 1	9	17	25	33	41	49	57
		PUNPCKLDQ xmm1, xmm7 ; 2	10	18	26	34	42	50	58	
		PUNPCKHDQ xmm6, xmm7 ; 3	11	19	27	35	43	51	59

		MOVDQA xmm2, xmm3
		MOVDQA xmm7, xmm8

		PUNPCKLQDQ xmm2, xmm5
		PUNPCKHQDQ xmm3, xmm5
		PUNPCKLQDQ xmm7, xmm9
		PUNPCKHQDQ xmm8, xmm9
				

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm4
		MOVDQA [rcx+32], xmm1
		MOVDQA [rcx+48], xmm6
		MOVDQA [rcx+64], xmm2
		MOVDQA [rcx+80], xmm3
		MOVDQA [rcx+96], xmm7
		MOVDQA [rcx+112], xmm8


	RET
H264_BypassAdjustH8x8	ENDP

ALIGN 16
H264_BypassAdjustV8x8	PROC
	AND edx, edx
	JNZ field_set
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+2], 1
		PINSRW xmm0, WORD PTR [rcx+10], 2
		PINSRW xmm0, WORD PTR [rcx+12], 3
		PINSRW xmm0, WORD PTR [rcx+28], 4
		PINSRW xmm0, WORD PTR [rcx+30], 5
		PINSRW xmm0, WORD PTR [rcx+54], 6
		PINSRW xmm0, WORD PTR [rcx+56], 7
		

		PINSRW xmm1, WORD PTR [rcx+4], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+14], 2
		PINSRW xmm1, WORD PTR [rcx+26], 3
		PINSRW xmm1, WORD PTR [rcx+32], 4
		PINSRW xmm1, WORD PTR [rcx+52], 5
		PINSRW xmm1, WORD PTR [rcx+58], 6
		PINSRW xmm1, WORD PTR [rcx+84], 7


		PINSRW xmm2, WORD PTR [rcx+6], 0
		PINSRW xmm2, WORD PTR [rcx+8], 1
		PINSRW xmm2, WORD PTR [rcx+24], 2
		PINSRW xmm2, WORD PTR [rcx+34], 3
		PINSRW xmm2, WORD PTR [rcx+50], 4
		PINSRW xmm2, WORD PTR [rcx+60], 5
		PINSRW xmm2, WORD PTR [rcx+82], 6
		PINSRW xmm2, WORD PTR [rcx+86], 7
		

		PINSRW xmm3, WORD PTR [rcx+18], 0
		PINSRW xmm3, WORD PTR [rcx+22], 1
		PINSRW xmm3, WORD PTR [rcx+36], 2
		PINSRW xmm3, WORD PTR [rcx+48], 3
		PINSRW xmm3, WORD PTR [rcx+62], 4
		PINSRW xmm3, WORD PTR [rcx+80], 5
		PINSRW xmm3, WORD PTR [rcx+88], 6
		PINSRW xmm3, WORD PTR [rcx+106], 7


		PINSRW xmm4, WORD PTR [rcx+20], 0
		PINSRW xmm4, WORD PTR [rcx+38], 1
		PINSRW xmm4, WORD PTR [rcx+46], 2
		PINSRW xmm4, WORD PTR [rcx+64], 3
		PINSRW xmm4, WORD PTR [rcx+78], 4
		PINSRW xmm4, WORD PTR [rcx+90], 5
		PINSRW xmm4, WORD PTR [rcx+104], 6
		PINSRW xmm4, WORD PTR [rcx+108], 7
		

		PINSRW xmm5, WORD PTR [rcx+40], 0
		PINSRW xmm5, WORD PTR [rcx+44], 1
		PINSRW xmm5, WORD PTR [rcx+66], 2
		PINSRW xmm5, WORD PTR [rcx+76], 3
		PINSRW xmm5, WORD PTR [rcx+92], 4
		PINSRW xmm5, WORD PTR [rcx+102], 5
		PINSRW xmm5, WORD PTR [rcx+110], 6
		PINSRW xmm5, WORD PTR [rcx+120], 7


		PINSRW xmm6, WORD PTR [rcx+42], 0
		PINSRW xmm6, WORD PTR [rcx+68], 1
		PINSRW xmm6, WORD PTR [rcx+74], 2
		PINSRW xmm6, WORD PTR [rcx+94], 3
		PINSRW xmm6, WORD PTR [rcx+100], 4
		PINSRW xmm6, WORD PTR [rcx+112], 5
		PINSRW xmm6, WORD PTR [rcx+118], 6
		PINSRW xmm6, WORD PTR [rcx+122], 7
		

		PINSRW xmm7, WORD PTR [rcx+70], 0
		PINSRW xmm7, WORD PTR [rcx+72], 1
		PINSRW xmm7, WORD PTR [rcx+96], 2
		PINSRW xmm7, WORD PTR [rcx+98], 3
		PINSRW xmm7, WORD PTR [rcx+104], 4
		PINSRW xmm7, WORD PTR [rcx+106], 5
		PINSRW xmm7, WORD PTR [rcx+124], 6
		PINSRW xmm7, WORD PTR [rcx+126], 7


	JMP byop
align 16
	field_set:
		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+6], 1
		PINSRW xmm0, WORD PTR [rcx+16], 2
		PINSRW xmm0, WORD PTR [rcx+30], 3
		PINSRW xmm0, WORD PTR [rcx+44], 4
		PINSRW xmm0, WORD PTR [rcx+60], 5
		PINSRW xmm0, WORD PTR [rcx+76], 6
		PINSRW xmm0, WORD PTR [rcx+104], 7
		

		PINSRW xmm1, WORD PTR [rcx+2], 0
		PINSRW xmm1, WORD PTR [rcx+8], 1
		PINSRW xmm1, WORD PTR [rcx+28], 2
		PINSRW xmm1, WORD PTR [rcx+42], 3
		PINSRW xmm1, WORD PTR [rcx+58], 4
		PINSRW xmm1, WORD PTR [rcx+74], 5
		PINSRW xmm1, WORD PTR [rcx+90], 6
		PINSRW xmm1, WORD PTR [rcx+106], 7


		PINSRW xmm2, WORD PTR [rcx+4], 0
		PINSRW xmm2, WORD PTR [rcx+14], 1
		PINSRW xmm2, WORD PTR [rcx+32], 2
		PINSRW xmm2, WORD PTR [rcx+46], 3
		PINSRW xmm2, WORD PTR [rcx+62], 4
		PINSRW xmm2, WORD PTR [rcx+78], 5
		PINSRW xmm2, WORD PTR [rcx+92], 6
		PINSRW xmm2, WORD PTR [rcx+116], 7
		

		PINSRW xmm3, WORD PTR [rcx+10], 0
		PINSRW xmm3, WORD PTR [rcx+18], 1
		PINSRW xmm3, WORD PTR [rcx+40], 2
		PINSRW xmm3, WORD PTR [rcx+56], 3
		PINSRW xmm3, WORD PTR [rcx+72], 4
		PINSRW xmm3, WORD PTR [rcx+88], 5
		PINSRW xmm3, WORD PTR [rcx+102], 6
		PINSRW xmm3, WORD PTR [rcx+118], 7


		PINSRW xmm4, WORD PTR [rcx+12], 0
		PINSRW xmm4, WORD PTR [rcx+26], 1
		PINSRW xmm4, WORD PTR [rcx+48], 2
		PINSRW xmm4, WORD PTR [rcx+64], 3
		PINSRW xmm4, WORD PTR [rcx+80], 4
		PINSRW xmm4, WORD PTR [rcx+94], 5
		PINSRW xmm4, WORD PTR [rcx+108], 6
		PINSRW xmm4, WORD PTR [rcx+120], 7
		

		PINSRW xmm5, WORD PTR [rcx+20], 0
		PINSRW xmm5, WORD PTR [rcx+34], 1
		PINSRW xmm5, WORD PTR [rcx+50], 2
		PINSRW xmm5, WORD PTR [rcx+66], 3
		PINSRW xmm5, WORD PTR [rcx+82], 4
		PINSRW xmm5, WORD PTR [rcx+96], 5
		PINSRW xmm5, WORD PTR [rcx+110], 6
		PINSRW xmm5, WORD PTR [rcx+122], 7


		PINSRW xmm6, WORD PTR [rcx+22], 0
		PINSRW xmm6, WORD PTR [rcx+36], 1
		PINSRW xmm6, WORD PTR [rcx+52], 2
		PINSRW xmm6, WORD PTR [rcx+68], 3
		PINSRW xmm6, WORD PTR [rcx+84], 4
		PINSRW xmm6, WORD PTR [rcx+98], 5
		PINSRW xmm6, WORD PTR [rcx+112], 6
		PINSRW xmm6, WORD PTR [rcx+124], 7
		

		PINSRW xmm7, WORD PTR [rcx+24], 0
		PINSRW xmm7, WORD PTR [rcx+38], 1
		PINSRW xmm7, WORD PTR [rcx+54], 2
		PINSRW xmm7, WORD PTR [rcx+70], 3
		PINSRW xmm7, WORD PTR [rcx+86], 4
		PINSRW xmm7, WORD PTR [rcx+100], 5
		PINSRW xmm7, WORD PTR [rcx+114], 6
		PINSRW xmm7, WORD PTR [rcx+126], 7
		
align 16
	byop:

		PADDW xmm1, xmm0
		PADDW xmm2, xmm1
		PADDW xmm3, xmm2
		PADDW xmm4, xmm3
		PADDW xmm5, xmm4
		PADDW xmm6, xmm5
		PADDW xmm7, xmm6
			

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm2
		MOVDQA [rcx+48], xmm3
		MOVDQA [rcx+64], xmm4
		MOVDQA [rcx+80], xmm5
		MOVDQA [rcx+96], xmm6
		MOVDQA [rcx+112], xmm7


	RET
H264_BypassAdjustV8x8	ENDP

ALIGN 16
H264_Scan0Reverse8x8	PROC

	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+2], 1
	PINSRW xmm0, WORD PTR [rcx+10], 2
	PINSRW xmm0, WORD PTR [rcx+12], 3
	PINSRW xmm0, WORD PTR [rcx+28], 4
	PINSRW xmm0, WORD PTR [rcx+30], 5
	PINSRW xmm0, WORD PTR [rcx+54], 6
	PINSRW xmm0, WORD PTR [rcx+56], 7
		

	PINSRW xmm1, WORD PTR [rcx+4], 0
	PINSRW xmm1, WORD PTR [rcx+8], 1
	PINSRW xmm1, WORD PTR [rcx+14], 2
	PINSRW xmm1, WORD PTR [rcx+26], 3
	PINSRW xmm1, WORD PTR [rcx+32], 4
	PINSRW xmm1, WORD PTR [rcx+52], 5
	PINSRW xmm1, WORD PTR [rcx+58], 6
	PINSRW xmm1, WORD PTR [rcx+84], 7


	PINSRW xmm2, WORD PTR [rcx+6], 0
	PINSRW xmm2, WORD PTR [rcx+8], 1
	PINSRW xmm2, WORD PTR [rcx+24], 2
	PINSRW xmm2, WORD PTR [rcx+34], 3
	PINSRW xmm2, WORD PTR [rcx+50], 4
	PINSRW xmm2, WORD PTR [rcx+60], 5
	PINSRW xmm2, WORD PTR [rcx+82], 6
	PINSRW xmm2, WORD PTR [rcx+86], 7
		

	PINSRW xmm3, WORD PTR [rcx+18], 0
	PINSRW xmm3, WORD PTR [rcx+22], 1
	PINSRW xmm3, WORD PTR [rcx+36], 2
	PINSRW xmm3, WORD PTR [rcx+48], 3
	PINSRW xmm3, WORD PTR [rcx+62], 4
	PINSRW xmm3, WORD PTR [rcx+80], 5
	PINSRW xmm3, WORD PTR [rcx+88], 6
	PINSRW xmm3, WORD PTR [rcx+106], 7


	PINSRW xmm4, WORD PTR [rcx+20], 0
	PINSRW xmm4, WORD PTR [rcx+38], 1
	PINSRW xmm4, WORD PTR [rcx+46], 2
	PINSRW xmm4, WORD PTR [rcx+64], 3
	PINSRW xmm4, WORD PTR [rcx+78], 4
	PINSRW xmm4, WORD PTR [rcx+90], 5
	PINSRW xmm4, WORD PTR [rcx+104], 6
	PINSRW xmm4, WORD PTR [rcx+108], 7
		

	PINSRW xmm5, WORD PTR [rcx+40], 0
	PINSRW xmm5, WORD PTR [rcx+44], 1
	PINSRW xmm5, WORD PTR [rcx+66], 2
	PINSRW xmm5, WORD PTR [rcx+76], 3
	PINSRW xmm5, WORD PTR [rcx+92], 4
	PINSRW xmm5, WORD PTR [rcx+102], 5
	PINSRW xmm5, WORD PTR [rcx+110], 6
	PINSRW xmm5, WORD PTR [rcx+120], 7


	PINSRW xmm6, WORD PTR [rcx+42], 0
	PINSRW xmm6, WORD PTR [rcx+68], 1
	PINSRW xmm6, WORD PTR [rcx+74], 2
	PINSRW xmm6, WORD PTR [rcx+94], 3
	PINSRW xmm6, WORD PTR [rcx+100], 4
	PINSRW xmm6, WORD PTR [rcx+112], 5
	PINSRW xmm6, WORD PTR [rcx+118], 6
	PINSRW xmm6, WORD PTR [rcx+122], 7
		

	PINSRW xmm7, WORD PTR [rcx+70], 0
	PINSRW xmm7, WORD PTR [rcx+72], 1
	PINSRW xmm7, WORD PTR [rcx+96], 2
	PINSRW xmm7, WORD PTR [rcx+98], 3
	PINSRW xmm7, WORD PTR [rcx+104], 4
	PINSRW xmm7, WORD PTR [rcx+106], 5
	PINSRW xmm7, WORD PTR [rcx+124], 6
	PINSRW xmm7, WORD PTR [rcx+126], 7


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_Scan0Reverse8x8	ENDP

ALIGN 16
H264_Scan1Reverse8x8	PROC
	PINSRW xmm0, WORD PTR [rcx], 0
	PINSRW xmm0, WORD PTR [rcx+6], 1
	PINSRW xmm0, WORD PTR [rcx+16], 2
	PINSRW xmm0, WORD PTR [rcx+30], 3
	PINSRW xmm0, WORD PTR [rcx+44], 4
	PINSRW xmm0, WORD PTR [rcx+60], 5
	PINSRW xmm0, WORD PTR [rcx+76], 6
	PINSRW xmm0, WORD PTR [rcx+104], 7
		

	PINSRW xmm1, WORD PTR [rcx+2], 0
	PINSRW xmm1, WORD PTR [rcx+8], 1
	PINSRW xmm1, WORD PTR [rcx+28], 2
	PINSRW xmm1, WORD PTR [rcx+42], 3
	PINSRW xmm1, WORD PTR [rcx+58], 4
	PINSRW xmm1, WORD PTR [rcx+74], 5
	PINSRW xmm1, WORD PTR [rcx+90], 6
	PINSRW xmm1, WORD PTR [rcx+106], 7


	PINSRW xmm2, WORD PTR [rcx+4], 0
	PINSRW xmm2, WORD PTR [rcx+14], 1
	PINSRW xmm2, WORD PTR [rcx+32], 2
	PINSRW xmm2, WORD PTR [rcx+46], 3
	PINSRW xmm2, WORD PTR [rcx+62], 4
	PINSRW xmm2, WORD PTR [rcx+78], 5
	PINSRW xmm2, WORD PTR [rcx+92], 6
	PINSRW xmm2, WORD PTR [rcx+116], 7
		

	PINSRW xmm3, WORD PTR [rcx+10], 0
	PINSRW xmm3, WORD PTR [rcx+18], 1
	PINSRW xmm3, WORD PTR [rcx+40], 2
	PINSRW xmm3, WORD PTR [rcx+56], 3
	PINSRW xmm3, WORD PTR [rcx+72], 4
	PINSRW xmm3, WORD PTR [rcx+88], 5
	PINSRW xmm3, WORD PTR [rcx+102], 6
	PINSRW xmm3, WORD PTR [rcx+118], 7


	PINSRW xmm4, WORD PTR [rcx+12], 0
	PINSRW xmm4, WORD PTR [rcx+26], 1
	PINSRW xmm4, WORD PTR [rcx+48], 2
	PINSRW xmm4, WORD PTR [rcx+64], 3
	PINSRW xmm4, WORD PTR [rcx+80], 4
	PINSRW xmm4, WORD PTR [rcx+94], 5
	PINSRW xmm4, WORD PTR [rcx+108], 6
	PINSRW xmm4, WORD PTR [rcx+120], 7
		

	PINSRW xmm5, WORD PTR [rcx+20], 0
	PINSRW xmm5, WORD PTR [rcx+34], 1
	PINSRW xmm5, WORD PTR [rcx+50], 2
	PINSRW xmm5, WORD PTR [rcx+66], 3
	PINSRW xmm5, WORD PTR [rcx+82], 4
	PINSRW xmm5, WORD PTR [rcx+96], 5
	PINSRW xmm5, WORD PTR [rcx+110], 6
	PINSRW xmm5, WORD PTR [rcx+122], 7


	PINSRW xmm6, WORD PTR [rcx+22], 0
	PINSRW xmm6, WORD PTR [rcx+36], 1
	PINSRW xmm6, WORD PTR [rcx+52], 2
	PINSRW xmm6, WORD PTR [rcx+68], 3
	PINSRW xmm6, WORD PTR [rcx+84], 4
	PINSRW xmm6, WORD PTR [rcx+98], 5
	PINSRW xmm6, WORD PTR [rcx+112], 6
	PINSRW xmm6, WORD PTR [rcx+124], 7
		

	PINSRW xmm7, WORD PTR [rcx+24], 0
	PINSRW xmm7, WORD PTR [rcx+38], 1
	PINSRW xmm7, WORD PTR [rcx+54], 2
	PINSRW xmm7, WORD PTR [rcx+70], 3
	PINSRW xmm7, WORD PTR [rcx+86], 4
	PINSRW xmm7, WORD PTR [rcx+100], 5
	PINSRW xmm7, WORD PTR [rcx+114], 6
	PINSRW xmm7, WORD PTR [rcx+126], 7


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_Scan1Reverse8x8	ENDP

ALIGN 16
H264_Scan0Reverse4x4	PROC

		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+2], 1
		PINSRW xmm0, WORD PTR [rcx+10], 2
		PINSRW xmm0, WORD PTR [rcx+12], 3
		PINSRW xmm0, WORD PTR [rcx+4], 4
		PINSRW xmm0, WORD PTR [rcx+8], 5
		PINSRW xmm0, WORD PTR [rcx+14], 6
		PINSRW xmm0, WORD PTR [rcx+24], 7

		PINSRW xmm1, WORD PTR [rcx+6], 0
		PINSRW xmm1, WORD PTR [rcx+16], 1
		PINSRW xmm1, WORD PTR [rcx+22], 2
		PINSRW xmm1, WORD PTR [rcx+26], 3
		PINSRW xmm1, WORD PTR [rcx+18], 4
		PINSRW xmm1, WORD PTR [rcx+20], 5
		PINSRW xmm1, WORD PTR [rcx+28], 6
		PINSRW xmm1, WORD PTR [rcx+30], 7

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1

		ADD rcx, 32

	DEC edx
	JNZ H264_Scan0Reverse4x4


	RET
H264_Scan0Reverse4x4	ENDP

ALIGN 16
H264_Scan1Reverse4x4	PROC

		PINSRW xmm0, WORD PTR [rcx], 0
		PINSRW xmm0, WORD PTR [rcx+4], 1
		PINSRW xmm0, WORD PTR [rcx+16], 2
		PINSRW xmm0, WORD PTR [rcx+24], 3
		PINSRW xmm0, WORD PTR [rcx+2], 4
		PINSRW xmm0, WORD PTR [rcx+10], 5
		PINSRW xmm0, WORD PTR [rcx+18], 6
		PINSRW xmm0, WORD PTR [rcx+26], 7

		PINSRW xmm1, WORD PTR [rcx+6], 0
		PINSRW xmm1, WORD PTR [rcx+12], 1
		PINSRW xmm1, WORD PTR [rcx+20], 2
		PINSRW xmm1, WORD PTR [rcx+28], 3
		PINSRW xmm1, WORD PTR [rcx+8], 4
		PINSRW xmm1, WORD PTR [rcx+14], 5
		PINSRW xmm1, WORD PTR [rcx+22], 6
		PINSRW xmm1, WORD PTR [rcx+30], 7

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1

		ADD rcx, 32

	DEC edx
	JNZ H264_Scan1Reverse4x4


	RET
H264_Scan1Reverse4x4	ENDP

END

