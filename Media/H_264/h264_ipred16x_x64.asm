
; safe-fail video codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE

.CONST
ALIGN 16
i_scale			WORD	1, 2, 3, 4, 5, 6, 7, 8

i_vector		WORD	-7, -7,		-6, -7,		-5, -7,		-4, -7
ix_inc			WORD	4, 0,		4, 0,		4, 0,		4, 0
iy_inc			WORD	0, 1,		0, 1,		0, 1,		0, 1




.CODE

ALIGN 16
H264_VerticalIPred16	PROC
	MOVDQA xmm0, [rdx+480]
	MOVDQA xmm2, [rdx+496]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2

	PUNPCKLQDQ	xmm0, xmm0
	PUNPCKHQDQ	xmm1, xmm1
	PUNPCKLQDQ	xmm2, xmm2
	PUNPCKHQDQ	xmm3, xmm3

	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm0
	MOVDQA xmm6, xmm0	
	MOVDQA xmm7, xmm0

	MOVDQA xmm8, xmm1
	MOVDQA xmm9, xmm1
	MOVDQA xmm10, xmm1	
	MOVDQA xmm11, xmm1

	PADDW xmm4, [rcx]
	PADDW xmm5, [rcx+16]
	PADDW xmm8, [rcx+32]
	PADDW xmm9, [rcx+48]
	PADDW xmm6, [rcx+64]
	PADDW xmm7, [rcx+80]
	PADDW xmm10, [rcx+96]
	PADDW xmm11, [rcx+112]

	MOVDQA [rcx], xmm4
	MOVDQA [rcx+16], xmm5
	MOVDQA [rcx+32], xmm8
	MOVDQA [rcx+48], xmm9
	MOVDQA [rcx+64], xmm6
	MOVDQA [rcx+80], xmm7
	MOVDQA [rcx+96], xmm10
	MOVDQA [rcx+112], xmm11


	MOVDQA xmm4, xmm2
	MOVDQA xmm5, xmm2
	MOVDQA xmm6, xmm2	
	MOVDQA xmm7, xmm2

	MOVDQA xmm8, xmm3
	MOVDQA xmm9, xmm3
	MOVDQA xmm10, xmm3
	MOVDQA xmm11, xmm3

	PADDW xmm4, [rcx+128]
	PADDW xmm5, [rcx+144]
	PADDW xmm8, [rcx+160]
	PADDW xmm9, [rcx+176]
	PADDW xmm6, [rcx+192]
	PADDW xmm7, [rcx+208]
	PADDW xmm10, [rcx+224]
	PADDW xmm11, [rcx+240]

	MOVDQA [rcx+128], xmm4
	MOVDQA [rcx+144], xmm5
	MOVDQA [rcx+160], xmm8
	MOVDQA [rcx+176], xmm9
	MOVDQA [rcx+192], xmm6
	MOVDQA [rcx+208], xmm7
	MOVDQA [rcx+224], xmm10
	MOVDQA [rcx+240], xmm11


	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm0
	MOVDQA xmm6, xmm0	
	MOVDQA xmm7, xmm0

	MOVDQA xmm8, xmm1
	MOVDQA xmm9, xmm1
	MOVDQA xmm10, xmm1	
	MOVDQA xmm11, xmm1

	PADDW xmm4, [rcx+256]
	PADDW xmm5, [rcx+272]
	PADDW xmm8, [rcx+288]
	PADDW xmm9, [rcx+304]
	PADDW xmm6, [rcx+320]
	PADDW xmm7, [rcx+336]
	PADDW xmm10, [rcx+352]
	PADDW xmm11, [rcx+368]

	MOVDQA [rcx+256], xmm4
	MOVDQA [rcx+272], xmm5
	MOVDQA [rcx+288], xmm8
	MOVDQA [rcx+304], xmm9
	MOVDQA [rcx+320], xmm6
	MOVDQA [rcx+336], xmm7
	MOVDQA [rcx+352], xmm10
	MOVDQA [rcx+368], xmm11


	MOVDQA xmm4, xmm2
	MOVDQA xmm5, xmm2
	MOVDQA xmm6, xmm2	
	MOVDQA xmm7, xmm2

	MOVDQA xmm8, xmm3
	MOVDQA xmm9, xmm3
	MOVDQA xmm10, xmm3	
	MOVDQA xmm11, xmm3

	PADDW xmm4, [rcx+384]
	PADDW xmm5, [rcx+400]
	PADDW xmm8, [rcx+416]
	PADDW xmm9, [rcx+432]
	PADDW xmm6, [rcx+448]
	PADDW xmm7, [rcx+464]
	PADDW xmm10, [rcx+480]
	PADDW xmm11, [rcx+496]

	MOVDQA [rcx+384], xmm4
	MOVDQA [rcx+400], xmm5
	MOVDQA [rcx+416], xmm8
	MOVDQA [rcx+432], xmm9
	MOVDQA [rcx+448], xmm6
	MOVDQA [rcx+464], xmm7
	MOVDQA [rcx+480], xmm10
	MOVDQA [rcx+496], xmm11


	RET
H264_VerticalIPred16	ENDP

ALIGN 16
H264_HorizontalIPred16	PROC
	PINSRW xmm0, WORD PTR [rdx+30], 0
	PINSRW xmm0, WORD PTR [rdx+30], 1
	PINSRW xmm0, WORD PTR [rdx+62], 2
	PINSRW xmm0, WORD PTR [rdx+62], 3

	PUNPCKLDQ xmm0, xmm0

	PINSRW xmm1, WORD PTR [rdx+94], 0
	PINSRW xmm1, WORD PTR [rdx+94], 1
	PINSRW xmm1, WORD PTR [rdx+126], 2
	PINSRW xmm1, WORD PTR [rdx+126], 3

	PUNPCKLDQ xmm1, xmm1

	PINSRW xmm2, WORD PTR [rdx+158], 0
	PINSRW xmm2, WORD PTR [rdx+158], 1
	PINSRW xmm2, WORD PTR [rdx+190], 2
	PINSRW xmm2, WORD PTR [rdx+190], 3

	PUNPCKLDQ xmm2, xmm2

	PINSRW xmm3, WORD PTR [rdx+222], 0
	PINSRW xmm3, WORD PTR [rdx+222], 1
	PINSRW xmm3, WORD PTR [rdx+254], 2
	PINSRW xmm3, WORD PTR [rdx+254], 3

	PUNPCKLDQ xmm3, xmm3


	PINSRW xmm4, WORD PTR [rdx+286], 0
	PINSRW xmm4, WORD PTR [rdx+286], 1
	PINSRW xmm4, WORD PTR [rdx+318], 2
	PINSRW xmm4, WORD PTR [rdx+318], 3

	PUNPCKLDQ xmm4, xmm4

	PINSRW xmm5, WORD PTR [rdx+350], 0
	PINSRW xmm5, WORD PTR [rdx+350], 1
	PINSRW xmm5, WORD PTR [rdx+382], 2
	PINSRW xmm5, WORD PTR [rdx+382], 3

	PUNPCKLDQ xmm5, xmm5

	PINSRW xmm6, WORD PTR [rdx+414], 0
	PINSRW xmm6, WORD PTR [rdx+414], 1
	PINSRW xmm6, WORD PTR [rdx+446], 2
	PINSRW xmm6, WORD PTR [rdx+446], 3

	PUNPCKLDQ xmm6, xmm6

	PINSRW xmm7, WORD PTR [rdx+478], 0
	PINSRW xmm7, WORD PTR [rdx+478], 1
	PINSRW xmm7, WORD PTR [rdx+510], 2
	PINSRW xmm7, WORD PTR [rdx+510], 3

	PUNPCKLDQ xmm7, xmm7



	MOVDQA xmm8, xmm0
	MOVDQA xmm9, xmm1
	MOVDQA xmm10, xmm0
	MOVDQA xmm11, xmm1

	MOVDQA xmm12, xmm2
	MOVDQA xmm13, xmm3
	MOVDQA xmm14, xmm2
	MOVDQA xmm15, xmm3
	
	PADDW xmm8, [rcx]
	PADDW xmm9, [rcx+16]
	PADDW xmm10, [rcx+32]
	PADDW xmm11, [rcx+48]
	PADDW xmm12, [rcx+64]
	PADDW xmm13, [rcx+80]
	PADDW xmm14, [rcx+96]
	PADDW xmm15, [rcx+112]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm9
	MOVDQA [rcx+32], xmm10
	MOVDQA [rcx+48], xmm11
	MOVDQA [rcx+64], xmm12
	MOVDQA [rcx+80], xmm13
	MOVDQA [rcx+96], xmm14
	MOVDQA [rcx+112], xmm15


	MOVDQA xmm8, xmm0
	MOVDQA xmm9, xmm1
	MOVDQA xmm10, xmm0
	MOVDQA xmm11, xmm1

	MOVDQA xmm12, xmm2
	MOVDQA xmm13, xmm3
	MOVDQA xmm14, xmm2
	MOVDQA xmm15, xmm3
	
	PADDW xmm8, [rcx+128]
	PADDW xmm9, [rcx+144]
	PADDW xmm10, [rcx+160]
	PADDW xmm11, [rcx+176]
	PADDW xmm12, [rcx+192]
	PADDW xmm13, [rcx+208]
	PADDW xmm14, [rcx+224]
	PADDW xmm15, [rcx+240]

	MOVDQA [rcx+128], xmm8
	MOVDQA [rcx+144], xmm9
	MOVDQA [rcx+160], xmm10
	MOVDQA [rcx+176], xmm11
	MOVDQA [rcx+192], xmm12
	MOVDQA [rcx+208], xmm13
	MOVDQA [rcx+224], xmm14
	MOVDQA [rcx+240], xmm15

	MOVDQA xmm8, xmm4
	MOVDQA xmm9, xmm5
	MOVDQA xmm10, xmm4
	MOVDQA xmm11, xmm5

	MOVDQA xmm12, xmm6
	MOVDQA xmm13, xmm7
	MOVDQA xmm14, xmm6
	MOVDQA xmm15, xmm7
	
	PADDW xmm8, [rcx+256]
	PADDW xmm9, [rcx+272]
	PADDW xmm10, [rcx+288]
	PADDW xmm11, [rcx+304]
	PADDW xmm12, [rcx+320]
	PADDW xmm13, [rcx+336]
	PADDW xmm14, [rcx+352]
	PADDW xmm15, [rcx+368]

	MOVDQA [rcx+256], xmm8
	MOVDQA [rcx+272], xmm9
	MOVDQA [rcx+288], xmm10
	MOVDQA [rcx+304], xmm11
	MOVDQA [rcx+320], xmm12
	MOVDQA [rcx+336], xmm13
	MOVDQA [rcx+352], xmm14
	MOVDQA [rcx+368], xmm15

	MOVDQA xmm8, xmm4
	MOVDQA xmm9, xmm5
	MOVDQA xmm10, xmm4
	MOVDQA xmm11, xmm5

	MOVDQA xmm12, xmm6
	MOVDQA xmm13, xmm7
	MOVDQA xmm14, xmm6
	MOVDQA xmm15, xmm7
	
	PADDW xmm8, [rcx+384]
	PADDW xmm9, [rcx+400]
	PADDW xmm10, [rcx+416]
	PADDW xmm11, [rcx+432]
	PADDW xmm12, [rcx+448]
	PADDW xmm13, [rcx+464]
	PADDW xmm14, [rcx+480]
	PADDW xmm15, [rcx+496]

	MOVDQA [rcx+384], xmm8
	MOVDQA [rcx+400], xmm9
	MOVDQA [rcx+416], xmm10
	MOVDQA [rcx+432], xmm11
	MOVDQA [rcx+448], xmm12
	MOVDQA [rcx+464], xmm13
	MOVDQA [rcx+480], xmm14
	MOVDQA [rcx+496], xmm15

	RET
H264_HorizontalIPred16	ENDP

ALIGN 16
H264_LevelShift16	PROC
	MOV r9d, edx
	SHL r9d, 16
	MOV r9w, dx

	MOVD xmm0, r9d
	PSHUFD xmm0, xmm0, 0

	MOVDQA xmm1, xmm0
	MOVDQA xmm2, xmm0
	MOVDQA xmm3, xmm0
	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm0
	MOVDQA xmm6, xmm0
	MOVDQA xmm7, xmm0
	MOVDQA xmm8, xmm0

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7



	ADD rcx, 128

	MOVDQA xmm0, xmm8
	MOVDQA xmm1, xmm8
	MOVDQA xmm2, xmm8
	MOVDQA xmm3, xmm8
	MOVDQA xmm4, xmm8
	MOVDQA xmm5, xmm8
	MOVDQA xmm6, xmm8
	MOVDQA xmm7, xmm8

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7


	ADD rcx, 128

	MOVDQA xmm0, xmm8
	MOVDQA xmm1, xmm8
	MOVDQA xmm2, xmm8
	MOVDQA xmm3, xmm8
	MOVDQA xmm4, xmm8
	MOVDQA xmm5, xmm8
	MOVDQA xmm6, xmm8
	MOVDQA xmm7, xmm8

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	ADD rcx, 128

	MOVDQA xmm0, xmm8
	MOVDQA xmm1, xmm8
	MOVDQA xmm2, xmm8
	MOVDQA xmm3, xmm8
	MOVDQA xmm4, xmm8
	MOVDQA xmm5, xmm8
	MOVDQA xmm6, xmm8
	MOVDQA xmm7, xmm8

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm2, [rcx+32]
	PADDW xmm3, [rcx+48]
	PADDW xmm4, [rcx+64]
	PADDW xmm5, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]

	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7


	RET
H264_LevelShift16	ENDP

ALIGN 16
H264_DCIPred16	PROC
	MOV rax, rcx
	
	XOR r10, r10
	MOV rcx, 3
	

	AND rdx, rdx
	JZ no_left
		MOVSX r11d, WORD PTR [rdx+30]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+62]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+94]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+126]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+158]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+190]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+222]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+254]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+286]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+318]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+350]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+382]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+414]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+446]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+478]
		ADD r10d, r11d
		MOVSX r11d, WORD PTR [rdx+510]
		ADD r10d, r11d

		ADD r10d, 8

		INC cl
align 16
	no_left:
		AND r8, r8
		JZ do_fill
			MOVSX r11d, WORD PTR [r8+480]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+482]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+484]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+486]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+488]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+490]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+492]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+494]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+496]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+498]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+500]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+502]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+504]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+506]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+508]
			ADD r10d, r11d
			MOVSX r11d, WORD PTR [r8+510]
			ADD r10d, r11d
			
			ADD r10d, 8

			INC cl	

align 16
	do_fill:
		SAR r10d, cl
		
		MOV r9d, r10d
		SHL r10d, 16
		MOV r10w, r9w



		MOVD xmm0, r10d
		PSHUFD xmm0, xmm0, 0

		MOVDQA xmm1, xmm0
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm0
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm0
		MOVDQA xmm8, xmm0

		PADDW xmm1, [rax]
		PADDW xmm2, [rax+16]
		PADDW xmm3, [rax+32]
		PADDW xmm4, [rax+48]
		PADDW xmm5, [rax+64]
		PADDW xmm6, [rax+80]
		PADDW xmm7, [rax+96]
		PADDW xmm8, [rax+112]

		MOVDQA [rax], xmm1
		MOVDQA [rax+16], xmm2
		MOVDQA [rax+32], xmm3
		MOVDQA [rax+48], xmm4
		MOVDQA [rax+64], xmm5
		MOVDQA [rax+80], xmm6
		MOVDQA [rax+96], xmm7
		MOVDQA [rax+112], xmm8

		MOVDQA xmm1, xmm0
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm0
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm0
		MOVDQA xmm8, xmm0

		PADDW xmm1, [rax+128]
		PADDW xmm2, [rax+144]
		PADDW xmm3, [rax+160]
		PADDW xmm4, [rax+176]
		PADDW xmm5, [rax+192]
		PADDW xmm6, [rax+208]
		PADDW xmm7, [rax+224]
		PADDW xmm8, [rax+240]

		MOVDQA [rax+128], xmm1
		MOVDQA [rax+144], xmm2
		MOVDQA [rax+160], xmm3
		MOVDQA [rax+176], xmm4
		MOVDQA [rax+192], xmm5
		MOVDQA [rax+208], xmm6
		MOVDQA [rax+224], xmm7
		MOVDQA [rax+240], xmm8

		MOVDQA xmm1, xmm0
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm0
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm0
		MOVDQA xmm8, xmm0

		PADDW xmm1, [rax+256]
		PADDW xmm2, [rax+272]
		PADDW xmm3, [rax+288]
		PADDW xmm4, [rax+304]
		PADDW xmm5, [rax+320]
		PADDW xmm6, [rax+336]
		PADDW xmm7, [rax+352]
		PADDW xmm8, [rax+368]

		MOVDQA [rax+256], xmm1
		MOVDQA [rax+272], xmm2
		MOVDQA [rax+288], xmm3
		MOVDQA [rax+304], xmm4
		MOVDQA [rax+320], xmm5
		MOVDQA [rax+336], xmm6
		MOVDQA [rax+352], xmm7
		MOVDQA [rax+368], xmm8

		MOVDQA xmm1, xmm0
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm0
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm0
		MOVDQA xmm8, xmm0

		PADDW xmm1, [rax+384]
		PADDW xmm2, [rax+400]
		PADDW xmm3, [rax+416]
		PADDW xmm4, [rax+432]
		PADDW xmm5, [rax+448]
		PADDW xmm6, [rax+464]
		PADDW xmm7, [rax+480]
		PADDW xmm8, [rax+496]

		MOVDQA [rax+384], xmm1
		MOVDQA [rax+400], xmm2
		MOVDQA [rax+416], xmm3
		MOVDQA [rax+432], xmm4
		MOVDQA [rax+448], xmm5
		MOVDQA [rax+464], xmm6
		MOVDQA [rax+480], xmm7
		MOVDQA [rax+496], xmm8


	RET
H264_DCIPred16	ENDP

ALIGN 16
H264_PlaneIPred16	PROC
	MOVDQA xmm7, XMMWORD PTR [i_scale]
	

	PINSRW xmm0, r9d, 7
	PINSRW xmm0, WORD PTR [rdx+30], 6
	PINSRW xmm0, WORD PTR [rdx+62], 5
	PINSRW xmm0, WORD PTR [rdx+94], 4
	PINSRW xmm0, WORD PTR [rdx+126], 3
	PINSRW xmm0, WORD PTR [rdx+158], 2
	PINSRW xmm0, WORD PTR [rdx+190], 1
	PINSRW xmm0, WORD PTR [rdx+222], 0


	PINSRW xmm1, WORD PTR [rdx+286], 0
	PINSRW xmm1, WORD PTR [rdx+318], 1
	PINSRW xmm1, WORD PTR [rdx+350], 2
	PINSRW xmm1, WORD PTR [rdx+382], 3
	PINSRW xmm1, WORD PTR [rdx+414], 4
	PINSRW xmm1, WORD PTR [rdx+446], 5
	PINSRW xmm1, WORD PTR [rdx+478], 6
	PINSRW xmm1, WORD PTR [rdx+510], 7


	PINSRW xmm2, r9d, 7
	PINSRW xmm2, WORD PTR [r8+480], 6
	PINSRW xmm2, WORD PTR [r8+482], 5
	PINSRW xmm2, WORD PTR [r8+484], 4
	PINSRW xmm2, WORD PTR [r8+486], 3
	PINSRW xmm2, WORD PTR [r8+488], 2
	PINSRW xmm2, WORD PTR [r8+490], 1
	PINSRW xmm2, WORD PTR [r8+492], 0
	
	MOVDQA xmm3, [r8+496]

	PCMPEQW xmm4, xmm4
	PSLLD xmm4, 5

	MOVSX eax, WORD PTR [rdx+510]
	MOVSX edx, WORD PTR [r8+510]
	ADD eax, edx
	INC eax
	SHL eax, 4
	
	MOVD xmm5, eax
	PSHUFD xmm5, xmm5, 0 ; a+16, a+16, a+16, a+16
	

	PSUBW xmm1, xmm0 ; V
	PSUBW xmm3, xmm2 ; H

	PMADDWD xmm1, xmm7
	PMADDWD xmm3, xmm7
	
	PHADDD xmm3, xmm1
	PHADDD xmm3, xmm3 ; H, V

	MOVDQA xmm6, xmm3
	PSLLD xmm3, 2
	PADDD xmm6, xmm3
	PSUBD xmm6, xmm4
	PSRAD xmm6, 6 ; b, c, b, c, b, c, b, c
	PACKSSDW xmm6, xmm6

	MOVDQA xmm0, XMMWORD PTR [i_vector]
	MOVDQA xmm1, XMMWORD PTR [ix_inc]
	MOVDQA xmm2, XMMWORD PTR [iy_inc]
	
	MOVDQA xmm4, xmm0
	PADDW xmm4, xmm1
	PADDW xmm4, xmm1
	
;01	
	MOVDQA xmm3, xmm0 ; y: 0
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11


	MOVDQA xmm3, xmm0
	PADDW xmm3, xmm1 ; y: 0

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14

	ADD rcx, 64

;23		
	PSUBW xmm3, xmm1
	PADDW xmm3, xmm2; y: 4	
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11


	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PADDW xmm3, xmm1 ; y: 4

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14

	ADD rcx, 64
;45		
	MOVDQA xmm3, xmm4 ; y: 0
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11

	MOVDQA xmm3, xmm4
	PADDW xmm3, xmm1 ; y: 0

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14
	
	ADD rcx, 64
;67		
	PSUBW xmm3, xmm1
	PADDW xmm3, xmm2 ; y: 4
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11

	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PADDW xmm3, xmm1 ; y: 4

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14

	ADD rcx, 64

	
	PADDW xmm3, xmm2
	PSUBW xmm3, xmm1 ; x8 y8
	MOVDQA xmm4, xmm3

	PSUBW xmm3, xmm1
	PSUBW xmm3, xmm1 ; x0 y8

	MOVDQA xmm0, xmm3 ; y: 0

;89		
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11


	MOVDQA xmm3, xmm0
	PADDW xmm3, xmm1 ; y: 0

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14

	ADD rcx, 64

;1011		
	PSUBW xmm3, xmm1
	PADDW xmm3, xmm2; y: 4	
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11


	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PADDW xmm3, xmm1 ; y: 4

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14

	ADD rcx, 64
;1213		
	MOVDQA xmm3, xmm4 ; y: 0
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11

	MOVDQA xmm3, xmm4
	PADDW xmm3, xmm1 ; y: 0

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 1

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 2

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 3

	MOVDQA xmm15, xmm3
	PMADDWD xmm15, xmm6
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14
	
	ADD rcx, 64
;1415		
	PSUBW xmm3, xmm1
	PADDW xmm3, xmm2 ; y: 4
	
	MOVDQA xmm8, xmm3
	PMADDWD xmm8, xmm6
	PADDD xmm8, xmm5
	PSRAD xmm8, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm9, xmm3
	PMADDWD xmm9, xmm6
	PADDD xmm9, xmm5
	PSRAD xmm9, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm10, xmm3
	PMADDWD xmm10, xmm6
	PADDD xmm10, xmm5
	PSRAD xmm10, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm11, xmm3
	PMADDWD xmm11, xmm6
	PADDD xmm11, xmm5
	PSRAD xmm11, 5

	PACKSSDW xmm8, xmm9
	PACKSSDW xmm10, xmm11

	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PSUBW xmm3, xmm2
	PADDW xmm3, xmm1 ; y: 4

	MOVDQA xmm12, xmm3
	PMADDWD xmm12, xmm6
	PADDD xmm12, xmm5
	PSRAD xmm12, 5

	PADDW xmm3, xmm2 ; y: 5

	MOVDQA xmm13, xmm3
	PMADDWD xmm13, xmm6
	PADDD xmm13, xmm5
	PSRAD xmm13, 5

	PADDW xmm3, xmm2 ; y: 6

	MOVDQA xmm14, xmm3
	PMADDWD xmm14, xmm6
	PADDD xmm14, xmm5
	PSRAD xmm14, 5

	PADDW xmm3, xmm2 ; y: 7

	MOVDQA xmm15, xmm6
	PMADDWD xmm15, xmm3
	PADDD xmm15, xmm5
	PSRAD xmm15, 5

	PACKSSDW xmm12, xmm13
	PACKSSDW xmm14, xmm15

		
	PADDW xmm8, [rcx]
	PADDW xmm10, [rcx+16]
	PADDW xmm12, [rcx+32]
	PADDW xmm14, [rcx+48]

	MOVDQA [rcx], xmm8
	MOVDQA [rcx+16], xmm10
	MOVDQA [rcx+32], xmm12
	MOVDQA [rcx+48], xmm14


	RET
H264_PlaneIPred16	ENDP






ALIGN 16
H264_ChromaShake1	PROC
; 01
	MOVDQA xmm0, [rcx]
	MOVDQA xmm2, [rcx+16]
	MOVDQA xmm4, [rcx+32]
	MOVDQA xmm5, [rcx+48]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	PUNPCKLQDQ xmm2, xmm5
	PUNPCKHQDQ xmm3, xmm5


; 23
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm6, [rcx+80]
	MOVDQA xmm8, [rcx+96]
	MOVDQA xmm9, [rcx+112]

	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6
	PUNPCKLQDQ xmm4, xmm8
	PUNPCKHQDQ xmm5, xmm8
	PUNPCKLQDQ xmm6, xmm9
	PUNPCKHQDQ xmm7, xmm9


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3

	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_ChromaShake1	ENDP


ALIGN 16
H264_ChromaShake2	PROC
; 01
	MOVDQA xmm0, [rcx]
	MOVDQA xmm2, [rcx+16]
	MOVDQA xmm4, [rcx+32]
	MOVDQA xmm5, [rcx+48]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	PUNPCKLQDQ xmm2, xmm5
	PUNPCKHQDQ xmm3, xmm5


; 23
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm6, [rcx+80]
	MOVDQA xmm8, [rcx+96]
	MOVDQA xmm9, [rcx+112]

	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6
	PUNPCKLQDQ xmm4, xmm8
	PUNPCKHQDQ xmm5, xmm8
	PUNPCKLQDQ xmm6, xmm9
	PUNPCKHQDQ xmm7, xmm9


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7


	ADD rcx, 128
; 01
	MOVDQA xmm0, [rcx]
	MOVDQA xmm2, [rcx+16]
	MOVDQA xmm4, [rcx+32]
	MOVDQA xmm5, [rcx+48]

	MOVDQA xmm1, xmm0
	MOVDQA xmm3, xmm2
	PUNPCKLQDQ xmm0, xmm4
	PUNPCKHQDQ xmm1, xmm4
	PUNPCKLQDQ xmm2, xmm5
	PUNPCKHQDQ xmm3, xmm5


; 23
	MOVDQA xmm4, [rcx+64]
	MOVDQA xmm6, [rcx+80]
	MOVDQA xmm8, [rcx+96]
	MOVDQA xmm9, [rcx+112]

	MOVDQA xmm5, xmm4
	MOVDQA xmm7, xmm6
	PUNPCKLQDQ xmm4, xmm8
	PUNPCKHQDQ xmm5, xmm8
	PUNPCKLQDQ xmm6, xmm9
	PUNPCKHQDQ xmm7, xmm9


	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm2
	MOVDQA [rcx+48], xmm3
	MOVDQA [rcx+64], xmm4
	MOVDQA [rcx+80], xmm5
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	RET
H264_ChromaShake2	ENDP





ALIGN 16
H264_VerticalIChroma	PROC
	INC r8
	SHL r8, 6

	MOVDQA xmm0, [rdx + r8 - 16]
	MOVDQA xmm1, xmm0
	
	SHR r8, 7

	PUNPCKLQDQ xmm0, xmm0
	PUNPCKHQDQ xmm1, xmm1


align 16
	c_loop:
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm1
		MOVDQA xmm7, xmm1

		MOVDQA xmm8, xmm0
		MOVDQA xmm9, xmm0
		MOVDQA xmm10, xmm1
		MOVDQA xmm11, xmm1


		PADDW xmm4, [rcx]
		PADDW xmm5, [rcx+16]
		PADDW xmm6, [rcx+32]
		PADDW xmm7, [rcx+48]

		PADDW xmm8, [rcx+64]
		PADDW xmm9, [rcx+80]
		PADDW xmm10, [rcx+96]
		PADDW xmm11, [rcx+112]


		MOVDQA [rcx], xmm4
		MOVDQA [rcx+16], xmm5
		MOVDQA [rcx+32], xmm6
		MOVDQA [rcx+48], xmm7
		MOVDQA [rcx+64], xmm8
		MOVDQA [rcx+80], xmm9
		MOVDQA [rcx+96], xmm10
		MOVDQA [rcx+112], xmm11


		ADD rcx, 128

	DEC r8
	JNZ c_loop

	RET
H264_VerticalIChroma	ENDP


ALIGN 16
H264_HorizontalIChroma	PROC
	PINSRW xmm0, WORD PTR [rdx+14], 0
	PINSRW xmm0, WORD PTR [rdx+14], 1
	PINSRW xmm0, WORD PTR [rdx+30], 2
	PINSRW xmm0, WORD PTR [rdx+30], 3

	PINSRW xmm1, WORD PTR [rdx+46], 0
	PINSRW xmm1, WORD PTR [rdx+46], 1
	PINSRW xmm1, WORD PTR [rdx+62], 2
	PINSRW xmm1, WORD PTR [rdx+62], 3

	PINSRW xmm2, WORD PTR [rdx+78], 0
	PINSRW xmm2, WORD PTR [rdx+78], 1
	PINSRW xmm2, WORD PTR [rdx+94], 2
	PINSRW xmm2, WORD PTR [rdx+94], 3

	PINSRW xmm3, WORD PTR [rdx+110], 0
	PINSRW xmm3, WORD PTR [rdx+110], 1
	PINSRW xmm3, WORD PTR [rdx+126], 2
	PINSRW xmm3, WORD PTR [rdx+126], 3
		

	PUNPCKLDQ xmm0, xmm0
	PUNPCKLDQ xmm1, xmm1
	PUNPCKLDQ xmm2, xmm2
	PUNPCKLDQ xmm3, xmm3

	MOVDQA xmm4, xmm0
	MOVDQA xmm5, xmm1
	MOVDQA xmm6, xmm2
	MOVDQA xmm7, xmm3

	PADDW xmm0, [rcx]
	PADDW xmm1, [rcx+16]
	PADDW xmm4, [rcx+32]
	PADDW xmm5, [rcx+48]
	PADDW xmm2, [rcx+64]
	PADDW xmm3, [rcx+80]
	PADDW xmm6, [rcx+96]
	PADDW xmm7, [rcx+112]
	
	MOVDQA [rcx], xmm0
	MOVDQA [rcx+16], xmm1
	MOVDQA [rcx+32], xmm4
	MOVDQA [rcx+48], xmm5
	MOVDQA [rcx+64], xmm2
	MOVDQA [rcx+80], xmm3
	MOVDQA [rcx+96], xmm6
	MOVDQA [rcx+112], xmm7

	CMP r8d, 1
	JZ exit
		ADD rcx, 128
		ADD rdx, 128


		PINSRW xmm0, WORD PTR [rdx+14], 0
		PINSRW xmm0, WORD PTR [rdx+14], 1
		PINSRW xmm0, WORD PTR [rdx+30], 2
		PINSRW xmm0, WORD PTR [rdx+30], 3

		PINSRW xmm1, WORD PTR [rdx+46], 0
		PINSRW xmm1, WORD PTR [rdx+46], 1
		PINSRW xmm1, WORD PTR [rdx+62], 2
		PINSRW xmm1, WORD PTR [rdx+62], 3

		PINSRW xmm2, WORD PTR [rdx+78], 0
		PINSRW xmm2, WORD PTR [rdx+78], 1
		PINSRW xmm2, WORD PTR [rdx+94], 2
		PINSRW xmm2, WORD PTR [rdx+94], 3

		PINSRW xmm3, WORD PTR [rdx+110], 0
		PINSRW xmm3, WORD PTR [rdx+110], 1
		PINSRW xmm3, WORD PTR [rdx+126], 2
		PINSRW xmm3, WORD PTR [rdx+126], 3
		

		PUNPCKLDQ xmm0, xmm0
		PUNPCKLDQ xmm1, xmm1
		PUNPCKLDQ xmm2, xmm2
		PUNPCKLDQ xmm3, xmm3

		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm1
		MOVDQA xmm6, xmm2
		MOVDQA xmm7, xmm3

		PADDW xmm0, [rcx]
		PADDW xmm1, [rcx+16]
		PADDW xmm4, [rcx+32]
		PADDW xmm5, [rcx+48]
		PADDW xmm2, [rcx+64]
		PADDW xmm3, [rcx+80]
		PADDW xmm6, [rcx+96]
		PADDW xmm7, [rcx+112]
	
		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm1
		MOVDQA [rcx+32], xmm4
		MOVDQA [rcx+48], xmm5
		MOVDQA [rcx+64], xmm2
		MOVDQA [rcx+80], xmm3
		MOVDQA [rcx+96], xmm6
		MOVDQA [rcx+112], xmm7

	exit:

	RET
H264_HorizontalIChroma	ENDP


ALIGN 16
H264_DCIChroma	PROC
	
	; rdx - left
	; r8 - up
	PCMPEQD xmm8, xmm8

	PCMPEQD xmm11, xmm11
	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3
	PXOR xmm4, xmm4
	PXOR xmm5, xmm5
	PXOR xmm6, xmm6
	PXOR xmm7, xmm7

	MOV r10d, 1

	INC r9
	SHL r9, 6


	AND r8, r8
	JZ no_up
		INC r10d


		PUNPCKLWD xmm0, [r8+r9-16]
		PUNPCKHWD xmm1, [r8+r9-16]

		PSRAD xmm0, 16
		PSRAD xmm1, 16

		PHADDD xmm0, xmm0
		PHADDD xmm1, xmm1
		PSUBD xmm0, xmm8
		PSUBD xmm1, xmm8
		PHADDD xmm0, xmm0
		PHADDD xmm1, xmm1
				
		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1
		MOVDQA xmm4, xmm0
		MOVDQA xmm5, xmm1
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm1

		PXOR xmm11, xmm11

	no_up:


	SHR r9, 6

	AND rdx, rdx
	JZ no_left
		INC r10d

		PINSRW xmm10, WORD PTR [rdx+14], 1
		PINSRW xmm10, WORD PTR [rdx+30], 3
		PINSRW xmm10, WORD PTR [rdx+46], 5
		PINSRW xmm10, WORD PTR [rdx+62], 7
		PSRAD xmm10, 16


		PINSRW xmm2, WORD PTR [rdx+78], 1
		PINSRW xmm2, WORD PTR [rdx+94], 3
		PINSRW xmm2, WORD PTR [rdx+110], 5
		PINSRW xmm2, WORD PTR [rdx+126], 7
		PSRAD xmm2, 16

		PHADDD xmm10, xmm10
		PHADDD xmm2, xmm2
		PSUBD xmm10, xmm8
		PSUBD xmm2, xmm8
		PHADDD xmm10, xmm10
		PHADDD xmm2, xmm2


		PADDD xmm0, xmm10
		PAND xmm10, xmm11
		POR xmm1, xmm10
				
		PADDD xmm3, xmm2


		CMP r9w, 2
		JBE no_left
			ADD rdx, 128

			PINSRW xmm4, WORD PTR [rdx+14], 1
			PINSRW xmm4, WORD PTR [rdx+30], 3
			PINSRW xmm4, WORD PTR [rdx+46], 5
			PINSRW xmm4, WORD PTR [rdx+62], 7
			PSRAD xmm4, 16


			PINSRW xmm6, WORD PTR [rdx+78], 1
			PINSRW xmm6, WORD PTR [rdx+94], 3
			PINSRW xmm6, WORD PTR [rdx+110], 5
			PINSRW xmm6, WORD PTR [rdx+126], 7
			PSRAD xmm6, 16

			PHADDD xmm4, xmm4
			PHADDD xmm6, xmm6
			PSUBD xmm4, xmm8
			PSUBD xmm6, xmm8
			PHADDD xmm4, xmm4
			PHADDD xmm6, xmm6

			PADDD xmm5, xmm4
			PADDD xmm7, xmm6
				
align 16
	no_left:
		MOVD xmm9, r10d
		
		PSRAD xmm0, xmm9
		PSRAD xmm1, 2
		PSRAD xmm2, 2
		PSRAD xmm3, xmm9

		PACKSSDW xmm0, xmm0
		MOVDQA xmm8, xmm0
		PACKSSDW xmm1, xmm1
		MOVDQA xmm9, xmm1
		PACKSSDW xmm2, xmm2
		MOVDQA xmm10, xmm2
		PACKSSDW xmm3, xmm3
		MOVDQA xmm11, xmm3


		PADDW xmm0, [rcx]
		PADDW xmm8, [rcx+16]
		PADDW xmm1, [rcx+32]
		PADDW xmm9, [rcx+48]
		PADDW xmm2, [rcx+64]
		PADDW xmm10, [rcx+80]
		PADDW xmm3, [rcx+96]
		PADDW xmm11, [rcx+112]

		MOVDQA [rcx], xmm0
		MOVDQA [rcx+16], xmm8
		MOVDQA [rcx+32], xmm1
		MOVDQA [rcx+48], xmm9
		MOVDQA [rcx+64], xmm2
		MOVDQA [rcx+80], xmm10
		MOVDQA [rcx+96], xmm3
		MOVDQA [rcx+112], xmm11


		CMP r9w, 2
		JBE exit
			ADD rcx, 128

			PSRAD xmm4, 2
			PSRAD xmm5, xmm9
			PSRAD xmm6, 2
			PSRAD xmm7, xmm9

			PACKSSDW xmm4, xmm4
			MOVDQA xmm8, xmm4
			PACKSSDW xmm5, xmm5
			MOVDQA xmm9, xmm5
			PACKSSDW xmm6, xmm6
			MOVDQA xmm10, xmm6
			PACKSSDW xmm7, xmm7
			MOVDQA xmm11, xmm7


			PADDW xmm4, [rcx]
			PADDW xmm8, [rcx+16]
			PADDW xmm4, [rcx+32]
			PADDW xmm9, [rcx+48]
			PADDW xmm6, [rcx+64]
			PADDW xmm10, [rcx+80]
			PADDW xmm7, [rcx+96]
			PADDW xmm11, [rcx+112]

			MOVDQA [rcx], xmm4
			MOVDQA [rcx+16], xmm8
			MOVDQA [rcx+32], xmm5
			MOVDQA [rcx+48], xmm9
			MOVDQA [rcx+64], xmm6
			MOVDQA [rcx+80], xmm10
			MOVDQA [rcx+96], xmm7
			MOVDQA [rcx+112], xmm11

align 16
	exit:
	RET
H264_DCIChroma	ENDP


ALIGN 16
H264_PlaneIChroma	PROC
	MOVDQA xmm7, XMMWORD PTR [i_scale]

	MOVDQA xmm4, XMMWORD PTR [i_vector]
	MOVDQA xmm5, XMMWORD PTR [ix_inc]
	MOVDQA xmm6, XMMWORD PTR [iy_inc]
	PADDW xmm4, xmm5

	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm3, xmm3

	MOV eax, r9d
	SHR r9d, 16
	AND eax, 3

	INC eax
	SHL eax, 6
	ADD r8, rax

	PINSRW xmm3, r9d, 3
	PINSRW xmm3, WORD PTR [r8-16], 2
	PINSRW xmm3, WORD PTR [r8-14], 1
	PINSRW xmm3, WORD PTR [r8-12], 0

	MOVD xmm2, QWORD PTR [r8-8]

	MOVSX r10d, WORD PTR [r8-2]

	PSUBW xmm2, xmm3
	PMADDWD xmm2, xmm7 ; H


	CMP eax, 160
	JA cb_422	
		PINSRW xmm1, r9d, 3
		PINSRW xmm1, WORD PTR [rdx+14], 2
		PINSRW xmm1, WORD PTR [rdx+30], 1
		PINSRW xmm1, WORD PTR [rdx+46], 0
				
		PINSRW xmm0, WORD PTR [rdx+78], 0
		PINSRW xmm0, WORD PTR [rdx+94], 1
		PINSRW xmm0, WORD PTR [rdx+110], 2
		PINSRW xmm0, WORD PTR [rdx+126], 3

		MOVSX r11d, WORD PTR [rdx+126]
		
		PADDW xmm4, xmm6
		PADDW xmm4, xmm6
		PADDW xmm4, xmm6
		PADDW xmm4, xmm6

		PSUBW xmm0, xmm1
		PMADDWD xmm0, xmm7 ; V
		
		MOV r9d, 1
		
		JMP cb_set_left
align 16
	cb_422:
		PINSRW xmm1, r9d, 7
		PINSRW xmm1, WORD PTR [rdx+14], 6
		PINSRW xmm1, WORD PTR [rdx+30], 5
		PINSRW xmm1, WORD PTR [rdx+46], 4
		PINSRW xmm1, WORD PTR [rdx+62], 3
		PINSRW xmm1, WORD PTR [rdx+78], 2
		PINSRW xmm1, WORD PTR [rdx+94], 1
		PINSRW xmm1, WORD PTR [rdx+110], 0

		PINSRW xmm0, WORD PTR [rdx+142], 0
		PINSRW xmm0, WORD PTR [rdx+158], 1
		PINSRW xmm0, WORD PTR [rdx+174], 2
		PINSRW xmm0, WORD PTR [rdx+190], 3
		PINSRW xmm0, WORD PTR [rdx+206], 4
		PINSRW xmm0, WORD PTR [rdx+222], 5
		PINSRW xmm0, WORD PTR [rdx+238], 6
		PINSRW xmm0, WORD PTR [rdx+254], 7
		
		MOVSX r11d, WORD PTR [rdx+254]

		PSUBW xmm0, xmm1
		PMADDWD xmm0, xmm7

		PHADDD xmm0, xmm0 ; V
	
		XOR r9d, r9d
cb_set_left:

	MOVD xmm1, r9d
	SHL r9d, 1
	ADD r9d, 2

	PHADDD xmm2, xmm2 ; H
	PHADDD xmm0, xmm0 ; V

	PSLLD xmm2, 1
	MOVDQA xmm3, xmm2
	PSLLD xmm3, 4
	PADDD xmm2, xmm3 ; 34
	
	PSLLD xmm0, xmm1
	MOVDQA xmm3, xmm0
	MOVD xmm1, r9d
	PSLLD xmm3, xmm1
	PADDD xmm0, xmm3

	PUNPCKLDQ xmm2, xmm0
	PUNPCKLQDQ xmm2, xmm2

	PCMPEQD xmm1, xmm1
	PSLLD xmm1, 5		

	PSUBD xmm2, xmm1
	PSRAD xmm2, 6 ; b, c, b, c, b, c, b, c

	PACKSSDW xmm2, xmm2	
	
	
	ADD r10d, r11d
	INC r10d
	SHL r10d, 4
		
	MOVD xmm1, r10d
	PSHUFD xmm1, xmm1, 0 ; a + 16, a + 16, a + 16, a + 16
	

	SHR rax, 6


align 16
	cb_loop:

		MOVDQA xmm8, xmm2
		PMADDWD xmm8, xmm4
		PADDD xmm8, xmm1
		PSRAD xmm8, 5

		PADDW xmm4, xmm6 ; y: 1

		MOVDQA xmm9, xmm2
		PMADDWD xmm9, xmm4
		PADDD xmm9, xmm1
		PSRAD xmm9, 5

		PADDW xmm4, xmm6 ; y: 2

		MOVDQA xmm10, xmm2
		PMADDWD xmm10, xmm4
		PADDD xmm10, xmm1
		PSRAD xmm10, 5

		PADDW xmm4, xmm6 ; y: 3

		MOVDQA xmm11, xmm2
		PMADDWD xmm11, xmm4
		PADDD xmm11, xmm1
		PSRAD xmm11, 5

		PACKSSDW xmm8, xmm9
		PACKSSDW xmm10, xmm11

		PSUBW xmm4, xmm6
		PSUBW xmm4, xmm6
		PSUBW xmm4, xmm6
		PADDW xmm4, xmm5





		MOVDQA xmm9, xmm2
		PMADDWD xmm9, xmm4
		PADDD xmm9, xmm1
		PSRAD xmm9, 5

		PADDW xmm4, xmm6 ; y: 1

		MOVDQA xmm11, xmm2
		PMADDWD xmm11, xmm4
		PADDD xmm11, xmm1
		PSRAD xmm11, 5

		PADDW xmm4, xmm6 ; y: 2

		MOVDQA xmm12, xmm2
		PMADDWD xmm12, xmm4
		PADDD xmm12, xmm1
		PSRAD xmm12, 5

		PADDW xmm4, xmm6 ; y: 3

		MOVDQA xmm13, xmm2
		PMADDWD xmm13, xmm4
		PADDD xmm13, xmm1
		PSRAD xmm13, 5

		PACKSSDW xmm9, xmm11
		PACKSSDW xmm12, xmm13

		PADDW xmm4, xmm6 ; y: 3
		PSUBW xmm4, xmm5

		PADDW xmm8, [rcx]
		PADDW xmm10, [rcx+16]
		PADDW xmm9, [rcx+32]
		PADDW xmm12, [rcx+48]

		MOVDQA [rcx], xmm8
		MOVDQA [rcx+16], xmm10
		MOVDQA [rcx+32], xmm9
		MOVDQA [rcx+48], xmm12

		ADD rcx, 64

	DEC eax
	JNZ cb_loop


	RET
H264_PlaneIChroma	ENDP



END

