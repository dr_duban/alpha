/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"

#include "System\SysUtils.h"

Video::H264::pIPred Video::H264::i_pred_4x4[9];
Video::H264::pIPred Video::H264::i_pred_8x8[9];

__forceinline unsigned int Video::H264::Filter4x4(const short * s_ptr, unsigned int b_idx, unsigned int cmp_ss) {
	static const UI_64 up_offset[16] = {240, 244, 12, 28,		248, 252, 76, 92,		44, 60, 140, 156,		108, 124, 204, 220};
	static const UI_64 xup_offset[16] = {4, 4, 16, 0,		4, 4*256-12, 16, 0,		16, 48, 16, 0,		16, 0, 16, 0};
	static const UI_64 left_offset[16] = {0, 0, 64, 32,		16, 64, 48, 96,		128, 128, 192, 160,		144, 192, 176, 224};
	static const UI_64 x_idx[16] = {0, 0, 0, 15,		0, 0, 31, 79,		0, 47, 0, 143,		63, 111, 159, 207};

	const short * left_ptr(s_ptr-4*256), * up_ptr(s_ptr - decoder_cfg.pline_offset), * xup_ptr(0);
	unsigned int left_scale(16), pp_result(0);

	if (((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) left_ptr = 0;
	if (((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) up_ptr = 0;

	if (up_map[b_idx]) up_ptr = s_ptr;


	decoder_cfg.mb_x->x_tent[13] = decoder_cfg.mb_x->x_tent[14] = decoder_cfg.mb_x->x_tent[15] = 0;


	if (up_ptr) {
		up_ptr += up_offset[b_idx];


		if (ur_map[b_idx] != 1) {
			xup_ptr = up_ptr + xup_offset[b_idx];
			
			if (ur_map[b_idx] == 3)
				if (((decoder_cfg.mb_c->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_c->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) xup_ptr = 0;
		}

		decoder_cfg.mb_x->x_tent[0] = up_ptr[0];
		decoder_cfg.mb_x->x_tent[1] = up_ptr[1];
		decoder_cfg.mb_x->x_tent[2] = up_ptr[2];
		decoder_cfg.mb_x->x_tent[3] = up_ptr[3];

		if (xup_ptr) {
			decoder_cfg.mb_x->x_tent[4] = xup_ptr[0];
			decoder_cfg.mb_x->x_tent[5] = xup_ptr[1];
			decoder_cfg.mb_x->x_tent[6] = xup_ptr[2];
			decoder_cfg.mb_x->x_tent[7] = xup_ptr[3];
		} else {
			decoder_cfg.mb_x->x_tent[4] = up_ptr[3];
			decoder_cfg.mb_x->x_tent[5] = up_ptr[3];
			decoder_cfg.mb_x->x_tent[6] = up_ptr[3];
			decoder_cfg.mb_x->x_tent[7] = up_ptr[3];
		}

		pp_result = 1;
	}

	if (left_map[b_idx]) {
		left_ptr = s_ptr;
		left_scale = 4;
	}
		
	if (left_ptr) {
		left_ptr += (left_offset[b_idx] + left_scale - 1);
		
		decoder_cfg.mb_x->x_tent[8] = left_ptr[0]; 
		decoder_cfg.mb_x->x_tent[9] = left_ptr[left_scale];		
		decoder_cfg.mb_x->x_tent[10] = left_ptr[2*left_scale]; 
		decoder_cfg.mb_x->x_tent[11] = left_ptr[2*left_scale + left_scale];


		pp_result |= 2;
	}


	switch (ul_map[b_idx]) {
		case 0: // x
			decoder_cfg.mb_x->x_tent[14] = -1;
			decoder_cfg.mb_x->x_tent[15] = s_ptr[x_idx[b_idx]];
		break;
		case 1: // left
			if (left_ptr) {
				decoder_cfg.mb_x->x_tent[14] = -1;
				decoder_cfg.mb_x->x_tent[15] = left_ptr[-16];
			}
		break;
		case 2: // up
			if (up_ptr) {
				decoder_cfg.mb_x->x_tent[14] = -1;
				decoder_cfg.mb_x->x_tent[15] = up_ptr[-1];
			}
		break;
		case 3: // d
			if ((decoder_cfg.mb_d->flag_type & FLAG_MB_AVAILABLE) && ((decoder_cfg.mb_d->flag_type & FLAG_MB_TYPE_I) || (current_slice.pic_set->constrained_intra_pred_flag == 0))) {
				s_ptr -= decoder_cfg.pline_offset + 3*256 + 1;

				decoder_cfg.mb_x->x_tent[14] = -1;
				decoder_cfg.mb_x->x_tent[15] = s_ptr[0];			
			}
		break;
	}

		
	
	
	switch (pp_result) {
		case 0:
			for (unsigned int i(0);i<15;i++) decoder_cfg.mb_x->x_tent[i] = current_slice.gray_val[cmp_ss];
		break;
		case 1:
			decoder_cfg.mb_x->x_tent[8] = decoder_cfg.mb_x->x_tent[0];
			decoder_cfg.mb_x->x_tent[9] = decoder_cfg.mb_x->x_tent[1];
			decoder_cfg.mb_x->x_tent[10] = decoder_cfg.mb_x->x_tent[2];
			decoder_cfg.mb_x->x_tent[11] = decoder_cfg.mb_x->x_tent[3];
		break;
		case 2:
			decoder_cfg.mb_x->x_tent[0] = decoder_cfg.mb_x->x_tent[8];
			decoder_cfg.mb_x->x_tent[1] = decoder_cfg.mb_x->x_tent[9];
			decoder_cfg.mb_x->x_tent[2] = decoder_cfg.mb_x->x_tent[10];
			decoder_cfg.mb_x->x_tent[3] = decoder_cfg.mb_x->x_tent[11];
		break;
	}

	decoder_cfg.mb_x->x_tent[12] = decoder_cfg.mb_x->x_tent[11];

	return pp_result;
}


void Video::H264::RestoreSamples4x4(short * lev_ptr, unsigned int cmp_sel) {
	short * cmp_ptr(lev_ptr+(cmp_sel<<8));
	unsigned int f_val = (((unsigned int)decoder_cfg.mb_x->qp[cmp_sel]*10923) & 0xFFFF0000);
	f_val |= (decoder_cfg.mb_x->qp[cmp_sel] - (f_val >> 14) - (f_val >> 15));


	lev_ptr = cmp_ptr;	

	if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
		if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_0) {

			if (decoder_cfg.mb_x->coded_block[cmp_sel])
			if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
					
				if (decoder_cfg.mb_x->field_decoding_flag) {
					H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[1][cmp_sel], 16);
				} else {
					H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[0][cmp_sel], 16);
				}
			}

			for (unsigned int i(0);i<16;i++) {
				if (decoder_cfg.mb_x->coded_block[cmp_sel] & (1<<i)) {
					if (decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) {
						switch (decoder_cfg.mb_x->intra_pred_mode[i]) {
							case -2: // vertical
								H264_BypassAdjustV4x4(lev_ptr, decoder_cfg.mb_x->field_decoding_flag);
							break;
							case -1:
								H264_BypassAdjustH4x4(lev_ptr, decoder_cfg.mb_x->field_decoding_flag);
							break;
							default:
								if (decoder_cfg.mb_x->field_decoding_flag) H264_Scan1Reverse4x4(lev_ptr, 1);
								else H264_Scan0Reverse4x4(lev_ptr, 1);

						}
					}
				}



				Filter4x4(cmp_ptr, i, cmp_sel>0);

				i_pred_4x4[decoder_cfg.mb_x->intra_pred_mode[i] + 2](lev_ptr, decoder_cfg.mb_x->x_tent);


				lev_ptr += 16;

			}
		} else { // i16
			const short * left_ptr(lev_ptr-4*256), * up_ptr(lev_ptr - decoder_cfg.pline_offset);				

			if (decoder_cfg.mb_x->coded_block[cmp_sel] | (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel))) {
				if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
					
					if (decoder_cfg.mb_x->field_decoding_flag) {						
						if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransform1(lev_ptr, cmp_sel);
						H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[1][cmp_sel], (cmp_sel<<16) | 0x1010);

					} else {						
						if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransform0(lev_ptr, cmp_sel);
						H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[0][cmp_sel], (cmp_sel<<16) | 0x1010);

					}				
				} else {
					switch (decoder_cfg.mb_x->intra_pred_mode[0]) {
						case 0: // vertical
							H264_BypassAdjustV4x4a(lev_ptr, 0x0404, decoder_cfg.mb_x->field_decoding_flag);
						break;
						case 1:
							H264_BypassAdjustH4x4a(lev_ptr, 0x0404, decoder_cfg.mb_x->field_decoding_flag);
						break;
						default:
							if (decoder_cfg.mb_x->field_decoding_flag) H264_Scan1Reverse4x4(lev_ptr, 16);
							else H264_Scan0Reverse4x4(lev_ptr, 16);

					}
				}
			}


			if (((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) left_ptr = 0;
			if (((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) up_ptr = 0;

			switch (decoder_cfg.mb_x->intra_pred_mode[0]) {
				case 0: // vertical					
					if (up_ptr) H264_VerticalIPred16(lev_ptr, up_ptr);
				break;
				case 1: // horizontal					
					if (left_ptr) H264_HorizontalIPred16(lev_ptr, left_ptr);
				break;
				case 2: // DC
					if ((up_ptr) || (left_ptr)) {
						H264_DCIPred16(lev_ptr, left_ptr, up_ptr);
					} else {
						if (cmp_sel == 0) H264_LevelShift16(lev_ptr, current_slice.gray_val[cmp_sel]);
					}
				break;
				case 3: // plane
				//	decoder_cfg.clip_vals[3][0] = up_ptr[-1-3*256];decoder_cfg.clip_vals[0]
					if ((up_ptr) && (left_ptr)) H264_PlaneIPred16(lev_ptr, left_ptr, up_ptr, up_ptr[-1-3*256]);
				break;

			}

		}

	} else { // inter
		if (cmp_sel == 0)
		if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_P) {
			MVPredP();
		} else { // b type
			MVPredB();
		}

		decoder_cfg.MapRL0();

		if (decoder_cfg.mb_x->coded_block[cmp_sel]) {
			if (decoder_cfg.mb_x->field_decoding_flag) {
				H264_SamplesI1Transform4x4(cmp_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[1][cmp_sel], 16);
			} else {
				H264_SamplesI0Transform4x4(cmp_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[0][cmp_sel], 16);
			}
		}

		InterPredLuma(cmp_ptr, cmp_sel);
	}

	H264_SamplesShake4x4(cmp_ptr);
}


__forceinline unsigned int Video::H264::Filter8x8(const short * s_ptr, unsigned int b_idx, unsigned int cmp_ss) {
	unsigned int result(0);
	const short * left_ptr(s_ptr-4*256), * up_ptr(s_ptr - decoder_cfg.pline_offset), * xup_ptr(0);


	decoder_cfg.mb_x->x_tent[25] = 0;
	decoder_cfg.mb_x->x_tent[26] = 32;
	
	if (((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) left_ptr = 0;
	if (((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) up_ptr = 0;

	switch (b_idx) {
		case 0:
			if (up_ptr) {
				if ((decoder_cfg.mb_d->flag_type & FLAG_MB_AVAILABLE) && ((decoder_cfg.mb_d->flag_type & FLAG_MB_TYPE_I) || (current_slice.pic_set->constrained_intra_pred_flag == 0))) {
					decoder_cfg.mb_x->x_tent[24] = up_ptr[-1-3*256];
					decoder_cfg.mb_x->x_tent[25] = -1;					
				}

				up_ptr += 240;
				xup_ptr = up_ptr + 8;
			}
		break;
		case 1:
			if (up_ptr) {				
				decoder_cfg.mb_x->x_tent[24] = up_ptr[247];
				decoder_cfg.mb_x->x_tent[25] = -1;

				up_ptr += 248;

				if ((decoder_cfg.mb_c->flag_type & FLAG_MB_AVAILABLE) && ((decoder_cfg.mb_c->flag_type & FLAG_MB_TYPE_I) || (current_slice.pic_set->constrained_intra_pred_flag == 0))) {
					xup_ptr = up_ptr + 4*256 - 8;
				}				
			}

			decoder_cfg.mb_x->x_tent[26] = 16;
			left_ptr = s_ptr;
		break;
		case 2:			
			if (left_ptr) {
				decoder_cfg.mb_x->x_tent[24] = left_ptr[127];
				decoder_cfg.mb_x->x_tent[25] = -1;

				left_ptr += 128;
			}

			up_ptr = s_ptr + 56;
			xup_ptr = s_ptr + 120;
			
		break;
		case 3:
			up_ptr = s_ptr + 120;
			left_ptr = s_ptr + 128;

			decoder_cfg.mb_x->x_tent[24] = s_ptr[63];
			decoder_cfg.mb_x->x_tent[25] = -1;
			decoder_cfg.mb_x->x_tent[26] = 16;

		break;
	}



	switch (result = H264_ReferenceFilter8x8(decoder_cfg.mb_x->x_tent, left_ptr, up_ptr, xup_ptr)) {
		case 0:
			for (unsigned int i(0);i<25;i++) decoder_cfg.mb_x->x_tent[i] = current_slice.gray_val[cmp_ss];
		break;
		case 1: // up is on
			decoder_cfg.mb_x->x_tent[16] = decoder_cfg.mb_x->x_tent[0];
			decoder_cfg.mb_x->x_tent[17] = decoder_cfg.mb_x->x_tent[1];
			decoder_cfg.mb_x->x_tent[18] = decoder_cfg.mb_x->x_tent[2];
			decoder_cfg.mb_x->x_tent[19] = decoder_cfg.mb_x->x_tent[3];
			decoder_cfg.mb_x->x_tent[20] = decoder_cfg.mb_x->x_tent[4];
			decoder_cfg.mb_x->x_tent[21] = decoder_cfg.mb_x->x_tent[5];
			decoder_cfg.mb_x->x_tent[22] = decoder_cfg.mb_x->x_tent[6];
			decoder_cfg.mb_x->x_tent[23] = decoder_cfg.mb_x->x_tent[7];
		break;
		case 2: // lwft is on
			decoder_cfg.mb_x->x_tent[0] = decoder_cfg.mb_x->x_tent[16];
			decoder_cfg.mb_x->x_tent[1] = decoder_cfg.mb_x->x_tent[17];
			decoder_cfg.mb_x->x_tent[2] = decoder_cfg.mb_x->x_tent[18];
			decoder_cfg.mb_x->x_tent[3] = decoder_cfg.mb_x->x_tent[19];
			decoder_cfg.mb_x->x_tent[4] = decoder_cfg.mb_x->x_tent[20];
			decoder_cfg.mb_x->x_tent[5] = decoder_cfg.mb_x->x_tent[21];
			decoder_cfg.mb_x->x_tent[6] = decoder_cfg.mb_x->x_tent[22];
			decoder_cfg.mb_x->x_tent[7] = decoder_cfg.mb_x->x_tent[23];

		break;

	}
	


	return result;
}

void Video::H264::RestoreSamples8x8(short * lev_ptr, unsigned int cmp_sel) {	
	short * cmp_ptr(lev_ptr + (cmp_sel<<8));
	unsigned int f_val = (((unsigned int)decoder_cfg.mb_x->qp[cmp_sel]*10923) & 0xFFFF0000);
	f_val |= (decoder_cfg.mb_x->qp[cmp_sel] - (f_val>>14) - (f_val>>15));

	lev_ptr = cmp_ptr;

	if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
		for (unsigned int i(0);i<4;i++) {
			if (decoder_cfg.mb_x->coded_block[cmp_sel] & (0x000F << (i << 2))) {
				if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
					if (decoder_cfg.mb_x->field_decoding_flag) H264_SamplesI1Transform8x8(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_8x8[1][cmp_sel]);
					else H264_SamplesI0Transform8x8(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_8x8[0][cmp_sel]);

				} else {
					switch (decoder_cfg.mb_x->intra_pred_mode[i<<2]) {
						case -2: // vertical
							H264_BypassAdjustV8x8(lev_ptr, decoder_cfg.mb_x->field_decoding_flag);
						break;
						case -1: // horizontal
							H264_BypassAdjustH8x8(lev_ptr, decoder_cfg.mb_x->field_decoding_flag);
						break;
						default:
							if (decoder_cfg.mb_x->field_decoding_flag) H264_Scan1Reverse8x8(lev_ptr);
							else H264_Scan0Reverse8x8(lev_ptr);
					}
				}
			}
					
			
			Filter8x8(cmp_ptr, i, cmp_sel>0);

			i_pred_8x8[decoder_cfg.mb_x->intra_pred_mode[i<<2] + 2](lev_ptr, decoder_cfg.mb_x->x_tent);

			lev_ptr += 64;
		}

		H264_SamplesShake8x8(cmp_ptr);

	} else {
		if (cmp_sel == 0)
		if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_P) {
			MVPredP();
		} else { // b type
			MVPredB();
		}

		decoder_cfg.MapRL0();

		for (unsigned int i(0);i<4;i++) {
			if (decoder_cfg.mb_x->coded_block[cmp_sel] & (0x000F << (i << 2))) {
				if (decoder_cfg.mb_x->field_decoding_flag) H264_SamplesI1Transform8x8(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_8x8[1][cmp_sel], 4);
				else H264_SamplesI0Transform8x8(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_8x8[0][cmp_sel], 4);
			}
			
			lev_ptr += 64;			
		}

		InterPredLuma(cmp_ptr, cmp_sel);
		H264_SamplesShake4x4(cmp_ptr);
	}	
}


















void Video::H264::RestoreChroma1(short * lev_ptr, unsigned int cmp_sel) {
	lev_ptr += (cmp_sel<<8);
	const short * left_ptr(lev_ptr-4*256), * up_ptr(lev_ptr - decoder_cfg.pline_offset);

	unsigned int f_val = (((unsigned int)decoder_cfg.mb_x->qp[cmp_sel]*10923) & 0xFFFF0000);
	f_val |= (decoder_cfg.mb_x->qp[cmp_sel] - (f_val >> 14) - (f_val >> 15));

	
	if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
		if (decoder_cfg.mb_x->coded_block[cmp_sel] | (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel))) {
			if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
				if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransformChroma1(lev_ptr, cmp_sel);
				if (decoder_cfg.mb_x->field_decoding_flag) {				
					H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[1][cmp_sel], 0x0104);
				} else {				
					H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[0][cmp_sel], 0x0104);
				}

			} else {
				switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
					case 1: // horizontal
						H264_BypassAdjustH4x4a(lev_ptr, 0x0202, decoder_cfg.mb_x->field_decoding_flag);
					break;
					case 2: // verticval
						H264_BypassAdjustV4x4a(lev_ptr, 0x0202, decoder_cfg.mb_x->field_decoding_flag);
					break;
					default:
						if (decoder_cfg.mb_x->field_decoding_flag) {
							H264_Scan1Reverse4x4(lev_ptr, 4);
						} else {
							H264_Scan0Reverse4x4(lev_ptr, 4);
						}

				}
			}
		}




		if (((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) left_ptr = 0;
		if (((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) up_ptr = 0;
		

		switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
			case 0: // DC
				if ((up_ptr) || (left_ptr)) H264_DCIChroma(lev_ptr, left_ptr, up_ptr, 1);
			break;
			case 1: // horizontal				
				if (left_ptr) H264_HorizontalIChroma(lev_ptr, left_ptr, 1);
			break;
			case 2: // vertical				
				if (up_ptr) H264_VerticalIChroma(lev_ptr, up_ptr, 1);
			break;
			case 3: // plane
				if ((up_ptr) && (left_ptr)) H264_PlaneIChroma(lev_ptr, left_ptr, up_ptr, ((int)up_ptr[63-4*256]<<16) | 1);
			break;

		}

		
	} else {
		if (decoder_cfg.mb_x->coded_block[cmp_sel] | (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel))) {
			if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
				if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransformChroma1(lev_ptr, cmp_sel);
				if (decoder_cfg.mb_x->field_decoding_flag) {				
					H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[1][cmp_sel], 0x0104);
				} else {				
					H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[0][cmp_sel], 0x0104);
				}

			} else {
				switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
					case 1: // horizontal
						H264_BypassAdjustH4x4a(lev_ptr, 0x0202, decoder_cfg.mb_x->field_decoding_flag);
					break;
					case 2: // verticval
						H264_BypassAdjustV4x4a(lev_ptr, 0x0202, decoder_cfg.mb_x->field_decoding_flag);
					break;
					default:
						if (decoder_cfg.mb_x->field_decoding_flag) {
							H264_Scan1Reverse4x4(lev_ptr, 4);
						} else {
							H264_Scan0Reverse4x4(lev_ptr, 4);
						}

				}
			}
		}


		InterPredChroma1(lev_ptr, cmp_sel);

	}

	H264_ChromaShake1(lev_ptr);

}


void Video::H264::RestoreChroma2(short * lev_ptr, unsigned int cmp_sel) {
	lev_ptr += (cmp_sel<<8);
	const short * left_ptr(lev_ptr-4*256), * up_ptr(lev_ptr - decoder_cfg.pline_offset);
	
	unsigned int f_val = (((unsigned int)decoder_cfg.mb_x->qp[cmp_sel]*10923) & 0xFFFF0000);
	f_val |= (decoder_cfg.mb_x->qp[cmp_sel] - (f_val >> 14) - (f_val >> 15));


	if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) {
		if (decoder_cfg.mb_x->coded_block[cmp_sel] | (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel))) {
			if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
				if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransformChroma2(lev_ptr, cmp_sel);

				if (decoder_cfg.mb_x->field_decoding_flag) {				
					H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[1][cmp_sel], 0x0208);
				} else {
					H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->intra_scaling_list_4x4[0][cmp_sel], 0x0208);
				}

			} else {
				switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
					case 1: // horizontal
						H264_BypassAdjustH4x4a(lev_ptr, 0x0402, decoder_cfg.mb_x->field_decoding_flag);
					break;
					case 2: // verticval
						H264_BypassAdjustV4x4a(lev_ptr, 0x0402, decoder_cfg.mb_x->field_decoding_flag);
					break;
					default:
						if (decoder_cfg.mb_x->field_decoding_flag) {
							H264_Scan1Reverse4x4(lev_ptr, 8);
						} else {
							H264_Scan0Reverse4x4(lev_ptr, 8);
						}

				}
			}
		}



		if (((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) left_ptr = 0;
		if (((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) == 0) || (((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) && (current_slice.pic_set->constrained_intra_pred_flag))) up_ptr = 0;

		switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
			case 0: // DC
				if ((up_ptr) || (left_ptr)) H264_DCIChroma(lev_ptr, left_ptr, up_ptr, 3);
			break;
			case 1: // horizontal
				if (left_ptr) H264_HorizontalIChroma(lev_ptr, left_ptr, 3);
			break;
			case 2: // verticval
				if (up_ptr) H264_VerticalIChroma(lev_ptr, up_ptr, 3);
			break;
			case 3: // plane
				if ((up_ptr) && (left_ptr)) H264_PlaneIChroma(lev_ptr, left_ptr, up_ptr, ((int)up_ptr[127-4*256]<<16) | 3);
			break;

		}

	} else {
		if (decoder_cfg.mb_x->coded_block[cmp_sel] | (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel))) {
			if ((decoder_cfg.mb_x->flag_type & FLAG_MB_T_BYPASS) == 0) {
				if (decoder_cfg.mb_x->coded_block[3] & (1<<cmp_sel)) H264_DCTransformChroma2(lev_ptr, cmp_sel);

				if (decoder_cfg.mb_x->field_decoding_flag) {				
					H264_SamplesI1Transform4x4(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[1][cmp_sel], 0x0208);
				} else {
					H264_SamplesI0Transform4x4(lev_ptr, f_val, current_slice.pic_set->inter_scaling_list_4x4[0][cmp_sel], 0x0208);
				}

			} else {
				switch (decoder_cfg.mb_x->intra_chroma_pred_mode) {
					case 1: // horizontal
						H264_BypassAdjustH4x4a(lev_ptr, 0x0402, decoder_cfg.mb_x->field_decoding_flag);
					break;
					case 2: // verticval
						H264_BypassAdjustV4x4a(lev_ptr, 0x0402, decoder_cfg.mb_x->field_decoding_flag);
					break;
					default:
						if (decoder_cfg.mb_x->field_decoding_flag) {
							H264_Scan1Reverse4x4(lev_ptr, 8);
						} else {
							H264_Scan0Reverse4x4(lev_ptr, 8);
						}

				}
			}
		}


		InterPredChroma2(lev_ptr, cmp_sel);

	}

	H264_ChromaShake2(lev_ptr);
}







// ===================================================
const unsigned int Video::H264::up_map[16] =		{0, 0, 1, 1,	0, 0, 1, 1,		1, 1, 1, 1,		1, 1, 1, 1};
const unsigned int Video::H264::left_map[16] =	{0, 1, 0, 1,	1, 1, 1, 1,		0, 1, 0, 1,		1, 1, 1, 1};	
const unsigned int Video::H264::ur_map[16] = {2, 2, 0, 1,	2, 3, 0, 1,		0, 0, 0, 1,		0, 1, 0, 1 };
const unsigned int Video::H264::ul_map[16] = {3, 2, 1, 0,	2, 2, 0, 0,		1, 0, 1, 0,		0, 0, 0, 0};
const unsigned int Video::H264::up_idx[16] =		{10, 11, 0, 1,	14, 15, 4, 5,	2, 3, 8, 9,		6, 7, 12, 13};
const unsigned int Video::H264::left_idx[16] =	{5, 0, 7, 2,	1, 4, 3, 6,		13, 8, 15, 10,	9, 12, 11, 14};
const unsigned int Video::H264::ur_idx[16] =	{11, 14, 1, 4,	15, 10, 5, 0,	3, 6, 9, 12,	7, 2, 13, 8};
const unsigned int Video::H264::ul_idx[16] = {15, 10, 5, 0,		11, 14, 1, 4,	7, 2, 13, 8,	3, 6, 9, 12};


const short Video::H264::qpc_map[22] = { 29, 30, 31, 32, 32, 33, 34, 34, 35, 35,	36, 36, 37, 37, 37, 38, 38, 38, 39, 39,		39, 39 };

__declspec(align(16)) const short Video::H264::default_intra_scaling_4x4[16] = {
	 6, 13, 13, 20, 20, 20, 28, 28, 28, 28, 32, 32, 32, 37, 37, 42	
};

__declspec(align(16)) const short Video::H264::default_inter_scaling_4x4[16] = {
	10, 14, 14, 20, 20, 20, 24, 24, 24, 24, 27, 27, 27, 30, 30, 34
};

__declspec(align(16)) const short Video::H264::default_intra_scaling_8x8[64] = {
	 6, 10, 10, 13, 11, 13, 16, 16, 16, 16, 18, 18, 18, 18, 18, 23,
	23, 23, 23, 23, 23, 25, 25, 25, 25, 25, 25, 25, 27, 27, 27, 27,
	27, 27, 27, 27, 29, 29, 29, 29, 29, 29, 29, 31, 31, 31, 31, 31,
	31, 33, 33, 33, 33, 33, 36, 36, 36, 36, 38, 38, 38, 40, 40, 42
};

__declspec(align(16)) const short Video::H264::default_inter_scaling_8x8[64] = {
	 9, 13, 13, 15, 13, 15, 17, 17, 17, 17, 19, 19, 19, 19, 19, 21,
	21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 24, 24, 24, 24,
	24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 27, 27, 27, 27, 27,
	27, 28, 28, 28, 28, 28, 30, 30, 30, 30, 32, 32, 32, 33, 33, 35
};

__declspec(align(16)) const short Video::H264::norm_adjust_4x4[2][6][16] = {
	10, 13, 13, 10, 16, 10, 13, 13, 13, 13, 16, 10, 16, 13, 13, 16, 
	11, 14, 14, 11, 18, 11, 14, 14, 14, 14, 18, 11, 18, 14, 14, 18, 
	13, 16, 16, 13, 20, 13, 16, 16, 16, 16, 20, 13, 20, 16, 16, 20, 
	14, 18, 18, 14, 23, 14, 18, 18, 18, 18, 23, 14, 23, 18, 18, 23, 
	16, 20, 20, 16, 25, 16, 20, 20, 20, 20, 25, 16, 25, 20, 20, 25, 
	18, 23, 23, 18, 29, 18, 23, 23, 23, 23, 29, 18, 29, 23, 23, 29, 

	10, 13, 13, 10, 13, 16, 13, 16, 10, 13, 10, 13, 13, 16, 13, 16, 
	11, 14, 14, 11, 14, 18, 14, 18, 11, 14, 11, 14, 14, 18, 14, 18, 
	13, 16, 16, 13, 16, 20, 16, 20, 13, 16, 13, 16, 16, 20, 16, 20, 
	14, 18, 18, 14, 18, 23, 18, 23, 14, 18, 14, 18, 18, 23, 18, 23, 
	16, 20, 20, 16, 20, 25, 20, 25, 16, 20, 16, 20, 20, 25, 20, 25, 
	18, 23, 23, 18, 23, 29, 23, 29, 18, 23, 18, 23, 23, 29, 23, 29

};

__declspec(align(16)) const short Video::H264::norm_adjust_8x8[2][6][64] = {
	20, 19, 19, 25, 18, 25, 19, 24, 24, 19, 20, 18, 32, 18, 20, 19, 19, 24, 24, 19, 19, 25, 18, 25, 18, 25, 18, 25, 19, 24, 24, 19, 19, 24, 24, 19, 18, 32, 18, 20, 18, 32, 18, 24, 24, 19, 19, 24, 24, 18, 25, 18, 25, 18, 19, 24, 24, 19, 18, 32, 18, 24, 24, 18, 
	22, 21, 21, 28, 19, 28, 21, 26, 26, 21, 22, 19, 35, 19, 22, 21, 21, 26, 26, 21, 21, 28, 19, 28, 19, 28, 19, 28, 21, 26, 26, 21, 21, 26, 26, 21, 19, 35, 19, 22, 19, 35, 19, 26, 26, 21, 21, 26, 26, 19, 28, 19, 28, 19, 21, 26, 26, 21, 19, 35, 19, 26, 26, 19, 
	26, 24, 24, 33, 23, 33, 24, 31, 31, 24, 26, 23, 42, 23, 26, 24, 24, 31, 31, 24, 24, 33, 23, 33, 23, 33, 23, 33, 24, 31, 31, 24, 24, 31, 31, 24, 23, 42, 23, 26, 23, 42, 23, 31, 31, 24, 24, 31, 31, 23, 33, 23, 33, 23, 24, 31, 31, 24, 23, 42, 23, 31, 31, 23, 
	28, 26, 26, 35, 25, 35, 26, 33, 33, 26, 28, 25, 45, 25, 28, 26, 26, 33, 33, 26, 26, 35, 25, 35, 25, 35, 25, 35, 26, 33, 33, 26, 26, 33, 33, 26, 25, 45, 25, 28, 25, 45, 25, 33, 33, 26, 26, 33, 33, 25, 35, 25, 35, 25, 26, 33, 33, 26, 25, 45, 25, 33, 33, 25, 
	32, 30, 30, 40, 28, 40, 30, 38, 38, 30, 32, 28, 51, 28, 32, 30, 30, 38, 38, 30, 30, 40, 28, 40, 28, 40, 28, 40, 30, 38, 38, 30, 30, 38, 38, 30, 28, 51, 28, 32, 28, 51, 28, 38, 38, 30, 30, 38, 38, 28, 40, 28, 40, 28, 30, 38, 38, 30, 28, 51, 28, 38, 38, 28, 
	36, 34, 34, 46, 32, 46, 34, 43, 43, 34, 36, 32, 58, 32, 36, 34, 34, 43, 43, 34, 34, 46, 32, 46, 32, 46, 32, 46, 34, 43, 43, 34, 34, 43, 43, 34, 32, 58, 32, 36, 32, 58, 32, 43, 43, 34, 34, 43, 43, 32, 46, 32, 46, 32, 34, 43, 43, 34, 32, 58, 32, 43, 43, 32, 

	20, 19, 25, 19, 18, 19, 20, 24, 19, 18, 19, 25, 19, 19, 24, 20, 32, 18, 24, 18, 24, 18, 20, 32, 25, 24, 32, 24, 19, 19, 19, 25, 19, 18, 24, 18, 19, 18, 25, 24, 20, 19, 25, 19, 18, 24, 32, 19, 18, 24, 18, 24, 19, 18, 25, 24, 32, 24, 24, 18, 19, 18, 24, 18, 
	22, 21, 28, 21, 19, 21, 22, 26, 21, 19, 21, 28, 21, 21, 26, 22, 35, 19, 26, 19, 26, 19, 22, 35, 28, 26, 35, 26, 21, 21, 21, 28, 21, 19, 26, 19, 21, 19, 28, 26, 22, 21, 28, 21, 19, 26, 35, 21, 19, 26, 19, 26, 21, 19, 28, 26, 35, 26, 26, 19, 21, 19, 26, 19, 
	26, 24, 33, 24, 23, 24, 26, 31, 24, 23, 24, 33, 24, 24, 31, 26, 42, 23, 31, 23, 31, 23, 26, 42, 33, 31, 42, 31, 24, 24, 24, 33, 24, 23, 31, 23, 24, 23, 33, 31, 26, 24, 33, 24, 23, 31, 42, 24, 23, 31, 23, 31, 24, 23, 33, 31, 42, 31, 31, 23, 24, 23, 31, 23, 
	28, 26, 35, 26, 25, 26, 28, 33, 26, 25, 26, 35, 26, 26, 33, 28, 45, 25, 33, 25, 33, 25, 28, 45, 35, 33, 45, 33, 26, 26, 26, 35, 26, 25, 33, 25, 26, 25, 35, 33, 28, 26, 35, 26, 25, 33, 45, 26, 25, 33, 25, 33, 26, 25, 35, 33, 45, 33, 33, 25, 26, 25, 33, 25, 
	32, 30, 40, 30, 28, 30, 32, 38, 30, 28, 30, 40, 30, 30, 38, 32, 51, 28, 38, 28, 38, 28, 32, 51, 40, 38, 51, 38, 30, 30, 30, 40, 30, 28, 38, 28, 30, 28, 40, 38, 32, 30, 40, 30, 28, 38, 51, 30, 28, 38, 28, 38, 30, 28, 40, 38, 51, 38, 38, 28, 30, 28, 38, 28, 
	36, 34, 46, 34, 32, 34, 36, 43, 34, 32, 34, 46, 34, 34, 43, 36, 58, 32, 43, 32, 43, 32, 36, 58, 46, 43, 58, 43, 34, 34, 34, 46, 34, 32, 43, 32, 34, 32, 46, 43, 36, 34, 46, 34, 32, 43, 58, 34, 32, 43, 32, 43, 34, 32, 46, 43, 58, 43, 43, 32, 34, 32, 43, 32


};


const unsigned short Video::H264::zz_4x4[2][16] = {
	0, 1, 4, 8, 5, 2, 3, 6, 9, 12, 13, 10, 7, 11, 14, 15,
	0, 4, 1, 8, 12, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15
};

const unsigned short Video::H264::zz_8x8[2][64] = {
	0, 1, 8, 16, 9, 2, 3, 10, 17, 24, 32, 25, 18, 11, 4, 5, 12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6, 7, 14, 21, 28, 35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51, 58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63,
	0, 8, 16, 1, 9, 24, 32, 17, 3, 25, 40, 48, 56, 33, 10, 4, 18, 41, 49, 57, 26, 11, 4, 18, 34, 42, 50, 58, 35, 12, 5, 20, 35, 43, 51, 59, 28, 13, 6, 21, 36, 44, 52, 60, 29, 14, 22, 37, 45, 53, 61, 30, 7, 15, 38, 46, 54, 62, 23, 31, 39, 47, 55, 63
};

