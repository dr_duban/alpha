/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\H264.h"
#include "System\SysUtils.h"



#include <stdio.h>


__forceinline unsigned char Video::H264::CABAC::GetSubMBPType(DecoderState & d_cfg) {
	unsigned char result(0);

	if (result = (GetBinVal(21, d_cfg) ^ 1)) {
		if (GetBinVal(22, d_cfg)) {
			result = 3 - GetBinVal(23, d_cfg);
		}
	}

	return result;
}


__forceinline unsigned char Video::H264::CABAC::GetSubMBBType(DecoderState & d_cfg) {
	unsigned char result(0);

	if (result = (unsigned char)GetBinVal(36, d_cfg)) { // bin 0
		if (GetBinVal(37, d_cfg)) { // bin 1
			if (GetBinVal(38, d_cfg)) { // bin 2
				if (GetBinVal(39, d_cfg)) { // bin 3
					result = 11 + GetBinVal(39, d_cfg); // bin 4
				} else {
					result = 5 + (2 << GetBinVal(39, d_cfg)); // bin 4
					result += GetBinVal(39, d_cfg); // bin 5
				}
			} else {
				result += (2 << GetBinVal(39, d_cfg)); // bin 3
				result += GetBinVal(39, d_cfg); // bin 4
			}
		} else {
			result += GetBinVal(39, d_cfg); // bin 2
		}
	}

	return result;
}



__forceinline unsigned char Video::H264::CABAC::GetRefIdx(DecoderState & d_cfg, unsigned int tidx, unsigned int part_idx) {
	unsigned int ctx_idx(54);
	unsigned char result(0);

	if ((d_cfg.flag_set & FLAG_AFRO_FRAME) && ((d_cfg.mb_x->field_decoding_flag & 1) == 0)) result = 0x11;

	switch (part_idx) {
		case 0:
			if ((d_cfg.mb_a->field_decoding_flag & 1) == 0) result &= 0x10;
				
			if (d_cfg.mb_a->sub[1].ref_idx[tidx] > (result & 1))				
				ctx_idx++;

			result >>= 4;
			if ((d_cfg.mb_b->field_decoding_flag & 1) == 0) result = 0;
				
			if (d_cfg.mb_b->sub[2].ref_idx[tidx] > result)				
				ctx_idx += 2;

		break;
		case 1:
			if (d_cfg.mb_x->sub[0].ref_idx[tidx] > 0)
				ctx_idx++;

			if ((d_cfg.mb_b->field_decoding_flag & 1) == 0) result = 0;
				
			if (d_cfg.mb_b->sub[3].ref_idx[tidx] > (result & 1))
				ctx_idx += 2;

		break;
		case 2:
			if ((d_cfg.mb_b->field_decoding_flag & 1) == 0) result = 0;
				
			if (d_cfg.mb_a->sub[3].ref_idx[tidx] > (result & 1))
				ctx_idx++;

			if (d_cfg.mb_x->sub[0].ref_idx[tidx] > 0)
				ctx_idx += 2;

		break;
		case 3:
			if (d_cfg.mb_x->sub[2].ref_idx[tidx] > 0)
				ctx_idx++;

			if (d_cfg.mb_x->sub[1].ref_idx[tidx] > 0)
				ctx_idx += 2;

		break;

	}


	if (result = (unsigned char)GetBinVal(ctx_idx, d_cfg)) {		
		if (GetBinVal(58, d_cfg)) {
			result++;
			for (;GetBinVal(59, d_cfg);result++);
		}
	}

	return result;
}


__forceinline unsigned char Video::H264::CABAC::GetIntraChromaPredMode(DecoderState & d_cfg) {
	unsigned char result(0);
	unsigned int ctx_idx(64);

	if (d_cfg.mb_a->intra_chroma_pred_mode) ctx_idx++;
	if (d_cfg.mb_b->intra_chroma_pred_mode) ctx_idx++;
	

	if (GetBinVal(ctx_idx, d_cfg)) {
		result++;
		if (GetBinVal(67, d_cfg)) {
			result++;
			if (GetBinVal(67, d_cfg)) {
				result++;
			}
		}
	}

	return result;
}



__forceinline unsigned short Video::H264::CABAC::GetEndOfSliceFlag(DecoderState & d_cfg) {
	return GetBinVal276(d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetPrevIntraPredModeFlag(DecoderState & d_cfg) {	
	return GetBinVal(68, d_cfg);
}

__forceinline char Video::H264::CABAC::GetRemIntraMode(DecoderState & d_cfg) {
	return (char)(GetBinVal(69, d_cfg) | (GetBinVal(69, d_cfg)<<1) | (GetBinVal(69, d_cfg)<<2));
}


__forceinline unsigned short Video::H264::CABAC::GetMBSkipFlag(DecoderState & d_cfg) {	
	unsigned int ctx_idx(11);

	if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_B) ctx_idx = 24;

	if ((d_cfg.mb_a->flag_type & (FLAG_MB_TYPE_SKIP | FLAG_MB_AVAILABLE)) == FLAG_MB_AVAILABLE) ctx_idx++;	
	if ((d_cfg.mb_b->flag_type & (FLAG_MB_TYPE_SKIP | FLAG_MB_AVAILABLE)) == FLAG_MB_AVAILABLE) ctx_idx++;	
	
	return GetBinVal(ctx_idx, d_cfg);
}


__forceinline char Video::H264::CABAC::GetMBQPDelta(DecoderState & d_cfg) {
	unsigned int ctx_idx(60);
	char result(0);
	
	if (d_cfg.mb_p->qp_delta) ctx_idx++;

	if (result = (char)GetBinVal(ctx_idx, d_cfg)) {
		if (GetBinVal(62, d_cfg)) {
			result++;
			for (;GetBinVal(63, d_cfg);result++);
						
		}

		result++;

		if (result & 1) result = -(result>>1);
		else result >>= 1;
		
	}

	return result;
}


__forceinline unsigned char Video::H264::CABAC::GetMBFieldDecodingFlag(DecoderState & d_cfg) {
	unsigned int ctx_idx(70);

	ctx_idx += (d_cfg.mb_a->field_decoding_flag & 1);
	ctx_idx += (d_cfg.mb_b->field_decoding_flag & 1);
	
	return (unsigned char)GetBinVal(ctx_idx, d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetTransformSize8x8Flag(DecoderState & d_cfg) {
	unsigned int ctx_idx(399);
	
	ctx_idx += ((d_cfg.mb_a->flag_type & FLAG_MB_8x8_SIZE) >> 12);
	ctx_idx += ((d_cfg.mb_b->flag_type & FLAG_MB_8x8_SIZE) >> 12);
	
	return GetBinVal(ctx_idx, d_cfg);

}

__forceinline unsigned short Video::H264::CABAC::GetSignificantCoeffFlag(DecoderState & d_cfg, unsigned int level_idx, unsigned short cat_idx) {
	return GetBinVal(level_idx + si_coeff_offset[d_cfg.mb_x->field_decoding_flag & 1][cat_idx], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetLastSignificantCoeffFlag(DecoderState & d_cfg, unsigned int level_idx, unsigned short cat_idx) {
	return GetBinVal(level_idx + last_si_coeff_offset[d_cfg.mb_x->field_decoding_flag & 1][cat_idx], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetSignificantCoeffFlagChromaDC(DecoderState & d_cfg, unsigned int level_idx) { // cat 3
	return GetBinVal(sig_flag_chroma_dc_offset[level_idx] + sig_flag_chroma_dc_offset[4+(d_cfg.mb_x->field_decoding_flag & 1)], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetLastSignificantCoeffFlagChromaDC(DecoderState & d_cfg, unsigned int level_idx) { // cat 3
	return GetBinVal(sig_flag_chroma_dc_offset[level_idx] + sig_flag_chroma_dc_offset[6+(d_cfg.mb_x->field_decoding_flag & 1)], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetSignificantCoeffFlag8x8(DecoderState & d_cfg, unsigned int level_idx, unsigned short cat_idx) { // cat 5, 9, 13
	return GetBinVal(si_coeff_offset[d_cfg.mb_x->field_decoding_flag & 1][cat_idx] + scoefflag_inc_8x8[d_cfg.mb_x->field_decoding_flag & 1][level_idx], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetLastSignificantCoeffFlag8x8(DecoderState & d_cfg, unsigned int level_idx, unsigned short cat_idx) { // cat 5, 9, 13
	return GetBinVal(last_si_coeff_offset[d_cfg.mb_x->field_decoding_flag & 1][cat_idx] + lscoefflag_inc_8x8[level_idx], d_cfg);
}

__forceinline unsigned short Video::H264::CABAC::GetCoeffSignFlag(DecoderState & d_cfg) {
	return GetBinVal(d_cfg);
}





__forceinline unsigned short Video::H264::CABAC::GetCodedBlockFlag8x8(DecoderState & d_cfg, unsigned short block_idx, unsigned short cmp_sel) { // cat 5, 9, 13

	unsigned int ctx_idx(coded_block_flag_offset[block_idx & 0x0F]);
		
	switch (block_idx & 0x0F00) {
		case 0:
			if (d_cfg.mb_a->flag_type) {
				if ((d_cfg.mb_a->flag_type & (FLAG_MB_LUMA_0 | FLAG_MB_8x8_SIZE)) == (FLAG_MB_LUMA_0 | FLAG_MB_8x8_SIZE)) { // trans block on
					if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0010) ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_b->flag_type) {
				if ((d_cfg.mb_b->flag_type & (FLAG_MB_LUMA_0 | FLAG_MB_8x8_SIZE)) == (FLAG_MB_LUMA_0 | FLAG_MB_8x8_SIZE)) { // trans block on
					if (d_cfg.mb_b->coded_block[cmp_sel] & 0x0100) ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0100:
			if (d_cfg.mb_x->flag_type & FLAG_MB_LUMA_1)
				if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0001) ctx_idx++;

			if (d_cfg.mb_b->flag_type) {
				if ((d_cfg.mb_b->flag_type & (FLAG_MB_LUMA_1 | FLAG_MB_8x8_SIZE)) == (FLAG_MB_LUMA_1 | FLAG_MB_8x8_SIZE)) { // trans block on
					if (d_cfg.mb_b->coded_block[cmp_sel] & 0x1000) ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0200:
			if (d_cfg.mb_a->flag_type) {
				if ((d_cfg.mb_a->flag_type & (FLAG_MB_LUMA_2 | FLAG_MB_8x8_SIZE)) == (FLAG_MB_LUMA_2 | FLAG_MB_8x8_SIZE)) { // trans block on
					if (d_cfg.mb_a->coded_block[cmp_sel] & 0x1000) ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_x->flag_type & FLAG_MB_LUMA_2)
				if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0001) ctx_idx += 2;

		break;
		case 0x0300:
			if ((d_cfg.mb_x->flag_type & FLAG_MB_LUMA_3) && (d_cfg.mb_x->coded_block[cmp_sel] & 0x0100)) ctx_idx++;
			if ((d_cfg.mb_x->flag_type & FLAG_MB_LUMA_3) && (d_cfg.mb_x->coded_block[cmp_sel] & 0x0010)) ctx_idx += 2;

		break;

	}
	

	return GetBinVal(ctx_idx, d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetCodedBlockFlag(DecoderState & d_cfg, unsigned short block_idx, unsigned short cmp_sel) { // cat 1, 2, 7, 8, 11, 12	
	const MacroBlockSet * mbl(d_cfg.mb_a), * mbu(d_cfg.mb_b);
	unsigned int ctx_idx(coded_block_flag_offset[block_idx & 0x0F]);
	
	block_idx >>= 8;
	block_idx &= 0x0F;



	if (left_map[block_idx]) mbl = d_cfg.mb_x;
	if (up_map[block_idx]) mbu = d_cfg.mb_x;

	if (mbl->flag_type) {
		if (mbl->coded_block[cmp_sel] & left_cbf[block_idx]) { // trans block on
			ctx_idx++;
		} else { // no trans
			if (mbl->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
		}

	} else {
		if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
			ctx_idx++;
		}
	}

	if (mbu->flag_type) {
		if (mbu->coded_block[cmp_sel] & up_cbf[block_idx]) { // trans block on
			ctx_idx += 2;
		} else { // no trans
			if (mbu->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
		}

	} else {
		if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
			ctx_idx += 2;
		}
	}


	return GetBinVal(ctx_idx, d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetCodedBlockFlagDC(DecoderState & d_cfg, unsigned short block_cat, unsigned short cmp_sel) { // cat 0, 6, 10, 3

	unsigned int ctx_idx(coded_block_flag_offset[block_cat]);
	
	if (d_cfg.mb_a->flag_type) {
		if ((d_cfg.mb_a->coded_block[3] >> cmp_sel) & 1) {
			ctx_idx++;
		} else {
			if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM)
				ctx_idx++;
		}
	} else {
		if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I)	
			ctx_idx++;
	}


	if (d_cfg.mb_b->flag_type) {
		if ((d_cfg.mb_b->coded_block[3] >> cmp_sel) & 1) {
			ctx_idx += 2;
		} else {
			if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM)
				ctx_idx += 2;
		}
	} else {
		if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I)	
			ctx_idx += 2;
	}


	return GetBinVal(ctx_idx, d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetCodedBlockFlagChroma1AC(DecoderState & d_cfg, unsigned short block_idx, unsigned short cmp_sel) { // cat 4

	unsigned int ctx_idx(coded_block_flag_offset[4]);
		
	switch (block_idx & 0x0F00) {
		case 0:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0002) {
					ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_b->flag_type) {
				if (d_cfg.mb_b->coded_block[cmp_sel] & 0x0004) { // trans block on
					ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0100:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 1) ctx_idx++;

			if (d_cfg.mb_b->flag_type) {
				if (d_cfg.mb_b->coded_block[cmp_sel] & 0x0008) { // trans block on
					ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0200:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0008) { // trans block on
					ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_x->coded_block[cmp_sel] & 1) ctx_idx += 2;

		break;
		case 0x0300:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 4) ctx_idx++;
			if (d_cfg.mb_x->coded_block[cmp_sel] & 2) ctx_idx += 2;

		break;

	}
	
	return GetBinVal(ctx_idx, d_cfg);
}


__forceinline unsigned short Video::H264::CABAC::GetCodedBlockFlagChroma2AC(DecoderState & d_cfg, unsigned short block_idx, unsigned short cmp_sel) { // cat 4

	unsigned int ctx_idx(coded_block_flag_offset[4]);

	switch (block_idx & 0x0F00) {
		case 0:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0002) {
					ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_b->flag_type) {
				if (d_cfg.mb_b->coded_block[cmp_sel] & 0x0040) { // trans block on
					ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0100:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 1) ctx_idx++;

			if (d_cfg.mb_b->flag_type) {
				if (d_cfg.mb_b->coded_block[cmp_sel] & 0x0080) { // trans block on
					ctx_idx += 2;

				} else { // no trans
					if (d_cfg.mb_b->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx += 2;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx += 2;
				}
			}

		break;
		case 0x0200:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0008) { // trans block on
					ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x001) ctx_idx += 2;
			
		break;
		case 0x0300:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0004) ctx_idx++;
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0002) ctx_idx += 2;

		break;


		case 0x0400:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0020) { // trans block on
					 ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0004) ctx_idx += 2;

		break;
		case 0x0500:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0010) ctx_idx++;
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0008) ctx_idx += 2;

		break;
		case 0x0600:
			if (d_cfg.mb_a->flag_type) {
				if (d_cfg.mb_a->coded_block[cmp_sel] & 0x0080) { // trans block on
					ctx_idx++;

				} else { // no trans
					if (d_cfg.mb_a->flag_type & FLAG_MB_TYPE_I_PCM) ctx_idx++;
				}

			} else {
				if (d_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) { // intra, always 1
					ctx_idx++;
				}
			}

			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0010) ctx_idx += 2;
		break;
		case 0x0700:
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0040) ctx_idx++;
			if (d_cfg.mb_x->coded_block[cmp_sel] & 0x0020) ctx_idx += 2;

		break;

	}

	return GetBinVal(ctx_idx, d_cfg);
}



__forceinline short Video::H264::CABAC::GetCoeffAbsLevel(DecoderState & d_cfg, unsigned int block_idx) {
	short result(0), tmp_val(0);
	unsigned int ctx_idx(coeff_abs_offset[block_idx]), br_pointer(0), ctx_val(0);

	if (d_cfg.above_flag == 0) d_cfg.labs_selector = coeff_abs_idx_inc[0][d_cfg.labs_selector];

	if (result = GetBinVal(ctx_idx + coeff_abs_idx_inc[0][d_cfg.labs_selector], d_cfg)) {		
		if (block_idx != 3) d_cfg.above_flag = 1;
		else d_cfg.above_flag = 2;

		ctx_idx += coeff_abs_idx_inc[d_cfg.above_flag][d_cfg.labs_selector];
		d_cfg.labs_selector = coeff_abs_idx_inc[d_cfg.above_flag][d_cfg.labs_selector];


//*

		br_pointer = context_vars[ctx_idx];
		br_pointer <<= 8;
		br_pointer |= ((codi_range - 256) & 0xFF);
		br_pointer <<= 9;
		br_pointer |= codi_offset;

		br_pointer = bin_run_tab[br_pointer];

		codi_range = br_pointer & 0x01FF;
		br_pointer >>= 9;
		codi_offset = br_pointer & 0x01FF;
		br_pointer >>= 9;

		result += (br_pointer & 0x0F);

		if (br_pointer & 0x20) {
			RenormD(d_cfg);
		}

		
		if ((br_pointer & 0x0F) && (result < 14))
		for (;;) {
			ctx_val = (br_pointer >>= 6);
			br_pointer <<= 8;

			br_pointer |= ((codi_range - 256) & 0xFF);
			br_pointer <<= 9;
			br_pointer |= codi_offset;

			br_pointer = bin_run_tab[br_pointer];			

			if ((result + ((br_pointer >> 18) & 0x0F)) <= 14) {
				codi_range = br_pointer & 0x01FF;
				br_pointer >>= 9;
				codi_offset = br_pointer & 0x01FF;
				br_pointer >>= 9;

				result += (br_pointer & 0x0F);

				if (br_pointer & 0x20) {
					RenormD(d_cfg);
				}				

				if (((br_pointer & 0x0F) == 0) || (result >= 14)) break;
			} else {
				context_vars[ctx_idx] = ctx_val;

				for (;result<14;result++)
					if (GetBinVal(ctx_idx, d_cfg) == 0) break;


				br_pointer = (context_vars[ctx_idx] << 6);
				break;
			}

			
		}

		context_vars[ctx_idx] = (br_pointer >> 6);

/*/


		if (GetBinVal(ctx_idx, d_cfg)) {
			result++;
			if (GetBinVal(ctx_idx, d_cfg)) {
				result++;
				if (GetBinVal(ctx_idx, d_cfg)) {
					result++;
					if (GetBinVal(ctx_idx, d_cfg)) {
						result++;

						for (;result<14;result++)
							if (GetBinVal(ctx_idx, d_cfg) == 0) break;
					}
				}
			}
		}
//*/

	
		if (result == 14) {		

			if (GetBinVal(d_cfg)) {
				for (tmp_val = 1;GetBinVal(d_cfg);tmp_val++);
		
				result += (1<<tmp_val) - 1;

				ctx_idx = 0;
				for (;tmp_val--;) {
					ctx_idx <<= 1;
					ctx_idx |= GetBinVal(d_cfg);
				}
				result += ctx_idx;
			}
		}
	}

	return result+1;
}


// ===================================================================================================================================================================

const unsigned short Video::H264::psubtype_pred_mode[4] = {
	VAL_SUB_PMODE_L0,
	(VAL_SUB_PMODE_L0 | FLAG_SUB_PART_V),
	(VAL_SUB_PMODE_L0 | FLAG_SUB_PART_H),
	(VAL_SUB_PMODE_L0 | FLAG_SUB_PART_V | FLAG_SUB_PART_H)
};

const unsigned short Video::H264::bsubtype_pred_mode[16] = {
	0, VAL_SUB_PMODE_L0, VAL_SUB_PMODE_L1, VAL_SUB_PMODE_BI,
	(FLAG_SUB_PART_V | VAL_SUB_PMODE_L0), (FLAG_SUB_PART_H | VAL_SUB_PMODE_L0), (FLAG_SUB_PART_V | VAL_SUB_PMODE_L1), (FLAG_SUB_PART_H | VAL_SUB_PMODE_L1), (FLAG_SUB_PART_V | VAL_SUB_PMODE_BI), (FLAG_SUB_PART_H | VAL_SUB_PMODE_BI),
	(FLAG_SUB_PART_H | FLAG_SUB_PART_V | VAL_SUB_PMODE_L0), (FLAG_SUB_PART_H | FLAG_SUB_PART_V | VAL_SUB_PMODE_L1), (FLAG_SUB_PART_H | FLAG_SUB_PART_V | VAL_SUB_PMODE_BI),
	0, 0, 0
};

const unsigned int Video::H264::bmb_part_mode[24] = {0, 0, 0, 0, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, FLAG_MB_PART_V, FLAG_MB_PART_H, 0, 0};

const unsigned short Video::H264::bmb_pred_mode[24][4] = {
	0,					0,					0,					0,				
	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,
	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,
	
	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,

	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,
	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L1,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L0,

	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,
	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_BI,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,
	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_BI,

	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L0,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L0,
	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_L1,
	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L1,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_L1,

	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,
	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,	VAL_SUB_PMODE_BI,
	0,					0,					0,					0,				
	0,					0,					0,					0


};

void Video::H264::IncMbAddr() {
		
	decoder_cfg.current_mb_ptr++;
	decoder_cfg.frame_ptr += 3*256;
	decoder_cfg.current_mb_offset += 4*256;

	if (current_slice.pic_set->num_slice_groups) {
	//	for ()

		
	}
	
	decoder_cfg.mb_p = decoder_cfg.mb_x;
	
	if ((decoder_cfg.flag_set & FLAG_AFRO_FRAME) == 0) {
		decoder_cfg.line_ptr++;

		if (decoder_cfg.line_ptr > decoder_cfg.slice_width) {
			decoder_cfg.line_counter++;
			decoder_cfg.line_ptr = 1;

			System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);
			decoder_cfg.mb_u = decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr);			
			
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


			decoder_cfg.frame_ptr += 4*256;
			decoder_cfg.current_mb_offset += 4*256;
			
		}
				
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);

		decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr);

		if (decoder_cfg.current_mb_ptr >= decoder_cfg.slice_width) {
			decoder_cfg.mb_b = decoder_cfg.mb_x - ((decoder_cfg.slice_width + 1)<<3);
			decoder_cfg.mb_d = decoder_cfg.mb_b - 8;

			if (decoder_cfg.line_ptr == decoder_cfg.slice_width) decoder_cfg.mb_c = decoder_cfg.mb_u;
			else decoder_cfg.mb_c = decoder_cfg.mb_b + 8;
		} else {
			decoder_cfg.mb_b = decoder_cfg.mb_c = decoder_cfg.mb_d = decoder_cfg.mb_u;		
		}


	} else {

		if (decoder_cfg.current_mb_ptr & 1) decoder_cfg.line_ptr++;

		System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr + decoder_cfg.pline_offset, 4*256*sizeof(short), 0, 0);


		if (decoder_cfg.line_ptr > decoder_cfg.slice_width) {
			decoder_cfg.line_counter++;
			System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);
			System::MemoryFill_SSE3(decoder_cfg.frame_ptr + decoder_cfg.pline_offset, 4*256*sizeof(short), 0, 0);
			
			decoder_cfg.mb_u = decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr);
			
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


			decoder_cfg.line_ptr = 1;
			decoder_cfg.frame_ptr += 4*256;
			decoder_cfg.current_mb_offset += 4*256;
		}

		if (decoder_cfg.current_mb_ptr & 1) decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr + decoder_cfg.pline_offset);
		else decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr);
		
		if ((decoder_cfg.current_mb_ptr >= (decoder_cfg.slice_width<<1)) || (decoder_cfg.current_mb_ptr & 1)) {
			decoder_cfg.mb_b = decoder_cfg.mb_x - ((decoder_cfg.slice_width + 1)<<3);
			
			if (decoder_cfg.current_mb_ptr & 1) decoder_cfg.mb_d = decoder_cfg.mb_u;
			else decoder_cfg.mb_d = decoder_cfg.mb_b - 8;

			if (decoder_cfg.line_ptr == decoder_cfg.slice_width) decoder_cfg.mb_c = decoder_cfg.mb_u;
			else decoder_cfg.mb_c = decoder_cfg.mb_b + 8;
		} else {
			decoder_cfg.mb_b = decoder_cfg.mb_c = decoder_cfg.mb_d = decoder_cfg.mb_u;
		}

	}


	decoder_cfg.mb_a = decoder_cfg.mb_x - 8;
	
	decoder_cfg.mb_x->flag_type = FLAG_MB_AVAILABLE;
	decoder_cfg.mb_x->par_val = decoder_cfg.flag_set & FLAG_SECOND_FIELD;



	if (decoder_cfg.mb_p->flag_type & FLAG_MB_AVAILABLE) {
		decoder_cfg.mb_x->qp[0] = decoder_cfg.mb_p->qp[0];
	} else {
		decoder_cfg.mb_x->qp[0] = current_slice.qpy + (current_slice.seq_set->cmp_depth[0]<<2) + (current_slice.seq_set->cmp_depth[0]<<1);;
	}
	
	decoder_cfg.frame_ptr += 256;
	
}

UI_64 Video::H264::GetSliceDataE() {
	UI_64 result(1), temp_val(0);

	decoder_cfg.ByteAlign();
	cabac.Reset(decoder_cfg);


	if (decoder_cfg.frame_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {
		if (decoder_cfg.flag_set & FLAG_SECOND_FIELD) {
			decoder_cfg.frame_ptr += vui_params.second_field_offset;
		}
		
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr, 4*256*sizeof(short), 0, 0);

		decoder_cfg.line_counter = current_slice.first_mb / decoder_cfg.slice_width;
		decoder_cfg.line_ptr = current_slice.first_mb % decoder_cfg.slice_width;
		

		if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) {
			decoder_cfg.current_mb_offset = decoder_cfg.line_counter*(decoder_cfg.slice_width + 1)*4*256*2;
			if (current_slice.id_rec.bottom_field_flag) decoder_cfg.current_mb_offset += decoder_cfg.pline_offset;			
		} else {
			decoder_cfg.current_mb_offset = decoder_cfg.line_counter*(decoder_cfg.slice_width + 1)*4*256;
		}

		
		System::MemoryFill_SSE3(decoder_cfg.frame_ptr + decoder_cfg.current_mb_offset, 4*256*sizeof(short), 0, 0);

		 
		decoder_cfg.mb_u = decoder_cfg.mb_x = reinterpret_cast<MacroBlockSet *>(decoder_cfg.frame_ptr + decoder_cfg.current_mb_offset);
		
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[0] = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


		decoder_cfg.current_mb_offset += decoder_cfg.line_ptr*4*256;

		decoder_cfg.frame_ptr += decoder_cfg.current_mb_offset+256;
		
		
		decoder_cfg.current_mb_ptr = -1;
		decoder_cfg.flag_set = 0;
		
		for (IncMbAddr();decoder_cfg.IsSource(cabac.codi_range);IncMbAddr()) {
			if (decoder_cfg.line_counter >= vui_params.c_height) {
				break;
			}
			
#ifdef _DEBUG
if (((current_slice.first_mb + decoder_cfg.current_mb_ptr) == 0x0) && (pic_list[decoder_cfg.current_frame_ptr].time_stamp >= 0x018704)) { // 
	temp_val = 2;
}
#endif
			switch (current_slice._type) {
				case 1: case 6: // B
					decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_B;
				case 0: case 5: // P
				case 3: case 8: // SP
				
					if (cabac.GetMBSkipFlag(decoder_cfg)) {
						decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_SKIP;
						decoder_cfg.mb_x->field_decoding_flag = current_slice.id_rec.field_pic_flag;

						if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_B) {
							result = SkipPredB();
						} else {
							result = SkipPredP();
						}

						decoder_cfg.MapRL0();

						if (result) {
							SamplesCopy();
						} else {
							InterPredLuma(decoder_cfg.frame_ptr, 0);
							H264_SamplesShake4x4(decoder_cfg.frame_ptr);

							switch (decoder_cfg.chroma_array_type) {
								case 1:
									InterPredChroma1(decoder_cfg.frame_ptr + 256, 1);
									H264_ChromaShake1(decoder_cfg.frame_ptr + 256);
									InterPredChroma1(decoder_cfg.frame_ptr + 512, 2);
									H264_ChromaShake1(decoder_cfg.frame_ptr + 512);
									
								break;
								case 2:
									InterPredChroma2(decoder_cfg.frame_ptr + 256, 1);
									H264_ChromaShake2(decoder_cfg.frame_ptr + 256);
									InterPredChroma2(decoder_cfg.frame_ptr, 2);
									H264_ChromaShake2(decoder_cfg.frame_ptr + 512);
								break;
								default:
									InterPredLuma(decoder_cfg.frame_ptr + 256, 1);
									H264_SamplesShake4x4(decoder_cfg.frame_ptr + 256);
									InterPredLuma(decoder_cfg.frame_ptr + 512, 2);
									H264_SamplesShake4x4(decoder_cfg.frame_ptr + 512);
							}
						}

						result = 2;
					}

					if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_B) == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_P;
				break;

			}


			if (result & 1) {
				if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) {
					if (((decoder_cfg.current_mb_ptr & 1)==0) || ((decoder_cfg.current_mb_ptr & 1) && (result & 2))) {
						decoder_cfg.mb_x->field_decoding_flag = cabac.GetMBFieldDecodingFlag(decoder_cfg);
					} else {
						if (decoder_cfg.mb_a) decoder_cfg.mb_x->field_decoding_flag = decoder_cfg.mb_a->field_decoding_flag;
						else if (decoder_cfg.mb_b) decoder_cfg.mb_x->field_decoding_flag = decoder_cfg.mb_b->field_decoding_flag;
					}
				} else {
					decoder_cfg.mb_x->field_decoding_flag = current_slice.id_rec.field_pic_flag;
				}

				if (decoder_cfg.flag_set & FLAG_FIELD_PAIR) decoder_cfg.mb_x->field_decoding_flag = 0x10;

				switch (current_slice._type) {
					case 3: case 8: // SP	
						decoder_cfg.mb_x->flag_type |= FLAG_MB_TYPE_S;
					case 0: case 5: // P
						GetMBLayerPE();
					break;
					case 1: case 6: // B
						GetMBLayerBE();
					break;
					case 2: case 7: // I
						GetMBLayerIE();
					break;
					case 4: case 9: // SI
						GetMBLayerSIE();
					break;
				}

				result &= 1;
			}

			result &= 2;



			SetMBDeblock();
			
			
			if ((decoder_cfg.flag_set & FLAG_AFRO_FRAME) && ((decoder_cfg.current_mb_ptr & 1) == 0)) {
				result |= 1;
			} else {
				if (cabac.GetEndOfSliceFlag(decoder_cfg) == 0) result |= 1;
			}

			if ((result & 1) == 0) break;
		}	

		if ((result & 1) == 0) {
			result = decoder_cfg.IsMoreRBSP();
			
			if ((current_slice.first_mb + decoder_cfg.current_mb_ptr + 1) >= current_slice.seq_set->pic_width_mbs*current_slice.seq_set->pic_height_mapunits) {
				result |= RESULT_DECODE_COMPLETE;

			}

	//		System::MemoryFill_SSE3(decoder_cfg.frame_ptr + 3*256, (decoder_cfg.pline_offset<<1), 0, 0);
		} else {
			result = 'caba';
			result <<= 32;
		}

		if (unsigned int * vp = reinterpret_cast<unsigned int *>(pic_list[decoder_cfg.current_frame_ptr].buffer.Acquire())) {

			vui_params.num_units_tick = current_slice.id_rec.field_pic_flag;
			vui_params.time_scale = current_slice.id_rec.bottom_field_flag;

			System::MemoryCopy(vp, &vui_params, sizeof(VUI));

			pic_list[decoder_cfg.current_frame_ptr].buffer.Release();
		}

		pic_list[decoder_cfg.current_frame_ptr].buffer.Release();

	}
	
	return result;
}


UI_64 Video::H264::GetMBLayerPE() {
	unsigned char no_sub_mb_part_size_lt8x8(1);

	decoder_cfg.mb_x->flag_type |= cabac.GetMBTypeP(decoder_cfg);

	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[1].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[2].ref_idx[1] = 0xFFFF;
		reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[3].ref_idx[1] = 0xFFFF;


		decoder_cfg.mb_x->sub[0].type_flag = decoder_cfg.mb_x->sub[1].type_flag = decoder_cfg.mb_x->sub[2].type_flag = decoder_cfg.mb_x->sub[3].type_flag = VAL_SUB_PMODE_L0;

		if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) < 3) {
			switch (decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) {
				case 1:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_PART_V;
				break;
				case 2:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_PART_H;
				break;

			}

			MBPredE();

		} else { // P_88
			decoder_cfg.mb_x->flag_type |= (FLAG_MB_PART_V | FLAG_MB_PART_H);

			SMBPPredE();

			for (unsigned char i(0);i<4;i++) {
				if (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) no_sub_mb_part_size_lt8x8 = 0;
			}
		}

		decoder_cfg.mb_x->flag_type |= (cabac.GetCodedBlockPattern(decoder_cfg) << 16);
			
		if ((decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) && (current_slice.pic_set->trasnform_8x8_mode_flag) && (no_sub_mb_part_size_lt8x8)) {
			if (cabac.GetTransformSize8x8Flag(decoder_cfg)) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;

		}


		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {			
			ResidualE();
		} else {
			Restore();
		}

	} else {
		GetMBLayerIE();
	}
	

	return 0;

}

UI_64 Video::H264::GetMBLayerBE() {
	unsigned char no_sub_mb_part_size_lt8x8(1);

	decoder_cfg.mb_x->flag_type |= cabac.GetMBTypeB(decoder_cfg);

	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		
		if ((decoder_cfg.mb_x->flag_type & (FLAG_MB_TYPE_0 | MASK_MB_TYPE_ID)) < 22) {
			decoder_cfg.mb_x->flag_type |= bmb_part_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID];

			decoder_cfg.mb_x->sub[0].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][0];
			decoder_cfg.mb_x->sub[1].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][1];
			decoder_cfg.mb_x->sub[2].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][2];
			decoder_cfg.mb_x->sub[3].type_flag = bmb_pred_mode[decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID][3];

			MBPredE();				
					
		} else { // B_88
			if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) == 22) {
				decoder_cfg.mb_x->flag_type |= (FLAG_MB_PART_V | FLAG_MB_PART_H);

				SMBBPredE();

				for (unsigned char i(0);i<4;i++) {
					if ((decoder_cfg.mb_x->sub[i].type_flag & MASK_SUB_TYPE)) {
						if (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) no_sub_mb_part_size_lt8x8 = 0;
					} else {
						if (current_slice.seq_set->direct_8x8_inference_flag == 0) no_sub_mb_part_size_lt8x8 = 0;
					}
				}
			}			
		}
			
		decoder_cfg.mb_x->flag_type |= (cabac.GetCodedBlockPattern(decoder_cfg) << 16);
			
		if ((decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) && (current_slice.pic_set->trasnform_8x8_mode_flag) && (no_sub_mb_part_size_lt8x8) && ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) || (current_slice.seq_set->direct_8x8_inference_flag))) {
			if (cabac.GetTransformSize8x8Flag(decoder_cfg)) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;

		}

		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
			ResidualE();
		} else {
			Restore();
		}

	} else {
		// intra
		GetMBLayerIE();
	}



	return 0;

}

UI_64 Video::H264::GetMBLayerIE() {
	unsigned int ic(16>>(current_slice.seq_set->chroma_height + current_slice.seq_set->chroma_width)), ti(0), result(0);
	
	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) decoder_cfg.mb_x->flag_type |= cabac.GetMBTypeI(decoder_cfg);


	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I_PCM) == 0) {
		if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_0) && (current_slice.pic_set->trasnform_8x8_mode_flag)) {
			if (cabac.GetTransformSize8x8Flag(decoder_cfg)) decoder_cfg.mb_x->flag_type |= FLAG_MB_8x8_SIZE;
		}


		if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_0) {
			// mb_pred
			MBPredIE();

			if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
				decoder_cfg.mb_x->intra_chroma_pred_mode = cabac.GetIntraChromaPredMode(decoder_cfg);
			}

			decoder_cfg.mb_x->flag_type |= (cabac.GetCodedBlockPattern(decoder_cfg) << 16);

			if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
				ResidualE();
			} else {
				Restore();
			}

		} else {
			if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) > 12) decoder_cfg.mb_x->flag_type |= MASK_MB_LUMA;

			decoder_cfg.mb_x->intra_pred_mode[0] = ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID)-1) & 3;
	
			switch (((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID)-1) >> 2) {
				case 1: case 4:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_CHROMA_DC;
				break;
				case 2: case 5:
					decoder_cfg.mb_x->flag_type |= FLAG_MB_CHROMA_AC;
				break;

			}

			if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
				decoder_cfg.mb_x->intra_chroma_pred_mode = cabac.GetIntraChromaPredMode(decoder_cfg);
			}

			ResidualE();
		}

		

	} else { // I_PCM		
		decoder_cfg.mb_x->qp[1] = current_slice.pic_set->chroma_qpinx_offset[1];
		decoder_cfg.mb_x->qp[2] = current_slice.pic_set->chroma_qpinx_offset[2];


		decoder_cfg.ByteAlign();
		cabac.Reset(decoder_cfg);
		
		result = current_slice.seq_set->cmp_depth[0]+8;

		for (unsigned int i(0);i<16;i++) {// pcm_luma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+1] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+2] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+3] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+4] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+5] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+6] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+7] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+8] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+9] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+10] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+11] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+12] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+13] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+14] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+15] = H264_DS_GetNumber(&decoder_cfg, result);

		}

		result = current_slice.seq_set->cmp_depth[1]+8;

		for (unsigned int i(0);i<ic;i++) { // pcm_chroma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+1+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+2+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+3+256] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+4+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+5+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+6+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+7+256] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+8+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+9+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+10+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+11+256] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+12+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+13+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+14+256] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+15+256] = H264_DS_GetNumber(&decoder_cfg, result);
		}

		for (unsigned int i(0);i<ic;i++) { // pcm_chroma
			ti = i<<4;
			decoder_cfg.frame_ptr[ti+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+1+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+2+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+3+512] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+4+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+5+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+6+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+7+512] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+8+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+9+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+10+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+11+512] = H264_DS_GetNumber(&decoder_cfg, result);

			decoder_cfg.frame_ptr[ti+12+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+13+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+14+512] = H264_DS_GetNumber(&decoder_cfg, result);
			decoder_cfg.frame_ptr[ti+15+512] = H264_DS_GetNumber(&decoder_cfg, result);

		}

		cabac.Reset(decoder_cfg);
	}
	
	return 0;
}

UI_64 Video::H264::GetMBLayerSIE() {

	decoder_cfg.mb_x->flag_type |= cabac.GetMBTypeSI(decoder_cfg);

	if ((decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_I) == 0) {
		MBPredIE();

		if ((decoder_cfg.chroma_array_type == 1) || (decoder_cfg.chroma_array_type == 2)) {
			decoder_cfg.mb_x->intra_chroma_pred_mode = cabac.GetIntraChromaPredMode(decoder_cfg);
		}
		
		decoder_cfg.mb_x->flag_type |= (cabac.GetCodedBlockPattern(decoder_cfg) << 16);

		if (decoder_cfg.mb_x->flag_type & (MASK_MB_LUMA | FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) {
			ResidualE();
		} else {
			Restore();
		}

	} else {
		// intra
		GetMBLayerIE();
	}
		
	return 0;

}

void Video::H264::MBPredIE() {
	const MacroBlockSet * mbl(0), * mbu(0);
	unsigned int dc_pred_mode(0);
	char val_a(0);


	if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // 8x8		
		dc_pred_mode = ((decoder_cfg.mb_a->flag_type & decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_a->flag_type & decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_a->intra_pred_mode[5] < decoder_cfg.mb_b->intra_pred_mode[10])?decoder_cfg.mb_a->intra_pred_mode[5]:decoder_cfg.mb_b->intra_pred_mode[10];

		if (cabac.GetPrevIntraPredModeFlag(decoder_cfg)) {
			decoder_cfg.mb_x->intra_pred_mode[0] = decoder_cfg.mb_x->intra_pred_mode[1] = decoder_cfg.mb_x->intra_pred_mode[2] = decoder_cfg.mb_x->intra_pred_mode[3] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[0] = cabac.GetRemIntraMode(decoder_cfg) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[0] >= val_a) decoder_cfg.mb_x->intra_pred_mode[0]++;

			decoder_cfg.mb_x->intra_pred_mode[1] = decoder_cfg.mb_x->intra_pred_mode[2] = decoder_cfg.mb_x->intra_pred_mode[3] = decoder_cfg.mb_x->intra_pred_mode[0];
		}




		dc_pred_mode = ((decoder_cfg.mb_b->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_b->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_x->intra_pred_mode[1] < decoder_cfg.mb_b->intra_pred_mode[14])?decoder_cfg.mb_x->intra_pred_mode[1]:decoder_cfg.mb_b->intra_pred_mode[14];

		if (cabac.GetPrevIntraPredModeFlag(decoder_cfg)) {
			decoder_cfg.mb_x->intra_pred_mode[4] = decoder_cfg.mb_x->intra_pred_mode[5] = decoder_cfg.mb_x->intra_pred_mode[6] = decoder_cfg.mb_x->intra_pred_mode[7] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[4] = cabac.GetRemIntraMode(decoder_cfg) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[4] >= val_a) decoder_cfg.mb_x->intra_pred_mode[4]++;

			decoder_cfg.mb_x->intra_pred_mode[5] = decoder_cfg.mb_x->intra_pred_mode[6] = decoder_cfg.mb_x->intra_pred_mode[7] = decoder_cfg.mb_x->intra_pred_mode[4];
		}




		dc_pred_mode = ((decoder_cfg.mb_a->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
		if (dc_pred_mode == 0) if ((decoder_cfg.mb_a->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

		val_a = 0;
		if (dc_pred_mode == 0) val_a = (decoder_cfg.mb_a->intra_pred_mode[13] < decoder_cfg.mb_x->intra_pred_mode[2])?decoder_cfg.mb_a->intra_pred_mode[13]:decoder_cfg.mb_x->intra_pred_mode[2];

		if (cabac.GetPrevIntraPredModeFlag(decoder_cfg)) {
			decoder_cfg.mb_x->intra_pred_mode[8] = decoder_cfg.mb_x->intra_pred_mode[9] = decoder_cfg.mb_x->intra_pred_mode[10] = decoder_cfg.mb_x->intra_pred_mode[11] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[8] = cabac.GetRemIntraMode(decoder_cfg) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[8] >= val_a) decoder_cfg.mb_x->intra_pred_mode[8]++;

			decoder_cfg.mb_x->intra_pred_mode[9] = decoder_cfg.mb_x->intra_pred_mode[10] = decoder_cfg.mb_x->intra_pred_mode[11] = decoder_cfg.mb_x->intra_pred_mode[8];
		}

		



		val_a = (decoder_cfg.mb_x->intra_pred_mode[9] < decoder_cfg.mb_x->intra_pred_mode[6])?decoder_cfg.mb_x->intra_pred_mode[9]:decoder_cfg.mb_x->intra_pred_mode[6];

		if (cabac.GetPrevIntraPredModeFlag(decoder_cfg)) {
			decoder_cfg.mb_x->intra_pred_mode[12] = decoder_cfg.mb_x->intra_pred_mode[13] = decoder_cfg.mb_x->intra_pred_mode[14] = decoder_cfg.mb_x->intra_pred_mode[15] = val_a;
		} else {
			decoder_cfg.mb_x->intra_pred_mode[12] = cabac.GetRemIntraMode(decoder_cfg) - 2;
			if (decoder_cfg.mb_x->intra_pred_mode[12] >= val_a) decoder_cfg.mb_x->intra_pred_mode[12]++;

			decoder_cfg.mb_x->intra_pred_mode[13] = decoder_cfg.mb_x->intra_pred_mode[14] = decoder_cfg.mb_x->intra_pred_mode[15] = decoder_cfg.mb_x->intra_pred_mode[12];
		}
		
	} else {
		for (unsigned int bidx(0);bidx<16;bidx++) {
			mbl = decoder_cfg.mb_a;
			if (left_map[bidx]) mbl = decoder_cfg.mb_x;

			mbu = decoder_cfg.mb_b;
			if (up_map[bidx]) mbu = decoder_cfg.mb_x;
						
			dc_pred_mode = ((mbl->flag_type & mbu->flag_type & FLAG_MB_AVAILABLE) ^ FLAG_MB_AVAILABLE);
			if (dc_pred_mode == 0) if ((mbl->flag_type & mbu->flag_type & FLAG_MB_TYPE_I) == 0) dc_pred_mode = current_slice.pic_set->constrained_intra_pred_flag;

			val_a = 0;
			if (dc_pred_mode == 0) val_a = (mbl->intra_pred_mode[left_idx[bidx]] < mbu->intra_pred_mode[up_idx[bidx]])?mbl->intra_pred_mode[left_idx[bidx]]:mbu->intra_pred_mode[up_idx[bidx]];


			if (cabac.GetPrevIntraPredModeFlag(decoder_cfg)) {
				decoder_cfg.mb_x->intra_pred_mode[bidx] = val_a;

			} else {
				decoder_cfg.mb_x->intra_pred_mode[bidx] = cabac.GetRemIntraMode(decoder_cfg) - 2;
				if (decoder_cfg.mb_x->intra_pred_mode[bidx] >= val_a) decoder_cfg.mb_x->intra_pred_mode[bidx]++;
			}
		}
	}
}

void Video::H264::MBPredE() {

	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0)
			decoder_cfg.mb_x->sub[0].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 0);
		
		switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
			case FLAG_MB_PART_V:
				decoder_cfg.mb_x->sub[1].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
				if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L0)
					decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[2].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 2);
			break;
			case FLAG_MB_PART_H:
				decoder_cfg.mb_x->sub[2].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
				if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L0)
					decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[1].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 1);
			break;
			case 0:
				decoder_cfg.mb_x->sub[1].ref_idx[0] = decoder_cfg.mb_x->sub[2].ref_idx[0] = decoder_cfg.mb_x->sub[3].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];
			break;
			
		}
	}

	if ((current_slice.num_ref_idx_active[1] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1)
			decoder_cfg.mb_x->sub[0].ref_idx[1] = cabac.GetRefIdx(decoder_cfg, 1, 0);
		
		switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {			
			case FLAG_MB_PART_V:
				decoder_cfg.mb_x->sub[1].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
				if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L1)
					decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[2].ref_idx[1] = cabac.GetRefIdx(decoder_cfg, 1, 2);
			break;
			case FLAG_MB_PART_H:
				decoder_cfg.mb_x->sub[2].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
				if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L1)
					decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[1].ref_idx[1] = cabac.GetRefIdx(decoder_cfg, 1, 1);				
			break;
			case 0:
				decoder_cfg.mb_x->sub[1].ref_idx[1] = decoder_cfg.mb_x->sub[2].ref_idx[1] = decoder_cfg.mb_x->sub[3].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];
			break;
		}
	}

	if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0) {
		cabac.GetMVD(decoder_cfg.mb_x->sub[0].mvd[0][0], decoder_cfg, 0x00000000);

		for (unsigned int i(1);i<4;i++) {
			decoder_cfg.mb_x->sub[0].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
			decoder_cfg.mb_x->sub[0].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
		}

	}

	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
		case FLAG_MB_PART_V:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L0) {
				cabac.GetMVD(decoder_cfg.mb_x->sub[2].mvd[0][0], decoder_cfg, 0x00000008);

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[2].mvd[0][0][0];
					decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[2].mvd[0][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[2].mvd[0][0][0];
					decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[2].mvd[0][0][1];
				}

			}
		break;
		case FLAG_MB_PART_H:		
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}

			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L0) {
				cabac.GetMVD(decoder_cfg.mb_x->sub[1].mvd[0][0], decoder_cfg, 0x00000004);

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[1].mvd[0][0][0];
					decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[1].mvd[0][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[1].mvd[0][0][0];
					decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[1].mvd[0][0][1];
				}
			}
		break;
		
	}


	if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1) {
		cabac.GetMVD(decoder_cfg.mb_x->sub[0].mvd[1][0], decoder_cfg, 0x00010000);

		for (unsigned int i(1);i<4;i++) {
			decoder_cfg.mb_x->sub[0].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
			decoder_cfg.mb_x->sub[0].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
		}
	}

	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H)) {
		case FLAG_MB_PART_V:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L1) {
				cabac.GetMVD(decoder_cfg.mb_x->sub[2].mvd[1][0], decoder_cfg, 0x00010008);

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[2].mvd[1][0][0];
					decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[2].mvd[1][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[2].mvd[1][0][0];
					decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[2].mvd[1][0][1];
				}


			}
		break;
		case FLAG_MB_PART_H:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}


			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L1) {
				cabac.GetMVD(decoder_cfg.mb_x->sub[1].mvd[1][0], decoder_cfg, 0x00010004);

				for (unsigned int i(1);i<4;i++) {
					decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[1].mvd[1][0][0];
					decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[1].mvd[1][0][1];
				}

				for (unsigned int i(0);i<4;i++) {
					decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[1].mvd[1][0][0];
					decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[1].mvd[1][0][1];
				}
			}
		break;
		case 0:
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[1].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[1].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[1].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[2].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[2].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[2].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[3].mvd[0][i][0] = decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				decoder_cfg.mb_x->sub[3].mvd[0][i][1] = decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			}
			for (unsigned int i(0);i<4;i++) {
				decoder_cfg.mb_x->sub[3].mvd[1][i][0] = decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				decoder_cfg.mb_x->sub[3].mvd[1][i][1] = decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			}

		break;
	}
}

void Video::H264::SMBPPredE() {
	
	decoder_cfg.mb_x->sub[0].type_flag = cabac.GetSubMBPType(decoder_cfg);
	decoder_cfg.mb_x->sub[0].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[0].type_flag];
	decoder_cfg.mb_x->sub[1].type_flag = cabac.GetSubMBPType(decoder_cfg);
	decoder_cfg.mb_x->sub[1].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[1].type_flag];
	decoder_cfg.mb_x->sub[2].type_flag = cabac.GetSubMBPType(decoder_cfg);
	decoder_cfg.mb_x->sub[2].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[2].type_flag];
	decoder_cfg.mb_x->sub[3].type_flag = cabac.GetSubMBPType(decoder_cfg);
	decoder_cfg.mb_x->sub[3].type_flag |= psubtype_pred_mode[decoder_cfg.mb_x->sub[3].type_flag];
	


	if ((decoder_cfg.mb_x->flag_type & MASK_MB_TYPE_ID) != 4)
	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		decoder_cfg.mb_x->sub[0].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 0);
		decoder_cfg.mb_x->sub[1].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 1);
		decoder_cfg.mb_x->sub[2].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 2);
		decoder_cfg.mb_x->sub[3].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, 3);
	}

	for (unsigned int i(0);i<4;i++)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));

			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];

		break;
		case FLAG_SUB_PART_V:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][2], decoder_cfg, (i<<2) | 2);
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][2][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][2][1];

		break;
		case FLAG_SUB_PART_H:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][1], decoder_cfg, (i<<2) | 1);
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][1][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2) | 0);
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][1], decoder_cfg, (i<<2) | 1);
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][2], decoder_cfg, (i<<2) | 2);
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][3], decoder_cfg, (i<<2) | 3);
		break;
	}
}

void Video::H264::SMBBPredE() {

	for (unsigned int i(0);i<4;i++) {
		decoder_cfg.mb_x->sub[i].type_flag = cabac.GetSubMBBType(decoder_cfg);
		decoder_cfg.mb_x->sub[i].type_flag |= bsubtype_pred_mode[decoder_cfg.mb_x->sub[i].type_flag];

	}
	

	if ((current_slice.num_ref_idx_active[0] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		for (unsigned int i(0);i<4;i++)
		if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L0)
			decoder_cfg.mb_x->sub[i].ref_idx[0] = cabac.GetRefIdx(decoder_cfg, 0, i);
	}


	if ((current_slice.num_ref_idx_active[1] > 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ current_slice.id_rec.field_pic_flag) & 1)) {
		for (unsigned int i(0);i<4;i++)
		if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L1)
			decoder_cfg.mb_x->sub[i].ref_idx[1] = cabac.GetRefIdx(decoder_cfg, 1, i);
		
	}

	for (unsigned int i(0);i<4;i++)
	if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L0)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));

			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];

		break;
		case FLAG_SUB_PART_V:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[0][1][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][1][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][2], decoder_cfg, (i<<2) | 2);
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][2][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][2][1];

		break;
		case FLAG_SUB_PART_H:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][0], decoder_cfg, (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[0][2][0] = decoder_cfg.mb_x->sub[i].mvd[0][0][0];
			decoder_cfg.mb_x->sub[i].mvd[0][2][1] = decoder_cfg.mb_x->sub[i].mvd[0][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][1], decoder_cfg, (i<<2) | 1);
			decoder_cfg.mb_x->sub[i].mvd[0][3][0] = decoder_cfg.mb_x->sub[i].mvd[0][1][0];
			decoder_cfg.mb_x->sub[i].mvd[0][3][1] = decoder_cfg.mb_x->sub[i].mvd[0][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			for (unsigned int j(0);j<4;j++)
				cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[0][j], decoder_cfg, (i<<2) | j);

		break;
	}

	for (unsigned int i(0);i<4;i++)
	if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L1)
	switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
		case 0:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][0], decoder_cfg, 0x00010000 | (i<<2));

			decoder_cfg.mb_x->sub[i].mvd[1][1][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][1][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
			decoder_cfg.mb_x->sub[i].mvd[1][2][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][2][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];

		break;
		case FLAG_SUB_PART_V:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][0], decoder_cfg, 0x00010000 | (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[1][1][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][1][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][2], decoder_cfg, 0x00010002 | (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][2][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][2][1];

		break;
		case FLAG_SUB_PART_H:
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][0], decoder_cfg, 0x00010000 | (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[1][2][0] = decoder_cfg.mb_x->sub[i].mvd[1][0][0];
			decoder_cfg.mb_x->sub[i].mvd[1][2][1] = decoder_cfg.mb_x->sub[i].mvd[1][0][1];
				
			cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][1], decoder_cfg, 0x00010001 | (i<<2));
			decoder_cfg.mb_x->sub[i].mvd[1][3][0] = decoder_cfg.mb_x->sub[i].mvd[1][1][0];
			decoder_cfg.mb_x->sub[i].mvd[1][3][1] = decoder_cfg.mb_x->sub[i].mvd[1][1][1];
			
		break;
		case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
			for (unsigned int j(0);j<4;j++)
				cabac.GetMVD(decoder_cfg.mb_x->sub[i].mvd[1][j], decoder_cfg, 0x00010000 | (i<<2) | j);

		break;
	}

}

void Video::H264::SetQPs() {
	short qpbdo(52 + (current_slice.seq_set->cmp_depth[0]<<2) + (current_slice.seq_set->cmp_depth[0]<<1));
	
	if (decoder_cfg.mb_x->qp_delta = cabac.GetMBQPDelta(decoder_cfg)) {
		decoder_cfg.mb_x->qp[0] += decoder_cfg.mb_x->qp_delta;
		
		if ((unsigned int)decoder_cfg.mb_x->qp[0] > qpbdo) {
			if (decoder_cfg.mb_x->qp[0] > 0) {
				decoder_cfg.mb_x->qp[0] -= qpbdo;
			} else {
				decoder_cfg.mb_x->qp[0] += qpbdo;
			}
		}

//		decoder_cfg.mb_x->qp[0] %= qpbdo; // qpy'
	}

	if (current_slice.seq_set->qpprime_y_zero_transform_bypass_flag) {
		if (decoder_cfg.mb_x->qp[0] == 0) decoder_cfg.mb_x->flag_type |= FLAG_MB_T_BYPASS;
	}

	decoder_cfg.mb_x->qp[1] = decoder_cfg.mb_x->qp[2] = decoder_cfg.mb_x->qp[0] - (current_slice.seq_set->cmp_depth[0]<<2) + (current_slice.seq_set->cmp_depth[0]<<1);

	if (decoder_cfg.mb_x->flag_type & FLAG_MB_TYPE_S) {
		decoder_cfg.mb_x->qp[0] = decoder_cfg.mb_x->qp[1] = decoder_cfg.mb_x->qp[2] = current_slice.qsy;
	}	
	
	decoder_cfg.mb_x->qp[1] += current_slice.pic_set->chroma_qpinx_offset[1];
	decoder_cfg.mb_x->qp[2] += current_slice.pic_set->chroma_qpinx_offset[2];

	qpbdo = (current_slice.seq_set->cmp_depth[1]<<2)+(current_slice.seq_set->cmp_depth[1]<<1);
		
	decoder_cfg.mb_x->qp[1] = Clip3(-qpbdo, 51, decoder_cfg.mb_x->qp[1]); // qPI
	decoder_cfg.mb_x->qp[2] = Clip3(-qpbdo, 51, decoder_cfg.mb_x->qp[2]);

	if (decoder_cfg.mb_x->qp[1] > 29) decoder_cfg.mb_x->qp[1] = qpc_map[decoder_cfg.mb_x->qp[1]-30];
	if (decoder_cfg.mb_x->qp[2] > 29) decoder_cfg.mb_x->qp[2] = qpc_map[decoder_cfg.mb_x->qp[2]-30];

	decoder_cfg.mb_x->qp[1] += qpbdo;
	decoder_cfg.mb_x->qp[2] += qpbdo;
}

UI_64 Video::H264::ResidualE() {
	unsigned short i16flag(0x0002);

	SetQPs();

// residual luma

	if ((decoder_cfg.mb_x->flag_type & (FLAG_MB_TYPE_I | FLAG_MB_TYPE_0)) == FLAG_MB_TYPE_I) {
		ResidualDCE(decoder_cfg.frame_ptr, 0, 0); // luma Intra 16x16 DC, category 0
		i16flag = 0x0001;
	}
	
	if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 5
		if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8E(decoder_cfg.frame_ptr, 0, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 5);
			
		RestoreSamples8x8(decoder_cfg.frame_ptr, 0);
	} else { // category 2
		if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4E(decoder_cfg.frame_ptr, 0, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag);
			// sample construction
		RestoreSamples4x4(decoder_cfg.frame_ptr, 0);

	}


	switch (decoder_cfg.chroma_array_type) {
		case 1:
			if (decoder_cfg.mb_x->flag_type & (FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) { // Chroma DC, category 3
				ResidualChromaDCE(decoder_cfg.frame_ptr, 1, 3);
				ResidualChromaDCE(decoder_cfg.frame_ptr, 2, 3);
			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_CHROMA_AC) { // Chroma AC, category 4
				ResidualChroma1ACE(decoder_cfg.frame_ptr, 1, 4);
				ResidualChroma1ACE(decoder_cfg.frame_ptr, 2, 4);
			}

			RestoreChroma1(decoder_cfg.frame_ptr, 1);
			RestoreChroma1(decoder_cfg.frame_ptr, 2);
		break;
		case 2:
			if (decoder_cfg.mb_x->flag_type & (FLAG_MB_CHROMA_DC | FLAG_MB_CHROMA_AC)) { // Chroma DC, category 3
				ResidualChromaDCE(decoder_cfg.frame_ptr, 1, 0x0107);
				ResidualChromaDCE(decoder_cfg.frame_ptr, 2, 0x0107);
			}
		
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_CHROMA_AC) { // Chroma AC, category 4
				ResidualChroma2ACE(decoder_cfg.frame_ptr, 1, 4);
				ResidualChroma2ACE(decoder_cfg.frame_ptr, 2, 4);
			}

			RestoreChroma2(decoder_cfg.frame_ptr, 1);
			RestoreChroma2(decoder_cfg.frame_ptr, 2);
		break;
		case 3:			
		// residual Cb
			if (i16flag == 0x0001) {
				ResidualDCE(decoder_cfg.frame_ptr, 1, 6); // Cb Intra 16x16 DC, category 6

			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // caetgory 9
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8E(decoder_cfg.frame_ptr, 1, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 9);
			
				RestoreSamples8x8(decoder_cfg.frame_ptr, 1);
			} else {
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4E(decoder_cfg.frame_ptr, 1, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag | 6);

				RestoreSamples4x4(decoder_cfg.frame_ptr, 1);
			}


		// residual Cr
			if (i16flag == 0x0001) {
				ResidualDCE(decoder_cfg.frame_ptr, 2, 10); // Cr Intra 16x16 DC, category 10
			}

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 13
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual8x8E(decoder_cfg.frame_ptr, 2, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | 13);
		
				RestoreSamples8x8(decoder_cfg.frame_ptr, 2);
			} else {
				if (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) Residual4x4E(decoder_cfg.frame_ptr, 2, (decoder_cfg.mb_x->flag_type & MASK_MB_LUMA) | i16flag | 10);

				RestoreSamples4x4(decoder_cfg.frame_ptr, 2);
			}


		break;
	}


	return 0;
}


UI_64 Video::H264::Restore() {

// luma
	if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 5
		RestoreSamples8x8(decoder_cfg.frame_ptr, 0);
	} else { // category 2
		RestoreSamples4x4(decoder_cfg.frame_ptr, 0);
	}


	switch (decoder_cfg.chroma_array_type) {
		case 1:
			RestoreChroma1(decoder_cfg.frame_ptr, 1);
			RestoreChroma1(decoder_cfg.frame_ptr, 2);
		break;
		case 2:
			RestoreChroma2(decoder_cfg.frame_ptr, 1);
			RestoreChroma2(decoder_cfg.frame_ptr, 1);
		break;
		case 3:			
		// Cb

			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // caetgory 9
				RestoreSamples8x8(decoder_cfg.frame_ptr, 1);
			} else {
				RestoreSamples4x4(decoder_cfg.frame_ptr, 1);
			}


		// Cr
			if (decoder_cfg.mb_x->flag_type & FLAG_MB_8x8_SIZE) { // category 13	
				RestoreSamples8x8(decoder_cfg.frame_ptr, 2);
			} else {
				RestoreSamples4x4(decoder_cfg.frame_ptr, 2);
			}


		break;
	}	


	return 0;
}


UI_64 Video::H264::Residual8x8E(short * co_ptr, unsigned short cmp_sel, unsigned int cat_idx) { // 5, 9, 13
	UI_64 result(1);
	unsigned int en_idx(0), pass_val((cat_idx & MASK_MB_LUMA) >> 15);

	co_ptr += (cmp_sel<<8);

	cat_idx &= 0x000F;

	for (unsigned int idx(0);idx<4;idx++, co_ptr += 64) {
		pass_val >>= 1;		
		if ((pass_val & 1) == 0) continue;

		result = 1;
		if (decoder_cfg.chroma_array_type == 3) result = cabac.GetCodedBlockFlag8x8(decoder_cfg, cat_idx | (idx << 8), cmp_sel);

		if (result) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (0x000F << (idx << 2));

			result = 0;
			en_idx = 63;

			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag8x8(decoder_cfg, i, cat_idx)) {
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag8x8(decoder_cfg, i, cat_idx)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;		
		
			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>0;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}

				}
				if ((result >>= 1) == 0) break;
			}
		}
	}
	
	return result;
}



UI_64 Video::H264::Residual4x4E(short * co_ptr, unsigned short cmp_sel, unsigned int cat_idx) { // cat 1, 2, 7, 8, 11, 12
	UI_64 result(0);
	unsigned int en_idx(0), en_idx_val(cat_idx & 0x0001), pass_val((cat_idx & MASK_MB_LUMA) >> 15);

	co_ptr += (cmp_sel<<8);

	
	cat_idx &= 0x000F;
	for (unsigned int idx(0);idx<4;idx++, co_ptr += 64) {
		pass_val >>= 1;		
		if ((pass_val & 1) == 0) continue;

		if (cabac.GetCodedBlockFlag(decoder_cfg, cat_idx | (idx << 10), cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << (idx<<2));			
			
			en_idx = 15 - en_idx_val;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, cat_idx)) { // i
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, cat_idx)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;
		
			en_idx += en_idx_val;
	
			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>0;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
				if ((result >>= 1) == 0) break;
			}
		}

		if (cabac.GetCodedBlockFlag(decoder_cfg, cat_idx | (((idx<<2)|1) << 8), cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << ((idx<<2)|1));
			
			
			en_idx = 15 - en_idx_val;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, cat_idx)) { // i
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, cat_idx)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;
		
			en_idx += en_idx_val + 16;
	
			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>16;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
				if ((result >>= 1) == 0) break;
			}
		}

		if (cabac.GetCodedBlockFlag(decoder_cfg, cat_idx | (((idx<<2)|2) << 8), cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << ((idx<<2)|2));
			
			
			en_idx = 15 - en_idx_val;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, cat_idx)) { // i
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, cat_idx)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;
		
			en_idx += en_idx_val+32;
	
			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>32;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
				if ((result >>= 1) == 0) break;
			}
		}

		if (cabac.GetCodedBlockFlag(decoder_cfg, cat_idx | (((idx<<2)|3) << 8), cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << ((idx<<2)|3));
			
			
			en_idx = 15 - en_idx_val;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, cat_idx)) { // i
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, cat_idx)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;
		
			en_idx += en_idx_val+48;
	
			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>48;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, cat_idx);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
				if ((result >>= 1) == 0) break;
			}
		}		
	}

	return result;
}


UI_64 Video::H264::ResidualDCE(short * co_ptr, unsigned short cmp_sel, unsigned int block_cat) { // category 0, 6, 10
	UI_64 result(0);
	unsigned int en_idx(15);

	if (cabac.GetCodedBlockFlagDC(decoder_cfg, block_cat, cmp_sel)) {
		co_ptr -= 48;
		co_ptr += (cmp_sel<<4);

		decoder_cfg.mb_x->coded_block[3] |= (1 << cmp_sel);


		for (unsigned int i(0);i<en_idx;i++) {
			result <<= 1;
			if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, block_cat)) {
				result |= 1;
				if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, block_cat)) {
					en_idx = i;
					result >>= 1;
				}
			}
		}
		
		decoder_cfg.above_flag = 0;
		decoder_cfg.labs_selector = 15;

		co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, block_cat);

		if (cabac.GetCoeffSignFlag(decoder_cfg)) {
			co_ptr[en_idx] = -co_ptr[en_idx];
		}

		for (unsigned int i(en_idx);i>0;i--) {
			if (result & 1) {				
				co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, block_cat);

				if (cabac.GetCoeffSignFlag(decoder_cfg)) {
					co_ptr[i-1] = -co_ptr[i-1];
				}
			}
			if ((result >>= 1) == 0) break;
		}
	}
	
	return result;
}


UI_64 Video::H264::ResidualChromaDCE(short * co_ptr, unsigned short cmp_sel, unsigned int zidx) { // cat 3
	UI_64 result(0);	
	unsigned int en_idx(zidx & 0x000F);


	if (cabac.GetCodedBlockFlagDC(decoder_cfg, 3, cmp_sel)) {
		decoder_cfg.mb_x->coded_block[3] |= (1 << cmp_sel);

		zidx >>= 8;

		co_ptr -= 48;
		co_ptr += (cmp_sel<<4);


		for (unsigned int i(0);i<en_idx;i++) {
			result <<= 1;
			if (cabac.GetSignificantCoeffFlagChromaDC(decoder_cfg, i>>zidx)) {
				result |= 1;

				if (cabac.GetLastSignificantCoeffFlagChromaDC(decoder_cfg, i>>zidx)) {
					en_idx = i;
					result >>= 1;
				}
			}
		}
		
		decoder_cfg.above_flag = 0;
		decoder_cfg.labs_selector = 15;

		co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, 3);

		if (cabac.GetCoeffSignFlag(decoder_cfg)) {
			co_ptr[en_idx] = -co_ptr[en_idx];
		}

		for (unsigned int i(en_idx);i>0;i--) {
			if (result & 1) {				
				co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, 3);

				if (cabac.GetCoeffSignFlag(decoder_cfg)) {
					co_ptr[i-1] = -co_ptr[i-1];
				}
			}
			if ((result >>= 1) == 0) break;
		}
	}
	
	return result;
}
			
UI_64 Video::H264::ResidualChroma1ACE(short * co_ptr, unsigned short cmp_sel, unsigned int block_idx) { // cat 4
	UI_64 result(0);
	unsigned int en_idx(0);

	co_ptr += (cmp_sel<<8);

	for (unsigned int idx(0);idx<4;idx++) {

		if (cabac.GetCodedBlockFlagChroma1AC(decoder_cfg, block_idx, cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << idx);

			en_idx = 14;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, 4)) {
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, 4)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;

			en_idx++;

			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, 4);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>1;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, 4);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
				if ((result >>= 1) == 0) break;
			}
		}

		co_ptr += 16;
		block_idx += 0x0100;
	}
	
	return result;
}

UI_64 Video::H264::ResidualChroma2ACE(short * co_ptr, unsigned short cmp_sel, unsigned int block_idx) { // cat 4
	UI_64 result(0);

	unsigned int en_idx(0);

	co_ptr += (cmp_sel<<8);

	for (unsigned int idx(0);idx<8;idx++) {
		
		if (cabac.GetCodedBlockFlagChroma2AC(decoder_cfg, block_idx, cmp_sel)) {
			decoder_cfg.mb_x->coded_block[cmp_sel] |= (1 << idx);

			en_idx = 14;
			for (unsigned int i(0);i<en_idx;i++) {
				result <<= 1;
				if (cabac.GetSignificantCoeffFlag(decoder_cfg, i, 4)) {
					result |= 1;
					if (cabac.GetLastSignificantCoeffFlag(decoder_cfg, i, 4)) {
						en_idx = i;
						result >>= 1;
					}
				}
			}
		
			decoder_cfg.above_flag = 0;
			decoder_cfg.labs_selector = 15;

			en_idx++;

			co_ptr[en_idx] = cabac.GetCoeffAbsLevel(decoder_cfg, 4);

			if (cabac.GetCoeffSignFlag(decoder_cfg)) {
				co_ptr[en_idx] = -co_ptr[en_idx];
			}

			for (unsigned int i(en_idx);i>1;i--) {
				if (result & 1) {				
					co_ptr[i-1] = cabac.GetCoeffAbsLevel(decoder_cfg, 4);

					if (cabac.GetCoeffSignFlag(decoder_cfg)) {
						co_ptr[i-1] = -co_ptr[i-1];
					}
				}
			
				if ((result >>= 1) == 0) break;
			}
		}
		co_ptr += 16;
		block_idx += 0x0100;
	}
	
	return result;
}




// =========================================================================================================================================================================================================================

const unsigned int Video::H264::DecoderState::cbp_map12[2][48] = {
	0x002F0000, 0x001F0000, 0x000F0000, 0x00000000, 0x00170000, 0x001B0000, 0x001D0000, 0x001E0000, 0x00070000, 0x000B0000, 0x000D0000, 0x000E0000, 0x00270000, 0x002B0000, 0x002D0000, 0x002E0000,
	0x00100000, 0x00030000, 0x00050000, 0x000A0000, 0x000C0000, 0x00130000, 0x00150000, 0x001A0000, 0x001C0000, 0x00230000, 0x00250000, 0x002A0000, 0x002C0000, 0x00010000, 0x00020000, 0x00040000,
	0x00080000, 0x00110000, 0x00120000, 0x00140000, 0x00180000, 0x00060000, 0x00090000, 0x00160000, 0x00190000, 0x00200000, 0x00210000, 0x00220000, 0x00240000, 0x00280000, 0x00260000, 0x00290000,
	
	0x00000000, 0x00100000, 0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00200000, 0x00030000, 0x00050000, 0x000A0000, 0x000C0000, 0x000F0000, 0x002F0000, 0x00070000, 0x000B0000, 0x000D0000,
	0x000E0000, 0x00060000, 0x00090000, 0x001F0000, 0x00230000, 0x00250000, 0x002A0000, 0x002C0000, 0x00210000, 0x00220000, 0x00240000, 0x00280000, 0x00270000, 0x002B0000, 0x002D0000, 0x002E0000,
	0x00110000, 0x00120000, 0x00140000, 0x00180000, 0x00130000, 0x00150000, 0x001A0000, 0x001C0000, 0x00170000, 0x001B0000, 0x001D0000, 0x001E0000, 0x00160000, 0x00190000, 0x00260000, 0x00290000
};

const unsigned int Video::H264::DecoderState::cbp_map03[2][16] = {
	0x000F0000, 0x00000000, 0x00070000, 0x000B0000, 0x000D0000, 0x000E0000, 0x00030000, 0x00050000, 0x000A0000, 0x000C0000, 0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00060000, 0x00090000,
	0x00000000, 0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00030000, 0x00050000, 0x000A0000, 0x000C0000, 0x000F0000, 0x00070000, 0x000B0000, 0x000D0000, 0x000E0000, 0x00060000, 0x00090000
};



