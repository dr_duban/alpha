/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"
#include "System\SysUtils.h"

#pragma warning(disable:4244)

Video::H264::pInterpolate Video::H264::ifuncs[4][4];

short Video::H264::FormatMbaffVal(short m_val, unsigned char fflag) {
	if (fflag) {
		m_val <<= 1;
	} else {
		if (m_val < 0) m_val++;
		m_val >>= 1;		
	}

	return m_val;
}

unsigned short Video::H264::FormatMbaffVal(unsigned short m_val, unsigned char fflag) {
	if (m_val != 0xFFFF)
	if (fflag) {
		m_val <<= 1;
	} else {
		m_val >>= 1;		
	}

	return m_val;
}

void Video::H264::FormatICfg(InterPredCfg * i_cfg, unsigned int p_idx, unsigned int c_idx) {

	if (left_map[p_idx]) {
		i_cfg[0].mbi = decoder_cfg.mb_x;
	} else {
		i_cfg[0].mbi = decoder_cfg.mb_a;
	}

	i_cfg[0].part_idx = (left_idx[p_idx] >> 2);
	i_cfg[0].sub_idx = (left_idx[p_idx] & 3);


	if (up_map[p_idx]) {
		i_cfg[1].mbi = decoder_cfg.mb_x;
	} else {
		i_cfg[1].mbi = decoder_cfg.mb_b;
	}

	i_cfg[1].part_idx = (up_idx[p_idx] >> 2);
	i_cfg[1].sub_idx = (up_idx[p_idx] & 3);



	switch (ur_map[c_idx]) {
		case 0:
			i_cfg[2].mbi = decoder_cfg.mb_x;
		break;
		case 1:
			i_cfg[2].mbi = decoder_cfg.mb_u;
		break;
		case 2:
			i_cfg[2].mbi = decoder_cfg.mb_b;
		break;
		default:
			i_cfg[2].mbi = decoder_cfg.mb_c;

	}

	i_cfg[2].part_idx = (ur_idx[c_idx] >> 2);
	i_cfg[2].sub_idx = (ur_idx[c_idx] & 3);


	switch (ul_map[p_idx]) {
		case 0:
			i_cfg[3].mbi = decoder_cfg.mb_x;
		break;
		case 1:
			i_cfg[3].mbi = decoder_cfg.mb_a;
		break;
		case 2:
			i_cfg[3].mbi = decoder_cfg.mb_b;
		break;
		default:
			i_cfg[3].mbi = decoder_cfg.mb_d;
	}

	i_cfg[3].part_idx = (ul_idx[p_idx] >> 2);
	i_cfg[3].sub_idx = (ul_idx[p_idx] & 3);






	if ((i_cfg[2].mbi->flag_type & FLAG_MB_AVAILABLE) == 0) {
		i_cfg[2].mbi = i_cfg[3].mbi;
		i_cfg[2].part_idx = i_cfg[3].part_idx;
		i_cfg[2].sub_idx = i_cfg[3].sub_idx;
	}


	for (unsigned int i(0);i<4;i++) {
		if (i_cfg[i].mbi->flag_type & FLAG_MB_TYPE_I) {
			reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].ref_idx[0] = 0xFFFF;
			reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].ref_idx[1] = 0xFFFF;

			reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].mvd[0][i_cfg[i].sub_idx][0] = reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].mvd[0][i_cfg[i].sub_idx][1] = 0;
			reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].mvd[1][i_cfg[i].sub_idx][0] = reinterpret_cast<MacroBlockSet::SubSet *>(const_cast<short *>(i_cfg[i].mbi->x_tent))[i_cfg[i].part_idx].mvd[1][i_cfg[i].sub_idx][1] = 0;

		}
	}
}

void Video::H264::MVMedianPred(MacroBlockSet::SubSet & mvc, const InterPredCfg * i_cfg, unsigned int list_idx, unsigned int xps_idx) {
	unsigned int sflag(0);
	short minc[2], maxc[2], zval[2];

	if (((i_cfg[1].mbi->flag_type | i_cfg[2].mbi->flag_type) & FLAG_MB_AVAILABLE) == 0) sflag = 1;

	if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0x11)) {
		if (mvc.ref_idx[list_idx] == reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].ref_idx[list_idx]) sflag |= 1;
		if (mvc.ref_idx[list_idx] == reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].ref_idx[list_idx]) sflag |= 2;			
		if (mvc.ref_idx[list_idx] == reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].ref_idx[list_idx]) sflag |= 4;

	} else {
		if (mvc.ref_idx[list_idx] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].ref_idx[list_idx], i_cfg[0].mbi->field_decoding_flag)) sflag |= 1;
		if (mvc.ref_idx[list_idx] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].ref_idx[list_idx], i_cfg[1].mbi->field_decoding_flag)) sflag |= 2;
		if (mvc.ref_idx[list_idx] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].ref_idx[list_idx], i_cfg[2].mbi->field_decoding_flag)) sflag |= 4;
	}
	
	switch (sflag) {
		case 1:
			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0x11)) {
				mvc.mvd[list_idx][xps_idx][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][0];
				mvc.mvd[list_idx][xps_idx][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][1];					
			} else {
				mvc.mvd[list_idx][xps_idx][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][0], i_cfg[0].mbi->field_decoding_flag);
				mvc.mvd[list_idx][xps_idx][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][1], i_cfg[0].mbi->field_decoding_flag);
			}
		break;
		case 2:
			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[1].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[1].mbi->field_decoding_flag) == 0x11)) {
				mvc.mvd[list_idx][xps_idx][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][0];
				mvc.mvd[list_idx][xps_idx][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][1];					
			} else {
				mvc.mvd[list_idx][xps_idx][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][0], i_cfg[1].mbi->field_decoding_flag);
				mvc.mvd[list_idx][xps_idx][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][1], i_cfg[1].mbi->field_decoding_flag);					
			}
		break;
		case 4:
			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[2].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[2].mbi->field_decoding_flag) == 0x11)) {
				mvc.mvd[list_idx][xps_idx][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][0];
				mvc.mvd[list_idx][xps_idx][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][1];					
			} else {
				mvc.mvd[list_idx][xps_idx][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][0], i_cfg[2].mbi->field_decoding_flag);
				mvc.mvd[list_idx][xps_idx][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][1], i_cfg[2].mbi->field_decoding_flag);					
			}
		break;
		default:
			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[0].mbi->field_decoding_flag) == 0x11)) {
				minc[0] = maxc[0] = mvc.mvd[list_idx][xps_idx][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][0];
				minc[1] = maxc[1] = mvc.mvd[list_idx][xps_idx][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][1];
			} else {
				minc[0] = maxc[0] = mvc.mvd[list_idx][xps_idx][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][0], i_cfg[0].mbi->field_decoding_flag);
				minc[1] = maxc[1] = mvc.mvd[list_idx][xps_idx][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[0].mbi->x_tent)[i_cfg[0].part_idx].mvd[list_idx][i_cfg[0].sub_idx][1], i_cfg[0].mbi->field_decoding_flag);
			}

			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[1].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[1].mbi->field_decoding_flag) == 0x11)) {
				zval[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][0];
				zval[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][1];
			} else {
				zval[0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][0], i_cfg[1].mbi->field_decoding_flag);
				zval[1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[1].mbi->x_tent)[i_cfg[1].part_idx].mvd[list_idx][i_cfg[1].sub_idx][1], i_cfg[1].mbi->field_decoding_flag);
			}

			minc[0] += ((zval[0] - minc[0])>>16)&(zval[0] - minc[0]);
			maxc[0] += ((maxc[0] - zval[0])>>16)&(zval[0] - maxc[0]);
			mvc.mvd[list_idx][xps_idx][0] += zval[0];

			minc[1] += ((zval[1] - minc[1])>>16)&(zval[1] - minc[1]);
			maxc[1] += ((maxc[1] - zval[1])>>16)&(zval[1] - maxc[1]);
			mvc.mvd[list_idx][xps_idx][1] += zval[1];

			if (((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[2].mbi->field_decoding_flag) == 0) || ((decoder_cfg.mb_x->field_decoding_flag ^ i_cfg[2].mbi->field_decoding_flag) == 0x11)) {
				zval[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][0];
				zval[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][1];
			} else {
				zval[0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][0], i_cfg[2].mbi->field_decoding_flag);
				zval[1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(i_cfg[2].mbi->x_tent)[i_cfg[2].part_idx].mvd[list_idx][i_cfg[2].sub_idx][1], i_cfg[2].mbi->field_decoding_flag);
			}
	
			minc[0] += ((zval[0] - minc[0])>>16)&(zval[0] - minc[0]);
			maxc[0] += ((maxc[0] - zval[0])>>16)&(zval[0] - maxc[0]);
			mvc.mvd[list_idx][xps_idx][0] += zval[0];

			minc[1] += ((zval[1] - minc[1])>>16)&(zval[1] - minc[1]);
			maxc[1] += ((maxc[1] - zval[1])>>16)&(zval[1] - maxc[1]);
			mvc.mvd[list_idx][xps_idx][1] += zval[1];

			mvc.mvd[list_idx][xps_idx][0] -= (minc[0] + maxc[0]);
			mvc.mvd[list_idx][xps_idx][1] -= (minc[1] + maxc[1]);
			

	}				

}








void Video::H264::SamplesCopy() {
	if (reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0] != 0xFFFF) {
		if (short * ref0_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.ref_pic_list[0][reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]]].buffer.Acquire())) {
			System::MemoryCopy_SSE3(decoder_cfg.frame_ptr, ref0_ptr + decoder_cfg.current_mb_offset + 256, 3*256*sizeof(short));

			pic_list[decoder_cfg.ref_pic_list[0][reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[0]]].buffer.Release();
		}
		
	} else {
		if (short * ref0_ptr = reinterpret_cast<short *>(pic_list[decoder_cfg.ref_pic_list[1][reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]]].buffer.Acquire())) {
			System::MemoryCopy_SSE3(decoder_cfg.frame_ptr, ref0_ptr + decoder_cfg.current_mb_offset + 256, 3*256*sizeof(short));

			pic_list[decoder_cfg.ref_pic_list[1][reinterpret_cast<const MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent)[0].ref_idx[1]]].buffer.Release();
		}
	}
}


void Video::H264::InterPredLuma(short * lev_ptr, unsigned int cmp_sel) {
	InterPredSet iset;

	const short * ref0_ptr(0), * ref1_ptr(0);
	__int64 pic0_offset(0), pic1_offset(0), temp_o(0);
	unsigned int pic0(0), pic1(0);

	const MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);


	pic0_offset = pic1_offset = 0;
	iset.l_offset = (decoder_cfg.pline_offset<<1);
	iset._reserved = 0;

	for (unsigned int i(0);i<4;i++) { // part run		

		ref0_ptr = ref1_ptr = 0;
		pic0_offset = pic1_offset = 0;

		if (mvc[i].ref_idx[0] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) { // pair or afro
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				}

			}
*/			
			pic0 = decoder_cfg.ref_pic_list[0][mvc[i].ref_idx[0]];
			ref0_ptr = reinterpret_cast<short *>(pic_list[pic0].buffer.Acquire());
				
			if (pic_list[pic0]._reserved & FLAG_SECOND_FIELD) {
				pic0_offset = vui_params.second_field_offset;
			}
			
			if (ref0_ptr) ref0_ptr += pic0_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;
		}

		if (mvc[i].ref_idx[1] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) {
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				}

			}
*/
			pic1 = decoder_cfg.ref_pic_list[1][mvc[i].ref_idx[1]];
			ref1_ptr = reinterpret_cast<short *>(pic_list[pic1].buffer.Acquire());

			if (pic_list[pic1]._reserved & FLAG_SECOND_FIELD) {
				pic1_offset = vui_params.second_field_offset;
			}

			if (ref1_ptr) ref1_ptr += pic1_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;

		}



		
		for (unsigned int j(0);j<4;j++) { // sub run
	//		

			if (ref0_ptr) {
				iset.map_flags = 0;
				
				iset.mvd[0] = (mvc[i].mvd[0][j][0] >> 2) + ((i & 1)<<3) + ((j & 1)<<2); iset.mvd[1] = (mvc[i].mvd[0][j][1] >> 2) + ((i & 2)<<2) + ((j & 2)<<1);

				pic0_offset = (iset.mvd[0] >> 4);				
				iset.mvd[0] -= (pic0_offset << 4);
				
				temp_o = decoder_cfg.line_ptr + pic0_offset - decoder_cfg.slice_width;
				if (temp_o >= 0) {
					if (temp_o > 0) {
						iset.mvd[0] += (temp_o<<4);
						iset.map_flags |= 0x20;

					} else {
						if (iset.mvd[0] > 9) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					temp_o = decoder_cfg.line_ptr + pic0_offset - 1;
					if (temp_o <= 0) {
						if (temp_o < 0) {
							iset.mvd[0] += (temp_o<<4);
							iset.map_flags |= 0x10; // left border
						} else {
							if (iset.mvd[0] < 2) iset.map_flags |= 0x10; // left border
						}
						pic0_offset = 1-decoder_cfg.line_ptr;						
					}
				}

				pic0_offset <<= 10;

				if (iset.mvd[0] < 2) iset.map_flags |= 0x01; // left line
				if (iset.mvd[0] > 9) iset.map_flags |= 0x02; // right line 



				pic1_offset = (iset.mvd[1] >> 4);
				iset.mvd[1] -= (pic1_offset<<4);
				
				temp_o = decoder_cfg.line_counter + pic1_offset + 1 - (__int64)current_slice.seq_set->pic_height_mapunits;
				if (temp_o >= 0) {
					if (temp_o > 0) {
						iset.mvd[1] += (temp_o<<4);
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > 9) iset.map_flags |= 0x80; // bottom border
					}
					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					temp_o = decoder_cfg.line_counter + pic1_offset;
					if (temp_o <= 0) {
						if (temp_o < 0) {
							iset.mvd[1] += (temp_o<<4);
							iset.map_flags |= 0x40; // top border
						} else {
							if (iset.mvd[1] < 2) iset.map_flags |= 0x40; // top border
						}
						pic1_offset = -decoder_cfg.line_counter;						
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] < 2) iset.map_flags |= 0x04; // top line
				if (iset.mvd[1] > 9) iset.map_flags |= 0x08; // bottom line 


				iset.func = ifuncs[mvc[i].mvd[0][j][0] & 3][mvc[i].mvd[0][j][1] & 3];

				H264_InterpolateLuma(&iset, ref0_ptr + pic0_offset + pic1_offset, 0, decoder_cfg.clip_vals[0]);

			}

			if (ref1_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[1][j][0] >> 2) + ((i & 1)<<3) + ((j & 1)<<2); iset.mvd[1] = (mvc[i].mvd[1][j][1] >> 2) + ((i & 2)<<2) + ((j & 2)<<1);

				pic0_offset = (iset.mvd[0] >> 4);
				iset.mvd[0] -= (pic0_offset<<4);
				
				temp_o = decoder_cfg.line_ptr + pic0_offset - decoder_cfg.slice_width;
				if (temp_o >= 0) {
					if (temp_o > 0) {
						iset.mvd[0] += (temp_o<<4);
						iset.map_flags |= 0x20;

					} else {
						if (iset.mvd[0] > 9) iset.map_flags |= 0x20; // right border
					}
					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					temp_o = decoder_cfg.line_ptr + pic0_offset - 1;
					if (temp_o <= 0) {
						if (temp_o < 0) {
							iset.mvd[0] += (temp_o<<4);
							iset.map_flags |= 0x10; // left border
						} else {
							if (iset.mvd[0] < 2) iset.map_flags |= 0x10; // left border
						}
						pic0_offset = 1-decoder_cfg.line_ptr;						
					}
				}

				pic0_offset <<= 10;

				if (iset.mvd[0] < 2) iset.map_flags |= 0x01; // left line
				if (iset.mvd[0] > 9) iset.map_flags |= 0x02; // right line 



				pic1_offset = (iset.mvd[1] >> 4);
				iset.mvd[1] -= (pic1_offset<<4);
				
				temp_o = decoder_cfg.line_counter + pic1_offset + 1 - (__int64)current_slice.seq_set->pic_height_mapunits;
				if (temp_o >= 0) {
					if (temp_o > 0) {
						iset.mvd[1] += (temp_o<<4);
						iset.map_flags |= 0x80;

					} else {
						if (iset.mvd[1] > 9) iset.map_flags |= 0x80; // bottom border
					}
					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					temp_o = decoder_cfg.line_counter + pic1_offset;
					if (temp_o <= 0) {
						if (temp_o < 0) {
							iset.mvd[1] += (temp_o<<4);
							iset.map_flags |= 0x40; // top border
						} else {
							if (iset.mvd[1] < 2) iset.map_flags |= 0x40; // top border
						}
						pic1_offset = -decoder_cfg.line_counter;						
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] < 2) iset.map_flags |= 0x04; // top line
				if (iset.mvd[1] > 9) iset.map_flags |= 0x08; // bottom line 
			
				iset.func = ifuncs[mvc[i].mvd[1][j][0] & 3][mvc[i].mvd[1][j][1] & 3];

				H264_InterpolateLuma(&iset, ref1_ptr + pic0_offset + pic1_offset, ref0_ptr, decoder_cfg.clip_vals[0]);

			}
			
						

			if ((current_slice.weight_type == 0) || ((current_slice.weight_type == 2) && ((ref0_ptr == 0) || (ref1_ptr == 0)))) {
				H264_DefaultLumaUpdate(lev_ptr, decoder_cfg.clip_vals[0]);
			} else {
				SetInterWeights(iset, mvc[i].ref_idx[0], mvc[i].ref_idx[1], cmp_sel);
				H264_WeightedLumaUpdate(lev_ptr, &iset, decoder_cfg.clip_vals[0]);
			}

			lev_ptr += 16;
		}
		

		if (ref0_ptr) {
			pic_list[pic0].buffer.Release();
		}

		if (ref1_ptr) {
			pic_list[pic1].buffer.Release();
		}

	}
}



void Video::H264::InterPredChroma1(short * lev_ptr, unsigned int cmp_sel) {
	InterPredSet iset;
	const short * ref0_ptr(0), * ref1_ptr(0);
	__int64 pic0_offset, pic1_offset;
	unsigned int pic0(0), pic1(0);
	
	const MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);

	short y_off[2] = {0, 0};

	pic0_offset = pic1_offset = 0;
	iset.l_offset = (decoder_cfg.pline_offset<<1);
	iset._reserved = 112; // cmp_sel;
	
	

	for (unsigned int i(0);i<4;i++) { // part run		

		ref0_ptr = ref1_ptr = 0;
		pic0_offset = pic1_offset = 0;

		y_off[0] = y_off[1] = 0;

		if (mvc[i].ref_idx[0] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) { // pair or afro
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				}
			}
*/
			pic0 = decoder_cfg.ref_pic_list[0][mvc[i].ref_idx[0]];
			
			ref0_ptr = reinterpret_cast<short *>(pic_list[pic0].buffer.Acquire());

			if (pic_list[pic0]._reserved & FLAG_SECOND_FIELD) {
				pic0_offset = vui_params.second_field_offset;
			}
			
			if (ref0_ptr) {
				ref0_ptr += pic0_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;
			}
		}

		if (mvc[i].ref_idx[1] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) {
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				}

			}
*/
			pic1 = decoder_cfg.ref_pic_list[1][mvc[i].ref_idx[1]];
			ref1_ptr = reinterpret_cast<short *>(pic_list[pic1].buffer.Acquire());

			if (pic_list[pic1]._reserved & FLAG_SECOND_FIELD) {
				pic1_offset = vui_params.second_field_offset;
			}

			if (ref1_ptr) {
				ref1_ptr += pic1_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;
			}
		}

			
		
		for (unsigned int j(0);j<4;j++) { // sub run
			if (ref0_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[0][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[0][j][1]>>3) + ((i & 2)<<1) + (j & 2) + y_off[0];
				
				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 3);
				iset.mvd[1] -= (pic1_offset << 3);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 7;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((1<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}
					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 8 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((1<<3) - 3)) iset.map_flags |= 0x08; // bottom line 
	
				H264_InterpolateChroma(&iset, ref0_ptr + pic0_offset + pic1_offset, 0, (((UI_64)mvc[i].mvd[0][j][1] & 7) << 32) | (mvc[i].mvd[0][j][0] & 7));
			}

			if (ref1_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[1][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[1][j][1]>>3) + ((i & 2)<<1) + (j & 2) + y_off[1];

				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}
					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 3);
				iset.mvd[1] -= (pic1_offset << 3);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 7;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((1<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}
					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 8 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((1<<3) - 3)) iset.map_flags |= 0x08; // bottom line 

				H264_InterpolateChroma(&iset, ref1_ptr + pic0_offset + pic1_offset, ref0_ptr, (((UI_64)mvc[i].mvd[1][j][1] & 7) << 32) | (mvc[i].mvd[1][j][0] & 7));
			}
			
			

			if ((current_slice.weight_type == 0) || ((current_slice.weight_type == 2) && ((ref0_ptr == 0) || (ref1_ptr == 0)))) {
				H264_DefaultChromaUpdate(lev_ptr + (j<<2) - ((j&1)<<1));
			} else {
				// set up weights
				iset.map_flags = current_slice.gray_val[1];

				SetInterWeights(iset, mvc[i].ref_idx[0], mvc[i].ref_idx[1], cmp_sel);
				H264_WeightedChromaUpdate(lev_ptr + (j<<2) - ((j&1)<<1), &iset);
			}						
		}
	

		lev_ptr += 16;


		if (ref0_ptr) {
			pic_list[pic0].buffer.Release();
		}

		if (ref1_ptr) {
			pic_list[pic1].buffer.Release();
		}
	}
}

void Video::H264::InterPredChroma2(short * lev_ptr, unsigned int cmp_sel) {
	InterPredSet iset;
	const short * ref0_ptr(0), * ref1_ptr(0);
	__int64 pic0_offset, pic1_offset;
	unsigned int pic0(0), pic1(0);
	
	const MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);

	pic0_offset = pic1_offset = 0;
	iset.l_offset = (decoder_cfg.pline_offset<<1);
	iset._reserved = 240; // cmp_sel;
	
	

	for (unsigned int i(0);i<4;i++) { // part run		

		ref0_ptr = ref1_ptr = 0;
		pic0_offset = pic1_offset = 0;

		if (mvc[i].ref_idx[0] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) { // pair or afro
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[0] & 1)) pic0_offset = vui_params.second_field_offset;
				}
			}
*/
			pic0 = decoder_cfg.ref_pic_list[0][mvc[i].ref_idx[0]];
			
			ref0_ptr = reinterpret_cast<short *>(pic_list[pic0].buffer.Acquire());

			if (pic_list[pic0]._reserved & FLAG_SECOND_FIELD) {
				pic0_offset = vui_params.second_field_offset;
			}
			
			if (ref0_ptr) ref0_ptr += pic0_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;
		}

		if (mvc[i].ref_idx[1] < MAX_REF_IDX) {
/*
			if ((!current_slice.field_pic_flag) && (decoder_cfg.mb_x->field_decoding_flag)) {
				if (decoder_cfg.mb_x->field_decoding_flag & 0x10) { // pair
					if (decoder_cfg.mb_x->par_val ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				} else {
					if ((decoder_cfg.current_mb_ptr & 1) ^ (mvc[i].ref_idx[1] & 1)) pic1_offset = vui_params.second_field_offset;
				}

			}
*/
			pic1 = decoder_cfg.ref_pic_list[1][mvc[i].ref_idx[1]];
			ref1_ptr = reinterpret_cast<short *>(pic_list[pic1].buffer.Acquire());

			if (pic_list[pic1]._reserved & FLAG_SECOND_FIELD) {
				pic1_offset = vui_params.second_field_offset;
			}

			if (ref1_ptr) ref1_ptr += pic1_offset + ((cmp_sel + 1)<<8) + decoder_cfg.current_mb_offset;
		}

			
		
		for (unsigned int j(0);j<4;j++) { // sub run
			if (ref0_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[0][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[0][j][1]>>2) + (((i & 2) + (j & 2))<<2) - (j & 2);
				
				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 2);
				iset.mvd[1] -= (pic1_offset << 2);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 15;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}

					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 16 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x08; // bottom line 
	
				H264_InterpolateChroma(&iset, ref0_ptr + pic0_offset + pic1_offset, 0, (((UI_64)mvc[i].mvd[0][j][1] & 3) << 33) | (mvc[i].mvd[0][j][0] & 7));
			}

			if (ref1_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[1][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[1][j][1]>>2) + (((i & 2) + (j & 2))<<2) - (j & 2);

				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 2);
				iset.mvd[1] -= (pic1_offset << 2);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 15;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}

					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 16 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}

				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x08; // bottom line 

				H264_InterpolateChroma(&iset, ref1_ptr + pic0_offset + pic1_offset, ref0_ptr, (((UI_64)mvc[i].mvd[1][j][1] & 3) << 33) | (mvc[i].mvd[1][j][0] & 7));
			}
			
			

			if ((current_slice.weight_type == 0) || ((current_slice.weight_type == 2) && ((ref0_ptr == 0) || (ref1_ptr == 0)))) {
				H264_DefaultChromaUpdate(lev_ptr + ((j & 2)<<2) + ((j & 1)<<1));
			} else {
				// set up weights
				iset.map_flags = current_slice.gray_val[1];

				SetInterWeights(iset, mvc[i].ref_idx[0], mvc[i].ref_idx[1], cmp_sel);
				H264_WeightedChromaUpdate(lev_ptr + ((j & 2)<<2) + ((j & 1)<<1), &iset);
			}





















			if (ref0_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[0][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[0][j][1]>>2) + (((i & 2) + (j & 2))<<2) - (j & 2) + 2;
				
				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 2);
				iset.mvd[1] -= (pic1_offset << 2);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 15;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}

					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 16 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}


				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x08; // bottom line 
	
				H264_InterpolateChroma(&iset, ref0_ptr + pic0_offset + pic1_offset, 0, (((UI_64)mvc[i].mvd[0][j][1] & 3) << 33) | (mvc[i].mvd[0][j][0] & 7));
			}

			if (ref1_ptr) {
				iset.map_flags = 0;

				iset.mvd[0] = (mvc[i].mvd[1][j][0]>>3) + ((i & 1)<<2) + ((j & 1)<<1); iset.mvd[1] = (mvc[i].mvd[1][j][1]>>2) + (((i & 2) + (j & 2))<<2) - (j & 2) + 2;

				pic0_offset = (iset.mvd[0] >> 3);
				iset.mvd[0] -= (pic0_offset << 3);
				
				if ((decoder_cfg.line_ptr + pic0_offset) >= decoder_cfg.slice_width) {
					if ((decoder_cfg.line_ptr + pic0_offset) > decoder_cfg.slice_width) {
						iset.mvd[0] = 7;
						iset.map_flags |= 0x20;
					} else {
						if (iset.mvd[0] > 5) iset.map_flags |= 0x20; // right border
					}

					pic0_offset = (decoder_cfg.slice_width - decoder_cfg.line_ptr);
				} else {
					if ((decoder_cfg.line_ptr + pic0_offset) < 1) {
						iset.mvd[0] = 8 - iset.mvd[0];
						iset.map_flags |= 0x10; // left border

						pic0_offset = 1 - decoder_cfg.line_ptr;
					}					
				}

				pic0_offset <<= 10;
								
				if (iset.mvd[0] > 5) iset.map_flags |= 0x02; // right line 





				pic1_offset = (iset.mvd[1] >> 2);
				iset.mvd[1] -= (pic1_offset << 2);
				
				if ((decoder_cfg.line_counter + pic1_offset + 1) >= (__int64)current_slice.seq_set->pic_height_mapunits) {
					if ((decoder_cfg.line_counter + pic1_offset + 1) > (__int64)current_slice.seq_set->pic_height_mapunits) {
						iset.mvd[1] = 15;
						iset.map_flags |= 0x80;
					} else {
						if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x80; // bottom border
					}

					pic1_offset = (current_slice.seq_set->pic_height_mapunits - decoder_cfg.line_counter - 1);
				} else {
					if ((decoder_cfg.line_counter + pic1_offset) < 0) {
						if (pic1_offset < -1) {
							iset.mvd[1] = 2;
						} else {
							iset.mvd[1] = 16 - iset.mvd[1];
						}
						iset.map_flags |= 0x40; // top border

						pic1_offset = -decoder_cfg.line_counter;
					}
				}


				pic1_offset *= decoder_cfg.pline_offset;

				if (iset.mvd[1] > ((2<<3) - 3)) iset.map_flags |= 0x08; // bottom line 

				H264_InterpolateChroma(&iset, ref1_ptr + pic0_offset + pic1_offset, ref0_ptr, (((UI_64)mvc[i].mvd[1][j][1] & 3) << 33) | (mvc[i].mvd[1][j][0] & 7));
			}
			
			

			if ((current_slice.weight_type == 0) || ((current_slice.weight_type == 2) && ((ref0_ptr == 0) || (ref1_ptr == 0)))) {
				H264_DefaultChromaUpdate(lev_ptr + ((j & 2)<<2) + ((j & 1)<<1) + 8);
			} else {
				// set up weights
				iset.map_flags = current_slice.gray_val[1];

				SetInterWeights(iset, mvc[i].ref_idx[0], mvc[i].ref_idx[1], cmp_sel);
				H264_WeightedChromaUpdate(lev_ptr + ((j & 2)<<2) + ((j & 1)<<1) + 8, &iset);
			}						

		}
	

		lev_ptr += 32;


		if (ref0_ptr) {
			pic_list[pic0].buffer.Release();
		}

		if (ref1_ptr) {
			pic_list[pic1].buffer.Release();
		}

	}

}

int Video::H264::InterPicDist(unsigned int pic0, unsigned int pic1, unsigned int p_val) {
	int result(0);

	if (pic0 == -1) {
		result = pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & p_val];
	} else {
		pic0 = decoder_cfg.ref_pic_list[0][pic0];
		result = pic_list[pic0].id_rec.field_ocount[pic_list[pic0]._reserved & p_val];
	}


	if (pic1 == -1) {
		result -= pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & p_val];
	} else {
		pic1 = decoder_cfg.ref_pic_list[1][pic1];
		result -= pic_list[pic1].id_rec.field_ocount[pic_list[pic1]._reserved & p_val];
	}

	return result;
}

void Video::H264::SetInterWeights(InterPredSet & iset, unsigned short pic0, unsigned short pic1, unsigned int cmp_sel) {
	int td_val(0);
	
	if (current_slice.weight_type == 1) {
		// explicit
		iset.logWD = current_slice.weight_denom_bits[cmp_sel];

		if (pic0 < MAX_REF_IDX) {
	//		if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) pic0 >>= 1;
			
			iset.w1 = iset.w0 = current_slice.weight_table[pic0][0][cmp_sel].c_weight; //  << current_slice.seq_set->cmp_depth[cmp_sel];
			iset.o1 = iset.o0 = current_slice.weight_table[pic0][0][cmp_sel].c_offset << current_slice.seq_set->cmp_depth[cmp_sel];
		}

		if (pic1 < MAX_REF_IDX) {
	//		if (decoder_cfg.flag_set & FLAG_AFRO_FRAME) pic1 >>= 1;
			
			iset.w1 = current_slice.weight_table[pic1][1][cmp_sel].c_weight; //  << current_slice.seq_set->cmp_depth[cmp_sel];
			iset.o1 = current_slice.weight_table[pic1][1][cmp_sel].c_offset << current_slice.seq_set->cmp_depth[cmp_sel];

			if (pic0 >= MAX_REF_IDX) {
				iset.w0 = iset.w1;
				iset.o0 = iset.o1;
			}
		}

	} else {
		iset.logWD = 5;
		iset.o0 = iset.o1 = 0;		
		iset.w0 = iset.w1 = 32;

		if ((pic1 < MAX_REF_IDX) && (pic0 < MAX_REF_IDX)) {
			if (((pic_list[decoder_cfg.ref_pic_list[0][pic0]]._reserved | pic_list[decoder_cfg.ref_pic_list[1][pic1]]._reserved) & FLAG_LONG_REF) == 0) {
				if (td_val = InterPicDist(pic0, pic1)) {
					td_val = Clip3(-128, 127, -td_val);

					if (td_val < 0) {
						td_val = - td_val;
						td_val = -(16384 + (td_val>>1))/td_val;
					} else {
						td_val = (16384 + (td_val>>1))/td_val;
					}

					td_val = (Clip3(-1024, 1023, (td_val*Clip3(-128, 127, -InterPicDist(pic0, -1)) + 32)>>6)>>2);

					if ((td_val > -64) || (td_val < 128)) {
						iset.w0 = 64 - td_val;
						iset.w1 = td_val;
					}
				}
			}
		}
	}
}

