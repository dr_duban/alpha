/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\H264.h"
#include "System\SysUtils.h"

const unsigned int Video::H264::col_map_idx[4][4] = {	{0, 4, 0, 0},	// frame/frame frame/field frame/afro
														{1, 0, 2, 0},	// field/frame field/field fild/afro
														{0, 5, 6, 0},	// afro/frame afro/field afro/afro
														{0, 0, 0, 0} };


const unsigned int Video::H264::col_remap[4][16] = {
	0, 1, 2, 3,
	4, 5, 6, 7,
	8, 9, 10, 11,
	12, 13, 14, 15,

	0, 1, 8, 9,
	4, 5, 12, 13,
	0, 1, 8, 9,
	4, 5, 12, 13,

	0, 1, 0, 1,
	4, 5, 4, 5,
	2, 3, 2, 3,
	6, 7, 6, 7,

	8, 9, 8, 9,
	12, 13, 12, 13,
	10, 11, 10, 11,
	14, 15, 14, 15
	

};



void Video::H264::BSubPredCalc(MacroBlockSet::SubSet & mvc, const InterPredCfg * ip_cfg, unsigned int ip_idx, unsigned int list_idx, unsigned int p_idx) {
	mvc.ref_idx[list_idx] = decoder_cfg.mb_x->sub[p_idx].ref_idx[list_idx];
			
	if (decoder_cfg.mb_x->field_decoding_flag == ip_cfg[ip_idx].mbi->field_decoding_flag) {
		if (mvc.ref_idx[list_idx] == reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].ref_idx[list_idx]) {
			mvc.mvd[list_idx][0][0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].mvd[list_idx][ip_cfg[ip_idx].sub_idx][0];
			mvc.mvd[list_idx][0][1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].mvd[list_idx][ip_cfg[ip_idx].sub_idx][1];
		} else {				
			MVMedianPred(mvc, ip_cfg, list_idx, 0);
		}
	} else {
		if (mvc.ref_idx[list_idx] == FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].ref_idx[list_idx], ip_cfg[ip_idx].mbi->field_decoding_flag)) {
			mvc.mvd[list_idx][0][0] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].mvd[list_idx][ip_cfg[ip_idx].sub_idx][0], ip_cfg[ip_idx].mbi->field_decoding_flag);
			mvc.mvd[list_idx][0][1] = FormatMbaffVal(reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[ip_idx].mbi->x_tent)[ip_cfg[ip_idx].part_idx].mvd[list_idx][ip_cfg[ip_idx].sub_idx][1], ip_cfg[ip_idx].mbi->field_decoding_flag);
		} else {
			MVMedianPred(mvc, ip_cfg, list_idx, 0);
		}
	}

	mvc.mvd[list_idx][0][0] += decoder_cfg.mb_x->sub[p_idx].mvd[list_idx][0][0];
	mvc.mvd[list_idx][0][1] += decoder_cfg.mb_x->sub[p_idx].mvd[list_idx][0][1];
			
	mvc.mvd[list_idx][1][0] = mvc.mvd[list_idx][0][0];
	mvc.mvd[list_idx][1][1] = mvc.mvd[list_idx][0][1];
	mvc.mvd[list_idx][2][0] = mvc.mvd[list_idx][0][0];
	mvc.mvd[list_idx][2][1] = mvc.mvd[list_idx][0][1];
	mvc.mvd[list_idx][3][0] = mvc.mvd[list_idx][0][0];
	mvc.mvd[list_idx][3][1] = mvc.mvd[list_idx][0][1];


}


void Video::H264::MVPredB() {
	InterPredCfg ip_cfg[4];
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);
	
	
	switch (decoder_cfg.mb_x->flag_type & (FLAG_MB_PART_V | FLAG_MB_PART_H | FLAG_MB_TYPE_0)) {
		case FLAG_MB_PART_V:
// part 0
			FormatICfg(ip_cfg, 0, 5);

			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0) BSubPredCalc(mvc[0], ip_cfg, 1, 0, 0);
			else mvc[0].ref_idx[0] = 0xFFFF;
			
			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1) BSubPredCalc(mvc[0], ip_cfg, 1, 1, 0);
			else mvc[0].ref_idx[1] = 0xFFFF;

			mvc[1] = mvc[0];


// part 1
			FormatICfg(ip_cfg, 8, 13);

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L0) BSubPredCalc(mvc[2], ip_cfg, 0, 0, 2);
			else mvc[2].ref_idx[0] = 0xFFFF;

			if (decoder_cfg.mb_x->sub[2].type_flag & VAL_SUB_PMODE_L1) BSubPredCalc(mvc[2], ip_cfg, 0, 1, 2);
			else mvc[2].ref_idx[1] = 0xFFFF;

			mvc[3] = mvc[2];


		break;
		case FLAG_MB_PART_H:
// part 0
			FormatICfg(ip_cfg, 0, 1);

			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0) BSubPredCalc(mvc[0], ip_cfg, 0, 0, 0);
			else mvc[0].ref_idx[0] = 0xFFFF;

			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1) BSubPredCalc(mvc[0], ip_cfg, 0, 1, 0);
			else mvc[0].ref_idx[1] = 0xFFFF;

			mvc[2] = mvc[0];

// part 1
			FormatICfg(ip_cfg, 4, 5);

			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L0) BSubPredCalc(mvc[1], ip_cfg, 2, 0, 1);
			else mvc[1].ref_idx[0] = 0xFFFF;

			if (decoder_cfg.mb_x->sub[1].type_flag & VAL_SUB_PMODE_L1) BSubPredCalc(mvc[1], ip_cfg, 2, 1, 1);
			else mvc[1].ref_idx[1] = 0xFFFF;

			mvc[3] = mvc[1];

		break;
		case 0:

			FormatICfg(ip_cfg, 0, 5);

			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L0) {
				mvc[0].ref_idx[0] = decoder_cfg.mb_x->sub[0].ref_idx[0];

				MVMedianPred(mvc[0], ip_cfg, 0, 0);

				mvc[0].mvd[0][0][0] += decoder_cfg.mb_x->sub[0].mvd[0][0][0];
				mvc[0].mvd[0][0][1] += decoder_cfg.mb_x->sub[0].mvd[0][0][1];
			
				mvc[0].mvd[0][1][0] = mvc[0].mvd[0][0][0];
				mvc[0].mvd[0][1][1] = mvc[0].mvd[0][0][1];
				mvc[0].mvd[0][2][0] = mvc[0].mvd[0][0][0];
				mvc[0].mvd[0][2][1] = mvc[0].mvd[0][0][1];
				mvc[0].mvd[0][3][0] = mvc[0].mvd[0][0][0];
				mvc[0].mvd[0][3][1] = mvc[0].mvd[0][0][1];
			} else {
				mvc[0].ref_idx[0] = 0xFFFF;
			}

			if (decoder_cfg.mb_x->sub[0].type_flag & VAL_SUB_PMODE_L1) {
				mvc[0].ref_idx[1] = decoder_cfg.mb_x->sub[0].ref_idx[1];

				MVMedianPred(mvc[0], ip_cfg, 1, 0);

				mvc[0].mvd[1][0][0] += decoder_cfg.mb_x->sub[0].mvd[1][0][0];
				mvc[0].mvd[1][0][1] += decoder_cfg.mb_x->sub[0].mvd[1][0][1];
			
				mvc[0].mvd[1][1][0] = mvc[0].mvd[1][0][0];
				mvc[0].mvd[1][1][1] = mvc[0].mvd[1][0][1];
				mvc[0].mvd[1][2][0] = mvc[0].mvd[1][0][0];
				mvc[0].mvd[1][2][1] = mvc[0].mvd[1][0][1];
				mvc[0].mvd[1][3][0] = mvc[0].mvd[1][0][0];
				mvc[0].mvd[1][3][1] = mvc[0].mvd[1][0][1];
			} else {
				mvc[0].ref_idx[1] = 0xFFFF;
			}


			mvc[3] = mvc[2] = mvc[1] = mvc[0];

		break;
		case FLAG_MB_TYPE_0:
			SkipPredB();

		break;
		case (FLAG_MB_PART_V | FLAG_MB_PART_H): // B_8x8
			MVPredSubB();

		break;

	}

}


void Video::H264::MVPredSubB() {
	InterPredCfg ip_cfg[4];
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent);

	for (unsigned int i(0);i<4;i++) {
		if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_BI) {
			if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L0) {
				mvc[i].ref_idx[0] = decoder_cfg.mb_x->sub[i].ref_idx[0];

				switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
					case FLAG_SUB_PART_V:

						FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 0, 0);
						mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
						mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];
						mvc[i].mvd[0][1][0] = mvc[i].mvd[0][0][0];
						mvc[i].mvd[0][1][1] = mvc[i].mvd[0][0][1];

						FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 3);
						MVMedianPred(mvc[i], ip_cfg, 0, 2);
						mvc[i].mvd[0][2][0] += decoder_cfg.mb_x->sub[i].mvd[0][2][0];
						mvc[i].mvd[0][2][1] += decoder_cfg.mb_x->sub[i].mvd[0][2][1];
						mvc[i].mvd[0][3][0] = mvc[i].mvd[0][2][0];
						mvc[i].mvd[0][3][1] = mvc[i].mvd[0][2][1];

					break;
					case FLAG_SUB_PART_H:
						FormatICfg(ip_cfg, (i<<2), (i<<2));
						MVMedianPred(mvc[i], ip_cfg, 0, 0);
						mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
						mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];				
						mvc[i].mvd[0][2][0] = mvc[i].mvd[0][0][0];
						mvc[i].mvd[0][2][1] = mvc[i].mvd[0][0][1];

						FormatICfg(ip_cfg, (i<<2)+1, (i<<2)+1);
						MVMedianPred(mvc[i], ip_cfg, 0, 1);
						mvc[i].mvd[0][1][0] += decoder_cfg.mb_x->sub[i].mvd[0][1][0];
						mvc[i].mvd[0][1][1] += decoder_cfg.mb_x->sub[i].mvd[0][1][1];
						mvc[i].mvd[0][3][0] = mvc[i].mvd[0][1][0];
						mvc[i].mvd[0][3][1] = mvc[i].mvd[0][1][1];

					break;
					case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
						FormatICfg(ip_cfg, (i<<2), (i<<2));
						MVMedianPred(mvc[i], ip_cfg, 0, 0);
						mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
						mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];
						FormatICfg(ip_cfg, (i<<2) + 1, (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 0, 1);
						mvc[i].mvd[0][1][0] += decoder_cfg.mb_x->sub[i].mvd[0][1][0];
						mvc[i].mvd[0][1][1] += decoder_cfg.mb_x->sub[i].mvd[0][1][1];
						FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 2);
						MVMedianPred(mvc[i], ip_cfg, 0, 2);
						mvc[i].mvd[0][2][0] += decoder_cfg.mb_x->sub[i].mvd[0][2][0];
						mvc[i].mvd[0][2][1] += decoder_cfg.mb_x->sub[i].mvd[0][2][1];
						FormatICfg(ip_cfg, (i<<2) + 3, (i<<2) + 3);
						MVMedianPred(mvc[i], ip_cfg, 0, 3);
						mvc[i].mvd[0][3][0] += decoder_cfg.mb_x->sub[i].mvd[0][3][0];
						mvc[i].mvd[0][3][1] += decoder_cfg.mb_x->sub[i].mvd[0][3][1];

					break;
					case 0:
						FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 0, 0);

						mvc[i].mvd[0][0][0] += decoder_cfg.mb_x->sub[i].mvd[0][0][0];
						mvc[i].mvd[0][0][1] += decoder_cfg.mb_x->sub[i].mvd[0][0][1];

						mvc[i].mvd[0][1][0] = mvc[i].mvd[0][0][0];
						mvc[i].mvd[0][1][1] = mvc[i].mvd[0][0][1];
						mvc[i].mvd[0][2][0] = mvc[i].mvd[0][0][0];
						mvc[i].mvd[0][2][1] = mvc[i].mvd[0][0][1];
						mvc[i].mvd[0][3][0] = mvc[i].mvd[0][0][0];
						mvc[i].mvd[0][3][1] = mvc[i].mvd[0][0][1];

					break;

				}
			} else {
				mvc[i].ref_idx[0] = 0xFFFF;
			}


			if (decoder_cfg.mb_x->sub[i].type_flag & VAL_SUB_PMODE_L1) {
				mvc[i].ref_idx[1] = decoder_cfg.mb_x->sub[i].ref_idx[1];

				switch (decoder_cfg.mb_x->sub[i].type_flag & (FLAG_SUB_PART_V | FLAG_SUB_PART_H)) {
					case FLAG_SUB_PART_V:

						FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 1, 0);
						mvc[i].mvd[1][0][0] += decoder_cfg.mb_x->sub[i].mvd[1][0][0];
						mvc[i].mvd[1][0][1] += decoder_cfg.mb_x->sub[i].mvd[1][0][1];
						mvc[i].mvd[1][1][0] = mvc[i].mvd[1][0][0];
						mvc[i].mvd[1][1][1] = mvc[i].mvd[1][0][1];

						FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 3);
						MVMedianPred(mvc[i], ip_cfg, 1, 2);
						mvc[i].mvd[1][2][0] += decoder_cfg.mb_x->sub[i].mvd[1][2][0];
						mvc[i].mvd[1][2][1] += decoder_cfg.mb_x->sub[i].mvd[1][2][1];
						mvc[i].mvd[1][3][0] = mvc[i].mvd[1][2][0];
						mvc[i].mvd[1][3][1] = mvc[i].mvd[1][2][1];

					break;
					case FLAG_SUB_PART_H:
						FormatICfg(ip_cfg, (i<<2), (i<<2));
						MVMedianPred(mvc[i], ip_cfg, 1, 0);
						mvc[i].mvd[1][0][0] += decoder_cfg.mb_x->sub[i].mvd[1][0][0];
						mvc[i].mvd[1][0][1] += decoder_cfg.mb_x->sub[i].mvd[1][0][1];				
						mvc[i].mvd[1][2][0] = mvc[i].mvd[1][0][0];
						mvc[i].mvd[1][2][1] = mvc[i].mvd[1][0][1];

						FormatICfg(ip_cfg, (i<<2)+1, (i<<2)+1);
						MVMedianPred(mvc[i], ip_cfg, 1, 1);
						mvc[i].mvd[1][1][0] += decoder_cfg.mb_x->sub[i].mvd[1][1][0];
						mvc[i].mvd[1][1][1] += decoder_cfg.mb_x->sub[i].mvd[1][1][1];
						mvc[i].mvd[1][3][0] = mvc[i].mvd[1][1][0];
						mvc[i].mvd[1][3][1] = mvc[i].mvd[1][1][1];

					break;
					case (FLAG_SUB_PART_V | FLAG_SUB_PART_H):
						FormatICfg(ip_cfg, (i<<2), (i<<2));
						MVMedianPred(mvc[i], ip_cfg, 1, 0);
						mvc[i].mvd[1][0][0] += decoder_cfg.mb_x->sub[i].mvd[1][0][0];
						mvc[i].mvd[1][0][1] += decoder_cfg.mb_x->sub[i].mvd[1][0][1];
						FormatICfg(ip_cfg, (i<<2) + 1, (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 1, 1);
						mvc[i].mvd[1][1][0] += decoder_cfg.mb_x->sub[i].mvd[1][1][0];
						mvc[i].mvd[1][1][1] += decoder_cfg.mb_x->sub[i].mvd[1][1][1];
						FormatICfg(ip_cfg, (i<<2) + 2, (i<<2) + 2);
						MVMedianPred(mvc[i], ip_cfg, 1, 2);
						mvc[i].mvd[1][2][0] += decoder_cfg.mb_x->sub[i].mvd[1][2][0];
						mvc[i].mvd[1][2][1] += decoder_cfg.mb_x->sub[i].mvd[1][2][1];
						FormatICfg(ip_cfg, (i<<2) + 3, (i<<2) + 3);
						MVMedianPred(mvc[i], ip_cfg, 1, 3);
						mvc[i].mvd[1][3][0] += decoder_cfg.mb_x->sub[i].mvd[1][3][0];
						mvc[i].mvd[1][3][1] += decoder_cfg.mb_x->sub[i].mvd[1][3][1];

					break;
					case 0:
						FormatICfg(ip_cfg, (i<<2), (i<<2) + 1);
						MVMedianPred(mvc[i], ip_cfg, 1, 0);

						mvc[i].mvd[1][0][0] += decoder_cfg.mb_x->sub[i].mvd[1][0][0];
						mvc[i].mvd[1][0][1] += decoder_cfg.mb_x->sub[i].mvd[1][0][1];

						mvc[i].mvd[1][1][0] = mvc[i].mvd[1][0][0];
						mvc[i].mvd[1][1][1] = mvc[i].mvd[1][0][1];
						mvc[i].mvd[1][2][0] = mvc[i].mvd[1][0][0];
						mvc[i].mvd[1][2][1] = mvc[i].mvd[1][0][1];
						mvc[i].mvd[1][3][0] = mvc[i].mvd[1][0][0];
						mvc[i].mvd[1][3][1] = mvc[i].mvd[1][0][1];

					break;

				}
			} else {
				mvc[i].ref_idx[1] = 0xFFFF;
			}

		} else {
			MVPredB8x8(i);
		}
	}
}



unsigned int Video::H264::SkipPredB() { // skip, direct_16x16
	const short * co_ptr(0);
	InterPredCfg ip_cfg[4];
	UI_64 col_offset(0), col_adjust(0);
	const MacroBlockSet::SubSet * mvcol(0);
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent), temp_mbss;
	int abs_dPOC[2] = {0, 0};
	unsigned int c_map_selector(0), sign_val(0);

	unsigned short col_id = decoder_cfg.ref_pic_list[1][0];

	if (pic_list[col_id]._reserved & (FLAG_PAIR_REF | FLAG_AFRO_REF)) {
		abs_dPOC[0] = pic_list[col_id].id_rec.field_ocount[0] - pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];
		if (abs_dPOC[0] < 0) abs_dPOC[0] = -abs_dPOC[0];
		abs_dPOC[1] = pic_list[col_id].id_rec.field_ocount[1] - pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];
		if (abs_dPOC[1] < 0) abs_dPOC[1] = -abs_dPOC[1];
			
	}

	c_map_selector = col_map_idx[((pic_list[decoder_cfg.current_frame_ptr]._reserved >> 29) - 1) & 3][((pic_list[col_id]._reserved >> 29) - 1) & 3];
	
	co_ptr = reinterpret_cast<short *>(pic_list[col_id].buffer.Acquire());
	if (pic_list[col_id]._reserved & FLAG_SECOND_FIELD) co_ptr += vui_params.second_field_offset;

	if (co_ptr) {
		col_offset = decoder_cfg.current_mb_offset >> 10;

		switch (c_map_selector) {
			case 1: // field2frame
				col_offset = (decoder_cfg.line_counter<<1)*(vui_params.c_width + 1) + decoder_cfg.line_ptr - 1;
				col_adjust = (vui_params.c_width + 1);
				c_map_selector = 1;
			break;
			case 2: // field2afro
				col_offset <<= 1;
				c_map_selector = 0;

				if (reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) {
					col_offset += (pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD);
				} else {
					col_adjust = 1;
					c_map_selector = 1;
				}

			break;
			case 4: // frame2field
				col_offset = (decoder_cfg.line_counter>>1)*(vui_params.c_width + 1) + decoder_cfg.line_ptr - 1;
				c_map_selector = 2 + (decoder_cfg.line_counter & 1);
			break;
			case 5: // afro2field
				c_map_selector = 0;

				if ((decoder_cfg.mb_x->field_decoding_flag & 1) == 0) {
					c_map_selector = 2 + ((current_slice.first_mb | decoder_cfg.current_mb_ptr) & 1);
				}

				col_offset >>= 1;
			break;
			case 6: // afro2afro
				c_map_selector = 0;

				if (decoder_cfg.mb_x->field_decoding_flag & 1) {
					if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) == 0) {						
						col_offset &= ~1;

						col_adjust = 1;
						c_map_selector = 1;
					}
				} else {
					if (reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) {
						col_offset &= ~1;
						if (abs_dPOC[0] >= abs_dPOC[1]) col_offset |= 1;

						c_map_selector = 2 + ((current_slice.first_mb | decoder_cfg.current_mb_ptr) & 1);
					}
				}								
				
			break;

		}

		col_offset <<= 10;
		col_adjust <<= 10;
		
		if (current_slice.seq_set->direct_8x8_inference_flag) {
			for (unsigned int i(0);i<4;i++) {
				if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(i>>1))->flag_type & FLAG_MB_TYPE_I) == 0) {
					mvcol = reinterpret_cast<const MacroBlockSet::SubSet *>(reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(i>>1))->x_tent);

					sign_val = col_remap[c_map_selector][(i<<2) + i];

					if (mvcol[sign_val >> 2].ref_idx[0] != 0xFFFF) {
						mvc[i].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[0];
						mvc[i].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

						mvc[i].mvd[0][0][0] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][0];
						mvc[i].mvd[0][0][1] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][1];

					} else {
						if (mvcol[sign_val >> 2].ref_idx[1] != 0xFFFF) {
							mvc[i].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[1];
							mvc[i].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

							mvc[i].mvd[0][0][0] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][0];
							mvc[i].mvd[0][0][1] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][1];
						}
					}

				} else {
					mvc[i].ref_idx[0] = mvc[i].ref_idx[1] = 0xFFFF;
					mvc[i].mvd[0][0][0] = mvc[i].mvd[0][0][1] = 0;					
				}

				
				mvc[i].mvd[0][1][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][1][1] = mvc[i].mvd[0][0][1];
				mvc[i].mvd[0][2][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][2][1] = mvc[i].mvd[0][0][1];
				mvc[i].mvd[0][3][0] = mvc[i].mvd[0][0][0];
				mvc[i].mvd[0][3][1] = mvc[i].mvd[0][0][1];
			}

		} else {
			for (unsigned int i(0);i<4;i++) {
				mvcol = reinterpret_cast<const MacroBlockSet::SubSet *>(reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(i>>1))->x_tent);
				if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(i>>1))->flag_type & FLAG_MB_TYPE_I) == 0) {
					for (unsigned int j(0);j<4;j++) {
					
						sign_val = col_remap[c_map_selector][(i<<2) + j];

						if (mvcol[sign_val >> 2].ref_idx[0] != 0xFFFF) {
							mvc[i].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[0];
							mvc[i].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

							mvc[i].mvd[0][j][0] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][0];
							mvc[i].mvd[0][j][1] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][1];

						} else {
							if (mvcol[sign_val >> 2].ref_idx[1] != 0xFFFF) {
								mvc[i].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[1];
								mvc[i].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

								mvc[i].mvd[0][j][0] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][0];
								mvc[i].mvd[0][j][1] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][1];
							}
						}

					}
				} else {
					mvc[i].ref_idx[0] = mvc[i].ref_idx[1] = 0xFFFF;
					mvc[i].mvd[0][0][0] = mvc[i].mvd[0][0][1] = 0;
					mvc[i].mvd[0][1][0] = mvc[i].mvd[0][1][1] = 0;
					mvc[i].mvd[0][2][0] = mvc[i].mvd[0][2][1] = 0;
					mvc[i].mvd[0][3][0] = mvc[i].mvd[0][3][1] = 0;
				}
				
			}
		}

		pic_list[col_id].buffer.Release();

		// process

		if (current_slice.direct_spatial_mv_pred_flag) {
			FormatICfg(ip_cfg, 0, 5);
			
			temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0];
			temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[1];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0] < temp_mbss.ref_idx[0]) temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[1] < temp_mbss.ref_idx[1]) temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[1];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0] < temp_mbss.ref_idx[0]) temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[1] < temp_mbss.ref_idx[1]) temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[1];
			
			if ((temp_mbss.ref_idx[0] & temp_mbss.ref_idx[1]) == 0xFFFF) {
				mvc[0].ref_idx[0] = mvc[0].ref_idx[1] = 0;
				mvc[0].mvd[0][0][0] = mvc[0].mvd[0][0][1] = 0;
				mvc[0].mvd[0][1][0] = mvc[0].mvd[0][1][1] = 0;
				mvc[0].mvd[0][2][0] = mvc[0].mvd[0][2][1] = 0;
				mvc[0].mvd[0][3][0] = mvc[0].mvd[0][3][1] = 0;

				mvc[1].ref_idx[0] = mvc[1].ref_idx[1] = 0;
				mvc[1].mvd[0][0][0] = mvc[1].mvd[0][0][1] = 0;
				mvc[1].mvd[0][1][0] = mvc[1].mvd[0][1][1] = 0;
				mvc[1].mvd[0][2][0] = mvc[1].mvd[0][2][1] = 0;
				mvc[1].mvd[0][3][0] = mvc[1].mvd[0][3][1] = 0;

				mvc[2].ref_idx[0] = mvc[2].ref_idx[1] = 0;
				mvc[2].mvd[0][0][0] = mvc[2].mvd[0][0][1] = 0;
				mvc[2].mvd[0][1][0] = mvc[2].mvd[0][1][1] = 0;
				mvc[2].mvd[0][2][0] = mvc[2].mvd[0][2][1] = 0;
				mvc[2].mvd[0][3][0] = mvc[2].mvd[0][3][1] = 0;

				mvc[3].ref_idx[0] = mvc[3].ref_idx[1] = 0;
				mvc[3].mvd[0][0][0] = mvc[3].mvd[0][0][1] = 0;
				mvc[3].mvd[0][1][0] = mvc[3].mvd[0][1][1] = 0;
				mvc[3].mvd[0][2][0] = mvc[3].mvd[0][2][1] = 0;
				mvc[3].mvd[0][3][0] = mvc[3].mvd[0][3][1] = 0;

			} else {
				MVMedianPred(temp_mbss, ip_cfg, 0, 0);
				MVMedianPred(temp_mbss, ip_cfg, 1, 0);


				for (unsigned int i(0);i<4;i++) {

					abs_dPOC[0] = pic_list[col_id]._reserved & FLAG_LONG_REF;				
					if (mvc[i].ref_idx[0]) abs_dPOC[0] |= 0x2000; // colocated
					abs_dPOC[1] = abs_dPOC[0];


					mvc[i].ref_idx[1] = temp_mbss.ref_idx[1];
					if (mvc[i].ref_idx[1] != 0xFFFF) {
						for (unsigned int j(0);j<4;j++) {
							abs_dPOC[0] = abs_dPOC[1];
							if (((mvc[i].mvd[0][j][0] > 1) || (mvc[i].mvd[0][j][0] < -1)) || ((mvc[i].mvd[0][j][1] > 1) || (mvc[i].mvd[0][j][1] < -1))) abs_dPOC[0] |= 0x4000;

							if (mvc[i].ref_idx[1] != abs_dPOC[0]) {
								mvc[i].mvd[1][j][0] = temp_mbss.mvd[1][0][0];
								mvc[i].mvd[1][j][1] = temp_mbss.mvd[1][0][1];
							}
						}
					}



					mvc[i].ref_idx[0] = temp_mbss.ref_idx[0];
					if (mvc[i].ref_idx[0] != 0xFFFF) {
						for (unsigned int j(0);j<4;j++) {
							abs_dPOC[0] = abs_dPOC[1];
							if (((mvc[i].mvd[0][j][0] > 1) || (mvc[i].mvd[0][j][0] < -1)) || ((mvc[i].mvd[0][j][1] > 1) || (mvc[i].mvd[0][j][1] < -1))) abs_dPOC[0] |= 0x4000; // colocated

							if (mvc[i].ref_idx[0] == abs_dPOC[0]) {
								mvc[i].mvd[0][j][0] = mvc[i].mvd[0][j][1] = 0;
							} else {
								mvc[i].mvd[0][j][0] = temp_mbss.mvd[0][0][0];
								mvc[i].mvd[0][j][1] = temp_mbss.mvd[0][0][1];
							}
						}
					} else {
						mvc[i].mvd[0][0][0] = mvc[i].mvd[0][0][1] = 0;
						mvc[i].mvd[0][1][0] = mvc[i].mvd[0][1][1] = 0;
						mvc[i].mvd[0][2][0] = mvc[i].mvd[0][2][1] = 0;
						mvc[i].mvd[0][3][0] = mvc[i].mvd[0][3][1] = 0;
					}				
				}
			}

		} else { // temporal			


			for (unsigned int i(0);i<4;i++) {
				if (mvc[i].ref_idx[0] == 0xFFFF) mvc[i].ref_idx[0] = 0;
				else {
					mvc[i].ref_idx[0] = 0;
					for (unsigned int k(0);k<=current_slice.num_ref_idx_active[0];k++) {
						if (decoder_cfg.ref_pic_list[0][k] == mvc[i].ref_idx[1]) {
							mvc[i].ref_idx[0] = k;
							break;
						}
					}
				}

				mvc[i].ref_idx[1] = 0;


				switch (c_map_selector) {
					case 1: // frame2filed
						for (unsigned int j(0);j<4;j++) {
							mvc[i].mvd[0][j][0] /= 2;
							mvc[i].mvd[0][j][1] /= 2;						
						}	
					break;
					case 2: case 3: // field2frame
						for (unsigned int j(0);j<4;j++) {
							mvc[i].mvd[0][j][0] <<= 1;
							mvc[i].mvd[0][j][1] <<= 1;
						}
				}



				abs_dPOC[0] = -InterPicDist(mvc[i].ref_idx[0], 0, pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD);

				if ((pic_list[decoder_cfg.ref_pic_list[0][mvc[i].ref_idx[0]]]._reserved & FLAG_LONG_REF) || (abs_dPOC[0] == 0)) {


				} else {
					abs_dPOC[0] = Clip3(-128, 127, abs_dPOC[0]);
					
					abs_dPOC[1] = abs_dPOC[0];
					if (abs_dPOC[0] < 0) abs_dPOC[0] = -abs_dPOC[0];


					abs_dPOC[0] = ((abs_dPOC[0]>>1) + 16384)/abs_dPOC[1];
										
					abs_dPOC[1] = Clip3(-128, 127, -InterPicDist(mvc[i].ref_idx[0], -1, pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD));

					abs_dPOC[0] = abs_dPOC[1] = Clip3(-1024, 1023, (abs_dPOC[0]*abs_dPOC[1] + 32) >> 6);

					for (unsigned int j(0);j<4;j++) {
						mvc[i].mvd[1][j][0] = -mvc[i].mvd[0][j][0];
						mvc[i].mvd[1][j][1] = -mvc[i].mvd[0][j][1];

						mvc[i].mvd[0][j][0] = ((abs_dPOC[0]*mvc[i].mvd[0][j][0] + 128) >> 8);
						mvc[i].mvd[0][j][1] = ((abs_dPOC[1]*mvc[i].mvd[0][j][1] + 128) >> 8);

						mvc[i].mvd[1][j][0] += mvc[i].mvd[0][j][0];
						mvc[i].mvd[1][j][1] += mvc[i].mvd[0][j][1];

					}
				}
			}
		}
	}


	return 0;
}





void Video::H264::MVPredB8x8(unsigned int p_idx) {
	const short * co_ptr(0);
	InterPredCfg ip_cfg[4];
	UI_64 col_offset(0), col_adjust(0);
	const MacroBlockSet::SubSet * mvcol(0);
	MacroBlockSet::SubSet * mvc = reinterpret_cast<MacroBlockSet::SubSet *>(decoder_cfg.mb_x->x_tent), temp_mbss;
	int abs_dPOC[2] = {0, 0};
	unsigned int c_map_selector(0), sign_val(0);

	unsigned short col_id = decoder_cfg.ref_pic_list[1][0];

	if (pic_list[col_id]._reserved & (FLAG_PAIR_REF | FLAG_AFRO_REF)) {
		abs_dPOC[0] = pic_list[col_id].id_rec.field_ocount[0] - pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];
		if (abs_dPOC[0] < 0) abs_dPOC[0] = -abs_dPOC[0];
		abs_dPOC[1] = pic_list[col_id].id_rec.field_ocount[1] - pic_list[decoder_cfg.current_frame_ptr].id_rec.field_ocount[pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD];
		if (abs_dPOC[1] < 0) abs_dPOC[1] = -abs_dPOC[1];
			
	}

	c_map_selector = col_map_idx[((pic_list[decoder_cfg.current_frame_ptr]._reserved >> 29) - 1) & 3][((pic_list[col_id]._reserved >> 29) - 1) & 3];
		
	co_ptr = reinterpret_cast<short *>(pic_list[col_id].buffer.Acquire());

	if (co_ptr) {
		col_offset = decoder_cfg.current_mb_offset >> 10;

		switch (c_map_selector) {
			case 1: // field2frame
				col_offset = (decoder_cfg.line_counter<<1)*(vui_params.c_width + 1) + decoder_cfg.line_ptr - 1;
				col_adjust = (vui_params.c_width + 1);
				c_map_selector = 1;
			break;
			case 2: // field2afro
				col_offset <<= 1;
				c_map_selector = 0;

				if (reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) {
					col_offset += (pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD);
				} else {
					col_adjust = 1;
					c_map_selector = 1;
				}

			break;
			case 4: // frame2field
				col_offset = (decoder_cfg.line_counter>>1)*(vui_params.c_width + 1) + decoder_cfg.line_ptr - 1;
				c_map_selector = 2 + (decoder_cfg.line_counter & 1);
			break;
			case 5: // afro2field
				c_map_selector = 0;

				if ((decoder_cfg.mb_x->field_decoding_flag & 1) == 0) {
					c_map_selector = 2 + ((current_slice.first_mb | decoder_cfg.current_mb_ptr) & 1);
				}

				col_offset >>= 1;
			break;
			case 6: // afro2afro
				c_map_selector = 0;

				if (decoder_cfg.mb_x->field_decoding_flag & 1) {
					if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) == 0) {						
						col_offset &= ~1;

						col_adjust = 1;
						c_map_selector = 1;
					}
				} else {
					if (reinterpret_cast<const MacroBlockSet *>(co_ptr + (col_offset<<10))->field_decoding_flag & 1) {
						col_offset &= ~1;
						if (abs_dPOC[0] >= abs_dPOC[1]) col_offset |= 1;

						c_map_selector = 2 + ((current_slice.first_mb | decoder_cfg.current_mb_ptr) & 1);
					}
				}								
				
			break;

		}

		col_offset <<= 10;
		col_adjust <<= 10;
		
		if (current_slice.seq_set->direct_8x8_inference_flag) {
			if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(p_idx>>1))->flag_type & FLAG_MB_TYPE_I) == 0) {
				mvcol = reinterpret_cast<const MacroBlockSet::SubSet *>(reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(p_idx>>1))->x_tent);
				sign_val = col_remap[c_map_selector][(p_idx<<2) + p_idx];

				if (mvcol[sign_val>>2].ref_idx[0] != 0xFFFF) {
					mvc[p_idx].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[0];
					mvc[p_idx].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

					mvc[p_idx].mvd[0][0][0] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][0];
					mvc[p_idx].mvd[0][0][1] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][1];

				} else {
					if (mvcol[sign_val>>2].ref_idx[1] != 0xFFFF) {
						mvc[p_idx].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[1];
						mvc[p_idx].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

						mvc[p_idx].mvd[0][0][0] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][0];
						mvc[p_idx].mvd[0][0][1] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][1];
					}
				}

			} else {
				mvc[p_idx].ref_idx[0] = mvc[p_idx].ref_idx[1] = 0xFFFF;
				mvc[p_idx].mvd[0][0][0] = mvc[p_idx].mvd[0][0][1] = 0;
				mvc[p_idx].mvd[1][0][0] = mvc[p_idx].mvd[1][0][1] = 0;
			}

			
			mvc[p_idx].mvd[0][1][0] = mvc[p_idx].mvd[0][0][0];
			mvc[p_idx].mvd[0][1][1] = mvc[p_idx].mvd[0][0][1];
			mvc[p_idx].mvd[0][2][0] = mvc[p_idx].mvd[0][0][0];
			mvc[p_idx].mvd[0][2][1] = mvc[p_idx].mvd[0][0][1];
			mvc[p_idx].mvd[0][3][0] = mvc[p_idx].mvd[0][0][0];
			mvc[p_idx].mvd[0][3][1] = mvc[p_idx].mvd[0][0][1];
			

		} else {
			if ((reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(p_idx>>1))->flag_type & FLAG_MB_TYPE_I) == 0) {
				mvcol = reinterpret_cast<const MacroBlockSet::SubSet *>(reinterpret_cast<const MacroBlockSet *>(co_ptr + col_offset + col_adjust*(p_idx>>1))->x_tent);

				for (unsigned int j(0);j<4;j++) {				
					
					sign_val = col_remap[c_map_selector][(p_idx<<2) + j];

					if (mvcol[sign_val>>2].ref_idx[0] != 0xFFFF) {							
						mvc[p_idx].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[0];
						mvc[p_idx].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

						mvc[p_idx].mvd[0][j][0] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][0];
						mvc[p_idx].mvd[0][j][1] = mvcol[sign_val >> 2].mvd[0][sign_val & 3][1];

					} else {
						if (mvcol[sign_val>>2].ref_idx[1] != -1) {
							mvc[p_idx].ref_idx[0] = mvcol[sign_val >> 2].ref_idx[1];
							mvc[p_idx].ref_idx[1] = mvcol[sign_val >> 2].type_flag;

							mvc[p_idx].mvd[0][j][0] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][0];
							mvc[p_idx].mvd[0][j][1] = mvcol[sign_val >> 2].mvd[1][sign_val & 3][1];
						}

					}

				}
			} else {
				mvc[p_idx].ref_idx[0] = mvc[p_idx].ref_idx[1] = 0xFFFF;
				
				mvc[p_idx].mvd[0][0][0] = mvc[p_idx].mvd[0][0][1] = 0;
				mvc[p_idx].mvd[0][1][0] = mvc[p_idx].mvd[0][1][1] = 0;
				mvc[p_idx].mvd[0][2][0] = mvc[p_idx].mvd[0][2][1] = 0;
				mvc[p_idx].mvd[0][3][0] = mvc[p_idx].mvd[0][3][1] = 0;
				
			}
		}

		pic_list[col_id].buffer.Release();


// process

		if (current_slice.direct_spatial_mv_pred_flag) {
			FormatICfg(ip_cfg, 0, 5);
			
			temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[0];
			temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[0].mbi->x_tent)[ip_cfg[0].part_idx].ref_idx[1];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0] < temp_mbss.ref_idx[0]) temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[0];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[1] < temp_mbss.ref_idx[1]) temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[1].mbi->x_tent)[ip_cfg[1].part_idx].ref_idx[1];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0] < temp_mbss.ref_idx[0]) temp_mbss.ref_idx[0] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[0];
			if (reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[1] < temp_mbss.ref_idx[1]) temp_mbss.ref_idx[1] = reinterpret_cast<const MacroBlockSet::SubSet *>(ip_cfg[2].mbi->x_tent)[ip_cfg[2].part_idx].ref_idx[1];
			
			if ((temp_mbss.ref_idx[0] & temp_mbss.ref_idx[1]) == 0xFFFF) {
				mvc[p_idx].ref_idx[0] = mvc[p_idx].ref_idx[1] = 0;

				mvc[p_idx].mvd[0][0][0] = mvc[p_idx].mvd[0][0][1] = 0;
				mvc[p_idx].mvd[0][1][0] = mvc[p_idx].mvd[0][1][1] = 0;
				mvc[p_idx].mvd[0][2][0] = mvc[p_idx].mvd[0][2][1] = 0;
				mvc[p_idx].mvd[0][3][0] = mvc[p_idx].mvd[0][3][1] = 0;
			} else {
							
				abs_dPOC[0] = pic_list[col_id]._reserved & FLAG_LONG_REF;
				if (mvc[p_idx].ref_idx[0]) abs_dPOC[0] |= 0x2000;
				abs_dPOC[1] = abs_dPOC[0];

				

				mvc[p_idx].ref_idx[1] = temp_mbss.ref_idx[1];
				if (mvc[p_idx].ref_idx[1] != 0xFFFF) {
					MVMedianPred(temp_mbss, ip_cfg, 1, 0);

					for (unsigned int j(0);j<4;j++) {
						abs_dPOC[0] = abs_dPOC[1];
						if (((mvc[p_idx].mvd[0][j][0] > 1) || (mvc[p_idx].mvd[0][j][0] < -1)) || ((mvc[p_idx].mvd[0][j][1] > 1) || (mvc[p_idx].mvd[0][j][1] < -1))) abs_dPOC[0] |= 0x4000;

						if (mvc[p_idx].ref_idx[1] != abs_dPOC[0]) {
							mvc[p_idx].mvd[1][j][0] = temp_mbss.mvd[1][0][0];
							mvc[p_idx].mvd[1][j][1] = temp_mbss.mvd[1][0][1];
						}
					}
				}


				mvc[p_idx].ref_idx[0] = temp_mbss.ref_idx[0];
				if (mvc[p_idx].ref_idx[0] != 0xFFFF) {
					MVMedianPred(temp_mbss, ip_cfg, 0, 0);

					for (unsigned int j(0);j<4;j++) {
						abs_dPOC[0] = abs_dPOC[1];
						if (((mvc[p_idx].mvd[0][j][0] > 1) || (mvc[p_idx].mvd[0][j][0] < -1)) || ((mvc[p_idx].mvd[0][j][1] > 1) || (mvc[p_idx].mvd[0][j][1] < -1))) abs_dPOC[0] |= 0x4000;

						if (mvc[p_idx].ref_idx[0] == abs_dPOC[0]) {
							mvc[p_idx].mvd[0][j][0] = mvc[p_idx].mvd[0][j][1] = 0;
						} else {
							mvc[p_idx].mvd[0][j][0] = temp_mbss.mvd[0][0][0];
							mvc[p_idx].mvd[0][j][1] = temp_mbss.mvd[0][0][1];
						}
					}
				} else {
					mvc[p_idx].mvd[0][0][0] = mvc[p_idx].mvd[0][0][1] = 0;
					mvc[p_idx].mvd[0][1][0] = mvc[p_idx].mvd[0][1][1] = 0;
					mvc[p_idx].mvd[0][2][0] = mvc[p_idx].mvd[0][2][1] = 0;
					mvc[p_idx].mvd[0][3][0] = mvc[p_idx].mvd[0][3][1] = 0;
				}
			}
		} else { // temporal			

			if (mvc[p_idx].ref_idx[0] == 0xFFFF) mvc[p_idx].ref_idx[0] = 0;
			else {
				mvc[p_idx].ref_idx[0] = 0;
				for (unsigned int k(0);k<=current_slice.num_ref_idx_active[0];k++) {
					if (decoder_cfg.ref_pic_list[0][k] == mvc[p_idx].ref_idx[1]) {
						mvc[p_idx].ref_idx[0] = k;
						break;
					}
				}
			}

			mvc[p_idx].ref_idx[1] = 0;


			switch (c_map_selector) {
				case 1: // frame2filed
					for (unsigned int j(0);j<4;j++) {
						mvc[p_idx].mvd[0][j][0] /= 2;
						mvc[p_idx].mvd[0][j][1] /= 2;						
					}	
				break;
				case 2: case 3: // field2frame
					for (unsigned int j(0);j<4;j++) {
						mvc[p_idx].mvd[0][j][0] <<= 1;
						mvc[p_idx].mvd[0][j][1] <<= 1;
					}
			}



			abs_dPOC[0] = -InterPicDist(mvc[p_idx].ref_idx[0], 0, pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD);

			if ((pic_list[decoder_cfg.ref_pic_list[0][mvc[p_idx].ref_idx[0]]]._reserved & FLAG_LONG_REF) || (abs_dPOC[0] == 0)) {


			} else {
				abs_dPOC[0] = Clip3(-128, 127, abs_dPOC[0]);
					
				abs_dPOC[1] = abs_dPOC[0];
				if (abs_dPOC[0] < 0) abs_dPOC[0] = -abs_dPOC[0];


				abs_dPOC[0] = ((abs_dPOC[0]>>1) + 16384)/abs_dPOC[1];

				abs_dPOC[1] = -InterPicDist(mvc[p_idx].ref_idx[0], -1, pic_list[decoder_cfg.current_frame_ptr]._reserved & FLAG_SECOND_FIELD);
				abs_dPOC[1] = Clip3(-128, 127, abs_dPOC[1]);

				abs_dPOC[0] = abs_dPOC[1] = Clip3(-1024, 1023, (abs_dPOC[0]*abs_dPOC[1] + 32) >> 6);

				for (unsigned int j(0);j<4;j++) {
					mvc[p_idx].mvd[1][j][0] = -mvc[p_idx].mvd[0][j][0];
					mvc[p_idx].mvd[1][j][1] = -mvc[p_idx].mvd[0][j][1];

					mvc[p_idx].mvd[0][j][0] = ((abs_dPOC[0]*mvc[p_idx].mvd[0][j][0] + 128) >> 8);
					mvc[p_idx].mvd[0][j][1] = ((abs_dPOC[1]*mvc[p_idx].mvd[0][j][1] + 128) >> 8);

					mvc[p_idx].mvd[1][j][0] += mvc[p_idx].mvd[0][j][0];
					mvc[p_idx].mvd[1][j][1] += mvc[p_idx].mvd[0][j][1];

				}
			}
		}
	}
}

