/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Media\MKV.h"
#include "System\SysUtils.h"

#include "Media\Source.h"


const unsigned int Video::MKV::stream_type_map[256] = {
	0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x1B,		0x24, 0x81, 0x03, 0x03, 0x03, 0x86, 0x00, 0x00,
	0x87, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


Memory::Allocator * Video::MKV::mkv_pool = 0;


Video::MKV::MKV(unsigned int pflag) : Program(pflag), terminator(1), reciever_thread(0), block_counter(0), action_code(0), action_size(0), temp_offset(0), temp_val(0), track_runner(0), track_top(0), parse_step(0), att_runner(0), att_top(0), time_scale(1000000), avc_size(4), no_video(1), temp_vid(-1), fuck_mode_on(0) {
	System::MemoryFill(&ebml, sizeof(EBMLHeader), 0);
}

Video::MKV::~MKV() {
	terminator = 0;
	if (reciever_thread) {
		SetEvent(program_block[0]);
		if (reciever_thread) {
			WaitForSingleObject(reciever_thread, INFINITE);
			CloseHandle(reciever_thread);
		}
	}


	temp_buf.Destroy();
	att_obj.Destroy();
	mkv_obj.Destroy();

	source.Destroy(); //  delete source;


}


UI_64 Video::MKV::Initialize() {
	wchar_t pool_name[] = {8, L'm',L'k', L'v', L' ', L'p', L'o', L'o', L'l'};
	UI_64 result(0);

	if (mkv_pool) return -1;

	if (mkv_pool = new Memory::Allocator(0)) {
		result = mkv_pool->Init(new Memory::Buffer(), 0x040000, pool_name);
	} else result = ERROR_OUTOFMEMORY;


	return result;
}

UI_64 Video::MKV::Finalize() {
	if (mkv_pool) delete mkv_pool;
	mkv_pool = 0;

	return 0;
}

bool Video::MKV::IsMKV(Data::Stream & src) {
	UI_64 b_size(1024);
	bool result(false);
	char ival(0);


	if (char * src_ptr = reinterpret_cast<char *>(src.MoveLock(b_size, (UI_64)0))) { // reinterpret_cast<char *>(src->MoveLock(b_size, (UI_64)0))
		ival = src_ptr[b_size];
		src_ptr[b_size] = 0;
		result = (Strings::SearchAStr(src_ptr, "matroska") != 0);
		src_ptr[b_size] = ival;
	}

	return result;
}

void Video::MKV::NVFlush(TrackHeader * tk_ptr) {
	for (unsigned int j(0);j<track_runner;j++) {
		if (tk_ptr[j]._sequence) {
			if (!tk_ptr[j]._sequence->IsVideo()) {
				tk_ptr[j]._sequence->Flush(current_time, sync_start);
			}
		}
	}

	sync_start = -1;
}

void Video::MKV::NVReset() {
	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (!tk_hdr[j]._sequence->IsVideo()) {
					tk_hdr[j]._sequence->Reset();
				}
			}
		}
		mkv_obj.Release();
	}
	
	sync_start = -1;
}

unsigned int Video::MKV::EnableAudioSource(unsigned int a_sel, const OpenGL::Element & a_plane) {
	if (a_sel < audio.count) {
		if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {
						if ((audio.flags & AUDIO_SOURCE_EXCLUSIVE) && (a_sel != 0)) {
							tk_hdr[j]._sequence->ResetSat();
							tk_hdr[j]._sequence->Off();
						}
						if (a_sel-- == 0) {
							tk_hdr[j]._sequence->On();
							tk_hdr[j]._sequence->ResetSatRecord(a_plane);
						}
					}
				}
			}
			mkv_obj.Release();
		}
	}

	return a_sel;
}

unsigned int Video::MKV::IsAudioActive(unsigned int a_sel) {
	unsigned int result(0);

	if (a_sel < audio.count) {
		if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {
						if (a_sel-- == 0) {
							result = tk_hdr[j]._sequence->TestFlag(SOURCE_NO_RENDER);
							break;
						}
					}
				}
			}
			mkv_obj.Release();
		}
	}

	return result;
}

unsigned int Video::MKV::GetAudioLNG(unsigned int a_sel) {
	unsigned int result(a_sel);

	if (a_sel < audio.count) {
		if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
			for (unsigned int j(0);j<track_runner;j++) {
				if (tk_hdr[j]._sequence) {
					if (tk_hdr[j]._sequence->IsAudio()) {
						if (a_sel-- == 0) {
							if (tk_hdr[j].lang_id) result = tk_hdr[j].lang_id;							
							break;
						}
					}
				}
			}
			mkv_obj.Release();
		}
	}

	return result;
}

unsigned int Video::MKV::DisableAudioSource(unsigned int a_sel) {
	


	return a_sel;
}


void Video::MKV::SetAudioVolume(float a_val) {
	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsAudio()) {
					tk_hdr[j]._sequence->SetVolume(&a_val);
				}
			}
		}
		mkv_obj.Release();
	}
}


Media::Source * Video::MKV::GetVSource(UI_64 & p_id) {
	Media::Source * result(0);
	unsigned int s_val(0);

	if (p_id >= video.count) p_id = 0;
	s_val = p_id;

	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsVideo())
				if (s_val-- == 0) {
					result = tk_hdr[j]._sequence;
					break;
				}
			}
		}
		mkv_obj.Release();
	}

	return result;
}

Media::Source * Video::MKV::GetASource(UI_64 & p_id) {
	Media::Source * result(0);
	unsigned int s_val(0);

	if (p_id >= video.count) p_id = 0;
	s_val = p_id;

	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
		for (unsigned int j(0);j<track_runner;j++) {
			if (tk_hdr[j]._sequence) {
				if (tk_hdr[j]._sequence->IsAudio())
				if (s_val-- == 0) {
					result = tk_hdr[j]._sequence;
					break;
				}
			}
		}
		mkv_obj.Release();
	}

	return result;
}



Media::Program * Video::MKV::SetSource(const Data::Stream & src, unsigned int preview_flag) {
	MKV * result(0);
	UI_64 eval(0);
		
	if (result = new MKV(preview_flag)) {
		eval = result->mkv_obj.New(_tid, 32*sizeof(TrackHeader), mkv_pool);
		eval |= result->temp_buf.New(0, 131072);

		if (eval != -1) {
			if (TrackHeader * thdr = reinterpret_cast<TrackHeader *>(result->mkv_obj.Acquire())) {
//				System::MemoryFill(thdr, 32*sizeof(TrackHeader), 0);
				result->mkv_obj.Release();

				result->track_top = 32;

				result->source = src;

				eval = 0;
				if (result->program_block[0]) {
					if (result->reciever_thread = CreateThread(0, 0, ReceiverLoop, result, 0, 0)) {

					} else {
						eval = GetLastError();
					}
				}
			}
		}
		
		if (eval) {
			delete result;
			result = 0;
		}
	}


	return result;
}


DWORD __stdcall Video::MKV::ReceiverLoop(void * cntx) {
	UI_64 result(0);
	UI_64 block_size(0), scan_limit(24);
	double target_val(0);

	I_64 pool_index[2*MAX_POOL_COUNT];

	MKV * mkv_ptr = reinterpret_cast<MKV *>(cntx);
	const unsigned char * block_ptr(0);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // THREAD_PRIORITY_ABOVE_NORMAL THREAD_PRIORITY_TIME_CRITICAL
		
	Memory::Chain::RegisterThread(pool_index);

	scan_limit = mkv_ptr->source.GetSize()/FILE_BUFFER_SIZE + 1;
	if (scan_limit > 24) scan_limit = 24;
//	mkv_ptr->source->Reset();

	for (;mkv_ptr->terminator;) {
		if ((mkv_ptr->block_counter > scan_limit) && (mkv_ptr->no_video)) {
			mkv_ptr->terminator = 0;			
			break;
		}

		block_ptr = 0;

		if (mkv_ptr->search_mode == 1) {
			target_val = mkv_ptr->search_mark;
			target_val /= (mkv_ptr->stop_mark - mkv_ptr->start_mark);

			block_size = FILE_BUFFER_SIZE;
			mkv_ptr->source.MoveLock(block_size, target_val);

			mkv_ptr->search_mode = 2;

			mkv_ptr->search_mark += mkv_ptr->start_mark;
			if (mkv_ptr->search_mark == mkv_ptr->stop_mark) mkv_ptr->search_mark = mkv_ptr->start_mark;
			
			mkv_ptr->search_idx++;
		// drop any pay payload in hands
			if (mkv_ptr->program_block[1] == 0) {
				mkv_ptr->NVReset();
			}


			for (unsigned int j(0);j<16;j++) {
				block_size = FILE_BUFFER_SIZE;
				if (block_ptr = mkv_ptr->source.IncLock(block_size)) {
					if (mkv_ptr->source.GetStatus() & STREAM_REPEAT_FLAG) {
						mkv_ptr->search_mark = mkv_ptr->start_mark;

						mkv_ptr->NVReset();
					}
				}

		// search for cluster start				
				for (;block_size >= 4;block_ptr++, block_size--) {
					if (((block_ptr[0] ^ 0x1F) | (block_ptr[1] ^ 0x43) | (block_ptr[2] ^ 0xB6) | (block_ptr[3] ^ 0x75)) == 0) {
						mkv_ptr->action_size = 0;
						mkv_ptr->temp_offset = 0;
						mkv_ptr->parse_step = 0;
						mkv_ptr->action_code = 0;

						break;
					}
				}

				if (block_size > 3) {
					break;
				}
			}	
		}



	

		
		if (!block_ptr) {
			block_size = FILE_BUFFER_SIZE;
			if (block_ptr = mkv_ptr->source.IncLock(block_size)) {
				mkv_ptr->block_counter++;
				if (mkv_ptr->source.GetStatus() & STREAM_REPEAT_FLAG) {					
					mkv_ptr->search_mark = mkv_ptr->start_mark;

					mkv_ptr->NVReset();
					
					mkv_ptr->search_mode = 2;
				}
			}
		}

		if (block_ptr) {
			result = mkv_ptr->ParseBlock(block_ptr, block_size);

			if (((result >> 40) ^ '\0mkv') == 0) {
				mkv_ptr->terminator = 0;

				if (mkv_ptr->pending_req.element_obj) {
					mkv_ptr->pending_req.flags = OpenGL::DecodeRecord::FlagDestroy;
					Decode(mkv_ptr->pending_req);
				}
			}
		} else {
			// analyze p_size

		}

	}

	mkv_ptr->Terminate();

	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_ptr->mkv_obj.Acquire())) {
		for (;mkv_ptr->track_runner--;) {
			if (tk_hdr[mkv_ptr->track_runner]._sequence) {
				delete tk_hdr[mkv_ptr->track_runner]._sequence;
				tk_hdr[mkv_ptr->track_runner]._sequence = 0;
			}
		}
		
		mkv_ptr->track_runner = 0;
		mkv_ptr->mkv_obj.Release();
	}

	Memory::Chain::UnregisterThread();

	return result;
}

UI_64 Video::MKV::ParseBlock(const unsigned char * bptr, UI_64 block_size) {
	UI_64 result(0), tval(0);

	switch (parse_step) {
		case 1: 
			temp_offset--;
		goto action_code_id;

		case 2:	goto action_code_size_1;

		case 3:
			tval = temp_val;
			temp_val = 0;

			temp_offset--;

		goto action_code_size_2;

		case 4:	goto action_code_parse;


	}

	for (;block_size && terminator && (search_mode != 1);) {
parse_step = 1;

		action_code = bptr[0];
		bptr++; block_size--;
		action_size = 7 - System::BitIndexHigh_32(action_code);

		if (action_size > block_size) {
			for (unsigned int i(0);i<block_size;i++) {
				action_code <<= 8;
				action_code |= bptr[0];
				bptr++;
			}

			temp_offset = block_size+1;
			block_size = 0;
			break;
		}

action_code_id:
parse_step = 2;
		for (UI_64 i(temp_offset);i<action_size;i++) {
			action_code <<= 8;
			action_code |= bptr[0];
			bptr++; block_size--;
		}

		action_size = 0;

		if (block_size == 0) break;

action_code_size_1:
parse_step = 3;

		tval = bptr[0];
		bptr++; block_size--;
// 0xFF - special case

		action_size = 7 - System::BitIndexHigh_64(tval);
				
		tval ^= (0x80>>action_size);

		if (action_size > block_size) {
			for (UI_64 i(0);i<block_size;i++) {
				tval <<= 8;
				tval |= bptr[i];
			}

			temp_val = tval; temp_offset = block_size + 1;

			block_size = 0;
			break;
		}

action_code_size_2:
parse_step = 4;

		for (UI_64 i(temp_offset);i<action_size;i++) {
			tval <<= 8;
			tval |= bptr[0];
			bptr++; block_size--;
		}

		action_size = tval;
		temp_offset = 0;

		if (block_size == 0) break;

action_code_parse:
		if ((action_code) && (action_size)) {
			if ((action_code == 0x000000A0) || (action_code == 0x1A45DFA3) || (action_code == 0x1941A469) || (action_code == 0x18538067) || (action_code == 0x1549A966) || (action_code == 0x1F43B675) || (action_code == 0x1654AE6B)) action_size = 0;
/*		
			case 0x000000A0: // block group
			case 0x1941A469: // attachment
			case 0x18538067: // segment
			case 0x1549A966: // segment info
			case 0x1F43B675: // cluster
// track ========================================================================================================================================================================================
			case 0x1654AE6B: // track
				action_size = 0;
			break;
*/

			if (action_code == 0x000000AE) {// track entry
				if (track_runner < track_top) {
					track_runner++;
				} else {

					if (mkv_obj.Expand(32*sizeof(TrackHeader)) != -1) {
						if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {
							System::MemoryFill(tk_hdr + track_top, sizeof(TrackHeader), 0);
							mkv_obj.Release();
						}

						track_top += 32;

						track_runner++;
					}					
				}
								
				action_size = 0;
			}



			if (action_size)
			if ((action_size-temp_offset) <= block_size) {
				if (temp_offset) {
					if (unsigned char * t_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
						System::MemoryCopy(t_ptr + temp_offset, bptr, (action_size - temp_offset));
						
						result = ParseAction(t_ptr, action_size);	

						temp_buf.Release();
					}

					bptr += (action_size - temp_offset);
					block_size -= (action_size - temp_offset);

				} else {
					result = ParseAction(bptr, action_size);

					bptr += action_size;
					block_size -= action_size;
				}

				action_size = 0;
			} else {
				if (temp_offset) {
					if (unsigned char * t_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
						System::MemoryCopy(t_ptr + temp_offset, bptr, block_size);
						temp_buf.Release();

						temp_offset += block_size;
					}
				} else {
					if (temp_buf.Resize(action_size+32) != -1) {
						if (void * t_ptr = temp_buf.Acquire()) {
							System::MemoryCopy(t_ptr, bptr, block_size);

							temp_buf.Release();

							temp_offset = block_size;
						}
					} else {
					// error, reset op
					}
				}
			}
			
		} else {
			// error
		}

		if ((action_size) || (((result>>40) ^ '\0mkv') == 0)) break;

		parse_step = 0;
		temp_offset = 0;


	}

	return result;
}


UI_64 Video::MKV::GetElBytes(const unsigned char * b_ptr, void * xval, UI_64 elsize, UI_64 ssize) {	
	switch (elsize) {
		case 4:
			reinterpret_cast<unsigned int *>(xval)[0] = 0;

			for (unsigned int i(0);i<ssize;i++) {
				reinterpret_cast<unsigned int *>(xval)[0] <<= 8;
				reinterpret_cast<unsigned char *>(xval)[0] = b_ptr[0];
				b_ptr++;
			}

		break;
		case 8:
			reinterpret_cast<unsigned __int64 *>(xval)[0] = 0;

			for (unsigned int i(0);i<ssize;i++) {
				reinterpret_cast<unsigned __int64 *>(xval)[0] <<= 8;
				reinterpret_cast<unsigned char *>(xval)[0] = b_ptr[0];
				b_ptr++;
			}

		break;
		default:
			System::MemoryCopy(xval, b_ptr, ssize);

	}


	return 0;
}


UI_64 Video::MKV::ParseAction(const unsigned char * b_ptr, UI_64 b_size) {
	UI_64 result(0), x_val(0), la_count(0);
	I_64 t_val(0), tt_val(0);

	SFSco::Object temp_obj;
	float some_val(0);
	unsigned short motherfucker_size(0), motherfucker_offset(0), motherfucker_count(0), unknown_block_fuck(0);

	if (ebml.start_code == 0) {
		if (action_code == 0x1A45DFA3) {
			ebml.start_code = action_code;			
		}
	}

	if (TrackHeader * tk_hdr = reinterpret_cast<TrackHeader *>(mkv_obj.Acquire())) {

		if (terminator == 1) {
			switch (action_code) {
// header ===================================================================================================================================================================================================
				case 0x000042F7:
					GetElBytes(b_ptr, &ebml.read_version, 4, b_size);
				break;
				case 0x000042F2:
					GetElBytes(b_ptr, &ebml.max_id_length, 4, b_size);
				break;
				case 0x000042F3:
					GetElBytes(b_ptr, &ebml.max_elsize, 4, b_size);
				break;
				case 0x00004282:
					GetElBytes(b_ptr, &ebml.doc_type, 8, b_size);
				break;
				case 0x00004287:
					GetElBytes(b_ptr, &ebml.doc_type_version, 4, b_size);
				break;
				case 0x00004285:
					GetElBytes(b_ptr, &ebml.doc_type_read_version, 4, b_size);
				break;

				case 0x002AD7B1: // timecode scale
					GetElBytes(b_ptr, &time_scale, 8, b_size);
				break;
				case 0x00004489: // duration
					GetElBytes(b_ptr, &some_val, 4, b_size);
					start_mark = 0;
					stop_mark = some_val*time_scale/100000;
				break;



				case 0x000000D7: // track number
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].id, 4, b_size);
				break;
				case 0x0023E383: // defaut duration
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].default_duration, 8, b_size);
				break;
				case 0x0022B59C: // language
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].lang_id, 4, b_size);

					if (tk_hdr[track_runner-1]._sequence)
					if ((tk_hdr[track_runner-1]._sequence->IsAudio()) && Media::Source::TestLNG(tk_hdr[track_runner-1].lang_id)) {
						for (unsigned int i(0);i<track_runner;i++) {
							if (tk_hdr[i]._sequence) {
								if (tk_hdr[i]._sequence->IsAudio()) {
									tk_hdr[i]._sequence->Off();

									break;
								}
							}
						}

						tk_hdr[track_runner-1]._sequence->On();

					}
				break;
				case 0x00007446: // attachment link
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].attachment_id, 8, b_size);
				break;
				case 0x00000086: // codec ID
					if (track_runner <= track_top) {
						GetElBytes(b_ptr, tk_hdr[track_runner-1].codec_string, 32, b_size);
					
						if (action_code == 0x00000086) {
							tk_hdr[track_runner-1].codec_string[31] = 0;

							tk_hdr[track_runner-1].codec_id = -1;

							for (unsigned int i(0);i<29;i++) {
								if (Strings::SearchAStr(tk_hdr[track_runner-1].codec_string, codec_id_strings[i])) {
									tk_hdr[track_runner-1].codec_id = i;
									break;
								}
							}

							if (tk_hdr[track_runner-1].codec_id != -1) {
								if (tk_hdr[track_runner-1]._sequence) {
									delete tk_hdr[track_runner-1]._sequence;
									tk_hdr[track_runner-1]._sequence = 0;

									pts[0] = pts[1] = 0;
								}

								if (tk_hdr[track_runner-1]._sequence = Media::CodedSource::Create(stream_type_map[tk_hdr[track_runner-1].codec_id])) {
									if (tk_hdr[track_runner-1]._sequence->IsVideo()) {
										no_video = 0;
										if (++video.count == 1) {
											tk_hdr[track_runner-1]._sequence->On();
											temp_vid = track_runner - 1;
										}

										if (program_block[1]) tk_hdr[track_runner-1]._sequence->SetRenderSkip(-1);
									}

									if (program_block[1] == 0)
									if (tk_hdr[track_runner-1]._sequence->IsAudio()) {
										if (++audio.count == 1) tk_hdr[track_runner-1]._sequence->On();
										else {
											if (Media::Source::TestLNG(tk_hdr[track_runner-1].lang_id)) {
												for (unsigned int i(0);i<track_runner;i++) {
													if (tk_hdr[i]._sequence) {
														if (tk_hdr[i]._sequence->IsAudio()) {
															tk_hdr[i]._sequence->Off();
															
															break;
														}
													}
												}

												tk_hdr[track_runner-1]._sequence->On();
											}
										}
									}
								}
/*
								switch (tk_hdr[track_runner-1].codec_id) {
									case 2: // mpeg1
								
									break;;
									case 3: // mpeg2

									break;
									case 7: // h264
									break;
									case 8: // h265

									break;

									case 9: // AC3
									
									break;
									case 10: // mp3

									break;
									case 11: // mp2

									break;
									case 12: // mp1

									break;
									case 13: // DTS

									break;
									case 14: // PCM int

									break;
									case 15: // PCM float

									break;
									case 21: // AAC
										if (tk_hdr[track_runner-1]._sequence = Media::CodedSource::Create(0x11)) {
											source_count++;
										}

									break;

									case 23: // ascII

									break;
									case 24: // UTF8
									
									break;
									case 26: // ASS

									break;

								}
*/
							}
						}
					}

				break;
				case 0x000063A2: // codec motherfucker private
					if (track_runner <= track_top)  {
						t_val = -1;

						if (tk_hdr[track_runner-1]._sequence)
						switch (tk_hdr[track_runner-1].codec_id) {
							case 0x07:
								motherfucker_offset = 5;
							
								avc_size = 1 + (b_ptr[4] & 0x03);

								motherfucker_count = (b_ptr[motherfucker_offset++] & 0x1F);
								for (unsigned short si(0);si<motherfucker_count;si++) {
									motherfucker_size = b_ptr[motherfucker_offset++];
									motherfucker_size <<= 8;
									motherfucker_size |= b_ptr[motherfucker_offset++];

								
									if ((motherfucker_offset + motherfucker_size) <= b_size) {										
										tk_hdr[track_runner-1]._sequence->DecodeUnit(b_ptr + motherfucker_offset, motherfucker_size, t_val);

										motherfucker_offset += motherfucker_size;
									}
								}

								motherfucker_count = b_ptr[motherfucker_offset++];
								for (unsigned short si(0);si<motherfucker_count;si++) {
									motherfucker_size = b_ptr[motherfucker_offset++];
									motherfucker_size <<= 8;
									motherfucker_size |= b_ptr[motherfucker_offset++];

							
									if ((motherfucker_offset + motherfucker_size) <= b_size) {
										tk_hdr[track_runner-1]._sequence->DecodeUnit(b_ptr + motherfucker_offset, motherfucker_size, t_val);
										motherfucker_offset += motherfucker_size;
									}
								}
							
							break;
							case 0x15:
								tk_hdr[track_runner-1]._sequence->DecodeUnit(b_ptr, b_size, t_val);

							break;
						}
					}

				break;

// attachment =============================================================================================================================================================================
				case 0x000061A7: // file attachment
					if (att_runner >= att_top) {
					// resize
						if (att_obj.Expand(4*sizeof(Attachment)) != -1) {
							att_top += 4;
						}
					}

					if (att_runner < att_top) {
						temp_obj.Blank();
						if (Attachment * att_hdr = reinterpret_cast<Attachment *>(att_obj.Acquire())) {
							System::MemoryFill(att_hdr+att_runner, sizeof(Attachment), 0);
							System::MemoryCopy(att_hdr+att_runner, &temp_obj, sizeof(SFSco::Object));
							att_obj.Release();
												
						}
					}
				
					att_runner++;
				break;
				case 0x0000466E: // file name
					if (att_runner <= att_top) {
						if (Attachment * att_hdr = reinterpret_cast<Attachment *>(att_obj.Acquire())) {
							GetElBytes(b_ptr, att_hdr[att_runner-1].file_name, 256, b_size);
							att_hdr[att_runner-1].file_name[255] = 0;

							att_obj.Release();
												
						}
					}

				break;
				case 0x00004660: // file mime type
					if (att_runner <= att_top) {
						if (Attachment * att_hdr = reinterpret_cast<Attachment *>(att_obj.Acquire())) {
							GetElBytes(b_ptr, att_hdr[att_runner-1].mime_type, 256, b_size);
							att_hdr[att_runner-1].mime_type[255] = 0;

							att_obj.Release();												
						}
					}

				break;

				case 0x0000465C: // file data
					if (Attachment * att_hdr = reinterpret_cast<Attachment *>(att_obj.Acquire())) {
						temp_obj = att_hdr[att_runner-1].data;

						att_obj.Release();
					}

					if (!temp_obj) temp_obj.New(0, b_size);

					if (unsigned char * dptr = reinterpret_cast<unsigned char *>(temp_obj.Acquire())) {
//						System::MemoryCopy_SSE3(dptr, b_ptr, b_size);

						temp_obj.Release();

						if (Attachment * att_hdr = reinterpret_cast<Attachment *>(att_obj.Acquire())) {
							att_hdr[att_runner-1].data = temp_obj;

							att_obj.Release();
						}

					}

				break;

			}
		}

		switch (action_code) {
			case 0x00004286:
// FlagSSNext
				if (terminator > 1)
				if (pending_req.element_obj) {
					pending_req.flags = OpenGL::DecodeRecord::FlagSSNext;
					pending_req.pic_id = -1;
					Decode(pending_req);
				}

				GetElBytes(b_ptr, &ebml.version, 4, b_size);

				search_mark = start_mark;

				NVReset();

				search_mode = 2;

			break;

// cluster =====================================================================================================================================================================================================
			case 0x000000E7: // timecode
				GetElBytes(b_ptr, &current_block.time_code, 8, b_size);
			break;


			case 0x000000A1: // block				
			case 0x000000A3: // simple block				
				// block structure
				unknown_block_fuck = 1;

				current_block.track_id = b_ptr[0]; b_ptr++; b_size--;
				result = 7 - System::BitIndexHigh_32(current_block.track_id);
						
				current_block.track_id ^= (0x80>>result);
						
				for (UI_64 i(0);i<result;i++) {
					current_block.track_id <<= 8;
					current_block.track_id |= b_ptr[0]; b_ptr++; b_size--;
				}
						
				current_block.time_offset = b_ptr[0]; b_ptr++; b_size--;
				current_block.time_offset <<= 8;
				current_block.time_offset |= b_ptr[0]; b_ptr++; b_size--;

				current_block.flags._oval = b_ptr[0]; b_ptr++; b_size--;
				
#ifdef _DEBUG
	if ((current_block.time_code == 0x2951) && (current_block.time_offset == 0x0e32)) {
		t_val = 0;
	}

#endif

				if (current_block.flags.lacing) {
					la_count = b_ptr[0]; b_ptr++; b_size--;

				} else {
					la_count = 0;
					la_size[0] = b_size;
				}

				switch (current_block.flags.lacing) {					

					case 1: // xiph
						la_size[la_count] = b_size;
						for (unsigned int i(0);i<la_count;i++) {
							for (la_size[i] = 0;b_ptr[0] == 255;la_size[i] += 255, la_size[la_count]--, b_ptr++, b_size--);
							la_size[i] += b_ptr[0]; b_ptr++; b_size--;
							la_size[la_count] -= (la_size[i] + 1);
						}
					break;
					case 2: // fized size
						la_size[0] = b_size / (la_count + 1);

						for (unsigned int i(1);i<=la_count;i++) {
							la_size[i] = la_size[i - 1];
						}
					break;
					case 3: // EBML
						la_size[0] = b_ptr[0]; b_ptr++; b_size--;
						result = 7 - System::BitIndexHigh_32(la_size[0]);
						la_size[0] ^= (0x80>>result);

						for (unsigned j(0);j<result;j++) {
							la_size[0] <<= 8;
							la_size[0] |= b_ptr[0];
							b_ptr++;
							b_size--;
						}

						la_size[la_count] = b_size - la_size[0];

						for (unsigned int i(1);i<la_count;i++) {
							la_size[i] = b_ptr[0]; b_ptr++; b_size--; la_size[la_count]--;

							result = 7 - System::BitIndexHigh_32(la_size[i]);

							la_size[i] ^= (0x80>>result);

							for (unsigned int j(0);j<result;j++) {
								la_size[i] <<= 8;
								la_size[i] |= b_ptr[0];
								b_ptr++;
								b_size--;
								la_size[la_count]--;
							}

							la_size[i] -= ((64 << ((result<<3) - result)) - 1);
							la_size[i] += la_size[i-1];

							la_size[la_count] -= la_size[i];
						}

					break;

				}

				// do actual decoding
				for (unsigned int i(0);i<track_runner;i++) {
					if (tk_hdr[i].id == current_block.track_id) {
						if (tk_hdr[i]._sequence) {
							t_val = ((current_block.time_code + current_block.time_offset)*time_scale)/100000;

							for (unsigned int lai(0);lai<=la_count;lai++) { // decode blocks
								for (;la_size[lai] > 4;) {									

									result = 0;
									
									if (search_mode == 11) {
										SetAudioVolume(reinterpret_cast<double *>(&search_mark)[0]);
										search_mode = 0;
									}


									if (tk_hdr[i]._sequence->IsVideo()) {

										if (current_block.flags.lacing) {
											x_val = la_size[lai]; 
										} else { // in some mkvs size of the first chunk is incorrect for some reason
											x_val = 0;

											switch (avc_size) {
												case 4:
													x_val |= b_ptr[0]; x_val <<= 8; b_ptr++; la_size[lai]--;
												case 3:
													x_val |= b_ptr[0]; x_val <<= 8; b_ptr++; la_size[lai]--;
												case 2:
													x_val |= b_ptr[0]; x_val <<= 8; b_ptr++; la_size[lai]--;
												case 1:
													x_val |= b_ptr[0]; b_ptr++; la_size[lai]--;
												break;
												default:
													x_val = la_size[lai];
											}											
											

											if (x_val > la_size[lai]) {
												if (unknown_block_fuck) {
													fuck_mode_on = 1;
													unknown_block_fuck = 0;

													x_val >>= 8;
													if (x_val > (la_size[lai] + 1)) {
														result = 'mkvh';
														result <<= 32;
														break;
													} else {
														b_ptr--; la_size[lai]++;
													}
												} else {
													result = 'mkvh';
													result <<= 32;
													break;
												}
											}
										}

										if (search_mode == 2) {
											if ((t_val >= search_mark) || (program_block[1])) {
												search_mode = 0;
												pts[0] = pts[1] = 0;

												tk_hdr[i]._sequence->JumpOn();

											} else {
												x_val = la_size[lai];
											}
										}

									} else {
/*

#ifdef _DEBUG
{
	static FILE * t_file(0);

	if (t_file == 0) {
		t_file = fopen("mkv_marks", "w");
	}

	if (t_file) {
		fprintf(t_file, "%lld\t%llu\t%llu\t%d\n", t_val, current_block.time_code, current_block.time_offset, current_block.track_id);
	}

}
#endif
*/
										unknown_block_fuck = 0;
										x_val = la_size[lai];
										
									}



									if (!search_mode) {
										if (terminator) terminator = 2;

										if (tk_hdr[i]._sequence->IsAudio()) {
											if (sync_start == -1) sync_start = t_val;
										}

										tt_val = t_val;

										if (!tk_hdr[i]._sequence->IsVideo()) {
											if (fuck_mode_on) tk_hdr[i]._sequence->SetComplete(COMPLETE_MKV_FUCK);

										}

										result = tk_hdr[i]._sequence->DecodeUnit(b_ptr, x_val, tt_val);

										if ((result >> 32) && (unknown_block_fuck)) {
											x_val >>= 8;
											b_ptr--; la_size[lai]++;

											tt_val = t_val;

											result = tk_hdr[i]._sequence->DecodeUnit(b_ptr, x_val, tt_val);
										}

										if (result >= RESULT_DECODE_COMPLETE) {
											if (result & RESULT_DECODE_COMPLETE) {
												if (tk_hdr[i]._sequence->IsVideo()) {

													if (program_block[1]) {
														SetSTP();

													} else {
														if (i == temp_vid) {
															current_time = tt_val;
														}
													
														if (pending_req.program) {
															if (GetVSource(pending_req.pic_id)) {
																pending_req.flags |= OpenGL::DecodeRecord::FlagSourceOn;
																Decode(pending_req);

																pending_req.program = 0;
															}
														}

													
														if (sync_start >= 0) {
															NVFlush(tk_hdr);
														}
														
														 if (SetPTS(tt_val) < 0) {
															tk_hdr[i]._sequence->SetRenderSkip(1);														
														}
													}
												}
											} else {
												result >>= 32;
/*					
													case -3: // end of source
													case -4: // codec is not set
													case -6: // slice header is missing
													case -91: case -92: // damaged bit stream
													case -94: // slice parsing error
*/
											}

										}
/*										
										else {
												// test for pending source select, send decode record on success

											if (pending_req.program) {
												if (GetVSource(pending_req)) {
													pending_req.flags = OpenGL::DecodeRecord::FlagSourceOn;
													Program::Decode(pending_req);

													pending_req.program = 0;
												}
											}
										}
*/
									}

									unknown_block_fuck = 0;

									if (la_size[lai] >= x_val) {
										b_ptr += x_val; la_size[lai] -= x_val;
									} else {
										break;
									}
								}
								
							}

							break;
						}
					}
				}

			break;
/*
				case 0x00234E7A: // default decoded field duration
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].default_field_duration, 8, b_size);
				break;

				case 0x00006FAB: // track overlay
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].track_overlay, 8, b_size);
				break;
				case 0x000056AA: // codec delay
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].codec_delay, 8, b_size);
				break;
				case 0x000056BB: // seak preroll
					if (track_runner <= track_top) GetElBytes(b_ptr, &tk_hdr[track_runner-1].seek_prerol, 8, b_size);
				break;

			case 0x000000A2: // block virtual
				result = 2;
			break;
			case 0x000075A1: // block additions
				result = 3;
			break;
			case 0x000000A6: // block more
				result = 4;
			break;
			case 0x000000EE: // block add ID
				result = 5;
			break;
			case 0x000000A5: // block additional
				result = 6;
			break;
			case 0x0000009B: // block duration
				GetElBytes(bptr, &current_block.reference_duration, 8, b_size);
			break;
			case 0x000000FA: // reference priority
				result = 8;
			break;
			case 0x000000FB: // reference block
				GetElBytes(bptr, &current_block.refeernce_time_stamp, 8, b_size);
			break;
			case 0x000000FD: // reference virtual
				result = 10;
			break;
			case 0x000075A2: // discard padding
				result = 12;
			break;
			case 0x0000008E: // slices
				result = 13;
			break;
			case 0x000000E8: // time slice
				result = 15;
			break;
			case 0x000000CC: // lace number
				result = 16;
			break;
			case 0x000000CD: // frame number
				result = 17;
			break;
			case 0x000000CB: // block addition ID
				result = 18;
			break;
			case 0x000000CE: // delay
				result = 19;
			break;
			case 0x000000CF: // slice duration
				result = 20;
			break;




// cue =======================================================================================================================================================================================================
			case 0x1C53BB6B: // cues
				result = 21;
			break;
			case 0x000000BB: // cue point

			break;
			case 0x000000B3: // cue time

			break;
			case 0x000000B7: // cue track positions

			break;
			case 0x000000F7: // cue track
			
			break;
			case 0x000000F1: // cue cluster position

			break;
			case 0x000000F0: // cue relative position

			break;
			case 0x000000B2: // cue duration

			break;
			case 0x00005378: // cue block number

			break;
			case 0x000000EA: // cue code state

			break;
			case 0x000000DB: // cue reference

			break;
			case 0x00000096: // cue ref time

			break;
			case 0x00000097: // cue ref cluster

			break;
			case 0x0000535F: // cue ref number

			break;
			case 0x000000EB: // cue ref codec state

			break;


// chapters =========================================================================================================================================================================================================
			case 0x1043A770: // chapters
				result = 22;
			break;
			case 0x000045B9: // edition entry

			break;
			case 0x000045BC: // edition UID
				
			break;
			case 0x000045BD: // edition flag hidden

			break;
			case 0x000045DB: // edition flag default

			break;
			case 0x000045DD: // edition flag ordered

			break;
			case 0x000000B6: // chapter atom

			break;
			case 0x000073C4: // chapter UID

			break;
			case 0x00005654: // chapter string UID

			break;
			case 0x00000091: // chapter time start

			break;
			case 0x00000092: // chapter time end

			break;
			case 0x00000098: // chapter flag hidden

			break;
			case 0x00004598: // chapter flag enabled

			break;
			case 0x00006E67: // chapter segment UID

			break;
			case 0x00006EBC: // chapter segment edition UID

			break;
			case 0x000063C3: // chapter physical equiv

			break;
			case 0x0000008F: // chapter track

			break;
			case 0x00000089: // chapter track number

			break;
			case 0x00000080: // chapter display

			break;
			case 0x00000085: // chap string

			break;
			case 0x00006944: // chap process

			break;
			case 0x00006955: // chap process codec id
			
			break;
			case 0x0000450D: // chap process private
			
			break;
			case 0x00006911: // chap process command

			break;
			case 0x00006922: // chap process time

			break;
			case 0x00006933: // chap process data

			break;
*/

// stuff ==================================================================================================================================================================================

/*
			case 0x00006D80: // content encodings
			case 0x00006240: // content encoding
			case 0x00005031: // content encoding order
			case 0x00005032: // content encoding scope
			case 0x00005033: // content encoding type
			case 0x00005034: // content compression
			case 0x00004254: // content compression algo
			case 0x00004255: // content compression settings
			case 0x00005035: // content encryption
			case 0x000047E1: // content encryption algo




		case 0x00005854: // silent tracks
		case 0x000058D7: // silent track number
		case 0x000000A7: // position
		case 0x000000AB: // previous size


		case 0x000073C5: // track uid
		case 0x000055EE: // max block addition ID
		case 0x0000536E: // track name
				

		case 0x00006624: // track translate
		case 0x000066FC: // track translate edition UID
		case 0x000066BF: // track translate codec
		case 0x000066A5: // track translate track ID
		case 0x00000083: // track type
		case 0x000000B9: // flag enabled
		case 0x00000088: // flag default
		case 0x000055AA: // flag forced
		case 0x0000009C: // flag lacing


		case 0x114D9B74: // meta seek info
		case 0x00004DBB: // seek entry
		case 0x000053AB: // seek id
		case 0x000053AC: // seek position


		case 0x0000437C: // chap language
		case 0x0000437E: // chap country


		case 0x0000467E: // file description
		case 0x000046AE: // file UID
		case 0x00004675: // file referral
		case 0x00004661: // file used start time
		case 0x00004662: // file used end time


		
		case 0x000073A4: // segment UID
		case 0x00007384: // segment file name
		case 0x003CB923: // previous UID
		case 0x003C83AB: // previous file name
		case 0x003EB923: // next UID
		case 0x003E83BB: // next file name
		case 0x00004444: // segment family
		case 0x00006924: // chapter translate
		case 0x000069FC: // chapter translate edition UID
		case 0x000069BF: // chapter translate codec
		case 0x000069A5: // chapter translate ID
		
		
		case 0x00004461: // date UTC
		case 0x00007BA9: // title
		case 0x00004D80: // muxing app
		case 0x00005741: // writing app


		case 0x000000BF: // crc val
		case 0x000047E6: // content signature hash algo
		case 0x000047E5: // content signature algo
		case 0x000047E2: // content enc key id
		case 0x000047E4: // content signature key id
		case 0x000047E3: // content signature


		case 0x000000B0: // pixel width
		case 0x000000BA: // pixel height
		case 0x000054AA: // pixel crop bottom
		case 0x000054BB: // pixel crop top
		case 0x000054CC: // pixel crop left
		case 0x000054DD: // pixel crop right
		case 0x000054B0: // display width
		case 0x000054BA: // display height
		case 0x000054B2: // display unit
		case 0x000054B3: // aspect ration type
		case 0x002EB524: // colour space
		case 0x002383E3: // frame rate
		case 0x000000E0: // video settings
		case 0x0000009A: // flag interlaced
		case 0x000053B8: // stereo mode
		case 0x000053C0: // alpha mode
		case 0x002FB523: // gamma value
		case 0x000000E1: // audio settings
		case 0x000000B5: // sampling frequency
		case 0x000078B5: // output sampling frequency
		case 0x0000009F: // channels
		case 0x00006264: // bit depth
		case 0x000000E2: // track operation
		case 0x000000E3: // track combine planes
		case 0x000000E4: // track plane
		case 0x000000E5: // track plane UID
		case 0x000000E6: // track plane type
		case 0x000000E9: // track join blocks
		case 0x000000ED: // track join UID


		case 0x00007D7B: // channels positions
		case 0x000000C0: // trick track UID
		case 0x000000C1: // trick track segment UID
		case 0x000000C6: // trick track flag
		case 0x000000C7: // trick master track UID
		case 0x000000C4: // trick master track segment UID


		case 0x000053B9: // case old stereo mode
		case 0x003A9697: // codec settings
		case 0x003B4040: // codec info URL
		case 0x0026B240: // codec download URL
		case 0x000000AA: // codec decode ALL
		case 0x00258688: // codec name
		case 0x00006DE7: // min cache
		case 0x00006DF8: // max cache
		case 0x0023314F: // track timecode offset
		case 0x0000537F: // track offset
		case 0x000000AF: // encrypted block
		case 0x000000C8: // reference frame
		case 0x000000C9: // reference offset
		case 0x000000CA: // reference time ocde
		case 0x1B538667: // signature slot
		case 0x00007E8A: // signature algo
		case 0x00007E9A: // signature hash
		case 0x00007EA5: // public key
		case 0x00007EB5: // signature
		case 0x00007E5B: // signature elements
		case 0x00007E7B: // signature element list
		case 0x00006532: // signed element
		case 0x000000EC: // void/invalid data
		case 0x000000A4: // codec state

	*/
			
			default:
				pts[0] = pts[1] = 0;

		}

		mkv_obj.Release();
	}
		

	return result;
}




// =================================================================================================================================================================================================
const char Video::MKV::codec_id_strings[][32] = {
	{'V', '_', 'M', 'S',	'/', 'V', 'F', 'V',		'/', 'F', 'O', 'U',		'R', 'C', 'C', 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'R', 'E',	'A', 'L', '/', 'R',		'V', 0, 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'M', 'P',	'E', 'G', '1', 0,		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'M', 'P',	'E', 'G', '2', 0,		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'V', '_', 'T', 'H',	'E', 'O', 'R', 'A',		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'S', 'N',	'O', 'W', 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'M', 'P',	'E', 'G', '4', '/',		'I', 'S', 'O', '/',		'A', 'S', 'P', 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'V', '_', 'M', 'P',	'E', 'G', '4', '/',		'I', 'S', 'O', '/',		'A', 'V', 'C', 0,	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'V', '_', 'M', 'P',	'E', 'G', '4', '/',		'I', 'S', 'O', '/',		'H', 'E', 'V', 'C',	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'A', 'C',	'3', 0, 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'M', 'P',	'E', 'G', '/', 'L',		'3', 0, 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'M', 'P',	'E', 'G', '/', 'L',		'1', 0, 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'A', '_', 'M', 'P',	'E', 'G', '/', 'L',		'2', 0, 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'D', 'T',	'S', 0, 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'P', 'C',	'M', '/', 'I', 'N',		'T', '/', 'L', 'I',		'T', 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'P', 'C',	'M', '/', 'F', 'L',		'O', 'A', 'T', '/',		'I', 'E', 'E', 'E',	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'A', '_', 'E', 'A',	'C', '3', 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'W', 'A',	'V', 'P', 'A', 'C',		'K', '4', 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'V', 'O',	'R', 'B', 'I', 'S',		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'F', 'L',	'A', 'C', 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},

	{'A', '_', 'R', 'E',	'A', 'L', 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'A', 'A',	'C', 0, 0, 0,			0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'A', '_', 'M', 'S',	'/', 'A', 'C', 'M',		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'S', '_', 'T', 'E',	'X', 'T', '/', 'A',		'S', 'C', 'I', 'I',		0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'S', '_', 'T', 'E',	'X', 'T', '/', 'U',		'T', 'F', '8', 0,		0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'S', '_', 'T', 'E',	'X', 'T', '/', 'S',		'S', 'A', 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'S', '_', 'T', 'E',	'X', 'T', '/', 'A',		'S', 'S', 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	{'S', '_', 'T', 'E',	'X', 'T', '/', 'U',		'S', 'F', 0, 0,			0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0},
	
	{'S', '_', 'V', 'O',	'B', 'S', 'U', 'B',		0, 0, 0, 0,				0, 0, 0, 0,			0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0}


};


