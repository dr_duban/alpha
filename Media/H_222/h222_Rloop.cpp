/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Media\h222.h"
#include "System\SysUtils.h"



UI_64 Video::H222::GetSpan() {
	UI_64 p_size(1024), temp_val(0);
	unsigned short pack_id(0);
	unsigned char adaptation_val(0), service_val(0), control_val(0);

	start_mark = stop_mark = -1;


	if (source) {

		if (unsigned char * src_ptr = source.MoveLock(p_size, (UI_64)0)) {
			if ((src_ptr[4] == sync_byte) && (src_ptr[192+4] == sync_byte) && (src_ptr[960+4] == sync_byte)) {
				ts_type = 192;

				source.MoveLock(p_size, (UI_64)4);
			}

		}



		for (unsigned int i(0);i<4096;i++) {
			p_size = ts_type;

			if (unsigned char * p_ptr = source.DecLock(p_size)) {
			
				if ((p_ptr[0] == sync_byte) && ((p_ptr[1] & 0x80) == 0)) { // transport stream
					
					pack_id = ((unsigned short)(p_ptr[1] & 0x1F) << 8) | p_ptr[2];
					adaptation_val = p_ptr[3];

					p_ptr += 4;
					p_size -= 4;

					if (pack_id == 0x1FFF) continue;
			
					switch (adaptation_val & 0x30) {				
						case 0x30: case 0x20: // adaptation field
							temp_val = p_ptr[0] + 1;
							p_ptr += temp_val;;
							p_size -= temp_val;
						if ((adaptation_val & 0x40) == 0) break;
			
						case 0x10: // payload
							if ((p_ptr[0] == 0) && (p_ptr[1] == 0) && (p_ptr[2] == 1)) {
								temp_val = 0;

								if ((p_ptr[3] == 0xBD) || (p_ptr[3] == 0xC0)) temp_val = 1;
							
								if ((p_ptr[3] & 0xF0) == 0xE0) {
									switch (p_ptr[3] & 0x1F) {
										case 0x10: // 16 | ECM stream
										case 0x11: // 17 | EMM stream
										case 0x12: // 18 | H.222.0, 13818-1 A, 13818-6 DSMCC
										case 0x18: // 24 | H.222.1 type E
										case 0x1F: // 31 | program stream directory
										// data bytes

										break;					

										default:
											temp_val = 1;

									}
								}
							
								if (temp_val){
									p_ptr += 7;
									p_size -= 7;

									switch (p_ptr[0] & 0xC0) {
										case 0x80:
											p_ptr += 2;
											p_size -= 2;

											stop_mark = ((UI_64)(p_ptr[0] & 0x0E) << 29) | ((UI_64)p_ptr[1] << 22) | (((UI_64)p_ptr[2] & 0xFE) << 14) | ((UI_64)p_ptr[3] << 7) | ((UI_64)p_ptr[4] >> 1);

										break;
										case 0xC0:
											p_ptr += 2;
											p_size -= 2;

											stop_mark = ((UI_64)(p_ptr[0] & 0x0E) << 29) | ((UI_64)p_ptr[1] << 22) | (((UI_64)p_ptr[2] & 0xFE) << 14) | ((UI_64)p_ptr[3] << 7) | ((UI_64)p_ptr[4] >> 1);

										break;
									}
								}
							}

						break;
					}				
				}	
			}

			if (stop_mark != -1) break;
		}

		p_size = ts_type;
		switch (ts_type) {
			case 192:
				source.MoveLock(p_size, (UI_64)4);
			break;
			default:
				source.MoveLock(p_size, (UI_64)0);

		}

		for (unsigned int i(0);i<2048;i++) {
			p_size = ts_type;

			if (unsigned char * p_ptr = source.IncLock(p_size)) {
			
				if ((p_ptr[0] == sync_byte) && ((p_ptr[1] & 0x80) == 0)) { // transport stream
					
					pack_id = ((unsigned short)(p_ptr[1] & 0x1F) << 8) | p_ptr[2];
					adaptation_val = p_ptr[3];

					p_ptr += 4;
					p_size -= 4;

					if (pack_id == 0x1FFF) continue;
			
					switch (adaptation_val & 0x30) {				
						case 0x30: case 0x20: // adaptation field
							temp_val = p_ptr[0] + 1;
							p_ptr += temp_val;;
							p_size -= temp_val;
						if ((adaptation_val & 0x40) == 0) break;
			
						case 0x10: // payload
							if ((p_ptr[0] == 0) && (p_ptr[1] == 0) && (p_ptr[2] == 1)) {
								temp_val = 0;

								if ((p_ptr[3] == 0xBD) || (p_ptr[3] == 0xC0)) temp_val = 1;
							
								if ((p_ptr[3] & 0xF0) == 0xE0) {
									switch (p_ptr[3] & 0x1F) {
										case 0x10: // 16 | ECM stream
										case 0x11: // 17 | EMM stream
										case 0x12: // 18 | H.222.0, 13818-1 A, 13818-6 DSMCC
										case 0x18: // 24 | H.222.1 type E
										case 0x1F: // 31 | program stream directory
										// data bytes

										break;					

										default:
											temp_val = 1;

									}
								}
							
								if (temp_val){
									p_ptr += 7;
									p_size -= 7;

									switch (p_ptr[0] & 0xC0) {
										case 0x80:
											p_ptr += 2;
											p_size -= 2;

											start_mark = ((UI_64)(p_ptr[0] & 0x0E) << 29) | ((UI_64)p_ptr[1] << 22) | (((UI_64)p_ptr[2] & 0xFE) << 14) | ((UI_64)p_ptr[3] << 7) | ((UI_64)p_ptr[4] >> 1);

										break;
										case 0xC0:
											p_ptr += 2;
											p_size -= 2;

											start_mark = ((UI_64)(p_ptr[0] & 0x0E) << 29) | ((UI_64)p_ptr[1] << 22) | (((UI_64)p_ptr[2] & 0xFE) << 14) | ((UI_64)p_ptr[3] << 7) | ((UI_64)p_ptr[4] >> 1);

										break;
									}
								}
							}

						break;
					}				
				}	
			}

			if (start_mark != -1) break;
		}

		start_mark /= 9;
		stop_mark /= 9;
		
	}

	return 1;
}




DWORD __stdcall Video::H222::ReceiverLoop(void * cntx) {
	DWORD result(0);
	PIDvector * pid_tab(0);
	double target_val(0);
	UI_64 scan_limit(0x00020000);

	I_64 pool_index[2*MAX_POOL_COUNT];

	H222 * ts_ptr = reinterpret_cast<H222 *>(cntx);
	const unsigned char * packet_ptr(0);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // THREAD_PRIORITY_ABOVE_NORMAL THREAD_PRIORITY_TIME_CRITICAL
// read start/stop markers

	Memory::Chain::RegisterThread(pool_index);

	ts_ptr->GetSpan();
	scan_limit = ts_ptr->source.GetSize()/3760 + 1;
	if (scan_limit > 0x00020000) scan_limit = 0x00020000;


	for (;ts_ptr->terminator;) {
		if ((ts_ptr->packet_counter > scan_limit) && (ts_ptr->no_video)) {			
			ts_ptr->terminator = 0;			
			break;
		}

		
		if (ts_ptr->search_mode == 1) {
			target_val = ts_ptr->search_mark;
			target_val /= (ts_ptr->stop_mark - ts_ptr->start_mark);

			ts_ptr->current_packet.l_size = 0x200000;
			packet_ptr = ts_ptr->source.MoveLock(ts_ptr->current_packet.l_size, target_val);
			ts_ptr->search_mode = 2;


			ts_ptr->search_mark += ts_ptr->start_mark;
			if (ts_ptr->search_mark == ts_ptr->stop_mark) ts_ptr->search_mark = ts_ptr->start_mark;
			
			ts_ptr->search_idx++;

			// drop any pay payload in hands
			ts_ptr->payload_length = 0;
			ts_ptr->temp_offset = 0;

			
			// search for sync byte, transport stream and peyload start
			for (unsigned int i(0);i<ts_ptr->ts_type;i++) {
				if (((packet_ptr[i] ^ sync_byte) | (packet_ptr[i+ts_ptr->ts_type] ^ sync_byte) | (packet_ptr[i+ts_ptr->ts_type*2] ^ sync_byte) | (packet_ptr[i+ts_ptr->ts_type*3] ^ sync_byte)) == 0) {
					ts_ptr->current_packet.l_size = i;
					ts_ptr->source.IncLock(ts_ptr->current_packet.l_size);

					break;
				}
			}

			for (unsigned int i(0);i<11111;i++) {
				ts_ptr->current_packet.l_size = ts_ptr->ts_type;
				packet_ptr = ts_ptr->source.IncLock(ts_ptr->current_packet.l_size);

				if ((packet_ptr[0] == sync_byte) && ((packet_ptr[1] & 0xC0) == 0x40)) { // no error, payload start
					break;
				}
			}
		}


		ts_ptr->current_packet.l_size = ts_ptr->ts_type;
		if (packet_ptr = ts_ptr->source.IncLock(ts_ptr->current_packet.l_size)) {
			ts_ptr->current_packet.l_size -= ts_ptr->ts_type - 188;

			if (ts_ptr->source.GetStatus() & STREAM_REPEAT_FLAG) {
				ts_ptr->search_mark = ts_ptr->start_mark;
				ts_ptr->NVReset();

				ts_ptr->search_mode = 2;
			}



			ts_ptr->packet_counter++;

#ifdef _DEBUG
	if (ts_ptr->packet_counter > 0x0e00) {
		result = 0;
	}

#endif

			if ((packet_ptr[0] == sync_byte) && ((packet_ptr[1] & 0x80) == 0)) { // transport stream
				ts_ptr->current_packet.adaptation_flags._bval = 0;
				ts_ptr->current_packet.service_flags._bval = packet_ptr[1];


				ts_ptr->current_packet.id = ((unsigned short)(packet_ptr[1] & 0x1F) << 8) | packet_ptr[2];
				ts_ptr->current_packet.control_flags._bval = packet_ptr[3];
		
				packet_ptr += 4;
				ts_ptr->current_packet.l_size -= 4;
			
				if (ts_ptr->current_packet.id != 0x1FFF)
				switch (ts_ptr->current_packet.control_flags.adaptation_field) {				
					case 3: case 2: // adaptation field
						result = ts_ptr->GetAF(packet_ptr);
						packet_ptr += result;
						ts_ptr->current_packet.l_size -= result;
					if ((ts_ptr->current_packet.control_flags.adaptation_field & 1) == 0) break;
			
					case 1: // payload						
					// maybe scrambled				
						ts_ptr->DecodePacket(packet_ptr);
						
					break;
				}

					
			} else {
				if ((packet_ptr[0] == 0) && (packet_ptr[1] == 0) && (packet_ptr[2] == 1) && (packet_ptr[3] == pes_header_code)) { // program stream




				}
			}
						
		} else {
			// analyze p_size

		}

	}
	

	ts_ptr->Terminate();

	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_ptr->ts_obj.Acquire((void **)&pid_tab))) {
		for (;ts_hdr->pid_runner--;) {

			if (pid_tab[0].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pid_tab[0].pid_obj.Acquire())) {
					delete str_hdr->_sequence;
					str_hdr->_sequence = 0;
					pid_tab[0].pid_obj.Release();
				}
			}

			pid_tab[0].pid_obj.Destroy();
			pid_tab++;
		}

		ts_hdr->pid_runner = 0;
		ts_ptr->ts_obj.Release(pid_tab);
	}
		
	Memory::Chain::UnregisterThread();


	return result;
}



UI_64 Video::H222::GetAF(const unsigned char * packet_ptr) {
	UI_64 result(packet_ptr[0]), tval(0);
	SFSco::Object program_obj;
	PIDvector * pid_tab;

	packet_ptr++;

	if (result) {
		current_packet.adaptation_flags._bval = packet_ptr[0];
	
		packet_ptr++; 

		if (TransportHeader * tshdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pid_tab))) {
			for (UI_64 i(0);i<tshdr->pid_runner;i++) {
				if ((current_packet.id == pid_tab[i].pid_val) && (pid_tab[i].pid_obj.GetTID() == program_tid)) {
					program_obj = pid_tab[i].pid_obj;
					break;
				}
			}

			if (program_obj) {
				if (ProgramHeader * phdr = reinterpret_cast<ProgramHeader *>(program_obj.Acquire())) {
					if (current_packet.adaptation_flags.PCR) {
						phdr->clock_frequency = ((unsigned __int64)packet_ptr[0] << 25) | ((UI_64)packet_ptr[1] << 17) | ((UI_64)packet_ptr[2] << 9) | ((UI_64)packet_ptr[3] << 1) | (packet_ptr[4] >> 7);
						packet_ptr += 4;		
						phdr->clock_frequency *= 300;
						tval = ((UI_64)(packet_ptr[0] & 1) << 8) | packet_ptr[1];

						phdr->clock_frequency += tval;

						packet_ptr += 2;
					}

					if (current_packet.adaptation_flags.OPCR) {
						phdr->original_clock_frequency = ((unsigned __int64)packet_ptr[0] << 25) | ((UI_64)packet_ptr[1] << 17) | ((UI_64)packet_ptr[2] << 9) | ((UI_64)packet_ptr[3] << 1) | (packet_ptr[4] >> 7);
						packet_ptr += 4;
						phdr->original_clock_frequency *= 300;
						tval = ((UI_64)(packet_ptr[0] & 1) << 8) | packet_ptr[1];

						phdr->original_clock_frequency += tval;

						packet_ptr += 2;
					}

				
					program_obj.Release();
				}

			}


			if (current_packet.adaptation_flags.splicing_point) {
				tshdr->adaptation_cfg.splice_countdown = packet_ptr[0];
				packet_ptr++;
			}

			if (current_packet.adaptation_flags.transport_private_data) {
			

				packet_ptr += (packet_ptr[0]+1);
			}

			if (current_packet.adaptation_flags.adaptation_field_extension) {
				tval = packet_ptr[1];

				packet_ptr += 2;
				if (tval & 0x80) {
					tshdr->adaptation_cfg.ltw_valid_flag = packet_ptr[0] & 0x80;
					tshdr->adaptation_cfg.legal_time_window_offset = ((UI_64)(packet_ptr[0] & 0x7F) << 8) | packet_ptr[1];

					packet_ptr += 2;
				}

				if (tval & 0x40) {
					tshdr->adaptation_cfg.piecewise_rate = ((UI_64)(packet_ptr[0] & 0x3F) << 16) | ((UI_64)packet_ptr[1] << 8) | packet_ptr[2];
					packet_ptr += 3;
				}

				if (tval & 0x20) {
					tshdr->adaptation_cfg.splice_type = packet_ptr[0] >> 4;

					tshdr->adaptation_cfg.decoding_time_stamp_nau = ((unsigned __int64)packet_ptr[0] & 0x0E) << 29;
					tshdr->adaptation_cfg.decoding_time_stamp_nau |= ((UI_64)packet_ptr[1] << 22) | (((UI_64)packet_ptr[2] & 0xFE) << 14);
					tshdr->adaptation_cfg.decoding_time_stamp_nau |= ((UI_64)packet_ptr[3] << 7) | (packet_ptr[4] >> 1);

					packet_ptr += 5;
				}
			}

			ts_obj.Release(pid_tab);
		}
	}

	return (result+1);
}


