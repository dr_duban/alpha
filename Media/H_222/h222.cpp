/*
** safe-fail video codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\SysUtils.h"
#include "Media\h222.h"


UI_64 Video::H222::DecodePacket(const unsigned char * packet_ptr) {
	UI_64 result(-1);
	PIDvector * pat_table(0);
	SFSco::Object pid_obj;

	switch (current_packet.id) { // ts global tables
		case 0: // pat
		case 1: // cat
		case 2: // decription table
		case 3: // IPMP
			pid_obj = ts_obj;
		break;

		case 4: case 5: case 6: case 7:
		case 8: case 9: case 10: case 11:
		case 12: case 13: case 14: case 15:
		
		break;

		default:
			if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
				for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
					if (current_packet.id == pat_table[i].pid_val) {
						pid_obj = pat_table[i].pid_obj;
						result = 1;
						if (pid_obj.GetTID() == program_tid) {
							if (ProgramHeader * phdr = reinterpret_cast<ProgramHeader *>(pid_obj.Acquire())) {
								if (current_packet.id == phdr->pcr_pid) result = 0;
								pid_obj.Release();
							}
						} 
						
						if (result) break;
					}
				}

				ts_obj.Release(pat_table);
			}
	
	}
	

	if (pid_obj) {
		switch (pid_obj.GetTID()) {
			case stream_tid:
				GetStreamData(packet_ptr, pid_obj);
			break;
			case program_tid: case _tid:
				GetTableSection(packet_ptr, pid_obj);
			break;
		}
	} else {
		// error
	}

	return result;
}

UI_64 Video::H222::GetTableSection(const unsigned char * packet_ptr, SFSco::Object & program_obj) {
	UI_64 result(0);

	if (current_packet.service_flags.start_indicator) {
	 // psi pointer
		current_packet.l_size -= (packet_ptr[0]+1);
		packet_ptr += (packet_ptr[0]+1);
		
	}
		

	if (unsigned char * temp_ptr = reinterpret_cast<unsigned char *>(temp_buf.Acquire())) {
		if (temp_offset) {
			if (payload_length > current_packet.l_size) {
				System::MemoryCopy(temp_ptr + temp_offset, packet_ptr, current_packet.l_size);
				temp_offset += current_packet.l_size;

				payload_length -= current_packet.l_size;

			} else {
				System::MemoryCopy(temp_ptr + temp_offset, packet_ptr, current_packet.l_size);
				payload_length += temp_offset;
				temp_offset = 0;
				packet_ptr = temp_ptr;
			}


		} else {			
			payload_length = ((packet_ptr[1] & 0x0F) << 8) | packet_ptr[2];
			
			if (payload_length > current_packet.l_size) {
				System::MemoryCopy(temp_ptr, packet_ptr, current_packet.l_size);
				temp_offset = current_packet.l_size;

				payload_length -= current_packet.l_size;
			}
		}


		if (!temp_offset) {
			payload_length -= 9; // - flags, sections ids, CRC32
		
			switch (current_packet.id) {
				case 0: // pat
					if (packet_ptr[0] == 0) {
						if ((result = payload_length >> 2) && ((payload_length & 3) == 0)) {

							if (ExpandMapTable(result) != -1) {										

								result = payload_length >> 2;

								for (;result--;) {
									AddProgram(packet_ptr+8);

									packet_ptr += 4;
			
								}					
						
							}

						// 4 byte crc
					
						} else {
						// error
						}
					}

				break;
				case 1: // cat
					if (packet_ptr[0] == 1) {


					}
				break;
				case 2: // description table
					if (packet_ptr[0] == 3) {


					}
				break;
				case 3: // IPMP
					if (packet_ptr[0] == 7) {


					}

				break;
				default:
					if (packet_ptr[0] < 0x40) {
						if (ExpandMapTable() != -1) ExtractProgramData(packet_ptr+8, program_obj);
						else {
							// error
						}
					} else {
						// private tables

					}

			}
		}

		temp_buf.Release();
	}

	return result;
}


UI_64 Video::H222::ExtractProgramData(const unsigned char * packet_ptr, SFSco::Object & program_obj) {
	UI_64 result(0);

	if (ProgramHeader * prg_hdr = reinterpret_cast<ProgramHeader *>(program_obj.Acquire())) {
		if (current_packet.id == prg_hdr->program_map_pid) {
			prg_hdr->pcr_pid = ((unsigned short)(packet_ptr[0] & 0x1F) << 8) | packet_ptr[1];

			AddMapVector(prg_hdr->pcr_pid, program_obj);

			result = ((UI_64)(packet_ptr[2] & 0x0F) << 8) | packet_ptr[3];
			packet_ptr += 4;

			if (payload_length >= (result + 4)) {
				payload_length -= (result + 4);

				for (;result;) {
					switch (packet_ptr[0]) {
						case 0xFF:


						default:
							result -= (packet_ptr[1]+2);
							packet_ptr += (packet_ptr[1]+2);
						

					}

				}

				program_obj.Release();
				result = 0;
				for (;payload_length;) {
					if (ExpandMapTable() != -1) {
						result = AddStream(packet_ptr, program_obj);
						if (result != -1) {
							payload_length -= result;
							packet_ptr += result;
						} else {
						// error

					
						}
					} else {
					// error
					}

				}
			} else {
				// error	
			}

		} else {

			program_obj.Release();
		}

		
		
		
	}


	return result;
}

UI_64 Video::H222::DataSwitch(StreamHeader * str_hdr, const unsigned char * temp_ptr) {
	UI_64 result(0);

	switch (str_hdr->id & 0xE0) {			
		case 0xA0:
			switch (str_hdr->id & 0x1F) {
				case 0x1A: // pes header
								
				break;
				case 0x1B: // system header

				break;
				case 0x1C: // 12 |  program stream map

				break;
				case 0x1D: // 13 | private stream 1
					ExtractStreamData(str_hdr, temp_ptr);
				break;
				case 0x1E: // 14 | padding stream
											
				break;
				case 0x1F: // 15 | private stream 2

				break;

			}
		break;
		case 0xC0: // 48 + xxxxx | 13818-3, 11172-3, 13818-7, 14496-3 audio stream number (low 5 bits)
			ExtractStreamData(str_hdr, temp_ptr);
		break;
		case 0xE0:
			switch (str_hdr->id & 0x1F) {
				case 0x10: // 16 | ECM stream
				case 0x11: // 17 | EMM stream
				case 0x12: // 18 | H.222.0, 13818-1 A, 13818-6 DSMCC
				case 0x18: // 24 | H.222.1 type E
				case 0x1F: // 31 | program stream directory
					// data bytes

				break;					

				default:
					ExtractStreamData(str_hdr, temp_ptr);

/*					
				case 0x13: // 19 | 13522 stream
				case 0x14: // 20 | H.222.1 type A
				case 0x15: // 21 | H.222.1 type B
				case 0x16: // 22 | H.222.1 type C
				case 0x17: // 23 | H.222.1 type D
				case 0x19: // 25 | ancillary stream
				case 0x1A: // 26 | 14496-1 SL
				case 0x1B: // 27 | 14496-1 FlexMux
				case 0x1C: // 28 | metadata stream
				case 0x1D: // 29 | extended stream id
				case 0x1E: // 30 | erserved data stream
					
					break;
							
				default:
					if ((str_hdr->id & 0xF0) == 0xE0) {  // 32 + xxxx | H.262, 13818-2, 11172-2, 14496-2, H.264, 14496-10 video stream number (low 4 bits)
						ExtractStreamData(str_hdr, temp_ptr);

					}
*/
			}
		break;

	}			

	return result;
}

UI_64 Video::H222::GetStreamData(const unsigned char * packet_ptr, SFSco::Object & stream_obj) {
	UI_64 result(0);
	unsigned char * temp_ptr(0);
	StreamHeader * str_hdr(0);

	if (str_hdr = reinterpret_cast<StreamHeader *>(stream_obj.Acquire())) {
		if ((str_hdr->temp_offset + 256) > str_hdr->temp_size) {
			result = 1;
		}
		stream_obj.Release();
	}
	
	if (result)
	if (stream_obj.Expand(65536) != -1) {
		if (str_hdr = reinterpret_cast<StreamHeader *>(stream_obj.Acquire())) {
			str_hdr->temp_size += 65536;
			stream_obj.Release();
		}
	} else {
			// error
	}

	if (str_hdr = reinterpret_cast<StreamHeader *>(stream_obj.Acquire((void **)&temp_ptr))) {
		if ((str_hdr->temp_offset + current_packet.l_size) <= str_hdr->temp_size) {
			if (str_hdr->last_search_state != search_idx) {
				str_hdr->last_search_state = search_idx;

				str_hdr->payload_length = 0;
				str_hdr->temp_offset = 0;
			}


			if (str_hdr->temp_offset) {
				if (!current_packet.service_flags.start_indicator) {
					if (str_hdr->payload_length) {
						if (str_hdr->payload_length > current_packet.l_size) {
							System::MemoryCopy(temp_ptr + str_hdr->temp_offset, packet_ptr, current_packet.l_size);
							str_hdr->temp_offset += current_packet.l_size;

							str_hdr->payload_length -= current_packet.l_size;

						} else {
							System::MemoryCopy(temp_ptr + str_hdr->temp_offset, packet_ptr, current_packet.l_size);
							str_hdr->payload_length = str_hdr->temp_offset+current_packet.l_size;
							str_hdr->temp_offset = 0;
//						packet_ptr = temp_ptr;
						}
					} else {
						System::MemoryCopy(temp_ptr + str_hdr->temp_offset, packet_ptr, current_packet.l_size);
						str_hdr->temp_offset += current_packet.l_size;
					}
	
				} else {
					str_hdr->payload_length = str_hdr->temp_offset;

					str_hdr->temp_offset = 0;
//				packet_ptr = temp_ptr;
				}
			}

			if (str_hdr->temp_offset == 0) {
				if (str_hdr->payload_length > 3) {
					DataSwitch(str_hdr, temp_ptr);
				}
	

				if (current_packet.service_flags.start_indicator) {
					str_hdr->payload_length = 0;
							
					if ((packet_ptr[0] == 0) && (packet_ptr[1] == 0) && (packet_ptr[2] == 1)) {	
						str_hdr->id = packet_ptr[3];
						str_hdr->payload_length = ((UI_64)packet_ptr[4] << 8) | packet_ptr[5]; // PES packet length
						packet_ptr += 6;

						current_packet.l_size -= 6;

						if ((str_hdr->payload_length == 0) && ((str_hdr->id & 0xE0) != 0xE0)) {
						// error

						}


						if ((str_hdr->payload_length > current_packet.l_size) || (str_hdr->payload_length == 0)) {
							System::MemoryCopy(temp_ptr, packet_ptr, current_packet.l_size);
							str_hdr->temp_offset = current_packet.l_size;

							if (str_hdr->payload_length) str_hdr->payload_length -= current_packet.l_size;
											
						} else {						
							DataSwitch(str_hdr, packet_ptr);
						}
					}
				}
			}
		} else {
			result = 'ups';
		}

		stream_obj.Release(temp_ptr);
	}

	return result;
}




UI_64 Video::H222::ExtractStreamData(StreamHeader * str_hdr, const unsigned char * packet_ptr) {
	UI_64 result(0), r_val(0);
	I_64 t_val(0);
	const unsigned char * p_ptr(0);
	PES_flags sflags;
	TransportPacket::XtentionFlags xflags;

	if ((packet_ptr[0] & 0xC0) == 0x80) {
		sflags._bvsl[0] = packet_ptr[0];
		sflags._bvsl[1] = packet_ptr[1];

		result = packet_ptr[2];

		packet_ptr += 3;
		str_hdr->payload_length -= 3;

		switch (sflags.PTS_DTS) {
			case 2:
				str_hdr->presentation_time_stamp = ((unsigned __int64)(packet_ptr[0] & 0x0E) << 29) | ((UI_64)packet_ptr[1] << 22) | (((UI_64)packet_ptr[2] & 0xFE) << 14) | ((UI_64)packet_ptr[3] << 7) | ((UI_64)packet_ptr[4] >> 1);
				packet_ptr += 5;
				str_hdr->payload_length -= 5;
				result -= 5;
			break;
			case 3:
				str_hdr->presentation_time_stamp = ((unsigned __int64)(packet_ptr[0] & 0x0E) << 29) | ((UI_64)packet_ptr[1] << 22) | (((UI_64)packet_ptr[2] & 0xFE) << 14) | ((UI_64)packet_ptr[3] << 7) | ((UI_64)packet_ptr[4] >> 1);
				packet_ptr += 5;				
				str_hdr->decoding_time_stamp = ((unsigned __int64)(packet_ptr[0] & 0x0E) << 29) | ((UI_64)packet_ptr[1] << 22) | (((UI_64)packet_ptr[2] & 0xFE) << 14) | ((UI_64)packet_ptr[3] << 7) | ((UI_64)packet_ptr[4] >> 1);
				packet_ptr += 5;				
				str_hdr->payload_length -= 10;
				result -= 10;
			break;
			
		}
		

		if (sflags.ESCR) {
			str_hdr->elementary_stream_clock_reference = ((unsigned __int64)(packet_ptr[0] & 0x38) << 27) | ((UI_64)(packet_ptr[0] & 3) << 28) | ((UI_64)packet_ptr[1] << 20) | ((UI_64)(packet_ptr[2] & 0xF8) << 12) | ((UI_64)(packet_ptr[2] & 3) << 13) | ((UI_64)packet_ptr[3] << 5) | ((UI_64)packet_ptr[4] >> 3);
			t_val = ((UI_64)(packet_ptr[4] & 3) << 7) | (packet_ptr[5] >> 7);
			packet_ptr += 6;
			str_hdr->payload_length -= 6;
			result -= 6;

			str_hdr->elementary_stream_clock_reference *= 300;
			str_hdr->elementary_stream_clock_reference += t_val;
		}

		if (sflags.ES_rate) {
			str_hdr->elementary_stream_rate = (((UI_64)packet_ptr[0] & 0x7F) << 15) | ((UI_64)packet_ptr[1] << 7) | ((UI_64)packet_ptr[2] >> 1);
			packet_ptr += 3;
			str_hdr->payload_length -= 3;
			result -= 3;
		}

		if (sflags.DSM_trick_mode) {
			str_hdr->trick.type = packet_ptr[0] >> 5;
			switch (str_hdr->trick.type) {
				case 0: // fast forward
				case 3: // fast reverse
					str_hdr->trick.field_id = (packet_ptr[0] >> 3) & 3;
					str_hdr->trick.intra_slice_refresh = packet_ptr[0] & 4;
					str_hdr->trick.frequency_truncation = packet_ptr[0] & 3;
				break;
				case 1: // slow motion
				case 4: // slow reverse
					str_hdr->trick.field_repeat_count = packet_ptr[0] & 0x1F;
				break;
				case 2: // freeze frame
					str_hdr->trick.field_id = (packet_ptr[0] >> 3) & 3;
				break;
			}

			packet_ptr++;
			str_hdr->payload_length--;
			result--;
		}

		if (sflags.additional_copyright) {


			packet_ptr++;
			str_hdr->payload_length--;
			result--;
		}

		if (sflags.CRC) {
			str_hdr->previous_packet_CRC = ((unsigned short)packet_ptr[0] << 8) | packet_ptr[1];
			packet_ptr += 2;
			str_hdr->payload_length -= 2;
			result -= 2;
		}

		if (sflags.extension) {
			xflags._bval = packet_ptr[0];
			packet_ptr++;
			str_hdr->payload_length--;
			result--;

			if (xflags.private_data) {

				packet_ptr += 16;
				str_hdr->payload_length -= 16;
				result -= 16;
			}

			if (xflags.pack_header_field) {
				r_val = packet_ptr[0];
				p_ptr = packet_ptr + 1;
				// pes header
				if ((packet_ptr[1] == 0) && (packet_ptr[2] == 0) && (packet_ptr[3] == 1) && (packet_ptr[4] == pes_header_code)) {
					packet_ptr += 5;

					str_hdr->pes_header.system_clock = ((unsigned __int64)(packet_ptr[0] & 0x38) << 27) | ((UI_64)(packet_ptr[0] & 3) << 28) | ((UI_64)packet_ptr[1] << 20) | ((UI_64)(packet_ptr[2] & 0xF8) << 14) | ((UI_64)(packet_ptr[2] & 3) << 13) | ((UI_64)packet_ptr[3] << 5) | (packet_ptr[4] >> 3);
					str_hdr->pes_header.system_clock *= 300;
					str_hdr->pes_header.system_clock += ((UI_64)(packet_ptr[4] & 3) << 7) | (packet_ptr[5] >> 1);
					
					str_hdr->pes_header.mux_rate = ((UI_64)packet_ptr[6] << 14) | ((UI_64)packet_ptr[7] << 6) | (packet_ptr[8] >> 2);
					t_val = (packet_ptr[9] & 7);

					packet_ptr += (10+t_val);

					if (result > (19+t_val)) {
						if ((packet_ptr[0] == 0) && (packet_ptr[1] == 0) && (packet_ptr[2] == 1) && (packet_ptr[3] == system_header_code)) {
							packet_ptr += 4;


						}
					}
				}

				packet_ptr = p_ptr + r_val;
				str_hdr->payload_length -= (r_val+1);
				result -= (r_val+1);
			}

			if (xflags.sequence_counter) {
				str_hdr->extension.iso_11172._bvsl[0] = packet_ptr[0];
				str_hdr->extension.iso_11172._bvsl[1] = packet_ptr[1];

				packet_ptr += 2;
				str_hdr->payload_length -= 2;
				result -= 2;
			}

			if (xflags.pSTD_buffer) {
				if (packet_ptr[0] & 2) str_hdr->extension.pstd_buffer_size = 1024;
				else str_hdr->extension.pstd_buffer_size = 128;
				str_hdr->extension.pstd_buffer_size *= ((UI_64)(packet_ptr[0] & 0x1F) | packet_ptr[1]);

				packet_ptr += 2;
				str_hdr->payload_length -= 2;
				result -= 2;
			}

			if (xflags.x2) { // applyies only to extended strem 0xFD
				r_val = (packet_ptr[0] & 0x7F) + 1;


				packet_ptr += r_val;
				str_hdr->payload_length -= r_val;
				result -= r_val;
			}
		}

	
		if (result < 256) {
			packet_ptr += result;
			str_hdr->payload_length -= result;
			result = 0;
		}

		if (result == 0) {
// possible data bytes
			if (str_hdr->payload_length) {
				if (str_hdr->_sequence) {					
					t_val = str_hdr->presentation_time_stamp/9;


					if (search_mode == 11) {
						SetAudioVolume(reinterpret_cast<double *>(&search_mark)[0]);
						search_mode = 0;
					}


					if (t_val == start_mark) {
// FlagSSNext			
						if (terminator > 1)
						if (pending_req.element_obj) {
							pending_req.flags = OpenGL::DecodeRecord::FlagSSNext;
							pending_req.pic_id = -1;
							Decode(pending_req);
						}

						search_mark = start_mark;
		
						NVReset();

						search_mode = 2;
					}

					if (str_hdr->_sequence->IsVideo()) {
						if (search_mode == 2) {
							if ((t_val >= search_mark) || (program_block[1])) {
								search_mode = 0;
								pts[0] = pts[1] = 0;

								str_hdr->_sequence->JumpOn();
							}
						}
					} else {
						if (temp_vid) {
							if (str_hdr->_sequence->IsAudio()) {
								if (sync_start == -1) sync_start = t_val;
							}
						}
					}

					if (!search_mode) {
						result = str_hdr->_sequence->Decode(packet_ptr, str_hdr->payload_length, t_val);											
						
						if (result >= RESULT_DECODE_COMPLETE) {
							if (result & RESULT_DECODE_COMPLETE) {
								if (str_hdr->_sequence->IsVideo()) {

									if (program_block[1]) {
										SetSTP();

									} else {
										if (terminator) terminator = 2;

									
										if ((UI_64)str_hdr->_sequence == temp_vid) {
											current_time = t_val;
										}

										if (pending_req.program) {											

											if (GetVSource(pending_req.pic_id)) {
												pending_req.flags |= OpenGL::DecodeRecord::FlagSourceOn;
												Decode(pending_req);

												pending_req.program = 0;
											}
										}

										if (sync_start >= 0) {
											NVFlush();
										}
																												
										if (SetPTS(t_val) < 0) {
											str_hdr->_sequence->SetRenderSkip(1);										
										}
									}
								}
							} else {
								result >>= 32;
/*						
						case -3: // end of source
						case -4: // codec is not set
						case -6: // slice header is missing
						case -91: case -92: // damaged bit stream
						case -94: // slice parsing error
*/


							}

						}
/*						
						else {
						// test for pending source select, send decode record on success

							if (pending_req.program) {
								if (GetVSource(pending_req)) {
									pending_req.flags = OpenGL::DecodeRecord::FlagSourceOn;
									Media::Program::Decode(pending_req);

									pending_req.program = 0;
								}
							}
						}
*/
					}
				}
/*
				for (;str_hdr->payload_length;) {

					str_hdr->payload_length--;
				}
*/

				str_hdr->payload_length = 0;
			}
		} else {
			// error
		}

	}


	return result;
}


// transport stream ===============================================================================================================
Memory::Allocator * Video::H222::ts_pool = 0;

UI_64 Video::H222::Initialize() {
	wchar_t pool_name[] = {9, L'h',L'2', L'2', L'2', L' ', L'p', L'o', L'o', L'l'};
	UI_64 result(0);
	if (ts_pool) return -1;

	if (ts_pool = new Memory::Allocator(0)) {
		result = ts_pool->Init(new Memory::Buffer(), 0x040000, pool_name);
	} else result = ERROR_OUTOFMEMORY;

	

	return result;
}

UI_64 Video::H222::Finalize() {
	if (ts_pool) delete ts_pool;
	ts_pool = 0;

	return 0;
}


Video::H222::H222(unsigned int pflag) : Program(pflag), reciever_thread(0), terminator(1), temp_size(8192), temp_offset(0), payload_length(0), packet_counter(0), no_video(1), temp_vid(0), ts_type(188) {
	
}

Video::H222::~H222() {

	terminator = 0;

	if (program_block[0]) {
		SetEvent(program_block[0]);
		if (reciever_thread) {
			WaitForSingleObject(reciever_thread, INFINITE);	
			CloseHandle(reciever_thread);
		}
	}


	ts_obj.Destroy();
	temp_buf.Destroy();
	code_buf.Destroy();

//	if (source) delete source;
	source.Destroy();
}

bool Video::H222::IsT222(Data::Stream & src) {
	UI_64 b_size(1024);
	bool result(false);
	
	if (unsigned char * src_ptr = src.MoveLock(b_size, (UI_64)0)) {		
		result = (((src_ptr[0] == sync_byte) && (src_ptr[188] == sync_byte) && (src_ptr[940] == sync_byte)) || ((src_ptr[4] == sync_byte) && (src_ptr[192+4] == sync_byte) && (src_ptr[960+4] == sync_byte))); // sync byte

	}

	return result;
}


Media::Program * Video::H222::SetSource(const Data::Stream & src, unsigned int preview_flag) {
	UI_64 eval(-1);
	Video::H222 * result(0);
	
	if (result = new H222(preview_flag)) {
		eval = result->ts_obj.New(_tid, ts_header_offset+32*sizeof(PIDvector), ts_pool);
		eval |= result->temp_buf.New(0, result->temp_size);

		if (eval != -1) {
			if (TransportHeader * tshdr = reinterpret_cast<TransportHeader *>(result->ts_obj.Acquire())) {
//				System::MemoryFill(tshdr, ts_header_offset, 0);
				tshdr->pid_top = 32;

				result->ts_obj.Release();

				result->ts_obj.Set(&result->ts_obj, ts_header_offset);

				result->source = src;

				eval = 0;
				if (result->program_block[0]) {
					if (result->reciever_thread = CreateThread(0, 0, ReceiverLoop, result, 0, 0)) {

					} else {
						eval = GetLastError();
					}

				} else {
					eval = -1;
				}
			}
		}

		if (eval) {
			delete result;
			result = 0;
		}
	}
	
	return result;
}

void Video::H222::NVFlush() {	
	PIDvector * pat_table(0);
	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if (pat_table[i].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
					if (str_hdr->_sequence) {
						if (!str_hdr->_sequence->IsVideo()) {
							str_hdr->_sequence->Flush(current_time, sync_start);
						}
					}
					pat_table[i].pid_obj.Release();
				}
			}
		}
		
		ts_obj.Release(pat_table);
	}
		
	sync_start = -1;
}

void Video::H222::NVReset() {
	PIDvector * pat_table(0);
	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if (pat_table[i].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
					if (str_hdr->_sequence) {
						if (!str_hdr->_sequence->IsVideo()) {
							str_hdr->_sequence->Reset();
						}
					}
					pat_table[i].pid_obj.Release();
				}
			}
		}
		
		ts_obj.Release(pat_table);
	}
	
	sync_start = -1;
}

unsigned int Video::H222::EnableAudioSource(unsigned int a_sel, const OpenGL::Element & a_plane) {
	PIDvector * pat_table(0);
	if (a_sel < audio.count) {
		if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
			for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
				if (pat_table[i].pid_obj.GetTID() == stream_tid) {
					if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
						if (str_hdr->_sequence) {
							if (str_hdr->_sequence->IsAudio()) {
								if ((audio.flags & AUDIO_SOURCE_EXCLUSIVE) && (a_sel != 0)) {
									str_hdr->_sequence->ResetSat();
										str_hdr->_sequence->Off();
									}
									if (a_sel-- == 0) {
										str_hdr->_sequence->On();
										str_hdr->_sequence->ResetSatRecord(a_plane);
									}
								}
							}
						pat_table[i].pid_obj.Release();
					}
				}
			}		
			ts_obj.Release(pat_table);
		}
	}

	return a_sel;
}

unsigned int Video::H222::IsAudioActive(unsigned int a_sel) {
	PIDvector * pat_table(0);
	unsigned int result(0);

	if (a_sel < audio.count) {
		if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
			for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
				if (pat_table[i].pid_obj.GetTID() == stream_tid) {
					if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
						if (str_hdr->_sequence) {
							if (str_hdr->_sequence->IsAudio()) {						
								if (a_sel-- == 0) {
									result = str_hdr->_sequence->TestFlag(SOURCE_NO_RENDER);
									break;
								}
							}
						}
						pat_table[i].pid_obj.Release();
					}
				}
			}		
			ts_obj.Release(pat_table);
		}
	}

	return result;
}

unsigned int Video::H222::GetAudioLNG(unsigned int a_sel) {
	PIDvector * pat_table(0);
	unsigned int result(a_sel);

	if (a_sel < audio.count) {
		if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
			for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
				if (pat_table[i].pid_obj.GetTID() == stream_tid) {
					if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
						if (str_hdr->_sequence) {
							if (str_hdr->_sequence->IsAudio()) {						
								if (a_sel-- == 0) {									
									a_sel = str_hdr->_sequence->GetLNGID();
									if (a_sel != -1) result = a_sel;

									break;
								}
							}
						}
						pat_table[i].pid_obj.Release();
					}
				}
			}		
			ts_obj.Release(pat_table);
		}
	}

	return result;
}

unsigned int Video::H222::DisableAudioSource(unsigned int a_sel) {
	


	return a_sel;
}

void Video::H222::SetAudioVolume(float a_val) {
	PIDvector * pat_table(0);
	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if (pat_table[i].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
					if (str_hdr->_sequence) {
						if (str_hdr->_sequence->IsAudio()) {
							str_hdr->_sequence->SetVolume(&a_val);
						}
					}
					pat_table[i].pid_obj.Release();
				}
			}
		}
		
		ts_obj.Release(pat_table);
	}
}




Media::Source * Video::H222::GetVSource(UI_64 & p_id) {	
	PIDvector * pat_table(0);
	unsigned int s_val(0);

	Media::Source * result(0);

	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
		if (p_id >= ts_hdr->pid_runner) p_id = 0;
		s_val = p_id;

		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if (pat_table[i].pid_obj)
			if (pat_table[i].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
					if (str_hdr->_sequence) {
						if (str_hdr->_sequence->IsVideo())
						if (s_val-- == 0) {								
							result = str_hdr->_sequence;
						}
					}
					pat_table[i].pid_obj.Release();
				}
			}
			if (result) break;
		}
		
		ts_obj.Release(pat_table);
	} else {
		// set pending selct source flag

	}

	return result;
}


Media::Source * Video::H222::GetASource(UI_64 & p_id) {	
	PIDvector * pat_table(0);
	unsigned int s_val(0);

	Media::Source * result(0);

	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pat_table))) {
		if (p_id >= ts_hdr->pid_runner) p_id = 0;
		s_val = p_id;

		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if (pat_table[i].pid_obj)
			if (pat_table[i].pid_obj.GetTID() == stream_tid) {
				if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(pat_table[i].pid_obj.Acquire())) {
					if (str_hdr->_sequence) {
						if (str_hdr->_sequence->IsAudio())
						if (s_val-- == 0) {								
							result = str_hdr->_sequence;
						}
					}
					pat_table[i].pid_obj.Release();
				}
			}
			if (result) break;
		}
		
		ts_obj.Release(pat_table);
	} else {
		// set pending selct source flag

	}

	return result;
}

/*
Video::Source * Video::H222::SelectSource(unsigned int s_id) {	
	Source * result(0);

	if (s_id < source_count) {
		current_source = s_id;
		result = GetCurrentSource();
	}

	return result;
}
*/

UI_64 Video::H222::AddProgram(const unsigned char * packet_ptr) {
	SFSco::Object program_obj;	
	UI_64 oval = program_obj.New(program_tid, p_header_offset, ts_pool);

	if (oval != -1) {
		if (ProgramHeader * p_header = reinterpret_cast<ProgramHeader *>(program_obj.Acquire())) {
//			System::MemoryFill(p_header, p_header_offset, 0);

			p_header->number = ((UI_64)packet_ptr[0] << 8) | packet_ptr[1];
			p_header->program_map_pid = (((UI_64)packet_ptr[2] & 0x1F) << 8) | packet_ptr[3];
			p_header->pcr_pid = 0x1FFF;

			program_obj.Release();

			if (AddMapVector(p_header->program_map_pid, program_obj) == 0) {
				program_obj.Destroy();
			}			
		}
	}

	return oval;
}

UI_64 Video::H222::AddStream(const unsigned char * packet_ptr, SFSco::Object & program_obj) {
	UI_64 result(0), xle(0), nexister(0);
	SFSco::Object stream_obj;

	result = stream_obj.New(stream_tid, s_header_offset + 65536, ts_pool);
	if (result != -1) {
		stream_obj.Set(&stream_obj, s_header_offset);
		if (StreamHeader * str_hdr = reinterpret_cast<StreamHeader *>(stream_obj.Acquire())) {
//			System::MemoryFill(str_hdr, s_header_offset, 0);
			
			str_hdr->_type = packet_ptr[0];
			str_hdr->PID[0] = ((unsigned short)(packet_ptr[1] & 0x1F) << 8) | packet_ptr[2];
			str_hdr->temp_size = 65536;

			nexister = AddMapVector(str_hdr->PID[0], stream_obj);

			xle = ((UI_64)(packet_ptr[3] & 0x0F) << 8) | packet_ptr[4];

			result = 5 + xle;
			packet_ptr += 5;

			if (nexister) {
				if (ProgramHeader * phdr = reinterpret_cast<ProgramHeader *>(program_obj.Acquire())) {
					phdr->pid_list[phdr->list_length++] = str_hdr->PID[0];

					program_obj.Release();
				}

				for (;xle;) {
					switch (packet_ptr[0]) {
						case 5:
							switch (packet_ptr[1]) {
								case 4:
									if (((packet_ptr[2]=='H') && (packet_ptr[3]=='E') && (packet_ptr[4]=='V') && (packet_ptr[5]=='C')) ||
										((packet_ptr[3]=='2') && (packet_ptr[4]=='6') && (packet_ptr[5]=='5')))	{

										str_hdr->_type = 0x24;

									} else {
										if (((packet_ptr[2]=='A') && (packet_ptr[3]=='V') && (packet_ptr[4]=='C')) ||
											((packet_ptr[3]=='2') && (packet_ptr[4]=='6') && (packet_ptr[5]=='4')))	{

											str_hdr->_type = 0x1B;

										}
									}


								break;

							}
							xle -= (packet_ptr[1] + 2);
							packet_ptr += (packet_ptr[1] + 2);

						break;
						default:
							xle -= (packet_ptr[1] + 2);
							packet_ptr += (packet_ptr[1] + 2);

					}
				// descriptors

				}

				if (str_hdr->_sequence) {
					delete str_hdr->_sequence;
					str_hdr->_sequence = 0;

					pts[0] = pts[1] = 0;
				}

				if (str_hdr->_sequence = Media::CodedSource::Create(str_hdr->_type)) {
					if (str_hdr->_sequence->IsVideo()) {
						no_video = 0;

						if (++video.count == 1) {
							str_hdr->_sequence->On();

							temp_vid = (UI_64)str_hdr->_sequence;
						}

						if (program_block[1]) str_hdr->_sequence->SetRenderSkip(-1);
					}

					if (str_hdr->_sequence->IsAudio()) {
						if (program_block[1] == 0)
						if (++audio.count == 1) str_hdr->_sequence->On();
					}					
				}				
			}

			stream_obj.Release();
			if (nexister == 0) {
				stream_obj.Destroy();
			}
		}
	}

	return result;
}

UI_64 Video::H222::ExpandMapTable(UI_64 xcount) {

	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire())) {
		if ((xcount + ts_hdr->pid_runner) < ts_hdr->pid_top) {
			xcount = 0;
		}
		ts_obj.Release();
	}

	if (xcount) {
		xcount = ts_obj.Expand((((xcount>>5) + 1)<<5)*sizeof(PIDvector));

		if (xcount != -1)
		if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire())) {
			ts_hdr->pid_top += (((xcount>>5) + 1)<<5);

			ts_obj.Release();
		}

	}
	
	return xcount;
}

UI_64 Video::H222::AddMapVector(unsigned short pid, const SFSco::Object & pid_obj) {
	PIDvector * pid_tab(0);

	if (TransportHeader * ts_hdr = reinterpret_cast<TransportHeader *>(ts_obj.Acquire((void **)&pid_tab))) {
		for (UI_64 i(0);i<ts_hdr->pid_runner;i++) {
			if ((pid == pid_tab[i].pid_val) && (pid_obj.GetTID() == pid_tab[i].pid_obj.GetTID())) {
				pid = 0;
				break;
			}
		}

		if (pid) {
			pid_tab[ts_hdr->pid_runner].pid_val = pid;
			pid_tab[ts_hdr->pid_runner++].pid_obj = pid_obj;
		}

		ts_obj.Release(pid_tab);
	}

	return pid;
}


