/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\JPEG.h"
#include "System\SysUtils.h"

#include "Math\CalcSpecial.h"

#pragma warning(disable:4244)

__declspec(align(16)) short Pics::JPEG::idct_cofs[8][8][64];
const unsigned char Pics::JPEG::zz_sequence[] = {	
	
													0, 1, 5, 6, 14, 15, 27, 28,
													2, 4, 7, 13, 16, 26, 29, 42,
													3, 8, 12, 17, 25, 30, 41, 43,
													9, 11, 18, 24, 31, 40, 44, 53,
													10, 19, 23, 32, 39, 45, 52, 54,
													20, 22, 33, 38, 46, 51, 55, 60,
													21, 34, 37, 47, 50, 56, 59, 61,
													35, 36, 48, 49, 57, 58, 62, 63,
	
	
													0, 1, 8, 16, 9, 2, 3, 10,
													17, 24, 32, 25, 18, 11, 4, 5,
													12, 19, 26, 33, 40, 48, 41, 34,
													27, 20, 13, 6, 7, 14, 21, 28,
													35, 42, 49, 56, 57, 50, 43, 36,
													29, 22, 15, 23, 30, 37, 44, 51,
													58, 59, 52, 45, 38, 31, 39, 46,
													53, 60, 61, 54, 47, 55, 62, 63

												};



Pics::JPEG::JPEG() : lines_count(0) {

	_currentFrame.ecs_type = 0;
	System::MemoryFill(&_decoder, sizeof(Decoder), 0);
/*
	for (unsigned int i(0);i<2;i++) {
		for (unsigned int j(0);j<4;j++) {
			for (unsigned int k(0);k<16;k++) {
				_decoder.h_table[i][j][k] = 0;
			}
		}
	}
*/
}

Pics::JPEG::~JPEG() {
	for (unsigned int i(0);i<2;i++) {
		for (unsigned int j(0);j<4;j++) {			
			if (_decoder.h_table[i][j][0]) free(_decoder.h_table[i][j][0]);
		}
	}

	if (f_domain) f_domain.Destroy();
}


UI_64 Pics::JPEG::LoadQuantizationTable(const unsigned char * ipo) {
	UI_64 result(ipo[2]), selector(0);
	int counter(0);
	
	result <<= 8;
	result += ipo[3] + 2;

	ipo += 4;

	counter = result;
	counter -= 4;

	for (;counter>0;) {
		if (counter < 65) {
			result = -1;
			break;
		}

		selector = ipo[0] & 3;
	
		if (ipo[0] >> 4) { // short
			++ipo;

			if (counter < 129) {
				result = -2;
				break;
			}

			for (unsigned int i(0);i<64;i++) {
				_decoder.q_table[selector][i] = ipo[2*i];
				_decoder.q_table[selector][i] <<= 8;
				_decoder.q_table[selector][i] |= ipo[2*i+1];
			}
			
			counter -= 64;
			ipo += 64;			

		} else {
			++ipo;
			
			for (unsigned int i(0);i<64;i++) _decoder.q_table[selector][i] = ipo[i];
			
		}
				
		counter -= 65;
		ipo += 64;
	}

	return result;
}

UI_64 Pics::JPEG::LoadHuffmanTable(const unsigned char * ipo) {
	UI_64 result(ipo[2]), ttype(0), tdest(0);
	int counter(0), hlength(0);
	unsigned short hcode(0), icount;

	result <<= 8;
	result += ipo[3] + 2;
	
	ipo += 4;

	counter = result;
	counter -= 4;

	for (;counter > 0;) {
		if (counter < 17) {
			result = -1;
			break;
		}

		ttype = ipo[0] >> 4;
		tdest = ipo[0] & 3;

		if (_decoder.h_table[ttype][tdest][0]) free(_decoder.h_table[ttype][tdest][0]);
		
		for (unsigned int i(0);i<16;i++) {
			_decoder.h_table[ttype][tdest][i] = 0;
			_decoder.h_codes[ttype][tdest][2*i] = 0;
			_decoder.h_codes[ttype][tdest][2*i+1] = 0;
		}

		hlength = 0; ipo++; hcode = 0;
		for (unsigned int i(0);i<16;i++) {
			_decoder.h_codes[ttype][tdest][2*i+1] = hcode;

			icount = ipo[i];
			hcode += icount;
			
			_decoder.h_codes[ttype][tdest][2*i] = hcode;

			hcode <<= 1;

			hlength += icount;
		}

		counter -= 17;

		if (counter < hlength) {
			result = -2;
			break;
		}

		if (_decoder.h_table[ttype][tdest][0] = reinterpret_cast<unsigned char *>(malloc(hlength))) {
			memcpy(_decoder.h_table[ttype][tdest][0], ipo+16, hlength);
			for (unsigned int i(1);i<16;i++) _decoder.h_table[ttype][tdest][i] = _decoder.h_table[ttype][tdest][i-1] + ipo[i-1];
		
		} else {
			break; // error
		}

		counter -= hlength;
		ipo += (16 + hlength);
	}

	return result;
}

UI_64 Pics::JPEG::LoadFrameHeader(const unsigned char * ipo) {
	UI_64 result(ipo[2]);
	result <<= 8;
	result += ipo[3] + 2;

	_currentFrame.ecs_type &= 0x80;

	switch (ipo[1]) {
		case 0xC0: case 0xC1: case 0xC2: case 0xC3:	case 0xC5: case 0xC6: case 0xC7: // huffman
			_currentFrame.ecs_type |= (0x10 | (ipo[1] & 0x0F));
		break;
		case 0xC9: case 0xCA: case 0xCB: case 0xCD: case 0xCE: case 0xCF: // arithmetic
			_currentFrame.ecs_type |= (0x20 | (ipo[1] & 0x0F));
		break;
		case 0xDE: // h mode
			_currentFrame.ecs_type = 0x80;
		break;	
	}
	

	ipo += 4;

	_currentFrame.bit_precision = ipo[0];
	_currentFrame.s_reserved = (1 << (_currentFrame.bit_precision-1)) - 1;
	_currentFrame.precision = (ipo[0]+7)/8;

	_currentFrame.level_shift = _currentFrame.precision*8 - ipo[0];

	_currentFrame.height = ipo[1];
	_currentFrame.height <<= 8;
	_currentFrame.height |= ipo[2];

	_currentFrame.width = ipo[3];
	_currentFrame.width <<= 8;
	_currentFrame.width |= ipo[4];

	_currentFrame.componentsCount = ipo[5];

	_currentFrame.max_hf = _currentFrame.max_vf = 1;
	

	ipo += 6;

	for (unsigned char i(0);i<_currentFrame.componentsCount;i++) {
		_currentFrame.component[i].f_offset = -1;

		_currentFrame.component[i].id = ipo[0];
				
		_currentFrame.component[i].h_factor = ipo[1] >> 4;
		_currentFrame.component[i].v_factor = ipo[1] & 0x0F;
		_currentFrame.component[i].qt_selector = ipo[2] & 3;

		if (_currentFrame.component[i].h_factor > _currentFrame.max_hf) _currentFrame.max_hf = _currentFrame.component[i].h_factor;
		if (_currentFrame.component[i].v_factor > _currentFrame.max_vf) _currentFrame.max_vf = _currentFrame.component[i].v_factor;

		ipo += 3;
	}

	if (((_currentFrame.ecs_type & 0x0F) < 2) && (_currentFrame.componentsCount == 1)) {
		_currentFrame.max_hf = _currentFrame.max_vf = 1;

		_currentFrame.component[0].h_factor = _currentFrame.component[0].h_factor = 1;
	}

	_currentFrame.c_height = (_currentFrame.height + (_currentFrame.max_vf<<3) - 1)/(_currentFrame.max_vf<<3);
	_currentFrame.c_width = (_currentFrame.width + (_currentFrame.max_hf<<3) - 1)/(_currentFrame.max_hf<<3);

	_currentFrame.c_height *= (_currentFrame.max_vf<<3);
	_currentFrame.c_width *= (_currentFrame.max_hf<<3);

	_currentFrame.c_height = (_currentFrame.c_height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
	_currentFrame.c_width = (_currentFrame.c_width + (1<<size_paragraph) - 1) & (-1 << size_paragraph);

	_currentFrame.line_offset = _currentFrame.c_width*_currentFrame.precision*4;

	return result;
}

void Pics::JPEG::GetFrameComponent(unsigned char dcs, unsigned char acs, ScanHeader::ComponentSet & cset) {
	for (unsigned char i(0);i<_currentFrame.componentsCount;i++) {
		if (_currentFrame.component[i].id == cset.id) {
			cset.id = i*_currentFrame.precision;

			cset.qt_selector = _decoder.q_table[_currentFrame.component[i].qt_selector];
			cset.dc_table = _decoder.h_table[0][dcs];
			cset.dc_codes = _decoder.h_codes[0][dcs];
			cset.ac_table = _decoder.h_table[1][acs];
			cset.ac_codes = _decoder.h_codes[1][acs];

			cset.h_factor = _currentFrame.component[i].h_factor;			
			cset.v_factor = _currentFrame.component[i].v_factor;

			cset.hh_f = _currentFrame.max_hf/cset.h_factor;
			cset.vv_f = _currentFrame.max_vf/cset.v_factor;
			
			cset.unit_line_offset = 8*4*_currentFrame.precision*cset.hh_f;
			

			break;
		}
	}
}

UI_64 Pics::JPEG::LoadScanHeader(const unsigned char * ipo) {
	UI_64 result(ipo[2]);
	unsigned char mhf(1), mvf(1);
	result <<= 8;
	result += 2+ipo[3];

	ipo += 4;

	_currentScan.precision = _currentFrame.precision;
	_currentScan.line_offset = _currentFrame.line_offset;
	_currentScan.unit_line_offset = 8*4*_currentFrame.precision*_currentFrame.max_hf;
	_currentScan.block_offset = _currentScan.line_offset << 3;
	_currentScan.mcu_offset = _currentScan.block_offset*_currentFrame.max_vf;
	_currentScan.reserved_s = _currentFrame.s_reserved;

	_currentScan._reserved[0] = (_currentFrame.width + (_currentFrame.max_hf<<3) - 1)/(_currentFrame.max_hf<<3);
	_currentScan._reserved[0] *= ((_currentFrame.max_hf<<3)*_currentFrame.precision*4);

	_currentFrame.line_offset = _currentFrame.c_width*_currentFrame.precision*4;


	_currentScan.componentsCount = ipo[0];

	ipo++;
	
	for (unsigned char i(0);i<_currentScan.componentsCount;i++) {
		_currentScan.component[i].dc_prr = 0;
		_currentScan.component[i].id = ipo[0];

		GetFrameComponent(ipo[1] >> 4, ipo[1] & 3, _currentScan.component[i]);

		ipo += 2;
	}


	_currentScan.spectral_start = ipo[0];
	_currentScan.spectral_range = ipo[1] - ipo[0] + 1;

	_currentScan.previous_pt = ipo[2] >> 4;
	_currentScan.current_pt = (ipo[2] & 0x0F) + _currentFrame.level_shift;

	
	return result;
}

UI_64 Pics::JPEG::GetXTables(const unsigned char * src_ptr, unsigned int fsize) {
	UI_64 skipdi(0);

	for (;fsize;) {
		if (src_ptr[0] == 0xFF) {
			switch (src_ptr[1]) {
				case 0xC4:
					skipdi = LoadHuffmanTable(src_ptr);
				break;
				case 0xDB:
					skipdi = LoadQuantizationTable(src_ptr);

				break;
				default:
					skipdi = src_ptr[2];											
					skipdi = ((skipdi <<= 8) + 2 + src_ptr[3]);

			}

			if (fsize < skipdi) break;
			
			src_ptr += skipdi;
			fsize -= skipdi;


		} else break;
	}

	return 0;
}

UI_64 Pics::JPEG::XDecode(StripDesc & str_dsc, unsigned char * pi_ptr) {
	const unsigned char * img_ptr(0);
	UI_64 skipdi(0), result(1);

	System::MemoryFill(&_currentScan, sizeof(ScanHeader), 0);

	if (IsJPEG(reinterpret_cast<const char *>(str_dsc.pointer), 3) != -1) {
//	if ((src_ptr[0] == 0xFF) && (src_ptr[1] == 0xD8)) { // SOI
		img_ptr = str_dsc.pointer + 2;
		str_dsc.size -= 2;

		result = 0;

		for (;;) {
			if (img_ptr[0] == 0xFF) {
				if (img_ptr[1] == 0xD9) {
					if (f_domain) {
						if (const void * ampo = f_domain.Acquire()) {
							for (unsigned char cc(0);cc<_currentFrame.componentsCount;cc++) {
								_currentFrame.component[cc].qt_pointer = _decoder.q_table[_currentFrame.component[cc].qt_selector];
								_currentFrame.component[cc].h_factor = _currentFrame.max_hf/_currentFrame.component[cc].h_factor;
								_currentFrame.component[cc].v_factor = _currentFrame.max_vf/_currentFrame.component[cc].v_factor;

								_currentFrame.component[cc].uline_offset = 8*4*_currentFrame.precision*_currentFrame.component[cc].h_factor;
								_currentFrame.component[cc].block_offset = 8*4*_currentFrame.precision*_currentFrame.c_width*_currentFrame.component[cc].v_factor;
							}
							
							JPEG_IDCT(&_currentFrame, ampo, pi_ptr, _currentScan._reserved[0]);
													
							f_domain.Release();
						}

					}

					result = 123;
					break; // EOI
				}

				switch (img_ptr[1]) {
					case 0xC4:
						skipdi = LoadHuffmanTable(img_ptr);
					break;
					case 0xCC: // DAC
						skipdi = img_ptr[2];
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);
					break;
					case 0xDB:
						skipdi = LoadQuantizationTable(img_ptr);

					break;
					case 0xDA:
						if (_currentFrame.ecs_type & 0x10) {
							skipdi = LoadScanHeader(img_ptr);
							
							
							skipdi += DecodeHuffmanScan(img_ptr+skipdi, str_dsc.size - skipdi, pi_ptr);
							
							str_dsc.row_width = _currentFrame.c_width;
							str_dsc.row_count = _currentFrame.c_height;
										
							skipdi -= 2;
						} else {
							result = 3647951;
						}
											
					break;
					case 0xDC: // number of lines
						lines_count = img_ptr[4];
						lines_count <<= 8;
						lines_count |= img_ptr[5];
											
						skipdi = 6;
					break;
					case 0xDD:
						// restart on
						_currentScan._reserved[2] = img_ptr[4];
						_currentScan._reserved[2] <<= 8;
						_currentScan._reserved[2] |= img_ptr[5];

						skipdi = 6;
					break;
					case 0xDF: // expand
						skipdi = 3;
					break;
										
					case 0xC0: case 0xC1: case 0xC2: case 0xC3: // non-differencial Huffman										
											
						skipdi = LoadFrameHeader(img_ptr);
	

						if ((_currentFrame.precision > 2) || (_currentFrame.precision == 0) || (_currentFrame.componentsCount == 0)) {
							result = 36479812;
							break;
						}
						
						if ((!result) && (img_ptr[1] == 0xC2)) {
							if (!f_domain) {
								UI_64 offset(0), wval(0), hval(0);

								wval = (_currentFrame.width + (_currentFrame.max_hf<<3) - 1)/(_currentFrame.max_hf<<3);
								wval *= (_currentFrame.max_hf<<3);

								hval = (_currentFrame.height + (_currentFrame.max_vf<<3) - 1)/(_currentFrame.max_vf<<3);
								hval *= (_currentFrame.max_vf<<3);
								

								for (unsigned char i(0);i<_currentFrame.componentsCount;i++) {							
									_currentFrame.component[i].f_offset = offset;

									result = _currentFrame.component[i].h_factor*_currentFrame.component[i].v_factor;							
									result *= wval*hval/(_currentFrame.max_hf*_currentFrame.max_vf);

									_currentFrame.component[i].mcu_count = result >> 6;

									offset += result;
						
									
								}

								result = f_domain.New(jpeg_fdomain_tid, offset*sizeof(unsigned short), SFSco::large_mem_mgr);

								result = 0;
								offset = 0;
							}
						}


					break;						

					case 0xC5: case 0xC6: case 0xC7: // differential Huffman

					case 0xC9: case 0xCA: case 0xCB: // non-differential Arithmetic
					case 0xCD: case 0xCE: case 0xCF: // differential Arithmetic											
										
					case 0xDE: // hierarchical progression
						skipdi = 0;
						result = 696906969; // unsupported format
					break;

					case 0xE1: // APP1 (EXIF)
						skipdi = img_ptr[2];											
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);


					break;

					default:
						skipdi = img_ptr[2];											
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);

				}

				if (str_dsc.size < skipdi) {
					result = 65498701; // unexpected end of file / corrupted data	
					break;
				}

				img_ptr += skipdi;
				str_dsc.size -= skipdi;


				if (result) break;
			} else {
				for (;(str_dsc.size>1) && (img_ptr[0] != 0xFF);str_dsc.size--, img_ptr++);

				if (str_dsc.size < 2) break;
		
			}
		}
	}

	if (result == 123) result = 0;


	return result;
}

UI_64 Pics::JPEG::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	const unsigned char * img_ptr(0);
	UI_64 skipdi(0), result(0);
	unsigned int signature[8], signon(fsize);

	SFSco::Object pi_obj;

	imid = -1;

	if (fsize > 65536) signon = 65536;

	System::MemoryFill(&_currentScan, sizeof(ScanHeader), 0);

	if (IsJPEG(reinterpret_cast<const char *>(src_ptr), signon) != -1) {
//	if ((src_ptr[0] == 0xFF) && (src_ptr[1] == 0xD8)) { // SOI
		img_ptr = src_ptr + 2;
		fsize -= 2;

		signon = 1;

		for (;;) {
			if (img_ptr[0] == 0xFF) {
				if (img_ptr[1] == 0xD9) {
					if (f_domain) {
						if (void * pipo = pi_obj.Acquire()) {
							if (const void * ampo = f_domain.Acquire()) {
								for (unsigned char cc(0);cc<_currentFrame.componentsCount;cc++) {
									_currentFrame.component[cc].qt_pointer = _decoder.q_table[_currentFrame.component[cc].qt_selector];
									_currentFrame.component[cc].h_factor = _currentFrame.max_hf/_currentFrame.component[cc].h_factor;
									_currentFrame.component[cc].v_factor = _currentFrame.max_vf/_currentFrame.component[cc].v_factor;

									_currentFrame.component[cc].uline_offset = 8*4*_currentFrame.precision*_currentFrame.component[cc].h_factor;
									_currentFrame.component[cc].block_offset = 8*4*_currentFrame.precision*_currentFrame.c_width*_currentFrame.component[cc].v_factor;
								}
							
								signon = _currentFrame.componentsCount;
								
								JPEG_IDCT(&_currentFrame, ampo, pipo, _currentScan._reserved[0]);
								
								_currentFrame.componentsCount = signon;

								f_domain.Release();
							}
							pi_obj.Release();
						}

					}

					if (imid == -2) {
						Properties props;

						props.im_obj = pi_obj;

						for (unsigned int i(0);i<8;i++) props.signature[i] = signature[i];

						switch (_currentFrame.componentsCount) {
							case 1: props.norgba = Image::BW_special; break;								
							case 4: 
								if (_currentFrame.component[2].id == 'Y') props.norgba = Image::CMYJ;
								else props.norgba = Image::YCCK;
							break;
							default:
								if ((_currentFrame.ecs_type & 0x0F) != 3) props.norgba = Image::YCbCr;
								else props.norgba = 0;
						}

							
						props.width = _currentFrame.width;
						props.height = _currentFrame.height;
						props.c_width = _currentFrame.c_width;
						props.c_height = _currentFrame.c_height;

						props.precision = _currentFrame.precision;
							
						imid = pi_list.Add(props);
					
					}

					result = 123;
					break; // EOI
				}

				switch (img_ptr[1]) {
					case 0xC4:
						skipdi = LoadHuffmanTable(img_ptr);
						if (signon & 1) {
							SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), skipdi, signon);
							signon |= 0x80000000;
						}
					break;
					case 0xCC: // DAC
						skipdi = img_ptr[2];
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);
					break;
					case 0xDB:
						skipdi = LoadQuantizationTable(img_ptr);
						if (signon & 1) {
							SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), skipdi, signon);
							signon |= 0x80000000;
						}

					break;
					case 0xDA:
						if (signon & 1) {
							SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), skipdi, signon);
							signon = 0;

							imid = pi_list.FindIt(signature, 32);
							if (imid != -1) {
								result = 69;
								break;
							} else {
								imid = -2;
							}
								
						}

						if (_currentFrame.ecs_type & 0x10) {
							skipdi = LoadScanHeader(img_ptr);
							
							if (unsigned char * pipo = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
								skipdi += DecodeHuffmanScan(img_ptr+skipdi, fsize - skipdi, pipo);
								pi_obj.Release();
							}
											
							skipdi -= 2;
						} else {
							result = 3647951;
						}
											
					break;
					case 0xDC: // number of lines
						lines_count = img_ptr[4];
						lines_count <<= 8;
						lines_count |= img_ptr[5];
											
						skipdi = 6;
					break;
					case 0xDD:
						// restart on
						_currentScan._reserved[2] = img_ptr[4];
						_currentScan._reserved[2] <<= 8;
						_currentScan._reserved[2] |= img_ptr[5];

						skipdi = 6;
					break;
					case 0xDF: // expand
						skipdi = 3;
					break;
										
					case 0xC0: case 0xC1: case 0xC2: case 0xC3: // non-differencial Huffman										
											
						skipdi = LoadFrameHeader(img_ptr);
	
						if (signon & 1) {
							SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), skipdi, signon);
							signon |= 0x80000000;
						}

						if ((_currentFrame.precision > 2) || (_currentFrame.precision == 0) || (_currentFrame.componentsCount == 0)) {
							result = 36479812;
							break;
						}
						
						if (!pi_obj) {
							result = pi_obj.New(image_rstr_tid, _currentFrame.c_width*_currentFrame.c_height*_currentFrame.precision*4, SFSco::large_mem_mgr);
							
							if (result != -1) {
								if (void * pipo = pi_obj.Acquire()) {
									UI_64 fival(0);
									if (_currentFrame.componentsCount <= 3) {
										if (_currentFrame.precision == 1) fival = 0xFF000000FF000000;
										else fival = 0xFFFF000000000000;
									}

									System::MemoryFill_SSE3(pipo, _currentFrame.c_width*_currentFrame.c_height*_currentFrame.precision*4, fival, fival);

									pi_obj.Release();
								}

								result = 0;

							}

						}

						if ((!result) && (img_ptr[1] == 0xC2)) {
							if (!f_domain) {
								UI_64 offset(0), wval(0), hval(0);

								wval = (_currentFrame.width + (_currentFrame.max_hf<<3) - 1)/(_currentFrame.max_hf<<3);
								wval *= (_currentFrame.max_hf<<3);

								hval = (_currentFrame.height + (_currentFrame.max_vf<<3) - 1)/(_currentFrame.max_vf<<3);
								hval *= (_currentFrame.max_vf<<3);
								

								for (unsigned char i(0);i<_currentFrame.componentsCount;i++) {							
									_currentFrame.component[i].f_offset = offset;

									result = _currentFrame.component[i].h_factor*_currentFrame.component[i].v_factor;							
									result *= wval*hval/(_currentFrame.max_hf*_currentFrame.max_vf);

									_currentFrame.component[i].mcu_count = result >> 6;

									offset += result;
						
									
								}

								result = f_domain.New(jpeg_fdomain_tid, offset*sizeof(unsigned short), SFSco::large_mem_mgr);

								result = 0;
								offset = 0;
							}
						}


					break;						

					case 0xC5: case 0xC6: case 0xC7: // differential Huffman

					case 0xC9: case 0xCA: case 0xCB: // non-differential Arithmetic
					case 0xCD: case 0xCE: case 0xCF: // differential Arithmetic											
										
					case 0xDE: // hierarchical progression
						skipdi = 0;
						result = 696906969; // unsupported format
					break;

					case 0xE1: // APP1 (EXIF)
						skipdi = img_ptr[2];											
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);


					break;

					default:
						skipdi = img_ptr[2];											
						skipdi = ((skipdi <<= 8) + 2 + img_ptr[3]);

				}

				if (fsize < skipdi) {
					result = 65498701; // unexpected end of file / corrupted data	
					break;
				}

				img_ptr += skipdi;
				fsize -= skipdi;


				if (result) break;
			} else {
				for (;(fsize>1) && (img_ptr[0] != 0xFF);fsize--, img_ptr++);

				if (fsize < 2) break;
			}
		}
	}

	if (imid == -2) {
		imid = -1;
		pi_obj.Destroy();
	}

	return result;
}



UI_64 Pics::JPEG::DecodeHuffmanScan(const unsigned char * ipo, unsigned int fsize, unsigned char * pi_ptr) {
	UI_64 result(-1), t_val(0);


	switch (_currentFrame.ecs_type & 0x0F) {
		case 0: case 1:// nd sequential
			result = JPEG_HuffmanDCT(fsize, &_currentScan, pi_ptr, ipo);

		break;
		case 2: // nd progressive
			if (unsigned short * ampo = reinterpret_cast<unsigned short *>(f_domain.Acquire())) {
				result = 0;
				for (unsigned char i(0);i<_currentScan.componentsCount;i++) {
					result = _currentScan.component[i].id/_currentFrame.precision;

					_currentScan.component[i].qt_selector = ampo + _currentFrame.component[result].f_offset;
					_currentScan.component[i]._reserved = reinterpret_cast<UI_64>(_currentScan.component[i].qt_selector);
					_currentScan.component[i].unit_line_offset = _currentScan.block_offset/2/_currentScan.component[i].hh_f;

				}

				if (_currentScan.componentsCount == 1) {
					if (_currentScan.component[0].h_factor > 1) {
						t_val = (_currentFrame.c_width>>3) - ((_currentFrame.width + 7) >> 3);
						t_val <<= 5;

						_currentScan.component[0].h_factor = _currentScan.component[0].v_factor = (t_val | 1);

						_currentScan.component[0].unit_line_offset -= (((UI_64)_currentScan.component[0].h_factor>>5)<<7);
					}
				}


				if (_currentScan.previous_pt == 0) {
					result = JPEG_HuffmanSA_MSB(fsize, &_currentScan, ipo);
				} else {
					if (_currentScan.spectral_start != 0) result = JPEG_HuffmanSA_LSB(fsize, &_currentScan, ipo);
					else result = JPEG_ReadSA_LSB(fsize, &_currentScan, ipo);
				}

				f_domain.Release();

			}

		break;
		case 3: // nd lossless
			_currentScan.previous_pt = _currentFrame.bit_precision - _currentScan.current_pt + 2*_currentFrame.level_shift - 1;
			_currentScan.unit_line_offset = _currentFrame.max_hf;
			_currentScan.line_offset = ((_currentFrame.width + _currentFrame.max_hf - 1)/_currentFrame.max_hf)*_currentFrame.max_hf;
			_currentScan.block_offset = _currentFrame.c_width;
			_currentScan.mcu_offset = _currentFrame.c_width*_currentFrame.max_hf;
			_currentScan.spectral_range = 1;

			for (unsigned char i(0);i<_currentScan.componentsCount;i++) {
				_currentScan.component[i].ac_table = reinterpret_cast<unsigned char **>(_currentScan.block_offset*_currentFrame.c_height);
				_currentScan.component[i].ac_codes = 0;
					
				_currentScan.component[i].qt_selector = reinterpret_cast<unsigned short *>((UI_64)1 << _currentScan.previous_pt);

				_currentScan.component[i]._reserved_c = 1;					
				_currentScan.component[i].hh_f = 0;
			}

			result = JPEG_HuffmanLL(fsize, &_currentScan, pi_ptr + _currentScan.component[0].id, ipo);
		break;

	}


	return result;
}


UI_64 Pics::JPEG::InitializeDecoder() {
	float nrm(1.0f), nrm2(1.0f);

	for (unsigned char y(0);y<8;y++) {
		for (unsigned char x(0);x<8;x++) {
			for (unsigned char v(0);v<8;v++) {
				for (unsigned char u(0);u<8;u++) {
					nrm = nrm2 = 16384.0f;
					if ((u == 0) && (v == 0)) nrm = 8192.0f;
					else if ((u == 0) || (v == 0)) nrm = 1.15852375e4f;

					if ((x == 0) && (y == 0)) nrm2 = 8192.0f;
					else if ((x == 0) || (y == 0)) nrm2 = 1.15852375e4f;


					idct_cofs[y][x][zz_sequence[v*8+u]] = nrm*Calc::FloatSin(90.f+11.25f*(2*y+1)*v)*Calc::FloatSin(90.0f+11.25f*(2*x+1)*u);					
					fdct_cofs[zz_sequence[y*8+x]][v][u] = nrm2*Calc::FloatSin(90.f+11.25f*(2*v+1)*y)*Calc::FloatSin(90.0f+11.25f*(2*u+1)*x);
				}
			}
		}
	}

	mu_factors[0] = 128;
	for (unsigned int ii(1);ii<128;ii++) mu_factors[ii] = (16383+ii)/ii;
	

	return 0;
}

UI_64 Pics::JPEG::IsJPEG(const char * ipo, UI_64 lim) {
	UI_64 result(-1);
	char smarker[] = {0xFF, 0xD8, 0xFF};

/*
	if (result = reinterpret_cast<UI_64>(Strings::SearchBlock(ipo, lim, smarker, 2))) {
		result -= reinterpret_cast<UI_64>(ipo);
	} else {
		result = -1;
	}
*/
	if ((ipo[0] == smarker[0]) && (ipo[1] == smarker[1]) || (ipo[2] == smarker[2])) result = 0;
	return result;


}

