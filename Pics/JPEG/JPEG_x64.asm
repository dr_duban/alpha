
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


EXTERN	?idct_cofs@JPEG@Pics@@0PAY17EA@FA:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
JPEG_HuffmanDCT	PROC
	MOV [rsp+32], r9

	PUSH rbp
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

		SUB rsp, 264

		MOV [rsp+248], r8
		MOV [rsp+256], r8
		

		MOV rsi, r9		
		MOV ebp, ecx ; file pointer
		MOV r9, rdx ; ScanHeader pointer
				
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx

		MOV r10, rsp
		MOV r11b, 5
	align 16
		rsp_reset:
			MOV [r10], rax
			MOV [r10+8], rax
			MOV [r10+16], rax
			MOV [r10+24], rax

			ADD r10, 32
		DEC r11b
		JNZ rsp_reset


		XOR r10, r10 ; bit accum
		XOR r11, r11 ; bit runner 1
		XOR r12, r12 ; block runner
		XOR r14, r14

; r10, r11 - temp

; r14 - free
; r13 - huffman codes pointer
; r15 - huffman vals pointer
; rdi - component pointer


		CLD


align 16
	mcu_loop:

		MOV dl, [r9]
		LEA rdi, [r9+64]

	align 16
		component_loop:
			MOV dh, [rdi+42]
			AND dh, 7

		align 16
			v_factor_loop:			
			MOV bh, [rdi+41]
			AND bh, 7

			align 16
				h_factor_loop:
		
					XOR rcx, rcx

					MOV r15, [rdi] ; dc table
					MOV r13, [rdi+16] ; set dc codes

					SUB r15, 8
					SUB r13, 4


				align 16
					stream_scan_loop:

						AND bl, 7
						JNZ noff

						LODSB
						
						DEC ebp
						JZ exit
	
						CMP al, 0FFh
						JNZ	noff
							SHL eax, 8

							LODSB

							DEC ebp
							JZ exit

							TEST al, al
							JNZ exit
								SHR eax, 8
					align 16
						noff:
												

						TEST ch, ch
						JZ nextbit_loop
							ADD cl, 8
							JMP extraction_point

								
					align 16
						nextbit_loop:
							CMP bl, 8
							JAE stream_scan_loop
								INC bl
								INC r11b
								CMP r11b, 16
								JA exit
																
								SHL al, 1
								RCL r10w, 1
																
							CMP r10w, [r13+r11*4]
						JAE nextbit_loop

						SUB r10w, [r13+r11*4+2]

						MOV r11, [r15+r11*8]						
						MOV cl, [r10+r11] ; decoded val
						MOV ch, cl
						SHR cl, 4 ; run length
																
						XOR r11, r11					

						AND ch, 0Fh
						JZ eob_on
							ADD r12b, cl
							JMP extraction_on
						
						align 16
							eob_on:
								TEST r12b, r12b
								JZ move_it
									CMP cl, 0Fh
									JB skip_it
										XOR r10, r10
										INC cl
										ADD r12b, cl
										JMP stream_scan_loop

									skip_it:
										TEST cl, cl
										JZ idct_seq


					align 16
						extraction_on:
							MOV cl, bl
							MOV bl, 0
							AND cl, 7
							JZ stream_scan_loop
								SHR eax, cl

								XOR cl, 7
								INC cl

						align 16
							extraction_point:
								CMP cl, ch
								JAE read_it
									SHL eax, 8
									JMP stream_scan_loop
								read_it:
									SUB cl, ch
									XOR r11, r11
									ROR eax, cl

									XCHG r11w, ax								
									ROL eax, 8
			
									XOR cl, 7
									INC cl					
									MOV bl, cl

									DEC ch
									MOVZX ecx, ch

									MOV r10, 1									
									SHL r10w, cl
				

									CMP r11w, r10w
									JAE move_it
										SHL r10w, 1
										DEC r10w
										SUB r11w, r10w
					align 16
						move_it:

							XOR r10, r10
							ADD r10b, r12b
							JNZ do_ital
								ADD r11w, [rdi+46]
								MOV [rdi+46], r11w

								MOV r15, [rdi+8] ; ac table
								MOV r13, [rdi+24] ; set ac codes
					
								SUB r15, 8
								SUB r13, 4

							do_ital: 
								MOV [rsp+r10*2], r11w

								XOR r10, r10	
								XOR r11, r11
					

							INC r12b
							CMP r12b, [r9+2]
							JB stream_scan_loop


					align 16
						idct_seq:
							
							ADD r12b, 7
							SHR r12b, 3
							JZ end_of_block
							
								MOV cl, r12b
														
								PXOR xmm0, xmm0
								

								MOV r10, rsp
								MOV r13, [rdi+32]
															
								MOVDQA xmm8, [r10]
								PMULLW xmm8, [r13]
								MOVDQA [r10], xmm0
								DEC r12b
								JZ process_block
									MOVDQA xmm9, [r10+16]
									PMULLW xmm9, [r13+16]
									MOVDQA [r10+16], xmm0
									DEC r12b
									JZ process_block
										MOVDQA xmm10, [r10+32]
										PMULLW xmm10, [r13+32]
										MOVDQA [r10+32], xmm0
										DEC r12b
										JZ process_block
											MOVDQA xmm11, [r10+48]
											PMULLW xmm11, [r13+48]
											MOVDQA [r10+48], xmm0
											DEC r12b
											JZ process_block
												MOVDQA xmm12, [r10+64]
												PMULLW xmm12, [r13+64]
												MOVDQA [r10+64], xmm0
												DEC r12b
												JZ process_block
													MOVDQA xmm13, [r10+80]
													PMULLW xmm13, [r13+80]
													MOVDQA [r10+80], xmm0
													DEC r12b
													JZ process_block
														MOVDQA xmm14, [r10+96]
														PMULLW xmm14, [r13+96]
														MOVDQA [r10+96], xmm0
														DEC r12b
														JZ process_block
															MOVDQA xmm15, [r10+112]
															PMULLW xmm15, [r13+112]
															MOVDQA [r10+112], xmm0
															DEC r12b


							align 16
								process_block:
									MOV r12b, cl
									LEA r13, [?idct_cofs@JPEG@Pics@@0PAY17EA@FA]
									MOVD xmm7, r13

									MOV r15, [rsp+248]	
									MOVZX r10, BYTE PTR [rdi+40] ; component id
									ADD r15, r10 ; current block pointer

									MOV cl, [rdi+41]
									AND cl, 7
									SUB cl, bh
									JZ nl_adjust
										MOV r10, [rdi+48]
										ADD r15, r10
										DEC cl
										JZ nl_adjust
											ADD r15, r10
											DEC cl
											JZ nl_adjust
												ADD r15, r10
												DEC cl
						
								align 16
									nl_adjust:
										MOV cl, [rdi+42]
										AND cl, 7
										SUB cl, dh
										JZ r15_done
											MOV r10, [r9+24]
											ADD r15, r10
											DEC cl
											JZ r15_done
												ADD r15, r10
												DEC cl
												JZ r15_done
													ADD r15, r10
													DEC cl
								

								align 16
									r15_done:
										
										MOV ch, 64

								align 16
									sample_loop:
										MOVD r13, xmm7
										MOV cl, r12b
																																							
									
										MOVDQA xmm0, [r13]
										PMADDWD xmm0, xmm8
										DEC cl
										JZ amp_offload
											MOVDQA xmm1, [r13+16]
											PMADDWD xmm1, xmm9
											PADDD xmm0, xmm1
											DEC cl
											JZ amp_offload
												MOVDQA xmm1, [r13+32]
												PMADDWD xmm1, xmm10
												PADDD xmm0, xmm1
												DEC cl
												JZ amp_offload
													MOVDQA xmm1, [r13+48]
													PMADDWD xmm1, xmm11
													PADDD xmm0, xmm1
													DEC cl
													JZ amp_offload
														MOVDQA xmm1, [r13+64]
														PMADDWD xmm1, xmm12
														PADDD xmm0, xmm1
														DEC cl
														JZ amp_offload
															MOVDQA xmm1, [r13+80]
															PMADDWD xmm1, xmm13
															PADDD xmm0, xmm1
															DEC cl
															JZ amp_offload
																MOVDQA xmm1, [r13+96]
																PMADDWD xmm1, xmm14
																PADDD xmm0, xmm1
																DEC cl
																JZ amp_offload
																	MOVDQA xmm1, [r13+112]
																	PMADDWD xmm1, xmm15
																	PADDD xmm0, xmm1
																	DEC cl


									align 16
										amp_offload:
							
											MOVDQA xmm1, xmm0
											PSRLDQ xmm1, 8
											PADDD xmm0, xmm1
											
											MOVD r10, xmm0			
											MOVD r13, xmm7
											ADD r13, 128
											MOVD xmm7, r13

											MOV cl, r12b
																			
											MOV r12d, r10d
											SHR r10, 32

											ADD r10d, r12d
											
											MOV r12b, cl
											
											MOV cl, 16
											SUB cl, [r9+4]
											SHR r10d, cl


											MOV cl, [rdi+43]


											CMP BYTE PTR [r9+5], 1 ; precision
											JA double_precision
												MOV r13w, 7Fh
												CMP r10w, r13w
												CMOVG r10w, r13w
												NOT r13w
												CMP r10w, r13w
												CMOVL r10w, r13w
												
												XOR r10b, r13b
												MOV [r15], r10b

												MOV r14b, r10b
												ROL r14, 8

												ADD r15, 4
												
												DEC cl
												JZ new_line
													MOV [r15], r10b
													ADD r15, 4
													DEC cl
													JZ new_line
														MOV [r15], r10b
														ADD r15, 4
														DEC cl
														JZ new_line
															MOV [r15], r10b
															ADD r15, 4


												JMP new_line
										align 16
											double_precision:
												XOR r10w, 8000h
												MOV [r15], r10w

												MOV r14b, r10b
												ROR r10w, 8
												MOV r8b, r10b

												ROR r10w, 8
												ROL r14, 8
												ROL r8, 8
																																		

												ADD r15, 8
												
												DEC cl
												JZ new_line
													MOV [r15], r10w
													ADD r15, 8
													DEC cl
													JZ new_line
														MOV [r15], r10w
														ADD r15, 8
														DEC cl
														JZ new_line
															MOV [r15], r10w
															ADD r15, 8
															
												
										align 16
											new_line:											
												DEC ch
												TEST ch, 7
												JNZ sample_loop

													MOV cl, [rdi+44]
													SUB r15, [rdi+48]
													
													CMP BYTE PTR [r9+5], 1 ; precision
													JA double_p														
														ADD r15, [r9+8]

														DEC cl
														JZ no_nl
															MOVZX r10, BYTE PTR [rdi+43]
															ROR r12, 8
															MOV r12b, r10b
															SHL r10, 2

															align 16
															hhf_loop_1:
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8															
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8
																MOV [r15], r14b
																ADD r15, r10
																ROL r14, 8

																SUB r15, [rdi+48]
																ADD r15, 4
															DEC r12b
															JNZ hhf_loop_1

															ROL r12, 8
															SUB r15, r10
															ADD r15, [r9+8]
																														
															DEC cl
															JZ no_nl
																ROR r12, 8
																MOVZX r10, BYTE PTR [rdi+43]
																MOV r12b, r10b
																SHL r10, 2

																align 16
																hhf_loop_2:
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8															
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8
																	MOV [r15], r14b
																	ADD r15, r10
																	ROL r14, 8

																	SUB r15, [rdi+48]
																	ADD r15, 4
																DEC r12b
																JNZ hhf_loop_2

																ROL r12, 8
																SUB r15, r10
																ADD r15, [r9+8]
																																
																DEC cl
																JZ no_nl
																	ROR r12, 8
																	MOVZX r10, BYTE PTR [rdi+43]
																	MOV r12b, r10b
																	SHL r10, 2
																	align 16
																	hhf_loop_3:
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8															
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8
																		MOV [r15], r14b
																		ADD r15, r10
																		ROL r14, 8

																		SUB r15, [rdi+48]
																		ADD r15, 4
																	DEC r12b
																	JNZ hhf_loop_3

																	ROL r12, 8
																	SUB r15, r10
																	ADD r15, [r9+8]
													JMP no_nl
												align 16
													double_p:														
																																							
														ADD r15, [r9+8]

														DEC cl
														JZ no_nl
															ROR r12, 8
															MOVZX r11, BYTE PTR [rdi+43]
															MOV r12b, r11b
															SHL r11, 3

															align 16
															hhf2_loop_1:
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8
																MOV r10b, r8b
																SHL r10w, 8
																MOV r10b, r14b
																MOV [r15], r10w
																ADD r15, r11
																ROL r14, 8
																ROL r8, 8

																SUB r15, [rdi+48]
																ADD r15, 8
															DEC r12b
															JNZ hhf2_loop_1

															ROL r12, 8
															SUB r15, r11
															ADD r15, [r9+8]

															DEC cl
															JZ no_nl
																ROR r12, 8
																MOVZX r11, BYTE PTR [rdi+43]
																MOV r12b, r11b
																SHL r11, 3

															align 16
																hhf2_loop_2:
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8
																	MOV r10b, r8b
																	SHL r10w, 8
																	MOV r10b, r14b
																	MOV [r15], r10w
																	ADD r15, r11
																	ROL r14, 8
																	ROL r8, 8

																	SUB r15, [rdi+48]
																	ADD r15, 8
																DEC r12b
																JNZ hhf2_loop_2

																ROL r12, 8
																SUB r15, r11
																ADD r15, [r9+8]

																DEC cl
																JZ no_nl
																	ROR r12, 8
																	MOVZX r11, BYTE PTR [rdi+43]
																	MOV r12b, r11b
																	SHL r11, 3

																align 16
																	hhf2_loop_3:
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8
																		MOV r10b, r8b
																		SHL r10w, 8
																		MOV r10b, r14b
																		MOV [r15], r10w
																		ADD r15, r11
																		ROL r14, 8
																		ROL r8, 8

																		SUB r15, [rdi+48]
																		ADD r15, 8
																	DEC r12b
																	JNZ hhf2_loop_1

																	ROL r12, 8
																	SUB r15, r11
																	ADD r15, [r9+8]

															
														
										align 16
											no_nl:
												
									TEST ch, ch
									JNZ sample_loop
														
									
					align 16
						end_of_block:
							XOR r10, r10
							XOR r11, r11
							XOR r12, r12
														
							
				DEC bh
				JNZ h_factor_loop
				

			DEC dh
			JNZ v_factor_loop

			ADD rdi, 64
		DEC dl
		JNZ component_loop

			MOV r13w, [r9+56]
			AND r13w, r13w
			JZ no_reset
				INC WORD PTR [r9+58]
				CMP r13w, [r9+58]
				JA no_reset
					MOV WORD PTR [r9+58], 0
										

					; search for reset marker
					MOV ecx, ebp
					MOV al, 0FFh
					MOV r11b, 0D7h

					MOV rdi, rsi

					reset_search:
						REPNZ SCASB											
						JECXZ reset_eof

						MOV r11b, [rdi]
						XOR r11b, 0D7h

					CMP r11b, 15
					JA reset_search
					
					
					DEC ecx
					INC rdi

					CMP r11, 7
					JA reset_eof

					MOV ebp, ecx
					MOV rsi, rdi

					MOV r11b, [r9]
					LEA rax, [r9+110]
									
					reset_loop:
						MOV WORD PTR [rax], 0
						ADD rax, 64
						DEC r11b
					JNZ reset_loop
					
					XOR rax, rax
					MOV bl, al

					


		no_reset:


			MOV r13, [rsp+248]
			ADD r13, [r9+16] ; unit
			
			MOV [rsp+248], r13
			SUB r13, [rsp+256] ; line_b

			CMP r13, [r9+40]
			JB mcu_loop
				MOV r13, [rsp+256]
				ADD r13, [r9+32] ; mcu		
				MOV [rsp+248], r13
				MOV [rsp+256], r13

	JMP mcu_loop
align 16
	reset_eof:
		MOV rsi, rdi
		XOR r11, r11



	exit:
		MOV rax, rsi

		CMP r11b, 16
		CMOVA rax, r11		
		

	ADD rsp, 264

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx
	POP rbp
	
	SUB rax, [rsp+32]

	RET
JPEG_HuffmanDCT	ENDP


ALIGN 16
JPEG_HuffmanLL	PROC
	MOV [rsp+32], r9

	PUSH rbp
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


		MOV r14, r8 ; destination

		MOV rsi, r9		
		MOV ebp, ecx ; file pointer
		MOV r9, rdx ; ScanHeader pointer
				
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx

		XOR r10, r10 ; bit accum
		XOR r11, r11 ; bit runner 1
		XOR r12, r12 ; block runner
		XOR r14, r14


		PXOR xmm0, xmm0
		PXOR xmm1, xmm1

; r10, r11 - temp

; r14 - free
; r13 - huffman codes pointer
; r15 - huffman vals pointer
; rdi - component pointer


		CLD


align 16
	mcu_loop:

		MOV dl, [r9]
		LEA rdi, [r9+64]


	align 16
		component_loop:
			MOV dh, [rdi+42]
			AND dh, 7

			MOV cl, [r9+2] ; global first line
			MOV [rdi+44], cl

			MOVZX rcx, BYTE PTR [r9]
			SUB cl, dl
							
			LEA r14, [r8+rcx]

			MOVD r12, xmm0; current mcu
						

			MOV r15, [rdi] ; dc table
			MOV r13, [rdi+16] ; set dc codes

			SUB r15, 8
			SUB r13, 4

		align 16
			v_factor_loop:			
				MOV bh, [rdi+41]
				AND bh, 7

			align 16
				h_factor_loop:		

						AND bl, 7
						JNZ noff

						LODSB
						
						DEC ebp
						JZ exit
	
						CMP al, 0FFh
						JNZ	noff
							SHL eax, 8

							LODSB

							DEC ebp
							JZ exit

							TEST al, al
							JZ stuffby
								XOR al, 0D7h
								CMP al, 8
								JAE exit
									LEA rax, [r9+109]
									MOV r11b, [r9]									
									reset_loop:
										MOV BYTE PTR [rax], 1 ; reset flag									
										ADD rax, 64
									DEC r11b
									JNZ reset_loop

									XOR rax, rax
									
								JMP h_factor_loop
							stuffby:
							SHR eax, 8
					align 16
						noff:
												

						TEST ch, ch
						JZ nextbit_loop
							ADD cl, 8
							JMP extraction_point

								
					align 16
						nextbit_loop:
							CMP bl, 8
							JAE h_factor_loop
								INC bl
								INC r11b
								CMP r11b, 16
								JA exit
																
								SHL al, 1
								RCL r10w, 1
																
							CMP r10w, [r13+r11*4]
						JAE nextbit_loop

						SUB r10w, [r13+r11*4+2]

						MOV r11, [r15+r11*8]
						MOV cl, [r10+r11] ; decoded val
						MOV ch, cl
																
						XOR r11, r11					

						AND ch, 0Fh
						JZ move_it

						CMP ch, 0Fh
						JBE extraction_on
							MOV r11d, 8000h
							MOV ch, 0
							JMP move_it

					align 16
						extraction_on:
							MOV cl, bl
							MOV bl, 0
							AND cl, 7
							JZ h_factor_loop
								SHR eax, cl

								XOR cl, 7
								INC cl

						align 16
							extraction_point:
								CMP cl, ch
								JAE read_it
									SHL eax, 8
									JMP h_factor_loop
								read_it:
									SUB cl, ch									
									ROR eax, cl

									XCHG r11w, ax								
									ROL eax, 8
			
									XOR cl, 7
									INC cl					
									MOV bl, cl

									DEC ch
									MOVZX ecx, ch

									MOV r10, 1									
									SHL r10d, cl

									CMP r11d, r10d
									JAE move_it
										SHL r10d, 1
										DEC r10d
										SUB r11d, r10d
					align 16
						move_it:
							MOV cl, [r9+4]
							SHL r11d, cl
								
							MOV ecx, [rdi+32]						
							
							CMP BYTE PTR [rdi+45], 1 ; reset flag
							JZ endof_sample								

								CMP BYTE PTR [r9+5], 1
								JNZ double_precision

									CMP BYTE PTR [rdi+44], 1; first line
									JZ predictor_1_on										
									
										CMP BYTE PTR [rdi+44], 2 ; new line flag
										JZ predictor_2_on
											

											CMP BYTE PTR [r9+1], 1
											JNZ predictor_2
												predictor_1_on:
													MOVZX ecx, BYTE PTR [r14+r12*4-4]

												JMP endof_sample
											predictor_2:
												CMP BYTE PTR [r9+1], 2
												JNZ predictor_3
													predictor_2_on:
														MOV r10, r12
														SUB r10, [r9+24]

														MOVZX ecx, BYTE PTR [r14+r10*4]

													JMP endof_sample
												predictor_3:
													CMP BYTE PTR [r9+1], 3
													JNZ predictor_4
														MOV r10, r12
														SUB r10, [r9+24]

														MOVZX ecx, BYTE PTR [r14+r10*4-4]

														JMP endof_sample
													predictor_4:
														CMP BYTE PTR [r9+1], 4
														JNZ predictor_5
															MOVZX ecx, BYTE PTR [r14+r12*4-4]
	
															MOV r10, r12
															SUB r10, [r9+24]

															MOVZX r10d, BYTE PTR [r14+r10*4]
															ADD ecx, r10d

															MOV r10, r12
															SUB r10, [r9+24]

															MOVZX r10d, BYTE PTR [r14+r10*4-4]

															SUB ecx, r10d

															JMP endof_sample
														predictor_5:
															CMP BYTE PTR [r9+1], 5
															JNZ predictor_6
	
																MOV r10, r12
																SUB r10, [r9+24]
															
																MOVZX ecx, BYTE PTR [r14+r10*4]																
																MOVZX r10d, BYTE PTR [r14+r10*4-4]

																SUB ecx, r10d
																SAR ecx, 1

																MOVZX r10d, BYTE PTR [r14+r12*4-4]
																ADD ecx, r10d

																JMP endof_sample
															predictor_6:
																CMP BYTE PTR [r9+1], 6
																JNZ predictor_7

																	MOVZX ecx, BYTE PTR [r14+r12*4-4]
		
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, BYTE PTR [r14+r10*4-4]
																	SUB ecx, r10d
																	SAR ecx, 1
															
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, BYTE PTR [r14+r10*4]

																	ADD ecx, r10d

																	JMP endof_sample
																predictor_7:

																	MOVZX ecx, BYTE PTR [r14+r12*4-4]
	
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, BYTE PTR [r14+r10*4]
																	ADD ecx, r10d

																	SAR ecx, 1



							JMP endof_sample
						align 16
							double_precision:

									CMP BYTE PTR [rdi+44], 1; first line
									JZ predictor_1_on2										
									
										CMP BYTE PTR [rdi+44], 2 ; new line flag
										JZ predictor_2_on2
											

											CMP BYTE PTR [r9+1], 1
											JNZ predictor_22
												predictor_1_on2:
													MOVZX ecx, WORD PTR [r14+r12*8-8]

												JMP endof_sample
											predictor_22:
												CMP BYTE PTR [r9+1], 2
												JNZ predictor_32
													predictor_2_on2:
														MOV r10, r12
														SUB r10, [r9+24]

														MOVZX ecx, WORD PTR [r14+r10*8]

													JMP endof_sample
												predictor_32:
													CMP BYTE PTR [r9+1], 3
													JNZ predictor_42
														MOV r10, r12
														SUB r10, [r9+24]

														MOVZX ecx, WORD PTR [r14+r10*8-8]

														JMP endof_sample
													predictor_42:
														CMP BYTE PTR [r9+1], 4
														JNZ predictor_52
															MOVZX ecx, WORD PTR [r14+r12*8-8]
	
															MOV r10, r12
															SUB r10, [r9+24]

															MOVZX r10d, WORD PTR [r14+r10*8]
															ADD ecx, r10d

															MOV r10, r12
															SUB r10, [r9+24]

															MOVZX r10d, WORD PTR [r14+r10*8-8]

															SUB ecx, r10d

															JMP endof_sample
														predictor_52:
															CMP BYTE PTR [r9+1], 5
															JNZ predictor_62
	
																MOV r10, r12
																SUB r10, [r9+24]
															
																MOVZX ecx, WORD PTR [r14+r10*8]																
																MOVZX r10d, WORD PTR [r14+r10*8-8]

																SUB ecx, r10d
																SAR ecx, 1

																MOVZX r10d, WORD PTR [r14+r12*8-8]
																ADD ecx, r10d

																JMP endof_sample
															predictor_62:
																CMP BYTE PTR [r9+1], 6
																JNZ predictor_72

																	MOVZX ecx, WORD PTR [r14+r12*8-8]
		
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, WORD PTR [r14+r10*8-8]
																	SUB ecx, r10d
																	SAR ecx, 1
															
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, WORD PTR [r14+r10*8]

																	ADD ecx, r10d

																	JMP endof_sample
																predictor_72:

																	MOVZX ecx, WORD PTR [r14+r12*8-8]
	
																	MOV r10, r12
																	SUB r10, [r9+24]

																	MOVZX r10d, WORD PTR [r14+r10*8]
																	ADD ecx, r10d

																	SAR ecx, 1







					align 16
						endof_sample:
							ADD r11d, ecx
							MOV BYTE PTR [rdi+45], 0 ; reset flag
							
							CMP BYTE PTR [r9+5], 1
							JNZ db_prec
								MOV [r14+r12*4], r11b
								JMP done_set
							db_prec:
								MOV [r14+r12*8], r11w
							done_set:

							XOR r10, r10
							XOR r11, r11					
							XOR rcx, rcx

							INC r12
														
							
				DEC bh
				JNZ h_factor_loop
				
					CMP dh, 1
					JBE no_vadjust						
						SUB r12, [r9+16]
						ADD r12, [r9+24] ; line offset

						MOV cl, [r9+2]
						AND cl, 2
						MOV [rdi+44], cl
												
					no_vadjust:				

			DEC dh
			JNZ v_factor_loop

			ADD rdi, 64
		DEC dl
		JNZ component_loop	


			XOR r10, r10
		
			MOVZX r11, BYTE PTR [r9+2]
			CMP r11b, 2
			CMOVZ r11, r10

			MOV [r9+2], r11b

			MOVD r12, xmm0
			MOVD r11, xmm1

			ADD r12, [r9+16]
			MOV r10, r12
						
			SUB r10, r11
			CMP r10, [r9+8]
			JB no_adjust
				MOV BYTE PTR [r9+2], 2
				MOV r12, [r9+32]
				ADD r12, r11
				MOVD xmm1, r12
			no_adjust:

			MOVD xmm0, r12
		
			XOR r10, r10
			XOR r11, r11

			CMP r12, [r9+72]
			CMOVAE rsi, r11
			JAE exit

	JMP mcu_loop

	exit:
		MOV rax, rsi

		CMP r11b, 16
		CMOVA rax, r11		
		

		
	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx
	POP rbp
	
	SUB rax, [rsp+32]



	RET
JPEG_HuffmanLL	ENDP

END

