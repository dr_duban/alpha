
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




EXTERN	?fdct_cofs@JPEG@Pics@@0PAY177FA:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16

y_scal_602	DWORD	04B232646h, 000000E98h, 04B232646h, 000000E98h
pb_scal_602	DWORD	0D598EA68h, 000004000h, 0D598EA68h, 000004000h
pr_scal_602	DWORD	0CA684000h, 00000F598h, 0CA684000h, 00000F598h

y_scal_709	DWORD	05B8CB36h, 00000093Eh, 05B8CB36h, 00000093Eh
pb_scal_709	DWORD	0CEABF156h, 000004000h, 0CEABF156h, 000004000h
pr_scal_709	DWORD	0C5DB4000h, 00000FA22h, 0C5DB4000h, 00000FA22h



half_12	DWORD	008000800h, 008000800h, 008000800h, 008000800h
one_w	DWORD	80008000h, 80008000h, 80008000h, 80008000h
b_pred	DWORD	128, 0, 0, 0
lb_shuf	DWORD	0C080400h, 0E0A0602h, 0D090501h, 0F0B0703h



.CODE


ALIGN 16
JPEG_DCT_20	PROC

	MOV r8, rcx

	MOV rax, [rdx+16]
	SHL rax, 4

	ADD rcx, [rdx+48] ; cb_offset	
	MOV r9, rcx



	MOV r10, [rdx+64]
;	SUB r10, [rdx+48] ; cb byte count
	SHR r10, 8

	XOR r11, r11

align 16
	cb_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+128]
		PADDW xmm1, [rcx+144]
		PADDW xmm2, [rcx+160]
		PADDW xmm3, [rcx+176]
		PADDW xmm4, [rcx+192]
		PADDW xmm5, [rcx+208]
		PADDW xmm6, [rcx+224]
		PADDW xmm7, [rcx+240]

		PADDW xmm0, [rcx+rax]
		PADDW xmm1, [rcx+rax+16]
		PADDW xmm2, [rcx+rax+32]
		PADDW xmm3, [rcx+rax+48]
		PADDW xmm4, [rcx+rax+64]
		PADDW xmm5, [rcx+rax+80]
		PADDW xmm6, [rcx+rax+96]
		PADDW xmm7, [rcx+rax+112]

		PADDW xmm0, [rcx+rax+128]
		PADDW xmm1, [rcx+rax+144]
		PADDW xmm2, [rcx+rax+160]
		PADDW xmm3, [rcx+rax+176]
		PADDW xmm4, [rcx+rax+192]
		PADDW xmm5, [rcx+rax+208]
		PADDW xmm6, [rcx+rax+224]
		PADDW xmm7, [rcx+rax+240]

		PSRAW xmm0, 2
		PSRAW xmm1, 2
		PSRAW xmm2, 2
		PSRAW xmm3, 2
		PSRAW xmm4, 2
		PSRAW xmm5, 2
		PSRAW xmm6, 2
		PSRAW xmm7, 2

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 256

		ADD r11, 256
		CMP r11, rax
		JB cb_dec
			XOR r11, r11
			ADD rcx, rax
		cb_dec:
	DEC r10
	JNZ cb_loop

	SUB r9, r8
	MOV [rdx+144], r9
	ADD r9, r8


	MOV r10, [rdx+64]
;	SUB r10, [rdx+56]
	SHR r10, 8

	XOR r11, r11

align 16
	cr_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+128]
		PADDW xmm1, [rcx+144]
		PADDW xmm2, [rcx+160]
		PADDW xmm3, [rcx+176]
		PADDW xmm4, [rcx+192]
		PADDW xmm5, [rcx+208]
		PADDW xmm6, [rcx+224]
		PADDW xmm7, [rcx+240]

		PADDW xmm0, [rcx+rax]
		PADDW xmm1, [rcx+rax+16]
		PADDW xmm2, [rcx+rax+32]
		PADDW xmm3, [rcx+rax+48]
		PADDW xmm4, [rcx+rax+64]
		PADDW xmm5, [rcx+rax+80]
		PADDW xmm6, [rcx+rax+96]
		PADDW xmm7, [rcx+rax+112]

		PADDW xmm0, [rcx+rax+128]
		PADDW xmm1, [rcx+rax+144]
		PADDW xmm2, [rcx+rax+160]
		PADDW xmm3, [rcx+rax+176]
		PADDW xmm4, [rcx+rax+192]
		PADDW xmm5, [rcx+rax+208]
		PADDW xmm6, [rcx+rax+224]
		PADDW xmm7, [rcx+rax+240]

		PSRAW xmm0, 2
		PSRAW xmm1, 2
		PSRAW xmm2, 2
		PSRAW xmm3, 2
		PSRAW xmm4, 2
		PSRAW xmm5, 2
		PSRAW xmm6, 2
		PSRAW xmm7, 2

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 256

		ADD r11, 256
		CMP r11, rax
		JB cr_dec
			XOR r11, r11
			ADD rcx, rax
		cr_dec:
	DEC r10
	JNZ cr_loop

	SFENCE

	SUB r9, r8
	MOV [rdx+152], r9


	RET
JPEG_DCT_20	ENDP





ALIGN 16
JPEG_DCT_40	PROC
	
	MOV r8, rcx

	MOV rax, [rdx+16]
	SHL rax, 4

	ADD rcx, [rdx+48] ; cb_offset	
	MOV r9, rcx



	MOV r10, [rdx+64]
;	SUB r10, [rdx+48] ; cb byte count
	SHR r10, 7

	XOR r11, r11

align 16
	cb_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+rax]
		PADDW xmm1, [rcx+rax+16]
		PADDW xmm2, [rcx+rax+32]
		PADDW xmm3, [rcx+rax+48]
		PADDW xmm4, [rcx+rax+64]
		PADDW xmm5, [rcx+rax+80]
		PADDW xmm6, [rcx+rax+96]
		PADDW xmm7, [rcx+rax+112]

		PSRAW xmm0, 1
		PSRAW xmm1, 1
		PSRAW xmm2, 1
		PSRAW xmm3, 1
		PSRAW xmm4, 1
		PSRAW xmm5, 1
		PSRAW xmm6, 1
		PSRAW xmm7, 1

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 128

		ADD r11, 128
		CMP r11, rax
		JB cb_dec
			XOR r11, r11
			ADD rcx, rax
		cb_dec:

	DEC r10
	JNZ cb_loop


	SUB r9, r8
	MOV [rdx+144], r9
	ADD r9, r8

	MOV r10, [rdx+64]
;	SUB r10, [rdx+56]
	SHR r10, 7

	XOR r11, r11

align 16
	cr_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+rax]
		PADDW xmm1, [rcx+rax+16]
		PADDW xmm2, [rcx+rax+32]
		PADDW xmm3, [rcx+rax+48]
		PADDW xmm4, [rcx+rax+64]
		PADDW xmm5, [rcx+rax+80]
		PADDW xmm6, [rcx+rax+96]
		PADDW xmm7, [rcx+rax+112]

		PSRAW xmm0, 1
		PSRAW xmm1, 1
		PSRAW xmm2, 1
		PSRAW xmm3, 1
		PSRAW xmm4, 1
		PSRAW xmm5, 1
		PSRAW xmm6, 1
		PSRAW xmm7, 1

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 128

		ADD r11, 128
		CMP r11, rax
		
		JB cr_dec
			XOR r11, r11
			ADD rcx, rax
		cr_dec:
	DEC r10
	JNZ cr_loop

	SFENCE

	SUB r9, r8
	MOV [rdx+152], r9


	RET
JPEG_DCT_40	ENDP




ALIGN 16
JPEG_DCT_22	PROC
	
	MOV r8, rcx

	MOV rax, [rdx+16]
	SHL rax, 4

	ADD rcx, [rdx+48] ; cb_offset	
	MOV r9, rcx


	MOV r10, [rdx+64]
;	SUB r10, [rdx+48] ; cb byte count
	SHL r10, 7


align 16
	cb_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+128]
		PADDW xmm1, [rcx+144]
		PADDW xmm2, [rcx+160]
		PADDW xmm3, [rcx+176]
		PADDW xmm4, [rcx+192]
		PADDW xmm5, [rcx+208]
		PADDW xmm6, [rcx+224]
		PADDW xmm7, [rcx+240]

		PSRAW xmm0, 1
		PSRAW xmm1, 1
		PSRAW xmm2, 1
		PSRAW xmm3, 1
		PSRAW xmm4, 1
		PSRAW xmm5, 1
		PSRAW xmm6, 1
		PSRAW xmm7, 1

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 256

	DEC r10
	JNZ cb_loop

	SUB r9, r8
	MOV [rdx+144], r9
	ADD r9, r8

	MOV r10, [rdx+64]
;	SUB r10, [rdx+56]
	SHR r10, 7

align 16
	cr_loop:
		MOVDQA xmm0, [rcx]
		MOVDQA xmm1, [rcx+16]
		MOVDQA xmm2, [rcx+32]
		MOVDQA xmm3, [rcx+48]
		MOVDQA xmm4, [rcx+64]
		MOVDQA xmm5, [rcx+80]
		MOVDQA xmm6, [rcx+96]
		MOVDQA xmm7, [rcx+112]

		PADDW xmm0, [rcx+128]
		PADDW xmm1, [rcx+144]
		PADDW xmm2, [rcx+160]
		PADDW xmm3, [rcx+176]
		PADDW xmm4, [rcx+192]
		PADDW xmm5, [rcx+208]
		PADDW xmm6, [rcx+224]
		PADDW xmm7, [rcx+240]

		PSRAW xmm0, 1
		PSRAW xmm1, 1
		PSRAW xmm2, 1
		PSRAW xmm3, 1
		PSRAW xmm4, 1
		PSRAW xmm5, 1
		PSRAW xmm6, 1
		PSRAW xmm7, 1

		MOVNTDQ [r9], xmm0
		MOVNTDQ [r9+16], xmm1
		MOVNTDQ [r9+32], xmm2
		MOVNTDQ [r9+48], xmm3
		MOVNTDQ [r9+64], xmm4
		MOVNTDQ [r9+80], xmm5
		MOVNTDQ [r9+96], xmm6
		MOVNTDQ [r9+112], xmm7

		
		ADD r9, 128
		ADD rcx, 256

	DEC r10
	JNZ cr_loop

	SFENCE

	SUB r9, r8
	MOV [rdx+152], r9


	RET
JPEG_DCT_22	ENDP




ALIGN 16
JPEG_LLScanPack	PROC
	PUSH rsi
	PUSH rdi

	PUSH r15
	PUSH r14
	PUSH rbx

	MOV rdi, [rdx+32]
	MOV rsi, [rdx+r8*8+40]
	ADD rsi, rcx
	ADD rdi, rcx

	MOV [rdx+72], rdi

	MOV r8, [rdx+80]

	MOV r10, [rdx+96] ; codes
	MOV r11, [rdx+104] ; lengthes

	XOR rax, rax
	XOR rbx, rbx
	XOR rcx, rcx
	XOR r9, r9
	XOR r14, r14
	XOR r15, r15

align 16
	pi_loop:
		LODSW

		MOV r9b, al
		MOV cl, [r11+r9]

		ADD ch, cl
		SHL rbx, cl
		OR bx, [r10+r9*2]

		SHR ax, 8

		AND r9b, 0Fh
		JZ no_ebits
			MOV cl, r9b

			MOV r9w, 80h
			AND r9b, al
			ROL r9b, 1
			SUB al, r9b

			MOV r9w, 1
			SHL r9w, cl
			DEC r9w
			
			AND ax, r9w

			ADD ch, cl
			SHL rbx, cl
			OR bx, ax

	no_ebits:

		CMP ch, 8
		JB no_bshift
			SUB ch, 8
			MOV cl, ch

			ROR rbx, cl
			MOV r15b, bl
			MOV bl, 0
			ROL rbx, cl

			MOV cl, 0
			ADD r15b, 1
			ADC cl, 0
			SUB r15b, 1

			ROR r15, 8

			INC r14b
			CMP r14b, 8
			JB no_offload

				MOVNTI [rdi], r15

				ADD rdi, 8				
				XOR r15, r15
				XOR r14, r14
			no_offload:

			TEST cl, cl
			JZ no_ebits
				ROR r15, 8
				
				INC r14b
				CMP r14b, 8
				JB no_ebits
					MOVNTI [rdi], r15

					ADD rdi, 8					
					XOR r15, r15
					XOR r14, r14
		JMP no_ebits
	align 16
		no_bshift:


	DEC r8
	JNZ pi_loop

align 16
	last_offload:

		CMP ch, 8
		JB no_obytes
			SUB ch, 8

			MOV cl, ch
			ROR rbx, cl
			
			MOV r15b, bl
			MOV bl, 0
			ROL rbx, cl

			MOV cl, 0
			ADD r15b, 1
			ADC cl, 0
			SUB r15b, 1
				
			ROR r15, 8

			INC r14b
			CMP r14b, 8
			JB no_offload_1
				MOVNTI [rdi], r15

				ADD rdi, 8
				XOR r15, r15
				XOR r14, r14
			no_offload_1:
				
			TEST cl, cl
			JZ last_offload
				ROR r15, 8

				INC r14b
				CMP r14b, 8
				JB last_offload
					MOVNTI [rdi], r15
					ADD rdi, 8
					XOR r15, r15
					XOR r14, r14
					

		JMP last_offload

	no_obytes:
			
		TEST ch, ch
		JZ no_pbyte
			MOV cl, 8
			SUB cl, ch
			MOV al, 1

			SHL al, cl				
			SHL bl, cl
			DEC al

			OR bl, al

			INC r14b

			MOV r15b, bl

			ROR r15, 8
		no_pbyte:
			ROL r15, 8
				
			MOV cl, 0
			ADD r15b, 1
			ADC cl, 0
			SUB r15b, 1

			ROR r15, 8

			CMP r14b, 8
			JB no_offload_2
				MOVNTI [rdi], r15
				ADD rdi, 8
				XOR r15, r15
				XOR r14, r14
			no_offload_2:

			MOV ch, cl
			
			MOV cl, 8
			SUB cl, r14b
			SHL cl, 3

			SHR r15, cl

			SHR rcx, 8
			
			ADD cl, r14b
			JZ no_leftover
				do_leftover:
					MOV [rdi], r15b
					INC rdi
					SHR r15, 8
				DEC cl
				JNZ do_leftover
		no_leftover:


	SFENCE

	SUB rdi, [rdx+72]
	MOV [rdx+72], rdi

	POP rbx
	POP r14
	POP r15

	POP rdi
	POP rsi

	RET
JPEG_LLScanPack	ENDP

ALIGN 16
JPEG_LLStats_SSSE3	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12
	
	PUSH rsi
	PUSH rdi
	PUSH rbx

	

	MOV r15, [r8]
	MOV r11, [r8+8] ; height
	MOV r9, [r8+16] ; c_width
	
	
	AND r15, 7
	JZ no_r15_sub
		SUB r15, 8
	no_r15_sub:
	SHL r15, 1


	MOV rdi, [r8+32]
	ADD rdi, rcx
	MOV rsi, rdx	
	MOV r12, [r8+40]
	ADD r12, rcx
	MOV r13, [r8+48]
	ADD r13, rcx
	MOV r14, [r8+56]
	ADD r14, rcx

	XOR r10, r10
	XOR rbx, rbx
	XOR rdx, rdx
	SHR r9, 3

	MOV [r8+112], rdi
	MOV [r8+120], r9d

	PXOR xmm14, xmm14
			
	MOVDQA xmm10, XMMWORD PTR [b_pred]
	MOVDQA xmm9, XMMWORD PTR [b_pred]
	MOVDQA xmm8, XMMWORD PTR [b_pred]

	MOVDQA xmm15, XMMWORD PTR [lb_shuf]

	SHL r15, 16
	NOT r15w

; first line
align 16
	first_line:

		MOVDQA xmm0, [rsi]
		MOVDQA xmm2, [rsi+16]

		PSHUFB xmm0, xmm15
		PSHUFB xmm2, xmm15
		
		MOVDQA xmm1, xmm0
		PUNPCKLDQ xmm0, xmm2
		PUNPCKHDQ xmm1, xmm2

		MOVDQA xmm2, xmm0 ; b
		PSRLDQ xmm2, 8

		PUNPCKLQDQ xmm0, xmm14 ; r
		PUNPCKLQDQ xmm1, xmm14 ; g


		MOVD QWORD PTR [rdi], xmm0
		MOVDQA xmm3, xmm0
		PSLLDQ xmm3, 1
		POR xmm8, xmm3

		PSUBB xmm0, xmm8
		PSRLDQ xmm8, 8

		PABSB xmm3, xmm0

		MOVD rax, xmm3 ; category
		MOVD rbx, xmm0 ; ebits

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOVNTI [r12], r10

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+512]

		MOVNTI [r12+8], r10



		MOVD QWORD PTR [rdi+8], xmm1
		MOVDQA xmm3, xmm1
		PSLLDQ xmm3, 1
		POR xmm9, xmm3

		PSUBB xmm1, xmm9
		PSRLDQ xmm9, 8

		PABSB xmm3, xmm1

		MOVD rax, xmm3 ; category
		MOVD rbx, xmm1 ; ebits

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]
		MOVNTI [r13], r10

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1024]

		MOVNTI [r13+8], r10



		MOVD QWORD PTR [rdi+16], xmm2
		MOVDQA xmm3, xmm2
		PSLLDQ xmm3, 1
		POR xmm10, xmm3

		PSUBB xmm2, xmm10
		PSRLDQ xmm10, 8

		PABSB xmm3, xmm2

		MOVD rax, xmm3 ; category
		MOVD rbx, xmm2 ; ebits

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOVNTI [r14], r10

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOV dl, al
		BSR dx, dx
		CMOVZ dx, r15w
		INC dx
		MOV r10b, dl
		ROR r10, 8
		MOV r10b, bl
		ROR r10, 8
		SHR rax, 8
		SHR rbx, 8

		INC QWORD PTR [rcx+rdx*8+1536]

		MOVNTI [r14+8], r10
	

		ADD rdi, 24
		ADD r12, 16
		ADD r13, 16
		ADD r14, 16

		ADD rsi, 32		

	DEC r9d
	JNZ first_line

	DEC r11d
	JZ exit

; every other line


align 16
	ro_loop:
		MOV rdi, [r8+112]
		MOV r9d, [r8+120]

		SAR r15, 16

		ADD r12, r15
		ADD r13, r15
		ADD r14, r15

		SHL r15, 16
		NOT r15w
	
	align 16
		co_loop:


			MOVDQA xmm0, [rsi]
			MOVDQA xmm2, [rsi+16]

			PSHUFB xmm0, xmm15
			PSHUFB xmm2, xmm15
		
			MOVDQA xmm1, xmm0
			PUNPCKLDQ xmm0, xmm2
			PUNPCKHDQ xmm1, xmm2

			MOVDQA xmm2, xmm0 ; cr
			PSRLDQ xmm2, 8

			PUNPCKLQDQ xmm0, xmm14 ; y
			PUNPCKLQDQ xmm1, xmm14 ; cb


			MOVD xmm3, QWORD PTR [rdi]
			MOVD QWORD PTR [rdi], xmm0
			PSUBB xmm0, xmm3
			PABSB xmm3, xmm0

			MOVD rax, xmm3 ; category
			MOVD rbx, xmm0 ; ebits

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOVNTI [r12], r10

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+512]

			MOVNTI [r12+8], r10



			MOVD xmm3, QWORD PTR [rdi+8]
			MOVD QWORD PTR [rdi+8], xmm1
			PSUBB xmm1, xmm3
			PABSB xmm3, xmm1

			MOVD rax, xmm3 ; category
			MOVD rbx, xmm1 ; ebits

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOVNTI [r13], r10

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]
	
			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1024]

			MOVNTI [r13+8], r10



			MOVD xmm3, QWORD PTR [rdi+16]
			MOVD QWORD PTR [rdi+16], xmm2
			PSUBB xmm2, xmm3
			PABSB xmm3, xmm2

			MOVD rax, xmm3 ; category
			MOVD rbx, xmm2 ; ebits

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOVNTI [r14], r10

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOV dl, al
			BSR dx, dx
			CMOVZ dx, r15w
			INC dx
			MOV r10b, dl
			ROR r10, 8
			MOV r10b, bl
			ROR r10, 8
			SHR rax, 8
			SHR rbx, 8

			INC QWORD PTR [rcx+rdx*8+1536]

			MOVNTI [r14+8], r10
	

			ADD r12, 16
			ADD r13, 16
			ADD r14, 16

			
			ADD rdi, 24
			ADD rsi, 32

		DEC r9d
		JNZ co_loop

		
	DEC r11d
	JNZ ro_loop

exit:
	SFENCE

	INC QWORD PTR [rcx+632]
	INC QWORD PTR [rcx+1144]
	INC QWORD PTR [rcx+1656]


	POP rbx
	POP rdi
	POP rsi

	POP r12
	POP r13
	POP r14
	POP r15

	RET
JPEG_LLStats_SSSE3	ENDP

ALIGN 16
JPEG_LLStats_SSE3	PROC

	RET
JPEG_LLStats_SSE3	ENDP


ALIGN 16
JPEG_ScanPack	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12

	PUSH rsi
	PUSH rdi
	
	PUSH rbx

		MOV rdi, rcx
		MOV rsi, [rdx+r8*8+32]
		MOV r11, [rdx+r8*8+40]

		MOV r8, rdx
		MOV r12, [r8+96] ; dc codes
		MOV r13, [r8+104] ; dc lengthes
		MOV r14, [r8+112] ; ac codes
		MOV r15, [r8+120] ; ac lengthes
		
		MOV [r8+72], rdi

		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx
		XOR r9, r9
		XOR r10, r10
		
	align 16
		dc_loop:
			CMP rsi, r11
			JAE last_offload

			XOR rax, rax

			LODSB

			MOV cl, [r13+rax]
			ADD bl, cl

			SHL rdx, cl
			OR dx, [r12+rax*2]

			MOV cl, 0Fh
			AND cl, al
			JZ nodc_bits
				MOV r9w, 1
			
				ADD bl, cl
				SHL rdx, cl
			
				SHL r9w, cl
				DEC r9w

				CMP cl, 8
				JA dc_wbits
					LODSB

					AND ax, r9w
					OR dx, ax
				JMP nodc_bits
				dc_wbits:
					LODSW
				
					AND ax, r9w
					OR dx, ax
		align 16
			nodc_bits:

			CMP bl, 8
			JB no_obytes_1
				SUB bl, 8

				MOV cl, bl
				ROR rdx, cl
				MOV r10b, dl
				MOV dl, 0
				ROL rdx, cl

				MOV cl, 0
				ADD r10b, 1
				ADC cl, 0
				SUB r10b, 1

				ROR r10, 8

				INC bh
				CMP bh, 8
				JB no_offload_1					
					MOVNTI [rdi], r10
					ADD rdi, 8
					XOR r10, r10
					MOV bh, 0
				no_offload_1:
				
				TEST cl, cl				
				JZ nodc_bits
					ROR r10, 8

					INC bh
					CMP bh, 8
					JB nodc_bits
						MOVNTI [rdi], r10
						ADD rdi, 8
						XOR r10, r10
						MOV bh, 0
					

				JMP nodc_bits
		align 16
			no_obytes_1:


			MOV ch, 0

		align 16
			ac_loop:
				XOR rax, rax

				LODSB

				MOV cl, [r15+rax]
				ADD bl, cl

				SHL rdx, cl
				OR dx, [r14+rax*2]

				TEST al, al
				JZ dc_loop
					MOV cl, 0Fh
					AND cl, al

					SHR al, 4
					INC al

					ADD ch, al


					TEST cl, cl
					JZ noac_bits

						MOV r9w, 1
			
						ADD bl, cl
						SHL rdx, cl
			
						SHL r9w, cl
						DEC r9w

						CMP cl, 8
						JA ac_wbits
							LODSB

							AND ax, r9w
							OR dx, ax
							JMP noac_bits
						ac_wbits:
							LODSW
				
							AND ax, r9w
							OR dx, ax
					align 16
						noac_bits:

						CMP bl, 8
						JB no_obytes_2
							SUB bl, 8

							MOV cl, bl
							ROR rdx, cl
							MOV r10b, dl
							MOV dl, 0
							ROL rdx, cl

							MOV cl, 0
							ADD r10b, 1
							ADC cl, 0
							SUB r10b, 1

							ROR r10, 8

							INC bh
							CMP bh, 8
							JB no_offload_3
								MOVNTI [rdi], r10
								ADD rdi, 8
								XOR r10, r10
								MOV bh, 0
							no_offload_3:
				
							TEST cl, cl							
							JZ noac_bits
								ROR r10, 8

								INC bh
								CMP bh, 8
								JB noac_bits
									MOVNTI [rdi], r10
									ADD rdi, 8
									XOR r10, r10
									MOV bh, 0
								

						JMP noac_bits
				align 16
					no_obytes_2:

					CMP ch, 63
					JB ac_loop
							
		JMP dc_loop
	align 16
		last_offload:
			CMP bl, 8
			JB no_obytes_3
				SUB bl, 8

				MOV cl, bl
				ROR rdx, cl
				MOV r10b, dl
				MOV dl, 0
				ROL rdx, cl

				MOV cl, 0
				ADD r10b, 1
				ADC cl, 0
				SUB r10b, 1
				
				ROR r10, 8

				INC bh
				CMP bh, 8
				JB no_offload_5
					MOVNTI [rdi], r10
					ADD rdi, 8
					XOR r10, r10
					MOV bh, 0
				no_offload_5:
				
				TEST cl, cl
				JZ last_offload
					ROR r10, 8

					INC bh
					CMP bh, 8
					JB last_offload
						MOVNTI [rdi], r10
						ADD rdi, 8
						XOR r10, r10
						MOV bh, 0

			JMP last_offload
		no_obytes_3:
			
			TEST bl, bl
			JZ no_pbyte
				MOV cl, 8
				SUB cl, bl
				MOV al, 1

				SHL al, cl				
				SHL dl, cl
				DEC al

				OR dl, al

				INC bh

				MOV r10b, dl

				ROR r10, 8
			no_pbyte:
				ROL r10, 8
				
				MOV cl, 0
				ADD r10b, 1
				ADC cl, 0
				SUB r10b, 1

				ROR r10, 8

				CMP bh, 8
				JB no_offload_7
					MOVNTI [rdi], r10
					ADD rdi, 8
					XOR r10, r10
					MOV bh, 0
				no_offload_7:

				MOV bl, bh
				ADD bl, cl

				MOV cl, 8
				SUB cl, bh
				SHL cl, 3

				SHR r10, cl

				TEST bl, bl
				JZ no_leftover

					do_leftover:
						MOV [rdi], r10b
						INC rdi
						SHR r10, 8
					DEC bl
					JNZ do_leftover

				no_leftover:

		SUB rdi, [r8+72]
		MOV [r8+72], rdi

	SFENCE

	POP rbx

	POP rdi
	POP rsi

	POP r12
	POP r13
	POP r14
	POP r15

	RET
JPEG_ScanPack	ENDP

ALIGN 16
JPEG_FDCT_CT_SSSE3	PROC
	PUSH rbp

	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12
	
	PUSH rsi
	PUSH rdi
	PUSH rbx

	LEA rbp, [y_scal_602]
	CMP r9, 1
	JNZ no_type_1
		LEA rbp, [y_scal_709]
	no_type_1:

	PXOR xmm14, xmm14

	MOV rsi, rdx

	MOV r15, [r8+112]

	SHL r15, 2

	CMP QWORD PTR [r8+72], 20
	JNZ nohwad20
		MOV r9, [r8]
		ADD r9, 15
		SHR r9, 4
		SHL r9, 1

		MOV rax, [r8+8]
		ADD rax, 15
		SHR rax, 4
		SHL rax, 1

		JMP hawd

	nohwad20:

		CMP QWORD PTR [r8+72], 40
		JNZ nohwad40
			MOV r9, [r8]
			ADD r9, 7
			SHR r9, 3

			MOV rax, [r8+8]
			ADD rax, 15
			SHR rax, 4
			SHL rax, 1

			JMP hawd
		nohwad40:

			CMP QWORD PTR [r8+72], 22
			JNZ nohwad22
				MOV r9, [r8]
				ADD r9, 15
				SHR r9, 4
				SHL r9, 1

				MOV rax, [r8+8]
				ADD rax, 7
				SHR rax, 3

				JMP hawd
			nohwad22:

				MOV r9, [r8]
				ADD r9, 7
				SHR r9, 3

				MOV rax, [r8+8]
				ADD rax, 7
				SHR rax, 3


	hawd:
		MOV rbx, r9
		SHL rbx, 3
		MOV [r8+16], rbx
		MOV [r8+112], rbx

		MOV rbx, rax
		SHL rbx, 3
		MOV [r8+24], rbx
		MOV [r8+120], rbx

		MOV rbx, rdx
		MOV r10, rax

			XOR rdx, rdx
			MUL r9

			SHL rax, 6
			MOV [r8+64], rax

			MOV rdx, [r8+32]
			LEA rdx, [rdx+rax*4]
			MOV [r8+40], rdx
			MOV r12, rdx
			ADD r12, rcx

			LEA rdx, [rdx+rax*2]
			MOV [r8+48], rdx
			MOV r13, rdx
			ADD r13, rcx


			LEA rdx, [rdx+rax*2]
			MOV [r8+56], rdx
			MOV r14, rdx
			ADD r14, rcx


		MOV rax, r10
		MOV rdx, rbx
	
	
	MOV r8d, r9d
	
	

align 16
	ro_loop:


; color transform y
	
			MOVDQA xmm15, XMMWORD PTR [rbp]
			MOVDQA xmm13, XMMWORD PTR [half_12]

			XOR rbx, rbx
			MOVDQA xmm0, [rsi]
			MOVDQA xmm2, [rsi+16]
			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2
			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14						
			PMADDWD xmm0, xmm15
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PHADDD xmm0, xmm1
			PHADDD xmm2, xmm3
			PSRAD xmm0, 11
			PSRAD xmm2, 11
			PACKSSDW xmm0, xmm2
			PSUBW xmm0, xmm13

			ADD rbx, r15
			MOVDQA xmm1, [rsi+rbx]
			MOVDQA xmm3, [rsi+rbx+16]
			MOVDQA xmm2, xmm1
			MOVDQA xmm4, xmm3
			PUNPCKLBW xmm1, xmm14
			PUNPCKHBW xmm2, xmm14
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14						
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PHADDD xmm1, xmm2
			PHADDD xmm3, xmm4
			PSRAD xmm1, 11
			PSRAD xmm3, 11
			PACKSSDW xmm1, xmm3
			PSUBW xmm1, xmm13

			ADD rbx, r15
			MOVDQA xmm2, [rsi+rbx]
			MOVDQA xmm4, [rsi+rbx+16]
			MOVDQA xmm3, xmm2
			MOVDQA xmm5, xmm4
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14						
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PHADDD xmm2, xmm3
			PHADDD xmm4, xmm5
			PSRAD xmm2, 11
			PSRAD xmm4, 11
			PACKSSDW xmm2, xmm4
			PSUBW xmm2, xmm13

			ADD rbx, r15
			MOVDQA xmm3, [rsi+rbx]
			MOVDQA xmm5, [rsi+rbx+16]
			MOVDQA xmm4, xmm3
			MOVDQA xmm6, xmm5
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14						
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PHADDD xmm3, xmm4
			PHADDD xmm5, xmm6
			PSRAD xmm3, 11
			PSRAD xmm5, 11
			PACKSSDW xmm3, xmm5
			PSUBW xmm3, xmm13

			ADD rbx, r15
			MOVDQA xmm4, [rsi+rbx]
			MOVDQA xmm6, [rsi+rbx+16]
			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14						
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PHADDD xmm4, xmm5
			PHADDD xmm6, xmm7
			PSRAD xmm4, 11
			PSRAD xmm6, 11
			PACKSSDW xmm4, xmm6
			PSUBW xmm4, xmm13

			ADD rbx, r15
			MOVDQA xmm5, [rsi+rbx]
			MOVDQA xmm7, [rsi+rbx+16]
			MOVDQA xmm6, xmm5
			MOVDQA xmm8, xmm7
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14						
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PHADDD xmm5, xmm6
			PHADDD xmm7, xmm8
			PSRAD xmm5, 11
			PSRAD xmm7, 11
			PACKSSDW xmm5, xmm7
			PSUBW xmm5, xmm13

			ADD rbx, r15
			MOVDQA xmm6, [rsi+rbx]
			MOVDQA xmm8, [rsi+rbx+16]
			MOVDQA xmm7, xmm6
			MOVDQA xmm9, xmm8
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14
			PUNPCKLBW xmm8, xmm14
			PUNPCKHBW xmm9, xmm14						
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PHADDD xmm6, xmm7
			PHADDD xmm8, xmm9
			PSRAD xmm6, 11
			PSRAD xmm8, 11
			PACKSSDW xmm6, xmm8
			PSUBW xmm6, xmm13

			ADD rbx, r15
			MOVDQA xmm7, [rsi+rbx]
			MOVDQA xmm9, [rsi+rbx+16]
			MOVDQA xmm8, xmm7
			MOVDQA xmm10, xmm9
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14
			PUNPCKLBW xmm9, xmm14
			PUNPCKHBW xmm10, xmm14						
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PMADDWD xmm10, xmm15
			PHADDD xmm7, xmm8
			PHADDD xmm9, xmm10
			PSRAD xmm7, 11
			PSRAD xmm9, 11
			PACKSSDW xmm7, xmm9
			PSUBW xmm7, xmm13



; calc y freqs
			MOV edi, 8
			XOR r10, r10
			LEA r11, [?fdct_cofs@JPEG@Pics@@0PAY177FA]

		align 16
			y_freq_loop:				
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm1
				PMADDWD xmm8, [r11]
				PMADDWD xmm9, [r11+16]		
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				PMADDWD xmm10, [r11+32]
				PMADDWD xmm11, [r11+48]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm4
				MOVDQA xmm11, xmm5
				PMADDWD xmm10, [r11+64]
				PMADDWD xmm11, [r11+80]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm6
				MOVDQA xmm11, xmm7
				PMADDWD xmm10, [r11+96]
				PMADDWD xmm11, [r11+112]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11

				PHADDD xmm8, xmm9

				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
					
				PHADDD xmm9, xmm10
				PHADDD xmm8, xmm9
				
				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				PHADDD xmm8, xmm9


				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11

				ADD r11, 128
				MOVDQA xmm11, xmm0
				MOVDQA xmm12, xmm1
				PMADDWD xmm11, [r11]
				PMADDWD xmm12, [r11+16]		
				MOVDQA xmm13, xmm2
				MOVDQA xmm15, xmm3
				PMADDWD xmm13, [r11+32]
				PMADDWD xmm15, [r11+48]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm4
				MOVDQA xmm15, xmm5
				PMADDWD xmm13, [r11+64]
				PMADDWD xmm15, [r11+80]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm6
				MOVDQA xmm15, xmm7
				PMADDWD xmm13, [r11+96]
				PMADDWD xmm15, [r11+112]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15

				PHADDD xmm11, xmm12
				PHADDD xmm10, xmm11

				PHADDD xmm9, xmm10
			
				PSRAD xmm8, 18
				PSRAD xmm9, 18

				MOVDQA xmm10, xmm8
				PACKSSDW xmm8, xmm9
					
				MOVNTDQ [r12], xmm8
				PABSW xmm8, xmm8				
								
				PABSD xmm9, xmm9
				PABSD xmm10, xmm10
				
				MOVDQA xmm12, xmm8
				MOVDQA xmm11, xmm9

				PMINSW xmm8, [rcx+r10]
				PMAXSW xmm12, [rcx+r10+512]

				MOVDQA [rcx+r10], xmm8
				MOVDQA [rcx+r10+512], xmm12
							
				MOVDQA xmm8, xmm10
			
				PUNPCKLDQ xmm8, xmm14
				PUNPCKHDQ xmm10, xmm14
				PUNPCKLDQ xmm9, xmm14
				PUNPCKHDQ xmm11, xmm14

				PADDQ xmm8, [rcx+r10*4+1024]
				PADDQ xmm10, [rcx+r10*4+1040]
				PADDQ xmm9, [rcx+r10*4+1056]
				PADDQ xmm11, [rcx+r10*4+1072]

				MOVDQA [rcx+r10*4+1024], xmm8
				MOVDQA [rcx+r10*4+1040], xmm10
				MOVDQA [rcx+r10*4+1056], xmm9
				MOVDQA [rcx+r10*4+1072], xmm11
			
				ADD r11, 128
				ADD r12, 16
				ADD r10, 16
			DEC edi
			JNZ y_freq_loop





; color transform cb
	
			MOVDQA xmm15, XMMWORD PTR [rbp+16]

			XOR rbx, rbx
			MOVDQA xmm0, [rsi]
			MOVDQA xmm2, [rsi+16]
			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2
			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14						
			PMADDWD xmm0, xmm15
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PHADDD xmm0, xmm1
			PHADDD xmm2, xmm3
			PSRAD xmm0, 11
			PSRAD xmm2, 11
			PACKSSDW xmm0, xmm2

			ADD rbx, r15
			MOVDQA xmm1, [rsi+rbx]
			MOVDQA xmm3, [rsi+rbx+16]
			MOVDQA xmm2, xmm1
			MOVDQA xmm4, xmm3
			PUNPCKLBW xmm1, xmm14
			PUNPCKHBW xmm2, xmm14
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14						
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PHADDD xmm1, xmm2
			PHADDD xmm3, xmm4
			PSRAD xmm1, 11
			PSRAD xmm3, 11
			PACKSSDW xmm1, xmm3

			ADD rbx, r15
			MOVDQA xmm2, [rsi+rbx]
			MOVDQA xmm4, [rsi+rbx+16]
			MOVDQA xmm3, xmm2
			MOVDQA xmm5, xmm4
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14						
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PHADDD xmm2, xmm3
			PHADDD xmm4, xmm5
			PSRAD xmm2, 11
			PSRAD xmm4, 11
			PACKSSDW xmm2, xmm4

			ADD rbx, r15
			MOVDQA xmm3, [rsi+rbx]
			MOVDQA xmm5, [rsi+rbx+16]
			MOVDQA xmm4, xmm3
			MOVDQA xmm6, xmm5
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14						
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PHADDD xmm3, xmm4
			PHADDD xmm5, xmm6
			PSRAD xmm3, 11
			PSRAD xmm5, 11
			PACKSSDW xmm3, xmm5

			ADD rbx, r15
			MOVDQA xmm4, [rsi+rbx]
			MOVDQA xmm6, [rsi+rbx+16]
			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14						
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PHADDD xmm4, xmm5
			PHADDD xmm6, xmm7
			PSRAD xmm4, 11
			PSRAD xmm6, 11
			PACKSSDW xmm4, xmm6

			ADD rbx, r15
			MOVDQA xmm5, [rsi+rbx]
			MOVDQA xmm7, [rsi+rbx+16]
			MOVDQA xmm6, xmm5
			MOVDQA xmm8, xmm7
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14						
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PHADDD xmm5, xmm6
			PHADDD xmm7, xmm8
			PSRAD xmm5, 11
			PSRAD xmm7, 11
			PACKSSDW xmm5, xmm7

			ADD rbx, r15
			MOVDQA xmm6, [rsi+rbx]
			MOVDQA xmm8, [rsi+rbx+16]
			MOVDQA xmm7, xmm6
			MOVDQA xmm9, xmm8
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14
			PUNPCKLBW xmm8, xmm14
			PUNPCKHBW xmm9, xmm14						
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PHADDD xmm6, xmm7
			PHADDD xmm8, xmm9
			PSRAD xmm6, 11
			PSRAD xmm8, 11
			PACKSSDW xmm6, xmm8

			ADD rbx, r15
			MOVDQA xmm7, [rsi+rbx]
			MOVDQA xmm9, [rsi+rbx+16]
			MOVDQA xmm8, xmm7
			MOVDQA xmm10, xmm9
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14
			PUNPCKLBW xmm9, xmm14
			PUNPCKHBW xmm10, xmm14						
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PMADDWD xmm10, xmm15
			PHADDD xmm7, xmm8
			PHADDD xmm9, xmm10
			PSRAD xmm7, 11
			PSRAD xmm9, 11
			PACKSSDW xmm7, xmm9



; calc cb freqs
			MOV edi, 8
			XOR r10, r10
			LEA r11, [?fdct_cofs@JPEG@Pics@@0PAY177FA]

		align 16
			cb_freq_loop:
				
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm1
				PMADDWD xmm8, [r11]
				PMADDWD xmm9, [r11+16]		
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				PMADDWD xmm10, [r11+32]
				PMADDWD xmm11, [r11+48]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm4
				MOVDQA xmm11, xmm5
				PMADDWD xmm10, [r11+64]
				PMADDWD xmm11, [r11+80]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm6
				MOVDQA xmm11, xmm7
				PMADDWD xmm10, [r11+96]
				PMADDWD xmm11, [r11+112]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11

				PHADDD xmm8, xmm9

				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
					
				PHADDD xmm9, xmm10
				PHADDD xmm8, xmm9
				
				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				PHADDD xmm8, xmm9


				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11

				ADD r11, 128
				MOVDQA xmm11, xmm0
				MOVDQA xmm12, xmm1
				PMADDWD xmm11, [r11]
				PMADDWD xmm12, [r11+16]		
				MOVDQA xmm13, xmm2
				MOVDQA xmm15, xmm3
				PMADDWD xmm13, [r11+32]
				PMADDWD xmm15, [r11+48]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm4
				MOVDQA xmm15, xmm5
				PMADDWD xmm13, [r11+64]
				PMADDWD xmm15, [r11+80]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm6
				MOVDQA xmm15, xmm7
				PMADDWD xmm13, [r11+96]
				PMADDWD xmm15, [r11+112]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15

				PHADDD xmm11, xmm12
				PHADDD xmm10, xmm11

				PHADDD xmm9, xmm10
			
				PSRAD xmm8, 18
				PSRAD xmm9, 18

				MOVDQA xmm10, xmm8
				PACKSSDW xmm8, xmm9
					
				MOVNTDQ [r13], xmm8
				PABSW xmm8, xmm8				
								
				PABSD xmm9, xmm9
				PABSD xmm10, xmm10
				
				MOVDQA xmm12, xmm8
				MOVDQA xmm11, xmm9

				PMINSW xmm8, [rcx+r10+128]
				PMAXSW xmm12, [rcx+r10+640]

				MOVDQA [rcx+r10+128], xmm8
				MOVDQA [rcx+r10+640], xmm12
							
				MOVDQA xmm8, xmm10
			
				PUNPCKLDQ xmm8, xmm14
				PUNPCKHDQ xmm10, xmm14
				PUNPCKLDQ xmm9, xmm14
				PUNPCKHDQ xmm11, xmm14

				PADDQ xmm8, [rcx+r10*4+1536]
				PADDQ xmm10, [rcx+r10*4+1552]
				PADDQ xmm9, [rcx+r10*4+1568]
				PADDQ xmm11, [rcx+r10*4+1584]

				MOVDQA [rcx+r10*4+1536], xmm8
				MOVDQA [rcx+r10*4+1552], xmm10
				MOVDQA [rcx+r10*4+1568], xmm9
				MOVDQA [rcx+r10*4+1584], xmm11
			
				ADD r11, 128
				ADD r13, 16
				ADD r10, 16
			DEC edi
			JNZ cb_freq_loop



; color transform cr
	
			MOVDQA xmm15, XMMWORD PTR [rbp+32]

			XOR rbx, rbx
			MOVDQA xmm0, [rsi]
			MOVDQA xmm2, [rsi+16]
			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2
			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14						
			PMADDWD xmm0, xmm15
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PHADDD xmm0, xmm1
			PHADDD xmm2, xmm3
			PSRAD xmm0, 11
			PSRAD xmm2, 11
			PACKSSDW xmm0, xmm2

			ADD rbx, r15
			MOVDQA xmm1, [rsi+rbx]
			MOVDQA xmm3, [rsi+rbx+16]
			MOVDQA xmm2, xmm1
			MOVDQA xmm4, xmm3
			PUNPCKLBW xmm1, xmm14
			PUNPCKHBW xmm2, xmm14
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14						
			PMADDWD xmm1, xmm15
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PHADDD xmm1, xmm2
			PHADDD xmm3, xmm4
			PSRAD xmm1, 11
			PSRAD xmm3, 11
			PACKSSDW xmm1, xmm3

			ADD rbx, r15
			MOVDQA xmm2, [rsi+rbx]
			MOVDQA xmm4, [rsi+rbx+16]
			MOVDQA xmm3, xmm2
			MOVDQA xmm5, xmm4
			PUNPCKLBW xmm2, xmm14
			PUNPCKHBW xmm3, xmm14
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14						
			PMADDWD xmm2, xmm15
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PHADDD xmm2, xmm3
			PHADDD xmm4, xmm5
			PSRAD xmm2, 11
			PSRAD xmm4, 11
			PACKSSDW xmm2, xmm4

			ADD rbx, r15
			MOVDQA xmm3, [rsi+rbx]
			MOVDQA xmm5, [rsi+rbx+16]
			MOVDQA xmm4, xmm3
			MOVDQA xmm6, xmm5
			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14						
			PMADDWD xmm3, xmm15
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PHADDD xmm3, xmm4
			PHADDD xmm5, xmm6
			PSRAD xmm3, 11
			PSRAD xmm5, 11
			PACKSSDW xmm3, xmm5

			ADD rbx, r15
			MOVDQA xmm4, [rsi+rbx]
			MOVDQA xmm6, [rsi+rbx+16]
			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6
			PUNPCKLBW xmm4, xmm14
			PUNPCKHBW xmm5, xmm14
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14						
			PMADDWD xmm4, xmm15
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PHADDD xmm4, xmm5
			PHADDD xmm6, xmm7
			PSRAD xmm4, 11
			PSRAD xmm6, 11
			PACKSSDW xmm4, xmm6

			ADD rbx, r15
			MOVDQA xmm5, [rsi+rbx]
			MOVDQA xmm7, [rsi+rbx+16]
			MOVDQA xmm6, xmm5
			MOVDQA xmm8, xmm7
			PUNPCKLBW xmm5, xmm14
			PUNPCKHBW xmm6, xmm14
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14						
			PMADDWD xmm5, xmm15
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PHADDD xmm5, xmm6
			PHADDD xmm7, xmm8
			PSRAD xmm5, 11
			PSRAD xmm7, 11
			PACKSSDW xmm5, xmm7

			ADD rbx, r15
			MOVDQA xmm6, [rsi+rbx]
			MOVDQA xmm8, [rsi+rbx+16]
			MOVDQA xmm7, xmm6
			MOVDQA xmm9, xmm8
			PUNPCKLBW xmm6, xmm14
			PUNPCKHBW xmm7, xmm14
			PUNPCKLBW xmm8, xmm14
			PUNPCKHBW xmm9, xmm14						
			PMADDWD xmm6, xmm15
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PHADDD xmm6, xmm7
			PHADDD xmm8, xmm9
			PSRAD xmm6, 11
			PSRAD xmm8, 11
			PACKSSDW xmm6, xmm8

			ADD rbx, r15
			MOVDQA xmm7, [rsi+rbx]
			MOVDQA xmm9, [rsi+rbx+16]
			MOVDQA xmm8, xmm7
			MOVDQA xmm10, xmm9
			PUNPCKLBW xmm7, xmm14
			PUNPCKHBW xmm8, xmm14
			PUNPCKLBW xmm9, xmm14
			PUNPCKHBW xmm10, xmm14						
			PMADDWD xmm7, xmm15
			PMADDWD xmm8, xmm15
			PMADDWD xmm9, xmm15
			PMADDWD xmm10, xmm15
			PHADDD xmm7, xmm8
			PHADDD xmm9, xmm10
			PSRAD xmm7, 11
			PSRAD xmm9, 11
			PACKSSDW xmm7, xmm9



; calc cr freqs
			MOV edi, 8
			XOR r10, r10
			LEA r11, [?fdct_cofs@JPEG@Pics@@0PAY177FA]

		align 16
			cr_freq_loop:				
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm1
				PMADDWD xmm8, [r11]
				PMADDWD xmm9, [r11+16]		
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				PMADDWD xmm10, [r11+32]
				PMADDWD xmm11, [r11+48]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm4
				MOVDQA xmm11, xmm5
				PMADDWD xmm10, [r11+64]
				PMADDWD xmm11, [r11+80]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11
				MOVDQA xmm10, xmm6
				MOVDQA xmm11, xmm7
				PMADDWD xmm10, [r11+96]
				PMADDWD xmm11, [r11+112]
				PADDD xmm8, xmm10
				PADDD xmm9, xmm11

				PHADDD xmm8, xmm9

				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
					
				PHADDD xmm9, xmm10
				PHADDD xmm8, xmm9
				
				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				PHADDD xmm8, xmm9


				ADD r11, 128
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				PMADDWD xmm9, [r11]
				PMADDWD xmm10, [r11+16]		
				MOVDQA xmm11, xmm2
				MOVDQA xmm12, xmm3
				PMADDWD xmm11, [r11+32]
				PMADDWD xmm12, [r11+48]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm4
				MOVDQA xmm12, xmm5
				PMADDWD xmm11, [r11+64]
				PMADDWD xmm12, [r11+80]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7
				PMADDWD xmm11, [r11+96]
				PMADDWD xmm12, [r11+112]
				PADDD xmm9, xmm11
				PADDD xmm10, xmm12

				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11
				PHADDD xmm9, xmm10

				ADD r11, 128
				MOVDQA xmm10, xmm0
				MOVDQA xmm11, xmm1
				PMADDWD xmm10, [r11]
				PMADDWD xmm11, [r11+16]		
				MOVDQA xmm12, xmm2
				MOVDQA xmm13, xmm3
				PMADDWD xmm12, [r11+32]
				PMADDWD xmm13, [r11+48]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm4
				MOVDQA xmm13, xmm5
				PMADDWD xmm12, [r11+64]
				PMADDWD xmm13, [r11+80]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13
				MOVDQA xmm12, xmm6
				MOVDQA xmm13, xmm7
				PMADDWD xmm12, [r11+96]
				PMADDWD xmm13, [r11+112]
				PADDD xmm10, xmm12
				PADDD xmm11, xmm13

				PHADDD xmm10, xmm11

				ADD r11, 128
				MOVDQA xmm11, xmm0
				MOVDQA xmm12, xmm1
				PMADDWD xmm11, [r11]
				PMADDWD xmm12, [r11+16]		
				MOVDQA xmm13, xmm2
				MOVDQA xmm15, xmm3
				PMADDWD xmm13, [r11+32]
				PMADDWD xmm15, [r11+48]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm4
				MOVDQA xmm15, xmm5
				PMADDWD xmm13, [r11+64]
				PMADDWD xmm15, [r11+80]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15
				MOVDQA xmm13, xmm6
				MOVDQA xmm15, xmm7
				PMADDWD xmm13, [r11+96]
				PMADDWD xmm15, [r11+112]
				PADDD xmm11, xmm13
				PADDD xmm12, xmm15

				PHADDD xmm11, xmm12
				PHADDD xmm10, xmm11

				PHADDD xmm9, xmm10
			
				PSRAD xmm8, 18
				PSRAD xmm9, 18

				MOVDQA xmm10, xmm8
				PACKSSDW xmm8, xmm9
					
				MOVNTDQ [r14], xmm8
				PABSW xmm8, xmm8				
								
				PABSD xmm9, xmm9
				PABSD xmm10, xmm10
				
				MOVDQA xmm12, xmm8
				MOVDQA xmm11, xmm9

				PMINSW xmm8, [rcx+r10+256]
				PMAXSW xmm12, [rcx+r10+768]

				MOVDQA [rcx+r10+256], xmm8
				MOVDQA [rcx+r10+768], xmm12
							
				MOVDQA xmm8, xmm10
			
				PUNPCKLDQ xmm8, xmm14
				PUNPCKHDQ xmm10, xmm14
				PUNPCKLDQ xmm9, xmm14
				PUNPCKHDQ xmm11, xmm14

				PADDQ xmm8, [rcx+r10*4+2048]
				PADDQ xmm10, [rcx+r10*4+2064]
				PADDQ xmm9, [rcx+r10*4+2080]
				PADDQ xmm11, [rcx+r10*4+2096]

				MOVDQA [rcx+r10*4+2048], xmm8
				MOVDQA [rcx+r10*4+2064], xmm10
				MOVDQA [rcx+r10*4+2080], xmm9
				MOVDQA [rcx+r10*4+2096], xmm11
			
				ADD r11, 128
				ADD r14, 16
				ADD r10, 16
			DEC edi
			JNZ cr_freq_loop


			ADD rsi, 32


		DEC r9d
		JNZ ro_loop

		MOV r9d, r8d
		LEA rdx, [rdx+r15*8]
		MOV rsi, rdx
	DEC eax
	JNZ ro_loop


	SFENCE

	POP rbx
	POP rdi
	POP rsi

	POP r12
	POP r13
	POP r14
	POP r15

	POP rbp
	RET
JPEG_FDCT_CT_SSSE3	ENDP



ALIGN 16
JPEG_ApplyQTs_SSSE3	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12

	PUSH rsi
	PUSH rdi

	PUSH rbx


	XOR r12, r12
	XOR r13, r13
	XOR r14, r14
	XOR r15, r15
	XOR r11, r11
	XOR r10, r10
	
	MOV rdi, [rdx+32]
	MOV rsi, [rdx+40]
	MOV r8, [rdx+64]

	LEA rdi, [rdi+r8*2]

	ADD rdi, rcx
	ADD rsi, rcx	

MOV [rdx+32], rdi

	MOV rbx, rcx
	
	SHR r8, 6
	MOV r9, r8
	
	MOVDQA xmm14, XMMWORD PTR [one_w]
		
	PXOR xmm15, xmm15

	MOVDQA xmm0, [rbx]
	MOVDQA xmm1, [rbx+16]
	MOVDQA xmm2, [rbx+32]
	MOVDQA xmm3, [rbx+48]
	MOVDQA xmm4, [rbx+64]
	MOVDQA xmm5, [rbx+80]
	MOVDQA xmm6, [rbx+96]
	MOVDQA xmm7, [rbx+112]

	MOVDQA [rbx], xmm15
	MOVDQA [rbx+16], xmm15
	MOVDQA [rbx+32], xmm15
	MOVDQA [rbx+48], xmm15
	MOVDQA [rbx+64], xmm15
	MOVDQA [rbx+80], xmm15
	MOVDQA [rbx+96], xmm15
	MOVDQA [rbx+112], xmm15


	XOR rcx, rcx
	XOR rax, rax
	
	
align 16
	y_scale_loop:
		MOVDQA xmm8, [rsi]
		MOVDQA xmm9, [rsi+16]
		MOVDQA xmm10, [rsi+32]
		MOVDQA xmm11, [rsi+48]

		
		PMULHW xmm8, xmm0
		MOVDQA xmm12, xmm8
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm8, xmm12

		PMULHW xmm9, xmm1
		MOVDQA xmm12, xmm9
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm9, xmm12

		PMULHW xmm10, xmm2
		MOVDQA xmm12, xmm10
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm10, xmm12

		PMULHW xmm11, xmm3
		MOVDQA xmm12, xmm11
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm11, xmm12
		


				
		PABSW xmm12, xmm8
		
		MOVD r10, xmm8
		PSRLDQ xmm8, 8
		MOVD r11, xmm8
		
		MOVDQA xmm8, xmm12
		PCMPEQW xmm8, xmm15

		MOVD r12, xmm12
		PSRLDQ xmm12, 8
		MOVD r13, xmm12
		
		PMOVMSKB r14d, xmm8
		
; dc predictor
		MOV ax, r10w
		
		SUB ax, r15w
		JZ zero_dc_0
			MOV r15w, ax
			
			MOV cx, 8000h
			AND cx, ax
			ROL cx, 1

			SUB r15w, cx
			
			MOV cx, ax
			NEG cx
			CMOVS cx, ax
						
			BSR ax, cx
			INC ax

		zero_dc_0:

		INC QWORD PTR [rbx+rax*8]

		STOSB
		
		TEST al, al
		JZ no_dcdb_0
			XCHG ax, r15w
			STOSB			
			CMP r15w, 8
			JBE no_dcdb_0
				SHR ax, 8
				STOSB
		no_dcdb_0:


		MOV r15w, r10w

		SHR r10, 16
		SHR r12, 16
		SHR r14b, 2
	
; ac_cofs

		XOR ecx, ecx

		zero_counter_0:
			ADD cx, 0101h
			SHR r14b, 2
			JNC zero_counter_0_exit
				SHR r10, 16
				SHR r12, 16
		JMP zero_counter_0
		
	zero_counter_0_exit:		
		DEC cl

		CMP ch, 3
		JA do_r11_0
			
			SHL cl, 4

			BSR ax, r12w
			INC ax

			OR al, cl			
			MOV cl, al
			AND cl, 0Fh

			INC QWORD PTR [rbx+rax*8+512]

			STOSB
			
			MOV ax, r12w
			XOR ax, r10w
			SHL ax, 1
			MOV ax, r10w
			SBB ax, 0
			
			STOSB			
				
			CMP cl, 8
			JBE no_acdb_0
				SHR ax, 8
				STOSB
			no_acdb_0:


			SHR r10, 16
			SHR r12, 16

			MOV cl, 0
		JMP zero_counter_0

	align 16
		do_r11_0:
			MOV ch, 0
			SHR r14d, 8

			zero_counter_1:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_1_exit
					SHR r11, 16
					SHR r13, 16
			JMP zero_counter_1

		zero_counter_1_exit:
			DEC cl

			CMP ch, 4  ; 7
			JA do_xmm9_0
				
				SHL cl, 4

				BSR ax, r13w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh


				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r13w
				XOR ax, r11w
				SHL ax, 1
				MOV ax, r11w
				SBB ax, 0
				
				STOSB			

				CMP cl, 8
				JBE no_acdb_1
					SHR ax, 8
					STOSB
				no_acdb_1:

				SHR r11, 16
				SHR r13, 16

				MOV cl, 0
			JMP zero_counter_1


	align 16
		do_xmm9_0:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			zero_counter_2:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_2_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_2

		zero_counter_2_exit:
			DEC cl

			CMP ch, 4 ; 11
			JA do_r11_1
				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh


				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE no_acdb_2
					SHR ax, 8
					STOSB
				no_acdb_2:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_2

		align 16
			do_r11_1:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_3:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_3_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_3

			zero_counter_3_exit:
				DEC cl

				CMP ch, 4 ; 15
				JA do_xmm10_0
					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh


					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_3
						SHR ax, 8
						STOSB
					no_acdb_3:


					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_3

	align 16
		do_xmm10_0:

			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			zero_counter_4:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_4_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_4

		zero_counter_4_exit:
			DEC cl

			CMP ch, 4 ; 19
			JA do_r11_2
				CMP cl, 16
				JB no_zero_run_0
					MOV ax, 0F0h
					
					zero_run_0:
						INC QWORD PTR [rbx+rax*8+512]					
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_0
				no_zero_run_0:
												
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh


				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE no_acdb_4
					SHR ax, 8
					STOSB
				no_acdb_4:


				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_4

		align 16
			do_r11_2:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_5:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_5_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_5

			zero_counter_5_exit:
				DEC cl

				CMP ch, 4 ; 23
				JA do_xmm11_0
					CMP cl, 16
					JB no_zero_run_1
						MOV ax, 0F0h
												
						zero_run_1:
							INC QWORD PTR [rbx+rax*8+512]						
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_1
					no_zero_run_1:

										
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh


					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_5
						SHR ax, 8
						STOSB
					no_acdb_5:


					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_5

	align 16
		do_xmm11_0:

			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			zero_counter_6:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_6_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_6

		zero_counter_6_exit:
			DEC cl

			CMP ch, 4 ; 27
			JA do_r11_3
				CMP cl, 16
				JB no_zero_run_2
					MOV ax, 0F0h
					
					zero_run_2:
						INC QWORD PTR [rbx+rax*8+512]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_2
				no_zero_run_2:

								
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh


				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE no_acdb_6
					SHR ax, 8
					STOSB
				no_acdb_6:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_6

		align 16
			do_r11_3:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_7:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_7_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_7

			zero_counter_7_exit:
				DEC cl

				CMP ch, 4 ; 31
				JA do_xmm8_1
					CMP cl, 16
					JB no_zero_run_3
						MOV ax, 0F0h
						
						zero_run_3:
							INC QWORD PTR [rbx+rax*8+512]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_3
					no_zero_run_3:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh


					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_7
						SHR ax, 8
						STOSB
					no_acdb_7:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_7
	align 16
		do_xmm8_1:

			MOVDQA xmm8, [rsi+64]
			MOVDQA xmm9, [rsi+80]
			MOVDQA xmm10, [rsi+96]
			MOVDQA xmm11, [rsi+112]
		
			PMULHW xmm8, xmm4
			MOVDQA xmm12, xmm8
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm8, xmm12

			PMULHW xmm9, xmm5
			MOVDQA xmm12, xmm9
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm9, xmm12

			PMULHW xmm10, xmm6
			MOVDQA xmm12, xmm10
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm10, xmm12

			PMULHW xmm11, xmm7
			MOVDQA xmm12, xmm11
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm11, xmm12


			PABSW xmm12, xmm8
		
			MOVD r10, xmm8
			PSRLDQ xmm8, 8
			MOVD r11, xmm8
		
			MOVDQA xmm8, xmm12
			PCMPEQW xmm8, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm8
			
			MOV ch, 0

			zero_counter_8:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_8_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_8

		zero_counter_8_exit:
			DEC cl

			CMP ch, 4 ; 35
			JA do_r11_4
				CMP cl, 16
				JB no_zero_run_4
					MOV ax, 0F0h
					
					zero_run_4:
						INC QWORD PTR [rbx+rax*8+512]					
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_4
				no_zero_run_4:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh


				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE no_acdb_8
					SHR ax, 8
					STOSB
				no_acdb_8:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_8

		align 16
			do_r11_4:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_9:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_9_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_9

			zero_counter_9_exit:
				DEC cl

				CMP ch, 4 ; 35
				JA do_xmm9_1
					CMP cl, 16
					JB no_zero_run_5
						MOV ax, 0F0h
						
						zero_run_5:
							INC QWORD PTR [rbx+rax*8+512]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_5
					no_zero_run_5:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh


					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_9
						SHR ax, 8
						STOSB
					no_acdb_9:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_9

	align 16
		do_xmm9_1:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			zero_counter_10:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_10_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_10

		zero_counter_10_exit:
			DEC cl

			CMP ch, 4 ; 39
			JA do_r11_5
				CMP cl, 16
				JB no_zero_run_6
					MOV ax, 0F0h
					
					zero_run_6:
						INC QWORD PTR [rbx+rax*8+512]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_6
				no_zero_run_6:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE no_acdb_10
					SHR ax, 8
					STOSB
				no_acdb_10:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_10

		align 16
			do_r11_5:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_11:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_11_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_11

			zero_counter_11_exit:
				DEC cl

				CMP ch, 4 ; 43
				JA do_xmm10_1
					CMP cl, 16
					JB no_zero_run_7
						MOV ax, 0F0h
						
						zero_run_7:
							INC QWORD PTR [rbx+rax*8+512]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_7
					no_zero_run_7:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_11
						SHR ax, 8
						STOSB
					no_acdb_11:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_11

	align 16
		do_xmm10_1:
			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			zero_counter_12:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_12_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_12

		zero_counter_12_exit:
			DEC cl

			CMP ch, 4 ; 47
			JA do_r11_6
				CMP cl, 16
				JB no_zero_run_8
					MOV ax, 0F0h
					
					zero_run_8:
						INC QWORD PTR [rbx+rax*8+512]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_8
				no_zero_run_8:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE no_acdb_12
					SHR ax, 8
					STOSB
				no_acdb_12:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_12

		align 16
			do_r11_6:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_13:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_13_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_13

			zero_counter_13_exit:
				DEC cl

				CMP ch, 4 ; 51
				JA do_xmm11_1
					CMP cl, 16
					JB no_zero_run_9
						MOV ax, 0F0h
						
						zero_run_9:
							INC QWORD PTR [rbx+rax*8+512]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_9
					no_zero_run_9:


					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_13
						SHR ax, 8
						STOSB
					no_acdb_13:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_13

	align 16
		do_xmm11_1:
			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			zero_counter_14:
				ADD cx, 0101h
				SHR r14b, 2
				JNC zero_counter_14_exit
					SHR r10, 16
					SHR r12, 16
			JMP zero_counter_14

		zero_counter_14_exit:
			DEC cl

			CMP ch, 4 ; 55
			JA do_r11_7
				CMP cl, 16
				JB no_zero_run_10
					MOV ax, 0F0h
					
					zero_run_10:
						INC QWORD PTR [rbx+rax*8+512]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE zero_run_10
				no_zero_run_10:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+512]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE no_acdb_14
					SHR ax, 8
					STOSB
				no_acdb_14:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP zero_counter_14

		align 16
			do_r11_7:
				MOV ch, 0
				SHR r14d, 8

				zero_counter_15:
					ADD cx, 0101h
					SHR r14b, 2
					JNC zero_counter_15_exit
						SHR r11, 16
						SHR r13, 16
				JMP zero_counter_15

			zero_counter_15_exit:
				DEC cl

				CMP ch, 4 ; 59
				JA do_xmmxx
					CMP cl, 16
					JB no_zero_run_11
						MOV ax, 0F0h
						
						zero_run_11:
							INC QWORD PTR [rbx+rax*8+512]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE zero_run_11
					no_zero_run_11:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+512]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE no_acdb_15
						SHR ax, 8
						STOSB
					no_acdb_15:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP zero_counter_15
		align 16
			do_xmmxx:
				TEST cl, cl
				JZ no_eob
					XOR eax, eax
					STOSB

					INC QWORD PTR [rbx+512]

				no_eob:



		ADD rsi, 128
	DEC r8
	JNZ y_scale_loop

	INC QWORD PTR [rbx+120]
	INC QWORD PTR [rbx+2552]

MOV [rdx+40], rdi
	MOV r8, r9





	XOR r15, r15
	XOR rax, rax
	XOR rcx, rcx




	MOVDQA xmm0, [rbx+128]
	MOVDQA xmm1, [rbx+144]
	MOVDQA xmm2, [rbx+160]
	MOVDQA xmm3, [rbx+176]
	MOVDQA xmm4, [rbx+192]
	MOVDQA xmm5, [rbx+208]
	MOVDQA xmm6, [rbx+224]
	MOVDQA xmm7, [rbx+240]

	MOVDQA [rbx+128], xmm15
	MOVDQA [rbx+144], xmm15
	MOVDQA [rbx+160], xmm15
	MOVDQA [rbx+176], xmm15
	MOVDQA [rbx+192], xmm15
	MOVDQA [rbx+208], xmm15
	MOVDQA [rbx+224], xmm15
	MOVDQA [rbx+240], xmm15




align 16
	cb_scale_loop:
		MOVDQA xmm8, [rsi]
		MOVDQA xmm9, [rsi+16]
		MOVDQA xmm10, [rsi+32]
		MOVDQA xmm11, [rsi+48]

		PMULHW xmm8, xmm0
		MOVDQA xmm12, xmm8
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm8, xmm12

		PMULHW xmm9, xmm1
		MOVDQA xmm12, xmm9
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm9, xmm12

		PMULHW xmm10, xmm2
		MOVDQA xmm12, xmm10
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm10, xmm12

		PMULHW xmm11, xmm3
		MOVDQA xmm12, xmm11
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm11, xmm12

				
		PABSW xmm12, xmm8
		
		MOVD r10, xmm8
		PSRLDQ xmm8, 8
		MOVD r11, xmm8
		
		MOVDQA xmm8, xmm12
		PCMPEQW xmm8, xmm15

		MOVD r12, xmm12
		PSRLDQ xmm12, 8
		MOVD r13, xmm12
		
		PMOVMSKB r14d, xmm8
		
; dc predictor
		MOV ax, r10w
		
		SUB ax, r15w
		JZ cb_zero_dc_0
			MOV r15w, ax
			
			MOV cx, 8000h
			AND cx, ax
			ROL cx, 1

			SUB r15w, cx
			
			MOV cx, ax
			NEG cx
			CMOVS cx, ax
						
			BSR ax, cx
			INC ax

		cb_zero_dc_0:

		INC QWORD PTR [rbx+rax*8+128]

		STOSB
		
		TEST al, al
		JZ cb_no_dcdb_0
			XCHG ax, r15w
			STOSB			
			CMP r15w, 8
			JBE cb_no_dcdb_0
				SHR ax, 8
				STOSB
		cb_no_dcdb_0:


		MOV r15w, r10w

		SHR r10, 16
		SHR r12, 16
		SHR r14b, 2
		
; ac_cofs

		XOR ecx, ecx

		cb_zero_counter_0:
			ADD cx, 0101h
			SHR r14b, 2
			JNC cb_zero_counter_0_exit				
				SHR r10, 16
				SHR r12, 16
		JMP cb_zero_counter_0
	cb_zero_counter_0_exit:
		DEC cl

		CMP ch, 3
		JA cb_do_r11_0
			
			SHL cl, 4

			BSR ax, r12w
			INC ax

			OR al, cl
			MOV cl, al
			AND cl, 0Fh

			INC QWORD PTR [rbx+rax*8+2560]

			STOSB
			
			MOV ax, r12w
			XOR ax, r10w
			SHL ax, 1
			MOV ax, r10w
			SBB ax, 0
			
			STOSB
						
			CMP cl, 8
			JBE cb_no_acdb_0
				SHR ax, 8
				STOSB
			cb_no_acdb_0:

			SHR r10, 16
			SHR r12, 16

			MOV cl, 0
		JMP cb_zero_counter_0

	align 16
		cb_do_r11_0:
			MOV ch, 0
			SHR r14d, 8

			cb_zero_counter_1:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_1_exit
					SHR r11, 16
					SHR r13, 16
			JMP cb_zero_counter_1

		cb_zero_counter_1_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_xmm9_0
				
				SHL cl, 4

				BSR ax, r13w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r13w
				XOR ax, r11w
				SHL ax, 1
				MOV ax, r11w
				SBB ax, 0
				
				STOSB			

				CMP cl, 8
				JBE cb_no_acdb_1
					SHR ax, 8
					STOSB
				cb_no_acdb_1:

				SHR r11, 16
				SHR r13, 16

				MOV cl, 0
			JMP cb_zero_counter_1


	align 16
		cb_do_xmm9_0:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			cb_zero_counter_2:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_2_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_2

		cb_zero_counter_2_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_1
				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE cb_no_acdb_2
					SHR ax, 8
					STOSB
				cb_no_acdb_2:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_2

		align 16
			cb_do_r11_1:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_3:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_3_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_3

			cb_zero_counter_3_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm10_0
					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_3
						SHR ax, 8
						STOSB
					cb_no_acdb_3:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_3

	align 16
		cb_do_xmm10_0:

			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			cb_zero_counter_4:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_4_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_4

		cb_zero_counter_4_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_2
				CMP cl, 16
				JB cb_no_zero_run_0
					MOV ax, 0F0h
					
					cb_zero_run_0:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_0
				cb_no_zero_run_0:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE cb_no_acdb_4
					SHR ax, 8
					STOSB
				cb_no_acdb_4:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_4

		align 16
			cb_do_r11_2:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_5:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_5_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_5

			cb_zero_counter_5_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm11_0
					CMP cl, 16
					JB cb_no_zero_run_1
						MOV ax, 0F0h
						
						cb_zero_run_1:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_1
					cb_no_zero_run_1:


					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_5
						SHR ax, 8
						STOSB
					cb_no_acdb_5:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_5

	align 16
		cb_do_xmm11_0:

			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			cb_zero_counter_6:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_6_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_6

		cb_zero_counter_6_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_3
				CMP cl, 16
				JB cb_no_zero_run_2
					MOV ax, 0F0h
					
					cb_zero_run_2:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_2
				cb_no_zero_run_2:
				
				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cb_no_acdb_6
					SHR ax, 8
					STOSB
				cb_no_acdb_6:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_6

		align 16
			cb_do_r11_3:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_7:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_7_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_7

			cb_zero_counter_7_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm8_1
					CMP cl, 16
					JB cb_no_zero_run_3
						MOV ax, 0F0h
						
						cb_zero_run_3:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_3
					cb_no_zero_run_3:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_7
						SHR ax, 8
						STOSB
					cb_no_acdb_7:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_7
	align 16
		cb_do_xmm8_1:

			MOVDQA xmm8, [rsi+64]
			MOVDQA xmm9, [rsi+80]
			MOVDQA xmm10, [rsi+96]
			MOVDQA xmm11, [rsi+112]
		
			PMULHW xmm8, xmm4
			MOVDQA xmm12, xmm8
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm8, xmm12

			PMULHW xmm9, xmm5
			MOVDQA xmm12, xmm9
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm9, xmm12

			PMULHW xmm10, xmm6
			MOVDQA xmm12, xmm10
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm10, xmm12

			PMULHW xmm11, xmm7
			MOVDQA xmm12, xmm11
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm11, xmm12


			PABSW xmm12, xmm8
		
			MOVD r10, xmm8
			PSRLDQ xmm8, 8
			MOVD r11, xmm8
		
			MOVDQA xmm8, xmm12
			PCMPEQW xmm8, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm8
			
			MOV ch, 0

			cb_zero_counter_8:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_8_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_8

		cb_zero_counter_8_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_4
				CMP cl, 16
				JB cb_no_zero_run_4
					MOV ax, 0F0h
					
					cb_zero_run_4:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_4
				cb_no_zero_run_4:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cb_no_acdb_8
					SHR ax, 8
					STOSB
				cb_no_acdb_8:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_8

		align 16
			cb_do_r11_4:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_9:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_9_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_9

			cb_zero_counter_9_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm9_1
					CMP cl, 16
					JB cb_no_zero_run_5
						MOV ax, 0F0h
						
						cb_zero_run_5:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_5
					cb_no_zero_run_5:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_9
						SHR ax, 8
						STOSB
					cb_no_acdb_9:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_9

	align 16
		cb_do_xmm9_1:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			cb_zero_counter_10:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_10_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_10

		cb_zero_counter_10_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_5
				CMP cl, 16
				JB cb_no_zero_run_6
					MOV ax, 0F0h
					
					cb_zero_run_6:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_6
				cb_no_zero_run_6:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cb_no_acdb_10
					SHR ax, 8
					STOSB
				cb_no_acdb_10:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_10

		align 16
			cb_do_r11_5:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_11:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_11_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_11

			cb_zero_counter_11_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm10_1
					CMP cl, 16
					JB cb_no_zero_run_7
						MOV ax, 0F0h
						
						cb_zero_run_7:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_7
					cb_no_zero_run_7:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_11
						SHR ax, 8
						STOSB
					cb_no_acdb_11:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_11

	align 16
		cb_do_xmm10_1:
			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			cb_zero_counter_12:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_12_exit
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_12

		cb_zero_counter_12_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_6
				CMP cl, 16
				JB cb_no_zero_run_8
					MOV ax, 0F0h
					
					cb_zero_run_8:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_8
				cb_no_zero_run_8:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cb_no_acdb_12
					SHR ax, 8
					STOSB
				cb_no_acdb_12:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_12

		align 16
			cb_do_r11_6:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_13:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_13_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_13

			cb_zero_counter_13_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmm11_1
					CMP cl, 16
					JB cb_no_zero_run_9
						MOV ax, 0F0h
						
						cb_zero_run_9:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_9
					cb_no_zero_run_9:


					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_13
						SHR ax, 8
						STOSB
					cb_no_acdb_13:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_13

	align 16
		cb_do_xmm11_1:
			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			cb_zero_counter_14:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cb_zero_counter_14_exit					
					SHR r10, 16
					SHR r12, 16
			JMP cb_zero_counter_14

		cb_zero_counter_14_exit:
			DEC cl

			CMP ch, 4
			JA cb_do_r11_7
				CMP cl, 16
				JB cb_no_zero_run_10
					MOV ax, 0F0h
					
					cb_zero_run_10:
						INC QWORD PTR [rbx+rax*8+2560]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cb_zero_run_10
				cb_no_zero_run_10:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+2560]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cb_no_acdb_14
					SHR ax, 8
					STOSB
				cb_no_acdb_14:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cb_zero_counter_14

		align 16
			cb_do_r11_7:
				MOV ch, 0
				SHR r14d, 8

				cb_zero_counter_15:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cb_zero_counter_15_exit
						SHR r11, 16
						SHR r13, 16
				JMP cb_zero_counter_15

			cb_zero_counter_15_exit:
				DEC cl

				CMP ch, 4
				JA cb_do_xmmxx
					CMP cl, 16
					JB cb_no_zero_run_11
						MOV ax, 0F0h
						
						cb_zero_run_11:
							INC QWORD PTR [rbx+rax*8+2560]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cb_zero_run_11
					cb_no_zero_run_11:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+2560]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cb_no_acdb_15
						SHR ax, 8
						STOSB
					cb_no_acdb_15:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cb_zero_counter_15
		align 16
			cb_do_xmmxx:

				TEST cl, cl
				JZ cb_no_eob
					XOR eax, eax
					STOSB

					INC QWORD PTR [rbx+2560]

				cb_no_eob:


		ADD rsi, 128
	DEC r8
	JNZ cb_scale_loop


	INC QWORD PTR [rbx+4600]

MOV [rdx+48], rdi
	MOV r8, r9







	XOR r15, r15
	XOR rax, rax
	XOR rcx, rcx





	MOVDQA xmm0, [rbx+256]
	MOVDQA xmm1, [rbx+272]
	MOVDQA xmm2, [rbx+288]
	MOVDQA xmm3, [rbx+304]
	MOVDQA xmm4, [rbx+320]
	MOVDQA xmm5, [rbx+336]
	MOVDQA xmm6, [rbx+352]
	MOVDQA xmm7, [rbx+368]

	MOVDQA [rbx+256], xmm15
	MOVDQA [rbx+272], xmm15
	MOVDQA [rbx+288], xmm15
	MOVDQA [rbx+304], xmm15
	MOVDQA [rbx+320], xmm15
	MOVDQA [rbx+336], xmm15
	MOVDQA [rbx+352], xmm15
	MOVDQA [rbx+368], xmm15



align 16
	cr_scale_loop:
		MOVDQA xmm8, [rsi]
		MOVDQA xmm9, [rsi+16]
		MOVDQA xmm10, [rsi+32]
		MOVDQA xmm11, [rsi+48]

		PMULHW xmm8, xmm0
		MOVDQA xmm12, xmm8
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm8, xmm12

		PMULHW xmm9, xmm1
		MOVDQA xmm12, xmm9
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm9, xmm12

		PMULHW xmm10, xmm2
		MOVDQA xmm12, xmm10
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm10, xmm12

		PMULHW xmm11, xmm3
		MOVDQA xmm12, xmm11
		PAND xmm12, xmm14
		PSRLW xmm12, 15
		PADDW xmm11, xmm12

				
		PABSW xmm12, xmm8
		
		MOVD r10, xmm8
		PSRLDQ xmm8, 8
		MOVD r11, xmm8
		
		MOVDQA xmm8, xmm12
		PCMPEQW xmm8, xmm15

		MOVD r12, xmm12
		PSRLDQ xmm12, 8
		MOVD r13, xmm12
		
		PMOVMSKB r14d, xmm8
		
; dc predictor
		MOV ax, r10w
		
		SUB ax, r15w
		JZ cr_zero_dc_0
			MOV r15w, ax
			
			MOV cx, 8000h
			AND cx, ax
			ROL cx, 1

			SUB r15w, cx
			
			MOV cx, ax
			NEG cx
			CMOVS cx, ax
						
			BSR ax, cx
			INC ax

		cr_zero_dc_0:

		INC QWORD PTR [rbx+rax*8+128]

		STOSB
		
		TEST al, al
		JZ cr_no_dcdb_0
			XCHG ax, r15w
			STOSB			
			CMP r15w, 8
			JBE cr_no_dcdb_0
				SHR ax, 8
				STOSB
		cr_no_dcdb_0:


		MOV r15w, r10w

		SHR r10, 16
		SHR r12, 16
		SHR r14b, 2
		
; ac_cofs

		XOR ecx, ecx

		cr_zero_counter_0:
			ADD cx, 0101h
			SHR r14b, 2
			JNC cr_zero_counter_0_exit
				SHR r10, 16
				SHR r12, 16
		JMP cr_zero_counter_0

	cr_zero_counter_0_exit:
		DEC cl

		CMP ch, 3
		JA cr_do_r11_0
			
			SHL cl, 4

			BSR ax, r12w
			INC ax

			OR al, cl
			MOV cl, al
			AND cl, 0Fh

			INC QWORD PTR [rbx+rax*8+4608]

			STOSB
			
			MOV ax, r12w
			XOR ax, r10w
			SHL ax, 1
			MOV ax, r10w
			SBB ax, 0
			
			STOSB
						
			CMP cl, 8
			JBE cr_no_acdb_0
				SHR ax, 8
				STOSB
			cr_no_acdb_0:

			SHR r10, 16
			SHR r12, 16

			MOV cl, 0
		JMP cr_zero_counter_0

	align 16
		cr_do_r11_0:
			MOV ch, 0
			SHR r14d, 8

			cr_zero_counter_1:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_1_exit
					SHR r11, 16
					SHR r13, 16
			JMP cr_zero_counter_1

		cr_zero_counter_1_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_xmm9_0
				
				SHL cl, 4

				BSR ax, r13w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r13w
				XOR ax, r11w
				SHL ax, 1
				MOV ax, r11w
				SBB ax, 0
				
				STOSB			

				CMP cl, 8
				JBE cr_no_acdb_1
					SHR ax, 8
					STOSB
				cr_no_acdb_1:

				SHR r11, 16
				SHR r13, 16

				MOV cl, 0
			JMP cr_zero_counter_1


	align 16
		cr_do_xmm9_0:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			cr_zero_counter_2:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_2_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_2

		cr_zero_counter_2_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_1
				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE cr_no_acdb_2
					SHR ax, 8
					STOSB
				cr_no_acdb_2:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_2

		align 16
			cr_do_r11_1:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_3:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_3_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_3

			cr_zero_counter_3_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm10_0
					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_3
						SHR ax, 8
						STOSB
					cr_no_acdb_3:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_3

	align 16
		cr_do_xmm10_0:

			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			cr_zero_counter_4:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_4_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_4

		cr_zero_counter_4_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_2
				CMP cl, 16
				JB cr_no_zero_run_0
					MOV ax, 0F0h
					
					cr_zero_run_0:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_0
				cr_no_zero_run_0:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB

				CMP cl, 8
				JBE cr_no_acdb_4
					SHR ax, 8
					STOSB
				cr_no_acdb_4:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_4

		align 16
			cr_do_r11_2:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_5:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_5_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_5

			cr_zero_counter_5_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm11_0
					CMP cl, 16
					JB cr_no_zero_run_1
						MOV ax, 0F0h
						
						cr_zero_run_1:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_1
					cr_no_zero_run_1:


					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_5
						SHR ax, 8
						STOSB
					cr_no_acdb_5:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_5

	align 16
		cr_do_xmm11_0:

			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			cr_zero_counter_6:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_6_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_6

		cr_zero_counter_6_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_3
				CMP cl, 16
				JB cr_no_zero_run_2
					MOV ax, 0F0h
					
					cr_zero_run_2:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_2
				cr_no_zero_run_2:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cr_no_acdb_6
					SHR ax, 8
					STOSB
				cr_no_acdb_6:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_6

		align 16
			cr_do_r11_3:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_7:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_7_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_7

			cr_zero_counter_7_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm8_1
					CMP cl, 16
					JB cr_no_zero_run_3
						MOV ax, 0F0h
						
						cr_zero_run_3:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_3
					cr_no_zero_run_3:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_7
						SHR ax, 8
						STOSB
					cr_no_acdb_7:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_7
	align 16
		cr_do_xmm8_1:

			MOVDQA xmm8, [rsi+64]
			MOVDQA xmm9, [rsi+80]
			MOVDQA xmm10, [rsi+96]
			MOVDQA xmm11, [rsi+112]
		
			PMULHW xmm8, xmm4
			MOVDQA xmm12, xmm8
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm8, xmm12

			PMULHW xmm9, xmm5
			MOVDQA xmm12, xmm9
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm9, xmm12

			PMULHW xmm10, xmm6
			MOVDQA xmm12, xmm10
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm10, xmm12

			PMULHW xmm11, xmm7
			MOVDQA xmm12, xmm11
			PAND xmm12, xmm14
			PSRLW xmm12, 15
			PADDW xmm11, xmm12


			PABSW xmm12, xmm8
		
			MOVD r10, xmm8
			PSRLDQ xmm8, 8
			MOVD r11, xmm8
		
			MOVDQA xmm8, xmm12
			PCMPEQW xmm8, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm8
			
			MOV ch, 0

			cr_zero_counter_8:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_8_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_8

		cr_zero_counter_8_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_4
				CMP cl, 16
				JB cr_no_zero_run_4
					MOV ax, 0F0h
					
					cr_zero_run_4:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_4
				cr_no_zero_run_4:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cr_no_acdb_8
					SHR ax, 8
					STOSB
				cr_no_acdb_8:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_8

		align 16
			cr_do_r11_4:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_9:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_9_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_9

			cr_zero_counter_9_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm9_1
					CMP cl, 16
					JB cr_no_zero_run_5
						MOV ax, 0F0h
						
						cr_zero_run_5:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_5
					cr_no_zero_run_5:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_9
						SHR ax, 8
						STOSB
					cr_no_acdb_9:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_9

	align 16
		cr_do_xmm9_1:

			PABSW xmm12, xmm9
		
			MOVD r10, xmm9
			PSRLDQ xmm9, 8
			MOVD r11, xmm9
		
			MOVDQA xmm9, xmm12
			PCMPEQW xmm9, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm9
			
			MOV ch, 0

			cr_zero_counter_10:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_10_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_10

		cr_zero_counter_10_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_5
				CMP cl, 16
				JB cr_no_zero_run_6
					MOV ax, 0F0h
					
					cr_zero_run_6:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_6
				cr_no_zero_run_6:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cr_no_acdb_10
					SHR ax, 8
					STOSB
				cr_no_acdb_10:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_10

		align 16
			cr_do_r11_5:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_11:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_11_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_11

			cr_zero_counter_11_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm10_1
					CMP cl, 16
					JB cr_no_zero_run_7
						MOV ax, 0F0h
						
						cr_zero_run_7:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_7
					cr_no_zero_run_7:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_11
						SHR ax, 8
						STOSB
					cr_no_acdb_11:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_11

	align 16
		cr_do_xmm10_1:
			PABSW xmm12, xmm10
		
			MOVD r10, xmm10
			PSRLDQ xmm10, 8
			MOVD r11, xmm10
		
			MOVDQA xmm10, xmm12
			PCMPEQW xmm10, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm10
			
			MOV ch, 0

			cr_zero_counter_12:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_12_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_12

		cr_zero_counter_12_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_6
				CMP cl, 16
				JB cr_no_zero_run_8
					MOV ax, 0F0h
					
					cr_zero_run_8:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_8
				cr_no_zero_run_8:

				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cr_no_acdb_12
					SHR ax, 8
					STOSB
				cr_no_acdb_12:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_12

		align 16
			cr_do_r11_6:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_13:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_13_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_13

			cr_zero_counter_13_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmm11_1
					CMP cl, 16
					JB cr_no_zero_run_9
						MOV ax, 0F0h
						
						cr_zero_run_9:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_9
					cr_no_zero_run_9:


					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh
					
					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_13
						SHR ax, 8
						STOSB
					cr_no_acdb_13:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_13

	align 16
		cr_do_xmm11_1:
			PABSW xmm12, xmm11
		
			MOVD r10, xmm11
			PSRLDQ xmm11, 8
			MOVD r11, xmm11
		
			MOVDQA xmm11, xmm12
			PCMPEQW xmm11, xmm15

			MOVD r12, xmm12
			PSRLDQ xmm12, 8
			MOVD r13, xmm12
		
			PMOVMSKB r14d, xmm11
			
			MOV ch, 0

			cr_zero_counter_14:
				ADD cx, 0101h
				SHR r14b, 2
				JNC cr_zero_counter_14_exit
					SHR r10, 16
					SHR r12, 16
			JMP cr_zero_counter_14

		cr_zero_counter_14_exit:
			DEC cl

			CMP ch, 4
			JA cr_do_r11_7
				CMP cl, 16
				JB cr_no_zero_run_10
					MOV ax, 0F0h
					
					cr_zero_run_10:
						INC QWORD PTR [rbx+rax*8+4608]
						STOSB
						SUB cl, 16
					CMP cl, 16
					JAE cr_zero_run_10
				cr_no_zero_run_10:


				
				SHL cl, 4

				BSR ax, r12w
				INC ax

				OR al, cl
				MOV cl, al
				AND cl, 0Fh

				INC QWORD PTR [rbx+rax*8+4608]

				STOSB
			
				MOV ax, r12w
				XOR ax, r10w
				SHL ax, 1
				MOV ax, r10w
				SBB ax, 0
			
				STOSB
				
				CMP cl, 8
				JBE cr_no_acdb_14
					SHR ax, 8
					STOSB
				cr_no_acdb_14:

				SHR r10, 16
				SHR r12, 16

				MOV cl, 0
			JMP cr_zero_counter_14

		align 16
			cr_do_r11_7:
				MOV ch, 0
				SHR r14d, 8

				cr_zero_counter_15:
					ADD cx, 0101h
					SHR r14b, 2
					JNC cr_zero_counter_15_exit
						SHR r11, 16
						SHR r13, 16
				JMP cr_zero_counter_15

			cr_zero_counter_15_exit:
				DEC cl

				CMP ch, 4
				JA cr_do_xmmxx
					CMP cl, 16
					JB cr_no_zero_run_11
						MOV ax, 0F0h
						
						cr_zero_run_11:
							INC QWORD PTR [rbx+rax*8+4608]
							STOSB
							SUB cl, 16
						CMP cl, 16
						JAE cr_zero_run_11
					cr_no_zero_run_11:

					
					SHL cl, 4

					BSR ax, r13w
					INC ax

					OR al, cl
					MOV cl, al
					AND cl, 0Fh

					INC QWORD PTR [rbx+rax*8+4608]

					STOSB
			
					MOV ax, r13w
					XOR ax, r11w
					SHL ax, 1
					MOV ax, r11w
					SBB ax, 0
			
					STOSB

					CMP cl, 8
					JBE cr_no_acdb_15
						SHR ax, 8
						STOSB
					cr_no_acdb_15:

					SHR r11, 16
					SHR r13, 16

					MOV cl, 0
				JMP cr_zero_counter_15
		align 16
			cr_do_xmmxx:

				TEST cl, cl
				JZ cr_no_eob
					XOR eax, eax
					STOSB

					INC QWORD PTR [rbx+4608]

				cr_no_eob:


		ADD rsi, 128
	DEC r8
	JNZ cr_scale_loop

	MOV [rdx+56], rdi

	INC QWORD PTR [rbx+248]
	INC QWORD PTR [rbx+6648]

	MOVDQA xmm0, [rbx+128]
	MOVDQA xmm1, [rbx+144]
	MOVDQA xmm2, [rbx+160]
	MOVDQA xmm3, [rbx+176]
	MOVDQA xmm4, [rbx+192]
	MOVDQA xmm5, [rbx+208]
	MOVDQA xmm6, [rbx+224]
	MOVDQA xmm7, [rbx+240]

	MOVDQA [rbx+256], xmm0
	MOVDQA [rbx+272], xmm1
	MOVDQA [rbx+288], xmm2
	MOVDQA [rbx+304], xmm3
	MOVDQA [rbx+320], xmm4
	MOVDQA [rbx+336], xmm5
	MOVDQA [rbx+352], xmm6
	MOVDQA [rbx+368], xmm7



	POP rbx

	POP rdi
	POP rsi

	POP r12
	POP r13
	POP r14
	POP r15

	RET
JPEG_ApplyQTs_SSSE3	ENDP


ALIGN 16
JPEG_ApplyQTs_SSE3	PROC

	RET
JPEG_ApplyQTs_SSE3	ENDP


END

