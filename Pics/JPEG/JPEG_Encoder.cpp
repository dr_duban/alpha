/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\JPEG.h"
#include "Compression\Pack.h"

#include "System\SysUtils.h"

__declspec(align(16)) short Pics::JPEG::fdct_cofs[64][8][8];

unsigned short Pics::JPEG::mu_factors[128];
const unsigned char jpg_hdrs[] = {0xFF, 0xD8, 0xFF, 0xC3, 0xFF, 0xC2, 0xFF, 0xDA, 0xFF, 0xD9};

const unsigned char cmp_id[] = {'Y', 'c', 's'};


UI_64 Pics::JPEG::Save(unsigned int iid, void * f_out, unsigned int cmp_level) {
	unsigned char qt[6][64], length_table[6][256];
	unsigned short codes_table[6][256];

	Properties iprops;

	DWORD bcount(0);
	UI_64 result(0), xnums[16], * ave_vals(0);
	SFSco::Object f_domain;
	unsigned int eb_size(0);
	unsigned short mdiv(0), mdivo(0), q_fuc(0);


	if (iprops = pi_list[iid]) {			
		xnums[0] = iprops.width;
		xnums[1] = iprops.height;
		xnums[2] = iprops.c_width;
		xnums[3] = iprops.c_height;

		xnums[14] = iprops.c_width;
		xnums[15] = iprops.c_height;
		xnums[9] = 0;

		xnums[4] = 18*64*sizeof(UI_64);

		xnums[8] = xnums[2]*xnums[3];

//		xnums[5] = xnums[4] + (xnums[8]<<2);
//		xnums[6] = xnums[5] + (xnums[8]<<1);
//		xnums[7] = xnums[6] + (xnums[8]<<1);

		xnums[10] = xnums[0]*xnums[1];

		eb_size = (xnums[8] << 1) + xnums[8] + (xnums[4]>>2);
		
		if (f_domain.New(jpeg_fdomain_tid, (eb_size<<2)) != -1) {
			if (unsigned short * amps = reinterpret_cast<unsigned short *>(f_domain.Acquire())) {
				System::MemoryFill_SSE3(amps, 64*sizeof(UI_64), 0x7FFF7FFF7FFF7FFF, 0x7FFF7FFF7FFF7FFF);
		//		System::MemoryFill_SSE3(amps + 256, 17*64*sizeof(UI_64), 0, 0);
							
				if (unsigned int * pipo = reinterpret_cast<unsigned int *>(iprops.im_obj.Acquire())) {

					WriteFile(f_out, jpg_hdrs, 2, &bcount, 0); // start of the image

					if (cmp_level <= 100) { // dct		
						JPEG_FDCT_CT_SSSE3(amps, pipo, xnums, 0);
						
// build qts
						result = (xnums[2] >> 3);
						result *= (xnums[3] >> 3);

						ave_vals =  reinterpret_cast<UI_64 *>(amps+512);

//						for (unsigned int ii(0);ii<192;ii++) amps[ii+256] = ((amps[ii] + amps[ii+256])>>3); // mid point
						for (unsigned int ii(0);ii<192;ii++) ave_vals[ii] /= result; // median

						System::MemoryFill(qt, 64*4, 0x01010101);

										

						for (unsigned int ij(0);ij<3;ij++) {
							mdiv = 1;

							for (unsigned int kk(0);kk<64;kk++) {
								if (amps[kk+ij*64+256])	qt[4][kk] = SFSco::BitIndex16High(amps[kk+ij*64+256]) + 1;
								else qt[4][kk] = 0;

								if (ave_vals[kk+ij*64]) qt[5][kk] = SFSco::BitIndex64High(ave_vals[kk+ij*64]) + 1;
								else qt[5][kk] = 0;

								if (kk)
								if (mdiv < qt[5][kk]) {
									mdiv = kk;
								}
								
							}

							mdiv = qt[4][mdiv];


							if (cmp_level < 100) {
								if ((qt[4][0] - qt[5][0]) > 1) qt[ij][0] = 2;								

								q_fuc = qt[4][0];
								q_fuc = (q_fuc*(100-cmp_level))/151;
							
								qt[ij][0] += q_fuc;

								mdivo = (cmp_level / 11) + 1;							
							
								q_fuc = 2 + (100-cmp_level)/13;

								for (unsigned int kk(1);kk<64;kk++) {
									if ((kk % mdivo) == 0) q_fuc++;
								
									if (kk >= 6) {
										if (qt[5][kk]) qt[ij][kk] = ((mdiv*q_fuc) / qt[5][kk]);
										else qt[ij][kk] = (mdiv*q_fuc);
									} else {
										if (qt[5][kk]) qt[ij][kk] = (((mdiv*(q_fuc+1))>>1) / qt[5][kk]);
										else qt[ij][kk] = ((mdiv*(q_fuc+1))>>1);

									}
								}
							}


							for (unsigned int ii(0);ii<64;ii++) {
								if ((qt[ij][ii]<128) && (qt[ij][ii]>0)) amps[ii+ij*64] = mu_factors[qt[ij][ii]];
								else {
									qt[ij][ii] = 128;
									amps[ii+ij*64] = mu_factors[0];
								}
							}							
						}





						System::MemoryFill_SSE3(amps + 256, 5*64*sizeof(UI_64), 0, 0);
						SFSco::JPG_ApplyQTs(amps, xnums);

						qt[3][0] = 0xFF; qt[3][1] = 0xDB; // qt table
						qt[3][2] = 0; qt[3][3] = 197; qt[3][4] = 0;					
						WriteFile(f_out, qt[3], 5, &bcount, 0);
						WriteFile(f_out, qt[0], 64, &bcount, 0);
						qt[3][0] = 1;
						WriteFile(f_out, qt[3], 1, &bcount, 0);
						WriteFile(f_out, qt[1], 64, &bcount, 0);
						qt[3][0] = 2;
						WriteFile(f_out, qt[3], 1, &bcount, 0);
						WriteFile(f_out, qt[2], 64, &bcount, 0);


						qt[3][0] = 0xFF; qt[3][1] = 0xC0; // start of the baseline frame
						qt[3][2] = 0; qt[3][3] = 17;
						qt[3][4] = 8; // sample precision
						qt[3][5] = ((xnums[1] >> 8) & 0x00FF); qt[3][6] = (xnums[1] & 0x00FF); // height
						qt[3][7] = ((xnums[0] >> 8) & 0x00FF); qt[3][8] = (xnums[0] & 0x00FF); // width
						qt[3][9] = 3; // component count
						
						qt[3][10] = 'Y'; // component name
						qt[3][11] = 0x11; // samplng factors
						qt[3][12] = 0; // qt selector

						qt[3][13] = 'c'; // component name
						qt[3][14] = 0x11; // samplng factors
						qt[3][15] = 1; // qt selector

						qt[3][16] = 's'; // component name
						qt[3][17] = 0x11; // samplng factors
						qt[3][18] = 2; // qt selector
					
						WriteFile(f_out, qt[3], 19, &bcount, 0); // frame header



					
					
	// huffman tables
					
						System::MemoryFill(codes_table, 6*256*sizeof(unsigned short), 0);

						mdiv = Pack::GenHuffmanTable(codes_table[0], length_table[0], reinterpret_cast<UI_64 *>(amps+2304), 0x01100100); // ac cr					
						--reinterpret_cast<UI_64 *>(amps+3328)[mdiv];

						if (length_table[0][255] != mdiv) {
							mdivo = 255; q_fuc = (0xFFFF >> (16 - mdiv));
							for (short mm(254);mm>=0;mm--) {							
								if (codes_table[0][mm] == q_fuc) {
									mdivo = mm;
									break;
								}
							}

							for (short mm(mdivo+1);mm<256;mm++) {
								if (length_table[0][mm] == length_table[0][255]) {
									codes_table[0][mdivo] = codes_table[0][mm];
									length_table[0][mdivo] = length_table[0][mm];
									mdivo = mm;
								}
							}
						}

						mdiv = Pack::GenHuffmanTable(codes_table[1], length_table[1], reinterpret_cast<UI_64 *>(amps+1280), 0x01100100); // ac cb
						--reinterpret_cast<UI_64 *>(amps+2304)[mdiv];

						if (length_table[1][255] != mdiv) {
							mdivo = 255; q_fuc = (0xFFFF >> (16 - mdiv));
							for (short mm(254);mm>=0;mm--) {							
								if (codes_table[1][mm] == q_fuc) {
									mdivo = mm;
									break;
								}
							}

							for (short mm(mdivo+1);mm<256;mm++) {
								if (length_table[1][mm] == length_table[1][255]) {
									codes_table[1][mdivo] = codes_table[1][mm];
									length_table[1][mdivo] = length_table[1][mm];
									mdivo = mm;
								}
							}
						}

						mdiv = Pack::GenHuffmanTable(codes_table[2], length_table[2], reinterpret_cast<UI_64 *>(amps+256), 0x01100100); // ac y
						--reinterpret_cast<UI_64 *>(amps+1280)[mdiv];

						if (length_table[2][255] != mdiv) {
							mdivo = 255; q_fuc = (0xFFFF >> (16 - mdiv));
							for (short mm(254);mm>=0;mm--) {							
								if (codes_table[2][mm] == q_fuc) {
									mdivo = mm;
									break;
								}
							}

							for (short mm(mdivo+1);mm<256;mm++) {
								if (length_table[2][mm] == length_table[2][255]) {
									codes_table[2][mdivo] = codes_table[2][mm];
									length_table[2][mdivo] = length_table[2][mm];
									mdivo = mm;
								}
							}
						}


						mdiv = Pack::GenHuffmanTable(codes_table[3], length_table[3], reinterpret_cast<UI_64 *>(amps+128), 0x01100010); // dc cbcr
						--reinterpret_cast<UI_64 *>(amps+192)[mdiv];

						if (length_table[3][15] != mdiv) {
							mdivo = 255; q_fuc = (0xFFFF >> (16 - mdiv));
							for (short mm(14);mm>=0;mm--) {							
								if (codes_table[3][mm] == q_fuc) {
									mdivo = mm;
									break;
								}
							}

							for (short mm(mdivo+1);mm<16;mm++) {
								if (length_table[3][mm] == length_table[3][15]) {
									codes_table[3][mdivo] = codes_table[3][mm];
									length_table[3][mdivo] = length_table[3][mm];
									mdivo = mm;
								}
							}
						}


						mdiv = Pack::GenHuffmanTable(codes_table[4], length_table[4], reinterpret_cast<UI_64 *>(amps), 0x01100010); // dc y
						--reinterpret_cast<UI_64 *>(amps+64)[mdiv];

						if (length_table[4][15] != mdiv) {
							mdivo = 255; q_fuc = (0xFFFF >> (16 - mdiv));
							for (short mm(14);mm>=0;mm--) {							
								if (codes_table[4][mm] == q_fuc) {
									mdivo = mm;
									break;
								}
							}

							for (short mm(mdivo+1);mm<16;mm++) {
								if (length_table[4][mm] == length_table[4][15]) {
									codes_table[4][mdivo] = codes_table[4][mm];
									length_table[4][mdivo] = length_table[4][mm];
									mdivo = mm;
								}
							}
						}

						qt[3][0] = 0xFF; qt[3][1] = 0xFE; // comment
						qt[3][2] = 0; qt[3][3] = 15;
						
						qt[3][4] = 's'; qt[3][5] = 'a'; qt[3][6] = 'f'; qt[3][7] = 'e'; qt[3][8] = '-';
						qt[3][9] = 'f'; qt[3][10] = 'a'; qt[3][11] = 'i'; qt[3][12] = 'l'; qt[3][13] = '.';
						qt[3][14] = 'n'; qt[3][15] = 'e'; qt[3][16] = 't';
						
						WriteFile(f_out, qt[3], qt[3][3] + 2, & bcount, 0);

		// out dc tables
						qt[3][0] = 0xFF; qt[3][1] = 0xC4; // huffman table
						qt[3][2] = 0; qt[3][3] = 0;
					
						qt[3][4] = 0; // Tc:Th					
						for (unsigned int uu(1);uu<16;uu++) qt[3][4+uu] = reinterpret_cast<UI_64 *>(amps+64)[uu];
						qt[3][20] = 0;
						for (unsigned int uu(1);uu<16;uu++) {
							if (reinterpret_cast<UI_64 *>(amps+64)[uu])
							for (unsigned int vv(0);vv<12;vv++) {
								if (length_table[4][vv] == uu) qt[3][21+qt[3][3]++] = vv;
							}
						}

						qt[3][21+qt[3][3]++] = 1; // Tc:Th					
						for (unsigned int uu(1);uu<16;uu++) qt[3][20+qt[3][3]+uu] = reinterpret_cast<UI_64 *>(amps+192)[uu];
						qt[3][36+qt[3][3]] = 0;
						for (unsigned int uu(1);uu<16;uu++) {
							if (reinterpret_cast<UI_64 *>(amps+192)[uu])
							for (unsigned int vv(0);vv<12;vv++) {
								if (length_table[3][vv] == uu) qt[3][37+qt[3][3]++] = vv;
							}
						}

						qt[3][3] += 35;

						WriteFile(f_out, qt[3], qt[3][3]+2, &bcount, 0); // dc tables out


		// y component
						length_table[5][0] = 0xFF; length_table[5][1] = 0xC4;
					
						mdiv = 0;
						length_table[5][4] = 0x10;
						for (unsigned int uu(1);uu<=16;uu++) length_table[5][4+uu] = reinterpret_cast<UI_64 *>(amps+1280)[uu];
						for (unsigned int uu(1);uu<=16;uu++) {
							if (reinterpret_cast<UI_64 *>(amps+1280)[uu])
							for (unsigned int vv(0);vv<255;vv++) {
								if (length_table[2][vv] == uu) length_table[5][21+mdiv++] = vv;
							}
						}

						mdiv += 19;

						length_table[5][2] = (mdiv >> 8);
						length_table[5][3] = (mdiv & 0xFF);

						WriteFile(f_out, length_table[5], mdiv+2, &bcount, 0); // ac table out

						qt[3][0] = 0xFF; qt[3][1] = 0xDA; // start of the scan
						qt[3][2] = 0; qt[3][3] = 8;
						qt[3][4] = 1;
						qt[3][5] = 'Y';
						qt[3][6] = 0; // tables
						qt[3][7] = 0; qt[3][8] = 63;
						qt[3][9] = 0;

						WriteFile(f_out, qt[3], 10, &bcount, 0); // y scan header

					

			// code the stream
						xnums[12] = reinterpret_cast<UI_64>(codes_table[4]);
						xnums[13] = reinterpret_cast<UI_64>(length_table[4]);
						xnums[14] = reinterpret_cast<UI_64>(codes_table[2]);
						xnums[15] = reinterpret_cast<UI_64>(length_table[2]);

						JPEG_ScanPack(amps + 9*64*sizeof(UI_64), xnums, 0);

						WriteFile(f_out, amps + 9*64*sizeof(UI_64), xnums[9], &bcount, 0);


		// cb component
						length_table[5][0] = 0xFF; length_table[5][1] = 0xC4;
					
						mdiv = 0;
						length_table[5][4] = 0x11;
						for (unsigned int uu(1);uu<=16;uu++) length_table[5][4+uu] = reinterpret_cast<UI_64 *>(amps+2304)[uu];
						for (unsigned int uu(1);uu<=16;uu++) {
							if (reinterpret_cast<UI_64 *>(amps+2304)[uu])
							for (unsigned int vv(0);vv<255;vv++) {
								if (length_table[1][vv] == uu) length_table[5][21+mdiv++] = vv;
							}
						}

						mdiv += 19;
							
						length_table[5][2] = (mdiv >> 8);
						length_table[5][3] = (mdiv & 0xFF);

						WriteFile(f_out, length_table[5], mdiv+2, &bcount, 0); // ac table out

						qt[3][0] = 0xFF; qt[3][1] = 0xDA; // start of the scan
						qt[3][2] = 0; qt[3][3] = 8;
						qt[3][4] = 1;
						qt[3][5] = 'c';
						qt[3][6] = 0x11; // tables
						qt[3][7] = 0; qt[3][8] = 63;
						qt[3][9] = 0;

						WriteFile(f_out, qt[3], 10, &bcount, 0); // cb scan header

					

			// code the stream
						xnums[12] = reinterpret_cast<UI_64>(codes_table[3]);
						xnums[13] = reinterpret_cast<UI_64>(length_table[3]);
						xnums[14] = reinterpret_cast<UI_64>(codes_table[1]);
						xnums[15] = reinterpret_cast<UI_64>(length_table[1]);

						JPEG_ScanPack(amps + 9*64*sizeof(UI_64), xnums, 1);

						WriteFile(f_out, amps + 9*64*sizeof(UI_64), xnums[9], &bcount, 0);



		// cr component

						length_table[5][0] = 0xFF; length_table[5][1] = 0xC4;
						
						mdiv = 0;
						length_table[5][4] = 0x11;
						for (unsigned int uu(1);uu<=16;uu++) length_table[5][4+uu] = reinterpret_cast<UI_64 *>(amps+3328)[uu];
						for (unsigned int uu(1);uu<=16;uu++) {
							if (reinterpret_cast<UI_64 *>(amps+3328)[uu])
							for (unsigned int vv(0);vv<255;vv++) {
								if (length_table[0][vv] == uu) length_table[5][21+mdiv++] = vv;
							}
						}

						mdiv += 19;

						length_table[5][2] = (mdiv >> 8);
						length_table[5][3] = (mdiv & 0xFF);

						WriteFile(f_out, length_table[5], mdiv+2, &bcount, 0); // ac table out
				
						qt[3][0] = 0xFF; qt[3][1] = 0xDA; // start of the scan
						qt[3][2] = 0; qt[3][3] = 8;
						qt[3][4] = 1;
						qt[3][5] = 's';
						qt[3][6] = 0x11; // tables
						qt[3][7] = 0; qt[3][8] = 63;
						qt[3][9] = 0;

						WriteFile(f_out, qt[3], 10, &bcount, 0); // cr scan header

					

			// code the stream
						xnums[12] = reinterpret_cast<UI_64>(codes_table[3]);
						xnums[13] = reinterpret_cast<UI_64>(length_table[3]);
						xnums[14] = reinterpret_cast<UI_64>(codes_table[0]);
						xnums[15] = reinterpret_cast<UI_64>(length_table[0]);

						JPEG_ScanPack(amps + 9*64*sizeof(UI_64), xnums, 2);

						WriteFile(f_out, amps + 9*64*sizeof(UI_64), xnums[9], &bcount, 0);


					} else { // lossless
						qt[3][0] = 0xFF; qt[3][1] = 0xC3; // start of the baseline frame
						qt[3][2] = 0; qt[3][3] = 17;
						qt[3][4] = 8; // sample precision
						qt[3][5] = ((xnums[1] >> 8) & 0x00FF); qt[3][6] = (xnums[1] & 0x00FF); // height
						qt[3][7] = ((xnums[0] >> 8) & 0x00FF); qt[3][8] = (xnums[0] & 0x00FF); // width
						qt[3][9] = 3; // component count

						qt[3][10] = 'Y'; // component name
						qt[3][11] = 0x11; // samplng factors
						qt[3][12] = 0; // qt selector

						qt[3][13] = 'c'; // component name
						qt[3][14] = 0x11; // samplng factors
						qt[3][15] = 0; // qt selector

						qt[3][16] = 's'; // component name
						qt[3][17] = 0x11; // samplng factors
						qt[3][18] = 0; // qt selector
					
						WriteFile(f_out, qt[3], 19, &bcount, 0); // frame header

						SFSco::JPG_LLStats(amps, pipo, xnums);


						System::MemoryFill(codes_table, 6*256*sizeof(unsigned short), 0);
							
						mdiv = Pack::GenHuffmanTable(codes_table[0], length_table[0], reinterpret_cast<UI_64 *>(amps+768), 0x01100010); // ac cr
						--reinterpret_cast<UI_64 *>(amps+832)[mdiv];

						mdiv = Pack::GenHuffmanTable(codes_table[1], length_table[1], reinterpret_cast<UI_64 *>(amps+512), 0x01100010); // ac cb
						--reinterpret_cast<UI_64 *>(amps+576)[mdiv];

						mdiv = Pack::GenHuffmanTable(codes_table[2], length_table[2], reinterpret_cast<UI_64 *>(amps+256), 0x01100010); // ac y
						--reinterpret_cast<UI_64 *>(amps+320)[mdiv];


					
						for (unsigned int mm(0);mm<3;mm++) {
							qt[3][0] = 0xFF; qt[3][1] = 0xC4; // huffman table
							qt[3][2] = 0; qt[3][3] = 0;
					
							qt[3][4] = 0; // Tc:Th					
							for (unsigned int uu(1);uu<16;uu++) qt[3][4+uu] = reinterpret_cast<UI_64 *>(amps+320+(mm<<8))[uu];
							qt[3][20] = 0;
							for (unsigned int uu(1);uu<16;uu++) {
								if (reinterpret_cast<UI_64 *>(amps+320+(mm<<8))[uu])
								for (unsigned int vv(0);vv<12;vv++) {
									if (length_table[2-mm][vv] == uu) qt[3][21+qt[3][3]++] = vv;
								}
							}

							qt[3][3] += 19;

							WriteFile(f_out, qt[3], qt[3][3]+2, &bcount, 0); // dc table out
					
					
							qt[3][0] = 0xFF; qt[3][1] = 0xDA; // start of the scan
							qt[3][2] = 0; qt[3][3] = 8;
							qt[3][4] = 1;
							qt[3][5] = cmp_id[mm]; //'Y';
							qt[3][6] = 0; // tables
							qt[3][7] = 2; // predictor
							qt[3][8] = 0;
							qt[3][9] = 0; // point transform

							WriteFile(f_out, qt[3], 10, &bcount, 0);

							xnums[12] = reinterpret_cast<UI_64>(codes_table[2-mm]);
							xnums[13] = reinterpret_cast<UI_64>(length_table[2-mm]);

							JPEG_LLScanPack(amps, xnums, mm);

							WriteFile(f_out, amps + (xnums[4]>>1), xnums[9], &bcount, 0);

						}
					}


					WriteFile(f_out, jpg_hdrs+8, 2, &bcount, 0); // end of the image

					iprops.im_obj.Release();
				}

				f_domain.Release();
			}
			
			f_domain.Destroy();
		}
	}

	return result;
}




