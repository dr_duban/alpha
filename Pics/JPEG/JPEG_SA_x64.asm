
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




EXTERN	?idct_cofs@JPEG@Pics@@0PAY17EA@FA:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
JPEG_HuffmanSA_MSB	PROC
	MOV [rsp+32], r8

	PUSH rbp
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

		MOV rsi, r8		
		MOV ebp, ecx ; file pointer
		MOV r8, rdx ; ScanHeader pointer
				
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx


		XOR r10, r10 ; bit accum
		XOR r11, r11 ; bit runner 1
		XOR r12, r12 ; block runner
		XOR r14, r14

; r10, r11 - temp

; r9 - free
; r13 - huffman codes pointer
; r15 - huffman vals pointer
; rdi - component pointer


		CLD


align 16
	mcu_loop:

		MOV dl, [r8]
		LEA rdi, [r8+64]

	align 16
		component_loop:
			MOV dh, [rdi+42]
			AND dh, 7

			MOV r14, [rdi+32]

			MOV r15, [rdi] ; dc table
			MOV r13, [rdi+16] ; set dc codes

			SUB r15, 8
			SUB r13, 4

			CMP BYTE PTR [r8+1], 0
			JZ v_factor_loop
				MOV r15, [rdi+8] ; ac table
				MOV r13, [rdi+24] ; set ac codes

				SUB r15, 8
				SUB r13, 4


		align 16
			v_factor_loop:			
				MOV bh, [rdi+41]
				AND bh, 7

			align 16
				h_factor_loop:		
					XOR rcx, rcx

				align 16
					stream_scan_loop:

						AND bl, 7
						JNZ noff

						LODSB
						
						DEC ebp
						JZ exit
	
						CMP al, 0FFh
						JNZ	noff
							SHL eax, 8

							LODSB

							DEC ebp
							JZ exit

							TEST al, al
							JZ stuffby
								XOR al, 0D7h
								CMP al, 8
								JAE exit
									LEA rax, [r8+110]
									MOV r11b, [r8]
									
									reset_loop:
										MOV WORD PTR [rax], 0
										ADD rax, 64
									DEC r11b
									JNZ reset_loop

									XOR rax, rax
									XOR r10, r10

								JMP stream_scan_loop
							stuffby:
							SHR eax, 8
					align 16
						noff:
												

						TEST ch, ch
						JZ nextbit_loop
							ADD cl, 8
							JMP extraction_point

								
					align 16
						nextbit_loop:
							CMP bl, 8
							JAE stream_scan_loop
								INC bl
								INC r11b
								CMP r11b, 16
								JA exit
																
								SHL al, 1
								RCL r10w, 1
																
							CMP r10w, [r13+r11*4]
						JAE nextbit_loop

						SUB r10w, [r13+r11*4+2]

						MOV r11, [r15+r11*8]						
						MOV cl, [r10+r11] ; decoded val
						MOV ch, cl
						SHR cl, 4 ; run length						
																
						XOR r11, r11					

						AND ch, 0Fh
						JZ eob_on
							ADD r12b, cl
							JMP extraction_on
						
						align 16
							eob_on:
								MOV r10b, [r8+1]
								OR r10b, r12b
								JZ move_it
									CMP cl, 0Fh
									JB skip_it
										XOR r10, r10
										INC cl
										ADD r12b, cl
										JMP stream_scan_loop

									skip_it:
										TEST cl, cl
										JZ idct_seq
											MOV ch, cl
											OR r12w, 0400h


					align 16
						extraction_on:
							MOV cl, bl
							MOV bl, 0
							AND cl, 7
							JZ stream_scan_loop
								SHR eax, cl

								XOR cl, 7
								INC cl

						align 16
							extraction_point:
								CMP cl, ch
								JAE read_it
									SHL eax, 8
									JMP stream_scan_loop
								read_it:
									SUB cl, ch
									XOR r11, r11
									ROR eax, cl
									XCHG r11w, ax									
									ROL eax, 8
					
									XOR cl, 7
									INC cl					
									MOV bl, cl
			
									DEC ch
									MOVZX ecx, ch

									MOV r10, 1									
									SHL r10w, cl
				
									CMP r12w, 0FFh
									JB no_eob
										AND r12w, 0FFh										

										SHL r10w, 1
										OR r11w, r10w
										DEC r11w

										JMP idct_seq
									no_eob:

									CMP r11w, r10w
									JAE move_it
										SHL r10w, 1
										DEC r10w
										SUB r11w, r10w
					align 16
						move_it:
							MOV cl, [r8+4]
							SHL r11w, cl

							MOVZX r10, BYTE PTR [r8+1]
							ADD r10b, r12b
							JNZ do_ital
								ADD r11w, [rdi+46]
								MOV [rdi+46], r11w

							do_ital: 


							MOV [r14+r10*2], r11w

							XOR r10, r10	
							XOR r11, r11
					

							INC r12b
							CMP r12b, [r8+2]
							JB stream_scan_loop


					align 16
						idct_seq:
							MOVZX r9, r11w

							XOR r10, r10
							XOR r11, r11
							XOR r12, r12

							ADD r14, 128
														
							
				DEC bh
				JNZ h_factor_loop
				
					CMP dh, 1
					JBE noextra_off					
						MOVZX r10, BYTE PTR [rdi+41]
						AND r10b, 7
						SHL r10, 7
						SUB r14, r10

						ADD r14, [rdi+48]
						XOR r10, r10
					noextra_off:

						
			DEC dh
			JNZ v_factor_loop
				
				SHL r9, 7
											
				MOV cl, [rdi+42]				
				SHR cl, 1
				JZ noextra_off_2
					MOV r14, [rdi+32]
					
					MOVZX r10, BYTE PTR [rdi+41]
					AND r10b, 7
					SHL r10, 7
					ADD r14, r10


					ADD r14, r9				
					XOR r9, r9

					
					MOV r10, r14
					SUB r10, [rdi+56]

				r10_adjust:
					SUB r10, [rdi+48]
					JS noextra_off_2
						MOV r14, [rdi+48]

						CMP cl, 15
						JA adjust_special
							SHL r14, cl
							JMP line_r14

						adjust_special:
							MOVZX r11, cl
							SHR r11b, 4
							
							SHL r11, 7
							ADD r14, r11

					line_r14:
						ADD r14, [rdi+56]						

						MOV [rdi+56], r14

						CMP r10, [rdi+48]
						JAE r10_adjust

						ADD r14, r10

			align 16
				noextra_off_2:


					XOR r10, r10
					XOR r11, r11
										
					ADD r14, r9
					MOV [rdi+32], r14

			ADD rdi, 64
		DEC dl
		JNZ component_loop

	JMP mcu_loop

	exit:
		MOV rax, rsi

		CMP r11b, 16
		CMOVA rax, r11

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx
	POP rbp
	
	SUB rax, [rsp+32]

	RET
JPEG_HuffmanSA_MSB	ENDP

ALIGN 16
JPEG_HuffmanSA_LSB	PROC
	MOV [rsp+32], r8

	PUSH rbp
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

		MOV rsi, r8		
		MOV ebp, ecx ; file pointer
		MOV r8, rdx ; ScanHeader pointer
				
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx


		XOR r10, r10 ; bit accum
		XOR r11, r11 ; bit runner 1
		XOR r12, r12 ; block runner
		XOR r14, r14

; r10, r11 - temp

; r9 - free
; r13 - huffman codes pointer
; r15 - huffman vals pointer
; rdi - component pointer


		CLD

		MOV cl, [r8+4]									
		MOV dx, 1
		SHL dx, cl


		LEA rdi, [r8+64]

		MOV r14, [rdi+32]

		MOV r15, [rdi+8] ; ac table
		MOV r13, [rdi+24] ; set ac codes

		SUB r15, 8
		SUB r13, 4



align 16
	mcu_loop:
		XOR r10, r10
		XOR r11, r11
		XOR r12, r12

		align 16
			stream_scan_loop:

				AND bl, 7
				JNZ noff

				LODSB
						
				DEC ebp
				JZ exit
	
				CMP al, 0FFh
				JNZ	noff
					SHL eax, 8

					LODSB

					DEC ebp
					JZ exit

					TEST al, al
					JZ stuffby
						XOR al, 0D7h
						CMP al, 8
						JB stream_scan_loop
						JMP exit
					stuffby:
						SHR eax, 8
			align 16
				noff:

					TEST ch, ch
					JZ nextbit_loop
						ADD cl, 8
						JMP eob_xtraction

								
				align 16
					nextbit_loop:
						CMP bl, 8
						JAE stream_scan_loop
							INC bl
							INC r11b
							CMP r11b, 16
							JA exit

																
							SHL al, 1
							RCL r10w, 1
																
						CMP r10w, [r13+r11*4]
					JAE nextbit_loop

					SUB r10w, [r13+r11*4+2]

					MOV r11, [r15+r11*8]						
					MOV cl, [r10+r11] ; decoded val
					MOV ch, cl
					SHR cl, 4 ; run length						
						

					XOR r11, r11					

					AND ch, 0Fh
					JZ eob_on
						MOVZX r9, BYTE PTR [r8+1]
						ADD r9b, r12b
						LEA r9, [r14+r9*2]
								
						TEST cl, cl
						JZ zrzr_loop
								
							SHL ecx, 8
							MOV cl, ch
																
							zero_loop:
								CMP WORD PTR [r9], 0
								JZ skip_inc
									ADD ecx, 010101h
								skip_inc:
									ADD r9, 2
							DEC cl
							JNZ zero_loop

							SHR ecx, 8

						zrzr_loop:
							CMP WORD PTR [r9], 0
							JZ no_zero
								ADD cx, 0101h
								ADD r9, 2
						JMP zrzr_loop

					align 16
						no_zero:
							
							ADD r12b, cl
							JMP extraction_on
						
						align 16
							eob_on:
								CMP cl, 0Fh
								JB skip_it										
									XOR r10, r10
										
									MOVZX r9, BYTE PTR [r8+1]
									ADD r9b, r12b
									LEA r9, [r14+r9*2]

									
									INC cl
									SHL ecx, 8
									MOV cl, ch

									zeob_loop:
										CMP WORD PTR [r9], 0
										JZ skip_zeob
											ADD ecx, 010101h
										skip_zeob:
											ADD r9, 2
									DEC cl
									JNZ zeob_loop
											
									SHR ecx, 8
									ADD r12b, cl
											
									TEST ch, ch
									JZ stream_scan_loop											
										OR r12w, 0100h
										JMP extraction_on

								skip_it:
									TEST cl, cl
									JZ idct_seq
										MOV ch, cl
											
										MOV cl, bl
										MOV bl, 0
										AND cl, 7
										JZ stream_scan_loop
											SHR eax, cl

											XOR cl, 7
											INC cl

										align 16
											eob_xtraction:
												CMP cl, ch
												JAE read_eob
													SHL eax, 8
													JMP stream_scan_loop
												read_eob:
													SUB cl, ch												
													ROR eax, cl

													XOR cl, 7
													INC cl
													MOV bl, cl


													SHR ecx, 8

													XCHG r11w, ax
													ROL eax, 8

													MOV r10w, 1
													SHL r10w, cl

													OR r11w, r10w
					
													DEC r11w														

												JMP idct_seq












					align 16
						extraction_on:
							AND bl, 7
							JNZ noff_1

							LODSB
						
							DEC ebp
							JZ exit
	
							CMP al, 0FFh
							JNZ	noff_1
								SHL eax, 8

								LODSB

								DEC ebp
								JZ exit

								TEST al, al
								JNZ exit
									SHR eax, 8
							align 16
								noff_1:
									CMP r12w, 0100h
									JAE no_chad
										DEC ch
										INC bl
										SHL al, 1

										RCL r11w, 2
										DEC r11w
										OR r12w, 0200h
								no_chad:

									TEST ch, ch
									JZ co_move
										
									align 16	
										zr_loop:
											CMP bl, 8
											JAE extraction_on
												SUB r9, 2
												CMP WORD PTR [r9], 0
												JZ zr_loop
													
													INC bl
													
													SHL al, 1
													JNC	ski_adjust
														OR [r9], dx
													ski_adjust:																					

										DEC ch
										JNZ zr_loop
																	

								align 16
									co_move:
										TEST r12w, 0100h
										JZ move_it
											XOR r10, r10
											AND r12w, 0FFh
											JMP stream_scan_loop

									
					align 16
						move_it:
							AND r12w, 0FFh

							MOV cl, [r8+4]
							SHL r11w, cl

							MOVZX r10, BYTE PTR [r8+1]
							ADD r10b, r12b

							MOV [r14+r10*2], r11w

							XOR r10, r10	
							XOR r11, r11
					
							
							INC r12b
							CMP r12b, [r8+2]
							JB stream_scan_loop


					align 16
						idct_seq:
																			
							INC r11w
							CMP r12b, [r8+2]
							JAE non_search
															
								MOVZX r9, BYTE PTR [r8+1]
								ADD r9b, [r8+2]
								
								SUB r12b, [r8+2]

																
								LEA r9, [r14+2*r9-2]

							align 16
								next_ebit:
									CMP WORD PTR [r9], 0
									JZ no_f_correction
										CMP bl, 8
										JAE next_stream_byte
											INC bl
																																
											SHL al, 1
											JNC no_f_correction
												OR [r9], dx
											no_f_correction:
												SUB r9, 2
											
								INC r12b
								JNZ next_ebit
								JMP non_search

							align 16
								next_stream_byte:
									AND bl, 7
									JNZ next_ebit
									
									LODSB
						
									DEC ebp
									JZ exit
	
									CMP al, 0FFh
									JNZ	next_ebit
										SHL eax, 8

										LODSB

										DEC ebp
										JZ exit

										TEST al, al
										JNZ exit
											SHR eax, 8

								JMP next_ebit

						align 16																	
							non_search:							
								ADD r14, 128


								MOV r10, r14
								SUB r10, [rdi+56]
								SUB r10, [rdi+48]
								JS no_extra_off_1
									MOVZX r9, BYTE PTR [rdi+41]
									SHR r9b, 5
									JZ no_extra_off_1
										SHL r9, 7
										ADD r14, r9

										MOV [rdi+56], r14
										ADD r14, r10

								no_extra_off_1:
								


								DEC r11w
								JZ mcu_loop
									
											
								align 16
									eob_scan_loop:
										
										MOVZX r9, BYTE PTR [r8+1]
										ADD r9b, [r8+2]

										MOV cl, [r8+2]

										LEA r9, [r14+r9*2-2]
									
										cl_scan_loop:
											CMP WORD PTR [r9], 0
											JZ no_ff_correction
												CMP bl, 8
												JAE next_sbyte
													INC bl
																																
													SHL al, 1
													JNC no_ff_correction
														OR [r9], dx
													no_ff_correction:
														SUB r9, 2
											

										DEC cl
										JNZ cl_scan_loop
																				
										ADD r14, 128

										MOV r10, r14
										SUB r10, [rdi+56]
										SUB r10, [rdi+48]
										JS no_extra_off_2
											MOVZX r9, BYTE PTR [rdi+41]
											SHR r9b, 5
											JZ no_extra_off_2
												SHL r9, 7
												ADD r14, r9

												MOV [rdi+56], r14
												ADD r14, r10
										no_extra_off_2:

									DEC r11w
									JNZ eob_scan_loop
									JMP mcu_loop			
																		
								align 16
									next_sbyte:
										MOV bl, 0
																													
										LODSB
						
										DEC ebp
										JZ exit
	
										CMP al, 0FFh
										JNZ	cl_scan_loop
											SHL eax, 8

											LODSB

											DEC ebp
											JZ exit

											TEST al, al
											JNZ exit											
												SHR eax, 8

										
										JMP cl_scan_loop
										
	
	
																		


	exit:
		MOV rax, rsi

		CMP r11b, 16
		CMOVA rax, r11

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx
	POP rbp
	
	SUB rax, [rsp+32]



	RET
JPEG_HuffmanSA_LSB	ENDP


ALIGN 16
JPEG_ReadSA_LSB	PROC	
	PUSH rbp
	PUSH rbx
	PUSH rsi
	PUSH rdi


		MOV r11, r8

		MOV rsi, r8		
		MOV ebp, ecx ; file pointer
		MOV r8, rdx ; ScanHeader pointer
				
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx
		XOR rdx, rdx

		XOR r10, r10 ; bit accum

		MOV r10w, 1
		MOV cl, [r8+4]
		SHL r10w, cl

		CLD

		LODSB
						
		DEC ebp
		JZ exit
	
			CMP al, 0FFh
			JNZ	mcu_loop
				SHL eax, 8

				LODSB

				DEC ebp
				JZ exit

				TEST al, al
				JNZ exit
					SHR eax, 8
align 16
	mcu_loop:

		MOV dl, [r8]
		LEA rdi, [r8+64]

	align 16
		component_loop:
			MOV dh, [rdi+42]
			AND dh, 7
			MOV r9, [rdi+32]


		align 16
			v_factor_loop:			
				MOV bh, [rdi+41]
				AND bh, 7

			align 16
				h_factor_loop:
					CMP bl, 8
					JAE stream_scan_loop
						INC bl
						SHL al, 1

						JNC next_block
							OR [r9], r10w

						next_block:												

							ADD r9, 128
														
				DEC bh
				JNZ h_factor_loop
				
				CMP dh, 1
				JBE noextra_off					
					MOVZX rcx, BYTE PTR [rdi+41]
					AND cl, 7
					SHL rcx, 7
					SUB r9, rcx

					ADD r9, [rdi+48]					
				noextra_off:


			DEC dh
			JNZ v_factor_loop

				MOV cl, [rdi+42]
				AND cl, 7
				SHR cl, 1
				JZ noextra_off_2
					MOV r9, [rdi+32]

					MOVZX rcx, BYTE PTR [rdi+41]
					AND cl, 7
					SHL rcx, 7
					ADD r9, rcx

					MOV rcx, r9
					SUB rcx, [rdi+56]
					CMP rcx, [rdi+48]
					JB noextra_off_2
						MOV r9, [rdi+48]
						SHL r9, cl
						ADD r9, [rdi+56]						

						MOV [rdi+56], r9
				noextra_off_2:

				MOV [rdi+32], r9

			ADD rdi, 64
		DEC dl
		JNZ component_loop

	JMP mcu_loop


	align 16
		stream_scan_loop:

			MOV bl, 0

			LODSB
						
			DEC ebp
			JZ exit
	
				CMP al, 0FFh
				JNZ	h_factor_loop
					SHL eax, 8

					LODSB

					DEC ebp
					JZ exit

					TEST al, al
					JZ stuffby
						XOR al, 0D7h
						CMP al, 8
						JAE exit
						JMP stream_scan_loop
					stuffby:
						SHR eax, 8

		JMP h_factor_loop


	exit:
		MOV rax, rsi
		SUB rax, r11

	POP rdi
	POP rsi
	POP rbx
	POP rbp
		
	RET
JPEG_ReadSA_LSB	ENDP


ALIGN 16
JPEG_IDCT	PROC

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	PUSH rsi
	PUSH rdi
	PUSH rbx

	MOV rsi, rdx
	MOV rdx, r9
	MOV rbx, rcx

	LEA r9, [rbx+32]
	
	MOV [rsp+64], r8
	
	

align 16
	component_loop:
;		MOV rsi, [r9]
;		LEA rsi, [rdx+rsi*2]		

		MOV rdi, [rsp+64]
		MOVZX r8, BYTE PTR [rbx]
		
		MOV rax, [r9+32]

		ADD [rsp+64], r8
		MOV [rsp+72], rdi
		

	align 16
		data_unit_loop:
			LEA r13, [?idct_cofs@JPEG@Pics@@0PAY17EA@FA]

			MOVAPS xmm0, [rsi]
			MOVAPS xmm1, [rsi+16]
			MOVAPS xmm2, [rsi+32]
			MOVAPS xmm3, [rsi+48]
			MOVAPS xmm4, [rsi+64]
			MOVAPS xmm5, [rsi+80]
			MOVAPS xmm6, [rsi+96]
			MOVAPS xmm7, [rsi+112]

			ADD rsi, 128

			MOV r15, [r9+8]

			PMULLW xmm0, [r15]
			PMULLW xmm1, [r15+16]
			PMULLW xmm2, [r15+32]
			PMULLW xmm3, [r15+48]
			PMULLW xmm4, [r15+64]
			PMULLW xmm5, [r15+80]
			PMULLW xmm6, [r15+96]				
			PMULLW xmm7, [r15+112]

			MOV ch, 64

		align 16
			sample_loop:
			
				MOVAPS xmm8, [r13]
				MOVAPS xmm9, [r13+16]
				MOVAPS xmm10, [r13+32]
				MOVAPS xmm11, [r13+48]

				PMADDWD xmm8, xmm0				
				PMADDWD xmm9, xmm1				
				PMADDWD xmm10, xmm2				
				PMADDWD xmm11, xmm3

				PADDD xmm8, xmm9
				PADDD xmm10, xmm11

				MOVAPS xmm9, [r13+64]
				MOVAPS xmm11, [r13+80]
				MOVAPS xmm12, [r13+96]
				MOVAPS xmm13, [r13+112]

				PMADDWD xmm9, xmm4				
				PMADDWD xmm11, xmm5				
				PMADDWD xmm12, xmm6				
				PMADDWD xmm13, xmm7

				PADDD xmm9, xmm11
				PADDD xmm12, xmm13
			
				PADDD xmm8, xmm10
				PADDD xmm9, xmm12

				PADDD xmm8, xmm9

				MOVAPS xmm9, xmm8
				PSRLDQ xmm9, 8
				PADDD xmm8, xmm9

				MOVD r11, xmm8
				MOV r12, r11
				SHR r12, 32

				ADD r13, 128

				ADD r12d, r11d

				MOV cl, 16
				SUB cl, [rbx+4]
				SHR r12d, cl
				
				MOV cl, [r9+41]

				CMP BYTE PTR [rbx], 1 ; precision
				JA double_precision
					MOV r10w, 7Fh
					CMP r12w, r10w
					CMOVG r12w, r10w
					NOT r10w
					CMP r12w, r10w
					CMOVL r12w, r10w

					XOR r12b, r10b
					MOV [rdi], r12b

					MOV r14b, r12b
					ROL r14, 8

					ADD rdi, 4
												
					DEC cl
					JZ new_line
						MOV [rdi], r12b
						ADD rdi, 4
						DEC cl
						JZ new_line
							MOV [rdi], r12b
							ADD rdi, 4
							DEC cl
							JZ new_line
								MOV [rdi], r12b
								ADD rdi, 4
								JMP new_line
			align 16
				double_precision:
					XOR r12w, 8000h
					MOV [rdi], r12w

					MOV r14b, r12b
					ROR r12w, 8
					MOV r15b, r12b

					ROR r12w, 8
					ROL r14, 8
					ROL r15, 8
									

					ADD rdi, 8
												
					DEC cl
					JZ new_line
						MOV [rdi], r12w
						ADD rdi, 8
						DEC cl
						JZ new_line
							MOV [rdi], r12w
							ADD rdi, 8
							DEC cl
							JZ new_line
								MOV [rdi], r12w
								ADD rdi, 8



			align 16
				new_line:
					DEC ch
					TEST ch, 7
					JNZ sample_loop
						MOV cl, [r9+42]

						SUB rdi, [r9+16]
													
						CMP BYTE PTR [rbx], 1 ; precision
						JA double_p														
							ADD rdi, [rbx+16]																											

							DEC cl
							JZ no_nl
								MOVZX r10, BYTE PTR [r9+41]
								ROR r12, 8
								MOV r12b, r10b
								SHL r10, 2

							align 16
								hhf_loop_1:
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8															
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8
									MOV [rdi], r14b
									ADD rdi, r10
									ROL r14, 8

									SUB rdi, [r9+16]
									ADD rdi, 4
								DEC r12b
								JNZ hhf_loop_1

								ROL r12, 8
								SUB rdi, r10
								ADD rdi, [rbx+16]
																														
								DEC cl
								JZ no_nl
									ROR r12, 8
									MOVZX r10, BYTE PTR [r9+41]
									MOV r12b, r10b
									SHL r10, 2

								align 16
									hhf_loop_2:
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8															
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8
										MOV [rdi], r14b
										ADD rdi, r10
										ROL r14, 8

										SUB rdi, [r9+16]
										ADD rdi, 4
									DEC r12b
									JNZ hhf_loop_2

									ROL r12, 8
									SUB rdi, r10
									ADD rdi, [rbx+16]
																																
									DEC cl
									JZ no_nl
										ROR r12, 8
										MOVZX r10, BYTE PTR [r9+41]
										MOV r12b, r10b
										SHL r10, 2
									align 16
										hhf_loop_3:
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8															
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											MOV [rdi], r14b
											ADD rdi, r10
											ROL r14, 8
											
											SUB rdi, [r9+16]
											ADD rdi, 4
										DEC r12b
										JNZ hhf_loop_3

										ROL r12, 8
										SUB rdi, r10
										ADD rdi, [rbx+16]
										
										JMP no_nl
									align 16
										double_p:														
																																							
											ADD rdi, [r9+16]


											DEC cl
											JZ no_nl
												ROR r12, 8
												MOVZX r11, BYTE PTR [r9+41]
												MOV r12b, r11b
												SHL r11, 2
															
												align 16
												hhf2_loop_1:
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8
													MOV r10b, r15b
													SHL r10w, 8
													MOV r10b, r14b
													MOV [rdi], r10w
													ADD rdi, r11
													ROL r14, 8
													ROL r15, 8

													SUB rdi, [r9+16]
													ADD rdi, 8
												DEC r12b
												JNZ hhf2_loop_1

													ROL r12, 8

													SUB rdi, r11
													ADD rdi, [rbx+16]															

													DEC cl
													JZ no_nl
												
													ROR r12, 8
													MOVZX r11, BYTE PTR [r9+41]
													MOV r12b, r11b
													SHL r11, 2
															
													align 16
													hhf2_loop_2:
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8
														MOV r10b, r15b
														SHL r10w, 8
														MOV r10b, r14b
														MOV [rdi], r10w
														ADD rdi, r11
														ROL r14, 8
														ROL r15, 8

														SUB rdi, [r9+16]
														ADD rdi, 8
													DEC r12b
													JNZ hhf2_loop_2

													ROL r12, 8

													SUB rdi, r11
													ADD rdi, [rbx+16]															

													DEC cl
													JZ no_nl
														ROR r12, 8
														MOVZX r11, BYTE PTR [r9+41]
														MOV r12b, r11b
														SHL r11, 2
															
														align 16
														hhf2_loop_3:
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															MOV r10b, r15b
															SHL r10w, 8
															MOV r10b, r14b
															MOV [rdi], r10w
															ADD rdi, r11
															ROL r14, 8
															ROL r15, 8
															
															SUB rdi, [r9+16]
															ADD rdi, 8
														DEC r12b
														JNZ hhf2_loop_3
															
														ROL r12, 8
	
														SUB rdi, r11
														ADD rdi, [rbx+16]															

																
														
										align 16
											no_nl:

				


			TEST ch, ch
			JNZ sample_loop

			SUB rdi, [r9+24]
			ADD rdi, [r9+16]

			MOV r13, rdi
			SUB r13, [rsp+72]	
				
			CMP r13, rdx
			JB no_line_adjust			
				MOV rdi, [rsp+72]
				ADD rdi, [r9+24]
				MOV [rsp+72], rdi

			no_line_adjust:


		DEC rax
		JNZ data_unit_loop

		ADD r9, 48

	DEC BYTE PTR [rbx+24]
	JNZ component_loop

	POP rbx
	POP rdi
	POP rsi
	POP r15
	POP r14
	POP r13
	POP r12
	
	RET
JPEG_IDCT	ENDP

END

