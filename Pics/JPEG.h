/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Pics\Image.h"

namespace Pics {

	class JPEG : public Image {
	public:
		struct Decoder {
			unsigned short q_table[4][64];
			unsigned short h_codes[2][4][32];
			unsigned char * h_table[2][4][16];
		};


		struct ScanHeader {
			struct ComponentSet {
				unsigned char ** dc_table, ** ac_table;
				unsigned short * dc_codes, *ac_codes;

				unsigned short * qt_selector;

				unsigned char id, h_factor, v_factor, hh_f, vv_f, _reserved_c;
				unsigned short dc_prr;
				UI_64 unit_line_offset, _reserved;
			};

			unsigned char componentsCount;
			unsigned char spectral_start;
			unsigned char spectral_range;
			unsigned char previous_pt;

			unsigned char current_pt;
			unsigned char precision;
			unsigned short reserved_s;


			UI_64 line_offset, unit_line_offset, block_offset, mcu_offset, _reserved[3];

			ComponentSet component[4];
		};

		struct FrameHeader {
			struct ComponentSet {
				UI_64 f_offset;
				unsigned short * qt_pointer;
				UI_64 uline_offset, block_offset, mcu_count;

				unsigned char id;
				unsigned char h_factor;
				unsigned char v_factor;
				unsigned char qt_selector;

				unsigned char _reserved_c[4];
			};


			unsigned char precision, max_hf, max_vf, ecs_type, level_shift, c_reserved;
			unsigned short s_reserved;

			unsigned short height, c_width;
			unsigned short width, c_height;

			UI_64 line_offset;

			unsigned char componentsCount, bit_precision, _reserved[6];

			ComponentSet component[4];

			FrameHeader() : componentsCount(0), height(1), c_width(1), width(1), c_height(1) {

			}
		};


	private:
		class Exif {


		};


		static __declspec(align(16)) short idct_cofs[8][8][64];
		static __declspec(align(16)) short fdct_cofs[64][8][8];

		static const unsigned char zz_sequence[128];


		__declspec(align(16)) Decoder _decoder;

		SFSco::Object f_domain;

		unsigned short lines_count;

		FrameHeader _currentFrame;
		ScanHeader _currentScan;

		void GetFrameComponent(unsigned char, unsigned char, ScanHeader::ComponentSet &);

		UI_64 LoadHuffmanTable(const unsigned char *);
		UI_64 LoadQuantizationTable(const unsigned char *);

		UI_64 LoadFrameHeader(const unsigned char *);
		UI_64 LoadScanHeader(const unsigned char *);

		UI_64 DecodeHuffmanScan(const unsigned char *, unsigned int, unsigned char *);

	public:
		static const UI_64 jpeg_fdomain_tid = 4022940631;
		static unsigned short mu_factors[128];

		static UI_64 IsJPEG(const char *, UI_64 limit);
		static UI_64 InitializeDecoder();


		static UI_64 Save(unsigned int, void *, unsigned int);
		virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);

		UI_64 GetXTables(const unsigned char *, unsigned int);

		UI_64 XDecode(StripDesc &, unsigned char *);

		JPEG();
		virtual ~JPEG();

		// default color space YCbCr
	};


	extern "C" {
		UI_64 JPEG_HuffmanDCT(unsigned int, const JPEG::ScanHeader *, void *, const void *);
		UI_64 JPEG_HuffmanSA_MSB(unsigned int, const JPEG::ScanHeader *, const void *);
		UI_64 JPEG_HuffmanSA_LSB(unsigned int, const JPEG::ScanHeader *, const void *);
		UI_64 JPEG_ReadSA_LSB(unsigned int, const JPEG::ScanHeader *, const void *);

		UI_64 JPEG_IDCT(const JPEG::FrameHeader *, const void *, void *, UI_64);
		UI_64 JPEG_HuffmanLL(unsigned int, const JPEG::ScanHeader *, void *, const void *);

		void JPEG_ScanPack(unsigned short *, const UI_64 *, unsigned int);
		void JPEG_LLScanPack(unsigned short *, const UI_64 *, unsigned int);

		UI_64 JPEG_FDCT_CT_SSSE3(unsigned short *, const unsigned int *, const UI_64 *, unsigned int col_type);

		UI_64 JPEG_DCT_20(unsigned short *, const UI_64 *);
		UI_64 JPEG_DCT_40(unsigned short *, const UI_64 *);
		UI_64 JPEG_DCT_22(unsigned short *, const UI_64 *);

		void JPEG_ApplyQTs_SSSE3(unsigned short *, const UI_64 *);
		void JPEG_ApplyQTs_SSE3(unsigned short *, const UI_64 *);

		void JPEG_LLStats_SSSE3(unsigned short *, const unsigned int *, const UI_64 *);
		void JPEG_LLStats_SSE3(unsigned short *, const unsigned int *, const UI_64 *);

	}


}