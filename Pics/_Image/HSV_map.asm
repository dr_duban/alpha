
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


.CONST

ALIGN 16
float_FFFF	DWORD	3E2AAAABh, 37800080h, 477FFF00h, 47000000h
float_one	DWORD	3F800000h, 3F800000h, 3F800000h, 80008000h
float_6240	DWORD	40C00000h, 40000000h, 40800000h, 3B360B61h


.CODE

ALIGN 16
Image_HSV_interpolate	PROC
	MOV rax, rdx
	SHL rax, 4

	SHUFPS xmm2, xmm2, 0

align 16
	i_loop:
		MOVAPS xmm0, [rcx]
		MOVAPS xmm1, [rcx+rax]

		SUBPS xmm1, xmm0
		MULPS xmm1, xmm2

		ADDPS xmm0, xmm1

		MOVNTPS [rcx], xmm0

		ADD rcx, 16

	DEC edx
	JNZ i_loop



	RET
Image_HSV_interpolate	ENDP

ALIGN 16
Image_Remap_HSV2	PROC
	PUSH rsi
	PUSH rdi

	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV rbx, rdx

	XOR rdx, rdx

	

	MOV eax, [r8] ; h
	MOV edi, DWORD PTR [r8+4] ; s

	MUL edi

	LEA rsi, [rax*4]
	SHL rsi, 2 ; V plane offst

	
	SHL rdi, 4 ; H line offset


	XORPS xmm1, xmm1
	MOVAPS xmm11, XMMWORD PTR [float_one]
	MOVAPS xmm10, xmm11
	MOVDQU xmm13, [r8]
	CVTDQ2PS xmm13, xmm13
	SUBPS xmm13, xmm11

	MOVDQA xmm0, xmm13
	CMPPS xmm0, xmm1, 4

	DIVPS xmm11, xmm13
	ANDPS xmm11, xmm0

	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; height
	MOV r8d, [rcx+20] ; c_width

	SHR r8d, 1
	MOV r9d, r8d


	MOVAPS xmm12, XMMWORD PTR [float_6240]
	MOVAPS xmm14, XMMWORD PTR [float_FFFF]
	
	

	remap_loop:
			MOVDQA xmm0, [r11]			
			MOVDQA xmm9, xmm0

			PXOR xmm1, xmm1

			PUNPCKLWD xmm0, xmm1
			PUNPCKHWD xmm9, xmm1
			
			CVTDQ2PS xmm0, xmm0
			CVTDQ2PS xmm9, xmm9
			
			MOVAPS xmm2, xmm14
			SHUFPS xmm2, xmm2, 55h

			MULPS xmm0, xmm2
			MULPS xmm9, xmm2





			MOVAPS xmm1, xmm0
			MOVAPS xmm2, xmm0
			MOVAPS xmm3, xmm0

			SHUFPS xmm1, xmm1, 0C9h ; GBR
			SHUFPS xmm2, xmm2, 0D2h ; BRG

			MOVAPS xmm8, xmm1
			SUBPS xmm8, xmm2 

			MAXPS xmm3, xmm1
			MAXPS xmm3, xmm2 ; M (V)

			MINPS xmm2, xmm1
			MINPS xmm2, xmm0 ; m

			MOVAPS xmm1, xmm3
			SUBPS xmm1, xmm2 ; C = M - m

			XORPS xmm2, xmm2
			CMPPS xmm2, xmm1, 4 ; non-zero C

			DIVPS xmm8, xmm1
			ADDPS xmm8, xmm12
			ANDPD xmm8, xmm2

			CMPPS xmm0, xmm3, 0
			ANDPS xmm8, xmm0 ; H
			MOVMSKPS eax, xmm0

			DIVPS xmm1, xmm3
			XORPS xmm2, xmm2
			CMPPS xmm2, xmm3, 4
			ANDPS xmm1, xmm2 ; S

			
			SHR eax, 1
			JC hin_place
				SHUFPS xmm8, xmm8, 9
				SHR eax, 1
				JC hin_place
					SHUFPS xmm8, xmm8, 9
			hin_place:

			MOVSS xmm2, xmm8
			CMPSS xmm2, xmm12, 5
			ANDPS xmm2, xmm12

			SUBSS xmm8, xmm2
			MULSS xmm8, xmm14

			UNPCKLPS xmm8, xmm1

			SHUFPS xmm8, xmm3, 04h ; HSV
			MOVAPS xmm15, xmm8 ; HSV copy
			MULPS xmm8, xmm13 ; pointer

			CVTTPS2DQ xmm0, xmm8 ; indices
			CVTDQ2PS xmm1, xmm0
			SUBPS xmm8, xmm1 ; weights

			ADDPS xmm1, xmm10
			CMPPS xmm1, xmm13, 2

			MOVD r12d, xmm0 ; H index
			PSRLDQ xmm0, 4
			
			MOVD r13d, xmm0 ; S index
			SHL r13d, 4

			PSRLDQ xmm0, 4
			MOVD r14d, xmm0 ; V index

			; first V plane

			XOR edx, edx
			MOV eax, r14d

			MUL rsi

			MOV r15, rax
			 
			XOR edx, edx
			MOV eax, r12d

			MUL rdi

			ADD r15, rax

			MOVMSKPS eax, xmm1

			LEA r15, [r15 + r13]
			
			MOV r12, r15
			MOV r13, r15
			

			MOVAPS xmm0, [rbx+r15] ; (000)

			XOR rdx, rdx
			ROR eax, 1
			SBB rdx, 0
			AND rdx, rdi
			LEA r12, [r12 + rdx]

			MOVAPS xmm1, [rbx+r12] ; (100) H point

			XOR rdx, rdx
			ROR eax, 1
			ADC rdx, 0
			SHL rdx, 4
			
			LEA r13, [r13 + rdx]
			LEA r12, [r12 + rdx]
			

			MOVAPS xmm2, [rbx+r12] ; (110) HS point
			MOVAPS xmm3, [rbx+r13] ; (010) S point


		; second plane
			XOR rdx, rdx
			ROR eax, 1
			SBB rdx, 0
			AND rdx, rsi

			LEA r15, [r15 + rdx]
			LEA r12, [r12 + rdx]
			LEA r13, [r13 + rdx]


			MOVAPS xmm7, [rbx+r13] ; (011) SV point
			MOVAPS xmm6, [rbx+r12] ; (111) HSV point
			
			MOVAPS xmm4, [rbx+r15] ; (001) V point

			XOR rdx, rdx
			ROL eax, 3
			SBB rdx, 0
			AND rdx, rdi
			LEA r15, [r15 + rdi]
			
			

									
		; V filter
			MOVAPS xmm5, xmm8
			SHUFPS xmm5, xmm5, 0AAh


			SUBPS xmm6, xmm2
			SUBPS xmm7, xmm3

			MULPS xmm6, xmm5
			MULPS xmm7, xmm5

			ADDPS xmm2, xmm6
			ADDPS xmm3, xmm7

			MOVAPS xmm7, [rbx+r15] ; (101) HV point

			SUBPS xmm4, xmm0
			SUBPS xmm7, xmm1

			MULPS xmm4, xmm5
			MULPS xmm7, xmm5

			ADDPS xmm0, xmm4
			ADDPS xmm1, xmm7


		; S filter
			MOVAPS xmm5, xmm8
			SHUFPS xmm5, xmm5, 055h

			SUBPS xmm3, xmm0
			SUBPS xmm2, xmm1

			MULPS xmm3, xmm5
			MULPS xmm2, xmm5

			ADDPS xmm0, xmm3
			ADDPS xmm1, xmm2

		; H filter
			SHUFPS xmm8, xmm8, 0

			SUBPS xmm1, xmm0
			MULPS xmm1, xmm8

			ADDPS xmm0, xmm1






			XORPS xmm1, xmm1			
			XORPS xmm2, xmm2

			MOVSS xmm1, xmm0
			MOVSS xmm2, xmm15 ; original H

			SHUFPS xmm15, xmm15, 99h ; SV
			SHUFPS xmm0, xmm0, 99h

			MULPS xmm0, xmm15 ; scaled SV

			MOVAPS xmm3, xmm12
			SHUFPS xmm3, xmm3, 0FFh

			MULSS xmm1, xmm3

			ADDSS xmm1, xmm2
			ADDSS xmm1, xmm10

			MOVSS xmm2, xmm1
			CMPSS xmm2, xmm10, 6
			ANDPS xmm2, xmm10
			SUBSS xmm1, xmm2


			MULSS xmm1, xmm12 ; H6
			SHUFPS xmm1, xmm1, 0			
			SUBPS xmm1, xmm12

			MOVSS xmm2, xmm0

			SHUFPS xmm2, xmm2, 0 ; S
			SHUFPS xmm0, xmm0, 55h ; V

			MULPS xmm2, xmm0 ; C
			SUBPS xmm0, xmm2 ; m
			UNPCKLPS xmm0, xmm10
			SHUFPS xmm0, xmm0, 0C0h

			XORPS xmm3, xmm3
			XORPS xmm7, xmm7
			CMPPS xmm6, xmm6, 0
			MOVSS xmm7, xmm6
			UNPCKLPS xmm6, xmm3

			MOVMSKPS edx, xmm1 ; negative
			MOVAPS xmm5, xmm14
			SHUFPS xmm5, xmm5, 0AAh

			AND edx, 7

			CMP edx, 1
			JZ h_select
				SHUFPS xmm7, xmm7, 0E4h
				SHUFPS xmm6, xmm6, 0F0h
				SHUFPS xmm1, xmm1, 9
				CMP edx, 7
				JZ h_select
					SHUFPS xmm7, xmm7, 10h
					SHUFPS xmm6, xmm6, 0C7h
					SHUFPS xmm1, xmm1, 9
			h_select:


				ANDPS xmm2, xmm6
				ADDSS xmm1, xmm10
								
				CMPSS xmm3, xmm1, 1 ; positive
				MOVAPS xmm4, xmm3
				SHUFPS xmm4, xmm4, 0
				XORPS xmm7, xmm4
				ANDPS xmm7, xmm6

				ANDPS xmm3, xmm1
				SUBSS xmm1, xmm3
				SHUFPS xmm5, xmm5, 0
				SUBSS xmm1, xmm3
				
			
				MULSS xmm1, xmm2 ; X			
				SHUFPS xmm1, xmm1, 0
				ANDPS xmm1, xmm7
				SUBPS xmm2, xmm1
				ADDPS xmm2, xmm0
				MULPS xmm2, xmm5
									
				
			MOVAPS xmm0, xmm9 ; second pixel
				
				MOVAPS xmm9, xmm2












			MOVAPS xmm1, xmm0
			MOVAPS xmm2, xmm0
			MOVAPS xmm3, xmm0

			SHUFPS xmm1, xmm1, 0C9h ; GBR
			SHUFPS xmm2, xmm2, 0D2h ; BRG

			MOVAPS xmm8, xmm1
			SUBPS xmm8, xmm2 

			MAXPS xmm3, xmm1
			MAXPS xmm3, xmm2 ; M (V)

			MINPS xmm2, xmm1
			MINPS xmm2, xmm0 ; m

			MOVAPS xmm1, xmm3
			SUBPS xmm1, xmm2 ; C = M - m

			XORPS xmm2, xmm2
			CMPPS xmm2, xmm1, 4 ; non-zero C

			DIVPS xmm8, xmm1
			ADDPS xmm8, xmm12
			ANDPD xmm8, xmm2

			CMPPS xmm0, xmm3, 0
			ANDPS xmm8, xmm0 ; H
			MOVMSKPS eax, xmm0

			DIVPS xmm1, xmm3
			XORPS xmm2, xmm2
			CMPPS xmm2, xmm3, 4
			ANDPS xmm1, xmm2 ; S

			
			SHR eax, 1
			JC hin_place_s
				SHUFPS xmm8, xmm8, 9
				SHR eax, 1
				JC hin_place_s
					SHUFPS xmm8, xmm8, 9
			hin_place_s:

			MOVSS xmm2, xmm8
			CMPSS xmm2, xmm12, 5
			ANDPS xmm2, xmm12

			SUBSS xmm8, xmm2
			MULSS xmm8, xmm14

			UNPCKLPS xmm8, xmm1

			SHUFPS xmm8, xmm3, 04h ; HSV
			MOVAPS xmm15, xmm8 ; HSV copy
			MULPS xmm8, xmm13 ; pointer

			CVTTPS2DQ xmm0, xmm8 ; indices
			CVTDQ2PS xmm1, xmm0
			SUBPS xmm8, xmm1 ; weights

			ADDPS xmm1, xmm10
			CMPPS xmm1, xmm13, 2

			MOVD r12d, xmm0 ; H index
			PSRLDQ xmm0, 4			
			
			MOVD r13d, xmm0 ; S index
			SHL r13d, 4

			PSRLDQ xmm0, 4
			MOVD r14d, xmm0 ; V index

			; first V plane

			XOR edx, edx
			MOV eax, r14d

			MUL rsi

			MOV r15, rax
			 
			XOR edx, edx
			MOV eax, r12d

			MUL rdi

			ADD r15, rax

			MOVMSKPS eax, xmm1

			LEA r15, [r15 + r13]
			
			MOV r12, r15
			MOV r13, r15
			

			MOVAPS xmm0, [rbx+r15] ; (000)

			XOR rdx, rdx
			ROR eax, 1
			SBB rdx, 0
			AND rdx, rdi
			LEA r12, [r12 + rdx]

			MOVAPS xmm1, [rbx+r12] ; (100) H point

			XOR rdx, rdx
			ROR eax, 1
			ADC rdx, 0
			SHL rdx, 4
			LEA r13, [r13 + rdx]
			LEA r12, [r12 + rdx]


			MOVAPS xmm2, [rbx+r12] ; (110) HS point
			MOVAPS xmm3, [rbx+r13] ; (010) S point


		; second plane
			XOR rdx, rdx
			ROR eax, 1
			SBB rdx, 0
			AND rdx, rsi

			LEA r15, [r15 + rdx]
			LEA r12, [r12 + rdx]
			LEA r13, [r13 + rdx]


			MOVAPS xmm7, [rbx+r13] ; (011) SV point
			MOVAPS xmm6, [rbx+r12] ; (111) HSV point
			
			MOVAPS xmm4, [rbx+r15] ; (001) V point


			XOR rdx, rdx
			ROL eax, 3
			SBB rdx, 0
			AND rdx, rdi
			LEA r15, [r15 + rdx]

			
			
			MOV eax, 477FFF00h
									
		; V filter
			MOVAPS xmm5, xmm8
			SHUFPS xmm5, xmm5, 0AAh


			SUBPS xmm6, xmm2
			SUBPS xmm7, xmm3

			MULPS xmm6, xmm5
			MULPS xmm7, xmm5

			ADDPS xmm2, xmm6
			ADDPS xmm3, xmm7

			MOVAPS xmm7, [rbx+r15] ; (101) HV point

			SUBPS xmm4, xmm0
			SUBPS xmm7, xmm1

			MULPS xmm4, xmm5
			MULPS xmm7, xmm5

			ADDPS xmm0, xmm4
			ADDPS xmm1, xmm7


		; S filter
			MOVAPS xmm5, xmm8
			SHUFPS xmm5, xmm5, 055h

			SUBPS xmm3, xmm0
			SUBPS xmm2, xmm1

			MULPS xmm3, xmm5
			MULPS xmm2, xmm5

			ADDPS xmm0, xmm3
			ADDPS xmm1, xmm2

		; H filter
			SHUFPS xmm8, xmm8, 0

			SUBPS xmm1, xmm0
			MULPS xmm1, xmm8

			ADDPS xmm0, xmm1






			XORPS xmm1, xmm1			
			XORPS xmm2, xmm2

			MOVSS xmm1, xmm0
			MOVSS xmm2, xmm15 ; original H

			SHUFPS xmm15, xmm15, 99h ; SV
			SHUFPS xmm0, xmm0, 99h

			MULPS xmm0, xmm15 ; scaled SV

			MOVAPS xmm3, xmm12
			SHUFPS xmm3, xmm3, 0FFh

			MULSS xmm1, xmm3

			ADDSS xmm1, xmm2
			ADDSS xmm1, xmm10

			MOVSS xmm2, xmm1
			CMPSS xmm2, xmm10, 6
			ANDPS xmm2, xmm10
			SUBSS xmm1, xmm2


			MULSS xmm1, xmm12 ; H6
			SHUFPS xmm1, xmm1, 0			
			SUBPS xmm1, xmm12

			MOVSS xmm2, xmm0

			SHUFPS xmm2, xmm2, 0 ; S
			SHUFPS xmm0, xmm0, 55h ; V

			MULPS xmm2, xmm0 ; C
			SUBPS xmm0, xmm2 ; m
			UNPCKLPS xmm0, xmm10
			SHUFPS xmm0, xmm0, 0C0h

			XORPS xmm3, xmm3
			XORPS xmm7, xmm7
			CMPPS xmm6, xmm6, 0
			MOVSS xmm7, xmm6
			UNPCKLPS xmm6, xmm3

			MOVMSKPS edx, xmm1 ; negative

			MOVAPS xmm5, xmm14
			SHUFPS xmm5, xmm5, 0AAh

			AND edx, 7

			CMP edx, 1
			JZ h_select_s
				SHUFPS xmm7, xmm7, 0E4h
				SHUFPS xmm6, xmm6, 0F0h
				SHUFPS xmm1, xmm1, 9
				CMP edx, 7
				JZ h_select_s
					SHUFPS xmm7, xmm7, 10h
					SHUFPS xmm6, xmm6, 0C7h
					SHUFPS xmm1, xmm1, 9
			h_select_s:


				ANDPS xmm2, xmm6
				ADDSS xmm1, xmm10
								
				CMPSS xmm3, xmm1, 1 ; positive
				MOVAPS xmm4, xmm3
				SHUFPS xmm4, xmm4, 0
				XORPS xmm7, xmm4
				ANDPS xmm7, xmm6

				ANDPS xmm3, xmm1
				SUBSS xmm1, xmm3
				SHUFPS xmm5, xmm5, 0
				SUBSS xmm1, xmm3
				
			
				MULSS xmm1, xmm2 ; X			
				SHUFPS xmm1, xmm1, 0
				ANDPS xmm1, xmm7
				SUBPS xmm2, xmm1
				ADDPS xmm2, xmm0
				MULPS xmm2, xmm5
									
				MOVAPS xmm3, xmm10
				SHUFPS xmm3, xmm3, 0FFh
				MOVAPS xmm4, xmm14
				SHUFPS xmm4, xmm4, 0FFh

				SUBPS xmm9, xmm4
				SUBPS xmm2, xmm4
				
				CVTPS2DQ xmm9, xmm9
				CVTPS2DQ xmm2, xmm2

				PACKSSDW xmm9, xmm2
				XORPS xmm9, xmm3


				MOVNTDQ [r11], xmm9


			ADD r11, 16

		DEC r8d
		JNZ remap_loop

		MOV r8d, r9d

	DEC r10d
	JNZ remap_loop

	POP r15
	POP r14
	POP r13
	POP r12

	POP rbx

	POP rdi
	POP rsi

	RET
Image_Remap_HSV2	ENDP


ALIGN 16
Image_Remap_HSV1	PROC

	RET
Image_Remap_HSV1	ENDP


END

