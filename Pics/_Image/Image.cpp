/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Pics\Image.h"
#include "Pics\JPEG.h"
#include "Pics\BMP.h"
#include "Pics\PNG.h"
#include "Pics\GIF.h"
#include "Pics\TIFF.h"


#include "OpenGL\Core\OGL_15.h"

#include "System\SysUtils.h"

#include "Compression\Deflate.h"


#define IMGT_COUNT				3


#pragma warning(disable:4244)


UI_64 Pics::Image::bend_of_line = 1;
UI_64 Pics::Image::current_glimage = -1;

SFSco::IOpenGL * Pics::Image::main_ogl = 0;

SFSco::List<Pics::Image::Properties> Pics::Image::pi_list(256);

unsigned int Pics::Image::eq_runner = 0;
unsigned int Pics::Image::eq_top = 0;
unsigned int Pics::Image::dq_runner[2];
unsigned int Pics::Image::dq_top[2];

Control::SimpleQueue Pics::Image::prev_queue;

unsigned int Pics::Image::current_de_queue = 0;

HANDLE Pics::Image::_de_thread = 0;
HANDLE Pics::Image::_en_thread = 0;
HANDLE Pics::Image::prev_thread = 0;
HANDLE Pics::Image::de_block = 0;

SFSco::Object Pics::Image::decoding_queue[2];
SFSco::Object Pics::Image::en_queue;

SFSco::IOpenGL::ScreenSet Pics::Image::shot_set;
SFSco::Object Pics::Image::ss_directory;
SFSco::Object Pics::Image::noname;


unsigned int Pics::Image::render_runner = 0;
unsigned int Pics::Image::render_top = 0;
unsigned int Pics::Image::slide_delay = 100;

SFSco::Object Pics::Image::render_queue;


const wchar_t Pics::Image::imgt_strings[][6] = {
	{3, L'J', L'P', 'G'},
	{3, L'P', L'N', 'G'},
	{3, L'B', L'M', 'P'}

};


unsigned int Pics::Image::GetIMGTCount() {
	return IMGT_COUNT;
}

const wchar_t * Pics::Image::GetIMGTString(unsigned int it_id) {
	const wchar_t * result(0);	

	if (it_id < IMGT_COUNT) {
		result = imgt_strings[it_id];
	}

	return result;


}


Pics::Image::Properties & Pics::Image::Properties::operator =(const Properties & ref_p) {
	System::MemoryCopy_SSE3(this, &ref_p, sizeof(Properties));
	return *this;
}

Pics::Image::Properties::Properties(const Properties & ref_p) {
	*this = ref_p;
}

Pics::Image::Properties::Properties() {
	Blank();
};

Pics::Image::Properties::operator bool() {
	return (im_obj);
};

void Pics::Image::Properties::Blank() {
	f_source = INVALID_HANDLE_VALUE;

	signature[0] = signature[1] = signature[2] = signature[3] = signature[4] = signature[5] = signature[6] = signature[7] = 0;

	width = height = c_width = c_height = x_location = y_location = 0;
	image_count = interval = 0;
	next_index = prev_index = -1;
	precision = 1;
	norgba = direction = c_reserved = 0;

	im_obj.Blank();
				
	_reserved[0] = _reserved[1] = _reserved[2] = _reserved[3] = _reserved[4] = _reserved[5] = _reserved[6] = _reserved[7] = _reserved[8] = _reserved[9] = 0;
}

void Pics::Image::Properties::Destroy() {					
	im_obj.Destroy();

	if (f_source != INVALID_HANDLE_VALUE) CloseHandle(f_source);
	f_source = INVALID_HANDLE_VALUE;
}










UI_64 Pics::Image::Initialize(SFSco::IOpenGL ** oglm) {
	wchar_t q_name[] = {19, L'i',L'm', L'a', L'g', L'e', L' ', L'p', L'r', L'e', L'v', L'i', L'e', L'w', L' ', L'q', L'u', L'e', L'u', L'e'};
	UI_64 result(0);


	if (!oglm) return -52;
	main_ogl = *oglm;



	JPEG::InitializeDecoder();
	PNG::InitializeEncoder();

	InitRIMMTable();

	dq_runner[0] = dq_runner[1] = 0;
	dq_top[0] = dq_top[1] = 0;


	shot_set.i_type = SFSco::program_cfg.screenshot_type; // ImageType::JPG;
	shot_set.qd_factor = SFSco::program_cfg.screenshot_quality;
	shot_set.q_factor = reinterpret_cast<double &>(SFSco::program_cfg.screenshot_quality)*100.0;

	if (!result) {
		result = en_queue.New(image_en_queue_tid, encoding_queue_capacity*sizeof(CodecSet));

		result |= decoding_queue[0].New(image_de_queue_tid, decoding_queue_capacity*sizeof(OpenGL::DecodeRecord));
		result |= decoding_queue[1].New(image_de_queue_tid, decoding_queue_capacity*sizeof(OpenGL::DecodeRecord));


		if (result != -1) {
			eq_top = encoding_queue_capacity;
			dq_top[0] = decoding_queue_capacity;
			dq_top[1] = decoding_queue_capacity;

			result = prev_queue.Init(sizeof(OpenGL::DecodeRecord), decoding_queue_capacity, q_name, SFSco::mem_mgr, image_de_queue_tid);

		}
	}

	if (!result) {
		result = ss_directory.New(0, 512*sizeof(wchar_t));
		if (wchar_t * d_ptr = reinterpret_cast<wchar_t *>(ss_directory.Acquire())) {
			d_ptr[0] = 10;

			d_ptr[1] = L'.';
			d_ptr[2] = L'\\';
			d_ptr[3] = L't';
			d_ptr[4] = L'e';
			d_ptr[5] = L'm';
			d_ptr[6] = L'p';
			d_ptr[7] = L'_';
			d_ptr[8] = L'd';
			d_ptr[9] = L'\\';

			ss_directory.Release();


			if (render_queue.New(render_q_tid, render_queue_size*sizeof(OpenGL::DecodeRecord)) != -1) {
				render_top = render_queue_size;
				render_runner = 0;

				result = 0;
			}
		}	
	}


	if (de_block = CreateEvent(0, 0, 0, 0)) {
		if (!result) {
			_de_thread = CreateThread(0, 0x20000, DecoderLoop, 0, 0, 0);
			if (!_de_thread) result = GetLastError();
		}
		
		if (!result) {
			WaitForSingleObject(de_block, 10000);
			prev_thread = CreateThread(0, 0x20000, PreviewLoop, 0, 0, 0);
			if (!prev_thread) result = GetLastError();
		}

		if (!result) {
			_en_thread = CreateThread(0, 0x20000, EncoderLoop, 0, 0, 0);
			if (!_en_thread) result = GetLastError();
		}
		CloseHandle(de_block);
		de_block = 0;
	}


	return result;
}

UI_64 Pics::Image::Finalize() {
	UI_64 result(0);

	SFSco::program_cfg.screenshot_type = shot_set.i_type; // ImageType::JPG;
	SFSco::program_cfg.screenshot_quality = shot_set.qd_factor;

	bend_of_line = 0;

	if (prev_thread) {
		WaitForSingleObject(prev_thread, INFINITE);
		CloseHandle(prev_thread);
	}

	if (_de_thread) {
		WaitForSingleObject(_de_thread, INFINITE);
		CloseHandle(_de_thread);
	}

	if (_en_thread) {
		WaitForSingleObject(_en_thread, INFINITE);
		CloseHandle(_en_thread);
	}

	pi_list.Destroy();

	return result;
}


Pics::Image::Image() {
	
}

Pics::Image::~Image() {
	
}

void Pics::Image::GetWH(UI_64 iid, float & iw, float & ih) {
	if (Properties props = pi_list[iid]) {
		iw = props.width;
		ih = props.height;

	}

}

unsigned int Pics::Image::OGLBind(UI_64 im_id, float * box_vals, unsigned int tile_w, unsigned int tile_h) {
	unsigned int result(0), col_count(1), row_count(1), tot_count(0), level_idx(0), tile_size(0), row_adjust(0), tile_cw(0);
	GLenum dtype(GL_UNSIGNED_BYTE);

	if (Properties i_props = pi_list.Remove(im_id)) {
		
		OpenGL::glGenTextures(1, &result);

		if (result) {
			OpenGL::ActiveTexture(GL_TEXTURE0 + OpenGL::Core::rect_unit);

			if ((tile_w) && (tile_h)) {
				col_count = (i_props.width + tile_w - 1) / tile_w;
				row_count = (i_props.height + tile_h - 1) / tile_h;
			}

			if ((row_count | col_count) == 1) {

				OpenGL::glBindTexture(GL_TEXTURE_2D, result);

				OpenGL::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
				OpenGL::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				OpenGL::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				OpenGL::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				OpenGL::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

				if (i_props.precision == 2) dtype = GL_UNSIGNED_SHORT;

				if (void * impo = i_props.im_obj.Acquire()) {
					OpenGL::glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, i_props.c_width, i_props.c_height, 0, GL_RGBA, dtype, impo);
					OpenGL::glFlush();

					i_props.im_obj.Release();
				}

				box_vals[0] = i_props.c_width;
				box_vals[1] = i_props.c_height;
				box_vals[2] = i_props.width;
				box_vals[3] = i_props.height;

			} else {
				OpenGL::glBindTexture(GL_TEXTURE_2D_ARRAY, result);
				OpenGL::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
				OpenGL::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				OpenGL::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				OpenGL::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				OpenGL::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				tot_count = row_count*col_count;
				tile_cw = (tile_w + (1<<size_paragraph) - 1) & (-1 << size_paragraph);

				if (tot_count > OpenGL::Core::max_texture_layers) tot_count = OpenGL::Core::max_texture_layers;
				OpenGL::TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, tile_cw, tile_h, tot_count, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0); // Core::max_texture_layers
				
				tile_size = tile_cw*tile_h;
				row_adjust = i_props.c_width*tile_h - col_count*tile_size;

				level_idx = 0;

				if (unsigned int * impo = reinterpret_cast<unsigned int *>(i_props.im_obj.Acquire())) {
					for (unsigned int i(0);i<row_count;i++) {
						for (unsigned int j(0);j<col_count;j++) {
							OpenGL::TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, level_idx++, tile_cw, tile_h, 1, GL_RGBA, GL_UNSIGNED_BYTE, impo);
							impo += tile_size;
							if (level_idx >= tot_count) break;
						}
						impo += row_adjust;
					}
					i_props.im_obj.Release();
				}

				box_vals[0] = tile_cw;
				box_vals[1] = tile_h;
				box_vals[2] = tile_w;
				box_vals[3] = tile_h;

			}
		}

		i_props.Destroy();

		if ((i_props.interval) && (i_props.prev_index != -1)) {
			pi_list.Remove(i_props.prev_index).Destroy();
		}

		for (;(i_props.next_index != -1);) {
			i_props = pi_list.Remove(i_props.next_index);
			i_props.Destroy();
		}
	}


	return result;
}


int Pics::Image::OGLoad(UI_64 ssel) {
	int result(0);
	GLenum dtype(GL_UNSIGNED_BYTE);

	if (current_glimage != ssel)
	if (Properties props = pi_list[ssel]) {
		if (const unsigned char * impo = reinterpret_cast<const unsigned char *>(props.im_obj.Acquire())) {
			if (props.precision == 2) dtype = GL_UNSIGNED_SHORT;
			OpenGL::glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, props.c_width, props.c_height, 0, GL_RGBA, dtype, impo);
			current_glimage = ssel;
				
			props.im_obj.Release();
		}
	}

	return result;
}


UI_64 Pics::Image::Create(unsigned int wi, unsigned int he, unsigned int ini_flag) {
	UI_64 result(-1), ssize(0);

	Image::Properties sprops;


	sprops.width = wi;
	sprops.height = he;
	sprops.c_width = (wi + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
	sprops.c_height = (he + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
	
	for (unsigned int i(0);i<8;i++) sprops.signature[i] = 0;

	ssize = sprops.c_width*sprops.c_height;

	if (sprops.im_obj.New(image_rstr_tid, ssize*sizeof(unsigned int), SFSco::large_mem_mgr) != -1) {
		if (ini_flag) {
			if (unsigned int * pipo = reinterpret_cast<unsigned int *>(sprops.im_obj.Acquire())) {
				for (UI_64 i(0);i<ssize;i++) {
					pipo[i] = 0xFF000000 | System::Gen32();

				}

				sprops.im_obj.Release();
			}
		}


		result = pi_list.Add(sprops);
	}

	return result;
}


UI_64 Pics::Image::LoadRes(unsigned int rid, unsigned int rty, HMODULE libin) {
	UI_64 iid(-1);

	if (HRSRC tres = FindResource(libin, (LPCWSTR)rid, (LPCWSTR)rty)) {
		if (void * rsc_ptr = LockResource(LoadResource(libin, tres))) {
			iid = Load(rsc_ptr, SizeofResource(libin, tres));
		}
	}

	return iid;
}

UI_64 Pics::Image::Load(const void * src_ptr, unsigned int isize) {
	UI_64 result(-1);
	UI_64 iid(-1), tsize(65536);

	if (isize < tsize) tsize = isize;

	if (JPEG::IsJPEG(reinterpret_cast<const char *>(src_ptr), tsize) != -1) {
		JPEG jimg;
		result = jimg.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);

	} else {
		if (PNG::IsPNG(reinterpret_cast<const char *>(src_ptr), tsize)) {
			PNG pnguk;
			result = pnguk.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);

		} else {
			if (Bitmap::IsBMP(reinterpret_cast<const char *>(src_ptr), tsize)) {
				Bitmap bmpuk;
				UnmapViewOfFile(src_ptr);
				result = bmpuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);

			} else {
				if (Icon::IsICO(reinterpret_cast<const char *>(src_ptr), tsize)) {
					Icon iconuk;
					UnmapViewOfFile(src_ptr);
					result = iconuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);

				} else {
					if (GIF::IsGIF(reinterpret_cast<const char *>(src_ptr), tsize)) {
						GIF gifuk;
						UnmapViewOfFile(src_ptr);
						result = gifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);
					} else {
						if (TIFF::IsTIFF(reinterpret_cast<const char *>(src_ptr), tsize)) {
							TIFF tifuk;
							UnmapViewOfFile(src_ptr);
							result = tifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), isize, iid);
						} else {


						}
					}
				}
			}
		}
	}

	if (iid != -1) {
		// color space transform
		if (Properties props = pi_list[iid]) {
			if (unsigned char * impo = reinterpret_cast<unsigned char *>(props.im_obj.Acquire())) {
				if (props.norgba) {
					if (props.precision == 1) ToRGBA_1(props.norgba, impo, props.c_width, props.height);
					else ToRGBA_2(props.norgba, reinterpret_cast<unsigned short *>(impo), props.c_width, props.height);
				}

				if (props.norgba != YCbCr) props.norgba = 0;

				props.im_obj.Release();
			}
				
			pi_list.Set(iid, props);
		}

		// filters

	}

		
	return iid;
}


UI_64 Pics::Image::Load(const wchar_t * fname) {
	UI_64 result(-1), blr(65536);
	UI_64 iid(-1), tsize(65536);

	HANDLE fhandle = CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	LARGE_INTEGER fsize;

	if (fhandle != INVALID_HANDLE_VALUE) {
		if (GetFileSizeEx(fhandle, &fsize)) {
			if (fsize.QuadPart < 0x10000000) {
				if (fsize.LowPart < blr) blr = 0;
				if (fsize.LowPart < tsize) tsize = fsize.LowPart;
				
				if (HANDLE file_map = CreateFileMapping(fhandle, 0, PAGE_READONLY, 0, 0, 0)) {
					if (const char * src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, blr))) {
						if (JPEG::IsJPEG(src_ptr, tsize) != -1) {
							JPEG jimg;
							UnmapViewOfFile(src_ptr);
							if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = jimg.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);
						} else {
							if (PNG::IsPNG(src_ptr, tsize)) {
								PNG pnguk;
								UnmapViewOfFile(src_ptr);
								if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = pnguk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

							} else {
								if (Bitmap::IsBMP(src_ptr, tsize)) {
									Bitmap bmpuk;
									UnmapViewOfFile(src_ptr);
									if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = bmpuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

								} else {
									if (Icon::IsICO(src_ptr, tsize)) {
										Icon iconuk;
										UnmapViewOfFile(src_ptr);
										if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = iconuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

									} else {
										if (GIF::IsGIF(src_ptr, tsize)) {
											GIF gifuk;
											UnmapViewOfFile(src_ptr);
											if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = gifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);
										} else {
											if (TIFF::IsTIFF(src_ptr, tsize)) {
												TIFF tifuk;
												UnmapViewOfFile(src_ptr);
												if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = tifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);
											} else {


											}
										}
									}
								}
							}
						}
					
						UnmapViewOfFile(src_ptr);
					}
					CloseHandle(file_map);
				}
			} else {
				// file too large

			}
		}		
	}

	if (iid != -1) {
		// color space transform
		if (Properties props = pi_list[iid]) {
			if (unsigned char * impo = reinterpret_cast<unsigned char *>(props.im_obj.Acquire())) {
				if (props.norgba) {
					if (props.precision == 1) ToRGBA_1(props.norgba, impo, props.c_width, props.height);
					else ToRGBA_2(props.norgba, reinterpret_cast<unsigned short *>(impo), props.c_width, props.height);
				}

				if (props.norgba != YCbCr) props.norgba = 0;

				props.im_obj.Release();
			}
			props.f_source = fhandle;
			pi_list.Set(iid, props);
		}

		// filters

	} else {
		if (fhandle != INVALID_HANDLE_VALUE) CloseHandle(fhandle);
	}

		
	return iid;
}

UI_64 Pics::Image::Load(SFSco::Object & fname_obj, UI_64 str_offset) {
	UI_64 result(-1), blr(65536);
	UI_64 iid(-1), tsize(16384);

	HANDLE fhandle(INVALID_HANDLE_VALUE);
	LARGE_INTEGER fsize;

	if (const wchar_t * fname_ptr = reinterpret_cast<const wchar_t *>(fname_obj.Acquire())) {
		fhandle = CreateFile(fname_ptr + str_offset, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		fname_obj.Release();
	}

	if (fhandle != INVALID_HANDLE_VALUE) {
		if (GetFileSizeEx(fhandle, &fsize)) {
			if (fsize.QuadPart < 0x10000000) {
				if (fsize.LowPart < blr) blr = 0;
				if (fsize.LowPart < tsize) tsize = fsize.LowPart;
				
				if (HANDLE file_map = CreateFileMapping(fhandle, 0, PAGE_READONLY, 0, 0, 0)) {
					if (const char * src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, blr))) {
						if (JPEG::IsJPEG(src_ptr, tsize) != -1) {						
							JPEG jimg;
							UnmapViewOfFile(src_ptr);
							if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = jimg.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

						} else {
							if (PNG::IsPNG(src_ptr, tsize)) {
								PNG pnguk;
								UnmapViewOfFile(src_ptr);
								if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = pnguk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

							} else {
								if (Bitmap::IsBMP(src_ptr, tsize)) {
									Bitmap bmpuk;
									UnmapViewOfFile(src_ptr);
									if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = bmpuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

								} else {
									if (Icon::IsICO(src_ptr, tsize)) {
										Icon iconuk;
										UnmapViewOfFile(src_ptr);
										if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = iconuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);

									} else {
										if (GIF::IsGIF(src_ptr, tsize)) {
											GIF gifuk;
											UnmapViewOfFile(src_ptr);
											if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = gifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);
										} else {
											if (TIFF::IsTIFF(src_ptr, tsize)) {
												TIFF tifuk;
												UnmapViewOfFile(src_ptr);
												if (src_ptr = reinterpret_cast<const char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) result = tifuk.Load(reinterpret_cast<const unsigned char *>(src_ptr), fsize.LowPart, iid);
											} else {


											}
										}
									}
								}
							}
						}
					
						UnmapViewOfFile(src_ptr);
					}
					CloseHandle(file_map);
				}
			} else {
				// file too large

			}
		}
				
	}

	if (iid != -1) {
		// color space transform
		if (Properties props = pi_list[iid]) {	
			if (unsigned char * impo = reinterpret_cast<unsigned char *>(props.im_obj.Acquire())) {
				if (props.norgba) {
					if (props.precision == 1) ToRGBA_1(props.norgba, impo, props.c_width, props.height);
					else ToRGBA_2(props.norgba, reinterpret_cast<unsigned short *>(impo), props.c_width, props.height);
				}

				if (props.norgba != YCbCr) props.norgba = 0;

				props.im_obj.Release();	
			}
			props.f_source = fhandle;
			pi_list.Set(iid, props);
		}

		// filters

	} else {
		if (fhandle != INVALID_HANDLE_VALUE) CloseHandle(fhandle);

	}

		
	return iid;
}


	
void Pics::Image::ToRGBA_1(unsigned char ctype, unsigned char * pipo, unsigned int c_width, unsigned int height) {
	switch (ctype) {
/*
		case YCbCr:
			YCC2RGB_1(pipo, c_width, height);
		break;
*/
		case CMYK:
			Image_CMY2RGB_1(pipo, c_width, height);
		break;
		case YCCK:
			Image_YCK2RGB_1(pipo, c_width, height);
		break;
		case CMYJ:
			Image_CMJ2RGB_1(pipo, c_width, height);
		break;
		case BW_special:
			Image_BW2YB_1(pipo, c_width, height);
		break;

	}
}

void Pics::Image::ToRGBA_2(unsigned char ctype, unsigned short * pipo, unsigned int c_width, unsigned int height) {
	switch (ctype) {
/*
		case YCbCr:
			YCC2RGB_2(pipo, c_width, height);
		break;
*/
		case CMYK:
			Image_CMY2RGB_2(pipo, c_width, height);
		break;
		case BW_special:
			Image_BW2YB_2(pipo, c_width, height);
		break;

	}
}



void Pics::Image::Free(UI_64 iid) {

	for (Properties iprops = pi_list.Remove(iid);;iprops = pi_list.Remove(iprops.next_index)) {
		iprops.Destroy();

		if ((iprops.interval) && (iprops.prev_index != -1)) {
			pi_list.Remove(iprops.prev_index).Destroy();
		}

		if (iprops.next_index == -1) break;
	}

}

UI_64 Pics::Image::SaveAs(UI_64 imid, SFSco::IOpenGL::ScreenSet & scfg, SFSco::Object & fname_obj) {
	UI_64 result(0), xlength(0);
	unsigned int hval(0), wval(0);

	Control::Nucleus tnucl;


	if (Properties props = pi_list[imid]) {
		wval = props.width;
		hval = props.height;

		if ((scfg.screen_width == 0) || (scfg.screen_width > wval)) scfg.screen_width = wval;
		if ((scfg.screen_height == 0) || (scfg.screen_height > hval)) scfg.screen_height = hval;

		if (scfg.offset_x > wval) scfg.offset_x = 0;
		if (scfg.offset_y > hval) scfg.offset_y = 0;

		if ((scfg.offset_x + scfg.screen_width) > wval) scfg.offset_x = wval - scfg.screen_width;
		if ((scfg.offset_y + scfg.screen_height) > hval) scfg.offset_y = hval - scfg.screen_height;

		scfg.full_width = props.c_width;
		scfg.full_height = props.c_height;

	}		
	

	if (CodecSet * cd_set = reinterpret_cast<CodecSet *>(en_queue.Acquire())) {
		for (unsigned int i(0);i<eq_top;i++) {
			if (!cd_set[i].data_obj) {
				result = i;
				break;
			}
		}

		if (result != -1) {
			cd_set[result].data_obj = tnucl;
			cd_set[result].pic_cfg = scfg;
			cd_set[result].pic_cfg.cmp_count = 4;
			cd_set[result].pic_cfg.i_id = imid;
			
			cd_set[result].name_obj = fname_obj;

			System::LockedInc_32(&eq_runner);
			result = 0;
		}

		en_queue.Release();
	

		if (eq_runner >= eq_top) {
			if (en_queue.Expand(encoding_queue_capacity*sizeof(CodecSet)) != -1) {
				result = 0;
				eq_top += encoding_queue_capacity;
			}
		}
	}

	return result;
}


UI_64 Pics::Image::Encode(Control::Nucleus & data_obj, const SFSco::IOpenGL::ScreenSet & scfg, SFSco::Object & fname_obj) {
	UI_64 result(-1), xlength(0);

	if (CodecSet * cd_set = reinterpret_cast<CodecSet *>(en_queue.Acquire())) {
		for (unsigned int i(0);i<eq_top;i++) {
			if (!cd_set[i].data_obj) {
				result = i;
				break;
			}
		}

		if (result != -1) {
			cd_set[result].data_obj = data_obj;
			cd_set[result].pic_cfg = scfg;
			cd_set[result].pic_cfg.i_id = 0;
			cd_set[result].name_obj = fname_obj;

			System::LockedInc_32(&eq_runner);
			result = 0;
		}

		en_queue.Release();

		if (eq_runner >= eq_top) {
			if (en_queue.Expand(encoding_queue_capacity*sizeof(CodecSet)) != -1) {
				result = 0;
				eq_top += encoding_queue_capacity;
			}
		}
	}

	return result;
}



DWORD __stdcall Pics::Image::EncoderLoop(void * context) {
	DWORD result(0), r_count(0);
	I_64 pool_index[2*MAX_POOL_COUNT];

	UI_64 iid(-1);
	unsigned int signature[8], iw(0), ih(0), cw(0), ch(0), xtype(SFSco::ImageType::JPG), flipper(0), hval(0), cto_val(0);
	void * src_ptr(0);
	HANDLE f_out(INVALID_HANDLE_VALUE);
	Pack::Deflate deflator;
	
	SFSco::Object npi_obj, temp_obj;

	Memory::Chain::RegisterThread(pool_index);

	if ((deflator.Reset() != -1) && (temp_obj.New(0, temp_eobj_size, SFSco::large_mem_mgr) != -1))
	if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {
		for (;bend_of_line;) {
			WaitForSingleObject(ebreaker, 67);

			if (CodecSet * ccfg = reinterpret_cast<CodecSet *>(en_queue.Acquire())) {
				for (unsigned int ii(0);eq_runner;ii++) {
					ii %= eq_top;

					r_count = GetTickCount();
					if ((ccfg[ii].data_obj) || (ccfg[ii].pic_cfg.i_id != -1)) {
						switch (ccfg[ii].pic_cfg.i_type) {
							case SFSco::ImageType::BMP: case SFSco::ImageType::GIF: case SFSco::ImageType::ICO: case SFSco::ImageType::PNG: case SFSco::ImageType::SFI:
								xtype = ccfg[ii].pic_cfg.i_type;
							break;
							default:
								xtype = SFSco::ImageType::JPG;
						}
						
						iid = -1;

						if (ccfg[ii].pic_cfg.offset_x == -1) {
							ccfg[ii].pic_cfg.offset_x = 0;
							ccfg[ii].pic_cfg.full_width = ccfg[ii].pic_cfg.screen_width;
						}

						if (ccfg[ii].pic_cfg.offset_y == -1) {
							flipper = 1;
							ccfg[ii].pic_cfg.offset_y = 0;
							ccfg[ii].pic_cfg.full_height = ccfg[ii].pic_cfg.screen_height;
						}


						iw = ccfg[ii].pic_cfg.screen_width; ih = ccfg[ii].pic_cfg.screen_height;
						cw = (iw + (1<<size_paragraph) - 1) >> size_paragraph; cw <<= size_paragraph;
						ch = (ih + (1<<size_paragraph) - 1) >> size_paragraph; ch <<= size_paragraph;
						
						hval = ccfg[ii].pic_cfg.i_flags;
						cto_val = (ccfg[ii].pic_cfg.i_flags & SFSco::IOpenGL::ScreenSet::FlagDataCT);

						en_queue.Release();
						ccfg = 0;
						
						if ((hval & SFSco::IOpenGL::ScreenSet::FlagDataDestroy) == 0) {
							result = npi_obj.New(image_rstr_tid, cw*ch*4, SFSco::large_mem_mgr);
						} else {
							result = -2;
						}

						if (result != -1) {						

							if (ccfg = reinterpret_cast<CodecSet *>(en_queue.Acquire())) {
								
								if (unsigned int * pipo = reinterpret_cast<unsigned int *>(npi_obj.Acquire())) {
									
									if (ccfg[ii].data_obj.Acquire(&src_ptr)) {
									
										Image_FromScreen(pipo, src_ptr, ccfg[ii].pic_cfg, flipper);
	
										ccfg[ii].data_obj.Release(src_ptr);
									} else {
										if (Properties props = pi_list[ccfg[ii].pic_cfg.i_id]) {
											if (unsigned int * src_img = reinterpret_cast<unsigned int *>(props.im_obj.Acquire())) {
														
												Image_FromScreen(pipo, src_img, ccfg[ii].pic_cfg, 0);

												props.im_obj.Release();

												if (props.norgba) {
													if (props.precision == 1) {
														if (props.norgba == YCbCr) Image_YCC2RGB_1(reinterpret_cast<unsigned char *>(pipo), props.c_width, props.height);

													} else {
														if (props.norgba == YCbCr) Image_YCC2RGB_2(reinterpret_cast<unsigned short *>(pipo), props.c_width, props.height);
														
													}
												}	
					
											} else {
												result = 0;
											}

										} else {
											result = 0;
										}

									}

									npi_obj.Release();
									ccfg[ii].data_obj.ResetState();
								} else {
									if (ccfg[ii].pic_cfg.i_flags & SFSco::IOpenGL::ScreenSet::FlagDataDestroy) npi_obj = ccfg[ii].data_obj;
								}

								if (result) {
									src_ptr = 0;									
									if (char * cpipo = reinterpret_cast<char *>(npi_obj.Acquire())) {
										SFSco::sha256(signature, cpipo, (ccfg[ii].pic_cfg.full_width+ccfg[ii].pic_cfg.screen_height)*ccfg[ii].pic_cfg.cmp_count, 1);
										npi_obj.Release();
									}

									
									if ((ccfg[ii].pic_cfg.i_flags & SFSco::IOpenGL::ScreenSet::FlagDstSpecial) == 0) {										
										if (wchar_t * dir_ptr = reinterpret_cast<wchar_t *>(ss_directory.Acquire())) {
											result = dir_ptr[0];

											if (wchar_t * fnm_ptr = reinterpret_cast<wchar_t *>(ccfg[ii].name_obj.Acquire())) {
												if (fnm_ptr[0] > 256) fnm_ptr[0] = 256;
												
												System::MemoryCopy(dir_ptr + result, fnm_ptr+1, fnm_ptr[0]);
												
												result += fnm_ptr[0];

												ccfg[ii].name_obj.Release();												
											} else {
												reinterpret_cast<UI_64 *>(signature + 1)[0] ^= System::GetTime();

												Strings::StringToHexString_SSE3(dir_ptr+result, signature + 1, 8, 1);
												result += 16;
											}

											dir_ptr[result++] = L'.';
											switch (xtype) {
												case SFSco::ImageType::BMP:
													dir_ptr[result++] = L'b';
													dir_ptr[result++] = L'm';
													dir_ptr[result++] = L'p';
												break;
												case SFSco::ImageType::GIF:
													dir_ptr[result++] = L'g';
													dir_ptr[result++] = L'i';
													dir_ptr[result++] = L'f';
												break;
												case SFSco::ImageType::ICO:
													dir_ptr[result++] = L'i';
													dir_ptr[result++] = L'c';
													dir_ptr[result++] = L'o';
												break;
												case SFSco::ImageType::JPG:
													dir_ptr[result++] = L'j';
													dir_ptr[result++] = L'p';
													dir_ptr[result++] = L'g';
												break;
												case SFSco::ImageType::PNG:
													dir_ptr[result++] = L'p';
													dir_ptr[result++] = L'n';
													dir_ptr[result++] = L'g';
												break;
												case SFSco::ImageType::SFI:
													dir_ptr[result++] = L's';
													dir_ptr[result++] = L'f';
													dir_ptr[result++] = L'i';
												break;
											}

											dir_ptr[result] = 0;

											f_out = CreateFile(dir_ptr + 1, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

											ss_directory.Release();
										}
									} else {
										if (wchar_t * fnm_ptr = reinterpret_cast<wchar_t *>(ccfg[ii].name_obj.Acquire())) {
											f_out = CreateFile(fnm_ptr + 1, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

											ccfg[ii].name_obj.Release();
										}
									}
								}
								
								ccfg[ii].name_obj.Destroy();
								
								hval = ccfg[ii].pic_cfg.q_factor;

								en_queue.Release();
								ccfg = 0;
							}


							if (f_out != INVALID_HANDLE_VALUE) {
								Properties props;

								props.im_obj = npi_obj;

								for (unsigned int i(0);i<8;i++) props.signature[i] = signature[i];

								props.width = iw;
								props.height = ih;
								props.c_width = cw;
								props.c_height = ch;

								iid = pi_list.Add(props);


								if (iid != -1)
								switch (xtype) {
									case SFSco::ImageType::BMP:
										if (hval > 60) hval = 32;
										else {
											if (hval > 30) hval = 8;
											else {
												if (hval > 1) hval = 4;
												else hval = 1;
											}

											hval |= 0x0100;
										}

										Bitmap::Save(iid, f_out, hval, temp_obj); // 0x0002
									break;
									case SFSco::ImageType::GIF:

									break;
									case SFSco::ImageType::ICO:

									break;
									case SFSco::ImageType::JPG:
										JPEG::Save(iid, f_out, hval); // 20
									break;
									case SFSco::ImageType::PNG:
										PNG::Save(iid, f_out, deflator);
									break;

								}
								
								Free(iid);

								CloseHandle(f_out);
							} else {
								npi_obj.Destroy();
							}

							f_out = INVALID_HANDLE_VALUE;
						}
											
						npi_obj.Blank();
						System::LockedDec_32(&eq_runner);

					}

					if (!ccfg) ccfg = reinterpret_cast<CodecSet *>(en_queue.Acquire());
					ccfg[ii].data_obj.Blank();

					r_count = GetTickCount() - r_count;
				}

				en_queue.Release();
			}
		}

		CloseHandle(ebreaker);
	} else {
		result = GetLastError();
	}

	Memory::Chain::UnregisterThread();

	return result;
}




UI_64 Pics::Image::EnablePage(OpenGL::DecodeRecord & source_record) {
	source_record._reserved[6] = 0;

	if (source_record.program != -1) {
		if (Properties i_props = pi_list[source_record.program]) {

			source_record.width = i_props.width;
			source_record.height = i_props.height;
			source_record.c_width = i_props.c_width;
			source_record.c_height = i_props.c_height;

			if (i_props.norgba == YCbCr) {
				source_record._reserved[5] = 6;
			} else {
				source_record._reserved[5] = 0;
			}

			if (i_props.precision > 1) source_record.precision = 0x10000000;
			else source_record.precision = 0;

			if (i_props.interval) {
				source_record.flags |= OpenGL::DecodeRecord::FlagAnimated;
				source_record.start_time = 0;
				source_record.target_time = i_props.interval;
				source_record._reserved[0] = i_props.next_index;

			} else {
				source_record.start_time = 0;
				source_record.target_time = 0;
				source_record._reserved[0] = -1;
			}

			source_record._reserved[6] = i_props.c_width;
			source_record._reserved[6] *= i_props.c_height;

			if (i_props.precision != 1) source_record._reserved[6] <<= 1;					

			if (OpenGL::Core::SetDecodeView(source_record)) { // change element_obj to corresponding icon element
				source_record.flags |= OpenGL::DecodeRecord::FlagRefresh;
				AddDREntry(source_record);
			
			} else {
				source_record.flags |= OpenGL::DecodeRecord::FlagRefresh;
				UpdateDREntry(source_record);
			}
		
		}
	}

	return source_record._reserved[6];
}

UI_64 Pics::Image::RefreshPage(OpenGL::DecodeRecord & source_record) {
	OpenGL::Element pic_el(source_record.element_obj);
	source_record._reserved[6] = 0;

	if (source_record.program != -1) {
		if (Properties i_props = pi_list[source_record.program]) {

			source_record.width = i_props.width;
			source_record.height = i_props.height;
			source_record.c_width = i_props.c_width;
			source_record.c_height = i_props.c_height;

			if (i_props.norgba == YCbCr) {
				source_record._reserved[5] = 6;
			} else {
				source_record._reserved[5] = 0;
			}

			if (i_props.precision > 1) source_record.precision = 0x10000000;
			else source_record.precision = 0;

			if (i_props.interval) {
				source_record.flags |= OpenGL::DecodeRecord::FlagAnimated;
			} else {
				source_record._reserved[0] = -1;
			}

			source_record._reserved[6] = i_props.c_width;
			source_record._reserved[6] *= i_props.c_height;

			if (i_props.precision != 1) source_record._reserved[6] <<= 1;					

			source_record.flags |= OpenGL::DecodeRecord::FlagRefresh;

			source_record.element_obj.Up().Up();
			if (OpenGL::Core::SetDecodeView(source_record) == 0) {
				source_record.flags &= (~OpenGL::DecodeRecord::FlagSize);
				source_record.element_obj = pic_el;

				UpdateDREntry(source_record);
			}		
		}
	}

	return source_record._reserved[6];
}
