/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\Image.h"

#include "System\SysUtils.h"

#include "OpenGL\Core\OGL_15.h"

#pragma warning(disable:4244)


UI_64 Pics::Image::AddDREntry(OpenGL::DecodeRecord & new_rec) {
	UI_64 result(-1);

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].program == -1) {
				result = i;
				break;
			}
		}

		render_queue.Release();
	}

	if (result == -1) {
		if (render_runner >= render_top) {			
			if (render_queue.Expand(render_queue_size*sizeof(OpenGL::DecodeRecord)) != -1) {
				render_top += render_queue_size;
			}
		}

		if (render_runner < render_top) result = render_runner++;
	}

	if (result != -1) {		
		if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
			System::MemoryCopy_SSE3(de_rec + result, &new_rec, sizeof(OpenGL::DecodeRecord));

			render_queue.Release();
		}
	}

	return result;
}

UI_64 Pics::Image::UpdateDREntry(OpenGL::DecodeRecord & new_rec) {
	UI_64 result(-1);

	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].element_obj == new_rec.element_obj) {

				de_rec[i].width = new_rec.width;
				de_rec[i].height = new_rec.height;
				de_rec[i].c_width = new_rec.c_width;
				de_rec[i].c_height = new_rec.c_height;

				de_rec[i].pic_id = new_rec.pic_id;

				de_rec[i].start_time = new_rec.start_time;
				de_rec[i].target_time = new_rec.target_time;
				de_rec[i]._reserved[0] = new_rec._reserved[0];
				de_rec[i]._reserved[4] = new_rec._reserved[4];
				de_rec[i]._reserved[5] = new_rec._reserved[5];
				de_rec[i]._reserved[6] = new_rec._reserved[6];

				de_rec[i].flags = new_rec.flags & (OpenGL::DecodeRecord::RecordFlags | OpenGL::DecodeRecord::FlagRefresh);

				break;
			}
		}

		render_queue.Release();
	}

	return result;
}


UI_64 Pics::Image::RemoveDREntry(UI_64 s_id) {
	if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
		for (unsigned int i(0);i<render_runner;i++) {
			if (de_rec[i].program == s_id) {
				Free(de_rec[i].program);

				de_rec[i].program = -1;
			}
		}

		render_queue.Release();
	}

	return 0;
}



UI_64 Pics::Image::TexUpdate(OpenGL::Core * core_ptr) {
	UI_64 result(0), rup(0);
	__int64 d_val(0);
	unsigned int si_count(render_runner), i_val(0);
	Control::Nucleus ime_buf;
	
	OpenGL::DecodeRecord current_record;
	Properties i_props, p_props, x_props;
	
	OpenGL::ActiveTexture(GL_TEXTURE0 + OpenGL::Core::image_unit);

	for (unsigned int si(0);si<si_count;si++) {
		current_record.program = -1;

		if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
			if (((de_rec[si].flags & OpenGL::DecodeRecord::FlagProgram) == 0) && (de_rec[si].program != -1)) {
				System::MemoryCopy_SSE3(&current_record, de_rec + si, sizeof(OpenGL::DecodeRecord));
			}
														
			render_queue.Release();
		}

		if ((current_record.program != -1) && (current_record._reserved[6])) {
			if ((current_record.flags & OpenGL::DecodeRecord::FlagAnimated) && (current_record.start_time)) { // animated images only
				d_val = System::GetUCount(current_record.start_time, current_record.target_time);

				if (d_val < 25) {
					if ((p_props = pi_list[current_record.program]) && (i_props = pi_list[current_record._reserved[0]]) && (x_props = pi_list[p_props.prev_index])) {
														
						current_record.target_time += i_props.interval;

						if (((i_props.height + i_props.y_location) <= p_props.height) && (i_props.width <= p_props.width))
						if (unsigned int * pipo = reinterpret_cast<unsigned int *>(x_props.im_obj.Acquire())) {

							switch (i_props.c_reserved) {
								case 3:  // first
									if (unsigned int * first_frame = reinterpret_cast<unsigned int *>(p_props.im_obj.Acquire())) {
										System::MemoryCopy_SSE3(pipo, first_frame, current_record._reserved[6]*sizeof(unsigned int));
										p_props.im_obj.Release();
									}
								break;
								case 2: // bg color
									System::MemoryFill_SSE3(pipo, current_record._reserved[6]*sizeof(unsigned int), 0, 0);

								break;
							}

							if (unsigned int * second_frame = reinterpret_cast<unsigned int *>(i_props.im_obj.Acquire())) {
								reinterpret_cast<unsigned int *>(current_record._reserved + 1)[0] = i_props.c_width;
								reinterpret_cast<unsigned int *>(current_record._reserved + 1)[1] = i_props.height;
								reinterpret_cast<unsigned int *>(current_record._reserved + 1)[2] = i_props.x_location;
								reinterpret_cast<unsigned int *>(current_record._reserved + 1)[3] = i_props.y_location;

								switch (reinterpret_cast<unsigned int *>(current_record._reserved + 1)[2] & 3) {
									case 0:
										Image_AddPicture0(pipo, second_frame, p_props.c_width, reinterpret_cast<unsigned int *>(current_record._reserved + 1));
									break;
									case 1:
										Image_AddPicture1(pipo, second_frame, p_props.c_width, reinterpret_cast<unsigned int *>(current_record._reserved + 1));
									break;
									case 2:
										Image_AddPicture2(pipo, second_frame, p_props.c_width, reinterpret_cast<unsigned int *>(current_record._reserved + 1));
									break;
									case 3:
										Image_AddPicture3(pipo, second_frame, p_props.c_width, reinterpret_cast<unsigned int *>(current_record._reserved + 1));
									break;
								}

								i_props.im_obj.Release();
							}


							x_props.im_obj.Release();
						}

						current_record.flags &= OpenGL::DecodeRecord::RecordFlags;
																						
						rup = core_ptr->SetDecodeFrame(current_record, x_props.im_obj, current_record._reserved[5]);
							
						current_record.flags &= OpenGL::DecodeRecord::RecordFlags;

						if (rup & 1) {
							current_record.flags |= OpenGL::DecodeRecord::FlagSize;
							Decode(current_record);

							current_record.flags ^= OpenGL::DecodeRecord::FlagSize;
						}


						current_record._reserved[0] = i_props.next_index;
						if (current_record._reserved[0] == -1) current_record._reserved[0] = current_record.program;

						if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
							if (rup & 1) {									
								de_rec[si].flags |= current_record.flags;
								de_rec[si].precision = current_record.precision;

								de_rec[si].pic_box[0] = current_record.pic_box[0];
								de_rec[si].pic_box[1] = current_record.pic_box[1];
								de_rec[si].pic_box[2] = current_record.pic_box[2];
								de_rec[si].pic_box[3] = current_record.pic_box[3];

								rup ^= 1;
							}

							de_rec[si].target_time = current_record.target_time;
							de_rec[si]._reserved[0] = current_record._reserved[0];

							render_queue.Release();
						}							
					}
				}
			} else {
				if (current_record.flags & OpenGL::DecodeRecord::FlagRefresh) {
					current_record.flags &= ~OpenGL::DecodeRecord::FlagRefresh;
					if (p_props = pi_list[current_record.program]) {
						rup |= core_ptr->SetDecodeFrame(current_record, p_props.im_obj, current_record._reserved[5]);
						
						current_record.flags &= OpenGL::DecodeRecord::RecordFlags;

						if (rup & 1) {
							current_record.flags |= OpenGL::DecodeRecord::FlagSize;
							Decode(current_record);

							current_record.flags ^= OpenGL::DecodeRecord::FlagSize;
						}

					
						

						if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
							if (rup & 1) {
								rup ^= 1;
						
								System::MemoryCopy(de_rec + si, &current_record, sizeof(OpenGL::DecodeRecord));

							} else {

								de_rec[si].flags = current_record.flags;
							}

							render_queue.Release();										
						}
					}
				}
			}
		}									
	}

	return rup;
}

