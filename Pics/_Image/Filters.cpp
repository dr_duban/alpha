/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\Image.h"


void Pics::Image_Dither(unsigned int * pipo, const unsigned int * plt, unsigned int wi, unsigned int hi) {
	int tmpvl[16];
	unsigned int cwi((wi + (1<<Image::size_paragraph) - 1) & (-1 << Image::size_paragraph)), * line_ptr(0), distance(0), min_di(0), min_kk(0), col_count(plt[0]);	

	for (unsigned int ii(0);ii<hi;ii++) {		
		line_ptr = pipo;
		for (unsigned int jj(0);jj<wi;jj++) {
			tmpvl[0] = line_ptr[jj] & 0xFF;
			tmpvl[1] = (line_ptr[jj]>>8) & 0xFF;
			tmpvl[2] = (line_ptr[jj]>>16) & 0xFF;
			tmpvl[3] = (line_ptr[jj]>>24) & 0xFF;

			min_di = 0xFFFFFFFF;
			for (unsigned int kk(0);kk<col_count;kk++) {
				tmpvl[4] = tmpvl[0] - (plt[kk+4] & 0xFF);
				tmpvl[5] = tmpvl[1] - ((plt[kk+4]>>8) & 0xFF);
				tmpvl[6] = tmpvl[2] - ((plt[kk+4]>>16) & 0xFF);
				tmpvl[7] = tmpvl[3] - ((plt[kk+4]>>24) & 0xFF);


				distance = tmpvl[4]*tmpvl[4] + tmpvl[5]*tmpvl[5] + tmpvl[6]*tmpvl[6] + tmpvl[7]*tmpvl[7];

				if (distance < min_di) {
					min_kk = kk;
					min_di = distance;

					tmpvl[8] = tmpvl[4];
					tmpvl[9] = tmpvl[5];
					tmpvl[10] = tmpvl[6];
					tmpvl[11] = tmpvl[7];
				}
			}

			line_ptr[jj] = min_kk;

			if (jj < (wi-1)) {
				tmpvl[4] = line_ptr[jj+1] & 0xFF;
				tmpvl[5] = (line_ptr[jj+1]>>8) & 0xFF;
				tmpvl[6] = (line_ptr[jj+1]>>16) & 0xFF;
				tmpvl[7] = (line_ptr[jj+1]>>24) & 0xFF;				

				tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 3) - tmpvl[8]) >> 4);
				tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 3) - tmpvl[9]) >> 4);
				tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 3) - tmpvl[10]) >> 4);
				tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 3) - tmpvl[11]) >> 4);

				if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
				if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
				if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
				if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

				if (tmpvl[12] < 0) tmpvl[12] = 0;
				if (tmpvl[13] < 0) tmpvl[13] = 0;
				if (tmpvl[14] < 0) tmpvl[14] = 0;
				if (tmpvl[15] < 0) tmpvl[15] = 0;

				tmpvl[12] &= 0xFF;				
				tmpvl[13] &= 0xFF;				
				tmpvl[14] &= 0xFF;				
				tmpvl[15] &= 0xFF;

				line_ptr[jj+1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

			}

			if (ii < (hi-1)) {
				if (jj>0) { // option 3
					tmpvl[4] = line_ptr[jj+cwi-1] & 0xFF;
					tmpvl[5] = (line_ptr[jj+cwi-1]>>8) & 0xFF;
					tmpvl[6] = (line_ptr[jj+cwi-1]>>16) & 0xFF;
					tmpvl[7] = (line_ptr[jj+cwi-1]>>24) & 0xFF;				

					tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 2) - tmpvl[8]) >> 4);
					tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 2) - tmpvl[9]) >> 4);
					tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 2) - tmpvl[10]) >> 4);
					tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 2) - tmpvl[11]) >> 4);

					if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
					if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
					if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
					if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

					if (tmpvl[12] < 0) tmpvl[12] = 0;
					if (tmpvl[13] < 0) tmpvl[13] = 0;
					if (tmpvl[14] < 0) tmpvl[14] = 0;
					if (tmpvl[15] < 0) tmpvl[15] = 0;

					tmpvl[12] &= 0xFF;				
					tmpvl[13] &= 0xFF;				
					tmpvl[14] &= 0xFF;				
					tmpvl[15] &= 0xFF;

					line_ptr[jj+cwi-1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

				}

				// option 51
				tmpvl[4] = line_ptr[jj+cwi] & 0xFF;
				tmpvl[5] = (line_ptr[jj+cwi]>>8) & 0xFF;
				tmpvl[6] = (line_ptr[jj+cwi]>>16) & 0xFF;
				tmpvl[7] = (line_ptr[jj+cwi]>>24) & 0xFF;				

				tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 2) + tmpvl[8]) >> 4);
				tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 2) + tmpvl[9]) >> 4);
				tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 2) + tmpvl[10]) >> 4);
				tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 2) + tmpvl[11]) >> 4);

				if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
				if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
				if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
				if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

				if (tmpvl[12] < 0) tmpvl[12] = 0;
				if (tmpvl[13] < 0) tmpvl[13] = 0;
				if (tmpvl[14] < 0) tmpvl[14] = 0;
				if (tmpvl[15] < 0) tmpvl[15] = 0;

				tmpvl[12] &= 0xFF;				
				tmpvl[13] &= 0xFF;				
				tmpvl[14] &= 0xFF;				
				tmpvl[15] &= 0xFF;

				line_ptr[jj+cwi] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

				if (jj < (wi-1)) {
					tmpvl[4] = line_ptr[jj+cwi+1] & 0xFF;
					tmpvl[5] = (line_ptr[jj+cwi+1]>>8) & 0xFF;
					tmpvl[6] = (line_ptr[jj+cwi+1]>>16) & 0xFF;
					tmpvl[7] = (line_ptr[jj+cwi+1]>>24) & 0xFF;				

					tmpvl[12] = (((tmpvl[4] << 4) + tmpvl[8]) >> 4);
					tmpvl[13] = (((tmpvl[5] << 4) + tmpvl[9]) >> 4);
					tmpvl[14] = (((tmpvl[6] << 4) + tmpvl[10]) >> 4);
					tmpvl[15] = (((tmpvl[7] << 4) + tmpvl[11]) >> 4);

					if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
					if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
					if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
					if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

					if (tmpvl[12] < 0) tmpvl[12] = 0;
					if (tmpvl[13] < 0) tmpvl[13] = 0;
					if (tmpvl[14] < 0) tmpvl[14] = 0;
					if (tmpvl[15] < 0) tmpvl[15] = 0;

					tmpvl[12] &= 0xFF;				
					tmpvl[13] &= 0xFF;				
					tmpvl[14] &= 0xFF;				
					tmpvl[15] &= 0xFF;

					line_ptr[jj+cwi+1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);
				}
			}

		}

		pipo += cwi;
	}

}

void Pics::Image_Dither_I(unsigned int * pipo, const UI_64 * plane_set, unsigned int wi, unsigned int hi) {
	int tmpvl[16];	
	unsigned int cwi((wi + (1<<Image::size_paragraph) - 1) & (-1 << Image::size_paragraph)), * line_ptr(0), distance(0), min_di(0), min_kk(0);	

	unsigned int mask_set[4];

	mask_set[0] = 0x000000FF; mask_set[1] = 0x0000FF00; mask_set[2] = 0x00FF0000; mask_set[3] = 0xFF000000;

	for (unsigned int ii(0);ii<hi;ii++) {		
		line_ptr = pipo;
		for (unsigned int jj(0);jj<wi;jj++) {
			min_kk = 0;

			for (;plane_set[min_kk*16+8];) {
				if ((line_ptr[jj] & mask_set[plane_set[min_kk*16+3]]) < (plane_set[min_kk*16+8] & 0xFFFFFFFF)) {
					// volume 1
					min_kk = plane_set[min_kk*16+12];
				} else {
					// volume 2
					min_kk = plane_set[min_kk*16+13];
				}

			}
		
			
			tmpvl[8] = (line_ptr[jj] & 0xFF) - plane_set[min_kk*16];
			tmpvl[9] = ((line_ptr[jj]>>8) & 0xFF) - plane_set[min_kk*16+1];
			tmpvl[10] = ((line_ptr[jj]>>16) & 0xFF) - plane_set[min_kk*16+2];
			tmpvl[11] = 0;

			line_ptr[jj] = plane_set[min_kk*16+4];

			if (jj < (wi-1)) {
				tmpvl[4] = line_ptr[jj+1] & 0xFF;
				tmpvl[5] = (line_ptr[jj+1]>>8) & 0xFF;
				tmpvl[6] = (line_ptr[jj+1]>>16) & 0xFF;
				tmpvl[7] = (line_ptr[jj+1]>>24) & 0xFF;				

				tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 3) - tmpvl[8]) >> 4);
				tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 3) - tmpvl[9]) >> 4);
				tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 3) - tmpvl[10]) >> 4);
				tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 3) - tmpvl[11]) >> 4);

				if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
				if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
				if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
				if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

				if (tmpvl[12] < 0) tmpvl[12] = 0;
				if (tmpvl[13] < 0) tmpvl[13] = 0;
				if (tmpvl[14] < 0) tmpvl[14] = 0;
				if (tmpvl[15] < 0) tmpvl[15] = 0;

				tmpvl[12] &= 0xFF;				
				tmpvl[13] &= 0xFF;				
				tmpvl[14] &= 0xFF;				
				tmpvl[15] &= 0xFF;

				line_ptr[jj+1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

			}

			if (ii < (hi-1)) {
				if (jj>0) { // option 3
					tmpvl[4] = line_ptr[jj+cwi-1] & 0xFF;
					tmpvl[5] = (line_ptr[jj+cwi-1]>>8) & 0xFF;
					tmpvl[6] = (line_ptr[jj+cwi-1]>>16) & 0xFF;
					tmpvl[7] = (line_ptr[jj+cwi-1]>>24) & 0xFF;				

					tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 2) - tmpvl[8]) >> 4);
					tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 2) - tmpvl[9]) >> 4);
					tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 2) - tmpvl[10]) >> 4);
					tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 2) - tmpvl[11]) >> 4);

					if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
					if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
					if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
					if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

					if (tmpvl[12] < 0) tmpvl[12] = 0;
					if (tmpvl[13] < 0) tmpvl[13] = 0;
					if (tmpvl[14] < 0) tmpvl[14] = 0;
					if (tmpvl[15] < 0) tmpvl[15] = 0;

					tmpvl[12] &= 0xFF;				
					tmpvl[13] &= 0xFF;				
					tmpvl[14] &= 0xFF;				
					tmpvl[15] &= 0xFF;

					line_ptr[jj+cwi-1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

				}

				// option 51
				tmpvl[4] = line_ptr[jj+cwi] & 0xFF;
				tmpvl[5] = (line_ptr[jj+cwi]>>8) & 0xFF;
				tmpvl[6] = (line_ptr[jj+cwi]>>16) & 0xFF;
				tmpvl[7] = (line_ptr[jj+cwi]>>24) & 0xFF;				

				tmpvl[12] = (((tmpvl[4] << 4) + (tmpvl[8] << 2) + tmpvl[8]) >> 4);
				tmpvl[13] = (((tmpvl[5] << 4) + (tmpvl[9] << 2) + tmpvl[9]) >> 4);
				tmpvl[14] = (((tmpvl[6] << 4) + (tmpvl[10] << 2) + tmpvl[10]) >> 4);
				tmpvl[15] = (((tmpvl[7] << 4) + (tmpvl[11] << 2) + tmpvl[11]) >> 4);

				if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
				if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
				if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
				if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

				if (tmpvl[12] < 0) tmpvl[12] = 0;
				if (tmpvl[13] < 0) tmpvl[13] = 0;
				if (tmpvl[14] < 0) tmpvl[14] = 0;
				if (tmpvl[15] < 0) tmpvl[15] = 0;

				tmpvl[12] &= 0xFF;				
				tmpvl[13] &= 0xFF;				
				tmpvl[14] &= 0xFF;				
				tmpvl[15] &= 0xFF;

				line_ptr[jj+cwi] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);

				if (jj < (wi-1)) {
					tmpvl[4] = line_ptr[jj+cwi+1] & 0xFF;
					tmpvl[5] = (line_ptr[jj+cwi+1]>>8) & 0xFF;
					tmpvl[6] = (line_ptr[jj+cwi+1]>>16) & 0xFF;
					tmpvl[7] = (line_ptr[jj+cwi+1]>>24) & 0xFF;				

					tmpvl[12] = (((tmpvl[4] << 4) + tmpvl[8]) >> 4);
					tmpvl[13] = (((tmpvl[5] << 4) + tmpvl[9]) >> 4);
					tmpvl[14] = (((tmpvl[6] << 4) + tmpvl[10]) >> 4);
					tmpvl[15] = (((tmpvl[7] << 4) + tmpvl[11]) >> 4);

					if (tmpvl[12] > 0x00FF) tmpvl[12] = 0x00FF;
					if (tmpvl[13] > 0x00FF) tmpvl[13] = 0x00FF;
					if (tmpvl[14] > 0x00FF) tmpvl[14] = 0x00FF;
					if (tmpvl[15] > 0x00FF) tmpvl[15] = 0x00FF;

					if (tmpvl[12] < 0) tmpvl[12] = 0;
					if (tmpvl[13] < 0) tmpvl[13] = 0;
					if (tmpvl[14] < 0) tmpvl[14] = 0;
					if (tmpvl[15] < 0) tmpvl[15] = 0;

					tmpvl[12] &= 0xFF;				
					tmpvl[13] &= 0xFF;				
					tmpvl[14] &= 0xFF;				
					tmpvl[15] &= 0xFF;

					line_ptr[jj+cwi+1] = tmpvl[12] | (tmpvl[13] << 8) | (tmpvl[14] << 16) | (tmpvl[15] << 24);
				}
			}

		}

		pipo += cwi;
	}

}


void Pics::Image_Nearest(unsigned int * pipo, const unsigned int * plt, unsigned int wi, unsigned int hi) {
	int tmpvl[16];	
	unsigned int cwi((wi + (1 << Image::size_paragraph) - 1) & (-1 << Image::size_paragraph)), * line_ptr(0), distance(0), min_di(0), min_kk(0), col_count(plt[0]);	

	for (unsigned int ii(0);ii<hi;ii++) {		
		line_ptr = pipo;
		for (unsigned int jj(0);jj<wi;jj++) {
			tmpvl[0] = line_ptr[jj] & 0xFF;
			tmpvl[1] = (line_ptr[jj]>>8) & 0xFF;
			tmpvl[2] = (line_ptr[jj]>>16) & 0xFF;
			tmpvl[3] = (line_ptr[jj]>>24) & 0xFF;

			min_di = 0xFFFFFFFF;
			for (unsigned int kk(0);kk<col_count;kk++) {
				tmpvl[4] = tmpvl[0] - (plt[kk+4] & 0xFF);
				tmpvl[5] = tmpvl[1] - ((plt[kk+4]>>8) & 0xFF);
				tmpvl[6] = tmpvl[2] - ((plt[kk+4]>>16) & 0xFF);
				tmpvl[7] = tmpvl[3] - ((plt[kk+4]>>24) & 0xFF);


				distance = tmpvl[4]*tmpvl[4] + tmpvl[5]*tmpvl[5] + tmpvl[6]*tmpvl[6] + tmpvl[7]*tmpvl[7];

				if (distance < min_di) {
					min_kk = kk;
					min_di = distance;

					tmpvl[8] = tmpvl[4];
					tmpvl[9] = tmpvl[5];
					tmpvl[10] = tmpvl[6];
					tmpvl[11] = tmpvl[7];
				}
			}

			line_ptr[jj] = min_kk;
		}

		pipo += cwi;
	}

}

void Pics::Image_Nearest_I(unsigned int * pipo, UI_64 * plane_set, unsigned int wi, unsigned int hi) {
	unsigned int cwi((wi + (1 << Image::size_paragraph) - 1) & (-1 << Image::size_paragraph)), * line_ptr(0), min_di(0), min_kk(0);
	unsigned int mask_set[4];

	mask_set[0] = 0x000000FF; mask_set[1] = 0x0000FF00; mask_set[2] = 0x00FF0000; mask_set[3] = 0xFF000000;

	for (unsigned int ii(0);ii<hi;ii++) {		
		line_ptr = pipo;
		for (unsigned int jj(0);jj<wi;jj++) {
			min_kk = 0;

			for (;plane_set[min_kk*16+8];) {
				if ((line_ptr[jj] & mask_set[plane_set[min_kk*16+3]]) < (plane_set[min_kk*16+8] & 0xFFFFFFFF)) {
					// volume 1
					min_kk = plane_set[min_kk*16+12];
				} else {
					// volume 2
					min_kk = plane_set[min_kk*16+13];
				}

			}

			line_ptr[jj] = plane_set[min_kk*16+4];		
		}

		pipo += cwi;
	}

}

UI_64 Pics::Image::Median(UI_64 * cols, const unsigned int * pipo, unsigned int wi, unsigned int hi, unsigned int col_count) {
	UI_64 * plane_set(cols + (histogram_size>>3)), mxd(0), split_index(0), top_index(1), current_plane(0), mxi(0), up_index(0), colcopy(col_count);
	unsigned int * colarr(0);

/*
	plane set layout

	0..3: first moment
	4..7: second moment
	
	8: plane
	9: plane
	10: top_left_front
	11: bottom_right_back

	12: left_index
	13: right_index
	14: next up
	15: next down
*/

	Image_Build3DHistogram(cols, pipo, wi, hi);

	
	plane_set[0] = cols[327670]/cols[327670+8];
	plane_set[1] = cols[327670+1]/cols[327670+8];
	plane_set[2] = cols[327670+2]/cols[327670+8];
	plane_set[3] = 0;

	plane_set[4] = (cols[327670+4] - cols[327670]*plane_set[0])/cols[327670+8];
	plane_set[5] = (cols[327670+5] - cols[327670+1]*plane_set[1])/cols[327670+8];
	plane_set[6] = (cols[327670+6] - cols[327670+2]*plane_set[2])/cols[327670+8];
	plane_set[7] = 0;

// define first plane
	mxd = plane_set[4]; mxi = 0;
	if (plane_set[5] > mxd) {
		mxd = plane_set[5];
		mxi = 1;
	}
	if (plane_set[6] > mxd){
		mxd = plane_set[6];
		mxi = 2;
	}

	plane_set[3] = mxi;
	plane_set[7] = mxd;	
		

	plane_set[10] = 0;
	plane_set[11] = 0x0000010001000100;
	
	plane_set[12] = -1;
	plane_set[13] = -1;
	plane_set[14] = -1;
	plane_set[15] = -1;

// split op


	for (;--col_count;) {
	// set plane for current_split_index
		if (plane_set[split_index*16+7] < 128) break;

		mxi = plane_set[split_index*16+3];

		plane_set[split_index*16+8] = ((plane_set[split_index*16+mxi]+7) & 0x0000FFF8) << (mxi*8);
		plane_set[split_index*16+8] |= (plane_set[split_index*16+8] << 32);
		plane_set[split_index*16+9] = plane_set[split_index*16+8];

		Image_ConfigureVolumes(plane_set, split_index, top_index, cols);

	// split volume out
		plane_set[split_index*16+12] = top_index;
		plane_set[split_index*16+13] = top_index+1;
			

		if (plane_set[split_index*16+15] != -1) {
			up_index = split_index;

			split_index = plane_set[split_index*16+15];
			plane_set[split_index*16+14] = -1;
			
			plane_set[up_index*16+15] = -1;
		}

		mxd = -1;
		for (mxi = split_index;;mxi = plane_set[mxi*16+15]) {
			if (mxi == -1) break;
			
			mxd = mxi;

			if (plane_set[top_index*16+7] > plane_set[mxi*16+7]) {
				up_index = plane_set[mxi*16+14];
				plane_set[mxi*16+14] = top_index;
				
				plane_set[top_index*16+14] = up_index;
				plane_set[top_index*16+15] = mxi;

				if (up_index != -1) {
					plane_set[up_index*16+15] = top_index;

				} else {
					split_index = top_index;
				}
				
				break;
			}
		}

		if (mxi == -1) {
			if (top_index > 1) {
				plane_set[mxd*16+15] = top_index;
				plane_set[top_index*16+14] = mxd;
			} else {
				plane_set[top_index*16+14] = -1;
				split_index = top_index;
			}

			plane_set[top_index*16+15] = -1;
		}

		top_index++;

		mxd = -1;
		for (mxi = split_index;;mxi = plane_set[mxi*16+15]) {
			if (mxi == -1) break;
			
			mxd = mxi;

			if (plane_set[top_index*16+7] > plane_set[mxi*16+7]) {
				up_index = plane_set[mxi*16+14];
				plane_set[mxi*16+14] = top_index;
				
				plane_set[top_index*16+14] = up_index;
				plane_set[top_index*16+15] = mxi;

				if (up_index != -1) {
					plane_set[up_index*16+15] = top_index;

				} else {
					split_index = top_index;
				}
				
				break;
			}
		}

		if (mxi == -1) {
			plane_set[mxd*16+15] = top_index;
			plane_set[top_index*16+14] = mxd;

			plane_set[top_index*16+15] = -1;
		}

		top_index++;

	}

	colarr = reinterpret_cast<unsigned int *>(plane_set + top_index*16);
	colarr[0] = colcopy;
	colarr[1] = split_index;
	colarr += 4;
	mxd = 0;
	col_count = colcopy;

	for (mxi = split_index;colcopy;mxi = plane_set[mxi*16+15]) {
		if (mxi == -1) break;
		
		colarr[0] = plane_set[mxi*16] | (plane_set[mxi*16+1]<<8) | (plane_set[mxi*16+2]<<16) | 0xFF000000;
		plane_set[mxi*16+4] = mxd++; plane_set[mxi*16+5] = colarr[0];
		colarr[col_count] = plane_set[mxi*16+6];

		colarr++;
		colcopy--;
	}

	for (UI_64 i(0);i<colcopy;i++) {
		colarr[0] = 0xFF000000;
		colarr[col_count] = 0;
		colarr++;
	}

	return (top_index<<7);
}


/*
void Pics::Image::SortColors_I(unsigned int * cols, UI_64 * plane_set) {
	unsigned int col_count(cols[0]), max_co(0), spliti(cols[1]);

	cols += 4+col_count;

	for (unsigned int j(0);j<col_count;j++) {
		max_co = 0;
		cols[col_count + j] = j;

		for (unsigned int i(0);i<col_count;i++) {
			if (cols[i] > max_co) {
				max_co = cols[i];
				cols[col_count + j] = i;
			}
		}
		
		cols[cols[col_count + j]] = 0;
	}

	cols -= col_count;

	for (unsigned int i(0);i<col_count;i++) {
		cols[col_count+i] = cols[cols[2*col_count+i]];
	}
	
	max_co = col_count;
	for (;max_co;spliti = plane_set[spliti*16+15]) {
		if (spliti == -1) break;
				
		for (unsigned int i(0);i<col_count;i++) {
			if (cols[2*col_count+i] == plane_set[spliti*16+4]) {
				plane_set[spliti*16+4] = i;
				break;
			}
		}

		max_co--;
	}

}

void Pics::Image::SortColors_II(unsigned int * cols, UI_64 * plane_set) {
	unsigned int col_count(cols[0]), min_co(0xFFFFFFFF), spliti(cols[1]);

	cols += 4+col_count;

	for (unsigned int j(0);j<col_count;j++) {
		min_co = 0xFFFFFFFF;
		cols[col_count + j] = j;

		for (unsigned int i(0);i<col_count;i++) {
			if (cols[i] < min_co) {
				min_co = cols[i];
				cols[col_count + j] = i;
			}
		}
		
		cols[cols[col_count + j]] = 0xFFFFFFFF;
	}

	cols -= col_count;

	for (unsigned int i(0);i<col_count;i++) {
		cols[col_count+i] = cols[cols[2*col_count+i]];
	}
	
	min_co = col_count;
	for (;min_co;spliti = plane_set[spliti*16+15]) {
		if (spliti == -1) break;
				
		for (unsigned int i(0);i<col_count;i++) {
			if (cols[2*col_count+i] == plane_set[spliti*16+4]) {
				plane_set[spliti*16+4] = i;
				break;
			}
		}

		min_co--;
	}

}
*/



