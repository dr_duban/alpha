
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



; EXTERN	?size_paragraph@Image@Pics@@1IB:DWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16
one_mask	DWORD	0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh
tri_mask	DWORD	0FF000000h, 0FF000000h, 0FF000000h, 0FF000000h	
tr2_mask	DWORD	0, 0FFFF0000h, 0, 0FFFF0000h
bw2_mask	DWORD	0FFFFFFFFh, 0, 0FFFFFFFFh, 0
bw1_mask	DWORD	00000FFFFh, 00000FFFFh, 00000FFFFh, 00000FFFFh

cb601_factor	DWORD	0FFEA0000h, 000000071h, 0FFEA0000h, 000000071h
cr601_factor	DWORD	0FFD2005Ah, 000000000h, 0FFD2005Ah, 000000000h
y601_offset	DWORD	02200D300h, 00000C780h, 02200D300h, 00000C780h


cb709_factor	DWORD	0FFF40000h, 000000077h, 0FFF40000h, 000000077h
cr709_factor	DWORD	0FFE20064h, 000000000h, 0FFE20064h, 000000000h
y709_offset		DWORD	01500CE00h, 00000C480h, 01500CE00h, 00000C480h

float_8192		DWORD	46000000h, 46000000h, 46000000h, 46000000h
dword_x8000		DWORD	00008000h, 00008000h, 00008000h, 00008000h
word_8000		DWORD	80008000h, 80008000h, 80008000h, 80008000h


yuy_shift	DWORD	000800010h, 000800010h, 000800010h, 000800010h
yuv_shift	DWORD	000800010h, 000000080h, 000800010h, 000000080h,

yuv_r		DWORD	00000012Ah, 000000199h, 00000012Ah, 000000199h
yuv_g		DWORD	0FF9C012Ah, 00000FF30h, 0FF9C012Ah, 00000FF30h
yuv_b		DWORD	00204012Ah, 000000000h, 00204012Ah, 000000000h

yuy_r0		DWORD	00000012Ah, 001990000h, 00000012Ah, 001990000h
yuy_g0		DWORD	0FF9C012Ah, 0FF300000h, 0FF9C012Ah, 0FF300000h
yuy_b0		DWORD	00204012Ah, 000000000h, 00204012Ah, 000000000h

yuy_r1		DWORD	000000000h, 00199012Ah, 000000000h, 00199012Ah
yuy_g1		DWORD	0FF9C0000h, 0FF30012Ah, 0FF9C0000h, 0FF30012Ah
yuy_b1		DWORD	002040000h, 00000012Ah, 002040000h, 00000012Ah

alpha		DWORD	0FF000000h, 0FF000000h, 0FF000000h, 0FF000000h

rgb_shuf0	DWORD	000000102h, 000030405h, 000060708h, 000090A0Bh
rgb_shuf1	DWORD	000040506h, 000070809h, 0000A0B0Ch, 0000D0E0Fh

ycc_shuf	DWORD	0C080400h, 0D090501h, 0E0A0602h, 0
cc_offset	DWORD	00800080h, 00800080h, 00800080h, 00800080h


rgb_y	WORD	0,		8192,	0,		8192,	0,		8192,	0,		8192
y_part	WORD	10773,	10773,	10773,	10773,	10773,	10773,	10773,	10773
cc_part	WORD	9070,	9070,	9070,	9070,	9070,	9070,	9070,	9070

r0_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
g0_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
b0_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
x0_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r1_cc	WORD	0,		12902,	0,		12902,	0,		12902,	0,		12902
g1_cc	WORD	-1532,	-3834,	-1532,	-3834,	-1532,	-3834,	-1532,	-3834
b1_cc	WORD	15204,	0,		15204,	0,		15204,	0,		15204,	0
x1_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r2_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
g2_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
b2_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
x2_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r3_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
g3_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
b3_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0
x3_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r4_cc	WORD	0,		11469,	0,		11469,	0,		11469,	0,		11469
g4_cc	WORD	-2719,	-5832, -2719,	-5832, -2719,	-5832, -2719,	-5832
b4_cc	WORD	14582,	0,		14582,	0,		14582,	0,		14582,	0
x4_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r5_cc	WORD	0,		11485,	0,		11485,	0,		11485,	0,		11485
g5_cc	WORD	-2819,	-5850,	-2819,	-5850,	-2819,	-5850,	-2819,	-5850
b5_cc	WORD	14516,	0,		14516,	0,		14516,	0,		14516,	0
x5_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r6_cc	WORD	0,		11485,	0,		11485,	0,		11485,	0,		11485
g6_cc	WORD	-2819,	-5850,	-2819,	-5850,	-2819,	-5850,	-2819,	-5850
b6_cc	WORD	14516,	0,		14516,	0,		14516,	0,		14516,	0
x6_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r7_cc	WORD	0,		12911,	0,		12911,	0,		12911,	0,		12911
g7_cc	WORD	-1856,	-3904,	-1856,	-3904,	-1856,	-3904,	-1856,	-3904
b7_cc	WORD	14959,	0,		14959,	0,		14959,	0,		14959,	0
x7_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r8_cc	WORD	-8192,	8192,	-8192,	8192,	-8192,	8192,	-8192,	8192
g8_cc	WORD	8192,	0,		8192,	0,		8192,	0,		8192,	0
b8_cc	WORD	-8192,	-8192,	-8192,	-8192,	-8192,	-8192,	-8192,	-8192
x8_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r9_cc	WORD	0,		24160,	0,		24160,	0,		24160,	0,		24160
g9_cc	WORD	-2696,	-4680,	-2696,	-4680,	-2696,	-4680,	-2696,	-4680
b9_cc	WORD	30825,	0,		30825,	0,		30825,	0,		30825,	0
x9_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0

r10_cc	WORD	0,		12080,	0,		12080,	0,		12080,	0,		12080
g10_cc	WORD	-1348,	-9361,	-2696,	-9361,	-2696,	-9361,	-2696,	-9361
b10_cc	WORD	15412,	0,		15412,	0,		15412,	0,		15412,	0
x10_cc	WORD	0, 0, 0, 0,		0, 0, 0, 0




.CODE




ALIGN 16
Image_Convert2RGBA	PROC
	
	MOV r11, rcx ; destination

	MOV rax, rdx			
	SHL rax, 6
	JZ exit


	MOVDQA xmm11, XMMWORD PTR [cc_offset]
	MOVDQA xmm10, XMMWORD PTR [ycc_shuf]
	MOVDQA xmm12, XMMWORD PTR [rgb_y]

	LEA r10, [r0_cc]

	MOVDQA xmm13, [r10 + rax] ; r_cc
	MOVDQA xmm14, [r10 + rax + 16] ; g_cc
	MOVDQA xmm15, [r10 + rax + 32] ; b_cc

	
	
	SHR r8d, 3
	MOV r10d, r8d ; c_width
		

align 16
	c_loop:
				PXOR xmm2, xmm2
				MOVDQA xmm0, [rcx]
				MOVDQA xmm1, [rcx + 16]
		
				PSHUFB xmm0, xmm10
				PSHUFB xmm1, xmm10						

				MOVDQA xmm3, xmm0
				PUNPCKHDQ xmm3, xmm1				
				PUNPCKLDQ xmm0, xmm1
				MOVDQA xmm1, xmm0

				PUNPCKLBW xmm0, xmm2
				PUNPCKHBW xmm1, xmm2
				PUNPCKLBW xmm3, xmm2

				PSUBW xmm1, xmm11
				PSUBW xmm3, xmm11


				MOVDQA xmm2, xmm1
				PUNPCKLWD xmm1, xmm3
				PUNPCKHWD xmm2, xmm3
				
				MOVDQA xmm4, xmm2
				MOVDQA xmm5, xmm2
				MOVDQA xmm6, xmm2

				PMADDWD xmm4, xmm13
				PMADDWD xmm5, xmm14
				PMADDWD xmm6, xmm15

				MOVDQA xmm2, xmm1
				MOVDQA xmm3, xmm1

				PMADDWD xmm1, xmm13
				PMADDWD xmm2, xmm14
				PMADDWD xmm3, xmm15
				
				MOVDQA xmm7, xmm0
				PUNPCKLWD xmm0, xmm0
				PUNPCKHWD xmm7, xmm7

				PMADDWD xmm0, xmm12
				PMADDWD xmm7, xmm12



				PADDD xmm1, xmm0
				PADDD xmm2, xmm0
				PADDD xmm3, xmm0

				PSRAD xmm1, 13
				PSRAD xmm2, 13
				PSRAD xmm3, 13


				PADDD xmm4, xmm7
				PADDD xmm5, xmm7
				PADDD xmm6, xmm7

				PSRAD xmm4, 13
				PSRAD xmm5, 13
				PSRAD xmm6, 13





				PACKSSDW xmm1, xmm4 ; red
				PACKSSDW xmm2, xmm5 ; green
				PACKSSDW xmm3, xmm6 ; blue

				PCMPEQW xmm4, xmm4
				PSRLW xmm4, 4		; alpha

				MOVDQA xmm5, xmm1
				MOVDQA xmm6, xmm3

				PUNPCKLWD xmm1, xmm2 ; rg
				PUNPCKHWD xmm5, xmm2 ; rg
				PUNPCKLWD xmm3, xmm4 ; ba
				PUNPCKHWD xmm6, xmm4 ; ba
	
				MOVDQA xmm0, xmm1
				MOVDQA xmm2, xmm5

				PUNPCKLDQ xmm0, xmm3
				PUNPCKHDQ xmm1, xmm3

				PUNPCKLDQ xmm2, xmm6
				PUNPCKHDQ xmm5, xmm6

				PACKUSWB xmm0, xmm1
				PACKUSWB xmm2, xmm5
			

				MOVNTDQ [rcx], xmm0
				MOVNTDQ [rcx+16], xmm2


				LEA rcx, [rcx + 32]


		DEC r8d
		JNZ c_loop
						
		MOV r8d, r10d

	DEC r9d
	JNZ c_loop
			

	SFENCE

exit:

	RET
Image_Convert2RGBA	ENDP






ALIGN 16
Image_I4202RGBA_SSSE3	PROC

	PUSH r15
	PUSH r14

	PUSH rsi
	PUSH rdi


	MOV rsi, rdx
	MOV rdi, rcx
		
	MOV r15d, [r8+8] ; c_width
	MOV r9d, [r8+4] ; height	
	MOV r8d, [r8] ; width

	XOR rdx, rdx
	MOV eax, r8d
	MUL r9
		
	MOV r10, rax ; u	
	MOV r11, rax
	SHR r11, 2
	ADD r10, rsi ; v


	MOV rdx, r15
	SUB r15, r8
	SHL r15, 2
	SHL rdx, 2

	MOV rax, r8

	SHR r8, 3
	SHR r9, 1

	MOV ecx, r8d
	XOR r14, r14

	PXOR xmm14, xmm14

	MOVDQA xmm11, XMMWORD PTR [yuv_r]
	MOVDQA xmm12, XMMWORD PTR [yuv_g]
	MOVDQA xmm13, XMMWORD PTR [yuv_b]

	MOVDQA xmm10, XMMWORD PTR [yuv_shift]
	MOVDQA xmm9, XMMWORD PTR [alpha]

align 16
	pi_loop:

		MOVD xmm0, QWORD PTR [rsi]
		MOVD xmm1, QWORD PTR [rsi+rax]		
	
		MOVD xmm2, DWORD PTR [r10]
		MOVD xmm4, DWORD PTR [r10+r11]

		PUNPCKLBW xmm2, xmm2
		PUNPCKLBW xmm4, xmm4

		PUNPCKLBW xmm0, xmm2
		PUNPCKLBW xmm1, xmm2
				
		PUNPCKLBW xmm4, xmm14

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		PUNPCKLWD xmm0, xmm4
		PUNPCKHWD xmm2, xmm4

		PUNPCKLWD xmm1, xmm4
		PUNPCKHWD xmm3, xmm4




		MOVDQA xmm4, xmm0
		PUNPCKLBW xmm0, xmm14
		PUNPCKHBW xmm4, xmm14

		PSUBW xmm0, xmm10
		PSUBW xmm4, xmm10

		MOVDQA xmm5, xmm0
		MOVDQA xmm6, xmm4

		PMADDWD xmm0, xmm11
		PMADDWD xmm4, xmm11
		PHADDD xmm0, xmm4 ; red

		MOVDQA xmm4, xmm5
		MOVDQA xmm7, xmm6

		PMADDWD xmm4, xmm12
		PMADDWD xmm7, xmm12
		PHADDD xmm4, xmm7 ; green

		PMADDWD xmm5, xmm13
		PMADDWD xmm6, xmm13
		PHADDD xmm5, xmm6 ; blue


		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm0, xmm4
		PUNPCKLDQ xmm5, xmm14

		MOVDQA xmm8, xmm0

		PUNPCKLQDQ xmm0, xmm5 ; rgba0 
		PUNPCKHQDQ xmm8, xmm5 ; rgba1

		PUNPCKHDQ xmm6, xmm4
		PUNPCKHDQ xmm7, xmm14

		MOVDQA xmm4, xmm6
		PUNPCKLQDQ xmm6, xmm7 ; rgba2
		PUNPCKHQDQ xmm4, xmm7 ; rgba3

		PSRAD xmm0, 8
		PSRAD xmm8, 8
		PSRAD xmm6, 8
		PSRAD xmm4, 8

		PACKSSDW xmm0, xmm8
		PACKSSDW xmm6, xmm4

		PACKUSWB xmm0, xmm6
		POR xmm0, xmm9




		MOVDQA xmm4, xmm2
		PUNPCKLBW xmm2, xmm14
		PUNPCKHBW xmm4, xmm14

		PSUBW xmm2, xmm10
		PSUBW xmm4, xmm10

		MOVDQA xmm5, xmm2
		MOVDQA xmm6, xmm4

		PMADDWD xmm2, xmm11
		PMADDWD xmm4, xmm11
		PHADDD xmm2, xmm4 ; red

		MOVDQA xmm4, xmm5
		MOVDQA xmm7, xmm6

		PMADDWD xmm4, xmm12
		PMADDWD xmm7, xmm12
		PHADDD xmm4, xmm7 ; green

		PMADDWD xmm5, xmm13
		PMADDWD xmm6, xmm13
		PHADDD xmm5, xmm6 ; blue


		MOVDQA xmm6, xmm2
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm2, xmm4
		PUNPCKLDQ xmm5, xmm14

		MOVDQA xmm8, xmm2

		PUNPCKLQDQ xmm2, xmm5 ; rgba0 
		PUNPCKHQDQ xmm8, xmm5 ; rgba1

		PUNPCKHDQ xmm6, xmm4
		PUNPCKHDQ xmm7, xmm14

		MOVDQA xmm4, xmm6
		PUNPCKLQDQ xmm6, xmm7 ; rgba2
		PUNPCKHQDQ xmm4, xmm7 ; rgba3

		PSRAD xmm2, 8
		PSRAD xmm8, 8
		PSRAD xmm6, 8
		PSRAD xmm4, 8

		PACKSSDW xmm2, xmm8
		PACKSSDW xmm6, xmm4

		PACKUSWB xmm2, xmm6
		POR xmm2, xmm9


	MOVNTDQ [rdi+r14], xmm0
	MOVNTDQ [rdi+r14+16], xmm2



		MOVDQA xmm4, xmm1
		PUNPCKLBW xmm1, xmm14
		PUNPCKHBW xmm4, xmm14

		PSUBW xmm1, xmm10
		PSUBW xmm4, xmm10

		MOVDQA xmm5, xmm1
		MOVDQA xmm6, xmm4

		PMADDWD xmm1, xmm11
		PMADDWD xmm4, xmm11
		PHADDD xmm1, xmm4 ; red

		MOVDQA xmm4, xmm5
		MOVDQA xmm7, xmm6

		PMADDWD xmm4, xmm12
		PMADDWD xmm7, xmm12
		PHADDD xmm4, xmm7 ; green

		PMADDWD xmm5, xmm13
		PMADDWD xmm6, xmm13
		PHADDD xmm5, xmm6 ; blue


		MOVDQA xmm6, xmm1
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm1, xmm4
		PUNPCKLDQ xmm5, xmm14

		MOVDQA xmm8, xmm1

		PUNPCKLQDQ xmm1, xmm5 ; rgba0 
		PUNPCKHQDQ xmm8, xmm5 ; rgba1

		PUNPCKHDQ xmm6, xmm4
		PUNPCKHDQ xmm7, xmm14

		MOVDQA xmm4, xmm6
		PUNPCKLQDQ xmm6, xmm7 ; rgba2
		PUNPCKHQDQ xmm4, xmm7 ; rgba3

		PSRAD xmm1, 8
		PSRAD xmm8, 8
		PSRAD xmm6, 8
		PSRAD xmm4, 8

		PACKSSDW xmm1, xmm8
		PACKSSDW xmm6, xmm4

		PACKUSWB xmm1, xmm6
		POR xmm1, xmm9




		MOVDQA xmm4, xmm3
		PUNPCKLBW xmm3, xmm14
		PUNPCKHBW xmm4, xmm14

		PSUBW xmm3, xmm10
		PSUBW xmm4, xmm10

		MOVDQA xmm5, xmm3
		MOVDQA xmm6, xmm4

		PMADDWD xmm3, xmm11
		PMADDWD xmm4, xmm11
		PHADDD xmm3, xmm4 ; red

		MOVDQA xmm4, xmm5
		MOVDQA xmm7, xmm6

		PMADDWD xmm4, xmm12
		PMADDWD xmm7, xmm12
		PHADDD xmm4, xmm7 ; green

		PMADDWD xmm5, xmm13
		PMADDWD xmm6, xmm13
		PHADDD xmm5, xmm6 ; blue


		MOVDQA xmm6, xmm3
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm3, xmm4
		PUNPCKLDQ xmm5, xmm14

		MOVDQA xmm8, xmm3

		PUNPCKLQDQ xmm3, xmm5 ; rgba0 
		PUNPCKHQDQ xmm8, xmm5 ; rgba1

		PUNPCKHDQ xmm6, xmm4
		PUNPCKHDQ xmm7, xmm14

		MOVDQA xmm4, xmm6
		PUNPCKLQDQ xmm6, xmm7 ; rgba2
		PUNPCKHQDQ xmm4, xmm7 ; rgba3

		PSRAD xmm3, 8
		PSRAD xmm8, 8
		PSRAD xmm6, 8
		PSRAD xmm4, 8

		PACKSSDW xmm3, xmm8
		PACKSSDW xmm6, xmm4

		PACKUSWB xmm3, xmm6
		POR xmm3, xmm9

		ADD rdi, rdx

	MOVNTDQ [rdi+r14], xmm1
	MOVNTDQ [rdi+r14+16], xmm3

		SUB rdi, rdx



		ADD rdi, 32
		ADD rsi, 8
		ADD r10, 4
		
	DEC r8d
	JNZ pi_loop
		ADD r14, r15
		
		LEA rsi, [rsi+rcx*8]
		MOV r8d, ecx
		ADD rdi, rdx

	DEC r9d
	JNZ pi_loop


	SFENCE

	POP rdi
	POP rsi

	POP r14
	POP r15

	RET
Image_I4202RGBA_SSSE3	ENDP

ALIGN 16
Image_YUY22RGBA_SSSE3	PROC

	PXOR xmm15, xmm15

	MOVDQA xmm14, XMMWORD PTR [yuy_r0]
	MOVDQA xmm13, XMMWORD PTR [yuy_g0]
	MOVDQA xmm12, XMMWORD PTR [yuy_b0]

	MOVDQA xmm11, XMMWORD PTR [yuy_r1]
	MOVDQA xmm10, XMMWORD PTR [yuy_g1]
	MOVDQA xmm9, XMMWORD PTR [yuy_b1]
	
	MOVDQA xmm8, XMMWORD PTR [alpha]
	MOVDQA xmm7, XMMWORD PTR [yuy_shift]

	MOV r11, rdx
	
	MOV r10d, [r8+8]	
	MOV r9d, [r8+4]	
	MOV r8d, [r8]

	XOR rdx, rdx
	MOV eax, [rcx+12]
	MUL r10

	SUB r10, r8
	SHL r10, 2
	SHL rax, 2

	SHR r8d, 3
	MOV edx, r8d

	XOR rax, rax

align 16
	pi_loop:

		MOVDQU xmm0, [r11]
		MOVDQA xmm1, xmm0
		
		PUNPCKLBW xmm0, xmm15
		PUNPCKHBW xmm1, xmm15

		PSUBW xmm0, xmm7
		PSUBW xmm1, xmm7

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm0

		PMADDWD xmm2, xmm14 ; red
		PMADDWD xmm3, xmm11 ; red
		PHADDD xmm2, xmm3 ; red

		MOVDQA xmm3, xmm0
		MOVDQA xmm4, xmm0

		PMADDWD xmm3, xmm13 ; green
		PMADDWD xmm4, xmm10 ; green
		PHADDD xmm3, xmm4 ; green

		MOVDQA xmm4, xmm0

		PMADDWD xmm4, xmm12 ; blue
		PMADDWD xmm0, xmm9 ; blue
		PHADDD xmm4, xmm0 ; blue


		MOVDQA xmm0, xmm2
		MOVDQA xmm5, xmm4

		PUNPCKLDQ xmm0, xmm3
		PUNPCKHDQ xmm2, xmm3

		PUNPCKLDQ xmm5, xmm15
		PUNPCKHDQ xmm4, xmm15

		MOVDQA xmm6, xmm0
		PUNPCKLQDQ xmm0, xmm5 ; rgba0 
		PUNPCKHQDQ xmm6, xmm5 ; rgba2
			

		MOVDQA xmm5, xmm2
		PUNPCKLQDQ xmm2, xmm4 ; rgba1
		PUNPCKHQDQ xmm5, xmm4 ; rgba3

		PSRAD xmm0, 8
		PSRAD xmm6, 8
		PSRAD xmm2, 8
		PSRAD xmm5, 8

		PACKSSDW xmm0, xmm2
		PACKSSDW xmm6, xmm5

		PACKUSWB xmm0, xmm6

		POR xmm0, xmm8


		MOVDQA xmm2, xmm1
		MOVDQA xmm3, xmm1

		PMADDWD xmm2, xmm14 ; red
		PMADDWD xmm3, xmm11 ; red
		PHADDD xmm2, xmm3 ; red

		MOVDQA xmm3, xmm1
		MOVDQA xmm4, xmm1

		PMADDWD xmm3, xmm13 ; green
		PMADDWD xmm4, xmm10 ; green
		PHADDD xmm3, xmm4 ; green

		MOVDQA xmm4, xmm1

		PMADDWD xmm4, xmm12 ; blue
		PMADDWD xmm1, xmm9 ; blue
		PHADDD xmm4, xmm1 ; blue


		MOVDQA xmm1, xmm2
		MOVDQA xmm5, xmm4

		PUNPCKLDQ xmm1, xmm3
		PUNPCKHDQ xmm2, xmm3

		PUNPCKLDQ xmm5, xmm15
		PUNPCKHDQ xmm4, xmm15

		MOVDQA xmm6, xmm1
		PUNPCKLQDQ xmm1, xmm5 ; rgba4
		PUNPCKHQDQ xmm6, xmm5 ; rgba6
				

		MOVDQA xmm5, xmm2
		PUNPCKLQDQ xmm2, xmm4 ; rgba5
		PUNPCKHQDQ xmm5, xmm4 ; rgba7

		PSRAD xmm1, 8
		PSRAD xmm6, 8
		PSRAD xmm2, 8
		PSRAD xmm5, 8

		PACKSSDW xmm1, xmm2
		PACKSSDW xmm6, xmm5

		PACKUSWB xmm1, xmm6

		POR xmm1, xmm8

			
	MOVNTDQ [rcx+rax], xmm0
	MOVNTDQ [rcx+rax+16], xmm1
		

		ADD rcx, 32
		ADD r11, 16

		DEC r8d
		JNZ pi_loop

		ADD rax, r10	

		MOV r8d, edx
	DEC r9d
	JNZ pi_loop

	SFENCE

	RET
Image_YUY22RGBA_SSSE3	ENDP


ALIGN 16
Image_BGR2RGBA_SSSE3	PROC

	PXOR xmm14, xmm14
	MOVDQA xmm8, XMMWORD PTR [alpha]

	MOVDQA xmm10, XMMWORD PTR [rgb_shuf0]
	MOVDQA xmm11, XMMWORD PTR [rgb_shuf1]

	MOV r10, rdx

	MOV r11d, [r8+8]
	MOV r9d, [r8+4] ; height
	MOV r8d, [r8] ; width
	

	XOR rdx, rdx
	MOV eax, r9d
	MUL r11
	
	SUB rax, r11
	SHL rax, 2

	ADD r11, r8
	SHL r11, 2


	SHR r8d, 3
	MOV edx, r8d
		

align 16
	pi_loop:
		MOVDQU xmm0, [r10]
		MOVDQU xmm1, [r10+8]

		PSHUFB xmm0, xmm10
		PSHUFB xmm1, xmm11

		POR xmm0, xmm8
		POR xmm1, xmm8

	MOVNTDQ [rcx+rax], xmm0
	MOVNTDQ [rcx+rax+16], xmm1
	

		ADD r10, 24
		ADD rcx, 32

		DEC r8d
		JNZ pi_loop
		
		SUB rax, r11
		
		MOV r8d, edx
	DEC r9d
	JNZ pi_loop

	SFENCE

	RET
Image_BGR2RGBA_SSSE3	ENDP




ALIGN 16
Image_Remap_1	PROC
	PUSH rsi
	PUSH rdi

	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV rdi, r9

	MOV rsi, [rcx]
	MOV ebx, [rcx+12] ; height
	MOV ecx, [rcx+20] ; c_width

	SHR ecx, 2
	MOV r9d, ecx


align 16
	remap_loop:
			MOV r10, [rsi]
			MOV r11, [rsi+8]


			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 16
			ROR r10, 8

			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 16
			ROR r10, 8

			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 16
			ROR r10, 16

			MOV r12w, 0FFFFh
			ROR r12, 16





			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 16
			ROR r10, 8

			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 16
			ROR r10, 8

			MOVZX rax, r10b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 16
			ROR r10, 16

			MOV r13w, 0FFFFh
			ROR r13, 16





			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 16
			ROR r11, 8

			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 16
			ROR r11, 8

			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 16
			ROR r11, 16

			MOV r14w, 0FFFFh
			ROR r14, 16





			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 16
			ROR r11, 8

			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 16
			ROR r11, 8

			MOVZX rax, r11b
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 16
			ROR r11, 16

			MOV r15w, 0FFFFh
			ROR r15, 16


			MOVNTI [rdi], r12
			MOVNTI [rdi+8], r13
			MOVNTI [rdi+16], r14
			MOVNTI [rdi+24], r15


			ADD rsi, 16
			ADD rdi, 32

		DEC ecx
		JNZ remap_loop

		MOV ecx, r9d

	DEC ebx
	JNZ remap_loop

	SFENCE

	POP r15
	POP r14
	POP r13
	POP r12

	POP rbx

	POP rdi
	POP rsi
	RET
Image_Remap_1	ENDP


ALIGN 16
Image_Remap_2	PROC
	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; height	
	MOV ebx, [rcx+20] ; c_width
	MOV ecx, [rcx+16]

	SHR ebx, 2
	MOV r9d, ebx


align 16
	remap_loop:
			MOV r12, [r11]
			MOV r13, [r11+8]
			MOV r14, [r11+16]
			MOV r15, [r11+24]


			MOVZX rax, r12w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 16

			MOVZX rax, r12w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 16

			MOVZX rax, r12w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r12w, [rdx+rax*2]
			ROR r12, 32




			MOVZX rax, r13w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 16

			MOVZX rax, r13w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 16

			MOVZX rax, r13w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r13w, [rdx+rax*2]
			ROR r13, 32



			MOVZX rax, r14w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 16

			MOVZX rax, r14w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 16

			MOVZX rax, r14w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r14w, [rdx+rax*2]
			ROR r14, 32





			MOVZX rax, r15w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 16

			MOVZX rax, r15w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 16

			MOVZX rax, r15w
			SHR rax, cl
			CMP eax, r8d
			CMOVA eax, r8d
			MOV r15w, [rdx+rax*2]
			ROR r15, 32




			MOVNTI [r11], r12
			MOVNTI [r11+8], r13
			MOVNTI [r11+16], r14
			MOVNTI [r11+24], r15


			ADD r11, 32

		DEC ebx
		JNZ remap_loop

		MOV ebx, r9d

	DEC r10d
	JNZ remap_loop

	SFENCE

	POP r15
	POP r14
	POP r13
	POP r12


	POP rbx
	RET
Image_Remap_2	ENDP


ALIGN 16
Image_Transform_1	PROC
	MOVAPS xmm0, [rdx]
	MOVAPS xmm1, [rdx+16]
	MOVAPS xmm2, [rdx+32]
	MOVAPS xmm3, [rdx+48]

	MULPS xmm0, XMMWORD PTR [float_8192]
	MULPS xmm1, XMMWORD PTR [float_8192]
	MULPS xmm2, XMMWORD PTR [float_8192]
	MULPS xmm3, XMMWORD PTR [float_8192]

	MOVAPS xmm4, xmm0

	UNPCKLPS xmm0, xmm1
	UNPCKHPS xmm4, xmm1

	MOVAPS xmm1, xmm2

	UNPCKLPS xmm2, xmm3
	UNPCKHPS xmm1, xmm3

	MOVAPS xmm3, xmm1

	MOVHLPS xmm3, xmm4
	MOVLHPS xmm4, xmm1

	MOVAPS xmm1, xmm2
	MOVHLPS xmm1, xmm0
	MOVLHPS xmm0, xmm2

	MOVAPS xmm2, xmm4



	CVTPS2DQ xmm0, xmm0
	CVTPS2DQ xmm1, xmm1
	CVTPS2DQ xmm2, xmm2
;	CVTPS2DQ xmm3, xmm3

	PXOR xmm3, xmm3


	PACKSSDW xmm0, xmm1
	PACKSSDW xmm2, xmm3

	PXOR xmm1, xmm1
	MOVDQA xmm3, XMMWORD PTR [tri_mask]
	
	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; height
	MOV r8d, [rcx+20] ; c_width

	SHR r8, 3
	MOV r9, r8


align 16
	transform_loop:
			MOVDQA xmm4, [r11]
			MOVDQA xmm6, [r11+16]

			MOVDQA xmm14, xmm4
			MOVDQA xmm15, xmm6

			PAND xmm14, xmm3
			PAND xmm15, xmm3



			MOVDQA xmm5, xmm4
			MOVDQA xmm7, xmm6

			PUNPCKLBW xmm4, xmm1
			PUNPCKHBW xmm5, xmm1

			PUNPCKLBW xmm6, xmm1
			PUNPCKHBW xmm7, xmm1



			MOVDQA xmm9, xmm4
			PUNPCKLQDQ xmm4, xmm4
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm4
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm4, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm4, xmm8
			PHADDD xmm9, xmm10

			PSRAD xmm4, 13
			PSRAD xmm9, 13

			PACKSSDW xmm4, xmm9





			MOVDQA xmm9, xmm5
			PUNPCKLQDQ xmm5, xmm5
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm5
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm5, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm5, xmm8
			PHADDD xmm9, xmm10

			PSRAD xmm5, 13
			PSRAD xmm9, 13

			PACKSSDW xmm5, xmm9





			MOVDQA xmm9, xmm6
			PUNPCKLQDQ xmm6, xmm6
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm6
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm6, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm6, xmm8
			PHADDD xmm9, xmm10

			PSRAD xmm6, 13
			PSRAD xmm9, 13

			PACKSSDW xmm6, xmm9






			MOVDQA xmm9, xmm7
			PUNPCKLQDQ xmm7, xmm7
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm7
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm7, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm7, xmm8
			PHADDD xmm9, xmm10

			PSRAD xmm7, 13
			PSRAD xmm9, 13

			PACKSSDW xmm7, xmm9


			PACKUSWB xmm4, xmm5
			PACKUSWB xmm6, xmm7

			POR xmm4, xmm14
			POR xmm6, xmm15

			MOVNTDQ [r11], xmm4
			MOVNTDQ [r11+16], xmm6



			ADD r11, 32
		DEC r8
		JNZ transform_loop

		MOV r8, r9

	DEC r10
	JNZ transform_loop

	SFENCE

	RET
Image_Transform_1	ENDP

ALIGN 16
Image_Transform_2	PROC
	MOVAPS xmm0, [rdx]
	MOVAPS xmm1, [rdx+16]
	MOVAPS xmm2, [rdx+32]
	MOVAPS xmm3, [rdx+48]

	MULPS xmm0, XMMWORD PTR [float_8192]
	MULPS xmm1, XMMWORD PTR [float_8192]
	MULPS xmm2, XMMWORD PTR [float_8192]
	MULPS xmm3, XMMWORD PTR [float_8192]

; transpose
	MOVAPS xmm4, xmm0

	UNPCKLPS xmm0, xmm1
	UNPCKHPS xmm4, xmm1

	MOVAPS xmm1, xmm2

	UNPCKLPS xmm2, xmm3
	UNPCKHPS xmm1, xmm3

	MOVAPS xmm3, xmm1

	MOVHLPS xmm3, xmm4
	MOVLHPS xmm4, xmm1

	MOVAPS xmm1, xmm2
	MOVHLPS xmm1, xmm0
	MOVLHPS xmm0, xmm2

	MOVAPS xmm2, xmm4



	CVTPS2DQ xmm0, xmm0
	CVTPS2DQ xmm1, xmm1
	CVTPS2DQ xmm2, xmm2
;	CVTPS2DQ xmm3, xmm3

	PXOR xmm3, xmm3

	MOVDQA xmm11, xmm0
	MOVDQA xmm12, xmm2	

	PACKSSDW xmm0, xmm1
	PACKSSDW xmm2, xmm3

	MOV eax, -8192
	MOVD xmm5, eax
	PSLLDQ xmm5, 12

	POR xmm11, xmm5
	POR xmm12, xmm5
	POR xmm1, xmm5

	PHADDD xmm11, xmm1
	PHADDD xmm12, xmm3

	PHADDD xmm11, xmm12
	
	PSLLD xmm11, 15
	MOVDQA xmm1, xmm11
	
	MOVDQA xmm3, XMMWORD PTR [tr2_mask]
	
	MOVDQA xmm11, XMMWORD PTR [word_8000]

	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; height
	MOV r8d, [rcx+20] ; c_width

	SHR r8, 3
	MOV r9, r8


align 16
	transform_loop:
			MOVDQA xmm4, [r11]
			MOVDQA xmm5, [r11+16]
			MOVDQA xmm6, [r11+32]
			MOVDQA xmm7, [r11+48]

			MOVDQA xmm12, xmm4
			MOVDQA xmm13, xmm5
			MOVDQA xmm14, xmm6
			MOVDQA xmm15, xmm7

			PAND xmm12, xmm3
			PAND xmm13, xmm3
			PAND xmm14, xmm3
			PAND xmm15, xmm3


			
		PXOR xmm4, xmm11

			MOVDQA xmm9, xmm4
			PUNPCKLQDQ xmm4, xmm4
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm4
			MOVDQA xmm10, xmm9		

			PMADDWD xmm4, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm4, xmm8
			PHADDD xmm9, xmm10

		PADDD xmm4, xmm1
		PADDD xmm9, xmm1

			PSRAD xmm4, 13
			PSRAD xmm9, 13

			PACKSSDW xmm4, xmm9
		PXOR xmm4, xmm11


		PXOR xmm5, xmm11	

			MOVDQA xmm9, xmm5
			PUNPCKLQDQ xmm5, xmm5
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm5
			MOVDQA xmm10, xmm9
			

			PMADDWD xmm5, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm5, xmm8
			PHADDD xmm9, xmm10

		PADDD xmm5, xmm1
		PADDD xmm9, xmm1

			PSRAD xmm5, 13
			PSRAD xmm9, 13
			
			PACKSSDW xmm5, xmm9
		PXOR xmm5, xmm11



		PXOR xmm6, xmm11

			MOVDQA xmm9, xmm6
			PUNPCKLQDQ xmm6, xmm6
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm6
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm6, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm6, xmm8
			PHADDD xmm9, xmm10

		PADDD xmm6, xmm1
		PADDD xmm9, xmm1

			PSRAD xmm6, 13
			PSRAD xmm9, 13

			PACKSSDW xmm6, xmm9
		PXOR xmm6, xmm11


			
			
		PXOR xmm7, xmm11

			MOVDQA xmm9, xmm7
			PUNPCKLQDQ xmm7, xmm7
			PUNPCKHQDQ xmm9, xmm9

			MOVDQA xmm8, xmm7
			MOVDQA xmm10, xmm9
				

			PMADDWD xmm7, xmm0
			PMADDWD xmm8, xmm2
				
			PMADDWD xmm9, xmm0
			PMADDWD xmm10, xmm2

			PHADDD xmm7, xmm8
			PHADDD xmm9, xmm10

		PADDD xmm7, xmm1
		PADDD xmm9, xmm1

			PSRAD xmm7, 13
			PSRAD xmm9, 13

			PACKSSDW xmm7, xmm9
		PXOR xmm7, xmm11



			POR xmm4, xmm12
			POR xmm5, xmm13
			POR xmm6, xmm14
			POR xmm7, xmm15

			MOVNTDQ [r11], xmm4
			MOVNTDQ [r11+16], xmm5
			MOVNTDQ [r11+32], xmm6
			MOVNTDQ [r11+48], xmm7


			ADD r11, 64
		DEC r8
		JNZ transform_loop

		MOV r8, r9

	DEC r10
	JNZ transform_loop

	SFENCE

	RET
Image_Transform_2	ENDP


ALIGN 16
Image_FromScreen	PROC
	
	PUSH rsi
	PUSH rdi

		MOV rsi, rdx
		MOV rdi, rcx

		XOR rdx, rdx

		MOV eax, [r8]
		MUL DWORD PTR [r8+12]
		ADD eax, [r8+8]

		MOV edx, [r8] ; full_width

		MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
		MOV r11d, 1
		SHL r11d, cl
		SUB r11d, 1


		MOV ecx, [r8+16]
		

		MOV r10d, [r8+16]
		ADD r10d, r11d
		NOT r11d
		AND r10d, r11d

		SUB r10d, ecx
		SUB edx, ecx
				
		TEST r9d, r9d
		JZ forward_c
			MOV r9d, eax
			MOV r11d, edx
			
				ADD r10d, ecx

				XOR rdx, rdx
				MOV eax, [r8+20]
				DEC eax
				MUL r10d

				LEA rdi, [rdi+rax*4]

				ADD r10d, ecx
				NEG r10

			MOV eax, r9d
			MOV edx, r11d

		forward_c:

		SHL r10, 2

		CMP DWORD PTR [r8+28], 4
		JNZ tripple_co
			SHL rdx, 2
			LEA rsi, [rsi+rax*4]			
		
			MOV r11d, [r8+20]
			MOV r9d, ecx

	align 16
		wi_loop_4:
			REP MOVSD

			MOV ecx, r9d

			ADD rsi, rdx

			ADD rdi, r10			

		DEC r11d
		JNZ wi_loop_4

		JMP exit
	tripple_co:
		CMP DWORD PTR [r8+28], 3
		JNZ double_co
			LEA rdx, [rdx+2*rdx]
			LEA rsi, [rsi+rax*2]
			ADD rsi, rax			
		
			MOV r11d, [r8+20]
			MOV r9d, ecx

			MOV eax, 0FF000000h
	align 16
		wi_loop_3:
			LODSB
			ROR eax, 8
			LODSB
			ROR eax, 8
			LODSB
			ROR eax, 16
			
			STOSD

		DEC ecx
		JNZ wi_loop_3

			MOV ecx, r9d

			ADD rsi, rdx
			ADD rdi, r10			

		DEC r11d
		JNZ wi_loop_3


		JMP exit
	double_co:
		CMP DWORD PTR [r8+28], 2
		JNZ single_co
			ADD rdx, rdx
			LEA rsi, [rsi+rax*2]
					
			MOV r11d, [r8+20]
			MOV r9d, ecx

			MOV eax, 0FF000000h
	align 16
		wi_loop_2:
			LODSB
			ROR eax, 8
			LODSB
			ROL eax, 8
			
			STOSD

		DEC ecx
		JNZ wi_loop_2

			MOV ecx, r9d

			ADD rsi, rdx
			ADD rdi, r10			

		DEC r11d
		JNZ wi_loop_2


		JMP exit
	single_co:			
			ADD rsi, rax			
		
			MOV r11d, [r8+20]
			MOV r9d, ecx

			MOV eax, 0FF000000h
	align 16
		wi_loop_1:
			LODSB
			STOSD

		DEC ecx
		JNZ wi_loop_1
			MOV ecx, r9d

			ADD rsi, rdx
			ADD rdi, r10			

		DEC r11d
		JNZ wi_loop_1

exit:

	POP rdi
	POP rsi

	RET
Image_FromScreen	ENDP


ALIGN 16
Image_YCC2RGB_1	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx
		
	MOVDQA xmm14, XMMWORD PTR [one_mask]
	MOVDQA xmm15, XMMWORD PTR [tri_mask]
	MOVDQA xmm5, XMMWORD PTR [cb601_factor]
	MOVDQA xmm6, XMMWORD PTR [cr601_factor]
	MOVDQA xmm4, XMMWORD PTR [y601_offset]


	PXOR xmm7, xmm7

align 16
	row_loop:
			MOVDQA xmm0, [rcx]
			MOVDQA xmm1, [rcx+16]

			MOVDQA xmm2, xmm0
			MOVDQA xmm3, xmm1

			PUNPCKLBW xmm0, xmm7
			PUNPCKHBW xmm2, xmm7

			PSHUFLW xmm8, xmm0, 0
			PSHUFLW xmm9, xmm2, 0

			PSHUFHW xmm8, xmm8, 0			
			PSHUFHW xmm9, xmm9, 0

			PSLLW xmm8, 6
			PSLLW xmm9, 6

			PADDW xmm8, xmm4
			PADDW xmm9, xmm4

			PSHUFLW xmm10, xmm0, 55h
			PSHUFLW xmm11, xmm2, 55h

			PSHUFHW xmm10, xmm10, 55h			
			PSHUFHW xmm11, xmm11, 55h

			PMULLW xmm10, xmm5
			PMULLW xmm11, xmm5

			PSHUFLW xmm0, xmm0, 0AAh
			PSHUFLW xmm2, xmm2, 0AAh

			PSHUFHW xmm0, xmm0, 0AAh			
			PSHUFHW xmm2, xmm2, 0AAh

			PMULLW xmm0, xmm6
			PMULLW xmm2, xmm6

			PADDW xmm8, xmm10
			PADDW xmm9, xmm11

			PADDW xmm0, xmm8
			PADDW xmm2, xmm9

			PXOR xmm8, xmm8
			PXOR xmm9, xmm9

			PCMPGTW xmm8, xmm0
			PCMPGTW xmm9, xmm2

			PXOR xmm8, xmm14
			PXOR xmm9, xmm14

			PAND xmm0, xmm8
			PAND xmm2, xmm9

			PSRLW xmm0, 6
			PSRLW xmm2, 6



			PUNPCKLBW xmm1, xmm7
			PUNPCKHBW xmm3, xmm7

			PSHUFLW xmm8, xmm1, 0
			PSHUFLW xmm9, xmm3, 0

			PSHUFHW xmm8, xmm8, 0		
			PSHUFHW xmm9, xmm9, 0

			PSLLW xmm8, 6
			PSLLW xmm9, 6

			PADDW xmm8, xmm4
			PADDW xmm9, xmm4
			
			PSHUFLW xmm10, xmm1, 55h
			PSHUFLW xmm11, xmm3, 55h

			PSHUFHW xmm10, xmm10, 55h		
			PSHUFHW xmm11, xmm11, 55h

			PMULLW xmm10, xmm5
			PMULLW xmm11, xmm5

			PSHUFLW xmm1, xmm1, 0AAh
			PSHUFLW xmm3, xmm3, 0AAh

			PSHUFHW xmm1, xmm1, 0AAh			
			PSHUFHW xmm3, xmm3, 0AAh

			PMULLW xmm1, xmm6
			PMULLW xmm3, xmm6

			PADDW xmm8, xmm10
			PADDW xmm9, xmm11
			
			PADDW xmm1, xmm8
			PADDW xmm3, xmm9

			PXOR xmm8, xmm8
			PXOR xmm9, xmm9

			PCMPGTW xmm8, xmm1
			PCMPGTW xmm9, xmm3

			PXOR xmm8, xmm14
			PXOR xmm9, xmm14

			PAND xmm1, xmm8
			PAND xmm3, xmm9

			PSRLW xmm1, 6
			PSRLW xmm3, 6
			

			PACKUSWB xmm0, xmm2
			PACKUSWB xmm1, xmm3

			POR xmm0, xmm15
			POR xmm1, xmm15


			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1

			ADD rcx, 32
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_YCC2RGB_1	ENDP

ALIGN 16
Image_YCK2RGB_1	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx
		
	MOVDQA xmm14, XMMWORD PTR [one_mask]
	MOVDQA xmm15, XMMWORD PTR [tri_mask]

	MOVDQA xmm5, XMMWORD PTR [cb709_factor]
	MOVDQA xmm6, XMMWORD PTR [cr709_factor]
	MOVDQA xmm4, XMMWORD PTR [y709_offset]


	PXOR xmm7, xmm7

align 16
	row_loop:
			MOVDQA xmm0, [rcx]
			MOVDQA xmm1, [rcx+16]

			MOVDQA xmm2, xmm0
			MOVDQA xmm3, xmm1

			PUNPCKLBW xmm0, xmm7
			PUNPCKHBW xmm2, xmm7

			PSHUFLW xmm8, xmm0, 0
			PSHUFLW xmm9, xmm2, 0

			PSHUFHW xmm8, xmm8, 0			
			PSHUFHW xmm9, xmm9, 0

			PSLLW xmm8, 6
			PSLLW xmm9, 6

			PADDW xmm8, xmm4
			PADDW xmm9, xmm4

			PSHUFLW xmm10, xmm0, 55h
			PSHUFLW xmm11, xmm2, 55h

			PSHUFHW xmm10, xmm10, 55h			
			PSHUFHW xmm11, xmm11, 55h

			PMULLW xmm10, xmm5
			PMULLW xmm11, xmm5

			PSHUFLW xmm12, xmm0, 0AAh
			PSHUFLW xmm13, xmm2, 0AAh

			PSHUFHW xmm12, xmm12, 0AAh			
			PSHUFHW xmm13, xmm13, 0AAh

			PMULLW xmm12, xmm6
			PMULLW xmm13, xmm6

			PSHUFLW xmm0, xmm0, 0FFh
			PSHUFLW xmm2, xmm2, 0FFh

			PSHUFHW xmm0, xmm0, 0FFh			
			PSHUFHW xmm2, xmm2, 0FFh

			PADDW xmm8, xmm10
			PADDW xmm9, xmm11

			PADDW xmm12, xmm8
			PADDW xmm13, xmm9

			PXOR xmm8, xmm8
			PXOR xmm9, xmm9

			PCMPGTW xmm8, xmm12
			PCMPGTW xmm9, xmm13

			PXOR xmm8, xmm14
			PXOR xmm9, xmm14

			PAND xmm12, xmm8
			PAND xmm13, xmm9

			PSRLW xmm12, 6
			PSRLW xmm13, 6

			PMULLW xmm12, xmm0
			PMULLW xmm13, xmm2

			PSRLW xmm12, 8
			PSRLW xmm13, 8

			PSUBW xmm0, xmm12
			PSUBW xmm2, xmm13




			PUNPCKLBW xmm1, xmm7
			PUNPCKHBW xmm3, xmm7

			PSHUFLW xmm8, xmm1, 0
			PSHUFLW xmm9, xmm3, 0

			PSHUFHW xmm8, xmm8, 0		
			PSHUFHW xmm9, xmm9, 0

			PSLLW xmm8, 6
			PSLLW xmm9, 6

			PADDW xmm8, xmm4
			PADDW xmm9, xmm4
			
			PSHUFLW xmm10, xmm1, 55h
			PSHUFLW xmm11, xmm3, 55h

			PSHUFHW xmm10, xmm10, 55h		
			PSHUFHW xmm11, xmm11, 55h

			PMULLW xmm10, xmm5
			PMULLW xmm11, xmm5

			PSHUFLW xmm12, xmm1, 0AAh
			PSHUFLW xmm13, xmm3, 0AAh

			PSHUFHW xmm12, xmm12, 0AAh			
			PSHUFHW xmm13, xmm13, 0AAh

			PMULLW xmm12, xmm6
			PMULLW xmm13, xmm6

			PSHUFLW xmm1, xmm1, 0FFh
			PSHUFLW xmm3, xmm3, 0FFh

			PSHUFHW xmm1, xmm1, 0FFh			
			PSHUFHW xmm3, xmm3, 0FFh

			PADDW xmm8, xmm10
			PADDW xmm9, xmm11
			
			PADDW xmm12, xmm8
			PADDW xmm13, xmm9

			PXOR xmm8, xmm8
			PXOR xmm9, xmm9

			PCMPGTW xmm8, xmm12
			PCMPGTW xmm9, xmm13

			PXOR xmm8, xmm14
			PXOR xmm9, xmm14

			PAND xmm12, xmm8
			PAND xmm13, xmm9

			PSRLW xmm12, 6
			PSRLW xmm13, 6

			PMULLW xmm12, xmm1
			PMULLW xmm13, xmm3

			PSRLW xmm12, 8
			PSRLW xmm13, 8

			PSUBW xmm1, xmm12
			PSUBW xmm3, xmm13


			PACKUSWB xmm0, xmm2
			PACKUSWB xmm1, xmm3

			POR xmm0, xmm15
			POR xmm1, xmm15


			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1

			ADD rcx, 32
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_YCK2RGB_1	ENDP


ALIGN 16
Image_CMY2RGB_1	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx

	MOVDQA xmm2, XMMWORD PTR [one_mask]
	MOVDQA xmm3, XMMWORD PTR [tri_mask]

	PXOR xmm7, xmm7

align 16
	row_loop:
			MOVDQA xmm0, xmm2
			MOVDQA xmm1, xmm2

			PSUBB xmm0, [rcx]
			PSUBB xmm1, [rcx+16]			
			
			MOVDQA xmm4, xmm0
			MOVDQA xmm5, xmm1

			PUNPCKLBW xmm0, xmm7
			PUNPCKHBW xmm4, xmm7

			PSHUFLW xmm8, xmm0, 0FFh
			PSHUFLW xmm9, xmm4, 0FFh

			PSHUFHW xmm8, xmm8, 0FFh			
			PSHUFHW xmm9, xmm9, 0FFh
			
			PMULLW xmm0, xmm8
			PMULLW xmm4, xmm9

			PSRLW xmm0, 8
			PSRLW xmm4, 8

			PUNPCKLBW xmm1, xmm7
			PUNPCKHBW xmm5, xmm7

			PSHUFLW xmm8, xmm1, 0FFh
			PSHUFLW xmm9, xmm5, 0FFh

			PSHUFHW xmm8, xmm8, 0FFh			
			PSHUFHW xmm9, xmm9, 0FFh
			
			PMULLW xmm1, xmm8
			PMULLW xmm5, xmm9

			PSRLW xmm1, 8
			PSRLW xmm5, 8
			
			PACKUSWB xmm0, xmm4
			PACKUSWB xmm1, xmm5
						
			
			POR xmm0, xmm3
			POR xmm1, xmm3

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1

			ADD rcx, 32
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_CMY2RGB_1	ENDP

ALIGN 16
Image_CMJ2RGB_1	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx

	MOVDQA xmm2, XMMWORD PTR [one_mask]
	MOVDQA xmm3, XMMWORD PTR [tri_mask]

	PXOR xmm7, xmm7

align 16
	row_loop:
			MOVDQA xmm0, [rcx]
			MOVDQA xmm1, [rcx+16]

			MOVDQA xmm4, xmm0
			MOVDQA xmm5, xmm1

			PUNPCKLBW xmm0, xmm7
			PUNPCKHBW xmm4, xmm7

			PSHUFLW xmm8, xmm0, 0FFh
			PSHUFLW xmm9, xmm4, 0FFh

			PSHUFHW xmm8, xmm8, 0FFh			
			PSHUFHW xmm9, xmm9, 0FFh
			
			PMULLW xmm0, xmm8
			PMULLW xmm4, xmm9

			PSRLW xmm0, 8
			PSRLW xmm4, 8

			PUNPCKLBW xmm1, xmm7
			PUNPCKHBW xmm5, xmm7

			PSHUFLW xmm8, xmm1, 0FFh
			PSHUFLW xmm9, xmm5, 0FFh

			PSHUFHW xmm8, xmm8, 0FFh			
			PSHUFHW xmm9, xmm9, 0FFh
			
			PMULLW xmm1, xmm8
			PMULLW xmm5, xmm9

			PSRLW xmm1, 8
			PSRLW xmm5, 8
			
			PACKUSWB xmm0, xmm4
			PACKUSWB xmm1, xmm5
					
			
			POR xmm0, xmm3
			POR xmm1, xmm3

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1

			ADD rcx, 32
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_CMJ2RGB_1	ENDP


ALIGN 16
Image_BW2YB_1	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx
	
	MOVDQA xmm6, XMMWORD PTR [bw1_mask]
	MOVDQA xmm7, XMMWORD PTR [tri_mask]

align 16
	row_loop:
			MOVDQA xmm0, [rcx]
			MOVDQA xmm1, [rcx+16]
			
			MOVDQA xmm8, xmm0
			MOVDQA xmm9, xmm1
			
			PSLLD xmm8, 8
			PSLLD xmm9, 8

			POR xmm8, xmm0
			POR xmm9, xmm1

			MOVDQA xmm0, xmm6
			MOVDQA xmm1, xmm6

			PSUBB xmm0, xmm8
			PSUBB xmm1, xmm9

			POR xmm0, xmm7
			POR xmm1, xmm7

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1

			ADD rcx, 32
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_BW2YB_1	ENDP



ALIGN 16
Image_YCC2RGB_2	PROC

	RET
Image_YCC2RGB_2	ENDP


ALIGN 16
Image_CMY2RGB_2	PROC

	RET
Image_CMY2RGB_2	ENDP


ALIGN 16
Image_BW2YB_2	PROC
	SHR edx, 3 ; cw count
	MOV r9d, edx
	
	MOVDQA xmm6, XMMWORD PTR [bw2_mask]
	MOVDQA xmm7, XMMWORD PTR [tr2_mask]

align 16
	row_loop:
			MOVDQA xmm0, [rcx]
			MOVDQA xmm1, [rcx+16]
			MOVDQA xmm2, [rcx+32]
			MOVDQA xmm3, [rcx+48]
			
			MOVDQA xmm8, xmm0
			MOVDQA xmm9, xmm1
			
			PSLLD xmm8, 16
			PSLLD xmm9, 16

			POR xmm8, xmm0
			POR xmm9, xmm1

			MOVDQA xmm0, xmm6
			MOVDQA xmm1, xmm6

			PSUBW xmm0, xmm8
			PSUBW xmm1, xmm9

			POR xmm0, xmm7
			POR xmm1, xmm7

			MOVDQA xmm8, xmm2
			MOVDQA xmm9, xmm3
			
			PSLLD xmm8, 16
			PSLLD xmm9, 16

			POR xmm8, xmm2
			POR xmm9, xmm3

			MOVDQA xmm2, xmm6
			MOVDQA xmm3, xmm6

			PSUBW xmm2, xmm8
			PSUBW xmm3, xmm9

			POR xmm2, xmm7
			POR xmm3, xmm7

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1
			MOVNTDQ [rcx+32], xmm2
			MOVNTDQ [rcx+48], xmm3

			ADD rcx, 64
		DEC r9d
		JNZ row_loop

		MOV r9d, edx

	DEC r8d
	JNZ row_loop

	SFENCE

	RET
Image_BW2YB_2	ENDP



ALIGN 16
Image_PaletteSwap	PROC	
	MOV edx, [rcx]
	SHR edx, 2

	LEA rcx, [rcx+16]
		
align 16
	swap_loop:
		MOV eax, [rcx]
		MOV r8d, [rcx+4]
		MOV r9d, [rcx+8]
		MOV r10d, [rcx+12]

		BSWAP eax
		ROR eax, 8
		BSWAP r8d
		ROR r8d, 8
		BSWAP r9d
		ROR r9d, 8
		BSWAP r10d
		ROR r10d, 8

		MOV [rcx], eax
		MOV [rcx+4], r8d
		MOV [rcx+8], r9d
		MOV [rcx+12], r10d

		ADD rcx, 16
	DEC edx
	JNZ swap_loop

	RET
Image_PaletteSwap	ENDP


END

