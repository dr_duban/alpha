
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



;EXTERN	?size_paragraph@Image@Pics@@1IB:DWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16
dword_one	DWORD	1, 1, 1, 1
co_shuf		DWORD	04010400h, 04030402h, 04010400h, 04030402h

.CODE

ALIGN 16
Image_Normalize_2	PROC
	MOVDQU xmm12, [rdx]
	MOVDQU xmm14, [rdx+16]

	PXOR xmm10, xmm10
	
	MOV eax, 80008000h
	MOVD xmm11, eax
	PSHUFD xmm11, xmm11, 0
	MOVDQA xmm13, xmm11
	PUNPCKLWD xmm13, xmm10
		
	MOVDQA xmm15, xmm14

	PUNPCKLDQ xmm14, xmm10
	PUNPCKHDQ xmm15, xmm10


	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; hright
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; width

	SHR r8d, 3
	MOV eax, r8d

align 16
	row_loop:
		MOVDQA xmm0, [r11]
		MOVDQA xmm4, [r11+16]

		MOVDQA xmm8, [r11+32]
		MOVDQA xmm9, [r11+48]

		MOVDQA xmm2, xmm0
		MOVDQA xmm6, xmm4

		PUNPCKLWD xmm0, xmm10
		PUNPCKHWD xmm2, xmm10
		PUNPCKLWD xmm4, xmm10
		PUNPCKHWD xmm6, xmm10

		
		PSUBD xmm0, xmm12
		PSUBD xmm2, xmm12
		PSUBD xmm4, xmm12
		PSUBD xmm6, xmm12
		
		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PCMPGTD xmm1, xmm10
		PCMPGTD xmm3, xmm10
		PCMPGTD xmm5, xmm10
		PCMPGTD xmm7, xmm10

		PAND xmm0, xmm1
		PAND xmm2, xmm3
		PAND xmm4, xmm5
		PAND xmm6, xmm7

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm0, xmm10
		PUNPCKHDQ xmm1, xmm10
		PMULUDQ xmm0, xmm14
		PMULUDQ xmm1, xmm15
		PSRLQ xmm0, 12
		PSRLQ xmm1, 12
		PSHUFD xmm0, xmm0, 0F8h
		PSHUFD xmm1, xmm1, 085h
		POR xmm0, xmm1

		PUNPCKLDQ xmm2, xmm10
		PUNPCKHDQ xmm3, xmm10
		PMULUDQ xmm2, xmm14
		PMULUDQ xmm3, xmm15
		PSRLQ xmm2, 12
		PSRLQ xmm3, 12
		PSHUFD xmm2, xmm2, 0F8h
		PSHUFD xmm3, xmm3, 085h
		POR xmm2, xmm3

		PUNPCKLDQ xmm4, xmm10
		PUNPCKHDQ xmm5, xmm10
		PMULUDQ xmm4, xmm14
		PMULUDQ xmm5, xmm15
		PSRLQ xmm4, 12
		PSRLQ xmm5, 12
		PSHUFD xmm4, xmm4, 0F8h
		PSHUFD xmm5, xmm5, 085h
		POR xmm4, xmm5

		PUNPCKLDQ xmm6, xmm10
		PUNPCKHDQ xmm7, xmm10
		PMULUDQ xmm6, xmm14
		PMULUDQ xmm7, xmm15
		PSRLQ xmm6, 12
		PSRLQ xmm7, 12
		PSHUFD xmm6, xmm6, 0F8h
		PSHUFD xmm7, xmm7, 085h
		POR xmm6, xmm7

		PSUBD xmm0, xmm13
		PSUBD xmm2, xmm13
		PSUBD xmm4, xmm13
		PSUBD xmm6, xmm13

		PACKSSDW xmm0, xmm2
		PACKSSDW xmm4, xmm6

		PXOR xmm0, xmm11
		PXOR xmm4, xmm11


		MOVDQA xmm2, xmm8
		MOVDQA xmm6, xmm9

		PUNPCKLWD xmm8, xmm10
		PUNPCKHWD xmm2, xmm10
		PUNPCKLWD xmm9, xmm10
		PUNPCKHWD xmm6, xmm10

		
		PSUBD xmm8, xmm12
		PSUBD xmm2, xmm12
		PSUBD xmm9, xmm12
		PSUBD xmm6, xmm12

		MOVDQA xmm1, xmm8
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm9
		MOVDQA xmm7, xmm6

		PCMPGTD xmm1, xmm10
		PCMPGTD xmm3, xmm10
		PCMPGTD xmm5, xmm10
		PCMPGTD xmm7, xmm10

		PAND xmm8, xmm1
		PAND xmm2, xmm3
		PAND xmm9, xmm5
		PAND xmm6, xmm7

		MOVDQA xmm1, xmm8
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm9
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm8, xmm10
		PUNPCKHDQ xmm1, xmm10
		PMULUDQ xmm8, xmm14
		PMULUDQ xmm1, xmm15
		PSRLQ xmm8, 12
		PSRLQ xmm1, 12
		PSHUFD xmm8, xmm8, 0F8h
		PSHUFD xmm1, xmm1, 085h
		POR xmm8, xmm1

		PUNPCKLDQ xmm2, xmm10
		PUNPCKHDQ xmm3, xmm10
		PMULUDQ xmm2, xmm14
		PMULUDQ xmm3, xmm15
		PSRLQ xmm2, 12
		PSRLQ xmm3, 12
		PSHUFD xmm2, xmm2, 0F8h
		PSHUFD xmm3, xmm3, 085h
		POR xmm2, xmm3

		PUNPCKLDQ xmm9, xmm10
		PUNPCKHDQ xmm5, xmm10
		PMULUDQ xmm9, xmm14
		PMULUDQ xmm5, xmm15
		PSRLQ xmm9, 12
		PSRLQ xmm5, 12
		PSHUFD xmm9, xmm9, 0F8h
		PSHUFD xmm5, xmm5, 085h
		POR xmm9, xmm5

		PUNPCKLDQ xmm6, xmm10
		PUNPCKHDQ xmm7, xmm10
		PMULUDQ xmm6, xmm14
		PMULUDQ xmm7, xmm15
		PSRLQ xmm6, 12
		PSRLQ xmm7, 12
		PSHUFD xmm6, xmm6, 0F8h
		PSHUFD xmm7, xmm7, 085h
		POR xmm6, xmm7

		PSUBD xmm8, xmm13
		PSUBD xmm2, xmm13
		PSUBD xmm9, xmm13
		PSUBD xmm6, xmm13

		PACKSSDW xmm8, xmm2
		PACKSSDW xmm9, xmm6

		PXOR xmm8, xmm11
		PXOR xmm9, xmm11

		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+16], xmm4
		MOVNTDQ [r11+32], xmm8
		MOVNTDQ [r11+48], xmm9

		ADD r11, 64

		DEC r8d
		JNZ row_loop

		MOV r8d, eax

	DEC r10d
	JNZ row_loop

	SFENCE

	RET
Image_Normalize_2	ENDP


ALIGN 16
Image_Normalize_1	PROC
	MOVDQU xmm12, [rdx]
	MOVDQU xmm14, [rdx+16]

	PXOR xmm10, xmm10
	PCMPEQD xmm11, xmm11
	MOVDQA xmm13, xmm11
	PSLLD xmm13, 24
		
	MOVDQA xmm15, xmm14

	PUNPCKLDQ xmm14, xmm10
	PUNPCKHDQ xmm15, xmm10

	MOV r11, [rcx]
	MOV r10d, [rcx+12] ; hright
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; width

	SHR r8d, 3
	MOV eax, r8d

align 16
	row_loop:
		MOVDQA xmm0, [r11]
		MOVDQA xmm8, [r11+16]

		MOVDQA xmm4, xmm0		

		PUNPCKLBW xmm0, xmm10
		PUNPCKHBW xmm4, xmm10

		MOVDQA xmm2, xmm0
		MOVDQA xmm6, xmm4

		PUNPCKLWD xmm0, xmm10
		PUNPCKHWD xmm2, xmm10
		PUNPCKLWD xmm4, xmm10
		PUNPCKHWD xmm6, xmm10
		
		PSUBD xmm0, xmm12
		PSUBD xmm2, xmm12
		PSUBD xmm4, xmm12
		PSUBD xmm6, xmm12

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PSRAD xmm1, 31
		PSRAD xmm3, 31
		PSRAD xmm5, 31
		PSRAD xmm7, 31

		PXOR xmm1, xmm11
		PXOR xmm3, xmm11
		PXOR xmm5, xmm11
		PXOR xmm7, xmm11

		PAND xmm0, xmm1
		PAND xmm2, xmm3
		PAND xmm4, xmm5
		PAND xmm6, xmm7

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm0, xmm10
		PUNPCKHDQ xmm1, xmm10
		PMULUDQ xmm0, xmm14
		PMULUDQ xmm1, xmm15
		PSRLQ xmm0, 12
		PSRLQ xmm1, 12
		PSHUFD xmm0, xmm0, 0F8h
		PSHUFD xmm1, xmm1, 085h
		POR xmm0, xmm1

		PUNPCKLDQ xmm2, xmm10
		PUNPCKHDQ xmm3, xmm10
		PMULUDQ xmm2, xmm14
		PMULUDQ xmm3, xmm15
		PSRLQ xmm2, 12
		PSRLQ xmm3, 12
		PSHUFD xmm2, xmm2, 0F8h
		PSHUFD xmm3, xmm3, 085h
		POR xmm2, xmm3

		PUNPCKLDQ xmm4, xmm10
		PUNPCKHDQ xmm5, xmm10
		PMULUDQ xmm4, xmm14
		PMULUDQ xmm5, xmm15
		PSRLQ xmm4, 12
		PSRLQ xmm5, 12
		PSHUFD xmm4, xmm4, 0F8h
		PSHUFD xmm5, xmm5, 085h
		POR xmm4, xmm5

		PUNPCKLDQ xmm6, xmm10
		PUNPCKHDQ xmm7, xmm10
		PMULUDQ xmm6, xmm14
		PMULUDQ xmm7, xmm15
		PSRLQ xmm6, 12
		PSRLQ xmm7, 12
		PSHUFD xmm6, xmm6, 0F8h
		PSHUFD xmm7, xmm7, 085h
		POR xmm6, xmm7

		PACKSSDW xmm0, xmm2
		PACKSSDW xmm4, xmm6

		PACKUSWB xmm0, xmm4
		POR xmm0, xmm13




		MOVDQA xmm4, xmm8		

		PUNPCKLBW xmm8, xmm10
		PUNPCKHBW xmm4, xmm10

		MOVDQA xmm2, xmm8
		MOVDQA xmm6, xmm4

		PUNPCKLWD xmm8, xmm10
		PUNPCKHWD xmm2, xmm10
		PUNPCKLWD xmm4, xmm10
		PUNPCKHWD xmm6, xmm10
		
		PSUBD xmm8, xmm12
		PSUBD xmm2, xmm12
		PSUBD xmm4, xmm12
		PSUBD xmm6, xmm12

		MOVDQA xmm1, xmm8
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PSRAD xmm1, 31
		PSRAD xmm3, 31
		PSRAD xmm5, 31
		PSRAD xmm7, 31

		PXOR xmm1, xmm11
		PXOR xmm3, xmm11
		PXOR xmm5, xmm11
		PXOR xmm7, xmm11

		PAND xmm8, xmm1
		PAND xmm2, xmm3
		PAND xmm4, xmm5
		PAND xmm6, xmm7


		MOVDQA xmm1, xmm8
		MOVDQA xmm3, xmm2
		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm8, xmm10
		PUNPCKHDQ xmm1, xmm10
		PMULUDQ xmm8, xmm14
		PMULUDQ xmm1, xmm15
		PSRLQ xmm8, 12
		PSRLQ xmm1, 12
		PSHUFD xmm8, xmm8, 0F8h
		PSHUFD xmm1, xmm1, 085h
		POR xmm8, xmm1

		PUNPCKLDQ xmm2, xmm10
		PUNPCKHDQ xmm3, xmm10
		PMULUDQ xmm2, xmm14
		PMULUDQ xmm3, xmm15
		PSRLQ xmm2, 12
		PSRLQ xmm3, 12
		PSHUFD xmm2, xmm2, 0F8h
		PSHUFD xmm3, xmm3, 085h
		POR xmm2, xmm3

		PUNPCKLDQ xmm4, xmm10
		PUNPCKHDQ xmm5, xmm10
		PMULUDQ xmm4, xmm14
		PMULUDQ xmm5, xmm15
		PSRLQ xmm4, 12
		PSRLQ xmm5, 12
		PSHUFD xmm4, xmm4, 0F8h
		PSHUFD xmm5, xmm5, 085h
		POR xmm4, xmm5

		PUNPCKLDQ xmm6, xmm10
		PUNPCKHDQ xmm7, xmm10
		PMULUDQ xmm6, xmm14
		PMULUDQ xmm7, xmm15
		PSRLQ xmm6, 12
		PSRLQ xmm7, 12
		PSHUFD xmm6, xmm6, 0F8h
		PSHUFD xmm7, xmm7, 085h
		POR xmm6, xmm7

		PACKSSDW xmm8, xmm2
		PACKSSDW xmm4, xmm6

		PACKUSWB xmm8, xmm4
		POR xmm8, xmm13






		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+16], xmm8

		ADD r11, 32

		DEC r8d
		JNZ row_loop

		MOV r8d, eax

	DEC r10d
	JNZ row_loop

	SFENCE

	RET
Image_Normalize_1	ENDP






ALIGN 16
Image_Nearest_SSE41	PROC
	PUSH r15
	PUSH r14
	PUSH r13

	MOV r15d, [rdx]
	SHR r15d, 2

	LEA rdx, [rdx+16]

	MOV r10d, r8d

	MOV r14, rcx

		MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
		MOV r11d, 1
		SHL r11d, cl
		SUB r11d, 1

	MOV rcx, r14

	ADD r10d, r11d
	NOT r11d
	AND r10d, r11d

	SHL r10, 2

	ADD r8d, 3
	SHR r8d, 2

	PCMPEQD xmm15, xmm15
	PXOR xmm14, xmm14
	MOVDQA xmm13, XMMWORD PTR [dword_one]
	
	SUB rcx, r10
align 16
	ro_loop:
		ADD rcx, r10
		
		MOV r11, rcx
		MOV eax, r8d

	align 16
		co_loop:
			MOVDQA xmm0, [r11]			
			MOVDQA xmm1, xmm0

			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14

			PXOR xmm10, xmm10
			PXOR xmm11, xmm11
			PCMPEQD xmm6, xmm6

			MOV r14d, r15d
			MOV r13, rdx
		align 16
			color_search:
				MOVDQA xmm2, [r13]
				MOVDQA xmm3, xmm2

				PUNPCKLBW xmm2, xmm14
				PUNPCKHBW xmm3, xmm14

				PSHUFD xmm4, xmm2, 44h
				MOVDQA xmm5, xmm4

				PSUBW xmm4, xmm0
				PSUBW xmm5, xmm1

				PMADDWD xmm4, xmm4
				PMADDWD xmm5, xmm5
				PHADDD xmm4, xmm5

				PMINUD xmm6, xmm4

				PCMPEQD xmm4, xmm6
				PXOR xmm4, xmm15
				PAND xmm10, xmm4
				PXOR xmm4, xmm15
				PAND xmm4, xmm11
				POR xmm10, xmm4

				PADDD xmm11, xmm13

				PSHUFD xmm4, xmm2, 0EEh
				MOVDQA xmm5, xmm4

				PSUBW xmm4, xmm0
				PSUBW xmm5, xmm1

				PMADDWD xmm4, xmm4
				PMADDWD xmm5, xmm5
				PHADDD xmm4, xmm5

				PMINUD xmm6, xmm4

				PCMPEQD xmm4, xmm6
				PXOR xmm4, xmm15
				PAND xmm10, xmm4
				PXOR xmm4, xmm15
				PAND xmm4, xmm11
				POR xmm10, xmm4

				PADDD xmm11, xmm13

				PSHUFD xmm4, xmm3, 44h
				MOVDQA xmm5, xmm4

				PSUBW xmm4, xmm0
				PSUBW xmm5, xmm1

				PMADDWD xmm4, xmm4
				PMADDWD xmm5, xmm5
				PHADDD xmm4, xmm5

				PMINUD xmm6, xmm4

				PCMPEQD xmm4, xmm6
				PXOR xmm4, xmm15
				PAND xmm10, xmm4
				PXOR xmm4, xmm15
				PAND xmm4, xmm11
				POR xmm10, xmm4

				PADDD xmm11, xmm13


				PSHUFD xmm4, xmm3, 0EEh
				MOVDQA xmm5, xmm4

				PSUBW xmm4, xmm0
				PSUBW xmm5, xmm1

				PMADDWD xmm4, xmm4
				PMADDWD xmm5, xmm5
				PHADDD xmm4, xmm5

				PMINUD xmm6, xmm4

				PCMPEQD xmm4, xmm6
				PXOR xmm4, xmm15
				PAND xmm10, xmm4
				PXOR xmm4, xmm15
				PAND xmm4, xmm11
				POR xmm10, xmm4

				PADDD xmm11, xmm13


				ADD r13, 16
			DEC r14d
			JNZ color_search

			MOVNTDQ [r11], xmm10

			ADD r11, 16
		DEC eax
		JNZ co_loop
	DEC r9d
	JNZ ro_loop

	SFENCE

	POP r13
	POP r14
	POP r15

	RET
Image_Nearest_SSE41	ENDP



ALIGN 16
Image_Dither_SSE41	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12
	PUSH rsi

	MOV r15d, [rdx]
	SHR r15d, 2

	LEA rdx, [rdx+16]

	PCMPEQD xmm15, xmm15
	PXOR xmm14, xmm14
	

	MOVDQA xmm13, XMMWORD PTR [dword_one]

	MOV r14d, r8d

	MOV r10, rcx

		MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
		MOV r11d, 1
		SHL r11d, cl
		SUB r11d, 1

	MOV rcx, r10

	ADD r14d, r11d
	NOT r11d
	AND r14d, r11d

	SHL r14, 2

	ADD r8d, 3
	SHR r8d, 2

	SUB rcx, r14
align 16
	ro_loop:
		ADD rcx, r14
		MOV rsi, rcx
		MOV r12d, r8d

		PXOR xmm7, xmm7
		PXOR xmm11, xmm11


	align 16
		co_loop:
			
			MOVDQA xmm0, [rsi]
			MOVDQA xmm1, xmm0

			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14

			PSHUFD xmm2, xmm0, 44h
			
			PSLLW xmm2, 4
			PADDW xmm2, xmm7
			PSRAW xmm2, 4

			PCMPEQD xmm12, xmm12
			PXOR xmm5, xmm5
			PXOR xmm6, xmm6

			MOV r11, rdx
			MOV eax, r15d


		align 16
			color_search_1:
				MOVDQA xmm3, [r11]
				MOVDQA xmm4, xmm3

				PUNPCKLBW xmm3, xmm14
				PUNPCKHBW xmm4, xmm14

				PSUBW xmm3, xmm2
				PSUBW xmm4, xmm2


				PMADDWD xmm3, xmm3
				PMADDWD xmm4, xmm4
				
				PHADDD xmm3, xmm4

				PMINUD xmm12, xmm3
				PCMPEQD xmm3, xmm12
		
				PXOR xmm3, xmm15

				PAND xmm5, xmm3
				PXOR xmm3, xmm15

				PAND xmm3, xmm6
				POR xmm5, xmm3

				PADDD xmm6, xmm13
				
				ADD r11, 16
			DEC eax
			JNZ color_search_1


			PSHUFD xmm3, xmm12, 1Bh			
			PMINUD xmm3, xmm12
			PSHUFD xmm4, xmm3, 0B1h
			PMINUD xmm3, xmm4

			PCMPEQD xmm3, xmm12

			PMOVMSKB eax, xmm3

			XOR r11d, r11d
			SHR eax, 4
			JC du_stuff_1
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_1
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_1
			INC r11d
			PSRLDQ xmm5, 4

		align 16
			du_stuff_1:
				MOVD eax, xmm5
				LEA r11, [r11+rax*4]

				MOVD xmm8, r11d

				MOVD xmm3, DWORD PTR [rdx+r11*4]
				PSHUFB xmm3, XMMWORD PTR [co_shuf]				

				MOVDQA xmm6, xmm2

				PSUBW xmm6, xmm3
				MOVDQA xmm4, xmm6

				PSLLW xmm6, 3

				PSHUFD xmm2, xmm0, 0EEh
				PSLLW xmm2, 4

				PADDW xmm2, xmm6
				PSUBW xmm2, xmm4

				PSRAW xmm2, 4				
				PSRAW xmm6, 1

				PSRLDQ xmm6, 8
				MOVDQA xmm5, xmm4
				PADDW xmm5, xmm6 ; 51				
				

				
				CMP r12d, r8d
				JZ no_offload
				CMP r9d, 1
				JZ no_offload
					PSUBW xmm6, xmm4 ; 3
					PSLLDQ xmm6, 8
					PADDW xmm10, xmm6

					MOVDQA xmm3, [rsi+r14-16]
					MOVDQA xmm4, xmm3

					PUNPCKLBW xmm3, xmm14
					PUNPCKHBW xmm4, xmm14

					PSLLW xmm3, 4
					PSLLW xmm4, 4

					PADDW xmm3, xmm9
					PADDW xmm4, xmm10

					PSRAW xmm3, 4
					PSRAW xmm4, 4

					PACKUSWB xmm3, xmm4
					MOVDQA [rsi+r14-16], xmm3
			align 16
				no_offload:


				MOVDQA xmm9, xmm5
				PADDW xmm9, xmm11

			PCMPEQD xmm12, xmm12
			PXOR xmm5, xmm5
			PXOR xmm6, xmm6

			MOV r11, rdx
			MOV eax, r15d


		align 16
			color_search_2:
				MOVDQA xmm3, [r11]
				MOVDQA xmm4, xmm3

				PUNPCKLBW xmm3, xmm14
				PUNPCKHBW xmm4, xmm14

				PSUBW xmm3, xmm2
				PSUBW xmm4, xmm2


				PMADDWD xmm3, xmm3
				PMADDWD xmm4, xmm4
				
				PHADDD xmm3, xmm4

				PMINUD xmm12, xmm3
				PCMPEQD xmm3, xmm12
		
				PXOR xmm3, xmm15

				PAND xmm5, xmm3
				PXOR xmm3, xmm15

				PAND xmm3, xmm6
				POR xmm5, xmm3

				PADDD xmm6, xmm13
				
				ADD r11, 16
			DEC eax
			JNZ color_search_2


			PSHUFD xmm3, xmm12, 1Bh			
			PMINUD xmm3, xmm12
			PSHUFD xmm4, xmm3, 0B1h
			PMINUD xmm3, xmm4

			PCMPEQD xmm3, xmm12

			PMOVMSKB eax, xmm3

			XOR r11d, r11d
			SHR eax, 4
			JC du_stuff_2
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_2
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_2
			INC r11d
			PSRLDQ xmm5, 4

		align 16
			du_stuff_2:
				MOVD eax, xmm5
				LEA r11, [r11+rax*4]

				MOVD xmm5, r11d
				PSLLDQ xmm5, 4
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [rdx+r11*4]
				PSHUFB xmm3, XMMWORD PTR [co_shuf]				

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm10, xmm4

				PSLLW xmm4, 3

				PSHUFD xmm2, xmm1, 044h
				PSLLW xmm2, 4

				PADDW xmm2, xmm4
				PSUBW xmm2, xmm10

				PSRAW xmm2, 4				
				PSRAW xmm4, 1

				PSLLDQ xmm10, 8 ; 1
				PADDW xmm4, xmm10
				PSRLDQ xmm10, 8
				PSUBW xmm4, xmm10 ; 35

				PADDW xmm9, xmm4




			PCMPEQD xmm12, xmm12
			PXOR xmm5, xmm5
			PXOR xmm6, xmm6

			MOV r11, rdx
			MOV eax, r15d


		align 16
			color_search_3:
				MOVDQA xmm3, [r11]
				MOVDQA xmm4, xmm3

				PUNPCKLBW xmm3, xmm14
				PUNPCKHBW xmm4, xmm14

				PSUBW xmm3, xmm2
				PSUBW xmm4, xmm2


				PMADDWD xmm3, xmm3
				PMADDWD xmm4, xmm4
				
				PHADDD xmm3, xmm4

				PMINUD xmm12, xmm3
				PCMPEQD xmm3, xmm12
		
				PXOR xmm3, xmm15

				PAND xmm5, xmm3
				PXOR xmm3, xmm15

				PAND xmm3, xmm6
				POR xmm5, xmm3

				PADDD xmm6, xmm13
				
				ADD r11, 16
			DEC eax
			JNZ color_search_3


			PSHUFD xmm3, xmm12, 1Bh			
			PMINUD xmm3, xmm12
			PSHUFD xmm4, xmm3, 0B1h
			PMINUD xmm3, xmm4

			PCMPEQD xmm3, xmm12

			PMOVMSKB eax, xmm3

			XOR r11d, r11d
			SHR eax, 4
			JC du_stuff_3
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_3
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_3
			INC r11d
			PSRLDQ xmm5, 4

		align 16
			du_stuff_3:
				MOVD eax, xmm5
				LEA r11, [r11+rax*4]

				MOVD xmm5, r11d
				PSLLDQ xmm5, 8
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [rdx+r11*4]
				PSHUFB xmm3, XMMWORD PTR [co_shuf]				

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm5, xmm4

				PSLLW xmm4, 3

				PSHUFD xmm2, xmm1, 0EEh
				PSLLW xmm2, 4

				PADDW xmm2, xmm4
				PSUBW xmm2, xmm5

				PSRAW xmm2, 4
				PSRAW xmm4, 1

				PSRLDQ xmm4, 8				
				PADDW xmm5, xmm4 ; 51
				PADDW xmm10, xmm5
				PSLLDQ xmm5, 8
				PSLLDQ xmm4, 8
				PSUBW xmm4, xmm5
				PADDW xmm9, xmm4

			

			PCMPEQD xmm12, xmm12
			PXOR xmm5, xmm5
			PXOR xmm6, xmm6

			MOV r11, rdx
			MOV eax, r15d


		align 16
			color_search_4:
				MOVDQA xmm3, [r11]
				MOVDQA xmm4, xmm3

				PUNPCKLBW xmm3, xmm14
				PUNPCKHBW xmm4, xmm14

				PSUBW xmm3, xmm2
				PSUBW xmm4, xmm2


				PMADDWD xmm3, xmm3
				PMADDWD xmm4, xmm4
				
				PHADDD xmm3, xmm4

				PMINUD xmm12, xmm3
				PCMPEQD xmm3, xmm12
		
				PXOR xmm3, xmm15

				PAND xmm5, xmm3
				PXOR xmm3, xmm15

				PAND xmm3, xmm6
				POR xmm5, xmm3

				PADDD xmm6, xmm13
				
				ADD r11, 16
			DEC eax
			JNZ color_search_4


			PSHUFD xmm3, xmm12, 1Bh			
			PMINUD xmm3, xmm12
			PSHUFD xmm4, xmm3, 0B1h
			PMINUD xmm3, xmm4

			PCMPEQD xmm3, xmm12

			PMOVMSKB eax, xmm3

			XOR r11d, r11d
			SHR eax, 4
			JC du_stuff_4
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_4
			PSRLDQ xmm5, 4
			INC r11d
			SHR eax, 4
			JC du_stuff_4
			INC r11d
			PSRLDQ xmm5, 4

		align 16
			du_stuff_4:
				MOVD eax, xmm5
				LEA r11, [r11+rax*4]

				MOVD xmm5, r11d
				PSLLDQ xmm5, 12
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [rdx+r11*4]
				PSHUFB xmm3, XMMWORD PTR [co_shuf]				

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm11, xmm4

				PSLLW xmm4, 3

				MOVDQA xmm7, xmm4
				PSUBW xmm7, xmm11

				PSRAW xmm4, 1

				PSLLDQ xmm11, 8 ; 1
				PADDW xmm4, xmm11
				PSRLDQ xmm11, 8
				PSUBW xmm4, xmm11 ; 35

				PADDW xmm10, xmm4
								


; index offload
			MOVNTDQ [rsi], xmm8
			ADD rsi, 16


		DEC r12d
		JNZ co_loop

		CMP r9d, 1
		JZ no_offload_e

			MOVDQA xmm3, [rsi+r14-16]
			MOVDQA xmm4, xmm3

			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14

			PSLLW xmm3, 4
			PSLLW xmm4, 4

			PADDW xmm3, xmm9
			PADDW xmm4, xmm10

			PSRAW xmm3, 4
			PSRAW xmm4, 4

			PACKUSWB xmm3, xmm4
			MOVDQA [rsi+r14-16], xmm3
	align 16
		no_offload_e:


	DEC r9d
	JNZ ro_loop

	SFENCE

	POP rsi
	POP r12
	POP r13
	POP r14
	POP r15

	RET
Image_Dither_SSE41	ENDP


ALIGN 16
Image_Dither_I_SSSE3	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12
	PUSH rsi


	PCMPEQD xmm15, xmm15
	PXOR xmm14, xmm14
	MOVDQA xmm13, XMMWORD PTR [co_shuf]
	

	MOV r14d, r8d
	
	MOV r10, rcx

		MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
		MOV r11d, 1
		SHL r11d, cl
		SUB r11d, 1

	MOV rcx, r10

	ADD r14d, r11d
	NOT r11d
	AND r14d, r11d

	SHL r14, 2
	ADD r8d, 3
	SHR r8d, 2

	MOV r11, rdx

	SUB rcx, r14
align 16
	ro_loop:
		ADD rcx, r14
		MOV rsi, rcx
		MOV r12d, r8d

		PXOR xmm7, xmm7
		PXOR xmm11, xmm11


	align 16
		co_loop:
			
			MOVDQA xmm0, [rsi]
			MOVDQA xmm1, xmm0

			PUNPCKLBW xmm0, xmm14
			PUNPCKHBW xmm1, xmm14

			PSHUFD xmm2, xmm0, 44h
			
			PSLLW xmm2, 4
			PADDW xmm2, xmm7
			PSRAW xmm2, 4


			XOR r15, r15
			MOV eax, [r11+64]
		align 16
			color_search_1:
				MOVD xmm3, eax
				PUNPCKLBW xmm3, xmm14
				MOVDQA xmm4, xmm3

				PCMPGTW xmm4, xmm14
				PAND xmm4, xmm2
				
				PCMPGTW xmm3, xmm4
				MOVD rax, xmm3
				
				MOV rdx, [r11+r15+96]
				TEST rax, rax
				CMOVZ rdx, [r11+r15+104]

				MOV r15, rdx
				SHL r15, 7
				
				MOV eax, [r11+r15+64]
				TEST eax, eax
			JNZ color_search_1



			MOVD xmm8, DWORD PTR [r11+r15+32]

			MOVD xmm3, DWORD PTR [r11+r15+40]
			PSHUFB xmm3, xmm13

			MOVDQA xmm6, xmm2

			PSUBW xmm6, xmm3
			MOVDQA xmm4, xmm6

			PSLLW xmm6, 3

			PSHUFD xmm2, xmm0, 0EEh
			PSLLW xmm2, 4

			PADDW xmm2, xmm6
			PSUBW xmm2, xmm4

			PSRAW xmm2, 4				
			PSRAW xmm6, 1

			PSRLDQ xmm6, 8
			MOVDQA xmm5, xmm4
			PADDW xmm5, xmm6 ; 51				
				

				
			CMP r12d, r8d
			JZ no_offload
			CMP r9d, 1
			JZ no_offload
				PSUBW xmm6, xmm4 ; 3
				PSLLDQ xmm6, 8
				PADDW xmm10, xmm6

				MOVDQA xmm3, [rsi+r14-16]
				MOVDQA xmm4, xmm3

				PUNPCKLBW xmm3, xmm14
				PUNPCKHBW xmm4, xmm14

				PSLLW xmm3, 4
				PSLLW xmm4, 4

				PADDW xmm3, xmm9
				PADDW xmm4, xmm10

				PSRAW xmm3, 4
				PSRAW xmm4, 4

				PACKUSWB xmm3, xmm4
				MOVDQA [rsi+r14-16], xmm3
		align 16
			no_offload:


				MOVDQA xmm9, xmm5
				PADDW xmm9, xmm11


				XOR r15, r15
				MOV eax, [r11+64]
			align 16
				color_search_2:
					MOVD xmm3, eax
					PUNPCKLBW xmm3, xmm14
					MOVDQA xmm4, xmm3

					PCMPGTW xmm4, xmm14
					PAND xmm4, xmm2
				
					PCMPGTW xmm3, xmm4
					MOVD rax, xmm3
				
					MOV rdx, [r11+r15+96]
					TEST rax, rax
					CMOVZ rdx, [r11+r15+104]

					MOV r15, rdx
					SHL r15, 7
					
					MOV eax, [r11+r15+64]
					TEST eax, eax
				JNZ color_search_2



				MOVD xmm5, DWORD PTR [r11+r15+32]
				PSLLDQ xmm5, 4
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [r11+r15+40]
				PSHUFB xmm3, xmm13

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm10, xmm4

				PSLLW xmm4, 3

				PSHUFD xmm2, xmm1, 044h
				PSLLW xmm2, 4

				PADDW xmm2, xmm4
				PSUBW xmm2, xmm10

				PSRAW xmm2, 4				
				PSRAW xmm4, 1

				PSLLDQ xmm10, 8 ; 1
				PADDW xmm4, xmm10
				PSRLDQ xmm10, 8
				PSUBW xmm4, xmm10 ; 35

				PADDW xmm9, xmm4



				XOR r15, r15
				MOV eax, [r11+64]
			align 16
				color_search_3:
					MOVD xmm3, eax
					PUNPCKLBW xmm3, xmm14
					MOVDQA xmm4, xmm3

					PCMPGTW xmm4, xmm14
					PAND xmm4, xmm2
				
					PCMPGTW xmm3, xmm4
					MOVD rax, xmm3
				
					MOV rdx, [r11+r15+96]
					TEST rax, rax
					CMOVZ rdx, [r11+r15+104]

					MOV r15, rdx
					SHL r15, 7
					
					MOV eax, [r11+r15+64]
					TEST eax, eax
				JNZ color_search_3



				MOVD xmm5, DWORD PTR [r11+r15+32]
				PSLLDQ xmm5, 8
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [r11+r15+40]
				PSHUFB xmm3, xmm13

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm5, xmm4

				PSLLW xmm4, 3

				PSHUFD xmm2, xmm1, 0EEh
				PSLLW xmm2, 4

				PADDW xmm2, xmm4
				PSUBW xmm2, xmm5

				PSRAW xmm2, 4
				PSRAW xmm4, 1

				PSRLDQ xmm4, 8				
				PADDW xmm5, xmm4 ; 51
				PADDW xmm10, xmm5
				PSLLDQ xmm5, 8
				PSLLDQ xmm4, 8
				PSUBW xmm4, xmm5
				PADDW xmm9, xmm4

				XOR r15, r15
				MOV eax, [r11+64]
			align 16
				color_search_4:
					MOVD xmm3, eax
					PUNPCKLBW xmm3, xmm14
					MOVDQA xmm4, xmm3

					PCMPGTW xmm4, xmm14
					PAND xmm4, xmm2
				
					PCMPGTW xmm3, xmm4
					MOVD rax, xmm3
				
					MOV rdx, [r11+r15+96]
					TEST rax, rax
					CMOVZ rdx, [r11+r15+104]

					MOV r15, rdx
					SHL r15, 7
					
					MOV eax, [r11+r15+64]
					TEST eax, eax
				JNZ color_search_4



				MOVD xmm5, DWORD PTR [r11+r15+32]
				PSLLDQ xmm5, 12
				POR xmm8, xmm5

				MOVD xmm3, DWORD PTR [r11+r15+40]
				PSHUFB xmm3, xmm13

				MOVDQA xmm4, xmm2

				PSUBW xmm4, xmm3
				MOVDQA xmm11, xmm4

				PSLLW xmm4, 3

				MOVDQA xmm7, xmm4
				PSUBW xmm7, xmm11

				PSRAW xmm4, 1

				PSLLDQ xmm11, 8 ; 1
				PADDW xmm4, xmm11
				PSRLDQ xmm11, 8
				PSUBW xmm4, xmm11 ; 35

				PADDW xmm10, xmm4
								


; index offload
			MOVNTDQ [rsi], xmm8
			ADD rsi, 16


		DEC r12d
		JNZ co_loop

		CMP r9d, 1
		JZ no_offload_e

			MOVDQA xmm3, [rsi+r14-16]
			MOVDQA xmm4, xmm3

			PUNPCKLBW xmm3, xmm14
			PUNPCKHBW xmm4, xmm14

			PSLLW xmm3, 4
			PSLLW xmm4, 4

			PADDW xmm3, xmm9
			PADDW xmm4, xmm10

			PSRAW xmm3, 4
			PSRAW xmm4, 4

			PACKUSWB xmm3, xmm4
			MOVDQA [rsi+r14-16], xmm3
	align 16
		no_offload_e:


	DEC r9d
	JNZ ro_loop

	SFENCE

	POP rsi
	POP r12
	POP r13
	POP r14
	POP r15

	RET
Image_Dither_I_SSSE3	ENDP



ALIGN 16
Image_AddPicture0	PROC
	PXOR xmm7, xmm7
	
	MOV r11, rdx
	XOR rdx, rdx

	MOV eax, [r9 + 12] ; frag offset
	MUL r8
	
	ADD eax, [r9 + 8]
	
	LEA rcx, [rcx+rax*4]
	SHL r8, 2

	MOV rdx, rcx

	MOV r10d, [r9 + 4] ; height
	
	MOV eax, [r9]
	SHR eax, 4
	MOV r9d, eax

align 16
	add_loop:
	
			MOVDQA xmm0, [r11]
			MOVDQA xmm1, [r11+16]
			MOVDQA xmm2, [r11+32]
			MOVDQA xmm3, [r11+48]

			MOVDQA xmm4, xmm0
			MOVDQA xmm5, xmm1
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm5, xmm7

			PAND xmm4, [rcx]
			PAND xmm5, [rcx+16]

			POR xmm0, xmm4
			POR xmm1, xmm5
	
			MOVDQA xmm4, xmm2
			MOVDQA xmm5, xmm3
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm5, xmm7

			PAND xmm4, [rcx+32]
			PAND xmm5, [rcx+48]

			POR xmm2, xmm4
			POR xmm3, xmm5
	

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1
			MOVNTDQ [rcx+32], xmm2
			MOVNTDQ [rcx+48], xmm3

			ADD r11, 64
			ADD rcx, 64

		DEC eax
		JNZ add_loop

		LEA rdx, [rdx+r8]
		
		MOV rcx, rdx
		MOV eax, r9d

	DEC r10d
	JNZ add_loop

	SFENCE

	RET
Image_AddPicture0	ENDP



ALIGN 16
Image_AddPicture1	PROC
	PXOR xmm7, xmm7
	PXOR xmm6, xmm6
	PXOR xmm5, xmm5

	MOV r11, rdx
	XOR rdx, rdx

	MOV eax, [r9 + 12] ; frag offset
	MUL r8
	
	MOV r10d, [r9 + 8]
	AND r10d, 0FFFFFFFCh
	ADD eax, r10d
	
	LEA rcx, [rcx+rax*4]
	SHL r8, 2

	MOV rdx, rcx

	MOV r10d, [r9 + 4] ; height
	
	MOV eax, [r9]
	SHR eax, 4
	MOV r9d, eax


align 16
	add_loop:
	
			MOVDQA xmm0, [r11]
			MOVDQA xmm6, xmm0
			PSLLDQ xmm0, 4
			PSRLDQ xmm6, 12
			POR xmm0, xmm5

			MOVDQA xmm1, [r11+16]
			MOVDQA xmm5, xmm1
			PSLLDQ xmm1, 4
			PSRLDQ xmm5, 12
			POR xmm1, xmm6

			MOVDQA xmm2, [r11+32]
			MOVDQA xmm6, xmm2
			PSLLDQ xmm2, 4
			PSRLDQ xmm6, 12
			POR xmm2, xmm5

			MOVDQA xmm3, [r11+48]
			MOVDQA xmm5, xmm3
			PSLLDQ xmm3, 4
			PSRLDQ xmm5, 12
			POR xmm3, xmm6






			MOVDQA xmm4, xmm0
			MOVDQA xmm6, xmm1
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx]
			PAND xmm6, [rcx+16]

			POR xmm0, xmm4
			POR xmm1, xmm6
	
			MOVDQA xmm4, xmm2
			MOVDQA xmm6, xmm3
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx+32]
			PAND xmm6, [rcx+48]

			POR xmm2, xmm4
			POR xmm3, xmm6
	

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1
			MOVNTDQ [rcx+32], xmm2
			MOVNTDQ [rcx+48], xmm3

			ADD r11, 64
			ADD rcx, 64

		DEC eax
		JNZ add_loop


		MOVDQA xmm4, xmm5
		PCMPEQD xmm4, xmm7

		PAND xmm4, [rcx]

		POR xmm4, xmm5
		MOVNTDQ [rcx], xmm4
		

		LEA rdx, [rdx+r8]
		
		MOV rcx, rdx
		MOV eax, r9d

	DEC r10d
	JNZ add_loop

	SFENCE

	RET



	RET
Image_AddPicture1	ENDP


ALIGN 16
Image_AddPicture2	PROC
	PXOR xmm7, xmm7
	PXOR xmm6, xmm6
	PXOR xmm5, xmm5

	MOV r11, rdx
	XOR rdx, rdx

	MOV eax, [r9 + 12] ; frag offset
	MUL r8
	
	MOV r10d, [r9 + 8]
	AND r10d, 0FFFFFFFCh
	ADD eax, r10d
	
	LEA rcx, [rcx+rax*4]
	SHL r8, 2

	MOV rdx, rcx

	MOV r10d, [r9 + 4] ; height
	
	MOV eax, [r9]
	SHR eax, 4
	MOV r9d, eax


align 16
	add_loop:
	
			MOVDQA xmm0, [r11]
			MOVDQA xmm6, xmm0
			PSLLDQ xmm0, 8
			PSRLDQ xmm6, 8
			POR xmm0, xmm5

			MOVDQA xmm1, [r11+16]
			MOVDQA xmm5, xmm1
			PSLLDQ xmm1, 8
			PSRLDQ xmm5, 8
			POR xmm1, xmm6

			MOVDQA xmm2, [r11+32]
			MOVDQA xmm6, xmm2
			PSLLDQ xmm2, 8
			PSRLDQ xmm6, 8
			POR xmm2, xmm5

			MOVDQA xmm3, [r11+48]
			MOVDQA xmm5, xmm3
			PSLLDQ xmm3, 8
			PSRLDQ xmm5, 8
			POR xmm3, xmm6






			MOVDQA xmm4, xmm0
			MOVDQA xmm6, xmm1
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx]
			PAND xmm6, [rcx+16]

			POR xmm0, xmm4
			POR xmm1, xmm6
	
			MOVDQA xmm4, xmm2
			MOVDQA xmm6, xmm3
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx+32]
			PAND xmm6, [rcx+48]

			POR xmm2, xmm4
			POR xmm3, xmm6
	

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1
			MOVNTDQ [rcx+32], xmm2
			MOVNTDQ [rcx+48], xmm3

			ADD r11, 64
			ADD rcx, 64

		DEC eax
		JNZ add_loop


		MOVDQA xmm4, xmm5
		PCMPEQD xmm4, xmm7

		PAND xmm4, [rcx]

		POR xmm4, xmm5
		MOVNTDQ [rcx], xmm4
		

		LEA rdx, [rdx+r8]
		
		MOV rcx, rdx
		MOV eax, r9d

	DEC r10d
	JNZ add_loop

	SFENCE

	RET



	RET
Image_AddPicture2	ENDP

ALIGN 16
Image_AddPicture3	PROC
	PXOR xmm7, xmm7
	PXOR xmm6, xmm6
	PXOR xmm5, xmm5

	MOV r11, rdx
	XOR rdx, rdx

	MOV eax, [r9 + 12] ; frag offset
	MUL r8
	
	MOV r10d, [r9 + 8]
	AND r10d, 0FFFFFFFCh
	ADD eax, r10d
	
	LEA rcx, [rcx+rax*4]
	SHL r8, 2

	MOV rdx, rcx

	MOV r10d, [r9 + 4] ; height
	
	MOV eax, [r9]
	SHR eax, 4
	MOV r9d, eax


align 16
	add_loop:
	
			MOVDQA xmm0, [r11]
			MOVDQA xmm6, xmm0
			PSLLDQ xmm0, 12
			PSRLDQ xmm6, 4
			POR xmm0, xmm5

			MOVDQA xmm1, [r11+16]
			MOVDQA xmm5, xmm1
			PSLLDQ xmm1, 12
			PSRLDQ xmm5, 4
			POR xmm1, xmm6

			MOVDQA xmm2, [r11+32]
			MOVDQA xmm6, xmm2
			PSLLDQ xmm2, 12
			PSRLDQ xmm6, 4
			POR xmm2, xmm5

			MOVDQA xmm3, [r11+48]
			MOVDQA xmm5, xmm3
			PSLLDQ xmm3, 12
			PSRLDQ xmm5, 4
			POR xmm3, xmm6






			MOVDQA xmm4, xmm0
			MOVDQA xmm6, xmm1
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx]
			PAND xmm6, [rcx+16]

			POR xmm0, xmm4
			POR xmm1, xmm6
	
			MOVDQA xmm4, xmm2
			MOVDQA xmm6, xmm3
			PCMPEQD xmm4, xmm7
			PCMPEQD xmm6, xmm7

			PAND xmm4, [rcx+32]
			PAND xmm6, [rcx+48]

			POR xmm2, xmm4
			POR xmm3, xmm6
	

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx+16], xmm1
			MOVNTDQ [rcx+32], xmm2
			MOVNTDQ [rcx+48], xmm3

			ADD r11, 64
			ADD rcx, 64

		DEC eax
		JNZ add_loop


		MOVDQA xmm4, xmm5
		PCMPEQD xmm4, xmm7

		PAND xmm4, [rcx]

		POR xmm4, xmm5
		MOVNTDQ [rcx], xmm4
		

		LEA rdx, [rdx+r8]
		
		MOV rcx, rdx
		MOV eax, r9d

	DEC r10d
	JNZ add_loop

	SFENCE

	RET



	RET
Image_AddPicture3	ENDP



END

