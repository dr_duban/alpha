
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


; EXTERN	?size_paragraph@Image@Pics@@1IB:DWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
word_32	WORD	32, 32, 32, 32, 32, 32, 32, 32
word_8	WORD	8, 8, 8, 8, 8, 8, 8, 8
mask_5	DWORD	0F8F8F8h, 0F8F8F8h, 0F8F8F8h, 0F8F8F8h

.CODE
Image_Build3DHistogram	PROC

	PUSH rsi



	MOV rsi, rdx

	MOV r10, r8

	MOV rax, rcx

		MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
		MOV r11d, 1
		SHL r11d, cl
		SUB r11d, 1

	MOV rcx, rax

	ADD r10d, r11d
	NOT r11d
	AND r10d, r11d

	SHL r10, 2 ; cwi


	SHR r8d, 2
	MOV r11d, r8d

	PXOR xmm15, xmm15
	MOVDQA xmm14, XMMWORD PTR [mask_5]

; set table


align 16
	pi_loop:
		MOVDQA xmm0, [rsi]
		MOVDQA xmm1, xmm0
		MOVDQA xmm4, xmm0
	
		PUNPCKLBW xmm0, xmm15
		PUNPCKHBW xmm1, xmm15
		
		MOVDQA xmm2, xmm0
		MOVDQA xmm8, xmm0
		MOVDQA xmm3, xmm1		
		MOVDQA xmm9, xmm1

		PMULLW xmm8, xmm8
		PMULLW xmm9, xmm9

		MOVDQA xmm10, xmm8
		MOVDQA xmm11, xmm9

		PUNPCKLWD xmm8, xmm15
		PUNPCKHWD xmm10, xmm15
		PUNPCKLWD xmm9, xmm15
		PUNPCKHWD xmm11, xmm15

		PUNPCKLWD xmm0, xmm15
		PUNPCKHWD xmm2, xmm15
		PUNPCKLWD xmm1, xmm15
		PUNPCKHWD xmm3, xmm15



		
		PAND xmm4, xmm14
		
		MOVD eax, xmm4 ; index		
		ROR eax, 8
		SHR ax, 3
		ROR eax, 5
		SHR ax, 3
		ROL eax, 14
		
		LEA rax, [rax+rax*4]

		PSRLDQ xmm4, 4
	
		MOVDQA xmm6, xmm0
		MOVDQA xmm7, xmm8

		PUNPCKLDQ xmm0, xmm15
		PUNPCKHDQ xmm6, xmm15
		PUNPCKLDQ xmm8, xmm15
		PUNPCKHDQ xmm7, xmm15

		PADDQ xmm0, [rcx+rax]
		PADDQ xmm6, [rcx+rax+16]
		PADDQ xmm8, [rcx+rax+32]
		PADDQ xmm7, [rcx+rax+48]

		MOVDQA [rcx+rax], xmm0
		MOVDQA [rcx+rax+16], xmm6
		MOVDQA [rcx+rax+32], xmm8
		MOVDQA [rcx+rax+48], xmm7

		INC QWORD PTR [rcx+rax+64]




		MOVD eax, xmm4 ; index		
		ROR eax, 8
		SHR ax, 3
		ROR eax, 5
		SHR ax, 3
		ROL eax, 14
				
		LEA rax, [rax+rax*4]

		PSRLDQ xmm4, 4
	
		MOVDQA xmm6, xmm2
		MOVDQA xmm7, xmm10

		PUNPCKLDQ xmm2, xmm15
		PUNPCKHDQ xmm6, xmm15
		PUNPCKLDQ xmm10, xmm15
		PUNPCKHDQ xmm7, xmm15

		PADDQ xmm2, [rcx+rax]
		PADDQ xmm6, [rcx+rax+16]
		PADDQ xmm10, [rcx+rax+32]
		PADDQ xmm7, [rcx+rax+48]

		MOVDQA [rcx+rax], xmm2
		MOVDQA [rcx+rax+16], xmm6
		MOVDQA [rcx+rax+32], xmm10
		MOVDQA [rcx+rax+48], xmm7

		INC QWORD PTR [rcx+rax+64]



		MOVD eax, xmm4 ; index		
		ROR eax, 8
		SHR ax, 3
		ROR eax, 5
		SHR ax, 3
		ROL eax, 14

		LEA rax, [rax+rax*4]

		PSRLDQ xmm4, 4
	
		MOVDQA xmm6, xmm1
		MOVDQA xmm7, xmm9

		PUNPCKLDQ xmm1, xmm15
		PUNPCKHDQ xmm6, xmm15
		PUNPCKLDQ xmm9, xmm15
		PUNPCKHDQ xmm7, xmm15

		PADDQ xmm1, [rcx+rax]
		PADDQ xmm6, [rcx+rax+16]
		PADDQ xmm9, [rcx+rax+32]
		PADDQ xmm7, [rcx+rax+48]

		MOVDQA [rcx+rax], xmm1
		MOVDQA [rcx+rax+16], xmm6
		MOVDQA [rcx+rax+32], xmm9
		MOVDQA [rcx+rax+48], xmm7

		INC QWORD PTR [rcx+rax+64]



		MOVD eax, xmm4 ; index		
		ROR eax, 8
		SHR ax, 3
		ROR eax, 5
		SHR ax, 3
		ROL eax, 14

		LEA rax, [rax+rax*4]

	
		MOVDQA xmm6, xmm3
		MOVDQA xmm7, xmm11

		PUNPCKLDQ xmm3, xmm15
		PUNPCKHDQ xmm6, xmm15
		PUNPCKLDQ xmm11, xmm15
		PUNPCKHDQ xmm7, xmm15

		PADDQ xmm3, [rcx+rax]
		PADDQ xmm6, [rcx+rax+16]
		PADDQ xmm11, [rcx+rax+32]
		PADDQ xmm7, [rcx+rax+48]

		MOVDQA [rcx+rax], xmm3
		MOVDQA [rcx+rax+16], xmm6
		MOVDQA [rcx+rax+32], xmm11
		MOVDQA [rcx+rax+48], xmm7

		INC QWORD PTR [rcx+rax+64]


		ADD rsi, 16

	DEC r11d
	JNZ pi_loop

	MOV r11d, r8d
	ADD rdx, r10
	MOV rsi, rdx

	DEC r9d
	JNZ pi_loop









; format table

	MOV r11, rcx
	MOV rsi, rcx



	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3
	PXOR xmm4, xmm4
		
	MOV r10d, 32

align 16
	r00_loop:
		PADDQ xmm0, [rsi]
		PADDQ xmm1, [rsi+16]
		PADDQ xmm2, [rsi+32]
		PADDQ xmm3, [rsi+48]
		PADDQ xmm4, [rsi+64]

		MOVDQA [rsi], xmm0
		MOVDQA [rsi+16], xmm1
		MOVDQA [rsi+32], xmm2
		MOVDQA [rsi+48], xmm3
		MOVDQA [rsi+64], xmm4

		ADD rsi, 80

	DEC r10d
	JNZ r00_loop


	SUB rsi, r11

	MOV r9d, 31


	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3
	PXOR xmm4, xmm4

	MOV r10d, 32
	
	align 16
		rg0_loop:
			MOVDQA xmm5, [r11]
			MOVDQA xmm6, [r11+16]
			MOVDQA xmm7, [r11+32]
			MOVDQA xmm8, [r11+48]
			MOVDQA xmm9, [r11+64]
			
			PADDQ xmm0, [rsi+r11]
			PADDQ xmm1, [rsi+r11+16]
			PADDQ xmm2, [rsi+r11+32]
			PADDQ xmm3, [rsi+r11+48]
			PADDQ xmm4, [rsi+r11+64]		
	
			PADDQ xmm5, xmm0
			PADDQ xmm6, xmm1
			PADDQ xmm7, xmm2
			PADDQ xmm8, xmm3
			PADDQ xmm9, xmm4
			
			MOVDQA [rsi+r11], xmm5
			MOVDQA [rsi+r11+16], xmm6
			MOVDQA [rsi+r11+32], xmm7
			MOVDQA [rsi+r11+48], xmm8
			MOVDQA [rsi+r11+64], xmm9
	
			ADD r11, 80
		DEC r10d
		JNZ rg0_loop

		PXOR xmm0, xmm0
		PXOR xmm1, xmm1
		PXOR xmm2, xmm2
		PXOR xmm3, xmm3
		PXOR xmm4, xmm4

		MOV r10d, 32

	DEC r9d
	JNZ rg0_loop


	ADD r11, rsi	
	SUB rcx, r11
	
	MOV r8d, 31
	MOV r10d, 32


	PXOR xmm0, xmm0
	PXOR xmm1, xmm1
	PXOR xmm2, xmm2
	PXOR xmm3, xmm3
	PXOR xmm4, xmm4
	

align 16
	rgb_loop:

			MOVDQA xmm5, [rcx+r11]
			MOVDQA xmm6, [rcx+r11+16]
			MOVDQA xmm7, [rcx+r11+32]
			MOVDQA xmm8, [rcx+r11+48]
			MOVDQA xmm9, [rcx+r11+64]

			PADDQ xmm0, [r11]
			PADDQ xmm1, [r11+16]
			PADDQ xmm2, [r11+32]
			PADDQ xmm3, [r11+48]
			PADDQ xmm4, [r11+64]

			PADDQ xmm5, xmm0
			PADDQ xmm6, xmm1
			PADDQ xmm7, xmm2
			PADDQ xmm8, xmm3
			PADDQ xmm9, xmm4

			MOVDQA [r11], xmm5
			MOVDQA [r11+16], xmm6
			MOVDQA [r11+32], xmm7
			MOVDQA [r11+48], xmm8
			MOVDQA [r11+64], xmm9

			ADD r11, 80

		DEC r10d
		JNZ rgb_loop


		MOV r9d, 31
		MOV r10d, 32

		PXOR xmm0, xmm0
		PXOR xmm1, xmm1
		PXOR xmm2, xmm2
		PXOR xmm3, xmm3
		PXOR xmm4, xmm4
			

	
	align 16
		rgj_loop:
				MOVDQA xmm5, [rcx+r11]
				MOVDQA xmm6, [rcx+r11+16]
				MOVDQA xmm7, [rcx+r11+32]
				MOVDQA xmm8, [rcx+r11+48]
				MOVDQA xmm9, [rcx+r11+64]
				
				SUB r11, rsi

					PSUBQ xmm5, [rcx+r11]
					PSUBQ xmm6, [rcx+r11+16]
					PSUBQ xmm7, [rcx+r11+32]
					PSUBQ xmm8, [rcx+r11+48]
					PSUBQ xmm9, [rcx+r11+64]

					PADDQ xmm5, [r11]
					PADDQ xmm6, [r11+16]
					PADDQ xmm7, [r11+32]
					PADDQ xmm8, [r11+48]
					PADDQ xmm9, [r11+64]

				ADD r11, rsi

				PADDQ xmm0, [r11]
				PADDQ xmm1, [r11+16]
				PADDQ xmm2, [r11+32]
				PADDQ xmm3, [r11+48]
				PADDQ xmm4, [r11+64]
	

				PADDQ xmm5, xmm0
				PADDQ xmm6, xmm1
				PADDQ xmm7, xmm2
				PADDQ xmm8, xmm3
				PADDQ xmm9, xmm4

			
				MOVDQA [r11], xmm5
				MOVDQA [r11+16], xmm6
				MOVDQA [r11+32], xmm7
				MOVDQA [r11+48], xmm8
				MOVDQA [r11+64], xmm9
	
				ADD r11, 80

			DEC r10d
			JNZ rgj_loop

			PXOR xmm0, xmm0
			PXOR xmm1, xmm1
			PXOR xmm2, xmm2
			PXOR xmm3, xmm3
			PXOR xmm4, xmm4

			MOV r10d, 32

		DEC r9d
		JNZ rgj_loop
		

	DEC r8d
	JNZ rgb_loop

	
	POP rsi

	RET
Image_Build3DHistogram	ENDP


ALIGN 16
Image_ConfigureVolumes	PROC
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


	SHL rdx, 7
	SHL r8, 7

	ADD r8, rcx
	ADD rcx, rdx


	PXOR xmm15, xmm15
	PCMPEQD xmm14, xmm14

	MOVD xmm0, DWORD PTR [rcx+64] ; plane 0
	PUNPCKLBW xmm0, xmm15

	MOVDQA xmm1, [rcx+80] ; volume 0
	MOVDQA xmm2, xmm1

	MOVDQA xmm3, xmm0
	PCMPEQW xmm3, xmm15

	PAND xmm1, xmm3
	POR xmm1, xmm0 ; volume 2

	MOVDQA [r8+192], xmm15 ; plane 2
	MOVDQA [r8+208], xmm1

	PSLLDQ xmm0, 8

	PSHUFD xmm3, xmm3, 04Eh

	PAND xmm2, xmm3
	POR xmm2, xmm0 ; volume 1
	
	MOVDQA [r8+64], xmm15 ; plane 1
	MOVDQA [r8+80], xmm2


; volume 1
	
	PSUBW xmm2, XMMWORD PTR [word_8]
	PSRLW xmm2, 3	
	MOVDQA xmm3, xmm2

	PUNPCKLWD xmm2, xmm15
	PUNPCKHWD xmm3, xmm15

	MOVD r10d, xmm3 ; rh
	PSRLDQ xmm3, 4
	MOVD r11d, xmm3 ; gh
	PSRLDQ xmm3, 4
	MOVD r12d, xmm3 ; bh

	MOVD r13d, xmm2 ; rl
	PSRLDQ xmm2, 4
	MOVD r14d, xmm2 ; gl
	PSRLDQ xmm2, 4
	MOVD r15d, xmm2 ; bl

	MOV edx, 8420h

	CMP r10d, 31
	CMOVA r10d, edx
	CMP r11d, 31
	CMOVA r11d, edx
	CMP r12d, 31
	CMOVA r12d, edx

	CMP r13d, 31
	CMOVA r13d, edx
	CMP r14d, 31
	CMOVA r14d, edx
	CMP r15d, 31
	CMOVA r15d, edx


	SHL r10d, 4
	SHL r11d, 9
	SHL r12d, 14

	SHL r13d, 4
	SHL r14d, 9
	SHL r15d, 14

	MOV edx, 80000h

; hhh
	MOV rax, r10
	OR rax, r11
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	MOVDQA xmm2, [r9+rax]
	MOVDQA xmm3, [r9+rax+16]
	MOVDQA xmm4, [r9+rax+32]
	MOVDQA xmm5, [r9+rax+48]
	MOVDQA xmm6, [r9+rax+64]

; lll
	MOV rax, r13
	OR rax, r14
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; llh
	MOV rax, r13
	OR rax, r14
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; hhl
	MOV rax, r10
	OR rax, r11
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; hll
	MOV rax, r10
	OR rax, r14
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; lhh
	MOV rax, r13
	OR rax, r11
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; lhl
	MOV rax, r13
	OR rax, r11
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; hlh
	MOV rax, r10
	OR rax, r14
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]
	

	MOVDQA [r8], xmm2
	MOVDQA [r8+16], xmm3
	MOVDQA [r8+32], xmm4
	MOVDQA [r8+48], xmm5

	MOVD r10, xmm6

	XOR rdx, rdx
	MOV rax, [r8]
	MOV r12, rax
	DIV r10
	MOV r15, rdx
	MOV [r8], rax
	MUL r12
	MOV r12, rax
	MOV rax, r15
	MUL QWORD PTR [r8]
	ADD r12, rax

	XOR rdx, rdx
	MOV rax, [r8+8]
	MOV r13, rax
	DIV r10
	MOV r15, rdx
	MOV [r8+8], rax
	MUL r13
	MOV r13, rax
	MOV rax, r15
	MUL QWORD PTR [r8+8]
	ADD r13, rax

	XOR rdx, rdx
	MOV rax, [r8+16]
	MOV r14, rax
	DIV r10
	MOV r15, rdx
	MOV [r8+16], rax
	MUL r14
	MOV r14, rax
	MOV rax, r15
	MUL QWORD PTR [r8+16]
	ADD r14, rax

	XOR rdx, rdx
	MOV rax, [r8+32]	
	SUB rax, r12
	DIV r10
	MOV [r8+32], rax
	MOV r12, rax

	XOR rdx, rdx
	MOV rax, [r8+40]
	SUB rax, r13
	DIV r10
	MOV [r8+40], rax
	MOV r13, rax

	XOR rdx, rdx
	MOV rax, [r8+48]
	SUB rax, r14
	DIV r10
	MOV [r8+48], r10
	
	MOV rdx, 2
	XOR r10, r10

	CMP rax, r13
	CMOVB rax, r13
	SBB rdx, 0
	CMP rax, r12
	CMOVB rax, r12
	CMOVB rdx, r10

	MOV [r8+24], rdx
	MOV [r8+56], rax



; volume 2
	
	PSUBW xmm1, XMMWORD PTR [word_8]
	PSRLW xmm1, 3
	MOVDQA xmm3, xmm1

	PUNPCKLWD xmm1, xmm15
	PUNPCKHWD xmm3, xmm15

	MOVD r10d, xmm3 ; rh
	PSRLDQ xmm3, 4
	MOVD r11d, xmm3 ; gh
	PSRLDQ xmm3, 4
	MOVD r12d, xmm3 ; bh

	MOVD r13d, xmm1 ; rl
	PSRLDQ xmm1, 4
	MOVD r14d, xmm1 ; gl
	PSRLDQ xmm1, 4
	MOVD r15d, xmm1 ; bl


	MOV edx, 8420h

	CMP r10d, 31
	CMOVA r10d, edx
	CMP r11d, 31
	CMOVA r11d, edx
	CMP r12d, 31
	CMOVA r12d, edx

	CMP r13d, 31
	CMOVA r13d, edx
	CMP r14d, 31
	CMOVA r14d, edx
	CMP r15d, 31
	CMOVA r15d, edx


	SHL r10d, 4
	SHL r11d, 9
	SHL r12d, 14

	SHL r13d, 4
	SHL r14d, 9
	SHL r15d, 14

	MOV edx, 80000h

; hhh
	MOV rax, r10
	OR rax, r11
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	MOVDQA xmm2, [r9+rax]
	MOVDQA xmm3, [r9+rax+16]
	MOVDQA xmm4, [r9+rax+32]
	MOVDQA xmm5, [r9+rax+48]
	MOVDQA xmm6, [r9+rax+64]

; lll
	MOV rax, r13
	OR rax, r14
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; llh
	MOV rax, r13
	OR rax, r14
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; hhl
	MOV rax, r10
	OR rax, r11
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; hll
	MOV rax, r10
	OR rax, r14
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; lhh
	MOV rax, r13
	OR rax, r11
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]

; lhl
	MOV rax, r13
	OR rax, r11
	OR rax, r15
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PADDQ xmm2, [r9+rax]
	PADDQ xmm3, [r9+rax+16]
	PADDQ xmm4, [r9+rax+32]
	PADDQ xmm5, [r9+rax+48]
	PADDQ xmm6, [r9+rax+64]

; hlh
	MOV rax, r10
	OR rax, r14
	OR rax, r12
	TEST rax, rdx
	CMOVNZ rax, rdx
	LEA rax, [rax+rax*4]
	PSUBQ xmm2, [r9+rax]
	PSUBQ xmm3, [r9+rax+16]
	PSUBQ xmm4, [r9+rax+32]
	PSUBQ xmm5, [r9+rax+48]
	PSUBQ xmm6, [r9+rax+64]
	

	MOVDQA [r8+128], xmm2
	MOVDQA [r8+144], xmm3
	MOVDQA [r8+160], xmm4
	MOVDQA [r8+176], xmm5

	MOVD r10, xmm6


	XOR rdx, rdx
	MOV rax, [r8+128]
	MOV r12, rax
	DIV r10
	MOV r15, rdx
	MOV [r8+128], rax
	MUL r12
	MOV r12, rax
	MOV rax, r15
	MUL QWORD PTR [r8+128]
	ADD r12, rax

	XOR rdx, rdx
	MOV rax, [r8+136]
	MOV r13, rax
	DIV r10
	MOV r15, rdx
	MOV [r8+136], rax
	MUL r13
	MOV r13, rax
	MOV rax, r15
	MUL QWORD PTR [r8+136]
	ADD r13, rax

	XOR rdx, rdx
	MOV rax, [r8+144]
	MOV r14, rax
	DIV r10
	MOV r15, rdx
	MOV [r8+144], rax
	MUL r14
	MOV r14, rax
	MOV rax, r15
	MUL QWORD PTR [r8+144]
	ADD r14, rax

	XOR rdx, rdx
	MOV rax, [r8+160]
	SUB rax, r12
	DIV r10
	MOV [r8+160], rax
	MOV r12, rax

	XOR rdx, rdx
	MOV rax, [r8+168]
	SUB rax, r13
	DIV r10
	MOV [r8+168], rax
	MOV r13, rax

	XOR rdx, rdx
	MOV rax, [r8+176]
	SUB rax, r14
	DIV r10
	MOV [r8+176], r10
	
		
	MOV rdx, 2
	XOR r10, r10

	CMP rax, r13
	CMOVB rax, r13
	SBB rdx, 0
	CMP rax, r12
	CMOVB rax, r12
	CMOVB rdx, r10

	MOV [r8+152], rdx
	MOV [r8+184], rax



	POP r15
	POP r14
	POP r13
	POP r12

	RET
Image_ConfigureVolumes	ENDP

END

