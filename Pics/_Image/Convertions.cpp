/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\Image.h"


unsigned int Pics::Image_YUY22RGBA(unsigned int * pipo, const unsigned char * src, const unsigned int * box_vals) {
	int c(0), d(0), e(0), r(0), g(0), b(0);
	unsigned int wi(box_vals[0]>>1), hi(box_vals[1]), loff(box_vals[2]-box_vals[0]), xpo(0), result(0);
				
	for (unsigned int i(0); i < hi; i++) {
		for (unsigned int j(0); j < wi; j++) {

			c = src[0] - 16;
			d = src[1] - 128;
			e = src[3] - 128;
					
			r = 128 + e*409;
			g = 128 - (d*100 + e*208);
			b = 128 + d*516;
										
			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					
			pipo[xpo] = c | (d<<8) | (e<<16) | 0xFF000000;

			c = src[2] - 16;

			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					
			pipo[xpo+1] = c | (d<<8) | (e<<16) | 0xFF000000;


			pipo += 2;
			src += 4;
		}

		xpo += loff;
		
	}

	return result;

}




unsigned int Pics::Image_I4202RGBA(unsigned int * pipo, const unsigned char * src, const unsigned int * box_vals) {
	int c(0), d(0), e(0), r(0), g(0), b(0);
	unsigned int ie(box_vals[0]>>1), je(box_vals[1]>>1), area(box_vals[0]*box_vals[1]), mline(box_vals[2]), loff(box_vals[2]-box_vals[0]), wi(box_vals[0]), xpo(0), result(0);


	const unsigned char * u = src + area;
	const unsigned char * v = u + (area>>2);

	for (unsigned int j(0); j < je; j++) {
		for (unsigned int i(0); i < ie; i++) {
			c = src[0] - 16;
			d = u[0] - 128;
			e = v[0] - 128;
					
			r = 128 + e*409;
			g = 128 - (d*100 + e*208);
			b = 128 + d*516;
										
			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					

			pipo[xpo] = c | (d<<8) | (e<<16) | 0xFF000000;

			c = src[1] - 16;

			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					
			pipo[xpo+1] = c | (d<<8) | (e<<16) | 0xFF000000;
			
			c = src[wi] - 16;

			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					
			pipo[mline+xpo] = c | (d<<8) | (e<<16) | 0xFF000000;

			c = src[wi+1] - 16;

			d = (g + c*298) >> 8;
			e = (b + c*298) >> 8;
			c = (r + c*298) >> 8;
										
			if (c > 255) c = 255; if (c < 0) c = 0;
			if (d > 255) d = 255; if (d < 0) d = 0;
			if (e > 255) e = 255; if (e < 0) e = 0;
					
			pipo[mline+xpo+1] = c | (d<<8) | (e<<16) | 0xFF000000;
			
			pipo += 2;
			src += 2;
			u++; v++;
		}
		
		src += wi;
		pipo += wi;
		xpo += 2*loff;
				
	}

	return result;

}

unsigned int Pics::Image_BGR2RGBA(unsigned int * pipo, const unsigned char * src, const unsigned int * box_vals) {	
	unsigned int wi(box_vals[0] >> 3), he(box_vals[1]), loff(box_vals[2]+box_vals[0]), result(0);

	pipo += (box_vals[1]-1)*box_vals[2];

	for (unsigned int i(0);i<he;i++) {
		for (unsigned int j(0);j<wi;j++) {
			pipo[0] = (unsigned int)src[2] | ((unsigned int)src[1] << 8) | ((unsigned int)src[0] << 16) | 0xFF000000;
			pipo[1] = (unsigned int)src[5] | ((unsigned int)src[4] << 8) | ((unsigned int)src[3] << 16) | 0xFF000000;
			pipo[2] = (unsigned int)src[8] | ((unsigned int)src[7] << 8) | ((unsigned int)src[6] << 16) | 0xFF000000;
			pipo[3] = (unsigned int)src[11] | ((unsigned int)src[10] << 8) | ((unsigned int)src[9] << 16) | 0xFF000000;
			
			pipo[4] = (unsigned int)src[14] | ((unsigned int)src[13] << 8) | ((unsigned int)src[12] << 16) | 0xFF000000;
			pipo[5] = (unsigned int)src[17] | ((unsigned int)src[16] << 8) | ((unsigned int)src[15] << 16) | 0xFF000000;
			pipo[6] = (unsigned int)src[20] | ((unsigned int)src[19] << 8) | ((unsigned int)src[18] << 16) | 0xFF000000;
			pipo[7] = (unsigned int)src[23] | ((unsigned int)src[22] << 8) | ((unsigned int)src[21] << 16) | 0xFF000000;
			
			pipo += 8;
			src += 24;
		}

		pipo -= loff;
	}

	return result;

}


