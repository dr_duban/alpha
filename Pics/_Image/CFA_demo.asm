
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net

; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




.CODE

ALIGN 16
Image_DeBayer_2RGGB	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHL r9d, 1
	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, [r11+16]

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, [r11+r9+16]

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm0, xmm0 ; r
		PUNPCKHQDQ xmm1, xmm1 ; g

		PUNPCKLQDQ xmm2, xmm2 ; r
		PUNPCKHQDQ xmm3, xmm3 ; g


		PSLLQ xmm1, 16
		PSLLQ xmm3, 16


		PUNPCKLQDQ xmm4, xmm4 ; g
		PUNPCKHQDQ xmm5, xmm5 ; b

		PUNPCKLQDQ xmm6, xmm6 ; g
		PUNPCKHQDQ xmm7, xmm7 ; b


		PSLLQ xmm4, 16
		PSLLQ xmm6, 16

		PSLLQ xmm5, 32
		PSLLQ xmm7, 32


		POR xmm0, xmm5
		POR xmm2, xmm7


		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2


		MOVNTDQ [r11], xmm1
		MOVNTDQ [r11+16], xmm3
		MOVNTDQ [r11+r9], xmm4
		MOVNTDQ [r11+r9+16], xmm6


		ADD r11, 32


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax

	RET
Image_DeBayer_2RGGB	ENDP

ALIGN 16
Image_DeBayer_1RGGB	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, xmm0
		

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, xmm4



		PUNPCKLDQ xmm0, xmm0 ; rrgg
		PUNPCKHDQ xmm2, xmm2 ; rrgg
		MOVDQA xmm1, xmm0
				
		PUNPCKLQDQ xmm0, xmm2 ; r
		PUNPCKHQDQ xmm1, xmm2 ; g
		
		PSLLD xmm1, 8
		
		PUNPCKLDQ xmm4, xmm4 ; ggbb
		PUNPCKHDQ xmm6, xmm6 ; ggbb
		MOVDQA xmm5, xmm4
				
		PUNPCKLQDQ xmm4, xmm6 ; g
		PUNPCKHQDQ xmm5, xmm6 ; b
		
		PSLLD xmm4, 8
		PSLLD xmm5, 16

		POR xmm0, xmm5 ; rb
		
		
		POR xmm1, xmm0
		POR xmm4, xmm0


		MOVNTDQ [r11], xmm1
		MOVNTDQ [r11+r9], xmm4
		

		ADD r11, 16


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax


	RET
Image_DeBayer_1RGGB	ENDP




ALIGN 16
Image_DeBayer_2RGGBu	PROC
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV eax, 0FFFF0000h

	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0

	MOVDQA xmm10, xmm9
	PSRLD xmm10, 16

	MOV r11, [rcx] ; source ptr
	
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	MOV r12d, [rcx+24] ; bcount


	MOV rbx, r11
	MOV rcx, rdx


	MOV eax, r12d
	MUL r8
	SHR rax, 3
	MOV r13, rax


	SHR r8d, 3
		
	MOV rdx, rcx
	MOV r15, rcx

	MOV ecx, r8d
	SHL rcx, 32


	MOV cl, r12b
	MOV ch, cl

	SUB cl, 16
	NEG cl

	SHR r10d, 1
	SHR r12d, 1
	
	SHL r9d, 1

	PXOR xmm8, xmm8
	XOR rax, rax

align 16
	row_loop:
		SHL r10, 32

		MOV rax, [r11]
		

		CMP ch, 16
		JZ load_rax0
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax0:
		MOVD xmm0, rax

		MOV rax, [r11+r12]

		CMP ch, 16
		JZ load_rax1
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax1:
		MOVD xmm2, rax

		PUNPCKLQDQ xmm0, xmm2
		

		MOV rax, [r11+r13]
		
		CMP ch, 16
		JZ load_rax2
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax2:

		MOVD xmm4, rax

		ADD r13, r12
		MOV rax, [r11+r13]
		SUB r13, r12

		CMP ch, 16
		JZ load_rax3
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax3:

		MOVD xmm6, rax

		PUNPCKLQDQ xmm4, xmm6


		SHR r10, 32


		LEA r11, [r11 + r12*2]
	
		MOVDQA xmm1, xmm0

		PAND xmm0, xmm10 ; r low
		PAND xmm1, xmm9 ; g high

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1
				
		PUNPCKLDQ xmm0, xmm9 ; r
		PUNPCKHDQ xmm2, xmm9 ; r

		PUNPCKLDQ xmm1, xmm8 ; g
		PUNPCKHDQ xmm3, xmm8 ; g
		


		MOVDQA xmm5, xmm4
		
		PAND xmm4, xmm10 ; g low
		PAND xmm5, xmm9 ; b high
		
		PSLLD xmm4, 16
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm4, xmm8 ; g
		PUNPCKHDQ xmm6, xmm8 ; g

		PUNPCKLDQ xmm5, xmm8 ; b
		PUNPCKHDQ xmm7, xmm8 ; b

		PSLLQ xmm5, 16
		PSLLQ xmm7, 16

		POR xmm0, xmm5 ; rb
		POR xmm2, xmm7 ; rb

		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2





		MOVDQA xmm0, xmm1
		MOVDQA xmm2, xmm3

		PUNPCKLQDQ xmm0, xmm0
		PUNPCKHQDQ xmm1, xmm1

		PUNPCKLQDQ xmm2, xmm2
		PUNPCKHQDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3




		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm4, xmm4
		PUNPCKHQDQ xmm5, xmm5

		PUNPCKLQDQ xmm6, xmm6
		PUNPCKHQDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8, rcx
			SHR r8, 32

			LEA rbx, [rbx + r13*2]
			LEA r15, [r15 + r9*2]

			MOV r11, rbx
			MOV rdx, r15

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	
	RET
Image_DeBayer_2RGGBu	ENDP



ALIGN 16
Image_DeBayer_1RGGBu	PROC
	PUSH rbx

	MOV eax, 0FF00FF00h
	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0
		
	MOVDQA xmm10, xmm9
	PSRLW xmm10, 8

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	
	MOV rbx, r11
	MOV rcx, rdx

	MOV eax, r8d
	SHR r8d, 4
	SHR r10d, 1

	PXOR xmm8, xmm8

align 16
	row_loop:
		
		MOVDQU xmm0, [r11] ; rg
		MOVDQU xmm4, [r11+rax] ; gb

		ADD r11, 16
	
		MOVDQA xmm1, xmm0
				
		PAND xmm0, xmm10 ; r low
		PAND xmm1, xmm9 ; g high


		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		PUNPCKLWD xmm0, xmm9
		PUNPCKHWD xmm2, xmm9

		PUNPCKLWD xmm1, xmm8
		PUNPCKHWD xmm3, xmm8		
		



		MOVDQA xmm5, xmm4

		PAND xmm4, xmm10 ; g low
		PAND xmm5, xmm9 ; b high

		PSLLW xmm4, 8
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLWD xmm4, xmm8
		PUNPCKHWD xmm6, xmm8

		PUNPCKLWD xmm5, xmm8
		PUNPCKHWD xmm7, xmm8

		PSLLD xmm5, 8
		PSLLD xmm7, 8

		POR xmm0, xmm5 ; rb
		POR xmm2, xmm7 ; rb

		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2


		MOVDQA xmm0, xmm1
		MOVDQA xmm2, xmm3

		PUNPCKLDQ xmm0, xmm0
		PUNPCKHDQ xmm1, xmm1

		PUNPCKLDQ xmm2, xmm2
		PUNPCKHDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3


		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm4, xmm4
		PUNPCKHDQ xmm5, xmm5

		PUNPCKLDQ xmm6, xmm6
		PUNPCKHDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			SHR r8d, 4

			LEA rbx, [rbx + rax*2]
			LEA rcx, [rcx + r9*2]

			MOV r11, rbx
			MOV rdx, rcx

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP rbx

	RET
Image_DeBayer_1RGGBu	ENDP
















ALIGN 16
Image_DeBayer_2GBRG	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHL r9d, 1
	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, [r11+16]

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, [r11+r9+16]

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm0, xmm0 ; g
		PUNPCKHQDQ xmm1, xmm1 ; b

		PUNPCKLQDQ xmm2, xmm2 ; g
		PUNPCKHQDQ xmm3, xmm3 ; b

		PSLLQ xmm0, 16
		PSLLQ xmm2, 16

		PSLLQ xmm1, 32
		PSLLQ xmm3, 32


		PUNPCKLQDQ xmm4, xmm4 ; r
		PUNPCKHQDQ xmm5, xmm5 ; g

		PUNPCKLQDQ xmm6, xmm6 ; r
		PUNPCKHQDQ xmm7, xmm7 ; g


		PSLLQ xmm5, 16
		PSLLQ xmm7, 16


		POR xmm1, xmm4
		POR xmm3, xmm6


		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3


		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+16], xmm2
		MOVNTDQ [r11+r9], xmm5
		MOVNTDQ [r11+r9+16], xmm7


		ADD r11, 32


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax

	RET
Image_DeBayer_2GBRG	ENDP

ALIGN 16
Image_DeBayer_1GBRG	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, xmm0
		

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, xmm4



		PUNPCKLDQ xmm0, xmm0 ; ggbb
		PUNPCKHDQ xmm2, xmm2 ; ggbb
		MOVDQA xmm1, xmm0
				
		PUNPCKLQDQ xmm0, xmm2 ; g
		PUNPCKHQDQ xmm1, xmm2 ; b
		
		PSLLD xmm0, 8
		PSLLD xmm1, 16
		
		PUNPCKLDQ xmm4, xmm4 ; rrgg
		PUNPCKHDQ xmm6, xmm6 ; rrgg
		MOVDQA xmm5, xmm4
				
		PUNPCKLQDQ xmm4, xmm6 ; r
		PUNPCKHQDQ xmm5, xmm6 ; g
		
		PSLLD xmm5, 8

		POR xmm1, xmm4 ; rb
		
		
		POR xmm0, xmm1
		POR xmm5, xmm1


		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+r9], xmm5
		

		ADD r11, 16


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax


	RET
Image_DeBayer_1GBRG	ENDP




ALIGN 16
Image_DeBayer_2GBRGu	PROC
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV eax, 0FFFF0000h

	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0

	MOVDQA xmm10, xmm9
	PSRLD xmm10, 16

	MOV r11, [rcx] ; source ptr
	
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	MOV r12d, [rcx+24] ; bcount


	MOV rbx, r11
	MOV rcx, rdx


	MOV eax, r12d
	MUL r8
	SHR rax, 3
	MOV r13, rax


	SHR r8d, 3
		
	MOV rdx, rcx
	MOV r15, rcx

	MOV ecx, r8d
	SHL rcx, 32


	MOV cl, r12b
	MOV ch, cl

	SUB cl, 16
	NEG cl

	SHR r10d, 1
	SHR r12d, 1
	
	SHL r9d, 1

	PXOR xmm8, xmm8
	XOR rax, rax

align 16
	row_loop:
		SHL r10, 32

		MOV rax, [r11]
		

		CMP ch, 16
		JZ load_rax0
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax0:
		MOVD xmm0, rax

		MOV rax, [r11+r12]

		CMP ch, 16
		JZ load_rax1
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax1:
		MOVD xmm2, rax

		PUNPCKLQDQ xmm0, xmm2
		

		MOV rax, [r11+r13]
		
		CMP ch, 16
		JZ load_rax2
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax2:

		MOVD xmm4, rax

		ADD r13, r12
		MOV rax, [r11+r13]
		SUB r13, r12

		CMP ch, 16
		JZ load_rax3
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax3:

		MOVD xmm6, rax

		PUNPCKLQDQ xmm4, xmm6


		SHR r10, 32


		LEA r11, [r11 + r12*2]
	
		MOVDQA xmm1, xmm0

		PAND xmm0, xmm10 ; g low
		PAND xmm1, xmm9 ; b high

		PSLLD xmm0, 16

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1
				
		PUNPCKLDQ xmm0, xmm8 ; g
		PUNPCKHDQ xmm2, xmm8 ; g

		PUNPCKLDQ xmm1, xmm8 ; b
		PUNPCKHDQ xmm3, xmm8 ; b
		
		PSLLQ xmm1, 16
		PSLLQ xmm3, 16


		MOVDQA xmm5, xmm4
		
		PAND xmm4, xmm10 ; r low
		PAND xmm5, xmm9 ; g high
		
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm4, xmm9 ; r
		PUNPCKHDQ xmm6, xmm9 ; r

		PUNPCKLDQ xmm5, xmm8 ; g
		PUNPCKHDQ xmm7, xmm8 ; g


		POR xmm1, xmm4 ; rb
		POR xmm3, xmm6 ; rb

		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3





		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLQDQ xmm0, xmm0
		PUNPCKHQDQ xmm1, xmm1

		PUNPCKLQDQ xmm2, xmm2
		PUNPCKHQDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3




		MOVDQA xmm4, xmm5
		MOVDQA xmm6, xmm7

		PUNPCKLQDQ xmm4, xmm4
		PUNPCKHQDQ xmm5, xmm5

		PUNPCKLQDQ xmm6, xmm6
		PUNPCKHQDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8, rcx
			SHR r8, 32

			LEA rbx, [rbx + r13*2]
			LEA r15, [r15 + r9*2]

			MOV r11, rbx
			MOV rdx, r15

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	
	RET
Image_DeBayer_2GBRGu	ENDP



ALIGN 16
Image_DeBayer_1GBRGu	PROC
	PUSH rbx

	MOV eax, 0FF00FF00h
	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0
		
	MOVDQA xmm10, xmm9
	PSRLW xmm10, 8

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	
	MOV rbx, r11
	MOV rcx, rdx

	MOV eax, r8d
	SHR r8d, 4
	SHR r10d, 1

	PXOR xmm8, xmm8

align 16
	row_loop:
		
		MOVDQU xmm0, [r11] ; gb
		MOVDQU xmm4, [r11+rax] ; rg

		ADD r11, 16
	
		MOVDQA xmm1, xmm0
				
		PAND xmm0, xmm10 ; g low
		PAND xmm1, xmm9 ; b high

		PSLLW xmm0, 8

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		PUNPCKLWD xmm0, xmm8
		PUNPCKHWD xmm2, xmm8

		PUNPCKLWD xmm1, xmm8
		PUNPCKHWD xmm3, xmm8		
		
		PSLLD xmm1, 8
		PSLLD xmm3, 8


		MOVDQA xmm5, xmm4

		PAND xmm4, xmm10 ; r low
		PAND xmm5, xmm9 ; g high

		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLWD xmm4, xmm9
		PUNPCKHWD xmm6, xmm9

		PUNPCKLWD xmm5, xmm8
		PUNPCKHWD xmm7, xmm8


		POR xmm1, xmm4 ; rb
		POR xmm3, xmm6 ; rb

		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3


		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLDQ xmm0, xmm0
		PUNPCKHDQ xmm1, xmm1

		PUNPCKLDQ xmm2, xmm2
		PUNPCKHDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3


		MOVDQA xmm4, xmm5
		MOVDQA xmm6, xmm7

		PUNPCKLDQ xmm4, xmm4
		PUNPCKHDQ xmm5, xmm5

		PUNPCKLDQ xmm6, xmm6
		PUNPCKHDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			SHR r8d, 4

			LEA rbx, [rbx + rax*2]
			LEA rcx, [rcx + r9*2]

			MOV r11, rbx
			MOV rdx, rcx

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP rbx

	RET
Image_DeBayer_1GBRGu	ENDP












ALIGN 16
Image_DeBayer_2BGGR	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHL r9d, 1
	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, [r11+16]

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, [r11+r9+16]

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm0, xmm0 ; b
		PUNPCKHQDQ xmm1, xmm1 ; g

		PUNPCKLQDQ xmm2, xmm2 ; b
		PUNPCKHQDQ xmm3, xmm3 ; g


		PSLLQ xmm0, 32
		PSLLQ xmm2, 32
		PSLLQ xmm1, 16
		PSLLQ xmm3, 16


		PUNPCKLQDQ xmm4, xmm4 ; g
		PUNPCKHQDQ xmm5, xmm5 ; r

		PUNPCKLQDQ xmm6, xmm6 ; g
		PUNPCKHQDQ xmm7, xmm7 ; r


		PSLLQ xmm4, 16
		PSLLQ xmm6, 16


		POR xmm0, xmm5
		POR xmm2, xmm7


		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2


		MOVNTDQ [r11], xmm1
		MOVNTDQ [r11+16], xmm3
		MOVNTDQ [r11+r9], xmm4
		MOVNTDQ [r11+r9+16], xmm6


		ADD r11, 32


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax

	RET
Image_DeBayer_2BGGR	ENDP

ALIGN 16
Image_DeBayer_1BGGR	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, xmm0
		

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, xmm4



		PUNPCKLDQ xmm0, xmm0 ; bbgg
		PUNPCKHDQ xmm2, xmm2 ; bbgg
		MOVDQA xmm1, xmm0
				
		PUNPCKLQDQ xmm0, xmm2 ; b
		PUNPCKHQDQ xmm1, xmm2 ; g
		
		PSLLD xmm0, 16
		PSLLD xmm1, 8
		
		PUNPCKLDQ xmm4, xmm4 ; ggrr
		PUNPCKHDQ xmm6, xmm6 ; ggrr
		MOVDQA xmm5, xmm4
				
		PUNPCKLQDQ xmm4, xmm6 ; g
		PUNPCKHQDQ xmm5, xmm6 ; r
		
		PSLLD xmm4, 8


		POR xmm0, xmm5 ; rb
		
		
		POR xmm1, xmm0
		POR xmm4, xmm0


		MOVNTDQ [r11], xmm1
		MOVNTDQ [r11+r9], xmm4
		

		ADD r11, 16


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax


	RET
Image_DeBayer_1BGGR	ENDP




ALIGN 16
Image_DeBayer_2BGGRu	PROC
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV eax, 0FFFF0000h

	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0

	MOVDQA xmm10, xmm9
	PSRLD xmm10, 16

	MOV r11, [rcx] ; source ptr
	
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	MOV r12d, [rcx+24] ; bcount


	MOV rbx, r11
	MOV rcx, rdx


	MOV eax, r12d
	MUL r8
	SHR rax, 3
	MOV r13, rax


	SHR r8d, 3
		
	MOV rdx, rcx
	MOV r15, rcx

	MOV ecx, r8d
	SHL rcx, 32


	MOV cl, r12b
	MOV ch, cl

	SUB cl, 16
	NEG cl

	SHR r10d, 1
	SHR r12d, 1
	
	SHL r9d, 1

	PXOR xmm8, xmm8
	XOR rax, rax

align 16
	row_loop:
		SHL r10, 32

		MOV rax, [r11]
		

		CMP ch, 16
		JZ load_rax0
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax0:
		MOVD xmm0, rax

		MOV rax, [r11+r12]

		CMP ch, 16
		JZ load_rax1
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax1:
		MOVD xmm2, rax

		PUNPCKLQDQ xmm0, xmm2
		

		MOV rax, [r11+r13]
		
		CMP ch, 16
		JZ load_rax2
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax2:

		MOVD xmm4, rax

		ADD r13, r12
		MOV rax, [r11+r13]
		SUB r13, r12

		CMP ch, 16
		JZ load_rax3
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax3:

		MOVD xmm6, rax

		PUNPCKLQDQ xmm4, xmm6


		SHR r10, 32


		LEA r11, [r11 + r12*2]
	
		MOVDQA xmm1, xmm0

		PAND xmm0, xmm10 ; b low
		PAND xmm1, xmm9 ; g high

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1
				
		PUNPCKLDQ xmm0, xmm8 ; b
		PUNPCKHDQ xmm2, xmm8 ; b

		PUNPCKLDQ xmm1, xmm8 ; g
		PUNPCKHDQ xmm3, xmm8 ; g
		
		PSLLQ xmm0, 32
		PSLLQ xmm2, 32

		MOVDQA xmm5, xmm4
		
		PAND xmm4, xmm10 ; g low
		PAND xmm5, xmm9 ; r high
		
		PSLLD xmm4, 16
		PSRLD xmm5, 16
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm4, xmm8 ; g
		PUNPCKHDQ xmm6, xmm8 ; g

		PUNPCKLDQ xmm5, xmm9 ; r
		PUNPCKHDQ xmm7, xmm9 ; r


		POR xmm0, xmm5 ; rb
		POR xmm2, xmm7 ; rb

		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2





		MOVDQA xmm0, xmm1
		MOVDQA xmm2, xmm3

		PUNPCKLQDQ xmm0, xmm0
		PUNPCKHQDQ xmm1, xmm1

		PUNPCKLQDQ xmm2, xmm2
		PUNPCKHQDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3




		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm4, xmm4
		PUNPCKHQDQ xmm5, xmm5

		PUNPCKLQDQ xmm6, xmm6
		PUNPCKHQDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8, rcx
			SHR r8, 32

			LEA rbx, [rbx + r13*2]
			LEA r15, [r15 + r9*2]

			MOV r11, rbx
			MOV rdx, r15

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	
	RET
Image_DeBayer_2BGGRu	ENDP



ALIGN 16
Image_DeBayer_1BGGRu	PROC
	PUSH rbx

	MOV eax, 0FF00FF00h
	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0
		
	MOVDQA xmm10, xmm9
	PSRLW xmm10, 8

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	
	MOV rbx, r11
	MOV rcx, rdx

	MOV eax, r8d
	SHR r8d, 4
	SHR r10d, 1

	PXOR xmm8, xmm8

align 16
	row_loop:
		
		MOVDQU xmm0, [r11] ; bg
		MOVDQU xmm4, [r11+rax] ; bb

		ADD r11, 16
	
		MOVDQA xmm1, xmm0
				
		PAND xmm0, xmm10 ; b low
		PAND xmm1, xmm9 ; g high


		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		PUNPCKLWD xmm0, xmm8
		PUNPCKHWD xmm2, xmm8

		PUNPCKLWD xmm1, xmm8
		PUNPCKHWD xmm3, xmm8		
		
		PSLLD xmm0, 16
		PSLLD xmm2, 16


		MOVDQA xmm5, xmm4

		PAND xmm4, xmm10 ; g low
		PAND xmm5, xmm9 ; r high

		PSLLW xmm4, 8
		PSRLW xmm5, 8
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLWD xmm4, xmm8
		PUNPCKHWD xmm6, xmm8

		PUNPCKLWD xmm5, xmm9
		PUNPCKHWD xmm7, xmm9


		POR xmm0, xmm5 ; rb
		POR xmm2, xmm7 ; rb

		POR xmm1, xmm0
		POR xmm3, xmm2

		POR xmm4, xmm0
		POR xmm6, xmm2


		MOVDQA xmm0, xmm1
		MOVDQA xmm2, xmm3

		PUNPCKLDQ xmm0, xmm0
		PUNPCKHDQ xmm1, xmm1

		PUNPCKLDQ xmm2, xmm2
		PUNPCKHDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3


		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm4, xmm4
		PUNPCKHDQ xmm5, xmm5

		PUNPCKLDQ xmm6, xmm6
		PUNPCKHDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			SHR r8d, 4

			LEA rbx, [rbx + rax*2]
			LEA rcx, [rcx + r9*2]

			MOV r11, rbx
			MOV rdx, rcx

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP rbx

	RET
Image_DeBayer_1BGGRu	ENDP
















ALIGN 16
Image_DeBayer_2GRBG	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHL r9d, 1
	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, [r11+16]

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, [r11+r9+16]

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLQDQ xmm0, xmm0 ; g
		PUNPCKHQDQ xmm1, xmm1 ; r

		PUNPCKLQDQ xmm2, xmm2 ; g
		PUNPCKHQDQ xmm3, xmm3 ; r

		PSLLQ xmm0, 16
		PSLLQ xmm2, 16


		PUNPCKLQDQ xmm4, xmm4 ; b
		PUNPCKHQDQ xmm5, xmm5 ; g

		PUNPCKLQDQ xmm6, xmm6 ; b
		PUNPCKHQDQ xmm7, xmm7 ; g

		PSLLQ xmm4, 32
		PSLLQ xmm6, 32
		PSLLQ xmm5, 16
		PSLLQ xmm7, 16


		POR xmm1, xmm4
		POR xmm3, xmm6


		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3


		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+16], xmm2
		MOVNTDQ [r11+r9], xmm5
		MOVNTDQ [r11+r9+16], xmm7


		ADD r11, 32


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax

	RET
Image_DeBayer_2GRBG	ENDP

ALIGN 16
Image_DeBayer_1GRBG	PROC

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, r9d

	SHR r8d, 4

	MOV eax, r8d

	SHR r10d, 1

align 16
	row_loop:
		
		MOVDQA xmm0, [r11]
		MOVDQA xmm2, xmm0
		

		MOVDQA xmm4, [r11+r9]
		MOVDQA xmm6, xmm4



		PUNPCKLDQ xmm0, xmm0 ; ggrr
		PUNPCKHDQ xmm2, xmm2 ; ggrr
		MOVDQA xmm1, xmm0
				
		PUNPCKLQDQ xmm0, xmm2 ; g
		PUNPCKHQDQ xmm1, xmm2 ; r
		
		PSLLD xmm0, 8
		
		
		PUNPCKLDQ xmm4, xmm4 ; bbgg
		PUNPCKHDQ xmm6, xmm6 ; bbgg
		MOVDQA xmm5, xmm4
				
		PUNPCKLQDQ xmm4, xmm6 ; b
		PUNPCKHQDQ xmm5, xmm6 ; g
		
		PSLLD xmm5, 8
		PSLLD xmm4, 16

		POR xmm1, xmm4 ; rb
		
		
		POR xmm0, xmm1
		POR xmm5, xmm1


		MOVNTDQ [r11], xmm0
		MOVNTDQ [r11+r9], xmm5
		

		ADD r11, 16


		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			ADD r11, r9

	DEC r10d
	JNZ row_loop


	XOR rax, rax


	RET
Image_DeBayer_1GRBG	ENDP




ALIGN 16
Image_DeBayer_2GRBGu	PROC
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV eax, 0FFFF0000h

	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0

	MOVDQA xmm10, xmm9
	PSRLD xmm10, 16

	MOV r11, [rcx] ; source ptr
	
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	MOV r12d, [rcx+24] ; bcount


	MOV rbx, r11
	MOV rcx, rdx


	MOV eax, r12d
	MUL r8
	SHR rax, 3
	MOV r13, rax


	SHR r8d, 3
		
	MOV rdx, rcx
	MOV r15, rcx

	MOV ecx, r8d
	SHL rcx, 32


	MOV cl, r12b
	MOV ch, cl

	SUB cl, 16
	NEG cl

	SHR r10d, 1
	SHR r12d, 1
	
	SHL r9d, 1

	PXOR xmm8, xmm8
	XOR rax, rax

align 16
	row_loop:
		SHL r10, 32

		MOV rax, [r11]
		

		CMP ch, 16
		JZ load_rax0
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax0:
		MOVD xmm0, rax

		MOV rax, [r11+r12]

		CMP ch, 16
		JZ load_rax1
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax1:
		MOVD xmm2, rax

		PUNPCKLQDQ xmm0, xmm2
		

		MOV rax, [r11+r13]
		
		CMP ch, 16
		JZ load_rax2
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16
	load_rax2:

		MOVD xmm4, rax

		ADD r13, r12
		MOV rax, [r11+r13]
		SUB r13, r12

		CMP ch, 16
		JZ load_rax3
			MOV r14, rax
			BSWAP r14

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV ax, r14w
			SHL ax, cl
			ROR rax, 16

	load_rax3:

		MOVD xmm6, rax

		PUNPCKLQDQ xmm4, xmm6


		SHR r10, 32


		LEA r11, [r11 + r12*2]
	
		MOVDQA xmm1, xmm0

		PAND xmm0, xmm10 ; g low
		PAND xmm1, xmm9 ; r high

		PSLLD xmm0, 16
		PSRLD xmm1, 16

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1
				
		PUNPCKLDQ xmm0, xmm8 ; g
		PUNPCKHDQ xmm2, xmm8 ; g

		PUNPCKLDQ xmm1, xmm9 ; r
		PUNPCKHDQ xmm3, xmm9 ; r
		

		MOVDQA xmm5, xmm4
		
		PAND xmm4, xmm10 ; b low
		PAND xmm5, xmm9 ; g high
		
		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLDQ xmm4, xmm8 ; b
		PUNPCKHDQ xmm6, xmm8 ; b

		PUNPCKLDQ xmm5, xmm8 ; g
		PUNPCKHDQ xmm7, xmm8 ; g

		PSLLQ xmm4, 32
		PSLLQ xmm6, 32

		POR xmm1, xmm4 ; rb
		POR xmm3, xmm6 ; rb

		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3





		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLQDQ xmm0, xmm0
		PUNPCKHQDQ xmm1, xmm1

		PUNPCKLQDQ xmm2, xmm2
		PUNPCKHQDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3




		MOVDQA xmm4, xmm5
		MOVDQA xmm6, xmm7

		PUNPCKLQDQ xmm4, xmm4
		PUNPCKHQDQ xmm5, xmm5

		PUNPCKLQDQ xmm6, xmm6
		PUNPCKHQDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8, rcx
			SHR r8, 32

			LEA rbx, [rbx + r13*2]
			LEA r15, [r15 + r9*2]

			MOV r11, rbx
			MOV rdx, r15

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	
	RET
Image_DeBayer_2GRBGu	ENDP



ALIGN 16
Image_DeBayer_1GRBGu	PROC
	PUSH rbx

	MOV eax, 0FF00FF00h
	MOVD xmm9, eax
	PSHUFD xmm9, xmm9, 0
		
	MOVDQA xmm10, xmm9
	PSRLW xmm10, 8

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	
	MOV rbx, r11
	MOV rcx, rdx

	MOV eax, r8d
	SHR r8d, 4
	SHR r10d, 1

	PXOR xmm8, xmm8

align 16
	row_loop:
		
		MOVDQU xmm0, [r11] ; gr
		MOVDQU xmm4, [r11+rax] ; bg

		ADD r11, 16
	
		MOVDQA xmm1, xmm0
				
		PAND xmm0, xmm10 ; g low
		PAND xmm1, xmm9 ; r high

		PSLLW xmm0, 8
		PSRLW xmm1, 8

		MOVDQA xmm2, xmm0
		MOVDQA xmm3, xmm1

		PUNPCKLWD xmm0, xmm8
		PUNPCKHWD xmm2, xmm8

		PUNPCKLWD xmm1, xmm9
		PUNPCKHWD xmm3, xmm9		
		


		MOVDQA xmm5, xmm4

		PAND xmm4, xmm10 ; b low
		PAND xmm5, xmm9 ; g high

		
		MOVDQA xmm6, xmm4
		MOVDQA xmm7, xmm5

		PUNPCKLWD xmm4, xmm8
		PUNPCKHWD xmm6, xmm8

		PUNPCKLWD xmm5, xmm8
		PUNPCKHWD xmm7, xmm8

		PSLLD xmm4, 16
		PSLLD xmm6, 16

		POR xmm1, xmm4 ; rb
		POR xmm3, xmm6 ; rb

		POR xmm0, xmm1
		POR xmm2, xmm3

		POR xmm5, xmm1
		POR xmm7, xmm3


		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLDQ xmm0, xmm0
		PUNPCKHDQ xmm1, xmm1

		PUNPCKLDQ xmm2, xmm2
		PUNPCKHDQ xmm3, xmm3


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3


		MOVDQA xmm4, xmm5
		MOVDQA xmm6, xmm7

		PUNPCKLDQ xmm4, xmm4
		PUNPCKHDQ xmm5, xmm5

		PUNPCKLDQ xmm6, xmm6
		PUNPCKHDQ xmm7, xmm7

		MOVNTDQ [rdx+r9], xmm4
		MOVNTDQ [rdx+r9+16], xmm5
		MOVNTDQ [rdx+r9+32], xmm6
		MOVNTDQ [rdx+r9+48], xmm7

		ADD rdx, 64
		

		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			SHR r8d, 4

			LEA rbx, [rbx + rax*2]
			LEA rcx, [rcx + r9*2]

			MOV r11, rbx
			MOV rdx, rcx

	DEC r10d
	JNZ row_loop

	XOR rax, rax

	POP rbx

	RET
Image_DeBayer_1GRBGu	ENDP





END

