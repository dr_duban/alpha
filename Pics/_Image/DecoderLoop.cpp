/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\Image.h"

#include "System\SysUtils.h"


#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"

#include "OpenGL\Core\OGL_WGL.h"




#pragma warning(disable:4244)


UI_64 Pics::Image::Decode(OpenGL::DecodeRecord & id_entry) {
	UI_64 result(0);
	unsigned int d_queue(0), q_pos(0);

	if (id_entry.flags & OpenGL::DecodeRecord::FlagPreview) {
		if (id_entry.flags & 1) {
			prev_queue.Clear();
		}

		prev_queue.Push(&id_entry);
		
		result = 1;
	} else {
		if ((id_entry.flags & OpenGL::DecodeRecord::RecordFlags) != OpenGL::DecodeRecord::RecordFlags) {
			d_queue = (current_de_queue & 1);
			q_pos = System::LockedInc_32(dq_runner + d_queue);

			if (q_pos >= dq_top[d_queue]) { // resize
				if (decoding_queue[d_queue].Expand(decoding_queue_capacity*sizeof(OpenGL::DecodeRecord)) != -1) {
					dq_top[d_queue] += decoding_queue_capacity;

				}
			}
			
			if (q_pos < dq_top[d_queue])
			if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
		
				System::MemoryCopy_SSE3(d_record + q_pos, &id_entry, sizeof(OpenGL::DecodeRecord));

				decoding_queue[d_queue].Release();
			}

			result = 1;
		} else {
			if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
				for (unsigned int j(0);j<render_runner;j++) {
					if ((de_rec[j].element_obj == id_entry.element_obj) && (de_rec[j].program != -1)) {
						
						de_rec[j].flags |= (id_entry.flags & (~OpenGL::DecodeRecord::RecordFlags));

						result = 1;
						break;
					}
				}
				render_queue.Release();
			}
		}
	}

	return result;
}



DWORD __stdcall Pics::Image::DecoderLoop(void * context) {
	wchar_t temp_str[64];
	I_64 pool_index[2*MAX_POOL_COUNT];

	DWORD result(0);
	
	UI_64 image_id(-1);

	unsigned int d_queue(0), _wao(0), pre_delay(0), slide_show(-1); // noise_val(0x01000065), 
	UI_64 ni_available(0), list_increment(1), t_val(0);

	SFSco::Object fname_obj;
	Properties i_props;

	WNDCLASSEX wclass;
	OpenGL::DecodeRecord current_record;
	OpenGL::APPlane d_plane;

	System::MemoryFill(&wclass, sizeof(WNDCLASSEX), 0);
	wclass.cbSize = sizeof(WNDCLASSEX);
	wclass.lpfnWndProc = DefWindowProc;
	
	OpenGL::Core::GetHeaderString(18, temp_str);
	wclass.lpszClassName = temp_str + 1;
	wclass.style = CS_OWNDC;

	WORD _atom = RegisterClassEx(&wclass);		
	if (!_atom) result = GetLastError();

	Memory::Chain::RegisterThread(pool_index);

	if (HWND w_handle = CreateWindowEx(0, (LPCWSTR)_atom, 0, WS_POPUP, 0, 0, 16, 16, 0, 0, 0, 0)) {
		if (HDC w_hdc = GetDC(w_handle)) {				
			int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3, WGL_CONTEXT_MINOR_VERSION_ARB, 2, WGL_CONTEXT_PROFILE_MASK_ARB,  WGL_CONTEXT_CORE_PROFILE_BIT_ARB, WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0, 0};

			if (SetPixelFormat(w_hdc, OpenGL::Core::GetPFI(), OpenGL::Core::GetPFD())) {
				if (HGLRC gl_rc = WGL::CreateContextAttribs(w_hdc, 0, attribs)) {
					if (main_ogl->ShareRC(gl_rc) == 0) {
						if (WGL::MakeContextCurrent(w_hdc, w_hdc, gl_rc)) {
							

							OpenGL::glDisable(GL_DEPTH_TEST);
							OpenGL::glDisable(GL_CULL_FACE);
							OpenGL::glDisable(GL_STENCIL_TEST);
							OpenGL::glDisable(GL_SCISSOR_TEST);									
							OpenGL::glDisable(GL_BLEND);
																																				
							OpenGL::GenVertexArrays(1, &_wao);
							OpenGL::BindVertexArray(_wao);
									
							OpenGL::ClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
									
							OpenGL::glPixelStorei(GL_PACK_ALIGNMENT, 4);
							OpenGL::glPixelStorei(GL_PACK_ROW_LENGTH, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_ROWS, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
								
							OpenGL::ActiveTexture(GL_TEXTURE0);


							SetEvent(de_block);

							result = 0;


							if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {
								for (;bend_of_line;) {
									if ((dq_runner[0] | dq_runner[1]) == 0) WaitForSingleObject(ebreaker, 23);

									d_queue = (System::LockedInc_32(&current_de_queue) & 1);

									if ((dq_runner[d_queue]) || (slide_show != -1)) {
										if (slide_show != -1) {
											if (pre_delay-- == 0) {
												pre_delay = slide_delay;


												if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
													if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
														d_record[dq_runner[d_queue]].flags = OpenGL::DecodeRecord::FlagNextTrack;
														d_record[dq_runner[d_queue]].element_obj = de_rec[slide_show].element_obj;

														dq_runner[d_queue]++;

														render_queue.Release();
													}
													decoding_queue[d_queue].Release();
												}
											}
										}


										for (unsigned int i(0);i<dq_runner[d_queue];i++) {
											if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
												System::MemoryCopy_SSE3(&current_record, d_record + i, sizeof(OpenGL::DecodeRecord));

												decoding_queue[d_queue].Release();
											} else break;
																						


											result = -1;

											if (current_record.element_obj)
											if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
												for (unsigned int j(0);j<render_runner;j++) {
													if ((de_rec[j].element_obj == current_record.element_obj) && (de_rec[j].program != -1)) {
														current_record.start_time = de_rec[j].start_time;
														current_record.target_time = de_rec[j].target_time;
														current_record._reserved[0] = de_rec[j]._reserved[0];

														result = j;
														break;
													}
												}
												
												render_queue.Release();
											}

										
											if (result != -1) {
												if (current_record.flags & OpenGL::DecodeRecord::ProgramOptions) {

													if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
														current_record.flags |= de_rec[result].flags;
														current_record.fname_id = de_rec[result].fname_id;
														current_record.program = de_rec[result].program;
																								

														list_increment = 0;
														ni_available = 0;

														switch (current_record.flags & OpenGL::DecodeRecord::ProgramOptions) {
															case OpenGL::DecodeRecord::FlagDestroy:
																for (unsigned int k(0);k<render_runner;k++) {
																	if ((de_rec[k].program == de_rec[result].program) && (k != result)) de_rec[k].program = -1;
																}

																Free(de_rec[result].program);
																de_rec[result].program = -1;

																d_plane = reinterpret_cast<OpenGL::APPlane &>(de_rec[result].element_obj); // .Destroy();

															break;

															case OpenGL::DecodeRecord::FlagReset:
																
																for (unsigned int pp(0);pp<render_runner;pp++) {
																	if ((de_rec[pp].program == de_rec[result].program) && (result != pp)) {																	
																		if (!result) {																		
																			if (Properties i_pro = pi_list[de_rec[pp].program]) {
																				de_rec[pp].flags |= OpenGL::DecodeRecord::FlagRefresh;
																				
																				if ((de_rec[pp].flags & OpenGL::DecodeRecord::FlagAnimated) && (de_rec[pp].start_time == 0)) {
																					de_rec[pp].start_time = System::GetTime();
																				}																		
																			}
																		}
																	}
																}

															break;


															case OpenGL::DecodeRecord::FlagPlayAll:
																// enter slide show
																reinterpret_cast<OpenGL::APPlane &>(current_record.element_obj).RootScreen();

																if (slide_show != result) {
																	slide_show = result;
																	pre_delay = slide_delay;
																} else {
																	slide_show = -1;
																}

															break;
															case OpenGL::DecodeRecord::FlagKill:
																Free(de_rec[result].program);
																		
																if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
																	if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Acquire())) {
																		UI_64 * offset_table = reinterpret_cast<UI_64 *>(pl_ptr + reinterpret_cast<UI_64 *>(pl_ptr)[0]);
															
																		if (current_record.fname_id <= offset_table[0]) {
																			if (pl_ptr[offset_table[current_record.fname_id]]) {																				

																				System::MemoryCopy(pl_ptr + 8 + pl_ptr[8] + 1, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]*sizeof(wchar_t));
																				pl_ptr[8 + pl_ptr[8] + 1 + pl_ptr[offset_table[current_record.fname_id]]] = L'\0';
																						
																				if (DeleteFile(pl_ptr + 9)) {
																					pl_ptr[offset_table[current_record.fname_id]] = 0;
																					ni_available++;

																					current_record.flags |= OpenGL::DecodeRecord::FlagNextTrack;
																				}
																			}
																		}

																		reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Release();
																	}															
														
																	current_record.element_obj.Release();
																}														

																		
																// adjust play list

															case OpenGL::DecodeRecord::FlagSSNext:
																if (current_record.pic_id != -1) {
																	current_record.fname_id = current_record.pic_id;
																}

																current_record.flags = OpenGL::DecodeRecord::FlagNextTrack;

															case OpenGL::DecodeRecord::FlagNextTrack:
																list_increment = 2;
			
															case OpenGL::DecodeRecord::FlagPrevTrack:
																		
																current_record.fname_id += --list_increment;
																		
																ni_available++;

																if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
																	if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Acquire())) {
																		UI_64 * offset_table = reinterpret_cast<UI_64 *>(pl_ptr + reinterpret_cast<UI_64 *>(pl_ptr)[0]);
															
																		for (;(current_record.fname_id >= 1) && (current_record.fname_id <= offset_table[0]);current_record.fname_id += list_increment) {
																			if ((pl_ptr[offset_table[current_record.fname_id]])) {
																				reinterpret_cast<UI_64 *>(pl_ptr)[1] = current_record.fname_id;

																				if (de_rec[result].name_text_id != -1) reinterpret_cast<OpenGL::Core *>(el_hdr->_context)->RewriteText(de_rec[result].name_text_id, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]);
																				System::MemoryCopy(pl_ptr + 8 + pl_ptr[8] + 1, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]*sizeof(wchar_t));
																				pl_ptr[8 + pl_ptr[8] + 1 + pl_ptr[offset_table[current_record.fname_id]]] = L'\0';
																				ni_available = 5;

																				break;
																			}
																		}

																		if (ni_available == 2) {
																			for (;(current_record.fname_id >= 1) && (current_record.fname_id <= offset_table[0]);current_record.fname_id -= list_increment) {
																				if ((pl_ptr[offset_table[current_record.fname_id]])) {
																					reinterpret_cast<UI_64 *>(pl_ptr)[1] = current_record.fname_id;

																					if (de_rec[result].name_text_id != -1) reinterpret_cast<OpenGL::Core *>(el_hdr->_context)->RewriteText(de_rec[result].name_text_id, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]);
																					System::MemoryCopy(pl_ptr + 8 + pl_ptr[8] + 1, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]*sizeof(wchar_t));
																					pl_ptr[8 + pl_ptr[8] + 1 + pl_ptr[offset_table[current_record.fname_id]]] = L'\0';
																					ni_available = 5;

																					break;
																				}
																			}
																		}
																		reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Release();
																	}																																

																	current_record.element_obj.Release();
																}														


															// destroy all screens exept one
																if (ni_available == 5) {
																	t_val = 0;
																			
																	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(de_rec[result].element_obj.Acquire())) {
																		t_val = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_count;

																		de_rec[result].element_obj.Release();
																	}

																		
																	for (unsigned int mm(1);mm<t_val;mm++) {
																		current_record.element_obj = de_rec[result].element_obj;
																		current_record.element_obj.Down().Left().Down();
																		OpenGL::Core::ClearDecodeView(current_record.element_obj, false);
																	}


																// destroy image															
																	t_val = 0;
																	current_record.element_obj = de_rec[result].element_obj;
																	current_record.element_obj.Down().Left().Down();

																	for (unsigned int k(0);k<render_runner;k++) {
																		// one screen remains
																		if ((de_rec[k].program == de_rec[result].program) && (k != result))
																		if (de_rec[k].element_obj != current_record.element_obj) {
																			de_rec[k].program = -1;
																		} else {
																			t_val = k;
																		}
																	}

																	Free(de_rec[result].program);
																	de_rec[result].program = -1;
																	
																	current_record.element_obj = de_rec[result].element_obj;
																	de_rec[result].fname_id = current_record.fname_id;
																}
															break;
														}
														
														render_queue.Release();

														if (d_plane) d_plane.Destroy();
														d_plane.Blank();

														if (current_record.flags &= (OpenGL::DecodeRecord::FlagNextTrack | OpenGL::DecodeRecord::FlagPrevTrack)) {
															current_record.flags |= OpenGL::DecodeRecord::FlagFile;

															if (OpenGL::DecodeRecord * d_record = reinterpret_cast<OpenGL::DecodeRecord *>(decoding_queue[d_queue].Acquire())) {
																for (unsigned int ii(i+1);ii<dq_runner[d_queue];ii++) {											
																	if (d_record[ii].element_obj == current_record.element_obj) d_record[ii].element_obj.Blank();
																}

																decoding_queue[d_queue].Release();
															}

															switch (ni_available) {
																case 5:
																	current_record.program = -1;

																	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
															
																		current_record.program = Load(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list, 9);

																		current_record.element_obj.Release();

																		if (current_record.program == -1) {
																			current_record.program = Create(512, 512, 1);
																		}
																	}


																	// change program
																	if (current_record.program != -1) {
																		if (OpenGL::DecodeRecord * de_rec = reinterpret_cast<OpenGL::DecodeRecord *>(render_queue.Acquire())) {
																			de_rec[t_val].program = de_rec[result].program = current_record.program;
																			de_rec[t_val].pic_id = 0;																																									

																			render_queue.Release();
																		}

																				
																		current_record.pic_id = 0;
																		current_record.flags |= OpenGL::DecodeRecord::FlagRefresh;

																		EnablePage(current_record);
																		
																	} else {
																		// error
																	}
																break;
																case 1: // terminator
																	if (slide_show != -1) {
																		// exit slide show
																		reinterpret_cast<OpenGL::APPlane &>(current_record.element_obj).RootScreen();

																		slide_show = -1;
																	}

																break;
																case 2: // no more files

																break;
															}
														}
													}
												} else {
													// source options

													switch (current_record.flags & OpenGL::DecodeRecord::SourceOptions) {
														case OpenGL::DecodeRecord::FlagTakeAShot:
															shot_set.full_width = 0;
															shot_set.full_height = 0;
															shot_set.offset_x = 0;
															shot_set.offset_y = 0;
															shot_set.screen_width = 0;
															shot_set.screen_height = 0;

															shot_set.i_flags = 0;

															SaveAs(current_record.program, shot_set);
															
														break;
														case OpenGL::DecodeRecord::FlagSize:
															if (current_record.flags & OpenGL::DecodeRecord::SizeMask) { // feedback for slider
																current_record.element_obj.SetXTra(current_record._reserved[4]);
															} else {
																RefreshPage(current_record);
															}


														break;

													}
												}
											} else {
												current_record.program = -1;

												if (current_record.flags & OpenGL::DecodeRecord::FlagFile) {														
													if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(current_record.element_obj.Acquire())) {
														if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Acquire())) {
															UI_64 * offset_table = reinterpret_cast<UI_64 *>(pl_ptr + reinterpret_cast<UI_64 *>(pl_ptr)[0]);
															
															if (current_record.fname_id <= offset_table[0]) {
																if (pl_ptr[offset_table[current_record.fname_id]]) {
																	reinterpret_cast<UI_64 *>(pl_ptr)[1] = current_record.fname_id;

																	if (current_record.name_text_id != -1) reinterpret_cast<OpenGL::Core *>(el_hdr->_context)->RewriteText(current_record.name_text_id, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]);

																	System::MemoryCopy(pl_ptr + 8 + pl_ptr[8] + 1, pl_ptr + offset_table[current_record.fname_id] + 1, pl_ptr[offset_table[current_record.fname_id]]*sizeof(wchar_t));
																	pl_ptr[8 + pl_ptr[8] + 1 + pl_ptr[offset_table[current_record.fname_id]]] = L'\0';
																			
																	result = 5;
																}
															}
															reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Release();
														}															
														
														if (result != -1) {
															current_record.program = Load(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list, 9); // result

															if (current_record.program == -1) {
																current_record.program = Create(512, 512, 1);
															}
														}
														current_record.element_obj.Release();
													}
												}

														

												if (current_record.program != -1) {
												// adjust 
													current_record.flags |= OpenGL::DecodeRecord::FlagProgram;
													AddDREntry(current_record);
													current_record.flags ^= OpenGL::DecodeRecord::FlagProgram;

													current_record.pic_id = 0;
													EnablePage(current_record);

												} else {
												// error
																										
												}
											}
										}

										dq_runner[d_queue] = 0;
									}
								}

								CloseHandle(ebreaker);
							} else {
								result = GetLastError();
							}
							

							OpenGL::DeleteVertexArrays(1, &_wao);												
							WGL::MakeContextCurrent(w_hdc, w_hdc, 0);
								
						} else {
							result = GetLastError();
						}
					} else {
						result = GetLastError();
					}
						
					wglDeleteContext(gl_rc);
				} else {
					result = GetLastError();
				}
			}
			ReleaseDC(w_handle, w_hdc);
		} else {
			result = GetLastError();
		}
			
		DestroyWindow(w_handle);

	} else {
		result = GetLastError();
	}

	Memory::Chain::UnregisterThread();

	return result;
}


DWORD __stdcall Pics::Image::PreviewLoop(void * context) {
	wchar_t temp_str[64];
	I_64 pool_index[2*MAX_POOL_COUNT];
	DWORD result(0);
	
	UI_64 image_id(-1), ii(0);

	unsigned int _wao(0);
	UI_64 sibuf_size(0);

	SFSco::Object fname_obj, small_size;
	Properties i_props;

	
	OpenGL::DecodeRecord current_record;

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

	Memory::Chain::RegisterThread(pool_index);

	OpenGL::Core::GetHeaderString(18, temp_str);
	if (HWND w_handle = CreateWindowEx(0, temp_str + 1, 0, WS_POPUP, 0, 0, 16, 16, 0, 0, 0, 0)) {
		if (HDC w_hdc = GetDC(w_handle)) {				
			int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3, WGL_CONTEXT_MINOR_VERSION_ARB, 2, WGL_CONTEXT_PROFILE_MASK_ARB,  WGL_CONTEXT_CORE_PROFILE_BIT_ARB, WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0, 0};

			if (SetPixelFormat(w_hdc, OpenGL::Core::GetPFI(), OpenGL::Core::GetPFD())) {
				if (HGLRC gl_rc = WGL::CreateContextAttribs(w_hdc, 0, attribs)) {
					if (main_ogl->ShareRC(gl_rc) == 0) {
						if (WGL::MakeContextCurrent(w_hdc, w_hdc, gl_rc)) {
							

							OpenGL::glDisable(GL_DEPTH_TEST);
							OpenGL::glDisable(GL_CULL_FACE);
							OpenGL::glDisable(GL_STENCIL_TEST);
							OpenGL::glDisable(GL_SCISSOR_TEST);									
							OpenGL::glDisable(GL_BLEND);
																																				
							OpenGL::GenVertexArrays(1, &_wao);
							OpenGL::BindVertexArray(_wao);
									
							OpenGL::ClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
									
							OpenGL::glPixelStorei(GL_PACK_ALIGNMENT, 4);
							OpenGL::glPixelStorei(GL_PACK_ROW_LENGTH, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_ROWS, 0);
							OpenGL::glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
								
							OpenGL::ActiveTexture(GL_TEXTURE0);







							if (small_size.New(0, 1024*1024*sizeof(unsigned int), SFSco::large_mem_mgr) != -1) {
								sibuf_size = 1048576;
										
								result = 0;

									
								if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {
									for (;bend_of_line;) {
										WaitForSingleObject(ebreaker, 53);

										if (ii = prev_queue.Pull(&current_record)) {
											for (;; ii = prev_queue.Pull(&current_record)) {												
												fname_obj.Set(SFSco::mem_mgr, current_record.fname_id);

												image_id = Load(fname_obj);
												fname_obj.Destroy();

												if (image_id != -1) {
													i_props = pi_list.Remove(image_id);

													current_record.width = i_props.width;
													current_record.height = i_props.height;
													current_record.c_width = i_props.c_width;
													current_record.c_height = i_props.c_height;

													if (i_props.precision > 1) current_record.precision = 0x10000000;
													else current_record.precision = 0;
												} else {
													i_props.im_obj.Blank();
												}

												if (i_props.norgba == YCbCr) {
													current_record._reserved[5] = 6;
												} else {
													current_record._reserved[5] = 0;
												}

												OpenGL::Core::SetDecodePreview(current_record, i_props.im_obj, small_size);

												if (image_id != -1) {
													i_props.Destroy();
	
													if ((i_props.interval) && (i_props.prev_index != -1)) {
														pi_list.Remove(i_props.prev_index).Destroy();
													}

													for (;i_props.next_index != -1;) {
														i_props = pi_list.Remove(i_props.next_index);
														i_props.Destroy();
													}
												}

												if (ii == 2) break;
											}
										}
									}

									CloseHandle(ebreaker);
								} else {
									result = GetLastError();
								}
								small_size.Destroy();
							}





							OpenGL::DeleteVertexArrays(1, &_wao);												
							WGL::MakeContextCurrent(w_hdc, w_hdc, 0);
								
						} else {
							result = GetLastError();
						}
					} else {
						result = GetLastError();
					}
						
					wglDeleteContext(gl_rc);
				} else {
					result = GetLastError();
				}
			}
			ReleaseDC(w_handle, w_hdc);
		} else {
			result = GetLastError();
		}
			
		DestroyWindow(w_handle);

	} else {
		result = GetLastError();
	}

	Memory::Chain::UnregisterThread();

	return result;
}



