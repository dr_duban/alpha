;
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



; EXTERN	?size_paragraph@Image@Pics@@1IB:DWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
Bitmap_Read1	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r13d, [rdx+16] ; width
		MOV r15, [rdx+32]
		
		MOV edx, r13d

		MOV rdi, r8
		
		MOV r11d, [rcx-8]
		MOV r12d, [rcx-4]

		OR r11d, 0FF000000h
		OR r12d, 0FF000000h

		BSWAP r11d
		BSWAP r12d

		ROR r11d, 8
		ROR r12d, 8

	align 16
		x_loop:
			MOV al, [rcx]
			MOV ah, 8

			INC rcx
			
			DEC r9d
			JS exit

		align 16
			byte_loop:
				SHL al, 1
				RCR r10d, 1
				SAR r10d, 31

				AND r10d, r12d
				CMOVZ r10d, r11d
				MOVNTI [rdi], r10d
				ADD rdi, 4
								

				DEC r13d
				JNZ nextpi_1
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit

					JMP x_loop				
				nextpi_1:

			DEC ah
			JNZ byte_loop
		JMP x_loop

	exit:

		MOV rax, rcx


	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	RET
Bitmap_Read1	ENDP


ALIGN 16
Bitmap_Read4	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r13d, [rdx+16] ; width
		MOV r15, [rdx+32] ; c_width
		MOV edx, r13d
		
		MOV rdi, r8
	
		XOR rax, rax
		XOR r10, r10

	align 16
		x_loop:
			MOV al, [rcx]
			SHR al, 4

			MOV r10b, [rcx]
			AND r10b, 0Fh



			INC rcx
			
			DEC r9d
			JS exit


				MOV r12d, [r11+rax*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ nextpi_1
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit

					JMP x_loop				
				nextpi_1:

				MOV r12d, [r11+r10*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit

		JMP x_loop

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Read4	ENDP

ALIGN 16
Bitmap_ReadRLE4	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r15, [rdx+32]

		
		MOV rdi, r8
	
		XOR rcx, rcx
		XOR r12, r12
		XOR r10, r10

	align 16
		x_loop:
			MOV cx, [rsi]
			ADD rsi, 2
			
			SUB r9d, 2
			JS exit

				TEST cl, cl
				JZ escape
					ROR cx, 8

					MOVZX r12, cl
					SHR r12b, 4
					MOVZX r10, cl
					AND r10b, 0Fh
									


					MOV r12d, [r11+r12*4]
					OR r12d, 0FF000000h
					BSWAP r12d
					ROR r12d, 8

					MOV r10d, [r11+r10*4]
					OR r10d, 0FF000000h
					BSWAP r10d
					ROR r10d, 8

				align 16
					copy_loop:

						MOVNTI [rdi], r12d
						ADD rdi, 4
					
						DEC ch
						JZ x_loop

						MOVNTI [rdi], r10d
						ADD rdi, 4

					DEC ch
					JNZ copy_loop

				JMP x_loop
			align 16
				escape:
					TEST ch, ch
					JZ endof_line
						CMP ch, 1
						JZ exit
							CMP ch, 2
							JZ pi_offset
								SHR rcx, 8

								XOR rax, rax
								MOV ah, cl								

								INC ecx
								SHR ecx, 1

								MOV al, cl

								INC ecx
								AND ecx, 0FFFEh
								
								SUB r9d, ecx
								JS exit
									AND al, 1

								align 16
									set_loop:
										MOVZX r12, BYTE PTR [rsi]
										MOVZX r10, BYTE PTR [rsi]
										INC rsi

										SHR r12b, 4
										
										MOV r12d, [r11+r12*4]
										OR r12d, 0FF000000h
										BSWAP r12d
										ROR r12d, 8

										MOVNTI [rdi], r12d
										ADD rdi, 4

										DEC ah
										JZ si_adjust

										AND r10b, 0Fh
										MOV r10d, [r11+r10*4]
										OR r10d, 0FF000000h
										BSWAP r10d
										ROR r10d, 8

										MOVNTI [rdi], r10d
										ADD rdi, 4
									DEC ah
									JNZ set_loop

								si_adjust:
									ADD rsi, rax
								JMP x_loop
							pi_offset:								
								SUB r9d, 2
								JS exit
									MOVZX rcx, BYTE PTR [rsi]
									LEA rdi, [rdi+rcx*4]

									XOR rdx, rdx
									MOVZX rax, BYTE PTR [rsi+1]

									MUL r15
									ADD rdi, rax

									ADD rsi, 2
							JMP x_loop
					endof_line:
						SUB r8, r15
						MOV rdi, r8

						CMP r14, r8
						JBE x_loop

	exit:

		MOV rax, rsi

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_ReadRLE4	ENDP



ALIGN 16
Bitmap_Read8	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r13d, [rdx+16]		
		MOV r15, [rdx+32]
	
		MOV edx, r13d
		
		MOV rdi, r8
	
		XOR rax, rax
		XOR r10, r10

	align 16
		x_loop:
			MOV al, [rcx]
			INC rcx
			
			DEC r9d
			JS exit

				MOV r12d, [r11+rax*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit
		JMP x_loop

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Read8	ENDP


ALIGN 16
Bitmap_ReadRLE8	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors
		MOV r15, [rdx+32]
		
		MOV rdi, r8
	
		XOR rcx, rcx
		XOR r12, r12
		XOR r10, r10

	align 16
		x_loop:
			MOV cx, [rsi]
			ADD rsi, 2
			
			SUB r9d, 2
			JS exit

				TEST cl, cl
				JZ escape
					ROR cx, 8

					MOV r12b, cl
					SHR rcx, 8

					MOV eax, [r11+r12*4]
					OR eax, 0FF000000h
					BSWAP eax
					ROR eax, 8

					copy_loop:
						MOVNTI [rdi], eax
						ADD rdi, 4
					
					DEC cl
					JNZ copy_loop

				JMP x_loop
			align 16
				escape:
					TEST ch, ch
					JZ endof_line
						CMP ch, 1
						JZ exit
							CMP ch, 2
							JZ pi_offset
								SHR rcx, 8

								MOV al, cl
						
								INC ecx
								AND ecx, 0FFFEh

								SUB r9d, ecx
								JS exit
									MOV cl, al
									MOV ch, al
									AND cl, 1

								align 16
									set_loop:
										MOV r12b, [rsi]
										INC rsi
										MOV eax, [r11+r12*4]
										BSWAP eax
										ROR eax, 8

										MOVNTI [rdi], eax
										ADD rdi, 4
									DEC ch
									JNZ set_loop

									ADD rsi, rcx
								JMP x_loop
							pi_offset:								
								SUB r9d, 2
								JS exit
									MOVZX rcx, BYTE PTR [rsi]
									LEA rdi, [rdi+rcx*4]

									XOR rdx, rdx
									MOVZX rax, BYTE PTR [rsi+1]

									MUL r15
									ADD rdi, rax

									ADD rsi, 2
							JMP x_loop
					endof_line:
						SUB r8, r15
						MOV rdi, r8

						CMP r14, r8
						JBE x_loop

	exit:

		MOV rax, rsi

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_ReadRLE8	ENDP


ALIGN 16
Bitmap_Read16	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		
		MOV edx, r13d
	
		
		MOV rdi, r8
	
		XOR rax, rax
		XOR r10, r10

	align 16
		x_loop:
			MOV ax, [rcx]
			ADD rcx, 2
			
			SUB r9d, 2
			JS exit

				MOV r12d, [r11+rax*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit
		JMP x_loop

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Read16	ENDP

ALIGN 16
Bitmap_MaskRead16	PROC
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]

		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		
		MOV rdi, r8

		MOV r10, rcx

		BSR ecx, DWORD PTR [rdx+20]		
		BSR r11d, DWORD PTR [rdx+24]		
		BSR ebx, DWORD PTR [rdx+28]
		
		SHL ecx, 8
		SHL ebx, 8
		MOV bl, r11b
		
		SUB ch, 7
		SUB bl, 15
		SUB bh, 23	


		XOR rax, rax

	align 16
		x_loop:
			MOV ax, [r10]

			ADD r10, 2
			
			SUB r9d, 2
			JS exit
				MOV r12d, 0FF000000h

				MOV r11d, [rdx+20]
				AND r11d, eax
				MOV cl, ch

				NEG cl
				JNS red_high
					MOV cl, ch
					SHR r11d, cl
					JMP set_red
				red_high:
					SHL r11d, cl
				set_red:					
					OR r12d, r11d

				MOV r11d, [rdx+24]
				AND r11d, eax
				MOV cl, bl

				NEG cl
				JNS green_high
					MOV cl, bl
					SHR r11d, cl
					JMP set_green
				green_high:
					SHL r11d, cl
				set_green:
					AND r11d, 0FF00h
					OR r12d, r11d



				MOV r11d, [rdx+28]
				AND r11d, eax
				MOV cl, bh
	
				NEG cl
				JNS blue_high
					MOV cl, bh
					SHR r11d, cl
					JMP set_blue
				blue_high:
					SHL r11d, cl
				set_blue:
					AND r11d, 0FF0000h
					OR r12d, r11d


				MOVNTI [rdi], r12d
				ADD rdi, 4

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, [rdx+16]
											
						MOV rax, r10
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD r10, rax
						SUB r9d, eax
						JS exit

					JMP x_loop				

	exit:

		MOV rax, r10

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx

	RET
Bitmap_MaskRead16	ENDP

ALIGN 16
Bitmap_Copy24	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		MOV edx, r13d
		
		
		MOV rdi, r8
		

	align 16
		x_loop:
			MOV eax, [rcx]
			BSWAP eax
			SHR eax, 8
			OR eax, 0FF000000h

			ADD rcx, 3
			
			SUB r9d, 3
			JS exit

				MOVNTI [rdi], eax
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit

					JMP x_loop				

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Copy24	ENDP

ALIGN 16
Bitmap_Read24	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		MOV edx, r13d		
		
		MOV rdi, r8
	
		XOR rax, rax
		XOR r10, r10

	align 16
		x_loop:
			MOV eax, [rcx]
			AND eax, 0FFFFFFh

			ADD rcx, 3
			
			SUB r9d, 3
			JS exit

				MOV r12d, [r11+rax*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit
		JMP x_loop

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Read24	ENDP

ALIGN 16
Bitmap_Copy32	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		MOV edx, r13d
		
		MOV rdi, r8
		

	align 16
		x_loop:
			MOV eax, [rcx]
			BSWAP eax
			SHR eax, 8
			OR eax, 0FF000000h

			ADD rcx, 4
			
			SUB r9d, 4
			JS exit

				MOVNTI [rdi], eax
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
					JMP x_loop				

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Copy32	ENDP

ALIGN 16
Bitmap_Read32	PROC
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]
		MOV r11, [rdx+8] ; colors

		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		MOV edx, r13d
		
		MOV rdi, r8
	
		XOR rax, rax
		XOR r10, r10

	align 16
		x_loop:
			MOV eax, [rcx]

			ADD rcx, 4
			
			SUB r9d, 4
			JS exit

				MOV r12d, [r11+rax*4]
				OR r12d, 0FF000000h
				BSWAP r12d
				ROR r12d, 8

				MOVNTI [rdi], r12d
				ADD rdi, 4								

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, edx
					
		JMP x_loop

	exit:

		MOV rax, rcx

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi

	RET
Bitmap_Read32	ENDP

ALIGN 16
Bitmap_MaskRead32	PROC
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	
		MOV rsi, rcx

		MOV r14, [rdx]

		MOV r13d, [rdx+16]
		MOV r15, [rdx+32]
		
		MOV rdi, r8

		MOV r10, rcx

		BSR ecx, DWORD PTR [rdx+20]		
		BSR r11d, DWORD PTR [rdx+24]		
		BSR ebx, DWORD PTR [rdx+28]
		
		SHL ecx, 8
		SHL ebx, 8
		MOV bl, r11b
		
		SUB ch, 7
		SUB bl, 15
		SUB bh, 23	


		XOR rax, rax

	align 16
		x_loop:
			MOV eax, [r10]

			ADD r10, 4
			
			SUB r9d, 4
			JS exit
				MOV r12d, 0FF000000h

				MOV r11d, [rdx+20]
				AND r11d, eax
				MOV cl, ch

				NEG cl
				JNS red_high
					MOV cl, ch
					SHR r11d, cl
					JMP set_red
				red_high:
					SHL r11d, cl
				set_red:
					OR r12d, r11d


				MOV r11d, [rdx+24]
				AND r11d, eax
				MOV cl, bl

				NEG cl
				JNS green_high
					MOV cl, bl
					SHR r11d, cl
					JMP set_green
				green_high:
					SHL r11d, cl
				set_green:
					AND r11d, 0FF00h
					OR r12d, r11d



				MOV r11d, [rdx+28]
				AND r11d, eax
				MOV cl, bh
	
				NEG cl
				JNS blue_high
					MOV cl, bh
					SHR r11d, cl
					JMP set_blue
				blue_high:
					SHL r11d, cl
				set_blue:
					AND r11d, 0FF0000h
					OR r12d, r11d


				MOVNTI [rdi], r12d
				ADD rdi, 4

				DEC r13d
				JNZ x_loop
					SUB r8, r15
					CMP r14, r8
					JA exit
						MOV rdi, r8
						MOV r13d, [rdx+16]

					JMP x_loop				

	exit:

		MOV rax, r10

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx

	RET
Bitmap_MaskRead32	ENDP


ALIGN 16
Bitmap_AlphaSet	PROC
	PUSH rsi
	PUSH rdi
	PUSH r15
	PUSH r14
	
		XOR r15, r15
		MOV rsi, rcx
				
		MOV r14, rdx
		MOV rdi, rdx

		XOR rdx, rdx
		MOV eax, r8d
		
		MUL r9
		SUB eax, r8d
		SHL rax, 2

		ADD rdi, rax

		MOV eax, r8d
		MOV rdx, 3
		SHR eax, 3
		ADD eax, edx
		NOT rdx
		AND rax, rdx
		
		XOR rdx, rdx

		MUL r9d
		MOV r9d, eax		

		MOV r15d, r8d

		MOV rax, rcx

			MOV ecx, 4 ; [?size_paragraph@Image@Pics@@1IB]
			MOV r11d, 1
			SHL r11d, cl
			SUB r11d, 1

			ADD r15d, r11d
			NOT r11d
			AND r15d, r11d
			SHL r15, 2
		
		MOV rcx, rax

		XOR rax, rax
		MOV r11d, r8d

		MOV rdx, rdi		


	align 16
		x_loop:
			MOV al, [rcx]
			MOV ah, 8

			INC rcx
			
			DEC r9d
			JS exit

		align 16
			byte_loop:
				XOR r10, r10

				SHL al, 1
				RCL r10b, 1
				DEC r10b

				MOV [rdi+3], r10b
				ADD rdi, 4

				DEC r8d
				JNZ nextpi_1
					SUB rdx, r15
					CMP r14, rdx
					JA exit
						MOV rdi, rdx
						MOV r8d, r11d						
					
						MOV rax, rcx
						SUB rax, rsi
					
						DEC rax
						XOR rax, 3
						AND rax, 3
											
						ADD rcx, rax
						SUB r9d, eax
						JS exit

					JMP x_loop				
				nextpi_1:

			DEC ah
			JNZ byte_loop
		JMP x_loop

	exit:

	POP r14
	POP r15
	POP rdi
	POP rsi
	RET
Bitmap_AlphaSet	ENDP



END

