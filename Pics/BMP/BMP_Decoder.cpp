/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\BMP.h"

#include "System\SysUtils.h"

#include "Pics\PNG.h"
// brga color space

#pragma warning(disable:4244)

Pics::Bitmap::Bitmap() {

}

Pics::Bitmap::~Bitmap() {

}

UI_64 Pics::Bitmap::Decode(const InfoHeader * ihdr, const unsigned char * img_ptr, unsigned char * pipo, unsigned int fsize) {
	UI_64 result(reinterpret_cast<UI_64>(img_ptr));
	
	ReadConfig r_cfg;
	
	r_cfg.first_line = pipo;	
	r_cfg._rgba = img_ptr;

	r_cfg.width = ihdr->width;
	r_cfg.l_offset = (ihdr->width + (1<<size_paragraph) - 1) & ( -1 << size_paragraph);
	r_cfg.l_offset <<= 2;

	r_cfg._mask[0] = r_cfg._mask[1] = r_cfg._mask[2] = -1;

	switch (ihdr->bit_count) {
		case 1:
			img_ptr += 8;
			result = Bitmap_Read1(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + 8;							
		break;
		case 4:
			img_ptr += 64;
			if (ihdr->compression == BI_RLE4) {
				result = Bitmap_ReadRLE4(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
			} else {
				result = Bitmap_Read4(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
			}
			result += 64;
		break;
		case 8:
			img_ptr += 1024;
			if (ihdr->compression == BI_RLE8) {
				result = Bitmap_ReadRLE8(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
			} else {
				result = Bitmap_Read8(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
			}
			result += 1024;
		break;
		case 16:
			if (ihdr->compression == BI_BITFIELDS) {				
				const unsigned int * h_mask = reinterpret_cast<const unsigned int *>(ihdr+1);
				r_cfg._mask[0] = h_mask[0];
				r_cfg._mask[1] = h_mask[1];
				r_cfg._mask[2] = h_mask[2];
					
				img_ptr += 12;
				result = Bitmap_MaskRead16(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + 12;
			} else {
				if (ihdr->index_count == 0) {
					r_cfg._mask[0] = 0x00007C00;
					r_cfg._mask[1] = 0x000003E0;
					r_cfg._mask[2] = 0x0000001F;

					result = Bitmap_MaskRead16(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
				} else {
					img_ptr += ihdr->index_count;
					result = Bitmap_Read16(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + ihdr->index_count;
				}
			}
		break;
		case 24:
			if (ihdr->index_count == 0) {
				result = Bitmap_Copy24(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
			} else {
				img_ptr += ihdr->index_count;
				result = Bitmap_Read24(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + ihdr->index_count;
			}							
		break;
		case 32:
			if (ihdr->compression == BI_BITFIELDS) {				
				const unsigned int * h_mask = reinterpret_cast<const unsigned int *>(ihdr+1);
				r_cfg._mask[0] = h_mask[0];
				r_cfg._mask[1] = h_mask[1];
				r_cfg._mask[2] = h_mask[2];

				img_ptr += 12;

				result = Bitmap_MaskRead32(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + 12;
			} else {
				if (ihdr->index_count == 0) {
					result = Bitmap_Copy32(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize);
				} else {
					img_ptr += ihdr->index_count;
					result = Bitmap_Read32(img_ptr, &r_cfg, pipo+r_cfg.l_offset*(ihdr->height-1), fsize) + ihdr->index_count;
				}
			}
							
		break;
	}

	return (result - reinterpret_cast<UI_64>(img_ptr));
}

UI_64 Pics::Bitmap::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	const unsigned char * img_ptr(0);
	UI_64 skipdi(0), result(0);
	unsigned int signature[8], signon(fsize);

	SFSco::Object pi_obj;

	imid = -1;

	if (fsize > 16384) signon = 16384;

	if (IsBMP(reinterpret_cast<const char *>(src_ptr), signon)) {
		const FileHeader * fhdr = reinterpret_cast<const FileHeader *>(src_ptr + 2);
		const InfoHeader * ihdr = reinterpret_cast<const InfoHeader *>(src_ptr + sizeof(FileHeader) + 2);

		img_ptr = reinterpret_cast<const unsigned char *>(ihdr) + ihdr->s_size;

		SFSco::sha256(signature, reinterpret_cast<const char *>(src_ptr), fsize, 1);

		imid = pi_list.FindIt(signature, 32);
		if (imid != -1) {
			return 69;
		}

		if (ihdr->bit_count) {
			c_width = (ihdr->width + (1<<size_paragraph) - 1) & (-1 << size_paragraph); c_height = (ihdr->height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
			
			result = pi_obj.New(image_rstr_tid, c_width*c_height*4, SFSco::large_mem_mgr);

			if (result != -1) {
				Image::Properties sprops;

				if (unsigned char * pipo = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
					Decode(ihdr, img_ptr, pipo, fsize);
					pi_obj.Release();
				}


				sprops.im_obj = pi_obj;

				for (unsigned int i(0);i<8;i++) sprops.signature[i] = signature[i];

				sprops.width = ihdr->width;
				sprops.height = ihdr->height;
				sprops.c_width = c_width;
				sprops.c_height = c_height;

				imid = pi_list.Add(sprops);	
				
				result = 123;

			}

		} else {
			// wrapper type
		}

	}

	return result;
}


UI_64 Pics::Bitmap::IsBMP(const char * ipo, UI_64 lim) {
	char smarker[] = {0x42, 0x4D, 0x00};

	return ((ipo[0] == smarker[0]) && (ipo[1] == smarker[1]));
//	return reinterpret_cast<UI_64>(SearchBlock(ipo, lim, smarker, 2));

}


// icon =================================================================================================================================================================================================================================

Pics::Icon::Icon() {
	first_image_id = previous_image_id = -1;
}

Pics::Icon::~Icon() {


}


UI_64 Pics::Icon::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	const unsigned char * img_ptr(0);
	UI_64 result(0), img_po(0), silim(0);
	unsigned int signature[8], signon(fsize), i_size(0);
	Image::Properties iprop;
	
	SFSco::Object pi_obj;

	imid = -1;

	if (fsize > 16384) signon = 16384;

	if (IsICO(reinterpret_cast<const char *>(src_ptr), signon)) {
		const DirHeader * fhdr = reinterpret_cast<const DirHeader *>(src_ptr);
		const EntryHeader * ehdr = reinterpret_cast<const EntryHeader *>(src_ptr + 6);

		SFSco::sha256(signature, reinterpret_cast<const char *>(src_ptr), fsize, 1);

		imid = pi_list.FindIt(signature, 32);
		if (imid != -1) {
			return 69;
		}

		result = 0;
		

		for (unsigned short i(0);i<fhdr->entry_count;i++) {
			img_ptr = src_ptr + ehdr[i].offset;

			if (Pics::PNG::IsPNG(reinterpret_cast<const char *>(img_ptr), 16)) {

				PNG pnguk;
				result = pnguk.Load(img_ptr, fsize - ehdr[i].offset, imid);

				if (imid != -1) {
					iprop = pi_list[imid];
					i_size = iprop.width;
										
					if (previous_image_id != -1) {
						if (iprop = pi_list[previous_image_id]) {
							if (i_size < iprop.width) {
								iprop.next_index = imid;
							} else {
								i_size = 0;
							}

							pi_list.Set(previous_image_id, iprop);
						}
					}
					
					if (i_size) {
						if (first_image_id == -1) first_image_id = imid;
						previous_image_id = imid;
					} else {
						iprop = pi_list.Remove(imid);
						iprop.Destroy();
						imid = previous_image_id;
					}
				}

			} else {
				
				Bitmap::InfoHeader ihdr = reinterpret_cast<const Bitmap::InfoHeader *>(img_ptr)[0];
				
				if (ehdr[i].width) ihdr.width = ehdr[i].width;
				else ihdr.width = 256;

				if (ehdr[i].height) ihdr.height = ehdr[i].height;
				else ihdr.height = 256;

				c_width = (ihdr.width + (1<<size_paragraph) - 1) & (-1 << size_paragraph); c_height = (ihdr.height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);

				silim = ehdr[i].size;

				if (i>0) SFSco::sha256(signature, reinterpret_cast<const char *>(src_ptr), silim, 0x80000001);

				result = pi_obj.New(image_rstr_tid, c_width*c_height*4, SFSco::large_mem_mgr);

				if (result != -1) {					
					if (unsigned char * pipo = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
						img_ptr += ihdr.s_size;
						img_po = Bitmap::Decode(&ihdr, img_ptr, pipo, silim);
						if (!img_ptr) {
							result = 36479641;	
							break;
						}

						img_po += 3; img_po &= ~(UI_64)3;

						Bitmap_AlphaSet(img_ptr+img_po, pipo, ihdr.width, ihdr.height);
												
						pi_obj.Release();

						result = 0;
					}

					if (!result) {
						Image::Properties sprops;
												
						sprops.im_obj = pi_obj;
									
						for (unsigned int i(0);i<8;i++) sprops.signature[i] = signature[i];
												
						sprops.width = ihdr.width;
						sprops.height = ihdr.height;
						sprops.c_width = c_width;
						sprops.c_height = c_height;
						
						i_size = -1;

						if (previous_image_id != -1) {
							iprop = pi_list[previous_image_id];

							if (sprops.width > iprop.width) {
								i_size = 0;
							}
						}
						
						if (i_size) {
							imid = pi_list.Add(sprops);

							if (previous_image_id != -1) {
								iprop.next_index = imid;

								pi_list.Set(previous_image_id, iprop);
							}

							if (first_image_id == -1) first_image_id = imid;
							previous_image_id = imid;
						} else {
							sprops.Destroy();
						}

					}

				}

			}

		}

		if (previous_image_id != first_image_id) {
			if (Image::Properties iprop = pi_list[previous_image_id]) {
				iprop.next_index = first_image_id;
				pi_list.Set(previous_image_id, iprop);
			}
		}

		imid = first_image_id;
	}

	return result;
}


UI_64 Pics::Icon::IsICO(const char * ipo, UI_64 lim) {
	return ((ipo[0] == 0) && (ipo[1] == 0) && (ipo[3] == 0) && ((ipo[2] == 1) || (ipo[2] == 2)));
}

