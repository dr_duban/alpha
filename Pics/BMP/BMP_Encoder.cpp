/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\BMP.h"
#include "System\SysUtils.h"

#pragma warning(disable:4244)

UI_64 Pics::Bitmap::Save(unsigned int iid, void * f_out, unsigned short bit_count, SFSco::Object & e_buffer) {
	unsigned int wi(0), hi(0), cwi(0), chi(0), * i_ptr(0), bb_count(0), ftype(bit_count & 0x00FF00);
	UI_64 result(0), coff(0);
	DWORD  bcount(0);
	

	FileHeader bmp_hdr;
	InfoHeader inf_hdr;

//	SFSco::Object pi_obj;

	bit_count &= 0x00FF;
	if (bit_count > 1 ) {
		if (bit_count < 16) {
			if ((bit_count > 1) && (bit_count < 5)) bb_count = 4;
			else if ((bit_count > 4) && (bit_count < 9)) bb_count = 8;
				else if (bit_count > 8) bb_count = 16;
		} else {
			bit_count = 24;
			bb_count = 24;
		}
	} else {
		bit_count = 1;
		bb_count = 1;
	}

	if (Properties iprops = pi_list[iid]) {			
		wi = iprops.width;
		hi = iprops.height;
		cwi = iprops.c_width;
		chi = iprops.c_height;
			
// write header
		WriteFile(f_out, "BM", 2, &bcount, 0);

// file header
		bmp_hdr.size = 0;
		bmp_hdr._reserved[0] = 0;
		bmp_hdr._reserved[0] = 0;
		bmp_hdr.data_offset = 0;

		WriteFile(f_out, &bmp_hdr, sizeof(FileHeader), &bcount, 0);
// info header
		inf_hdr.s_size = sizeof(InfoHeader);
		inf_hdr.width = wi;
		inf_hdr.height = hi;
		inf_hdr.planes = 1;		
		inf_hdr.bit_count = bb_count; //bit_count;
		inf_hdr.compression = BI_RGB;
		inf_hdr.image_size = 0;

		inf_hdr.x_pels = inf_hdr.y_pels = 0;
	
		inf_hdr.index_count = 0;
		inf_hdr.index_range = 0;

		WriteFile(f_out, &inf_hdr, sizeof(InfoHeader), &bcount, 0);

// data
		if (unsigned char * e_line = reinterpret_cast<unsigned char *>(e_buffer.Acquire())) {
			if (unsigned int * pipo = reinterpret_cast<unsigned int *>(iprops.im_obj.Acquire())) {
	//		MemoryFill_SSE3(e_line, temp_eobj_size, 0, 0);
				
				if (bit_count >= 24) {
					pipo += cwi*hi;

					bmp_hdr.data_offset = GetFileSize(f_out, 0);					
					
					if (bit_count == 24) {
						chi = (wi*3 + 3) & (~3);

						for (unsigned int i(0);i<hi;i++) {
							pipo -= cwi;

							for (unsigned int j(0);j<wi;j++) {
								e_line[3*j] = (pipo[j]>>16) & 0xFF;
								e_line[3*j+1] = (pipo[j]>>8) & 0xFF;
								e_line[3*j+2] = pipo[j] & 0xFF;
							}

							WriteFile(f_out, e_line, chi, &bcount, 0);
							
						}

					} else {
						for (unsigned int i(0);i<hi;i++) {
							pipo -= cwi;
							WriteFile(f_out, pipo, wi*4, &bcount, 0);
						}
					}
						
				} else {
					System::MemoryFill_SSE3(e_line, histogram_size, 0, 0);
					coff = Median(reinterpret_cast<UI_64 *>(e_line), pipo, wi, hi, (1<<bit_count));

					e_line += histogram_size + coff;

					if (bb_count>1)	Image_PaletteSwap(e_line);
					else {
						e_line[15] = e_line[16];
						e_line[16] = e_line[18];
						e_line[18] = e_line[15];

						e_line[15] = e_line[20];
						e_line[20] = e_line[22];
						e_line[22] = e_line[15];
					}

					if (bb_count != bit_count) {					
						for (unsigned int ki(1 << bit_count);ki<(1 << bb_count);ki++) reinterpret_cast<unsigned int *>(e_line)[16+ki] = reinterpret_cast<unsigned int *>(e_line)[16];
							 
					}

					switch (bb_count) {
						case 8:	
							WriteFile(f_out, e_line + 16, 1024, &bcount, 0);
						break;
						case 4:							
							WriteFile(f_out, e_line + 16, 64, &bcount, 0);
						break;
						case 1:							
							WriteFile(f_out, e_line + 16, 8, &bcount, 0);
						break;
						case 16:
								
						break;
					}

					if (bb_count>1)	Image_PaletteSwap(e_line);
					else {
						e_line[15] = e_line[16];
						e_line[16] = e_line[18];
						e_line[18] = e_line[15];

						e_line[15] = e_line[20];
						e_line[20] = e_line[22];
						e_line[22] = e_line[15];
					}

					bmp_hdr.data_offset = GetFileSize(f_out, 0);
										
					switch (ftype) {
						case 0: // nearest
							Image_Nearest_I(pipo, reinterpret_cast<UI_64 *>(e_line-coff), wi, hi);
/*
						if (bit_count>1) SFSco::Image_Nearest(pipo, reinterpret_cast<unsigned int *>(e_line), wi, hi);
						else Nearest(pipo, reinterpret_cast<unsigned int *>(e_line), wi, hi);
*/

						break;
						case 0x100: // dither
							SFSco::Image_Dither_I(pipo, reinterpret_cast<UI_64 *>(e_line-coff), wi, hi);
/*
						if (bit_count>1) SFSco::Image_Dither(pipo, reinterpret_cast<unsigned int *>(e_line), wi, hi);
						else Dither(pipo, reinterpret_cast<unsigned int *>(e_line), wi, hi);
*/
						break;

					}

					pipo += cwi*hi;

					switch (bb_count) {
						case 8:
							for (unsigned int ij(0);ij<hi;ij++) {
								pipo -= cwi;
								
								chi = FormatXXLine(e_line+2048, pipo, wi);
								WriteFile(f_out, e_line+2048, chi, &bcount, 0);
							}

							e_line[2048] = 0; e_line[2049] = 1;
							WriteFile(f_out, e_line+2048, 2, &bcount, 0);
						break;
						case 4:
							for (unsigned int ij(0);ij<hi;ij++) {
								pipo -= cwi;
								
								chi = FormatXLine(e_line+2048, pipo, wi);
								WriteFile(f_out, e_line+2048, chi, &bcount, 0);
							}
							e_line[2048] = 0; e_line[2049] = 1;
							WriteFile(f_out, e_line+2048, 2, &bcount, 0);
						break;
						case 1:							
							for (unsigned int ij(0);ij<hi;ij++) {
								pipo -= cwi;
								
								chi = FormatBWLine(e_line+2048, pipo, wi);
								WriteFile(f_out, e_line+2048, chi, &bcount, 0);
							}							
						break;
						case 16:
								
						break;
					}
									
				}

				iprops.im_obj.Release();
			}

			e_buffer.Release();			
		}
	
		bmp_hdr.size = GetFileSize(f_out, 0);

		SetFilePointer(f_out, 2, 0, 0);
		WriteFile(f_out, &bmp_hdr, sizeof(FileHeader), &bcount, 0);
	}

	return result;
}


unsigned int Pics::Bitmap::FormatBWLine(unsigned char * fline, const unsigned int * pipo, unsigned int wi) {
	unsigned int result(0), xval(0), twi(wi >> 3);

	for (unsigned int ii(0);ii<twi;ii++) {
		xval = pipo[ii*8]; xval <<= 1;
		xval |= pipo[ii*8+1]; xval <<= 1;
		xval |= pipo[ii*8+2]; xval <<= 1;
		xval |= pipo[ii*8+3]; xval <<= 1;
		xval |= pipo[ii*8+4]; xval <<= 1;
		xval |= pipo[ii*8+5]; xval <<= 1;
		xval |= pipo[ii*8+6]; xval <<= 1;
		xval |= pipo[ii*8+7];

		fline[result++] = xval;
	}

	if (wi &= 7) {
		xval = pipo[twi*8];
		for (unsigned int jj(1);jj<wi;jj++) {
			 xval <<= 1; xval |= pipo[twi*8+jj];
		}
		
		fline[result++] = (xval << (8-wi));
	}

	for (;result & 3;) fline[result++] = 0;

	return result;

}



unsigned int Pics::Bitmap::FormatXXLine(unsigned char * fline, const unsigned int * pipo, unsigned int wi) {
	unsigned int result(0), t_val(0), twi(wi>>2);
	

	for (unsigned int ii(0);ii<twi;ii++) {
		t_val = pipo[ii*4];
		t_val |= (pipo[ii*4+1] << 8);
		t_val |= (pipo[ii*4+2] << 16);
		t_val |= (pipo[ii*4+3] << 24);

		reinterpret_cast<unsigned int *>(fline)[result++] = t_val;
	}

	if (wi &= 3) {
		t_val = pipo[twi*4];
		for (unsigned int ii(1);ii<wi;ii++) t_val |= (pipo[twi*4+ii] << ii*8);

		reinterpret_cast<unsigned int *>(fline)[result++] = t_val;
	}
	

	return (result<<2);
}


unsigned int Pics::Bitmap::FormatXLine(unsigned char * fline, const unsigned int * pipo, unsigned int wi) {
	unsigned int result(0), t_val(0), twi(0);
	
	twi = (wi >> 3);
	for (unsigned int ii(0);ii<twi;ii++) {
		t_val = ((pipo[ii*8] << 4) | pipo[ii*8+1]);
		t_val |= (((pipo[ii*8+2] << 4) | pipo[ii*8+3]) << 8);
		t_val |= (((pipo[ii*8+4] << 4) | pipo[ii*8+5]) << 16);
		t_val |= (((pipo[ii*8+6] << 4) | pipo[ii*8+7]) << 24);

		reinterpret_cast<unsigned int *>(fline)[result++] = t_val;
	}

	if (wi &= 7) {
		if (wi & 1) t_val = (pipo[twi*8+wi-1] << wi*4);
		
		wi >>= 1;
		
		for (unsigned int jj(0);jj<wi;jj++) t_val |= (((pipo[twi*8+jj*2] << 4) | pipo[twi*8+jj*2+1]) << jj*8);

		reinterpret_cast<unsigned int *>(fline)[result++] = t_val;
	}


	return (result<<2);
}


