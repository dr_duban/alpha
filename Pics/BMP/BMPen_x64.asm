
.CODE


ALIGN 16
?FormatBWLine@Bitmap@Pics@@CAIPEAEPEBII@Z	PROC
	PUSH rdi
	
	MOV rdi, rcx

	ADD r8d, 7
	SHR r8d, 3

align 16
	s_loop:
		XOR eax, eax
		OR eax, [rdx]
		SHL eax, 1
		OR eax, [rdx+4]
		SHL eax, 1
		OR eax, [rdx+8]
		SHL eax, 1
		OR eax, [rdx+12]
		SHL eax, 1
		OR eax, [rdx+16]
		SHL eax, 1
		OR eax, [rdx+20]
		SHL eax, 1
		OR eax, [rdx+24]
		SHL eax, 1
		OR eax, [rdx+28]
		
		STOSB
		ADD rdx, 32

	DEC r8d
	JNZ s_loop

	MOV rax, rdi
	SUB rax, rcx

	ADD rax, 3
	SHR rax, 2
	SHL rax, 2


	POP rdi
	RET
?FormatBWLine@Bitmap@Pics@@CAIPEAEPEBII@Z	ENDP


ALIGN 16
?Set16@Bitmap@Pics@@CA_KPEAIPEBIII@Z	PROC

	RET
?Set16@Bitmap@Pics@@CA_KPEAIPEBIII@Z	ENDP


ALIGN 16
?Set256@Bitmap@Pics@@CA_KPEAIPEBIII@Z	PROC

	RET
?Set256@Bitmap@Pics@@CA_KPEAIPEBIII@Z	ENDP

END