/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Pics\Image.h"

namespace Pics {

	class PNG: public Image {
		public:
			struct DecoderSet {
				UI_64 pi_color, c_predictor, t_color;
				UI_64 cl_offset, line_pointer, pi_offset, piline_offset;

				UI_64 first_image_id, previous_image_id;

				unsigned int chunk_num;
				unsigned int line_position;

				unsigned char filter_type, c_index, c_count;
				unsigned char i_pass;

				unsigned int _reserved[3];

			};


		private:
			static const unsigned char dt_map[];

			struct Header {
				unsigned int width, c_width;
				unsigned int height, c_height;
				unsigned int left, top;

				unsigned char precision;
				unsigned char bit_count; // 1, 2, 4, 8, 16
				unsigned char col_type; // 0 - grayscale, 2 - true color, 3 - index, 4 - gray alpha, 6 - true alpha
				unsigned char compression;

				unsigned char filter;
				unsigned char interlace;

			};


			
			const unsigned char * ReadX_0(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadX_80(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadX_81(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadX_82(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadX_83(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadX_84(const unsigned char *, unsigned int *, UI_64 &);
						
			const unsigned char * CopyGSA_0(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * CopyGSA_1(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * CopyGSA_2(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * CopyGSA_3(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * CopyGSA_4(const unsigned char *, unsigned int *, UI_64 &);
						

			DecoderSet _decoder;
			Header _header;

			UI_64 interlace_set[8][5];
			unsigned int palette[256];
			

			SFSco::Object p_line;
						
			void LoadHeader(const unsigned char *);
			void SetInterlace();
			void DecoderReset();

			const unsigned char * ReadLine_TC(const unsigned char *, unsigned int *, UI_64 &);			
			const unsigned char * ReadLine_GSA(const unsigned char *, unsigned int *, UI_64 &);
			const unsigned char * ReadLine_X(const unsigned char *, unsigned int *, UI_64 &);

			static const char smarker[];

		public:
			static UI_64 InitializeEncoder();

			static UI_64 IsPNG(const char *, UI_64 limit);
			static UI_64 Save(unsigned int, HANDLE, Pack::Deflate &);

			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);
			
			PNG();
			virtual ~PNG();
	};


	extern "C" {
		const unsigned char * PNG_CopyColor_0(PNG::DecoderSet &, const unsigned char *, unsigned int *, UI_64 &);
		const unsigned char * PNG_CopyColor_1(PNG::DecoderSet &, const unsigned char *, unsigned int *, UI_64 &);
		const unsigned char * PNG_CopyColor_2(PNG::DecoderSet &, const unsigned char *, unsigned int *, UI_64 &);
		const unsigned char * PNG_CopyColor_3(PNG::DecoderSet &, const unsigned char *, unsigned int *, UI_64 &);


		const unsigned char * PNG_CopyColor_4_SSSE3(void *, const unsigned char *, unsigned int *, UI_64 &);
		const unsigned char * PNG_CopyColor_4_SSE3(void *, const unsigned char *, unsigned int *, UI_64 &);

		void PNG_FilterLine_SSSE3(unsigned char *, const unsigned int *, unsigned int, UI_64 &);
		void PNG_FilterLine_SSE3(unsigned char *, const unsigned int *, unsigned int, UI_64 &);

	}

}


