/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Pics\Image.h"
#include "Pics\JPEG.h"


namespace Pics {

	class TIFF: public Image {
		private:
			struct Header {
				float calibration_transform[2][16];
				float color_transform[2][16];
				float chromatic_adaptation[2][16];
				float analog_balance[4], white_balance[4];


				unsigned int strip_offsets, rows_per_strip, strip_byte_count, sub_ifd_count, sub_ifd_offset, current_ifd;
				unsigned int width, height, c_height, c_width, t_width, t_height, alpha_on, ccitt_options;
				unsigned int f_subtype, current_page_id, precision;
				float temperature;
				unsigned int x_res[2], y_res[2];
				unsigned int level_offset[4], level_scale[4];
				unsigned int hsv_cfg[4];

				unsigned short bit_count[4], sample_count, interleaved, fill_order, tiled, strip_count;
				unsigned short compression, data_align, lzw_predictor;
				unsigned short color_type;
				
				unsigned short resolution_unit, transform_type[2];
				unsigned char dng_flag, c_reserved;
				unsigned char cfa_cfg[32];
				unsigned char so_type, sbc_type, scale_flag, transform_flag;
								
			};

			__declspec(align(16)) Header _header;

			SFSco::Object li_table, hsv_map;

			unsigned int byte_order, ltab_count;
			

			JPEG jpguk;
			unsigned int palette[256];

			unsigned int strip_2[4];
			
			void CalculateTransform(float *);

			UI_64 LoadHeader(const unsigned char *, UI_64);
			UI_64 LoadHeader_2(const unsigned char *, UI_64);

			UI_64 XUnPack(const unsigned char *, SFSco::Object &, unsigned int);
			UI_64 IUnPack(const unsigned char *, SFSco::Object &, unsigned int);
			UI_64 CopyRGB(const unsigned char *, SFSco::Object &, unsigned int);
			UI_64 CopyCMYK(const unsigned char *, SFSco::Object &, unsigned int);
			
			UI_64 JpegDecode(const unsigned char *, SFSco::Object &, unsigned int);
			UI_64 JpegGetTables(const unsigned char *, unsigned int, unsigned int);

			UI_64 DeMosaic(const unsigned char *, SFSco::Object &, unsigned int);
			UI_64 DeMosaic(SFSco::Object &);

			UI_64 GrayOUT(const unsigned char *, SFSco::Object &, unsigned int);


			static void InterpolateMatrix(float *, float mt[2][16], float *);

			static void LoadMatrix(float *, const unsigned char *, const unsigned short *);
			static void LoadMatrix2(float *, const unsigned char *, const unsigned short *);

			static __declspec(align(16)) const unsigned char white_counts[];
			static __declspec(align(16)) const unsigned short white_codes[];
			static __declspec(align(16)) const unsigned short white_vals[];


			static __declspec(align(16)) const unsigned char black_counts[];
			static __declspec(align(16)) const unsigned short black_codes[];
			static __declspec(align(16)) const unsigned short black_vals[];

			static __declspec(align(16)) const unsigned char counts_2D[];
			static __declspec(align(16)) const unsigned short codes_2D[];

		public:
			static UI_64 IsTIFF(const char *, UI_64 limit);

			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);
			
			TIFF();
			virtual ~TIFF();
	};


	extern "C" {

		UI_64 TIFF_BCopy_1X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_BCopy_4X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_BCopy_8X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_ICopy_8(Image::StripDesc &, unsigned int *, unsigned int);

		UI_64 TIFF_BUnPack_1X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_BUnPack_4X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_BUnPack_8X(Image::StripDesc &, unsigned int *, const unsigned int *);

		UI_64 TIFF_T3UnPack(Image::StripDesc &, unsigned int *);
		UI_64 TIFF_T41DUnPack(Image::StripDesc &, unsigned int *);
		UI_64 TIFF_T42DUnPack(Image::StripDesc &, unsigned int *);

		UI_64 TIFF_SampleCopy_4(Image::StripDesc &, unsigned int *);
		UI_64 TIFF_SampleCopy_3(Image::StripDesc &, unsigned int *);
		
		UI_64 TIFF_PredictSample_4(Image::StripDesc &, unsigned int *);
		UI_64 TIFF_PredictSample_3(Image::StripDesc &, unsigned int *);

		UI_64 TIFF_BPredict_8X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_BPredict_4X(Image::StripDesc &, unsigned int *, const unsigned int *);
		UI_64 TIFF_IPredict_8(Image::StripDesc &, unsigned int *, unsigned int);

		void TIFF_Detwist_1(Image::StripDesc &, unsigned int *);
		void TIFF_Detwist_2(Image::StripDesc &, unsigned int *);

		UI_64 TIFF_GrayOut_1(Image::StripDesc &, unsigned char *);
		UI_64 TIFF_GrayOut_2(Image::StripDesc &, unsigned char *);
		
	}

}


