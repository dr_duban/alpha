/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Pics\Image.h"


namespace Pics {

	class GIF: public Image {		
		private:
			struct DecoderSet {
				UI_64 cl_offset, line_pointer, piline_offset;
				UI_64 first_image_id, previous_image_id;

				unsigned short line_position;
				unsigned char i_pass, c_reserved;
			};

			struct ScreenDesc {
				unsigned short width;
				unsigned short height;

				unsigned char bit_count : 3;
				bool : 1;

				unsigned char color_res : 3;
				bool color_map : 1;

				unsigned char bgrnd_index;
				unsigned char _zero;
			};

			struct ImageDesc {
				unsigned short left;
				unsigned short top;
				unsigned short width;
				unsigned short height;

				unsigned char bit_count : 3;
				bool : 1;

				bool : 1;
				bool : 1;
				bool interlace : 1;
				bool color_map : 1;

			};

			struct ControlSet {
				unsigned int delay, t_index, type;
				
			};

			unsigned int palette[512];
			UI_64 interlace_set[5][3];

			unsigned int c_width, c_height;

			DecoderSet _decoder;
			ControlSet _cSet;

			void SetInterlace(const ImageDesc *);
			void DecoderReset();
		public:
			static UI_64 IsGIF(const char *, UI_64 limit);

			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);
			
			GIF();
			virtual ~GIF();
	};



}


