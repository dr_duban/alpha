/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"

#include "Pics\TIFF.h"
#include "Compression\LZW.h"

#pragma warning(disable:4244)

__declspec(align(16))
const unsigned char Pics::TIFF::white_counts[] = {0, 0, 0, 6, 6, 9, 12, 42, 16, 0, 3, 10, -1};

__declspec(align(16))
const unsigned short Pics::TIFF::white_codes[] = {
													0x0007, 0x0008, 0x000B, 0x000C, 0x000E, 0x000F,
													0x0007, 0x0008, 0x0012, 0x0013, 0x0014, 0x001B,
													0x0003, 0x0007, 0x0008, 0x0017, 0x0018, 0x002A, 0x002B, 0x0034, 0x0035,
													0x0003, 0x0004, 0x0008, 0x000C, 0x0013, 0x0017, 0x0018, 0x0024, 0x0027, 0x0028, 0x002B, 0x0037,

													0x0002, 0x0003, 0x0004, 0x0005, 0x000A, 0x000B, 0x0012, 0x0013, 0x0014, 0x0015,
													0x0016, 0x0017, 0x001A, 0x001B, 0x0024, 0x0025, 0x0028, 0x0029, 0x002A, 0x002B,
													0x002C, 0x002D, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x004A, 0x004B,													
													0x0052, 0x0053, 0x0054, 0x0055, 0x0058, 0x0059, 0x005A, 0x005B, 0x0064, 0x0065, 
													0x0067, 0x0068,
													
													0x0098, 0x0099, 0x009A, 0x009B, 0x00CC, 0x00CD, 0x00D2, 0x00D3,
													0x00D4, 0x00D5,	0x00D6, 0x00D7,	0x00D8, 0x00D9, 0x00DA, 0x00DB,

													0x0008, 0x000C, 0x000D,

													0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0017, 0x001C, 0x001D, 0x001E, 0x001F

												};

__declspec(align(16))
const unsigned short Pics::TIFF::white_vals[] = {
													2, 3, 4, 5, 6, 7,
													10, 11, 128, 8, 9, 64,
													13, 1, 12, 192, 1664, 16, 17, 14, 15,
													22, 23, 20, 19, 26, 21, 28, 27, 18, 24, 25, 256,

													 29,  30, 45, 46, 47, 48,  33,  34,  35,  36,
													 37,  38, 31, 32, 53, 54,  39,  40,  41,  42,
													 43,  44, 61, 62, 63,  0, 320, 384,  59,  60,
													 49,  50, 51, 52, 55, 56,  57,  58, 448, 512,
													640, 576,

													1472, 1536, 1600, 1728,  704,  768,  832,  896,
													 960, 1024, 1088, 1152, 1216, 1280, 1344, 1408,

													 1792, 1856, 1920,

													 1984, 2048, 2112, 2176, 2240, 2304, 2368, 2432, 2496, 2560
												};

__declspec(align(16))
const unsigned char Pics::TIFF::black_counts[] = {0, 2, 2, 2, 1, 2, 3, 2, 1, 5, 10, 54, 20, -1};

__declspec(align(16))
const unsigned short Pics::TIFF::black_codes[] = {
													0x0002, 0x0003,
													0x0002, 0x0003,
													0x0002, 0x0003,
													0x0003,

													0x0004, 0x0005,
													0x0004, 0x0005, 0x0007,
													0x0004, 0x0007,
													0x0018,
													0x0008, 0x000F, 0x0017, 0x0018, 0x0037,
													0x0008, 0x000C, 0x000D, 0x0017, 0x0018, 0x0028, 0x0037, 0x0067, 0x0068, 0x006C,

													0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0017, 0x001C, 0x001D, 0x001E, 0x001F,
													0x0024, 0x0027, 0x0028, 0x002B, 0x002C, 0x0033, 0x0034, 0x0035, 0x0037, 0x0038,
													0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005A, 0x005B,
													0x0064, 0x0065, 0x0066, 0x0067, 0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D,
													0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00D2, 0x00D3, 0x00D4, 0x00D5,
													0x00D6, 0x00D7,	0x00DA, 0x00DB, 
													
													0x004A, 0x004B, 0x004C, 0x004D, 0x0052, 0x0053, 0x0054, 0x0055, 0x005A, 0x005B,
													0x0064, 0x0065, 0x006C, 0x006D, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077													


												};

__declspec(align(16))
const unsigned short Pics::TIFF::black_vals[] = {
													3, 2,
													1, 4,
													6, 5,
													7,
													9, 8,
													10, 11, 12,
													13, 14,
													15,
													18, 64, 16, 17, 0,
													1792, 1856, 1920, 24, 25, 23, 22, 19, 20, 21,

													1984, 2048, 2112, 2176, 2240, 2304, 2368, 2432, 2496, 2560,
													52, 55, 56, 59, 60, 320, 384, 448, 53, 54,
													50, 51, 44, 45, 46, 47, 57, 58, 61, 256,
													48, 49, 62, 63, 30, 31, 32, 33, 40, 41,
													128, 192, 26, 27, 28, 29, 34, 35, 36, 37,
													38, 39, 42, 43,

													640, 704, 768, 832, 1280, 1344, 1408, 1472, 1536, 1600,
													1664, 1728, 512, 576, 896, 960, 1024, 1088, 1152, 1216

												};


__declspec(align(16))
const unsigned char Pics::TIFF::counts_2D[] = {1, 0, 3, 1, 0, 2, 2, 0, 0, 1, 0, 1, -1};

__declspec(align(16))
const unsigned short Pics::TIFF::codes_2D[] = {
									1,
									1, 2, 3,
									1,
									2, 3,
									2, 3,
									
									15,
									15
								};





enum TIFF_Tags : unsigned short {Photometrics = 262, Compression = 259, IHeight = 257, IWidth = 256, Resolution = 296, XResolution = 282, YResolution = 283, RowsPerStrip = 278, StripOffsets = 273, StripByteCounts = 279,
								BitsPerSample = 258,
								ColorMap = 320,
								SamplesPerPixel = 277,
								FillOrder = 266,
								NewSubfileType = 254,
								SubfileType = 255,
								Orientation = 274,
								MinSampleValue = 280,
								MaxSampleValue = 281,
								ExtraSamples = 338,
								CellWidth = 264,
								CellHeoght = 265,
								DiffPredictor = 317,
								YCbCr_coffs = 529,
								PlanarCfg = 284,
								T4Options = 292,
								T6Options = 293,

								PageNumber = 297,

								TileWidth = 322,
								TileHeight = 323,
								TileOffsets = 324,
								TileByteCounts = 325,


								DNGVersion = 50706,

								LinearizationTable = 50712,
								BlackRepeat = 51713,
								BlackLevel = 50714,
								BlackDeltaH = 50715,
								BlackDeltaV = 50716,
								WhiteLevel = 50717,

								CFAPlaneColor = 50710,
								CFALayout = 50711,
								
								ColorMatrix1 = 50721,
								ColorMatrix2 = 50722,
								CalibrationMatrix1 = 50723,
								CalibrationMatrix2 = 50724,
								CalibrationIID1 = 50778,
								CalibrationIID2 = 50779,
								ForwardMatrix1 = 50964,
								ForwardMatrix2 = 50965,
								AnalogBalance = 50727,
								AsShotNeutral = 50728,
								AsShotNeutralXY = 50729,

								HueSatMapDims = 50937,
								HueSatMapData1 = 50938,
								HueSatMapData2 = 50939,

								CFAPatternRepeat = 33421,
								CFAPattern = 33422,

								SubIFD = 330,
								

								JPEGTables = 347, // motherfuckers


								JPEG_LLP = 517,
								JPEG_QTT = 519,
								JPEG_DCT = 520,
								JPEG_ACT = 521



							};




Pics::TIFF::TIFF() : byte_order(0), ltab_count(0) {
	System::MemoryFill(&_header, sizeof(Header), 0);

	_header.interleaved = 1;
	_header.fill_order = 1;
	_header.data_align = 4;
	_header.precision = 1;
	_header.rows_per_strip = 1;

	_header.sbc_type = -1;
	_header.so_type = -1;

	_header.lzw_predictor = 1;

}

Pics::TIFF::~TIFF() {
	if (li_table) li_table.Destroy();
	if (hsv_map) hsv_map.Destroy();
}

void Pics::TIFF::LoadMatrix(float * m_ptr, const unsigned char * src_ptr, const unsigned short * img_ptr) {
	unsigned int val_off;
	unsigned short sval;

	Calc::FloatClear16(m_ptr, 1);
	m_ptr[0] = m_ptr[5] = m_ptr[10] = m_ptr[15] = 1.0f;

	val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
	sval = img_ptr[2]/3;
	if (sval > 4) sval = 4;

	for (unsigned short i(0);i<sval;i++) {
		m_ptr[i] = reinterpret_cast<const int *>(src_ptr+val_off)[i*6];
		m_ptr[i] /= reinterpret_cast<const int *>(src_ptr+val_off)[i*6+1];
					
		m_ptr[i+4] = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+2];
		m_ptr[i+4] /= reinterpret_cast<const int *>(src_ptr+val_off)[i*6+3];

		m_ptr[i+8] = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+4];
		m_ptr[i+8] /= reinterpret_cast<const int *>(src_ptr+val_off)[i*6+5];

	}
}

UI_64 Pics::TIFF::LoadHeader(const unsigned char * src_ptr, UI_64 skipdi) {
	const unsigned short * img_ptr = reinterpret_cast<const unsigned short *>(src_ptr + skipdi);
	unsigned short fid_count = img_ptr[0], sval(0);
	unsigned int val_off(0), ival(0);
	img_ptr += 1;

	for (unsigned short ie(0);ie<fid_count;ie++) {
		switch (img_ptr[0]) {
			case SubfileType:
				_header.f_subtype = img_ptr[4];
			break;
			case NewSubfileType:
				_header.f_subtype = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

			break;
			case SubIFD:
				_header.sub_ifd_count = reinterpret_cast<const unsigned int *>(img_ptr+2)[0];

				if (_header.sub_ifd_count > 1) {
					_header.sub_ifd_offset = reinterpret_cast<const unsigned int *>(img_ptr+2)[1];
				} else {
					_header.sub_ifd_offset = reinterpret_cast<UI_64>(img_ptr+4) - reinterpret_cast<UI_64>(src_ptr);
				}
			break;


			case IWidth:
				if (img_ptr[1] == 3) {
					_header.width = img_ptr[4];
				} else {
					_header.width = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				}
				_header.c_width = (_header.width + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
			break;
			case IHeight:
				if (img_ptr[1] == 3) {
					_header.height = img_ptr[4];
				} else {
					_header.height = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				}
				_header.c_height = (_header.height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
			//	_header.c_height <<= 3;
			break;
			case Compression:
				if (img_ptr[1] == 3) {
					_header.compression = img_ptr[4];
				} else {
					// error
				}
			break;
			case BitsPerSample:
				if (img_ptr[1] == 3) {
					if (img_ptr[2] < 3) {
						for (unsigned short i(0);i<img_ptr[2];i++) {
							_header.bit_count[i] = img_ptr[4-i];
							if (_header.bit_count[i] > 8) _header.precision = 2;
						}

					} else {
						const unsigned short * bcounts = reinterpret_cast<const unsigned short *>(src_ptr + reinterpret_cast<const unsigned int *>(img_ptr+4)[0]);
						for (unsigned short i(0);i<img_ptr[2];i++) {
							_header.bit_count[i] = bcounts[i];
							if (_header.bit_count[i] > 8) _header.precision = 2;
						}


					}
				} else {
					// error
				}
			break;
			case ColorMap:
				if (img_ptr[1] == 3) {
					unsigned short col_count = img_ptr[2]/3;
					const unsigned short * color_map = reinterpret_cast<const unsigned short *>(src_ptr + reinterpret_cast<const unsigned int *>(img_ptr+4)[0]);

					for (unsigned short i(0);i<col_count;i++) {
						palette[i] = 0xFF000000 | ((unsigned int)color_map[i] >> 8) | ((unsigned int)color_map[i+col_count] & 0xFF00) | (((unsigned int)(color_map[i+2*col_count] & 0xFF00)) << 8);
					}

				} else {
					// error
				}
			break;

			case TileWidth:
				_header.tiled |= 1;
				if (img_ptr[1] == 3) {
					_header.t_width = img_ptr[4];
				} else {
					_header.t_width = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				}
			break;
			case TileHeight:
				_header.tiled |= 1;
				if (img_ptr[1] == 3) {
					_header.t_height = img_ptr[4];
				} else {
					_header.t_height = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				}
			break;							

			case TileOffsets:
				_header.tiled |= 1;
	
			case StripOffsets:
				_header.strip_count = img_ptr[2];
				_header.so_type = img_ptr[1];
				_header.strip_offsets = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				if (img_ptr[1] == 3) {
					if (img_ptr[2] < 3) {
						_header.so_type = 0x80;
						for (unsigned short i(0);i<img_ptr[2];i++) {
							strip_2[i] = img_ptr[4-i];
						}
					}

				} else {
					if (img_ptr[2] == 1) {
						_header.so_type = 0x80;
						strip_2[0] = _header.strip_offsets;
					}
				}				
			break;
			case RowsPerStrip:
				if (img_ptr[1] == 3) {					
					_header.rows_per_strip = img_ptr[4];
				} else {
					_header.rows_per_strip = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				}				
			break;
			case TileByteCounts:
				_header.tiled |= 1;
			case StripByteCounts:
				_header.sbc_type = img_ptr[1];
				_header.strip_byte_count = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				if (img_ptr[1] == 3) {
					if (img_ptr[2] < 3) {
						_header.sbc_type = 0x80;
						for (unsigned short i(0);i<img_ptr[2];i++) {
							strip_2[2+i] = img_ptr[4-i];
						}
					}
				} else {
					if (img_ptr[2] == 1) {
						_header.sbc_type = 0x80;
						strip_2[2] = _header.strip_byte_count;
					}
				}
			break;


			case Photometrics:
				if (img_ptr[1] == 3) {
					_header.color_type = img_ptr[4];
				} else {
					// error
				}
			break;

			case PlanarCfg: // 1 - component interleave, 2 - strip interleave
				if (img_ptr[1] == 3) {
					_header.interleaved = img_ptr[4];
				} else {
					// error
				}

			break;
			case SamplesPerPixel:
				if (img_ptr[1] == 3) {
					_header.sample_count = img_ptr[4];
				} else {
					// error
				}

			break;
			case FillOrder:
				if (img_ptr[1] == 3) { // 1 - left-to-right
					_header.fill_order = img_ptr[4];
				} else {
					// error
				}

			break;
			case ExtraSamples:
				if (img_ptr[1] == 3) {
					_header.alpha_on = 0xFF000000*(img_ptr[4] == 1);
				} else {
					// error
				}
			break;				
			case T4Options: case T6Options:
				if (img_ptr[1] == 4) { // 0: 2D / 1D; 1: uncompressed allowed; 2: fill bits befor eol
					_header.ccitt_options = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.ccitt_options &= 0xFFFF;
				} else {
					// error
				}

			break;
			case DiffPredictor:
				if (img_ptr[1] == 3) {
					_header.lzw_predictor = img_ptr[4];
				} else {
					// error
				}

			break;

			case PageNumber:
				if (img_ptr[1] == 3) {
					_header.current_page_id = img_ptr[4];
				} else {
					// error
				}
			break;


			case DNGVersion:
				_header.dng_flag = 1;
			break;


			case CFAPlaneColor:
				
			break;
			case CFALayout:
				
			break;
			case CFAPattern:
				if ((_header.cfa_cfg[0] + _header.cfa_cfg[1]) <= 4) {
					for (unsigned char i(0);i<_header.cfa_cfg[1];i++) {
						for (unsigned char j(0);j<_header.cfa_cfg[0];j++) {
							_header.cfa_cfg[i*_header.cfa_cfg[0] + j + 2] = reinterpret_cast<const unsigned char *>(img_ptr+4)[i*_header.cfa_cfg[0] + j];
						}
					}
				} else {


				}
				
			break;
			case CFAPatternRepeat:
				_header.cfa_cfg[0] = img_ptr[4] & 0xFF;
				_header.cfa_cfg[1] = img_ptr[5] & 0xFF;
			break;


			case HueSatMapDims:
				val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				_header.hsv_cfg[0] = reinterpret_cast<const int *>(src_ptr+val_off)[0]; // hue count
				_header.hsv_cfg[1] = reinterpret_cast<const int *>(src_ptr+val_off)[1]; // sat count
				_header.hsv_cfg[2] = reinterpret_cast<const int *>(src_ptr+val_off)[2]; // val count
				_header.hsv_cfg[3] = 0;

				hsv_map.Resize((_header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2])*2*4*sizeof(float));
			break;
			case HueSatMapData1:
				_header.hsv_cfg[3] |= 1;

				ival = _header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2];
				val_off = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];

				if (float * hsv_ptr = reinterpret_cast<float *>(hsv_map.Acquire())) {
					System::MemoryFill_SSE3(hsv_ptr, ival*4*2*sizeof(float), 0, 0);

					for (unsigned int i(0);i<ival;i++) {
						hsv_ptr[0] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3];
						hsv_ptr[1] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3+1];
						hsv_ptr[2] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3+2];

						hsv_ptr += 4;
					}


					hsv_map.Release();
				}

			break;
			case HueSatMapData2:
				_header.hsv_cfg[3] |= 2;

				ival = _header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2];
				val_off = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];

				if (float * hsv_ptr = reinterpret_cast<float *>(hsv_map.Acquire())) {
					hsv_ptr += (ival*4);

					for (unsigned int i(0);i<ival;i++) {
						hsv_ptr[0] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3];
						hsv_ptr[1] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3+1];
						hsv_ptr[2] = reinterpret_cast<const float *>(src_ptr + val_off)[i*3+2];

						hsv_ptr += 4;
					}
					hsv_map.Release();
				}

			break;


			case BlackLevel:
				_header.scale_flag = 1;
				_header.level_offset[0] = _header.level_offset[1] = _header.level_offset[2] = _header.level_offset[3] = 0;

				sval = img_ptr[2];
				if (sval == 1) {
					if (img_ptr[1] == 3) {
						_header.level_offset[0] = img_ptr[4];
					} else {
						if (img_ptr[1] == 4) {
							_header.level_offset[0] = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
						}
					}
								
				} else {
					if (sval > 4) sval = 4;
					val_off = reinterpret_cast<const unsigned int *>(img_ptr +4)[0];

					if (img_ptr[1] == 3) {
						for (unsigned short i(0);i<sval;i++) {
							_header.level_offset[i] = reinterpret_cast<const unsigned short *>(src_ptr + val_off)[i];
						}
						
					} else {
						if (img_ptr[1] == 4) {
							for (unsigned short i(0);i<sval;i++) {
								_header.level_offset[i] = reinterpret_cast<const unsigned int *>(src_ptr + val_off)[i];
							}
						}
					}

				}
			break;
			case BlackDeltaH:
//				_header.scale_flag = 1;
			break;
			case BlackDeltaV:
//				_header.scale_flag = 1;
			break;
			case BlackRepeat:
//				_header.scale_flag = 1;
			break;
			case WhiteLevel:
				_header.scale_flag = 1;

				sval = img_ptr[2];
				if (sval == 1) {
					if (img_ptr[1] == 3) {
						_header.level_scale[0] = img_ptr[4];
					} else {
						if (img_ptr[1] == 4) {
							_header.level_scale[0] = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
						}
					}					
					
				} else {
					if (sval > 4) sval = 4;
					val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

					if (img_ptr[1] == 3) {
						for (unsigned short i(0);i<sval;i++) {
							_header.level_scale[i] = reinterpret_cast<const unsigned short *>(src_ptr + val_off)[i];
						}
						
					} else {
						if (img_ptr[1] == 4) {
							for (unsigned short i(0);i<sval;i++) {
								_header.level_scale[i] = reinterpret_cast<const unsigned int *>(src_ptr + val_off)[i];
							}
						}
					}
				}
			

			break;

			case LinearizationTable:
				val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				ltab_count = reinterpret_cast<const unsigned int *>(img_ptr+2)[0];
				
				if (li_table.Resize(ltab_count*sizeof(unsigned short)) != -1) {
					if (unsigned short * lit_ptr = reinterpret_cast<unsigned short *>(li_table.Acquire())) {
						for (unsigned int i(0);i<ltab_count;i++) {
							lit_ptr[i] = reinterpret_cast<const unsigned short *>(src_ptr+val_off)[i];
						}
						li_table.Release();
					}
				}

			break;

			case AsShotNeutral:
				_header.transform_flag |= 16;

				val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				_header.white_balance[0] = _header.white_balance[1] = _header.white_balance[2] = 0.0f;

				for (unsigned short i(0);i<img_ptr[2];i++) {
					_header.white_balance[i] = reinterpret_cast<const int *>(src_ptr+val_off)[i*2];
					_header.white_balance[i] /= reinterpret_cast<const int *>(src_ptr+val_off)[i*2+1];
				}

				_header.white_balance[3] = 1.0f;

			break;
			case AsShotNeutralXY:
				_header.transform_flag |= 16;

				val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				
				_header.white_balance[0] = reinterpret_cast<const int *>(src_ptr+val_off)[0];
				_header.white_balance[0] /= reinterpret_cast<const int *>(src_ptr+val_off)[1];
				_header.white_balance[1] = reinterpret_cast<const int *>(src_ptr+val_off)[2];
				_header.white_balance[1] /= reinterpret_cast<const int *>(src_ptr+val_off)[3];

				_header.white_balance[2] = 1.0f;
				_header.white_balance[3] = 0.0f;

			break;

			case AnalogBalance:
				_header.transform_flag |= 1;

				val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];

				_header.analog_balance[0] = _header.analog_balance[1] = _header.analog_balance[2] = _header.analog_balance[3] = 1.0f;

				for (unsigned short i(0);i<img_ptr[2];i++) {
					_header.analog_balance[i] = reinterpret_cast<const int *>(src_ptr+val_off)[i*2];
					_header.analog_balance[i] /= reinterpret_cast<const int *>(src_ptr+val_off)[i*2+1];
				}

			break;
			case CalibrationIID1:
				if (img_ptr[1] == 3) {
					_header.transform_type[0] = img_ptr[4];
					_header.transform_type[0] &= 0x00FF;
				} else {
					// error
				}

			break;
			case CalibrationIID2:
				if (img_ptr[1] == 3) {
					_header.transform_type[1] = img_ptr[4];
					_header.transform_type[1] &= 0x00FF;
				} else {
					// error
				}

			break;
			case ColorMatrix1:
				_header.transform_flag |= 2;

				LoadMatrix(_header.color_transform[0], src_ptr, img_ptr);
			break;
			case ColorMatrix2:
				_header.transform_flag |= 32;

				LoadMatrix(_header.color_transform[1], src_ptr, img_ptr);
			break;
			case CalibrationMatrix1:
				_header.transform_flag |= 4;

				LoadMatrix(_header.calibration_transform[0], src_ptr, img_ptr);
			break;
			case CalibrationMatrix2:
				_header.transform_flag |= 64;

				LoadMatrix(_header.calibration_transform[1], src_ptr, img_ptr);
			break;
			case ForwardMatrix1:
				_header.transform_flag |= 8;

				LoadMatrix(_header.chromatic_adaptation[0], src_ptr, img_ptr);
			break;
			case ForwardMatrix2:
				_header.transform_flag |= 128;

				LoadMatrix(_header.chromatic_adaptation[1], src_ptr, img_ptr);
			break;


			case JPEGTables:
			case JPEG_QTT:
			case JPEG_DCT:
			case JPEG_ACT:
				JpegGetTables(src_ptr, reinterpret_cast<const unsigned int *>(img_ptr+2)[1], reinterpret_cast<const unsigned int *>(img_ptr+2)[0]);
			break;
		}

		img_ptr += 6;
	}

	return reinterpret_cast<const unsigned int *>(img_ptr)[0];
}

void Pics::TIFF::LoadMatrix2(float * m_ptr, const unsigned char * src_ptr, const unsigned short * img_ptr) {
	unsigned int val_off(0), ival(0);
	unsigned short sval;

	Calc::FloatClear16(m_ptr, 1);
	m_ptr[0] = m_ptr[5] = m_ptr[10] = m_ptr[15] = 1.0f;

	val_off = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
	System::BSWAP_32(val_off);
				
	sval = img_ptr[3];
	System::BSWAP_16(sval);
	sval /= 3;

	if (sval > 4) sval = 4;
	for (unsigned short i(0);i<sval;i++) {
		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6];
		System::BSWAP_32(ival);
		m_ptr[i] = (int)ival;
		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+1];
		System::BSWAP_32(ival);
		m_ptr[i] /= (int)ival;
					
		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+2];
		System::BSWAP_32(ival);
		m_ptr[i+4] = (int)ival;
		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+3];
		System::BSWAP_32(ival);
		m_ptr[i+4] /= (int)ival;

		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+4];
		System::BSWAP_32(ival);
		m_ptr[i+8] = (int)ival;
		ival = reinterpret_cast<const int *>(src_ptr+val_off)[i*6+5];
		System::BSWAP_32(ival);
		m_ptr[i+8] /= (int)ival;

	}
}

UI_64 Pics::TIFF::LoadHeader_2(const unsigned char * src_ptr, UI_64 skipdi) {
	const unsigned short * img_ptr = reinterpret_cast<const unsigned short *>(src_ptr + skipdi);
	unsigned short fid_count = img_ptr[0], sval(0);
	unsigned int ival(0), ival2(0);

	System::BSWAP_16(fid_count);

	img_ptr += 1;

	for (unsigned short ie(0);ie<fid_count;ie++) {
		sval = img_ptr[0];
		switch (System::BSWAP_16(sval)) {
			case SubfileType:
				sval = img_ptr[4];
				_header.f_subtype = System::BSWAP_16(sval);
			break;
			case NewSubfileType:
				_header.f_subtype = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(_header.f_subtype);
			break;
			case SubIFD:
				_header.sub_ifd_count = reinterpret_cast<const unsigned int *>(img_ptr+2)[0];
				System::BSWAP_32(_header.sub_ifd_count);

				if (_header.sub_ifd_count > 1) {
					_header.sub_ifd_offset = reinterpret_cast<const unsigned int *>(img_ptr+2)[1];
					System::BSWAP_32(_header.sub_ifd_offset);

				} else {
					_header.sub_ifd_offset = reinterpret_cast<UI_64>(img_ptr+4) - reinterpret_cast<UI_64>(src_ptr);
				}				
				
			break;


			case IWidth:
				if (img_ptr[1] == 0x0300) {
					sval = img_ptr[4];
					_header.width = System::BSWAP_16(sval);
				} else {
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.width = System::BSWAP_32(ival);
				}

				_header.c_width = (_header.width + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
			break;
			case IHeight:
				if (img_ptr[1] == 0x0300) {
					sval = img_ptr[4];
					_header.height = System::BSWAP_16(sval);
				} else {
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.height = System::BSWAP_32(ival);
				}

				_header.c_height = (_header.height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
				
			break;
			case Compression:
				if (img_ptr[1] == 0x0300) {
					sval = img_ptr[4];
					_header.compression = System::BSWAP_16(sval);
				} else {
					// error
				}
			break;
			case BitsPerSample:
				if (img_ptr[1] == 0x0300) {
					if (img_ptr[3] < 0x0300) {
						for (unsigned short i(0);i<img_ptr[3];i+=0x0100) {
							sval = img_ptr[4-(i>>8)];
							_header.bit_count[i >> 8] = System::BSWAP_16(sval);

							if (_header.bit_count[i >> 8] > 8) _header.precision = 2;
						}

					} else {
						ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
						const unsigned short * bcounts = reinterpret_cast<const unsigned short *>(src_ptr + System::BSWAP_32(ival));
						for (unsigned short i(0);i<img_ptr[3];i+=0x0100) {
							sval = bcounts[i>>8];
							_header.bit_count[i>>8] = System::BSWAP_16(sval);

							if (_header.bit_count[i >> 8] > 8) _header.precision = 2;
						}

					}
				} else {
					// error
				}
			break;
			case ColorMap:
				if (img_ptr[1] == 0x0300) {
					unsigned short col_count = img_ptr[3];
					System::BSWAP_16(col_count);
					col_count /= 3;
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					const unsigned short * color_map = reinterpret_cast<const unsigned short *>(src_ptr + System::BSWAP_32(ival));

					for (unsigned short i(0);i<col_count;i++) {
						palette[i] = 0xFF000000 | (color_map[i] & 0x00FF) | ((color_map[i+col_count] & 0x00FF) << 8) | (((unsigned int)(color_map[i+2*col_count] & 0x00FF)) << 16);
					}

				} else {
					// error
				}
			break;

			case TileWidth:
				_header.tiled |= 1;
				if (img_ptr[1] == 0x0300) {
					sval = img_ptr[4];
					_header.t_width = System::BSWAP_16(sval);
				} else {
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.t_width = System::BSWAP_32(ival);
				}
			break;
			case TileHeight:
				_header.tiled |= 1;
				if (img_ptr[1] == 0x0300) {
					sval = img_ptr[4];
					_header.t_height = System::BSWAP_16(sval);
				} else {
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.t_height = System::BSWAP_32(ival);
				}
			break;							

			case TileOffsets:
				_header.tiled |= 1;

			case StripOffsets:
				_header.strip_count = img_ptr[3];
				System::BSWAP_16(_header.strip_count);
				_header.so_type = (img_ptr[1] >> 8);
				
				ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];				
				
				if (img_ptr[1] == 0x0300) {
					if (img_ptr[3] < 0x0300) {
						_header.so_type = 0x80;
						for (unsigned short i(0);i<img_ptr[3];i+=0x0100) {
							strip_2[i>>8] = img_ptr[4-(i>>8)];
						}
					}

				} else {
					if (img_ptr[3] == 0x0100) {
						_header.so_type = 0x80;
						strip_2[0] = ival;
					}
				}

				_header.strip_offsets = System::BSWAP_32(ival);
			break;

			case RowsPerStrip:
				if (img_ptr[1] == 0x0300) {					
					sval = img_ptr[4];					
					_header.rows_per_strip = System::BSWAP_16(sval);
				} else {
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.rows_per_strip = System::BSWAP_32(ival);
				}				
			break;

			case TileByteCounts:
				_header.tiled |= 1;

			case StripByteCounts:
				_header.sbc_type = (img_ptr[1] >> 8);

				ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				
				if (img_ptr[1] == 0x0300) {
					if (img_ptr[3] < 0x0300) {
						_header.sbc_type = 0x80;
						for (unsigned short i(0);i<img_ptr[3];i+=0x0100) {
							strip_2[2+(i>>8)] = img_ptr[4-(i>>8)];
						}
					}
				} else {
					if (img_ptr[3] == 0x0100) {
						_header.sbc_type = 0x80;
						strip_2[2] = ival;
					}
				}

				_header.strip_byte_count = System::BSWAP_32(ival);
			break;
			case Photometrics:
				if (img_ptr[1] == 0x0300) {
					_header.color_type = img_ptr[4];
					System::BSWAP_16(_header.color_type);
				} else {
					// error
				}
			break;

			case PlanarCfg: // 1 - component interleave, 2 - strip interleave
				if (img_ptr[1] == 0x0300) {
					_header.interleaved = img_ptr[4];
					System::BSWAP_16(_header.interleaved);
				} else {
					// error
				}

			break;
			case SamplesPerPixel:
				if (img_ptr[1] == 0x0300) {
					_header.sample_count = img_ptr[4];
					System::BSWAP_16(_header.sample_count);
				} else {
					// error
				}

			break;
			case FillOrder:
				if (img_ptr[1] == 0x0300) { // 1 - left-to-right
					_header.fill_order = img_ptr[4];
					System::BSWAP_16(_header.fill_order);
				} else {
					// error
				}

			break;
			case ExtraSamples:
				if (img_ptr[1] == 0x0300) {
					_header.alpha_on = 0xFF000000*(img_ptr[4] == 0x0100);
				} else {
					// error
				}
			break;			
			case T4Options: case T6Options:
				if (img_ptr[1] == 0x0400) { // 0: 2D / 1D; 1: uncompressed allowed; 2: fill bits befor eol
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					_header.ccitt_options = System::BSWAP_32(ival);
					_header.ccitt_options &= 0xFFFF;
				} else {
					// error
				}

			break;
			case DiffPredictor:
				if (img_ptr[1] == 0x0300) {
					_header.lzw_predictor = img_ptr[4];
					System::BSWAP_16(_header.lzw_predictor);
				} else {
					// error
				}

			break;

			case PageNumber:
				if (img_ptr[1] == 0x0300) {
					_header.current_page_id = img_ptr[4];
					System::BSWAP_32(_header.current_page_id);
				} else {
					// error
				}
			break;


			case DNGVersion:
				_header.dng_flag = 1;
			break;


			case CFAPlaneColor:
				
			break;
			case CFALayout:
				
			break;
			case CFAPattern:
				if ((_header.cfa_cfg[0] + _header.cfa_cfg[1]) <= 4) {
					for (unsigned char i(0);i<_header.cfa_cfg[1];i++) {
						for (unsigned char j(0);j<_header.cfa_cfg[0];j++) {
							_header.cfa_cfg[i*_header.cfa_cfg[0] + j + 2] = reinterpret_cast<const unsigned char *>(img_ptr+4)[i*_header.cfa_cfg[0] + j];
						}
					}
				} else {


				}
				
			break;
			case CFAPatternRepeat:
				sval = img_ptr[4];				
				System::BSWAP_16(sval);
				_header.cfa_cfg[0] = sval & 0xFF;

				sval = img_ptr[5];
				System::BSWAP_16(sval);
				_header.cfa_cfg[1] = sval & 0xFF;
				
			break;



			case HueSatMapDims:
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(ival2);

				_header.hsv_cfg[0] = reinterpret_cast<const int *>(src_ptr+ival2)[0];
				System::BSWAP_32(_header.hsv_cfg[0]);
				_header.hsv_cfg[1] = reinterpret_cast<const int *>(src_ptr+ival2)[1];
				System::BSWAP_32(_header.hsv_cfg[1]);
				_header.hsv_cfg[2] = reinterpret_cast<const int *>(src_ptr+ival2)[2];
				System::BSWAP_32(_header.hsv_cfg[2]);

				_header.hsv_cfg[3] = 0;

				hsv_map.Resize((_header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2])*2*4*sizeof(float));
			break;
			case HueSatMapData1:
				_header.hsv_cfg[3] |= 1;

				ival = _header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2];
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
				System::BSWAP_32(ival2);


				if (float * hsv_ptr = reinterpret_cast<float *>(hsv_map.Acquire())) {
					System::MemoryFill_SSE3(hsv_ptr, ival*4*2*sizeof(float), 0, 0);

					for (unsigned int i(0);i<ival;i++) {
						hsv_ptr[0] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3];
						hsv_ptr[1] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3+1];
						hsv_ptr[2] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3+2];

						hsv_ptr += 4;
					}

					hsv_map.Release();
				}

			break;
			case HueSatMapData2:
				_header.hsv_cfg[3] |= 2;

				ival = _header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2];
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
				System::BSWAP_32(ival2);


				if (float * hsv_ptr = reinterpret_cast<float *>(hsv_map.Acquire())) {
					hsv_ptr += ival2*4;
					for (unsigned int i(0);i<ival;i++) {
						hsv_ptr[0] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3];
						hsv_ptr[1] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3+1];
						hsv_ptr[2] = reinterpret_cast<const float *>(src_ptr + ival2)[i*3+2];

						hsv_ptr += 4;
					}
					hsv_map.Release();
				}

			break;


			case BlackLevel:
				_header.scale_flag = 1;
				_header.level_offset[0] = _header.level_offset[1] = _header.level_offset[2] = _header.level_offset[3] = 0;

				sval = img_ptr[3];
				System::BSWAP_16(sval);

				if (sval == 1) {
					if (img_ptr[1] == 0x0300) {
						_header.level_offset[0] = img_ptr[4];
						_header.level_offset[0] <<= 16;
					} else {
						if (img_ptr[1] == 0x0400) {
							_header.level_offset[0] = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
						}
					}
					
					System::BSWAP_32((unsigned int &)_header.level_offset[0]);
					
				} else {
					if (sval > 4) sval = 4;
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					System::BSWAP_32(ival);

					if (img_ptr[1] == 0x0300) {
						for (unsigned short i(0);i<sval;i++) {
							_header.level_offset[i] = reinterpret_cast<const unsigned short *>(src_ptr + ival)[i];
							_header.level_offset[i] <<= 16;
						}
						
					} else {
						if (img_ptr[1] == 0x0400) {
							for (unsigned short i(0);i<sval;i++) {
								_header.level_offset[i] = reinterpret_cast<const unsigned int *>(src_ptr + ival)[i];
							}
						}
					}

					for (unsigned short i(0);i<sval;i++) {
						System::BSWAP_32(_header.level_offset[i]);
					}

				}
			break;
			case BlackDeltaH:
//				_header.scale_flag = 1;
			break;
			case BlackDeltaV:
//				_header.scale_flag = 1;
			break;
			case BlackRepeat:
//				_header.scale_flag = 1;
			break;
			case WhiteLevel:
				_header.scale_flag = 1;

				sval = img_ptr[3];
				System::BSWAP_16(sval);

				if (sval == 1) {
					if (img_ptr[1] == 0x0300) {
						_header.level_scale[0] = img_ptr[4];
						_header.level_scale[0] <<= 16;
					} else {
						if (img_ptr[1] == 0x0400) {
							_header.level_scale[0] = reinterpret_cast<const unsigned int *>(img_ptr + 4)[0];
						}
					}

					System::BSWAP_32((unsigned int &)_header.level_scale[0]);

					
				} else {
					if (sval > 4) sval = 4;
					ival = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
					System::BSWAP_32(ival);

					if (img_ptr[1] == 0x0300) {
						for (unsigned short i(0);i<sval;i++) {
							_header.level_scale[i] = reinterpret_cast<const unsigned short *>(src_ptr + ival)[i];
							_header.level_scale[i] <<= 16;
						}
						
					} else {
						if (img_ptr[1] == 0x0400) {
							for (unsigned short i(0);i<sval;i++) {
								_header.level_scale[i] = reinterpret_cast<const unsigned int *>(src_ptr + ival)[i];
								System::BSWAP_32(_header.level_scale[i]);
							}
						}
					}
				}
			break;

			case LinearizationTable:
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(ival2);

				ltab_count = reinterpret_cast<const unsigned int *>(img_ptr+2)[0];
				System::BSWAP_32(ltab_count);

				if (li_table.Resize(ltab_count*sizeof(unsigned short)) != -1) {
					if (unsigned short * lit_ptr = reinterpret_cast<unsigned short *>(li_table.Acquire())) {
						for (unsigned int i(0);i<ltab_count;i++) {
							lit_ptr[i] = reinterpret_cast<const unsigned short *>(src_ptr+ival2)[i];
							System::BSWAP_16(lit_ptr[i]);
						}

						li_table.Release();
					}
				}

			break;
			

			case AsShotNeutral:
				_header.transform_flag |= 16;

				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(ival2);

				sval = img_ptr[3];
				System::BSWAP_16(sval);

				_header.white_balance[0] = _header.white_balance[1] = _header.white_balance[2] = 0.0;

				for (unsigned short i(0);i<sval;i++) {
					ival = reinterpret_cast<const int *>(src_ptr+ival2)[2*i]; System::BSWAP_32(ival);
					_header.white_balance[i] = ival;
					ival = reinterpret_cast<const int *>(src_ptr+ival2)[2*i+1]; System::BSWAP_32(ival);
					_header.white_balance[i] /= ival;
				}

				_header.white_balance[3] = 1.0f;

			break;
			case AsShotNeutralXY:
				_header.transform_flag |= 16;

				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(ival2);

				sval = img_ptr[3];
				System::BSWAP_16(sval);
				
				ival = reinterpret_cast<const int *>(src_ptr+ival2)[0]; System::BSWAP_32(ival);
				_header.white_balance[0] = ival;
				ival = reinterpret_cast<const int *>(src_ptr+ival2)[1]; System::BSWAP_32(ival);
				_header.white_balance[0] /= ival;
				ival = reinterpret_cast<const int *>(src_ptr+ival2)[2]; System::BSWAP_32(ival);
				_header.white_balance[1] = ival;
				ival = reinterpret_cast<const int *>(src_ptr+ival2)[3]; System::BSWAP_32(ival);
				_header.white_balance[1] /= ival;

				_header.white_balance[2] = 1.0f;
				_header.white_balance[3] = 0.0f;

			break;

			case AnalogBalance:
				_header.transform_flag |= 1;
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+4)[0];
				System::BSWAP_32(ival2);

				sval = img_ptr[3];
				System::BSWAP_16(sval);

				_header.analog_balance[0] = _header.analog_balance[1] = _header.analog_balance[2] = _header.analog_balance[3] = 1.0f;

				for (unsigned short i(0);i<sval;i++) {
					ival = reinterpret_cast<const int *>(src_ptr+ival2)[2*i]; System::BSWAP_32(ival);
					_header.analog_balance[i] = ival;
					ival = reinterpret_cast<const int *>(src_ptr+ival2)[2*i+1]; System::BSWAP_32(ival);
					_header.analog_balance[i] /= ival;
				}

			break;
			case CalibrationIID1:
				if (img_ptr[1] == 0x0300) {
					_header.transform_type[0] = img_ptr[4];
					System::BSWAP_16(_header.transform_type[0]);
					_header.transform_type[0] &= 0x00FF;
				} else {
					// error
				}

			break;
			case CalibrationIID2:
				if (img_ptr[1] == 0x0300) {
					_header.transform_type[1] = img_ptr[4];
					System::BSWAP_16(_header.transform_type[1]);
					_header.transform_type[1] &= 0x00FF;
				} else {
					// error
				}

			break;
			case ColorMatrix1:
				_header.transform_flag |= 2;

				LoadMatrix2(_header.color_transform[0], src_ptr, img_ptr);
			break;
			case ColorMatrix2:
				_header.transform_flag |= 32;

				LoadMatrix2(_header.color_transform[1], src_ptr, img_ptr);
			break;
			case CalibrationMatrix1:
				_header.transform_flag |= 4;

				LoadMatrix2(_header.calibration_transform[0], src_ptr, img_ptr);
			break;
			case CalibrationMatrix2:
				_header.transform_flag |= 64;

				LoadMatrix2(_header.calibration_transform[1], src_ptr, img_ptr);
			break;
			case ForwardMatrix1:
				_header.transform_flag |= 8;

				LoadMatrix2(_header.chromatic_adaptation[0], src_ptr, img_ptr);
			break;
			case ForwardMatrix2:
				_header.transform_flag |= 128;

				LoadMatrix2(_header.chromatic_adaptation[1], src_ptr, img_ptr);
			break;


			case JPEGTables:
			case JPEG_QTT:
			case JPEG_DCT:
			case JPEG_ACT:
				ival = reinterpret_cast<const unsigned int *>(img_ptr+2)[0];
				ival2 = reinterpret_cast<const unsigned int *>(img_ptr+2)[1];
				JpegGetTables(src_ptr, System::BSWAP_32(ival2), System::BSWAP_32(ival));
			break;

		}

		img_ptr += 6;
	}

	ival = reinterpret_cast<const unsigned int *>(img_ptr)[0];
	return System::BSWAP_32(ival);
}


UI_64 Pics::TIFF::IUnPack(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pii_offset(0), pi_runner(0);
	unsigned int topp(0), stripsco(0);
	unsigned short sval(0);
	
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);

	Pack::LZW lzwtor;
	SFSco::Object temp_obj;

	for (unsigned int sc(0);sc<_header.sample_count;sc++) {
		result |= (_header.bit_count[sc] != 8);
	}

	if (result) return result;

	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	
	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
		if (_header.sample_count < 4) System::MemoryFill_SSE3(pipo, _header.c_width*_header.c_height*4, 0xFF000000FF000000, 0xFF000000FF000000);
			

		stripsco = _header.strip_count / _header.sample_count;

		for (unsigned int i(0);i<_header.strip_count;i++) {
			if (byte_order == 1) {
				if (stripo_3) topp = stripo_3[i];
				else topp = stripo_4[i];
										
				if (strip_size_3) strip_desc.size = strip_size_3[i];
				else strip_desc.size = strip_size_4[i];
			} else {
				if (stripo_3) {
					sval = stripo_3[i];
					topp = System::BSWAP_16(sval);
				} else {
					topp = stripo_4[i];
					System::BSWAP_32(topp);
				}
										
				if (strip_size_3) {
					sval = strip_size_3[i];
					strip_desc.size = System::BSWAP_16(sval);
				} else {							
					strip_desc.size = strip_size_4[i];
					System::BSWAP_32(strip_desc.size);
				}

			}
		
			strip_desc.pointer = src_ptr + topp;

			if ((topp+strip_desc.size) > fsize) {
				result = 147852369;
				break;
			}

			if (_header.tiled == 0) { 
				if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
				else {
					if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
				}
								
				strip_desc.row_width = _header.width;
		
			} else {				
				strip_desc.row_width = _header.t_width;
				strip_desc.row_count = _header.t_height;

			}

			strip_desc.row_length = (_header.c_width << 2);
					

			switch (_header.compression) {
				case 1: // no compression
					TIFF_ICopy_8(strip_desc, pipo + pi_offset + pii_offset, i/stripsco);
				break;
				case 5: // LZW
					if (pipo) {
						pi_obj.Release();
						pipo = 0;
					}

					if (lzwtor.Reset(8, 128*(_header.fill_order == 1) | 16) != -1) {
						lzwtor.SetUSource(strip_desc.pointer, strip_desc.size);
												
						pi_runner = 0;
			
						for (;;) {
							result = lzwtor.UnPack(temp_obj);
											
							if ((result > 0) && (result < -1024)) {
								if (pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
									if (strip_desc.pointer = reinterpret_cast<const unsigned char *>(temp_obj.Acquire())) {
													
										strip_desc.size = result;
													
										if (_header.lzw_predictor == 2) {
											TIFF_IPredict_8(strip_desc, pipo + pi_offset + pii_offset + pi_runner, i/stripsco);
										} else {
											TIFF_ICopy_8(strip_desc, pipo + pi_offset + pii_offset + pi_runner, i/stripsco);
										}

										pi_runner += strip_desc.size;

										temp_obj.Release();

										if (result = lzwtor.Success()) break;
									} else {
										break;
									}
									
									pi_obj.Release();
									pipo = 0;
								}								
							}									
						}

						if (result & 16) {
							result = 0; // success											
						} else {									
							// discontinue with image (corrupted data stream)
							result = 65498701;										
						}								
					}
						
				break;
				case 32773: // PackBits

				break;
				default:
					result = 564825;

			}


			if (_header.tiled == 0) {
				pi_offset += _header.rows_per_strip*_header.c_width;
			} else {
				pii_offset += _header.t_width;

				if (pii_offset == _header.c_width) {
					pii_offset = 0;
					pi_offset += _header.t_height*_header.c_width;
				}
								
			}

			if (result) break;
		
		}

		if (pipo) {
			pi_obj.Release();
			pipo = 0;
		}
	}

	return result;
}


UI_64 Pics::TIFF::XUnPack(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pii_offset(0), pi_runner(0);
	unsigned int topp(0);
	unsigned short sval(0);
	
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);

	Pack::LZW lzwtor;
	SFSco::Object temp_obj;



	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	
	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
		if ((_header.compression == 2) || (_header.compression == 3) || (_header.compression == 4)) {
			System::MemoryFill_SSE3(pipo, _header.c_width*_header.c_height*4*_header.precision, -1, -1);			

		}

		strip_desc.flags = _header.ccitt_options;

		for (unsigned int i(0);i<_header.strip_count;i++) {

			if (byte_order == 1) {
				if (stripo_3) topp = stripo_3[i];
				else topp = stripo_4[i];
										
				if (strip_size_3) strip_desc.size = strip_size_3[i];
				else strip_desc.size = strip_size_4[i];
			} else {
				if (stripo_3) {
					sval = stripo_3[i];
					topp = System::BSWAP_16(sval);
				} else {
					topp = stripo_4[i];
					System::BSWAP_32(topp);
				}
										
				if (strip_size_3) {
					sval = strip_size_3[i];
					strip_desc.size = System::BSWAP_16(sval);
				} else {							
					strip_desc.size = strip_size_4[i];
					System::BSWAP_32(strip_desc.size);
				}

			}
		
			strip_desc.pointer = src_ptr + topp;

			if ((topp+strip_desc.size) > fsize) {
				result = 147852369;
				break;
			}

			if (_header.tiled == 0) { 
				if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
				else {
					if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
				}
								
				strip_desc.row_width = _header.width;
		
			} else {				
				strip_desc.row_width = _header.t_width;
				strip_desc.row_count = _header.t_height;

			}

			strip_desc.row_length = (_header.c_width << 2);
					
			if (_header.fill_order == 1) {
				strip_desc.flags |= 0x20000000;

				switch (_header.compression) {
					case 1: // no compression
						switch (_header.bit_count[0]) {
							case 1:
								TIFF_BCopy_1X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;
							case 4:
								TIFF_BCopy_4X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;
							case 8:
								TIFF_BCopy_8X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;

						}
					break;
					case 2: // T3
						TIFF_T3UnPack(strip_desc, pipo + pi_offset + pii_offset);
					break;
					case 3: // T4
						if (_header.ccitt_options & 1) {
							strip_desc.flags |= 0x10000000;
							TIFF_T42DUnPack(strip_desc, pipo + pi_offset + pii_offset);
						} else {
							TIFF_T41DUnPack(strip_desc, pipo + pi_offset + pii_offset);
						}
					break;
					case 4: // T6
						strip_desc.flags |= 0x80000000;
						TIFF_T42DUnPack(strip_desc, pipo + pi_offset + pii_offset);
					break;
					case 5: // LZW
						if (pipo) {
							pi_obj.Release();
							pipo = 0;
						}

						if (lzwtor.Reset(8, 128*(_header.fill_order == 1) | 16) != -1) {
							lzwtor.SetUSource(strip_desc.pointer, strip_desc.size);
													
							pi_runner = 0;							
			
							for (;;) {
								result = lzwtor.UnPack(temp_obj);
											
								if ((result > 0) && (result < -1024)) {
									if (pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
										if (strip_desc.pointer = reinterpret_cast<const unsigned char *>(temp_obj.Acquire())) {
													
											strip_desc.size = result;
													
											if (_header.lzw_predictor == 2) {
												switch (_header.bit_count[0]) {
													case 1:
														TIFF_BCopy_1X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;
													case 4:
														TIFF_BPredict_4X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;
													case 8:
														TIFF_BPredict_8X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;

												}
												
											} else {
												switch (_header.bit_count[0]) {
													case 1:
														TIFF_BCopy_1X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;
													case 4:
														TIFF_BCopy_4X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;
													case 8:
														TIFF_BCopy_8X(strip_desc, pipo + pi_offset + pii_offset + pi_runner, palette);
													break;

												}
											}

											pi_runner += strip_desc.size;

											temp_obj.Release();

											if (result = lzwtor.Success()) break;
											
										} else {
											break;
										}
									
										pi_obj.Release();
										pipo = 0;
									}
								}
							}									
							

							if (result & 16) {
								result = 0; // success											
							} else {									
								// discontinue with image (corrupted data stream)
								result = 65498701;										
							}								
						}
						
					break;
					case 32773: // PackBits
						switch (_header.bit_count[0]) {
							case 1:
								TIFF_BUnPack_1X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;
							case 4:
								TIFF_BUnPack_4X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;
							case 8:
								TIFF_BUnPack_8X(strip_desc, pipo + pi_offset + pii_offset, palette);
							break;

						}
					break;
					default:
						result = 564825;

				}

			} else {
				switch (_header.compression) {
					case 2: // T3
						TIFF_T3UnPack(strip_desc, pipo + pi_offset + pii_offset);
					break;
					case 3: // T4
						if (_header.ccitt_options & 1) {
							strip_desc.flags |= 0x10000000;
							TIFF_T42DUnPack(strip_desc, pipo + pi_offset + pii_offset);
						} else {
							TIFF_T41DUnPack(strip_desc, pipo + pi_offset + pii_offset);
						}
					break;
					case 4: // T6
						strip_desc.flags |= 0x80000000;
						TIFF_T42DUnPack(strip_desc, pipo + pi_offset + pii_offset);
					break;
			//		case 5: // LZW
						
			//		break;
					default:
						result = 564825;

				}
			}

	//		strip_desc.flags |= 0x04000000;

			if (_header.tiled == 0) {
				pi_offset += _header.rows_per_strip*_header.c_width;
			} else {
				pii_offset += _header.t_width;

				if (pii_offset == _header.c_width) {
					strip_desc.flags &= 0x2BFFFFFF;
			//		strip_desc.flags |= 0x40000000;

					pii_offset = 0;
					pi_offset += _header.t_height*_header.c_width;
				}
								
			}

			if (result) break;
		
		}

		if (pipo) {
			pi_obj.Release();
			pipo = 0;
		}
	}

	return result;
}

UI_64 Pics::TIFF::CopyRGB(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pi_runner(0), pii_offset(0);
	unsigned int topp(0), lopix(0);
	unsigned short sval(0);
	unsigned char el_pixel[4];
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);
	
	Pack::LZW lzwtor;
	SFSco::Object temp_obj;
	
	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
		if ((_header.bit_count[0] == _header.bit_count[1]) && (_header.bit_count[1] == _header.bit_count[2]) && (_header.bit_count[0] == 8)) {
			for (unsigned int sci(0);sci<_header.strip_count;sci++) {
				if (byte_order == 1) {
					if (stripo_3) topp = stripo_3[sci];
					else topp = stripo_4[sci];
										
					if (strip_size_3) strip_desc.size = strip_size_3[sci];
					else strip_desc.size = strip_size_4[sci];
				} else {
					if (stripo_3) {
						sval = stripo_3[sci];
						topp = System::BSWAP_16(sval);
					} else {
						topp = stripo_4[sci];
						System::BSWAP_32(topp);
					}
										
					if (strip_size_3) {
						sval = strip_size_3[sci];
						strip_desc.size = System::BSWAP_16(sval);
					} else {							
						strip_desc.size = strip_size_4[sci];
						System::BSWAP_32(strip_desc.size);
					}

				}

				strip_desc.pointer = src_ptr + topp;

				if ((topp+strip_desc.size) > fsize) {
					result = 1478536369;
					break;
				}

				if (_header.tiled == 0) {
					if (sci != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
					else {
						if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
					}
												
					strip_desc.row_width = _header.width;
		
				} else {
					strip_desc.row_width = _header.t_width;
					strip_desc.row_count = _header.t_height;						
				}

				strip_desc.row_length = (_header.c_width << 2);
					
				switch (_header.compression) {
					case 1: // no compression
						TIFF_SampleCopy_3(strip_desc, pipo + pi_offset + pii_offset);
					break;
					case 5: // LZW
						if (pipo) {
							pi_obj.Release();
							pipo = 0;
						}

						if (lzwtor.Reset(8, 128*(_header.fill_order == 1) | 16) != -1) {
							lzwtor.SetUSource(strip_desc.pointer, strip_desc.size);

							pi_runner = 0;
							lopix = 0;
							el_pixel[0] = el_pixel[1] = el_pixel[2] = 0; el_pixel[3] = 0xFF;

							for (;;) {
								result = lzwtor.UnPack(temp_obj);
											
								if ((result > 0) && (result < -1024)) {
									if (pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
										if (strip_desc.pointer = reinterpret_cast<const unsigned char *>(temp_obj.Acquire())) {
													
											if (lopix) {
												for (unsigned int li(lopix);li<3;li++) el_pixel[li] = ((unsigned int)strip_desc.pointer[li] << (li*8));

												if (_header.lzw_predictor == 2) {
													el_pixel[0] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[0];
													el_pixel[1] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[1];
													el_pixel[2] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[2];
												}																

												pipo[pi_offset + pii_offset + pi_runner] = reinterpret_cast<unsigned int *>(el_pixel)[0];

												result -= (3-lopix);
												pi_runner++;
											}

											lopix = result % 3;
											strip_desc.size = result - lopix;
													
											if (_header.lzw_predictor == 2) TIFF_PredictSample_3(strip_desc, pipo + pi_offset + pii_offset + pi_runner);
											else TIFF_SampleCopy_3(strip_desc, pipo + pi_offset + pii_offset + pi_runner);

											pi_runner += (strip_desc.size / 3);

											if (lopix) for (unsigned int li(0);li<lopix;li++) el_pixel[li] |= ((unsigned int)strip_desc.pointer[strip_desc.size+li] << (li*8));

											temp_obj.Release();
										}

										pi_obj.Release();
										pipo = 0;
									}

									if (result = lzwtor.Success()) break;
								} else {
									break;
								}
									
							}

							if (result & 16) {
								result = 0; // success											
							} else {									
								// discontinue with image (corrupted data stream)
								result = 65498701;										
							}								
						}
					break;
					case 32773: // PackBits
				
					break;
					default:
						result = 564825;

				}

				if (_header.tiled == 0) {
					pi_offset += _header.rows_per_strip*_header.c_width;
				} else {
					pii_offset += _header.t_width;

					if (pii_offset == _header.c_width) {
						pii_offset = 0;
						pi_offset += _header.t_height*_header.c_width;
					}
				}
					
				if (result) break;
	
			}
		} else {
			result = 45682654;
		}
	
		if (pipo) {
			pi_obj.Release();
			pipo = 0;
		}		
	}

	return result;
}

UI_64 Pics::TIFF::CopyCMYK(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pii_offset(0), pi_runner(0);
	unsigned int topp(0), ival(0), lopix(0);
	unsigned short sval(0);
	unsigned char el_pixel[4];

	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);

	Pack::LZW lzwtor;
	SFSco::Object temp_obj;
	
	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
		if ((_header.bit_count[0] == _header.bit_count[1]) && (_header.bit_count[1] == _header.bit_count[2]) && (_header.bit_count[2] == _header.bit_count[3]) && (_header.bit_count[0] == 8)) {
			for (unsigned int i(0);i<_header.strip_count;i++) {
				if (byte_order == 1) {
					if (stripo_3) topp = stripo_3[i];
					else topp = stripo_4[i];
										
					if (strip_size_3) strip_desc.size = strip_size_3[i];
					else strip_desc.size = strip_size_4[i];
				} else {
					if (stripo_3) {
						sval = stripo_3[i];
						topp = System::BSWAP_16(sval);
					} else {
						topp = stripo_4[i];
						System::BSWAP_32(topp);
					}
										
					if (strip_size_3) {
						sval = strip_size_3[i];
						strip_desc.size = System::BSWAP_16(sval);
					} else {							
						strip_desc.size = strip_size_4[i];
						System::BSWAP_32(strip_desc.size);
					}

				}
										
				strip_desc.pointer = src_ptr + topp;

				if ((topp+strip_desc.size) > fsize) {
					result = 1478536369;
					break;
				}

					
				if (_header.tiled == 0) {
					if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
					else {
						if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
					}
												
					strip_desc.row_width = _header.width;

				} else {
					strip_desc.row_width = _header.t_width;
					strip_desc.row_count = _header.t_height;

				}

				strip_desc.row_length = (_header.c_width << 2);

				switch (_header.compression) {
					case 1: // no compression
						TIFF_SampleCopy_4(strip_desc, pipo + pi_offset + pii_offset);
					break;
					case 5: // LZW
						if (pipo) {
							pi_obj.Release();
							pipo = 0;
						}

						if (lzwtor.Reset(8, 128*(_header.fill_order == 1) | 16) != -1) {
							lzwtor.SetUSource(strip_desc.pointer, strip_desc.size);

							pi_runner = 0;
							lopix = 0;
							el_pixel[0] = el_pixel[1] = el_pixel[2] = el_pixel[3] = 0;

							for (;;) {										
								result = lzwtor.UnPack(temp_obj);
										
								if ((result > 0) && (result < -1024)) {
									if (pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
										if (strip_desc.pointer = reinterpret_cast<const unsigned char *>(temp_obj.Acquire())) {
												
											if (lopix) {
												for (unsigned int li(lopix);li<4;li++) el_pixel[li] = ((unsigned int)strip_desc.pointer[li] << (li*8));

												if (_header.lzw_predictor == 2) {
													el_pixel[0] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[0];
													el_pixel[1] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[1];
													el_pixel[2] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[2];
													el_pixel[3] += reinterpret_cast<unsigned char *>(pipo + pi_offset + pii_offset + pi_runner -1)[3];
												}																

												pipo[pi_offset + pii_offset + pi_runner] = reinterpret_cast<unsigned int *>(el_pixel)[0];
												
												result -= (4-lopix);
												pi_runner++;
											}

											lopix = result % 4;
											strip_desc.size = result - lopix;
													
											if (_header.lzw_predictor == 2) TIFF_PredictSample_4(strip_desc, pipo + pi_offset + pii_offset + pi_runner);
											else TIFF_SampleCopy_4(strip_desc, pipo + pi_offset + pii_offset + pi_runner);

											pi_runner += (strip_desc.size / 4);

											if (lopix) for (unsigned int li(0);li<lopix;li++) el_pixel[li] |= ((unsigned int)strip_desc.pointer[strip_desc.size+li] << (li*8));

											temp_obj.Release();
										}

										pi_obj.Release();
										pipo = 0;
									}

									if (result = lzwtor.Success()) break;
								} else {
									break;
								}
									
							}

							if (result & 16) {
								result = 0; // success											
							} else {									
								// discontinue with image (corrupted data stream)
								result = 65498701;										
							}								
						}

					break;
					case 32773: // PackBits
				
					break;
					default:
						result = 564825;

				}

				if (_header.tiled == 0) {
					pi_offset += _header.rows_per_strip*_header.c_width;
				} else {
					pii_offset += _header.t_width;

					if (pii_offset == _header.c_width) {
						pii_offset = 0;
						pi_offset += _header.t_height*_header.c_width;
					}

				}
					
				if (result) break;
		
			}
		} else {
			result = 45682654;
		}

		if (pipo) {
			pi_obj.Release();
			pipo = 0;
		}
	}

	return result;
}


UI_64 Pics::TIFF::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	__declspec(align(16)) float c_trans[16], temp_val[4];
	
	const unsigned short * img_ptr(0);
	unsigned char * blopo(0);
	
	UI_64 skipdi(0), result(0), next_fid;
	unsigned int signature[8], signon(fsize), tval(0), image_count(0), current_image(-1), c_val(0), c_step(256), jpg_off(0), jpg_size(0);
	
	StripDesc s_dsc;
	SFSco::Object pi_obj;

	imid = -1;

	if (fsize > 16384) signon = 16384;

	if (byte_order = IsTIFF(reinterpret_cast<const char *>(src_ptr), signon)) {
		tval = 2048;
		if (fsize < 2048) tval = fsize;
		SFSco::sha256(signature, reinterpret_cast<const char *>(src_ptr), tval, 1);

		imid = pi_list.FindIt(signature, 32);
		if (imid != -1) {
			return 69;			
		}


		if (byte_order == 1) {
			next_fid = reinterpret_cast<const unsigned int *>(src_ptr)[1];
		} else {
			tval = reinterpret_cast<const unsigned int *>(src_ptr)[1];
			System::BSWAP_32(tval);
			next_fid = tval;
		}

		for (;next_fid;) {
	//		_header.scale_flag = 0;
			
			if (byte_order == 1) next_fid = LoadHeader(src_ptr, next_fid);				
			else next_fid = LoadHeader_2(src_ptr, next_fid);

			if ((_header.bit_count[0]>16) || (_header.bit_count[1]>16) || (_header.bit_count[2]>16) || (_header.bit_count[3]>16)) {
				result = 3698741;
				if (next_fid) continue;
				else break;
			}

			if (_header.f_subtype & 1) {				
				if (_header.current_ifd < _header.sub_ifd_count) {
					next_fid = reinterpret_cast<const unsigned int *>(src_ptr + _header.sub_ifd_offset)[_header.current_ifd++];
					if (byte_order != 1) System::BSWAP_64(next_fid);
				}

				if (next_fid) continue; // skip low resolution
			}



			if (_header.tiled != 0) {
				_header.c_width = (_header.width + _header.t_width - 1)/_header.t_width;
				_header.c_width *= _header.t_width;

				_header.c_height = (_header.height + _header.t_height - 1)/_header.t_height;
				_header.c_height;
				_header.c_height *= _header.t_height;

			}

			if ((_header.so_type == 0xFF) || (_header.sbc_type == 0xFF)) {

				return 368412579;
			}

		
			result = pi_obj.New(image_rstr_tid, _header.c_width*_header.c_height*_header.precision*4, SFSco::large_mem_mgr);
														
			if (result != -1) {
				if (_header.interleaved == 1) {
					switch (_header.color_type) {
						case 0:							
						case 1: // black/white, greysccale (1/4/8)
							tval = (1 << _header.bit_count[0]) - 1;
							c_val = 0; c_step = 256;

							if (_header.bit_count[0] > 1) c_step >>= _header.bit_count[0];
									
							if (_header.color_type == 1) {
								for	(unsigned int i(0);i<tval;i++) {
									palette[i] = 0xFF000000 | (c_val << 16) | (c_val << 8) | c_val;
									c_val += c_step;
								}
	
								palette[tval] = 0xFFFFFFFF;
							} else {
								for (unsigned int i(tval);i>0;i--) {
									palette[i] = 0xFF000000 | (c_val << 16) | (c_val << 8) | c_val;
									c_val += c_step;
								}

								palette[0] = 0xFFFFFFFF;

							}
						case 3: // palette (4/8)
							result = XUnPack(src_ptr, pi_obj, fsize);
						break;

						case 2: // RGB
						case 5: // CMYK
						case 6: // YCbCr						
							if ((_header.compression == 6) || (_header.compression == 7) || (_header.compression == 0x884C)) {
								result = JpegDecode(src_ptr, pi_obj, fsize);
							} else {
								switch (_header.sample_count) {
									case 3: result = CopyRGB(src_ptr, pi_obj, fsize); break;
									case 4: if (_header.alpha_on) result = CopyCMYK(src_ptr, pi_obj, fsize); break;
								}
							}
						break;
						case 32803: // CFA
							if (_header.sample_count == 1)
							if (_header.compression == 1) {
								result = DeMosaic(src_ptr, pi_obj, fsize);
							} else {
								result = JpegDecode(src_ptr, pi_obj, fsize);

								if (!result) {
									DeMosaic(pi_obj);
								}
							}
														
						break;
						case 34892: // linear raw
							if (_header.sample_count == 1) {
								if (_header.compression == 1) {
									result = GrayOUT(src_ptr, pi_obj, fsize);
								} else {


								}
//								_header.sample_count = 3;
							}
						break;
						case 4: // transperancy mask (1)
			//				BUnPack_1X();
						break;
						case 8: // CIELAB
									
						break;
					}
				} else {
					if (_header.interleaved == 2)
					if ((_header.color_type == 2) && (_header.color_type == 5) && (_header.color_type == 6)) {
						result = IUnPack(src_ptr, pi_obj, fsize);
					}
				}

				if (!result) {
					Properties iprops;					

					if (ltab_count) { // remap 
						SFSco::Object pi_obj2;

						if (_header.precision == 1) pi_obj2.New(image_rstr_tid, _header.c_width*_header.c_height*8, SFSco::large_mem_mgr);
						
						if (const unsigned short * lit_ptr = reinterpret_cast<const unsigned short *>(li_table.Acquire())) {
							if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
								s_dsc.pointer = pi_ptr;
								s_dsc.row_count = _header.height;
								s_dsc.row_length = (16 - _header.bit_count[0]);
								s_dsc.row_width = _header.c_width;

								if (_header.precision == 1) {
									if (unsigned short * pi2_ptr = reinterpret_cast<unsigned short *>(pi_obj2.Acquire())) {									
										Image_Remap_1(s_dsc, lit_ptr, ltab_count-1, pi2_ptr);

										pi_obj2.Release();
									}
								} else {
									Image_Remap_2(s_dsc, lit_ptr, ltab_count-1);

								}

								pi_obj.Release();
							}
							li_table.Release();
						}

						if (_header.precision == 1) {
							_header.precision = 2;
							pi_obj.Destroy();
							pi_obj = pi_obj2;							
						}
					} else {
						if (_header.precision > 1) {
							for (unsigned short i(0);i<_header.sample_count;i++) {
								_header.level_offset[i] <<= (16 - _header.bit_count[i]);
								_header.level_scale[i] <<= (16 - _header.bit_count[i]);							
							}
						}
					}

					iprops.im_obj = pi_obj;

					if (_header.scale_flag) { // 
						for (unsigned int i(0);i<_header.sample_count;i++) {
							if (_header.precision == 1) _header.level_scale[i] = 0x000FF000/(_header.level_scale[i] - _header.level_offset[i] + 1);
							else _header.level_scale[i] = 0x0FFFF000/(_header.level_scale[i] - _header.level_offset[i] + 1);
						}

						for (unsigned int i(_header.sample_count);i<4;i++) {
							_header.level_scale[i] = _header.level_scale[i-1];
							_header.level_offset[i] = _header.level_offset[i-1];
						}

						if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
							s_dsc.pointer = pi_ptr;
							s_dsc.row_count = _header.height;
							s_dsc.row_length = 0; // _header.c_width*_header.precision*sizeof(unsigned int)
							s_dsc.row_width = _header.c_width;

							if (_header.precision == 1) Image_Normalize_1(s_dsc, _header.level_offset);
							else Image_Normalize_2(s_dsc, _header.level_offset);

							pi_obj.Release();
						}
						
					}

					for (unsigned int i(0);i<8;i++) iprops.signature[i] = signature[i];

					if (_header.transform_flag) {//  
						CalculateTransform(c_trans);
						
						if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
							s_dsc.pointer = pi_ptr;
							s_dsc.row_count = _header.height;
							s_dsc.row_length = 0;
							s_dsc.row_width = _header.c_width;

							if (_header.precision == 1) Image_Transform_1(s_dsc, c_trans);
							else Image_Transform_2(s_dsc, c_trans);

							pi_obj.Release();
						}
					}


// apply HSV
					if (_header.hsv_cfg[0]) { // 
						if (_header.hsv_cfg[3] == 3) {
							temp_val[2] = _header.temperature;										

							temp_val[0] = temperature_table[_header.transform_type[0]];
							temp_val[1] = temperature_table[_header.transform_type[1]];

							if ((temp_val[2] <= temp_val[0]) || (temp_val[2] >= temp_val[1])) {
								if (temp_val[2] <= temp_val[0]) temp_val[2] = temp_val[0];
								else temp_val[2] = temp_val[1];
							}

							temp_val[0] = 1.0f/temp_val[0];
							temp_val[1] = 1.0f/temp_val[1];
							temp_val[2] = 1.0f/temp_val[2];

							temp_val[1] -= temp_val[0];
							temp_val[2] -= temp_val[0];
							temp_val[2] /= temp_val[1];

							temp_val[3] = 0.0;

							tval = _header.hsv_cfg[0]*_header.hsv_cfg[1]*_header.hsv_cfg[2];

							if (float * hsv_ptr = reinterpret_cast<float *>(hsv_map.Acquire())) {
								Image_HSV_interpolate(hsv_ptr, tval, temp_val[2]);

								hsv_map.Release();
							}

						}


						if (const short * hsv_ptr = reinterpret_cast<const short *>(hsv_map.Acquire())) {
							if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
								s_dsc.pointer = pi_ptr;
								s_dsc.row_count = _header.height;
								s_dsc.row_length = _header.hsv_cfg[3];
								s_dsc.row_width = _header.c_width;

								if (_header.precision == 1) Image_Remap_HSV1(s_dsc, hsv_ptr, _header.hsv_cfg);
								else Image_Remap_HSV2(s_dsc, hsv_ptr, _header.hsv_cfg);

								pi_obj.Release();
							}



							hsv_map.Release();
						}
					}
					
					if (_header.dng_flag && (_header.precision>1)) {
						if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
							s_dsc.pointer = pi_ptr;
							s_dsc.row_count = _header.height;
							s_dsc.row_length = 0;
							s_dsc.row_width = _header.c_width;
							
							Image_Remap_2(s_dsc, RIMM_table, 65535);

							pi_obj.Release();
						}
					}
					

					switch (_header.color_type) {
						case 5: iprops.norgba = Image::CMYK; break;
						case 6: iprops.norgba = Image::YCbCr; break;
					}
/*
					if (_header.sample_count == 1) {
						iprops.norgba = Image::BW_special;
					}
*/
					iprops.precision = _header.precision;
						
					iprops.width = _header.width;
					iprops.height = _header.height;
					iprops.c_width = _header.c_width;
					iprops.c_height = _header.c_height;

					imid = pi_list.Add(iprops);
							
					signon = 0;
				} else {
					pi_obj.Destroy();
				}
			}
	
			break; // no multiimage suppport
		}
	}


	return result;
}

void Pics::TIFF::CalculateTransform(float * c_trans) {
	__declspec(align(16)) float ca_trans[16], temp_trans[16], temp_val[4], prev_val[4], line_vec[4];
	unsigned int mf_hdr[8];

	Calc::FloatClear16(ca_trans, 1);
	ca_trans[0] = ca_trans[5] = ca_trans[10] = ca_trans[15] = 1.0f;

	if (_header.transform_flag & 16) { // white balance
		if (_header.white_balance[3]) { // convert to XY
			_header.white_balance[3] = 0.0;

			prev_val[0] = prev_val[1] = prev_val[2] = prev_val[3] = 0.0;

			temp_val[0] = temp_val[1] = temp_val[2] = 0.5f; temp_val[3] = 0.0;


			if (_header.transform_flag & 2) // cm present
			for (unsigned int iii(0);iii<100;iii++) {

				Calc::FloatClear16(c_trans, 1);
				c_trans[0] = c_trans[5] = c_trans[10] = c_trans[15] = 1.0f;

				if (_header.transform_flag & 1) { // analog balance
					c_trans[0] /= _header.analog_balance[0];
					c_trans[5] /= _header.analog_balance[1];
					c_trans[10] /= _header.analog_balance[2];
					c_trans[15] /= _header.analog_balance[3];
				}
									
				if ((_header.transform_flag & 0x22) == 0x22) { // two matrices
					line_vec[2] = CCT(temp_val);

					line_vec[0] = temperature_table[_header.transform_type[0]];
					line_vec[1] = temperature_table[_header.transform_type[1]];

					if ((line_vec[2] <= line_vec[0]) || (line_vec[2] >= line_vec[1])) {
						if (line_vec[2] <= line_vec[0]) line_vec[2] = line_vec[0];
						else line_vec[2] = line_vec[1];
					}

					line_vec[0] = 1.0f/line_vec[0];
					line_vec[1] = 1.0f/line_vec[1];
					line_vec[2] = 1.0f/line_vec[2];

					line_vec[1] -= line_vec[0];
					line_vec[2] -= line_vec[0];
	
					line_vec[2] /= line_vec[1];


					if ((_header.transform_flag & 0x44) == 0x44) { // cc present
						InterpolateMatrix(temp_trans, _header.calibration_transform, line_vec);

						mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
						Calc::MF_InverseMultiply44(mf_hdr,temp_trans, 4, c_trans);

					}

					InterpolateMatrix(temp_trans, _header.color_transform, line_vec);

					mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
					Calc::MF_InverseMultiply44(mf_hdr,temp_trans, 4, c_trans);

				} else {
					if ((_header.transform_flag & 4) == 4) { // cc present
						mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
						Calc::MF_InverseMultiply44(mf_hdr, _header.calibration_transform[0], 4, c_trans);

					}

					mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
					Calc::MF_InverseMultiply44(mf_hdr, _header.color_transform[0], 4, c_trans);
				}

				prev_val[0] = temp_val[0]; prev_val[1] = temp_val[1];

				Calc::MF_Multiply44(c_trans, 1, _header.white_balance, temp_val);

/*
	x = X/(X+Y+Z)
	y = Y/(X+Y+Z)
	Y=Y
*/				

				temp_val[3] = temp_val[0] + temp_val[1] + temp_val[2];
				temp_val[2] = temp_val[1];
				temp_val[0] /= temp_val[3];
				temp_val[1] /= temp_val[3];
				temp_val[3] = 0.0;

				if ((Calc::FloatAbs(prev_val[0] - temp_val[0])+Calc::FloatAbs(prev_val[1] - temp_val[1])) < 0.1e-4) break;
			}

			_header.white_balance[0] = temp_val[0];
			_header.white_balance[1] = temp_val[1];
			_header.white_balance[2] = temp_val[2];
			_header.white_balance[3] = 0.0;
		}
							
		_header.temperature = CCT(_header.white_balance);


		// construct ca
/*
	XYZ(d50) = {0.96423f, 1.0f, 0.8251f}

	X = x*Y/y
	Y = Y
	Z = (1 - x - y)*Y/y

*/


		_header.white_balance[3] = _header.white_balance[2]/_header.white_balance[1];
		_header.white_balance[1] = _header.white_balance[2];
		_header.white_balance[0] *= _header.white_balance[3];
		_header.white_balance[2] = _header.white_balance[3] - _header.white_balance[0] - _header.white_balance[1];
		_header.white_balance[3] = 0.0;
/*
		temp_val[0] = 0.96423f/_header.white_balance[0];
		temp_val[1] = 1.0f/_header.white_balance[1];
		temp_val[2] = 0.8251f/_header.white_balance[2];
		temp_val[3] = 1.0f;
*/
		
		Calc::FloatCopy16(ca_trans, BFD_transform[0], 1);

		Calc::MF_Multiply44(ca_trans, 1, _header.white_balance, temp_val);

		temp_val[0] = 0.996311133f/temp_val[0];
		temp_val[1] = 1.020415824f/temp_val[1];
		temp_val[2] = 0.818531507f/temp_val[2];
		temp_val[3] = 1.0f;

		Calc::MF_Scale44(ca_trans, temp_val, 4);
		Calc::MF_Multiply44(const_cast<float *>(BFD_transform[1]), 4, ca_trans, ca_trans);

	}




	Calc::FloatClear16(c_trans, 1);
	c_trans[0] = c_trans[5] = c_trans[10] = c_trans[15] = 1.0f;

	if (_header.transform_flag & 1) { // analog balance
		c_trans[0] /= _header.analog_balance[0];
		c_trans[5] /= _header.analog_balance[1];
		c_trans[10] /= _header.analog_balance[2];
		c_trans[15] /= _header.analog_balance[3];
	}




	if ((_header.transform_flag & 0x44) == 0x44) { // two matrices
		line_vec[2] = _header.temperature;										

		line_vec[0] = temperature_table[_header.transform_type[0]];
		line_vec[1] = temperature_table[_header.transform_type[1]];

		if ((line_vec[2] <= line_vec[0]) || (line_vec[2] >= line_vec[1])) {
			if (line_vec[2] <= line_vec[0]) line_vec[2] = line_vec[0];
			else line_vec[2] = line_vec[1];
		}

		line_vec[0] = 1.0f/line_vec[0];
		line_vec[1] = 1.0f/line_vec[1];
		line_vec[2] = 1.0f/line_vec[2];

		line_vec[1] -= line_vec[0];
		line_vec[2] -= line_vec[0];
		line_vec[2] /= line_vec[1];

		InterpolateMatrix(temp_trans, _header.calibration_transform, line_vec);

		mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
		Calc::MF_InverseMultiply44(mf_hdr, temp_trans, 4, c_trans);

	} else {
		if ((_header.transform_flag & 4) == 4) { // cc present
			mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
			Calc::MF_InverseMultiply44(mf_hdr, _header.calibration_transform[0], 4, c_trans);

		}
	}





	if (_header.transform_flag & 8) { // chramtic adaptation present
		temp_val[0] = 0.96423f/_header.white_balance[0];
		temp_val[1] = 1.0f/_header.white_balance[1];
		temp_val[2] = 0.8251f/_header.white_balance[2];
		temp_val[3] = 1.0f;

		Calc::MF_Scale44(c_trans, temp_val, 4);

		if ((_header.transform_flag & 0x88) == 0x88) { // two matrices
			line_vec[2] = _header.temperature;										

			line_vec[0] = temperature_table[_header.transform_type[0]];
			line_vec[1] = temperature_table[_header.transform_type[1]];

			if ((line_vec[2] <= line_vec[0]) || (line_vec[2] >= line_vec[1])) {
				if (line_vec[2] <= line_vec[0]) line_vec[2] = line_vec[0];
				else line_vec[2] = line_vec[1];
			}

			line_vec[0] = 1.0f/line_vec[0];
			line_vec[1] = 1.0f/line_vec[1];
			line_vec[2] = 1.0f/line_vec[2];

			line_vec[1] -= line_vec[0];
			line_vec[2] -= line_vec[0];
			line_vec[2] /= line_vec[1];


			InterpolateMatrix(temp_trans, _header.chromatic_adaptation, line_vec);

			mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
			Calc::MF_Multiply44(temp_trans, 4, c_trans, c_trans);								

		} else {
			if ((_header.transform_flag & 2) == 2) { // cm present
				mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
				Calc::MF_Multiply44(_header.chromatic_adaptation[0], 4, c_trans, c_trans);
			}
		}


	} else {
		if ((_header.transform_flag & 0x22) == 0x22) { // two matrices
			line_vec[2] = _header.temperature;										

			line_vec[0] = temperature_table[_header.transform_type[0]];
			line_vec[1] = temperature_table[_header.transform_type[1]];

			if ((line_vec[2] <= line_vec[0]) || (line_vec[2] >= line_vec[1])) {
				if (line_vec[2] <= line_vec[0]) line_vec[2] = line_vec[0];
				else line_vec[2] = line_vec[1];
			}

			line_vec[0] = 1.0f/line_vec[0];
			line_vec[1] = 1.0f/line_vec[1];
			line_vec[2] = 1.0f/line_vec[2];

			line_vec[1] -= line_vec[0];
			line_vec[2] -= line_vec[0];
			line_vec[2] /= line_vec[1];


			InterpolateMatrix(temp_trans, _header.color_transform, line_vec);

			mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
			Calc::MF_InverseMultiply44(mf_hdr, temp_trans, 4, c_trans);								

		} else {
			if ((_header.transform_flag & 2) == 2) { // cm present
				mf_hdr[0] = 4; mf_hdr[1] = 1; mf_hdr[2] = 4; mf_hdr[3] = 1; mf_hdr[4] = 4; mf_hdr[5] = 0; mf_hdr[6] = 0; mf_hdr[7] = 0;
				Calc::MF_InverseMultiply44(mf_hdr, _header.color_transform[0], 4, c_trans);

			}
		}

		Calc::MF_Multiply44(ca_trans, 4, c_trans, c_trans);
	}

	
	// rimm transform
	Calc::MF_Multiply44(const_cast<float *>(d50_RIMM[1]), 4, c_trans, c_trans);
	
}

void Pics::TIFF::InterpolateMatrix(float * result, float in_mat[2][16], float * t_vals) {
	for (unsigned int i(0);i<16;i++) {
		result[i] = in_mat[0][i] + (in_mat[1][i] - in_mat[0][i])*t_vals[2];
	}
}

UI_64 Pics::TIFF::DeMosaic(SFSco::Object & pi_obj) {
	UI_64 result(0);
	StripDesc strip_desc;

	strip_desc.row_count = _header.height;
	strip_desc.row_width = _header.width;

	strip_desc.row_length = (_header.c_width << 2);

	if (unsigned char * pi_ptr = reinterpret_cast<unsigned char *>(pi_obj.Acquire())) {
		strip_desc.pointer = pi_ptr;

		if ((_header.cfa_cfg[0] + _header.cfa_cfg[1]) == 4) {
			switch (reinterpret_cast<unsigned int *>(_header.cfa_cfg+2)[0]) {
				case 0x02010100: // RG GB
					if (_header.precision == 1) result = Image_DeBayer_1RGGB(strip_desc);
					else result = Image_DeBayer_2RGGB(strip_desc);
				break;
				case 0x01020001: // GR BG
					if (_header.precision == 1) result = Image_DeBayer_1GRBG(strip_desc);
					else result = Image_DeBayer_2GRBG(strip_desc);
				break;
				case 0x00010102: // BG GR
					if (_header.precision == 1) result = Image_DeBayer_1BGGR(strip_desc);
					else result = Image_DeBayer_2BGGR(strip_desc);
				break;
				default:
					if (_header.precision == 1) result = Image_DeBayer_1GBRG(strip_desc);
					else result = Image_DeBayer_2GBRG(strip_desc);

			}
		}

		pi_obj.Release();
	}

	return result;
}


UI_64 Pics::TIFF::GrayOUT(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pii_offset(0), pi_runner(0);
	unsigned int topp(0);
	unsigned short sval(0);
	
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);


	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {

		strip_desc.flags = _header.bit_count[0];

		for (unsigned int i(0);i<_header.strip_count;i++) {

			if (byte_order == 1) {
				if (stripo_3) topp = stripo_3[i];
				else topp = stripo_4[i];
										
				if (strip_size_3) strip_desc.size = strip_size_3[i];
				else strip_desc.size = strip_size_4[i];
			} else {
				if (stripo_3) {
					sval = stripo_3[i];
					topp = System::BSWAP_16(sval);
				} else {
					topp = stripo_4[i];
					System::BSWAP_32(topp);
				}
										
				if (strip_size_3) {
					sval = strip_size_3[i];
					strip_desc.size = System::BSWAP_16(sval);
				} else {							
					strip_desc.size = strip_size_4[i];
					System::BSWAP_32(strip_desc.size);
				}

			}
		
			strip_desc.pointer = src_ptr + topp;

			if ((topp+strip_desc.size) > fsize) {
				result = 147852369;
				break;
			}

			if (_header.tiled == 0) { 
				if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
				else {
					if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
				}
								
				strip_desc.row_width = _header.width;
		
			} else {				
				strip_desc.row_width = _header.t_width;
				strip_desc.row_count = _header.t_height;

			}

			strip_desc.row_length = (_header.c_width << 2);
						
			if (_header.precision == 1) result = TIFF_GrayOut_1(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)));
			else result = TIFF_GrayOut_2(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)*2));

			if (_header.tiled == 0) {
				pi_offset += _header.rows_per_strip*_header.c_width;
			} else {
				pii_offset += _header.t_width;

				if (pii_offset == _header.c_width) {

					pii_offset = 0;
					pi_offset += _header.t_height*_header.c_width;
				}
								
			}

			if (result) break;
		
		}

		pi_obj.Release();
	}


	return result;
}


UI_64 Pics::TIFF::DeMosaic(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pii_offset(0), pi_runner(0);
	unsigned int topp(0);
	unsigned short sval(0);
	
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);


	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {

		strip_desc.flags = _header.bit_count[0];

		for (unsigned int i(0);i<_header.strip_count;i++) {

			if (byte_order == 1) {
				if (stripo_3) topp = stripo_3[i];
				else topp = stripo_4[i];
										
				if (strip_size_3) strip_desc.size = strip_size_3[i];
				else strip_desc.size = strip_size_4[i];
			} else {
				if (stripo_3) {
					sval = stripo_3[i];
					topp = System::BSWAP_16(sval);
				} else {
					topp = stripo_4[i];
					System::BSWAP_32(topp);
				}
										
				if (strip_size_3) {
					sval = strip_size_3[i];
					strip_desc.size = System::BSWAP_16(sval);
				} else {							
					strip_desc.size = strip_size_4[i];
					System::BSWAP_32(strip_desc.size);
				}

			}
		
			strip_desc.pointer = src_ptr + topp;

			if ((topp+strip_desc.size) > fsize) {
				result = 147852369;
				break;
			}

			if (_header.tiled == 0) { 
				if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
				else {
					if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
				}
								
				strip_desc.row_width = _header.width;
		
			} else {				
				strip_desc.row_width = _header.t_width;
				strip_desc.row_count = _header.t_height;

			}

			strip_desc.row_length = (_header.c_width << 2);
			
			if ((_header.cfa_cfg[0] + _header.cfa_cfg[1]) == 4) {
				switch (reinterpret_cast<unsigned int *>(_header.cfa_cfg+2)[0]) {
					case 0x02010100: // RG GB
						if (_header.precision == 1) result = Image_DeBayer_1RGGBu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)));
						else result = Image_DeBayer_2RGGBu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)*2));
					break;
					case 0x01020001: // GR BG
						if (_header.precision == 1) result = Image_DeBayer_1GRBGu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)));
						else result = Image_DeBayer_2GRBGu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)*2));
					break;
					case 0x00010102: // BG GR
						if (_header.precision == 1) result = Image_DeBayer_1BGGRu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)));
						else result = Image_DeBayer_2BGGRu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)*2));
					break;
					default:
						if (_header.precision == 1) result = Image_DeBayer_1GBRGu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)));
						else result = Image_DeBayer_2GBRGu(strip_desc, reinterpret_cast<unsigned char *>(pipo + (pi_offset + pii_offset)*2));

				}
			}

			if (_header.tiled == 0) {
				pi_offset += _header.rows_per_strip*_header.c_width;
			} else {
				pii_offset += _header.t_width;

				if (pii_offset == _header.c_width) {

					pii_offset = 0;
					pi_offset += _header.t_height*_header.c_width;
				}
								
			}

			if (result) break;
		
		}

		pi_obj.Release();
	}


	return result;
}

UI_64 Pics::TIFF::JpegGetTables(const unsigned char * src_ptr, unsigned int t_offset, unsigned int t_size) {
	return jpguk.GetXTables(src_ptr + t_offset + 2, t_size - 4);
}


UI_64 Pics::TIFF::JpegDecode(const unsigned char * src_ptr, SFSco::Object & pi_obj, unsigned int fsize) {
	UI_64 result(0), pi_offset(0), pi_runner(0), pii_offset(0);
	unsigned int topp(0);
	unsigned short sval(0);
	
	StripDesc strip_desc;
	const unsigned short * stripo_3(0), * strip_size_3(0);
	const unsigned int * stripo_4(0), * strip_size_4(0);

	SFSco::Object temp_obj;



	switch (_header.so_type) {
		case 3:
			stripo_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_offsets);
		break;
		case 4:
			stripo_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_offsets);
		break;
		case 0x80:
			stripo_4 = strip_2;
		break;

	}

	switch (_header.sbc_type) {
		case 3:
			strip_size_3 = reinterpret_cast<const unsigned short *>(src_ptr + _header.strip_byte_count);
		break;
		case 4:
			strip_size_4 = reinterpret_cast<const unsigned int *>(src_ptr + _header.strip_byte_count);
		break;
		case 0x80:
			strip_size_4 = strip_2+2;
		break;

	}

	if (_header.tiled) {
		result = temp_obj.New(0, (_header.t_height + 16)*(_header.t_width + 16)*_header.precision*4, SFSco::large_mem_mgr);

	}

	if (result != -1)
	if (unsigned int * pipo = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
		if (_header.tiled == 0)
		if (_header.precision == 1) {
			System::MemoryFill_SSE3(pipo, _header.c_width*_header.c_height*4, 0xFF000000FF000000, 0xFF000000FF000000);
		} else {
			System::MemoryFill_SSE3(pipo, _header.c_width*_header.c_height*8, 0xFFFF000000000000, 0xFFFF000000000000);
		}


		strip_desc.flags = _header.ccitt_options;

		for (unsigned int i(0);i<_header.strip_count;i++) {

			if (byte_order == 1) {
				if (stripo_3) topp = stripo_3[i];
				else topp = stripo_4[i];
										
				if (strip_size_3) strip_desc.size = strip_size_3[i];
				else strip_desc.size = strip_size_4[i];
			} else {
				if (stripo_3) {
					sval = stripo_3[i];
					topp = System::BSWAP_16(sval);
				} else {
					topp = stripo_4[i];
					System::BSWAP_32(topp);
				}
										
				if (strip_size_3) {
					sval = strip_size_3[i];
					strip_desc.size = System::BSWAP_16(sval);
				} else {							
					strip_desc.size = strip_size_4[i];
					System::BSWAP_32(strip_desc.size);
				}

			}
		
			strip_desc.pointer = src_ptr + topp;

			if ((topp+strip_desc.size) > fsize) {
				result = 147852369;
				break;
			}

			if (_header.tiled == 0) { 
				if (i != (_header.strip_count-1)) strip_desc.row_count = _header.rows_per_strip;
				else {
					if ((strip_desc.row_count = _header.height % _header.rows_per_strip) == 0) strip_desc.row_count = _header.rows_per_strip;
				}
								
				strip_desc.row_width = _header.width;
		
			} else {				
				strip_desc.row_width = _header.t_width;
				strip_desc.row_count = _header.t_height;

			}

			strip_desc.row_length = (_header.c_width << 2);

			if (_header.tiled == 0) {
				result = jpguk.XDecode(strip_desc, reinterpret_cast<unsigned char *>(pipo + pi_offset));

				pi_offset += _header.rows_per_strip*strip_desc.row_width*_header.precision;

			} else {
				if (unsigned char * temp_po = reinterpret_cast<unsigned char *>(temp_obj.Acquire())) {
					if (_header.precision == 1) {
						System::MemoryFill_SSE3(temp_po, (_header.t_width+16)*(_header.t_height+16)*4, 0xFF000000FF000000, 0xFF000000FF000000);
					} else {
						System::MemoryFill_SSE3(temp_po, (_header.t_width+16)*(_header.t_height+16)*8, 0xFFFF000000000000, 0xFFFF000000000000);
					}

					result = jpguk.XDecode(strip_desc, temp_po);

					strip_desc.pointer = temp_po;
					strip_desc.size = _header.t_width;
					strip_desc.flags = _header.t_height;
					strip_desc.row_length = (_header.c_width<<2);

					if (!result) {
						if (_header.precision == 1) TIFF_Detwist_1(strip_desc, pipo + pi_offset + pii_offset);
						else TIFF_Detwist_2(strip_desc, pipo + (pi_offset + pii_offset)*2);
					} 

					pi_offset += _header.t_width;

					if (pi_offset == _header.c_width) {
						pi_offset = 0;
						pii_offset += _header.t_height*_header.c_width;
						
					}

					temp_obj.Release();
				}



			}


			if (result) break;
		
		}

		pi_obj.Release();
	}

	if (temp_obj) temp_obj.Destroy();

	return result;
}



UI_64 Pics::TIFF::IsTIFF(const char * ipo, UI_64 lim) {
	UI_64 result(0);

	result = ((ipo[0] == 'I') && (ipo[1] == 'I')) | 2*((ipo[0] == 'M') && (ipo[1] == 'M'));
	
	if (result == 1) {
		result &= ((ipo[2] == 42) && (ipo[3] == 0));
	} else {
		if (result == 2) {
			result *= ((ipo[2] == 0) && (ipo[3] == 42));
		}
	}

	return result;
}



