
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



EXTERN	?white_counts@TIFF@Pics@@0QBEB:QWORD
EXTERN	?white_codes@TIFF@Pics@@0QBGB:QWORD
EXTERN	?white_vals@TIFF@Pics@@0QBGB:QWORD

EXTERN	?black_counts@TIFF@Pics@@0QBEB:QWORD
EXTERN	?black_codes@TIFF@Pics@@0QBGB:QWORD
EXTERN	?black_vals@TIFF@Pics@@0QBGB:QWORD

EXTERN	?counts_2D@TIFF@Pics@@0QBEB:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
TIFF_GrayOut_2	PROC

	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15



	XOR rax, rax
	NOT rax
	SHL rax, 48

	MOVD xmm9, rax
	PUNPCKLQDQ xmm9, xmm9
	
	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	MOV r12d, [rcx+24]


	MOV rbx, r11
	MOV rcx, rdx


	MOV eax, r12d
	MUL r8
	SHR rax, 3
	MOV r13, rax ; next line offset


	SHR r8d, 4
		
	MOV rdx, rcx
	MOV rax, rcx

	MOV ecx, r8d
	SHL rcx, 32

	MOV cl, r12b
	MOV ch, cl

	SUB cl, 16
	NEG cl

	SHR r12d, 1	
	SHL r9d, 1

		

align 16
	row_loop:
		MOV r15, [r11]

		CMP ch, 16
		JZ load_r150
			MOV r14, r15
			BSWAP r14
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

	load_r150:
		MOVD xmm0, r15


		MOV r15, [r11+r12]

		CMP ch, 16
		JZ load_r151
			MOV r14, r15
			BSWAP r14
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

	load_r151:

		MOVD xmm2, r15
		PUNPCKLQDQ xmm0, xmm2

		LEA r11, [r11+r12*2]


		MOV r15, [r11]

		CMP ch, 16
		JZ load_r152
			MOV r14, r15
			BSWAP r14
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

	load_r152:

		MOVD xmm4, r15
		
		MOV r15, [r11+r12]

		CMP ch, 16
		JZ load_r153
			MOV r14, r15
			BSWAP r14
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16
		
			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

			ROR cx, 8
			ROL r14, cl
			ROR cx, 8
			MOV r15w, r14w
			SHL r15w, cl
			ROR r15, 16

	load_r153:

		MOVD xmm6, r15
		PUNPCKLQDQ xmm4, xmm6


		LEA r11, [r11+r12*2]

		MOVDQA xmm2, xmm0

		PUNPCKLWD xmm0, xmm0
		PUNPCKHWD xmm2, xmm2

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLDQ xmm0, xmm0
		PUNPCKHDQ xmm1, xmm1

		PUNPCKLDQ xmm2, xmm2
		PUNPCKHDQ xmm3, xmm3

		POR xmm0, xmm9
		POR xmm1, xmm9
		POR xmm2, xmm9
		POR xmm3, xmm9


		MOVDQA xmm6, xmm4

		PUNPCKLWD xmm4, xmm4
		PUNPCKHWD xmm6, xmm6

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLDQ xmm4, xmm4
		PUNPCKHDQ xmm5, xmm5

		PUNPCKLDQ xmm6, xmm6
		PUNPCKHDQ xmm7, xmm7

		POR xmm4, xmm9
		POR xmm5, xmm9
		POR xmm6, xmm9
		POR xmm7, xmm9


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3
		MOVNTDQ [rdx+64], xmm4
		MOVNTDQ [rdx+80], xmm5
		MOVNTDQ [rdx+96], xmm6
		MOVNTDQ [rdx+112], xmm7

		ADD rdx, 128


		DEC r8d
		JNZ row_loop

			MOV r8, rcx
			SHR r8, 32

			LEA rbx, [rbx + r13]
			LEA rax, [rax + r9]

			MOV r11, rbx
			MOV rdx, rax

	DEC r10d
	JNZ row_loop

	SFENCE

	XOR rax, rax

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx


	RET
TIFF_GrayOut_2	ENDP

ALIGN 16
TIFF_GrayOut_1	PROC
	PUSH rbx

	MOV eax, 0FF000000h
	MOVD xmm9, rax
	PSHUFD xmm9, xmm9, 0

	MOV r11, [rcx] ; source ptr
	MOV r10d, [rcx+12] ; row_count
	MOV r9d, [rcx+16] ; line offset
	MOV r8d, [rcx+20] ; frame width
	
	MOV rbx, r11
	MOV rcx, rdx

	MOV eax, r8d

	SHR r8d, 5
	


align 16
	row_loop:
		MOVDQU xmm0, [r11]
		MOVDQU xmm4, [r11+16]

		ADD r11, 32

		MOVDQA xmm2, xmm0

		PUNPCKLBW xmm0, xmm0
		PUNPCKHBW xmm2, xmm2

		MOVDQA xmm1, xmm0
		MOVDQA xmm3, xmm2

		PUNPCKLWD xmm0, xmm0
		PUNPCKHWD xmm1, xmm1

		PUNPCKLWD xmm2, xmm2
		PUNPCKHWD xmm3, xmm3

		POR xmm0, xmm9
		POR xmm1, xmm9
		POR xmm2, xmm9
		POR xmm3, xmm9


		MOVDQA xmm6, xmm4

		PUNPCKLBW xmm4, xmm4
		PUNPCKHBW xmm6, xmm6

		MOVDQA xmm5, xmm4
		MOVDQA xmm7, xmm6

		PUNPCKLWD xmm4, xmm4
		PUNPCKHWD xmm5, xmm5

		PUNPCKLWD xmm6, xmm6
		PUNPCKHWD xmm7, xmm7

		POR xmm4, xmm9
		POR xmm5, xmm9
		POR xmm6, xmm9
		POR xmm7, xmm9


		MOVNTDQ [rdx], xmm0
		MOVNTDQ [rdx+16], xmm1
		MOVNTDQ [rdx+32], xmm2
		MOVNTDQ [rdx+48], xmm3
		MOVNTDQ [rdx+64], xmm4
		MOVNTDQ [rdx+80], xmm5
		MOVNTDQ [rdx+96], xmm6
		MOVNTDQ [rdx+112], xmm7

		ADD rdx, 128

		DEC r8d
		JNZ row_loop

			MOV r8d, eax
			SHR r8d, 5

			LEA rbx, [rbx + rax]
			LEA rcx, [rcx + r9]

			MOV r11, rbx
			MOV rdx, rcx

	DEC r10d
	JNZ row_loop

	SFENCE

	XOR rax, rax

	POP rbx
	RET
TIFF_GrayOut_1	ENDP


ALIGN 16
TIFF_Detwist_2	PROC
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV r11, [rcx]
	

	MOV r8d, [rcx+20] ; source width
	
	MOV r9d, [rcx+8] ; tile width
	MOV r10d, [rcx+24] ; tile height

	MOV eax, [rcx+16] ; line offset
	SHL rax, 1
		
	SHR r9d, 3
	MOV r15d, r9d


	MOV rcx, rdx
	XOR r13, r13
	MOV r14, r11

align 16
	copy_loop:
		
			MOVDQA xmm0, [r11]
			MOVDQA xmm1, [r11+16]
			MOVDQA xmm2, [r11+32]
			MOVDQA xmm3, [r11+48]

			ADD r11, 64	

			MOVNTDQ [rdx], xmm0
			MOVNTDQ [rdx+16], xmm1
			MOVNTDQ [rdx+32], xmm2
			MOVNTDQ [rdx+48], xmm3

			ADD rdx, 64

		DEC r9d
		JNZ copy_loop

		MOV r9d, r15d
		
		LEA rcx, [rcx+rax]
		MOV rdx, rcx

		LEA r13, [r13 + r15*8]
		CMP r13, r8
		JB no_reset
			XOR r13, r13

			LEA r14, [r14+r8*8]
			MOV r11, r14

		no_reset:

	DEC r10d
	JNZ copy_loop


	SFENCE

	POP r15
	POP r14
	POP r13
	POP r12

	RET
TIFF_Detwist_2	ENDP

ALIGN 16
TIFF_Detwist_1	PROC
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	MOV r11, [rcx]
	

	MOV r8d, [rcx+20] ; source width
	
	MOV r9d, [rcx+8] ; tile width
	MOV r10d, [rcx+24] ; tile height

	MOV eax, [rcx+16] ; line offset
		
	SHR r9d, 3
	MOV r15d, r9d


	MOV rcx, rdx
	XOR r13, r13
	MOV r14, r11

align 16
	copy_loop:
		
			MOVDQA xmm0, [r11]
			MOVDQA xmm1, [r11+16]

			ADD r11, 32

			MOVNTDQ [rdx], xmm0
			MOVNTDQ [rdx+16], xmm1

			ADD rdx, 32

		DEC r9d
		JNZ copy_loop

		MOV r9d, r15d
		
		LEA rcx, [rcx+rax]
		MOV rdx, rcx

		LEA r13, [r13 + r15*8]
		CMP r13, r8
		JB no_reset
			XOR r13, r13

			LEA r14, [r14+r8*4]
			MOV r11, r14

		no_reset:

	DEC r10d
	JNZ copy_loop

	SFENCE

	POP r15
	POP r14
	POP r13
	POP r12

	RET
TIFF_Detwist_1	ENDP



ALIGN 16
TIFF_SampleCopy_4	PROC
	PUSH r12

		MOV r8, [rcx]
		MOV r9, rdx
		
		MOV eax, [rcx+12]
		MOV r10d, [rcx+16]
		
		MOV ecx, [rcx+20] ; width
		MOV r12d, ecx

	align 16
		row_loop:
			MOV r11d, [r8]
			MOVNTI [r9], r11d

			ADD r8, 4
			ADD r9, 4

			DEC ecx
			JNZ row_loop

			ADD rdx, r10
			MOV r9, rdx

			MOV ecx, r12d

		DEC eax
		JNZ row_loop

	SFENCE

	POP r12

	RET
TIFF_SampleCopy_4	ENDP


ALIGN 16
TIFF_SampleCopy_3	PROC
	PUSH rsi

		MOV rsi, [rcx]
		MOV r9, rdx
		
		MOV r11d, [rcx+12]
		MOV r10d, [rcx+16]
		
		MOV ecx, [rcx+20] ; width
		MOV r8d, ecx

	align 16
		row_loop:
			LODSB
			ROR eax, 8
			LODSB
			ROR eax, 8
			LODSB
			ROR eax, 8

			MOV al, 0FFh
			ROR eax, 8
			
			MOVNTI [r9], eax
			
			ADD r9, 4

			DEC ecx
			JNZ row_loop

			ADD rdx, r10
			MOV r9, rdx

			MOV ecx, r8d

		DEC r11d
		JNZ row_loop


	SFENCE

	POP rsi

	RET
TIFF_SampleCopy_3	ENDP


ALIGN 16
TIFF_PredictSample_3	PROC
	PUSH rsi

		MOV rsi, [rcx]
		MOV r9, rdx
		
		MOV r11d, [rcx+12]
		MOV r10d, [rcx+16]
		
		MOV ecx, [rcx+20] ; width
		MOV r8d, ecx

		XOR rax, rax

	align 16
		row_loop:
			ADD al, [rsi]
			ROR eax, 8
			ADD al, [rsi+1]
			ROR eax, 8
			ADD al, [rsi+2]
			ROR eax, 8

			MOV al, 0FFh
			ROR eax, 8
			
			MOVNTI [r9], eax
			
			ADD rsi, 3
			ADD r9, 4

			DEC ecx
			JNZ row_loop

			ADD rdx, r10
			MOV r9, rdx

			MOV ecx, r8d

		DEC r11d
		JNZ row_loop

	SFENCE

	POP rsi

	RET
TIFF_PredictSample_3	ENDP

ALIGN 16
TIFF_PredictSample_4	PROC
	PUSH rsi

		MOV rsi, [rcx]
		MOV r9, rdx
		
		MOV r11d, [rcx+12]
		MOV r10d, [rcx+16]
		
		MOV ecx, [rcx+20] ; width
		MOV r8d, ecx

		XOR rax, rax

	align 16
		row_loop:
			ADD al, [rsi]
			ROR eax, 8
			ADD al, [rsi+1]
			ROR eax, 8
			ADD al, [rsi+2]
			ROR eax, 8
			ADD al, [rsi+3]
			ROR eax, 8
			
			MOVNTI [r9], eax
			
			ADD rsi, 4
			ADD r9, 4

			DEC ecx
			JNZ row_loop

			ADD rdx, r10
			MOV r9, rdx

			MOV ecx, r8d

		DEC r11d
		JNZ row_loop

	SFENCE

	POP rsi

	RET
TIFF_PredictSample_4	ENDP

ALIGN 16
TIFF_BPredict_8X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width
		
		MOV r10d, r9d

		XOR rax, rax

	align 16
		strip_loop:
			ADD al, [rsi]
			MOV ecx, [r8+rax*4]

			MOVNTI [rdi], ecx
			ADD rdi, 4

			INC rsi

			DEC r10d
			JNZ strip_loop

			ADD rdx, rbx
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	SFENCE

	POP rbx
	POP rdi
	POP rsi

	RET
TIFF_BPredict_8X	ENDP


ALIGN 16
TIFF_IPredict_8	PROC
	PUSH rsi
	PUSH rdi

		ADD rdx, r8
		MOV rdi, rdx
		
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV r8d, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width
		
		MOV r10d, r9d

		XOR rax, rax

	align 16
		strip_loop:
			ADD al, [rsi]			
			MOV [rdi], al

			ADD rdi, 4
			INC rsi

			DEC r10d
			JNZ strip_loop

			ADD rdx, r8
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	POP rdi
	POP rsi

	RET
TIFF_IPredict_8	ENDP



ALIGN 16
TIFF_BCopy_1X	PROC
	PUSH rsi
	PUSH rdi

		MOV rdi, rdx

		MOVD xmm3, DWORD PTR [r8+4] ; white sample
		PUNPCKLDQ xmm3, xmm3
		MOVDQA xmm0, xmm3
		PSLLDQ xmm0, 8
		POR xmm3, xmm0

		PXOR xmm0, xmm0
		PXOR xmm1, xmm1

		MOV rsi, [rcx] ; source pointer
		
		MOV r11d, [rcx+12] ; row count
		MOV r10d, [rcx+16] ; c_width

		MOV r9d, r10d

		SHR r9d, 5
		MOV r8d, r9d

	align 16
		strip_loop:
			LODSB

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm0, ecx

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 4
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 8
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 12
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm1, ecx

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 4
			POR xmm1, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 8
			POR xmm1, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 12
			POR xmm1, xmm2

			PXOR xmm0, xmm3
			PXOR xmm1, xmm3

			MOVNTDQ [rdi], xmm0
			MOVNTDQ [rdi+16], xmm1
			
			ADD rdi, 32


			DEC r8d
			JNZ strip_loop

			ADD rdx, r10
			MOV rdi, rdx

			MOV r8d, r9d

		DEC r11d
		JNZ strip_loop

	SFENCE

	POP rdi
	POP rsi

	RET
TIFF_BCopy_1X	ENDP


ALIGN 16
TIFF_ICopy_8	PROC
	PUSH rsi
	PUSH rdi

		ADD rdx, r8
		MOV rdi, rdx
		
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV r8d, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width
		
		MOV r10d, r9d

		XOR rax, rax

	align 16
		strip_loop:
			LODSB
			MOV [rdi], al

			ADD rdi, 4

			DEC r10d
			JNZ strip_loop

			ADD rdx, r8
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	POP rdi
	POP rsi

	RET
TIFF_ICopy_8	ENDP


ALIGN 16
TIFF_BCopy_8X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width
		
		MOV r10d, r9d

		XOR rax, rax

	align 16
		strip_loop:
			LODSB
			MOV ecx, [r8+rax*4]

			MOVNTI [rdi], ecx
			ADD rdi, 4

			DEC r10d
			JNZ strip_loop

			ADD rdx, rbx
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	SFENCE

	POP rbx
	POP rdi
	POP rsi

	RET
TIFF_BCopy_8X	ENDP


ALIGN 16
TIFF_BCopy_4X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width

		ADD r9d, 1
		SHR r9d, 1
		MOV r10d, r9d

		XOR rax, rax

	align 16
		strip_loop:
			LODSB

			MOVZX rcx, al
			SHR rax, 4
			AND rcx, 0Fh

			MOV ecx, [r8+rcx*4]
			SHL rcx, 32
			OR ecx, [r8+rax*4]

			MOVNTI [rdi], rcx
			ADD rdi, 8

			DEC r10d
			JNZ strip_loop

			ADD rdx, rbx
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	SFENCE

	POP rbx
	POP rdi
	POP rsi


	RET
TIFF_BCopy_4X	ENDP

ALIGN 16
TIFF_BPredict_4X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width

		ADD r9d, 1
		SHR r9d, 1
		MOV r10d, r9d

		XOR rax, rax
		XOR r12, r12
		XOR r13, r13

	align 16
		strip_loop:
			LODSB

			MOVZX rcx, al
			AND rcx, 0Fh
			ADD r12, rcx

			SHR rax, 4
			ADD r13, rcx

			MOV ecx, [r8+r12*4]
			SHL rcx, 32
			OR ecx, [r8+r13*4]

			MOVNTI [rdi], rcx
			ADD rdi, 8

			DEC r10d
			JNZ strip_loop

			ADD rdx, rbx
			MOV rdi, rdx

			MOV r10d, r9d

		DEC r11d
		JNZ strip_loop

	SFENCE

	POP r13
	POP r12
	POP rbx
	POP rdi
	POP rsi


	RET
TIFF_BPredict_4X	ENDP



ALIGN 16
TIFF_BUnPack_1X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r15
	PUSH r14

		XOR r15, r15
		MOV rdi, rdx

		MOVD xmm3, DWORD PTR [r8+4] ; white sample
		PUNPCKLDQ xmm3, xmm3
		MOVDQA xmm0, xmm3
		PSLLDQ xmm0, 8
		POR xmm3, xmm0

		PXOR xmm0, xmm0
		PXOR xmm1, xmm1


		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width

		MOV r9d, ebx

		SHR r9d, 5
		MOV r8d, r9d

align 16
	next_byte:
		LODSB
		MOV r15b, al
		XOR r14, r14


		TEST r15b, 80h
		JZ strip_copy_loop
			TEST r15b, 7Fh
			JZ next_byte
				MOV r14b, r15b
				NEG r14b
				MOV r15b, 1




	align 16
		strip_copy_loop:
			LODSB

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm0, ecx

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 4
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 8
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 12
			POR xmm0, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm1, ecx

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 4
			POR xmm1, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 8
			POR xmm1, xmm2

			XOR rcx, rcx
			SHL al, 1
			RCL ecx, 1
			DEC ecx			
			MOVD xmm2, ecx
			PSLLDQ xmm2, 12
			POR xmm1, xmm2

			PXOR xmm0, xmm3
			PXOR xmm1, xmm3

			MOVNTDQ [rdi], xmm0
			MOVNTDQ [rdi+16], xmm1
			
			ADD rdi, 32
			
			DEC r8d

		DEC r15b
		JNZ strip_copy_loop

			TEST r14b, r14b
			JZ no_run						
				SUB r8d, r14d

			align 16
				run_copy_loop:

					MOVNTDQ [rdi], xmm0
					MOVNTDQ [rdi+16], xmm1
			
					ADD rdi, 32

				DEC r14b
				JNZ run_copy_loop

			no_run:
				CMP r8d, 0
				JL exit
				JNZ next_byte
					ADD rdx, rbx
					MOV rdi, rdx

					MOV r8d, r9d
				
		DEC r11d
		JNZ next_byte


		exit:

	SFENCE

	POP r14
	POP r15
	POP rbx
	POP rdi
	POP rsi

	RET
TIFF_BUnPack_1X	ENDP


ALIGN 16
TIFF_BUnPack_8X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r15
	PUSH r14

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		
		MOV r9d, [rcx+20] ; width
		MOV r10d, r9d

		XOR rax, rax

align 16
	next_byte:
		LODSB
		MOV r15b, al
		XOR r14, r14

		TEST r15b, 80h
		JZ strip_loop
			TEST r15b, 7Fh
			JZ next_byte
				MOV r14b, r15b
				NEG r14b
				MOV r15b, 1

	align 16
		strip_loop:
			LODSB
			MOV ecx, [r8+rax*4]

			MOVNTI [rdi], ecx
			ADD rdi, 4

		DEC r15b
		JNZ strip_loop


			TEST r14b, r14b
			JZ no_run						
				SUB r10d, r14d

			align 16
				run_copy_loop:
					MOVNTI [rdi], ecx			
					ADD rdi, 4

				DEC r14b
				JNZ run_copy_loop

			no_run:
				CMP r10d, 0
				JL exit
				JNZ next_byte
					ADD rdx, rbx
					MOV rdi, rdx

					MOV r10d, r9d

			

		DEC r11d
		JNZ next_byte
		
	exit:

	SFENCE

	POP r14
	POP r15
	POP rbx
	POP rdi
	POP rsi

	RET
TIFF_BUnPack_8X	ENDP


ALIGN 16
TIFF_BUnPack_4X	PROC
	PUSH rsi
	PUSH rdi
	PUSH rbx

	PUSH r14
	PUSH r15

		MOV rdi, rdx
		MOV rsi, [rcx]
		
		MOV r11d, [rcx+12] ; row count
		MOV ebx, [rcx+16] ; c_width
		MOV r9d, [rcx+20] ; width

		ADD r9d, 1
		SHR r9d, 1

		MOV r10d, r9d

		XOR rax, rax

align 16
	next_byte:
		LODSB

		MOV r15b, al
		XOR r14, r14

		TEST r15b, 80h
		JZ strip_loop
			TEST r15b, 7Fh
			JZ next_byte
				MOV r14b, r15b
				NEG r14b
				MOV r15b, 1

	align 16
		strip_loop:
			LODSB

			MOVZX rcx, al
			SHR rax, 4
			AND rcx, 0Fh

			MOV ecx, [r8+rcx*4]
			SHL rcx, 32
			OR ecx, [r8+rax*4]
			
			MOVNTI [rdi], rcx
			ADD rdi, 8

			DEC r15b
			JNZ strip_loop

			TEST r14b, r14b
			JZ no_run						
				SUB r10d, r14d

			align 16
				run_copy_loop:
					MOVNTI [rdi], rcx			
					ADD rdi, 8

				DEC r14b
				JNZ run_copy_loop

			no_run:
				CMP r10d, 0
				JL exit
				JNZ next_byte
					ADD rdx, rbx
					MOV rdi, rdx

					MOV r10d, r9d			

		DEC r11d
		JNZ next_byte


		exit:

	SFENCE

	POP r15
	POP r14
	POP rbx
	POP rdi
	POP rsi


	RET
TIFF_BUnPack_4X	ENDP


ALIGN 16
TIFF_T41DUnPack	PROC
	PUSH rbp
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15			

		MOV r8, rdx

		MOV rsi, [rcx] ; source pointer
		
		MOV ebp, [rcx+8]
		MOV r14d, [rcx+20] ; width
		
		MOV [rcx], rdx

		MOV r9, rcx
			
		XOR rax, rax
		XOR rbx, rbx
		XOR rcx, rcx


	; test for EOL

		MOV al, [rsi]
		ROR eax, 8
		MOV al, [rsi+1]

		ROL eax, 8

		MOV rdx, 0FFFh
		AND dx, ax

		CMP dx, 0800h
		JNZ exit

		MOV bh, 4
		SHR eax, 12
		
		TEST DWORD PTR [r9+24], 1
		JZ no2Din
			INC bh
			SHR eax, 1
			JNC exit
		no2Din:

		SUB ebp, 2		
		ADD rsi, 2

	align 16
		T4_1D_line_in::

			XOR r12, r12
			NOT r12
	
			XOR rdx, rdx
			XOR r11, r11 ; run length

			LEA r13, [?white_counts@TIFF@Pics@@0QBEB]
			LEA rdi, [?white_codes@TIFF@Pics@@0QBGB]
			LEA r15, [?white_vals@TIFF@Pics@@0QBGB]

	align 16
		stream_scan_loop:
			AND bh, 7
			JNZ nextbit_loop

			LODSB
						
			DEC ebp
			JZ exit

		align 16
			nextbit_loop:				
				CMP bh, 8
				JAE stream_scan_loop

				ADD bx, 0101h

				TEST DWORD PTR [r9+24], 20000000h
				JNZ le2ri_1
					SHR al, 1
					JMP pkup_1
				le2ri_1:
					SHL al, 1
				pkup_1:

				RCL edx, 1

				MOVZX ecx, BYTE PTR [r13] ; count
				INC r13 ; count_pointer

				TEST cl, cl
				JZ nextbit_loop
					CMP cl, 0FFh
					JZ exit
						ADD ah, cl

						XCHG dx, ax
							REPNE SCASW
						XCHG dx, ax

					JNZ nextbit_loop						
						XOR rdx, rdx
						
						SUB ah, cl
									
						MOV cl, bl						
						SUB r13, rcx

						MOV cl, ah
						SHL rcx, 1
						SUB rdi, rcx

						MOVZX ecx, WORD PTR [r15+rcx-2] ; val
						ADD r11d, ecx ; 

						MOV ah, 0
						MOV bl, 0

						CMP ecx, 64
						JAE nextbit_loop
	
			SUB r14d, r11d
			JS exit ; error
				
				TEST r12b, r12b
				JNZ skip_copy

			align 16
				bw_copy_loop:
					MOVNTI [r8], r12d
					ADD r8, 4
					DEC r11d
				JNZ bw_copy_loop
							
				skip_copy:
					LEA r8, [r8+r11*4]
					XOR r11, r11
						
			TEST r14d, r14d
			JZ next_line_set
				
				NOT r12d
				OR r12d, 0FF000000h

				TEST r12b, r12b
				JZ black_init
					LEA r13, [?white_counts@TIFF@Pics@@0QBEB]
					LEA rdi, [?white_codes@TIFF@Pics@@0QBGB]
					LEA r15, [?white_vals@TIFF@Pics@@0QBGB]
					JMP nextbit_loop

				black_init:
					LEA r13, [?black_counts@TIFF@Pics@@0QBEB]
					LEA rdi, [?black_codes@TIFF@Pics@@0QBGB]
					LEA r15, [?black_vals@TIFF@Pics@@0QBGB]
				
				JMP nextbit_loop	



		align 16
			next_line_set:
				TEST DWORD PTR [r9+24], 10000000h
				JNZ end_of2Dline
				; test for EOL

				MOV cl, 8
				SUB cl, bh

				ROR eax, cl
				
				LODSB
				DEC ebp
				
				CMP cl, 4
				JAE noxbyte
					ROR eax, 8
					LODSB
					DEC ebp

					ADD cl, 8

				noxbyte:

				ROL eax, cl
				MOV edx, eax

				MOV bh, 12
				SUB bh, cl
				
				TEST DWORD PTR [r9+24], 4
				JZ noeolpad
					SUB cl, 4
					SHR dx, cl
					
					XOR rax, rax
					XOR rbx, rbx
				noeolpad:
					
					AND dx, 0FFFh
					CMP dx, 0800h
					JNZ exit				

					SHR eax, 12


					MOV r8d, [r9+16]
					MOV r14d, [r9+20]

					ADD r8, [r9]
					MOV [r9], r8
				

		DEC DWORD PTR [r9+12]
		JNZ T4_1D_line_in

exit:

	SFENCE

	MOV [r9], rsi
	MOV [r9+8], ebp

	SHR ebx, 8

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	POP rdi
	POP rsi
	POP rbp

	RET
TIFF_T41DUnPack	ENDP


ALIGN 16
TIFF_T42DUnPack	PROC
	PUSH rbp
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15			

		XOR r12, r12
		NOT r12

		MOV r8, rdx

		MOV rsi, [rcx] ; source pointer
		
		MOV ebp, [rcx+8]
		MOV r14d, [rcx+20] ; width
		
		MOV [rcx], rdx

		MOV r9, rcx

		MOV r10d, [r9+16]
		NEG r10
		ADD r10, r8

		
		XOR r11, r11 ; run length
		XOR rdx, rdx
		XOR rax, rax
		XOR rcx, rcx

		MOV rbx, 0800h

		
	;	TEST DWORD PTR [r9+24], 40000000h
	;	CMOVZ r10, r8

		MOV r10, r8

	align 16
		line_start:
	;		TEST DWORD PTR [r9+24], 04000000h
	;		CMOVNZ r12d, [r8-4]

			TEST DWORD PTR [r9+24], 80000000h
			JNZ mode_selector
	; test for EOL

		MOV cl, 8
		SUB cl, bh

		ROR eax, cl
				
		LODSB
		DEC ebp
				
		CMP cl, 5
		JAE noxbyte
			ROR eax, 8
			LODSB
			DEC ebp

			ADD cl, 8

		noxbyte:

			ROL eax, cl
			MOV edx, eax

			MOV bh, 13
			SUB bh, cl
				
			AND dx, 0FFFh
			CMP dx, 0800h
			JNZ exit

			SHR eax, 13
			JC T4_1D_line_in

	align 16
		mode_selector:

			XOR rdx, rdx
			MOV bl, 0

			LEA r13, [?counts_2D@TIFF@Pics@@0QBEB]
			
	align 16
		mode_scan_loop:
			AND bh, 7
			JNZ nextmodebit_loop

			LODSB
						
			DEC ebp
			JZ exit

		align 16
			nextmodebit_loop:				
				CMP bh, 8
				JAE mode_scan_loop

				ADD bx, 0101h

				TEST DWORD PTR [r9+24], 20000000h
				JNZ le2ri_1
					SHR al, 1
					JMP pkup_1
				le2ri_1:
					SHL al, 1
				pkup_1:
				
				RCL edx, 1

				MOVZX ecx, BYTE PTR [r13] ; count
				INC r13 ; count_pointer

				TEST cl, cl
				JZ nextmodebit_loop
					CMP cl, 0FFh

					JZ exit

					TEST dx, dx
					JZ nextmodebit_loop
						CMP dx, 1
						JNZ no_one
							CMP bl, 1
							JZ no_one
								CMP bl, 4
								JA nextmodebit_loop
									MOV dx, 1
									MOV cl, 6
									SUB cl, bl								
								
									SHL dx, cl
								
						no_one:

					CMP dx, 3
					JA no_vertical
						
						DEC bl
						SHR bl, 1

						SHR dl, 1
						JC no_neg
							NEG bl
						no_neg:

						MOVSX rdx, bl
										
						XCHG rax, r12
						MOV ecx, r14d

						MOV rdi, r10

						CMP rax, 0
						JL same_color

						CMP eax, [rdi]
						JZ same_color
							REPNE SCASD

							TEST ecx, ecx
							JZ enddoo

					same_color:
							REPE SCASD

							
						JNZ switch_found
						enddoo:
							DEC rcx
							ADD rdi, 4

							TEST rdx, rdx
							JNZ switch_found
								TEST al, al
								CMOVNZ eax, r12d

								JNZ end_of2Dline									
									
									MOV rdi, r8
									MOV ecx, r14d
									REP STOSD									

									MOV eax, r12d

								JMP end_of2Dline
					align 16
						switch_found:
							DEC rdx

							LEA r10, [rdi + rdx*4]
							XCHG r14d, ecx

							SUB r14d, edx

							JS exit

							SUB ecx, r14d
							JS exit
							JZ no_black

							TEST al, al
							JNZ no_black
								MOV rdi, r8
								REP STOSD

								MOV r8, rdi
							no_black:
								XCHG eax, r12d
								
								TEST r14d, r14d
								JZ end_of2Dline
									NOT r12d
									OR r12d, 0FF000000h
								
									LEA r8, [r8+rcx*4]
								JMP mode_selector


					align 16
						no_vertical:
							CMP dx, 4
							JNZ no_pass

								XCHG rax, r12
								MOV ecx, r14d

								MOV rdi, r10
								
								CMP rax, 0
								JL same_color_1
								
								CMP eax, [rdi]
								JZ same_color_1
									REPNE SCASD

									TEST ecx, ecx
								JZ enddoo_1

								same_color_1:
									REPE SCASD

									TEST ecx, ecx
									JZ enddoo_1

										REPNE SCASD
											
								enddoo_1:
									
									LEA r10, [rdi-4]
									XCHG r14d, ecx

									SUB ecx, r14d
									JS exit
							
									DEC ecx

									TEST al, al
									JNZ no_black_1
										MOV rdi, r8
										REP STOSD

										MOV r8, rdi
									no_black_1:
										
										XCHG eax, r12d
								
									TEST r14d, r14d
									JZ end_of2Dline
								

									INC r14d
									LEA r8, [r8+rcx*4]

								JMP mode_selector
	

					align 16
						no_pass:
							CMP dx, 8
							JNZ no_horizontal
								MOV [r9+8], r12d
																
								XOR rdx, rdx
								MOV bl, 0

						align 16
							horimode:

								TEST r12b, r12b
								JZ black_init
									LEA r13, [?white_counts@TIFF@Pics@@0QBEB]
									LEA rdi, [?white_codes@TIFF@Pics@@0QBGB]
									LEA r15, [?white_vals@TIFF@Pics@@0QBGB]
								JMP stream_scan_loop
								black_init:
									LEA r13, [?black_counts@TIFF@Pics@@0QBEB]
									LEA rdi, [?black_codes@TIFF@Pics@@0QBGB]
									LEA r15, [?black_vals@TIFF@Pics@@0QBGB]
												


							align 16
								stream_scan_loop:
									AND bh, 7
									JNZ nextbit_loop

										LODSB
						
										DEC ebp
										JZ exit

							align 16
								nextbit_loop:
									CMP bh, 8
									JAE stream_scan_loop

									ADD bx, 0101h

									TEST DWORD PTR [r9+24], 20000000h
									JNZ le2ri_2
										SHR al, 1
										JMP pkup_2
									le2ri_2:
										SHL al, 1
									pkup_2:

									RCL edx, 1

									MOVZX ecx, BYTE PTR [r13] ; count
									INC r13 ; count_pointer

									TEST cl, cl
								JZ nextbit_loop
									CMP cl, 0FFh
									JZ exit
										
									ADD ah, cl

									XCHG dx, ax
										REPNE SCASW
									XCHG dx, ax

								JNZ nextbit_loop						
									XOR rdx, rdx
						
										SUB ah, cl
									
										MOV cl, bl						
										SUB r13, rcx

										MOV cl, ah
										SHL rcx, 1
										SUB rdi, rcx

										MOVZX ecx, WORD PTR [r15+rcx-2] ; val
										
										ADD r11d, ecx ; 

										MOV ah, 0
										MOV bl, 0

										CMP ecx, 64
										JAE nextbit_loop
	
									SUB r14d, r11d

									JS exit ; error
				
									
									LEA r10, [r10+r11*4]
							
									TEST r12b, r12b
									JNZ skip_copy
									TEST r11d, r11d
									JZ skip_copy

								align 16
									bw_copy_loop:
										MOVNTI [r8], r12d
										ADD r8, 4
										DEC r11d
									JNZ bw_copy_loop
							
									skip_copy:
										LEA r8, [r8+r11*4]
										XOR r11, r11
						
		
									NOT r12d
									OR r12d, 0FF000000h

								CMP [r9+8], r12d
								JNZ horimode

							TEST r14d, r14d
							JZ end_of2Dline

						JMP mode_selector

						align 16
							no_horizontal:
								CMP dx, 15
								JNZ exit



		align 16
			end_of2Dline::
				MOV r10, [r9]

				XOR r12, r12
				NOT r12

				MOV r8d, [r9+16]
				MOV r14d, [r9+20]
				ADD r8, r10

				MOV [r9], r8

		DEC DWORD PTR [r9+12]
		JNZ line_start


exit:

	SFENCE

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	POP rdi
	POP rsi
	POP rbp


	RET
TIFF_T42DUnPack	ENDP


ALIGN 16
TIFF_T3UnPack	PROC
	PUSH rbp
	PUSH rsi
	PUSH rdi
	PUSH rbx
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15			

		MOV r8, rdx

		MOV rsi, [rcx] ; source pointer
		
		MOV ebp, [rcx+8]
		MOV r14d, [rcx+20] ; width
		
		MOV [rcx], rdx

		MOV r9, rcx			
		XOR rcx, rcx

	align 16
		T3_line_in:
			XOR rax, rax
			XOR rbx, rbx

			XOR r12, r12
			NOT r12
	
			XOR rdx, rdx
			XOR r11, r11 ; run length

			LEA r13, [?white_counts@TIFF@Pics@@0QBEB]
			LEA rdi, [?white_codes@TIFF@Pics@@0QBGB]
			LEA r15, [?white_vals@TIFF@Pics@@0QBGB]

	align 16
		stream_scan_loop:
			AND bh, 7
			JNZ nextbit_loop

			LODSB
						
			DEC ebp
			JZ exit

		align 16
			nextbit_loop:				
				CMP bh, 8
				JAE stream_scan_loop

				ADD bx, 0101h

				TEST DWORD PTR [r9+24], 20000000h
				JNZ le2ri_1
					SHR al, 1
					JMP pkup_1
				le2ri_1:
					SHL al, 1
				pkup_1:

				RCL edx, 1

				MOVZX ecx, BYTE PTR [r13] ; count
				INC r13 ; count_pointer

				TEST cl, cl
				JZ nextbit_loop
					CMP cl, 0FFh
					JZ exit
						ADD ah, cl

						XCHG dx, ax
							REPNE SCASW
						XCHG dx, ax

					JNZ nextbit_loop						
						XOR rdx, rdx
						
						SUB ah, cl
									
						MOV cl, bl						
						SUB r13, rcx

						MOV cl, ah
						SHL rcx, 1
						SUB rdi, rcx

						MOVZX ecx, WORD PTR [r15+rcx-2] ; val
						ADD r11d, ecx ; 

						MOV ah, 0
						MOV bl, 0

						CMP ecx, 64
						JAE nextbit_loop
	
			SUB r14d, r11d
			JS exit ; error
				
				TEST r12b, r12b
				JNZ skip_copy

			align 16
				bw_copy_loop:
					MOVNTI [r8], r12d
					ADD r8, 4
					DEC r11d
				JNZ bw_copy_loop
							
				skip_copy:
					LEA r8, [r8+r11*4]
					XOR r11, r11
						
			TEST r14d, r14d
			JZ next_line_set
				
				NOT r12d
				OR r12d, 0FF000000h

				TEST r12b, r12b
				JZ black_init
					LEA r13, [?white_counts@TIFF@Pics@@0QBEB]
					LEA rdi, [?white_codes@TIFF@Pics@@0QBGB]
					LEA r15, [?white_vals@TIFF@Pics@@0QBGB]
					JMP nextbit_loop

				black_init:
					LEA r13, [?black_counts@TIFF@Pics@@0QBEB]
					LEA rdi, [?black_codes@TIFF@Pics@@0QBGB]
					LEA r15, [?black_vals@TIFF@Pics@@0QBGB]
				
				JMP nextbit_loop	



		align 16
			next_line_set:

				MOV r8d, [r9+16]
				MOV r14d, [r9+20]

				ADD r8, [r9]
				MOV [r9], r8
				

		DEC DWORD PTR [r9+12]
		JNZ T3_line_in

exit:

	SFENCE

	MOV [r9], rsi
	MOV [r9+8], ebp

	SHR ebx, 8

	POP r15
	POP r14
	POP r13
	POP r12
	POP rbx
	POP rdi
	POP rsi
	POP rbp

	RET
TIFF_T3UnPack	ENDP






END

