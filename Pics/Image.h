/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include <Windows.h>
#include "CoreLibrary\SFSCore.h"
#include "System\Nucleus.h"

#include "Compression\Deflate.h"

#include "OpenGL\OpenGL.h"

namespace Pics {



	class ICC_profile {
		private:


		public:

	};

	class Image {
		public:
			struct StripDesc {
				const unsigned char * pointer;
				unsigned int size;
				unsigned int row_count;
				unsigned int row_length;
				unsigned int row_width;
				unsigned int flags;
		//		unsigned int state;
			};

			static const unsigned int size_paragraph = 4;
		protected:

			struct Properties {
				HANDLE f_source;

				unsigned int signature[8];
				float width, height, c_width, c_height, x_location, y_location;
				UI_64 image_count, next_index, interval, prev_index;
				unsigned char precision, norgba, direction, c_reserved;

				SFSco::Object im_obj;
				
				UI_64 _reserved[10];


				Properties & operator =(const Properties & ref_p);
				Properties(const Properties & ref_p);

				Properties();
				
				operator bool();

				void Blank();
				void Destroy();
			};

		private:
			static const UI_64 render_q_tid = 961749227;
			static const unsigned int decoding_queue_capacity = 1024;
			static const unsigned int encoding_queue_capacity = 32;
			static const unsigned int render_queue_size = 128;

			static __declspec(align(16)) short mag_filter[65536];

			static const wchar_t imgt_strings[][6];

			static SFSco::IOpenGL * main_ogl;
			static SFSco::Object decoding_queue[2];
			static Control::SimpleQueue prev_queue;

			static unsigned int eq_runner, eq_top, dq_runner[2], dq_top[2], current_de_queue, render_runner, render_top, slide_delay;
			static SFSco::Object en_queue, render_queue;

			static void InitRIMMTable();

			static UI_64 AddDREntry(OpenGL::DecodeRecord & new_rec);
			static UI_64 UpdateDREntry(OpenGL::DecodeRecord & new_rec);
			static UI_64 RemoveDREntry(UI_64 s_id);
			static UI_64 EnablePage(OpenGL::DecodeRecord & source_record);
			static UI_64 RefreshPage(OpenGL::DecodeRecord & source_record);

		protected:
			struct CodecSet {
				Control::Nucleus data_obj;
				SFSco::IOpenGL::ScreenSet pic_cfg;
				SFSco::Object name_obj;
			};

			static unsigned short RIMM_table[65536];			

			static HANDLE _de_thread, _en_thread, prev_thread, de_block;

			static UI_64 bend_of_line, current_glimage;
			static SFSco::List<Properties> pi_list;

			static DWORD __stdcall DecoderLoop(void *);
			static DWORD __stdcall PreviewLoop(void *);
			static DWORD __stdcall EncoderLoop(void *);


			static UI_64 Median(UI_64 *, const unsigned int *, unsigned int, unsigned int, unsigned int);
// conversions
			

			static void ToRGBA_1(unsigned char, unsigned char *, unsigned int, unsigned int);
			static void ToRGBA_2(unsigned char, unsigned short *, unsigned int, unsigned int);






		public:			

			enum LightSource: unsigned short {
				UnknownSource = 0,
				D5700 = 12,
				N4600 = 13,
				W3900 = 14,
				WW3200 = 15,

				lightA = 17,
				lightB = 18,
				lightC = 19,
				D55 = 20,
				D65 = 21,
				D75 = 22,
				D50 = 23


			};


			static const UI_64 temp_eobj_size = 0x00800000;
			static const UI_64 histogram_size = 0x00282000;

			static const UI_64 image_rstr_tid = 4022941009;

			static const UI_64 image_en_queue_tid = 4022940553;
			static const UI_64 image_de_queue_tid = 4022940583;

			static __declspec(align(16)) const float d50_RIMM[2][16], d50_sRGB[2][16], BFD_transform[2][16];
			static const unsigned int temperature_table[256];

			static SFSco::Object noname;
			static SFSco::IOpenGL::ScreenSet shot_set;
			static SFSco::Object ss_directory;


			enum ColorSpace : unsigned char {YCbCr = 1, Y, CMYK, YCCK, CMYJ, BW_special = 128};
			
						
			Image();
			virtual ~Image();
			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &) = 0;
			
			static UI_64 TexUpdate(OpenGL::Core *);

			static UI_64 Load(const wchar_t * f_name);
			static UI_64 Load(SFSco::Object & fname_obj, UI_64 = 0);
			static UI_64 Load(const void * src_ptr, unsigned int src_size);
			static UI_64 LoadRes(unsigned int rid, unsigned int rty, HMODULE mdl);

			static UI_64 Create(unsigned int, unsigned int, unsigned int = 0);

			static unsigned int Decode(const wchar_t *);
			static unsigned int Decode(const void *, unsigned int);

			static UI_64 Save(Control::Nucleus &, const SFSco::IOpenGL::ScreenSet & , SFSco::Object & = noname);
			static UI_64 Encode(Control::Nucleus &, const SFSco::IOpenGL::ScreenSet & , SFSco::Object & = noname);
			
			static UI_64 Decode(OpenGL::DecodeRecord &);
			
			static void GetWH(UI_64, float &, float &);
			static int OGLoad(UI_64);
			
			static unsigned int OGLBind(UI_64, float *, unsigned int tile_w, unsigned int tile_h);
						
			static UI_64 SaveAs(UI_64, SFSco::IOpenGL::ScreenSet &, SFSco::Object & = noname);

			static void Free(UI_64);

			static UI_64 Initialize(SFSco::IOpenGL **);
			static UI_64 Finalize();


			static float CCT(const float * neutral_xy);





			static unsigned int GetIMGTCount();
			static const wchar_t * GetIMGTString(unsigned int);
	};



	extern "C" {

		UI_64 Image_DeBayer_1RGGBu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_1GRBGu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_1GBRGu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_1BGGRu(Image::StripDesc &, unsigned char *);

		UI_64 Image_DeBayer_2RGGBu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_2GRBGu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_2GBRGu(Image::StripDesc &, unsigned char *);
		UI_64 Image_DeBayer_2BGGRu(Image::StripDesc &, unsigned char *);


		UI_64 Image_DeBayer_1RGGB(Image::StripDesc &);
		UI_64 Image_DeBayer_1GRBG(Image::StripDesc &);
		UI_64 Image_DeBayer_1GBRG(Image::StripDesc &);
		UI_64 Image_DeBayer_1BGGR(Image::StripDesc &);

		UI_64 Image_DeBayer_2RGGB(Image::StripDesc &);
		UI_64 Image_DeBayer_2GRBG(Image::StripDesc &);
		UI_64 Image_DeBayer_2GBRG(Image::StripDesc &);
		UI_64 Image_DeBayer_2BGGR(Image::StripDesc &);



		void Image_Convert2RGBA(unsigned int *, UI_64, unsigned int, unsigned int);

		unsigned int Image_YUY22RGBA_SSSE3(unsigned int *, const unsigned char *, const unsigned int *);
		unsigned int Image_I4202RGBA_SSSE3(unsigned int *, const unsigned char *, const unsigned int *);
		unsigned int Image_BGR2RGBA_SSSE3(unsigned int *, const unsigned char *, const unsigned int *);

		unsigned int Image_YUY22RGBA(unsigned int *, const unsigned char *, const unsigned int *);
		unsigned int Image_I4202RGBA(unsigned int *, const unsigned char *, const unsigned int *);
		unsigned int Image_BGR2RGBA(unsigned int *, const unsigned char *, const unsigned int *);

		unsigned int Image_RGBA2F420(unsigned int *, const unsigned int *, const unsigned int *);

		UI_64 Image_Remap_1(Image::StripDesc &, const unsigned short *, unsigned int, unsigned short *);
		UI_64 Image_Remap_2(Image::StripDesc &, const unsigned short *, unsigned int);

		UI_64 Image_Transform_1(Image::StripDesc &, float *);
		UI_64 Image_Transform_2(Image::StripDesc &, float *);

		void Image_FromScreen(void *, const void *, const SFSco::IOpenGL::ScreenSet &, unsigned int);

		void Image_YCC2RGB_1(unsigned char *, unsigned int, unsigned int);
		void Image_YCK2RGB_1(unsigned char *, unsigned int, unsigned int);
		void Image_CMY2RGB_1(unsigned char *, unsigned int, unsigned int);
		void Image_CMJ2RGB_1(unsigned char *, unsigned int, unsigned int);
		void Image_BW2YB_1(unsigned char *, unsigned int, unsigned int);
		
		void Image_YCC2RGB_2(unsigned short *, unsigned int, unsigned int);
		void Image_CMY2RGB_2(unsigned short *, unsigned int, unsigned int);
		void Image_BW2YB_2(unsigned short *, unsigned int, unsigned int);

		void Image_PaletteSwap(unsigned char *);

		UI_64 Image_Normalize_1(Image::StripDesc &, unsigned int *);
		UI_64 Image_Normalize_2(Image::StripDesc &, unsigned int *);

		void Image_Dither_SSE41(unsigned int *, const unsigned int *, unsigned int, unsigned int);
		void Image_Dither(unsigned int *, const unsigned int *, unsigned int, unsigned int);
		void Image_Dither_I(unsigned int *, const UI_64 *, unsigned int, unsigned int);
		void Image_Dither_I_SSSE3(unsigned int *, const UI_64 *, unsigned int, unsigned int);

		void Image_Nearest_SSE41(unsigned int *, const unsigned int *, unsigned int, unsigned int);
		void Image_Nearest(unsigned int *, const unsigned int *, unsigned int, unsigned int);
		void Image_Nearest_I(unsigned int *, UI_64 *, unsigned int, unsigned int);


		void Image_AddPicture0(unsigned int *, const unsigned int *, unsigned int, unsigned int *);
		void Image_AddPicture1(unsigned int *, const unsigned int *, unsigned int, unsigned int *);
		void Image_AddPicture2(unsigned int *, const unsigned int *, unsigned int, unsigned int *);
		void Image_AddPicture3(unsigned int *, const unsigned int *, unsigned int, unsigned int *);

		UI_64 Image_Remap_HSV1(Image::StripDesc &, const short *, const unsigned int *);
		UI_64 Image_Remap_HSV2(Image::StripDesc &, const short *, const unsigned int *);

		void Image_HSV_interpolate(float *, unsigned int, float);

		void Image_Build3DHistogram(UI_64 *, const unsigned int *, unsigned int, unsigned int);
		void Image_ConfigureVolumes(UI_64 *, UI_64, UI_64, const UI_64 *);

	}

}





