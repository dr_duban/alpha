/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include <Windows.h>

#include "Compression\LZW.h"

#include "System\SysUtils.h"

#include "Pics\GIF.h"




Pics::GIF::GIF() {
	DecoderReset();
	
	_decoder.first_image_id = _decoder.previous_image_id = -1;

	_cSet.delay = 0;
	_cSet.t_index = -1;
	_cSet.type = 0;
}

Pics::GIF::~GIF() {

}

void Pics::GIF::DecoderReset() {
	_decoder.line_pointer = 0;
	_decoder.piline_offset = 0;
	_decoder.cl_offset = -1;
	_decoder.line_position = 0;
	_decoder.i_pass = 0;
}

void Pics::GIF::SetInterlace(const ImageDesc * idesc) {

	interlace_set[0][0] = idesc->height;
	interlace_set[0][1] = 0;
	interlace_set[0][2] = c_width;

	if (idesc->interlace) {
		interlace_set[1][0] = (idesc->height >> 3) + ((idesc->height & 7)>0);
		interlace_set[1][1] = 0;
		interlace_set[1][2] = 8*c_width;

		interlace_set[2][0] = (idesc->height >> 3) + ((idesc->height & 7)>4);
		interlace_set[2][1] = 4*c_width;
		interlace_set[2][2] = 8*c_width;

		interlace_set[3][0] = (idesc->height >> 2) + ((idesc->height & 3)>2);
		interlace_set[3][1] = 2*c_width;
		interlace_set[3][2] = 4*c_width;

		interlace_set[4][0] = (idesc->height >> 1);
		interlace_set[4][1] = c_width;
		interlace_set[4][2] = 2*c_width;

		_decoder.i_pass = 1;
	}
}

UI_64 Pics::GIF::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	const unsigned char * img_ptr(0), * blopo(0);
	UI_64 result(0), col_count(1);
	unsigned int signature[8], signon(fsize), tval(0), image_count(0), * destpi_po(0);

	const ImageDesc * i_desc(0);
	Properties iprops, xprops;

	SFSco::Object pi_obj;
	SFSco::Object temp_obj;


	Pack::LZW lzwtor;

	imid = -1;

	if (fsize > 16384) signon = 16384;


	if (IsGIF(reinterpret_cast<const char *>(src_ptr), signon)) {		
		const ScreenDesc * scrdsc = reinterpret_cast<const ScreenDesc *>(src_ptr + 6);
		
		tval = 1024;
		if (fsize < 1024) tval = fsize;
		SFSco::sha256(signature, reinterpret_cast<const char *>(src_ptr), tval, 1);


		imid = pi_list.FindIt(signature, 32);
		if (imid != -1) {
			return 69;			
		}


		img_ptr = src_ptr + 6 + 7; // sizeof(ScreenDesc);
		fsize -= (6 + 7); // sizeof(ScreenDesc)

		signon = 1;

		col_count = 2 << scrdsc->bit_count;

		if (scrdsc->color_map) {
			if (fsize > col_count*3) {
				for (UI_64 i(0);i<col_count;i++) {
					palette[i] = 0xFF000000 | ((unsigned int)img_ptr[3*i]) | ((unsigned int)img_ptr[3*i+1] << 8) | ((unsigned int)img_ptr[3*i+2] << 16);
				}
				for (UI_64 i(col_count);i<256;i++) {
					palette[i] = 0xFFFFFFFF;
				}


				img_ptr += col_count*3;
				fsize -= col_count*3;
			} else {
				result = 26842753;
			}
		} else {
			
		}

		if (!result)
		for (;;) {
			switch (img_ptr[0]) {
				case 0x2C: // image block
					if (fsize > 9) { // sizeof(ImageDesc)
						i_desc = reinterpret_cast<const ImageDesc *>(img_ptr+1);
						col_count = 2 << i_desc->bit_count;

						c_width = (i_desc->width + (1<<size_paragraph) - 1) & (-1 << size_paragraph); c_height = (i_desc->height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);

						result = pi_obj.New(image_rstr_tid, c_width*(c_height+4)*4, SFSco::large_mem_mgr);

						if (result != -1) {

							result = 0;
							DecoderReset();

							if (image_count) {
								tval = 256 + i_desc->color_map*3*col_count;
								if (fsize < tval) tval = fsize;
								SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), tval, 0x80000001);
							}

							img_ptr += (9 + 1); // sizeof(ImageDesc)
							fsize -= (9 + 1); // sizeof(ImageDesc)

							SetInterlace(i_desc);

							if (i_desc->color_map) {
								if (fsize > col_count*3) {
									for (UI_64 i(0);i<col_count;i++) {
										palette[256+i] = 0xFF000000 | ((unsigned int)img_ptr[3*i]) | ((unsigned int)img_ptr[3*i+1] << 8) | ((unsigned int)img_ptr[3*i+2] << 16);
									}
									for (UI_64 i(256+col_count);i<512;i++) {
										palette[i] = 0xFFFFFFFF;
									}


									img_ptr += col_count*3;
									fsize -= col_count*3;
								} else {
									result = 369874123;
									break;
								}
							}

							if (lzwtor.Reset(img_ptr[0]) != -1) {
								img_ptr++;
								fsize--;

								for (;;) {
									if (img_ptr[0]) {
										if (fsize > img_ptr[0]) {

											lzwtor.SetUSource(img_ptr+1, img_ptr[0]);
										
											for (;;) {
												result = lzwtor.UnPack(temp_obj);

												if ((result > 0) && (result < -1024)) {
													if (blopo = reinterpret_cast<unsigned char *>(temp_obj.Acquire())) {
														if (unsigned int * pi_po = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {											
														// blopo - byte wide indices
															for (;;) {
																if (!_decoder.line_position) {
																	_decoder.line_position = i_desc->width;

																	if ((_decoder.i_pass>0) && (_decoder.i_pass<7)) {
																		if (interlace_set[_decoder.i_pass][0] == 0) _decoder.cl_offset = -1;
																		for (;((interlace_set[_decoder.i_pass][0] == 0) && (_decoder.i_pass<7));++_decoder.i_pass); 
															
																	}

																	if (interlace_set[_decoder.i_pass][0]) {
																		--interlace_set[_decoder.i_pass][0];
																	} else {
																		// error
																	}

																	_decoder.line_position = i_desc->width;
																	_decoder.line_pointer = interlace_set[_decoder.i_pass][1];															
																	_decoder.piline_offset = interlace_set[_decoder.i_pass][2];
														
													
																	if (_decoder.cl_offset != -1) _decoder.cl_offset += _decoder.piline_offset;
																	else _decoder.cl_offset = 0;

																}

																destpi_po = pi_po + _decoder.cl_offset + _decoder.line_pointer;

																for (;_decoder.line_position;) {

																	if (!i_desc->color_map) destpi_po[0] = palette[blopo[0]];
																	else destpi_po[0] = palette[256+blopo[0]];

																	if (blopo[0] == _cSet.t_index) {
																		destpi_po[0] = 0;
																	}
		
																	destpi_po++;
																	blopo++;															
																	_decoder.line_pointer++;

																	--_decoder.line_position;
																	if (--result == 0) break;
																}

																if (!result) break;
															}

															pi_obj.Release();
														}
														temp_obj.Release();
													}

													if (result = lzwtor.Success()) break;

												} else {
													break;
												}

											}


											fsize -= (img_ptr[0]+1);
											img_ptr += (img_ptr[0]+1);
										
										
										} else {
											result = 258741369;
											break;
										}

									} else {
										if (result = lzwtor.GetData(temp_obj)) {
											if (blopo = reinterpret_cast<unsigned char *>(temp_obj.Acquire())) {
												if (unsigned int * pi_po = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {											
												// blopo - byte wide indices
													for (;;) {
														if (!_decoder.line_position) {
															_decoder.line_position = i_desc->width;

															if ((_decoder.i_pass>0) && (_decoder.i_pass<7)) {
																if (interlace_set[_decoder.i_pass][0] == 0) _decoder.cl_offset = -1;
																for (;((interlace_set[_decoder.i_pass][0] == 0) && (_decoder.i_pass<7));++_decoder.i_pass); 
															
															}

															if (interlace_set[_decoder.i_pass][0]) {
																--interlace_set[_decoder.i_pass][0];
															} else {
																// error
															}

															_decoder.line_position = i_desc->width;
															_decoder.line_pointer = interlace_set[_decoder.i_pass][1];															
															_decoder.piline_offset = interlace_set[_decoder.i_pass][2];
														
													
															if (_decoder.cl_offset != -1) _decoder.cl_offset += _decoder.piline_offset;
															else _decoder.cl_offset = 0;

														}

														destpi_po = pi_po + _decoder.cl_offset + _decoder.line_pointer;

														for (;_decoder.line_position;) {

															if (!i_desc->color_map) destpi_po[0] = palette[blopo[0]];
															else destpi_po[0] = palette[256+blopo[0]];

															if (blopo[0] == _cSet.t_index) {
																destpi_po[0] = 0;
															}
		
															destpi_po++;
															blopo++;															
															_decoder.line_pointer++;

															--_decoder.line_position;
															if (--result == 0) break;
														}

														if (!result) break;
													}

													pi_obj.Release();
												}
												temp_obj.Release();
											}

										}

										img_ptr++;
										fsize--;

										break;
									}

									if (result < -128) {
									
										if (result & 16) {
											result = 0; // success											
										} else {
											if (result == 1) {
												result = 0; // source end
											} else {
												result = 0; // service
											}
										}
									
									} else {
										// discontinue with image (corrupted data stream)
										result = 65498701;
										break;
									}								
								}
							} else {
								result = -1;
							
							}

							if (!result) {								
								iprops.Blank();

								iprops.im_obj = pi_obj;
								
								for (unsigned int i(0);i<8;i++) iprops.signature[i] = signature[i];

								iprops.width = i_desc->width;
								iprops.height = i_desc->height;
								iprops.c_width = c_width;
								iprops.c_height = c_height;
								
								iprops.interval = _cSet.delay*100;
								
								iprops.x_location = i_desc->left;
								iprops.y_location = i_desc->top;

								if (scrdsc->bgrnd_index != _cSet.t_index) {
									if (!i_desc->color_map) {
										iprops._reserved[4] = palette[scrdsc->bgrnd_index];
										iprops._reserved[4] <<= 32;
										iprops._reserved[4] |= palette[scrdsc->bgrnd_index];
									} else {
										iprops._reserved[4] = palette[256+scrdsc->bgrnd_index];
										iprops._reserved[4] <<= 32;
										iprops._reserved[4] |= palette[256+scrdsc->bgrnd_index];
									}
								} else {
									iprops._reserved[4] = 0;
								}

								if (_decoder.first_image_id == -1) {
	
									if (_cSet.delay) {
										xprops = iprops;
										xprops.im_obj.Clone();
										
										iprops.prev_index = pi_list.Add(xprops);
									}

								}


								iprops.c_reserved = _cSet.type;								

								imid = pi_list.Add(iprops);

								if (_decoder.previous_image_id != -1) {
									iprops = pi_list[_decoder.previous_image_id];
									iprops.next_index = imid;
									pi_list.Set(_decoder.previous_image_id, iprops);
								}
								
								if (_decoder.first_image_id == -1) _decoder.first_image_id = imid;
								_decoder.previous_image_id = imid;

								image_count++;
								
							}


						}
					} else {
						result = 3597515;
					}

				break;
				case 0x21: // extention
					switch (img_ptr[1]) {
						case 0xF9: // control extention
							img_ptr += 2;
							fsize -= 2;

							if (fsize > img_ptr[0]) {
								if (img_ptr[1] & 1) {
									_cSet.t_index = img_ptr[4];
								}

								_cSet.type = (img_ptr[1] >> 2) & 7;
								_cSet.delay = reinterpret_cast<const unsigned short *>(img_ptr+2)[0];
								if (_cSet.delay == 0) _cSet.delay = 10;


								fsize -= (img_ptr[0]+1);
								img_ptr += (img_ptr[0]+1);									
							} else {
								result = 369514753;
							}

							img_ptr++;
							fsize--;

						break;
						default:
							img_ptr += 2;
							fsize -= 2;

							for (;img_ptr[0];) {
								if (fsize > img_ptr[0]) {


									fsize -= (img_ptr[0]+1);
									img_ptr += (img_ptr[0]+1);									
								} else {
									result = 369514753;
									break;
								}

							}

							img_ptr++;
							fsize--;
					}

						
				break;
				case 0x3B: // end of image
					result = 123;
				break;
				default:
					result = 9571530;
					// unrecognized marker
						
			}

			if (result) break;

		}
		
	}

	imid = _decoder.first_image_id;

	return result;
}



UI_64 Pics::GIF::IsGIF(const char * ipo, UI_64 lim) {
	UI_64 result(1);
	char smarker[] = {0x47, 0x49, 0x46, 0x38};
	
	for (unsigned int i(0);i<4;i++) result &= (ipo[i] == smarker[i]);

	return result;

}



