/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "Pics\Image.h"

namespace Pics {
	class Bitmap: public Image {
		friend class Icon;
		public:
			struct ReadConfig {
				void * first_line;
				const void * _rgba;
				int width, _mask[3];
				UI_64 l_offset;

			};


		private:
			struct FileHeader {				
				unsigned int size;
				short _reserved[2];
				unsigned int data_offset;
			};

			struct InfoHeader {
				unsigned int s_size;
				int width;
				int height;
				unsigned short planes;
				unsigned short bit_count;
				unsigned int compression;
				unsigned int image_size;
				int x_pels;
				int y_pels;
				unsigned int index_count;
				unsigned int index_range;
			};
			

			unsigned int c_width, c_height;


			static UI_64 Decode(const InfoHeader *, const unsigned char *, unsigned char *, unsigned int);

			
// encoder			
			static UI_64 Set16(unsigned int *, const unsigned int *, unsigned int, unsigned int);
			static UI_64 Set256(unsigned int *, const unsigned int *, unsigned int, unsigned int);

			static unsigned int FormatBWLine(unsigned char *, const unsigned int *, unsigned int);
			static unsigned int FormatXLine(unsigned char *, const unsigned int *, unsigned int);
			static unsigned int FormatXXLine(unsigned char *, const unsigned int *, unsigned int);
		public:			
			static UI_64 IsBMP(const char *, UI_64 limit);

			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);
			static UI_64 Save(unsigned int, void *, unsigned short, SFSco::Object &);

			Bitmap();
			virtual ~Bitmap();
	};


	class Icon: public Image {
		private:
			struct DirHeader {
				unsigned short _reserved;
				unsigned short r_type; // 1 - icon, 2 - cursor
				unsigned short entry_count;
			};

			struct EntryHeader {
				unsigned char width;
				unsigned char height;
				unsigned char color_count;
				unsigned char _reserved;
				
				unsigned short planes;
				unsigned short bit_count;

				unsigned int size;
				unsigned int offset; // from file start

			};

			unsigned int first_image_id, previous_image_id;
			unsigned int c_width, c_height;

			

		public:

			static UI_64 IsICO(const char *, UI_64 limit);

			virtual UI_64 Load(const unsigned char *, unsigned int, UI_64 &);
			


			Icon();
			virtual ~Icon();

	};


	extern "C" {
		UI_64 Bitmap_Read1(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);

		UI_64 Bitmap_Read4(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_Read8(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_Read16(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_Read24(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_Read32(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);

		UI_64 Bitmap_ReadRLE4(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_ReadRLE8(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);

		UI_64 Bitmap_Copy24(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_Copy32(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);

		UI_64 Bitmap_MaskRead16(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);
		UI_64 Bitmap_MaskRead32(const unsigned char *, const Bitmap::ReadConfig *, void *, unsigned int);

		void Bitmap_AlphaSet(const unsigned char *, unsigned char *, unsigned int, unsigned int);

	}
}
