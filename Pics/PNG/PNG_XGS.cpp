/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include <Windows.h>
#include "Pics\PNG.h"


const unsigned char * Pics::PNG::ReadX_0(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	unsigned char i_val(0), i_mask(1 << _header.bit_count), i_shift(0);
	
	i_mask--;

	pictpo += _decoder.cl_offset + _decoder.line_pointer;
	for (;_decoder.line_position;) {
		i_shift = 8;
		
		for (;i_shift;) {
			i_shift -= _header.bit_count;
			i_val = li_po[0] >> i_shift;
			i_val &= i_mask;			

			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[i_val];
			
			pictpo += _decoder.pi_offset;
			if (--_decoder.line_position == 0) break;
		}
	
		li_po++;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;

	return li_po;
}


const unsigned char * Pics::PNG::ReadX_80(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));

	if (unsigned char * prictpo = reinterpret_cast<unsigned char *>(p_line.Acquire())) {
		prictpo += (interlace_set[_decoder.i_pass][0] - _decoder.line_position);
		pictpo += _decoder.cl_offset + _decoder.line_pointer;

		for (;_decoder.line_position;) {
			prictpo[0] = li_po[0];
			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[li_po[0]];
		
			pictpo += _decoder.pi_offset;
			li_po++;
			prictpo++;

			--_decoder.line_position;
			if (--count == 0) break;
		}

		_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
		_decoder.line_pointer -= _decoder.cl_offset;

		p_line.Release();
	}

	return li_po;
}

const unsigned char * Pics::PNG::ReadX_81(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	
	if (unsigned char * prictpo = reinterpret_cast<unsigned char *>(p_line.Acquire())) {
		prictpo += (interlace_set[_decoder.i_pass][0] - _decoder.line_position);
		pictpo += _decoder.cl_offset + _decoder.line_pointer;

		for (;_decoder.line_position;) {
			_decoder.c_predictor += li_po[0];
			_decoder.c_predictor &= 0xFF;

			prictpo[0] = _decoder.c_predictor;
			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[_decoder.c_predictor];
		
			pictpo += _decoder.pi_offset;
			li_po++;
			prictpo++;

			--_decoder.line_position;
			if (--count == 0) break;
		}

		_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
		_decoder.line_pointer -= _decoder.cl_offset;

		p_line.Release();
	}

	return li_po;
}

const unsigned char * Pics::PNG::ReadX_82(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	unsigned char top_val;

	if (unsigned char * prictpo = reinterpret_cast<unsigned char *>(p_line.Acquire())) {
		prictpo += (interlace_set[_decoder.i_pass][0] - _decoder.line_position);
		pictpo += _decoder.cl_offset + _decoder.line_pointer;

		for (;_decoder.line_position;) {
			top_val = prictpo[0];
			top_val += li_po[0];
			
			prictpo[0] = top_val;
			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[top_val];
		
			pictpo += _decoder.pi_offset;
		
			prictpo++;
			li_po++;

			--_decoder.line_position;
			if (--count == 0) break;
		}

		_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
		_decoder.line_pointer -= _decoder.cl_offset;

		p_line.Release();
	}

	return li_po;
}

const unsigned char * Pics::PNG::ReadX_83(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	size_t top_val(0);

	if (unsigned char * prictpo = reinterpret_cast<unsigned char *>(p_line.Acquire())) {
		prictpo += (interlace_set[_decoder.i_pass][0] - _decoder.line_position);
		pictpo += _decoder.cl_offset + _decoder.line_pointer;
		
		for (;_decoder.line_position;) {
			top_val = prictpo[0];
			top_val += _decoder.c_predictor;
			top_val >>= 1;
		
			top_val += li_po[0];
			top_val &= 0xFF;

			prictpo[0] = top_val;
			_decoder.c_predictor = top_val;

			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[top_val];				

			pictpo += _decoder.pi_offset;
		
			prictpo++;
			li_po++;

			--_decoder.line_position;
			if (--count == 0) break;
		}

		_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
		_decoder.line_pointer -= _decoder.cl_offset;

		p_line.Release();
	}

	return li_po;
}

const unsigned char * Pics::PNG::ReadX_84(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	int pa(0), pb(0), pc(0);
	unsigned char x_val(0);

	if (unsigned char * prictpo = reinterpret_cast<unsigned char *>(p_line.Acquire())) {
		prictpo += (interlace_set[_decoder.i_pass][0] - _decoder.line_position);
		pictpo += _decoder.cl_offset + _decoder.line_pointer;

		for (;_decoder.line_position;) {
			x_val = (_decoder.c_predictor >> 32);
			pa = prictpo[0];
			pa -= x_val;
		
			pb = (_decoder.c_predictor & 0xFFFFFFFF);
			pb -= x_val;
		
			pc = pa + pb;
		
			if (pa < 0) pa = -pa;
			if (pb < 0) pb = -pb;
			if (pc < 0) pc = -pc;

			if ((pa <= pb) && (pa <= pc)) {
				x_val = (_decoder.c_predictor & 0xFFFFFFFF);
			} else {
				if (pb <= pc) {
					x_val = prictpo[0];
				}
			}

			x_val += li_po[0];
					
			_decoder.c_predictor = ((size_t)prictpo[0] << 32) | x_val;
			prictpo[0] = x_val;
			
			reinterpret_cast<unsigned int *>(pictpo)[0] = palette[x_val];			

			pictpo += _decoder.pi_offset;
		
			prictpo++;
			li_po++;

			--_decoder.line_position;
			if (--count == 0) break;
		}

		_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
		_decoder.line_pointer -= _decoder.cl_offset;

		p_line.Release();
	}
	return li_po;
}

// grey scale alpha ======================================================================================================================

const unsigned char * Pics::PNG::CopyGSA_0(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));

	pictpo += _decoder.cl_offset + _decoder.line_pointer;

	for (;_decoder.line_position;) {
		if (_decoder.c_index) {
			reinterpret_cast<unsigned int *>(pictpo)[0] = (palette[li_po[0]] & 0x00FFFFFF);
			
			_decoder.c_index ^= 1;

			li_po++;
			if (--count == 0) break;			
		}

		_decoder.c_index ^= 1;

		reinterpret_cast<unsigned int *>(pictpo)[0] |= ((unsigned int)li_po[0] << 24);

		li_po++;

		pictpo += _decoder.pi_offset;
		--_decoder.line_position;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;

	return li_po;
}

const unsigned char * Pics::PNG::CopyGSA_1(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po));
	unsigned char x_val(0);

	pictpo += _decoder.cl_offset + _decoder.line_pointer;

	for (;_decoder.line_position;) {
		if (_decoder.c_index) {
			x_val = _decoder.c_predictor & 0xFF;
			x_val += li_po[0];
		
			reinterpret_cast<unsigned int *>(pictpo)[0] = (palette[x_val] & 0x00FFFFFF);
		
			_decoder.c_predictor &= 0xFF00;
			_decoder.c_predictor |= x_val;
			
			_decoder.c_index ^= 1;
			li_po++;
		
			if (--count == 0) break;

		}

		_decoder.c_index ^= 1;

		x_val = (_decoder.c_predictor >> 8) & 0xFF;
		x_val += li_po[0];
		
		reinterpret_cast<unsigned int *>(pictpo)[0] |= ((unsigned int)x_val << 24);
		
		li_po++;

		_decoder.c_predictor &= 0x00FF;
		_decoder.c_predictor |= ((size_t)x_val << 8);
	
		pictpo += _decoder.pi_offset;

		--_decoder.line_position;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;


	return li_po;
}

const unsigned char * Pics::PNG::CopyGSA_2(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po)), * prictpo(0);
	unsigned char top_val;

	pictpo += _decoder.cl_offset + _decoder.line_pointer;
	prictpo = pictpo;

	if (_decoder.cl_offset) prictpo -= _decoder.piline_offset;

	for (;_decoder.line_position;) {
		if (_decoder.c_index) {
			top_val = prictpo[0];
			top_val += li_po[0];			
			
			reinterpret_cast<unsigned int *>(pictpo)[0] = (palette[top_val] & 0x00FFFFFF);
			
			li_po++;
			_decoder.c_index ^= 1;

			if (--count == 0) break;
		}

		_decoder.c_index ^= 1;

		top_val = prictpo[3];
		top_val += li_po[0];			
			
		reinterpret_cast<unsigned int *>(pictpo)[0] |= ((unsigned int)top_val << 24);
			
		li_po++;
		
		prictpo += _decoder.pi_offset;
		pictpo += _decoder.pi_offset;

		--_decoder.line_position;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;

	return li_po;
}

const unsigned char * Pics::PNG::CopyGSA_3(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po)), * prictpo(0);
	size_t top_val(0);

	pictpo += _decoder.cl_offset + _decoder.line_pointer;
	prictpo = pictpo;

	if (_decoder.cl_offset) prictpo -= _decoder.piline_offset;

	for (;_decoder.line_position;) {
		if (_decoder.c_index) {
			top_val = _decoder.c_predictor & 0xFF;
			top_val += prictpo[0];
		
			top_val >>= 1;
		
			top_val += li_po[0];
			top_val &= 0xFF;
			
			_decoder.c_predictor &= 0xFF00;
			_decoder.c_predictor |= top_val;

			reinterpret_cast<unsigned int *>(pictpo)[0] = (palette[top_val] & 0x00FFFFFF);
			
			li_po++;
			_decoder.c_index ^= 1;

			if (--count == 0) break;
		}

		_decoder.c_index ^= 1;

		top_val = (_decoder.c_predictor >> 8) & 0xFF;
		top_val += prictpo[3];
		
		top_val >>= 1;
		
		top_val += li_po[0];
		top_val &= 0xFF;
			
		_decoder.c_predictor &= 0x00FF;
		_decoder.c_predictor |= (top_val << 8);

		reinterpret_cast<unsigned int *>(pictpo)[0] |= (top_val << 24);

		pictpo += _decoder.pi_offset;
		prictpo += _decoder.pi_offset;
			
		li_po++;

		--_decoder.line_position;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;

	return li_po;
}

const unsigned char * Pics::PNG::CopyGSA_4(const unsigned char * li_po, unsigned int * pi_po, size_t & count) {
	unsigned char * pictpo(reinterpret_cast<unsigned char *>(pi_po)), * prictpo(0);
	int pa(0), pb(0), pc(0);
	unsigned int x_val(0);

	pictpo += _decoder.cl_offset + _decoder.line_pointer;
	prictpo = pictpo;

	if (_decoder.cl_offset) prictpo -= _decoder.piline_offset;

	for (;_decoder.line_position;) {
		if (_decoder.c_index) {
			x_val = ((_decoder.c_predictor >> 32) & 0xFF);
			pa = prictpo[0];
			pa -= x_val;
		
			pb = (_decoder.c_predictor & 0xFF);
			pb -= x_val;
		
			pc = pa + pb;
		
			if (pa < 0) pa = -pa;
			if (pb < 0) pb = -pb;
			if (pc < 0) pc = -pc;

			if ((pa <= pb) && (pa <= pc)) {
				x_val = (_decoder.c_predictor & 0xFF);
			} else {
				if (pb <= pc) {
					x_val = prictpo[0];
				}
			}

			x_val += li_po[0];
			x_val &= 0xFF;
		
			_decoder.c_predictor &= 0xFFFFFF00FFFFFF00;
			_decoder.c_predictor |= ((size_t)prictpo[0] << 32) | x_val;
			
			reinterpret_cast<unsigned int *>(pictpo)[0] = (palette[x_val] & 0x00FFFFFF);

			_decoder.c_index ^= 1;
			li_po++;

			if (--count == 0) break;
		}

		_decoder.c_index ^= 1;

		x_val = ((_decoder.c_predictor >> 40) & 0xFF);
		pa = prictpo[3];
		pa -= x_val;
		
		pb = ((_decoder.c_predictor >> 8) & 0xFF);
		pb -= x_val;
		
		pc = pa + pb;
		
		if (pa < 0) pa = -pa;
		if (pb < 0) pb = -pb;
		if (pc < 0) pc = -pc;

		if ((pa <= pb) && (pa <= pc)) {
			x_val = ((_decoder.c_predictor >> 8) & 0xFF);
		} else {
			if (pb <= pc) {
				x_val = prictpo[3];
			}
		}

		x_val += li_po[0];
		x_val &= 0xFF;
		
		_decoder.c_predictor &= 0xFFFF00FFFFFF00FF;
		_decoder.c_predictor |= ((size_t)prictpo[0] << 40) | (x_val << 8);
			
		reinterpret_cast<unsigned int *>(pictpo)[0] |= (x_val << 24);

		pictpo += _decoder.pi_offset;
		prictpo += _decoder.pi_offset;

		li_po++;

		--_decoder.line_position;
		if (--count == 0) break;
	}

	_decoder.line_pointer = static_cast<size_t>(pictpo - reinterpret_cast<unsigned char *>(pi_po));
	_decoder.line_pointer -= _decoder.cl_offset;

	return li_po;
}

