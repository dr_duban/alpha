/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>

#include "Pics\PNG.h"

#include "System\SysUtils.h"


const char Pics::PNG::smarker[] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00};
const unsigned char Pics::PNG::dt_map[] = {0, 2, 3, 0};
void Pics::PNG::DecoderReset() {
	_decoder.pi_color = 0;
	_decoder.c_predictor = 0;
	_decoder.t_color = 0;


	_decoder.line_pointer = 0;
	_decoder.piline_offset = 0;
	_decoder.pi_offset = 0;
	_decoder.cl_offset = -1;

	_decoder.chunk_num = 0;

	_decoder.line_position = 0;
	
	_decoder.c_index = _decoder.c_count = 0;
	_decoder.filter_type = -1;
	

}

Pics::PNG::PNG() {
	DecoderReset();

	_decoder.first_image_id = _decoder.previous_image_id = -1;
	_decoder.i_pass = 0;
	
}

Pics::PNG::~PNG() {
	if (p_line) p_line.Destroy();
}

void Pics::PNG::SetInterlace() {
	interlace_set[0][0] = _header.width; // width
	interlace_set[0][1] = _header.height; // height
	interlace_set[0][2] = 0; // start
	interlace_set[0][3] = _header.precision*4; // pixel offset
	interlace_set[0][4] = interlace_set[0][3]*_header.c_width; // line offset

	_decoder.i_pass = 0;

	if (_header.interlace == 1) {

		interlace_set[1][0] = (_header.width >> 3) + ((_header.width & 7)>0);
		interlace_set[1][1] = (_header.height >> 3) + ((_header.height & 7)>0);
		interlace_set[1][2] = 0;
		interlace_set[1][3] = 8*_header.precision*4;
		interlace_set[1][4] = interlace_set[1][3]*_header.c_width;
		

		interlace_set[2][0] = (_header.width >> 3) + ((_header.width & 7)>4);
		interlace_set[2][1] = interlace_set[1][1];
		interlace_set[2][2] = 4*_header.precision*4;
		interlace_set[2][3] = interlace_set[1][3];
		interlace_set[2][4] = interlace_set[1][4];


		interlace_set[3][0] = (_header.width >> 2) + ((_header.width & 3)>0);
		interlace_set[3][1] = (_header.height >> 3) + ((_header.height & 7)>4);
		interlace_set[3][2] = interlace_set[2][2]*_header.c_width;
		interlace_set[3][3] = interlace_set[2][2];
		interlace_set[3][4] = interlace_set[2][4];


		interlace_set[4][0] = (_header.width >> 2) + ((_header.width & 3)>2);
		interlace_set[4][1] = (_header.height >> 2) + ((_header.height & 3)>0);
		interlace_set[4][2] = 2*_header.precision*4;
		interlace_set[4][3] = interlace_set[3][3];
		interlace_set[4][4] = interlace_set[3][2];


		interlace_set[5][0] = (_header.width >> 1) + (_header.width & 1);
		interlace_set[5][1] = (_header.height >> 2) + ((_header.height & 3)>2);
		interlace_set[5][2] = interlace_set[4][2]*_header.c_width;
		interlace_set[5][3] = interlace_set[4][2];
		interlace_set[5][4] = interlace_set[4][4];


		interlace_set[6][0] = _header.width >> 1;
		interlace_set[6][1] = (_header.height >> 1) + (_header.height & 1);
		interlace_set[6][2] = _header.precision*4;
		interlace_set[6][3] = interlace_set[5][3];
		interlace_set[6][4] = interlace_set[5][2];


		interlace_set[7][0] = _header.width;
		interlace_set[7][1] = _header.height >> 1;
		interlace_set[7][2] = interlace_set[6][2]*_header.c_width;
		interlace_set[7][3] = interlace_set[6][2];
		interlace_set[7][4] = interlace_set[6][4];

		_decoder.i_pass = 1;
	}

}

void Pics::PNG::LoadHeader(const unsigned char * img_ptr) {
	_header.width = reinterpret_cast<const unsigned int *>(img_ptr)[0];
	_header.height = reinterpret_cast<const unsigned int *>(img_ptr)[1];

	System::BSWAP_32(_header.width);
	System::BSWAP_32(_header.height);

	_header.c_width = (_header.width + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
	_header.c_height = (_header.height + (1<<size_paragraph) -1) & (-1 << size_paragraph);
	_header.left = _header.top = 0;

	img_ptr += 8;

	_header.bit_count = img_ptr[0];
	_header.col_type = img_ptr[1];
	_header.compression = img_ptr[2];
	_header.filter = img_ptr[3];
	_header.interlace = img_ptr[4];

	_header.precision = (_header.bit_count + 7) >> 3;

	SetInterlace();

	if ((_header.col_type & 2) == 0) {
		unsigned int colcount = (1 << _header.bit_count) - 1;
		unsigned int c_val(0), c_step(256);

		if (_header.bit_count > 1) c_step >>= _header.bit_count;

		for (unsigned int i(0);i<colcount;i++) {
			palette[i] = 0xFF000000 | (c_val << 16) | (c_val << 8) | c_val;
			c_val += c_step;
		}

		palette[colcount] = 0xFFFFFFFF;
	}

}

UI_64 Pics::PNG::Load(const unsigned char * src_ptr, unsigned int fsize, UI_64 & imid) {
	const unsigned char * img_ptr(0), * blopo(0);
	UI_64 skipdi(0), result(0);
	unsigned int signature[8], signon(fsize), tval(0), image_count(0), current_image(-1), delay_val(0);
	unsigned short delay_den(0);
	unsigned char dispose_type(0);

	SFSco::Object pi_obj;
	SFSco::Object temp_obj;

	imid = -1;

	if (fsize > 16384) signon = 16384;

	if (IsPNG(reinterpret_cast<const char *>(src_ptr), signon)) {
		Pack::Deflate deflator;

		img_ptr = src_ptr + 8;
		fsize -= 8;

		signon = 1;
		
		if (deflator.Reset() != -1) {

			for (;;) {
				tval = reinterpret_cast<const unsigned int *>(img_ptr)[0];
				System::BSWAP_32(tval);						

				skipdi = tval + 12;

				if (signon & 1) {
					if (reinterpret_cast<const unsigned int *>(img_ptr)[1] != 'TADI') {
						SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), skipdi, signon);
						signon |= 0x80000000;
					} else {					
						tval = 1024;
						if (skipdi<1024) tval = skipdi;					
						SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), tval, signon);

						signon = 0;

						imid = pi_list.FindIt(signature, 32);
						if (imid != -1) {
							_decoder.previous_image_id = _decoder.first_image_id = imid;
							result = 69;
							break;
						} 

					}
				} else {
					if ((!signon) && (reinterpret_cast<const unsigned int *>(img_ptr)[1] == 'TAdf')) {
						tval = 1024;
						if (skipdi<1024) tval = skipdi;					

						SFSco::sha256(signature, reinterpret_cast<const char *>(img_ptr), tval, 0x80000001);
						signon = 2;
					}
				}

				if ((reinterpret_cast<const unsigned int *>(img_ptr)[1] != 'TADI') && (reinterpret_cast<const unsigned int *>(img_ptr)[1] != 'TAdf')) {
					if (current_image == image_count) {
						Properties iprops, xprops;

						iprops.im_obj = pi_obj;

						for (unsigned int i(0);i<8;i++) iprops.signature[i] = signature[i];

						iprops.width = _header.width;
						iprops.height = _header.height;
						iprops.c_width = _header.c_width;
						iprops.c_height = _header.c_height;

						iprops.x_location = _header.left;
						iprops.y_location = _header.top;
																		
						iprops.precision = _header.precision;
						iprops.interval = delay_val;

						if (_decoder.first_image_id != -1) {
							// clear tails
							if (unsigned int * pip_o = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
								for (unsigned int ih(0);ih<_header.height;ih++) {
									for (unsigned int iw(_header.width);iw<_header.c_width;iw++) pip_o[iw] = 0;

									pip_o += _header.c_width;
								}

								pi_obj.Release();
							}
							
						} else {
							if (delay_val) {
								xprops = iprops;
								xprops.im_obj.Clone();								

								iprops.prev_index = pi_list.Add(xprops);
							}
						}


						iprops.c_reserved = dispose_type;

						imid = pi_list.Add(iprops);

						if (_decoder.previous_image_id != -1) {
							iprops = pi_list[_decoder.previous_image_id];
							iprops.next_index = imid;

							pi_list.Set(_decoder.previous_image_id, iprops);
						}

						if (_decoder.first_image_id == -1) {

							_decoder.first_image_id = imid;
						}

						_decoder.previous_image_id = imid;

						DecoderReset();
						deflator.Reset();

						image_count++;
						signon = 0;
					}
				}

				switch (reinterpret_cast<const unsigned int *>(img_ptr)[1]) {
					case 'LTcf': // frame control
						_header.width = reinterpret_cast<const unsigned int *>(img_ptr)[3];
						_header.height = reinterpret_cast<const unsigned int *>(img_ptr)[4];

						System::BSWAP_32(_header.width);
						System::BSWAP_32(_header.height);

						_header.c_width = (_header.width + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
						_header.c_height = (_header.height + (1<<size_paragraph) - 1) & (-1 << size_paragraph);
						
						_header.left = reinterpret_cast<const unsigned int *>(img_ptr)[5];
						_header.top = reinterpret_cast<const unsigned int *>(img_ptr)[6];

						System::BSWAP_32(_header.left);
						System::BSWAP_32(_header.top);

						delay_den = reinterpret_cast<const unsigned short *>(img_ptr)[14];
						delay_val = System::BSWAP_16(delay_den);

						delay_den = reinterpret_cast<const unsigned short *>(img_ptr)[15];
						System::BSWAP_16(delay_den);
						
						if (!delay_val) delay_val = 1;
						if (!delay_den) delay_den = 100;
						delay_val = (delay_val*10000)/delay_den;

						if (_decoder.first_image_id != -1) {
							SetInterlace();

							result = pi_obj.New(image_rstr_tid, _header.c_width*(_header.c_height+4)*_header.precision*4, SFSco::large_mem_mgr);

							if (result != -1) {
								if (unsigned int * pi_po = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
									UI_64 fival = 0, hcount(8);

									if (_header.col_type == 2) {
										if (_header.precision == 1) fival = 0xFF000000FF000000;
										else fival = 0xFFFF000000000000;
									}

									if (_header.height<8) hcount = _header.height;

									System::MemoryFill_SSE3(pi_po, hcount*_header.c_width*_header.precision*4, fival, fival);
									pi_obj.Release();
								}

								result = 0;
							}
						}

						dispose_type = dt_map[img_ptr[32] & 3];

/*
   24    dispose_op            (byte)           Type of frame area disposal to be done after rendering this frame
   25    blend_op              (byte)           Type of frame area rendering for this frame 

*/

					break;

					case 'RDHI': // header
						LoadHeader(img_ptr+8);

						if ((_header.precision != 1) || (_header.interlace > 1)){
							result = 36479812;
							break;
						}

						if ((_header.col_type == 0) || (_header.col_type == 3)) {
							if (p_line.New(0, 2*_header.width) != -1) {
/*
								if (void * plpo = p_line.Acquire()) {
									System::MemoryFill_SSE3(plpo, 2*_header.width, 0, 0);
									p_line.Release();
								}
*/
							} else {
								result = 35689147;
								break;
							}
						}

						if (!pi_obj) {
							result = pi_obj.New(image_rstr_tid, _header.c_width*(_header.c_height+4)*_header.precision*4, SFSco::large_mem_mgr);
														
							if (result != -1) {
								if (unsigned int * pi_po = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {
									UI_64 fival = 0, hcount(8);

									if (_header.col_type == 2) {
										if (_header.precision == 1) fival = 0xFF000000FF000000;
										else fival = 0xFFFF000000000000;
									}

									if (_header.height<8) hcount = _header.height;

									System::MemoryFill_SSE3(pi_po, hcount*_header.c_width*_header.precision*4, fival, fival);
									pi_obj.Release();
								}

								result = 0;
							}
						}

					break;
					case 'sYHp': // physical pixel dimentions 01
					break;
					case 'EMIt': // last modification time 01
					break;
					case 'tXTz': // text 0N
					break;
					case 'tXEt': // string 0N
					break;
					case 'tXTi': // UTF string 0N
					break;
					case 'TLPs': // suggested palette 0N
					break;
					case 'PCCi': // embedded ICC profile 01
					break;
					case 'BGRs': // standard RGB profile 01
					break;
					case 'MRHc': // chromaticities and white point 01
					break;
					case 'AMAg': // image gamma 01
					break;
					case 'TIBs': // significant bits 01
					break;
					case 'ETLP': // palette
						if ((skipdi > 12) && (_header.col_type == 3)){
							UI_64 p_count = (skipdi-12)/3;
							if (p_count > 256) {
								result = 852671;
								break;
							}

							for (UI_64 i(0);i<p_count;i++) {
								palette[i] = 0xFF000000 | (unsigned int)img_ptr[3*i+8] | ((unsigned int)img_ptr[3*i+9] << 8) | ((unsigned int)img_ptr[3*i+10] << 16);
							}

						}

					break;
					case 'SNRt': // transperancy 01
						switch (_header.col_type) {
							case 0:
								if (_header.precision == 1) _decoder.t_color = 0xFF000000 | img_ptr[8];
								else _decoder.t_color = 0xFFFF000000000000 | reinterpret_cast<const unsigned short *>(img_ptr+8)[0];

							break;
							case 2:
								if (_header.precision == 1) _decoder.t_color = 0xFF000000 | img_ptr[8] | ((UI_64)img_ptr[10] << 8) | ((UI_64)img_ptr[12] << 16);
								else _decoder.t_color = 0xFFFF000000000000 | (reinterpret_cast<const UI_64 *>(img_ptr+8)[0] & 0x0000FFFFFFFFFFFF);

							break;
							case 3:
								{
									UI_64 p_count = (skipdi-12);

									if (p_count > 256) {
										result = 8526713;
										break;
									}

									for (UI_64 i(0);i<p_count;i++) {
										palette[i] &= 0x00FFFFFF;
										palette[i] |= ((unsigned int)img_ptr[i+8] << 24);
									}

								}
							break;
						}
					break;
					case 'TSIh': // image histogram 01
					break;
					case 'DGKb': // background color 01
						switch (_header.col_type) {
							case 3: // index

							break;
							case 0: case 4: // gray scale : 2 bytes

							break;
							case 2: case 6: // RGB : 2 bytes per cmp

							break;

						}
					break;
					
					
					case 'LTca': // animation control

					break;
	
					case 'TAdf': // frame data
						result = 12;
					case 'TADI': // image data
						if (skipdi > 12) {							
							if (result != 12) result = 8;

							current_image = image_count;

							if (_decoder.chunk_num++ == 0) {
								
								if ((img_ptr[result] & 0x0F) == 8) {									
									if (img_ptr[result+1] & 0x20) result += 4;
									result += 2;
								} else {
									result = -8;
								}
							}


							if (result != -8) {
								deflator.SetUSource(img_ptr + result, skipdi - result - 4);

								for (;;) {
									result = deflator.UnPack(temp_obj);
									if ((result > 0) && (result < -1024)) {
										if (blopo = reinterpret_cast<unsigned char *>(temp_obj.Acquire())) {
											if (unsigned int * pi_po = reinterpret_cast<unsigned int *>(pi_obj.Acquire())) {											

												for (;;) {
													if (_decoder.line_position == 0) {
														if (_decoder.c_count == _decoder.c_index) {
															_decoder.filter_type = blopo[0];

															blopo++;
															result--;
														} else {
															_decoder.filter_type = (_decoder.pi_color >> ((4 + _decoder.c_index - _decoder.c_count)*8)) & 0xFF;
															_decoder.c_index++;
														}

														_decoder.c_predictor = 0;

														if (_header.col_type == 2) {
															if (_header.precision == 1) _decoder.c_predictor = 0xFF000000FF000000;
															else _decoder.c_predictor = 0xFFFF000000000000;
														}

														if ((_decoder.i_pass>0) && (_decoder.i_pass<7)) {
															if (interlace_set[_decoder.i_pass][1] == 0) _decoder.cl_offset = -1;
															for (;(((interlace_set[_decoder.i_pass][1] == 0) || (interlace_set[_decoder.i_pass][0] == 0)) && (_decoder.i_pass<7));++_decoder.i_pass); 
															
														}

														if (interlace_set[_decoder.i_pass][1]) {
															--interlace_set[_decoder.i_pass][1];
														} else {
															// error
														}

														_decoder.line_position = interlace_set[_decoder.i_pass][0];
														_decoder.line_pointer = interlace_set[_decoder.i_pass][2];
														_decoder.pi_offset = interlace_set[_decoder.i_pass][3];
														_decoder.piline_offset = interlace_set[_decoder.i_pass][4];
														
													
														if (_decoder.cl_offset != -1) _decoder.cl_offset += _decoder.piline_offset;
														else _decoder.cl_offset = 0;

														if (_header.col_type == 4) _decoder.c_index = 1;
														
													}

													if (!result) break;

													if (result < _decoder.c_count) {
														_decoder.pi_color = 0;
														_decoder.c_index = _decoder.c_count - result;
														result = 4 - _decoder.c_count;
														for (unsigned char i(_decoder.c_index);i<_decoder.c_count;i++) _decoder.pi_color |= ((UI_64)blopo[i-_decoder.c_index] << ((i+result)*8));

														result = 0;
														break;
													}
																										

													switch (_header.col_type) {
														case 2: // true color
														case 6: // true color + alpha
															_decoder.c_count = 3 + (_header.col_type >> 2);
															blopo = ReadLine_TC(blopo, pi_po, result);
														break;

														case 0: // grey scale 1/2/4/8/16
														case 3: // indexed 1/2/4/8															
															blopo = ReadLine_X(blopo, pi_po, result);
														break;

														case 4: // grey + alpha 8/16															
															blopo = ReadLine_GSA(blopo, pi_po, result);
														break;
													}

													if (!result) break;
												}

												pi_obj.Release();
											}

											temp_obj.Release();																
										}

										if (result = deflator.Success()) break;

									} else {
										break;
									}

								}
																
								if (result < -128) {
									
									if (result == 16) {
										result = 0;
									} else {
										if (result == 1) {
											result = 0;
										} else {
											result = 0;
										}
									}

									
								} else {
									// discontinue with image (corrupted data stream)
									result = 65498701;
								}
							}
				
						}

					break;

					case 'DNEI': // end of image
						if (reinterpret_cast<const unsigned int *>(img_ptr)[0] == 0) {

							result = 123;						
						}

					break;
						
				}


				if (fsize < skipdi) {
					result = 65498701; // unexpected end of file / corrupted data	
					break;
				}

				img_ptr += skipdi;
				fsize -= skipdi;


				if (result) break;
		
			}

			temp_obj.Destroy();
		}

	}

	if (_decoder.previous_image_id != _decoder.first_image_id) {
		if (Image::Properties iprop = pi_list[_decoder.previous_image_id]) {
			iprop.next_index = _decoder.first_image_id;
			pi_list.Set(_decoder.previous_image_id, iprop);
		}
	}

	imid = _decoder.first_image_id;


	return result;
}



const unsigned char * Pics::PNG::ReadLine_TC(const unsigned char * li_po, unsigned int * pi_po, UI_64 & count) {
	if (_header.bit_count == 8) {
		switch (_decoder.filter_type) {
			case 1: // X - a				
				li_po = PNG_CopyColor_1(_decoder, li_po, pi_po, count);
			break;
			case 2: // X - b
				li_po = PNG_CopyColor_2(_decoder, li_po, pi_po, count);
			break;
			case 3: // (a + b) / 2				
				li_po = PNG_CopyColor_3(_decoder, li_po, pi_po, count);
			break;
			case 4: // Paeth
				li_po = SFSco::PNG_CopyColor_4(&_decoder, li_po, pi_po, count);
			break;
			default:
				li_po = PNG_CopyColor_0(_decoder, li_po, pi_po, count);
		}
	} else {
		if (_header.bit_count == 16) {
			// double precision

		}
	}

	return li_po;
}


const unsigned char * Pics::PNG::ReadLine_GSA(const unsigned char * li_po, unsigned int * pi_po, UI_64 & count) {
	if (_header.bit_count == 8) {
		switch (_decoder.filter_type) {
			case 1: // X - a				
				li_po = CopyGSA_1(li_po, pi_po, count);
			break;
			case 2: // X - b
				li_po = CopyGSA_2(li_po, pi_po, count);
			break;
			case 3: // (a + b) / 2				
				li_po = CopyGSA_3(li_po, pi_po, count);
			break;
			case 4: // Paeth
				li_po = CopyGSA_4(li_po, pi_po, count);
			break;
			default:
				li_po = CopyGSA_0(li_po, pi_po, count);
		}
	} else {
		if (_header.bit_count == 16) {
			// double precision

		}
	}

	_decoder.c_count = _decoder.c_index;

	return li_po;
}

const unsigned char * Pics::PNG::ReadLine_X(const unsigned char * li_po, unsigned int * pi_po, UI_64 & count) {

	if (_header.bit_count == 8) {
		switch (_decoder.filter_type) {
			case 1: // X - a				
				li_po = ReadX_81(li_po, pi_po, count);
			break;
			case 2: // X - b
				li_po = ReadX_82(li_po, pi_po, count);
			break;
			case 3: // (a + b) / 2				
				li_po = ReadX_83(li_po, pi_po, count);
			break;
			case 4: // Paeth
				li_po = ReadX_84(li_po, pi_po, count);
			break;
			default:
				li_po = ReadX_80(li_po, pi_po, count);
		}
	} else {
		if (_header.bit_count < 8) { // 1, 2, 4
			if (_decoder.filter_type == 0) li_po = ReadX_0(li_po, pi_po, count);

		} else {
			// grey scale double precision
		}
	}

	return li_po;
}


UI_64 Pics::PNG::IsPNG(const char * ipo, UI_64 lim) {
	UI_64 result(1);	
	
	for (unsigned int i(0);i<8;i++) result &= (ipo[i] == smarker[i]);

	return result;

}

