
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16
	one_mask		DWORD	0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh
	sign_mask		DWORD	080008000h, 080008000h, 080008000h, 080008000h


.CODE

; first point ignore

ALIGN 16
PNG_FilterLine_SSSE3	PROC
	PUSH rsi
	PUSH rdi
		
	PUSH r13
	PUSH r14

		PXOR xmm8, xmm8
		PXOR xmm9, xmm9
		PXOR xmm10, xmm10
		PXOR xmm11, xmm11
		PXOR xmm12, xmm12
		
		MOV r14, r9
		MOV r9, [r14]

		MOV r11d, r8d

		SUB rdx, rcx
		MOV rdi, rcx

		MOV rsi, rdx		

	
		ADD r11d, 7		
		SHR r11d, 3		
		MOV r10d, r11d
		SHL r11, 5		
		NEG r11
		MOV r13, r11
		ADD r11, rcx

		DEC r10d


		CMP r8d, 16
		JB zero_filter
			SHL r10d, 1

			MOV eax, r10d
			DEC eax

			MOVDQA xmm0, [rsi+rdi]
			MOVDQA xmm1, xmm0
			PSLLDQ xmm1, 4

			PSUBB xmm0, xmm1
			MOVDQA [rdi], xmm0

			ADD rdi, 16


		align 16	
			sub_filter_loop:
				MOVDQU xmm1, [rsi+rdi-4]
				MOVDQA xmm0, [rsi+rdi]				

				PSUBB xmm0, xmm1

				MOVDQA [rdi], xmm0


				MOVDQA xmm2, xmm0
				PUNPCKLBW xmm0, xmm12
				PUNPCKHBW xmm2, xmm12
				PADDW xmm0, xmm2
				MOVDQA xmm2, xmm0
				PUNPCKLWD xmm0, xmm12
				PUNPCKHWD xmm2, xmm12
				PADDD xmm0, xmm2

				PADDD xmm8, xmm0


				ADD rdi, 16
			DEC eax
			JNZ sub_filter_loop

			MOVDQU xmm1, [rsi+rdi-4]
			MOVDQU xmm3, [rsi+rdi+12]

			MOVDQA xmm0, [rsi+rdi]
			MOVDQA xmm2, [rsi+rdi+16]

			PSUBB xmm0, xmm1
			PSUBB xmm2, xmm3

			MOVDQA [rdi], xmm0			
			MOVDQA [rdi+16], xmm2

			ADD rdi, 32

			ADD r9, 1			
			JNZ analize_all
				ADC r9, 0

				SUB rdi, r13
				SUB rdi, r13
				SUB rdi, r13
			
				JMP first_line
			analize_all:

; up filter
			MOV eax, r10d
			ADD rdx, r13
			MOV rsi, rdx
			SUB r11, r13

		align 16
			up_filter_loop:
				MOVDQA xmm0, [rsi+rdi]

				PSUBB xmm0, [rsi+r11]

				MOVDQA [rdi], xmm0
				

				MOVDQA xmm2, xmm0
				PUNPCKLBW xmm0, xmm12
				PUNPCKHBW xmm2, xmm12
				PADDW xmm0, xmm2
				MOVDQA xmm2, xmm0
				PUNPCKLWD xmm0, xmm12
				PUNPCKHWD xmm2, xmm12
				PADDD xmm0, xmm2

				PADDD xmm9, xmm0
				
				ADD r11, 16
				ADD rdi, 16
			DEC eax
			JNZ up_filter_loop

			MOVDQA xmm0, [rsi+rdi]
			PSUBB xmm0, [rsi+r11]
			MOVDQA [rdi], xmm0

			ADD rdi, 16
			ADD r11, 16

			MOVDQA xmm0, [rsi+rdi]
			PSUBB xmm0, [rsi+r11]
			MOVDQA [rdi], xmm0

			ADD rdi, 16
			ADD r11, 16

; ave filter
			MOV eax, r10d
			DEC eax

			ADD rdx, r13
			MOV rsi, rdx

			MOVDQA xmm0, [rsi+rdi]
			MOVDQA xmm1, xmm0
			PSLLDQ xmm1, 4
			MOVDQA xmm3, xmm1
			MOVDQA xmm2, [rsi+r11]
			MOVDQA xmm4, xmm2
			PUNPCKLBW xmm1, xmm12
			PUNPCKHBW xmm3, xmm12			
			PUNPCKLBW xmm2, xmm12
			PUNPCKHBW xmm4, xmm12
			PADDW xmm1, xmm2
			PADDW xmm3, xmm4			
			PSRLW xmm1, 1
			PSRLW xmm3, 1
			PACKUSWB xmm1, xmm3
			PSUBB xmm0, xmm1
			
			MOVDQA [rdi], xmm0

			ADD rdi, 16
			ADD r11, 16
		align 16
			ave_filter_loop:
				MOVDQU xmm1, [rsi+rdi-4]
				MOVDQA xmm0, [rsi+rdi]				

				MOVDQA xmm3, xmm1

				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm4, xmm2

				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12
			
				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12

				PADDW xmm1, xmm2
				PADDW xmm3, xmm4
			
				PSRLW xmm1, 1
				PSRLW xmm3, 1

				PACKUSWB xmm1, xmm3

				PSUBB xmm0, xmm1				

				MOVDQA [rdi], xmm0

				MOVDQA xmm2, xmm0
				PUNPCKLBW xmm0, xmm12
				PUNPCKHBW xmm2, xmm12
				PADDW xmm0, xmm2
				MOVDQA xmm2, xmm0
				PUNPCKLWD xmm0, xmm12
				PUNPCKHWD xmm2, xmm12
				PADDD xmm0, xmm2

				PADDD xmm10, xmm0


				ADD rdi, 16
				ADD r11, 16
			DEC eax
			JNZ ave_filter_loop

				MOVDQU xmm1, [rsi+rdi-4]
				MOVDQA xmm0, [rsi+rdi]
				MOVDQA xmm3, xmm1
				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm4, xmm2
				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12			
				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12
				PADDW xmm1, xmm2
				PADDW xmm3, xmm4			
				PSRLW xmm1, 1
				PSRLW xmm3, 1
				PACKUSWB xmm1, xmm3
				PSUBB xmm0, xmm1				
				MOVDQA [rdi], xmm0

				ADD rdi, 16
				ADD r11, 16

				MOVDQU xmm1, [rsi+rdi-4]
				MOVDQA xmm0, [rsi+rdi]
				MOVDQA xmm3, xmm1
				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm4, xmm2
				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12			
				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12
				PADDW xmm1, xmm2
				PADDW xmm3, xmm4			
				PSRLW xmm1, 1
				PSRLW xmm3, 1
				PACKUSWB xmm1, xmm3
				PSUBB xmm0, xmm1				
				MOVDQA [rdi], xmm0

				ADD rdi, 16
				ADD r11, 16
; pth filter
			MOV eax, r10d
			DEC eax

			ADD rdx, r13
			MOV rsi, rdx
								
				MOVDQA xmm0, [rsi+rdi]
				MOVDQA xmm1, xmm0
				PSLLDQ xmm1, 4
				
				MOVDQA xmm3, xmm1
				MOVDQA xmm13, xmm1

				

				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm5, xmm2
				PSLLDQ xmm5, 4

				MOVDQA xmm4, xmm2
				MOVDQA xmm14, xmm2

				MOVDQA xmm6, xmm5
				MOVDQA xmm15, xmm5



				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12

				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12

				PUNPCKLBW xmm5, xmm12
				PUNPCKHBW xmm6, xmm12

				PSUBW xmm2, xmm5 ; pa
				PSUBW xmm4, xmm6

				PSUBW xmm1, xmm5 ; pb
				PSUBW xmm3, xmm6

				MOVDQA xmm5, xmm1
				MOVDQA xmm6, xmm3

				PABSW xmm1, xmm1
				PABSW xmm3, xmm3
				
				PADDW xmm5, xmm2 ; pc
				PADDW xmm6, xmm4

				PABSW xmm2, xmm2
				PABSW xmm4, xmm4
				PABSW xmm5, xmm5
				PABSW xmm6, xmm6

			
				MOVDQA xmm7, xmm2

				PCMPGTW xmm2, xmm1
				PCMPGTW xmm7, xmm5
				POR xmm2, xmm7
				
				MOVDQA xmm7, xmm4

				PCMPGTW xmm4, xmm3
				PCMPGTW xmm7, xmm6
				POR xmm4, xmm7

				PCMPGTW xmm1, xmm5
				PCMPGTW xmm3, xmm6						

				MOVDQA xmm7, XMMWORD PTR [one_mask]
				
				PSRLW xmm1, 1
				PSRLW xmm3, 1

				PSRLW xmm2, 1
				PSRLW xmm4, 1

				PACKUSWB xmm2, xmm4
				PACKUSWB xmm1, xmm3
								
				
				PAND xmm15, xmm1 ; c
				PXOR xmm1, xmm7
				PAND xmm14, xmm1 ; b

				PAND xmm14, xmm2
				PAND xmm15, xmm2

				PXOR xmm2, xmm7
				POR xmm14, xmm15

				PAND xmm13, xmm2 ; a
				POR xmm13, xmm14


				PSUBB xmm0, xmm13				

				MOVDQA [rdi], xmm0

				ADD rdi, 16
				ADD r11, 16

		align 16
			pth_filter_loop:
				MOVDQU xmm1, [rsi+rdi-4] ; a
				MOVDQA xmm0, [rsi+rdi] ; x
				
				MOVDQA xmm3, xmm1
				MOVDQA xmm13, xmm1

				MOVDQU xmm5, [rsi+r11-4] ; c
				MOVDQA xmm6, xmm5
				MOVDQA xmm15, xmm5

				MOVDQA xmm2, [rsi+r11] ; b
				MOVDQA xmm4, xmm2
				MOVDQA xmm14, xmm2


				PUNPCKLBW xmm1, xmm12 ; a
				PUNPCKHBW xmm3, xmm12

				PUNPCKLBW xmm2, xmm12 ; b
				PUNPCKHBW xmm4, xmm12

				PUNPCKLBW xmm5, xmm12 ; c
				PUNPCKHBW xmm6, xmm12

				PSUBW xmm2, xmm5 ; pa
				PSUBW xmm4, xmm6

				PSUBW xmm1, xmm5 ; pb
				PSUBW xmm3, xmm6

				MOVDQA xmm5, xmm1
				MOVDQA xmm6, xmm3

				PABSW xmm1, xmm1
				PABSW xmm3, xmm3
				
				PADDW xmm5, xmm2 ; pc
				PADDW xmm6, xmm4

				PABSW xmm2, xmm2
				PABSW xmm4, xmm4
				PABSW xmm5, xmm5
				PABSW xmm6, xmm6

			
				MOVDQA xmm7, xmm2

				PCMPGTW xmm2, xmm1
				PCMPGTW xmm7, xmm5
				POR xmm2, xmm7
				
				MOVDQA xmm7, xmm4

				PCMPGTW xmm4, xmm3
				PCMPGTW xmm7, xmm6
				POR xmm4, xmm7

				PCMPGTW xmm1, xmm5
				PCMPGTW xmm3, xmm6						

				MOVDQA xmm7, XMMWORD PTR [one_mask]
				
				PSRLW xmm1, 1
				PSRLW xmm3, 1

				PSRLW xmm2, 1
				PSRLW xmm4, 1

				PACKUSWB xmm2, xmm4
				PACKUSWB xmm1, xmm3
								
				
				PAND xmm15, xmm1 ; c
				PXOR xmm1, xmm7
				PAND xmm14, xmm1 ; b

				PAND xmm14, xmm2
				PAND xmm15, xmm2

				PXOR xmm2, xmm7
				POR xmm14, xmm15

				PAND xmm13, xmm2 ; a
				POR xmm13, xmm14

				PSUBB xmm0, xmm13				

				MOVDQA [rdi], xmm0

				MOVDQA xmm2, xmm0
				PUNPCKLBW xmm0, xmm12
				PUNPCKHBW xmm2, xmm12
				PADDW xmm0, xmm2
				MOVDQA xmm2, xmm0
				PUNPCKLWD xmm0, xmm12
				PUNPCKHWD xmm2, xmm12
				PADDD xmm0, xmm2

				PADDD xmm11, xmm0


				ADD rdi, 16
				ADD r11, 16
			DEC eax
			JNZ pth_filter_loop

				MOVDQU xmm1, [rsi+rdi-4]				
				MOVDQA xmm0, [rsi+rdi]			
				MOVDQA xmm3, xmm1
				MOVDQA xmm13, xmm1
				MOVDQU xmm5, [rsi+r11-4]
				MOVDQA xmm6, xmm5
				MOVDQA xmm15, xmm5
				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm4, xmm2
				MOVDQA xmm14, xmm2
				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12
				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12
				PUNPCKLBW xmm5, xmm12
				PUNPCKHBW xmm6, xmm12
				PSUBW xmm2, xmm5 ; pa
				PSUBW xmm4, xmm6
				PSUBW xmm1, xmm5 ; pb
				PSUBW xmm3, xmm6
				MOVDQA xmm5, xmm1
				MOVDQA xmm6, xmm3
				PABSW xmm1, xmm1
				PABSW xmm3, xmm3				
				PADDW xmm5, xmm2 ; pc
				PADDW xmm6, xmm4
				PABSW xmm2, xmm2
				PABSW xmm4, xmm4
				PABSW xmm5, xmm5
				PABSW xmm6, xmm6			
				MOVDQA xmm7, xmm2
				PCMPGTW xmm2, xmm1
				PCMPGTW xmm7, xmm5
				POR xmm2, xmm7				
				MOVDQA xmm7, xmm4
				PCMPGTW xmm4, xmm3
				PCMPGTW xmm7, xmm6
				POR xmm4, xmm7
				PCMPGTW xmm1, xmm5
				PCMPGTW xmm3, xmm6
				MOVDQA xmm7, XMMWORD PTR [one_mask]	
				PSRLW xmm1, 1
				PSRLW xmm3, 1
				PSRLW xmm2, 1
				PSRLW xmm4, 1
				PACKUSWB xmm2, xmm4
				PACKUSWB xmm1, xmm3				
				PAND xmm15, xmm1 ; c
				PXOR xmm1, xmm7
				PAND xmm14, xmm1 ; b
				PAND xmm14, xmm2
				PAND xmm15, xmm2
				PXOR xmm2, xmm7
				POR xmm14, xmm15
				PAND xmm13, xmm2 ; a
				POR xmm13, xmm14
				PSUBB xmm0, xmm13				
				MOVDQA [rdi], xmm0

				ADD rdi, 16
				ADD r11, 16

				MOVDQU xmm1, [rsi+rdi-4]				
				MOVDQA xmm0, [rsi+rdi]				
				MOVDQA xmm3, xmm1
				MOVDQA xmm13, xmm1
				MOVDQU xmm5, [rsi+r11-4]
				MOVDQA xmm6, xmm5
				MOVDQA xmm15, xmm5
				MOVDQA xmm2, [rsi+r11]
				MOVDQA xmm4, xmm2
				MOVDQA xmm14, xmm2
				PUNPCKLBW xmm1, xmm12
				PUNPCKHBW xmm3, xmm12
				PUNPCKLBW xmm2, xmm12
				PUNPCKHBW xmm4, xmm12
				PUNPCKLBW xmm5, xmm12
				PUNPCKHBW xmm6, xmm12
				PSUBW xmm2, xmm5 ; pa
				PSUBW xmm4, xmm6
				PSUBW xmm1, xmm5 ; pb
				PSUBW xmm3, xmm6
				MOVDQA xmm5, xmm1
				MOVDQA xmm6, xmm3
				PABSW xmm1, xmm1
				PABSW xmm3, xmm3				
				PADDW xmm5, xmm2 ; pc
				PADDW xmm6, xmm4
				PABSW xmm2, xmm2
				PABSW xmm4, xmm4
				PABSW xmm5, xmm5
				PABSW xmm6, xmm6			
				MOVDQA xmm7, xmm2
				PCMPGTW xmm2, xmm1
				PCMPGTW xmm7, xmm5
				POR xmm2, xmm7				
				MOVDQA xmm7, xmm4
				PCMPGTW xmm4, xmm3
				PCMPGTW xmm7, xmm6
				POR xmm4, xmm7
				PCMPGTW xmm1, xmm5
				PCMPGTW xmm3, xmm6
				MOVDQA xmm7, XMMWORD PTR [one_mask]
				PSRLW xmm1, 1
				PSRLW xmm3, 1
				PSRLW xmm2, 1
				PSRLW xmm4, 1
				PACKUSWB xmm2, xmm4
				PACKUSWB xmm1, xmm3				
				PAND xmm15, xmm1 ; c
				PXOR xmm1, xmm7
				PAND xmm14, xmm1 ; b
				PAND xmm14, xmm2
				PAND xmm15, xmm2
				PXOR xmm2, xmm7
				POR xmm14, xmm15
				PAND xmm13, xmm2 ; a
				POR xmm13, xmm14
				PSUBB xmm0, xmm13				
				MOVDQA [rdi], xmm0

				ADD rdi, 16
				ADD r11, 16

			PHADDD xmm8, xmm9
			PHADDD xmm10, xmm11
			PHADDD xmm8, xmm10
			
			MOVDQA xmm10, xmm8
			
			MOVD rax, xmm10
			PSRLDQ xmm10, 8
			MOVD rdx, xmm10
			
			MOV r11d, eax
			SHR rax, 32

			MOV r10d, edx
			SHR rdx, 32
			
			CMP eax, r11d
			CMOVA eax, r11d

			CMP edx, r10d
			CMOVA edx, r10d

			CMP eax, edx
			CMOVA eax, edx

			MOVD xmm10, eax
			PSHUFD xmm10, xmm10, 0

			PCMPEQD xmm10, xmm8
			PMOVMSKB eax, xmm10

			BSF eax, eax
			SHR eax, 2

	first_line:
			INC eax

			MOV rsi, rdi			

			ADD rdi, r9
			MOV [rdi-1], al

			rsi_adjust:
				ADD rsi, r13
				INC al
				CMP al, 4
			JBE rsi_adjust
		
		JMP exit
align 16
	zero_filter:
		XOR rax, rax
		ADD rsi, rcx
		
		ADD r9, 1
		ADC r9, 0

		NEG r13

		ADD rdi, r9
		LEA rdi, [rdi+r13*4]
		MOV [rdi-1], al

exit:
	
	LEA r9, [r9+r8*4]
	SUB rsi, rdi

align 16
	copy_loop:
		MOV eax, [rsi+rdi]
		MOVNTI [rdi], eax
		ADD rdi, 4
	DEC r8d
	JNZ copy_loop

	MOV [r14], r9

	POP r14
	POP r13

	POP rdi
	POP rsi

	RET
PNG_FilterLine_SSSE3	ENDP

ALIGN 16
PNG_FilterLine_SSE3	PROC

	RET
PNG_FilterLine_SSE3	ENDP

END

