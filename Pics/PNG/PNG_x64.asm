
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


chunk_num		TEXTEQU <72>
line_position	TEXTEQU <76>
filter_type		TEXTEQU <80>
c_index			TEXTEQU <81>
c_count			TEXTEQU <82>
i_pass			TEXTEQU <83>
x_res			TEXTEQU <84>

.CONST

ALIGN 16
	one_mask		DWORD	0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh
	sign_mask		DWORD	080008000h, 080008000h, 080008000h, 080008000h
	reserved_line	DWORD	0, 0, 0, 0
	zero_line		DWORD	0, 0, 0, 0
.CODE


ALIGN 16
PNG_CopyColor_0	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15


		MOV r13, rcx

		MOV eax, [r13]

		MOV r15, [r13+16]

		ADD r8, [r13+24] ; cl_line
	
	PUSH r8

		ADD r8, [r13+32] ; line pointer
		MOV r11, [r13+40] ; pi_offset
		
		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
		XOR rcx, rcx
	
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx

		ADD r12, rdx
		SUB r12, rcx

	align 16
		scan_line_loop:			
				LODSB		
				ROR eax, 8
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			SUB cl, 4
			JZ set_pixel
				MOV al, cl
				ROR eax, 8
			set_pixel:

				MOV cl, dl

				MOV r14d, r15d
				XOR r14d, eax
				CMOVZ eax, r14d

				MOVNTI [r8], eax
				ADD r8, r11

				DEC r10d
							
				SUB r12, rdx
				JZ exit
				CMP r12, rdx				
				JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:

		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB
			ROR eax, 8
		DEC cl
		JNZ last_copy_loop
							
		MOV cl, dl
		XOR r12, r12

	
exit:
		MOV [r13], eax

	POP r11
	SUB r8, r11
		MOV [r13+32], r8

		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12	

	POP r15
	POP r14
	POP r13
	POP r12
	POP rsi

	RET
PNG_CopyColor_0	ENDP


ALIGN 16
PNG_CopyColor_1	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH rbx

		MOV r13, rcx
	
		MOV eax, [r13]
		MOVD xmm0, DWORD PTR [r13+8] ; predictor
		MOVD xmm3, DWORD PTR [r13+16] ; t_color	
	
		ADD r8, [r13+24] ; line offset
	PUSH r8
		ADD r8, [r13+32] ; line pointer

		MOV r11, [r13+40] ; pi offset
		
		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
		XOR rcx, rcx
	
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx
		
		ADD r12, rdx
		SUB r12, rcx


		PXOR xmm7, xmm7
		MOVDQA xmm4, XMMWORD PTR [one_mask]
	

	align 16
		scan_line_loop:			
				LODSB
				ROR eax, 8
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			AND cl, 4
			JNZ set_pixel
				MOV al, cl
				ROR eax, 8				
			set_pixel:

				MOV cl, dl

				MOVD xmm2, eax
				PADDB xmm0, xmm2

				MOVDQA xmm1, xmm0				
				PXOR xmm1, xmm3				
				PCMPEQD xmm1, xmm7
				PXOR xmm1, xmm4
				PAND xmm1, xmm0
											
				MOVD DWORD PTR [r8], xmm1

				ADD r8, r11				
				DEC r10d
							
				SUB r12, rdx
				JZ exit
				CMP r12, rdx
				JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:
		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB
			ROR eax, 8
		DEC cl
		JNZ last_copy_loop
							
		MOV cl, dl
		XOR r12, r12
				
	
exit:

		MOV [r13], eax
		MOVD DWORD PTR [r13+8], xmm0

	POP r11
		SUB r8, r11
		MOV [r13+32], r8

		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12
	
	POP rbx
	POP r13
	POP r12
	POP rsi


	RET
PNG_CopyColor_1	ENDP


ALIGN 16
PNG_CopyColor_2	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH rbx

		MOV r13, rcx
		XOR rcx, rcx

		MOV eax, [r13]
		MOVD xmm3, DWORD PTR [r13+16] ; t_color

		ADD r8, [r13+24] ; line offset
	PUSH r8
		ADD r8, [r13+32] ; line pointer

		MOV rbx, r8 ; previous line
		MOV r11, [r13+40] ; pi offset

		CMP [r13+24], rcx
		JZ fline
			SUB rbx, [r13+48]
		fline:
				
		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
	
	
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx

		ADD r12, rdx
		SUB r12, rcx


		PXOR xmm7, xmm7
		MOVDQA xmm4, XMMWORD PTR [one_mask]

	align 16
		scan_line_loop:			
				LODSB
				ROR eax, 8				
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			AND cl, 4
			JNZ set_pixel
				MOV al, cl
				ROR eax, 8				
			set_pixel:

				MOV cl, dl

			MOVD xmm0, eax
			MOVD xmm1, DWORD PTR [rbx]
			PADDB xmm0, xmm1

			MOVDQA xmm1, xmm0				
			PXOR xmm1, xmm3
			PCMPEQD xmm1, xmm7
			PXOR xmm1, xmm4
			PAND xmm1, xmm0
											
			MOVD DWORD PTR [r8], xmm1
				
			ADD r8, r11
			ADD rbx, r11

			DEC r10d
					
			SUB r12, rdx
			JZ exit
			CMP r12, rdx
			JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:

		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB				
			ROR eax, 8					
		DEC cl
		JNZ last_copy_loop
								
		MOV cl, dl
		XOR r12, r12

	
exit:
		MOV [r13], eax
	POP r11
		SUB r8, r11
		MOV [r13+32], r8
		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12
	
	POP rbx
	POP r13
	POP r12
	POP rsi


	RET
PNG_CopyColor_2	ENDP

ALIGN 16
PNG_CopyColor_3	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH r15

		MOV r13, rcx
		XOR rcx, rcx
		
		MOV eax, [r13]
		MOVD xmm0, DWORD PTR [r13+8] ; predictor
		MOVD xmm3, DWORD PTR [r13+16] ; t_color	
	
		ADD r8, [r13+24]
	PUSH r8
		ADD r8, [r13+32]
		MOV r15, r8 ; previous line

		MOV r11, [r13+40]

		CMP [r13+24], rcx
		JZ fline
			SUB r15, [r13+48]
		fline:


		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
	
	
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx

		ADD r12, rdx
		SUB r12, rcx

		PXOR xmm7, xmm7
		MOVDQA xmm4, XMMWORD PTR [one_mask]
	
	align 16
		scan_line_loop:
				LODSB
				ROR eax, 8
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			AND cl, 4
			JNZ set_pixel
				MOV al, cl
				ROR eax, 8
			set_pixel:

				MOV cl, dl
			
			MOVD xmm1, DWORD PTR [r15]
			PUNPCKLBW xmm1, xmm7
			PUNPCKLBW xmm0, xmm7

			PADDW xmm0, xmm1
			PSRLW xmm0, 1
			PACKUSWB xmm0, xmm0

			MOVD xmm2, eax
			PADDB xmm0, xmm2		
				
			MOVDQA xmm1, xmm0
			PXOR xmm1, xmm3
			PCMPEQD xmm1, xmm7
			PXOR xmm1, xmm4
			PAND xmm1, xmm0
				
			MOVD DWORD PTR [r8], xmm1

			ADD r8, r11
			ADD r15, r11

			DEC r10d

			SUB r12, rdx
			JZ exit
			CMP r12, rdx			
			JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:

		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB
			ROR eax, 8
		DEC cl
		JNZ last_copy_loop
											
		MOV cl, dl
		XOR r12, r12
	
	
exit:
		MOV [r13], eax
		MOVD DWORD PTR [r13+8], xmm0

	POP r11
		SUB r8, r11
		MOV [r13+32], r8

		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12
	
	POP r15

	POP r13
	POP r12
	POP rsi

	RET
PNG_CopyColor_3	ENDP


ALIGN 16
PNG_CopyColor_4_SSSE3	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH r15

		MOV r13, rcx
		XOR rcx, rcx

		MOV eax, [r13]
		MOVD xmm0, DWORD PTR [r13+8] ; predictor
		MOVD xmm12, DWORD PTR [r13+12] ; predictor
		MOVD xmm3, DWORD PTR [r13+16] ; t_color
		
		ADD r8, [r13+24]
	PUSH r8
		ADD r8, [r13+32]

		MOV r15, r8
		MOV r11, [r13+40]

		CMP [r13+24], rcx
		JZ fline
			SUB r15, [r13+48]
		fline:

		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
		
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx

		ADD r12, rdx
		SUB r12, rcx


		PXOR xmm7, xmm7
		MOVDQA xmm4, XMMWORD PTR [one_mask]
	
		PUNPCKLBW xmm12, xmm7

	align 16
		scan_line_loop:
				LODSB
				ROR eax, 8
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			AND cl, 4
			JNZ set_pixel
				MOV al, cl
				ROR eax, 8
			set_pixel:

				MOV cl, dl

		
			MOVD xmm1, DWORD PTR [r15]
			PUNPCKLBW xmm1, xmm7
			PUNPCKLBW xmm0, xmm7
						
			MOVDQA xmm13, xmm0 ; a
			MOVDQA xmm14, xmm1 ; b
			MOVDQA xmm15, xmm12 ; c

			PSUBW xmm0, xmm12
			PSUBW xmm1, xmm12 ; pa

			MOVDQA xmm12, xmm14

			MOVDQA xmm2, xmm0 ; pb
			PABSW xmm2, xmm2

			PADDW xmm0, xmm1 ; pc			
			PABSW xmm0, xmm0
			PABSW xmm1, xmm1
			
			MOVDQA xmm5, xmm1
			PCMPGTW xmm1, xmm2
			PCMPGTW xmm5, xmm0
			POR xmm1, xmm5			

			PCMPGTW xmm2, xmm0
						
			MOVDQA xmm5, xmm1
			PXOR xmm1, xmm4
			PAND xmm1, xmm13

			MOVDQA xmm0, xmm2
			PAND xmm2, xmm15
			PXOR xmm0, xmm4
			PAND xmm0, xmm14

			POR xmm0, xmm2
			PAND xmm0, xmm5

			POR xmm0, xmm1

			PACKUSWB xmm0, xmm0
			
			
			MOVD xmm2, eax
			PADDB xmm0, xmm2		
				
			MOVDQA xmm1, xmm0
			PXOR xmm1, xmm3
			PCMPEQD xmm1, xmm7
			PXOR xmm1, xmm4
			PAND xmm1, xmm0
				
			MOVD DWORD PTR [r8], xmm1
			
			ADD r8, r11
			ADD r15, r11

			DEC r10d
						
			SUB r12, rdx
			JZ exit
			CMP r12, rdx
			JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:
		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB
			ROR eax, 8
		DEC cl
		JNZ last_copy_loop
											
		MOV cl, dl
		XOR r12, r12
	


exit:
		PACKUSWB xmm12, xmm12

		MOV [r13], eax
		MOVD DWORD PTR [r13+8], xmm0
		MOVD DWORD PTR [r13+12], xmm12

	POP r11
		SUB r8, r11
		MOV [r13+32], r8

		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12
	
	POP r15

	POP r13
	POP r12
	POP rsi

	RET
PNG_CopyColor_4_SSSE3	ENDP


ALIGN 16
PNG_CopyColor_4_SSE3	PROC
	PUSH rsi
	PUSH r12
	PUSH r13
	PUSH r15

		MOV r13, rcx
		XOR rcx, rcx

		MOV eax, [r13]
		MOVD xmm0, DWORD PTR [r13+8] ; predictor
		MOVD xmm12, DWORD PTR [r13+12] ; predictor
		MOVD xmm3, DWORD PTR [r13+16] ; t_color
		
		ADD r8, [r13+24]
	PUSH r8
		ADD r8, [r13+32]

		MOV r15, r8
		MOV r11, [r13+40]

		CMP [r13+24], rcx
		JZ fline
			SUB r15, [r13+48]
		fline:

		MOV r10d, [r13+line_position]

		MOV rsi, rdx	
		MOV r12, [r9]

		XOR rdx, rdx
		
		MOV cl, [r13+c_index]
		MOV dl, [r13+c_count]

		TEST cl, cl
		CMOVZ rcx, rdx

		ADD r12, rdx
		SUB r12, rcx


		PXOR xmm7, xmm7
		MOVDQA xmm4, XMMWORD PTR [one_mask]
		MOVDQA xmm10, XMMWORD PTR [sign_mask]
	
		PUNPCKLBW xmm12, xmm7

	align 16
		scan_line_loop:
				LODSB
				ROR eax, 8
			DEC cl
			JNZ scan_line_loop

			MOV cl, dl				
			
			AND cl, 4
			JNZ set_pixel
				MOV al, cl
				ROR eax, 8
			set_pixel:

				MOV cl, dl

		
			MOVD xmm1, DWORD PTR [r15]
			PUNPCKLBW xmm1, xmm7
			PUNPCKLBW xmm0, xmm7
						
			MOVDQA xmm13, xmm0 ; a
			MOVDQA xmm14, xmm1 ; b
			MOVDQA xmm15, xmm12 ; c

			PSUBW xmm0, xmm12
			PSUBW xmm1, xmm12 ; pa

			MOVDQA xmm12, xmm14

			MOVDQA xmm2, xmm0 ; pb
			
		MOVDQA xmm8, xmm10
		MOVDQA xmm9, xmm4

		PAND xmm8, xmm2
		PCMPEQW xmm8, xmm7
		PXOR xmm9, xmm8
			
		PAND xmm9, xmm2
		PAND xmm2, xmm8
		PSUBW xmm2, xmm9


			PADDW xmm0, xmm1 ; pc			

		MOVDQA xmm8, xmm10
		MOVDQA xmm9, xmm4

		PAND xmm8, xmm0
		PCMPEQW xmm8, xmm7
		PXOR xmm9, xmm8
			
		PAND xmm9, xmm0
		PAND xmm0, xmm8
		PSUBW xmm0, xmm9


		MOVDQA xmm8, xmm10
		MOVDQA xmm9, xmm4

		PAND xmm8, xmm1
		PCMPEQW xmm8, xmm7
		PXOR xmm9, xmm8
			
		PAND xmm9, xmm1
		PAND xmm1, xmm8
		PSUBW xmm1, xmm9
			
			MOVDQA xmm5, xmm1
			PCMPGTW xmm1, xmm2
			PCMPGTW xmm5, xmm0
			POR xmm1, xmm5			

			PCMPGTW xmm2, xmm0
						
			MOVDQA xmm5, xmm1
			PXOR xmm1, xmm4
			PAND xmm1, xmm13

			MOVDQA xmm0, xmm2
			PAND xmm2, xmm15
			PXOR xmm0, xmm4
			PAND xmm0, xmm14

			POR xmm0, xmm2
			PAND xmm0, xmm5

			POR xmm0, xmm1

			PACKUSWB xmm0, xmm0
			
			
			MOVD xmm2, eax
			PADDB xmm0, xmm2		
				
			MOVDQA xmm1, xmm0
			PXOR xmm1, xmm3
			PCMPEQD xmm1, xmm7
			PXOR xmm1, xmm4
			PAND xmm1, xmm0
				
			MOVD DWORD PTR [r8], xmm1
			
			ADD r8, r11
			ADD r15, r11

			DEC r10d
						
			SUB r12, rdx
			JZ exit
			CMP r12, rdx
			JB last_byte_copy

		TEST r10d, r10d
		JNZ scan_line_loop
		JMP exit

last_byte_copy:
		MOV cl, r12b
		SUB dl, r12b

		last_copy_loop:
			LODSB
			ROR eax, 8
		DEC cl
		JNZ last_copy_loop
											
		MOV cl, dl
		XOR r12, r12
	


exit:
		PACKUSWB xmm12, xmm12

		MOV [r13], eax
		MOVD DWORD PTR [r13+8], xmm0
		MOVD DWORD PTR [r13+12], xmm12

	POP r11
		SUB r8, r11
		MOV [r13+32], r8

		MOV [r13+line_position], r10d

		MOV [r13+c_index], cl

		MOV rax, rsi
		MOV [r9], r12
	
	POP r15

	POP r13
	POP r12
	POP rsi

	RET
PNG_CopyColor_4_SSE3	ENDP


END

