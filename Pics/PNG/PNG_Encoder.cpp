/*
** safe-fail image codecs
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Pics\PNG.h"

#include "System\SysUtils.h"

#pragma warning(disable:4244)



const unsigned int png_eof_chunk[] = {0, 'DNEI', 0x826042AE, };

struct PNG_Header_Chunk {
	unsigned int length;
	unsigned int cid;
	
	unsigned int width;
	unsigned int height;

	unsigned char bit_depth;
	unsigned char col_type;
	unsigned char comprssion;
	unsigned char filter;
	unsigned char interlace;

	unsigned char crc[4];
};



UI_64 Pics::PNG::InitializeEncoder() {

	return 0;
}

UI_64 Pics::PNG::Save(unsigned int iid, HANDLE f_out, Pack::Deflate & deflator) {
	UI_64 result(0), f_line_o(-1);
	DWORD  bcount(0);

	unsigned int wi(0), hi(0), cwi(0), chi(0), eb_size(0), idat_hdr[8], ad32(1);

	PNG_Header_Chunk png_hdr;

	SFSco::Object e_buffer, com_obj;
	
	if (Properties iprops = pi_list[iid]) {			
		wi = iprops.width;
		hi = iprops.height;
		cwi = iprops.c_width;
		chi = iprops.c_height;
			
	
// header out
		WriteFile(f_out, smarker, 8, &bcount, 0);

		png_hdr.length = 13;
		System::BSWAP_32(png_hdr.length);
		png_hdr.cid = 'RDHI';

		png_hdr.width = wi;
		System::BSWAP_32(png_hdr.width);
		png_hdr.height = hi;
		System::BSWAP_32(png_hdr.height);
	

		png_hdr.bit_depth = 8;
		png_hdr.col_type = 6;

		png_hdr.comprssion = png_hdr.filter = png_hdr.interlace = 0;

		reinterpret_cast<unsigned int *>(png_hdr.crc)[0] = 0;

		SFSco::CRC32(reinterpret_cast<unsigned int *>(png_hdr.crc)[0], reinterpret_cast<const unsigned char *>(&png_hdr.cid), 17);

		System::BSWAP_32(reinterpret_cast<unsigned int *>(png_hdr.crc)[0]);

		WriteFile(f_out, &png_hdr, 25, &bcount, 0);


		idat_hdr[0] = 0x16000000;
		idat_hdr[1] = 'tXEt';
		idat_hdr[2] = 'tfoS';
		idat_hdr[3] = 'eraw';
		idat_hdr[4] = 'fas' << 8;
		idat_hdr[5] = 'af-e';
		idat_hdr[6] = 'n.li';
		idat_hdr[7] = 'te';

		ad32 = 0;
		SFSco::CRC32(ad32, reinterpret_cast<const unsigned char *>(idat_hdr+1), 26);
		System::BSWAP_32(ad32);

		WriteFile(f_out, idat_hdr, 30, &bcount, 0);
		WriteFile(f_out, &ad32, 4, &bcount, 0);
/*
	idat_hdr[0] = 0x09000000;
	idat_hdr[1] = 'sYHp';
	idat_hdr[2] = png_hdr.width;
	idat_hdr[3] = png_hdr.height;
	
	idat_hdr[5] = 0;
	SFSco::CRC32(idat_hdr[5], reinterpret_cast<const unsigned char *>(idat_hdr+1), 13);
	idat_hdr[4] = ((idat_hdr[5] >> 16) & 0x00FF00) | (idat_hdr[5] & 0x00FF0000) | ((idat_hdr[5] & 0x00FF00) << 16);
	idat_hdr[5] = idat_hdr[5] & 0x00FF;

	WriteFile(f_out, idat_hdr, 21, &bcount, 0);

*/

// data chunks

		idat_hdr[1] = 'TADI';

		eb_size = 0x40000 / wi;

		if (eb_size > 8) {
			eb_size &= ~(unsigned int)7;
		} else {
			eb_size = 8;
		}

		if (e_buffer.New(0, (eb_size+4)*(cwi+1)*4) != -1) {
			if (unsigned char * e_line = reinterpret_cast<unsigned char *>(e_buffer.Acquire())) {
				if (unsigned int * pipo = reinterpret_cast<unsigned int *>(iprops.im_obj.Acquire())) {
						
					deflator.Reset();

					for (unsigned int li(1);li<=hi;li++) {
						SFSco::PNG_FilterLine(e_line, pipo, wi, f_line_o);
					
						pipo += cwi;
						
						if ((li % eb_size) == 0) {
//						SFSco::ADLER32(ad32, e_line + (cwi<<4), f_line_o);

							deflator.SetUSource(e_line + (cwi<<4), f_line_o);
							result = deflator.Pack(com_obj);

							if (result)
							if (const unsigned char * pdata = reinterpret_cast<const unsigned char *>(com_obj.Acquire())) {
							// create new chunk
								idat_hdr[0] = result;
								idat_hdr[3] = 0;

								if (li == eb_size) {
									idat_hdr[0] += Pack::Deflate::ZLIB_Header(reinterpret_cast<unsigned char *>(idat_hdr+2));
									System::BSWAP_32(idat_hdr[0]);

									WriteFile(f_out, idat_hdr, 10, &bcount, 0);
									SFSco::CRC32(idat_hdr[3], reinterpret_cast<const unsigned char *>(idat_hdr+1), 6);
								} else {
									System::BSWAP_32(idat_hdr[0]);

									WriteFile(f_out, idat_hdr, 8, &bcount, 0);
									SFSco::CRC32(idat_hdr[3], reinterpret_cast<const unsigned char *>(idat_hdr+1), 4);
								}

								WriteFile(f_out, pdata, result, &bcount, 0);

								SFSco::CRC32(idat_hdr[3], pdata, result);
							
								System::BSWAP_32(idat_hdr[3]);

								WriteFile(f_out, idat_hdr+3, 4, &bcount, 0);

								com_obj.Release();
							}

							f_line_o = 0;
						}
					}

					if (f_line_o) {
					// output last data chunk
//					SFSco::ADLER32(ad32, e_line + (cwi<<4), f_line_o);
					
						deflator.SetUSource(e_line + (cwi<<4), f_line_o, true);
						result = deflator.Pack(com_obj);

						if (result)
						if (const unsigned char * pdata = reinterpret_cast<const unsigned char *>(com_obj.Acquire())) {
							// create new chunk
							idat_hdr[0] = result;
							idat_hdr[3] = 0;

							if (hi < eb_size) {
								idat_hdr[0] += Pack::Deflate::ZLIB_Header(reinterpret_cast<unsigned char *>(idat_hdr+2));
								System::BSWAP_32(idat_hdr[0]);

								WriteFile(f_out, idat_hdr, 10, &bcount, 0);
								SFSco::CRC32(idat_hdr[3], reinterpret_cast<const unsigned char *>(idat_hdr+1), 6);
							} else {
								System::BSWAP_32(idat_hdr[0]);

								WriteFile(f_out, idat_hdr, 8, &bcount, 0);
								SFSco::CRC32(idat_hdr[3], reinterpret_cast<const unsigned char *>(idat_hdr+1), 4);
							}

							WriteFile(f_out, pdata, result, &bcount, 0);

							SFSco::CRC32(idat_hdr[3], pdata, result);
												
//						BSWAP(ad32);

//						WriteFile(f_out, &ad32, 4, &bcount, 0);
//						SFSco::CRC32(idat_hdr[3], reinterpret_cast<const unsigned char *>(&ad32), 4);

							System::BSWAP_32(idat_hdr[3]);

							WriteFile(f_out, idat_hdr+3, 4, &bcount, 0);

							com_obj.Release();
						}

						f_line_o = 0;
					}

					iprops.im_obj.Release();
				}

				e_buffer.Release();
			}
			
			e_buffer.Destroy();
		}

// end of file
		WriteFile(f_out, png_eof_chunk, 12, &bcount, 0);

	}

	return result;
}



