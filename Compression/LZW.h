/*
** safe-fail compression
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "CoreLibrary\SFSCore.h"

namespace Pack {
	class LZW {
		private:
			struct DecoderSet {
				const unsigned char * srcptr;
				UI_64 srcsize;
								
				unsigned int byte_val;
								
				unsigned char byte_position, stage, fill_order, early_change, c_resesrved[2];
				unsigned short reset_val, end_ofs_val, code_val;
		
				UI_64 buffer_runner, range_start, last_word_length;
			
			};

			struct DictionarySet {
				UI_64 runner, top, maximum_word_length;
				unsigned char min_code_length, current_code_length;
				unsigned short codes_count, code_mask, s_reserved;
			};

			static const UI_64 decoder_buffer_size = 0x200000;
			static const UI_64 di_buffer_size = 0x20000;

			static unsigned char * Decode(DecoderSet &, unsigned char *, UI_64 &);

			static UI_64 GetCodeLengthCodes(DecoderSet &, unsigned char *);
			static UI_64 GetLiDiLengths(DecoderSet &, unsigned char *);
			
			static void CopySB(unsigned char * dst, unsigned char *);

			void SetTables(unsigned char *, unsigned short, unsigned short); // 

			DecoderSet _decoder;
			DictionarySet _dictionary;

			SFSco::Object _d_buffer, _dic_buffer;

			UI_64 code_table[4096][2]; // offset, length

			
		public:
			LZW();
			virtual ~LZW();

			virtual UI_64 Reset(unsigned char, unsigned char = 0, bool = true);
			virtual void SetUSource(const unsigned char * src, UI_64 srcsize);

			virtual UI_64 Pack();			
			virtual UI_64 UnPack(SFSco::Object &);

			virtual UI_64 GetData(SFSco::Object &);

			virtual UI_64 Success();

	};



}