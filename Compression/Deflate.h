/*
** safe-fail compression
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "CoreLibrary\SFSCore.h"

namespace Pack {

	class Deflate {
		public:
			struct DecoderSet {
				const unsigned char * srcptr;
				UI_64 srcsize;
								
				unsigned int byte_val;
								
				unsigned char byte_position;				
				unsigned char end_of_block;
				unsigned char stage, _reserved;
		
				unsigned short * literal_codes;				
				unsigned short * distance_codes;

				UI_64 repeater, buffer_runner, range_start;
			
			};

		private:
			static const UI_64 decoder_buffer_size = 0x200000;
			static const UI_64 search_buffer_size = 0x8000;
			static const UI_64 hash_depth = 15;

			static const unsigned char c_l_a[];
			

			void SetTables(unsigned char *, unsigned short, unsigned short); // 
			void SetFixedCodes(unsigned short *, unsigned short *, unsigned char *);

			UI_64 EncodeBlock(unsigned short *);
			void CodeABet(unsigned char *, UI_64 &);

			DecoderSet _decoder;
			SFSco::Object _d_buffer;


			UI_64 encoder_hash[256][hash_depth+1];
			UI_64 stats_table[640];

			unsigned char lengths_table[1028];
			unsigned short literal_table[640];
			unsigned short distance_table[128];

		public:
			static UI_64 ZLIB_Header(unsigned char *);

			Deflate();
			virtual ~Deflate();

			virtual UI_64 Reset();
						
			virtual void SetUSource(const unsigned char * src, UI_64 srcsize, bool last_chunk = false);
			
			virtual UI_64 UnPack(SFSco::Object &);
			virtual UI_64 Pack(SFSco::Object & out_buffer); // return packed size

			virtual UI_64 Success();

	};

	extern "C" {
		void Deflate_CopySB(unsigned char * dst, unsigned char *);
		UI_64 Deflate_GetCodeLengthCodes(Deflate::DecoderSet &, unsigned char *);
		UI_64 Deflate_GetLiDiLengths(Deflate::DecoderSet &, unsigned char *);
		unsigned char * Deflate_Decode(Deflate::DecoderSet &, unsigned char *, UI_64 &);

		UI_64 Deflate_MatchLength(const unsigned char *, UI_64, UI_64, UI_64);
	}


}

