
; safe-fail compression
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



;EXTERN	?search_buffer_size@Deflate@Pack@@0_KB:QWORD
EXTERN	?c_l_a@Deflate@Pack@@0QBEB:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE


ALIGN 16
Deflate_CopySB	PROC
	PUSH rsi
	PUSH rdi

	MOV rdi, rcx
	MOV rsi, rdx
	
	MOV rcx, 8000h ; [?search_buffer_size@Deflate@Pack@@0_KB]

	DEC rdi
	DEC rsi

	STD
		REP MOVSB
	CLD


	POP rdi
	POP rsi

	RET
Deflate_CopySB	ENDP


ALIGN 16
Deflate_GetCodeLengthCodes	PROC
	PUSH r12
	PUSH r13
	PUSH rbx
		MOV r8, rcx

		XOR rcx, rcx
		XOR rax, rax
		XOR r12, r12
		XOR r13, r13
		XOR r11, r11

		LEA r13, [?c_l_a@Deflate@Pack@@0QBEB]
		CMP QWORD PTR [r8+24], 0
		CMOVNZ r13, [r8+24]


		MOV r9, [r8] ; pointer
		MOV r10, [r8+8] ; count

		MOV eax, [r8+16]
		MOV cl, [r8+20]
		MOV r11b, [r8+44]
		
		CMP BYTE PTR [r8+23], 0
		JA second_line

			CMP cl, 5
			JAE noadjust1			
				ROR eax, cl
			
				MOV al, [r9]
				ROL eax, cl

				ADD cl, 8
				
				INC r9
				DEC r10
			noadjust1:

			MOV r11w, 01Fh
			AND r11b, al

			SUB cl, 5
			SHR eax, 5

			ADD r11w, 257
			MOV [rdx+20], r11w

			INC BYTE PTR [r8+23]
			
			AND r10, r10
			JZ exit

align 16
	second_line:
		CMP BYTE PTR [r8+23], 1
		JA third_line

			CMP cl, 5
			JAE noadjust2
				ROR eax, cl
			
				MOV al, [r9]

				ROL eax, cl

				ADD cl, 8
			
				INC r9
				DEC r10
			noadjust2:

			MOV r11w, 01Fh
			AND r11b, al
		
			SUB cl, 5
			SHR eax, 5

			INC r11b
			MOV [rdx+22], r11b

			ADD r11w, [rdx+20]
			MOV [rdx+24], r11w
		
			INC BYTE PTR [r8+23]
			
			AND r10, r10
			JZ exit 

align 16
	third_line:
		CMP BYTE PTR [r8+23], 2
		JA clc_loop

			CMP cl, 4
			JAE noadjust3
				ROR eax, cl
			
				MOV al, [r9]

				ROL eax, cl

				ADD cl, 8
			
				INC r9
				DEC r10
			noadjust3:

			MOV r11b, 0Fh
			AND r11b, al

			SUB cl, 4
			SHR eax, 4

			ADD r11b, 4

			INC BYTE PTR [r8+23]

			AND r10, r10
			JZ exit


	align 16
		clc_loop:

			CMP cl, 3			
			JAE noadjust4
				ROR eax, cl
			
				MOV al, [r9]

				ROL eax, cl

				ADD cl, 8
			
				INC r9
				DEC r10
			noadjust4:

			MOV bl, 7
			AND bl, al

			SUB cl, 3
			SHR eax, 3

			MOV r12b, [r13]
			MOV [rdx+r12], bl

			INC r13
			
			DEC r11b

			AND r10, r10
			JZ exit
		AND r11b, r11b
		JNZ clc_loop
			
		INC BYTE PTR [r8+23]

		XOR r11, r11
exit:
		MOV [r8], r9 ; pointer
		MOV [r8+8], r10 ; count


		MOV [r8+16], eax
		MOV [r8+20], cl

		MOV [r8+24], r13
		MOV [r8+44], r11b

		MOVZX rax, BYTE PTR [r8+23]
	POP rbx
	POP r13
	POP r12

	RET
Deflate_GetCodeLengthCodes	ENDP


ALIGN 16
Deflate_GetLiDiLengths	PROC
	PUSH rbx
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

		MOV r8, rcx
		
		XOR rcx, rcx
		XOR rax, rax
		XOR rbx, rbx
		XOR r10, r10
		XOR r11, r11
		XOR r13, r13
		XOR r15, r15
		
		MOV r13w, [rdx+26]

		MOV r12, [r8] ; pointer
		MOV r9, [r8+8] ; count

		MOV eax, [r8+16]
		MOV bl, [r8+20]		

		MOV cl, [r8+44]
		SHL ecx, 8

		MOV r10b, [r8+46]

		CMP BYTE PTR [r8+45], 16
		JZ r15_label_1

	align 16
		stream_scan_loop:

			AND bl, bl
			JNZ nextbit
				XOR r15, r15
				AND r9, r9
				JZ last_byte_reset

				MOV al, [r12]
				MOV bl, 8

				INC r12
				DEC r9					
					
			nextbit:
				DEC bl

				INC r11b
				CMP r11b, 7
				JA exit
																
				SHR al, 1
				RCL r10b, 1						
																
			CMP r10b, [rdx+r11*4+60]
			JAE stream_scan_loop


			SUB r10b, [rdx+r11*4+61]
			ADD r10b, [rdx+r11*4+62]
							
			LEA r11, [r11*4+r10+60]
														
			MOV r10b, [rdx+r11] ; decoded val
							
			XOR r11, r11

			CMP r10b, 15
			JA doextra
				MOV [rdx+r13+128], r10b
				XOR r10, r10

				INC r13w
				CMP r13w, [rdx+24]
				JB stream_scan_loop

				XOR r15, r15
				NOT r15

			JMP exit

			align 16
				doextra:

					MOV ch, 2

					SUB r10b, 16
					JZ extraction_on
						MOV cl, r10b
						SHL ch, cl
						DEC ch									
					extraction_on:
										

					CMP bl, ch
					JAE read_it
						MOV r15b, 16
						AND r9, r9
						JZ exit
				r15_label_1:

						MOV cl, bl

						ROR eax, cl
						MOV al, [r12]

						INC r12
						DEC r9
												
						ADD bl, 8
						ROL eax, cl

					read_it:
						SHR ecx, 8
						MOV r11b, 1
						SHL r11b, cl
						DEC r11b						

						AND r11b, al

						SUB bl, cl
						SHR eax, cl
					
						ADD r11b, 3

						AND r10b, r10b
						JNZ skip_code
							MOV r14d, eax
							LEA rdi, [rdx+r13+128] 
											
							MOV al, [rdi-1]
							MOV cl, r11b

							REP STOSB

							MOV eax, r14d							

						skip_code:
							
							AND r10b, 2
							SHL r10b, 2
							ADD r11b, r10b

							ADD r13w, r11w

							XOR r11, r11
							XOR r10, r10						
							
						CMP r13w, [rdx+24]
						JB stream_scan_loop

						XOR r15, r15
						NOT r15

last_byte_reset:
	AND r11b, r11b
	JZ exit
		CMP r11b, 7
		JA exit
			MOV bl, r11b
			reverse_loop10:
				SHR r10b, 1
				RCL al, 1
			DEC r11b
			JNZ reverse_loop10
			

exit:
		MOV [rdx+26], r13w

		SHR ecx, 8
		CMP r11b, 7
		CMOVA r15w, r11w

		MOV [r8], r12 ; pointer
		MOV [r8+8], r9 ; count

		MOV [r8+16], eax
		MOV [r8+20], bl
				
		MOV [r8+44], cl
		MOV [r8+45], r15b
		MOV [r8+46], r10b


		MOV rax, r15

	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rbx

	RET
Deflate_GetLiDiLengths	ENDP


ALIGN 16
Deflate_Decode	PROC
	PUSH rbx
	PUSH rsi
	PUSH rdi
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
	PUSH rbp

		MOV r15, rcx
		MOV rdi, rdx ; destination

		XOR rcx, rcx
		XOR rax, rax
		XOR rbx, rbx
		XOR rdx, rdx
		XOR r10, r10
		XOR r11, r11
		XOR r12, r12
		XOR r13, r13
		XOR rbp, rbp
		
		CLD

		MOV rbp, [r15] ; pointer
		MOV r9, [r15+8] ; count
				
		
		MOV eax, [r15+16]
		MOV bl, [r15+20]		
		MOV r12b, [r15+23]

		MOV r13, [r15+24]
		MOV r14, [r15+32]

		MOV dx, [r15+40]
		MOV r10w, [r15+42]
		MOV cl, [r15+44]

		SUB r13, 8		
		SUB r14, 8

		SHL ecx, 8

		PUSH r15
		MOV r15, [r8]


		CMP r12b, 1
		JZ r12_label_1
			CMP r12b, 2
			JZ x_distance
				CMP r12b, 3
				JZ r12_label_3
					CMP r12b, 4
					JZ r12_label_4

	align 16
		stream_scan_loop:
			AND bl, bl
			JNZ nextbit_loop
				XOR r12, r12

				AND r9, r9
				JZ last_byte

				MOV al, [rbp]
				MOV bl, 8

				INC rbp						
				DEC r9
				
								
			align 16
				nextbit_loop:					
					AND bl, bl
					JZ stream_scan_loop
					
					DEC bl
					INC r11b
					CMP r11b, 15
					JA exit
																
					SHR ax, 1
					RCL r10w, 1					
																
				CMP r10w, [r13+r11*8]
				JAE nextbit_loop

				SUB r10w, [r13+r11*8+2]
				SHL r10w, 1
				ADD r10w, [r13+r11*8+4]
							
				LEA r11, [r11*8+r10]
														
				MOV cx, [r13+r11] ; decoded val
							
				XOR r10, r10
				XOR r11, r11

				CMP cx, 255
				JA doextra
					MOV [rdi], cl
					INC rdi
					DEC r15

					CMP r15, 400
					JA stream_scan_loop

					XOR r12, r12
					JMP exit



			align 16
				doextra:
					OR r13, 1

					CMP cx, 256					
					JZ exit
					
					XOR r13, 1

					CMP cx, 264
					JA set_extra
						SUB cx, 254
						MOV dx, cx
						JMP x_distance
					
					set_extra:						
						SUB cx, 261
						MOV dx, cx

						AND dx, 3
						OR dx, 4

						SHR cx, 2						
						
						CMP cl, 6
						JB cont_inst
							MOV dx, 258
							JMP x_distance						
						cont_inst:
														
							SHL dx, cl
							ADD dx, 3
							
							MOV ch, cl						
							CMP bl, cl							
							JAE do_not_read
								MOV r12b, 1

								AND r9, r9
								JZ exit
					r12_label_1:
								MOV cl, bl
								
								ROR eax, cl

								MOV al, [rbp]
								ADD bl, 8

								INC rbp
								DEC r9
								
								ROL eax, cl								

							do_not_read:
								SHR ecx, 8
								
								MOV r10w, 1
								SHL r10w, cl
								DEC r10w

								AND r10w, ax
								SHR ax, cl
								SUB bl, cl


								ADD dx, r10w
								XOR r10, r10










		align 16
			x_distance:
				AND bl, bl
				JNZ nextbit_loopx
					MOV r12b, 2
					AND r9, r9
					JZ last_byte

					MOV al, [rbp]
					MOV bl, 8

					INC rbp						
					DEC r9
													
				align 16
					nextbit_loopx:					
						AND bl, bl
						JZ x_distance
					
						DEC bl
						INC r11b
						CMP r11b, 15
						JA exit
																
						SHR ax, 1
						RCL r10w, 1					
																
					CMP r10w, [r14+r11*8]
					JAE nextbit_loopx

					SUB r10w, [r14+r11*8+2]	
					SHL r10w, 1				
					ADD r10w, [r14+r11*8+4]
							
					LEA r11, [r11*8+r10]
														
					MOV cx, [r14+r11] ; decoded val
							
					XOR r10, r10
					XOR r11, r11

					CMP cl, 4
					JB copy_string
						SUB cl, 2
						MOV r10b, cl
						
						AND r10b, 1
						OR r10b, 2

						SHR cl, 1						
						
						SHL r10w, cl

						
						MOV ch, cl
						CMP bl, cl
						JAE no_add_byte
							MOV r12b, 3
							AND r9, r9
							JZ exit
					r12_label_3:
							MOV cl, bl
							ROR eax, cl

							MOV al, [rbp]
							ADD bl, 8

							INC rbp
							DEC r9
							
							ROL eax, cl

							CMP bl, ch
							JAE no_add_byte
								MOV r12b, 4
								AND r9, r9
								JZ exit
						r12_label_4:		
								MOV cl, bl
								ROR eax, cl
																
								MOV al, [rbp]
								ADD bl, 8
								

								INC rbp
								DEC r9
								
								ROL eax, cl														
							

														
							
						no_add_byte:
					
							SHR ecx, 8
							MOV r11w, 1
							SHL r11w, cl
							DEC r11w

							AND r11w, ax
							SHR eax, cl
							SUB bl, cl
						
							ADD r10w, r11w
							MOV cx, r10w

							XOR r11, r11
							XOR r10, r10

				align 16
					copy_string:						
						MOV rsi, rdi
						INC rcx
						SUB rsi, rcx

						SUB r15, rdx						

						MOV cx, dx
						REP MOVSB

				CMP r15, 400
				JA stream_scan_loop

				XOR r12, r12

			JMP exit

	last_byte:
		AND r11b, r11b
		JZ exit
			CMP r11b, 15
			JA exit
				MOV bl, r11b
				reverse_loop10:
					SHR r10w, 1
					RCL ax, 1
				DEC r11b
				JNZ reverse_loop10

exit:
	

		MOV [r8], r15
		POP r15

		SHR ecx, 8
		AND r13b, 1
		
		MOV [r15], rbp ; pointer
		MOV [r15+8], r9 ; count
			
		MOV [r15+16], eax
		MOV [r15+20], bl
		OR [r15+21], r13b
		MOV [r15+23], r12b		

		MOV [r15+40], dx
		MOV [r15+42], r10w
		
		XOR r13b, cl
		CMOVZ cx, r13w

		MOV [r15+44], cl

		XOR rax, rax

		AND r11, r11
		CMOVZ rax, rdi

	POP rbp
	POP r15
	POP r14
	POP r13
	POP r12
	POP rdi
	POP rsi
	POP rbx

	RET
Deflate_Decode	ENDP

ALIGN 16
Deflate_MatchLength	PROC
	
	ADD rcx, rdx
	SUB rcx, r8
	SUB r9, rdx

	MOV r11, 258
	CMP r9, r11
	CMOVA r9, r11

	MOV r10, r9
	AND r10, 7
	SHR r9, 3
	MOV r11, r9

	TEST r9, r9
	JZ leftover

	align 16
		r9_loop:
			MOV rdx, [rcx+r8]
			XOR rdx, [rcx]
			JNZ exit

			ADD rcx, 8
		DEC r9
		JNZ r9_loop

align 16
	leftover:
		MOV rdx, 1
		TEST r10, r10
		JZ exit
	align 16
		lo_1:
			MOV al, [rcx+r8]
			XOR al, [rcx]
			JNZ exit
			
			SHL rdx, 8
			
			INC rcx
		DEC r10
		JNZ lo_1

align 16
	exit:
		SUB r11, r9
		BSF rax, rdx
		SHR rax, 3		

		LEA rax, [rax+r11*8]

	RET
Deflate_MatchLength	ENDP

ALIGN 16
?RootOrder@@YAGGG@Z	PROC
	XOR rax, rax

align 16
	t_loop:
		SHR cx, 1
		RCL ax, 1
	DEC dl
	JNZ t_loop

	RET
?RootOrder@@YAGGG@Z	ENDP


END


