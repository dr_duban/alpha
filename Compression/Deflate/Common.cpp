/*
** safe-fail compression
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>
#include "Compression\Pack.h"

unsigned short __fastcall RootOrder(unsigned short, unsigned short);


unsigned short Pack::GenHuffmanTable(unsigned short * codes_table, unsigned char * length_table, UI_64 * stats_table, unsigned int aa_val) {
	unsigned short ss_i(0), ss_p(0), ss_c(0), ss_min(0), ss_s(0), s_ss(0), a_size(aa_val & 0xFFFF), top_length((aa_val>>16) & 0xFF), ror(aa_val>>24);
	unsigned char code_length(0);
		
	if (top_length > 16) top_length = 16;

	for (ss_i = 0;ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			ss_min = ss_i;
			codes_table[ss_i] = 0xFFFF;
			break;
		}

	}
	
// order frequencies
	for (ss_i=(ss_min+1);ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			if (stats_table[ss_i] <= stats_table[ss_min]) {					
				codes_table[ss_i] = ss_min;
				ss_min = ss_i;
				
			} else {
				ss_p = ss_min;

				for (;;) {
					ss_c = codes_table[ss_p];
					if (ss_c != 0xFFFF) {
						if (stats_table[ss_i] < stats_table[ss_c]) {
							codes_table[ss_p] = ss_i;
							codes_table[ss_i] = ss_c;

							break;
						}

						ss_p = ss_c;
					} else {
						codes_table[ss_p] = ss_i;
						codes_table[ss_i] = ss_c;
						break;
					}
				}
			}
		}
	}

// build a tree
	for (ss_i = a_size;;ss_i++) {
		s_ss = ss_min;
		stats_table[ss_i] = stats_table[ss_min];
		ss_s = ss_min = codes_table[ss_min] & 0x7FFF;
			
		stats_table[ss_i] += stats_table[ss_min];				
		ss_min = codes_table[ss_min];

		
		if (ss_min == 0xFFFF) {
			codes_table[s_ss] = 0xFFFF;
			break;
		}

		ss_min &= 0x7FFF;
		
		codes_table[ss_s] = ss_i | 0x8000;	

		if (stats_table[ss_i] <= stats_table[ss_min]) {
			codes_table[ss_i] = ss_min;
			ss_min = ss_i;
		} else {
			ss_p = ss_min;

			for (;;) {
				ss_c = codes_table[ss_p];
				if (ss_c != 0xFFFF) {					
					ss_c &= 0x7FFF;
					if (stats_table[ss_i] < stats_table[ss_c]) {
						codes_table[ss_p] = ss_i;
						codes_table[ss_i] = ss_c;

						break;
					}

					ss_p = ss_c;
				} else {
					codes_table[ss_p] = ss_i;
					codes_table[ss_i] = ss_c;

					break;
				}
			}
		}
	}

	for (ss_i=1;ss_i<a_size;ss_i++) stats_table[a_size+ss_i] = 0;


// build codes
	
	ss_s = 0;
	for (ss_i=0;ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			
			code_length = 1;

			ss_p = ss_i;
			for (;;) {
				ss_c = codes_table[ss_p];
				if (ss_c != 0xFFFF) {
					if ((ss_c & 0x8000) == 0) ss_c = codes_table[ss_c];						
					ss_p = ss_c & 0x7FFF;

					++code_length;
				} else {
					if (code_length > ss_s) ss_s = code_length;
					length_table[ss_i] = code_length;					
					++stats_table[a_size+code_length];

					break;
				}
			}
		} else {
			length_table[ss_i] = 0;
		}
	}

	if (ss_s>top_length) {	

		for (ss_i=ss_s;ss_i>top_length;ss_i--) {

			for (;stats_table[a_size+ss_i];) {
				ss_c = 1;
				for (;stats_table[a_size+ss_i] > (stats_table[a_size+ss_i-ss_c-1] << ss_c);ss_c++);


				s_ss = ss_min = stats_table[a_size+ss_i] >> ss_c;

				if (ss_min) {
					stats_table[a_size + ss_i - ss_c - 1] -= ss_min;
					stats_table[a_size + ss_i - ss_c] += ss_min;

					for (ss_p=0;ss_p<a_size;ss_p++) {
						if (length_table[ss_p] == (ss_i - ss_c - 1)) {
							length_table[ss_p] = ss_i - ss_c;
							if (--s_ss == 0) break;
						}
					}


					ss_min <<= ss_c;
	
					stats_table[a_size+ss_i-1] += ss_min;
					stats_table[a_size+ss_i] -= ss_min;

					for (ss_p=0;ss_p<a_size;ss_p++) {
						if (length_table[ss_p] == ss_i) {
							length_table[ss_p] = ss_i - 1;
							if (--ss_min == 0) break;
						}
					}
				} else {
				// ss_min == 0
				//	ss_min = 2;
					
				//	for (;ss_min < stats_table[a_size+ss_i];ss_min+=2);
					s_ss = ss_min = stats_table[a_size+ss_i];

					ss_s = ((s_ss + (1<<ss_c) - 1) >> ss_c);
					s_ss = (ss_s << ss_c) - ss_min;

					stats_table[a_size + ss_i - ss_c - 1] -= ss_s;
					stats_table[a_size + ss_i - ss_c] += ss_s;

					for (ss_p=0;ss_p<a_size;ss_p++) {
						if (length_table[ss_p] == (ss_i - ss_c - 1)) {
							length_table[ss_p] = ss_i - ss_c;

							if (--ss_s == 0) break;
						}
					}

					stats_table[a_size+ss_i-1] += ss_min;
					stats_table[a_size+ss_i] -= ss_min;

					for (ss_p=0;ss_p<a_size;ss_p++) {
						if (length_table[ss_p] == ss_i) {
							length_table[ss_p] = ss_i - 1;
							if (--ss_min == 0) break;
						}
					}


					if (s_ss >>= 1) {
						for (ss_s = 1;s_ss>0;s_ss>>=1, ss_s++) {
							if (s_ss & 1) {
								stats_table[a_size + ss_i - ss_s]--;
								stats_table[a_size + ss_i - ss_s - 1]++;

								for (ss_p=0;ss_p<a_size;ss_p++) {
									if (length_table[ss_p] == (ss_i - ss_s)) {
										length_table[ss_p] = ss_i - ss_s - 1;
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		ss_s = top_length;
	}
	
	ss_min = 0;
	for (ss_i=1;ss_i<=ss_s;ss_i++) {
		if (ss_c = stats_table[a_size+ss_i]) {
			for (ss_p=0;ss_c>0;ss_p++) {
				if (length_table[ss_p] == ss_i) {
					if (ror) codes_table[ss_p] = ss_min;
					else codes_table[ss_p] = RootOrder(ss_min, ss_i);

					++ss_min;
					--ss_c;
				}
			}
		}
		ss_min <<= 1;
	}

	return ss_s;
}



/*

#include "Compression\Deflate.h"

unsigned short __fastcall RootOrder(unsigned short, unsigned short);

unsigned short Pack::GenHuffmanTable(unsigned short * codes_table, unsigned char * length_table, UI_64 * stats_table, unsigned int aa_val) {
	unsigned short ss_i(0), ss_p(0), ss_c(0), ss_min(0), ss_s(0), s_ss(0), a_size(aa_val & 0xFFFF), top_length(aa_val>>16);
	unsigned char code_length(0);
		
	
	for (ss_i = 0;ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			ss_min = ss_i;
			codes_table[ss_i] = 0xFFFF;
			break;
		}

	}
	
// order frequencies
	for (ss_i=(ss_min+1);ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			if (stats_table[ss_i] <= stats_table[ss_min]) {					
				codes_table[ss_i] = ss_min;
				ss_min = ss_i;
				
			} else {
				ss_p = ss_min;

				for (;;) {
					ss_c = codes_table[ss_p];
					if (ss_c != 0xFFFF) {
						if (stats_table[ss_i] < stats_table[ss_c]) {
							codes_table[ss_p] = ss_i;
							codes_table[ss_i] = ss_c;

							break;
						}

						ss_p = ss_c;
					} else {
						codes_table[ss_p] = ss_i;
						codes_table[ss_i] = ss_c;
						break;
					}
				}
			}
		}
	}

// build a tree
	for (ss_i = a_size;;ss_i++) {
		s_ss = ss_min;
		stats_table[ss_i] = stats_table[ss_min];
		ss_s = ss_min = codes_table[ss_min] & 0x7FFF;
			
		stats_table[ss_i] += stats_table[ss_min];				
		ss_min = codes_table[ss_min];

		
		if (ss_min == 0xFFFF) {
			codes_table[s_ss] = 0xFFFF;
			break;
		}

		ss_min &= 0x7FFF;
		
		codes_table[ss_s] = ss_i | 0x8000;	

		if (stats_table[ss_i] <= stats_table[ss_min]) {
			codes_table[ss_i] = ss_min;
			ss_min = ss_i;
		} else {
			ss_p = ss_min;

			for (;;) {
				ss_c = codes_table[ss_p];
				if (ss_c != 0xFFFF) {					
					ss_c &= 0x7FFF;
					if (stats_table[ss_i] < stats_table[ss_c]) {
						codes_table[ss_p] = ss_i;
						codes_table[ss_i] = ss_c;

						break;
					}

					ss_p = ss_c;
				} else {
					codes_table[ss_p] = ss_i;
					codes_table[ss_i] = ss_c;

					break;
				}
			}
		}
	}

	for (ss_i=1;ss_i<a_size;ss_i++) stats_table[a_size+ss_i] = 0;


// build codes
	
	ss_s = 0;
	for (ss_i=0;ss_i<a_size;ss_i++) {
		if (stats_table[ss_i]) {
			
			code_length = 1;

			ss_p = ss_i;
			for (;;) {
				ss_c = codes_table[ss_p];
				if (ss_c != 0xFFFF) {
					if ((ss_c & 0x8000) == 0) ss_c = codes_table[ss_c];						
					ss_p = ss_c & 0x7FFF;

					++code_length;
				} else {
					if (code_length > ss_s) ss_s = code_length;
					length_table[ss_i] = code_length;					
					++stats_table[a_size+code_length];

					break;
				}
			}
		} else {
			length_table[ss_i] = 0;
		}
	}

	if (ss_s == top_length) {
		ss_min = 0;
		for (ss_i=1;ss_i<=top_length;ss_i++) {
			ss_min <<= 1;
			ss_min += stats_table[a_size+ss_i];			
		}

		if (ss_min == (1 << top_length)) ss_s++;
		
	}


	if (ss_s>top_length) {
		unsigned short coco[16], top_cap(0xFFFF);

		if (top_length<16) top_cap = (1 << top_length) - 1;

		ss_min = 0;
		coco[0] = top_cap;
		for (ss_i=1;ss_i<top_length;ss_i++) {
			if (ss_c = stats_table[a_size+ss_i]) {
				coco[ss_i] = top_cap - (ss_min << (top_length-ss_i));
				ss_min += ss_c;
			} else {
				coco[ss_i] = coco[ss_i-1];
			}

			ss_min <<= 1;
		}

		for (ss_i=ss_s;ss_i>0;ss_i--) {
			if (ss_i != top_length) {
				stats_table[a_size+top_length] += stats_table[a_size+ss_i];
				stats_table[a_size+ss_i] = 0;
			}

			if (ss_i<top_length) {
				if (coco[ss_i] >= stats_table[a_size+top_length]) {
					for (ss_p=0;ss_p<a_size;ss_p++) {
						if (length_table[ss_p] >= ss_i) length_table[ss_p] = top_length;

					}

					break;
				}
			}
		}
	}

	
	ss_min = 0;
	for (ss_i=1;ss_i<=top_length;ss_i++) {
		if (ss_c = stats_table[a_size+ss_i]) {
			for (ss_p=0;ss_c>0;ss_p++) {
				if (length_table[ss_p] == ss_i) {
					codes_table[ss_p] = RootOrder(ss_min, ss_i);

					++ss_min;
					--ss_c;
				}
			}
		}
		ss_min <<= 1;
	}

	return ss_s;
}







*/





