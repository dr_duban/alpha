/*
** safe-fail compression
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>
#include "Compression\Deflate.h"
#include "Compression\Pack.h"

#include "System\SysUtils.h"

const unsigned char Pack::Deflate::c_l_a[] = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

UI_64 Pack::Deflate::ZLIB_Header(unsigned char * dst) {
	dst[0] = 0x78;
	dst[1] = 0x5E; // 0x9C - default, 0x5E - fast, 0xDA - max

	return 2;
}

Pack::Deflate::Deflate() {

}

Pack::Deflate::~Deflate() {
	_d_buffer.Destroy();

}

UI_64 Pack::Deflate::Reset() {
	UI_64 result(0);
	_decoder.srcptr = 0;
	_decoder.srcsize = 0;

	_decoder.byte_position = 0;
	_decoder.byte_val = 0;
	_decoder.end_of_block = 1;
	_decoder.stage = 0;
	_decoder._reserved = 0;

	_decoder.literal_codes = literal_table;
	_decoder.distance_codes = distance_table;

	_decoder.repeater = 0;
	_decoder.buffer_runner = _decoder.range_start = search_buffer_size;

	System::MemoryFill_SSE3(encoder_hash, 256*(hash_depth+1)*sizeof(UI_64), 0, 0);
	System::MemoryFill_SSE3(stats_table, 640*sizeof(UI_64), 0, 0);

	stats_table[256] = 1;
	
	if (!_d_buffer) result = _d_buffer.New(0, decoder_buffer_size, SFSco::large_mem_mgr);
	return result;
}

UI_64 Pack::Deflate::Pack(SFSco::Object & out_obj) {
	UI_64 result(0), hash_runner(0), delta_runner(0), max_length(0), max_offset(0), band_gap(0), c_index(0);
	unsigned char la_code(0);

	if (!_d_buffer) return -1;

	out_obj.Blank();

	_decoder.buffer_runner = 1024;


	if (unsigned short * dst = reinterpret_cast<unsigned short *>(_d_buffer.Acquire())) {		

		for (;;) {

			la_code = _decoder.srcptr[_decoder.repeater];
			band_gap = 0;

			if (hash_runner = encoder_hash[la_code][hash_depth]) {
			
				max_length = 1;
			
				for (UI_64 li(1);li<=hash_depth;li++) {
					c_index = (hash_runner-li) & hash_depth;
					if (c_index == hash_depth) continue;
					
					if (encoder_hash[la_code][c_index] == 0) break;

					delta_runner = _decoder.repeater - encoder_hash[la_code][c_index];
			
					if (delta_runner < 32768) {					
						result = Deflate_MatchLength(_decoder.srcptr, _decoder.repeater, delta_runner, _decoder.srcsize);
						if (result > max_length) {
							max_length = result;
							max_offset = delta_runner;

							if (result > 200) break;
						}
					} else {
						break;
					}
				}

				if ((hash_runner %= hash_depth) == 0) encoder_hash[la_code][hash_depth] = 0;

				encoder_hash[la_code][hash_runner] = _decoder.repeater;
				++encoder_hash[la_code][hash_depth];

				++_decoder.repeater;


				if ((max_length > 2) && ((max_offset/max_length) < 512)) {
					// collect stats
					c_index = max_length;

					dst[_decoder.buffer_runner++] = max_length;
					dst[_decoder.buffer_runner++] = max_offset;
				
					if (max_length <= 10) {
						max_length -= 3;
					} else {
						if (max_length <= 18) {
							max_length = 8 + ((max_length-11)>>1);
						} else {
							if (max_length <= 34) {
								max_length = 12 + ((max_length-19)>>2);
							} else {
								if (max_length <= 66) {
									max_length = 16 + ((max_length-35)>>3);
								} else {
									if (max_length <= 130) {
										max_length = 20 + ((max_length-67)>>4);
									} else {
										if (max_length <= 257) {
											max_length = 24 + ((max_length-131)>>5);
										} else {
											max_length = 28;
										}
									}
								}
							}
						}
					}

					++stats_table[257+max_length];
					dst[_decoder.buffer_runner] = max_length;

								

					if (--max_offset > 3) {

						result = System::BitIndexHigh_64(max_offset);

						max_offset &= ((UI_64)1 << (result - 1));

						max_offset = (result<<1) + (max_offset > 0);

						max_offset &= 0x1F;
					}

					++stats_table[580+max_offset];

					dst[_decoder.buffer_runner++] |= (max_offset << 8);

					for (UI_64 mi(1);mi<c_index;mi++) {
						if (la_code != _decoder.srcptr[_decoder.repeater]) {
							if (band_gap) {
								if ((hash_runner = encoder_hash[la_code][hash_depth]%hash_depth) == 0) encoder_hash[la_code][hash_depth] = 0;
								encoder_hash[la_code][hash_runner] = _decoder.repeater-1;
					
								++encoder_hash[la_code][hash_depth];
							}
							
							band_gap = 0;

							la_code = _decoder.srcptr[_decoder.repeater];
							if ((hash_runner = encoder_hash[la_code][hash_depth]%hash_depth) == 0) encoder_hash[la_code][hash_depth] = 0;
							encoder_hash[la_code][hash_runner] = _decoder.repeater;
					
							++encoder_hash[la_code][hash_depth];
						} else {
							++band_gap;
						}

						++_decoder.repeater;
					}

					if (band_gap) {
						if ((hash_runner = encoder_hash[la_code][hash_depth]%hash_depth) == 0) encoder_hash[la_code][hash_depth] = 0;
						encoder_hash[la_code][hash_runner] = _decoder.repeater-1;
					
						++encoder_hash[la_code][hash_depth];

					}

				} else {
					// output literal
					++stats_table[la_code];
					dst[_decoder.buffer_runner++] = 0x8000 | la_code;
				}


			} else {			

			// output literal
				++stats_table[la_code];
				dst[_decoder.buffer_runner++] = 0x8000 | la_code;

				encoder_hash[la_code][0] = _decoder.repeater;
				encoder_hash[la_code][hash_depth] = 1;

				++_decoder.repeater;
			
			}


			if ((((_decoder.buffer_runner<<1)+10) > decoder_buffer_size) || (_decoder.repeater == _decoder.srcsize)) {

				if (result = EncodeBlock(dst)) {
					out_obj.Set(&_d_buffer, 0);

				}

				if (_decoder.repeater == _decoder.srcsize) {
					_decoder.repeater = 0;
					_decoder.srcsize = 0;
				}
		// reset stats
				System::MemoryFill_SSE3(encoder_hash, 256*(hash_depth+1)*sizeof(UI_64), 0, 0);
				System::MemoryFill_SSE3(stats_table, 640*sizeof(UI_64), 0, 0);

				stats_table[256] = 1;
				
				break;
			}

		}


		_d_buffer.Release();
	}

	return result;
}


void Pack::Deflate::CodeABet(unsigned char * dst_ptr, UI_64 & dst_po) {
	unsigned int i(0), j(0), lp(0);

	for (i=0;i<19;i++) stats_table[480+i] = 0;

// literal
	for (i=0;i<286;) {
		for (j=(i+1);j<286;j++) {
			if (lengths_table[j] != lengths_table[i]) break;
		}

		j -= (i + 1);
		i += j;
		
		++stats_table[480+lengths_table[i]];
		lengths_table[640+lp++] = lengths_table[i];

		
		if (lengths_table[i]) {
			for (;j>0;) {
				if (j > 2) {
					++stats_table[480+16];
					lengths_table[640+lp++] = 16;

					if (j<7) {
						lengths_table[640+lp++] = j-3;
						j = 0;
					} else {
						lengths_table[640+lp++] = 3;
						j -= 6;
					}

				} else {
					for (;j>0;j--) {
						++stats_table[480+lengths_table[i]];
						lengths_table[640+lp++] = lengths_table[i];
					}
				}
			}
		} else {
			for (;j>0;) {
				if (j > 2) {
					if (j<11) {
						++stats_table[480+17];
						lengths_table[640+lp++] = 17;

						lengths_table[640+lp++] = j-3;

						j = 0;
					} else {
						++stats_table[480+18];
						lengths_table[640+lp++] = 18;

						if (j < 138) {
							lengths_table[640+lp++] = j-11;
							j = 0;
						} else {
							lengths_table[640+lp++] = 127;
							j -= 138;
						}
					}
				} else {
					for (;j>0;j--) {
						++stats_table[480];
						lengths_table[640+lp++] = 0;
					}
				}
			}
		}

		i++;
	}
	
// disrtances
	for (i=320;i<350;) {
		for (j=(i+1);j<350;j++) {
			if (lengths_table[j] != lengths_table[i]) break;
		}

		j -= (i + 1);
		i += j;

		++stats_table[480+lengths_table[i]];
		lengths_table[640+lp++] = lengths_table[i];

		if (lengths_table[i]) {
			for (;j>0;) {
				if (j > 2) {
					++stats_table[480+16];
					lengths_table[640+lp++] = 16;

					if (j<7) {
						lengths_table[640+lp++] = j-3;
						j = 0;
					} else {
						lengths_table[640+lp++] = 3;
						j -= 6;
					}

				} else {
					for (;j>0;j--) {
						++stats_table[480+lengths_table[i]];
						lengths_table[640+lp++] = lengths_table[i];
					}
				}
			}
		} else {
			for (;j>0;) {
				if (j > 2) {
					if (j<11) {
						++stats_table[480+17];
						lengths_table[640+lp++] = 17;

						lengths_table[640+lp++] = j-3;

						j = 0;
					} else {
						++stats_table[480+18];
						lengths_table[640+lp++] = 18;

						if (j < 138) {
							lengths_table[640+lp++] = j-11;
							j = 0;
						} else {
							lengths_table[640+lp++] = 127;
							j -= 138;
						}
					}
				} else {
					for (;j>0;j--) {
						++stats_table[480];
						lengths_table[640+lp++] = 0;
					}
				}
			}
		}

		++i;
	}

	for (i=0;i<19;i++) lengths_table[480+i] = 0;

	GenHuffmanTable(reinterpret_cast<unsigned short *>(lengths_table+500), lengths_table+480, stats_table+480, 0x70013);

// output length
	for (unsigned int i(0);i<19;i++) {
		_decoder.byte_val |= (unsigned int)(lengths_table[480+c_l_a[i]] & 7) << _decoder.byte_position;
		_decoder.byte_position += 3;

		if (_decoder.byte_position >= 8) {
			dst_ptr[dst_po++] = _decoder.byte_val & 0xFF;
			_decoder.byte_val >>= 8;
			_decoder.byte_position -= 8;
		}
	}

// code literal & distance
	for (i=0;i<lp;i++) {
		_decoder.byte_val |= (unsigned int)lengths_table[500+2*lengths_table[640+i]] << _decoder.byte_position;
		_decoder.byte_position += lengths_table[480+lengths_table[640+i]];

		switch (lengths_table[640+i]) {
			case 16:				
				_decoder.byte_val |= (unsigned int)lengths_table[++i+640] << _decoder.byte_position;
				_decoder.byte_position += 2;
			break;
			case 17:
				_decoder.byte_val |= (unsigned int)lengths_table[++i+640] << _decoder.byte_position;
				_decoder.byte_position += 3;
			break;
			case 18:
				_decoder.byte_val |= (unsigned int)lengths_table[++i+640] << _decoder.byte_position;
				_decoder.byte_position += 7;
			break;		

		}

		for (;_decoder.byte_position >= 8; _decoder.byte_position -= 8) {
			dst_ptr[dst_po++] = _decoder.byte_val & 0xFF;
			_decoder.byte_val >>= 8;			
		}
	}
}

void Pack::Deflate::SetFixedCodes(unsigned short * li_codes, unsigned short * di_codes, unsigned char * le_table) {
	unsigned short co_mi(0x0030);

	for (unsigned int i(0);i<144;i++) {
		li_codes[i] = co_mi++;
		le_table[i] = 8;
	}

	co_mi = 0x0190;
	for (unsigned int i(144);i<256;i++) {
		li_codes[i] = co_mi++;
		le_table[i] = 9;
	}

	co_mi = 0;
	for (unsigned int i(256);i<280;i++) {
		li_codes[i] = co_mi++;
		le_table[i] = 7;
	}

	co_mi = 0x00C0;
	for (unsigned int i(280);i<288;i++) {
		li_codes[i] = co_mi++;
		le_table[i] = 8;
	}

	for (unsigned short i(0);i<30;i++) {
		di_codes[i] = i;
		le_table[i+320] = 5;
	}
}

UI_64 Pack::Deflate::EncodeBlock(unsigned short * src) {
	unsigned int eb_val(0);
	UI_64 result(0);
	UI_64 xbyte_val(0);
	unsigned char * dst_ptr = reinterpret_cast<unsigned char *>(src), e_bits(0);
	unsigned short high_val(0);

	src += 1024;
	_decoder.buffer_runner -= 1024;

// generate tables
	
	GenHuffmanTable(literal_table, lengths_table, stats_table, 0xF011E);
	GenHuffmanTable(distance_table, lengths_table+320, stats_table+580, 0xF001E);

// start coding		

// header, type 10 only

	_decoder.byte_val |= (0x1FDEC | lengths_table[1027]) << _decoder.byte_position;
	_decoder.byte_position += 17;

	for (;_decoder.byte_position >= 8; _decoder.byte_position -= 8) {
		dst_ptr[result++] = _decoder.byte_val & 0xFF;
		_decoder.byte_val >>= 8;			
	}


// alphabet
	CodeABet(dst_ptr, result);
	
	xbyte_val = _decoder.byte_val;
// data
	for (UI_64 si(0);si<_decoder.buffer_runner;si++) {
		if (src[si] & 0x8000) {
			// literal
			xbyte_val |= (UI_64)literal_table[src[si] & 0x7FFF] << _decoder.byte_position;
			_decoder.byte_position += lengths_table[src[si] & 0x7FFF];
		} else {
			// length
			e_bits = (src[si+2] & 0xFF);
			xbyte_val |= (UI_64)literal_table[257+e_bits] << _decoder.byte_position;
			_decoder.byte_position += lengths_table[257+e_bits];
			// extra bits

			if ((e_bits>7) && (e_bits<28)) {
				e_bits -= 4;
				eb_val = (e_bits & 3) + 4;
				e_bits >>= 2;

				eb_val = (eb_val << e_bits) + 3;

				eb_val = src[si] - eb_val;
				xbyte_val |= (UI_64)eb_val << _decoder.byte_position;
				_decoder.byte_position += e_bits;
			}

			// distance
			e_bits = (src[si+2]>>8);
			xbyte_val |= (UI_64)distance_table[e_bits] << _decoder.byte_position;
			_decoder.byte_position += lengths_table[320+e_bits];
			// extra bits
			if (e_bits>3) {
				e_bits -= 2;
				eb_val = (e_bits & 1) + 2;
				e_bits >>= 1;

				eb_val = (eb_val << e_bits) + 1;

				eb_val = src[si+1] - eb_val;
				xbyte_val |= (UI_64)eb_val << _decoder.byte_position;
				_decoder.byte_position += e_bits;

			}
			
			si += 2;

		}

		for (;_decoder.byte_position >= 8; _decoder.byte_position -= 8) {
			dst_ptr[result++] = xbyte_val & 0xFF;
			xbyte_val >>= 8;			
		}

	}

// end of data
	_decoder.byte_val = xbyte_val;

	_decoder.byte_val |= (unsigned int)literal_table[256] << _decoder.byte_position;
	_decoder.byte_position += lengths_table[256];

	if (lengths_table[1027]) _decoder.byte_position += 7;

	for (;_decoder.byte_position >= 8; _decoder.byte_position -= 8) {
		dst_ptr[result++] = _decoder.byte_val & 0xFF;
		_decoder.byte_val >>= 8;			
	}


	return result;
}



void Pack::Deflate::SetTables(unsigned char * clc, unsigned short li_count, unsigned short di_count) {
	unsigned short kk(0), ll(0), mm(0);
	
	_decoder.literal_codes = literal_table;
	_decoder.distance_codes = distance_table;
	
// literal table

	for (unsigned short i(0);i<li_count;i++) {
		if (clc[i]) clc[383+clc[i]]++;
	}

	kk = 0;
	ll = 128;
	for (unsigned char i(0);i<15;i++) { // codes
		mm = clc[i+384];
		if (mm) {
			literal_table[4*i+1] = kk;
			kk += mm;

			literal_table[4*i] = kk;
			literal_table[4*i+2] = ll - i*8;
							
			ll += (mm << 1);
						
		}

		kk <<= 1;
	}

	kk = 0;
	for (unsigned char k(1);k<16;k++) { // table
		for (unsigned short i(0);i<li_count;i++) {
			if (clc[i] == k) {
				literal_table[64+kk++] = i;
				if (--clc[k+383] == 0) break;
			}
		}
	}

// distance table

	for (unsigned short i(0);i<di_count;i++) {
		if (clc[i+li_count]) clc[383+clc[i+li_count]]++;		
	}

	kk = 0;
	ll = 128;
	for (unsigned short i(0);i<15;i++) { // codes
		mm = clc[i+384];
		if (mm) {
			distance_table[4*i+1] = kk;
			kk += mm;

			distance_table[4*i] = kk;
			distance_table[4*i+2] = ll - i*8;
							
			ll += (mm << 1);
						
		}

		kk <<= 1;
	}

	kk = 0;
	for (unsigned char k(1);k<16;k++) { // table
		for (unsigned short i(0);i<di_count;i++) {
			if (clc[i+li_count] == k) {
				distance_table[64+kk++] = i;
				if (--clc[k+383] == 0) break;
			}
		}
	}


}

void Pack::Deflate::SetUSource(const unsigned char * src, UI_64 srcsize, bool l_chunk) {
	_decoder.srcptr = src;
	_decoder.srcsize = srcsize;

	lengths_table[1027] = l_chunk;
}

// -3 buffer too small
// -2 corrupted file
// -1 success

/*
										result = 0x00200000 - Pack::Deflate::search_buffer_size - result;
										blopo = strpo + Pack::Deflate::search_buffer_size;


*/

UI_64 Pack::Deflate::UnPack(SFSco::Object & out_obj) {
	unsigned char block_type(0);
	unsigned char kk(0), ll(0); // clc[1024], 
	UI_64 oval(0), result(0), dst_size(decoder_buffer_size - _decoder.buffer_runner);

	if (!_d_buffer) return -1;
	
	out_obj.Blank();
	
	if (unsigned char * dst = reinterpret_cast<unsigned char *>(_d_buffer.Acquire())) {		

		if (_decoder.end_of_block & 1) _decoder.stage = 0;

		switch (_decoder.stage) {
			case 10: goto decode_it_label;
			case 1: goto code_length_read_label;
			case 2: goto alpha_length_read_label;
			case 101: goto plain_copy_label1;
			case 102: goto plain_copy_label2;
		}

		for (;;) {
			if (!_decoder.srcsize) break;

			if (_decoder.end_of_block & 1) {
				_decoder.stage = 0;

				if (!_decoder.byte_position) {
					block_type = _decoder.srcptr[0] & 0x07;				

					_decoder.byte_val = (_decoder.srcptr[0] >> 3);				
					_decoder.byte_position = 5;

					_decoder.srcptr++;
					_decoder.srcsize--;

				} else {
					if (_decoder.byte_position < 3) {
						block_type = (_decoder.srcptr[0] << _decoder.byte_position) & 0x07;					
						block_type |= _decoder.byte_val;

						_decoder.byte_position = 3 - _decoder.byte_position;

						_decoder.byte_val = (_decoder.srcptr[0] >> _decoder.byte_position);
						_decoder.byte_position = 8 - _decoder.byte_position;
					
						_decoder.srcptr++;
						_decoder.srcsize--;
					} else {
						block_type = _decoder.byte_val & 0x07;
										
						_decoder.byte_val >>= 3;
						_decoder.byte_position -= 3;

					}
				}

				_decoder.end_of_block = ((block_type & 1) << 7);

				if (block_type & 6) {
					System::MemoryFill(literal_table, 640*sizeof(unsigned short), 0);
					System::MemoryFill(distance_table, 128*sizeof(unsigned short), 0);

					if ((block_type & 6) == 4) {					
						System::MemoryFill(lengths_table, 1024, 0);
					
						_decoder.stage = 1;
						_decoder._reserved = 0;
						_decoder.repeater = 0;
						_decoder.literal_codes = 0;

				code_length_read_label:
						if (Deflate_GetCodeLengthCodes(_decoder, lengths_table) < 4) {
							result = -256;
							goto proper_exit;
						}

					

						for (unsigned char i(0);i<19;i++) {
							if (lengths_table[i]) lengths_table[31+lengths_table[i]]++;
						}

						kk = 0;
						ll = 32;
						for (unsigned char i(0);i<7;i++) { // codes
							if (lengths_table[i+32]) {
								lengths_table[4*i+65] = kk;
								kk += lengths_table[i+32];

								lengths_table[4*i+64] = kk;
								lengths_table[4*i+66] = ll - i*4;
							
								ll += lengths_table[i+32];
														
							}

							kk <<= 1;
						}

						kk = 0;
						for (unsigned char k(1);k<8;k++) { // table
							for (unsigned char i(0);i<19;i++) {
								if (lengths_table[i] == k) {
									lengths_table[96+kk++] = i;
									if (--lengths_table[k+31] == 0) break;
								}

							}
						}

						_decoder.stage = 2;
						_decoder._reserved = 0;
						_decoder.repeater = 0;

					alpha_length_read_label:
						oval = Deflate_GetLiDiLengths(_decoder, lengths_table);
						if (oval != -1) {
							if (oval != 8) result = -257;
							else result = -2;

							goto proper_exit;
						}
										
						SetTables(lengths_table+128, reinterpret_cast<unsigned short *>(lengths_table+20)[0], reinterpret_cast<unsigned short *>(lengths_table+20)[1]);								

					} else {
					// fixed codes

						literal_table[24] = 0x0018;
						literal_table[25] = 0;
						literal_table[26] = 80;

						literal_table[28] = 0x00C6;
						literal_table[29] = 0x0030;
						literal_table[30] = 120;

						literal_table[32] = 0x0200;
						literal_table[33] = 0x0190;
						literal_table[34] = 412;

						for (unsigned short i(0);i<24;i++) literal_table[i+64] = i+256;
					
						for (unsigned short i(0);i<144;i++) literal_table[i+88] = i;
						for (unsigned short i(0);i<6;i++) literal_table[i+232] = i+280;

						for (unsigned short i(0);i<112;i++) literal_table[i+238] = i+144;

					

						distance_table[16] = 30;
						distance_table[17] = 0;
						distance_table[18] = 96;

						for (unsigned short i(0);i<30;i++) distance_table[i+64] = i;
										
					}
				} else {
					_decoder.repeater = _decoder.byte_val;
					_decoder.byte_val = 0;

					if (_decoder.byte_position > 7) {
						_decoder.byte_position >>= 3;

						_decoder.repeater &= ((1 << 8*_decoder.byte_position) - 1);
					} else {
						_decoder.byte_position = 0;						
					}


					if (_decoder.srcsize <= 4) {
						_decoder.stage = 101;

						_decoder.srcsize += _decoder.byte_position;
						for (unsigned char i(_decoder.byte_position);i<_decoder.srcsize;i++) {
							_decoder.repeater |= ((unsigned int)_decoder.srcptr[0] << (i*8));							
							_decoder.srcptr++;
						}

						_decoder.byte_position = _decoder.srcsize;
						_decoder.srcsize = 0;

						result = - 258;
						goto proper_exit;
					} else {
				plain_copy_label1:
						if (_decoder.srcsize<=4) {
							result = -3;
							goto proper_exit;
						}
				
						for (unsigned char i(_decoder.byte_position);i<4;i++) {
							_decoder.repeater |= ((unsigned int)_decoder.srcptr[0] << (i*8));							
							_decoder.srcptr++;
							_decoder.srcsize--;
						}

						if (_decoder.byte_position<4) _decoder.byte_position = 4;
						
						if (oval = _decoder.repeater & 0xFFFF) {

							_decoder.repeater >>= 32;

							if (_decoder.byte_position -= 4) {
								reinterpret_cast<UI_64 *>(dst + _decoder.buffer_runner)[0] = _decoder.repeater;
								oval -= _decoder.byte_position;
								dst_size -= _decoder.byte_position;
								_decoder.buffer_runner += _decoder.byte_position;
							}
						
							_decoder.byte_position = 0;
							_decoder.stage = 102;
							_decoder.repeater = oval;

						plain_copy_label2:
							oval = _decoder.repeater;
							if (_decoder.srcsize < _decoder.repeater) {							
								oval = _decoder.srcsize;
							}				
					
							System::MemoryCopy_SSE3(dst + _decoder.buffer_runner, _decoder.srcptr, oval);
							dst_size -= oval;
							_decoder.buffer_runner += oval;

							_decoder.srcptr += oval;
							_decoder.srcsize -= oval;

							if (_decoder.repeater -= oval) {							
								result = -259;
								goto proper_exit;
							} else {							
								_decoder.end_of_block |= 1;
							}

						} else {
							_decoder.byte_position = 0;
							_decoder.end_of_block |= 1;
						}

						if (_decoder.end_of_block == 129) {
							result = (_decoder.buffer_runner-_decoder.range_start);
							goto proper_exit;
						}
					
						continue;
					}
				}
			}

			_decoder.stage = 10;		
			_decoder._reserved = 0;
			_decoder.repeater = 0;

		decode_it_label:
			

			if (!Deflate_Decode(_decoder, dst + _decoder.buffer_runner, dst_size)) {
				Reset();
				result = -4;
				goto proper_exit;
			}

			_decoder.buffer_runner = decoder_buffer_size - dst_size;


			if (_decoder.end_of_block == 129) break;
		
			if (dst_size < 0x10000) break;
			
		}
	
		out_obj.Set(&_d_buffer, _decoder.range_start);
		result = _decoder.buffer_runner - _decoder.range_start;		

		if (dst_size < 0x10000) {			
			Deflate_CopySB(dst+search_buffer_size, dst+_decoder.buffer_runner);
			_decoder.buffer_runner = search_buffer_size;
	
		}

		_decoder.range_start = _decoder.buffer_runner;


	proper_exit:
		_d_buffer.Release();

	}

	
	
	return result;
}

UI_64 Pack::Deflate::Success() {
	return 16*(_decoder.end_of_block == 129) + (_decoder.srcsize == 0);
}

