#pragma once

#include "CoreLibrary\SFSCore.h"

namespace Pack {
/*
	outputs code_length_counts at	stats_table[a_size+code_length]
*/

	unsigned short GenHuffmanTable(unsigned short * code_table, unsigned char * length_table, UI_64 * stats_table, unsigned int aa_val);


	class StreamReader {
		public:
			const unsigned char * src_ptr;
			__int64 src_size;

			unsigned __int64 bit_mask[4];


			StreamReader() : src_ptr(0), src_size(0) {
				bit_mask[0] = 0x00000080;
				bit_mask[1] = 0;
				bit_mask[2] = 0;
				bit_mask[3] = 8;
			};

			StreamReader & operator =(const StreamReader & ref_reader) {
				src_ptr = ref_reader.src_ptr;
				src_size = ref_reader.src_size;

				bit_mask[0] = ref_reader.bit_mask[0];
				bit_mask[1] = ref_reader.bit_mask[1];
				bit_mask[2] = ref_reader.bit_mask[2];
				bit_mask[3] = ref_reader.bit_mask[3];

				return *this;
			};

			StreamReader(const StreamReader & ref_reader) {
				*this = ref_reader;
			};


			__forceinline unsigned char NextBit() {
				unsigned char result(0);

				if (src_size > 0) {
					result = 1 + (((bit_mask[0] & bit_mask[1]) - 1) >> 7);

					if ((bit_mask[0] >>= 1) == 0) {
						bit_mask[0] = 0x00000080;

						src_ptr++;
						src_size--;

						bit_mask[1] <<= 8;
						bit_mask[1] |= src_ptr[0];

						bit_mask[3] = 9;
					}

					bit_mask[3]--;
				}

				return result;
			};

			__forceinline unsigned __int64 GetNumber(int bit_count) {
				unsigned __int64 result(0);

				if (src_size > 0) {
					for (;bit_count > bit_mask[3];bit_mask[3] += 8) {
						src_ptr++;
						src_size--;

						bit_mask[0] <<= 8;
						bit_mask[1] <<= 8;
						bit_mask[1] |= src_ptr[0];

					}

					result = (bit_mask[0] << 1) - 1;
					result &= bit_mask[1];

					bit_mask[3] -= bit_count;

					result >>= bit_mask[3];

							

					if ((bit_mask[0] >>= bit_count) == 0) {
						bit_mask[0] = 0x00000080;

						src_ptr++;
						src_size--;

						bit_mask[1] <<= 8;
						bit_mask[1] |= src_ptr[0];

						bit_mask[3] = 8;
					}
				}

				return result;
			};

			__forceinline short GetHuffmaVal(const unsigned int huf_tab[][2], unsigned int mco_le) {
				unsigned int code_val(0); // , code_bit(1);
				short result(0);

				if (src_size >= 0) {
					code_val = NextBit();

					for (unsigned int i(0);i<mco_le;i++) {
						result += huf_tab[i][0];

						if (code_val < huf_tab[i][1]) {
							result -= (huf_tab[i][1] - code_val);
							return result;
									
						} else {
							code_val <<= 1;
							code_val |= NextBit();
						}
					}
				}

				return 0;
			};


			__forceinline unsigned char NextByte() {
				unsigned char result(0);

				if (src_size > 0) {
					result = src_ptr[0];

					src_ptr++;
					src_size--;

					bit_mask[0] = 0x00000080;

					bit_mask[1] <<= 8;
					bit_mask[1] |= src_ptr[0];

					bit_mask[3] = 8;
													
				}

				return result;
			};

			__forceinline void Skip(unsigned int s_val) {
				if (src_size > s_val) {
					src_ptr += s_val;
					src_size -= s_val;

					bit_mask[1] = src_ptr[0];
				} else {
					src_size = 0;
				}
			};

			__forceinline void SkipBits(short bcount) {
				short by_skip(0);
				if (src_size > 0) {
					if (bit_mask[3] < bcount) {
						bcount -= bit_mask[3];

						by_skip = (bcount>>3);
						bcount &= 7;
						bit_mask[3] = 8 - bcount;

						if (by_skip < src_size) {
							src_ptr += by_skip;
							src_size -= by_skip;
						}

						src_ptr++;
						src_size--;

						bit_mask[0] = (0x80 >> bcount);
						bit_mask[1] = src_ptr[0];
								
					} else {
						GetNumber(bcount);
					}							
				}
			};


			__forceinline void ByteAlign() {
				if (src_size > 0) {
					if (bit_mask[0] ^ 0x00000080) {
						bit_mask[0] = 0x00000080;

						src_ptr++;
						src_size--;

						bit_mask[0] = 0x00000080;

						bit_mask[1] <<= 8;
						bit_mask[1] |= src_ptr[0];
								
						bit_mask[3] = 8;
					}
				}
			};

			__forceinline void SetSource(const unsigned char * sptr, UI_64 sval) {						
				src_ptr = sptr;
				bit_mask[0] = 0x00000080;
				bit_mask[1] = sptr[0];
				bit_mask[2] = 0;
				bit_mask[3] = 8;
				src_size = sval;
			};

			__forceinline bool IsSource(unsigned int bco) {
				return ((bco + 8) <= ((src_size<<3) + bit_mask[3]));

			}


	};


}

