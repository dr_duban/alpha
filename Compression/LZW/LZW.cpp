/*
** safe-fail compression
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>
#include "Compression\LZW.h"

#include "System\SysUtils.h"

Pack::LZW::LZW() {
	_dictionary.top = 0;
}

Pack::LZW::~LZW() {
	_d_buffer.Destroy();
	_dic_buffer.Destroy();
}


UI_64 Pack::LZW::Reset(unsigned char mcl, unsigned char order, bool clear_val) {
	UI_64 result(0);

								
	_decoder.srcptr = 0;
	_decoder.srcsize = 0;

	_decoder.byte_val = 0;

	_decoder.byte_position = 0;
	_decoder.stage = 0;

	_decoder.fill_order = order & 128;
	_decoder.early_change = order & 16;

	if (mcl < 16) {
		_dictionary.min_code_length = mcl;
		
		if (clear_val) {
			_decoder.reset_val = (1 << mcl);
			_decoder.end_ofs_val = _decoder.reset_val + 1;
			_dictionary.codes_count = _decoder.end_ofs_val + 1;

			_dictionary.min_code_length++;
			
		} else {
			_decoder.reset_val = -1;
			_decoder.end_ofs_val = -1;
			_dictionary.codes_count = 1 << mcl;
			
		}

		_dictionary.current_code_length = _dictionary.min_code_length;
	}

	_dictionary.code_mask = (1 << _dictionary.current_code_length) - 1;

	_decoder.code_val = 0;

	_decoder.buffer_runner = _decoder.range_start = 0;
	_dictionary.maximum_word_length = 1;
	_decoder.last_word_length = 0;

	System::MemoryFill(code_table, 8192*sizeof(UI_64), 0);

	for (unsigned short i(0);i<_dictionary.codes_count;i++) {
		code_table[i][0] = i;
		code_table[i][1] = 1;

	}


	if (!_d_buffer) result = _d_buffer.New(0, decoder_buffer_size);
	if (!_dic_buffer) result |= _dic_buffer.New(0, di_buffer_size);
	
	_dictionary.runner = 0;
	if (result != -1) {
		if (!_dictionary.top) _dictionary.top = di_buffer_size;

		if (unsigned char * dipo = reinterpret_cast<unsigned char *>(_dic_buffer.Acquire())) {
			for (unsigned short i(0);i<_dictionary.codes_count;i++) dipo[i] = i;
			_dictionary.runner = _dictionary.codes_count;

			_dic_buffer.Release();
		}

	} else _dictionary.top = 0;

	return result;

}

void Pack::LZW::SetUSource(const unsigned char * src, UI_64 srcsize) {
	_decoder.srcptr = src;
	_decoder.srcsize = srcsize;

}

UI_64 Pack::LZW::Pack() {

	return 0;
}


UI_64 Pack::LZW::UnPack(SFSco::Object & out_obj) {
	UI_64 result(0), tval(0);
	unsigned char boff(0);

	if ((!_d_buffer) || (!_dic_buffer)) return -1;
	
	out_obj.Blank();


	if (unsigned char * dst = reinterpret_cast<unsigned char *>(_d_buffer.Acquire())) {
		if (unsigned char * dicpo = reinterpret_cast<unsigned char *>(_dic_buffer.Acquire())) {

			dst += _decoder.buffer_runner;

			if (_decoder.stage == 2) {
				goto lzw_stage_2;
			}

			for (;;) {
				_decoder.stage = 1;

				for (;_decoder.byte_position<_dictionary.current_code_length;) {
					if (_decoder.srcsize == 0) {
						result = -256;
						goto proper_exit;
					}

					if (_decoder.fill_order == 0) _decoder.byte_val |= ((unsigned int)_decoder.srcptr[0] << _decoder.byte_position);
					else {
						_decoder.byte_val <<= 8;
						_decoder.byte_val |= (unsigned int)_decoder.srcptr[0];
					}
					
					_decoder.byte_position += 8;
					_decoder.srcptr++;
					
					_decoder.srcsize--;
				}


				if (_decoder.fill_order == 0) {
					_decoder.code_val = _decoder.byte_val & _dictionary.code_mask;
					_decoder.byte_val >>= _dictionary.current_code_length;
				} else {
					boff = (_decoder.byte_position - _dictionary.current_code_length);
					_decoder.code_val = _decoder.byte_val >> boff;
					_decoder.byte_val ^= ((unsigned int)_decoder.code_val << boff);
				}
				
				_decoder.byte_position -= _dictionary.current_code_length;
				
				if (_decoder.code_val == _decoder.reset_val) {
					_dictionary.current_code_length = _dictionary.min_code_length;
					_dictionary.code_mask = (1 << _dictionary.current_code_length) - 1;
					_dictionary.codes_count = _decoder.end_ofs_val + 1;					
					_dictionary.runner = _dictionary.codes_count;

					_dictionary.maximum_word_length = 1;
					_decoder.last_word_length = 0;

				} else {
					if (_decoder.code_val == _decoder.end_ofs_val) {
						_decoder.byte_position = 0;
						_decoder.byte_val = 0;
						_decoder.stage = 100;
						
						break;
					} else {
						_decoder.stage = 2;

					lzw_stage_2:

						if ((_dictionary.maximum_word_length + _decoder.buffer_runner) < decoder_buffer_size) {		
							if (_decoder.code_val == _dictionary.codes_count) {
								for (UI_64 i(0);i<_decoder.last_word_length;i++) {
									dst[i] = dst[i-_decoder.last_word_length];
								}
								dst[_decoder.last_word_length] = dst[0];


							} else {
								for (UI_64 i(0);i<code_table[_decoder.code_val][1];i++) {
									dst[i] = dicpo[code_table[_decoder.code_val][0]+i];
								}
							}

							if (_decoder.last_word_length) {
								// dictionary update
								tval = _decoder.last_word_length << 1;
								if ((tval + _dictionary.runner) > _dictionary.top) {
									// expand dictionary
									_dic_buffer.Release();
									_d_buffer.Release();

									if (tval < di_buffer_size) tval = di_buffer_size;

									if (_dic_buffer.Expand(tval) != -1) {
										if (dst = reinterpret_cast<unsigned char *>(_d_buffer.Acquire())) {
											dst += _decoder.buffer_runner;
											if (dicpo = reinterpret_cast<unsigned char *>(_dic_buffer.Acquire())) {

											} else {
												_d_buffer.Release();

												_decoder.stage = 0;
												result = -6;
												goto proper_exit;

											}
										} else {
											_decoder.stage = 0;
											result = -5;
											goto proper_exit;
										}

										_dictionary.top += tval;
									} else {
										_decoder.stage = 0;
										result = -4;
										goto proper_exit;
									}

								}

								code_table[_dictionary.codes_count][0] = _dictionary.runner;
								code_table[_dictionary.codes_count][1] = _decoder.last_word_length+1;

								if (code_table[_dictionary.codes_count][1] > _dictionary.maximum_word_length) _dictionary.maximum_word_length = code_table[_dictionary.codes_count][1];


								for (UI_64 ii(0);ii<=_decoder.last_word_length;ii++) {
									dicpo[_dictionary.runner+ii] = dst[ii-_decoder.last_word_length];
								}

								_dictionary.runner += code_table[_dictionary.codes_count][1];

								if (_decoder.early_change == 0) {
									if (++_dictionary.codes_count > _dictionary.code_mask) {
										if (_dictionary.current_code_length < 12) {
											_dictionary.code_mask <<= 1;
											_dictionary.code_mask |= 1;

											_dictionary.current_code_length++;
										}
									}
								} else {
									if (++_dictionary.codes_count >= _dictionary.code_mask) {
										if (_dictionary.current_code_length < 12) {
											_dictionary.code_mask <<= 1;
											_dictionary.code_mask |= 1;

											_dictionary.current_code_length++;
										}
									}

								}
							}

							dst += code_table[_decoder.code_val][1];

							_decoder.buffer_runner += code_table[_decoder.code_val][1];
							_decoder.last_word_length = code_table[_decoder.code_val][1];
														
						} else {
							break;
						}
						
					}					
					
				}

			}

			out_obj.Set(&_d_buffer, _decoder.range_start);
			result = _decoder.buffer_runner - _decoder.range_start;
			
			if (_decoder.buffer_runner == decoder_buffer_size) _decoder.buffer_runner = 0;

			_decoder.range_start = _decoder.buffer_runner;

	proper_exit:

			_dic_buffer.Release();
		}

		_d_buffer.Release();
	}

	return result;
}

UI_64 Pack::LZW::GetData(SFSco::Object & out_obj) {
	UI_64 result = _decoder.buffer_runner - _decoder.range_start;
	
	out_obj.Set(&_d_buffer, _decoder.range_start);
	
	_decoder.range_start = _decoder.buffer_runner;

	return result;
}

UI_64 Pack::LZW::Success() {
	return (16*(_decoder.stage == 100) + (_decoder.srcsize == 0));
}

/*

											0, 128, 64, 192, 32, 160, 96, 224, 16, 144, 80, 208, 48, 176, 112, 240,
											8, 136, 72, 200, 40, 168, 104, 232, 24, 152, 88, 216, 56, 184, 120, 248,
											4, 132, 68, 196, 36, 164, 100, 228, 20, 148, 84, 212, 52, 180, 116, 244,
											12, 140, 76, 204, 44, 172, 108, 236, 28, 156, 92, 220, 60, 188, 124, 252,
											
											2, 130, 66, 194, 34, 162, 98, 226, 18, 146, 82, 210, 50, 178, 114, 242,
											10, 138, 74, 202, 42, 170, 106, 234, 26, 154, 90, 218, 58, 186, 122, 250,
											6, 134, 70, 198, 38, 166, 102, 230, 22, 150, 86, 214, 54, 182, 118, 246,
											14, 142, 78, 206, 46, 174, 110, 238, 30, 158, 94, 222, 62, 190, 126, 254,

											1, 129, 65, 193, 33, 161, 97, 225, 17, 145, 81, 209, 49, 177, 113, 241,
											9, 137, 73, 201, 41, 169, 105, 233, 25, 153, 89, 217, 57, 185, 121, 249,
											5, 133, 69, 197, 37, 165, 101, 229, 21, 149, 85, 213, 53, 181, 117, 245,
											13, 141, 77, 205, 45, 173, 109, 237, 29, 157, 93, 221, 61, 189, 125, 253,

											3, 131, 67, 195, 35, 163, 99, 227, 19, 147, 83, 211, 51, 179, 115, 243,
											11, 139, 75, 203, 43, 171, 107, 235, 27, 155, 91, 219, 59, 187, 123, 251,
											7, 135, 71, 199, 39, 167, 103, 231, 23, 151, 87, 215, 55, 183, 119, 247,
											15, 143, 79, 207, 47, 175, 111, 239, 31, 159, 95, 223, 63, 191, 127, 255



*/
