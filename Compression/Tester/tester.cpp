#include <Windows.h>

#include "Compression\Deflate.h"



int wmain(int argc, wchar_t ** argv) {
	const wchar_t up_name[] = L"trench100free.otf";
	wchar_t pck_name[255];
	LARGE_INTEGER f_size;
	DWORD bcount(0);

	size_t result = SFSco::Initialize(), i_name(0);

	if (result != -1) {
		SFSco::Object out_obj;
		if (Pack::Deflate * deflator = new Pack::Deflate()) {
			deflator->Reset();
/*
	//		if (argc > 1) {
				i_name = wcslen(up_name); // argv[1]
				memcpy(pck_name, up_name, i_name*sizeof(wchar_t)); // argv[1]
				pck_name[i_name] = L'.'; pck_name[i_name+1] = L'd'; pck_name[i_name+2] = L'f'; pck_name[i_name+3] = L'l'; pck_name[i_name+4] = 0;

				HANDLE s_file = CreateFile(up_name, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0); // argv[1]

				result = 0;

				if (s_file != INVALID_HANDLE_VALUE) {
					GetFileSizeEx(s_file, &f_size);
					if (HANDLE f_map = CreateFileMapping(s_file, 0, PAGE_READONLY, 0, 0, 0)) {
						if (void * src_ptr = MapViewOfFile(f_map, FILE_MAP_READ, 0, 0, 0)) {
							deflator->SetUSource(reinterpret_cast<unsigned char *>(src_ptr), f_size.QuadPart, true);
							result = deflator->Pack(out_obj);

							UnmapViewOfFile(src_ptr);
						}

						CloseHandle(f_map);
					}

					CloseHandle(s_file);
				}


				if (result) {
					HANDLE o_file = CreateFile(pck_name, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

					if (o_file != INVALID_HANDLE_VALUE) {
						if (void * o_ptr = out_obj.Acquire()) {
							WriteFile(o_file, o_ptr, result, &bcount, 0);
							
							out_obj.Release();
						}


						CloseHandle(o_file);
					}
				}
	//		}
*/



			HANDLE s_file = CreateFile(L"mb_map", GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

			if (s_file != INVALID_HANDLE_VALUE) {
				GetFileSizeEx(s_file, &f_size);
				if (HANDLE f_map = CreateFileMapping(s_file, 0, PAGE_READONLY, 0, 0, 0)) {
					if (void * src_ptr = MapViewOfFile(f_map, FILE_MAP_READ, 0, 0, 0)) {
						deflator->SetUSource(reinterpret_cast<unsigned char *>(src_ptr), f_size.QuadPart, true);
						result = deflator->Pack(out_obj);

						UnmapViewOfFile(src_ptr);
					}

					CloseHandle(f_map);
				}

				CloseHandle(s_file);
			}

			if (result) {
				HANDLE o_file = CreateFile(L"mb_map_pk", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

				if (o_file != INVALID_HANDLE_VALUE) {
					if (void * o_ptr = out_obj.Acquire()) {
						WriteFile(o_file, o_ptr, result, &bcount, 0);
							
						out_obj.Release();
					}


					CloseHandle(o_file);
				}
			}


			out_obj.Destroy();
			delete deflator;
		}

		SFSco::Finalize();
	}


//	system("pause");

	return result;
}

