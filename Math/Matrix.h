/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "System\Memory.h"
#include "Math\Calc.h"

namespace Calc {
// column-major everything

class VectorF;




// column major matrix class =====================================================================
class MatrixF : public SFSco::Object {	
	public:
		struct Header {
	//		static const UI_64 _elements_offset = 32; // 16*((sizeof(Header)+15)/16)
		
			unsigned int roCount, divR, coCount, divC, rank;
	
			float det;

			unsigned int t_flag, reserved;

		};
	protected:
		Header * Init(void *, unsigned int rowc, unsigned int colc);

		void Init(Header * hdr, const float * src);
		
		
	private:

		
	public:
		static const UI_64 type_id = 5;

		unsigned int GetColumnCount() const;
		unsigned int GetRowCount() const;

		void SetColumnCount(unsigned int colC); // unsafe 
		
		unsigned int GetRank() const; // only if calculated previously by inverse operation, value is ivalided by any arythmec operation (most likely)
		float Determinant() const; // same as rank

		float * const GetElements() const;

// constructors 

		MatrixF();
		MatrixF(const MatrixF &);
		virtual ~MatrixF();


	// initializers
		
		UI_64 Create(unsigned int rowC, unsigned int colC, const float * src = 0, SFSco::IAllocator * meall = SFSco::mem_mgr);
		UI_64 Create(const MatrixF &);

		UI_64 Create(Object * container, UI_64 offset, unsigned int rowC, unsigned int colC);
		UI_64 Create(const VectorF &, unsigned int coCount);
		
		MatrixF & Zero(float = 0.0f);
		MatrixF & Identity(float = 1.0f);
		MatrixF & Translate(const VectorF &);
		MatrixF & Reflect(const VectorF &); // Householder reflection matrix
		MatrixF & Rotate(float angle, const VectorF &); // counter close wise rotation  VectorF(0.0f, 0.0f, 1.0f)


	// operations
		VectorF operator[](unsigned int);

		MatrixF & operator =(const MatrixF &);

		MatrixF & operator -=(const MatrixF &);
		MatrixF & operator +=(const MatrixF &);
		MatrixF & operator *=(const MatrixF &);
		
		MatrixF & operator *=(const float &);
		MatrixF operator *(const float &) const;

		MatrixF & operator -();
	
		MatrixF & operator !();

		MatrixF operator -(const MatrixF &) const;
		MatrixF operator +(const MatrixF &) const;
		MatrixF operator *(const MatrixF &) const;
				
		virtual UI_64 IsIdentity();
	// diagonal multiply
		MatrixF	operator /(const VectorF &) const;
		MatrixF	& operator /=(const VectorF &);


	// apply		
		MatrixF & operator >>(MatrixF &);
	// transpose apply		
		MatrixF & operator ()(MatrixF &);
	// inverse apply		
		MatrixF & operator [](MatrixF &);

	// transpose-multiply
		MatrixF & operator ^=(const MatrixF &);
		MatrixF operator ^(const MatrixF &) const;
	
	// inverse-multiply
		MatrixF & operator %=(const MatrixF &);
		MatrixF operator %(const MatrixF &);


		VectorF operator *(const VectorF &) const;
		VectorF & operator >>(VectorF &);

		VectorF operator ^(const VectorF &) const;
		VectorF & operator ()(VectorF &);

		VectorF operator %(const VectorF &);
		VectorF & operator [](VectorF &);


		// positive definite? how to dermine with full pivoting?

		// QR factorization: implement or not?

		
};

// column vector class ============================================================================
class VectorF: protected MatrixF {
	protected:
		Header * Init(char *, unsigned int rowc, unsigned int);
		void Init(Header * hdr, const float * src);
	private:


	public:
		static const UI_64 type_id = 7841*MatrixF::type_id;

		void * Acquire(void ** = 0);
		void Release(void * = 0);

		unsigned int GetComponentCount() const;
		float * const GetComponents() const;

		VectorF();
		VectorF(const VectorF &);

		virtual ~VectorF();

		// initializers 
		UI_64 Create(unsigned int cmp, float * src = 0, SFSco::IAllocator * alla = SFSco::mem_mgr);
		UI_64 Create(Object * container, UI_64 offset, unsigned int cmp, unsigned int t_flag = 0);
		
		UI_64 Create(unsigned int cmp, float scale);
		UI_64 Create(float, float, float, float = 1.0f);
		

		void Normalize();

		Single operator[](unsigned int);
		
		VectorF operator +(const VectorF &) const;
		VectorF operator -(const VectorF &) const;
		
		VectorF & operator =(const VectorF &);
		VectorF & operator +=(const VectorF &);
		VectorF & operator -=(const VectorF &);
		
	// component-wise multiply
		VectorF & operator *=(const float &);
		VectorF operator *(const float &) const;
		VectorF & operator *=(const VectorF &);
		VectorF operator *(const VectorF &) const;


		VectorF operator -() const;
				
		MatrixF operator %(const VectorF &) const;
		
	//	diagonal multiply
		MatrixF operator /(const MatrixF &) const;
		MatrixF & operator /=(MatrixF &) const;

	// transpose-multiply
		float operator ^(const VectorF &) const; 
		VectorF & operator ^=(const MatrixF &);
		VectorF operator ^(const MatrixF &) const;
		
		VectorF & Sqrt();

		float E1Norm();
		float E2Norm();		
		float SupNorm();

		virtual void Destroy();
};


}