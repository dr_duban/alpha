/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once
	#include "Math\MatrixSpecial.h"
	
extern "C"
namespace Calc {
	unsigned int Initialize(unsigned int detail_level);
	
	float FloatSqrt(float);
	float Float2Sqrt(float *);
	float Float4Sqrt(float *);
	float Float4NSqrt(float *, unsigned int);

	float FloatSin(float);
	float FloatCos(float);

	double RealSin(double);
	double RealCos(double);

	float FloatAbs(float);
	void Float4NAbs(float *, unsigned int = 1);

	size_t Randnum();

}

