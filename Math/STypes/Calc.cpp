/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include <Windows.h>
#include "CoreLibrary\SFSCore.h"

#include "Math\Calc.h"
#include "Math\CalcSpecial.h"

#include "System\SysUtils.h"

float Calc::FloatCos(float ang) {
	return Calc::FloatSin(ang + 90.0f);
}

// Single Class ===========================================================================================================================================================================================


Calc::Single::Single() {
	SetTID(type_id);
}

Calc::Single::Single(const Single & ref) : Object(ref) {

}

Calc::Single::~Single() {

}

UI_64 Calc::Single::Create(SFSco::Object * container, UI_64 offset) {
	Blank();
	Set(container, offset);

	return 0;
}

Calc::Single::operator float() {
	float result(0.0f);

	if (float * mval = reinterpret_cast<float *>(Acquire())) {
		result = mval[0];
		Release();
	}

	return result;
}

Calc::Single & Calc::Single::operator =(const float & val) {
	if (float * mval = reinterpret_cast<float *>(Acquire())) {
		mval[0] = val;

		Release();
	}

	return *this;
}

Calc::Single & Calc::Single::operator =(const Single & ref) {

	if (float * mval = reinterpret_cast<float *>(const_cast<Single &>(ref).Acquire())) {
		float val = mval[0];
		const_cast<Single &>(ref).Release();

		if (float * mval = reinterpret_cast<float *>(Acquire())) {
			mval[0] = val;

			Release();
		}
	}

	return *this;
}

Calc::Single & Calc::Single::operator +=(const float & val) {
	if (float * mval = reinterpret_cast<float *>(Acquire())) {
		mval[0] += val;

		Release();
	}

	return *this;
}

Calc::Single & Calc::Single::operator +=(const Single & ref) {

	if (float * mval = reinterpret_cast<float *>(const_cast<Single &>(ref).Acquire())) {
		float val = mval[0];
		const_cast<Single &>(ref).Release();

		if (float * mval = reinterpret_cast<float *>(Acquire())) {
			mval[0] += val;

			Release();
		}
	}

	return *this;
}

Calc::Single & Calc::Single::operator -=(const float & val) {
	if (float * mval = reinterpret_cast<float *>(Acquire())) {
		mval[0] -= val;

		Release();
	}

	return *this;
}

Calc::Single & Calc::Single::operator -=(const Single & ref) {

	if (float * mval = reinterpret_cast<float *>(const_cast<Single &>(ref).Acquire())) {
		float val = mval[0];
		const_cast<Single &>(ref).Release();

		if (float * mval = reinterpret_cast<float *>(Acquire())) {
			mval[0] -= val;

			Release();
		}
	}

	return *this;
}


// XInt ======================================================================================================================================================================

/*
	0: complete dw
	1: last bit index
	2: sign
	3: capacity
*/

Calc::XInt::XInt() {
	SetTID(type_id);
}

Calc::XInt::XInt(const XInt & ref) : Object(ref) {

}

Calc::XInt::~XInt() {

}

Calc::XInt & Calc::XInt::operator =(const XInt & ref) {
	UI_64 nsize = ref.GetSize();
	if (Resize(nsize) != -1) {
		if (const void * rptr = const_cast<XInt &>(ref).Acquire()) {
			if (void * mptr = Acquire()) {
				System::MemoryCopy_SSE3(mptr, rptr, nsize);

				Release();
			}
			const_cast<XInt &>(ref).Release();
		}
	}

	return *this;
}


Calc::XInt & Calc::XInt::operator =(UI_64 rval) {
	unsigned int newc(0);
	if (!*this) {
		newc = 4;	
		New(type_id, 8*sizeof(unsigned int));
	}

	if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
		if (!newc) newc = mptr[3];

	//	System::MemoryFill_SSE3(mptr, (newc+4)*sizeof(unsigned int), 0, 0);
				
		mptr[3] = newc;

		if (rval) {
			mptr[1] = SFSco::BitIndex64High(rval);		

			if (mptr[1]>31) {
				mptr[1] -= 32;
				mptr[0] = 2;
			
				mptr[4] = rval & 0xFFFFFFFF;
				mptr[5] = (rval >>= 32);			
			} else {
				mptr[0] = 1;

				mptr[4] = rval;
			}	
		}

		Release();
	}

	return *this;
}

Calc::XInt & Calc::XInt::operator +=(const XInt & ref) {
	unsigned int qco(0), carry(0);

	if (*this) {
		if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
			if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
				if (rptr[0] >= mptr[0]) {
					if (mptr[3] < rptr[0]) {
						qco = rptr[3] - mptr[3] + 4*((rptr[1]==31) && (rptr[0] == rptr[3]));
					}
				}

				if (!qco) { // calculate
					carry = XInt_Add(mptr, rptr);

					if (rptr[0]>mptr[0]) mptr[0] = rptr[0];

					if (mptr[2]) 
						if ((mptr[2] += carry) == 0) carry = 0;

					if (carry) {
						mptr[mptr[0]+3] = carry;
						mptr[0]++;
					}
					
					for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);
					mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
				}

				const_cast<XInt &>(ref).Release();
			}

			Release();
		}

		if (qco) {
			if (Expand(((qco+7)>>2)*4*sizeof(unsigned int)) != -1) {
				if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
					if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
						System::MemoryFill_SSE3(mptr + mptr[3] + 4, (qco+4)*sizeof(unsigned int), 0, 0);
						mptr[3] += ((qco+7)>>2)*4;

						carry = XInt_Add(mptr, rptr);

						if (rptr[0]>mptr[0]) mptr[0] = rptr[0];

						if (mptr[2]) 
							if ((mptr[2] += carry) == 0) carry = 0;

						if (carry) {
							mptr[mptr[0]+3] = carry;
							mptr[0]++;
						}

						for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);
						mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
						
						const_cast<XInt &>(ref).Release();
					}
				}

				Release();
			

			}
		}

	} else {
		*this = ref;
	}

	return *this;
}


Calc::XInt & Calc::XInt::operator -=(const XInt & ref) {
	unsigned int qco(0), carry(0);

	if (*this) {
		if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
			if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
				if (rptr[0] >= mptr[0]) {
					if (mptr[3] < rptr[0]) {
						qco = rptr[3] - mptr[3] + 4*((rptr[1]==31) && (rptr[0] == rptr[3]));
					}
				}

				if (!qco) { // calculate
					mptr[2] ^= XInt_Sub(mptr, rptr);

					if (rptr[0]>mptr[0]) mptr[0] = rptr[0];

					for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);					
					mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
				}

				const_cast<XInt &>(ref).Release();
			}

			Release();
		}

		if (qco) {
			if (Expand(((qco+7)>>2)*4*sizeof(unsigned int)) != -1) {
				if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
					if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
						System::MemoryFill_SSE3(mptr + mptr[3] + 4, (qco+4)*sizeof(unsigned int), 0, 0);
						mptr[3] += ((qco+7)>>2)*4;

						mptr[2] ^= XInt_Sub(mptr, rptr);

						if (rptr[0]>mptr[0]) mptr[0] = rptr[0];

						for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);					
						mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
						
						const_cast<XInt &>(ref).Release();
					}
				}

				Release();
			

			}
		}

	} else {
		*this = ref;
	}

	return *this;
}

bool Calc::XInt::operator >=(const XInt & ref) const {
	bool result(false);

	if (*this)
	if (const unsigned int * mptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(*this).Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			
			if (mptr[2] == rptr[2]) {
				if (mptr[0] == rptr[0]) {
					if (mptr[1] == rptr[1]) result = XInt_AEQU(mptr, rptr);
					else result = (mptr[1] > rptr[1]);
				} else {
					result = (mptr[0] > rptr[0]);
				}

			} else {
				result = (mptr[2] == 0);
			}

			const_cast<XInt &>(ref).Release();
		}
		const_cast<XInt &>(*this).Release();
	}	

	return result;
}

bool Calc::XInt::operator <=(const XInt & ref) const {
	bool result(false);

	if (*this)
	if (const unsigned int * mptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(*this).Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			
			if (mptr[2] == rptr[2]) {
				if (mptr[0] == rptr[0]) {
					if (mptr[1] == rptr[1]) result = XInt_BEQU(mptr, rptr);
					else result = (mptr[1] < rptr[1]);
				} else {
					result = (mptr[0] < rptr[0]);
				}

			} else {
				result = (mptr[2] != 0);
			}

			const_cast<XInt &>(ref).Release();
		}
		const_cast<XInt &>(*this).Release();
	}	

	return result;
}

bool Calc::XInt::operator ==(const XInt & ref) const {
	bool result(false);

	if (*this)
	if (const unsigned int * mptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(*this).Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			
			if ((mptr[2] == rptr[2]) && (mptr[0] == rptr[0]) && (mptr[1] == rptr[1])) result = XInt_EQU(mptr, rptr);

			const_cast<XInt &>(ref).Release();
		}
		const_cast<XInt &>(*this).Release();
	}	

	return result;
}

bool Calc::XInt::operator <(const XInt & ref) const {
	return !(*this >= ref);
}

bool Calc::XInt::operator >(const XInt & ref) const {
	return !(*this <= ref);
}

bool Calc::XInt::operator !=(const XInt & ref) const {
	return !(*this==ref);
}


Calc::XInt & Calc::XInt::operator &=(const XInt & ref) {
	unsigned int qco(0), top(0);
	if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			top = mptr[0];
			if (mptr[2]) {
				if (mptr[3]<rptr[0]) {
					qco = rptr[3] - mptr[3];
					top = rptr[0];
				}
			}

			if (qco == 0) {
				XInt_And(mptr, rptr);

				mptr[2] &= rptr[2];

				mptr[0] = top;
				for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);					
				mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
			}

			const_cast<XInt &>(ref).Release();					
		}			
		Release();
	}


	if (qco) {
		if (Expand(((qco+7)>>2)*4*sizeof(unsigned int)) != -1) {
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
					System::MemoryFill_SSE3(mptr + mptr[3] + 4, (qco+4)*sizeof(unsigned int), 0, 0);
					mptr[3] += ((qco+7)>>2)*4;

					XInt_And(mptr, rptr);

					mptr[2] &= rptr[2];

					mptr[0] = top;
					for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);
					mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);

					const_cast<XInt &>(ref).Release();
				}
				Release();
			}
		}
	}

	return *this;
}

Calc::XInt & Calc::XInt::operator ^=(const XInt & ref) {
	unsigned int qco(0);
	if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			if (mptr[3]<rptr[0]) qco = rptr[3] - mptr[3];
			else {				
				XInt_Xor(mptr, rptr);

				mptr[2] ^= rptr[2];

				if (mptr[0]<rptr[0]) mptr[0] = rptr[0];
				for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);
				mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);

			}
			
			const_cast<XInt &>(ref).Release();
		}			
		Release();
	}

	if (qco) {
		if (Expand(((qco+7)>>2)*4*sizeof(unsigned int)) != -1) {
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
					System::MemoryFill_SSE3(mptr + mptr[3] + 4, (qco+4)*sizeof(unsigned int), 0, 0);
					mptr[3] += ((qco+7)>>2)*4;

					XInt_Xor(mptr, rptr);

					mptr[2] ^= rptr[2];

					if (mptr[0]<rptr[0]) mptr[0] = rptr[0];
					for (;mptr[mptr[0]+3] == mptr[2];mptr[0]--);
					mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);


					const_cast<XInt &>(ref).Release();
				}
				Release();
			}
		}
	}

	return *this;
}



Calc::XInt & Calc::XInt::operator /=(const XInt & ref) {
	unsigned int qco(1);
	SFSco::Object result;
	
	if (*this) {
		if (ref > *this) {
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				qco = mptr[3];
				System::MemoryFill_SSE3(mptr, (qco+4)*sizeof(unsigned int), 0, 0);
				mptr[3] = qco;

				Release();
			}

		} else {
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
					qco = mptr[0] - rptr[0] + 1;

					const_cast<XInt &>(ref).Release();
				}
				Release();
			}

			if (result.New(type_id, ((qco+11)>>2)*4*sizeof(unsigned int)) != -1)
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
					if (unsigned int * vptr = reinterpret_cast<unsigned int *>(result.Acquire())) {
//						System::MemoryFill_SSE3(vptr, ((qco+11)>>2)*4*sizeof(unsigned int), 0, 0);
						
						vptr[3] = ((qco+11)>>2)*4;
						
						if ((mptr[2] == 0) && (rptr[2] == 0) && (rptr[0]>0)) {
							XInt_Div(mptr, rptr, vptr);
													
							for (;vptr[vptr[0]+3] == 0;) vptr[0]--;
							vptr[1] = SFSco::BitIndex32High(mptr[vptr[0]+3]);
						}

						result.Release();
					}

					const_cast<XInt &>(ref).Release();					
				}
			
				Release();
				Destroy();

				dynamic_cast<SFSco::Object &>(*this) = result;
			}
		}

	}

	return *this;
}

Calc::XInt & Calc::XInt::operator %=(const XInt & ref) {
		
	if (*this) {
		if (ref <= *this) {		
			if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
				if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			
					if ((mptr[2] == 0) && (rptr[2] == 0) && (rptr[0]>0)) {
						XInt_Rem(mptr, rptr);

						for (;mptr[mptr[0]+3] == 0;mptr[0]--);						
						mptr[1] = SFSco::BitIndex32High(mptr[mptr[0]+3]);
					}
				
					const_cast<XInt &>(ref).Release();
				}
			
				Release();
			}
		}
	}

	return *this;
}


Calc::XInt & Calc::XInt::operator *=(const XInt & ref) {
	UI_64 asi(0), bsi(0);
	SFSco::Object result;

	if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
		if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
			asi = mptr[0]-1;
			asi <<= 5;
			asi += mptr[1]+1;
			bsi = rptr[0]-1;
			bsi <<= 5;
			bsi += rptr[1]+1;

			asi *= bsi;
			asi += 31;
			asi >>= 5;
			
			if (asi > 0xFFFFFFFB) asi = 0;

			const_cast<XInt &>(ref).Release();
		}			
		Release();
	}

	if (asi)
	if (result.New(type_id, ((asi+11)>>2)*4*sizeof(unsigned int)) != -1) {
		if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
			if (const unsigned int * rptr = reinterpret_cast<const unsigned int *>(const_cast<XInt &>(ref).Acquire())) {
				if (unsigned int * vptr = reinterpret_cast<unsigned int *>(result.Acquire())) {
//					System::MemoryFill_SSE3(vptr, ((asi+11)>>2)*4*sizeof(unsigned int), 0, 0);

					vptr[0] = asi;
					vptr[2] = mptr[2] ^ rptr[2];
					vptr[3] = ((asi+11)>>2)*4;

					XInt_MulAB(mptr, rptr, vptr);
					
					vptr[1] = SFSco::BitIndex32High(vptr[vptr[0]+3]);					

					result.Release();
				}
				const_cast<XInt &>(ref).Release();

			}			
			Release();
			Destroy();

			dynamic_cast<SFSco::Object &>(*this) = result;
		}

	}

	return *this;
}

Calc::XInt & Calc::XInt::Square() {
	UI_64 asi(0);
	SFSco::Object result;

	if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
		asi = mptr[0]-1;
		asi <<= 5;
		asi += mptr[1]+1;

		asi *= asi;
		asi += 31;
		asi >>= 5;
			
		if (asi > 0xFFFFFFFB) asi = 0;
				
		Release();
	}

	if (asi)
	if (result.New(type_id, ((asi+11)>>2)*4*sizeof(unsigned int)) != -1) {
		if (unsigned int * mptr = reinterpret_cast<unsigned int *>(Acquire())) {
			if (unsigned int * vptr = reinterpret_cast<unsigned int *>(result.Acquire())) {
//				System::MemoryFill_SSE3(vptr, ((asi+11)>>2)*4*sizeof(unsigned int), 0, 0);

				vptr[0] = asi;
				vptr[2] = mptr[2] ^ mptr[2];
				vptr[3] = ((asi+11)>>2)*4;

				XInt_MulAB(mptr, mptr, vptr);
					
				vptr[1] = SFSco::BitIndex32High(vptr[vptr[0]+3]);					

				result.Release();

			}			
			Release();
			Destroy();

			dynamic_cast<SFSco::Object &>(*this) = result;
		}

	}

	return *this;
}

Calc::XInt & Calc::XInt::Pow(unsigned int pow, const XInt & mod) {
	XInt mplyer;

	for (;pow^1;pow>>=1) {
		Square();
		*this %= mod;
	}
	
	if (pow >>= 1) {
		mplyer = *this;

		for (;pow;pow>>=1) {
			mplyer.Square();
			mplyer %= mod;

			if (pow & 1) {
				*this *= mplyer;
				*this %= mod;
			}
		}
	}

	mplyer.Destroy();

	return *this;
}

Calc::XInt & Calc::XInt::operator ^(unsigned int pow) {	
	XInt mplyer;

	for (;pow^1;pow>>=1) Square();
	
	if (pow >>= 1) {
		mplyer = *this;

		for (;pow;pow>>=1) {
			mplyer.Square();
			if (pow & 1) *this *= mplyer;
		}
	}

	mplyer.Destroy();

	return *this;
}

Calc::XInt Calc::XInt::operator +(const XInt & ref) {
	XInt result;
	result = *this;

	result += ref;

	return result;
}

Calc::XInt Calc::XInt::operator -(const XInt & ref) {
	XInt result;
	result = *this;

	result -= ref;

	return result;
}

Calc::XInt Calc::XInt::operator /(const XInt & ref) {
	XInt result;
	result = *this;

	result /= ref;

	return result;
}

Calc::XInt Calc::XInt::operator %(const XInt & ref) {
	XInt result;
	result = *this;

	result %= ref;

	return result;
}

Calc::XInt Calc::XInt::operator *(const XInt & ref) {
	XInt result;
	result = *this;

	result *= ref;

	return result;
}

Calc::XInt Calc::XInt::operator ^(const XInt & ref) {
	XInt result;
	result = *this;

	result ^= ref;

	return result;
}

Calc::XInt Calc::XInt::operator &(const XInt & ref) {
	XInt result;
	result = *this;

	result &= ref;

	return result;
}
