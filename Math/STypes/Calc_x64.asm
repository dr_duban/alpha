
; safe-fail math
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
XInt_Add	PROC
	PUSH r12
	PUSH r13

	MOV r8d, [rcx]	
	MOV r9d, [rdx]

	XOR rax, rax

	MOV r12d, [rcx+8]
	MOV r13, rdx
	
	ADD rcx, 16
	ADD rdx, 16

	MOV r10d, r8d
	
	CMP r8d, r9d	
	CMOVA r8d, r9d
	CMOVA r12d, [rdx+8]
	CMOVA r13, rcx


	SUB r10d, r8d
	SUB r9d, r8d

	ADD r9d, r10d

	SUB rdx, rcx
	SUB r13, rcx

align 16
	add_loop:
		SHR rax, 32

		MOV r10d, [rcx+rdx]
		MOV r11d, [rcx]
		ADD rax, r10
		ADD rax, r11		
		
		MOVNTI [rcx], eax
		ADD rcx, 4
	
	DEC r8d
	JNZ add_loop

	TEST r9d, r9d
	JZ exit

align 16
	xadd_loop:

		SHR rax, 32

		MOV r10d, [rcx+r13]

		ADD rax, r10
		ADD rax, r12		
		
		MOVNTI [rcx], eax
		ADD rcx, 4

	DEC r9d
	JNZ xadd_loop

exit:
	SHR rax, 32

	POP r13
	POP r12

	RET
XInt_Add	ENDP


ALIGN 16
XInt_Sub	PROC
	PUSH r12
	PUSH r13
			

	MOV r8d, [rcx]
	MOV r9d, [rdx]

	XOR rax, rax

	MOV r12d, [rcx+8]
	MOV r13, rdx
	
	ADD rcx, 16
	ADD rdx, 16

	MOV r10d, r8d
	
	CMP r8d, r9d	
	CMOVA r8d, r9d
	CMOVA r12d, [rdx+8]
	CMOVA r13, rcx


	SUB r10d, r8d
	SUB r9d, r8d

	ADD r9d, r10d

	SUB rdx, rcx
	SUB r13, rcx

align 16
	sub_loop:
		SAR rax, 32

		MOV r10d, [rcx+rdx]
		MOV r11d, [rcx]
		
		ADD rax, r11
		SUB rax, r10		
		
		MOVNTI [rcx], eax
		ADD rcx, 4
	
	DEC r8d
	JNZ sub_loop

	TEST r9d, r9d
	JZ exit

align 16
	xsub_loop:
		SAR rax, 32

		MOV r10d, [rcx+r13]

		TEST r13, r13
		JNZ long_ref
			ADD rax, r10
			SUB rax, r12
		JMP contipo
		long_ref:
			ADD rax, r12
			SUB rax, r10
		contipo:
		
		MOVNTI [rcx], eax
		ADD rcx, 4

	DEC r9d
	JNZ xsub_loop

exit:
	SAR rax, 32

	POP r13
	POP r12

	RET
XInt_Sub	ENDP


ALIGN 16
XInt_AEQU	PROC

	MOV r8d, [rcx]
	
	MOV rax, 1
		
	SUB rdx, rcx
	LEA rcx, [rcx+r8*4+16]

	INC r8d
	
align 16
	cmp_loop:
		SUB rcx, 4
		DEC r8d
		JZ exit

		MOV r10d, [rcx+rdx]
	XOR r10d, [rcx]
	JZ cmp_loop

		XOR rax, rax

		MOV r10d, [rcx+rdx]
		SUB r10d, [rcx]

		ADC rax, 0

exit:


	RET
XInt_AEQU	ENDP


ALIGN 16
XInt_BEQU	PROC

	MOV r8d, [rcx]
	
	MOV rax, 1
		
	SUB rdx, rcx
	LEA rcx, [rcx+r8*4+16]

	INC r8d
	
align 16
	cmp_loop:
		SUB rcx, 4
		DEC r8d
		JZ exit

		MOV r10d, [rcx+rdx]
	XOR r10d, [rcx]
	JZ cmp_loop

		XOR rax, rax

		MOV r10d, [rcx]
		SUB r10d, [rcx+rdx]

		ADC rax, 0

exit:

	RET
XInt_BEQU	ENDP


ALIGN 16
XInt_EQU	PROC

	MOV r8d, [rcx]
	
	MOV rax, 1
		
	SUB rdx, rcx
	LEA rcx, [rcx+r8*4+16]

	INC r8d
	
align 16
	cmp_loop:
		SUB rcx, 4
		DEC r8d
		JZ exit

		MOV r10d, [rcx+rdx]
	XOR r10d, [rcx]
	JZ cmp_loop

		XOR rax, rax

exit:


	RET
XInt_EQU	ENDP


ALIGN 16
XInt_And	PROC
	PUSH r12
	PUSH r13

	MOV r8d, [rcx]	
	MOV r9d, [rdx]

	XOR rax, rax

	MOV r12d, [rcx+8]
	MOV r13, rdx
	
	ADD rcx, 16
	ADD rdx, 16

	MOV r10d, r8d
	
	CMP r8d, r9d	
	CMOVA r8d, r9d
	CMOVA r12d, [rdx+8]
	CMOVA r13, rcx


	SUB r10d, r8d
	SUB r9d, r8d

	ADD r9d, r10d

	SUB rdx, rcx
	SUB r13, rcx

align 16
	and_loop:
		MOV eax, [rcx+rdx]
		AND eax, [rcx]
		
		MOVNTI [rcx], eax
		ADD rcx, 4
	
	DEC r8d
	JNZ and_loop

	TEST r9d, r9d
	JZ exit

	MOV rax, r13
	AND rax, r12
	JZ exit	

align 16
	xadd_loop:

		MOV eax, [rcx+r13]
		AND eax, r12d		
		
		MOVNTI [rcx], eax
		ADD rcx, 4

	DEC r9d
	JNZ xadd_loop

exit:


	POP r13
	POP r12


	RET
XInt_And	ENDP

ALIGN 16
XInt_Xor	PROC
	PUSH r12
	PUSH r13

	MOV r8d, [rcx]	
	MOV r9d, [rdx]

	XOR rax, rax

	MOV r12d, [rcx+8]
	MOV r13, rdx
	
	ADD rcx, 16
	ADD rdx, 16

	MOV r10d, r8d
	
	CMP r8d, r9d	
	CMOVA r8d, r9d
	CMOVA r12d, [rdx+8]
	CMOVA r13, rcx


	SUB r10d, r8d
	SUB r9d, r8d

	ADD r9d, r10d

	SUB rdx, rcx
	SUB r13, rcx

align 16
	xor_loop:
		MOV eax, [rcx+rdx]
		XOR eax, [rcx]
		
		MOVNTI [rcx], eax
		ADD rcx, 4
	
	DEC r8d
	JNZ xor_loop

	TEST r9d, r9d
	JZ exit

align 16
	xadd_loop:

		MOV eax, [rcx+r13]
		XOR eax, r12d		
		
		MOVNTI [rcx], eax
		ADD rcx, 4

	DEC r9d
	JNZ xadd_loop

exit:


	POP r13
	POP r12
	

	RET
XInt_Xor	ENDP


ALIGN 16
XInt_Rem	PROC
	PUSH r15
	PUSH r14

	PUSH rsi
	PUSH rdi

	XOR r15, r15
	NOT r15
	SHL r15, 27
	ROL r15, 5

	MOV rsi, rcx
	MOV rdi, rdx
	MOV r8, [rsi]
	MOV r9, [rdi]
	MOV r10d, r8d
	MOV r11d, r9d	
	LEA rsi, [rsi+r10*4+16]
	LEA rdi, [rdi+r11*4+16]
	MOV eax, r10d
	SUB eax, r11d

	ROR r8, 32
	ROR r9, 32

	MOV r10d, eax
	SHL r10, 5
	MOV eax, r8d
	ADD r10, rax
	MOV eax, r9d
	SUB r10, rax	
	MOV rax, r10
	AND r10d, r15d
	SHR rax, 5

	SHL rax, 32
	OR r10, rax
		
align 16
	do_compare:
		XOR rax, rax

		MOV r11, r9
		MOV r14, r8
		SHR r11, 32		
		SHR r14, 32

		INC r11d
		SUB rsi, rdi

		MOV ecx, r8d
		SUB ecx, r9d
		JS bcmp_l
			DEC r11d
			SUB rdi, 4
			ADD rsi, 4

			MOV edx, [rsi+rdi-4]
			SHR edx, cl
			XOR edx, [rdi]
			JNZ cmp_out

		bcmp_l:
			AND ecx, r15d

	align 16
		cmp_loop:
			SUB rdi, 4
			DEC r11d
			JZ cmp_eq
				MOV rdx, [rsi+rdi-4]
				SHR rdx, cl
				XOR edx, [rdi]
				JZ cmp_loop
	
		cmp_out:			
			MOV rdx, [rsi+rdi-4]
			SHR rdx, cl
			SUB edx, [rdi]
			ADC rax, 0

	align 16
		cmp_eq:
			CMP rax, 1
			JNZ do_sub				
				DEC ecx
				AND ecx, r15d
				
				SUB r10, 1
				ADC r10, 0
				JC itis_done

				AND r10, r15

				DEC r8				
				
	align 16
		do_sub:
			NEG r11
			LEA rdi, [rdi+r11*4]
						
			MOV r11, r9
			SHR r11, 32
			
			MOV eax, [rsi+rdi]
			MOV edx, [rdi+4]
			SHL edx, cl
			SUB rax, rdx
			MOV [rsi+rdi], eax
			ADD rdi, 4

			XOR ecx, r15d
			INC ecx
					
			DEC r11d
			JZ after_sub

	align 16
		sub_loop:
			SAR rax, 32
			
			MOV edx, [rsi+rdi]
			ADD rax, rdx
			MOV rdx, [rdi]
			SHR rdx, cl
			AND edx, edx
			SUB rax, rdx		
			MOV [rsi+rdi], eax

			ADD rdi, 4
		DEC r11d
		JNZ sub_loop

	align 16
			after_sub:
				DEC ecx
				CMP r9d, ecx
				JB sub_complete
					INC ecx

					SHR rax, 32			
					ADD eax, [rsi+rdi]
					MOV edx, [rdi]
					SHR edx, cl			
					SUB eax, edx		
					MOV [rsi+rdi], eax

					ADD rsi, 4
			
		sub_complete:		
				ADD rsi, rdi
				ADD rdi, 4
			
				XOR rax, rax

				INC r14d
			align 16
				zero_loop:
					SUB rsi, 4
					DEC r14d
					JZ itis_done
				CMP eax, [rsi]
				JZ zero_loop
	
					BSR eax, [rsi]
					ADD rsi, 4
										
					SHL r14, 32
					OR r14, rax										
					MOV rax, r8
					MOV r8, r14
					SUB rax, r14
					JZ do_compare

					AND rax, r15						
					
					CMP r10, rax
					JB itis_done						
						SUB r10, rax						
						AND r10, r15
						
			JMP do_compare
align 16
itis_done:

	POP rdi
	POP rsi
		
	POP r14
	POP r15

	RET
XInt_Rem	ENDP

ALIGN 16
XInt_Div	PROC
	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12	

	PUSH rsi
	PUSH rdi

	XOR r15, r15
	NOT r15
	SHL r15, 27
	ROL r15, 5

	MOV rsi, rcx
	MOV rdi, rdx
	MOV r12, [rsi]
	MOV r13, [rdi]
	MOV r10d, r12d
	MOV r11d, r13d	
	LEA rsi, [rsi+r10*4+16]
	LEA rdi, [rdi+r11*4+16]
	MOV r9d, r10d
	SUB r9d, r11d

	ROR r12, 32
	ROR r13, 32

	MOV r10d, r9d
	SHL r10, 5
	MOV eax, r12d
	ADD r10, rax
	MOV eax, r13d
	SUB r10, rax	
	MOV rax, r10
	AND r10d, r15d
	SHR rax, 5
	
	XOR rdx, rdx
	SUB edx, r10d
	MOV [r8], eax
	ADC DWORD PTR [r8], 0	

	LEA r8, [r8+rax*4+16]
	SHL rax, 32
	OR r10, rax
	XOR r9, r9		

align 16
	do_compare:
		XOR rax, rax

		MOV r11, r13
		MOV r14, r12
		SHR r11, 32		
		SHR r14, 32

		INC r11d
		SUB rsi, rdi

		MOV ecx, r12d
		SUB ecx, r13d
		JS bcmp_l
			DEC r11d
			SUB rdi, 4
			ADD rsi, 4

			MOV edx, [rsi+rdi-4]
			SHR edx, cl
			XOR edx, [rdi]
			JNZ cmp_out

		bcmp_l:
			AND ecx, r15d

	align 16
		cmp_loop:
			SUB rdi, 4
			DEC r11d
			JZ cmp_eq
				MOV rdx, [rsi+rdi-4]
				SHR rdx, cl
				XOR edx, [rdi]
				JZ cmp_loop
	
		cmp_out:			
			MOV rdx, [rsi+rdi-4]
			SHR rdx, cl
			SUB edx, [rdi]
			ADC rax, 0

	align 16
		cmp_eq:
			CMP rax, 1
			JNZ do_sub				
				DEC ecx
				AND ecx, r15d
				
				SUB r10, 1
				ADC r10, 0
				JC itis_done

				DEC r12
				
				TEST r10d, r10d
				JNS do_sub					
					AND r10, r15

					MOVNTI [r8], r9d
					SUB r8, 4
					XOR r9d, r9d

	align 16
		do_sub:
			BTS r9d, r10d

			NEG r11
			LEA rdi, [rdi+r11*4]
						
			MOV r11, r13
			SHR r11, 32
			
			MOV eax, [rsi+rdi]
			MOV edx, [rdi+4]
			SHL edx, cl
			SUB rax, rdx
			MOV [rsi+rdi], eax
			ADD rdi, 4

			XOR ecx, r15d
			INC ecx
					
			DEC r11d
			JZ after_sub

	align 16
		sub_loop:
			SAR rax, 32
			
			MOV edx, [rsi+rdi]
			ADD rax, rdx
			MOV rdx, [rdi]
			SHR rdx, cl
			AND edx, edx			
			SUB rax, rdx		
			MOV [rsi+rdi], eax

			ADD rdi, 4
		DEC r11d
		JNZ sub_loop

	align 16
			after_sub:
				DEC ecx
				CMP r13d, ecx
				JB sub_complete
					INC ecx

					SHR rax, 32			
					ADD eax, [rsi+rdi]
					MOV edx, [rdi]
					SHR edx, cl			
					SUB eax, edx
					MOV [rsi+rdi], eax

					ADD rsi, 4
			
		sub_complete:		
				ADD rsi, rdi
				ADD rdi, 4
			
				XOR rax, rax

				INC r14d
			align 16
				zero_loop:
					SUB rsi, 4
					DEC r14d
					JZ itis_done
				CMP eax, [rsi]
				JZ zero_loop
	
					BSR eax, [rsi]
					ADD rsi, 4
										
					SHL r14, 32
					OR r14, rax										
					MOV rax, r12
					MOV r12, r14
					SUB rax, r14
					JZ do_compare
										
					AND rax, r15						
					
					CMP r10, rax
					JB itis_done						
						SUB r10, rax
						SHR rax, 32										
						
						TEST r10d, r10d
						JNS no_left							
							AND r10, r15
							JMP do_ofl
						no_left:

							SUB eax, 1
							ADC eax, 0											
							JZ do_compare
							do_ofl:

								MOVNTI [r8], r9d
								SUB r8, 4
								XOR r9, r9
							
								NEG rax
								LEA r8, [r8+rax*4]
						
			JMP do_compare
align 16
itis_done:
		MOVNTI [r8], r9d
		

	POP rdi
	POP rsi
		
	POP r12
	POP r13
	POP r14
	POP r15

	RET
XInt_Div	ENDP


ALIGN 16
XInt_MulAB	PROC
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15

	PUSH rsi
	PUSH rdi
	PUSH rbx

	MOV rsi, rcx
	MOV rdi, rdx

	MOV r10d, [rcx]
	
	CMP r10d, [rdx]
	CMOVA rsi, rdx
	CMOVA rdi, rcx

	MOV r12d, [rsi]
	MOV r13d, [rdi]
	DEC r13d
	JNZ no_1r13
		INC r13d
	no_1r13:
	
	INC r12d

	ADD rsi, 12
	ADD r8, 12
	ADD rdi, 16
	

	XOR rax, rax
	XOR r10, r10
	
	XOR rcx, rcx
	XOR rdx, rdx
	
		XOR r11, r11

	align 16
		next_edx:
			ADD rsi, 4
			ADD r8, 4
			
			DEC r12d
			JZ itis_done
			MOV edx, [rsi]
		TEST edx, edx
		JZ next_edx
		
		AND r10d, 1Fh
		JNZ on_run
	align 16
		off_run:
			BSF ecx, edx
			SAR edx, cl
			
			ADD r10d, ecx			
			CMP r10d, 32
			JAE next_edx

		on_run:
			ADD r11, 32
			INC edx
			JZ next_edx
			
				SUB r11, 32
				BSF eax, edx
				OR r11, rax
				MOV rax, r11

				CMP rax, 32
				JB tu_nix
					SUB rax, r10
					SUB r8, 4
				tu_nix:

				CMP rax, 2
				JA saddsub_action
					MOV r11d, eax
					MOV ecx, eax														
					SAR edx, cl
															
				align 16
					sadd_action:
						XOR rax, rax						

						MOV ecx, r10d

						MOV r9d, r13d
						MOV r14, rdi
						MOV rbx, r8
						SUB r14, rbx												
						
						TEST ecx, ecx
						JZ sadd_loop
							MOV eax, [rbx+r14]
							SHL eax, cl

							MOV r15d, [rbx]
							ADD rax, r15

							MOV [rbx], eax

							ADD rbx, 4
							SUB r14, 4
														
							NEG ecx
							AND ecx, 1Fh

					align 16
						sadd_loop:
							SHR rax, 32

							MOV r15, [rbx+r14]
							SHR r15, cl
							AND r15d, r15d
							ADD rax, r15

							MOV r15d, [rbx]
							ADD rax, r15

							MOV [rbx], eax

							ADD rbx, 4
						DEC r9d
						JNZ sadd_loop

						SHR rax, 32
						JZ no_carry1
							MOV [rbx], eax
						no_carry1:


						INC r10d
						AND r10d, 1Fh
						JNZ tu_nix2
							ADD r8, 4
						tu_nix2:

					DEC r11d
					JNZ sadd_action

				XOR edx, 1
				JZ next_edx
				JMP off_run
				

			align 16
				saddsub_action:
					CMP rax, 32
					CMOVB ecx, eax
					AND ecx, 1Fh
					SAR edx, cl



					XOR rax, rax						
					MOV rcx, r10

					CMP r11, 32
					CMOVAE rcx, rax
					JB tu_nix3
						ADD r8, 4
					tu_nix3:

					ADD rcx, r11
					AND rcx, 1Fh


					MOV r9d, r13d
					MOV r14, rdi
					MOV rbx, r8
					SUB r14, rbx												
						
					TEST ecx, ecx
					JZ ssadd_loop
						MOV eax, [rbx+r14]
						SHL eax, cl

						MOV r15d, [rbx]
						ADD rax, r15

						MOV [rbx], eax

						ADD rbx, 4
						SUB r14, 4
												
						NEG ecx
						AND ecx, 1Fh

					align 16
						ssadd_loop:
							SHR rax, 32

							MOV r15, [rbx+r14]
							SHR r15, cl
							AND r15d, r15d					
							ADD rax, r15

							MOV r15d, [rbx]
							ADD rax, r15

							MOV [rbx], eax

							ADD rbx, 4
						DEC r9d
						JNZ ssadd_loop

						SHR rax, 32
						JZ no_carry2
							MOV [rbx], eax
						no_carry2:


					

					XOR rax, rax						

					MOV r9d, r13d
					MOV r14, rdi

					MOV ecx, r10d

					CMP r11, 32
					CMOVAE r10, rax
					ADD r10, r11
					AND r10, 1Fh

					SHR r11, 5
					NEG r11

					LEA rbx, [r8+r11*4]
					SUB r14, rbx

					XOR r11, r11

					TEST ecx, ecx
					JZ ssub_loop
						MOV eax, [rbx]

						MOV r15d, [rbx+r14]
						SHL r15d, cl
						
						SUB rax, r15

						MOV [rbx], eax

						ADD rbx, 4
						SUB r14, 4
												
						NEG ecx
						AND ecx, 1Fh

					align 16
						ssub_loop:
							SAR rax, 32
							
							MOV r15d, [rbx]
							ADD rax, r15

							MOV r15, [rbx+r14]
							SHR r15, cl
							AND r15d, r15d
							SUB rax, r15

							MOV [rbx], eax

							ADD rbx, 4
						DEC r9d
						JNZ ssub_loop

						SAR rax, 32
						JZ no_carry3
						align 16
							after_ssub:
								MOV r15d, [rbx]
								ADD rax, r15
								MOV [rbx], eax
								ADD rbx, 4
							SAR rax, 32
							JNZ after_ssub
						no_carry3:


					
				XOR edx, 1
				JZ next_edx
				JMP off_run

align 16
itis_done:


	POP rbx
	POP rdi
	POP rsi

	POP r15
	POP r14
	POP r13
	POP r12

	RET
XInt_MulAB	ENDP



END
