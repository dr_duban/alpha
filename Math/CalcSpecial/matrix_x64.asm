
; safe-fail math
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


	
	matrix_hdr_coCount			TEXTEQU	<8>
	matrix_hdr_roCount			TEXTEQU <0>
	matrix_hdr_divC				TEXTEQU <12>
	matrix_hdr_divR				TEXTEQU <4>
	matrix_hdr_rank				TEXTEQU <16>
	matrix_hdr_det				TEXTEQU <20>

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST
ALIGN 16
	float_cs_mask	DWORD	7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh
	float_one		DWORD	3F800000h, 3F800000h, 3F800000h, 3F800000h
	sign_special	DWORD	80000000h, 00000000h, 80000000h, 00000000h
	float_special	DWORD	7fD22112h, 7fD22112h, 7fD22112h, 7fD22112h
	float_10_6		DWORD	358637BEh, 358637BEh, 358637BEh, 358637BEh
	float_10_5		DWORD	3727C5ACh, 3727C5ACh, 3727C5ACh, 3727C5ACh

.CODE

ALIGN 16
MF_IdentityCheck	PROC
	MOV r8d, edx
	MOV r9d, edx

	MOV r10d, 4

	XOR rax, rax
	XORPS xmm0, xmm0
		
	MOVAPS xmm1, [float_cs_mask]
	MOVAPS xmm2, [float_one]
	MOVAPS xmm5, [float_10_5]
	

align 16
	roloop:			
		MOV r8d, edx

		coloop:
			MOVAPS xmm3, [rcx]
				
			ANDPS xmm3, xmm1
			ADDPS xmm0, xmm3

			ADD rcx, 16
		SUB r8d, 1
		JNZ coloop
	SUB r10d, 1
	JNZ roloop
				
	SUBPS xmm0, xmm2
	ANDPS xmm0, xmm1
	SUBPS xmm0, xmm5

	MOVMSKPS rax, xmm0
	ADD rax, 1
	SHR rax, 4
	JZ theend

		MOV r10d, 4
		XORPS xmm0, xmm0
	SUB r9d, 1
	JNZ roloop
	
	theend:
	

	RET
MF_IdentityCheck	ENDP

ALIGN 16
MF_IdentityCheck3	PROC
		
	MOVAPS xmm3, XMMWORD PTR [float_cs_mask]
	MOVAPS xmm4, XMMWORD PTR [float_one]
	MOVAPS xmm5, XMMWORD PTR [float_10_5]

	MOVAPS xmm0, [rcx]
	ANDPS xmm0, xmm3
	MOVAPS xmm1, [rcx+16]
	ANDPS xmm1, xmm3
	MOVAPS xmm2, [rcx+32]
	ANDPS xmm2, xmm3

	SUBPS xmm4, xmm0
	ADDPS xmm1, xmm2
	SUBPS xmm4, xmm1

	ANDPS xmm4, xmm3
	SUBPS xmm4, xmm5

	MOVMSKPS rax, xmm4
	ADD rax, 1
	SHR rax, 3

	RET
MF_IdentityCheck3	ENDP

ALIGN 16
FloatPermute	PROC
	PUSH r12
	PUSH r14
	PUSH r15
	PUSH rbp
	
	XOR r14, r14
	MOV rbp, rsp
	PUSH 0

	
	MOV r9, [rdx+16]
	MOV r10, [rdx+16]

	MOV rax, 24
		

align 16
	analoop:
		MOV r11, [rdx+rax]
		CMP r11, [rdx+rax+8]
		JNZ realoop

		ADD rax, 16

	SUB r10, 1
	JNZ analoop

	realoop:

	TEST r10, r10
	JZ anadone

	XOR r15, r15
	MOV r12, r11

	align 16
		loopcontinue:
			ADD r15, 1

			PUSH r12
			
			XOR r12, r12
			NOT r12
			MOV [rdx+rax], r12
			MOV [rdx+rax+8], r12


			MOV r12, [rdx+rax+8]


			CMP r12, r11
			JZ endoftheloop

			ADD rax, 16
			SUB r10, 1

		align 16
			r12search:

				CMP r12, [rdx+rax]
				JZ loopcontinue

				ADD rax, 16
		
			SUB r10, 1
			JNZ r12search

				MOV rax, 24
				MOV r10, r9

			JMP r12search

			endoftheloop:
				PUSH r15
				
				XOR r15, 1
				AND r15, 1

				ADD r14, r15

				MOV rax, 24
				MOV r10, r9

			JMP analoop

	anadone:
	
	MOV rax, [rsp]	
	TEST rax, rax
	JZ nixzutun
	
	MOV r8, [rdx]
	MOV r9, [rdx+8]
	MOV r10, rsp
	
align 16
	prmloop:
		SUB rax, 1

		MOV r11, [rsp+8]
		MOV edx, [rcx+r11*4]

		ADD rsp, 16

	align 16
		copyloop:
			MOV r15, [rsp]
			ADD rsp, 8

			MOV r12d, [rcx+r15*4]
			MOVNTI [rcx+r11*4], r12d
			
			MOV r11, r15
		SUB rax, 1
		JNZ copyloop

			MOVNTI [rcx+r11*4], edx
		
			MOV rax, [rsp]
		TEST rax, rax
		JNZ prmloop

		MOV rsp, r10

		MOV rax, [rsp]

		ADD rcx, r9
	SUB r8, 1
	JNZ prmloop

	MOV rax, r14

		nixzutun:

	MOV rsp, rbp
	POP rbp

	POP r15
	POP r14
	POP r12

	RET
FloatPermute ENDP











; matrix ========================================================================================================================================================================================
ALIGN 16
MF_AddMatrix	PROC
	MOV eax, [rcx+matrix_hdr_divR]
	SHL rax, 4
		
	MOV ecx, [r8+matrix_hdr_divR]
	MOV r8d, [r8+matrix_hdr_coCount]
		
	MOV r10d, ecx	
	SHL r10, 4

	SUB rax, r10
	SHR r10, 4

	SUB rdx, r9		
	align 16
		lcloop:
			MOVAPS xmm0, [r9]
			ADDPS xmm0, [rdx+r9]
			
			MOVNTPS [rdx+r9], xmm0

			ADD r9, 16
			SUB ecx, 1			
		JNZ lcloop

		LEA rdx, [rdx + rax]		
		MOV ecx, r10d

		SUB r8d, 1
		JNZ lcloop


	RET
MF_AddMatrix	ENDP

ALIGN 16
MF_SubMatrix	PROC
	MOV eax, [rcx+matrix_hdr_divR]
	SHL rax, 4
		
	MOV ecx, [r8+matrix_hdr_divR]
	MOV r8d, [r8+matrix_hdr_coCount]

	MOV r10d, ecx

	SHL r10, 4

	SUB rax, r10
	SHR r10, 4

	SUB rdx, r9
	align 16
		lcloop:
			MOVAPS xmm0, [rdx+r9]
			SUBPS xmm0, [r9]
			
			MOVNTPS [rdx+r9], xmm0

			ADD r9, 16
			SUB ecx, 1			
		JNZ lcloop

		LEA rdx, [rdx + rax]	
		MOV ecx, r10d	

		SUB r8d, 1
		JNZ lcloop


	RET
MF_SubMatrix	ENDP


ALIGN 16
MF_DiagonalMultiply	PROC
	PUSH r11

		MOV eax, [rcx+matrix_hdr_coCount]

		MOV r8d, [r8+matrix_hdr_roCount]

		CMP eax, r8d
		CMOVA eax, r8d
			
		MOV r10, rdx

		MOV r11d, [rcx+matrix_hdr_divR]
		MOV ecx, r11d
		SHL r11, 4

	align 16
		coloop:
			MOVSS xmm0, DWORD PTR [r9]
			SHUFPS xmm0, xmm0, 0

			ADD r9, 4
			MOV r8d, r11d
	
		align 16
			roloop:
				MOVAPS xmm1, [rdx]
				MULPS xmm1, xmm0
				MOVNTPS [rdx], xmm1		
			
				ADD rdx, 16

			SUB r8d, 1
			JNZ roloop

			ADD r10, r11
			MOV rdx, r10

		SUB eax, 1
		JNZ coloop

	POP r11

	RET
MF_DiagonalMultiply	ENDP

ALIGN 16
MF_Multiply	PROC
	PUSH rbp
	MOV rbp, [rsp+48]
	
	PUSH rdi
	PUSH r13
	PUSH r14
	
		MOV r11, rdx ; [rcx+matrix_elements]
						
		MOV eax, [rcx+matrix_hdr_divR]
		MOV r13d, eax
				
		SHL rax, 4
		
		MOV r14d, [r8+matrix_hdr_coCount]
		MOV r8d, [r8+matrix_hdr_divR]
		MOV r10d, r8d

		MOV rdi, rbp
		SUB rdi, r11
		
	align 16
			coloop:				
				
				MOVAPS xmm4, [r9]
				MOVAPS xmm5, xmm4
				
				UNPCKLPS xmm4, xmm5

				MOVAPS xmm12, xmm5				

				UNPCKHPS xmm12, xmm5

				MOVHLPS xmm5, xmm4

				MOVLHPS xmm4, xmm4
				MOVLHPS xmm5, xmm5

				MOVHLPS xmm13, xmm12
				
				MOVLHPS xmm12, xmm12
				MOVLHPS xmm13, xmm13

				ADD r9, 16
								
				MOV ecx, r13d
				
				
	align 16	
				rowloop:

					MOVAPS xmm0, [r11]
					MOVAPS xmm1, [r11+rax]

					ADD r11, rax
						
					MULPS xmm0, xmm4
					MULPS xmm1, xmm5

					ADDPS xmm0, xmm1
					
					MOVAPS xmm1, [r11+rax]
					MOVAPS xmm2, [r11+2*rax]

					SUB r11, rax

					MULPS xmm1, xmm12
					MULPS xmm2, xmm13

					ADDPS xmm1, xmm2
					ADDPS xmm0, xmm1
				
					ADDPS xmm0, [rdi+r11]
					MOVNTPS [rdi+r11], xmm0
												
					ADD r11, 16

				SUB ecx, 1
				JNZ rowloop
				
				LEA r11, [r11 + 2*rax]
				ADD r11, rax
				
				MOV rdi, rbp
				SUB rdi, r11
				
				SUB r10d, 1
			JNZ coloop
						
			MOV r11, rdx

			ADD rbp, rax			
			MOV rdi, rbp			
			SUB rdi, r11

			MOV r10d, r8d

			SUB r14d, 1
		JNZ coloop

	POP r14
	POP r13
	POP rdi

	POP rbp
					
	RET
MF_Multiply ENDP

ALIGN 16
MF_Multiply44	PROC

	MOVAPS xmm4, [rcx]
	MOVAPS xmm5, [rcx+16]
	MOVAPS xmm12, [rcx+32]
	MOVAPS xmm13, [rcx+48]

	MOVAPS xmm0, xmm4

	UNPCKLPS xmm4, xmm5
	UNPCKHPS xmm0, xmm5

	MOVAPS xmm5, xmm12

	UNPCKLPS xmm12, xmm13
	UNPCKHPS xmm5, xmm13

	MOVAPS xmm13, xmm5

	MOVHLPS xmm13, xmm0
	MOVLHPS xmm0, xmm5

	MOVAPS xmm5, xmm12
	MOVHLPS xmm5, xmm4
	MOVLHPS xmm4, xmm12

	MOVAPS xmm12, xmm0

; transpose complete

	SUB r9, r8


align 16
	muloop:
		MOVAPS xmm0, [r8]
		
		MOVAPS xmm1, xmm0
		MOVAPS xmm2, xmm0
		MOVAPS xmm3, xmm0

		MULPS xmm0, xmm4
		MULPS xmm1, xmm5
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13

		HADDPS xmm0, xmm1
		HADDPS xmm2, xmm3
		HADDPS xmm0, xmm2

		MOVNTPS [r9+r8], xmm0

		ADD r8, 16
	SUB edx, 1
	JNZ muloop

	RET
MF_Multiply44	ENDP


ALIGN 16
MF_Multiply22	PROC

	XORPS xmm13, xmm13

	MOVAPS xmm0, [rcx]
	UNPCKLPS xmm0, [rcx+16]

	SUB r9, r8

align 16
	muloop:
		MOVDDUP xmm1, QWORD PTR [r8]
		
		MULPS xmm1, xmm0

		HADDPS xmm1, xmm13

		MOVNTPS [r9+r8], xmm1

		ADD r8, 16
	SUB edx, 1
	JNZ muloop

	RET
MF_Multiply22	ENDP


ALIGN 16
MF_TransposeMultiply	PROC
	PUSH rbp
	MOV rbp, [rsp+48]
	
	PUSH rsi
	PUSH r12
	PUSH r13	
	
		MOV r11, rdx ; [rcx+matrix_elements]		
				
		MOV eax, [rcx+matrix_hdr_divR]
		MOV r13d, eax
				
		SHL rax, 4

		MOV r12d, [r8+matrix_hdr_coCount]
		MOV r8d, [r8+matrix_hdr_divR]
		MOV r10d, r8d

		MOV rsi, r9
		SUB rsi, r11

	align 16
		coloop:

				MOV ecx, r13d
		
				XORPS xmm4, xmm4
				XORPS xmm5, xmm5
				XORPS xmm12, xmm12
				XORPS xmm13, xmm13

	align 16
				divRloop:				
				
					MOVAPS xmm2, [rsi+r11]
				
					MOVAPS xmm0, [r11]
					MOVAPS xmm1, [r11+rax]
					
					ADD r11, rax
						
					MULPS xmm0, xmm2
					MULPS xmm1, xmm2

					ADDPS xmm4, xmm0
					ADDPS xmm5, xmm1

					MOVAPS xmm0, [r11+rax]
					MOVAPS xmm1, [r11+2*rax]

					SUB r11, rax

					MULPS xmm0, xmm2
					MULPS xmm1, xmm2

					ADDPS xmm12, xmm0
					ADDPS xmm13, xmm1
												
					ADD r11, 16

				SUB ecx, 1
				JNZ divRloop

				HADDPS xmm4, xmm5
				HADDPS xmm12, xmm13

				HADDPS xmm4, xmm12

				MOVNTPS [rbp], xmm4
				ADD rbp, 16
				
				LEA r11, [r11 + 2*rax]
				ADD r11, rax

				MOV rsi, r9
				SUB rsi, r11

			SUB r10d, 1
			JNZ coloop

			MOV r11, rdx

			ADD r9, rax
			MOV rsi, r9
			SUB rsi, r11

			MOV r10d, r8d

		SUB r12d, 1
		JNZ coloop

	POP r13
	POP r12
	POP rsi

	POP rbp			
	RET
MF_TransposeMultiply ENDP

ALIGN 16
MF_TransposeMultiply44	PROC

;	MOV rax, [rcx+matrix_elements]

	MOVAPS xmm4, [rcx]
	MOVAPS xmm5, [rcx+16]
	MOVAPS xmm12, [rcx+32]
	MOVAPS xmm13, [rcx+48]

	SUB r9, r8

align 16
	muloop:
		MOVAPS xmm0, [r8]
		
		MOVAPS xmm1, xmm0
		MOVAPS xmm2, xmm0
		MOVAPS xmm3, xmm0

		MULPS xmm0, xmm4
		MULPS xmm1, xmm5
		MULPS xmm2, xmm12
		MULPS xmm3, xmm13

		HADDPS xmm0, xmm1
		HADDPS xmm2, xmm3
		HADDPS xmm0, xmm2

		MOVAPS [r9+r8], xmm0

		ADD r9, 16
	SUB edx, 1
	JNZ muloop

	RET
MF_TransposeMultiply44	ENDP


ALIGN 16
MF_TransposeMultiply22	PROC

;	MOV rax, [rcx+matrix_elements]

	XORPS xmm13, xmm13

	MOVAPS xmm0, [rcx]
	MOVHPS xmm0, QWORD PTR [rcx+16]

	SUB r9, r8

align 16
	muloop:
		MOVAPS xmm1, [r8]
		
		MULPS xmm1, xmm0

		HADDPS xmm1, xmm13

		MOVNTPS [r9+r8], xmm1

		ADD r9, 16
	SUB edx, 1
	JNZ muloop

	RET
MF_TransposeMultiply22	ENDP


ALIGN 16
MF_FromBuffer	PROC

	PUSH rsi
	PUSH rdi

		MOV rsi, r8

		MOV rdi, rdx

		MOV edx, [rcx+matrix_hdr_roCount]
					
		MOV eax, edx
		MOV r8d, edx

		MOV edx, [rcx+matrix_hdr_coCount]
		CMP edx, 1
		ADC edx, 0

		AND eax, 3
		CMP eax, 1
		JZ threetimes
		CMP eax, 2
		JZ twotiems

		XOR eax, eax

	align 16
		loop1:
			MOV ecx, r8d
			REP MOVSD

			STOSD

			SUB edx, 1
		JNZ loop1

		JMP copydone
				
	twotiems:

		XOR eax, eax

	align 16
		loop2:
			MOV ecx, r8d
			REP MOVSD

			STOSD
			STOSD

			SUB edx, 1
		JNZ loop2

		JMP copydone
						
	threetimes:
		XOR eax, eax

	align 16
		loop3:
			MOV ecx, r8d
			REP MOVSD

			STOSD
			STOSD
			STOSD

			SUB edx, 1
		JNZ loop3

		copydone:
				
			
	POP rdi
	POP rsi


	RET
MF_FromBuffer	ENDP

ALIGN 16
MF_Scale43	PROC
	MOVUPS xmm1, [rdx]
	MOVAPS xmm2, xmm1

	TEST r8, r8
	JZ noxmul
		MULPS xmm2, [r8]
		MOVAPS [r8], xmm2
	noxmul:

	MOVAPS xmm2, xmm1

	UNPCKLPS xmm1, xmm1
	UNPCKHPS xmm2, xmm2

	MOVAPS xmm4, xmm1
	MOVAPS xmm5, xmm2

	MOVLHPS xmm1, xmm1
	MOVHLPS xmm4, xmm4
	MOVLHPS xmm2, xmm2
	MOVHLPS xmm5, xmm5

	MULPS xmm1, [rcx]
	MULPS xmm4, [rcx+16]
	MULPS xmm2, [rcx+32]
	MULPS xmm5, [rcx+48]

	MOVAPS [rcx], xmm1
	MOVAPS [rcx+16], xmm4
	MOVAPS [rcx+32], xmm2
	MOVAPS [rcx+48], xmm5
		
	RET
MF_Scale43	ENDP


ALIGN 16
MF_Scale44	PROC
	MOVUPS xmm0, [rdx]

align 16
	size_loop:
		MOVAPS xmm1, [rcx]
		MULPS xmm1, xmm0
		MOVAPS [rcx], xmm1
		ADD rcx, 16
	DEC r8d
	JNZ size_loop
		
	RET
MF_Scale44	ENDP


ALIGN 16
MF_Scale	PROC
	MOVSS xmm0, DWORD PTR [r8]

	UNPCKLPS xmm0, xmm0
	MOVLHPS xmm0, xmm0

	MOV r8, rdx ; [rcx+matrix_elements]
		
	XOR edx, edx
		
	MOV eax, [rcx+matrix_hdr_divR]
	MOV ecx, [rcx+matrix_hdr_divC]

	MUL ecx

	MOV ecx, eax

align 16
	negateloop:
		MOVAPS xmm4, [r8]
		MOVAPS xmm5, [r8+16]
			
		MULPS xmm4, xmm0
		MULPS xmm5, xmm0

		MOVAPS xmm12, [r8+32]
		MOVAPS xmm13, [r8+48]

		MULPS xmm12, xmm0
		MULPS xmm13, xmm0
		
		MOVNTPS [r8], xmm4
		MOVNTPS [r8+16], xmm5
		MOVNTPS [r8+32], xmm12
		MOVNTPS [r8+48], xmm13
			
		ADD r8, 64

	SUB ecx, 1
	JNZ negateloop
		
	RET
MF_Scale	ENDP


ALIGN 16
MF_Negate	PROC

	MOVAPS xmm0, [sign_special]
	SHUFPS xmm0, xmm0, 0

	MOV r8, rdx
		
	XOR edx, edx
		
	MOV eax, [rcx+matrix_hdr_divR]
	MOV ecx, [rcx+matrix_hdr_divC]

	MUL ecx

	MOV ecx, eax

align 16
	negateloop:
		MOVAPS xmm4, [r8]
		MOVAPS xmm5, [r8+16]
			
		XORPS xmm4, xmm0
		XORPS xmm5, xmm0
			
		MOVAPS xmm12, [r8+32]
		MOVAPS xmm13, [r8+48]

		XORPS xmm12, xmm0
		XORPS xmm13, xmm0				
			
		MOVNTPS [r8], xmm4
		MOVNTPS [r8+16], xmm5
		MOVNTPS [r8+32], xmm12
		MOVNTPS [r8+48], xmm13
			
		ADD r8, 64

	SUB ecx, 1
	JNZ negateloop


	RET
MF_Negate	ENDP

ALIGN 16
MF_InverseMultiply	PROC
	PUSH rbp
	MOV rbp, [rsp+48]

	PUSH rsi
	PUSH rdi
	PUSH rbx

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
		
	MOVSS xmm15, [float_one]
	MOV r15, r9 ; rdx

	MOV r9d, [r8+matrix_hdr_coCount]
	MOV r8d, [r8+matrix_hdr_divR]

; search for first pivot
	MOV rsi, rdx ; [rcx+matrix_elements]
	MOV r11, rdx

	MOV eax, [rcx+matrix_hdr_divR]	
	MOV ebx, [rcx+matrix_hdr_coCount]
	MOV edx, [rcx+matrix_hdr_roCount]

	CMP ebx, edx
	CMOVB edx, ebx

	DEC edx
	MOV r12d, edx ; rank-1

	SUB rbp, rsi

	SHL rax, 4
	SUB rbp, rax
	SHR rax, 4

	XORPS xmm12, xmm12
	MOVAPS xmm13, XMMWORD PTR [float_cs_mask]

	PUSH 0
	PUSH 0

align 16
	firstpivotsearch:
			MOVAPS xmm0, [rsi]
			ANDPS xmm0, xmm13 ; clear sign

			MOVAPS xmm1, xmm0
			CMPPS xmm0, xmm12, 2 ; less

			MOVMSKPS rdx, xmm0
						
			XOR rdx, 0Fh
			JZ dienachste				

				MOVSS xmm12, DWORD PTR [rsi]				
				MOVSS xmm3, DWORD PTR [rsi+4]

				ANDPS xmm12, xmm13				
				ANDPS xmm3, xmm13

				MAXSS xmm12, xmm3

				MOVSS xmm3, DWORD PTR [rsi+8]
				ANDPS xmm3, xmm13

				MAXSS xmm12, xmm3

				MOVSS xmm3, DWORD PTR [rsi+12]
				ANDPS xmm3, xmm13

				MAXSS xmm12, xmm3
				
				UNPCKLPS xmm12, xmm12
				MOVLHPS xmm12, xmm12

				CMPPS xmm1, xmm12, 0; equal
				MOVMSKPS rdx, xmm1

				BSF rdx, rdx

				NEG rdx
				LEA rdx, [rax*4+rdx]

				MOV [rsp], rdx ; row
				MOV [rsp+8], rbx ; col
		align 16
			dienachste:

			ADD rsi, 16
		SUB eax, 1
		JNZ firstpivotsearch

		MOV eax, [rcx+matrix_hdr_divR]

	SUB ebx, 1
	JNZ firstpivotsearch



	

align 16
mainloop: ; until all pivots are found

; expand pivot to xmm12 (because of the sign)
	MOV rsi, r11 ; [rcx+matrix_elements]

	XOR rdx, rdx
	MOV eax, [rcx+matrix_hdr_coCount]
	SUB rax, [rsp+8]

	MOV ebx, [rcx+matrix_hdr_divR]	
	
	MUL ebx

	SHL rax, 4
	ADD rsi, rax ; pivot column

	MOV edx, ebx
	
	SHL rdx, 2
	SUB rdx, [rsp]
	SHL rdx, 2 ; pivot row offset
		
	MOVSS xmm12, DWORD PTR [rsi+rdx]
	UNPCKLPS xmm12, xmm12
	MOVLHPS xmm12, xmm12

	MULSS xmm15, xmm12 ; det

; apply to inp		
		
			MOV ebx, r9d
			MOV rdi, r15
			MOV r13d, r8d
			
			SUB rsi, rdi
		align 16								
			incoloop:
				MOV r14d, DWORD PTR [rdi+rdx]
				AND r14d, [float_cs_mask]
				JZ xmm1zero

					MOVSS xmm1, DWORD PTR [rdi+rdx]				
					MOVSS xmm2, xmm1

					UNPCKLPS xmm1, xmm1
					MOVLHPS xmm1, xmm1

					DIVPS xmm1, xmm12				
					ADDSS xmm2, xmm1

					MOVSS DWORD PTR [rdi+rdx], xmm2
				
			align 16
				inroloop:
					XORPS xmm4, xmm4
					MOVAPS xmm0, [rsi+rdi]
					
					MOVAPS xmm3, xmm0
					ANDPS xmm3, xmm13

					CMPPS xmm3, xmm4, 4 ; not equal
					MOVMSKPS r14, xmm3

					TEST r14, r14
					JZ nomodify
						MOVAPS xmm2, [rdi]
						MULPS xmm0, xmm1					
						SUBPS xmm2, xmm0
						MOVNTPS [rdi], xmm2
					nomodify:

					ADD rdi, 16
					
				SUB r13d, 1
				JNZ inroloop
			align 16
				xmm1zero:
					SHL r13, 4
					ADD rdi, r13
	
				MOV r13d, r8d
				SHL r13, 4					

				SUB rsi, r13

				SHR r13, 4

			SUB ebx, 1
			JNZ incoloop
			

; search for next pivot
	
		MOV rsi, rax
		MOV rdi, r11 ; [rcx+matrix_elements]
				
		MOV ebx, [rcx+matrix_hdr_coCount]
		MOV eax, [rcx+matrix_hdr_divR]
		
		TEST rsi, rsi
		JNZ nofirstcol
			SHL rax, 4
			ADD rdi, rax
			SUB rsi, rax
			SHR rax, 4
			
			SUB ebx, 1
		nofirstcol:

		XORPS xmm5, xmm5

		MOV r14, rdx ; pivot row offset

		PUSH 0
		PUSH 0
	align 16										
		mycoloop:
			XORPS xmm1, xmm1
			
			MOV r13d, [rdi]
			XOR r13d, [float_special]
			
			JZ skippit
				
				MOVSS xmm1, DWORD PTR [rdi+rdx]
				UNPCKLPS xmm1, xmm1
				MOVLHPS xmm1, xmm1

				DIVPS xmm1, xmm12
				
			align 16
				myroloop:
					XORPS xmm4, xmm4

					MOVAPS xmm0, [rsi+rdi]
					
					MOVAPS xmm3, xmm0
					ANDPS xmm3, xmm13
					
					MOVAPS xmm2, [rdi]

					CMPPS xmm3, xmm4, 4 ; not equal
					MOVMSKPS r13, xmm3					

					TEST r13, r13
					JZ nomodifyme
						MULPS xmm0, xmm1					
						SUBPS xmm2, xmm0				
						MOVAPS [rdi], xmm2

					nomodifyme:

					ANDPS xmm2, xmm13
					MOVAPS xmm4, xmm2

					CMPPS xmm2, xmm5, 2 ; less

					MOVMSKPS rdx, xmm2
						
					XOR rdx, 0Fh
					JZ dienachste_1
				
						MOVSS xmm5, DWORD PTR [rdi]				
						MOVSS xmm3, DWORD PTR [rdi+4]
						
						ANDPS xmm5, xmm13				
						ANDPS xmm3, xmm13

						MAXSS xmm5, xmm3

						MOVSS xmm3, DWORD PTR [rdi+8]
						ANDPS xmm3, xmm13

						MAXSS xmm5, xmm3

						MOVSS xmm3, DWORD PTR [rdi+12]
						ANDPS xmm3, xmm13

						MAXSS xmm5, xmm3
				
						UNPCKLPS xmm5, xmm5
						MOVLHPS xmm5, xmm5

						CMPPS xmm4, xmm5, 0; equal
						MOVMSKPS rdx, xmm4

						BSF rdx, rdx

						NEG rdx
						LEA rdx, [rax*4+rdx]

						MOV [rsp], rdx ; row
						MOV [rsp+8], ebx ; col
									
				align 16
					dienachste_1:
					
					ADD rdi, 16			
					
				SUB eax, 1
				JNZ myroloop
		align 16							
			skippit:	
				SHL rax, 4
				ADD rdi, rax

; offload xmm1 to xmem
				ADD rax, rbp
				MOV rdx, r14
				ADD rax, r14	
				MOVSS DWORD PTR [rdi+rax], xmm1

				MOV eax, [rcx+matrix_hdr_divR]			
				SHL rax, 4

				SUB rsi, rax
				JNZ notcolit					
					SUB ebx, 1
					SUB rsi, rax
					ADD rdi, rax
				notcolit:

				SHR rax, 4
			
		SUB ebx, 1
		JG mycoloop

		MOVAPS xmm12, xmm5

; mark previous pivot column unused
		
		MOV eax, [float_special]
		MOV [rsi+rdi], eax

		MOV rax, [rsp]
		OR rax, [rsp+8]
		JZ singularmatrix


SUB r12d, 1
JNZ mainloop


; reverse substitution
	MOV rsi, r11 ; [rcx+matrix_elements]
	ADD rbp, rsi

	XOR rdx, rdx
	MOV eax, [rcx+matrix_hdr_coCount]
	SUB rax, [rsp+8]
	MOV [rsp+8], rax

	MOV ebx, [rcx+matrix_hdr_divR]	
	
	MUL ebx

	SHL rax, 4
	ADD rsi, rax ; pivot column

	MOV edx, ebx
	
	SHL rdx, 2
	SUB rdx, [rsp]
	MOV [rsp], rdx
	SHL rdx, 2 ; pivot row offset

	MOVSS xmm12, DWORD PTR [rsi+rdx]
	UNPCKLPS xmm12, xmm12
	MOVLHPS xmm12, xmm12

	MULSS xmm15, xmm12
	
	SHL rbx, 4
	ADD rbp, rbx

	ADD rsp, 16

	MOV r14, rax

	MOV rsi, rbp
	ADD rsi, rax

	MOV rdi, r15

	MOV ebx, r9d
	MOV r13d, r8d

	SUB rsi, rdi
align 16								
	coloop1:
		MOV eax, [rdi+rdx]
		AND eax, [float_cs_mask]
		JZ xmm1zerof
		MOVSS xmm1, DWORD PTR [rdi+rdx]

		UNPCKLPS xmm1, xmm1
		MOVLHPS xmm1, xmm1

		DIVPS xmm1, xmm12				

		MOVSS DWORD PTR [rdi+rdx], xmm1

	align 16
		roloop1:
			XORPS xmm4, xmm4
			MOVAPS xmm0, [rsi+rdi]
			
; test for zero in xmm0, eax available
			MOVAPS xmm3, xmm0
			ANDPS xmm3, xmm13

			CMPPS xmm3, xmm4, 4 ; not equal
			MOVMSKPS rax, xmm3
			TEST rax, rax
			JZ nomodify1
				MOVAPS xmm2, [rdi]
				MULPS xmm0, xmm1			
				SUBPS xmm2, xmm0
				MOVNTPS [rdi], xmm2
			nomodify1:

			ADD rdi, 16
					
		SUB r13d, 1
		JNZ roloop1
	align 16			
		xmm1zerof:
			SHL r13, 4
			ADD rdi, r13
	
		MOV r13d, r8d
		SHL r13, 4					

		SUB rsi, r13

		SHR r13, 4

	SUB ebx, 1
	JNZ coloop1

	MOV eax, [rcx+matrix_hdr_coCount]
	MOV edx, [rcx+matrix_hdr_roCount]

	CMP eax, edx
	CMOVA eax, edx
	
	MOV [rcx+matrix_hdr_rank], eax

	SUB eax, 2
	JLE exita
	MOV r12d, eax
	

; rest of it

align 16
restloop:
	MOV rsi, rbp

	MOV rdi, r15
	
	XOR rdx, rdx
	MOV eax, [rcx+matrix_hdr_coCount]
	SUB rax, [rsp+8]
	MOV [rsp+8], rax

	MOV ebx, [rcx+matrix_hdr_divR]	
	
	MUL ebx

	SHL rax, 4
	ADD rsi, rax ; pivot column

	MOV edx, ebx
	
	SHL rdx, 2
	SUB rdx, [rsp]
	MOV [rsp], rdx
	SHL rdx, 2 ; pivot row offset

	ADD rsp, 16

	MOV ebx, r9d
	MOV r13d, r8d

	SUB rsi, rdi

align 16								
	restcoloop:
		MOV eax, [rdi+rdx]
		AND eax, [float_cs_mask]
		JZ xmm1zeror
		MOVSS xmm1, DWORD PTR [rdi+rdx]

		UNPCKLPS xmm1, xmm1
		MOVLHPS xmm1, xmm1

	align 16
		restroloop:
			MOVAPS xmm0, [rsi+rdi]
			
; test for zero in xmm0, eax available
			MOV eax, [rsi+rdi]
			OR eax, [rsi+rdi+4]
			OR eax, [rsi+rdi+8]
			OR eax, [rsi+rdi+12]

			AND eax, [float_cs_mask]
			JZ restnomodify
				MOVAPS xmm2, [rdi]
				MULPS xmm0, xmm1			
				SUBPS xmm2, xmm0
				MOVNTPS [rdi], xmm2
			restnomodify:

			ADD rdi, 16
					
		SUB r13d, 1
		JNZ restroloop
		
		xmm1zeror:
			SHL r13, 4
			ADD rdi, r13
				
		MOV r13d, r8d
		SHL r13, 4					

		SUB rsi, r13

		SHR r13, 4

	SUB ebx, 1
	JNZ restcoloop

SUB r12d, 1
JNZ restloop

	MOV eax, [rcx+matrix_hdr_coCount]
	SUB rax, [rsp+8]
	MOV [rsp+8], rax

	MOV edx, [rcx+matrix_hdr_divR]	
	
	SHL rdx, 2
	SUB rdx, [rsp]
	MOV [rsp], rdx

	MOV eax, [rcx+matrix_hdr_rank]
	
	JMP exita

singularmatrix:
	MOV eax, [rcx+matrix_hdr_coCount]
	MOV edx, [rcx+matrix_hdr_roCount]

	CMP eax, edx
	CMOVA eax, edx

	SUB eax, r12d

	MOV [rcx+matrix_hdr_rank], eax
	XOR rax, rax

exita:
	MOVSS DWORD PTR [rcx+matrix_hdr_det], xmm15

	MOV rdi, r11 ; [rcx+matrix_elements]
	MOV [rdi], r9

	MOV edx, [rcx+matrix_hdr_divR]
	SHL rdx, 4
	MOV [rdi+8], rdx

	MOV edx, [rcx+matrix_hdr_rank]
	MOV [rdi+16], rdx
	
	ADD rdi, 24

	SHL rdx, 4
	SUB rsp, rdx
	SHR rdx, 4

	ADD rsp, 16

	align 16
	pvecloop:
		POP QWORD PTR [rdi]
		POP QWORD PTR [rdi+8]
		ADD rdi, 16

	SUB edx, 1
	JNZ pvecloop


	POP r15
	POP r14
	POP r13
	POP r12

	POP rbx
	POP rdi
	POP rsi
	
	POP rbp
	RET
MF_InverseMultiply	ENDP


ALIGN 16
MF_InverseMultiply44 PROC

;	MOV rax, [rcx+matrix_elements]
		
	MOVAPS xmm14, [rdx]
	MOVAPS xmm15, [rdx+16]
	MOVAPS xmm10, [rdx+32]
	MOVAPS xmm11, [rdx+48]

	MOVAPS xmm0, xmm14
	SHUFPS xmm0, xmm14, 0B1h
	
	MOVAPS xmm1, xmm0
	MOVAPS xmm2, xmm0

	MULPS xmm0, xmm15
	MULPS xmm1, xmm10
	
	HSUBPS xmm0, xmm1
	MULPS xmm2, xmm11

	MOVAPS xmm1, xmm0
	
	SHUFPS xmm1, xmm0, 05h
	MOVAPS xmm3, xmm1

	MULPS xmm1, xmm10
	MULPS xmm3, xmm11
		
	SHUFPS xmm0, xmm0, 0AFh
	MOVAPS xmm4, xmm0
	
	MULPS xmm4, xmm11
	MULPS xmm0, xmm15

	SUBPS xmm0, xmm1

	MOVAPS xmm1, xmm15
	SHUFPS xmm1, xmm15, 0B1h

	MOVAPS xmm5, xmm1

	MULPS xmm1, xmm10
	MULPS xmm5, xmm11

	HSUBPS xmm2, xmm1
	MOVAPS xmm1, xmm2

	SHUFPS xmm2, xmm1, 0AFh
	MOVAPS xmm12, xmm2

	MULPS xmm2, xmm14
	MULPS xmm12, xmm11

	SUBPS xmm0, xmm2 ; row 4

	SHUFPS xmm1, xmm1, 05h
	MOVAPS xmm2, xmm1

	MULPS xmm1, xmm15
	MULPS xmm2, xmm10
	
	SUBPS xmm3, xmm1
	SUBPS xmm2, xmm4

	MOVAPS xmm1, xmm10
	SHUFPS xmm1, xmm10, 0B1h
	MULPS xmm1, xmm11

	HSUBPS xmm1, xmm5
	MOVAPS xmm5, xmm1

	SHUFPS xmm5, xmm1, 05h
	MOVAPS xmm4, xmm5

	MULPS xmm4, xmm14
	MULPS xmm5, xmm15

	SHUFPS xmm1, xmm1, 0AFh
	MOVAPS xmm13, xmm1

	MULPS xmm1, xmm14
	MULPS xmm13, xmm10

	SUBPS xmm2, xmm4 ; row 2
	ADDPS xmm3, xmm1 ; row 3

	ADDPS xmm12, xmm5
	SUBPS xmm12, xmm13 ; row 1

	SHUFPS xmm12, xmm12, 0B1h
	SHUFPS xmm2, xmm2, 0B1h

	XORPS xmm12, [sign_special]
	XORPS xmm2, [sign_special]

	SHUFPS xmm3, xmm3, 0B1h
	SHUFPS xmm0, xmm0, 0B1h

	XORPS xmm3, [sign_special]
	XORPS xmm0, [sign_special]

	MOVAPS xmm1, xmm12
	MULPS xmm1, xmm14

	HADDPS xmm1, xmm1
	HADDPS xmm1, xmm1


	MOVSS DWORD PTR [rcx+matrix_hdr_det], xmm1

	MOV eax, [rcx+matrix_hdr_det]
	AND eax, [float_cs_mask]

	JZ singular
		DIVPS xmm12, xmm1
		DIVPS xmm2, xmm1
		DIVPS xmm3, xmm1
		DIVPS xmm0, xmm1

		TEST r9, r9
		JZ offload
			
		align 16
			apploop:
				MOVAPS xmm1, [r9]
				MOVAPS xmm4, xmm1
				MOVAPS xmm5, xmm1
				MOVAPS xmm13, xmm1

				MULPS xmm1, xmm12
				MULPS xmm4, xmm2
				MULPS xmm5, xmm3
				MULPS xmm13, xmm0

				HADDPS xmm1, xmm4
				HADDPS xmm5, xmm13

				HADDPS xmm1, xmm5

				MOVNTPS [r9], xmm1

				ADD r9, 16

			SUB r8d, 1
			JNZ apploop
		JMP singular
		offload:

		;	MOV rdx, [rcx+matrix_elements]

			MOVAPS xmm1, xmm12

			UNPCKLPS xmm12, xmm2
			UNPCKHPS xmm1, xmm2

			MOVAPS xmm2, xmm3

			UNPCKLPS xmm3, xmm0
			UNPCKHPS xmm2, xmm0

			MOVAPS xmm0, xmm2

			MOVHLPS xmm0, xmm1
			MOVLHPS xmm1, xmm2

			MOVAPS xmm2, xmm3
			MOVHLPS xmm2, xmm12
			MOVLHPS xmm12, xmm3


			MOVAPS [rdx], xmm12
			MOVAPS [rdx+16], xmm2
			MOVAPS [rdx+32], xmm1
			MOVAPS [rdx+48], xmm0

	singular:
	

	RET
MF_InverseMultiply44 ENDP


ALIGN 16
MF_InverseMultiply22 PROC

;	MOV rax, [rcx+matrix_elements]
		
	XORPS xmm13, xmm13
	MOVAPS xmm0, [rdx]
	UNPCKLPS xmm0, [rdx+16]
	MOVAPS xmm1, xmm0

	SHUFPS xmm1, xmm1, 1Bh

	MULPS xmm1, xmm0
	HSUBPS xmm1, xmm1

	MOVSS DWORD PTR [rcx+matrix_hdr_det], xmm1

	MOV eax, [rcx+matrix_hdr_det]
	AND eax, [float_cs_mask]
	
	JZ singular
		SHUFPS xmm1, xmm1, 14h
		DIVPS xmm0, xmm1
		SHUFPS xmm0, xmm0, 27h	
		
		TEST r9, r9
		JZ offload
			

		align 16
			apploop:
				MOVDDUP xmm1, QWORD PTR [r9]
				MULPS xmm1, xmm0

				HADDPS xmm1, xmm13

				MOVNTPS [r9], xmm1

				ADD r9, 16

			SUB r8d, 1
			JNZ apploop
		JMP singular
		offload:

	;		MOV rdx, [rcx+matrix_elements]

			MOVHLPS xmm1, xmm0
			UNPCKLPS xmm0, xmm1

			MOVLPS QWORD PTR [rdx], xmm0
			MOVHPS QWORD PTR [rdx+16], xmm0

	singular:
	

	RET
MF_InverseMultiply22 ENDP

ALIGN 16
MF_InverseMultiply33 PROC

;	MOV rax, [rcx+matrix_elements]
		
	MOVAPS xmm0, [rdx]	
	MOVAPS xmm1, [rdx+16]	
	MOVAPS xmm2, [rdx+32]

	SHUFPS xmm2, xmm2, 86h
	XORPS xmm13, xmm13

	SHUFPS xmm1, xmm1, 29h
	SHUFPS xmm0, xmm0, 29h

	MOVAPS xmm3, xmm1
	MOVAPS xmm4, xmm0

	MULPS xmm3, xmm2
	MULPS xmm4, xmm2
		

	SHUFPS xmm1, xmm1, 71h
	MOVAPS xmm5, xmm1
	MULPS xmm5, xmm0

	SHUFPS xmm2, xmm2, 99h
	SHUFPS xmm1, xmm0, 36h

	MULPS xmm2, xmm1

	HSUBPS xmm3, xmm4
	HSUBPS xmm5, xmm2

	SHUFPS xmm1, xmm1, 0E1h
	MOVHLPS xmm2, xmm1
	MOVLHPS xmm2, xmm13

	MULPS xmm2, xmm1
	HSUBPS xmm2, xmm13

	MOVHLPS xmm4, xmm3
	
	MOVHLPS xmm0, xmm5
	UNPCKLPS xmm0, xmm13

	MOVLHPS xmm3, xmm0
	SHUFPS xmm4, xmm0, 0E4h

	MOVLHPS xmm5, xmm2

	MOVAPS xmm0, [sign_special]
	SHUFPS xmm0, xmm0, 0

	XORPS xmm4, xmm0
	
	MOVAPS xmm1, xmm3
	MULPS xmm1, [rdx]

	HADDPS xmm1, xmm1
	HADDPS xmm1, xmm1


	MOVSS DWORD PTR [rcx+matrix_hdr_det], xmm1

	MOV eax, [rcx+matrix_hdr_det]
	AND eax, [float_cs_mask]

	JZ singular
		DIVPS xmm3, xmm1
		DIVPS xmm4, xmm1
		DIVPS xmm5, xmm1

		TEST r9, r9
		JZ offload
			
		align 16
			apploop:
				MOVAPS xmm0, [r9]
				MOVAPS xmm1, xmm0
				MOVAPS xmm2, xmm0

				MULPS xmm0, xmm3
				MULPS xmm1, xmm4
				MULPS xmm2, xmm5

				HADDPS xmm0, xmm1
				HADDPS xmm2, xmm13

				HADDPS xmm0, xmm2

				MOVNTPS [r9], xmm0

				ADD r9, 16

			SUB r8d, 1
			JNZ apploop
		JMP singular
		offload:

		;	MOV rdx, [rcx+matrix_elements]

			MOVAPS xmm1, xmm3

			UNPCKLPS xmm3, xmm4
			UNPCKHPS xmm1, xmm4

			MOVAPS xmm4, xmm5

			UNPCKLPS xmm5, xmm13
			UNPCKHPS xmm4, xmm13

			MOVLHPS xmm1, xmm4

			MOVAPS xmm4, xmm5
			MOVHLPS xmm4, xmm3
			MOVLHPS xmm3, xmm5

			MOVAPS [rdx], xmm3
			MOVAPS [rdx+16], xmm4
			MOVAPS [rdx+32], xmm1
			MOVAPS [rdx+48], xmm13

	singular:
	

	RET
MF_InverseMultiply33 ENDP


; vector ==========================================================================================================================================================================================================================================================================
ALIGN 16
VF_E1Norm	PROC
	
	MOVAPS xmm1, [float_cs_mask]
	XORPS xmm0, xmm0

align 16
	calcloop:
		MOVAPS xmm2, [rcx]
		ANDPS xmm2, xmm1

		ADDPS xmm0, xmm2
		ADD rcx, 16
	SUB edx, 1
	JNZ calcloop

	HADDPS xmm0, xmm0
	HADDPS xmm0, xmm0

	RET
VF_E1Norm	ENDP

ALIGN 16
VF_SupNorm	PROC
	
	MOVAPS xmm1, [float_cs_mask]
	XORPS xmm0, xmm0

align 16
	calcloop:
		MOVAPS xmm2, [rcx]
		ANDPS xmm2, xmm1

		MAXPS xmm0, xmm2

		ADD rcx, 16
	SUB edx, 1
	JNZ calcloop

	MOVHLPS xmm2, xmm0
	MAXPS xmm0, xmm2

	MOVAPS xmm2, xmm2
	SHUFPS xmm2, xmm0, 1

	MAXSS xmm0, xmm2

	RET
VF_SupNorm	ENDP


ALIGN 16
VF_Sqrt	PROC
	
	XORPS xmm0, xmm0
	MOV rax, rcx
	MOV r8d ,edx

align 16
	testloop:
		ORPS xmm0, [rax]
		ADD rax, 16
	SUB r8d, 1
	JNZ testloop
	
	MOVMSKPS rax, xmm0

	TEST rax, rax
	JNZ negative

		calcloop:
			SQRTPS xmm0, [rcx]
			MOVAPS [rcx], xmm0
			ADD rcx, 16
		SUB edx, 1
		JNZ calcloop
		
		XOR rax, rax
	negative:
	
	RET
VF_Sqrt	ENDP


ALIGN 16
VF_DotProduct	PROC

		XORPS xmm0, xmm0
		SUB rdx, rcx

	align 16
		lcloop:
			MOVAPS xmm1, [rcx]
			MULPS xmm1, [rdx+rcx]
						
			ADD rcx, 16
			SUB r8d, 1

			ADDPS xmm0, xmm1
			
		JNZ lcloop

		HADDPS xmm0, xmm0
		HADDPS xmm0, xmm0

	RET
VF_DotProduct	ENDP

ALIGN 16
VF_AddVector	PROC
	SUB rdx, rcx
	align 16
		lcloop:
			MOVAPS xmm0, [rdx+rcx]
			ADDPS xmm0, [rcx]
			
			MOVAPS [rcx], xmm0

			ADD rcx, 16
			SUB r8d, 1
			
		JNZ lcloop

	RET
VF_AddVector	ENDP

ALIGN 16
VF_SubVector	PROC
	SUB rdx, rcx
	align 16
		lcloop:
			MOVAPS xmm0, [rcx]
			SUBPS xmm0, [rdx+rcx]
			
			MOVAPS [rcx], xmm0

			ADD rcx, 16
			SUB r8d, 1
			
		JNZ lcloop

	RET
VF_SubVector	ENDP

ALIGN 16
VF_Scale	PROC
	MOVSS xmm0, DWORD PTR [r8]

	UNPCKLPS xmm0, xmm0
	MOVLHPS xmm0, xmm0
		
align 16
	muloop:
		MOVAPS xmm1, [rcx]
		MULPS xmm1, xmm0
		MOVAPS [rcx], xmm1

		ADD rcx, 16
		SUB edx, 1
	JNZ muloop

	RET
VF_Scale	ENDP

ALIGN 16
VF_Negate	PROC
	MOVAPS xmm0, [sign_special]
	SHUFPS xmm0, xmm0, 0

align 16
	negateloop:
		MOVAPS xmm1, [rcx]
		XORPS xmm1, xmm0
			
		MOVAPS [rcx], xmm1
			
		ADD rcx, 16

		SUB edx, 1
	JNZ negateloop

	RET
VF_Negate	ENDP


ALIGN 16
VF_ComponentMultiply	PROC
	SUB rdx, rcx
align 16
	muloop:
		MOVAPS xmm0, [rdx+rcx]
		MULPS xmm0, [rcx]
		MOVNTPS [rcx], xmm0

		ADD rcx, 16
	SUB r8d, 1
	JNZ muloop

	RET
VF_ComponentMultiply	ENDP




END