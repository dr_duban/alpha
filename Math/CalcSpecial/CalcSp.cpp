/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>
#include "CoreLibrary\SFSCore.h"

#include "Math\CalcSpecial.h"

#define MAX_D_LEVEL	2048

__declspec(align(16)) double sin_fit_polyno[4*(MAX_D_LEVEL+1)];
double trigono_detail = 0.0f;


extern "C" void GenerateSinPolyFit(unsigned int);

unsigned int Calc::Initialize(unsigned int detail_level) {
	unsigned int dpower(0);	
	for (;detail_level>>=1;dpower++);

	SFSco::SetStdPrecisionFloat(0);

	detail_level = (dpower<=7)?128:(1<<dpower);
	
	if (detail_level>MAX_D_LEVEL) detail_level = MAX_D_LEVEL;
	
	trigono_detail = 2.0*detail_level;

	GenerateSinPolyFit(detail_level);

	return 0;
}


