
; safe-fail math
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


EXTERN	?sin_fit_polyno@@3PANA:DWORD
EXTERN	?trigono_detail@@3NA:QWORD

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16
double_pi180	QWORD	3F91DF46A2529D39h, 3F91DF46A2529D39h
double_2		QWORD	4000000000000000h, 4000000000000000h

double_1_360	QWORD	3F66C16C16C16C17h, 3F66C16C16C16C17h
double_05		QWORD	3FE0000000000000h, 3FE0000000000000h
double_cs_mask	QWORD	7FFFFFFFFFFFFFFFh, 7FFFFFFFFFFFFFFFh
double_pi		QWORD	400921FB54442D18h, 400921FB54442D18h
double_sign		QWORD	8000000000000000h, 8000000000000000h


float_cs_mask	DWORD	7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh

.CODE

ALIGN 16
HWRand	PROC

	RET
HWRand	ENDP


ALIGN 16
GenerateSinPolyFit	PROC
	SUB rsp, 16

		ADD ecx, 1
		LEA rdx, [?sin_fit_polyno@@3PANA]
		MOV DWORD PTR [rsp], 0

		FLDPI
		FDIV QWORD PTR [?trigono_detail@@3NA]


		FLDZ		
		FSUB st(0), st(1)
		

	align 16
		caloop:

			FLD st(0)
				FSIN
		
				FLD st(1)
					FADD st(0), st(3)
					FSIN

					FLD st(2)
						FADD st(0), st(4)
						FADD st(0), st(4)
						FSIN

						FLD st(0)
							FSUB st(0), st(2)
							FADD st(0), st(3)
							FSUB st(0), st(2)

							FLD st(0)
								FMUL [double_05]

							FSTP QWORD PTR [rdx]
		
							FLD st(3)
							FSUBP st(2), st(0)
		
							FLD [double_05]
							FMULP st(2), st(0)

							FILD DWORD PTR [rsp]
							FMULP st(1), st(0)

							FLD st(1)
								FSUB st(0), st(1)

							FSTP QWORD PTR [rdx+8]

							FILD DWORD PTR [rsp]
							FMULP st(1), st(0)

							FMUL [double_05]

							FILD DWORD PTR [rsp]
							FMULP st(2), st(0)

						FSUBP st(1), st(0)
					FSUBP st(1), st(0)
				FSTP QWORD PTR [rdx+16]
		
			FFREE st(0)
			FINCSTP

			FLDZ
			FSTP QWORD PTR [rdx+24]

			FADD st(0), st(1)

			ADD DWORD PTR [rsp], 1
			ADD rdx, 32

		DEC ecx
		JNZ caloop

		FFREE st(0)
		FINCSTP

		FFREE st(0)
		FINCSTP

	ADD rsp, 16
	RET
GenerateSinPolyFit	ENDP

ALIGN 16
FloatSin	PROC
	CVTSS2SD xmm0, xmm0
	MULSD xmm0, [double_1_360]	

	CVTTSD2SI rax, xmm0
	CVTSI2SD xmm1, rax
	SUBSD xmm0, xmm1

	MOVMSKPD rcx, xmm0

	ANDPD xmm0, [double_cs_mask]
	MOVSD xmm2, [double_05]

	ADDSD xmm0, xmm0

	CVTTSD2SI rax, xmm0
	CVTSI2SD xmm1, rax
	SUBSD xmm0, xmm1

	XOR rax, rcx
	ROR rax, 1	
		
	MINSD xmm2, xmm0
	ADDSD xmm2, xmm2
	SUBSD xmm2, xmm0

	LEA rdx, [?sin_fit_polyno@@3PANA]

	MULSD xmm2, [?trigono_detail@@3NA]
	CVTSD2SI rcx, xmm2
	SHL rcx, 5
	
	MOVDDUP xmm2, xmm2
	MULSD xmm2, xmm2
		
	XORPD xmm0, xmm0

	MULPD xmm2, [rdx+rcx]
	MOVHLPS xmm0, xmm2	
	ADDSD xmm2, QWORD PTR [rdx+rcx+16]
	ADDSD xmm0, xmm2

	MOVD xmm1, rax
	ORPD xmm0, xmm1
	
	CVTSD2SS xmm0, xmm0
	RET
FloatSin	ENDP


ALIGN 16
RealSin	PROC
	MOVSD QWORD PTR [rsp-8], xmm0
	FLD QWORD PTR [rsp-8]
	FMUL [double_pi180]
	
	FSIN
	
	FSTP QWORD PTR [rsp-8]
	MOVSD xmm0, QWORD PTR [rsp-8]

	RET
RealSin	ENDP


ALIGN 16
RealCos	PROC
	MOVSD QWORD PTR [rsp-8], xmm0
	FLD QWORD PTR [rsp-8]
	FMUL [double_pi180]
	
	FCOS
	
	FSTP QWORD PTR [rsp-8]
	MOVSD xmm0, QWORD PTR [rsp-8]


	RET
RealCos	ENDP


ALIGN 16
FloatSqrt	PROC
	SQRTSS xmm0, xmm0
	RET
FloatSqrt	ENDP

ALIGN 16
Float2Sqrt	PROC
	MOVLPS xmm0, QWORD PTR [rcx]
	SQRTPS xmm0, xmm0
	MOVLPS QWORD PTR [rcx], xmm0

	RET
Float2Sqrt	ENDP

ALIGN 16
Float4Sqrt	PROC
	MOVUPS xmm0, [rcx]
	SQRTPS xmm0, xmm0
	MOVUPS [rcx], xmm0

	RET
Float4Sqrt	ENDP

ALIGN 16
Float4NSqrt	PROC
	MOVUPS xmm0, [rcx]
	SQRTPS xmm0, xmm0
	MOVUPS [rcx], xmm0

	LEA rcx, [rcx + 16]
	DEC edx
	JNZ Float4Sqrt

	RET
Float4NSqrt	ENDP

ALIGN 16
FloatAbs	PROC
	ANDPS xmm0, [float_cs_mask]
	RET
FloatAbs	ENDP


ALIGN 16
Float4NAbs	PROC
	MOVAPS xmm1, XMMWORD PTR [float_cs_mask]
	
align 16
	abs_loop:
		MOVUPS xmm0, [rcx]		
		ANDPS xmm0, xmm1
		MOVUPS [rcx], xmm0
		ADD rcx, 16

	DEC edx
	JNZ abs_loop

	RET
Float4NAbs	ENDP



ALIGN 16
FloatClear4	PROC

	UNPCKLPS xmm2, xmm2
	MOVLHPS xmm2, xmm2

align 16
	filoop:
		MOVNTPS [rcx], xmm2
		ADD rcx, 16

		DEC rdx
	JNZ filoop

	RET
FloatClear4	ENDP

ALIGN 16
FloatClear16	PROC
	UNPCKLPS xmm2, xmm2
	MOVLHPS xmm2, xmm2


align 16
	filoop:
		MOVNTPS [rcx], xmm2
		MOVNTPS [rcx+16], xmm2
		MOVNTPS [rcx+32], xmm2
		MOVNTPS [rcx+48], xmm2

		ADD rcx, 64

		DEC rdx
	JNZ filoop

	RET
FloatClear16	ENDP

ALIGN 16
FloatClear4N	PROC
	MOVUPS xmm0, [rdx]

align 16
	cpy_loop:
		MOVNTPS [rcx], xmm0
		ADD rcx, 16
	DEC r8
	JNZ cpy_loop

	RET
FloatClear4N	ENDP

ALIGN 16
FloatCopy4	PROC
	SUB rdx, rcx

align 16
	filoop:
		MOVAPS xmm0, [rdx+rcx]
		MOVNTPS [rcx], xmm0

		ADD rcx, 16

		DEC r8
	JNZ filoop

	SFENCE
	RET
FloatCopy4	ENDP

ALIGN 16
FloatCopy16	PROC

	SUB rdx, rcx

align 16
	filoop:
		MOVAPS xmm0, [rdx+rcx]
		MOVAPS xmm1, [rdx+rcx+16]
		MOVAPS xmm2, [rdx+rcx+32]
		MOVAPS xmm3, [rdx+rcx+48]

		MOVNTPS [rcx], xmm0
		MOVNTPS [rcx+16], xmm1
		MOVNTPS [rcx+32], xmm2
		MOVNTPS [rcx+48], xmm3

		ADD rcx, 64

		DEC r8
	JNZ filoop

	SFENCE
	RET
FloatCopy16	ENDP




END


