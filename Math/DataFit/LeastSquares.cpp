/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Math\DataFit.h"
#include "Math\Calc.h"

// linear =========================================================================================================================================================================================================

Calc::LinearLS_F::~LinearLS_F() {

}

Calc::LinearLS_F::LinearLS_F(unsigned int datacount, unsigned int varcount, const float * src) : MatrixF(datacount, varcount+1), _qForm(varcount+1, varcount+1), _weights(datacount, 1.0f) {
	_qForm.Identity();

	FloatClear4(_elements, _divR, 1.0f);
	for (unsigned int i(_roCount);i<_divR*4;i++) _elements[i] = 0.0f;

	
	if (src) {
		_elements += _divR*4;
		_coCount--;

		if (_roCount != (_divR<<2)) {
			FromBuffer(src);
		} else {
			FloatCopy4(_elements, src, _divR*_coCount);
		}

		_coCount++;
		_elements -= _divR*4;

		if (_coCount != (_divC<<2)) FloatClear4(_elements+(_divR<<2)*_coCount, _divR*((_divC<<2)-_coCount));
		
		_qForm = *this ^ *this;

	}
}

Calc::VectorF Calc::LinearLS_F::operator()(const VectorF & pline) const {
	VectorF result(pline);

	if (_roCount != _coCount) {		
		_qForm[(*this)(result)];
	} else {		
		[result];
	}

	return result;
}

// polynomial ===========================================================================================================================================================================================================

Calc::PolynomialLS_F::~PolynomialLS_F() {

}

Calc::PolynomialLS_F::PolynomialLS_F(unsigned int datacount, unsigned int polydeg, const float * src) : LinearLS_F(datacount, polydeg, 0) {

	if (src) {
		_elements += _divR*4;
		
		for (unsigned int i(0);i<_roCount;i++) _elements[i] = src[i];
		for (unsigned int i(_roCount);i<_divR*4;i++) _elements[i] = 0.0f;		

		if (polydeg>1) Initialize();
				
		_elements -= _divR*4;

		if (_coCount != (_divC<<2)) FloatClear4(_elements+(_divR<<2)*_coCount, _divR*((_divC<<2)-_coCount));

		_qForm = *this ^ *this;
	}
}

// ========================================================================================================================================================================================================================


