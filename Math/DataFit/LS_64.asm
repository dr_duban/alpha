
; safe-fail math
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


	matrix_elements			TEXTEQU <8>
	matrix_coCount			TEXTEQU	<16>
	matrix_roCount			TEXTEQU <20>
	matrix_divC				TEXTEQU <24>
	matrix_divR				TEXTEQU <28>
	matrix_rank				TEXTEQU <32>
	matrix_det				TEXTEQU <36>

	vector_roCount			TEXTEQU <16>
	vector_divisor			TEXTEQU <20>


.CODE

ALIGN 16
?Initialize@PolynomialLS_F@Calc@@IEAAXXZ	PROC
	MOV r8, [rcx+matrix_elements]
	MOV edx, [rcx+matrix_divR]

	MOV r10d, edx
	MOV r9d, edx		
	SHL r9, 4

	XOR rax, rax

	MOV ecx, [rcx+matrix_coCount]		
	SUB ecx, 2

align 16
	powerloop:
		MOVAPS xmm0, [rax+r8] ; first
		MULPS xmm0, [r8] ; previous
		
		MOVAPS [r9+r8], xmm0 ; next

		ADD r8, 16
		
		SUB r10d, 1
		JNZ powerloop

		MOV r10d, edx
		SUB rax, r9

	SUB ecx, 1
	JNZ powerloop

	RET
?Initialize@PolynomialLS_F@Calc@@IEAAXXZ	ENDP



























ALIGN 16
?LinearLeastSquares2D_SSE@@YAMPEAMI00@Z	PROC
	SUB rsp, 16

	MOV [rsp], edx
	MOV [rsp+4], edx

	ADD edx, 3
	SHR edx, 2

	XORPS xmm3, xmm3 ; y
	XORPS xmm4, xmm4 ; x
	XORPS xmm5, xmm5 ; xy
	XORPS xmm6, xmm6 ; x2
	XORPS xmm7, xmm7 ; y2

align 16
	calcloop:

		MOVAPS xmm0, [r8]
		MOVAPS xmm1, [r9]

		ADDPS xmm3, xmm0
		ADDPS xmm4, xmm1

		MOVAPS xmm2, xmm0

		MULPS xmm0, xmm1
		MULPS xmm1, xmm1

		ADDPS xmm5, xmm0
		ADDPS xmm6, xmm1

		MULPS xmm2, xmm2
		ADDPS xmm7, xmm2

		ADD r8, 16
		ADD r9, 16

		SUB edx, 1
	JNZ calcloop


	CVTPI2PS xmm0, QWORD PTR [rsp]

	HADDPS xmm3, xmm3
	HADDPS xmm4, xmm4
	HADDPS xmm5, xmm5
	HADDPS xmm6, xmm6
	HADDPS xmm7, xmm7
		
	MOVLHPS xmm0, xmm0 ; N

	HADDPS xmm3, xmm4 ; y, x
	HADDPS xmm5, xmm5 ; xy
		
	MOVAPS xmm4, xmm3
	SHUFPS xmm4, xmm4, 33h ; x, y, x, y

	HADDPS xmm7, xmm6 ; y2, x2
	
	MULPS xmm4, xmm3 ; x.y, y.y, x.x, x.y

	MOVSS xmm7, xmm5 ; xy, y2, x2, x2

	MULPS xmm7, xmm0
	SUBPS xmm7, xmm4

	SHUFPS xmm7, xmm7, 60h ; Sxy, Sxy, Sxx, Syy

	MOVHLPS xmm6, xmm7
	MOVLHPS xmm7, xmm7
	
	UNPCKLPS xmm6, xmm7

	MOVHLPS xmm4, xmm3

	DIVPS xmm7, xmm6 ; b, b, c, c
	
	MULSS xmm4, xmm7 ; xb
	SUBSS xmm3, xmm4 ; y - bx
	DIVSS xmm3, xmm0 ; a

	MOVHLPS xmm0, xmm7
	MULSS xmm0, xmm7 ; r2

	MOVSS DWORD PTR [rcx], xmm7
	MOVSS DWORD PTR [rcx+4], xmm3


	ADD rsp, 16
	RET
?LinearLeastSquares2D_SSE@@YAMPEAMI00@Z	ENDP






END

