/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

float FloatCos(float);

namespace Calc {


// auxilary Single class
class Single: public SFSco::Object {
	private:

	public:
		static const UI_64 type_id = 4022940217;

		Single();
		Single(const Single &);
		virtual ~Single();

		UI_64 Create(SFSco::Object *, UI_64 offset);

		Single & operator =(const float &);
		Single & operator =(const Single &);

		Single & operator +=(const float &);
		Single & operator +=(const Single &);

		Single & operator -=(const float &);
		Single & operator -=(const Single &);

		operator float();
};

class XInt: public SFSco::Object {

/*
	layout:
		DWORD_0:	number size in DWORDs
		DWORD_1:	most significant DWORD most significant on-bit index
		DWORD_2:	_reserved_
		DWORD_3:	reserved DWORD count minus 4

		DWORD_4:
			...		number
			...		...

*/


	public:		
		static const UI_64 type_id = 4022940241;

		XInt();
		XInt(const XInt &);
		virtual ~XInt();

		XInt & Square();
		XInt & Pow(unsigned int, const XInt &);

		XInt & operator =(const XInt &);
		XInt & operator =(UI_64);
		
		
		XInt & operator +=(const XInt &);
		XInt & operator -=(const XInt &);
		XInt & operator /=(const XInt &);
		XInt & operator %=(const XInt &);
		XInt & operator *=(const XInt &);
		XInt & operator ^(const unsigned int); // power


		XInt operator -(const XInt &);
		XInt operator +(const XInt &);
		XInt operator /(const XInt &);
		XInt operator %(const XInt &);
		XInt operator *(const XInt &);

		XInt & operator ^=(const XInt &); // xor
		XInt & operator &=(const XInt &); // and		
		XInt operator ^(const XInt &);
		XInt operator &(const XInt &);


		bool operator !=(const XInt &) const;
		bool operator ==(const XInt &) const;
		bool operator >=(const XInt &) const;
		bool operator <=(const XInt &) const;
		bool operator <(const XInt &) const;
		bool operator >(const XInt &) const;
};


	extern "C" {
		unsigned int XInt_Add(unsigned int *, const unsigned int *);
		unsigned int XInt_Sub(unsigned int *, const unsigned int *);

		unsigned int XInt_Div(unsigned int * dividend, const unsigned int * divisor, unsigned int * quotient); //  on completion the remainder is in dividend
		unsigned int XInt_Rem(unsigned int * dividend, const unsigned int * divisor);  //  on completion the remainder is in dividend

		unsigned int XInt_Xor(unsigned int *, const unsigned int *);
		unsigned int XInt_And(unsigned int *, const unsigned int *);

		unsigned int XInt_MulAB(unsigned int * val_a, const unsigned int * val_b, unsigned int * result);
		unsigned int XInt_Mul(unsigned int *);

		bool XInt_AEQU(const unsigned int *, const unsigned int *);
		bool XInt_BEQU(const unsigned int *, const unsigned int *);
		bool XInt_EQU(const unsigned int *, const unsigned int *);

	}

}


