/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

#include "Math\Matrix.h"

namespace Calc {

	class LinearLS_F: public MatrixF {		
		protected:
			MatrixF _qForm;
			VectorF _weights;
			
		public:
			LinearLS_F(unsigned int datacount, unsigned int varcount, const float * src); // datacount - number of samples, src - matrix datacount x varcount of variables values
			virtual ~LinearLS_F();

			// normal equations solver
			VectorF operator()(const VectorF &) const; // result vector r of munimization problem || b - Ax ||, input vector is b containing datacount samples, R2 = || b - Ar ||^2 

			// weighted inner products: implement or not?
			
	};

	class PolynomialLS_F: public LinearLS_F {
		protected:
			void Initialize();

		public:
			PolynomialLS_F(unsigned int datacount, unsigned int polydeg, const float * src); // src - vector of varibles values at which samples are taken
			virtual ~PolynomialLS_F();

	};


}

