/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma once

extern "C"
namespace Calc {
	void FloatClear4(void * dest, UI_64 count, float = 0.0f); // count number of 4- float vals to cover
	void FloatClear4N(void * dest, const float *, UI_64 count);
	
	void FloatClear16(void * dest, UI_64 count, float = 0.0f); // count number of 16- float vals to cover
	void FloatCopy4(void * dest, const void * src, UI_64 count);
	void FloatCopy16(void * dest, const void * src, UI_64 count);
	
	
	UI_64 MF_IdentityCheck(const float *, unsigned int coc);
	UI_64 MF_IdentityCheck3(const float *);

	void MF_FromBuffer(void *, float * dst, const float *);

	void MF_Negate(void * matrix_hdr, float * elem);
	void MF_Scale(void * matrix_hdr, float * elem, const float &);
	void MF_Scale43(float * elem, const float *, float * = 0);
	void MF_Scale44(float * elem, const float *, unsigned int);

	void MF_DiagonalMultiply(void * matrix_hdr, float * melem, void * vector_hdr, float * vecel);


	int MF_InverseMultiply(void * matrix_ahdr, float * alem, void * matrix_dhdr, float * dlem, void * xmem); // 0 - singular matrix, full pivoting Gauss-Jordan ; void * outp, void * dst, unsigned int cocount, unsigned int rdiv
	void MF_TransposeMultiply(void * matrix_ahdr, float * alem, void * matrix_bhdr, float * blem, float * xlem); // void * outp, void * dst, unsigned int vcount, unsigned int vdiv
	void MF_Multiply(void * matrix_ahdr, float * alem, void * matrix_bhdr, float * blem, float * xlem);
		
		
	void MF_TransposeMultiply44(float * alem, unsigned int bcount, float * blem, float * xlem); // void * dst, unsigned int vcount, void * outp = 0
	void MF_TransposeMultiply22(float * alem, unsigned int bcount, float * blem, float * xlem);
	
	void MF_Multiply44(float * alem, unsigned int bcount, float * blem, float * xlem);
	void MF_Multiply22(float * alem, unsigned int bcount, float * blem, float * xlem);
	

	int MF_InverseMultiply44(void * matrix_ahdr, float * alem, unsigned int bcount, float * blem); // 0 - singular matrix, cofactor calculator
	int MF_InverseMultiply22(void * matrix_ahdr, float * alem, unsigned int bcount, float * blem); // 0 - singular matrix, cofactor calculator
	int MF_InverseMultiply33(void * matrix_ahdr, float * alem, unsigned int bcount, float * blem); // 0 - singular matrix, cofactor calculator

	void MF_AddMatrix(void * matrix_ahdr, float * alem, void * matrix_bhdr, const float * blem);
	void MF_SubMatrix(void * matrix_ahdr, float * alem, void * matrix_bhdr, const float * blem);
	
	
	void VF_Negate(float * elem, unsigned int);

	void VF_Scale(float * elem, unsigned int, const float &);

	void VF_SubVector(float *, const float *, unsigned int);
	void VF_AddVector(float *, const float *, unsigned int);
		
	unsigned int VF_Sqrt(float *, unsigned int);
				
	void VF_ComponentMultiply(float *, const float *, unsigned int);

	float VF_DotProduct(const float *, const float *, unsigned int);
	float VF_E1Norm(float *, unsigned int);
	float VF_SupNorm(float *, unsigned int);


	unsigned int FloatPermute(void * src, void * map);


}

/*
	map = permutaion matrix in form of the list

	first entry - number of vectors to permute
	second entry - byte offset between two sequencial vectors

	third entry - number of pivots in permutation matrix
	
	pairs indicating permutaion pivots position: (row, column)
	
	entries are unsigned 4-byte integers in x32, and unsigned 8-byte integers in x64
	

	0 result - proper permutation


*/




/*	
UI_64 Calc::Randnum() {

ALIGN 16
HWRand	PROC	SYSCALL
	rndloop:
		RDRAND eax
	JNC rndloop

	RET
HWRand	ENDP

	if (IDCPU::tossRand()) return HWRand();
	else {
// random num generator

		return 0;
	}

	return 0;
}
*/



