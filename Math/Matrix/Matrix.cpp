/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include <Windows.h>
#include "CoreLibrary\SFSCore.h"

#include "Math\Matrix.h"
#include "Math\Calc.h"

#include "Math\MatrixSpecial.h"
#include "Math\CalcSpecial.h"

#include "System\SysUtils.h"

Calc::MatrixF::Header * Calc::MatrixF::Init(void * dst, unsigned int rowc, unsigned int colc) {
	Header * result = reinterpret_cast<Header *>(dst);
	
	result->roCount = rowc;
	result->divR = (rowc+3)>>2;

	if (colc == -1) {
		colc = 0;
		result->t_flag = 1;
	} else {
		result->t_flag = 0;
	}

	result->coCount = colc;
	result->divC = (colc+3)>>2;

	result->rank = 0;
	result->det = 0.0f;
	result->reserved = 0;
	

	return result;

}

void Calc::MatrixF::Init(Header * hdr, const float * src) {
	if (hdr->roCount != (hdr->divR<<2)) {
		MF_FromBuffer(hdr, reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header)), src);
	} else {
		FloatCopy4(reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header)), src, hdr->divR*hdr->coCount);
	}

	if (hdr->coCount != (hdr->divC<<2)) FloatClear4(reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header)) + (hdr->divR<<2)*hdr->coCount, hdr->divR*((hdr->divC<<2) - hdr->coCount), 0.0f);
		
}

Calc::MatrixF::MatrixF() {
	SetTID(type_id);
}

Calc::MatrixF::MatrixF(const MatrixF & ref) : Object(ref) {

}

Calc::MatrixF::~MatrixF() {

}

// intializers =====================================
UI_64 Calc::MatrixF::Create(unsigned int rowC, unsigned int colC, const float * src, SFSco::IAllocator * meall) {
	UI_64 result = New(type_id, sizeof(Header) + ((rowC+3)>>2)*(colC?((colC+3)>>2):1)*16*sizeof(float), meall);
	
	if (result != -1) {
		if (void * mhepo = Acquire()) {
			Header * hdr = Init(mhepo, rowC, colC);
			if (src) Init(hdr, src);	
		
			Release();
		} else {
			result = -1;
		}
	}

	return result;
}

UI_64 Calc::MatrixF::Create(const MatrixF & ref) {
	UI_64 result(-1);

	if (*dynamic_cast<SFSco::Object *>(this)) {
		*this = ref;
		result = GetID();
	} else {	
		*dynamic_cast<SFSco::Object *>(this) = ref;

		if (Header * refhdr = reinterpret_cast<Header *>(Acquire())) {		
			UI_64 roc = refhdr->divR, coc = refhdr->divC;
			Release();
			refhdr = 0;

			result = Clone(roc*(coc?coc:1)*16*sizeof(float), sizeof(Header));
		}
	}
	
	return result;
}

UI_64 Calc::MatrixF::Create(const VectorF & ref, unsigned int repl) {
	float * refelements(0);
	UI_64 result(-1);

	if (Header * refhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire())) {		
		UI_64 roc = refhdr->divR, coc = (repl+3)>>2;

		const_cast<VectorF &>(ref).Release();
		refhdr = 0;

		result = New(type_id, sizeof(Header) + roc*(coc?coc:1)*16*sizeof(float));
		if (result != -1) {
			if (char * mhepo = reinterpret_cast<char *>(Acquire())) {
				refhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)));
				if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

				Init(mhepo, refhdr->roCount, repl);
						
				for (unsigned int i(0);i<repl;i++) FloatCopy4(mhepo+sizeof(Header)+i*refhdr->divR*4*sizeof(float), refelements, refhdr->divR);
	//			for (unsigned int i(repl);i<(coc<<2);i++) FloatClear4(mhepo+sizeof(Header)+i*refhdr->divR*4*sizeof(float), refhdr->divR, 0.0f);

				Release();
			}
		}
	
		if (refhdr) const_cast<VectorF &>(ref).Release();
	}
	
	return result;
}

UI_64 Calc::MatrixF::Create(Object * container, UI_64 offset, unsigned int rowC, unsigned int colC) {
	UI_64 result(-1);
	
	result = New(type_id, sizeof(Header));
	if (result != -1) {
		if (void * mhepo = Acquire()) {
			Init(mhepo, rowC, colC);
			
			if (container) Set(container, offset);

			Release();
		} else {
			result = -1;
		}
	}

	return result;
}
// =========
void Calc::MatrixF::SetColumnCount(unsigned int colC) {	
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->coCount = colC;
		hdr->divC = (colC+3)>>2;
		
		Release();
	}
}


unsigned int Calc::MatrixF::GetColumnCount() const {
	unsigned int result(0);
	if (const Header * hdr = reinterpret_cast<const Header *>(const_cast<MatrixF *>(this)->Acquire())) {
		result = hdr->coCount;
		const_cast<MatrixF *>(this)->Release();
	}

	return result;
}

unsigned int Calc::MatrixF::GetRowCount() const {
	unsigned int result(0);
	if (const Header * hdr = reinterpret_cast<const Header *>(const_cast<MatrixF *>(this)->Acquire())) {
		result = hdr->roCount;
		const_cast<MatrixF *>(this)->Release();
	}

	return result;
}

unsigned int Calc::MatrixF::GetRank() const {
	unsigned int result(0);
	if (const Header * hdr = reinterpret_cast<const Header *>(const_cast<MatrixF *>(this)->Acquire())) {
		result = hdr->rank;
		const_cast<MatrixF *>(this)->Release();
	}

	return result;
}

float * const Calc::MatrixF::GetElements() const {
	float * result(0);
	if (Header * hdr = reinterpret_cast<Header *>(const_cast<MatrixF *>(this)->Acquire(reinterpret_cast<void **>(&result)))) {
		if (!result) result = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
	}

	return result;
}

//////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator=(const MatrixF & ref) {	
	float * _elements(0), * refelements(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (Header * refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)))) {
			
			if (!_elements) // not external
			if ((hdr->divC != refhdr->divC) || (hdr->divR != refhdr->divR)) {
				UI_64 roc = refhdr->divR, coc = refhdr->divC;

				const_cast<MatrixF &>(ref).Release(refelements);
				Release(_elements);

				hdr = refhdr = 0;
				
				if (Resize(sizeof(Header) + roc*(coc?coc:1)*16*sizeof(float)) != -1) {
					hdr = reinterpret_cast<Header *>(Acquire());
					_elements = 0;
					
					refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)));					
				}
			}
				
			if (hdr) {
				if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char*>(hdr)+sizeof(Header));
				if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

				System::MemoryCopy(hdr, refhdr, sizeof(Header));

				if (hdr->divC) FloatCopy16(_elements, refelements, hdr->divC*hdr->divR);
				else FloatCopy4(_elements, refelements, hdr->divR);

			}

			if (refhdr) const_cast<MatrixF &>(ref).Release(refelements);
		}						

		if (hdr) Release(_elements);
	}

	return *this;
}



Calc::VectorF Calc::MatrixF::operator[](unsigned int cos) {
	VectorF result;
	void * vald(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(&vald))) {
		UI_64 offset(cos);

		if (cos>=hdr->coCount) offset = hdr->coCount-1;

		offset *= hdr->divR*sizeof(float)*4;		
		cos = hdr->roCount;
		if (!vald) offset += sizeof(Header);

		Release(vald);

		result.Create(this, offset, cos, -1);
	}

	return result; 
}

Calc::MatrixF & Calc::MatrixF::Zero(float zval) {
	float * _els(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_els)))) {
		if (!_els) _els = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		if (hdr->divC) FloatClear16(_els, hdr->divR*hdr->divC, zval);
		else FloatClear4(_els, hdr->divR, zval);

		Release(_els);
	}

	return *this;
}

Calc::MatrixF & Calc::MatrixF::Identity(float diel) {	
	float * _els(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_els)))) {
		unsigned int lal(hdr->coCount);
		if (lal<hdr->roCount) lal = hdr->roCount;
		
		if (!_els) _els = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		if (hdr->divC) FloatClear16(_els, hdr->divR*hdr->divC, 0.0f);
		else FloatClear4(_els, hdr->divR, 0.0f);

		for (unsigned int i(0);i<lal;i++) _els[i*((hdr->divR<<2)+1)] = diel;

		Release(_els);
	}
	
	return *this;
}

UI_64 Calc::MatrixF::IsIdentity() {
	UI_64 result(0);
	float * _els(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_els)))) {
		if (hdr->coCount == hdr->roCount) {
			if (!_els) _els = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
			result = MF_IdentityCheck(_els, hdr->divC);

		}
		Release(_els);
	}
	return result;
}

Calc::MatrixF & Calc::MatrixF::Reflect(const VectorF & planenrm) {	
	Identity() -= planenrm%planenrm*(2.0f/(planenrm^planenrm));

	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::Rotate(float cwangle, const VectorF & roaxis) {
	float * _elements(0), * roaxs(0);
	unsigned int roc(0), coc(0);
	MatrixF tmmat;

	Identity();

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		roc = hdr->roCount;
		coc = hdr->coCount;

		Release();
				
		if (tmmat.Create(roc, coc) != -1) {

			hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)));
			if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

			_elements[(hdr->divR<<2)+1] = _elements[(hdr->divR<<3)+2] = FloatCos(cwangle);
	
			_elements[(hdr->divR<<3)+1] = FloatSin(cwangle);
			_elements[(hdr->divR<<2)+2] = -_elements[(hdr->divR<<3)+1];

			if (Header * vhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(roaxis).Acquire(reinterpret_cast<void **>(&roaxs)))) {
				if (!roaxs) roaxs = reinterpret_cast<float *>(reinterpret_cast<char *>(vhdr) + sizeof(Header));
					
				float ilength2(0.0f), ilength(0.0f), olength(0.0f), val0(0.0f), val1(0.0f), * tels(0);
				unsigned int cooff(0);

				tmmat.Identity();

				ilength2 = roaxs[0]*roaxs[0];
				olength = FloatAbs(roaxs[0]);

				if (Header * tmhdr = reinterpret_cast<Header *>(tmmat.Acquire(reinterpret_cast<void **>(&tels)))) {
					if (!tels) tels = reinterpret_cast<float *>(reinterpret_cast<char *>(tmhdr) + sizeof(Header));
					for (unsigned int i(1);i<vhdr->roCount;i++) {
						ilength2 += roaxs[i]*roaxs[i];
						ilength = FloatSqrt(ilength2);

						if (ilength != olength) {
							val0 = olength/ilength;
							val1 = roaxs[i]/ilength;

							cooff = 0;

							for (unsigned int j(0);j<i;j++) {
								tels[cooff+i] = -val1*tels[cooff];
								tels[cooff] *= val0;				

								cooff += (hdr->divR<<2);
							}

							tels[cooff+i] = val0;
							tels[cooff] = val1;
			
							olength = ilength;
						}

					}

					tmmat.Release(tels);
				}

				tmmat(*this ^= tmmat);

				const_cast<VectorF &>(roaxis).Release(roaxs);
			}

			tmmat.Destroy();
		}
		Release(_elements);
	}

	return *this;
}

Calc::MatrixF & Calc::MatrixF::Translate(const VectorF & tvec) {
	float * _elements(0), * _components(0);
	
	Identity();

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		if (Header * vhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(tvec).Acquire(reinterpret_cast<void **>(&_components)))) {
			if (!_components) _components = reinterpret_cast<float *>(reinterpret_cast<char *>(vhdr)+sizeof(Header));
			unsigned int copco(hdr->roCount-1), coff((hdr->coCount-1)*(hdr->divR<<2));
						
			if (vhdr->roCount<copco) copco = vhdr->roCount;
			
			if (!_components) _components = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
			for (unsigned int i(0);i<copco;i++) _elements[coff+i] = _components[i];

			const_cast<VectorF &>(tvec).Release(_components);
		}
		Release(_elements);
	}
	return *this;
}

Calc::MatrixF & Calc::MatrixF::operator /=(const VectorF & ref) {
	float * _elements(0), * _components(0);
	
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
	
		if (Header * vhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&_components)))) {
			if (!_components) _components = reinterpret_cast<float *>(reinterpret_cast<char *>(vhdr) + sizeof(Header));

			MF_DiagonalMultiply(hdr, _elements, vhdr, _components);

			const_cast<VectorF &>(ref).Release(_components);
		}

		Release(_elements);
	}

	return *this;
}

Calc::MatrixF Calc::MatrixF::operator /(const VectorF & ref) const {
	MatrixF result;
	if (result.Create(*this) != -1) result /= ref;
	
	return result;
}


Calc::MatrixF & Calc::MatrixF::operator +=(const MatrixF & src) {
	float * alem(0), * blem(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&alem)))) {
		if (!alem) alem = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));
		if (Header * srchdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(src).Acquire(reinterpret_cast<void **>(&blem)))) {
			if (!blem) blem = reinterpret_cast<float *>(reinterpret_cast<char *>(srchdr)+sizeof(Header));
			
			if ((hdr->roCount >= srchdr->roCount) && (hdr->coCount >= srchdr->coCount)) MF_AddMatrix(hdr, alem, srchdr, blem);

			const_cast<MatrixF &>(src).Release(blem);
		}

		Release(alem);
	}

	return *this;
}

Calc::MatrixF & Calc::MatrixF::operator -=(const MatrixF & src) {
	float * alem(0), * blem(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&alem)))) {
		if (!alem) alem = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));
		if (Header * srchdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(src).Acquire(reinterpret_cast<void **>(&blem)))) {
			if (!blem) blem = reinterpret_cast<float *>(reinterpret_cast<char *>(srchdr)+sizeof(Header));
			
			if ((hdr->roCount >= srchdr->roCount) && (hdr->coCount >= srchdr->coCount)) MF_SubMatrix(hdr, alem, srchdr, blem);

			const_cast<MatrixF &>(src).Release(blem);
		}

		Release(alem);
	}

	return *this;
}

Calc::MatrixF Calc::MatrixF::operator+(const MatrixF & src) const {
	Calc::MatrixF result;
	if (result.Create(*this) != -1) result += src;

	return result;
}

Calc::MatrixF Calc::MatrixF::operator-(const MatrixF & src) const {
	Calc::MatrixF result;
	if (result.Create(*this) != -1) result -= src;

	return result;
}

Calc::MatrixF & Calc::MatrixF::operator*=(const float & scale) {
	float * _elements(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		MF_Scale(hdr, _elements, scale);

		Release(_elements);
	}
	
	return *this;
}

Calc::MatrixF Calc::MatrixF::operator*(const float & scale) const {
	MatrixF result;
	if (result.Create(*this) != -1) result *= scale;

	return result;
}


Calc::MatrixF & Calc::MatrixF::operator-() {
	float * _elements(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		MF_Negate(hdr, _elements);

		Release(_elements);
	}

	return *this;
}

Calc::MatrixF Calc::MatrixF::operator*(const MatrixF & ref) const {
	MatrixF result;
	if (result.Create(*this) != -1) result *= ref;
	
	return result;

}

Calc::MatrixF Calc::MatrixF::operator^(const MatrixF & ref) const {
	MatrixF result;

	if (result.Create(*this) != -1) result ^= ref;

	return result;
}


Calc::MatrixF Calc::MatrixF::operator%(const MatrixF & ref) {
	MatrixF result;

	if (result.Create(*this) != -1) result %= ref;

/*
	det = result.det;
	rank = result.rank;
	
	if (result.det != 0.0f) result.det = ref.det/result.det;
	if (ref.rank < result.rank) result.rank = ref.rank;
*/
	return result;
}



//////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator*=(const MatrixF & ref) {
	float * _elements(0), * refelements(0);	
	MatrixF tmatr; // roc, coc, 0, alla);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		if (Header * refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)))) {
			if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

			if (hdr->coCount == refhdr->roCount) {
				UI_64  roc = hdr->divR, coc = refhdr->divC, roc1 = refhdr->roCount, coc1 = refhdr->coCount;

				const_cast<MatrixF &>(ref).Release(refelements);
				Release(_elements);
				hdr = refhdr = 0;
								
				if (tmatr.Create(*this) != -1) {
					if (coc1 != roc1) coc1 = Resize(sizeof(Header) + roc*(coc?coc:1)*16*sizeof(float));
				
					if (coc1 != -1) {
						if (Header * thdr = reinterpret_cast<Header *>(tmatr.Acquire())) {
							float * newelements = reinterpret_cast<float *>(reinterpret_cast<char *>(thdr) + sizeof(Header));

							hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)));
							if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

							refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)));
							if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));


							hdr->coCount = refhdr->coCount;
							hdr->divC = refhdr->divC;

							hdr->rank = 0;
							hdr->det = 0.0f;

							if ((thdr->coCount == thdr->roCount) && (thdr->roCount<=4)) {
								if (thdr->roCount == 2) {
									MF_Multiply22(newelements, refhdr->coCount, refelements, _elements);
								} else {
									MF_Multiply44(newelements, refhdr->coCount, refelements, _elements);
								}

							} else {
								if (hdr->divC) FloatClear16(_elements, hdr->divR*hdr->divC, 0.0f);
								else FloatClear4(_elements, hdr->divR, 0.0f);

								MF_Multiply(thdr, newelements, refhdr, refelements, _elements);
							}


							tmatr.Release();

						}

					}
					tmatr.Destroy();
				}
			}

			if (refhdr) const_cast<MatrixF &>(ref).Release(refelements);
		}

		if (hdr) Release(_elements);
	}

	return *this;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator>>(MatrixF & ref) {
	float * _elements(0), * refelements(0);		
	MatrixF tmatr; // roc, coc, 0, alla);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		if (Header * refhdr = reinterpret_cast<Header *>(ref.Acquire(reinterpret_cast<void **>(&refelements)))) {
			if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

			if (hdr->coCount == refhdr->roCount) {
				UI_64  roc = hdr->divR, coc = refhdr->divC, roc1 = hdr->roCount, coc1 = hdr->coCount;

				ref.Release(refelements);
				Release(_elements);
				hdr = refhdr = 0;
				

				if (coc1 != roc1) coc1 = ref.Resize(sizeof(Header) + roc*(coc?coc:1)*16*sizeof(float));
				
				if (coc1 != -1) {
					hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)));
					if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

					refhdr = reinterpret_cast<Header *>(ref.Acquire(reinterpret_cast<void **>(&refelements)));
					if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));
				
					refhdr->roCount = hdr->roCount;
					refhdr->divR = hdr->divR;

					refhdr->rank = 0;
					refhdr->det = 0.0f;


					if ((hdr->coCount == hdr->roCount) && (hdr->roCount<=4)) {
						if (hdr->roCount == 2) {
							MF_Multiply22(_elements, refhdr->coCount, refelements, refelements);
						} else {
							MF_Multiply44(_elements, refhdr->coCount, refelements, refelements);
						}

					} else {
						if (tmatr.Create(ref) != -1) {
							if (Header * thdr = reinterpret_cast<Header *>(tmatr.Acquire())) {
								float * newelements = reinterpret_cast<float *>(reinterpret_cast<char *>(thdr) + sizeof(Header));
								
								if (refhdr->divC) FloatClear16(refelements, refhdr->divR*refhdr->divC, 0.0f);
								else FloatClear4(refelements, refhdr->divR, 0.0f);

								MF_Multiply(hdr, _elements, thdr, newelements, refelements);
							
								tmatr.Release();
							}
							tmatr.Destroy();
						}
					}
				}
			}
			if (refhdr) ref.Release(refelements);
		}

		if (hdr) Release(_elements);	
	}
	
	return ref;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator^=(const MatrixF & ref) {
	float * _elements(0), * refelements(0);	
	MatrixF tmatr; // roc, coc, 0, alla);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		if (Header * refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)))) {
			if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

			if (hdr->roCount == refhdr->roCount) {
				UI_64  roc = hdr->divC, coc = refhdr->divC, roc1 = refhdr->roCount, coc1 = refhdr->coCount, coc2 = hdr->coCount;

				const_cast<MatrixF &>(ref).Release(refelements);
				Release(_elements);
				hdr = refhdr = 0;

				
				if (tmatr.Create(*this) != -1) {
					if ((coc1 != roc1) || (coc2 != coc1)) coc1 = Resize(sizeof(Header) + (roc?roc:1)*(coc?coc:1)*16*sizeof(float));
				
					if (coc1 != -1) {
						if (Header * thdr = reinterpret_cast<Header *>(tmatr.Acquire())) {
							float * newelements = reinterpret_cast<float *>(reinterpret_cast<char *>(thdr) + sizeof(Header));

							hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)));
							if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

							refhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire(reinterpret_cast<void **>(&refelements)));
							if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

							hdr->roCount = hdr->coCount;
							hdr->divR = hdr->divC;

							hdr->coCount = refhdr->coCount;
							hdr->divC = refhdr->divC;

							hdr->rank = 0;
							hdr->det = 0.0f;

							if ((thdr->coCount == thdr->roCount) && (thdr->roCount<=4)) {
								if (thdr->roCount == 2) {
									MF_TransposeMultiply22(newelements, refhdr->coCount, refelements, _elements);
								} else {
									MF_TransposeMultiply44(newelements, refhdr->coCount, refelements, _elements);
								}

							} else {
								if (hdr->divR && hdr->divC) FloatClear16(_elements, hdr->divR*hdr->divC, 0.0f);
								else FloatClear4(_elements, (hdr->divR?hdr->divR:1)*(hdr->divC?hdr->divC:1), 0.0f);

								MF_TransposeMultiply(thdr, newelements, refhdr, refelements, _elements);
							}
							
							tmatr.Release();

						}
					}
					tmatr.Destroy();
				}
			}

			if (refhdr) const_cast<MatrixF &>(ref).Release(refelements);
		}

		if (hdr) Release(_elements);
	}

	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator()(MatrixF & ref) {
	float * _elements(0), * refelements(0);	
	MatrixF tmatr; // roc, coc, 0, alla);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		if (Header * refhdr = reinterpret_cast<Header *>(ref.Acquire(reinterpret_cast<void **>(&refelements)))) {
			if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));

			if (hdr->roCount == refhdr->roCount) {
				UI_64  roc = hdr->divC, coc = refhdr->divC, roc1 = hdr->roCount, coc1 = hdr->coCount;

				ref.Release(refelements);
				Release(_elements);
				hdr = refhdr = 0;

				
				if (coc1 != roc1) coc1 = ref.Resize(sizeof(Header) + (roc?roc:1)*(coc?coc:1)*16*sizeof(float));
				
				if (coc1 != -1) {
					hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)));
					if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

					refhdr = reinterpret_cast<Header *>(ref.Acquire(reinterpret_cast<void **>(&refelements)));
					if (!refelements) refelements = reinterpret_cast<float *>(reinterpret_cast<char *>(refhdr)+sizeof(Header));
				
					refhdr->roCount = hdr->coCount;
					refhdr->divR = hdr->divC;

					refhdr->rank = 0;
					refhdr->det = 0.0f;

					if ((hdr->coCount == hdr->roCount) && (hdr->roCount<=4)) {
						if (hdr->roCount == 2) {
							MF_TransposeMultiply22(_elements, refhdr->coCount, refelements, refelements);
						} else {
							MF_TransposeMultiply44(_elements, refhdr->coCount, refelements, refelements);
						}

					} else {
						if (tmatr.Create(ref) != -1) {
							if (Header * thdr = reinterpret_cast<Header *>(tmatr.Acquire())) {
								float * newelements = reinterpret_cast<float *>(reinterpret_cast<char *>(thdr) + sizeof(Header));

								if (refhdr->divR && refhdr->divC) FloatClear16(refelements, refhdr->divR*refhdr->divC, 0.0f);
								else FloatClear4(refelements, (refhdr->divR?refhdr->divR:1)*(refhdr->divC?refhdr->divC:1), 0.0f);

								MF_TransposeMultiply(hdr, _elements, thdr, newelements, refelements);
								
								tmatr.Release();
							}							
							tmatr.Destroy();
						}
					}
					
				}
			}

			if (refhdr) ref.Release(refelements);
		}

		if (hdr) Release(_elements);
	}

	return ref;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator%=(const MatrixF & ref) {	
	float * _elements(0), * relements(0);
	MatrixF tempA;

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {		
		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire())) {
			
			if (hdr->roCount == hdr->coCount) { // square matrix
				if (hdr->coCount == rhdr->roCount) {
					UI_64 roc = rhdr->roCount, coc = rhdr->coCount;

					const_cast<MatrixF &>(ref).Release();
					Release();
					hdr = rhdr = 0;
										
					if (tempA.Create(*this) != -1) {
						*this = ref;
				
						if (hdr = reinterpret_cast<Header *>(tempA.Acquire())) {
							_elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
							if (rhdr = reinterpret_cast<Header *>(Acquire())) {
								relements = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));

								if ((rhdr->roCount == roc) && (rhdr->coCount == coc))
								switch (hdr->coCount) {
									case 4:
										if (MF_InverseMultiply44(hdr, _elements, rhdr->coCount, relements)) {

										}
									break;
									case 3:
										if (MF_InverseMultiply33(hdr, _elements, rhdr->coCount, relements)) {

										}
									break;
									case 2:
										if (MF_InverseMultiply22(hdr, _elements, rhdr->coCount, relements)) {
												
										}

									break;
									default:
										coc = rhdr->divC;
										roc = rhdr->divR;
									
										tempA.Release();
										Release();
											
										hdr = rhdr = 0;
										SFSco::Object emem;
										if (emem.New(0, roc*(coc?coc:1)*16*sizeof(float)) != -1) { // 104059
										
											if (void * emempo = emem.Acquire()) {
								//				FloatClear16(emempo, roc*coc, 0.0f);

												hdr = reinterpret_cast<Header *>(tempA.Acquire());
												_elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
												rhdr = reinterpret_cast<Header *>(Acquire());
												relements = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));
										
												if (MF_InverseMultiply(hdr, _elements, rhdr, relements, emempo)) { // tempM._elements, emem, tempM.coCount, tempM.divR
													FloatPermute(relements, _elements);

						
												} else {
												//singular case

												}

												emem.Release();
											}

											emem.Destroy();
										}
								
								}
								if (rhdr) Release();
								rhdr = 0;
							}
							if (hdr) tempA.Release();
							hdr = 0;
						}
						tempA.Destroy();
					}
				}

			} else { // non-square case

			}
			if (rhdr) const_cast<MatrixF &>(ref).Release();
		}
		if (hdr) Release();
	}
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Calc::MatrixF & Calc::MatrixF::operator[](MatrixF & ref) {	
	float * _elements(0), * relements(0);
	MatrixF tempA;

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {		
		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<MatrixF &>(ref).Acquire())) {
			
			if (hdr->roCount == hdr->coCount) { // square matrix
				if (hdr->coCount == rhdr->roCount) {
					UI_64 roc(0), coc(0);

					ref.Release();
					Release();
					hdr = rhdr = 0;

					if (tempA.Create(*this) != -1) {
				
						if (hdr = reinterpret_cast<Header *>(tempA.Acquire())) {
							_elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
							if (rhdr = reinterpret_cast<Header *>(ref.Acquire(reinterpret_cast<void **>(&relements)))) {
								relements = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));
														
								switch (hdr->coCount) {
									case 4:
										if (MF_InverseMultiply44(hdr, _elements, rhdr->coCount, relements)) {

										}
									break;
									case 3:
										if (MF_InverseMultiply33(hdr, _elements, rhdr->coCount, relements)) {

										}
									break;
									case 2:
										if (MF_InverseMultiply22(hdr, _elements, rhdr->coCount, relements)) {
												
										}

									break;
									default:
										coc = rhdr->divC;
										roc = rhdr->divR;
									
										ref.Release();
										tempA.Release();
											
										hdr = rhdr = 0;
										SFSco::Object emem;
										if (emem.New(0, roc*(coc?coc:1)*16*sizeof(float)) != -1) {
																				
											if (void * emempo = emem.Acquire()) {
//												FloatClear16(emempo, roc*coc, 0.0f);

												hdr = reinterpret_cast<Header *>(tempA.Acquire());
												_elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
												rhdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&relements)));
												relements = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));
												
												if (MF_InverseMultiply(hdr, _elements, rhdr, relements, emempo)) { // tempM._elements, emem, tempM.coCount, tempM.divR
													FloatPermute(relements, _elements);

						
												} else {
													//singular case

												}

												emem.Release();
											}

											emem.Destroy();
										}
								}
								
							}
							if (rhdr) ref.Release();
							rhdr = 0;
						}
						if (hdr) tempA.Release();
						hdr = 0;

						tempA.Destroy();
					}					
				}

			} else { // non-square case

			}
			if (rhdr) const_cast<MatrixF &>(ref).Release();
		}
		if (hdr) Release();
	}

	return ref;
}



Calc::MatrixF & Calc::MatrixF::operator!() {	
	float * _elements(0), * relements(0);
	
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		if (hdr->roCount == hdr->coCount) { // square matrix
			switch (hdr->coCount) {
				case 4:
					if (MF_InverseMultiply44(hdr, _elements, 0, 0)) {

					}
				break;
				case 3:
					if (MF_InverseMultiply33(hdr, _elements, 0, 0)) {

					}
				break;
				case 2:
					if (MF_InverseMultiply22(hdr, _elements, 0, 0)) {
												
					}

				break;
				default:
				{
					UI_64 roc = hdr->divR, coc = hdr->divC;					
					
					Release(_elements);
					hdr = 0;

					MatrixF tempA;
					if (tempA.Create(*this) != -1) {
						Identity();

						SFSco::Object emem;
						if (emem.New(0, roc*coc*16*sizeof(float)) != -1) {
							if (void * emempo = emem.Acquire()) {
//								FloatClear16(emempo, roc*coc, 0.0f);

								hdr = reinterpret_cast<Header *>(tempA.Acquire());
								_elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));
							
								Header * rhdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&relements)));
								relements = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));
										
								if (MF_InverseMultiply(hdr, _elements, rhdr, relements, emempo)) { // tempM._elements, emem, tempM.coCount, tempM.divR
									FloatPermute(relements, _elements);

						
								} else {
												//singular case

								}

								Release(relements);

								emem.Release();
							}

							emem.Destroy();
						}
				
						if (hdr) tempA.Release();
						hdr = 0;

						tempA.Destroy();
					}
				}

			}
		} else { // non-square case

		}

		if (hdr) Release(_elements);

	}

	return *this;
}

Calc::VectorF Calc::MatrixF::operator*(const VectorF & ref) const {	
	return ((*this) * reinterpret_cast<const MatrixF &>(ref))[0];
}

Calc::VectorF Calc::MatrixF::operator^(const VectorF & ref) const {
	return ((*this) ^ reinterpret_cast<const MatrixF &>(ref))[0];
}

Calc::VectorF Calc::MatrixF::operator%(const VectorF & ref) {
	return ((*this) % reinterpret_cast<const MatrixF &>(ref))[0];
}

Calc::VectorF & Calc::MatrixF::operator>>(VectorF & ref) {
	(*this) >> reinterpret_cast<MatrixF &>(ref);
	return ref;
}

Calc::VectorF & Calc::MatrixF::operator()(VectorF & ref) {
	(*this)(reinterpret_cast<MatrixF &>(ref));
	return ref;
}

Calc::VectorF & Calc::MatrixF::operator[](VectorF & ref) {
	(*this)[reinterpret_cast<MatrixF &>(ref)];
	return ref;
}
