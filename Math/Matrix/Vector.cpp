/*
** safe-fail math
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Math\Matrix.h"
#include "Math\Calc.h"

#include "Math\CalcSpecial.h"

Calc::MatrixF::Header * Calc::VectorF::Init(char * dst, unsigned int rowc, unsigned int) {
	Header * result = reinterpret_cast<Header *>(dst);

	result->roCount = rowc;
	result->divR = (rowc+3)>>2;

	result->coCount = 1;
	result->divC = 0;

	result->rank = 1;
	result->det = 0.0f;

	return result;

}


void Calc::VectorF::Init(Header * hdr, const float * src) {
	float * dst = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

	FloatCopy4(dst, src, hdr->roCount/4);
	for (unsigned int i(hdr->roCount & 0xFFFFFFFC);i<hdr->roCount;i++) dst[i] = src[i];
	for (unsigned int i(hdr->roCount);i<4*hdr->divR;i++) dst[i] = 0.0f;

}

Calc::VectorF::~VectorF() {
	
}

void Calc::VectorF::Destroy() {
	Object::Destroy();
}

Calc::VectorF::VectorF() {
	SetTID(type_id);
}

Calc::VectorF::VectorF(const VectorF & ref) : MatrixF(ref) {

}

// intializers =====================================

UI_64 Calc::VectorF::Create(unsigned int cmp, float * src, SFSco::IAllocator * alla) {
	return MatrixF::Create(cmp, 0, src, alla);
}


UI_64 Calc::VectorF::Create(Object * container, UI_64 offset, unsigned int cmp, unsigned int t_flag) {
	return MatrixF::Create(container, offset, cmp, t_flag);
}


UI_64 Calc::VectorF::Create(unsigned int cmp, float scale) {
	UI_64 result = MatrixF::Create(cmp, 0);
	Zero(scale);

	return result;
}

UI_64 Calc::VectorF::Create(float x, float y, float z, float t) {
	float * _cmps(0);
	UI_64 result = MatrixF::Create(4,0);
	if (result != -1) {
		if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
			_cmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

			_cmps[0] = x;
			_cmps[1] = y;
			_cmps[2] = z;
			_cmps[3] = t;

			Release();
		}
	}

	return result;
}


void * Calc::VectorF::Acquire(void ** spo) {
	return Object::Acquire(spo);
}
		
void Calc::VectorF::Release(void * spo) {
	Object::Release(spo);
}


unsigned int Calc::VectorF::GetComponentCount() const {
	return GetRowCount();
}

float * const Calc::VectorF::GetComponents() const {
	return GetElements();
}

void Calc::VectorF::Normalize() {
	*this *= (1.0f/E2Norm());
}


Calc::VectorF & Calc::VectorF::operator=(const VectorF & ref) {
	MatrixF::operator=(ref);
	return *this;
}

float Calc::VectorF::operator^(const VectorF & ref) const {	
	float * acmps(0), * bcmps(0), result(0.0f);

	if (Header * hdr = reinterpret_cast<Header *>(const_cast<VectorF *>(this)->Acquire(reinterpret_cast<void **>(&acmps)))) {
		if (!acmps) acmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr+sizeof(Header)));
		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&bcmps)))) {
			if (!bcmps) bcmps = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));

			if (hdr->roCount == rhdr->roCount) result = VF_DotProduct(acmps, bcmps, rhdr->divR);

			const_cast<VectorF &>(ref).Release(bcmps);
		}
		const_cast<VectorF *>(this)->Release(acmps);
	}

	return result;
}


Calc::VectorF & Calc::VectorF::operator+=(const VectorF & ref) {
	float * acmps(0), * bcmps(0);

	if (Header * hdr = reinterpret_cast<Header *>(const_cast<VectorF *>(this)->Acquire(reinterpret_cast<void **>(&acmps)))) {
		if (!acmps) acmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr+sizeof(Header)));
		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&bcmps)))) {
			if (!bcmps) bcmps = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));

			if (hdr->roCount >= rhdr->roCount) VF_AddVector(acmps, bcmps, rhdr->divR);
			
			const_cast<VectorF &>(ref).Release(bcmps);
		}
		const_cast<VectorF *>(this)->Release(acmps);
	}

	return *this;
}

Calc::VectorF & Calc::VectorF::operator-=(const VectorF & ref) {
	float * acmps(0), * bcmps(0);

	if (Header * hdr = reinterpret_cast<Header *>(const_cast<VectorF *>(this)->Acquire(reinterpret_cast<void **>(&acmps)))) {
		if (!acmps) acmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr+sizeof(Header)));
		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&bcmps)))) {
			if (!bcmps) bcmps = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));

			if (hdr->roCount >= rhdr->roCount) VF_SubVector(acmps, bcmps, rhdr->divR);
			
			const_cast<VectorF &>(ref).Release(bcmps);
		}
		const_cast<VectorF *>(this)->Release(acmps);
	}

	return *this;
}

Calc::VectorF & Calc::VectorF::operator*=(const float & scale) {
	float * _elements(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

		VF_Scale(_elements, hdr->divR, scale);

		Release(_elements);
	}
	
	return *this;
}

Calc::VectorF Calc::VectorF::operator*(const float & scale) const {
	VectorF result;
	if (result.MatrixF::Create(*this) != -1) result *= scale;

	return result;
}


Calc::VectorF Calc::VectorF::operator-() const {
	VectorF result;
	float * _elements(0);

	if (result.MatrixF::Create(*this) != -1) {
		if (Header * hdr = reinterpret_cast<Header *>(result.Acquire(reinterpret_cast<void **>(&_elements)))) {
			if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr)+sizeof(Header));

			VF_Negate(_elements, hdr->divR);

			result.Release(_elements);
		}
	}
	
	return result;
}


Calc::VectorF Calc::VectorF::operator+(const VectorF & ref) const {
	VectorF result;
	if (result.MatrixF::Create(*this) != -1) result += ref;

	return result;
}

Calc::VectorF Calc::VectorF::operator-(const VectorF & ref) const {
	VectorF result;
	if (result.MatrixF::Create(*this) != -1) result -= ref;

	return result;
}

Calc::MatrixF Calc::VectorF::operator%(const VectorF & ref) const {
	unsigned int replicator(1);
	MatrixF result;

	if (Header * hdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire())) {
		replicator = hdr->roCount;

		const_cast<VectorF &>(ref).Release();
	}
		
	if (result.Create(*this, replicator) != -1) result ^= ref;
	
	return result;
}

Calc::Single Calc::VectorF::operator[](unsigned int indi) {
	Single result;
	void * vald(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(&vald))) {
		UI_64 offset(indi);

		if (indi>=hdr->roCount) offset = hdr->roCount-1;

		offset *= sizeof(float);
		if (!vald) offset += sizeof(Header);
		
		result.Create(this, offset);


		Release(vald);
		if (hdr->t_flag) Destroy();
				
	}

	return result;
}


float Calc::VectorF::E2Norm() {
	return FloatSqrt(*this ^ *this);
}

float Calc::VectorF::E1Norm() {
	float result(0.0f), * _cmps(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_cmps)))) {
		if (!_cmps) _cmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		result = VF_E1Norm(_cmps, hdr->divR);

		Release(_cmps);
	}

	return result;
}

float Calc::VectorF::SupNorm() {
	float result(0.0f), * _cmps(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_cmps)))) {
		if (!_cmps) _cmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		result = VF_SupNorm(_cmps, hdr->divR);

		Release(_cmps);
	}

	return result;
}


Calc::VectorF & Calc::VectorF::Sqrt() {
	float * _elements(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&_elements)))) {
		if (!_elements) _elements = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		VF_Sqrt(_elements, hdr->divR);

		Release(_elements);
	}
	

	return *this;
}

Calc::MatrixF Calc::VectorF::operator /(const MatrixF & ref) const {
	Calc::MatrixF result;
	if (result.Create(ref) != -1) *this /= result;

	return result;
}

Calc::MatrixF & Calc::VectorF::operator /=(MatrixF & ref) const {
	for (unsigned int i(0);i<ref.GetColumnCount();i++) (ref[i] *= *this).Destroy();

	return ref;
}


Calc::VectorF & Calc::VectorF::operator *=(const VectorF & ref) {
	float * acmps(0), * bcmps(0);
	if (Header * hdr = reinterpret_cast<Header *>(Acquire(reinterpret_cast<void **>(&acmps)))) {
		if (!acmps) acmps = reinterpret_cast<float *>(reinterpret_cast<char *>(hdr) + sizeof(Header));

		if (Header * rhdr = reinterpret_cast<Header *>(const_cast<VectorF &>(ref).Acquire(reinterpret_cast<void **>(&bcmps)))) {
			if (!bcmps) bcmps = reinterpret_cast<float *>(reinterpret_cast<char *>(rhdr)+sizeof(Header));

			if (hdr->roCount == rhdr->roCount) VF_ComponentMultiply(acmps, bcmps, hdr->divR);

			const_cast<VectorF &>(ref).Release(bcmps);
		}
		Release(acmps);
	}	

	return *this;

}

Calc::VectorF Calc::VectorF::operator *(const VectorF & ref) const {
	VectorF result;
	if (result.MatrixF::Create(*this) != -1) result *= ref;
	
	return result;
}



