/*
** safe-fail core library
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

namespace SFSco {

// Allocator ==================================================================================================================================
#define allocator_IFACE_ID			0x10001010
#define lallocator_IFACE_ID			0x10001020
#define xlallocator_IFACE_ID		0x10001030
#define fallocator_IFACE_ID			0x10001040

class IAllocator  {
	friend class Object;
	protected:
		virtual int IncRef(UI_64 id, void *&) = 0;
		virtual int DecRef(UI_64 id) = 0;
		
		virtual void ShiftRight(UI_64, UI_64) = 0;
		virtual void ShiftLeft(UI_64, UI_64) = 0;

		virtual UI_64 Next(UI_64) = 0;
		virtual UI_64 Back(UI_64) = 0;
		virtual UI_64 Left(UI_64) = 0;
		virtual UI_64 Right(UI_64) = 0;
		virtual UI_64 Down(UI_64) = 0;
		virtual UI_64 Up(UI_64) = 0;

		virtual UI_64 Alloc(UI_64 newsize, UI_64 type_id, UI_64 parent_id = -1) = 0;
		virtual UI_64 Realloc(UI_64 id, UI_64 newsize) = 0;
		virtual UI_64 Expand(UI_64 id, UI_64 esize) = 0;
		
		virtual int Free(UI_64 id) = 0;
				
	public:
		virtual int GetRefCount(UI_64 id) = 0;
		virtual void SpinWaitRC(UI_64 id, int) = 0;
		virtual UI_64 GetSize(UI_64 id) = 0;
		virtual UI_64 GetTypeID(UI_64 id) = 0;
		virtual int SetFlags(UI_64 id, int) = 0;
		virtual int GetFlags(UI_64) = 0;
		virtual bool IsValid(UI_64 id) = 0;	

};

extern IAllocator * mem_mgr, * large_mem_mgr, * xl_mem_mgr, * file_cache;

// OpenGL =====================================================================================================================================
#define opengl_core_IFACE_ID			0x10002010


	class IOpenGL {
	public:
		struct ScreenSet {
			enum ScreenFlags : unsigned int {FlagDataDestroy = 1, FlagDstSpecial = 2, FlagDataCT = 4};
						
			unsigned int full_width, full_height;
			unsigned int offset_x, offset_y;
			unsigned int screen_width, screen_height;
			unsigned int i_type, cmp_count, q_factor;
			unsigned int i_flags;

			UI_64 i_id, qd_factor;

			ScreenSet() {
				Blank();
			};

			ScreenSet(const ScreenSet & ref_s) {
				*this = ref_s;
			};

			void Blank() {
				full_width = full_height = 0;
				offset_x = offset_y = 0;
				screen_width = screen_height = 0;

				i_type = 0;
				cmp_count = 4;
				
				q_factor = 0;
				i_flags = 0;

				i_id = -1;
				qd_factor = 0;
			};

			ScreenSet & operator=(const ScreenSet & ref_s) {
				full_width = ref_s.full_width;
				full_height = ref_s.full_height;
				
				offset_x = ref_s.offset_x;
				offset_y = ref_s.offset_y;

				screen_width = ref_s.screen_width;
				screen_height = ref_s.screen_height;
				
				i_type = ref_s.i_type;
				cmp_count = ref_s.cmp_count;
				q_factor = ref_s.q_factor;

				i_flags = ref_s.i_flags;

				i_id = ref_s.i_id;

				qd_factor = ref_s.qd_factor;

				return *this;
			};


		};

		enum MenuCorner: unsigned int {LeftTop = 3, RightTop = 1, RightBottom = 0, LeftBottom = 2};
		enum InputKeys: unsigned int {LeftButton = 1, RightButton = 2, LRButtons = 3, NoButton = 4};

		virtual UI_64 Activate(void * window_handle, MenuCorner) = 0;
		virtual void Destroy() = 0;
		virtual unsigned int ShareRC(HGLRC scntx) = 0;

		virtual UI_64 PoMove(int, int) = 0;
		virtual UI_64 PoLock(InputKeys) = 0;
		virtual UI_64 PoUnLock(InputKeys) = 0;
		virtual UI_64 MoCursor(unsigned int) = 0;
		virtual UI_64 Scroll(int) = 0;

		virtual UI_64 Render() = 0;		
		virtual void Resize() = 0; // state modifier
		
		virtual void SetInfoPic(SFSco::Object *) = 0;

		virtual void ScreenShot(const ScreenSet &) = 0;
		virtual void Update() = 0;
		virtual void Refresh() = 0;

		virtual MenuCorner GetMenuCorner() = 0;
		virtual unsigned int IsRendered() = 0;

	};


// App =============================================================================================================================================================================================
#define main_menu_IFACE_ID			0x10003010
#define x_files_IFACE_ID			0x10003020
#define x_pic_IFACE_ID				0x10003030
#define x_video_IFACE_ID			0x10003040
#define x_audio_IFACE_ID			0x10003050

	class IGLApp {
		public:
			virtual UI_64 Initialize(IOpenGL **, void *, void *) = 0;
			virtual UI_64 Finalize() = 0;
					
			virtual void Change() = 0;
			virtual UI_64 AddFilter(IGLApp *) = 0;
			virtual const wchar_t * GetSupportedList() = 0;
			virtual unsigned int GetSupportedCount() = 0;
			virtual UI_64 Test(const wchar_t *) = 0;

			virtual UI_64 ConfigurePreview(SFSco::Object &) = 0;
			virtual UI_64 CreatePreview(UI_64 path_id, Object & button_obj, UI_64 icon_id, UI_64 text_id, UI_64 name_id, unsigned int) = 0;
			
			virtual void * GetSelectorProc() = 0;

	};


// Object ===========================================================================================================================================================
#define LINK_EXCLUSIVE		0x00000001

class Instance {
	protected:
		IAllocator * _allocator;
		UI_64 _al_id, _tid, _size;

		Instance() : _allocator(0), _al_id(-1), _tid(0), _size(0) {}
		
		Instance(const Instance & ref) : _allocator(ref._allocator), _al_id(ref._al_id), _tid(ref._tid), _size(ref._size) {}
		virtual ~Instance() {}
		
		void SetTID(UI_64 tid) { _tid = tid; }

	public:
		UI_64 GetID() const { return _al_id; }
		UI_64 GetTID() const { return _tid; }
		void Clear() { _allocator = 0; _al_id = -1; _size = 0; }

		bool operator ==(const Instance & ref) const {
			if (_al_id != -1) return ((_allocator == ref._allocator) && (_al_id == ref._al_id) && (ref._tid == _tid));
			else return false;
		}

		bool Is(UI_64 rtid) const {
			return ((_tid % rtid) == 0);
		}

		bool operator !=(const Instance & ref) const {
			return ((_allocator != ref._allocator) || (_al_id != ref._al_id) || (_tid != ref._tid));
			
		}

		operator bool() const {
			return ((_al_id != -1) && (_allocator != 0));
/*
			if ((_al_id != -1) && (_allocator != 0)) {
				return _allocator->IsValid(_al_id);
			} else return false;
*/
		}
};

class Object : public Instance {
	private:		
		Object * _external_node;				
		UI_64 _local_offset;
		
	public:
		UI_64 _reserved;

		static const UI_64 type_id = 4294967291;
		//very, very unsafe set; proper header must be allocated before this set
		void Set(Object * xnode, UI_64 xoffset) {			
			_external_node = 0;
			_local_offset = xoffset;
			
			if (xnode) {
				if (reinterpret_cast<UI_64>(xnode->_external_node) > 16) {
					_local_offset += xnode->_local_offset; // - xnode->_size;
					_external_node = xnode->_external_node;				
				} else {
					if (reinterpret_cast<UI_64>(xnode->_external_node) == 1) {
						_local_offset += xnode->_local_offset;
					}
				}
			}

			if (!_external_node) {
				if (xnode == this) _external_node = reinterpret_cast<Object *>(1);
				else _external_node = xnode;
			}
		}
		
		 // blank object
		Object() : Instance(), _external_node(0), _local_offset(0), _reserved(0) {}
		Object & Blank() { Clear(); _external_node = 0; _local_offset = 0; _reserved = 0; return *this; }
		// initializes instance with with rob
		Object(const Object & ref) {
			reinterpret_cast<UI_64 *>(this)[0] = reinterpret_cast<const UI_64 *>(&ref)[0];
			reinterpret_cast<UI_64 *>(this)[1] = reinterpret_cast<const UI_64 *>(&ref)[1];
			reinterpret_cast<UI_64 *>(this)[2] = reinterpret_cast<const UI_64 *>(&ref)[2];
			reinterpret_cast<UI_64 *>(this)[3] = reinterpret_cast<const UI_64 *>(&ref)[3];
			reinterpret_cast<UI_64 *>(this)[4] = reinterpret_cast<const UI_64 *>(&ref)[4];
			reinterpret_cast<UI_64 *>(this)[5] = reinterpret_cast<const UI_64 *>(&ref)[5];
			reinterpret_cast<UI_64 *>(this)[6] = reinterpret_cast<const UI_64 *>(&ref)[6];
			reinterpret_cast<UI_64 *>(this)[7] = reinterpret_cast<const UI_64 *>(&ref)[7];

		};
		
		Object & operator()(const Object & ref) {
			_allocator = ref._allocator;
			_al_id = ref._al_id;
			_tid = ref._tid;
			_size = ref._size;

			_external_node = ref._external_node;
			_local_offset = ref._local_offset;

			return *this;
		}
		
		Object & operator =(const Object & ref) {
			reinterpret_cast<UI_64 *>(this)[0] = reinterpret_cast<const UI_64 *>(&ref)[0];
			reinterpret_cast<UI_64 *>(this)[1] = reinterpret_cast<const UI_64 *>(&ref)[1];
			reinterpret_cast<UI_64 *>(this)[2] = reinterpret_cast<const UI_64 *>(&ref)[2];
			reinterpret_cast<UI_64 *>(this)[3] = reinterpret_cast<const UI_64 *>(&ref)[3];
			reinterpret_cast<UI_64 *>(this)[4] = reinterpret_cast<const UI_64 *>(&ref)[4];
			reinterpret_cast<UI_64 *>(this)[5] = reinterpret_cast<const UI_64 *>(&ref)[5];
			reinterpret_cast<UI_64 *>(this)[6] = reinterpret_cast<const UI_64 *>(&ref)[6];
			reinterpret_cast<UI_64 *>(this)[7] = reinterpret_cast<const UI_64 *>(&ref)[7];

			return *this;
		}

		virtual ~Object() {}
		
		virtual void Destroy() {
			if (_allocator) {
				if (_allocator->Free(_al_id) <= 0) Blank();
			}
		}

		int GetRefCount() {
			int result(0);

			if (_allocator) {
				result = _allocator->GetRefCount(_al_id);
			}

			return result;
		}

		void SpinWaitRC(int limit) {
			if (_allocator) {
				_allocator->SpinWaitRC(_al_id, limit);
			}
		}

		
		void Set(SFSco::IAllocator * alla, UI_64 id) {
			Blank();
		
			_al_id = id;
			if (_allocator = alla) {				
				_tid = _allocator->GetTypeID(id);
			}
		}
		
		
		UI_64 New(UI_64 tid, UI_64 osize, SFSco::IAllocator * alla = SFSco::mem_mgr) {
			UI_64 result(-1);

			Blank();

			if (!tid) tid = type_id;
			result = alla->Alloc(osize, tid);

			if (result != -1) {
				_allocator = alla;
				_al_id = result;
				_tid = tid;
				_size = osize;
			}
				
			return result;
		}


		UI_64 SetFlags(int flg) {
			UI_64 result(0);

			if (_allocator) {				
				result = _allocator->SetFlags(_al_id, flg);

			}

			return result;

		}

		UI_64 Clone(UI_64 xsize = 0, UI_64 xoff = 0) {
			UI_64 result(-1);

			if (_allocator) {
				if (_external_node) if (reinterpret_cast<UI_64>(_external_node) > 16) _size += xsize;

				result = _allocator->Alloc(_size, _tid);

				if (result != -1) {
					void * dst(0), * xsrc(0), * src(0);

					src = Acquire(&xsrc);
					if (reinterpret_cast<UI_64>(_external_node) == 1) xsrc = 0;

					_allocator->IncRef(result, dst);
					if (dst) {						
						if (!xsrc) {
							MemoryCopy_SSE(dst, reinterpret_cast<char *>(src) + xoff, _size - xoff);
						} else {
							MemoryCopy(dst, src, _size - xsize); // header
							MemoryCopy_SSE(reinterpret_cast<char *>(dst)+xoff, xsrc, xsize); // data
						}

						_allocator->DecRef(result);												
					}
					
					Release(xsrc);
					
					_external_node = 0;
					_local_offset = 0;

					_al_id = result;
				}
			}	

			return result;
		}


		
		UI_64 Child(UI_64 tid, UI_64 osize) {
			UI_64 result(-1);
	
			if (_allocator) {
				if (!tid) tid = type_id;
				result = _allocator->Alloc(osize, tid, _al_id);

				if (result != -1) {					
					_external_node = 0;					
					_local_offset = 0;

					_al_id = result;
					_tid = tid;

					_size = osize;
				}
			}

			return result;
		}

		UI_64 ShiftRight(UI_64 left_id) {			
			if (_allocator) {
				_allocator->ShiftRight(_al_id, left_id);
			}

			return 0;
		};

		UI_64 ShiftLeft(UI_64 left_id) {			
			if (_allocator) {
				_allocator->ShiftLeft(_al_id, left_id);
			}

			return 0;
		};

		// on every call of any of Acquire there must be a call of Release

		void * Acquire(void ** datap = 0) {
			void * result(0);
			if (datap) datap[0] = 0;

			if (_allocator) {
				_allocator->IncRef(_al_id, result);

				if (datap)
				if (_external_node) {
					if (reinterpret_cast<UI_64>(_external_node) == 1) datap[0] = result;
					else datap[0] = _external_node->Acquire();

					if (datap[0]) reinterpret_cast<char *&>(datap[0]) += _local_offset;
				}
			} else {
				if (reinterpret_cast<UI_64>(_external_node)>16) {
					result = reinterpret_cast<char *>(_external_node->Acquire())+_local_offset;
				}
			}

			return result;
		}
		
		void Release(void * epo = 0) {
			if (_external_node && epo) if (reinterpret_cast<UI_64>(_external_node) > 16) _external_node->Release();
			if (_allocator) {		
				if (_allocator->DecRef(_al_id) < 0) Blank();
			} else {
				if (reinterpret_cast<UI_64>(_external_node)>16) {
					_external_node->Release();
				}
			}
		}


		// all taken references must be releaseed prior to resize of any kind

		UI_64 GetSize() const {
			if (_allocator)	return _allocator->GetSize(_al_id);
			else return 0;
		}

		UI_64 Resize(UI_64 nsize) {
			UI_64 result(-1);

			if (_allocator) {
				result = _allocator->Realloc(_al_id, nsize);				
				_size = nsize;
				
			} else {
				result = New(_tid, nsize);
			}

			return result;
		}

		UI_64 Expand(UI_64 xsize) {
			UI_64 result(-1);

			if (_allocator) {
				result = _allocator->Expand(_al_id, xsize);				
				_size += xsize;
			} else {
				result = New(_tid, xsize);
			}

			return result;
		}

		Object & Next() {
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Next(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}
			}


			return *this;
		}

		Object & Back() {
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Back(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}
			}

			return *this;
		}

		Object & Left() {
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Left(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}
			}

			return *this;
		}

		Object & Right() {			
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Right(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}
			}

			return *this;
		}

		Object & Down() {			
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Down(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}
			}

			return *this;
		}

		Object & Up() {			
			if (!_external_node)
			if (_allocator) {
				_al_id = _allocator->Up(_al_id);
				if (_al_id != -1) {
					_tid = _allocator->GetTypeID(_al_id);
					_size = _allocator->GetSize(_al_id);
				}

				if (_al_id == -1) Blank();
			}

			return *this;
		}


};


template <typename T> class List {
	private:
		UI_64 _top, _runner, _xpander, _reserved;
		Object _list;

	public:
		List(unsigned int x_p = 32) : _top(0), _runner(0), _xpander(x_p) {};

		UI_64 Add(const T & ref_obj) {
			UI_64 result(0);
			T tempo, * slist(0);

			if (_runner >= _top) {
				if (_list.Expand(_xpander*sizeof(T)) != -1) {
					if (slist = reinterpret_cast<T *>(_list.Acquire())) {
						slist += _top;
						for (UI_64 ii(0);ii<_xpander;ii++) MemoryCopy(slist+ii, &tempo, sizeof(T));

						_list.Release();
						_top += _xpander;						
					}
				} else {
					result = -1;
				}
			}
			
			if (result == 0)
			if (slist = reinterpret_cast<T *>(_list.Acquire())) {
				result = -1;
				for (UI_64 ij(0);ij<_runner;ij++) {
					if (!slist[ij]) {
						slist[ij] = ref_obj;
						result = ij;

						break;
					}
				}

				if (result == -1) {
					slist[_runner] = ref_obj;
					result = _runner++;
				}


				_list.Release();
			}

			return result;
		};

		T operator [](UI_64 index) {
			T result;

			if (index < _runner) {
				if (T * slist = reinterpret_cast<T *>(_list.Acquire())) {
					result = slist[index];
					_list.Release();
				}
			}

			return result;
		};

		void Set(UI_64 index, const T & s_obj) {			
			if (index < _runner) {
				if (T * slist = reinterpret_cast<T *>(_list.Acquire())) {
					if (slist[index]) slist[index] = s_obj;

					_list.Release();
				}
			}
		};

		UI_64 FindIt(const void * sval, UI_64 length) {
			UI_64 result(-1);
			bool itis(false);

			if (length <= sizeof(T))
			if (T * slist = reinterpret_cast<T *>(_list.Acquire())) {
				for (UI_64 i(0);i<_runner;i++) {
					if (slist[i]) {
						itis = true;
						for (UI_64 jj(0);jj<length;jj++) {
							if (reinterpret_cast<unsigned char *>(slist+i)[jj] != reinterpret_cast<const unsigned char *>(sval)[jj]) {
								itis = false;
								break;
							}
						}

						if (itis) {
							result = i;
							break;
						}
					}
				}
				_list.Release();
			}

			return result;
		};

		T Remove(UI_64 index) {
			T result;

			if (index < _runner) {
				if (T * slist = reinterpret_cast<T *>(_list.Acquire())) {
					result = slist[index];
			
					slist[index].Blank();

					_list.Release();
				}
			}

			return result;

		};


		UI_64 Destroy() {
			if (T * slist = reinterpret_cast<T *>(_list.Acquire())) {
				for (UI_64 i(0);i<_runner;i++) {
					if (slist[i]) slist[i].Destroy();
				}

				_list.Release();
			}

			_list.Destroy();
			_runner = 0;

			return 0;
		};

		unsigned int GetCount() const {
			return _runner;
		};


};



}



