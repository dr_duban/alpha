/*
** safe-fail core library
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once


#ifndef COREFUNC_CDECL_INI
	#define COREFUNC_CDECL_INI(n,a,t) typedef t (__cdecl * p##n)a; extern p##n n
	#define COREFUNC_FASTCALL_INI(n,a,t) typedef t (__fastcall * p##n)a; extern p##n n	
#endif



typedef unsigned long long UI_64;
typedef long long I_64;

namespace SFSco {
	class Object;

	struct GlobalSet {
		static const UI_64 type_id = 982451497;
		static const wchar_t e_strings[][32];

		UI_64 audio_default_volume;
		UI_64 screenshot_quality;

		unsigned int media_start_on_load;
		unsigned int media_program_language;
		unsigned int media_repeat;
		unsigned int screenshot_type;		

		struct WinCfg {
			int x0, y0, w0, h0;
			int x1, y1, max_flag;

			WinCfg() : x0(0), y0(0), w0(0), h0(0), x1(0), y1(0), max_flag(0) {};
		} win_cfg;

		void Default();
	};

	struct InfoEntry {
		static const UI_64 type_id = 982451333;
		
		enum InfoTypes: unsigned int {LoadComplete = 1, MaxInfoString};

		UI_64 time_mark;
		UI_64 ie_val;
		unsigned int ie_type, val_type;
		UI_64 x_id;

		InfoEntry() : time_mark(0), ie_val(0), ie_type(0), val_type(0), x_id(-1) {};

		InfoEntry & operator =(const InfoEntry & ref_e) {
			time_mark = ref_e.time_mark;
			ie_val = ref_e.ie_val;
			ie_type = ref_e.ie_type;
			val_type = ref_e.val_type;
			x_id = ref_e.x_id;

			return *this;
		}
	};

	extern GlobalSet program_cfg;

	

	COREFUNC_CDECL_INI(Instantiate, (int), void *);
	COREFUNC_CDECL_INI(InitializeLocalObjects, (), UI_64);
	COREFUNC_CDECL_INI(FinalizeLocalObjects, (), void);
	COREFUNC_CDECL_INI(GetProgramBox, (GlobalSet::WinCfg &), void);
	COREFUNC_CDECL_INI(SetProgramBox, (const GlobalSet::WinCfg &), void);
	
//	COREFUNC_CDECL_INI(GetMemPtr, (UI_64), void *);


	COREFUNC_CDECL_INI(ISSSSE3, (), bool);
	
	

	UI_64 Initialize();
	int Finalize();	

	void FormatErrorMessage(UI_64);

// general =======================================================================================================================================================
#define SetLowPrecisionFloat_PROC_ID	0x00000001
#define SetStdPrecisionFloat_PROC_ID	0x00000002
#define LockedXOR_PROC_ID				0x00000003
#define isssse3_PROC_ID					0x00000004

	 // rounding 0 - nearest, 1 - lowest, 2 - highest, 3 - truncate
	COREFUNC_CDECL_INI(SetLowPrecisionFloat, (unsigned int rounding), void);
	COREFUNC_CDECL_INI(SetStdPrecisionFloat, (unsigned int rounding), void);
	COREFUNC_FASTCALL_INI(LockedXOR, (unsigned int *, unsigned int), unsigned int);


#define MemoryFill_SSE_PROC_ID		0x00000010
#define MemoryCopy_SSE_PROC_ID		0x00000020
#define MemoryFill_PROC_ID			0x00000030
#define MemoryCopy_PROC_ID			0x00000040
#define GetAffinityMask_PROC_ID		0x00000050

#define bit_index_low_16_PROC_ID	0x00000060
#define bit_index_low_32_PROC_ID	0x00000070
#define bit_index_low_64_PROC_ID	0x00000080
#define bit_index_high_16_PROC_ID	0x00000090
#define bit_index_high_32_PROC_ID	0x000000A0
#define bit_index_high_64_PROC_ID	0x000000B0

#define system_lock_inc64_PROC_ID	0x000000C0
#define system_lock_dec64_PROC_ID	0x000000D0

#define register_thread_PROC_ID		0x000000E0
#define unregister_thread_PROC_ID	0x000000F0


	COREFUNC_CDECL_INI(MemoryFill_SSE, (void * const destination, UI_64 byte_count, __int64 pattern_qword_0, __int64 pattern_qword_1), void);
	COREFUNC_CDECL_INI(MemoryFill, (void * const destination, UI_64 byte_count, unsigned int pattern_dword), void);
	
	COREFUNC_CDECL_INI(MemoryCopy, (void * const destination, const void * source, UI_64 byte_count), void);
	COREFUNC_CDECL_INI(MemoryCopy_SSE, (void * const destination, const void * source, UI_64 byte_count), void); // must be multiple of 64

	COREFUNC_CDECL_INI(GetAffinityMask, (unsigned int), UI_64);

	COREFUNC_FASTCALL_INI(BitIndex64Low, (const unsigned __int64), unsigned int);
	COREFUNC_FASTCALL_INI(BitIndex32Low, (const unsigned int), unsigned int);
	COREFUNC_FASTCALL_INI(BitIndex16Low, (const unsigned short), unsigned int);

	COREFUNC_FASTCALL_INI(BitIndex64High, (const unsigned __int64), unsigned int);
	COREFUNC_FASTCALL_INI(BitIndex32High, (const unsigned int), unsigned int);
	COREFUNC_FASTCALL_INI(BitIndex16High, (const unsigned short), unsigned int);

	COREFUNC_FASTCALL_INI(LockedInc, (unsigned __int64 *), unsigned __int64);
	COREFUNC_FASTCALL_INI(LockedDec, (unsigned __int64 *), unsigned __int64);

	COREFUNC_FASTCALL_INI(RegisterThread, (__int64 *), void);
	COREFUNC_FASTCALL_INI(UnregisterThread, (void), void);

// strings =======================================================================================================================================================

#define EscapeAStr_PROC_ID			0x00001010
#define StringToHexString_PROC_ID	0x00001020
#define IntToCStr_PROC_ID			0x00001030
#define UIntToCStr_PROC_ID			0x00001040
#define Int64ToCStr_PROC_ID			0x00001050
#define CStrToInt_PROC_ID			0x00001060
#define CStrToInt64_PROC_ID			0x00001070
#define FloatToCStr_PROC_ID			0x00001080
#define DoubleToCStr_PROC_ID		0x00001090
#define CStrToFloat_PROC_ID			0x000010A0
#define CStrToDouble_PROC_ID		0x000010B0
#define SearchAStr_PROC_ID			0x000010C0
#define SearchWStr_PROC_ID			0x000010D0
#define SearchBlock_PROC_ID			0x000010E0
#define ReplaceBlock_PROC_ID		0x000010F0
#define HexStringToString_PROC_ID	0x00001100


	COREFUNC_CDECL_INI(EscapeAStr, (void * dest, const void * src, unsigned int size), unsigned int); // escapse '\0' and '\\'

	COREFUNC_CDECL_INI(IntToCStr, (char * dest, int ival, int padding_on), int);
	COREFUNC_CDECL_INI(UIntToCStr, (char * dest, unsigned int uival, int padding_on), int);
	COREFUNC_CDECL_INI(Int64ToCStr, (char * dest, __int64 val, int padding_on), int); // amps below 1000000000000000000 (10^18) are accepted 

	COREFUNC_CDECL_INI(FloatToCStr, (char * dest, float val, int dec_places), int);
	COREFUNC_CDECL_INI(DoubleToCStr, (char * dest, double val, int dec_places), int);
	
	COREFUNC_CDECL_INI(CStrToInt, (const char * src, int & result), void);
	COREFUNC_CDECL_INI(CStrToInt64, (const char * src, __int64 & result), void);

	COREFUNC_CDECL_INI(CStrToDouble, (const char * src, double & result), void);
	COREFUNC_CDECL_INI(CStrToFloat, (const char * src, float & result), void);
	
	COREFUNC_CDECL_INI(StringToHexString, (void * dest, const void * src, UI_64 src_size, unsigned int wt), void);
	COREFUNC_CDECL_INI(HexStringToString, (void * dest, const void * src, UI_64 src_size), void);

	COREFUNC_CDECL_INI(SearchAStr, (const char * source, const char * sblock), char *);
	COREFUNC_CDECL_INI(SearchWStr, (const wchar_t * source, const wchar_t * sblock), wchar_t *);


	COREFUNC_CDECL_INI(SearchBlock, (const void *, UI_64, const void *, UI_64), void *);
	COREFUNC_CDECL_INI(ReplaceBlock, (void * source, unsigned int ssize, const void * sblock, unsigned int sbsize, const void * rblock, unsigned int rbsize), unsigned int);

// securitat ========================================================================================================================================================
#define forward_aes128_PROC_ID			0x00002010
#define inverse_aes128_PROC_ID			0x00002020
#define forward_aes192_PROC_ID			0x00002030
#define inverse_aes192_PROC_ID			0x00002040
#define forward_aes256_PROC_ID			0x00002050
#define inverse_aes256_PROC_ID			0x00002060
#define forward_DES_PROC_ID				0x00002070
#define inverse_DES_PROC_ID				0x00002080
#define forward_TDES_PROC_ID			0x00002090
#define inverse_TDES_PROC_ID			0x000020A0



#define CRC16_PROC_ID					0x00002100
#define CRC32_PROC_ID					0x00002110
#define sha256_PROC_ID					0x00002120
#define sha512_PROC_ID					0x00002130
#define md5_PROC_ID						0x00002140
#define ADLER32_PROC_ID					0x00002150


	COREFUNC_CDECL_INI(forward_aes128, (char * message, unsigned int mlength, char * ckey, char * xmessage), void);
	COREFUNC_CDECL_INI(inverse_aes128, (char * xmessage, unsigned int xmlength, char * ckey, char * message), void);

	COREFUNC_CDECL_INI(forward_aes192, (char * message, unsigned int mlength, char * ckey, char * xmessage), void);
	COREFUNC_CDECL_INI(inverse_aes192, (char * xmessage, unsigned int xmlength, char * ckey, char * message), void);

	COREFUNC_CDECL_INI(forward_aes256, (char * message, unsigned int mlength, char * ckey, char * xmessage), void);
	COREFUNC_CDECL_INI(inverse_aes256, (char * xmessage, unsigned int xmlength, char * ckey, char * message), void);

	COREFUNC_CDECL_INI(forward_des, (char * message, unsigned int mlength, char * ckey, char * xmessage), void);
	COREFUNC_CDECL_INI(inverse_des, (char * xmessage, unsigned int xmlength, char * ckey, char * message), void);

	COREFUNC_CDECL_INI(forward_3des, (char * message, unsigned int mlength, char * ckey, char * xmessage), void);
	COREFUNC_CDECL_INI(inverse_3des, (char * xmessage, unsigned int xmlength, char * ckey, char * message), void);

	COREFUNC_CDECL_INI(CRC16, (unsigned short & hash, const unsigned char * message, unsigned int mlength, int tid), void);
	COREFUNC_CDECL_INI(CRC32, (unsigned int & hash, const unsigned char * message, unsigned int mlength), void);
	COREFUNC_CDECL_INI(ADLER32, (unsigned int & hash, const unsigned char * message, unsigned int mlength), void);
	COREFUNC_CDECL_INI(md5, (unsigned int * hash, const char * message, unsigned int mlength, bool), void);
	COREFUNC_CDECL_INI(sha256, (unsigned int * hash, const char * message, unsigned int mlength, unsigned int), void);
	COREFUNC_CDECL_INI(sha512, (unsigned __int64 * hash, const char * message, unsigned __int64 mlength, unsigned int), void);

// net ================================================================================================================================================================
#define udpp_forwarddata_PROC_ID		0x00003010
#define udpp_initialize_PROC_ID			0x00003020
#define udpp_finalize_PROC_ID			0x00003030
#define udpp_dialin_PROC_ID				0x00003040

	typedef UI_64 (__stdcall * pActiveCom)(void *,unsigned int *,unsigned int *);


	COREFUNC_CDECL_INI(UDPP_ForwardData, (unsigned int *), int);
	COREFUNC_CDECL_INI(UDPP_Initialize, (unsigned int rem_addr, pActiveCom com_proc, unsigned int client_state_size, int client_profile_flag), unsigned int);
	COREFUNC_CDECL_INI(UDPP_Finalize, (void), int);
	COREFUNC_CDECL_INI(UDPP_Connect, (unsigned int), unsigned int);
	



// application ============================================================================================================================================================


#define application_get_time_PROC_ID	0x00004070
#define application_get_ucount_PROC_ID	0x00004080
#define application_timercal_PROC_ID	0x00004090
#define application_resetcal_PROC_ID	0x000040A0

	COREFUNC_FASTCALL_INI(GetTimeStamp, (void), unsigned __int64);
	COREFUNC_FASTCALL_INI(GetUCount, (__int64, __int64), __int64);
	
	COREFUNC_FASTCALL_INI(TimerCal, (unsigned __int64), void);
	COREFUNC_FASTCALL_INI(ResetCal, (unsigned __int64), void);

// OpenGL
#define opengl_initialize_PROC_ID		0x00005010
#define opengl_finalize_PROC_ID			0x00005020
#define opengl_getversion_PROC_ID		0x00005030
#define opengl_xyshift_PROC_ID			0x00005040
#define opengl_addie_PROC_ID			0x00005050
#define opengl_infostr_PROC_ID			0x00005060
#define opengl_hdrstr_PROC_ID			0x00005070

	COREFUNC_FASTCALL_INI(OpenGL_Initialize, (void * window_handle), int);
	COREFUNC_FASTCALL_INI(OpenGL_Finalize, (void), int);
	COREFUNC_CDECL_INI(OpenGL_Version, (void), unsigned int);
	COREFUNC_CDECL_INI(OpenGL_XYShift, (int &, int &), int);
	COREFUNC_CDECL_INI(AddIE, (const InfoEntry &), unsigned int);

	COREFUNC_CDECL_INI(GetHeaderStr, (unsigned int, wchar_t *), void);	
	COREFUNC_CDECL_INI(GetInfoStr, (unsigned int, wchar_t *, wchar_t *), void);

// Event
#define event_molupdate_PROC_ID		0x00006010
#define event_removegate_PROC_ID	0x00006020
#define event_create_PROC_ID		0x00006030
#define event_cancel_PROC_ID		0x00006040
#define event_setgate_PROC_ID		0x00006050
#define event_setdrain_PROC_ID		0x00006060
#define event_cleargates_PROC_ID	0x00006070
#define event_shutdown_PROC_ID		0x00006080

	COREFUNC_CDECL_INI(Event_MOLUpdate, (Object &), void);
	COREFUNC_CDECL_INI(Event_RemoveGate, (Object &), void);
	
	COREFUNC_CDECL_INI(Event_Cancel, (UI_64 drain_idx), void);
	COREFUNC_CDECL_INI(Event_SetGate, (UI_64 drain_idx, Object & gate_obj, UI_64 state_mask), UI_64); // add a gate for an event
	COREFUNC_CDECL_INI(Event_CreateDrain, (const void * callback, const Object * cbArg, const Object * cbObj), UI_64); // override drain for an event

	COREFUNC_CDECL_INI(Event_ShutDown, (void), void);

/*
// calculus ===========================================================================================================================================================================================================================
#define clear_4float_PROC_ID		0x00007010
#define clear_16float_PROC_ID		0x00007020
#define copy_4float_PROC_ID			0x00007030
#define copy_16float_PROC_ID		0x00007040
#define sqrt_float_PROC_ID			0x00007050
#define sin_float_PROC_ID			0x00007060
#define sin_real_PROC_ID			0x00007070
#define cos_real_PROC_ID			0x00007080
#define abs_float_PROC_ID			0x00007090

#define matrixf_negate_PROC_ID		0x000070A0
#define matrixf_scale_PROC_ID		0x000070B0
#define matrixf_d_multiply_PROC_ID	0x000070C0
#define matrixf_t_multiply_PROC_ID	0x000070D0
#define matrixf_multiply_PROC_ID	0x000070E0

#define matrixf_t_multiply4_PROC_ID	0x000070F0
#define matrixf_multiply4_PROC_ID	0x00007100
#define matrixf_t_multiply2_PROC_ID	0x00007110
#define matrixf_multiply2_PROC_ID	0x00007120

#define matrixf_i_multiply_PROC_ID	0x00007130
#define matrixf_i_multiply4_PROC_ID	0x00007140
#define matrixf_i_multiply3_PROC_ID	0x00007150
#define matrixf_i_multiply2_PROC_ID	0x00007160

#define matrixf_add_PROC_ID			0x00007170
#define matrixf_sub_PROC_ID			0x00007180

#define vectorf_negate_PROC_ID		0x00007190
#define vectorf_scale_PROC_ID		0x000071A0
#define vectorf_add_PROC_ID			0x000071B0
#define vectorf_sub_PROC_ID			0x000071C0

#define vectorf_sqrt_PROC_ID		0x000071D0
#define vectorf_c_multiply_PROC_ID	0x000071E0
#define vectorf_sup_norm_PROC_ID	0x000071F0
#define vectorf_e1_norm_PROC_ID		0x00007200

#define float_permute_PROC_ID		0x00007210
#define matrixf_from_b_PROC_ID		0x00007220

#define vectorf_dot_product_PROC_ID	0x00007230
#define matrixf_i_check_PROC_ID		0x00007240

	COREFUNC_CDECL_INI(Clear_4Float, (void * dest, UI_64 count, float clear_val), void); // count number of 4- float vals to cover
	COREFUNC_CDECL_INI(Clear_16Float, (void * dest, UI_64 count, float clear_val), void); // count number of 16- float vals to cover
	COREFUNC_CDECL_INI(Copy_4Float, (void * dest, const void * src, UI_64 count), void);
	COREFUNC_CDECL_INI(Copy_16Float, (void * dest, const void * src, UI_64 count), void);
	
	
	COREFUNC_CDECL_INI(Sqrt_Float, (float), float);

	COREFUNC_CDECL_INI(Sin_Float, (float), float);
	
	COREFUNC_CDECL_INI(Sin_Real, (double), double);
	COREFUNC_CDECL_INI(Cos_Real, (double), double);

	COREFUNC_CDECL_INI(Abs_Float, (float), float);

//	extern UI_64 Randnum();
	COREFUNC_CDECL_INI(MF_IdentityCheck, (const float *, unsigned int), UI_64);
	COREFUNC_CDECL_INI(MF_FromBuffer, (void *, float * dst, const float *), void);

	COREFUNC_CDECL_INI(MF_Negate, (void * matrix_hdr, float * elem), void);
	COREFUNC_CDECL_INI(MF_Scale, (void * matrix_hdr, float * elem, const float &), void);

	COREFUNC_CDECL_INI(MF_DiagonalMultiply, (void * matrix_hdr, float * melem, void * vector_hdr, float * vecel), void);
	
	COREFUNC_CDECL_INI(MF_InverseMultiply, (void * matrix_ahdr, float * alem, void * matrix_dhdr, float * dlem, void * xmem), int); // 0 - singular matrix, full pivoting Gauss-Jordan ; void * outp, void * dst, unsigned int cocount, unsigned int rdiv
	COREFUNC_CDECL_INI(MF_TransposeMultiply, (void * matrix_ahdr, float * alem, void * matrix_bhdr, float * blem, float * xlem), void); // void * outp, void * dst, unsigned int vcount, unsigned int vdiv
	COREFUNC_CDECL_INI(MF_Multiply, (void * matrix_ahdr, float * alem, void * matrix_bhdr, float * blem, float * xlem), void);
		
		
	COREFUNC_CDECL_INI(MF_TransposeMultiply44, (float * alem, unsigned int bcount, float * blem, float * xlem), void); // void * dst, unsigned int vcount, void * outp = 0
	COREFUNC_CDECL_INI(MF_TransposeMultiply22, (float * alem, unsigned int bcount, float * blem, float * xlem), void);
	
	COREFUNC_CDECL_INI(MF_Multiply44, (float * alem, unsigned int bcount, float * blem, float * xlem), void);
	COREFUNC_CDECL_INI(MF_Multiply22, (float * alem, unsigned int bcount, float * blem, float * xlem), void);
	

	COREFUNC_CDECL_INI(MF_InverseMultiply44, (void * matrix_ahdr, float * alem, unsigned int bcount, float * blem), int); // 0 - singular matrix, cofactor calculator
	COREFUNC_CDECL_INI(MF_InverseMultiply22, (void * matrix_ahdr, float * alem, unsigned int bcount, float * blem), int); // 0 - singular matrix, cofactor calculator
	COREFUNC_CDECL_INI(MF_InverseMultiply33, (void * matrix_ahdr, float * alem, unsigned int bcount, float * blem), int); // 0 - singular matrix, cofactor calculator

	COREFUNC_CDECL_INI(MF_AddMatrix, (void * matrix_ahdr, float * alem, void * matrix_bhdr, const float * blem), void);
	COREFUNC_CDECL_INI(MF_SubMatrix, (void * matrix_ahdr, float * alem, void * matrix_bhdr, const float * blem), void);
	
	
	COREFUNC_CDECL_INI(VF_Negate, (float * elem, unsigned int), void);

	COREFUNC_CDECL_INI(VF_Scale, (float * elem, unsigned int, const float &), void);

	COREFUNC_CDECL_INI(VF_SubVector, (float *, const float *, unsigned int), void);
	COREFUNC_CDECL_INI(VF_AddVector, (float *, const float *, unsigned int), void);
		
	COREFUNC_CDECL_INI(VF_Sqrt, (float *, unsigned int), unsigned int);
				
	COREFUNC_CDECL_INI(VF_ComponentMultiply, (float *, const float *, unsigned int), void);

	COREFUNC_CDECL_INI(VF_DotProduct, (const float *, const float *, unsigned int), float);
	COREFUNC_CDECL_INI(VF_E1Norm, (float *, unsigned int), float);
	COREFUNC_CDECL_INI(VF_SupNorm, (float *, unsigned int), float);

	COREFUNC_CDECL_INI(FloatPermute, (void * src, void * map), unsigned int);
*/

// font ===================================================================================================================================================
#define font_initialize_PROC_ID	0x00008010
#define font_finalize_PROC_ID	0x00008020

	COREFUNC_CDECL_INI(Font_Initialize, (void), UI_64);
	COREFUNC_CDECL_INI(Font_Finalize, (void), void);



}


#include "SFScoreInterfaces.h"




namespace SFSco {

	COREFUNC_CDECL_INI(Image_SaveAs, (UI_64, IOpenGL::ScreenSet &, SFSco::Object &), UI_64);
	
	COREFUNC_CDECL_INI(VideoDrain_Attach, (unsigned int, IOpenGL::ScreenSet &, unsigned int), unsigned int);



// image ===================================================================================================================================================
#define image_initialize_PROC_ID	0x00009010
#define image_finalize_PROC_ID		0x00009020
#define image_load_file_PROC_ID		0x00009030
#define image_load_data_PROC_ID		0x00009040
#define image_get_box_PROC_ID		0x00009050
#define image_dither_PROC_ID		0x00009060
#define image_dither_i_PROC_ID		0x00009070
#define image_nearest_PROC_ID		0x00009090
#define image_saveas_PROC_ID		0x000090A0
#define image_pfilter_on_PROC_ID	0x000090B0
#define image_pfilter_off_PROC_ID	0x000090C0
#define image_YUY22RGBA_PROC_ID		0x000090D0
#define image_I4202RGBA_PROC_ID		0x000090E0
#define image_BGR2RGBA_PROC_ID		0x000090F0


	COREFUNC_CDECL_INI(Image_Initialize, (SFSco::IOpenGL **), UI_64);
	COREFUNC_CDECL_INI(Image_Finalize, (void), UI_64);
	COREFUNC_CDECL_INI(Image_LoadFile, (const wchar_t *), UI_64);
	COREFUNC_CDECL_INI(Image_LoadData, (const void *, unsigned int), UI_64);
	COREFUNC_CDECL_INI(Image_GetWH, (UI_64, float &, float &), void);
	COREFUNC_CDECL_INI(Image_Dither, (unsigned int *, const unsigned int *, unsigned int, unsigned int), void);
	COREFUNC_CDECL_INI(Image_Dither_I, (unsigned int *, const UI_64 *, unsigned int, unsigned int), void);
	COREFUNC_CDECL_INI(Image_Nearest, (unsigned int *, const unsigned int *, unsigned int, unsigned int), void);
	COREFUNC_CDECL_INI(Image_EnablePostFilter, (void), void);
	COREFUNC_CDECL_INI(Image_DisablePostFilter, (void), void);
	COREFUNC_CDECL_INI(YUY22RGBA, (unsigned int *, const unsigned char *, const unsigned int *), unsigned int);
	COREFUNC_CDECL_INI(I4202RGBA, (unsigned int *, const unsigned char *, const unsigned int *), unsigned int);
	COREFUNC_CDECL_INI(BGR2RGBA, (unsigned int *, const unsigned char *, const unsigned int *), unsigned int);
	

	namespace ImageType {
		enum : unsigned int {JPG = 0, PNG, BMP, GIF, ICO, SFI};
	}
// png ======================================================================================================================================================
#define png_copycolor_4_PROC_ID	0x0000A010
#define png_filter_line_PROC_ID	0x0000A020

	COREFUNC_CDECL_INI(PNG_CopyColor_4, (void *, const unsigned char *, unsigned int *, UI_64 &), const unsigned char *);
	COREFUNC_CDECL_INI(PNG_FilterLine, (unsigned char *, const unsigned int *, unsigned int, UI_64 &), void);

// png ======================================================================================================================================================
#define jpg_applyqts_PROC_ID	0x0000B020
#define jpg_llstats_PROC_ID		0x0000B030

	COREFUNC_CDECL_INI(JPG_ApplyQTs, (unsigned short *, const UI_64 *), void);
	COREFUNC_CDECL_INI(JPG_LLStats, (unsigned short  *, const unsigned int *, const UI_64 *), void);




// files & streams =========================================================================================================================================
#define file_initialize_PROC_ID	0x0000D010
#define file_finalize_PROC_ID		0x0000D020

	COREFUNC_CDECL_INI(File_Initialize, (void), UI_64);
	COREFUNC_CDECL_INI(File_Finalize, (void), UI_64);


// video ====================================================================================================================================================
#define video_source_init_PROC_ID	0x0000C030
#define video_source_fin_PROC_ID	0x0000C040

#define video_cap_quality_PROC_ID	0x0000C0B0
#define video_get_count_PROC_ID		0x0000C0C0


#define video_drain_init_PROC_ID	0x0000F0D0
#define video_drain_fin_PROC_ID		0x0000F0E0
#define video_drain_create_PROC_ID	0x0000F0F0
#define video_drain_attach_PROC_ID	0x0000F100
#define video_drain_detach_PROC_ID	0x0000F110




	namespace VideoType {
		enum : unsigned int {H262 = 0, H264, H265, SFV, Undefined = 0xFFFFFFFF};
	}

	namespace ContainerType {
		enum : unsigned int {H222_TS = 0, H222_PS, MKV, MP4, AVI, Undefined = 0xFFFFFFFF};
	}



	COREFUNC_CDECL_INI(VideoSource_Initialize, (SFSco::IOpenGL **), UI_64);
	COREFUNC_CDECL_INI(VideoSource_Finalize, (void), UI_64);

	COREFUNC_CDECL_INI(VideoSource_Start, (unsigned int), void);
	COREFUNC_CDECL_INI(VideoSource_Stop, (unsigned int), void);
	
	COREFUNC_CDECL_INI(VideoSource_SetQuality, (unsigned int, unsigned int), void);


	COREFUNC_CDECL_INI(VideoDrain_Create, (unsigned int), unsigned int);
	COREFUNC_CDECL_INI(VideoDrain_Detach, (unsigned int), unsigned int);


// video ====================================================================================================================================================
#define audio_device_init_PROC_ID	0x0000C010
#define audio_device_fin_PROC_ID	0x0000C020

	COREFUNC_CDECL_INI(AudioDevice_Initialize, (void), UI_64);
	COREFUNC_CDECL_INI(AudioDevice_Finalize, (void), UI_64);



}

