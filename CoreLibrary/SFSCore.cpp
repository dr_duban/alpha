/*
** safe-fail core library
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include <Windows.h>

#define COREFUNC_CDECL_INI(n,a,t) typedef t (__cdecl * p##n)a; p##n n = 0
#define COREFUNC_FASTCALL_INI(n,a,t) typedef t (__fastcall * p##n)a; p##n n = 0

#include "CoreLibrary\SFScore.h"

#ifndef SFS_LIB_NAME
#define SFS_LIB_NAME	sf_core.dll
#endif



#define LIB_STR(a) #a
#define LIB_PASS(a) LIB_STR(a)
#define LIB_LPASS(a) L##a
#define STR_LSTR(a) LIB_LPASS(a)
#define SFS_LIB_QNAME LIB_PASS(SFS_LIB_NAME)


HMODULE sfs_core_lib = 0;
SFSco::IAllocator * SFSco::mem_mgr = 0;
SFSco::IAllocator * SFSco::large_mem_mgr = 0;
SFSco::IAllocator * SFSco::xl_mem_mgr = 0;
SFSco::IAllocator * SFSco::file_cache = 0;


UI_64 SFSco::Initialize() {
	UI_64 result(0);
	if (sfs_core_lib = LoadLibrary(STR_LSTR(SFS_LIB_QNAME))) {
		if ((Instantiate = reinterpret_cast<pInstantiate>(GetProcAddress(sfs_core_lib, (LPCSTR)1))) && 
			(FinalizeLocalObjects = reinterpret_cast<pFinalizeLocalObjects>(GetProcAddress(sfs_core_lib, (LPCSTR)2))) && 
			(GetProgramBox = reinterpret_cast<pGetProgramBox>(GetProcAddress(sfs_core_lib, (LPCSTR)3))) && 
			(InitializeLocalObjects = reinterpret_cast<pInitializeLocalObjects>(GetProcAddress(sfs_core_lib, (LPCSTR)4))) &&
			(SetProgramBox = reinterpret_cast<pSetProgramBox>(GetProcAddress(sfs_core_lib, (LPCSTR)5)))) 		
		
		{


			SetLowPrecisionFloat = reinterpret_cast<pSetLowPrecisionFloat>(Instantiate(SetLowPrecisionFloat_PROC_ID));
			SetStdPrecisionFloat = reinterpret_cast<pSetLowPrecisionFloat>(Instantiate(SetStdPrecisionFloat_PROC_ID));
			LockedXOR = reinterpret_cast<pLockedXOR>(Instantiate(LockedXOR_PROC_ID));

			MemoryFill_SSE = reinterpret_cast<pMemoryFill_SSE>(Instantiate(MemoryFill_SSE_PROC_ID));
			MemoryCopy_SSE = reinterpret_cast<pMemoryCopy_SSE>(Instantiate(MemoryCopy_SSE_PROC_ID));
			MemoryFill = reinterpret_cast<pMemoryFill>(Instantiate(MemoryFill_PROC_ID));
			MemoryCopy = reinterpret_cast<pMemoryCopy>(Instantiate(MemoryCopy_PROC_ID));
			
			LockedInc = reinterpret_cast<pLockedInc>(Instantiate(system_lock_inc64_PROC_ID));
			LockedDec = reinterpret_cast<pLockedDec>(Instantiate(system_lock_dec64_PROC_ID));

			GetAffinityMask = reinterpret_cast<pGetAffinityMask>(Instantiate(GetAffinityMask_PROC_ID));

			RegisterThread = reinterpret_cast<pRegisterThread>(Instantiate(register_thread_PROC_ID));
			UnregisterThread = reinterpret_cast<pUnregisterThread>(Instantiate(unregister_thread_PROC_ID));

			BitIndex16Low = reinterpret_cast<pBitIndex16Low>(Instantiate(bit_index_low_16_PROC_ID));
			BitIndex32Low = reinterpret_cast<pBitIndex32Low>(Instantiate(bit_index_low_32_PROC_ID));
			BitIndex64Low = reinterpret_cast<pBitIndex64Low>(Instantiate(bit_index_low_64_PROC_ID));

			BitIndex16High = reinterpret_cast<pBitIndex16High>(Instantiate(bit_index_high_16_PROC_ID));
			BitIndex32High = reinterpret_cast<pBitIndex32High>(Instantiate(bit_index_high_32_PROC_ID));
			BitIndex64High = reinterpret_cast<pBitIndex64High>(Instantiate(bit_index_high_64_PROC_ID));


			EscapeAStr = reinterpret_cast<pEscapeAStr>(Instantiate(EscapeAStr_PROC_ID));
			StringToHexString = reinterpret_cast<pStringToHexString>(Instantiate(StringToHexString_PROC_ID));
			HexStringToString = reinterpret_cast<pHexStringToString>(Instantiate(HexStringToString_PROC_ID));

			SearchAStr = reinterpret_cast<pSearchAStr>(Instantiate(SearchAStr_PROC_ID));
			SearchWStr = reinterpret_cast<pSearchWStr>(Instantiate(SearchWStr_PROC_ID));

			SearchBlock = reinterpret_cast<pSearchBlock>(Instantiate(SearchBlock_PROC_ID));
			ReplaceBlock = reinterpret_cast<pReplaceBlock>(Instantiate(ReplaceBlock_PROC_ID));


			IntToCStr = reinterpret_cast<pIntToCStr>(Instantiate(IntToCStr_PROC_ID));
			UIntToCStr = reinterpret_cast<pUIntToCStr>(Instantiate(UIntToCStr_PROC_ID));
			Int64ToCStr = reinterpret_cast<pInt64ToCStr>(Instantiate(Int64ToCStr_PROC_ID));
			CStrToInt = reinterpret_cast<pCStrToInt>(Instantiate(CStrToInt_PROC_ID));
			CStrToInt64 = reinterpret_cast<pCStrToInt64>(Instantiate(CStrToInt64_PROC_ID));

			FloatToCStr = reinterpret_cast<pFloatToCStr>(Instantiate(FloatToCStr_PROC_ID));
			DoubleToCStr = reinterpret_cast<pDoubleToCStr>(Instantiate(DoubleToCStr_PROC_ID));
			CStrToFloat = reinterpret_cast<pCStrToFloat>(Instantiate(CStrToFloat_PROC_ID));
			CStrToDouble = reinterpret_cast<pCStrToDouble>(Instantiate(CStrToDouble_PROC_ID));



			forward_aes128 = reinterpret_cast<pforward_aes128>(Instantiate(forward_aes128_PROC_ID));
			inverse_aes128 = reinterpret_cast<pinverse_aes128>(Instantiate(inverse_aes128_PROC_ID));
			forward_aes192 = reinterpret_cast<pforward_aes192>(Instantiate(forward_aes192_PROC_ID));
			inverse_aes192 = reinterpret_cast<pinverse_aes192>(Instantiate(inverse_aes192_PROC_ID));
			forward_aes256 = reinterpret_cast<pforward_aes256>(Instantiate(forward_aes256_PROC_ID));
			inverse_aes256 = reinterpret_cast<pinverse_aes256>(Instantiate(inverse_aes256_PROC_ID));

			forward_des = reinterpret_cast<pforward_des>(Instantiate(forward_DES_PROC_ID));
			inverse_des = reinterpret_cast<pinverse_des>(Instantiate(inverse_DES_PROC_ID));

			forward_3des = reinterpret_cast<pforward_3des>(Instantiate(forward_TDES_PROC_ID));
			inverse_3des = reinterpret_cast<pinverse_3des>(Instantiate(inverse_TDES_PROC_ID));

			sha256 = reinterpret_cast<psha256>(Instantiate(sha256_PROC_ID));
			sha512 = reinterpret_cast<psha512>(Instantiate(sha512_PROC_ID));
			md5 = reinterpret_cast<pmd5>(Instantiate(md5_PROC_ID));
			CRC32 = reinterpret_cast<pCRC32>(Instantiate(CRC32_PROC_ID));
			CRC16 = reinterpret_cast<pCRC16>(Instantiate(CRC16_PROC_ID));
			ADLER32 = reinterpret_cast<pADLER32>(Instantiate(ADLER32_PROC_ID));
/*
			UDPP_ForwardData = reinterpret_cast<pUDPP_ForwardData>(Instantiate(udpp_forwarddata_PROC_ID));
			UDPP_Initialize = reinterpret_cast<pUDPP_Initialize>(Instantiate(udpp_initialize_PROC_ID));
			UDPP_Finalize = reinterpret_cast<pUDPP_Finalize>(Instantiate(udpp_finalize_PROC_ID));
			UDPP_Connect = reinterpret_cast<pUDPP_Connect>(Instantiate(udpp_dialin_PROC_ID));
*/


			GetTimeStamp = reinterpret_cast<pGetTimeStamp>(Instantiate(application_get_time_PROC_ID));
			GetUCount = reinterpret_cast<pGetUCount>(Instantiate(application_get_ucount_PROC_ID));
			TimerCal = reinterpret_cast<pTimerCal>(Instantiate(application_timercal_PROC_ID));
			ResetCal = reinterpret_cast<pResetCal>(Instantiate(application_resetcal_PROC_ID));


			OpenGL_Initialize = reinterpret_cast<pOpenGL_Initialize>(Instantiate(opengl_initialize_PROC_ID));			
			OpenGL_Version = reinterpret_cast<pOpenGL_Version>(Instantiate(opengl_getversion_PROC_ID));
			OpenGL_XYShift = reinterpret_cast<pOpenGL_XYShift>(Instantiate(opengl_xyshift_PROC_ID));
			AddIE = reinterpret_cast<pAddIE>(Instantiate(opengl_addie_PROC_ID));

			GetHeaderStr = reinterpret_cast<pGetHeaderStr>(Instantiate(opengl_hdrstr_PROC_ID));
			GetInfoStr = reinterpret_cast<pGetInfoStr>(Instantiate(opengl_infostr_PROC_ID));

			Event_MOLUpdate = reinterpret_cast<pEvent_MOLUpdate>(Instantiate(event_molupdate_PROC_ID));
			Event_RemoveGate = reinterpret_cast<pEvent_RemoveGate>(Instantiate(event_removegate_PROC_ID));
			Event_Cancel = reinterpret_cast<pEvent_Cancel>(Instantiate(event_cancel_PROC_ID));
			Event_SetGate = reinterpret_cast<pEvent_SetGate>(Instantiate(event_setgate_PROC_ID));
			Event_CreateDrain = reinterpret_cast<pEvent_CreateDrain>(Instantiate(event_setdrain_PROC_ID));
			Event_ShutDown = reinterpret_cast<pEvent_ShutDown>(Instantiate(event_shutdown_PROC_ID));

			Font_Initialize = reinterpret_cast<pFont_Initialize>(Instantiate(font_initialize_PROC_ID));
			Font_Finalize = reinterpret_cast<pFont_Finalize>(Instantiate(font_finalize_PROC_ID));

			Image_Initialize = reinterpret_cast<pImage_Initialize>(Instantiate(image_initialize_PROC_ID));
			Image_Finalize = reinterpret_cast<pImage_Finalize>(Instantiate(image_finalize_PROC_ID));
			Image_LoadFile = reinterpret_cast<pImage_LoadFile>(Instantiate(image_load_file_PROC_ID));
			Image_LoadData = reinterpret_cast<pImage_LoadData>(Instantiate(image_load_data_PROC_ID));
			Image_GetWH = reinterpret_cast<pImage_GetWH>(Instantiate(image_get_box_PROC_ID));
			Image_Dither = reinterpret_cast<pImage_Dither>(Instantiate(image_dither_PROC_ID));
			Image_Dither_I = reinterpret_cast<pImage_Dither_I>(Instantiate(image_dither_i_PROC_ID));
			Image_Nearest = reinterpret_cast<pImage_Nearest>(Instantiate(image_nearest_PROC_ID));
			Image_SaveAs = reinterpret_cast<pImage_SaveAs>(Instantiate(image_saveas_PROC_ID));

			JPG_ApplyQTs = reinterpret_cast<pJPG_ApplyQTs>(Instantiate(jpg_applyqts_PROC_ID));
			JPG_LLStats = reinterpret_cast<pJPG_LLStats>(Instantiate(jpg_llstats_PROC_ID));

			PNG_CopyColor_4 = reinterpret_cast<pPNG_CopyColor_4>(Instantiate(png_copycolor_4_PROC_ID));
			PNG_FilterLine = reinterpret_cast<pPNG_FilterLine>(Instantiate(png_filter_line_PROC_ID));


			YUY22RGBA = reinterpret_cast<pYUY22RGBA>(Instantiate(image_YUY22RGBA_PROC_ID));
			I4202RGBA = reinterpret_cast<pI4202RGBA>(Instantiate(image_I4202RGBA_PROC_ID));
			BGR2RGBA = reinterpret_cast<pI4202RGBA>(Instantiate(image_BGR2RGBA_PROC_ID));
			
			VideoSource_Initialize = reinterpret_cast<pVideoSource_Initialize>(Instantiate(video_source_init_PROC_ID));
			VideoSource_Finalize = reinterpret_cast<pVideoSource_Finalize>(Instantiate(video_source_fin_PROC_ID));
			VideoSource_SetQuality = reinterpret_cast<pVideoSource_SetQuality>(Instantiate(video_cap_quality_PROC_ID));

			VideoDrain_Create = reinterpret_cast<pVideoDrain_Create>(Instantiate(video_drain_create_PROC_ID));
			VideoDrain_Attach = reinterpret_cast<pVideoDrain_Attach>(Instantiate(video_drain_attach_PROC_ID));
			VideoDrain_Detach = reinterpret_cast<pVideoDrain_Detach>(Instantiate(video_drain_detach_PROC_ID));


			AudioDevice_Initialize = reinterpret_cast<pAudioDevice_Initialize>(Instantiate(audio_device_init_PROC_ID));			
			AudioDevice_Finalize = reinterpret_cast<pAudioDevice_Finalize>(Instantiate(audio_device_fin_PROC_ID));


			File_Initialize = reinterpret_cast<pFile_Initialize>(Instantiate(file_initialize_PROC_ID));
			File_Finalize = reinterpret_cast<pFile_Finalize>(Instantiate(file_finalize_PROC_ID));

			ISSSSE3 = reinterpret_cast<pISSSSE3>(Instantiate(isssse3_PROC_ID));

			if (ISSSSE3()) {

				result = InitializeLocalObjects(); // must be called

				if (!result) {
					mem_mgr = reinterpret_cast<IAllocator *>(Instantiate(allocator_IFACE_ID));
					large_mem_mgr = reinterpret_cast<IAllocator *>(Instantiate(lallocator_IFACE_ID));
					xl_mem_mgr = reinterpret_cast<IAllocator *>(Instantiate(xlallocator_IFACE_ID));
					file_cache = reinterpret_cast<IAllocator *>(Instantiate(fallocator_IFACE_ID));
				}
			} else {
				result = 0x55504345;
				result <<= 32;

			}
		}



	} else {
		result = 0x45535953;
		result <<= 32;
		result |= GetLastError();
		
	}

	return result;
}

int SFSco::Finalize() {
	int result(0);
		
	if (FinalizeLocalObjects) FinalizeLocalObjects();

	if (sfs_core_lib) FreeLibrary(sfs_core_lib);

	return result;
}

void SFSco::FormatErrorMessage(UI_64 e_code) {
	wchar_t x_mes[512];
	const wchar_t * e_class(0), * e_message(0);

	switch (e_code >> 40) {
		case 0x00535953: // system error code
			e_class = GlobalSet::e_strings[0];

			if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, e_code & 0xFFFFFFFF, LANG_USER_DEFAULT, x_mes, 512, 0)) {
				e_message = x_mes;
			} else {
				e_message = GlobalSet::e_strings[6];
			}
		break;
		case 0x00414843: // chain error
			e_class = GlobalSet::e_strings[1];
			switch ((e_code >> 32) & 0x00FF) {
				case 0x53: // system
					if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, e_code & 0xFFFFFFFF, LANG_USER_DEFAULT, x_mes, 512, 0)) {
						e_message = x_mes;
					} else {
						e_message = GlobalSet::e_strings[6];
					}

				break;
				case 0x46: // file/existing
					e_message = GlobalSet::e_strings[2];
				break;
				case 0x45:

				break;
		//		default: // lock fuckup


			}
		break;
		case 0x004C474F: // opengl error

		break;
		case 0x00555043: // system requirements
			e_class = GlobalSet::e_strings[3];

			switch ((e_code >> 32) & 0x00FF) {
				case 0x45:
					e_message = GlobalSet::e_strings[4];
				break;
				case 0x57:
					e_message = GlobalSet::e_strings[5];
				break;

			}

		break;

	}

	if (e_message && e_class) MessageBox(0, e_message, e_class, MB_ICONSTOP);
}


// ===============================================================================================================================================================================================================================================

const wchar_t SFSco::GlobalSet::e_strings[][32] = {
	L"System Error",
	L"Chain Error",
	L"Corrupted cache File",
	L"System Requirements",
	L"Outdated CPU",
	L"Windows Vista or Greater",
	L"Uknown Error"

};



