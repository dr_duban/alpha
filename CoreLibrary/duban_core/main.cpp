/*
** safe-fail core library
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/



#include <Windows.h>

#define COREFUNC_CDECL_INI(n,a,t) typedef t (__cdecl * p##n)a; p##n n = 0
#define COREFUNC_FASTCALL_INI(n,a,t) typedef t (__fastcall * p##n)a; p##n n = 0


#include "CoreLibrary\SFSCore.h"

#include "System\SysUtils.h"
#include "System\CPUID.h"
#include "System\Memory.h"
#include "Securitat\Crypter.h"

#include "System\Control.h"
#include "OpenGL\OpenGL.h"
#include "OpenGL\Font.h"

#include "Pics\Image.h"
#include "Pics\PNG.h"
#include "Pics\JPEG.h"

#include "Math\CalcSpecial.h"
#include "Media\Capture.h"

#include "Data\Files.h"

#include "Apps\MainMenu.h"
#include "Apps\XFiles.h"
#include "Apps\XPic.h"
#include "Apps\XVideo.h"
#include "Apps\XAudio.h"
#include "Apps\NotificationArea.h"

#include "Audio\AudioRenderer.h"

Memory::Allocator * memacha_pool(0), * memacha_lool(0), * memacha_xlool(0), * file_pool(0);

SFSco::IAllocator * SFSco::mem_mgr = 0;
SFSco::IAllocator * SFSco::large_mem_mgr = 0;
SFSco::IAllocator * SFSco::xl_mem_mgr = 0;
SFSco::IAllocator * SFSco::file_cache = 0;

SFSco::GlobalSet SFSco::program_cfg;

WSADATA local_wsaData = {0};

void SFSco::GlobalSet::Default() {
	audio_default_volume = 0x3FF0000000000000;
	screenshot_quality = 0x3FE999999999999A;
	
	media_program_language = 0;
	
	media_start_on_load = 1;

	media_repeat = 1;
	screenshot_type = 0;

}


BOOL __stdcall DllMain(HINSTANCE hinst, DWORD reason, LPVOID reserved) {
	if (DLL_PROCESS_ATTACH == reason) {

		IDCPU::Initialize();
		SFSco::ISSSSE3 = IDCPU::tossSSSE3;

		Memory::Buffer::Initialize();
		
		SFSco::SetLowPrecisionFloat = SetLowPrecisionFloat;
		SFSco::SetStdPrecisionFloat = SetStdPrecisionFloat;

		SFSco::MemoryFill = System::MemoryFill;
		SFSco::MemoryFill_SSE = System::MemoryFill_SSE3;

		SFSco::MemoryCopy = System::MemoryCopy;
		SFSco::MemoryCopy_SSE = System::MemoryCopy_SSE3;
	
		SFSco::LockedInc = System::LockedInc_64;
		SFSco::LockedDec = System::LockedDec_64;

		SFSco::GetAffinityMask = IDCPU::GetCoreMask;

		SFSco::RegisterThread = Memory::Chain::RegisterThread;
		SFSco::UnregisterThread = Memory::Chain::UnregisterThread;


		SFSco::BitIndex16Low = System::BitIndexLow_16;
		SFSco::BitIndex32Low = System::BitIndexLow_32;
		SFSco::BitIndex64Low = System::BitIndexLow_64;

		SFSco::BitIndex16High = System::BitIndexHigh_16;
		SFSco::BitIndex32High = System::BitIndexHigh_32;
		SFSco::BitIndex64High = System::BitIndexHigh_64;


	//	SFSco::EscapeAStr = MyEscape;
		SFSco::EscapeAStr = Strings::MyEscape_SSE3;
		
	//	SFSco::StringToHexString = StringToHexString;
		SFSco::StringToHexString = Strings::StringToHexString_SSE3;
		SFSco::HexStringToString = Strings::HexStringToString_SSE3;

		SFSco::IntToCStr = Strings::IntToCStr;
		if (IDCPU::tossSSSE3()) SFSco::IntToCStr = Strings::IntToCStr_SSSE3;
			
		SFSco::UIntToCStr = Strings::UIntToCStr;
		if (IDCPU::tossSSSE3()) SFSco::UIntToCStr = Strings::UIntToCStr_SSSE3;
			
		SFSco::Int64ToCStr = Strings::Int64ToCStr;
		if (IDCPU::tossSSSE3()) SFSco::Int64ToCStr = Strings::Int64ToCStr_SSSE3;
			
		SFSco::CStrToInt = Strings::CStrToInt;

		SFSco::CStrToInt64 = Strings::CStrToInt64;

		SFSco::FloatToCStr = Strings::FloatToCStr_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::FloatToCStr = Strings::FloatToCStr_SSSE3;

		SFSco::DoubleToCStr = Strings::DoubleToCStr_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::DoubleToCStr = Strings::DoubleToCStr_SSSE3;

		SFSco::CStrToFloat = Strings::CStrToFloat;

		SFSco::CStrToDouble = Strings::CStrToDouble;



		SFSco::SearchAStr = Strings::SearchAStr;
		SFSco::SearchWStr = Strings::SearchWStr;

		SFSco::SearchBlock = Strings::SearchBlock;

/*
		SFSco::Clear_4Float = FloatClear4;
		SFSco::Clear_16Float = FloatClear16;
		SFSco::Copy_4Float = FloatCopy4;
		SFSco::Copy_16Float = FloatCopy16;

		SFSco::Sqrt_Float = FloatSqrt;
		SFSco::Sin_Float = FloatSin;
		SFSco::Sin_Real = RealSin;
		SFSco::Cos_Real = RealCos;
		SFSco::Abs_Float = FloatAbs;

		SFSco::MF_IdentityCheck = MF_IdentityCheck;
		SFSco::MF_Negate = MF_Negate;
		SFSco::MF_Scale = MF_Scale;
		SFSco::MF_DiagonalMultiply = MF_DiagonalMultiply;
		SFSco::MF_TransposeMultiply = MF_TransposeMultiply;
		SFSco::MF_Multiply = MF_Multiply;

		SFSco::MF_TransposeMultiply44 = MF_TransposeMultiply44;
		SFSco::MF_Multiply44 = MF_Multiply44;
		SFSco::MF_TransposeMultiply22 = MF_TransposeMultiply22;
		SFSco::MF_Multiply22 = MF_Multiply22;

		SFSco::MF_InverseMultiply = MF_InverseMultiply;
		SFSco::MF_InverseMultiply44 = MF_InverseMultiply44;
		SFSco::MF_InverseMultiply33 = MF_InverseMultiply33;
		SFSco::MF_InverseMultiply22 = MF_InverseMultiply22;

		SFSco::MF_AddMatrix = MF_AddMatrix;
		SFSco::MF_SubMatrix = MF_SubMatrix;

		SFSco::MF_FromBuffer = MF_FromBuffer;

		SFSco::VF_Negate = VF_Negate;
		SFSco::VF_Scale = VF_Scale;
		SFSco::VF_AddVector = VF_AddVector;
		SFSco::VF_SubVector = VF_SubVector;

		SFSco::VF_Sqrt = VF_Sqrt;
		SFSco::VF_ComponentMultiply = VF_ComponentMultiply;
		SFSco::VF_SupNorm = VF_SupNorm;
		SFSco::VF_E1Norm = VF_E1Norm;
		SFSco::FloatPermute = FloatPermute;
		SFSco::VF_DotProduct = VF_DotProduct;
*/


		SFSco::forward_aes128 = Crypt::forward_aes128;
		if (IDCPU::tossSSSE3()) SFSco::forward_aes128 = Crypt::forward_aes128_SSSE3;
		SFSco::inverse_aes128 = Crypt::reverse_aes128;
		if (IDCPU::tossSSSE3())  SFSco::inverse_aes128 = Crypt::reverse_aes128_SSSE3;

		SFSco::forward_aes192 = Crypt::forward_aes192;
		SFSco::inverse_aes192 = Crypt::reverse_aes192;

		SFSco::forward_aes256 = Crypt::forward_aes256;
		if (IDCPU::tossSSSE3()) SFSco::forward_aes256 = Crypt::forward_aes256_SSSE3;
		SFSco::inverse_aes256 = Crypt::reverse_aes256;
		if (IDCPU::tossSSSE3()) SFSco::inverse_aes256 = Crypt::reverse_aes256_SSSE3;

		SFSco::forward_des = Crypt::forward_DES;
		SFSco::inverse_des = Crypt::reverse_DES;
		SFSco::forward_3des = Crypt::forward_TDES;
		SFSco::inverse_3des = Crypt::reverse_TDES;
		

		SFSco::sha256 = Crypt::sha256;
		if (IDCPU::tossSSSE3()) SFSco::sha256 = Crypt::sha256_SSSE3;

		SFSco::sha512 = Crypt::sha512;
		if (IDCPU::tossSSSE3()) SFSco::sha512 = Crypt::sha512_SSSE3;

		SFSco::md5 = Crypt::md5;

		SFSco::CRC32 = Crypt::CRC_32;
		SFSco::CRC16 = Crypt::CRC_16;

		SFSco::ADLER32 = Crypt::ADLER_32;
/*
		SFSco::UDPP_ForwardData = UDPP::ControlBus::ForwardData;

		SFSco::UDPP_Initialize = UDPP::ControlBus::Initialize;

		SFSco::UDPP_Finalize = UDPP::ControlBus::Finalize;

		SFSco::UDPP_Connect = UDPP::ControlBus::DialIn;
*/	
/*
		SFSco::Launcher_AddInitializer = Control::Launcher::AddInitialzier;
		SFSco::Launcher_Run = Control::Launcher::Run;

		SFSco::Application_Create = Control::Application::Create;
		SFSco::Application_Close = Control::Application::Close;
		SFSco::Application_Run = Control::Application::Run;
*/
		SFSco::GetTimeStamp = System::GetTime;
		SFSco::GetUCount = System::GetUCount;

		SFSco::TimerCal = System::TimerCal;
		SFSco::ResetCal = System::ResetCal;

		SFSco::OpenGL_Initialize = OpenGL::Core::Initialize;
		SFSco::OpenGL_Finalize = OpenGL::Core::Finalize;
		SFSco::OpenGL_Version = OpenGL::Core::Version;
		SFSco::OpenGL_XYShift = OpenGL::Core::GetXYShifts;
		SFSco::AddIE = Apps::NotificationArea::AddInfoEntry;

		SFSco::GetHeaderStr = OpenGL::Core::GetHeaderString;		
		SFSco::GetInfoStr = OpenGL::Core::GetInfoString;

		SFSco::Event_MOLUpdate = Control::Event::MOLUpdate;
		SFSco::Event_RemoveGate = Control::Event::RemoveGate;
//		SFSco::Event_Create = Control::Event::Create;
		SFSco::Event_Cancel = Control::Event::Cancel;
//		SFSco::Event_ClearGates = Control::Event::ClearGates;
		SFSco::Event_CreateDrain = Control::Event::CreateDrain;
		SFSco::Event_SetGate = Control::Event::SetGate;

		SFSco::Event_ShutDown = Control::Event::ShutDown;
//		SFSco::Event_Finalize = Control::Event::Finalize;
/*		
		SFSco::Font_Create = OpenGL::Font::Create;
		SFSco::Font_InitializeRange = OpenGL::Font::InitializeRange;
		SFSco::Font_GetMetrixBox = OpenGL::Font::GetMetrixBox;
*/

		SFSco::Font_Initialize = OpenGL::Font::Initialize;
		SFSco::Font_Finalize = OpenGL::Font::Finalize;

		SFSco::Image_Initialize = Pics::Image::Initialize;
		SFSco::Image_Finalize = Pics::Image::Finalize;
		SFSco::Image_LoadFile = Pics::Image::Load;
		SFSco::Image_LoadData = Pics::Image::Load;
		SFSco::Image_GetWH = Pics::Image::GetWH;
		SFSco::Image_SaveAs = Pics::Image::SaveAs;

		SFSco::Image_Dither = Pics::Image_Dither;
		if (IDCPU::tossSSE41()) SFSco::Image_Dither = Pics::Image_Dither_SSE41;

		SFSco::Image_Dither_I = Pics::Image_Dither_I;
		if (IDCPU::tossSSSE3()) SFSco::Image_Dither_I = Pics::Image_Dither_I_SSSE3;

		SFSco::Image_Nearest = Pics::Image_Nearest;
		if (IDCPU::tossSSE41()) SFSco::Image_Nearest = Pics::Image_Nearest_SSE41;

		SFSco::JPG_ApplyQTs = Pics::JPEG_ApplyQTs_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::JPG_ApplyQTs = Pics::JPEG_ApplyQTs_SSSE3;
		SFSco::JPG_LLStats = Pics::JPEG_LLStats_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::JPG_LLStats = Pics::JPEG_LLStats_SSSE3;


		SFSco::PNG_CopyColor_4 = Pics::PNG_CopyColor_4_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::PNG_CopyColor_4 = Pics::PNG_CopyColor_4_SSSE3;
		SFSco::PNG_FilterLine = Pics::PNG_FilterLine_SSE3;
		if (IDCPU::tossSSSE3()) SFSco::PNG_FilterLine = Pics::PNG_FilterLine_SSSE3;



		SFSco::VideoSource_Initialize = Media::Source::Initialize;
		SFSco::VideoSource_Finalize = Media::Source::Finalize;

		SFSco::YUY22RGBA = Pics::Image_YUY22RGBA;
		if (IDCPU::tossSSSE3()) SFSco::YUY22RGBA = Pics::Image_YUY22RGBA_SSSE3;
		SFSco::I4202RGBA = Pics::Image_I4202RGBA;
		if (IDCPU::tossSSSE3()) SFSco::I4202RGBA = Pics::Image_I4202RGBA_SSSE3;
		SFSco::BGR2RGBA = Pics::Image_BGR2RGBA;
		if (IDCPU::tossSSSE3()) SFSco::BGR2RGBA = Pics::Image_BGR2RGBA_SSSE3;

		SFSco::VideoSource_SetQuality = Video::CaptureSource::SetSourceQuality;

		SFSco::VideoDrain_Create = Media::Drain::Create;
		SFSco::VideoDrain_Attach = Media::Drain::AttachSource;
		SFSco::VideoDrain_Detach = Media::Drain::DetachSource;

		
		SFSco::AudioDevice_Initialize = Audio::DefaultRenderer::Initialize;
		SFSco::AudioDevice_Finalize = Audio::DefaultRenderer::Finalize;



		SFSco::File_Initialize = Data::File::Initialize;
		SFSco::File_Finalize = Data::File::Finalize;

	} else if (DLL_PROCESS_DETACH) {
		Memory::Buffer::Finalize();
		IDCPU::Finalize();

	}

	return TRUE;
}

// ==========================================================================================

__declspec(dllexport) void * CreateSFSInstance(int IID) {
	
	switch (IID) {
		case SetLowPrecisionFloat_PROC_ID:
			return SFSco::SetLowPrecisionFloat;
		case SetStdPrecisionFloat_PROC_ID:
			return SFSco::SetStdPrecisionFloat;
		case isssse3_PROC_ID:
			return SFSco::ISSSSE3;


		case MemoryFill_SSE_PROC_ID:
			return SFSco::MemoryFill_SSE;
		case MemoryCopy_SSE_PROC_ID:
			return SFSco::MemoryCopy_SSE;
		case system_lock_inc64_PROC_ID:
			return SFSco::LockedInc;
		case system_lock_dec64_PROC_ID:
			return SFSco::LockedDec;

		case MemoryFill_PROC_ID:
			return SFSco::MemoryFill;
		case MemoryCopy_PROC_ID:
			return SFSco::MemoryCopy;
		case GetAffinityMask_PROC_ID:
			return SFSco::GetAffinityMask;
		
		case register_thread_PROC_ID:
			return SFSco::RegisterThread;
		case unregister_thread_PROC_ID:
			return SFSco::UnregisterThread;

		case bit_index_low_16_PROC_ID:
			return SFSco::BitIndex16Low;
		case bit_index_low_32_PROC_ID:
			return SFSco::BitIndex32Low;
		case bit_index_low_64_PROC_ID:
			return SFSco::BitIndex64Low;
		case bit_index_high_16_PROC_ID:
			return SFSco::BitIndex16High;
		case bit_index_high_32_PROC_ID:
			return SFSco::BitIndex32High;
		case bit_index_high_64_PROC_ID:
			return SFSco::BitIndex64High;



		case EscapeAStr_PROC_ID:
			return SFSco::EscapeAStr;
		case StringToHexString_PROC_ID:
			return SFSco::StringToHexString;
		case HexStringToString_PROC_ID:
			return SFSco::HexStringToString;
		case IntToCStr_PROC_ID:
			return SFSco::IntToCStr;
		case UIntToCStr_PROC_ID:
			return SFSco::UIntToCStr;
		case Int64ToCStr_PROC_ID:
			return SFSco::Int64ToCStr;
		case CStrToInt_PROC_ID:
			return SFSco::CStrToInt;
		case CStrToInt64_PROC_ID:
			return SFSco::CStrToInt64;
		case FloatToCStr_PROC_ID:
			return SFSco::FloatToCStr;
		case DoubleToCStr_PROC_ID:
			return SFSco::DoubleToCStr;
		case CStrToFloat_PROC_ID:
			return SFSco::CStrToFloat;
		case CStrToDouble_PROC_ID:
			return SFSco::CStrToDouble;
		case SearchAStr_PROC_ID:
			return SFSco::SearchAStr;
		case SearchWStr_PROC_ID:
			return SFSco::SearchWStr;

		case SearchBlock_PROC_ID:
			return SFSco::SearchBlock;
		case ReplaceBlock_PROC_ID:
			return SFSco::ReplaceBlock;


/*
		case clear_4float_PROC_ID:
			return SFSco::Clear_4Float;
		case clear_16float_PROC_ID:
			return SFSco::Clear_16Float;
		case copy_4float_PROC_ID:
			return SFSco::Copy_4Float;
		case copy_16float_PROC_ID:
			return SFSco::Copy_16Float;
		case sqrt_float_PROC_ID:
			return SFSco::Sqrt_Float;
		case sin_float_PROC_ID:
			return SFSco::Sin_Float;
		case sin_real_PROC_ID:
			return SFSco::Sin_Real;
		case cos_real_PROC_ID:
			return SFSco::Cos_Real;
		case abs_float_PROC_ID:
			return SFSco::Abs_Float;

		case matrixf_negate_PROC_ID:
			return SFSco::MF_Negate;
		case matrixf_scale_PROC_ID:
			return SFSco::MF_Scale;
		case matrixf_d_multiply_PROC_ID:
			return SFSco::MF_DiagonalMultiply;
		case matrixf_t_multiply_PROC_ID:
			return SFSco::MF_TransposeMultiply;
		case matrixf_multiply_PROC_ID:
			return SFSco::MF_Multiply;

		case matrixf_t_multiply4_PROC_ID:
			return SFSco::MF_TransposeMultiply44;
		case matrixf_multiply4_PROC_ID:
			return SFSco::MF_Multiply44;
		case matrixf_t_multiply2_PROC_ID:
			return SFSco::MF_TransposeMultiply22;
		case matrixf_multiply2_PROC_ID:
			return SFSco::MF_Multiply22;

		case matrixf_i_multiply_PROC_ID:
			return SFSco::MF_InverseMultiply;
		case matrixf_i_multiply4_PROC_ID:
			return SFSco::MF_InverseMultiply44;
		case matrixf_i_multiply3_PROC_ID:
			return SFSco::MF_InverseMultiply33;
		case matrixf_i_multiply2_PROC_ID:
			return SFSco::MF_InverseMultiply22;

		case matrixf_add_PROC_ID:
			return SFSco::MF_AddMatrix;
		case matrixf_sub_PROC_ID:
			return SFSco::MF_SubMatrix;

		case matrixf_i_check_PROC_ID:
			return SFSco::MF_IdentityCheck;
		

		case vectorf_negate_PROC_ID:
			return SFSco::VF_Negate;
		case vectorf_scale_PROC_ID:
			return SFSco::VF_Scale;
		case vectorf_add_PROC_ID:
			return SFSco::VF_AddVector;
		case vectorf_sub_PROC_ID:
			return SFSco::VF_SubVector;

		case vectorf_sqrt_PROC_ID:
			return SFSco::VF_Sqrt;
		case vectorf_c_multiply_PROC_ID:
			return SFSco::VF_ComponentMultiply;
		case vectorf_sup_norm_PROC_ID:
			return SFSco::VF_SupNorm;
		case vectorf_e1_norm_PROC_ID:
			return SFSco::VF_E1Norm;
		case vectorf_dot_product_PROC_ID:
			return SFSco::VF_DotProduct;


		case float_permute_PROC_ID:
			return SFSco::FloatPermute;
		case matrixf_from_b_PROC_ID:
			return SFSco::MF_FromBuffer;
*/

		case forward_aes128_PROC_ID:
			return SFSco::forward_aes128;
		case inverse_aes128_PROC_ID:
			return SFSco::inverse_aes128;
		case forward_aes192_PROC_ID:
			return SFSco::forward_aes192;
		case inverse_aes192_PROC_ID:
			return SFSco::inverse_aes192;
		case forward_aes256_PROC_ID:
			return SFSco::forward_aes256;
		case inverse_aes256_PROC_ID:
			return SFSco::inverse_aes256;
		case forward_DES_PROC_ID:
			return SFSco::forward_des;
		case inverse_DES_PROC_ID:
			return SFSco::inverse_des;
		case forward_TDES_PROC_ID:
			return SFSco::forward_3des;
		case inverse_TDES_PROC_ID:
			return SFSco::inverse_3des;

		case sha256_PROC_ID:
			return SFSco::sha256;
		case sha512_PROC_ID:
			return SFSco::sha512;
		case md5_PROC_ID:
			return SFSco::md5;
		case CRC32_PROC_ID:
			return SFSco::CRC32;
		case CRC16_PROC_ID:
			return SFSco::CRC16;
		case ADLER32_PROC_ID:
			return SFSco::ADLER32;





		case udpp_forwarddata_PROC_ID:
			return SFSco::UDPP_ForwardData;
		case udpp_initialize_PROC_ID:
			return SFSco::UDPP_Initialize;
		case udpp_finalize_PROC_ID:
			return SFSco::UDPP_Finalize;
		case udpp_dialin_PROC_ID:
			return SFSco::UDPP_Connect;

/*
		case launcher_addinitializer_PROC_ID:
			return SFSco::Launcher_AddInitializer;
		case launcher_run_PROC_ID:
			return SFSco::Launcher_Run;

		case application_create_PROC_ID:
			return SFSco::Application_Create;
		case application_close_PROC_ID:
			return SFSco::Application_Close;
		case application_run_PROC_ID:
			return SFSco::Application_Run;
*/

		case application_get_time_PROC_ID:
			return SFSco::GetTimeStamp;
		case application_get_ucount_PROC_ID:
			return SFSco::GetUCount;
		case application_timercal_PROC_ID:
			return SFSco::TimerCal;
		case application_resetcal_PROC_ID:
			return SFSco::ResetCal;


		case opengl_initialize_PROC_ID:
			return SFSco::OpenGL_Initialize;
		case opengl_getversion_PROC_ID:
			return SFSco::OpenGL_Version;
		case opengl_xyshift_PROC_ID:
			return SFSco::OpenGL_XYShift;
		case opengl_addie_PROC_ID:
			return SFSco::AddIE;
		case opengl_hdrstr_PROC_ID:
			return SFSco::GetHeaderStr;
		case opengl_infostr_PROC_ID:
			return SFSco::GetInfoStr;


		case event_molupdate_PROC_ID:
			return SFSco::Event_MOLUpdate;
		case event_removegate_PROC_ID:
			return SFSco::Event_RemoveGate;
		case event_cancel_PROC_ID:
			return SFSco::Event_Cancel;
		case event_setgate_PROC_ID:
			return SFSco::Event_SetGate;
		case event_setdrain_PROC_ID:
			return SFSco::Event_CreateDrain;
		case event_shutdown_PROC_ID:
			return SFSco::Event_ShutDown;

		
/*
		case font_create_PROC_ID:
			return SFSco::Font_Create;
		case font_irange_PROC_ID:
			return SFSco::Font_InitializeRange;
		case font_getbox_PROC_ID:
			return SFSco::Font_GetMetrixBox;
*/
		case font_initialize_PROC_ID:
			return SFSco::Font_Initialize;
		case font_finalize_PROC_ID:
			return SFSco::Font_Finalize;


		case image_initialize_PROC_ID:
			return SFSco::Image_Initialize;
		case image_finalize_PROC_ID:
			return SFSco::Image_Finalize;
		case image_load_file_PROC_ID:
			return SFSco::Image_LoadFile;
		case image_load_data_PROC_ID:
			return SFSco::Image_LoadData;
		case image_get_box_PROC_ID:
			return SFSco::Image_GetWH;
		case image_dither_PROC_ID:
			return SFSco::Image_Dither;
		case image_dither_i_PROC_ID:
			return SFSco::Image_Dither_I;
		case image_nearest_PROC_ID:
			return SFSco::Image_Nearest;
		case image_saveas_PROC_ID:
			return SFSco::Image_SaveAs;
		case image_pfilter_on_PROC_ID:
			return SFSco::Image_EnablePostFilter;
		case image_pfilter_off_PROC_ID:
			return SFSco::Image_DisablePostFilter;


		case jpg_applyqts_PROC_ID:
			return SFSco::JPG_ApplyQTs;
		case jpg_llstats_PROC_ID:
			return SFSco::JPG_LLStats;


		case png_copycolor_4_PROC_ID:
			return SFSco::PNG_CopyColor_4;
		case png_filter_line_PROC_ID:
			return SFSco::PNG_FilterLine;


		case image_YUY22RGBA_PROC_ID:
			return SFSco::YUY22RGBA;
		case image_I4202RGBA_PROC_ID:
			return SFSco::I4202RGBA;
		case image_BGR2RGBA_PROC_ID:
			return SFSco::BGR2RGBA;
/*
		case video_sfv_dframe_PROC_ID:
			return SFSco::SFV_DeltaFrame;
		case video_sfv_applyqt_PROC_ID:
			return SFSco::SFV_ApplyQT;
*/

		case video_source_init_PROC_ID:
			return SFSco::VideoSource_Initialize;
		case video_source_fin_PROC_ID:
			return SFSco::VideoSource_Finalize;

		case video_cap_quality_PROC_ID:
			return SFSco::VideoSource_SetQuality;

		case video_drain_create_PROC_ID:
			return SFSco::VideoDrain_Create;
		case video_drain_attach_PROC_ID:
			return SFSco::VideoDrain_Attach;
		case video_drain_detach_PROC_ID:
			return SFSco::VideoDrain_Detach;

		case file_initialize_PROC_ID:
			return SFSco::File_Initialize;
		case file_finalize_PROC_ID:
			return SFSco::File_Finalize;



		case audio_device_init_PROC_ID:
			return SFSco::AudioDevice_Initialize;
		case audio_device_fin_PROC_ID:
			return SFSco::AudioDevice_Finalize;



		case allocator_IFACE_ID:
			return SFSco::mem_mgr;
		case lallocator_IFACE_ID:
			return SFSco::large_mem_mgr;
		case xlallocator_IFACE_ID:
			return SFSco::xl_mem_mgr;
		case fallocator_IFACE_ID:
			return SFSco::file_cache;


		case opengl_core_IFACE_ID:
			return dynamic_cast<SFSco::IOpenGL *>(new OpenGL::Core());

		case main_menu_IFACE_ID:
			return dynamic_cast<SFSco::IGLApp *>(new Apps::MainMenu());
		case x_files_IFACE_ID:
			return dynamic_cast<SFSco::IGLApp *>(new Apps::XFiles());
		case x_pic_IFACE_ID:
			return dynamic_cast<SFSco::IGLApp *>(new Apps::XPic());
		case x_video_IFACE_ID:
			return dynamic_cast<SFSco::IGLApp *>(new Apps::XVideo());
		case x_audio_IFACE_ID:
			return dynamic_cast<SFSco::IGLApp *>(new Apps::XAudio());


	}

	return 0;
}

__declspec(dllexport) UI_64 InitializeLocalObjects() {
	wchar_t mem_name[] = {7, L'm', L'e', L'm', L' ', L'm', L'g', L'r'};
	wchar_t lmem_name[] = {13, L'l', L'a', L'r', L'g', L'e', L' ', L'm', L'e', L'm', L' ', L'm', L'g', L'r'};
	wchar_t xlmem_name[] = {10, L'x', L'l', L' ', L'm', L'e', L'm', L' ', L'm', L'g', L'r'};
	wchar_t cfile_name[] = {9, L'c', L'a', L'c', L'h', L'e', L' ', L'm', L'g', L'r'};

	UI_64 result(0);
	UI_64 h_val(-1);

	Calc::Initialize(512);

	if (result = Memory::Chain::Initialize()) return result;
	

	if (memacha_pool = new Memory::Allocator(0)) {
		if (result = memacha_pool->Init(new Memory::Buffer(), 0x00100000, mem_name)) {
			delete memacha_pool;
			memacha_pool = 0;
		} else {
			SFSco::mem_mgr = memacha_pool;			
		}
	} else result = ERROR_OUTOFMEMORY;

	if (!result) {
		if (memacha_lool = new Memory::Allocator(0)) {
			if (result = memacha_lool->Init(new Memory::Buffer(), 0x00100000, lmem_name)) {
				delete memacha_lool;
				memacha_lool = 0;
			} else {
				SFSco::large_mem_mgr = memacha_lool;
				OpenGL::Core::LoadStrings();
			}
		} else result = ERROR_OUTOFMEMORY;

		if (!result) {
			if (memacha_xlool = new Memory::Allocator(0)) {
				if (result = memacha_xlool->Init(new Memory::Buffer(), 0x08000000, xlmem_name)) {
					delete memacha_xlool;
					memacha_xlool = 0;
				} else {
					SFSco::xl_mem_mgr = memacha_xlool;
				}
			} else result = ERROR_OUTOFMEMORY;

			if (!result) {
				if (file_pool = new Memory::Allocator(5)) {
					if (result = file_pool->Init(new Memory::FileBuffer(), 0x00010000, cfile_name, L"cache_file")) {
						delete file_pool;
						file_pool = 0;
					} else {
						SFSco::file_cache = file_pool;
					}
				} else result = ERROR_OUTOFMEMORY;
			}
		}
	}

	if (!result) {		
		result = WSAStartup(0x202, &local_wsaData);
						
	}

	if (!result) {
		result = Control::Event::Initialize();

	}


	if (!result) {	
		CreateDirectory(L"temp_d", 0);

		h_val = file_pool->GetFirst(SFSco::GlobalSet::type_id);
		if (h_val != -1) {
			SFSco::Object global_obj;
			global_obj.Set(file_pool, h_val);

			if (SFSco::GlobalSet * g_ptr = reinterpret_cast<SFSco::GlobalSet *>(global_obj.Acquire())) {
				SFSco::program_cfg.audio_default_volume = g_ptr->audio_default_volume;
				SFSco::program_cfg.screenshot_quality = g_ptr->screenshot_quality;

				SFSco::program_cfg.media_program_language = g_ptr->media_program_language;
				SFSco::program_cfg.media_start_on_load = g_ptr->media_start_on_load;				
				SFSco::program_cfg.media_repeat = g_ptr->media_repeat;

				SFSco::program_cfg.screenshot_type = g_ptr->screenshot_type;
				
				SFSco::program_cfg.win_cfg = g_ptr->win_cfg;

				h_val = 0;
				global_obj.Release();
			}
		}

		if (h_val) SFSco::program_cfg.Default();
	}
	

	return result;
}

__declspec(dllexport) void FinalizeLocalObjects() {
	UI_64 h_val(-1);
	SFSco::Object global_obj;

	Control::Event::Finalize();

	WSACleanup();
	


	if (memacha_lool) delete memacha_lool;
	if (memacha_pool) delete memacha_pool;
	if (memacha_xlool) delete memacha_xlool;

	if (file_pool) {
		h_val = file_pool->GetFirst(SFSco::GlobalSet::type_id);
				
		if (h_val != -1) global_obj.Set(file_pool, h_val);
		else global_obj.New(SFSco::GlobalSet::type_id, sizeof(SFSco::GlobalSet), file_pool);
		
		if (SFSco::GlobalSet * g_ptr = reinterpret_cast<SFSco::GlobalSet *>(global_obj.Acquire())) {
			g_ptr->audio_default_volume = SFSco::program_cfg.audio_default_volume;
			g_ptr->screenshot_quality = SFSco::program_cfg.screenshot_quality;

			g_ptr->media_program_language = SFSco::program_cfg.media_program_language;
			g_ptr->media_start_on_load = SFSco::program_cfg.media_start_on_load;			
			g_ptr->media_repeat = SFSco::program_cfg.media_repeat;

			g_ptr->screenshot_type = SFSco::program_cfg.screenshot_type;

			g_ptr->win_cfg = SFSco::program_cfg.win_cfg;

			global_obj.Release();
		}
		
		delete file_pool;
	}

	Memory::Chain::Finalize();

	memacha_xlool = 0;
	memacha_lool = 0;
	memacha_pool = 0;
	file_pool = 0;
}



__declspec(dllexport) void GetProgramBox(SFSco::GlobalSet::WinCfg & w_cfg) {
	w_cfg = SFSco::program_cfg.win_cfg;
}

__declspec(dllexport) void SetProgramBox(const SFSco::GlobalSet::WinCfg & r_cfg) {
	SFSco::program_cfg.win_cfg = r_cfg;
}

