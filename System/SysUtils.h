/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once
	
	typedef unsigned long long UI_64;
	typedef long long I_64;

	extern "C"
	namespace System {

		UI_64 __fastcall LockedInc_64(UI_64 *);
		UI_64 __fastcall LockedDec_64(UI_64 *);

		UI_64 __fastcall LockedOr_64(UI_64 *,UI_64);
		UI_64 __fastcall LockedXor_64(UI_64 *,UI_64);
		UI_64 __fastcall LockedSub_64(UI_64 *,UI_64);
		UI_64 __fastcall LockedAdd_64(UI_64 *,UI_64);
	
		UI_64 __fastcall SpinWait_U64(UI_64 *);
		UI_64 __fastcall SpinWait_U64U(UI_64 *, UI_64);

		__int64 __fastcall SpinWait_I64(__int64 *);
		__int64 __fastcall SpinWait_I64I(__int64 *, __int64);

		unsigned int __fastcall SpinWait_U32(unsigned int *);
		unsigned int __fastcall SpinWait_U32U(unsigned int *, unsigned int);

		int __fastcall SpinWait_I32(int *);
		int __fastcall SpinWait_I32I(int *, int);


//		extern "C" __int64 LockedInc_I64(__int64 *);

		unsigned int __fastcall LockedInc_32(unsigned int *);
		unsigned int __fastcall LockedDec_32(unsigned int *);
		unsigned int __fastcall LockedOr_32(unsigned int *,unsigned int);
		unsigned int __fastcall LockedXor_32(unsigned int *,unsigned int);
		unsigned int __fastcall LockedSub_32(unsigned int *,unsigned int);
		unsigned int __fastcall LockedAdd_32(unsigned int *,unsigned int);
		


		unsigned short __fastcall LockedInc_16(unsigned short *);
		unsigned short __fastcall LockedDec_16(unsigned short *);
		unsigned short __fastcall LockedOr_16(unsigned short *,unsigned short);
		unsigned short __fastcall LockedXor_16(unsigned short *,unsigned short);
		
		short __fastcall SpinWait_I16(short *);
		unsigned short __fastcall SpinWait_U16(unsigned short *, unsigned short);

		unsigned int __fastcall BitCount_64(const UI_64);
		unsigned int __fastcall BitCount_32(const unsigned int);
		unsigned int __fastcall BitCount_16(const unsigned short);

		unsigned int __fastcall BitIndexLow_64(const UI_64);
		unsigned int __fastcall BitIndexLow_32(const unsigned int);
		unsigned int __fastcall BitIndexLow_16(const unsigned short);

		unsigned int __fastcall BitIndexHigh_64(const UI_64);
		unsigned int __fastcall BitIndexHigh_32(const unsigned int);
		unsigned int __fastcall BitIndexHigh_16(const unsigned short);

		unsigned short __fastcall BSWAP_16(unsigned short &);
//		extern "C" short __fastcall BSWAP_I16(short &);
		unsigned int __fastcall BSWAP_32(unsigned int &);
		UI_64 __fastcall BSWAP_64(UI_64 &);

		void __fastcall Swap_16(void *, unsigned short);
		void __fastcall Swap_32(void *, unsigned int);
		void __fastcall Swap_64(void *, unsigned int);

		void __fastcall XMMPush(void *);
		void __fastcall XMMPop(void *);

//		void __fastcall SFence();
		void __fastcall Serialize();


		unsigned int FormatErrorMessage(const wchar_t *,unsigned int);

		void MemoryFill_SSE3(void * const destination, UI_64 byte_count, __int64 pattern_qword_0, __int64 pattern_qword_1);
		void MemoryCopy_SSE3(void * const destination, const void * source, UI_64 byte_count); // must be a multiple of 64

		void MemoryFill(void * const destination, UI_64 byte_count, unsigned int pattern_dword);
		void MemoryCopy(void * const destination, const void * source, UI_64 byte_count);

		unsigned int Gen32(unsigned int * = 0);
		UI_64 Gen64(UI_64 * = 0);

		UI_64 __fastcall GetTime();
		void __fastcall TimerCal(UI_64);
		void __fastcall ResetCal(UI_64);
		__int64 __fastcall GetUCount(I_64, I_64);

	}

	extern "C"
	namespace Strings {
		void __fastcall A2W(wchar_t *); // 32 characters max, 64 byte space required
		unsigned int FormatNumberString(wchar_t * str, unsigned int s_len, unsigned int num_tot, wchar_t e_char);
		UI_64 FormatTimeVal(UI_64);

		unsigned int MyEscape(void * dest, const void * src, unsigned int size);
		unsigned int MyEscape_SSE3(void * dest, const void * src, unsigned int size); // escapes slashes and quotes

		int IntToCStr(char * dest, int ival, int padding_on); // if padding on is not zero zeros are appended to the result string until 11-charecter string is formed

		int UIntToCStr(char * dest, unsigned int uival, int padding_on);
		int Int64ToCStr(char * dest, __int64 val, int padding_on); // amps below 1000000000000000000 (10^18) are accepted, if padding on is not zero zeros are appended to the result string until 18-charecter string is formed

		int TimeToCStr(char * dest, UI_64); // time in tenth of millisecond

		int IntToCStr_SSSE3(char * dest, int ival, int padding_on);
	
		int UIntToCStr_SSSE3(char * dest, unsigned int uival, int padding_on);
		int Int64ToCStr_SSSE3(char * dest, __int64 val, int padding_on); // amps below 1000000000000000000 (10^18) are accepted 
		int UInt64ToCStr_SSSE3(char * dest, UI_64 val, int padding_on); // amps below 1000000000000000000 (10^18) are accepted 

		int FloatToCStr_SSSE3(char * dest, float val, int dec_places);
		int DoubleToCStr_SSSE3(char * dest, double val, int dec_places);

		int FloatToCStr_SSE3(char * dest, float val, int dec_places);
		int DoubleToCStr_SSE3(char * dest, double val, int dec_places);


		void CStrToInt(const char * src, int & result);
		void CStrToInt64(const char * src, __int64 & result);

		void CStrToDouble(const char * src, double & result);
		void CStrToFloat(const char * src, float & result);

		void StringToHexString_SSE3(void * dest, const void * src, UI_64 src_size, unsigned int wt); //
		void StringToHexString(void * dest, const void * src, UI_64 src_size, unsigned int wt);

		void HexStringToString_SSE3(void * dest, const void * src, UI_64 src_size); //
		void HexStringToString(void * dest, const void * src, UI_64 src_size);


		char * SearchAStr(const char * source, const char * sblock);
		wchar_t * SearchWStr(const wchar_t * source, const wchar_t * sblock);

		int CmpAStr(const wchar_t * s1, unsigned int s1_len, const wchar_t * s2, unsigned int s2_len);
		int CmpWStr(const wchar_t * s1, unsigned int s1_len, const wchar_t * s2, unsigned int s2_len);

		void * SearchStr_SSE3(const void * source, unsigned int ssize, const void * sblock, unsigned int sbsize);
		unsigned int ReplaceStr_SSE3(void * source, unsigned int ssize, const void * sblock, unsigned int sbsize, const void * rblock, unsigned int rbsize);

		void * SearchBlock(const void * source, UI_64, const void * sblock, UI_64);










	}


	/*

		TEST rdx, rdx
		JZ length_1
			MOV rdi, rcx
			MOV rsi, rcx

			XOR rcx, rcx
			REPNZ SCASW

			NEG rcx
			MOV rdx, rcx
			MOV rcx, rdi
		length_1:


		TEST r9, r9
		JZ length_2
			MOV rdi, r8
			MOV rsi, r8

			XOR rcx, rcx
			REPNZ SCASW

			NEG rcx
			MOV r9, rcx
			MOV r8, rdi
		length_2:

	*/

	/*

	
	CMP r9, 10
	JB small_a
		SHL r8, 1
		RCL r9, 1
		SHL r8, 1
		RCL r9, 1
		SHL r8, 1
		RCL r9, 1
		SHL r8, 1
		RCL r9, 1

		SHL r10, 1
		RCL r11, 1
		SHL r10, 1
		RCL r11, 1
		SHL r10, 1
		RCL r11, 1
		SHL r10, 1
		RCL r11, 1
		

		MOV rcx, 1000

		XOR rdx, rdx
		MOV rax, r11
		MUL rcx

		MOV r11, rax

		XOR rdx, rdx
		MOV rax, r10
		MUL rcx

		ADD r11, rdx


		XOR rdx, rdx
		MOV rax, r11
		
		DIV r9

		MOV [current_time], rax
		RET

align 16
		small_a:
			SHR r9, 1
			RCR r8, 1

			SHR r11, 1
			RCR r10, 1

		AND r11, r11
		JNZ small_a


		XOR rdx, rdx
		MOV rax, r10
		DIV r8

		MOV rcx, 1000
		MUL rcx

	*/


