/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "System\Memory.h"
#include "System\SysUtils.h"
#include "Securitat\Crypter.h"


#define PARAGRAPH		63
#define MIN_TABLE_COUNT	128
#define MIN_ESLOT_COUNT	128


#pragma warning(disable:4244)

// Chain ======================================================================================================================================
/*
	jump table (offsets)
				0: first
				1: last

				2..: jumps

	empty slot (32) size descend
				0: size
				1: offset
				2: size
				3: offset
				...

*/

__int64 * Memory::Chain::lock_ptr = 0;
__int64 Memory::Chain::lock_count = 0;

UI_64 Memory::Chain::Initialize() {
	UI_64 result(0);

	lock_ptr = reinterpret_cast<__int64 *>(VirtualAlloc(0, 65536, MEM_COMMIT, PAGE_READWRITE));

	if (!lock_ptr) result = GetLastError();

	return result;
}

void Memory::Chain::Finalize() {
	if (lock_ptr) VirtualFree(lock_ptr, 0, MEM_RELEASE);
	lock_ptr = 0;
}

__int64 * Memory::Chain::IncLock(const wchar_t * desc_ptr, I_64 * i_val) {
	__int64 id_val = InterlockedIncrement64(&lock_count) - 1;
	__int64 * result(lock_ptr + (id_val<<7));

	if (i_val) i_val[0] = id_val;
	result[2] = id_val;

	if (desc_ptr) {
		for (wchar_t i(1);i<=desc_ptr[0];i++) reinterpret_cast<wchar_t *>(result + 4)[i] = desc_ptr[i];
	}

	return result;
}

void Memory::Chain::RegisterThread(I_64 * c_ptr) {
	I_64 * thread_itable(lock_ptr + 7168);
	I_64 ctid = GetCurrentThreadId(), slid(0);
	
	for (slid = 1;slid <= thread_itable[0];slid++) {
		if (thread_itable[2*slid] == 0) {
			break;
		}
	}

	if ((slid+1) > thread_itable[0]) slid = InterlockedIncrement64(thread_itable);

	for (unsigned int i(0);i<64;i++) c_ptr[i] = 0;

	thread_itable[2*slid] = ctid;
	thread_itable[2*slid + 1] = reinterpret_cast<I_64>(c_ptr);

	for (unsigned int i(0);i<2*MAX_POOL_COUNT;i++) c_ptr[i] = 0;
}

void Memory::Chain::UnregisterThread() {
	I_64 * thread_itable(lock_ptr + 7168);
	I_64 ctid = GetCurrentThreadId();
	
	for (I_64 i(1);i<=thread_itable[0];i++) {
		if (thread_itable[2*i] == ctid) {
			thread_itable[2*i] = 0;
			thread_itable[2*i + 1] = 0;
			break;
		}
	}
}

Memory::Chain::Chain(UI_64 x_t) : service_flags(0), _eval(0), id_val(0), ptr_lock(0), _buffer(0) {
	System::MemoryFill(&chain_info, sizeof(ChainInfo), 0);
	
	if (x_t & SafeChain) chain_info.cid = 0x4E69414843534653; // SFSCHAiN
	else chain_info.cid = 0x4E49414843534653; // SFSCHAIN

	chain_info.ceiling = sizeof(ChainInfo);

	service_flags = (x_t & 7);	
}

Memory::Chain::~Chain() {	
	
	unsigned int aes_key[10];
	
	if (MapTableEntry * ma_ptr = chain_info.map_table) {
		ma_ptr[2].view_ptr = 0;
		ma_ptr[1].view_ptr = 0;

		GetSystemTimeAsFileTime(reinterpret_cast<LPFILETIME>(&chain_info.map_table));

		Crypt::sha256_SSSE3(ma_ptr[2].reserved, reinterpret_cast<char *>(chain_info.free_slot), ma_ptr[2].size, 0x00000001);
		Crypt::sha256_SSSE3(ma_ptr[1].reserved, reinterpret_cast<char *>(chain_info.empty_slot), ma_ptr[1].size, 0x00000001);

		Crypt::sha512_SSSE3(chain_info.sha512, reinterpret_cast<char *>(&chain_info), 48, 0x00000001);
		Crypt::sha512_SSSE3(chain_info.sha512, reinterpret_cast<char *>(ma_ptr), ma_ptr[0].size, 0x80000001);

		if (service_flags & SafeChain) {
			for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(ma_ptr[2].reserved) + 11)[i];
			for (unsigned int i(4);i<10;i++) aes_key[i] = 0;
			Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(chain_info.free_slot), ma_ptr[2].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(chain_info.free_slot));

			for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(ma_ptr[1].reserved) + 11)[i];
			for (unsigned int i(4);i<10;i++) aes_key[i] = 0;		
			Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(chain_info.empty_slot), ma_ptr[1].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(chain_info.empty_slot));

			for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.sha512) + 37)[i];
			for (unsigned int i(4);i<10;i++) aes_key[i] = 0;
			Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(ma_ptr), ma_ptr[0].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(ma_ptr));
		}

		_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.free_slot)[0]);
		_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.empty_slot)[0]);
		_buffer->ReleasePointer(reinterpret_cast<void **>(&ma_ptr)[0]);

		_buffer->SetSomeVal(&chain_info, sizeof(ChainInfo), 0);
	}

	if (_buffer) delete _buffer;
}

void Memory::Chain::ServiceLock() {
	I_64 * thread_itable(lock_ptr + 7168), * skip_vptr(0);
	I_64 ctid = GetCurrentThreadId();

	if (InterlockedIncrement64(ptr_lock) > 1) { // service lock
		for (I_64 i(1);i<=thread_itable[0];i++) {
			if (thread_itable[2*i] == ctid) {
				skip_vptr = reinterpret_cast<I_64 *>(thread_itable[2*i + 1]);
				break;
			}
		}

		if ((skip_vptr) && (ptr_lock[0] > 1)) { // this mechanism is used to service different threads in a sequence
			skip_vptr[2*id_val + 1] = 1;
			System::SpinWait_I64(skip_vptr + 2*id_val + 1);
		}	
	}	
}

void Memory::Chain::ServiceUnlock() {
	I_64 * thread_itable(lock_ptr + 7168), * skip_vptr(0);

	InterlockedDecrement64(ptr_lock);

	for (I_64 i(1);i<=thread_itable[0];i++) {
		if (skip_vptr = reinterpret_cast<I_64 *>(thread_itable[2*i + 1])) {
			if (skip_vptr[2*id_val + 1]) {
				skip_vptr[2*id_val + 1] = 0;
				break;
			}
		}
	}		
}


void Memory::Chain::Lock() {
	I_64 * thread_itable(lock_ptr + 7168);
	I_64 ctid = GetCurrentThreadId(), skip_val(0);
	
	for (I_64 i(1);i<=thread_itable[0];i++) {
		if (thread_itable[2*i] == ctid) {
			if (thread_itable[2*i + 1]) skip_val = reinterpret_cast<I_64 *>(thread_itable[2*i + 1])[2*id_val]++;
			break;
		}
	}

	if (skip_val == 0) while (System::SpinWait_I64(ptr_lock)>1);

	InterlockedIncrement64(ptr_lock + 32);
}

void Memory::Chain::Unlock() {
	I_64 * thread_itable(lock_ptr + 7168);
	I_64 ctid = GetCurrentThreadId();
	
	for (I_64 i(1);i<=thread_itable[0];i++) {
		if (thread_itable[2*i] == ctid) {
			if (thread_itable[2*i + 1]) reinterpret_cast<I_64 *>(thread_itable[2*i + 1])[2*id_val]--;
			break;
		}
	}

	InterlockedDecrement64(ptr_lock + 32);
}

void Memory::Chain::SpinWait() {
	I_64 * thread_itable(lock_ptr + 7168);
	I_64 ctid = GetCurrentThreadId(), skip_val(0);
	
	for (I_64 i(1);i<=thread_itable[0];i++) {
		if (thread_itable[2*i] == ctid) {
			if (thread_itable[2*i + 1]) skip_val = reinterpret_cast<I_64 *>(thread_itable[2*i + 1])[2*id_val];
			break;
		}
	}

	if (!skip_val) while(System::SpinWait_I64(ptr_lock)>1);
}

UI_64 Memory::Chain::BufferSize(UI_64 siz) {

	if ((chain_info.ceiling + siz) > _buffer->GetSize()) {
		while (System::SpinWait_I64(ptr_lock + 32)>1); // wait until all references are released

		if (!_eval) {
			_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.free_slot)[0]);
			_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.empty_slot)[0]);
			_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.map_table)[0]);

		
			if (_buffer->ReSize(chain_info.ceiling + siz)) {
				chain_info.map_table = reinterpret_cast<MapTableEntry *>(_buffer->GetPointer(chain_info.map_offset, chain_info.map_top*sizeof(MapTableEntry)));				

				if (chain_info.map_table) {
					chain_info.empty_slot = reinterpret_cast<UI_64 *>(_buffer->GetPointer(chain_info.map_table[1].offset, chain_info.map_table[1].size));
	
					chain_info.free_slot = reinterpret_cast<UI_64 *>(_buffer->GetPointer(chain_info.map_table[2].offset, chain_info.map_table[2].size));
				}
			} else {
				_eval = 0x41484353;
				_eval <<= 32;
				_eval |= _buffer->BugCheck();
			}
		}
	}

	return _eval;
}



UI_64 Memory::Chain::Init(Buffer * c_buf, UI_64 sz, const wchar_t * c_n, const wchar_t * f_n) {
	UI_64 sha64_vals[16];
	unsigned int sha32_vals[16];
	unsigned int * tmps(0);
	UI_64 result(0);

	if (ptr_lock) return 0;

	ptr_lock = IncLock(c_n, &id_val);
	
	if (f_n) service_flags |= FChain;

	if (_buffer = c_buf)
	switch (_buffer->Init(sz, f_n)) {
		case 1: // new created
			if (InsertLink(0, 0) != -1) {
				if (chain_info.map_table) {
					
					if (InsertLink((MIN_ESLOT_COUNT+1)*sizeof(UI_64), empty_slot_tid) != -1) {						
						chain_info.map_table[1].view_ptr = _buffer->GetPointer(chain_info.map_table[1].offset, chain_info.map_table[1].size);
						chain_info.empty_slot = reinterpret_cast<UI_64 *>(chain_info.map_table[1].view_ptr);

						if (InsertLink((MIN_ESLOT_COUNT+1)*2*sizeof(UI_64), free_slot_tid) != -1) {
							chain_info.map_table[2].view_ptr = _buffer->GetPointer(chain_info.map_table[2].offset, chain_info.map_table[2].size);
							chain_info.free_slot = reinterpret_cast<UI_64 *>(chain_info.map_table[2].view_ptr);
						}
					}

					if ((chain_info.empty_slot) && (chain_info.free_slot)) {
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[0] = 0;
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[1] = MIN_ESLOT_COUNT;
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[2] = 0;
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[3] = 0;

					
						reinterpret_cast<unsigned int *>(chain_info.empty_slot)[0] = 0;
						reinterpret_cast<unsigned int *>(chain_info.empty_slot)[1] = MIN_ESLOT_COUNT;
					}
				} else {
					result = 0x41484353;
					result <<= 32;
					result |= GetLastError();
				}
			} else {
				result = 0x41484345;
				result <<= 32;
				result |= 1;
			}

		break;
		case 2: // existing is opened
			// existing verify
			chain_info.cid = 0;
			_buffer->GetSomeVal(&chain_info, sizeof(ChainInfo), 0);
			Crypt::sha512_SSSE3(sha64_vals, reinterpret_cast<char *>(&chain_info), 48, 0x00000001);

			chain_info.map_table = 0;

			switch (chain_info.cid ^ 0x4E49414843534653) {
				case 0: // no crypt
					
				break;
				case 0x0020000000000000: // crypt
					service_flags |= SafeChain;
				break;
				default: // error
					result = 0x41484346;
					result <<= 32;
					result |= 1;
			}

			if (result == 0) {
				chain_info.map_table = reinterpret_cast<MapTableEntry *>(_buffer->GetPointer(chain_info.map_offset, chain_info.map_top*sizeof(MapTableEntry)));

				if (service_flags & SafeChain) {
					for (unsigned int i(0);i<4;i++) sha32_vals[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.sha512) + 37)[i];
					for (unsigned int i(4);i<10;i++) sha32_vals[i] = 0;

					Crypt::reverse_aes128_SSSE3(reinterpret_cast<char *>(chain_info.map_table), chain_info.map_top*sizeof(MapTableEntry), reinterpret_cast<char *>(sha32_vals), reinterpret_cast<char *>(chain_info.map_table));
				}

				Crypt::sha512_SSSE3(sha64_vals, reinterpret_cast<char *>(chain_info.map_table), chain_info.map_top*sizeof(MapTableEntry), 0x80000001);

				sha64_vals[8] = 0;
				for (unsigned int i(0);i<8;i++) {
					sha64_vals[8] |= (chain_info.sha512[i] ^ sha64_vals[i]);
				}

				if (sha64_vals[8]) {					
					result = 0x41484346;
					result <<= 32;

					result |= 2;
				} else {
					chain_info.map_table[1].view_ptr = _buffer->GetPointer(chain_info.map_table[1].offset, chain_info.map_table[1].size);
					chain_info.empty_slot = reinterpret_cast<UI_64 *>(chain_info.map_table[1].view_ptr);

					chain_info.map_table[2].view_ptr = _buffer->GetPointer(chain_info.map_table[2].offset, chain_info.map_table[2].size);
					chain_info.free_slot = reinterpret_cast<UI_64 *>(chain_info.map_table[2].view_ptr);

					if (service_flags & SafeChain) {
						for (unsigned int i(0);i<4;i++) sha32_vals[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[1].reserved) + 11)[i];
						for (unsigned int i(4);i<10;i++) sha32_vals[i] = 0;
						Crypt::reverse_aes128_SSSE3(reinterpret_cast<char *>(chain_info.map_table[1].view_ptr), chain_info.map_table[1].size, reinterpret_cast<char *>(sha32_vals), reinterpret_cast<char *>(chain_info.map_table[1].view_ptr));

						for (unsigned int i(0);i<4;i++) sha32_vals[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[2].reserved) + 11)[i];
						for (unsigned int i(4);i<10;i++) sha32_vals[i] = 0;
						Crypt::reverse_aes128_SSSE3(reinterpret_cast<char *>(chain_info.map_table[2].view_ptr), chain_info.map_table[2].size, reinterpret_cast<char *>(sha32_vals), reinterpret_cast<char *>(chain_info.map_table[2].view_ptr));
					}

					sha32_vals[8] = 0;
					Crypt::sha256_SSSE3(sha32_vals, reinterpret_cast<char *>(chain_info.map_table[1].view_ptr), chain_info.map_table[1].size, 0x00000001);
					for (unsigned int i(0);i<8;i++) {
						sha32_vals[8] |= (sha32_vals[i] ^ chain_info.map_table[1].reserved[i]);
					}

					Crypt::sha256_SSSE3(sha32_vals, reinterpret_cast<char *>(chain_info.map_table[2].view_ptr), chain_info.map_table[2].size, 0x00000001);
					for (unsigned int i(0);i<8;i++) {
						sha32_vals[8] |= (sha32_vals[i] ^ chain_info.map_table[2].reserved[i]);
					}

					if (sha32_vals[8]) {
						result = 0x41484346;
						result <<= 32;

						result |= 3;
					}
				}
			}
		break;
	}

	if (result) {
		_buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.map_table)[0]);

		if (chain_info.empty_slot) _buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.empty_slot)[0]);
		if (chain_info.free_slot) _buffer->ReleasePointer(reinterpret_cast<void **>(&chain_info.free_slot)[0]);
	}

	return result;
}


void Memory::Chain::ShiftRightTo(UI_64 ref_id, UI_64 right_id) {
	if ((ref_id | right_id) != -1)
	if (service_flags & XChain) {
// extraction
		chain_info.map_table[chain_info.map_table[ref_id].x_left].x_right = chain_info.map_table[ref_id].x_right;
		chain_info.map_table[chain_info.map_table[ref_id].x_right].x_left = chain_info.map_table[ref_id].x_left;
		
// insertion
		chain_info.map_table[ref_id].x_left = chain_info.map_table[right_id].x_left;
		chain_info.map_table[ref_id].x_right = right_id;

		chain_info.map_table[chain_info.map_table[ref_id].x_left].x_right = ref_id;
		chain_info.map_table[right_id].x_left = ref_id;

	} else {
		chain_info.map_table[chain_info.map_table[ref_id].back_id].next_id = chain_info.map_table[ref_id].next_id;
		chain_info.map_table[chain_info.map_table[ref_id].next_id].back_id = chain_info.map_table[ref_id].back_id;
		
		chain_info.map_table[ref_id].next_id = chain_info.map_table[right_id].next_id;
		chain_info.map_table[ref_id].back_id = right_id;

		chain_info.map_table[chain_info.map_table[right_id].next_id].back_id = ref_id;
		chain_info.map_table[right_id].next_id = ref_id;
	}
}

void Memory::Chain::ShiftLeftTo(UI_64 ref_id, UI_64 left_id) {
	if ((ref_id | left_id) != -1)
	if (service_flags & XChain) {
		chain_info.map_table[chain_info.map_table[ref_id].x_right].x_left = chain_info.map_table[ref_id].x_left;
		chain_info.map_table[chain_info.map_table[ref_id].x_left].x_right = chain_info.map_table[ref_id].x_right;
				
		chain_info.map_table[ref_id].x_left = left_id;
		chain_info.map_table[ref_id].x_right = chain_info.map_table[left_id].x_right;

		chain_info.map_table[chain_info.map_table[left_id].x_right].x_left = ref_id;
		chain_info.map_table[left_id].x_right = ref_id;

	} else {
		chain_info.map_table[chain_info.map_table[ref_id].back_id].next_id = chain_info.map_table[ref_id].next_id;
		chain_info.map_table[chain_info.map_table[ref_id].next_id].back_id = chain_info.map_table[ref_id].back_id;

		chain_info.map_table[ref_id].next_id = left_id;
		chain_info.map_table[ref_id].back_id = chain_info.map_table[left_id].back_id;;

		chain_info.map_table[chain_info.map_table[left_id].next_id].back_id = ref_id;
		chain_info.map_table[left_id].next_id = ref_id;
	}
}


/*
	eptyslot layout:
		
		: reserved_link_header (size is ceiling, index is emptyslot runner)

		: released_link_header_0
		: released_link_header_1
		.......


	freeslot layout:
		UI_64: free_slot_runner
		UI_64: ceiling

		UI_64: size_1
		UI_64: offset_1

		UI_64: size_2
		UI_64: offset_2
*/

UI_64 Memory::Chain::FindaSlot(UI_64 & val) {
	UI_64 result(0);
	unsigned int gap_idx(-1);
	
	if (chain_info.free_slot) {
		result = -1;

		for (unsigned int i(1);i <= reinterpret_cast<unsigned int *>(chain_info.free_slot)[0];i++) { 
			if (val <= chain_info.free_slot[2*i]) {
				if (result > chain_info.free_slot[2*i]) {
					result = chain_info.free_slot[2*i];
					gap_idx = i;
				}
			}
		}

		if (gap_idx != -1) {
			result = chain_info.free_slot[2*gap_idx+1];

			if (chain_info.free_slot[2*gap_idx] > (val + PARAGRAPH)) {
				chain_info.free_slot[2*gap_idx] -= val;
				chain_info.free_slot[2*gap_idx + 1] += val;
				
			} else {
				val = chain_info.free_slot[2*gap_idx];
				if (gap_idx < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
					chain_info.free_slot[2*gap_idx] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
					chain_info.free_slot[2*gap_idx + 1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
				}

				reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
			}

			chain_info.free_slot[1] -= val;			
		} else {
			result = 0;
		}
	}

	if (!result) {
		BufferSize(val);
		
		if (!_eval) {
			result = chain_info.ceiling;
			chain_info.ceiling += val;
		}
/*
		if (_buffer->BugCheck()) {
			InterlockedDecrement64(ptr_lock);
			return -1;
		}			
*/
	}


	return result;
}

unsigned int Memory::Chain::AddFreeSlot(UI_64 link_id, void * m_ptr) {
	__declspec(align(16)) char xmm_vals[512];

	unsigned int sha32_vals[16];
	UI_64 aslot(0), bslot(0);

	if (m_ptr) {
		System::XMMPush(xmm_vals);
		Crypt::sha256_SSSE3(sha32_vals, reinterpret_cast<char *>(&chain_info), sizeof(ChainInfo), 0);
		for (unsigned int i(4);i<10;i++) sha32_vals[i] = 0;
		Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(m_ptr), chain_info.map_table[link_id].size, reinterpret_cast<char *>(sha32_vals), reinterpret_cast<char *>(m_ptr));
		System::XMMPop(xmm_vals);
	}

	if ((chain_info.map_table[link_id].offset + chain_info.map_table[link_id].size) != chain_info.ceiling) {
		if (chain_info.free_slot) {
		
			chain_info.free_slot[1] += chain_info.map_table[link_id].size;
		
			for (unsigned int i(1);i<=reinterpret_cast<unsigned int *>(chain_info.free_slot)[0];i++) {
				if (chain_info.free_slot[2*i+1] == (chain_info.map_table[link_id].offset + chain_info.map_table[link_id].size)) bslot = i;
				if ((chain_info.free_slot[2*i+1] + chain_info.free_slot[2*i]) == chain_info.map_table[link_id].offset) aslot = i;
			}

			if (aslot || bslot) {
				if (aslot) {
					chain_info.free_slot[2*aslot] += chain_info.map_table[link_id].size;
					if (bslot) {
						chain_info.free_slot[2*aslot] += chain_info.free_slot[2*bslot];

						if (bslot < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
							chain_info.free_slot[2*bslot] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
							chain_info.free_slot[2*bslot+1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
						}
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
					}
				} else {
					chain_info.free_slot[2*bslot] += chain_info.map_table[link_id].size;
					chain_info.free_slot[2*bslot+1] = chain_info.map_table[link_id].offset;

				}
			} else {
				reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]++;

				chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]] = chain_info.map_table[link_id].size;
				chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1] = chain_info.map_table[link_id].offset;

				if (reinterpret_cast<unsigned int *>(chain_info.free_slot)[0] >= reinterpret_cast<unsigned int *>(chain_info.free_slot)[1]) {
					SpecialResize(2);
				}
			}
		}
	} else {
		chain_info.ceiling -= chain_info.map_table[link_id].size;
	}

	return 0;

}


void Memory::Chain::AddEmptyLink(UI_64 link_id) {
	if (chain_info.empty_slot) {
		chain_info.empty_slot[++reinterpret_cast<unsigned int *>(chain_info.empty_slot)[0]] = link_id;
		
		chain_info.map_table[link_id].size = 0;
		chain_info.map_table[link_id].flags = Removed;

		// next back readjust

		chain_info.map_table[chain_info.map_table[link_id].back_id].next_id = chain_info.map_table[link_id].next_id;
		chain_info.map_table[chain_info.map_table[link_id].next_id].back_id = chain_info.map_table[link_id].back_id;

		chain_info.map_table[link_id].next_id = chain_info.map_table[link_id].back_id = link_id;


		if (service_flags & XChain) {
			if (chain_info.map_table[link_id].x_up != link_id) {
				chain_info.map_table[chain_info.map_table[link_id].x_up].x_down = chain_info.map_table[link_id].x_up;
				chain_info.map_table[link_id].x_up = link_id;
			}

			if (chain_info.map_table[link_id].x_down != link_id) {
				chain_info.map_table[chain_info.map_table[link_id].x_down].x_up = chain_info.map_table[link_id].x_down;
				chain_info.map_table[link_id].x_down = link_id;
			}

			if (chain_info.map_table[link_id].x_left != link_id) {
				chain_info.map_table[chain_info.map_table[link_id].x_left].x_right = chain_info.map_table[link_id].x_right;
				chain_info.map_table[chain_info.map_table[link_id].x_right].x_left = chain_info.map_table[link_id].x_left;

				chain_info.map_table[link_id].x_right = chain_info.map_table[link_id].x_left = link_id;
			}
		}		

		if (reinterpret_cast<unsigned int *>(chain_info.empty_slot)[0] >= reinterpret_cast<unsigned int *>(chain_info.empty_slot)[1]) {
			SpecialResize(1);
		}
	}
}

void Memory::Chain::LinkSpinWait(UI_64 id, int limit) {	
	if (chain_info.map_table[id].ref_count > limit++)	while(System::SpinWait_I32(&chain_info.map_table[id].ref_count) > limit);
}

int Memory::Chain::GetLink(void *& l_ptr, UI_64 l_id) {
	__declspec(align(16)) char xmm_vals[512];

	volatile int result = -1;
	unsigned int aes_key[10];

	l_ptr = 0;
	
	if ((l_id > 2) && (l_id < chain_info.length)) {
		Lock();

		if (chain_info.map_table) {
			if ((chain_info.map_table[l_id].flags & (Remove | Removed)) == 0) {				
				result = InterlockedIncrement((long *)&chain_info.map_table[l_id].ref_count);
				if (chain_info.map_table[l_id].flags & FreeBlock) System::SpinWait_I32I(&chain_info.map_table[l_id].flags, FreeBlock);

				if (!chain_info.map_table[l_id].view_ptr) {
					l_ptr = _buffer->GetPointer(chain_info.map_table[l_id].offset, chain_info.map_table[l_id].size);
				} else {					
					if ((chain_info.map_table[l_id].flags & XClusive) == 0) {
						l_ptr = chain_info.map_table[l_id].view_ptr;
					}
				}

				if (l_ptr) {
					if (service_flags & SafeChain) {
						System::XMMPush(xmm_vals);
						for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[l_id].reserved) + 11)[i];
						for (unsigned int i(4);i<10;i++) aes_key[i] = 0;

						Crypt::reverse_aes128_SSSE3(reinterpret_cast<char *>(l_ptr), chain_info.map_table[l_id].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(l_ptr));
						System::XMMPop(xmm_vals);
					}

					chain_info.map_table[l_id].view_ptr = l_ptr;
				}
			}
		}

		if (!l_ptr) {
			if (result >= 0) InterlockedDecrement((long *)&chain_info.map_table[l_id].ref_count);
			result = -1;

			Unlock();			
		}
	}


	return result;
}

int Memory::Chain::FreeLink(UI_64 id) {
	volatile int result(0);
	unsigned int aes_key[10];
	
	
	if ((id > 2) && (id < chain_info.length)) {
		if (chain_info.map_table[id].view_ptr) {
			InterlockedOr((long *)&chain_info.map_table[id].flags, FreeBlock);
			result = InterlockedDecrement((long *)&chain_info.map_table[id].ref_count);

			if (result <= 0) {
				if (result < 0) { // fuck
					result = 0;
					chain_info.map_table[id].ref_count = 0;
				}

				if (service_flags & SafeChain) {
					Crypt::sha256_SSSE3(chain_info.map_table[id].reserved, reinterpret_cast<char *>(chain_info.map_table[id].view_ptr), chain_info.map_table[id].size, 1);

					for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[id].reserved) + 11)[i];
					for (unsigned int i(4);i<10;i++) aes_key[i] = 0;

					Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(chain_info.map_table[id].view_ptr), chain_info.map_table[id].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(chain_info.map_table[id].view_ptr));
				}

				_buffer->ReleasePointer(chain_info.map_table[id].view_ptr);

				if (chain_info.map_table[id].flags & Remove) {
					ServiceLock();
	
					AddFreeSlot(id);
					AddEmptyLink(id);

					ServiceUnlock();

					result = -1;
				}
			}
			InterlockedAnd((long *)&chain_info.map_table[id].flags, ~FreeBlock);

			Unlock();						
		}
	}

	return result;
}


int Memory::Chain::RemoveLink(UI_64 indi) {	
	int result(0);
	if ((indi > 2) && (indi < chain_info.length)) {
		SpinWait();
		if ((chain_info.map_table[indi].flags & (Removed | Remove)) == 0) { //  && (chain_info.map_table[indi].size))
			if (chain_info.map_table[indi].ref_count <= 0) {
				ServiceLock();
	
				AddFreeSlot(indi);
				AddEmptyLink(indi);

				ServiceUnlock();

			} else {
				InterlockedOr((long *)&chain_info.map_table[indi].flags, Remove);
			}
		}
		result = chain_info.map_table[indi].ref_count;
	}

	return result;
}


UI_64 Memory::Chain::InsertLink(UI_64 isize, UI_64 tid, UI_64 container) {
	unsigned int aes_key[10], reserved_val[8];
	UI_64 result(-1);
	UI_64 offset(0);	

	void * l_ptr(0);

	int flg(0);


	ServiceLock();
	
	_eval = 0;
	
	if (chain_info.length) {
		isize = (isize + PARAGRAPH) & (~PARAGRAPH);

		if (chain_info.map_table) {
			offset = FindaSlot(isize);
			
			if (offset) {
				l_ptr = _buffer->GetPointer(offset, isize);
				System::MemoryFill_SSE3(l_ptr, isize, 0, 0);

				if ((service_flags & SafeChain) && (chain_info.length > 2)) {
					Crypt::sha256_SSSE3(reserved_val, reinterpret_cast<char *>(l_ptr), isize, 1);

					for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(reserved_val) + 11)[i];
					for (unsigned int i(4);i<10;i++) aes_key[i] = 0;

					Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(l_ptr), isize, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(l_ptr));
				}

				_buffer->ReleasePointer(l_ptr);

				if (chain_info.empty_slot) {
					if (reinterpret_cast<unsigned int *>(chain_info.empty_slot)[0]) {
						result = chain_info.empty_slot[reinterpret_cast<unsigned int *>(chain_info.empty_slot)[0]--];
					}
				}

				if (result == -1) {
					result = chain_info.length++;

					chain_info.map_table[chain_info.map_table[0].back_id].next_id = result;
					chain_info.map_table[result].back_id = chain_info.map_table[0].back_id;

					chain_info.map_table[result].next_id = 0;
					chain_info.map_table[0].back_id = result;

				}
			
				chain_info.map_table[result].offset = offset;
				chain_info.map_table[result].size = isize;

			}
		}

	} else {
		chain_info.map_offset = chain_info.ceiling; // sizeof(ChainInfo);
		chain_info.map_top = MIN_TABLE_COUNT;
		
		tid = map_table_tid;

		chain_info.length = 1;

		if (chain_info.map_table = reinterpret_cast<MapTableEntry *>(_buffer->GetPointer(chain_info.map_offset, chain_info.map_top*sizeof(MapTableEntry)))) {			
			System::MemoryFill_SSE3(chain_info.map_table, MIN_TABLE_COUNT*sizeof(MapTableEntry), 0, 0);

			chain_info.map_table[0].offset = chain_info.map_offset;
			chain_info.map_table[0].size = chain_info.map_top*sizeof(MapTableEntry);
		}

		chain_info.ceiling += chain_info.map_top*sizeof(MapTableEntry);

		result = 0;
	}


	if (result != -1) {		
		GetSystemTimeAsFileTime(reinterpret_cast<LPFILETIME>(&chain_info.map_table[result].access_mark));
		chain_info.map_table[result].x_left = chain_info.map_table[result].x_right = chain_info.map_table[result].x_up = chain_info.map_table[result].x_down = result;
	
		chain_info.map_table[result].ref_count = 0;


		if ((service_flags & XChain) && (chain_info.length > 2)) {
			if (container != -1) {
				if (chain_info.map_table[container].x_down != container) {
					container = chain_info.map_table[container].x_down;			

					chain_info.map_table[result].x_left = container;
					chain_info.map_table[result].x_right = chain_info.map_table[container].x_right;
					chain_info.map_table[container].x_right = result;
								
					chain_info.map_table[chain_info.map_table[result].x_right].x_left = result;

				} else {
					chain_info.map_table[result].x_up = container;
					chain_info.map_table[container].x_down = result;				
				}
			} else {
				if (result > 3) {
					chain_info.map_table[result].x_left = 3; // 
					chain_info.map_table[result].x_right = chain_info.map_table[3].x_right;
					chain_info.map_table[3].x_right = result;
					chain_info.map_table[chain_info.map_table[result].x_right].x_left = result;
				}
			}

			flg |= XType;
		}
		
	
		chain_info.map_table[result].flags = flg;
		chain_info.map_table[result].type = tid;
	}


	if (chain_info.length >= chain_info.map_top) {
		SpecialResize(0);
	}


	ServiceUnlock();

	return result;

}

UI_64 Memory::Chain::ResizeLink(UI_64 indi, UI_64 nsize) {
	unsigned int aes_key[10];
	UI_64 result(-1), deltas(0), free_it(0);
	UI_64 o_val(0);	

	if (indi < 3) return 0;


	ServiceLock();

	_eval = 0;

	if (chain_info.map_table) {
		if (chain_info.map_table[indi].size) {

			if ((I_64)nsize < 0) nsize = chain_info.map_table[indi].size - nsize;
			
			if (chain_info.map_table[indi].size < nsize) {
				
				nsize = (nsize + PARAGRAPH) & ~PARAGRAPH;
				deltas = nsize - chain_info.map_table[indi].size;

				for (unsigned int i(1);i<=reinterpret_cast<unsigned int *>(chain_info.free_slot)[0];i++) {
				
					if (chain_info.map_table[indi].offset == (chain_info.free_slot[2*i] + chain_info.free_slot[2*i+1])) {
						if (deltas <= chain_info.free_slot[2*i]) { // shift left
							if ((deltas + PARAGRAPH) < chain_info.free_slot[2*i]) {
								chain_info.free_slot[2*i] -= deltas;
							
								while(System::SpinWait_I32(&chain_info.map_table[indi].ref_count)>1);
								
								o_val = (chain_info.free_slot[2*i] + chain_info.free_slot[2*i+1]);
								
							} else {
							
								while(System::SpinWait_I32(&chain_info.map_table[indi].ref_count)>1);

								o_val = chain_info.free_slot[2*i+1];
								deltas = chain_info.free_slot[2*i];								

								if (i < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
									chain_info.free_slot[2*i] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
									chain_info.free_slot[2*i+1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
								}
								reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
							}

							chain_info.free_slot[1] -= deltas;
							
							break;
						}
					} else {
						if ((chain_info.map_table[indi].offset + chain_info.map_table[indi].size) == chain_info.free_slot[2*i+1]) {
							if (deltas <= chain_info.free_slot[2*i]) {
								if ((deltas + PARAGRAPH) < chain_info.free_slot[2*i]) {
									chain_info.free_slot[2*i] -= deltas;
									chain_info.free_slot[2*i+1] += deltas;
																
								} else {
									deltas = chain_info.free_slot[2*i];

									if (i < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
										chain_info.free_slot[2*i] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
										chain_info.free_slot[2*i+1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
									}
									reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
								}
							
								o_val = chain_info.map_table[indi].offset;
								chain_info.free_slot[1] -= deltas;
								
								break;
							}
						}
					}
				}

				if (!o_val) {
					o_val = FindaSlot(nsize);
					free_it = 1;
				} else {
					nsize = chain_info.map_table[indi].size + deltas;
				}

				if (o_val) {
					if (void * s_ptr = _buffer->GetPointer(chain_info.map_table[indi].offset, chain_info.map_table[indi].size)) {
//					System::XMMPush(xmm_vals);

						if (service_flags & SafeChain) {
							for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[indi].reserved) + 11)[i];
							for (unsigned int i(4);i<10;i++) aes_key[i] = 0;

							Crypt::reverse_aes128_SSSE3(reinterpret_cast<char *>(s_ptr), chain_info.map_table[indi].size, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(s_ptr));
						}

						if (o_val) {
							if (void * d_ptr = _buffer->GetPointer(o_val, chain_info.map_table[indi].size)) {
								System::MemoryCopy_SSE3(d_ptr, s_ptr, chain_info.map_table[indi].size);

								_buffer->ReleasePointer(d_ptr);

								if (free_it) {
									AddFreeSlot(indi, s_ptr);
								}

								result = indi;

								chain_info.map_table[indi].offset = o_val;							
							}
						}

						_buffer->ReleasePointer(s_ptr);


						if (s_ptr = _buffer->GetPointer(chain_info.map_table[indi].offset, nsize)) { 

							System::MemoryFill_SSE3(reinterpret_cast<char *>(s_ptr) + chain_info.map_table[indi].size, nsize - chain_info.map_table[indi].size, 0, 0);

							if (service_flags & SafeChain) {
								Crypt::sha256_SSSE3(chain_info.map_table[indi].reserved, reinterpret_cast<char *>(s_ptr), nsize, 1);

								for (unsigned int i(0);i<4;i++) aes_key[i] = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(chain_info.map_table[indi].reserved) + 11)[i];
								for (unsigned int i(4);i<10;i++) aes_key[i] = 0;

								Crypt::forward_aes128_SSSE3(reinterpret_cast<char *>(s_ptr), nsize, reinterpret_cast<char *>(aes_key), reinterpret_cast<char *>(s_ptr));
							}

							_buffer->ReleasePointer(s_ptr);
						}

						chain_info.map_table[indi].size = nsize;
					}					
				}
			} else {
				result = indi;
			}
		}
	}
	
	if (result != -1) GetSystemTimeAsFileTime(reinterpret_cast<LPFILETIME>(&chain_info.map_table[result].access_mark));

	ServiceUnlock();

	return result;
}


void  Memory::Chain::SpecialResize(UI_64 indi) {
	UI_64 nsize(0), o_val(0), deltas(0), free_it(0);
	void * t_ptr(0);

	if (indi > 2) return;	
	
	switch (indi) {
		case 0:
			while(System::SpinWait_I64(ptr_lock + 32)>1);
/*
	}
			if (mb_val < 0) {
				for (UI_64 mi(0);mi<chain_info.length;mi++) {
					if (chain_info.map_table[mi].ref_count) {
						mb_val = mi;
						break;
					}
				}
			}
*/

			nsize = (chain_info.map_top + MIN_TABLE_COUNT)*sizeof(MapTableEntry);
		break;
		case 1:
			nsize = chain_info.map_table[1].size + MIN_ESLOT_COUNT*sizeof(UI_64);
		break;
		case 2:
			nsize = chain_info.map_table[2].size + MIN_ESLOT_COUNT*2*sizeof(UI_64);
		break;
	}

	nsize = (nsize + PARAGRAPH) & ~PARAGRAPH;
	deltas = nsize - chain_info.map_table[indi].size;

	for (unsigned int i(1);i<=reinterpret_cast<unsigned int *>(chain_info.free_slot)[0];i++) {
				
		if (chain_info.map_table[indi].offset == (chain_info.free_slot[2*i] + chain_info.free_slot[2*i+1])) {
			if (deltas <= chain_info.free_slot[2*i]) { // shift left
				if (deltas < chain_info.free_slot[2*i]) {
					chain_info.free_slot[2*i] -= deltas;
							
					while(System::SpinWait_I32(&chain_info.map_table[indi].ref_count)>1);								
					// do new view
					o_val = (chain_info.free_slot[2*i] + chain_info.free_slot[2*i+1]);

				} else {
							
					while(System::SpinWait_I32(&chain_info.map_table[indi].ref_count)>1);

					o_val = chain_info.free_slot[2*i+1];
					
					if (i < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
						chain_info.free_slot[2*i] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
						chain_info.free_slot[2*i+1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
					}
					reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
				}

				chain_info.free_slot[1] -= deltas;				
							
				break;
			}
		} else {
			if ((chain_info.map_table[indi].offset + chain_info.map_table[indi].size) == chain_info.free_slot[2*i+1]) {
				if (deltas <= chain_info.free_slot[2*i]) {
					if (deltas < chain_info.free_slot[2*i]) {
						chain_info.free_slot[2*i] -= deltas;
						chain_info.free_slot[2*i+1] += deltas;
																
					} else {						
						if (i < reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]) {
							chain_info.free_slot[2*i] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]];
							chain_info.free_slot[2*i+1] = chain_info.free_slot[2*reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]+1];
						}
						reinterpret_cast<unsigned int *>(chain_info.free_slot)[0]--;
					}
							
					chain_info.free_slot[1] -= deltas;
					o_val = chain_info.map_table[indi].offset;

					break;
				}
			}
		}
	}
	

	if (!o_val) {
		o_val = FindaSlot(nsize);
		free_it = 1;
	}


	if (o_val) {
		if (void * d_ptr = _buffer->GetPointer(o_val, nsize)) {
			switch (indi) {
				case 0:					
					System::MemoryCopy_SSE3(d_ptr, chain_info.map_table, chain_info.map_table[0].size);
					
					t_ptr = chain_info.map_table;
					chain_info.map_table = reinterpret_cast<MapTableEntry *>(d_ptr);
									
					System::MemoryFill_SSE3(chain_info.map_table + chain_info.map_top, MIN_TABLE_COUNT*sizeof(MapTableEntry), 0, 0);

					chain_info.map_offset = o_val;
					chain_info.map_top += MIN_TABLE_COUNT;
				break;
				case 1:
					System::MemoryCopy_SSE3(d_ptr, chain_info.empty_slot, chain_info.map_table[1].size);

					t_ptr = chain_info.empty_slot;
					chain_info.empty_slot = reinterpret_cast<UI_64 *>(d_ptr);
					
					chain_info.map_table[1].view_ptr = chain_info.empty_slot;
					
					reinterpret_cast<unsigned int *>(chain_info.empty_slot)[1] += MIN_ESLOT_COUNT;

				break;
				case 2:
					System::MemoryCopy_SSE3(d_ptr, chain_info.free_slot, chain_info.map_table[2].size);

					t_ptr = chain_info.free_slot;
					chain_info.free_slot = reinterpret_cast<UI_64 *>(d_ptr);
					
					chain_info.map_table[2].view_ptr = chain_info.free_slot;
	
					reinterpret_cast<unsigned int *>(chain_info.free_slot + 1)[1] += MIN_ESLOT_COUNT;
				break;
			}			

			if (free_it) {
				AddFreeSlot(indi, t_ptr);
			}

			_buffer->ReleasePointer(t_ptr);


			chain_info.map_table[indi].offset = o_val;
			chain_info.map_table[indi].size = nsize;
	
		}		
	}
}




UI_64 Memory::Chain::GetFirst(UI_64 tid) {
	UI_64 result(-1);

	if (chain_info.map_table)
	for (UI_64 i(0);i<chain_info.length;i++) {
		if (chain_info.map_table[i].type == tid) {
			result = i;
			break;
		}
	}

	return result;
}

UI_64 Memory::Chain::GetNext(UI_64 refl) {
	UI_64 result(refl);
	
	for (;;) {
		result = chain_info.map_table[result].next_id;
		if (chain_info.map_table[result].type == chain_info.map_table[refl].type) {
			break;
		}
	}

	return result;
}

UI_64 Memory::Chain::GetPrev(UI_64 refl) {
	UI_64 result(refl);
	
	for (;;) {
		result = chain_info.map_table[result].back_id;
		if (chain_info.map_table[result].type == chain_info.map_table[refl].type) {
			break;
		}
	}

	return result;
}


UI_64 Memory::Chain::GetLength() {
	return chain_info.length;
}

bool Memory::Chain::VerifyLinkType(UI_64 id, UI_64 tid) {
	return ((chain_info.map_table[id].type % tid) == 0);
}

UI_64 Memory::Chain::GetLinkSize(UI_64 id) {
	return chain_info.map_table[id].size;
}

bool Memory::Chain::IsLinkValid(UI_64 id) {
	return ((chain_info.map_table[id].size > 0) && ((chain_info.map_table[id].flags & Removed) == 0));
}

UI_64 Memory::Chain::GetLinkType(UI_64 id) {
	return chain_info.map_table[id].type;
}

int Memory::Chain::SetLinkFlags(UI_64 id, int flg) {
	return InterlockedOr((long *)&chain_info.map_table[id].flags, flg);
}

int Memory::Chain::TestLinkFlags(UI_64 id, int flg) {
	return ((chain_info.map_table[id].flags & flg) ^ flg);
}

int Memory::Chain::GetLinkFlags(UI_64 id) {
	return chain_info.map_table[id].flags;
}

int Memory::Chain::GetLinkRC(UI_64 id) {
	return chain_info.map_table[id].ref_count;
}

UI_64 Memory::Chain::NextLink(UI_64 id) {
	return chain_info.map_table[id].next_id;
}

UI_64 Memory::Chain::BackLink(UI_64 id) {
	return chain_info.map_table[id].back_id;
}

UI_64 Memory::Chain::DownLink(UI_64 id) {	
	return chain_info.map_table[id].x_down;
}

UI_64 Memory::Chain::UpLink(UI_64 id) {	
	return chain_info.map_table[id].x_up;
}

UI_64 Memory::Chain::LeftLink(UI_64 id) {	
	return chain_info.map_table[id].x_left;
}

UI_64 Memory::Chain::RightLink(UI_64 id) {
	return chain_info.map_table[id].x_right;
}

void * Memory::Chain::GetLinkPtr(UI_64 id) {
	if ((service_flags & FChain) == 0) {
		return _buffer->GetPointer(chain_info.map_table[id].offset, chain_info.map_table[id].size);
	}
	return 0;
}

void Memory::Chain::WaitLink(UI_64 id) {
	while (System::SpinWait_I32(&chain_info.map_table[id].ref_count) > 1);

}

// Allocator ===============================================================================================================================================================================================================================================
Memory::Allocator::Allocator(UI_64 x_t) : Chain(x_t) {

}

Memory::Allocator::~Allocator() {

}

void Memory::Allocator::ShiftRight(UI_64 ref_id, UI_64 left_id) {
	ShiftRightTo(ref_id, left_id);
}

void Memory::Allocator::ShiftLeft(UI_64 ref_id, UI_64 left_id) {
	ShiftLeftTo(ref_id, left_id);
}

UI_64 Memory::Allocator::Alloc(UI_64 ssize, UI_64 tid, UI_64 prnt) {
	return InsertLink(ssize, tid, prnt);
}

UI_64 Memory::Allocator::Realloc(UI_64 src, UI_64 ssize) {
	if (src != -1) {
		src = ResizeLink(src, ssize);
	} else {
		src = InsertLink(ssize, undefined_tid);
	}
	return src;
}

UI_64 Memory::Allocator::Expand(UI_64 src, UI_64 esize) {
	if (src != -1) {
		src = ResizeLink(src, -esize);
	} else {
		src = Alloc(esize, undefined_tid);
	}
	return src;
}



int Memory::Allocator::Free(UI_64 ref) {
	return RemoveLink(ref);
}

int Memory::Allocator::IncRef(UI_64 id, void *& refptr) {
	return GetLink(refptr, id);
}

int Memory::Allocator::DecRef(UI_64 id) {
	return FreeLink(id);
}

void Memory::Allocator::SpinWaitRC(UI_64 id, int limit) {
	LinkSpinWait(id, limit);
}

int Memory::Allocator::GetRefCount(UI_64 id) {
	SpinWait();
	return GetLinkRC(id);
}

UI_64 Memory::Allocator::GetSize(UI_64 id) {
	SpinWait();
	return GetLinkSize(id);
}

UI_64 Memory::Allocator::GetTypeID(UI_64 id) {
	SpinWait();
	return GetLinkType(id);
}

bool Memory::Allocator::IsValid(UI_64 id) {
	SpinWait();
	return IsLinkValid(id);
}

int Memory::Allocator::SetFlags(UI_64 id, int flg) {
	SpinWait();
	return SetLinkFlags(id, flg);
}

int Memory::Allocator::GetFlags(UI_64 id) {
	SpinWait();
	return GetLinkFlags(id);
}

UI_64 Memory::Allocator::Next(UI_64 id) {
	SpinWait();
	return NextLink(id);
}

UI_64 Memory::Allocator::Back(UI_64 id) {
	SpinWait();
	return BackLink(id);
}

UI_64 Memory::Allocator::Left(UI_64 id) {
	SpinWait();
	return LeftLink(id);
}

UI_64 Memory::Allocator::Right(UI_64 id) {
	SpinWait();
	return RightLink(id);
}

UI_64 Memory::Allocator::Down(UI_64 id) {
	SpinWait();
	return DownLink(id);
}

void Memory::Allocator::Wait(UI_64 id) {
	WaitLink(id);
}

UI_64 Memory::Allocator::Up(UI_64 id) {
	UI_64 result(0);

	SpinWait();
	for (UI_64 x_id = UpLink(id);x_id == id;x_id = UpLink(id)) {
		if (id != LeftLink(id)) {
			id = LeftLink(id);
		} else {
			result = -1;
			break;
		}		
	}

	if (result != -1) result = UpLink(id);

	return result;


	return UpLink(id);
}


