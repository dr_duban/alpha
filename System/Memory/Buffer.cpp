/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "System\Memory.h"
#include "System\SysUtils.h"


#ifdef _WIN64
	#define TOP_FLOOR		0x7000000000000001
	#define XPAND_FLAG		0x8000000000000000
	
	#define FLAGS_MASK_OUT	0x0FFFFFFFFFFFFFFF

#else
	#define TOP_FLOOR		0x70000001
	#define XPAND_FLAG		0x80000000
	
	#define FLAGS_MASK_OUT	0x0FFFFFFF

#endif
	

typedef ULONG_PTR (* pGLPM)(void);


UI_64 Memory::Buffer::_largePageSize = 0;
unsigned int Memory::Buffer::_eval = 0;
UI_64 Memory::Buffer::_granularity = 0;


unsigned int Memory::Buffer::BugCheck() {
	return _eval;
}

int Memory::Buffer::Initialize() {
	unsigned int xval(0);
	SYSTEM_INFO sysInfo = {0};

	GetSystemInfo(&sysInfo);
	_granularity = sysInfo.dwAllocationGranularity;

	HANDLE accessHandle(0);
	if (!OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,&accessHandle)) accessHandle = 0;


	if (HMODULE kerni = GetModuleHandle(L"kernel32.dll")) {			
		pGLPM gLpM = (pGLPM)GetProcAddress(kerni,"GetLargePageMinimum");
		if (gLpM) xval = gLpM();
	}
			
	
	if (xval) {
		LUID luid;
		
		if (accessHandle)
		if (LookupPrivilegeValue(0,L"SeLockMemoryPrivilege",&luid)) {
			TOKEN_PRIVILEGES tp;
			tp.PrivilegeCount = 1;
			tp.Privileges[0].Luid = luid;
			tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			if (AdjustTokenPrivileges(accessHandle,FALSE,&tp,sizeof(TOKEN_PRIVILEGES),0,0))
				if (!GetLastError()) _largePageSize = xval;
		}
	}

	_eval = 0;

	return 0;
}

void Memory::Buffer::Finalize() {

}


UI_64 Memory::Buffer::Init(UI_64 siz, const wchar_t * f_n) {
	UI_64 result(0);
	if (_node) return 0;

	if (!_rSize) {
		if (!siz) siz = _granularity;

		if (siz > TOP_FLOOR) siz = TOP_FLOOR;

		if (siz > 0x300000) {
			if (_largePageSize) {
				_pageSize = _largePageSize;
				_type |= MEM_LARGE_PAGES;
				_cType |= MEM_LARGE_PAGES;

			}

		}

		_scale = (_pageSize > _granularity) ? _pageSize : _granularity;

		_rSize = (siz + _scale - 1)/_scale;
		_rSize *= _scale;
	
		_type |= MEM_COMMIT; // if (dyn)
	}

	if (!_node) {
		_node = (char *)VirtualAlloc(0, _rSize, _type, PAGE_READWRITE);
		
		if (!_node) {
			_eval = GetLastError();

			if ((_eval == ERROR_NO_SYSTEM_RESOURCES) && (_type & MEM_LARGE_PAGES)) {
				_type &= (~MEM_LARGE_PAGES);
				_cType &= (~MEM_LARGE_PAGES);

				SetLastError(0);

				_pageSize = 4096;
				_scale = (_pageSize > _granularity) ? _pageSize : _granularity;

				_node = (char *)VirtualAlloc(0, _rSize, _type, PAGE_READWRITE);

				_eval = GetLastError();
			}
		}

		if (_node) {
			result = 1;
			_pointers[0] = _node;
			for (;;) {
				_node[_cSize] = 0;
				_cSize += _pageSize;
				if (_cSize >= _rSize) break;
			}
		}

	}


	return result;
}


Memory::Buffer::Buffer() : _pageSize(4096), _rSize(0), _type(MEM_RESERVE), _cType(MEM_COMMIT), _cSize(0), _node(0), _scale(0) {
	System::MemoryFill(_pointers, 40*sizeof(void *), 0);
}

Memory::Buffer::~Buffer() {
	for (int i(0);i<40;i++) if (_pointers[i]) VirtualFree(_pointers[i], 0, MEM_RELEASE);
}



int Memory::Buffer::ReSize(UI_64 siz) {
	int result(0);
	
	if (siz <= _rSize) return 0;

	if (siz<TOP_FLOOR) {
		UI_64 rco((siz + _scale - 1)/_scale);
		void * tmbf(0);
			
		_eval = 0;
		if ((_rSize+rco*_scale)<TOP_FLOOR/2) {
			tmbf = VirtualAlloc(_node+_rSize, rco*_scale, _type, PAGE_READWRITE);
			if (!tmbf) _eval = GetLastError();
		} else {				
			if (tmbf  = VirtualAlloc(_node+_rSize, ((UI_64)TOP_FLOOR - _rSize - 1), _type, PAGE_READWRITE)) {
				rco = ((UI_64)TOP_FLOOR - _rSize)/_scale;
			} else {
				rco = (UI_64)TOP_FLOOR/_scale;
				_eval = GetLastError();
			}
		}

			
		if (tmbf) {
			_rSize += rco*_scale;
				
			for (int i(0);i<40;i++)
				if (!_pointers[i]) {
					_pointers[i] = tmbf;
					break;
				}				
			
		} else {
				
			if (_eval == ERROR_INVALID_ADDRESS) {
				
				if (tmbf = VirtualAlloc(0, rco*_scale, _type, PAGE_READWRITE)) {
					_eval = 0;
					System::MemoryCopy_SSE3(tmbf, _node, _cSize); // if (_cSize)
					_rSize = rco*_scale;

					for (int i(0);i<40;i++) if (_pointers[i])
						if (!VirtualFree(_pointers[i], 0, MEM_RELEASE)) _eval = GetLastError();

					System::MemoryFill(_pointers, 40*sizeof(void *), 0);

					_node = (char *)tmbf;
					_pointers[0] = tmbf;
										
				} else {
					_eval = GetLastError();
				}
			}
		}

		if (!_eval) {
			for (;;) {
				_node[_cSize] = 0; // touch
				_cSize += _pageSize;
				if (_cSize >= _rSize) break;
			}
			result = 1;
		}
	} else {
// nowhere to go
		_eval = ERROR_NOT_ENOUGH_MEMORY;
	}



	return result;
}


UI_64 Memory::Buffer::GetSize() const {
	return _cSize;
}

void * Memory::Buffer::GetPointer(UI_64 offset, UI_64 size) {
	if ((_node) && (offset + size) <= _cSize) {
		return (_node + offset);
	}
	return 0;
}

void Memory::Buffer::ReleasePointer(void * & ptr) {
	ptr = 0;
}

void Memory::Buffer::GetSomeVal(void * v_ptr, UI_64 v_size, UI_64 v_offset) {
	if ((_node) && (v_offset + v_size) <= _cSize) {
		System::MemoryCopy(v_ptr, _node + v_offset, v_size);
	}
}

void Memory::Buffer::SetSomeVal(void * v_ptr, UI_64 v_size, UI_64 v_offset) {
	if ((_node) && (v_offset + v_size) <= _cSize) {
		System::MemoryCopy(_node + v_offset, v_ptr, v_size);
	}
}

// FileBuffer ========================================================================================================================================================================================================================================
Memory::FileBuffer::FileBuffer() : f_handle(INVALID_HANDLE_VALUE), map_handle(0) {
	_scale = _pageSize = 8192;
}

Memory::FileBuffer::~FileBuffer() {
	if (map_handle) CloseHandle(map_handle);
	if (f_handle != INVALID_HANDLE_VALUE) CloseHandle(f_handle);
}

UI_64 Memory::FileBuffer::Init(UI_64 siz, const wchar_t * f_n) {
	UI_64 result(0);

	if (!siz) siz = 65536;

	siz = (siz + _scale - 1)/_scale;
	siz *= _scale;

	if (f_handle == INVALID_HANDLE_VALUE) {
		f_handle = CreateFile(f_n, GENERIC_ALL, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		if (f_handle == INVALID_HANDLE_VALUE) {
			f_handle = CreateFile(f_n, GENERIC_ALL, 0, 0, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);

			if (f_handle != INVALID_HANDLE_VALUE) {
				if (SetFilePointerEx(f_handle, reinterpret_cast<LARGE_INTEGER &>(siz), 0, 0)) {
					if (SetEndOfFile(f_handle)) {
						_cSize = siz;
						if (map_handle = CreateFileMapping(f_handle, 0, PAGE_READWRITE, 0, 0, 0)) {
							result = 1;
						} else {
							_eval = GetLastError();
						}
					}
				}

			} else {
				_eval = GetLastError();
			}

		} else {
			if (GetFileSizeEx(f_handle, reinterpret_cast<PLARGE_INTEGER>(&_cSize))) {
				if (map_handle = CreateFileMapping(f_handle, 0, PAGE_READWRITE, 0, 0, 0)) {
					result = 2;
				} else {
					_eval = GetLastError();
				}
			}
		}
	}

	return result;
}



int Memory::FileBuffer::ReSize(UI_64 siz) {
	int result(0);
	
	if (map_handle) {
		CloseHandle(map_handle);
		map_handle = 0;

		siz = (siz + _scale - 1)/_scale;
		siz *= _scale;

		if (SetFilePointerEx(f_handle, reinterpret_cast<LARGE_INTEGER &>(siz), 0, 0)) {
			if (SetEndOfFile(f_handle)) {
				_cSize = siz;
				if (map_handle = CreateFileMapping(f_handle, 0, PAGE_READWRITE, 0, 0, 0)) {
					result = 1;
				} else {
					_eval = GetLastError();
				}
			}
		}
	}

	return result;
}


void * Memory::FileBuffer::GetPointer(UI_64 offset, UI_64 size) {
	void * result(0);

	UI_64 a_offset = offset & (~(_granularity - 1));
	size += offset + _granularity - 1;
	offset &= (_granularity - 1);

	size &= (~(_granularity - 1));
	
	if ((map_handle) && (size <= _cSize)) {
		size -= a_offset;
		result = MapViewOfFile(map_handle, FILE_MAP_WRITE, a_offset >> 32, a_offset & 0xFFFFFFFF, size);

		if (!result) _eval = GetLastError();
		else reinterpret_cast<char *&>(result) += offset;
	}
	return result;
}

void Memory::FileBuffer::ReleasePointer(void * & ptr) {
	if (map_handle) {
		if (UnmapViewOfFile(ptr)) {
			ptr = 0;
		}
	}
}

void Memory::FileBuffer::GetSomeVal(void * v_ptr, UI_64 v_size, UI_64 v_offset) {
	DWORD b_count(0);
	if ((f_handle != INVALID_HANDLE_VALUE) && (v_offset + v_size) <= _cSize) {
		if (SetFilePointerEx(f_handle, reinterpret_cast<LARGE_INTEGER &>(v_offset), 0, 0)) {
			ReadFile(f_handle, v_ptr, v_size, &b_count, 0);
		}		
	}
}

void Memory::FileBuffer::SetSomeVal(void * v_ptr, UI_64 v_size, UI_64 v_offset) {
	DWORD b_count(0);
	if ((f_handle != INVALID_HANDLE_VALUE) && (v_offset + v_size) <= _cSize) {
		if (SetFilePointerEx(f_handle, reinterpret_cast<LARGE_INTEGER &>(v_offset), 0, 0)) {
			WriteFile(f_handle, v_ptr, v_size, &b_count, 0);
		}
	}
}











