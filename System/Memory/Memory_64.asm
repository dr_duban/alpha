
; safe-fail base elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;


	link_index				TEXTEQU	<0>

	link_left				TEXTEQU <8>
	link_right				TEXTEQU <16>
	link_back				TEXTEQU <40>
	link_next				TEXTEQU <32>

	chain_length			TEXTEQU <424>
	chain_level				TEXTEQU <440>
	chain_toplink			TEXTEQU <464>

	buffer_node				TEXTEQU <360>


OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CODE

ALIGN 16
?SearchLink@Chain@Memory@@AEAAPEAVLink@2@_K@Z	PROC
		XOR rax, rax
		MOV r8, [rcx+chain_length]
		CMP r8, rdx
		JBE matchfo
		
		MOV r8, rcx
		MOV rcx, [r8+buffer_node]
		ADD rcx, [r8+chain_toplink]

	align 16
		searchloop:
			MOV rax, rcx

			MOV r8, [rcx+link_index]
			CMP r8, rdx
			JZ matchfo
				JB goright
					ADD rcx, [rcx+link_left]					
					JMP searchloop
				goright:
					ADD rcx, [rcx+link_right]					
		JMP searchloop

		matchfo:

	RET
?SearchLink@Chain@Memory@@AEAAPEAVLink@2@_K@Z	ENDP

?ParentId@Chain@Memory@@AEAA_K_K@Z	PROC
		MOV r8, [rcx+chain_level]
		SUB r8, 1
		SUB r8, rdx
		MOV rax, 1

	align 16
		esloop:
			SHR r8, 1
			JC ooo
				SHL rax, 1
		JMP esloop
		ooo:
			JZ dosub
				ADD rax, rdx
			JMP testit
		dosub:
			SUB rax, rdx
			NEG rax

		testit:
		
		CMP rax, [rcx+chain_length]
		JB getit
			MOV r8, rdx
			SHL r8, 1
			SUB rax, r8

			NEG rax
		getit:

	RET
?ParentId@Chain@Memory@@AEAA_K_K@Z	ENDP



END

