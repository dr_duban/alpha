﻿#include <Windows.h>

#include "System\Window.h"
#include "System\Control.h"

#include <Dbt.h>




#define WINDOW_MAIN_TIMER	1

class MainWindow: public Window {
	private:
		static MainWindow * _instance;
		
		int _refX, _refY, _po_flag, _cu_flag, _size_flag, _mo_in;
		unsigned __int64 local_time;

		SFSco::IGLApp * _main_menu, * _x_files;

		SFSco::IOpenGL::ScreenSet _screen_shot, vi_frame;
		SFSco::Object notification_area, function_area, menu_button;

		UINT_PTR o_timer;
		TRACKMOUSEEVENT _mo_event;

		MainWindow() : o_timer(0), _po_flag(0), _refX(0), _refY(0), _cu_flag(0), _size_flag(0), local_time(0), _main_menu(0), _x_files(0), _mo_in(0) {
			_screen_shot.offset_x = _screen_shot.offset_y = _screen_shot.screen_width = _screen_shot.screen_height = 0;
			_mo_event.cbSize = sizeof(TRACKMOUSEEVENT);
			_mo_event.dwFlags = TME_LEAVE;
			_mo_event.hwndTrack = 0;
			_mo_event.dwHoverTime = HOVER_DEFAULT;
		}

		virtual ~MainWindow() {			
			if (o_timer) KillTimer(_handle, o_timer);			
		}
		
		static void __fastcall ShutDown() {
			if (_instance) PostMessage(_instance->GetHandle(), WM_KEYDOWN, VK_ESCAPE, 0);
		}

		static void __fastcall Maximize() {
			if (_instance) PostMessage(_instance->GetHandle(), WM_SYSKEYDOWN, VK_RETURN, 0);
		}

		virtual unsigned int Window::Loop(LRESULT & res, UINT msg, WPARAM wp, LPARAM lp) {
			int xx(0), yy(0);
			unsigned int result(0), result2(0), sid(0);
			RECT w_rect;
			SFSco::GlobalSet::WinCfg r_cfg;

			switch (msg) {
				case WM_MOUSEMOVE:
					if (_mo_in == 0) {
						TrackMouseEvent(&_mo_event);
					}
					_mo_in = 1;

					xx = (lp<<16);
					xx >>= 16;
					yy = lp>>16;
										

					if (((xx>3) && (yy>3) && ((_width-xx)>3) && ((_height-yy)>3)) && (!(_size_flag & 0x80000000))) {
						_size_flag = 0;
						for (;;) {
							_cu_flag = ShowCursor(FALSE);
							if (_cu_flag<0) break;
						}
												
						if (_OGL->PoMove(xx, yy)) {

							if ((wp & MK_LBUTTON) && (_po_flag & 1)) {
								Relocate(xx-_refX, yy-_refY);							
						
							}
						}
					} else {
						_OGL->PoMove(0x7FFFFFFF, 0x7FFFFFFF);

						if (_po_flag & 1) {
							switch (_size_flag) {
								case 0x80000001:
									Size(_refX-xx, yy-_refY);
									Relocate(xx-_refX, 0);
									_refY = yy;
								break;
								case 0x80000002:
									Size(_refX-xx, _refY-yy);
									Relocate(xx-_refX, yy-_refY);
								break;
								case 0x80000003:
									Size(_refX-xx, 0);
									Relocate(xx-_refX, 0);
								break;
								case 0x80000004:
									Size(xx-_refX, yy-_refY);
									_refX = xx;
									_refY = yy;
								break;
								case 0x80000005:
									Size(xx-_refX, _refY-yy);
									Relocate(0, yy-_refY);
									_refX = xx;
								break;
								case 0x80000006:
									Size(xx-_refX, 0);
									_refX = xx;
								break;
								case 0x80000007:
									Size(0, yy-_refY);
									_refY = yy;
								break;
								case 0x80000008:
									Size(0, _refY-yy);
									Relocate(0, yy-_refY);
								break;
								

							}
						} else {
							for (;_cu_flag == 0;_cu_flag = ShowCursor(TRUE));

							if (xx<=3) {
								if  ((_height-yy)<=3) {
									SetCursor(LoadCursor(0, IDC_SIZENESW));
									_size_flag = 1;
								} else if (yy<=3) {
									SetCursor(LoadCursor(0, IDC_SIZENWSE));
									_size_flag = 2;
								} else {
									SetCursor(LoadCursor(0, IDC_SIZEWE));
									_size_flag = 3;
								}


							} else {
								if ((_width-xx)<=3) {
									if  ((_height-yy)<=3) {
										SetCursor(LoadCursor(0, IDC_SIZENWSE));
										_size_flag = 4;
									} else if (yy<=3) {
										SetCursor(LoadCursor(0, IDC_SIZENESW));
										_size_flag = 5;
									} else {
										SetCursor(LoadCursor(0, IDC_SIZEWE));
										_size_flag = 6;
									}
								} else {
									if ((yy<=3) || ((_height-yy)<=3)) {
										SetCursor(LoadCursor(0, IDC_SIZENS));
										_size_flag = 7 + (yy<=3);

									}
								}
							}						
						}
					}
					
					res = 0;
				break;
				case WM_MOUSEWHEEL:
					result2 = wp & 0xFFFFFFFF;
					_OGL->Scroll((int)result2 >> 16);
				break;
				case WM_MOUSELEAVE:
					_mo_in = 0;
					_OGL->PoMove(0x7FFFFFFF, 0x7FFFFFFF);
					_OGL->Refresh();
				break;
				case WM_LBUTTONDOWN:
					SetCapture(_handle);
					if (_OGL->PoLock(SFSco::IOpenGL::LeftButton)) { // no gl elements selected
						if (_size_flag) _size_flag |= 0x80000000;

						_refX = lp & 0xFFFF;
						_refY = lp >> 16;

						_po_flag |= 1;
					}
					res = 0;
				break;
				case WM_LBUTTONUP:
					ReleaseCapture();
					_OGL->PoUnLock(SFSco::IOpenGL::LeftButton);
					_refX = _refY = 0;
					_size_flag = 0;
					_po_flag &= ~1;
					res = 0;
				break;
				case WM_DESTROY:

					res = 0;
					PostQuitMessage(0);
				break;
				case WM_KEYDOWN:
					switch (wp) {

						case VK_ESCAPE:
							if (GetWindowRect(_handle, &w_rect)) {
								r_cfg.x0 = w_rect.left;
								r_cfg.y0 = w_rect.top;
								r_cfg.w0 = w_rect.right - w_rect.left;
								r_cfg.h0 = w_rect.bottom - w_rect.top;

								r_cfg.x1 = _x0;
								r_cfg.y1 = _y0;

								r_cfg.max_flag = _maximized;

								SFSco::SetProgramBox(r_cfg);

							}

							DestroyWindow(_handle);
						break;
						


						
						case VK_F11:
							_OGL->Update();
						break;
/*
						case VK_F4:
						{
							SFSco::IOpenGL::ScreenSet sset;
							sset.screen_width = 0; sset.screen_height = 0;
							sset.offset_x = 0; sset.offset_y = 0;
							sset.i_type = SFSco::ImageType::JPG;
							sset.q_factor = 70;

							result = SFSco::Image_LoadFile(L"..\\..\\Pics\\Extr\\PNG\\_112.png");

							SFSco::Image_SaveAs(result, sset, 0);
							
						}	
						break;
*/
						case VK_F8:
							_OGL->ScreenShot(_screen_shot);
						break;

					}

					res = 0;
				break;
				case WM_SYSKEYDOWN:
					switch (wp) {
						case VK_RETURN:
							// maximize - minimize
							Size(0x80000000, 0);							

						break;
						case VK_F4:
							if (GetWindowRect(_handle, &w_rect)) {
								r_cfg.x0 = w_rect.left;
								r_cfg.y0 = w_rect.top;
								r_cfg.w0 = w_rect.right - w_rect.left;
								r_cfg.h0 = w_rect.bottom - w_rect.top;

								r_cfg.x1 = _x0;
								r_cfg.y1 = _y0;

								r_cfg.max_flag = _maximized;

								SFSco::SetProgramBox(r_cfg);
							}

							DestroyWindow(_handle);
						break;
					}

					res = 0;
				break;
				case WM_PAINT:
					ValidateRect(_handle, 0);					
				break;


				case WM_DISPLAYCHANGE:

				break;
				case WM_DEVICECHANGE:
					if ((wp == DBT_DEVICEARRIVAL) || (wp == DBT_DEVICEREMOVECOMPLETE)) {
						_instance->_x_files->Change();


					}
				
				break;
			}

			if (msg == WM_TIMER) {
				if (_instance) _OGL->Render();
			}

			return result;
		}


	public:
		static SFSco::IOpenGL * main_ogl;

		static UI_64 __fastcall Create(SFSco::Object * ip_obj) {
			wchar_t temp_str[64];
			UI_64 result(-1);
			SFSco::GlobalSet::WinCfg w_cfg;
			
			if (!_instance) {				
				if (_instance = new MainWindow()) {
					SFSco::GetHeaderStr(20, temp_str);
					result = _instance->ClassInit(temp_str + 1, CS_OWNDC);

					if (!result) {
						SFSco::GetHeaderStr(29, temp_str);
						SFSco::GetProgramBox(w_cfg);
						
						if (w_cfg.w0 && w_cfg.h0) {
							result = w_cfg.h0;
							result <<= 16;
							result |= w_cfg.w0;
							result <<= 12;
							result |= w_cfg.y0;
							result <<= 12;
							result |= w_cfg.x0;

							result <<= 8;

		
							
							_instance->_x0 = w_cfg.x1;
							_instance->_y0 = w_cfg.y1;

							if (_instance->_maximized = w_cfg.max_flag) {	
								_instance->_mm_wi = 2*w_cfg.w0/3;;
								_instance->_mm_he = 2*w_cfg.h0/3;
							}
						}

						if (!result) {
							result = WILOC_CENTER_CENTER | WIBOX_67_67;
						}

						result = _instance->::Window::Create(temp_str + 1, GetModuleHandle(0), WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_POPUP, result);
						if (!result) {
							_instance->_mo_event.hwndTrack = _instance->_handle;
							ShowCursor(FALSE);						
					

							if (main_ogl = reinterpret_cast<SFSco::IOpenGL *>(SFSco::Instantiate(opengl_core_IFACE_ID))) {
								result = main_ogl->Activate(_instance->_handle, SFSco::IOpenGL::RightBottom);

								if (!result) {
									if (ip_obj) {
										main_ogl->SetInfoPic(ip_obj);
										ip_obj->Destroy();
									}

									_instance->_OGL = main_ogl;
									_instance->_OGL->MoCursor(0);
			
								}
							}
						}
					}

					_instance->local_time = SFSco::GetTimeStamp();
					
				} else {
					result = ERROR_OUTOFMEMORY;
				}

			}

			if (!result) {
				_instance->o_timer = SetTimer(_instance->_handle, WINDOW_MAIN_TIMER, 29, 0);
				if (!_instance->o_timer) result = GetLastError();
			}
			
			return result;
		}

		static unsigned int Build(int showcmd, SFSco::Object & ifnfo_pic) {
			unsigned int result(0);
			SFSco::IGLApp * j_app(0);

			if (_instance) {

				if (_instance->_main_menu = reinterpret_cast<SFSco::IGLApp *>(SFSco::Instantiate(main_menu_IFACE_ID))) {					
					result = _instance->_main_menu->Initialize(&main_ogl, ShutDown, Maximize);

					if (!result)
					if (_instance->_x_files = reinterpret_cast<SFSco::IGLApp *>(SFSco::Instantiate(x_files_IFACE_ID))) {
						result = _instance->_x_files->Initialize(&main_ogl, _instance->_main_menu, 0);

						if (!result) {
							if (j_app = reinterpret_cast<SFSco::IGLApp *>(SFSco::Instantiate(x_pic_IFACE_ID))) {
								result = j_app->Initialize(&main_ogl, 0, 0);
								_instance->_x_files->AddFilter(j_app);
							}
							
							if (j_app = reinterpret_cast<SFSco::IGLApp *>(SFSco::Instantiate(x_video_IFACE_ID))) {
								result |= j_app->Initialize(&main_ogl, 0, 0);
								_instance->_x_files->AddFilter(j_app);
							}

							if (j_app = reinterpret_cast<SFSco::IGLApp *>(SFSco::Instantiate(x_audio_IFACE_ID))) {
								result |= j_app->Initialize(&main_ogl, 0, 0);
								_instance->_x_files->AddFilter(j_app);
							}

							result = 0;
						}
					}
				}

				_instance->Show(showcmd);
			}

			return result;
		}

		static void Destroy() {
			if (_instance) {
				if (_instance->_main_menu) {
					_instance->_main_menu->Finalize();
				}

				delete _instance;
			}
			_instance = 0;
		}

		static MainWindow * GetInstance() {
			return _instance;
		}
};

SFSco::IOpenGL * MainWindow::main_ogl = 0;
MainWindow * MainWindow::_instance = 0;

int succ_val = 0;

int __stdcall wWinMain(HINSTANCE inst, HINSTANCE pinst, wchar_t * cmdline, int showcmd) {
	wchar_t temp_str[512];

	UI_64 result(0);
	SFSco::Object temp_obj;
	SFSco::InfoEntry i_entry;



// windows check =============================================================================================
	OSVERSIONINFOEX osVer;
	SecureZeroMemory(&osVer,sizeof(OSVERSIONINFOEX));
	osVer.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if (GetVersionEx((LPOSVERSIONINFO)&osVer)) {
		result = osVer.dwMajorVersion*100 + osVer.dwMinorVersion*10 + osVer.wServicePackMajor;
		if (result < 600) {
			result = 0x55504357;
			result <<= 32;
			result |= osVer.dwMajorVersion*100 + osVer.dwMinorVersion*10 + osVer.wServicePackMajor;
		} else {
			result = 0;
		}
	}




	if (!result) result = SFSco::Initialize();

	if (!result) {

		SFSco::SetLowPrecisionFloat(0);

		result = SFSco::File_Initialize();

		if (!result) {
			SFSco::GetHeaderStr(22, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, SFSco::Font_Initialize, 0, 0);
			SFSco::GetHeaderStr(23, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, SFSco::OpenGL_Initialize, 0, 0);
			SFSco::GetHeaderStr(24, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, MainWindow::Create, &temp_obj, (void *)1);
			SFSco::GetHeaderStr(25, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, SFSco::Image_Initialize, &MainWindow::main_ogl, 0);
			SFSco::GetHeaderStr(26, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, SFSco::VideoSource_Initialize, &MainWindow::main_ogl, 0);
			SFSco::GetHeaderStr(27, temp_str);
			Control::Launcher::AddInitializer(temp_str + 1, SFSco::AudioDevice_Initialize, 0, 0);

			SFSco::GetHeaderStr(30, temp_str);
			result = Control::Launcher::Run(200 | (201 << 16), temp_str + 1, &temp_obj);

			if ((result >> 40) == 0) { // result is init time
				// add result to info record
				GetSystemTimeAsFileTime(reinterpret_cast<LPFILETIME>(&i_entry.time_mark));
				i_entry.ie_val = result;
				i_entry.val_type = 3;
				i_entry.ie_type = SFSco::InfoEntry::LoadComplete;
				i_entry.x_id = -1;
				
				SFSco::AddIE(i_entry);

				SFSco::GetHeaderStr(28, temp_str);
				result = Control::Application::Create(temp_str + 1, APPLICATION_GLOBAL_SINGLE);

				if (!result) {
					MainWindow::Build(showcmd, temp_obj);
					
					Control::Application::Run(MainWindow::GetInstance());

					Control::Application::Close();
				}				

			} else {
				SFSco::FormatErrorMessage(result);
			}			

			SFSco::Event_ShutDown();

		succ_val = 1;
			SFSco::VideoSource_Finalize();
		succ_val = 2;
			SFSco::Image_Finalize();
		succ_val = 3;	
			MainWindow::Destroy();
		succ_val = 4;
			SFSco::Font_Finalize();
		}

	succ_val = 5;
		SFSco::File_Finalize();

	} else {
		SFSco::FormatErrorMessage(result);
	}
succ_val = 6;

	SFSco::Finalize();

	return result*succ_val;
}

