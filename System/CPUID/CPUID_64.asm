
; safe-fail base elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



	intel_procInfo				TEXTEQU	<0>		;			// 8
	intel_features				TEXTEQU	<8>		;			// 8
	intel_cacheParams			TEXTEQU	<16>	;			// 16 (256)
	intel_monitorParams			TEXTEQU	<272>	;			// 16
	intel_powerParams			TEXTEQU	<288>	;			// 12
	intel_dcaParams				TEXTEQU	<300>	;			// 4
	intel_performanceMonitor	TEXTEQU	<304>	;			// 12
	intel_procTopology			TEXTEQU	<316>	;			// 16 (256)
	intel_xsave					TEXTEQU	<572>	;			// 12

	intel_xFeatures				TEXTEQU	<584>	;			// 8
	intel_processorName			TEXTEQU	<592>	;			// 64
	intel_L2xFeatures			TEXTEQU	<656>	;			// 4
	intel_powerManagement		TEXTEQU	<660>	;			// 4
	intel_addressInfo			TEXTEQU	<664>	;			// 4


	amd_procInfo				TEXTEQU	<0>		;			// 8
	amd_features				TEXTEQU	<8>		;			// 8
	amd_monitorFeatures			TEXTEQU	<16>	;			// 16
	amd_powerFeatures			TEXTEQU	<32>	;			// 4
	amd_extendedFeatures		TEXTEQU	<36>	;			// 4
	amd_extendedFeatureState	TEXTEQU	<40>	;			// 16

	amd_xFeatureInfo			TEXTEQU	<56>	;			// 4
	amd_xFeatures				TEXTEQU	<60>	;			// 8

	amd_processorName			TEXTEQU	<68>	;			// 64

	amd_L1CacheInfo				TEXTEQU	<132>	;			// 16
	amd_L2CacheInfo				TEXTEQU	<148>	;			// 12
	amd_L3CacheInfo				TEXTEQU	<160>	;			// 4

	amd_powerManagement			TEXTEQU	<164>	;			// 4
	amd_longAddressIDs			TEXTEQU	<168>	;			// 4
	amd_coreInfo				TEXTEQU	<172>	;			// 4
	amd_svmInfo					TEXTEQU	<176>	;			// 12
	amd_tlb1GBInfo				TEXTEQU	<188>	;			// 8
	amd_performanceOptIDs		TEXTEQU	<196>	;			// 4
	amd_iSamplingInfo			TEXTEQU	<200>	;			// 4
	amd_profilingCapabilities	TEXTEQU	<204>	;			// 12

	amd_cacheProperties			TEXTEQU	<216>	;			// 16 (256)
	amd_exAPICInfo				TEXTEQU	<472>	;			// 12

	cpu_funcMAX					TEXTEQU	<0>
	cpu_eFuncMAX				TEXTEQU	<4>
	cpu_vendorString			TEXTEQU <8>

EXTERN ?sse_control@@3IA:DWORD	

.CODE

ALIGN 16
SetStdPrecisionFloat	PROC
	FINIT

	STMXCSR [rsp-8]
	MOV eax, [rsp-8]
	
	AND eax, 1FBFh
			
	AND ecx, 3
	MOV edx, ecx
	SHL edx, 13

	OR edx, eax

	MOV [rsp-8], edx			
	LDMXCSR [rsp-8]

	SHL ecx, 10
	OR ecx, 37Fh

	MOV [rsp-8], ecx
	FLDCW [rsp-8]

	RET
SetStdPrecisionFloat	ENDP


ALIGN 16
SetLowPrecisionFloat	PROC
	FINIT

	STMXCSR [rsp-8]
	MOV eax, [rsp-8]
	AND eax, 01FBFh
			
	AND ecx, 3
	MOV edx, ecx
	SHL edx, 13
	OR edx, 8040h

	OR edx, eax

	MOV [rsp-8], edx			
	LDMXCSR [rsp-8]

	SHL ecx, 10
	OR ecx, 27Fh

	MOV [rsp-8], ecx
	FLDCW [rsp-8]
	
	RET
SetLowPrecisionFloat	ENDP


ALIGN 16
RestoreFState	PROC
	LDMXCSR [?sse_control@@3IA]

	FINIT

	RET
RestoreFState	ENDP

ALIGN 16
Intel_ID		PROC
	PUSH rdi
	PUSH rbx	
		XOR rax, rax
		XOR r11, r11

		MOV rdi, rcx
		MOV r10, rdx
			
		ADD r11, 1
		CMP r11d, r10d
		JA efunctions

			MOV eax, r11d
			CPUID
		
			MOV [rdi+intel_procInfo], eax
			MOV [rdi+intel_procInfo+4], ebx				
			MOV [rdi+intel_features], ecx
			MOV [rdi+intel_features+4], edx


		ADD r11d, 3
		CMP r11d, r10d
		JA efunctions
	
			XOR r9, r9
			cacheploop:
				MOV rcx, r9
				MOV eax, r11d
			
				CPUID
				SHL r9, 4
			
				MOV [rdi+r9+intel_cacheParams], eax
				MOV [rdi+r9+intel_cacheParams+4], ebx					
				MOV [rdi+r9+intel_cacheParams+8], ecx
				MOV [rdi+r9+intel_cacheParams+12], edx
				
				SHR r9, 4
				ADD r9, 1
				AND eax, 0Fh
			JNZ cacheploop

		ADD r11d, 1
		CMP r11d, r10d
		JA efunctions

			MOV eax, r11d
			CPUID
				MOV [rdi+intel_monitorParams], eax
				MOV [rdi+intel_monitorParams+4], ebx
				MOV [rdi+intel_monitorParams+8], ecx
				MOV [rdi+intel_monitorParams+12], edx

		ADD r11d, 1
		CMP r11d, r10d
		JA efunctions

			MOV eax, r11d
			CPUID
			MOV [rdi+intel_powerParams], eax
			MOV [rdi+intel_powerParams+4], ebx
			MOV [rdi+intel_powerParams+8], ecx					

		ADD r11d, 3
		CMP r11d, r10d
		JA efunctions

			MOV eax, r11d
			CPUID
			MOV [rdi+intel_dcaParams], eax

		ADD r11d, 1
		CMP r11d, r10d
		JA efunctions
			
			MOV eax, r11d
			CPUID
			MOV [rdi+intel_performanceMonitor], eax
			MOV [rdi+intel_performanceMonitor+4], ebx			
			MOV [rdi+intel_performanceMonitor+8], edx

		ADD r11d, 1
		CMP r11d, r10d
		JA efunctions
		
			XOR r9, r9
			topologyloop:
				MOV rcx, r9
				MOV eax, r11d
				CPUID
				
				SHL r9, 4
				
				MOV [rdi+r9+intel_procTopology], eax
				MOV [rdi+r9+intel_procTopology+4], ebx					
				MOV [rdi+r9+intel_procTopology+8], ecx
				MOV [rdi+r9+intel_procTopology+12], edx
				
				SHR r9, 4
				ADD r9, 1
					
				OR eax, ebx
			JNZ topologyloop
	
		ADD r11d, 2
		CMP r11d, r10d
		JA efunctions
		
			XOR ecx, ecx
			MOV eax, r11d
			CPUID

			MOV [rdi+intel_xsave], eax
			MOV [rdi+intel_xsave+4], ebx			
			MOV [rdi+intel_xsave+8], ecx

		efunctions:
		
			MOV r11d, 80000001h
			CMP r11d, r8d
			JA exit
			
				MOV eax, r11d
				CPUID
				MOV [rdi+intel_xFeatures], edx
				MOV [rdi+intel_xFeatures+4], ecx			
					
				ADD r11d, 1
				CMP r11d, r8d
				JA exit

					MOV eax, r11d
					CPUID
					MOV [rdi+intel_processorName], eax
					MOV [rdi+intel_processorName+4], ebx		
					MOV [rdi+intel_processorName+8], ecx
					MOV [rdi+intel_processorName+12], edx

				ADD r11d, 1
				MOV eax, r11d
				CPUID
				MOV [rdi+intel_processorName+16], eax
				MOV [rdi+intel_processorName+20], ebx		
				MOV [rdi+intel_processorName+24], ecx
				MOV [rdi+intel_processorName+28], edx
				
				ADD r11d, 1
				MOV eax, r11d
				CPUID
				MOV [rdi+intel_processorName+32], eax
				MOV [rdi+intel_processorName+36], ebx		
				MOV [rdi+intel_processorName+40], ecx
				MOV [rdi+intel_processorName+44], edx

				ADD r11d, 2
				CMP r11d, r8d
				JA exit

					MOV eax, r11d
					CPUID
					MOV [rdi+intel_L2xFeatures], ecx


				ADD r11d, 1
				CMP r11d, r8d
				JA exit

					MOV eax, r11d
					CPUID
					MOV [rdi+intel_powerManagement], edx


				ADD r11d, 1
				CMP r11d, r8d
				JA exit

					MOV eax, r11d
					CPUID
					MOV [rdi+intel_addressInfo], eax

	exit:
	POP rbx
	POP rdi
		RET
Intel_ID ENDP



ALIGN 16

AMD_ID	PROC
	PUSH r15
	PUSH rbx

	XOR rax, rax
	XOR r15, r15

	MOV r11, rcx
	MOV r10, rdx
		
	ADD r15d, 1
	CMP r15d, r10d
	JA efunctions

		MOV eax, r15d
		CPUID
		MOV [r11+amd_procInfo], eax
		MOV [r11+amd_procInfo+4], ebx			
		MOV [r11+amd_features], ecx
		MOV [r11+amd_features+4], edx

	ADD r15d, 4
	CMP r15d, r10d
	JA efunctions
			
		MOV eax, r15d
		CPUID
		MOV [r11+amd_monitorFeatures], eax
		MOV [r11+amd_monitorFeatures+4], ebx			
		MOV [r11+amd_monitorFeatures+8], ecx
					

	ADD r15d, 1
	CMP r15d, r10d
	JA efunctions
			
		MOV eax, r15d
		CPUID
		MOV [r11+amd_powerFeatures], ecx

	ADD r15d, 1
	CMP r15d, r10d
	JA efunctions
			
		XOR ecx, ecx
		MOV eax, r15d
		CPUID
		MOV [r11+amd_extendedFeatures], ebx

	ADD r15d, 6
	CMP r15d, r10d
	JA efunctions

		XOR ecx, ecx
		MOV eax, r15d
		CPUID
		MOV [r11+amd_extendedFeatureState], eax
		MOV [r11+amd_extendedFeatureState+4], edx			
		MOV [r11+amd_extendedFeatureState+8], ebx
		MOV [r11+amd_extendedFeatureState+12], ecx



	efunctions:
	
		MOV r15d, 80000001h			
		CMP r15d, r8d
		JA exit
			
			MOV eax, r15d
			CPUID
			MOV [r11+amd_xFeatureInfo], ebx
			MOV [r11+amd_xFeatures], ecx			
			MOV [r11+amd_xFeatures+4], edx


		ADD r15d, 1
		CMP r15d, r8d
		JA exit

			MOV eax, r15d
			CPUID
			MOV [r11+amd_processorName], eax
			MOV [r11+amd_processorName+4], ebx		
			MOV [r11+amd_processorName+8], ecx
			MOV [r11+amd_processorName+12], edx

			ADD r15d, 1
			MOV eax, r15d
			CPUID
			MOV [r11+amd_processorName+16], eax
			MOV [r11+amd_processorName+20], ebx		
			MOV [r11+amd_processorName+24], ecx
			MOV [r11+amd_processorName+28], edx
			
			ADD r15d, 1
			MOV eax, r15d
			CPUID
			MOV [r11+amd_processorName+32], eax
			MOV [r11+amd_processorName+36], ebx		
			MOV [r11+amd_processorName+40], ecx
			MOV [r11+amd_processorName+44], edx

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_L1CacheInfo], eax
				MOV [r11+amd_L1CacheInfo+4], ebx		
				MOV [r11+amd_L1CacheInfo+8], ecx
				MOV [r11+amd_L1CacheInfo+12], edx

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_L2CacheInfo], eax
				MOV [r11+amd_L2CacheInfo+4], ebx		
				MOV [r11+amd_L2CacheInfo+8], ecx
				MOV [r11+amd_L3CacheInfo], edx

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_powerManagement], edx

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_longAddressIDs], eax
				MOV [r11+amd_coreInfo], ecx

			ADD r15d, 2
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_svmInfo], ebx
				MOV [r11+amd_svmInfo+4], eax
				MOV [r11+amd_svmInfo+8], edx

			ADD r15d, 15
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_tlb1GBInfo], eax
				MOV [r11+amd_tlb1GBInfo+4], ebx		

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_performanceOptIDs], eax

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_iSamplingInfo], eax

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_profilingCapabilities], eax
				MOV [r11+amd_profilingCapabilities+4], ebx
				MOV [r11+amd_profilingCapabilities+8], ecx

			ADD r15d, 1
			CMP r15d, r8d
			JA exit
				XOR r9, r9
				cacheploop:
					MOV rcx, r9
					MOV eax, r15d
					CPUID
					
					SHL r9, 4
					MOV [r11+r9+amd_cacheProperties], eax
					MOV [r11+r9+amd_cacheProperties+4], ebx		
					MOV [r11+r9+amd_cacheProperties+8], ecx
					MOV [r11+r9+amd_cacheProperties+12], edx
					SHR r9, 4
						
					ADD r9, 1
					AND eax, 0Fh
				JNZ cacheploop
				

			ADD r15d, 1
			CMP r15d, r8d
			JA exit

				MOV eax, r15d
				CPUID
				MOV [r11+amd_exAPICInfo], eax
				MOV [r11+amd_exAPICInfo+4], ebx
				MOV [r11+amd_exAPICInfo+8], ecx



	exit:
	POP rbx
	POP r15
		RET
AMD_ID	ENDP

ALIGN 16

CpuFuncCount PROC
	STMXCSR [?sse_control@@3IA]
		
	MOV r8, rbx

	MOV r9, rcx
	XOR eax, eax
	CPUID

	MOV [r9+cpu_funcMAX], eax
	MOV [r9+cpu_vendorString], ebx
	MOV [r9+cpu_vendorString+4], edx
	MOV [r9+cpu_vendorString+8], ecx
	
	MOV eax, 80000000h
	CPUID			
	MOV [r9+cpu_eFuncMAX], eax
	
	MOV rbx, r8

	RET
CpuFuncCount ENDP

END