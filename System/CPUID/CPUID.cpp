/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\CPUID.h"

#include "System\SysUtils.h"



char idcpu_location[4096];

unsigned int sse_control = -1;


IDCPU * IDCPU::_instance = 0;
/*
IDCPU::IDCPU() : _funcMAX(0),_eFuncMAX(0),_amdID(0),_intelID(0) {
	MemoryFill(_vendorString, 16, 0);
	MemoryFill(&_cpuVals, sizeof(stCPUNumbers), 0);

}

IDCPU::~IDCPU() {
	if (_amdID) delete _amdID;
	if (_intelID) delete _intelID;

}
*/
int IDCPU::Initialize() {
	int result(0);
	if (_instance) return -1;

	System::MemoryFill(idcpu_location, 4096, 0);
// test for IDCPU

#ifndef _WIN64
	__asm {		

		PUSHFD
		POP eax
		MOV edx, eax
		XOR edx, 00200000h
		PUSH edx
		POPFD
		PUSHFD
		POP edx
		XOR edx, eax
		SETE al
		MOVZX eax, al
		JE Exit

		STMXCSR [sse_control]

	}
#endif
	_instance = reinterpret_cast<IDCPU *>(idcpu_location);

#ifdef _WIN64
	CpuFuncCount(_instance);

#else
	__asm {
		PUSH esi
			MOV esi, [_instance]

			XOR eax, eax
			CPUID

				MOV [esi+IDCPU::_funcMAX], eax
				MOV [esi+IDCPU::_vendorString], ebx
				MOV [esi+IDCPU::_vendorString+4], edx
				MOV [esi+IDCPU::_vendorString+8], ecx
	
			MOV eax, 80000000h
			CPUID			
				MOV [esi+IDCPU::_eFuncMAX], eax



		POP esi
	}
#endif

	if (Strings::SearchAStr(_instance->_vendorString, "AMD")) {
		_instance->_amdID = new clAMDID(_instance->_funcMAX,_instance->_eFuncMAX);
		if (_instance->_amdID) {
			_instance->_cpuVals.physicalCount = _instance->_amdID->_coreInfo.coreCount+1;
			_instance->_cpuVals.logicalCount = 1;
			if (_instance->_amdID->_features.HTT) _instance->_cpuVals.logicalCount = _instance->_amdID->_procInfo.logicalProcessorCount/_instance->_cpuVals.physicalCount;

			_instance->_cpuVals.L1.rouphness = _instance->_amdID->_L1CacheInfo.dLineSize;
			_instance->_cpuVals.L1.wayCount = _instance->_amdID->_L1CacheInfo.dAssociativity;
			_instance->_cpuVals.L1.capacity = _instance->_amdID->_L1CacheInfo.dSize*1024;
			_instance->_cpuVals.L1.curvature = _instance->_cpuVals.L1.capacity/_instance->_cpuVals.L1.wayCount;
			_instance->_cpuVals.L1.capacity /= _instance->_cpuVals.logicalCount;

			_instance->_cpuVals.L2.rouphness = _instance->_amdID->_L2CacheInfo.lineSize;
			_instance->_cpuVals.L2.wayCount = 1;
			switch (_instance->_amdID->_L2CacheInfo.associativity) {
				case AMD::direct: _instance->_cpuVals.L2.wayCount = 1; break;
				case AMD::IIway: _instance->_cpuVals.L2.wayCount = 2; break;
				case AMD::IVway: _instance->_cpuVals.L2.wayCount = 4; break;
				case AMD::VIIIway: _instance->_cpuVals.L2.wayCount = 8; break;
				case AMD::XVIway: _instance->_cpuVals.L2.wayCount = 16; break;
				case AMD::XXXIIway: _instance->_cpuVals.L2.wayCount = 32; break;
				case AMD::XLVIIIway: _instance->_cpuVals.L2.wayCount = 48; break;
				case AMD::LXIVway: _instance->_cpuVals.L2.wayCount = 64; break;
				case AMD::XCVIway: _instance->_cpuVals.L2.wayCount = 96; break;
				case AMD::CXXVIIIway: _instance->_cpuVals.L2.wayCount = 128; break;

			}

			_instance->_cpuVals.L2.capacity = _instance->_amdID->_L2CacheInfo.size*1024;
			_instance->_cpuVals.L2.curvature = _instance->_cpuVals.L2.capacity/_instance->_cpuVals.L2.wayCount;
			_instance->_cpuVals.L2.capacity /= _instance->_cpuVals.logicalCount;
		
		} else {
			result = 3;
		}
	} else {

		if (Strings::SearchAStr(_instance->_vendorString, "Intel")) {

			_instance->_intelID = new clIntelID(_instance->_funcMAX,_instance->_eFuncMAX);

			if (_instance->_intelID) {
				for (int i(0);i<16;i++) {
					if (_instance->_intelID->_procTopology[i].levelType != Intel::Invalid) {
						if (_instance->_intelID->_procTopology[i].levelType == Intel::Thread) _instance->_cpuVals.logicalCount = _instance->_intelID->_procTopology[i].levelLogicalProcCount;
						if (_instance->_intelID->_procTopology[i].levelType == Intel::Core) _instance->_cpuVals.physicalCount = _instance->_intelID->_procTopology[i].levelLogicalProcCount;
			
					} else {
						break;
					}
				}

				if (_instance->_cpuVals.logicalCount) {
					_instance->_cpuVals.physicalCount /= _instance->_cpuVals.logicalCount;

					for (int i(0);i<16;i++) {					
						if (_instance->_intelID->_cacheParams[i].type == Intel::Data) {
							switch (_instance->_intelID->_cacheParams[i].level) {
								case 1:
									_instance->_cpuVals.L1.rouphness = _instance->_intelID->_cacheParams[i].coherencyLineSize+1;
									_instance->_cpuVals.L1.wayCount = _instance->_intelID->_cacheParams[i].waysOfAssociativity+1;
									_instance->_cpuVals.L1.curvature = _instance->_cpuVals.L1.rouphness*(_instance->_intelID->_cacheParams[i].physLinePartitions+1)*(_instance->_intelID->_cacheParams[i].numOfSets+1);
									_instance->_cpuVals.L1.capacity = _instance->_cpuVals.L1.wayCount*_instance->_cpuVals.L1.curvature/_instance->_cpuVals.logicalCount;
								break;
								case 2:
									_instance->_cpuVals.L2.rouphness = _instance->_intelID->_cacheParams[i].coherencyLineSize+1;
									_instance->_cpuVals.L2.wayCount = _instance->_intelID->_cacheParams[i].waysOfAssociativity+1;
									_instance->_cpuVals.L2.curvature = _instance->_cpuVals.L2.rouphness*(_instance->_intelID->_cacheParams[i].physLinePartitions+1)*(_instance->_intelID->_cacheParams[i].numOfSets+1);
									_instance->_cpuVals.L2.capacity = _instance->_cpuVals.L2.wayCount*_instance->_cpuVals.L2.curvature/_instance->_cpuVals.logicalCount;

								break;
								case 3:

								break;
							}

						} else {
							if (_instance->_intelID->_cacheParams[i].type == Intel::Null) break;
						}
					}

				} else {
					_instance->_cpuVals.physicalCount = _instance->_cpuVals.logicalCount = 1;
					if (_instance->_intelID->_procInfo.maxCOREcount) _instance->_cpuVals.logicalCount = _instance->_intelID->_procInfo.maxCOREcount;
					
				}

			} else {
				result = 3;
			}

		}
	
	}


#ifndef _WIN64
Exit:;
#endif

	return result;
}

#ifndef _WIN64
	void IDCPU::SetLowPrecisionFloat(unsigned int roundC) {
		__asm {
			FINIT

			STMXCSR [esp-4]
			MOV eax, [esp-4]
			AND eax, 01FBFh
			
			MOV ecx, [roundC]
			AND ecx, 3
			MOV edx, ecx
			SHL edx, 13
			OR edx, 8040h

			OR edx, eax

			MOV [esp-4], edx			
			LDMXCSR [esp-4]

			SHL ecx, 10
			OR ecx, 27Fh

			MOV [esp-4], ecx
			FLDCW [esp-4]

		}

	}


	void IDCPU::SetStdPrecisionFloat(unsigned int roundC) {
		__asm {
			FINIT

			STMXCSR [esp-4]
			MOV eax, [esp-4]
	
			AND eax, 1FBFh
			
			MOV ecx, [roundC]
			AND ecx, 3
			MOV edx, ecx
			SHL edx, 13

			OR edx, eax

			MOV [esp-4], edx			
			LDMXCSR [esp-4]

			SHL ecx, 10
			OR ecx, 37Fh

			MOV [esp-4], ecx
			FLDCW [esp-4]

		}

	}


#endif

void IDCPU::Finalize() {
	_instance = 0;

#ifdef _WIN64
	RestoreFState();

#else
	__asm {
		LDMXCSR [sse_control]

		FINIT
	}
#endif

}

bool IDCPU::tossRand() {
	if (_instance->_amdID) return 0;
	else return _instance->_intelID->_features.RDRAND;
}

bool IDCPU::tossSSE3() {
	if (_instance->_amdID) return _instance->_amdID->_features.SSE3;
	else return _instance->_intelID->_features.SSE3;
}

bool IDCPU::tossAVX() {
	if (_instance->_amdID) return _instance->_amdID->_features.AVX;
	else return _instance->_intelID->_features.AVX;
}

bool IDCPU::tossFPU() {
	if (_instance->_amdID) return _instance->_amdID->_features.FPU;
	else return _instance->_intelID->_features.FPU;
}

bool IDCPU::tossSSSE3() {
	if (_instance->_amdID) return (_instance->_amdID->_features.SSSE3 | _instance->_amdID->_features.AVX);
	else return (_instance->_intelID->_features.SSSE3 | _instance->_intelID->_features.AVX);
}

bool IDCPU::tossSSE41() {
	if (_instance->_amdID) return (tossSSSE3() && _instance->_amdID->_features.SSE4_1);
	else return (tossSSSE3() && _instance->_intelID->_features.SSE4_1);
}

bool IDCPU::tossSSE42() {
	if (_instance->_amdID) return (tossSSE41() && _instance->_amdID->_features.SSE4_2);
	else return (tossSSE41() && _instance->_intelID->_features.SSE4_2);
}

bool IDCPU::tossAES() {
	if (_instance->_amdID) return _instance->_amdID->_features.AES;
	else return _instance->_intelID->_features.AES;
}

bool IDCPU::toosX64() {
	if (_instance->_amdID) return _instance->_amdID->_xFeatures.LM;
	else return _instance->_intelID->_xFeatures.I64;
}

bool IDCPU::tossCLF() {
	if (_instance->_amdID) return _instance->_amdID->_features.CLFSH;
	else return _instance->_intelID->_features.CLFSH;
}

unsigned int IDCPU::GetPhysicalCount() {
	return _instance->_cpuVals.physicalCount;
}

const stCacheDesc & IDCPU::GetL1Config() {
	return _instance->_cpuVals.L1;
}

const stCacheDesc & IDCPU::GetL2Config() {
	return _instance->_cpuVals.L2;
}

size_t IDCPU::GetCoreMask(unsigned int cori) {
	size_t result = (_instance->_cpuVals.logicalCount<<1) - 1;
	size_t mpl(result);
	
	for (unsigned int i(1);;i++) {
		if ((mpl >>= 1) == 0) {
			mpl = i;
			break;
		}
	}

	return (result << ((cori % _instance->_cpuVals.physicalCount)*mpl));
	
}
// Intel ID ===========================================================================================================================================================
void * clIntelID::operator new (size_t ss) {
	return idcpu_location + 1024;
}

clIntelID::clIntelID(unsigned int fmax, unsigned int emax) {
#ifdef _WIN64
	Intel_ID(this, fmax, emax);

#else
	__asm {
		XOR eax, eax
		PUSH eax
			MOV esi, [this]

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_procInfo], eax
					MOV [esi+clIntelID::_procInfo+4], ebx
					MOV [esi+clIntelID::_features], ecx
					MOV [esi+clIntelID::_features+4], edx


			ADD DWORD PTR [esp], 3
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
				XOR edi, edi
				cacheploop:
					MOV ecx, edi
					MOV eax, [esp]
					CPUID
						SHL edi, 4
						MOV [esi+edi+clIntelID::_cacheParams], eax
						MOV [esi+edi+clIntelID::_cacheParams+4], ebx					
						MOV [esi+edi+clIntelID::_cacheParams+8], ecx
						MOV [esi+edi+clIntelID::_cacheParams+12], edx
						SHR edi, 4
					ADD edi, 1
					AND eax, 0Fh
				JNZ cacheploop

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_monitorParams], eax
					MOV [esi+clIntelID::_monitorParams+4], ebx
					MOV [esi+clIntelID::_monitorParams+8], ecx
					MOV [esi+clIntelID::_monitorParams+12], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_powerParams], eax
					MOV [esi+clIntelID::_powerParams+4], ebx
					MOV [esi+clIntelID::_powerParams+8], ecx					

			ADD DWORD PTR [esp], 3
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_dcaParams], eax

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
			
				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_performanceMonitor], eax
					MOV [esi+clIntelID::_performanceMonitor+4], ebx			
					MOV [esi+clIntelID::_performanceMonitor+8], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
				XOR edi, edi
				topologyloop:
					MOV ecx, edi
					MOV eax, [esp]
					CPUID
						SHL edi, 4
						MOV [esi+edi+clIntelID::_procTopology], eax
						MOV [esi+edi+clIntelID::_procTopology+4], ebx					
						MOV [esi+edi+clIntelID::_procTopology+8], ecx
						MOV [esi+edi+clIntelID::_procTopology+12], edx
						SHR edi, 4
					ADD edi, 1
					OR eax, ebx
				JNZ topologyloop
	
			ADD DWORD PTR [esp], 2
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
				XOR ecx, ecx
				MOV eax, [esp]
				CPUID
					MOV [esi+clIntelID::_xsave], eax
					MOV [esi+clIntelID::_xsave+4], ebx			
					MOV [esi+clIntelID::_xsave+8], ecx

			efunctions:
				MOV edi, 80000001h
				CMP edi, [emax]
				JA exit
				MOV [esp], edi
			
					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_xFeatures], edx
						MOV [esi+clIntelID::_xFeatures+4], ecx			
					
				ADD DWORD PTR [esp], 1
				MOV edi, [esp]
				CMP edi, [emax]
				JA exit

					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_processorName], eax
						MOV [esi+clIntelID::_processorName+4], ebx		
						MOV [esi+clIntelID::_processorName+8], ecx
						MOV [esi+clIntelID::_processorName+12], edx

				ADD DWORD PTR [esp], 1
					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_processorName+16], eax
						MOV [esi+clIntelID::_processorName+20], ebx		
						MOV [esi+clIntelID::_processorName+24], ecx
						MOV [esi+clIntelID::_processorName+28], edx
				ADD DWORD PTR [esp], 1
					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_processorName+32], eax
						MOV [esi+clIntelID::_processorName+36], ebx		
						MOV [esi+clIntelID::_processorName+40], ecx
						MOV [esi+clIntelID::_processorName+44], edx

				ADD DWORD PTR [esp], 2
				MOV edi, [esp]
				CMP edi, [emax]
				JA exit

					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_L2xFeatures], ecx


				ADD DWORD PTR [esp], 1
				MOV edi, [esi]
				CMP edi, [emax]
				JA exit

					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_powerManagement], edx


				ADD DWORD PTR [esp], 1
				MOV edi, [esp]
				CMP edi, [emax]
				JA exit

					MOV eax, [esp]
					CPUID
						MOV [esi+clIntelID::_addressInfo], eax

	exit:

		POP eax
		
	}
#endif

}
/*
clIntelID::~clIntelID() {

}
*/
// AMD ID =============================================================================================================================================================
void * clAMDID::operator new(size_t ss) {
	return idcpu_location + 1024;
}

clAMDID::clAMDID(unsigned int fmax, unsigned int emax) {
//	int xxx = sizeof(Intel::stXFeatureList);
#ifdef _WIN64
	AMD_ID(this, fmax, emax);

#else
	__asm {
		
		XOR eax, eax
		PUSH eax
			MOV esi, [this]

			ADD [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_procInfo], eax
					MOV [esi+clAMDID::_procInfo+4], ebx			
					MOV [esi+clAMDID::_features], ecx
					MOV [esi+clAMDID::_features+4], edx

			ADD DWORD PTR [esp], 4
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
			
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_monitorFeatures], eax
					MOV [esi+clAMDID::_monitorFeatures+4], ebx			
					MOV [esi+clAMDID::_monitorFeatures+8], ecx
					

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions
			
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_powerFeatures], ecx

			ADD DWORD PTR [esp], 1
			MOV edi, [esi]
			CMP edi, [fmax]
			JA efunctions
			
				XOR ecx, ecx
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_extendedFeatures], ebx

			ADD DWORD PTR [esp], 6
			MOV edi, [esp]
			CMP edi, [fmax]
			JA efunctions

				XOR ecx, ecx
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_extendedFeatureState], eax
					MOV [esi+clAMDID::_extendedFeatureState+4], edx			
					MOV [esi+clAMDID::_extendedFeatureState+8], ebx
					MOV [esi+clAMDID::_extendedFeatureState+12], ecx



		efunctions:
			MOV edi, 80000001h			
			CMP edi, [emax]
			JA exit
			MOV [esp], edi
			
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_xFeatureInfo], ebx
					MOV [esi+clAMDID::_xFeatures], ecx			
					MOV [esi+clAMDID::_xFeatures+4], edx


			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_processorName], eax
					MOV [esi+clAMDID::_processorName+4], ebx		
					MOV [esi+clAMDID::_processorName+8], ecx
					MOV [esi+clAMDID::_processorName+12], edx

			ADD DWORD PTR [esp], 1
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_processorName+16], eax
					MOV [esi+clAMDID::_processorName+20], ebx		
					MOV [esi+clAMDID::_processorName+24], ecx
					MOV [esi+clAMDID::_processorName+28], edx
			ADD DWORD PTR [esp], 1
				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_processorName+32], eax
					MOV [esi+clAMDID::_processorName+36], ebx		
					MOV [esi+clAMDID::_processorName+40], ecx
					MOV [esi+clAMDID::_processorName+44], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_L1CacheInfo], eax
					MOV [esi+clAMDID::_L1CacheInfo+4], ebx		
					MOV [esi+clAMDID::_L1CacheInfo+8], ecx
					MOV [esi+clAMDID::_L1CacheInfo+12], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_L2CacheInfo], eax
					MOV [esi+clAMDID::_L2CacheInfo+4], ebx		
					MOV [esi+clAMDID::_L2CacheInfo+8], ecx
					MOV [esi+clAMDID::_L3CacheInfo], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_powerManagement], edx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_longAddressIDs], eax
					MOV [esi+clAMDID::_coreInfo], ecx

			ADD DWORD PTR [esp], 2
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_svmInfo], ebx
					MOV [esi+clAMDID::_svmInfo+4], eax
					MOV [esi+clAMDID::_svmInfo+8], edx

			ADD DWORD PTR [esp], 15
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_tlb1GBInfo], eax
					MOV [esi+clAMDID::_tlb1GBInfo+4], ebx		

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_performanceOptIDs], eax

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_iSamplingInfo], eax

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_profilingCapabilities], eax
					MOV [esi+clAMDID::_profilingCapabilities+4], ebx
					MOV [esi+clAMDID::_profilingCapabilities+8], ecx

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit
				XOR edi, edi
				cacheploop:
					MOV ecx, edi
					MOV eax, [esp]
					CPUID
						SHL edi, 4
						MOV [esi+edi+clAMDID::_cacheProperties], eax
						MOV [esi+edi+clAMDID::_cacheProperties+4], ebx		
						MOV [esi+edi+clAMDID::_cacheProperties+8], ecx
						MOV [esi+edi+clAMDID::_cacheProperties+12], edx
						SHR edi, 4
						
					ADD edi, 1
					AND eax, 0Fh
				JNZ cacheploop
				

			ADD DWORD PTR [esp], 1
			MOV edi, [esp]
			CMP edi, [emax]
			JA exit

				MOV eax, [esp]
				CPUID
					MOV [esi+clAMDID::_exAPICInfo], eax
					MOV [esi+clAMDID::_exAPICInfo+4], ebx
					MOV [esi+clAMDID::_exAPICInfo+8], ecx



	exit:
		POP eax
	}
#endif
}

/*
clAMDID::~clAMDID() {


}
*/
