/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include <Windows.h>

#include "CoreLibrary\SFScore.h"

#define MAX_POOL_COUNT	64

namespace Memory {



class Buffer {
	private:		
		static UI_64 _largePageSize;		
		
		UI_64 _rSize; 
		unsigned int _type, _cType;
		void * _pointers[40];	
		
	protected:		
		static unsigned int _eval;
		static UI_64 _granularity;

		UI_64 _pageSize, _scale;
		UI_64  _cSize;
		char * _node;

	public:
		static const UI_64 type_id = 1000003;

		Buffer();
		virtual ~Buffer();

		UI_64 GetSize() const;
		virtual void * GetPointer(UI_64 offset, UI_64 size);
		virtual void ReleasePointer(void *&);

		virtual void GetSomeVal(void *, UI_64 size, UI_64 offset);
		virtual void SetSomeVal(void *, UI_64 size, UI_64 offset);
		
		virtual int ReSize(UI_64);		

		virtual UI_64 Init(UI_64 siz, const wchar_t * = 0);

		static int Initialize();
		static void Finalize();

		static unsigned int BugCheck();

};

class FileBuffer : public Buffer {
	private:
		HANDLE f_handle, map_handle;

	public:
		static const UI_64 type_id = 1000033*Buffer::type_id;

		FileBuffer();
		virtual ~FileBuffer();
				
		virtual void * GetPointer(UI_64 offset, UI_64 size);
		virtual void ReleasePointer(void *&);

		virtual void GetSomeVal(void *, UI_64 size, UI_64 offset);
		virtual void SetSomeVal(void *, UI_64 size, UI_64 offset);
		
		virtual int ReSize(UI_64);		

		virtual UI_64 Init(UI_64 siz, const wchar_t *);

		static int Initialize();
		static void Finalize();



};




class Chain {
	private:		
		static __int64 * lock_ptr;
		static __int64 lock_count;

		struct MapTableEntry {
			UI_64 offset;
			UI_64 size;
			UI_64 type;
			UI_64 access_mark;

			int ref_count, flags;
			void * view_ptr;
			UI_64 next_id;
			UI_64 back_id;

			UI_64 x_left, x_right;
			UI_64 x_up, x_down;
		
			unsigned int reserved[8];
		};		

		struct ChainInfo {
			UI_64 cid; // SFSCHAIN
			UI_64 map_offset;
			UI_64 map_top;
			UI_64 ceiling;

			UI_64 length;
			MapTableEntry * map_table;
			UI_64 * empty_slot;
			UI_64 * free_slot;

			UI_64 sha512[8];

		} chain_info;
		
		UI_64 service_flags, _eval;
		__int64 * ptr_lock, id_val;
		Buffer * _buffer;

/*
		UI_64 _bu_reserved[13];

		__int64 _memory_block, _mb_reserved[31];
		__int64 _service_block, _sb_reserved[31];
		__int64 _search_block, _bs_reserved[31];
*/		


		UI_64 SearchLink(UI_64);
		UI_64 ParentId(UI_64);
		
		unsigned int AddFreeSlot(UI_64, void * = 0);
		void AddEmptyLink(UI_64);
	
		UI_64 FindaSlot(UI_64 &);

		UI_64 BufferSize(UI_64);
		void SpecialResize(UI_64);

		void ServiceLock();
		void ServiceUnlock();
		
	protected:		
		
		void ShiftRightTo(UI_64, UI_64);
		void ShiftLeftTo(UI_64, UI_64);


	public:
		enum LinkFlags : int {

			Removed =	0x40000000,
			Remove =	0x20000000,
			XClusive =	0x10000000,
			FreeBlock =	0x08000000,			

//			Crypted =	0x00010000, // aes128
			Packed =	0x00100000, // deflate			
			
			XType =		0x00000001
		};

		enum StateFlags : UI_64 {
			XChain =		0x00000001,
			FChain =		0x00000002,
			SafeChain =		0x00000004

		};

		static const UI_64 type_id = 1000037;
		static const UI_64 map_table_tid = 961749101;
		static const UI_64 free_slot_tid = 4022940821;
		static const UI_64 empty_slot_tid = 4022940967;

		static const UI_64 undefined_tid = 982451653;

		static UI_64 Initialize();
		static void Finalize();
		static __int64 * IncLock(const wchar_t * = 0, I_64 * = 0);

		static void RegisterThread(I_64 *);
		static void UnregisterThread();

		Chain(UI_64);
		virtual ~Chain();

		virtual UI_64 Init(Buffer *, UI_64, const wchar_t * = 0, const wchar_t * = 0);
		UI_64 GetLength();

		bool IsLinkValid(UI_64);

		UI_64 GetLinkSize(UI_64 id);
		UI_64 GetLinkType(UI_64 id);
		
		int SetLinkFlags(UI_64 id, int flg);
		int TestLinkFlags(UI_64 id, int flg);
		int GetLinkFlags(UI_64 id);

		int GetLinkRC(UI_64 id);
		void LinkSpinWait(UI_64, int);

		bool VerifyLinkType(UI_64 id, UI_64 tid);

		UI_64 NextLink(UI_64 id);
		UI_64 BackLink(UI_64 id);
		UI_64 DownLink(UI_64);
		UI_64 UpLink(UI_64);
		UI_64 LeftLink(UI_64);
		UI_64 RightLink(UI_64);

		void WaitLink(UI_64);

		int GetLink(void *&, UI_64);
		int FreeLink(UI_64);
		
		int RemoveLink(UI_64 Link_index);

		UI_64 GetFirst(UI_64 type_id);
		UI_64 GetNext(UI_64);
		UI_64 GetPrev(UI_64);

		UI_64 ResizeLink(UI_64 Link_index, UI_64 new_size);
		UI_64 InsertLink(UI_64 Link_size, UI_64 tid, UI_64 parent = -1);

		void * GetLinkPtr(UI_64);

		void Lock();
		void Unlock(); // int
		void SpinWait();
				
};

class Allocator: public Chain, public SFSco::IAllocator {
	private:
		virtual int IncRef(UI_64, void *&);
		virtual int DecRef(UI_64);

		virtual int SetFlags(UI_64, int);
		virtual int GetFlags(UI_64);
		
		virtual void ShiftRight(UI_64, UI_64);
		virtual void ShiftLeft(UI_64, UI_64);
		
		virtual UI_64 Next(UI_64);
		virtual UI_64 Back(UI_64);
		virtual UI_64 Left(UI_64);
		virtual UI_64 Right(UI_64);
		virtual UI_64 Down(UI_64);
		virtual UI_64 Up(UI_64);
			

	public:
		static const UI_64 type_id = 1000039*Chain::type_id;
		
		virtual UI_64 Alloc(UI_64 newsize, UI_64 type_id, UI_64 parent = -1);
		virtual UI_64 Realloc(UI_64 id, UI_64 newsize);
		virtual UI_64 Expand(UI_64 id, UI_64 esize);

		virtual int Free(UI_64 id);

		virtual UI_64 GetSize(UI_64 id);
		virtual UI_64 GetTypeID(UI_64 id);

		virtual int GetRefCount(UI_64 id);
		virtual void SpinWaitRC(UI_64 id, int limit);

		virtual bool IsValid(UI_64);

		virtual void Wait(UI_64 id);
		
		Allocator(UI_64);
		virtual ~Allocator();

};


}


