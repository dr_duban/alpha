/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include <Windows.h>
#include "CoreLibrary\SFScore.h"


namespace Memory {

class FileBuffer {
	private:
		static unsigned int _eval;
		static UI_64 _granularity;

		UI_64 _rSize;
		HANDLE _handle, map_handle;		
		
	protected:
		HANDLE GetMH() const;
		UI_64 GetSize() const;
		int ReSize(UI_64);
		
		virtual int Init(UI_64, const wchar_t *);

		FileBuffer();
		virtual ~FileBuffer();

		UI_64 SetSomeVal(UI_64 offset, UI_64 val_size, void * val_ptr);

	public:
		static const UI_64 type_id = 11;

		static unsigned int BugCheck();
		
};



struct MapTableEntry {	

	UI_64 offset;
	UI_64 size;
	UI_64 type;
	UI_64 flags;

	I_64 ref_count;
	UI_64 access_mark;
	UI_64 next_id;
	UI_64 back_id;

	UI_64 xleft_id, xright_id;
	UI_64 xup_id, xdown_id;

	void * view_ptr;
	UI_64 reserved[3];

	unsigned int sha256[8];
};


struct ChainInfo {
	UI_64 cid; // SFSCHAIN
	UI_64 map_offset;
	UI_64 map_top;
	MapTableEntry * map_table;

	UI_64 ceiling;
	UI_64 length;
	UI_64 * empty_slot;
	UI_64 * free_slot;

	UI_64 sha512[8];

};



class FileChain: public FileBuffer {
	private:
		__int64 _memory_block, _service_block, _search_block, _state;

		UI_64 reserved_x;
		UI_64 service_flags;

		UI_64 ParentId(UI_64);
		
		unsigned int AddFreeSlot(UI_64);
		void AddEmptyLink(UI_64);
	
		UI_64 FindaSlot(UI_64 &);

		void SpecialResize(UI_64);	


	protected:
		ChainInfo chain_info;

		UI_64 GetState();
		UI_64 GetLink(void *&, UI_64, UI_64);
		void FreeLink(UI_64);

		void BufferSize(UI_64);

//		void ShiftRightTo(FileLinkHeader &, UI_64);
//		void ShiftLeftTo(FileLinkHeader &, UI_64);

	public:
		static const UI_64 type_id = 1031*FileBuffer::type_id;
		static const UI_64 map_table_tid = 961749101;
		static const UI_64 free_slot_tid = 961749121;
		static const UI_64 empty_slot_tid = 961749157;
		void Lock();
		void Unlock();

		FileChain();
		virtual ~FileChain();

		virtual int Init(UI_64, const wchar_t *);
		
	
		UI_64 GetLength();
		
		void * RemoveLink(UI_64 Link_index);

		UI_64 GetLinkSize(UI_64);
		UI_64 GetLinkType(UI_64);
		UI_64 SetLinkFlags(UI_64, UI_64);
		UI_64 TestLinkFlags(UI_64, UI_64);
		
		UI_64 DecLinkRefCount(UI_64);
		UI_64 IncLinkRefCount(UI_64);

		UI_64 NextLink(UI_64);
		UI_64 BackLink(UI_64);


		UI_64 ResizeLink(UI_64 Link_index, UI_64 new_size);
		UI_64 InsertLink(UI_64 Link_size, UI_64 tid = -1);

				
};




class FileAllocator: public FileChain, public SFSco::IAllocator {
	private:
		virtual __int64 IncRef(UI_64, void *&, __int64 &, UI_64 &);
		virtual __int64 DecRef(UI_64, void *&);

		virtual UI_64 SetFlags(UI_64, void *, UI_64);
		
		virtual UI_64 GetLink(UI_64, void *, UI_64 &);
		virtual void LinkIt(void * rpo, UI_64);
		virtual void ShiftRight(void *, UI_64);
		virtual void ShiftLeft(void *, UI_64);

		virtual void DropDown(void *);
		virtual void PickUp(void *, void *);
		
		virtual UI_64 Next(UI_64, void *&); // , UI_64 &, UI_64 &
		virtual UI_64 Back(UI_64, void *&);
		virtual UI_64 Left(UI_64, void *&);
		virtual UI_64 Right(UI_64, void *&);
		virtual UI_64 Down(UI_64, void *&);
		virtual UI_64 Up(UI_64, void *&);
			

	public:
		static const UI_64 type_id = 1033*FileChain::type_id;
		
		virtual UI_64 Alloc(UI_64 newsize, UI_64 type_id = -1, UI_64 parent = -1);
		virtual UI_64 Realloc(UI_64 id, UI_64 newsize);
		virtual UI_64 Expand(UI_64 id, UI_64 esize);

		virtual void * Free(UI_64 id);

		virtual UI_64 GetSize(UI_64 id);
		virtual UI_64 GetTypeID(UI_64 id);
		
		virtual UI_64 GetType();

		FileAllocator();
		virtual ~FileAllocator();


};


class FileXChain: protected FileChain {
	private:		
		UI_64 _x_id;

	protected:
		void DropDown(FileXLink *);
		void PickUp(FileXLink *, FileXLink *);

		void ShiftRightTo(XLink *, UI_64);
		void ShiftLeftTo(XLink *, UI_64);

		virtual UI_64 Relocate(char *, char *);


	public:
		static const UI_64 type_id = 1039*FileChain::type_id;
		
		virtual int Init(UI_64, const wchar_t *);

		UI_64 GetTop();
/*
		XLink * GetFirst(UI_64 type_id);
		XLink * GetNext(XLink *);
		XLink * GetPrev(XLink *);
*/
		void Lock(); // buffer wrapper
		void Unlock();

		FileXChain();
		virtual ~FileXChain();


		UI_64 InsertXLink(UI_64 size, UI_64 tid = -1, UI_64 container = -1);
		UI_64 ResizeXLink(UI_64 lindex, UI_64 new_size);

		XLink * GetXLink(UI_64);

		void * RemoveXLink(UI_64);

};

class FileXAllocator: public FileXChain, public SFSco::IAllocator {
	private:
		virtual __int64 IncRef(UI_64 id, void *&, __int64 & xstate, UI_64 & tid);
		virtual __int64 DecRef(void *&);
		
		virtual UI_64 SetFlags(void *, UI_64);

		virtual UI_64 GetLink(void *, UI_64 &);
		virtual void LinkIt(void * rpo, UI_64);
		virtual void ShiftRight(void *, UI_64);
		virtual void ShiftLeft(void *, UI_64);

		virtual void DropDown(void *);
		virtual void PickUp(void *, void *);

		virtual UI_64 Next(void **, UI_64 &, UI_64 &);
		virtual UI_64 Back(void **, UI_64 &, UI_64 &);

		virtual UI_64 Left(void **, UI_64 &, UI_64 &);
		virtual UI_64 Right(void **, UI_64 &, UI_64 &);
		virtual UI_64 Down(void **, UI_64 &, UI_64 &);
		virtual UI_64 Up(void **, UI_64 &, UI_64 &);


	public:
		static const UI_64 type_id = 1049*FileXChain::type_id;

		virtual UI_64 Alloc(UI_64 newsize, UI_64 type_id = -1, UI_64 parent = -1);
		virtual UI_64 Realloc(UI_64 id, UI_64 newsize);
		virtual UI_64 Expand(UI_64 id, UI_64 esize);

		virtual void * Free(UI_64 id);

		virtual UI_64 GetSize(UI_64 id);
		virtual UI_64 GetTypeID(UI_64 id);

		virtual UI_64 GetType();

		FileXAllocator();
		virtual ~FileXAllocator();


//		void ToObject(XLink *, SFSco::Object &);
//		UI_64 GetObject(UI_64, SFSco::Object &);


};



}


