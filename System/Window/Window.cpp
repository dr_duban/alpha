/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\Window.h"

#include <GdiPlus.h>

// Window ==================================================================================================================================================================================================================================

unsigned int Window::_winCount = 0;
unsigned int Window::_refCount = 0;
Window * Window::_wins[MAX_WINDOW_PER_APP] = {0};
int Window::screenW = 0;
int Window::screenH = 0;
int Window::screenX = 0;
int Window::screenY = 0;

Window::Window() : _handle(0), _atom(0), _parent(0), _x0(0), _y0(0), _width(1), _height(1), _cRadius(0), _maximized(0), _OGL(0) {
	unsigned int i(0);

	for (i=0;i<_winCount;i++) if (!_wins[i]) {
		_wins[i] = this;
		break;
	}

	if (i>=_winCount) {		
		_wins[_winCount++] = this;

		_winCount %= MAX_WINDOW_PER_APP;
	}

}


Window::~Window() {

	for (unsigned int i(0);i<_winCount;i++) if (_wins[i] == this) {
		_wins[i] = 0;
		break;
	}

	if (_OGL) _OGL->Destroy();

	_refCount--;
}


Window * Window::Find(HWND hwnd) {
	Window * result(0);

	for (unsigned int i(0);i<_winCount;i++) if (_wins[i]) if (_wins[i]->_handle == hwnd) {
		result = _wins[i];
		break;
	}

	return result;

}

void Window::Show(int flg) {
	if (flg == -1) flg = SW_SHOW;
	ShowWindow(_handle, flg);
	UpdateWindow(_handle);

	if (_OGL) _OGL->Update();
}

HWND Window::GetHandle() {
	return _handle;
}

void Window::GetName(wchar_t * buf, unsigned int bufsize) {
	GetWindowText(_handle, buf, bufsize);
}

unsigned int Window::GetNameLength() {
	return GetWindowTextLength(_handle);
}


LRESULT __stdcall Window::MsgProc(HWND hwnd, UINT msg, WPARAM wPa, LPARAM lPa) {
	LRESULT result(-101);

	if (Window * xwin = Find(hwnd)) xwin->Loop(result, msg, wPa, lPa);
	
	if (result == -101) result = DefWindowProc(hwnd, msg, wPa, lPa);	
	
	return result;
}

unsigned int Window::ClassInit(const wchar_t * className, unsigned int style, HBRUSH bg, unsigned int curso) {
	unsigned int result(0);

	if (!_atom) {
		WNDCLASSEX wclass = {0};
		wclass.cbSize = sizeof(WNDCLASSEX);

		wclass.lpfnWndProc = MsgProc;
		wclass.hInstance = GetModuleHandle(0); // GetModuleHandle(L"sf_core.dll");
		
		wclass.lpszClassName = className;
		wclass.style = style;

		wclass.hbrBackground = bg;
		wclass.hCursor = 0; //LoadCursor(0, MAKEINTRESOURCE(curso));

		_atom = RegisterClassEx(&wclass);

		if (!_atom) result = GetLastError();
	}

	return result;
}

//WS_EX_TOPMOST, WS_EX_APPWINDOW
unsigned int Window::Create(const wchar_t * name, HINSTANCE hin, unsigned int style, UI_64 pof, Window * parent, unsigned int exstyle) {
	unsigned int result(0);

	if (!_handle) {
		if (_atom) {
			HWND phw(0);
			

			RECT prect = {0};

			if (parent) {				
				_parent = parent;
				phw = parent->_handle;

				GetWindowRect(phw, &prect);

				prect.right -= prect.left;
				prect.bottom -= prect.top;

			} else {
												
				SystemParametersInfo(SPI_GETWORKAREA, 0, &prect, 0);

				prect.right -= prect.left;
				prect.bottom -= prect.top;

				screenW = prect.right;
				screenH = prect.bottom;
				screenX = prect.left; // + 1;
				screenY = prect.top;

			}


			switch (pof & WIBOX_MASK) {
				case WIBOX_50_50:
					_width = prect.right/2;
					_height = prect.bottom/2;
				break;
				case WIBOX_67_67:
					_width = 2*prect.right/3;
					_height = 2*prect.bottom/3;
				break;
				case WIBOX_67_50:
					_width = 2*prect.right/3;
					_height = prect.bottom/2;

				break;
				case WIBOX_50_67:
					_width = prect.right/2;
					_height = 2*prect.bottom/3;
				break;
				case WINBOX_MAX:
					_width = screenW;
					_height = screenH;
					_x0 = screenX;
					_y0 = screenY;
				break;
				default:
					_width = (pof >> 32) & 0x0FFFF;
					_height = (pof >> 48) & 0x0FFFF;

					_x0 = (pof >> 8) & 0x0FFF;
					_y0 = (pof >> 20) & 0x0FFF;

			}

			switch (pof & WILOC_MASK) {
				case WILOC_CENTER_CENTER:
					_x0 = (prect.right - _width)/2;
					_y0 = (prect.bottom - _height)/2;
				break;
				case WILOC_CENTER_20TOP:
					_x0 = (prect.right - _width)/2;
					_y0 = (prect.bottom - _height)/5;
				break;
				case WILOC_20LEFT_20TOP:
					_x0 = (prect.right - _width)/5;
					_y0 = (prect.bottom - _height)/5;
				break;
				case WILOC_20LEFT_CENTER:
					_x0 = (prect.right - _width)/5;
					_y0 = (prect.bottom - _height)/2;
				break;
				case WILOC_20RIGHT_CENTER:
					_x0 = 4*(prect.right - _width)/5;
					_y0 = (prect.bottom - _height)/2;
				break;
				case WILOC_20RIGHT_20TOP:
					_x0 = 4*(prect.right - _width)/5;
					_y0 = (prect.bottom - _height)/5;
				break;
				case WILOC_RIGHT_TOP:
					_x0 = prect.right - _width;
					_y0 = 0;
				break;
				case WILOC_RIGHT_BOTTOM:
					_x0 = prect.right - _width;
					_y0 = prect.bottom - _height;
				break;
				case WILOC_LEFT_BOTTOM:
					_x0 = 0;
					_y0 = prect.bottom - _height;
				break;
			}

			_handle = CreateWindowEx(exstyle, (LPCWSTR)_atom, name, style, _x0, _y0 , _width, _height, phw, 0, hin, 0);
			if (!_handle) result = GetLastError();
			else {
				MONITORINFO mofo;
				mofo.cbSize = sizeof(MONITORINFO);

				if (GetMonitorInfo(MonitorFromWindow(_handle, MONITOR_DEFAULTTONEAREST), &mofo)) {
					screenW = mofo.rcWork.right - mofo.rcWork.left;
					screenH = mofo.rcWork.bottom - mofo.rcWork.top;
						
					screenX = mofo.rcWork.left;
					screenY = mofo.rcWork.top;
				}
			}
		
		} else {
			result = -1;
		}
	}

	return result;
}

void Window::Relocate(int dx, int dy) {	
	if (_maximized == 0)
	if ((dx) || (dy)) SetWindowPos(_handle, 0, _x0 += dx, _y0 += dy, 0, 0, SWP_NOSIZE);
}

void Window::Size(int w, int h) {
	MONITORINFO mofo;
	if (_cRadius) SetWindowRgn(_handle,0,0);


	mofo.cbSize = sizeof(MONITORINFO);

	if (w == 0x80000000) {
		if (_maximized) {
			_width = _mm_wi; _height = _mm_he;			

			SetWindowPos(_handle, 0, _x0, _y0, _width, _height, SWP_SHOWWINDOW);

			if (_cRadius) SetWindowRgn(_handle, CreateRoundRectRgn(0, 0, _width+1, _height+1, _cRadius, _cRadius), FALSE);
			
		} else {
			_mm_wi = _width; _mm_he = _height;

			if (GetMonitorInfo(MonitorFromWindow(_handle, MONITOR_DEFAULTTONEAREST), &mofo)) {
				screenW = mofo.rcWork.right - mofo.rcWork.left;
				screenH = mofo.rcWork.bottom - mofo.rcWork.top;

				screenX = mofo.rcWork.left;
				screenY = mofo.rcWork.top;
			}
			
			_width = screenW; _height = screenH;
			
					
			SetWindowPos(_handle, 0, screenX, screenY, _width, _height, SWP_SHOWWINDOW);
			if (_cRadius) SetWindowRgn(_handle, CreateRoundRectRgn(0, 0, _width+1, _height+1, _cRadius, _cRadius), FALSE);

		}

		_maximized = ~_maximized;

	} else {
		if (w || h) {
			_width += w; _height += h;
			w = 0x80000000;

			SetWindowPos(_handle, 0, _x0, _y0, _width, _height, SWP_SHOWWINDOW);
			if (_cRadius) SetWindowRgn(_handle, CreateRoundRectRgn(0, 0, _width+1, _height+1, _cRadius, _cRadius), FALSE);
			
		}

	}

	if (w == 0x80000000)
		if (_OGL) _OGL->Resize();
		else InvalidateRect(_handle, 0, TRUE);
}

unsigned int Window::SetRoundRectRegion(int rad) {
	SetWindowRgn(_handle,0,0);
	_cRadius = rad;

	if (rad) SetWindowRgn(_handle, CreateRoundRectRgn(0, 0, _width+1, _height+1, _cRadius, _cRadius), TRUE);
	else SetWindowRgn(_handle, CreateRectRgn(0, 0, _width, _height), TRUE);
	return 0;
}

