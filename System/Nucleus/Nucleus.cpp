/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\Control.h"

Control::Nucleus::Nucleus(const Nucleus & ref) : Object(ref) {	

}

Control::Nucleus::Nucleus() {	
	SetTID(type_id);
}

Control::Nucleus::~Nucleus() {
	
}

void Control::Nucleus::Destroy() {
	SFSco::Event_RemoveGate(*this);

	SFSco::Object::Destroy();
}

void Control::Nucleus::Wait() const {
	bool result(true);

	while (result) {
		if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
			if (hdr->_r_d[0]) result = ((hdr->_state & hdr->_r_d[0]) == hdr->_r_d[0]);
			else result = false;
			const_cast<Nucleus *>(this)->Release();
		}
	}
}

void Control::Nucleus::Wait0() const {
	bool result(true);

	while (result) {
		if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {			
			result = (hdr->_state != 0);
			const_cast<Nucleus *>(this)->Release();
		}
	}
}


void Control::Nucleus::SetWaitMask(UI_64 flag) {
	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		hdr->_r_d[0] = flag;
		
		Release();
	}
}

void Control::Nucleus::AddWaitMask(UI_64 flag) {
	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		hdr->_r_d[0] |= flag;
		
		Release();
	}
}


UI_64 Control::Nucleus::Create(SFSco::IAllocator * alla) {
	UI_64 result = New(type_id, sizeof(Header), alla);
		
	if (result != -1) {
		if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
			hdr->_state = 0;
			
			hdr->_r_d[0] = 0;
			hdr->_r_d[1] = 0;

			hdr->gate_idx = -1;

			Release();
		}
	}

	return result;
}

UI_64 Control::Nucleus::Test(UI_64 flag) const {
	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		flag = (flag & hdr->_state) ^ flag;

		const_cast<Nucleus *>(this)->Release();
	}

	return flag;
}

UI_64 Control::Nucleus::Test() const {
	UI_64 result(-1);

	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		if (hdr->_r_d[0]) result = (hdr->_state & hdr->_r_d[0]) ^ hdr->_r_d[0];

		const_cast<Nucleus *>(this)->Release();
	}

	return result;
}

void Control::Nucleus::SetState(UI_64 flag) {
	if (flag)
	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {

		if ((hdr->_state & flag) != flag) {
			InterlockedOr((unsigned long long *)&hdr->_state, flag);

			flag = 0;
		}

		Release();

		if (!flag) SFSco::Event_MOLUpdate(*this);
	}
}

void Control::Nucleus::ClearState(UI_64 flag) {
	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		InterlockedAnd((unsigned long long *)&hdr->_state,  ~flag);

		Release();
	}
}

UI_64 Control::Nucleus::GetState() {
	UI_64 result(0);

	if (Header * hdr = reinterpret_cast<Header  *>(const_cast<Nucleus *>(this)->Acquire())) {
		result = hdr->_state;

		Release();
	}

	return result;
}


void Control::Nucleus::ResetState(UI_64 xstate) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {

		xstate = InterlockedExchange64((long long *)&hdr->_state, xstate);
		
		xstate &= hdr->_state;
		xstate ^= hdr->_state;
		
		hdr->_r_d[0] = 0;

		Release();
	
		if (xstate) SFSco::Event_MOLUpdate(*this);
	}	
}

