/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include <Windows.h>

#include "CoreLibrary\SFScore.h"

#define MAX_WINDOW_PER_APP 64

#define WILOC_MASK				0x000F
#define WILOC_CENTER_CENTER		0x0001
#define WILOC_CENTER_20TOP		0x0002
#define WILOC_20LEFT_20TOP		0x0003
#define WILOC_20LEFT_CENTER		0x0004
#define WILOC_20RIGHT_CENTER	0x0005
#define WILOC_20RIGHT_20TOP		0x0006
#define WILOC_RIGHT_TOP			0x0007
#define WILOC_RIGHT_BOTTOM		0x0008
#define WILOC_LEFT_BOTTOM		0x0009

#define WIBOX_MASK				0x00F0
#define WIBOX_50_50				0x0010
#define WIBOX_67_67				0x0020
#define WIBOX_50_67				0x0030
#define WIBOX_67_50				0x0040
#define WINBOX_MAX				0x0050


class Window {
	private:
		static Window * _wins[MAX_WINDOW_PER_APP];
		static unsigned int _winCount, _refCount;
		static LRESULT __stdcall MsgProc(HWND, UINT, WPARAM, LPARAM);
		static int screenW, screenH, screenX, screenY;


		ATOM _atom;
		Window * _parent;

	protected:
		static Window * Find(HWND);

		int _cRadius, _maximized, _mm_wi, _mm_he;
		int _x0, _y0, _width, _height;

		HWND _handle;

		SFSco::IOpenGL * _OGL;

		Window();

		virtual ~Window();
		virtual unsigned int Loop(LRESULT &, UINT, WPARAM, LPARAM) = 0;
		virtual void Relocate(int dx, int dy);
		virtual void Size(int w, int h);
		
		

		//CS_DBLCLKS, CS_DROPSHADOW, CS_OWNDC, CS_PARENTDC
		unsigned int ClassInit(const wchar_t * className, unsigned int style, HBRUSH = 0, unsigned int = 32512);
	public:
		void GetName(wchar_t *, unsigned int);
		unsigned int GetNameLength();

		//WS_EX_TOPMOST, WS_EX_APPWINDOW
		//WS_CAPTION, WS_CHILD, WS_CLIPCHILDREN, WS_CLIPSIBLINGS, WS_MAXIMIZE, WS_MAXIMIZEBOX, WS_MINIMIZE, WS_MINIMIZEBOX, WS_POPUP, WS_SIZEBOX, WS_SYSMENU, WS_VISIBLE
		unsigned int Create(const wchar_t * name, HINSTANCE, unsigned int style, UI_64 poflag = WILOC_CENTER_CENTER | WIBOX_67_67, Window * parent = 0, unsigned int exstyle = WS_EX_APPWINDOW);
		
		unsigned int SetRoundRectRegion(int rad);

		void Show(int = -1);
		void Hide();
		HWND GetHandle();
};
