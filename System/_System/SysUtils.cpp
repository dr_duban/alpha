/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "System\SysUtils.h"

#include "Windows.h"

const wchar_t cat_letters[] = L" kMGTPEZY";

wchar_t emBuf[MAX_PATH];

unsigned int System::FormatErrorMessage(const wchar_t * cap,unsigned int emn) {
	void * lpMsgBuf(0);
	if (!emn) emn = GetLastError();
	if (emn) { 
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, emn, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), emBuf, 0, 0);
		MessageBox(0, emBuf, cap, MB_OK | MB_ICONERROR);		
	}

	return emn;
}

void Strings::CStrToFloat(const char * src, float & result) {
	double tval(0);

	Strings::CStrToDouble(src, tval);

	result = tval;

}

unsigned int Strings::FormatNumberString(wchar_t * str, unsigned int s_len, unsigned int n_count, wchar_t e_char) {
	unsigned int cat(0), sep_lo(s_len);
	
	for (;sep_lo>=3;sep_lo-=3, cat++);
		
	if (sep_lo == 0) {
		if (str[0] < L'8') {
			sep_lo = 3;
			cat--;
		}
	}

	System::MemoryCopy(str + sep_lo + 1 + (sep_lo==0), str + sep_lo, (s_len - sep_lo)*sizeof(wchar_t));
	
	if (sep_lo == 0) {		
		str[0] = L'0';
		sep_lo = 1;
		s_len++;
	}

	if (sep_lo < s_len) str[sep_lo] = L'.';
	else n_count = s_len - 1;

/*	
	if (n_count > s_len) {
		if (cat == 0) {			
			if (s_len == 1) {				
				str[3] = L'0';
				str[4] = str[0];				
			} else {
				str[3] = str[0];
				str[4] = str[1];
			}
			str[0] = L'0';
			str[1] = L'.';
			str[2] = L'0';

			s_len = 4;
		}

		for (s_len++;s_len<=n_count;s_len++) str[s_len] = L'0';
	}
*/


	str[++n_count] = L' ';
	if (cat < 9) str[++n_count] = cat_letters[cat];
	
	str[++n_count] = e_char;
	str[++n_count] = L'\0';

	return n_count;
}

/*
*/