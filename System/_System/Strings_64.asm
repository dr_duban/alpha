
; safe-fail base elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



.CONST
ALIGN 16
quote_val	DWORD	027272727h, 027272727h, 027272727h, 027272727h
slash_val	DWORD	05C5C5C5Ch, 05C5C5C5Ch, 05C5C5C5Ch, 05C5C5C5Ch

word_mask	DWORD	00FF00FFh, 00FF00FFh, 00FF00FFh, 00FF00FFh
strint_mask	DWORD	0F0F0F0Fh, 0F0F0F0Fh, 0F0F0F0Fh, 0F0F0F0Fh
strint_offs	DWORD	30303030h, 30303030h, 30303030h, 30303030h
strint_shuf DWORD	0D0C0F0Eh, 09080B0Ah, 05040706h, 01000302h

string_hexl DWORD	39393939h, 39393939h, 39393939h, 39393939h
string_hexo	DWORD	07070707h, 07070707h, 07070707h, 07070707h

float_1016	QWORD	4341C37937E08000h
float_10	QWORD	4024000000000000h

metprefix	BYTE	79h, 7Ah, 61h, 66h,    70h, 6Eh, 75h, 6Dh,    20h, 6Bh, 4Dh, 47h,    54h, 50h, 45h, 5Ah,    59h, 20h, 20h, 20h


.CODE

ALIGN 16
FormatTimeVal	PROC
	MOV r10, 60
	
	XOR rdx, rdx
	MOV rax, rcx
	DIV r10
	MOVZX r9, dl ; seconds
	SHL r9, 8

	XOR rdx, rdx
	DIV r10
	MOV r9b, dl ; minutes
	SHL r9, 8

	MOV r10, 24
	XOR rdx, rdx
	DIV r10
	MOV r9b, dl ; hours


	SHL r9, 32
	OR rax, r9

	RET
FormatTimeVal	ENDP


ALIGN 16
TimeToCStr	PROC
	MOV r10, rsi
	MOV r11, rdi

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]


	MOV rax, rdx
	XOR rdx, rdx

	MOV r9d, 10
	DIV r9

; miliseconds
	XOR rdx, rdx
	MOV r9d, 1000
	DIV r9

	MOV [rcx], rdx
	FILD QWORD PTR [rcx]
	

;seconds
	XOR rdx, rdx
	MOV r9d, 60
	DIV r9

	MOV [rcx], rdx
	FILD QWORD PTR [rcx]
	

; minutes
	XOR rdx, rdx
	DIV r9

	MOV [rcx], rdx
	FILD QWORD PTR [rcx]
	
	MOV [rcx], rax
	FILD QWORD PTR [rcx]
	





	FBSTP TBYTE PTR [rcx]

	XOR rdx, rdx
	MOV r9, rcx

	TEST rax, rax
	JZ no_hours

		MOVDQU xmm2, [rcx]
		MOVDQU xmm3, [rcx]

		PSRLD xmm3, 4
		PAND xmm2, xmm4
		PAND xmm3, xmm4
		POR xmm2, xmm5
		POR xmm3, xmm5

		PUNPCKLBW xmm3, xmm2

		PSHUFB xmm3, XMMWORD PTR[strint_shuf]

		MOVDQU [rcx], xmm3
	
		MOV rsi, rcx
		MOV rdi, rcx

		MOV al, 30h
		MOV rcx, 16

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax
				
		
		INC ecx
		MOV edx, ecx

		REP MOVSB

		MOV BYTE PTR [rdi], 3Ah ; :
		INC edx
				


no_hours:

; minutes
	LEA rcx, [r9 + rdx]

	FBSTP TBYTE PTR [rcx]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5


	PUNPCKLBW xmm3, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]
	PSRLDQ xmm3, 14

	MOVD DWORD PTR [rcx], xmm3	
	MOV BYTE PTR [rcx+2], 3Ah ; :

	ADD edx, 3

; seconds
	LEA rcx, [r9 + rdx]

	FBSTP TBYTE PTR [rcx]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	PUNPCKLBW xmm3, xmm2	

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]
	PSRLDQ xmm3, 14

	MOVD DWORD PTR [rcx], xmm3
	MOV BYTE PTR [rcx+2], 2Eh ; :

	ADD edx, 3


; millisec
	LEA rcx, [r9 + rdx]

	FBSTP TBYTE PTR [rcx]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5


	PUNPCKLBW xmm3, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]
	PSRLDQ xmm3, 13

	MOVD DWORD PTR [rcx], xmm3	

	ADD edx, 3
		

	MOV eax, edx

	MOV rdi, r11
	MOV rsi, r10

	RET
TimeToCStr	ENDP

ALIGN 16
CmpWStr	PROC
	XOR rax, rax
	MOV r10, rsi
	MOV r11, rdi

		CMP rdx, r9
		CMOVA rdx, r9

		MOV rsi, rcx
		MOV rdi, r8

		MOV rcx, rdx

		REPZ CMPSW
		JZ cmp_done
			ADC rax, 0
			SHL rax, 1
			DEC rax

		cmp_done:
	MOV rdi, r11
	MOV rsi, r10
	RET
CmpWStr	ENDP


ALIGN 16
A2W	PROC
	PXOR xmm0, xmm0
	MOVDQU xmm1, XMMWORD PTR [rcx]
	MOVDQU xmm2, XMMWORD PTR [rcx+16]

	MOVDQA xmm4, xmm1
	MOVDQA xmm3, xmm2
	
	PUNPCKLBW xmm1, xmm0
	PUNPCKHBW xmm4, xmm0
	PUNPCKLBW xmm2, xmm0
	PUNPCKHBW xmm3, xmm0

	MOVDQU [rcx], xmm1
	MOVDQU [rcx+16], xmm4
	MOVDQU [rcx+32], xmm2
	MOVDQU [rcx+48], xmm3

	RET
A2W	ENDP

ALIGN 16
StringToHexString_SSE3	PROC
	MOVDQA xmm14, XMMWORD PTR [strint_mask]
	MOVDQA xmm15, XMMWORD PTR [strint_offs]
	
	MOVDQA xmm13, XMMWORD PTR [string_hexl]
	MOVDQA xmm12, XMMWORD PTR [string_hexo]

	PXOR xmm5, xmm5

	MOV r10, r8	
	SHR r10, 5
	JZ below_16

	

align 16
	transformloop:
		MOVDQU xmm2, [rdx]
		MOVDQU xmm3, [rdx+16]

		MOVDQA xmm8, xmm2
		MOVDQA xmm9, xmm3

		PSRLD xmm8, 4
		PSRLD xmm9, 4

		PAND xmm2, xmm14
		PAND xmm3, xmm14
		PAND xmm8, xmm14
		PAND xmm9, xmm14

		POR xmm2, xmm15
		POR xmm3, xmm15
		POR xmm8, xmm15
		POR xmm9, xmm15
	
		MOVDQA xmm0, xmm2
		MOVDQA xmm1, xmm3
		PCMPGTB xmm0, xmm13
		PCMPGTB xmm1, xmm13
		PAND xmm0, xmm12
		PAND xmm1, xmm12
		PADDD xmm2, xmm0
		PADDD xmm3, xmm1

		MOVDQA xmm0, xmm8
		MOVDQA xmm1, xmm9
		PCMPGTB xmm0, xmm13
		PCMPGTB xmm1, xmm13
		PAND xmm0, xmm12
		PAND xmm1, xmm12
		PADDD xmm8, xmm0
		PADDD xmm9, xmm1

		MOVDQA xmm0, xmm8
		MOVDQA xmm1, xmm9

		PUNPCKLBW xmm8, xmm2
		PUNPCKHBW xmm0, xmm2

		PUNPCKLBW xmm9, xmm3
		PUNPCKHBW xmm1, xmm3
			

		TEST r9d, r9d
		JNZ wcopy
			MOVDQU [rcx], xmm8
			MOVDQU [rcx+16], xmm0
			MOVDQU [rcx+32], xmm9
			MOVDQU [rcx+48], xmm1

			JMP acopy
	align 16
		wcopy:
			MOVDQA xmm3, xmm8
			MOVDQA xmm4, xmm0

			PUNPCKLBW xmm8, xmm5
			PUNPCKHBW xmm3, xmm5
			PUNPCKLBW xmm0, xmm5
			PUNPCKHBW xmm4, xmm5

			MOVDQU [rcx], xmm8
			MOVDQU [rcx+16], xmm3			
			MOVDQU [rcx+32], xmm0
			MOVDQU [rcx+48], xmm4

			ADD rcx, 64

			MOVDQA xmm3, xmm9
			MOVDQA xmm4, xmm1

			PUNPCKLBW xmm9, xmm5
			PUNPCKHBW xmm3, xmm5
			PUNPCKLBW xmm1, xmm5
			PUNPCKHBW xmm4, xmm5
			
			MOVDQU [rcx], xmm9
			MOVDQU [rcx+16], xmm3			
			MOVDQU [rcx+32], xmm1
			MOVDQU [rcx+48], xmm4
			
		acopy:


		ADD rdx, 32
		ADD rcx, 64
	DEC r10
	JNZ transformloop

align 16
	below_16:
		AND r8, 1Fh
		JNZ StringToHexString
	
	RET
StringToHexString_SSE3	ENDP

ALIGN 16
StringToHexString	PROC
	PUSH rdi
	PUSH rsi
		MOV rsi, rdx
		MOV rdi, rcx

		MOV dx, WORD PTR [string_hexl]
		MOV cx, WORD PTR [strint_offs]


	align 16
		rest_loop:
			XOR rax, rax
			LODSB

			MOV ah, al
			AND ah, 0Fh
			SHR al, 4
			
			OR ax, cx
			
			CMP al, dl
			JBE noalpha1
				ADD al, 7
			noalpha1:

			CMP ah, dh
			JBE noalpha2
				ADD ah, 7
			noalpha2:

			TEST r8d, r8d
			JZ no_w
				MOV r9w, ax
				SHR r9w, 8

				AND ax, 0FFh
				STOSW

				MOV ax, r9w
			no_w:

			STOSW
		DEC r8
		JNZ rest_loop


	POP rsi
	POP rdi

	RET
StringToHexString	ENDP



ALIGN 16
HexStringToString_SSE3	PROC
	MOVDQA xmm14, XMMWORD PTR [word_mask]
	MOVDQA xmm15, XMMWORD PTR [strint_offs]
	
	MOVDQA xmm13, XMMWORD PTR [string_hexl]
	MOVDQA xmm12, XMMWORD PTR [string_hexo]

	PXOR xmm5, xmm5

	MOV r10, r8	
	SHR r10, 5
	JZ below_16
		

align 16
	transformloop:
		MOVDQU xmm2, [rdx]
		MOVDQU xmm3, [rdx+16]

		MOVDQA xmm0, xmm2
		MOVDQA xmm1, xmm3
		PCMPGTB xmm0, xmm13
		PCMPGTB xmm1, xmm13
		PAND xmm0, xmm12
		PAND xmm1, xmm12
		PSUBD xmm2, xmm0
		PSUBD xmm3, xmm1

		PXOR xmm2, xmm15
		PXOR xmm3, xmm15

		MOVDQA xmm8, xmm2
		MOVDQA xmm9, xmm3
		PSLLD xmm2, 4
		PSLLD xmm3, 4
		PSRLDQ xmm8, 1
		PSRLDQ xmm9, 1
		POR xmm2, xmm8
		POR xmm3, xmm9

		PAND xmm2, xmm14
		PAND xmm3, xmm14

		PACKUSWB xmm2, xmm3

		MOVDQU [rcx], xmm2

		ADD rdx, 32
		ADD rcx, 16
	DEC r10
	JNZ transformloop

align 16
	below_16:
		AND r8, 1Fh		
		JNZ HexStringToString

	
	RET
HexStringToString_SSE3	ENDP

ALIGN 16
HexStringToString	PROC
	SHR r8, 1
	JZ exit

	PUSH rsi
	PUSH rdi

		MOV rsi, rdx
		MOV rdi, rcx

		MOV dx, WORD PTR [string_hexl]
		MOV cx, WORD PTR [strint_offs]

		align 16
			rest_loop:
				LODSW

				CMP al, dl
				JBE noalpha1
					SUB al, 7
				noalpha1:

				CMP ah, dh
				JBE noalpha2
					SUB ah, 7
				noalpha2:

				XOR ax, cx
				SHL al, 4
				OR al, ah

				STOSB

			DEC r8
			JNZ rest_loop

	POP rdi
	POP rsi
exit:

	RET
HexStringToString	ENDP



ALIGN 16
Int64ToCStr_SSSE3 PROC
	MOV r11, rsi
	MOV r10, rdi

	MOV [rcx], rdx
	FILD QWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	MOVDQA xmm5, xmm3

	PUNPCKLBW xmm3, xmm2
	PUNPCKHBW xmm5, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]

	MOV BYTE PTR [rcx], 5Fh
	MOVD DWORD PTR [rcx+1], xmm5
	MOVDQU [rcx+3], xmm3
	


	BTS edx, 19
	JNC nosign
		MOV BYTE PTR [rcx], 2Dh		
		ADD rsi, 1		
	nosign:

	MOV BYTE PTR [rcx+19], 0
	MOV eax, 19

	CMP r8d, 0
	JNE donomo
		XOR rdx, rdx
		MOV rsi, rcx
		LEA rdi, [rcx+1]

		MOV al, 30h
		MOV rcx, 18

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax

		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1

		REP MOVSB
	donomo:

	MOV rsi, r11
	MOV rdi, r10
	
	RET
Int64ToCStr_SSSE3 ENDP

ALIGN 16
UInt64ToCStr_SSSE3 PROC
	MOV r11, rsi
	MOV r10, rdi

	MOV rax, rdx
	XOR rdx, rdx

	MOV r9d, 10
	DIV r9
	
	OR dl, 30h

	MOV [rcx], rax
	FILD QWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	MOVDQA xmm5, xmm3

	PUNPCKLBW xmm3, xmm2
	PUNPCKHBW xmm5, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]

	MOV BYTE PTR [rcx], 5Fh
	MOVD DWORD PTR [rcx+1], xmm5
	MOVDQU [rcx+3], xmm3
		

	LEA rdi, [rcx+20]
	MOV eax, 19

	CMP r8d, 0
	JNE donomo
		XOR r9, r9
		MOV rsi, rcx
		LEA rdi, [rcx+1]

		MOV al, 30h
		MOV rcx, 18

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax

		MOV eax, r9d
		ADD eax, ecx
		ADD ecx, 2
		INC eax

		REP MOVSB
donomo:
	
	MOV [rdi-1], dl
	MOV BYTE PTR [rdi], 0

	INC eax

	MOV rsi, r11
	MOV rdi, r10
	
	RET
UInt64ToCStr_SSSE3 ENDP


ALIGN 16
UIntToCStr_SSSE3 PROC
	MOV r11, rsi
	MOV r10, rdi

	MOV [rcx], edx
	XOR edx, edx
	MOV [rcx+4], edx

	FILD QWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOV rsi, rcx
	MOV rdi, rcx

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	MOV r9d, [rcx+9]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	PUNPCKLBW xmm3, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]

	MOVDQU [rcx], xmm3
	
	ADD rsi, 5
	ADD rdi, 5

	MOV BYTE PTR [rcx+5], 5Fh

	AND r9d, 80h
	JZ nosign
		MOV BYTE PTR [rcx+5], 2Dh		
	nosign:
	

	MOV BYTE PTR [rcx+16], 0
	MOV eax, 11
	MOV ecx, 12

	CMP r8d, 0
	JNE donomo
		XOR edx, edx
		AND r9d, 80h
		JZ skipsign
			MOV BYTE PTR [rdi-5], 2Dh	
			ADD rsi, 1
			ADD edx, 1
		skipsign:
		
		MOV al, 30h
		ADD rdi, 1
		MOV rcx, 10

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax
		
		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1
		
	donomo:

	SUB rdi, 5

	REP MOVSB

	MOV rsi, r11
	MOV rdi, r10

	RET
UIntToCStr_SSSE3 ENDP


ALIGN 16
Int64ToCStr PROC
	PUSH rdi
	MOV r9, rsi
	

	MOV [rcx], rdx
	FILD QWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOV rsi, rcx
	MOV rdi, rcx

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	MOVDQA xmm5, xmm3

	PUNPCKLBW xmm3, xmm2
	PUNPCKHBW xmm5, xmm2

	MOVD r10, xmm3
	PSRLDQ xmm3, 8
	MOVD r11, xmm3

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+3], r11w	
	MOV [rcx+11], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+5], r11w	
	MOV [rcx+13], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+7], r11w	
	MOV [rcx+15], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+9], r11w	
	MOV [rcx+17], r10w

	
	MOVD edx, xmm5
	MOV [rcx+1], dx

	MOV BYTE PTR [rcx], 5Fh

	BTS edx, 19
	JNC nosign
		MOV BYTE PTR [rcx], 2Dh		
		ADD rsi, 1		
	nosign:

	MOV BYTE PTR [rcx+19], 0
	MOV eax, 19

	CMP r8d, 0
	JNE donomo
		MOV rdx, rsi
		SUB rdx, rdi

		MOV al, 30h
		ADD rdi, 1
		MOV rcx, 18

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax

		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1

		REP MOVSB
	donomo:

	MOV rsi, r9
	POP rdi
	
	RET
Int64ToCStr ENDP

ALIGN 16
IntToCStr	PROC
	PUSH rsi
	PUSH rdi

	MOV [rcx], edx
	FILD DWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOV rsi, rcx
	MOV rdi, rcx

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	MOV r9d, [rcx+9]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	PUNPCKLBW xmm3, xmm2

	MOVD r10, xmm3
	PSRLDQ xmm3, 8
	MOVD r11, xmm3

	ROL r11, 16
	ROL r10, 16

	MOV [rcx], r11w	
	ROL r11, 16
	MOV [rcx+2], r11w
	ROL r11, 16
	MOV [rcx+4], r11w
	ROL r11, 16
	MOV [rcx+6], r11w


	MOV [rcx+8], r10w	
	ROL r10, 16
	MOV [rcx+10], r10w	
	ROL r10, 16	
	MOV [rcx+12], r10w	
	ROL r10, 16	
	MOV [rcx+14], r10w
	
	ADD rsi, 5
	ADD rdi, 5

	MOV BYTE PTR [rcx+5], 5Fh

	AND r9d, 80h
	JZ nosign
		MOV BYTE PTR [rcx+5], 2Dh		
	nosign:
	

	MOV BYTE PTR [rcx+16], 0
	MOV eax, 11
	MOV ecx, 12

	AND r8d, r8d
	JNZ donomo
		XOR edx, edx
		AND r9d, 80h
		JZ skipsign
			MOV BYTE PTR [rdi-5], 2Dh	
			ADD rsi, 1
			ADD edx, 1
		skipsign:
		
		MOV al, 30h
		ADD rdi, 1
		MOV rcx, 10

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax
		
		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1
		
	donomo:
	
	AND r8d, r8d
	JZ no_rsi
		MOV rax, r8

		INC r8
		SUB rcx, r8
		ADD rsi, rcx

		
		MOV rcx, r8
	no_rsi:

	SUB rdi, 5	
	REP MOVSB



	POP rdi
	POP rsi	

	RET
IntToCStr	ENDP

ALIGN 16
IntToCStr_SSSE3	PROC
	MOV r11, rsi
	MOV r10, rdi

	MOV [rcx], edx
	FILD DWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOV rsi, rcx
	MOV rdi, rcx

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	MOV r9d, [rcx+9]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	PUNPCKLBW xmm3, xmm2

	PSHUFB xmm3, XMMWORD PTR[strint_shuf]

	MOVDQU [rcx], xmm3
	
	ADD rsi, 5
	ADD rdi, 5

	MOV BYTE PTR [rcx+5], 5Fh

	AND r9d, 80h
	JZ nosign
		MOV BYTE PTR [rcx+5], 2Dh		
	nosign:
	

	MOV BYTE PTR [rcx+16], 0
	MOV eax, 11
	MOV ecx, 12

	CMP r8d, 0
	JNE donomo
		XOR edx, edx
		AND r9d, 80h
		JZ skipsign
			MOV BYTE PTR [rdi-5], 2Dh	
			ADD rsi, 1
			ADD edx, 1
		skipsign:
		
		MOV al, 30h
		ADD rdi, 1
		MOV rcx, 10

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax
		
		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1
		
	donomo:

	SUB rdi, 5

	REP MOVSB

	MOV rsi, r11
	MOV rdi, r10

	RET
IntToCStr_SSSE3	ENDP



ALIGN 16
UIntToCStr	PROC
	PUSH rsi
	PUSH rdi

	MOV [rcx], edx
	XOR edx, edx
	MOV [rcx+4], edx

	FILD QWORD PTR [rcx]
	FBSTP TBYTE PTR [rcx]

	MOV rsi, rcx
	MOV rdi, rcx

	MOVDQA xmm4, XMMWORD PTR [strint_mask]
	MOVDQA xmm5, XMMWORD PTR [strint_offs]

	MOVDQU xmm2, [rcx]
	MOVDQU xmm3, [rcx]

	MOV r9d, [rcx+9]

	PSRLD xmm3, 4
	PAND xmm2, xmm4
	PAND xmm3, xmm4
	POR xmm2, xmm5
	POR xmm3, xmm5

	PUNPCKLBW xmm3, xmm2

	MOVD r10, xmm3
	PSRLDQ xmm3, 8
	MOVD r11, xmm3

	ROL r11, 16
	ROL r10, 16
	MOV [rcx], r11w	
	MOV [rcx+8], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+2], r11w	
	MOV [rcx+10], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+4], r11w	
	MOV [rcx+12], r10w

	ROL r11, 16
	ROL r10, 16
	MOV [rcx+6], r11w	
	MOV [rcx+14], r10w
	
	ADD rsi, 5
	ADD rdi, 5

	MOV BYTE PTR [rcx+5], 5Fh

	AND r9d, 80h
	JZ nosign
		MOV BYTE PTR [rcx+5], 2Dh		
	nosign:
	

	MOV BYTE PTR [rcx+16], 0
	MOV eax, 11
	MOV ecx, 12

	CMP r8d, 0
	JNE donomo
		XOR edx, edx
		AND r9d, 80h
		JZ skipsign
			MOV BYTE PTR [rdi-5], 2Dh	
			ADD rsi, 1
			ADD edx, 1
		skipsign:
		
		MOV al, 30h
		ADD rdi, 1
		MOV rcx, 10

		REPE SCASB

		SUB rdi, 1

		MOV rax, rsi
		MOV rsi, rdi
		MOV rdi, rax
		
		MOV eax, edx
		ADD eax, ecx
		ADD ecx, 2
		ADD eax, 1
		
	donomo:

	SUB rdi, 5

	REP MOVSB
		
	POP rdi
	POP rsi

	RET
UIntToCStr ENDP

ALIGN 16
CStrToInt	PROC
	MOV r10, rdi
	MOV r8, rsi

	MOV r9, rdx

	SUB rsp, 16
		XOR rdx, rdx
		
		MOV [rsp], rdx

		MOV rdi, rcx

		CMP BYTE PTR [rdi], 2Dh
		SETE dl

		ADD rdi, rdx

		ROR dx, 1
		MOV [rsp+8], dx

		XOR eax, eax
				
		MOV rcx, 11
		REPNE SCASB
		
		SUB rdi, 2
		
		SUB ecx, 10
		NEG ecx

		MOV rsi, rdi
		MOV rdi, rsp

		XOR edx, edx

		STD

align 16		
	readloop:
		LODSB

		AND al, 0Fh
		ADD al, 6
		AND al, 0Fh
		SUB al, 6
		
		MOV dl, al

		SUB ecx, 1
	JZ readdone
		LODSB

		AND al, 0Fh
		ADD al, 6
		AND al, 0Fh
		SUB al, 6
		SHL al, 4
					
		OR dl, al

		MOV [rdi], dl
		ADD rdi, 1

		SUB ecx, 1
	JNZ readloop
	JZ evenread
	readdone:
		MOV [rdi], dl
	evenread:

		CLD

		FBLD TBYTE PTR [rsp]
		FISTP DWORD PTR [r9]

	ADD rsp, 16
	MOV rdi, r10
	MOV rsi, r8
	RET
CStrToInt ENDP

ALIGN 16
CStrToInt64	PROC
	MOV r10, rdi
	MOV r8, rsi

	MOV r9, rdx

	SUB rsp, 16
		XOR rdx, rdx
		
		MOV [rsp], rdx

		MOV rdi, rcx

		CMP BYTE PTR [rdi], 2Dh
		SETE dl

		ADD rdi, rdx

		ROR dx, 1
		MOV [rsp+8], dx

		XOR eax, eax
				
		MOV rcx, 19
		REPNE SCASB
		
		SUB rdi, 2
		
		SUB ecx, 18
		NEG ecx

		MOV rsi, rdi
		MOV rdi, rsp

		XOR edx, edx

		STD

align 16		
	readloop:
		LODSB

		AND al, 0Fh
		ADD al, 6
		AND al, 0Fh
		SUB al, 6
		
		MOV dl, al

		SUB ecx, 1
	JZ readdone
		LODSB

		AND al, 0Fh
		ADD al, 6
		AND al, 0Fh
		SUB al, 6
		SHL al, 4
					
		OR dl, al

		MOV [rdi], dl
		ADD rdi, 1

		SUB ecx, 1
	JNZ readloop
	JZ evenread
	readdone:
		MOV [rdi], dl
	evenread:

		CLD

		FBLD TBYTE PTR [rsp]
		FISTP QWORD PTR [r9]

	ADD rsp, 16
	MOV rdi, r10
	MOV rsi, r8
	RET
CStrToInt64	ENDP

ALIGN 16
FloatToCStr_SSSE3	PROC
	MOV r10, rdi
	MOV r9, rbx

	PUSH rbx

	FSTCW [rsp]
	FSTCW [rsp+2]

	OR WORD PTR [rsp], 0400h
	FLDCW [rsp]

	MOV r11, rcx
	MOV rdi, rcx
	MOVSS DWORD PTR [rdi], xmm1
	FLD DWORD PTR [rdi]
		
	FXTRACT

	FLD st(0)
	FABS
	FLD1

	FCOMI st(0), st(1)
	JLE no_adjust_1
		FADD st(3), st(0)
	no_adjust_1:

	FLDLG2
	FMUL st(0), st(4)
	FISTP DWORD PTR [rdi]
	
	JLE no_adjust_2
		FSUB st(3), st(0)
	no_adjust_2:
	FSTP st(0)
	FSTP st(0)
	
	FSCALE
	
	MOV eax, [rdi]
	SUB eax, 17
	MOV edx, eax
	NEG edx

	CMOVNS eax, edx

	FLD1
	FLD QWORD PTR [float_10]

align 16
	muloop:
		SHR eax, 1
		JZ muldone
		JNC nomul
			FMUL st(1), st(0)
		nomul:
			FMUL st(0), st(0)
		JMP muloop
	muldone:

	FMULP st(1), st(0)

	NEG edx

	JS mulscale
		FDIVP st(1), st(0)
	JMP scaledone
	mulscale:
		FMULP st(1), st(0)
	scaledone:


	FBSTP TBYTE PTR [rdi]
	FSTP st(0)

	MOV cx, WORD PTR [rdi+8]
	MOVDQU xmm0, [rdi]

	AND ch, ch
	JZ nosign
		MOV BYTE PTR [rdi], 2Dh
		INC rdi
	nosign:

	MOV ch, cl
	SHR ch, 4
		
	AND cx, 0f0fh
	OR cx, 3030h

	MOV [rdi], ch


	TEST r8d, 80000000h
	JZ normalb
		CMP edx, 9
		JG normalb
		CMP edx, 0FFFFFFD7h
		JL normalb


		MOV ebx, 3
		AND r8d, ebx
		
		XOR eax, eax
		ADD edx, 41
		XCHG eax, edx
		DIV bl

		LEA rbx, [metprefix]
		XLATB

		ADD rdi, 1

		PSRLDQ xmm0, 6
		MOVD edx, xmm0
		
		MOV ebx, edx
		SHR ebx, 4

		AND edx, 0f0fh
		AND ebx, 0f0fh

		OR edx, 3030h
		OR ebx, 3030h

		XCHG bh, dl

		SHL edx, 16
		OR edx, ebx

		ROR edx, 8
		XCHG cl, dl

		MOV [rdi], edx
		MOVZX ebx, ah
		XOR ah, ah

		ADD rdi, rbx
		
		CMP r8d, 0
		JE nodetails

			MOV BYTE PTR [rdi], 2Eh
			INC rdi

			SHL ebx, 3
			XCHG ecx, ebx
		
			SHR edx, cl
			MOV [rdi], edx
		
			SHR ecx, 3
			XOR ecx, 3
			ADD ecx, 1
		
			MOV [rdi+rcx], bl				


	nodetails:
		LEA rdi, [rdi+r8]
		MOV BYTE PTR [rdi], 20h		
		MOV [rdi+1], al
		MOV BYTE PTR [rdi+2], 0	

		JMP exitdtc
align 16
	normalb:

		MOV BYTE PTR [rdi+1], 2eh
		
		ADD rdi, 2

		AND r8d, 07h
		JZ wrexp
			MOV [rdi], cl
			ADD rdi, 1
			SUB r8d, 1
			JZ wrexp

				MOVDQA xmm1, xmm0
				PSRLD xmm1, 4
				
				PAND xmm0, XMMWORD PTR [strint_mask]
				PAND xmm1, XMMWORD PTR [strint_mask]
				POR xmm0, XMMWORD PTR [strint_offs]
				POR xmm1, XMMWORD PTR [strint_offs]

				PUNPCKLBW xmm1, xmm0
				PSHUFB xmm1, XMMWORD PTR [strint_shuf]

				MOVDQU [rdi], xmm1
				ADD rdi, r8
		
		wrexp:
			MOV BYTE PTR [rdi], 45h
			ADD rdi, 1
			
			XOR eax, eax
			ADD edx, 17
			
			MOV BYTE PTR [rdi], 2bh
			JNS expos
				MOV BYTE PTR [rdi], 2dh
				NEG edx
			expos:
			ADD rdi, 1

			MOV ecx, 10
			XCHG eax, edx
			DIV ecx

			AND al, 0fh
			AND dl, 0fh
			OR al, 30h
			OR dl, 30h
			
			MOV [rdi], al
			MOV [rdi+1], dl
			MOV BYTE PTR [rdi+2], 0
		
			
	exitdtc:
		MOV rax, rdi
		SUB rax, r11
		ADD rax, 2

		FLDCW [rsp+2]
		POP rbx							
							
		MOV rbx, r9
		MOV rdi, r10
	RET
FloatToCStr_SSSE3	ENDP

ALIGN 16
DoubleToCStr_SSSE3 PROC
	MOV r10, rdi
	MOV r9, rbx
	
	PUSH rbx

	FSTCW [rsp]
	FSTCW [rsp+2]

	OR WORD PTR [rsp], 0400h
	FLDCW [rsp]

	MOV r11, rcx
	MOV rdi, rcx
	MOVSD QWORD PTR [rdi], xmm1
	FLD QWORD PTR [rdi]
	
	FXTRACT

	FLD st(0)
	FABS
	FLD1

	FCOMI st(0), st(1)
	JLE no_adjust_1
		FADD st(3), st(0)
	no_adjust_1:

	FLDLG2
	FMUL st(0), st(4)
	FISTP DWORD PTR [rdi]
	
	JLE no_adjust_2
		FSUB st(3), st(0)
	no_adjust_2:
	FSTP st(0)
	FSTP st(0)

	FSCALE
	
	MOV eax, [rdi]
	SUB eax, 17
	MOV edx, eax
	NEG edx

	CMOVNS eax, edx

	FLD1
	FLD QWORD PTR [float_10]

align 16
	muloop:
		SHR eax, 1
		JZ muldone
		JNC nomul
			FMUL st(1), st(0)
		nomul:
			FMUL st(0), st(0)
		JMP muloop
	muldone:

	FMULP st(1), st(0)

	NEG edx

	JS mulscale
		FDIVP st(1), st(0)
	JMP scaledone
	mulscale:
		FMULP st(1), st(0)
	scaledone:


	FBSTP TBYTE PTR [rdi]
	FSTP st(0)

	MOV cx, WORD PTR [rdi+8]
	MOVDQU xmm0, [rdi]

	
	AND ch, ch
	JZ nosign
		MOV BYTE PTR [rdi], 2Dh
		INC rdi
	nosign:

	MOV ch, cl
	SHR ch, 4
		
	AND cx, 0f0fh
	OR cx, 3030h

	MOV [rdi], ch


	TEST r8d, 80000000h
	JZ normalb
		CMP edx, 9
		JG normalb
		CMP edx, 0FFFFFFD7h
		JL normalb

		MOV ebx, 3
		AND r8d, ebx
		
		XOR eax, eax
		ADD edx, 41
		XCHG eax, edx
		DIV bl

		LEA rbx, [metprefix]
		XLATB

		ADD rdi, 1

		PSRLDQ xmm0, 6
		MOVD edx, xmm0
		
		MOV ebx, edx
		SHR ebx, 4

		AND edx, 0f0fh
		AND ebx, 0f0fh

		OR edx, 3030h
		OR ebx, 3030h

		XCHG bh, dl

		SHL edx, 16
		OR edx, ebx

		ROR edx, 8
		XCHG cl, dl

		MOV [rdi], edx
		MOVZX ebx, ah
		XOR ah, ah

		ADD rdi, rbx
		
		CMP r8d, 0
		JE nodetails

			MOV BYTE PTR [rdi], 2Eh
			ADD rdi, 1

			SHL ebx, 3
			XCHG ecx, ebx
		
			SHR edx, cl
			MOV [rdi], edx
		
			SHR ecx, 3
			XOR ecx, 3
			ADD ecx, 1
		
			MOV [rdi+rcx], bl				


	nodetails:
		LEA rdi, [rdi+r8]
		MOV BYTE PTR [rdi], 20h
		MOV [rdi+1], al
		MOV BYTE PTR [rdi+2], 0
				
		
		JMP exitdtc
align 16
	normalb:
		TEST r8d, 40000000h
		JZ normal_proc
		AND r8d, 0Fh
		JZ wrexp

			MOV [rdi + 1], cl
			
			MOVDQA xmm1, xmm0
			PSRLD xmm1, 4
				
			PAND xmm0, XMMWORD PTR [strint_mask]
			PAND xmm1, XMMWORD PTR [strint_mask]
			POR xmm0, XMMWORD PTR [strint_offs]
			POR xmm1, XMMWORD PTR [strint_offs]

			PUNPCKLBW xmm1, xmm0
			PSHUFB xmm1, XMMWORD PTR [strint_shuf]

			MOVDQU [rdi + 2], xmm1



			ADD edx, 17			
			JNS noexp_proc
				NEG edx
								
				SUB edx, r8d
				JAE noexp_zero_proc
					NEG edx

					XOR eax, eax
					MOV al, r8b
					SHL eax, 8
					ADD rdx, rdi
					
					sne_copy_loop:
						MOV al, [rdi + r8]
						MOV BYTE PTR [rdi + r8], 30h
						MOV [rdx + r8 + 1], al
					DEC r8
					JNZ sne_copy_loop

					MOV BYTE PTR [rdi + 1], 2eh

					SHR eax, 8
					LEA rdi, [rdi + rax]

					JMP exitdtc
				
				noexp_zero_proc:
					MOV BYTE PTR [rdi + 1], 2eh
					
					zero_loop:
						MOV BYTE PTR [rdi + 2], 30h
						INC rdi
					DEC r8d
					JNZ zero_loop
					

				JMP exitdtc
			noexp_proc:

			ADD rdi, rdx
			MOV edx, r8d

			ne_copy_loop:
				MOV al, [rdi + r8]
				MOV [rdi + r8 + 1], al
			DEC r8
			JNZ ne_copy_loop

			MOV BYTE PTR [rdi + 1], 2eh

			LEA rdi, [rdi + rdx]


		JMP exitdtc
	normal_proc:
		MOV BYTE PTR [rdi+1], 2eh
		
		ADD rdi, 2
		
		AND r8d, 0Fh
		JZ wrexp
			MOV [rdi], cl
			ADD rdi, 1
			SUB r8d, 1
			JZ wrexp

				MOVDQA xmm1, xmm0
				PSRLD xmm1, 4
				
				PAND xmm0, XMMWORD PTR [strint_mask]
				PAND xmm1, XMMWORD PTR [strint_mask]
				POR xmm0, XMMWORD PTR [strint_offs]
				POR xmm1, XMMWORD PTR [strint_offs]

				PUNPCKLBW xmm1, xmm0
				PSHUFB xmm1, XMMWORD PTR [strint_shuf]

				MOVDQU [rdi], xmm1
				ADD rdi, r8
		
		wrexp:

			MOV BYTE PTR [rdi], 45h
			ADD rdi, 1
						
			XOR eax, eax
			ADD edx, 17
			
			MOV BYTE PTR [rdi], 2bh
			JNS expos
				MOV BYTE PTR [rdi], 2dh
				NEG edx
			expos:
			ADD rdi, 1

			MOV ecx, 10
			XCHG eax, edx
			DIV ecx

			AND dl, 0fh
			OR dl, 30h
			
			MOV [rdi+2], dx

			XOR edx, edx
			DIV ecx

			AND al, 0fh
			AND dl, 0fh
			OR al, 30h
			OR dl, 30h
			
			MOV [rdi], al
			MOV [rdi+1], dl
			
			INC rdi
	exitdtc:
		MOV rax, rdi
		SUB rax, r11
		ADD rax, 2

		FLDCW [rsp+2]
		POP rbx							
									
		MOV rbx, r9
		MOV rdi, r10
									
	RET
DoubleToCStr_SSSE3	ENDP

ALIGN 16
FloatToCStr_SSE3	PROC
	PUSH rdi
	MOV r9, rbx

	PUSH rbx

	FSTCW [rsp]
	FSTCW [rsp+2]

	OR WORD PTR [rsp], 0400h
	FLDCW [rsp]

	MOV r11, rcx
	MOV rdi, rcx
	MOV r10, rcx

	MOVSS DWORD PTR [rdi], xmm1
	
	FLD DWORD PTR [rdi]
		
	FXTRACT

	FLD st(0)
	FABS
	FLD1

	FCOMI st(0), st(1)
	JLE no_adjust_1
		FADD st(3), st(0)
	no_adjust_1:

	FLDLG2
	FMUL st(0), st(4)
	FISTP DWORD PTR [rdi]
	
	JLE no_adjust_2
		FSUB st(3), st(0)
	no_adjust_2:
	FSTP st(0)
	FSTP st(0)
	
	FSCALE
	
	MOV eax, [rdi]
	SUB eax, 17
	MOV edx, eax
	NEG edx

	CMOVNS eax, edx

	FLD1
	FLD QWORD PTR [float_10]

align 16
	muloop:
		SHR eax, 1
		JZ muldone
		JNC nomul
			FMUL st(1), st(0)
		nomul:
			FMUL st(0), st(0)
		JMP muloop
	muldone:

	FMULP st(1), st(0)

	NEG edx

	JS mulscale
		FDIVP st(1), st(0)
	JMP scaledone
	mulscale:
		FMULP st(1), st(0)
	scaledone:


	FBSTP TBYTE PTR [rdi]
	FSTP st(0)

	MOV cx, WORD PTR [rdi+8]
	MOVDQU xmm0, [rdi]


	AND ch, ch
	JZ nosign
		MOV BYTE PTR [rdi], 2Dh
		INC rdi

	nosign:

	MOV ch, cl
	SHR ch, 4
		
	AND cx, 0f0fh
	OR cx, 3030h

	MOV [rdi], ch


	TEST r8d, 80000000h
	JZ normalb
		CMP edx, 9
		JG normalb
		CMP edx, 0FFFFFFD7h
		JL normalb


		MOV ebx, 3
		AND r8d, ebx
		
		XOR eax, eax
		ADD edx, 41
		XCHG eax, edx
		DIV bl

		LEA rbx, [metprefix]
		XLATB

		ADD rdi, 1

		PSRLDQ xmm0, 6
		MOVD edx, xmm0
		
		MOV ebx, edx
		SHR ebx, 4

		AND edx, 0f0fh
		AND ebx, 0f0fh

		OR edx, 3030h
		OR ebx, 3030h

		XCHG bh, dl

		SHL edx, 16
		OR edx, ebx

		ROR edx, 8
		XCHG cl, dl

		MOV [rdi], edx
		MOVZX ebx, ah
		XOR ah, ah

		ADD rdi, rbx
		
		CMP r8d, 0
		JE nodetails

			MOV BYTE PTR [rdi], 2Eh
			INC rdi

			SHL ebx, 3
			XCHG ecx, ebx
		
			SHR edx, cl
			MOV [rdi], edx
		
			SHR ecx, 3
			XOR ecx, 3
			ADD ecx, 1
		
			MOV [rdi+rcx], bl				


	nodetails:
		LEA rdi, [rdi+r8]
		MOV BYTE PTR [rdi], 20h		
		MOV [rdi+1], al
		MOV BYTE PTR [rdi+2], 0

		JMP exitdtc
align 16
	normalb:

		MOV BYTE PTR [rdi+1], 2eh
		
		ADD rdi, 2

		AND r8d, 07h
		JZ wrexp
			MOV [rdi], cl
			ADD rdi, 1
			SUB r8d, 1
			JZ wrexp

				MOVDQA xmm1, xmm0
				PSRLD xmm1, 4
				
				PAND xmm0, XMMWORD PTR [strint_mask]
				PAND xmm1, XMMWORD PTR [strint_mask]
				POR xmm0, XMMWORD PTR [strint_offs]
				POR xmm1, XMMWORD PTR [strint_offs]

				PUNPCKLBW xmm1, xmm0

				PSRLDQ xmm1, 8

				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi+4], ecx
				PSRLDQ xmm1, 4
				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi], ecx
				
				ADD rdi, r8
		
		wrexp:
			MOV BYTE PTR [rdi], 45h
			ADD rdi, 1
			
			XOR eax, eax
			ADD edx, 17
			
			MOV BYTE PTR [rdi], 2bh
			JNS expos
				MOV BYTE PTR [rdi], 2dh
				NEG edx
			expos:
			ADD rdi, 1

			MOV ecx, 10
			XCHG eax, edx
			DIV ecx

			AND al, 0fh
			AND dl, 0fh
			OR al, 30h
			OR dl, 30h
			
			MOV [rdi], al
			MOV [rdi+1], dl
			MOV BYTE PTR [rdi+2], 0


	exitdtc:
		MOV rax, rdi
		SUB rax, r11
		ADD rax, 2

		FLDCW [rsp+2]
		POP rbx
									
		MOV rbx, r9
		POP rdi
	RET
FloatToCStr_SSE3	ENDP

ALIGN 16
DoubleToCStr_SSE3	PROC
	PUSH rdi
	MOV r9, rbx
	
	PUSH rbx
	
	FSTCW [rsp]
	FSTCW [rsp+2]

	OR WORD PTR [rsp], 0400h
	FLDCW [rsp]

	MOV r11, rcx
	MOV rdi, rcx
	MOV r10, rcx

	MOVSD QWORD PTR [rdi], xmm1
	
	FLD QWORD PTR [rdi]
	
	FXTRACT

	FLD st(0)
	FABS
	FLD1

	FCOMI st(0), st(1)
	JLE no_adjust_1
		FADD st(3), st(0)
	no_adjust_1:

	FLDLG2
	FMUL st(0), st(4)
	FISTP DWORD PTR [rdi]
	
	JLE no_adjust_2
		FSUB st(3), st(0)
	no_adjust_2:
	FSTP st(0)
	FSTP st(0)
	
	FSCALE
	
	MOV eax, [rdi]
	SUB eax, 17
	MOV edx, eax
	NEG edx

	CMOVNS eax, edx

	FLD1
	FLD QWORD PTR [float_10]

align 16
	muloop:
		SHR eax, 1
		JZ muldone
		JNC nomul
			FMUL st(1), st(0)
		nomul:
			FMUL st(0), st(0)
		JMP muloop
	muldone:

	FMULP st(1), st(0)

	NEG edx

	JS mulscale
		FDIVP st(1), st(0)
	JMP scaledone
	mulscale:
		FMULP st(1), st(0)
	scaledone:


	FBSTP TBYTE PTR [rdi]
	FSTP st(0)

	MOV cx, WORD PTR [rdi+8]
	MOVDQU xmm0, [rdi]

	AND ch, ch
	JZ nosign
		MOV BYTE PTR [rdi], 2Dh
		INC rdi
	nosign:

	MOV ch, cl
	SHR ch, 4
		
	AND cx, 0f0fh
	OR cx, 3030h

	MOV [rdi], ch


	TEST r8d, 80000000h
	JZ normalb
		CMP edx, 9
		JG normalb
		CMP edx, 0FFFFFFD7h
		JL normalb

		MOV ebx, 3
		AND r8d, ebx
		
		XOR eax, eax
		ADD edx, 41
		XCHG eax, edx
		DIV bl

		LEA rbx, [metprefix]
		XLATB

		ADD rdi, 1

		PSRLDQ xmm0, 6
		MOVD edx, xmm0
		
		MOV ebx, edx
		SHR ebx, 4

		AND edx, 0f0fh
		AND ebx, 0f0fh

		OR edx, 3030h
		OR ebx, 3030h

		XCHG bh, dl

		SHL edx, 16
		OR edx, ebx

		ROR edx, 8
		XCHG cl, dl

		MOV [rdi], edx
		MOVZX ebx, ah
		XOR ah, ah

		ADD rdi, rbx
		
		CMP r8d, 0
		JE nodetails

			MOV BYTE PTR [rdi], 2Eh
			INC rdi

			SHL ebx, 3
			XCHG ecx, ebx
		
			SHR edx, cl
			MOV [rdi], edx
		
			SHR ecx, 3
			XOR ecx, 3
			ADD ecx, 1
		
			MOV [rdi+rcx], bl				


	nodetails:
		LEA rdi, [rdi+r8]
		MOV BYTE PTR [rdi], 20h		
		MOV [rdi+1], al
		MOV BYTE PTR [rdi+2], 0

		JMP exitdtc
align 16
	normalb:

		MOV BYTE PTR [rdi+1], 2eh
		
		ADD rdi, 2

		AND r8d, 0Fh
		JZ wrexp
			MOV [rdi], cl
			ADD rdi, 1
			SUB r8d, 1
			JZ wrexp

				MOVDQA xmm1, xmm0
				PSRLD xmm1, 4
				
				PAND xmm0, XMMWORD PTR [strint_mask]
				PAND xmm1, XMMWORD PTR [strint_mask]
				POR xmm0, XMMWORD PTR [strint_offs]
				POR xmm1, XMMWORD PTR [strint_offs]

				PUNPCKLBW xmm1, xmm0

				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi+12], ecx
				PSRLDQ xmm1, 4

				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi+8], ecx
				PSRLDQ xmm1, 4

				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi+4], ecx
				PSRLDQ xmm1, 4

				MOVD ecx, xmm1
				ROR ecx, 16
				MOV [rdi], ecx

				ADD rdi, r8
		
		wrexp:
			MOV BYTE PTR [rdi], 45h
			ADD rdi, 1
						
			XOR eax, eax
			ADD edx, 17
			
			MOV BYTE PTR [rdi], 2bh
			JNS expos
				MOV BYTE PTR [rdi], 2dh
				NEG edx
			expos:
			ADD rdi, 1

			MOV ecx, 10
			XCHG eax, edx
			DIV ecx

			AND dl, 0fh
			OR dl, 30h
			
			MOV [rdi+2], dx

			XOR edx, edx
			DIV ecx

			AND al, 0fh
			AND dl, 0fh
			OR al, 30h
			OR dl, 30h
			
			MOV [rdi], al
			MOV [rdi+1], dl
						
			INC rdi
	exitdtc:
		MOV rax, rdi
		SUB rax, r11
		ADD rax, 2

		FLDCW [rsp+2]
		POP rbx
								
		MOV rbx, r9
		POP rdi
	RET
DoubleToCStr_SSE3	ENDP

ALIGN 16
CStrToDouble	PROC
	MOV r11, rdi
	MOV r9, rsi
	MOV r8, rbx

	MOV r10, rdx

	SUB rsp, 40

		FLD1

		XOR rdx, rdx
		
		MOV [rsp], rdx
		MOV [rsp+8], rdx
		MOV [rsp+16], rdx
		MOV [rsp+24], rdx
		MOV [rsp+32], rdx

		MOV rdi, rcx
		MOV rsi, rcx

		CMP BYTE PTR [rdi], 2Dh
		SETZ dl

		ADD rdi, rdx
		ADD rsi, rdx

		ROR dx, 1
		MOV [rsp+8], dx


; look for decimal sep

		MOV ecx, 25
		XOR rax, rax
	
		REPNE SCASB
			
		SUB ecx, 24
		NEG ecx

		MOV dl, cl
				
		MOV rdi, rsi
		LEA rdi, [rsp+16]

		MOV ebx, 2Eh			


align 16
		dsearch:
			LODSB
			STOSB
			CMP al, bl
		LOOPNE dsearch
		
		MOV eax, 0
		MOV dh, cl

		JNE nodetails
			SUB rdi, 1
; look for e(E)
align 16
			esearch:
				LODSB
				STOSB
				XOR al, 65h
				AND al, 0DFh
			LOOPNZ esearch
; zero there is e
			MOV eax, 0
			CMOVNZ ebx, eax
			JNZ nodetails

			MOV BYTE PTR [rdi-1], 0
			TEST ecx, ecx
			CMOVZ ebx, eax
			JZ nodetails

				XOR eax, eax
				XOR edi, edi

				SUB bl, 1
			
				LODSB				
				XOR bl, al
				CMOVZ eax, edi
				CMP al, 2Bh
				CMOVZ eax, edi
								
				SUB ecx, 1
				JZ ecalc
					ROL eax, 8
					LODSB

					SUB ecx, 1
					JZ ecalc

						ROL eax, 8
						LODSB

						SUB ecx, 1
						JZ ecalc

							ROL eax, 8
							LODSB

				ecalc:
					AND eax, 0F0F0Fh
					ADD eax, 060606h
					AND eax, 0F0F0Fh
					SUB eax, 060606h

					MOVZX edi, ah
					MOVZX ecx, ah
					SHL ecx, 1
					SHL edi, 3
					
					ADD ecx, edi
					MOVZX edi, al
					ADD ecx, edi

					AND eax, 0FF0000h
					MOV edi, eax

					SHR eax, 9
					SHR edi, 11

					ADD ecx, eax
					SUB ecx, edi

					SHR edi, 3
					ADD ecx, edi

		nodetails:

			SUB dh, dl
			MOVSX edx, dh
			ADD edx, 19
			MOV eax, edx
			NEG eax

			TEST bl, bl
			CMOVZ edx, eax				
					
			SUB ecx, edx

			FLD QWORD PTR [float_10]

			JNS powercycle
			NEG ecx
											
align 16
		powercycle:
			SHR ecx, 1
			JZ muldone
				JNC nomul
					FMUL st(1), st(0)
				nomul:
					FMUL st(0), st(0)
		JMP powercycle
		muldone:

		FMULP st(1), st(0)

		MOV ecx, 9
		
		LEA rsi, [rsp+16]
		LEA rdi, [rsp+8]
		STD
align 16		
		bcdpack:		
			MOV ax, [rsi]
			ADD esi, 2
			
			AND ax, 0F0Fh
			ADD ax, 0606h
			AND ax, 0F0Fh
			SUB ax, 0606h

			SHL al, 4
			OR al, ah
			STOSB
			DEC ecx
		JNE bcdpack
		CLD
		FBLD TBYTE PTR [rsp]
		
		TEST bl, bl
		JNZ mulscale
			FDIV st(0), st(1)
		JMP scaledone
		mulscale:
			FMUL st(0), st(1)

		scaledone:
		FSTP QWORD PTR [r10]
		FSTP QWORD PTR [rsp+16]

	ADD rsp, 40

	MOV rbx, r8
	MOV rsi, r9
	MOV rdi, r11
	
	RET
CStrToDouble	ENDP


ALIGN 16
SearchAStr	PROC
	MOV r9, rdi

		MOV rdi, rcx
		MOV r8, rcx

		XOR rax, rax

		XOR rcx, rcx		
		NOT rcx

		REPNZ SCASB
		
		MOV r10, rcx

		XOR rcx, rcx
		NOT rcx

		MOV rdi, rdx

		REPNZ SCASB

		MOV r11, rcx
		ADD r11, 3
		NEG r11

		
		SUB rcx, r10
		ADD rcx, 1
		CMOVLE rcx, rax
		JLE sexit

		MOV r10, rcx

		MOV rdi, r8
		

align 16
	sloop:
		MOV al, [rdx]
		MOV rcx, r10
		REPNZ SCASB		
		JNZ sexit
			MOV r10, rcx
			MOV rcx, r11

			AND rcx, rcx
			JZ sexit
align 16
			dsearch:
				MOV al, [rdx+rcx]
				CMP al, [rdi+rcx-1]				
			LOOPZ dsearch

			JNZ sloop

			MOV rax, rdi
		sexit:

		SUB rdi, 1
		SUB rax, 1

		XOR rdi, rax
		CMOVNZ rax, rcx
	
	MOV rdi, r9

	RET
SearchAStr	ENDP

ALIGN 16
SearchWStr	PROC
	MOV r9, rdi

		MOV rdi, rcx
		MOV r8, rcx

		XOR rax, rax

		XOR rcx, rcx		
		NOT rcx

		REPNZ SCASW
		
		MOV r10, rcx

		XOR rcx, rcx
		NOT rcx

		MOV rdi, rdx

		REPNZ SCASW

		MOV r11, rcx
		ADD r11, 3
		NEG r11

		
		SUB rcx, r10
		ADD rcx, 1
		CMOVLE rcx, rax
		JLE sexit

		MOV r10, rcx

		MOV rdi, r8
		

align 16
	sloop:
		MOV ax, [rdx]
		MOV rcx, r10
		REPNZ SCASW
		JNZ sexit
			MOV r10, rcx
			MOV rcx, r11

			AND rcx, rcx
			JZ sexit
align 16
			dsearch:
				MOV ax, [rdx+rcx*2]
				CMP ax, [rdi+rcx*2-2]
			LOOPZ dsearch

			JNZ sloop

			MOV rax, rdi
		sexit:

		SUB rdi, 2
		SUB rax, 2

		XOR rdi, rax
		CMOVNZ rax, rcx
	
	MOV rdi, r9

	RET
SearchWStr	ENDP


ALIGN 16
SearchBlock	PROC
	MOV r11, rdi	

		MOV rdi, rcx
		XOR rax, rax
		
		SUB rdx, r9
		CMOVL rcx, rax
		JL sexit
	
		INC rdx
		DEC r9
		
align 16
	sloop:
		MOV al, [r8]
		MOV rcx, rdx
		REPNZ SCASB

		JNZ sexit
			MOV rdx, rcx
			MOV rcx, r9

			AND rcx, rcx
			JZ sexit

align 16
			dsearch:
				MOV al, [r8+rcx]
				CMP al, [rdi+rcx-1]				
			LOOPZ dsearch
						
			JNZ sloop

			MOV rax, rdi
		sexit:

		SUB rdi, 1
		SUB rax, 1

		XOR rdi, rax
		CMOVNZ rax, rcx
	
	MOV rdi, r11

	RET
SearchBlock	ENDP

; // mysql ======================================================================================================================================================

ALIGN 16
MyEscape_SSE3	PROC
	PUSH r15
	MOV r11, rsi
	MOV r10, rdi
	
		MOV rsi, rdx
		MOV rdi, rcx
		MOV r9, rcx

		MOV r15d, r8d

		SHR r15d, 6
		AND r8d, 63

		MOVDQA xmm6, XMMWORD PTR [slash_val]
		MOVDQA xmm7, XMMWORD PTR [quote_val]

		MOV dl, BYTE PTR [slash_val]
		MOV dh, BYTE PTR [quote_val]

align 16
		mainloop:
			TEST r15d, r15d
			JE firstpass

				MOVDQU xmm0, [rsi]
				MOVDQU xmm1, [rsi+16]
				MOVDQU xmm2, [rsi+32]
				MOVDQU xmm3, [rsi+48]

				MOVDQA xmm4, xmm0				
				MOVDQA xmm5, xmm0
				
				PCMPEQB xmm4, xmm6				
				PCMPEQB xmm5, xmm7

				POR xmm4, xmm5
				PMOVMSKB eax, xmm4

				MOVDQA xmm4, xmm1
				MOVDQA xmm5, xmm1

				PCMPEQB xmm4, xmm6
				PCMPEQB xmm5, xmm7

				POR xmm4, xmm5
				PMOVMSKB ecx, xmm4

				OR eax, ecx

				MOVDQA xmm4, xmm2				
				MOVDQA xmm5, xmm2
				
				PCMPEQB xmm4, xmm6				
				PCMPEQB xmm5, xmm7

				POR xmm4, xmm5
				PMOVMSKB ecx, xmm4

				OR eax, ecx

				MOVDQA xmm4, xmm3				
				MOVDQA xmm5, xmm3
				
				PCMPEQB xmm4, xmm6				
				PCMPEQB xmm5, xmm7

				POR xmm4, xmm5
				PMOVMSKB ecx, xmm4

				SUB r15d, 1				

				OR eax, ecx

				MOV ecx, 64

				JNE firstpass

					MOVDQU [rdi], xmm0
					MOVDQU [rdi+16], xmm1
					MOVDQU [rdi+32], xmm2
					MOVDQU [rdi+48], xmm3

					ADD rdi, rcx
					ADD rsi, rcx
				
				JMP mainloop

		firstpass:
			CMOVZ ecx, r8d
			JECXZ edone		
align 16				
		fploop:
			MOVZX eax, BYTE PTR [rsi]
			INC rsi
			CMP al, dl
			JE escapec
				CMP al, dh
				JNE noescape
			escapec:
				MOV [rdi], dl
				INC rdi
			noescape:
			MOV [rdi], al
			INC rdi			
			DEC ecx
		JNE fploop
		
		TEST r15d, r15d
		JNE mainloop
	edone:

		SFENCE

		SUB rdi, r9
		MOV rax, rdi

	MOV rsi, r11
	MOV rdi, r10
	POP r15
	RET
MyEscape_SSE3 ENDP


ALIGN 16
MyEscape	PROC
	MOV r10, rsi
	MOV r11, rdi

		MOV rsi, rdx
		MOV rdi, rcx
		MOV r9, rcx
		MOV ecx, r8d

		XOR rax, rax
		XOR rdx, rdx
		
		MOV dl, BYTE PTR [slash_val]
		MOV dh, BYTE PTR [quote_val]
				

align 16
		doescape:
			MOVZX eax, BYTE PTR [rsi]
			INC rsi
			CMP al, dl
			JE doit
				CMP al, dh
				JNE moveit
			doit:
				MOV BYTE PTR [rdi], dl
				INC rdi
			moveit:			
			MOV [rdi], al
			INC rdi			
			DEC ecx
		JNE doescape

	SFENCE

	SUB rdi, r9
	MOV rax, rdi

	MOV rdi, r11
	MOV rsi, r10
	
	RET
MyEscape	ENDP


END