
; safe-fail base elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE



.DATA
current_time	QWORD	0, 0
cal_stat		QWORD	0, 0, 0, 0, 0, 0, 0, 0, 0, 0
gen_32			DWORD	0, 0, 0, 0
gen_64			QWORD	0, 0, 0, 0

.CODE


ALIGN 16
ResetCal	PROC
	RDTSC
	SHL rdx, 32
	OR rax, rdx

	MOV r8, rax

	XOR rdx, rdx	
	MOV [cal_stat], 1

	MOV [cal_stat + 16], r8		; x
	MOV [cal_stat + 24], rdx
	
	MOV [cal_stat + 32], rcx	; y
	MOV [cal_stat + 40], rdx

	MUL r8

	MOV [cal_stat + 48], rax	; x2
	MOV [cal_stat + 56], rdx

	XOR rdx, rdx

	MOV rax, r8
	MUL rcx

	MOV [cal_stat + 64], rax	; xy
	MOV [cal_stat + 72], rdx


	RET
ResetCal	ENDP

ALIGN 16
TimerCal	PROC	

	RDTSC
	SHL rdx, 32
	OR rax, rdx

	MOV r9, rax

	MOV r8, [cal_stat + 8]
	INC r8
	CMP r8, 4096
	JB do_cal
		RET


do_cal:
	MOV [cal_stat + 8], r8

	XOR rdx, rdx
	MOV r8, [cal_stat]
	INC r8	
	MOV [cal_stat], r8

	

	ADD [cal_stat + 16], r9		; x
	ADC [cal_stat + 24], 0
	
	ADD [cal_stat + 32], rcx	; y
	ADC [cal_stat + 40], 0

	MUL r9

	ADD [cal_stat + 56], rdx
	ADD [cal_stat + 48], rax	; x2
	ADC [cal_stat + 56], 0

	XOR rdx, rdx

	MOV rax, r9
	MUL rcx

	ADD [cal_stat + 72], rdx	; xy
	ADD [cal_stat + 64], rax	; xy
	ADC [cal_stat + 72], 0




	XOR rdx, rdx
	MOV rax, [cal_stat + 56]
	MUL r8

	MOV r11, rax

	XOR rdx, rdx
	MOV rax, [cal_stat + 48]
	MUL r8

	ADD r11, rdx
	MOV r10, rax
	



	XOR rdx, rdx
	MOV rax, [cal_stat + 16]
	MOV rcx, rax
	MUL rcx

	SUB r11, rdx
	SUB r10, rax
	SBB r11, 0

	XOR rdx, rdx
	MOV rax, [cal_stat + 24]
	MUL rcx
		
	SHL rax, 1
	SUB r11, rax ; xx







	XOR rdx, rdx
	MOV rax, [cal_stat + 72]
	MUL r8

	MOV r9, rax

	XOR rdx, rdx
	MOV rax, [cal_stat + 64]
	MUL r8
	ADD r9, rdx
	MOV r8, rax


	XOR rdx, rdx
	MOV rcx, [cal_stat + 16]
	
	MOV rax, [cal_stat + 40]
	MUL rcx

	SUB r9, rax

	XOR rdx, rdx
	MOV rax, [cal_stat + 32]
	MUL rcx

	SUB r9, rdx
	SUB r8, rax
	SBB r9, 0

	XOR rdx, rdx
	MOV rcx, [cal_stat + 24]	
	MOV rax, [cal_stat + 32]
	MUL rcx

	XOR rdx, rdx
	MOV rcx, 1000
	MOV rax, r11
	MUL rcx

	MOV r11, rax

	XOR rdx, rdx
	MOV rax, r10
	MUL rcx

	ADD r11, rdx
	MOV r10, rax


	AND r9, r9	
	JZ small_a
		MOV rdx, r11
		MOV rax, r10

		BSR rcx, r9
		INC cl

		MOV ch, 64
		SUB ch, cl


		SHR r10, cl
		SHR r11, cl
		SHR r8, cl
		

		SHR rcx, 8


		SHL r9, cl
		SHL rdx, cl


		OR r10, rdx
		OR r8, r9
		
		

align 16
	small_a:
		MOV rdx, r11
		MOV rax, r10
			
		DIV r8
		MOV [current_time], rax


	RET
TimerCal	ENDP


ALIGN 16
GetTime	PROC
	RDTSC
	SHL rdx, 32
	OR rax, rdx

	RET
GetTime	ENDP


ALIGN 16
GetUCount	PROC
	MOV r8, rdx

	RDTSC
	SHL rdx, 32
	OR rax, rdx
		
	SUB rax, rcx ; current - prev
	
	XOR rdx, rdx
	
	MOV rcx, [current_time]
	DIV rcx
		
	SUB rax, r8
	NEG rax
	
	RET
GetUCount	ENDP



ALIGN 16
Gen32	PROC
	LEA r9, [gen_32]
	AND rcx, rcx
	CMOVZ rcx, r9

	MOV eax, [rcx]

	MOV r9d, eax

	MOV edx, eax	
	MOV r8d, edx
	
	SHR edx, 2
	ADC r8d, 0
	SHR edx, 20
	ADC r8d, 0
	SHR edx, 10
	ADC r8d, 1

	AND r8d, 1
	SHL eax, 1
	OR eax, r8d

	CMP eax, [rcx+4]
	JNZ no_period
		MOV r8d, eax
		INC DWORD PTR [rcx+8]
		
		RDTSC

		XOR eax, r8d
		DEC eax
		SBB eax, 0

		MOV [rcx+4], eax
no_period:

	MOV [rcx], eax

	MOV ecx, [rcx+8]
	ADD ecx, 24
	AND ecx, 01Fh

	ROR r9d, cl
	AND r9d, 0FFh
	JZ exit

	
align 16
	jmp_loop:
		MOV cl, r9b
		AND cl, 0Fh
		ROL cl, 3
		ROR eax, cl

		MOV edx, eax	
		MOV r8d, edx
	
		SHR edx, 2
		ADC r8d, 0
		SHR edx, 20
		ADC r8d, 0
		SHR edx, 10
		ADC r8d, 1

		AND r8d, 1
		SHL eax, 1
		OR eax, r8d

	DEC r9d
	JNZ jmp_loop



	exit:

	RET
Gen32	ENDP

ALIGN 16
Gen64	PROC
	LEA r9, [gen_64]
	AND rcx, rcx
	CMOVZ rcx, r9

	MOV rax, [rcx]

	MOV r9, rax
	
	
	MOV rdx, rax
	SHR rdx, 37
	MOV r8, rdx

	MOV r10, rax
	MOV r11, rax

	SHR r11, 1
	ADC r10, 0
	SHR r11, 1
	ADC r10, 0
	SHR r11, 1
	ADC r10, 0
	SHR r11, 1
	ADC r10, 0
	SHR r11, 32
	ADC r10, 1

	MOV r11, 1
	SHL r11, 37
	NOT r11

	AND r10, 1
	SHL rax, 1
	OR rax, r10

	AND rax, r11

	SHR r8, 1
	ADC rdx, 0
	SHR r8, 4
	ADC rdx, 0
	SHR r8, 22
	ADC rdx, 1

	AND rdx, 1
	SHL rdx, 37
	OR rax, rdx

	CMP rax, [rcx+8]
	JNZ no_period
		MOV r8, rax
		INC DWORD PTR [rcx+16]
		
		RDTSC
		SHL rdx, 32
		OR rax, rdx

		XOR rax, r8
		DEC rax
		SBB rax, 0
		MOV [rcx+8], rax
no_period:


	MOV [rcx], rax

	MOV rcx, [rcx+16]
	ADD rcx, 32
	AND rcx, 3Fh
	
	ROR r9, cl
	AND r9, 0FFh
	JZ exit
		

align 16
	jmp_loop:
		MOV cl, r9b
		AND cl, 1Fh
		ADD cl, 11
		ROL rax, cl

		MOV rdx, rax
		SHR rdx, 37
		MOV r8, rdx

		MOV r10, rax
		MOV r11, rax

		SHR r11, 1
		ADC r10, 0
		SHR r11, 1
		ADC r10, 0
		SHR r11, 1
		ADC r10, 0
		SHR r11, 1
		ADC r10, 0
		SHR r11, 32
		ADC r10, 1

		MOV r11, 1
		SHL r11, 37
		NOT r11

		AND r10, 1
		SHL rax, 1
		OR rax, r10

		AND rax, r11

		SHR r8, 1
		ADC rdx, 0
		SHR r8, 4
		ADC rdx, 0
		SHR r8, 22
		ADC rdx, 1

		AND rdx, 1
		SHL rdx, 37
		OR rax, rdx
		

	DEC r9
	JNZ jmp_loop

	exit:


	RET
Gen64	ENDP


ALIGN 16
Swap_16	PROC
	sloop:
		ROR WORD PTR [rcx], 8
		ADD rcx, 2
		SUB dx, 1
	JNZ sloop

	RET
Swap_16	ENDP


ALIGN 16
Swap_32	PROC	
	lloop:
		MOV eax, [rcx]
		BSWAP eax
		MOV [rcx], eax
		ADD rcx, 4
		SUB edx, 1
	JNZ lloop

	RET
Swap_32	ENDP

ALIGN 16
Swap_64	PROC	
	lloop:
		MOV rax, [rcx]
		BSWAP rax
		MOV [rcx], rax
		ADD rcx, 8
		SUB edx, 1
	JNZ lloop

	RET
Swap_64	ENDP




ALIGN 16
BSWAP_16	PROC
	MOV ax, [rcx]
	ROR ax, 8
	MOV [rcx], ax
	RET
BSWAP_16	ENDP


ALIGN 16
BSWAP_32	PROC
	MOV eax, [rcx]
	BSWAP eax
	MOV [rcx], eax
	RET
BSWAP_32	ENDP

ALIGN 16
BSWAP_64	PROC
	MOV rax, [rcx]
	BSWAP rax
	MOV [rcx], rax
	RET
BSWAP_64	ENDP



ALIGN 16
?Serialize@System@@YAXXZ	PROC
	XOR eax, eax
	CPUID
	RET
?Serialize@System@@YAXXZ	ENDP

ALIGN 16
LockedInc_64	PROC
	MOV rax, 1
	LOCK XADD [rcx], rax

	RET
LockedInc_64	ENDP


ALIGN 16
LockedDec_64	PROC
	XOR rax, rax
	NOT rax

	LOCK XADD [rcx], rax		
	
	RET
LockedDec_64	ENDP


ALIGN 16
LockedInc_32	PROC

	MOV eax, 1
	LOCK XADD [rcx], eax
	JNZ itsok
		LOCK XCHG [rcx], eax
		NOT eax
	itsok:

	RET
LockedInc_32	ENDP

ALIGN 16
LockedDec_32	PROC

	XOR eax, eax
	NOT eax
	
	LOCK XADD [rcx], eax
	JNS itsok
		LOCK XCHG [rcx], eax
		XOR eax, eax
	itsok:
		
	RET
LockedDec_32	ENDP








ALIGN 16
SpinWait_I64	PROC

	MOV rax, [rcx]
	AND rax, rax
	JNZ spinloop

	RET

align 16
	spinloop:
		XCHG rax, rax
		XCHG rax, rax
		XCHG rax, rax
		XCHG rax, rax

		PAUSE


		MOV rdx, [rcx]
	CMP rdx, rax
	JNL spinloop
	
	RET

SpinWait_I64	ENDP


ALIGN 16
SpinWait_I32	PROC
	MOV eax, [rcx]
	AND eax, eax
	JNZ spinloop

	RET

align 16
	spinloop:
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax

		PAUSE

		
		MOV edx, [rcx]
	CMP edx, eax
	JNL spinloop

	RET

SpinWait_I32	ENDP







ALIGN 16
SpinWait_I32I	PROC

		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax
		XCHG eax, eax

		PAUSE
		
		MOV eax, [rcx]
	AND eax, edx
	JNZ SpinWait_I32I
	
	RET
SpinWait_I32I	ENDP











ALIGN 16
?BitCount@System@@YAI_K@Z	PROC
	XOR rax, rax

	addloop:
		ADC eax, 0
		SHR rcx, 1
	JNZ addloop

	ADC eax, 0

	RET
?BitCount@System@@YAI_K@Z	ENDP


ALIGN 16
?BitCount@System@@YAII@Z	PROC
	XOR eax, eax

	addloop:
		ADC eax, 0
		SHR ecx, 1
	JNZ addloop

	ADC eax, 0

	RET
?BitCount@System@@YAII@Z	ENDP

ALIGN 16
?BitCount@System@@YAIG@Z	PROC
	XOR eax, eax

	addloop:
		ADC eax, 0
		SHR cx, 1
	JNZ addloop

	ADC eax, 0

	RET
?BitCount@System@@YAIG@Z	ENDP

ALIGN 16
BitIndexLow_64	PROC
	BSF rax, rcx
	RET
BitIndexLow_64	ENDP

ALIGN 16
BitIndexLow_32	PROC
	BSF eax, ecx
	RET
BitIndexLow_32	ENDP

ALIGN 16
BitIndexLow_16	PROC
	XOR eax, eax
	BSF ax, cx
	RET
BitIndexLow_16	ENDP

ALIGN 16
BitIndexHigh_64	PROC
	BSR rax, rcx
	RET
BitIndexHigh_64	ENDP

ALIGN 16
BitIndexHigh_32	PROC
	BSR eax, ecx

	RET
BitIndexHigh_32	ENDP

ALIGN 16
BitIndexHigh_16	PROC
	XOR eax, eax
	BSR ax, cx
	RET
BitIndexHigh_16	ENDP


ALIGN 16
MemoryFill_SSE3	PROC
				
		MOV rax, rdx
		SHR rax, 4
		AND rdx, 15

		MOVD xmm2, r8
		MOVD xmm3, r9

		PSLLDQ xmm3, 8
		POR xmm2, xmm3

		TEST rax, rax
		JZ recpy


		TEST rcx, 0Fh
		JNZ usto_loop
align 16
			sto_loop:
				MOVNTDQ [rcx], xmm2
				ADD rcx, 16
			DEC rax
			JNZ sto_loop
			JMP recpy
align 16
			usto_loop:
				MOVDQU [rcx], xmm2
				ADD rcx, 16
			DEC rax
			JNZ usto_loop
		
		recpy:
			MOV rax, r8

			CMP edx, 8			
			JB ochunk8
				MOVNTI [rcx], rax
				ADD rcx, 16
				MOV rax, r9
			ochunk8:

				AND edx, 7
				CMP edx, 4
				JB ochunk4
					MOVNTI [rcx], eax
					ADD rcx, 4
					SHR rax, 32
				ochunk4:
			
					AND edx, 3
					CMP edx, 2
					JB ochunk2
						MOV [rcx], ax
						ADD rcx, 2
						SHR eax, 16
					ochunk2:

						AND edx, 1
						JZ exita
							MOV [rcx], al				
						exita:
		SFENCE

	RET
MemoryFill_SSE3	ENDP

ALIGN 16
MemoryFill	PROC

	MOV r11, rdi
		
		MOV rdi, rcx

		MOV rcx, rdx		
		SHR rcx, 2		
		AND rdx, 3

		MOV eax, r8d		
			REP STOSD

		TEST rdx, rdx
		JZ exita
			STOSB
			SUB rdx, 1
			JZ exita
				SHR eax, 8
				STOSB
				SUB rdx, 1
				JZ exita
					SHR eax, 8
					STOSB			
		exita:
		SFENCE

	MOV rdi, r11
	RET
MemoryFill	ENDP


ALIGN 16
MemoryCopy_SSE3	PROC
	CMP r8, 512
	JB MemoryCopy

	MOV r11, rdi
	MOV r10, rsi

		MOV rdi, rcx
		MOV rsi, rdx

		MOV rdx, 64

		MOV rcx, r8


		SUB rdi, rsi
		JS noreverse
			NEG rdx
			ADD rsi, r8
			
			SUB rsi, 1

			ADD rdi, rsi

			STD				
				AND rcx, 3Fh
				REP MOVSB
			CLD

			SUB rdi, rsi

			ADD rsi, rdx			

			ADD rsi, 1
			
		noreverse:

		PREFETCHNTA [rsi]
		PREFETCHNTA [rsi+rdx]
		
		MOV rcx, r8
		SHR rcx, 6
		SUB rcx, 2


		TEST rdi, 0Fh
		JNZ unaligned_dest
			TEST rsi, 0Fh
			JNZ uu_cpy64
				
align 16			
				aa_cpy64:
				
					PREFETCHNTA [rsi+2*rdx]
													
					MOVDQA xmm2, [rsi]
					MOVDQA xmm3, [rsi+16]
					MOVDQA xmm4, [rsi+32]
					MOVDQA xmm5, [rsi+48]					
					

					MOVNTDQ [rdi+rsi], xmm2
					MOVNTDQ [rdi+rsi+16], xmm3
					MOVNTDQ [rdi+rsi+32], xmm4
					MOVNTDQ [rdi+rsi+48], xmm5

					ADD rsi, rdx

					SUB rcx, 1
				JNZ aa_cpy64

			JMP copye

		unaligned_dest:
			TEST rsi, 0Fh
			JNZ uu_cpy64
		align 16
				ua_cpy64:
					PREFETCHNTA [rsi+2*rdx]
								
					MOVDQA xmm2, [rsi]
					MOVDQA xmm3, [rsi+16]
					MOVDQA xmm4, [rsi+32]
					MOVDQA xmm5, [rsi+48]

					MOVDQU [rdi+rsi], xmm2
					MOVDQU [rdi+rsi+16], xmm3
					MOVDQU [rdi+rsi+32], xmm4
					MOVDQU [rdi+rsi+48], xmm5

					ADD rsi, rdx

					SUB rcx, 1
				JNZ ua_cpy64

				JMP copye

		align 16
				uu_cpy64:
					PREFETCHNTA [rsi+2*rdx]
					
					MOVDQU xmm2, [rsi]
					MOVDQU xmm3, [rsi+16]
					MOVDQU xmm4, [rsi+32]
					MOVDQU xmm5, [rsi+48]

					MOVDQU [rdi+rsi], xmm2
					MOVDQU [rdi+rsi+16], xmm3
					MOVDQU [rdi+rsi+32], xmm4
					MOVDQU [rdi+rsi+48], xmm5

					ADD rsi, rdx

					SUB rcx, 1
				JNZ uu_cpy64


	copye:
		MOVDQU xmm2, [rsi]
		MOVDQU xmm3, [rsi+16]
		MOVDQU xmm4, [rsi+32]
		MOVDQU xmm5, [rsi+48]

		MOVDQU [rdi+rsi], xmm2
		MOVDQU [rdi+rsi+16], xmm3
		MOVDQU [rdi+rsi+32], xmm4
		MOVDQU [rdi+rsi+48], xmm5

		ADD rsi, rdx

		MOVDQU xmm2, [rsi]
		MOVDQU xmm3, [rsi+16]
		MOVDQU xmm4, [rsi+32]
		MOVDQU xmm5, [rsi+48]

		MOVDQU [rdi+rsi], xmm2
		MOVDQU [rdi+rsi+16], xmm3
		MOVDQU [rdi+rsi+32], xmm4
		MOVDQU [rdi+rsi+48], xmm5

	
		ADD rsi, rdx

		NEG rdx
		JNS completo
			MOV rcx, r8
			AND rcx, 3Fh

			ADD rdi, rsi

			REP MOVSB

		completo:
		
	SFENCE

	MOV rsi, r10
	MOV rdi, r11

	RET
MemoryCopy_SSE3	ENDP

ALIGN 16
MemoryCopy	PROC
		MOV r11, rsi
		MOV r10, rdi

		MOV rdi, rcx
		MOV rsi, rdx

		MOV rcx, r8

		CMP rdi, rsi
		JS docopy
		JZ donothing
			STD
			SUB rcx, 1
			ADD rsi, rcx
			ADD rdi, rcx
			ADD rcx, 1
		docopy:
			REP MOVSB
		donothing:

		CLD
		MOV rdi, r10
		MOV rsi, r11
	RET
MemoryCopy	ENDP


ALIGN 16
XMMPush	PROC
	FXSAVE [rcx]
	RET
XMMPush	ENDP

ALIGN 16
XMMPop	PROC
	FXRSTOR [rcx]
	RET
XMMPop	ENDP


END