/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once


#include <Windows.h>
#include "System\Nucleus.h"

#include "System\Memory.h"
#include "CoreLibrary\SFScore.h"

#define APPLICATION_GLOBAL_SINGLE	1
#define APPLICATION_LOCAL_SINGLE	2

#define INVOKE_IT	WM_USER+0x0259
// do fastcall for all event handlers

namespace Control {



class SimpleQueue {
	private:
		__int64 * ptr_lock, pout_ptr;		
		UI_64 size_val, update_val, q_flags;
		I_64 capacity[2];
		SFSco::Object q_obj[2];

	public:
		static const UI_64 type_id = 982451629;
		SimpleQueue();
		~SimpleQueue();

		UI_64 Init(__int64 unit_size, __int64 initial_capacity, const wchar_t * queue_name, SFSco::IAllocator * = SFSco::mem_mgr, UI_64 = type_id);

		UI_64 Push(const void * unit_pointer);
		UI_64 Pop(void * unit_pointer); // 0 - empty, 1 - valid, 2 - complete
		UI_64 Pull(void * unit_pointer); // 0 - empty, 1 - valid, 2 - complete

		void Drop(); // clear current
		void Clear(); // clear all
		void Destroy();
};


class Launcher sealed {
	private:	
		static struct IEntry {
			void * cfg[4];			
			wchar_t message[128];
		} i_list[32];

		static unsigned int i_count;

		static DWORD __stdcall IniProc(void *);
		
		static HANDLE _oblock;		

		Launcher();
		~Launcher();

	public:
		static const UI_64 type_id = 4022940023;

		static unsigned int AddInitializer(const wchar_t * message, void * callback, const void * context1, void * context2); // initializer must return unsigned int, __fastcall

		static UI_64 Run(unsigned int bg_resource, const wchar_t * version_string, SFSco::Object *); // supply bitmap resource

};


class Application sealed {
	private:
		static Application * _instance;
		
		HANDLE _mutex;
		
		Application();
		~Application();

		static wchar_t _name[256];
		
	public:
		static const UI_64 type_id = 4022940041;

		static unsigned int Create(const wchar_t *, int);

//		static int Initialize(HINSTANCE, const wchar_t *, int = 0, BOOL = FALSE);
		static unsigned int Close();

		static unsigned int Run(void *);
};

class Event: protected SFSco::Object {
	private:
		static const UI_64 gate_table_tid	= 4022940083;
		static const UI_64 drain_table_tid	= 4022940109;
		static const UI_64 event_table_tid	= 4022940127;
		static const UI_64 event_entry_tid	= 4022940139;
		static const UI_64 drain_entry_tid	= 4022940149;

		static const UI_64 mol_table_tid	= 4022940151;

		
//		static SFSco::Object MOL_table[2];
//		static UI_64 _molRunner[2], _molCapacity[2], _mol_selector;

		static SimpleQueue MOL_table;

		static Event * _instance;
		static Memory::Allocator * _echain;

		static SFSco::Object _drains, _gates;		
		static UI_64 _drains_runner, _drains_top, _gates_runner, _gates_top, asta_la_vista;
			
		static HANDLE _processorThread;
		static DWORD __stdcall Processor(void *);
		
		Event();
		virtual ~Event();

		
	public:
		static const UI_64 type_id = 4022940067;

		static UI_64 __fastcall Initialize();
		static unsigned int Finalize();
		static void ShutDown();

		static void MOLUpdate(SFSco::Object &);

		static void Cancel(UI_64);
				
		static UI_64 SetGate(UI_64 event_idx, SFSco::Object & gate_obj, UI_64 state_mask); // add a gate for an event
		static UI_64 CreateDrain(const void * callback, const Object * cbArg, const Object * cbObj); // override drain for an event

		static void RemoveGate(SFSco::Object &); // removes reference of an Object from drains and gates tables, and relateed event_entries
				

};

	extern "C" UI_64 __fastcall Event_DrainIt(void * context, void * val, void * callback);
	extern "C" UI_64 __fastcall Control_PunchIt(void *);
}

