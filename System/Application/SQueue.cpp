#include "System\Control.h"
#include "System\SysUtils.h"


Control::SimpleQueue::SimpleQueue() : ptr_lock(0), pout_ptr(-1), size_val(0), update_val(0), q_flags(0) {
	capacity[0] = capacity[1] = 0;

	size_val = (size_val + 63) & (~63);
		
}

UI_64 Control::SimpleQueue::Init(__int64 s_val, __int64 u_val, const wchar_t * q_n, SFSco::IAllocator * q_a, UI_64 q_t) {
	UI_64 result(-1);
	
	if (!size_val) {
		ptr_lock = Memory::Chain::IncLock(q_n);

		if (s_val < 0) {
			size_val = sizeof(SFSco::Object);
			q_flags = 1;
		} else {
			size_val = ((s_val + 15) & (~15));
		}

		update_val = u_val;

		result = q_obj[0].New(q_t, update_val*size_val, q_a);
		result |= q_obj[1].New(q_t, update_val*size_val, q_a);

		if (result != -1) {
			capacity[0] = capacity[1] = update_val;
			result = 0;
		}
	}
	
	return result;
}

Control::SimpleQueue::~SimpleQueue() {

}

void Control::SimpleQueue::Destroy() {
	q_obj[0].Destroy();
	q_obj[1].Destroy();

}

UI_64 Control::SimpleQueue::Push(const void * src_ptr) {
	UI_64 result, q_selector;
	__int64 some_val;
		

	q_selector = (ptr_lock[0] & 1);
	result = ((q_selector + 1)<<5);
		
	if (ptr_lock[result] >= capacity[q_selector]) {
		if (q_obj[q_selector].Expand(update_val*size_val) != -1) {
			capacity[q_selector] += update_val;
		}
	}

	if (ptr_lock[result] < capacity[q_selector]) {
		if (q_flags) {
			if (SFSco::Object * q_ptr = reinterpret_cast<SFSco::Object *>(q_obj[q_selector].Acquire())) {				
				
				for (some_val = 0;some_val < ptr_lock[result];some_val++) {
					if (q_ptr[some_val] == reinterpret_cast<const SFSco::Object *>(src_ptr)[0]) {
						break;
					}
				}

				if ((some_val >= ptr_lock[result]) || (ptr_lock[result] == 0)) {
					System::MemoryCopy_SSE3(q_ptr + ptr_lock[result], src_ptr, sizeof(SFSco::Object));
					InterlockedIncrement64(ptr_lock + result);
				}

				q_obj[q_selector].Release();
			}


		} else {

			if (char * q_ptr = reinterpret_cast<char *>(q_obj[q_selector].Acquire())) {
				System::MemoryCopy_SSE3(q_ptr + ptr_lock[result]*size_val, src_ptr, size_val);
				InterlockedIncrement64(ptr_lock + result);

				q_obj[q_selector].Release();
			}
		}
	}


	return result;
}


UI_64 Control::SimpleQueue::Pop(void * dst_ptr) {
	UI_64 result, q_selector;
	
	q_selector = ((ptr_lock[0] & 1) ^ 1);
	result = ((q_selector + 1)<<5);
	
	if (ptr_lock[result]) {
		if (char * q_ptr = reinterpret_cast<char *>(q_obj[q_selector].Acquire())) {
			System::MemoryCopy_SSE3(dst_ptr, q_ptr + --ptr_lock[result]*size_val, size_val);

			q_obj[q_selector].Release();		
				
			if (ptr_lock[result]) {
				result = 1;
			} else {
				InterlockedIncrement64(ptr_lock);
				result = 2;
			}
		}
	}
	
	if (result > 2) {
		result = 0;
		InterlockedIncrement64(ptr_lock);
	}

	return result;
}


UI_64 Control::SimpleQueue::Pull(void * dst_ptr) {
	UI_64 result, q_selector;
	
	q_selector = ((ptr_lock[0] & 1) ^ 1);
	result = ((q_selector + 1)<<5);
	
	if (ptr_lock[result]) {
		if (pout_ptr < 0) pout_ptr = 0;
		else pout_ptr++;

		if (char * q_ptr = reinterpret_cast<char *>(q_obj[q_selector].Acquire())) {
			System::MemoryCopy_SSE3(dst_ptr, q_ptr + pout_ptr*size_val, size_val);

			q_obj[q_selector].Release();
				
			if ((pout_ptr + 1) < ptr_lock[result]) {
				result = 1;
			} else {
				pout_ptr = -1;

				InterlockedExchange64(ptr_lock + result, 0);
				InterlockedIncrement64(ptr_lock);
				result = 2;
			}
		}
	}

	if (result > 2) {
		result = 0;
		InterlockedIncrement64(ptr_lock);		
	}

	return result;
}


void Control::SimpleQueue::Drop() {
	UI_64 result, q_selector;
	
	q_selector = ((ptr_lock[0] & 1) ^ 1);
	result = ((q_selector + 1)<<5);
	
	InterlockedExchange64(ptr_lock + result, 0);
	InterlockedIncrement64(ptr_lock);

}

void Control::SimpleQueue::Clear() {	
	InterlockedExchange64(ptr_lock + 32, 0);
	InterlockedExchange64(ptr_lock + 64, 0);
}

