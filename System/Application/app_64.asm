
; safe-fail base elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE



.CODE

ALIGN 16
Control_PunchIt	PROC
	MOV rax, rcx
				
	SUB rsp, 40
		MOV rdx, [rax]
		MOV rcx, [rax+8]
		MOV r8, [rax+24]
		
		TEST rcx, rcx
		CMOVZ rcx, rdx
			
		CALL QWORD PTR [rax+16]
	ADD rsp, 40
		
	RET
Control_PunchIt	ENDP

ALIGN 16
Event_DrainIt	PROC
	SUB rsp, 40
	
	TEST rcx, rcx
	CMOVZ rcx, rdx

	CALL r8

	ADD rsp, 40
	RET
Event_DrainIt	ENDP


END


