/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\Control.h"
#include "System\SysUtils.h"

// Event =====================================================================================================================================================================
#define EVENT_SED_COUNT	2048
#define OBJECT_STRIDE	(sizeof(SFSco::Object)+sizeof(UI_64))/sizeof(UI_64)
#define DRAIN_ROW_STRIDE	(2*OBJECT_STRIDE+2)

#define MAX_EVENTS_PER_GATE	16

#define MIN_MOL_CAPACITY	2048


Control::SimpleQueue Control::Event::MOL_table;



Memory::Allocator * Control::Event::_echain = 0;

SFSco::Object Control::Event::_drains;
SFSco::Object Control::Event::_gates;

UI_64 Control::Event::_drains_runner = 0;
UI_64 Control::Event::_drains_top = 0;

UI_64 Control::Event::_gates_runner = 0;
UI_64 Control::Event::_gates_top = 0;



UI_64 Control::Event::asta_la_vista = 1;
HANDLE Control::Event::_processorThread = 0;

Control::Event * Control::Event::_instance = 0;
/* 
	gates_table layout
		active flag, event_runner, (gate_required_state, event_reference)x (max events per single gate)

	
	drains_table layaout
		(callback, callback_argument_object_data, class_object_data, ref_count)
			


*/ 


UI_64 __fastcall Control::Event::Initialize() {
	wchar_t pool_name[] = {10, L'e',L'v', L'e', L'n', L't', L' ', L'p', L'o', L'o', L'l'};
	wchar_t q_name[] = {11, L'e',L'v', L'e', L'n', L't', L' ', L'q', L'u', L'e', L'u', L'e'};

	UI_64 result(0);

	if (_instance) return 0;

	if (_echain = new Memory::Allocator(0)) {
		result = _echain->Init(new Memory::Buffer(), 0x00100000, pool_name);
		if (!result) {
			result = MOL_table.Init(sizeof(SFSco::Object), MIN_MOL_CAPACITY, q_name, _echain, mol_table_tid);
		}
	} else {
		result = ERROR_OUTOFMEMORY;
	}

	if (!result) {
		result = _gates.New(gate_table_tid, EVENT_SED_COUNT*MAX_EVENTS_PER_GATE*2*sizeof(UI_64), _echain); // list of all sources: indicies into _echain
		if (result != -1) {
			_gates_top = EVENT_SED_COUNT;

			result = _drains.New(drain_table_tid, EVENT_SED_COUNT*DRAIN_ROW_STRIDE*sizeof(UI_64), _echain); // list of all drains (context, context, collback, reserved)
			if (result != -1) {
				_drains_top = EVENT_SED_COUNT;
				result = 0;
			} else result = 698;
			
		} else result = 698;
	}
	
	if (!result) {
		if (_instance = new Event()) {
			_processorThread = CreateThread(0, 0x20000, Processor, 0, 0, 0);

			if (!_processorThread) result = GetLastError();
		} else {
			result = ERROR_OUTOFMEMORY;
		}
	}

	return result;
}

void Control::Event::ShutDown() {
	asta_la_vista = 0;
}

unsigned int Control::Event::Finalize() {
	asta_la_vista = 0;
	
	if (_processorThread) {
		WaitForSingleObject(_processorThread, INFINITE);
		CloseHandle(_processorThread);
	}

	if (_echain) delete _echain;
	_echain = 0;

	if (_instance) delete _instance;
	_instance = 0;

	return 0;
}

Control::Event::Event() {
	SetTID(type_id);
}

Control::Event::~Event() {

}

UI_64 Control::Event::SetGate(UI_64 drain_idx, SFSco::Object & gate_obj, UI_64 state_mask) {
	UI_64 result(-1), empst(-1);
	
	if (drain_idx >= _drains_runner) return -1;

	if (gate_obj.Is(Nucleus::type_id)) {
		if (Nucleus::Header * g_hdr = reinterpret_cast<Nucleus::Header *>(gate_obj.Acquire())) {
			result = g_hdr->gate_idx;
			gate_obj.Release();
		}
	} else {
		return -1;
	}
	
	if (result == -1)
	if (UI_64 * gate_table = reinterpret_cast<UI_64 *>(_gates.Acquire())) {
		for (UI_64 i(0);i<_gates_runner;i++) {
			if (gate_table[MAX_EVENTS_PER_GATE*2*i] == 0) {	// is it active				

				result = i;
				break;
			}
		}

		_gates.Release();
	}
	

	if (result == -1) {
		result = _gates_runner++;
		
		if (UI_64 * gate_table = reinterpret_cast<UI_64 *>(_gates.Acquire())) {			
			gate_table[MAX_EVENTS_PER_GATE*2*result] = 0;

			_gates.Release();
		}
	}

	if (Nucleus::Header * g_hdr = reinterpret_cast<Nucleus::Header *>(gate_obj.Acquire())) {
		g_hdr->gate_idx = result;
		gate_obj.Release();
	}

	if (UI_64 * gate_table = reinterpret_cast<UI_64 *>(_gates.Acquire())) {
		result *= MAX_EVENTS_PER_GATE*2;
		empst = -1;

		for (UI_64 i(1);i<=gate_table[result];i++) {
			if (gate_table[result + i*2 + 1] == -1) {
				empst = i;
				break;
			}

		}

		if (empst == -1) empst = ++gate_table[result];
		if (empst > (MAX_EVENTS_PER_GATE -1)) empst = (MAX_EVENTS_PER_GATE -1);

		result += empst*2;

		gate_table[result] = state_mask;
		gate_table[result+1] = drain_idx;


		if (UI_64 * drains_table = reinterpret_cast<UI_64 *>(_drains.Acquire())) {
			InterlockedIncrement64((__int64 *)drains_table+drain_idx*DRAIN_ROW_STRIDE+1+2*OBJECT_STRIDE); // SFSco::LockedInc

			_drains.Release();
		}

		_gates.Release();
	}
	

	if (_gates_runner >= _gates_top) {
		if (_gates.Expand(EVENT_SED_COUNT*MAX_EVENTS_PER_GATE*2*sizeof(SFSco::Object)) != -1) {
			_gates_top += EVENT_SED_COUNT;
		}
	}


	return result;
}

UI_64 Control::Event::CreateDrain(const void * callback, const Object * cbArg, const Object * cbObj) {
	/*
		find the drain in drains table
			
		if exists override drain reference in event entry

		else add drain to the table and override the reference

	*/
	UI_64 result(-1), empst(-1);
	SFSco::Object * aObj(0), tempo;


	if (void ** drains_table = reinterpret_cast<void **>(_drains.Acquire())) {		
		for (UI_64 i(0);i<_drains_runner;i++) {
			if (drains_table[i*DRAIN_ROW_STRIDE]) {
				if (drains_table[i*DRAIN_ROW_STRIDE] == callback) {					
					if (cbArg) {
						if (drains_table[i*DRAIN_ROW_STRIDE+1]) {
							aObj = reinterpret_cast<SFSco::Object *>(drains_table+i*DRAIN_ROW_STRIDE+1);
							if (aObj[0] == cbArg[0]) {
								if (cbObj) {
									if (drains_table[i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE]) {							
										aObj = reinterpret_cast<SFSco::Object *>(drains_table+i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE);
										if (aObj[0] == cbObj[0]) {
											result = i;
											break;
										}
									}
								} else {
									if (!drains_table[i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE]) {
										result = i;
										break;
									}
								}
							}
						}
					} else {
						if (!drains_table[i*DRAIN_ROW_STRIDE+1]) {
							if (cbObj) {
								if (drains_table[i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE]) {							
									aObj = reinterpret_cast<SFSco::Object *>(drains_table+i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE);
									if (aObj[0] == cbObj[0]) {
										result = i;
										break;
									}
								}
							} else {
								if (!drains_table[i*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE]) {
									result = i;
									break;
								}
							}
						}
					}
				}
			} else {
				if (empst == -1) empst = i;
			}
		}

		if (result == -1) {
			if (empst == -1) empst = System::LockedInc_64(&_drains_runner);			

			drains_table[empst*DRAIN_ROW_STRIDE] = const_cast<void *>(callback);
			drains_table[empst*DRAIN_ROW_STRIDE+1] = 0;
			drains_table[empst*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE] = 0;
						
			if (cbArg) {
				System::MemoryCopy(drains_table+empst*DRAIN_ROW_STRIDE+1, cbArg, sizeof(SFSco::Object));
			}
			if (cbObj) {
				System::MemoryCopy(drains_table+empst*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE, cbObj, sizeof(SFSco::Object));
			}

			drains_table[empst*DRAIN_ROW_STRIDE+1+2*OBJECT_STRIDE] = 0; // ref_count
			result = empst;
		}

/*
		if (result != -1) {
			LockedInc(reinterpret_cast<UI_64 *>(drains_table+result*DRAIN_ROW_STRIDE+1+2*OBJECT_STRIDE));
		}
*/

		_drains.Release();
			
	}

	if (_drains_runner >= _drains_top) {
		if (_drains.Expand(EVENT_SED_COUNT*DRAIN_ROW_STRIDE*sizeof(UI_64)) != -1) {
			_drains_top += EVENT_SED_COUNT;
		}

	}
	
	return result;
}

void Control::Event::RemoveGate(SFSco::Object & ref) {
	// clear related drain, gates and events
	UI_64 result(0);

	if (Nucleus::Header * r_hdr = reinterpret_cast<Nucleus::Header *>(ref.Acquire())) {
		if (r_hdr->gate_idx < _gates_runner) {
			result = MAX_EVENTS_PER_GATE*2*r_hdr->gate_idx;

			if (UI_64 * gate_table = reinterpret_cast<UI_64 *>(_gates.Acquire())) {
				if (UI_64 * drains_table = reinterpret_cast<UI_64 *>(_drains.Acquire())) {
					for (unsigned int i(1);i<=gate_table[result];i++) {						
						InterlockedDecrement64((long long *)drains_table + gate_table[result + i*2 +1]*DRAIN_ROW_STRIDE +1 + 2*OBJECT_STRIDE); //  -- SFSco::LockedDec

					}
				
					_drains.Release();
				}

				gate_table[result] = 0;
				
				r_hdr->gate_idx = -1;

				_gates.Release();
			}			
		}
		ref.Release();
	}

}

void Control::Event::Cancel(UI_64 drain_idx) {

	if (drain_idx >= _drains_runner) return;

	if (UI_64 * drains_table = reinterpret_cast<UI_64 *>(_drains.Acquire())) {
		drains_table[drain_idx*DRAIN_ROW_STRIDE] = 0;

		_drains.Release();
	}
}


DWORD __stdcall Control::Event::Processor(void * context) {
	I_64 pool_index[2*MAX_POOL_COUNT];

	UI_64 result(0), gate_selector(0), clear_state(0), in_state(0), ii(0);
	SFSco::Object * cbObj(0), * cbArg(0), obj_arg, obj_cntx;

	void ** drains_table = 0, * action_ptr(0);
	UI_64 * gates_table(0);

	Control::Nucleus nuclobj;

//	Control::Nucleus * mol_table(0);

	Memory::Chain::RegisterThread(pool_index);

	if (HANDLE ebreaker = CreateEvent(0, 0, 0, 0)) {

		for (;asta_la_vista;) {			
			WaitForSingleObject(ebreaker, 13);
			
			if (_gates_runner && _drains_runner) {
				if (ii = MOL_table.Pop(&nuclobj)) {					
					if ((drains_table = reinterpret_cast<void **>(_drains.Acquire())) && (gates_table = reinterpret_cast<UI_64 *>(_gates.Acquire()))) {
						for (;;ii = MOL_table.Pop(&nuclobj)) {
							gate_selector = -1;
							if (Nucleus::Header * n_hdr = reinterpret_cast<Nucleus::Header *>(nuclobj.Acquire())) {
								if (n_hdr->gate_idx < _gates_runner) { // mol_table[2*i]
									gate_selector = n_hdr->gate_idx*MAX_EVENTS_PER_GATE*2;

								}
								nuclobj.Release();
							}

							if (gate_selector != -1) {
								clear_state = 0;
								in_state = nuclobj.GetState();

							
								for (UI_64 j(1);j<=gates_table[gate_selector];j++) {
									if ((in_state & gates_table[gate_selector + j*2]) == gates_table[gate_selector + j*2]) {					
										clear_state |= gates_table[gate_selector + j*2];
					// drain it
										cbArg = cbObj = 0;

										action_ptr = drains_table[gates_table[gate_selector + j*2 + 1]*DRAIN_ROW_STRIDE];

										if (drains_table[gates_table[gate_selector + j*2 + 1]*DRAIN_ROW_STRIDE+1]) {												
											System::MemoryCopy(&obj_arg, drains_table+gates_table[gate_selector + j*2 + 1]*DRAIN_ROW_STRIDE+1, sizeof(SFSco::Object));
											cbArg = &obj_arg;
										}

										if (drains_table[gates_table[gate_selector + j*2 + 1]*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE]) {												
											System::MemoryCopy(&obj_cntx, drains_table+gates_table[gate_selector + j*2 + 1]*DRAIN_ROW_STRIDE+1+OBJECT_STRIDE, sizeof(SFSco::Object));
											cbObj = &obj_cntx;
										}
														
																
										_gates.Release(); gates_table = 0;
										_drains.Release(); drains_table = 0;


											Event_DrainIt(cbObj, cbArg, action_ptr);
							

								
										gates_table = reinterpret_cast<UI_64 *>(_gates.Acquire());
										drains_table = reinterpret_cast<void **>(_drains.Acquire());

									
										if ((!gates_table) || (!drains_table)) break;
									}
								}

								if (clear_state) nuclobj.ClearState(clear_state);
							}

							if (ii > 1) break;
						}
					}
				
					if (gates_table) _gates.Release();
					if (drains_table) _drains.Release();
				}
			}

	// memory manager fuckup
			for (unsigned int i(0);i<MAX_POOL_COUNT;i++) {
				if (pool_index[2*i]) {
					result += 3;
				}

			}

		}

		CloseHandle(ebreaker);
	}

	Memory::Chain::UnregisterThread();

	return result;
}

void Control::Event::MOLUpdate(SFSco::Object & nucl) {
	MOL_table.Push(&nucl);
}
