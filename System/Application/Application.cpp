/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\Control.h"
#include "System\Window.h"
#include <Ole2.h>
#include <GdiPlus.h>


#define INVOKE_IT	WM_USER+0x0259

const unsigned int launcher_hash[8] = {0xED4FFAAE, 0x1C66073F, 0xFC3AC758, 0x6B6EFB1C, 0xB1EDE1FF, 0x3BADB20C, 0xB2B9D6E9, 0x1EDB9AB5};

// splash screen

class SplashScreen: public Window {
	private:
		static SplashScreen * _instance;
		static ULONG_PTR _gdi_token;

		Gdiplus::Image * _bgi;
		Gdiplus::Font * _arial;
		Gdiplus::SolidBrush * _message_brush, * _version_brush;
		Gdiplus::PointF _message_pos, _version_pos;

		const wchar_t * _message_ptr, * _version_ptr;
		
		SplashScreen() : _bgi(0), _message_ptr(0), _arial(0), _message_brush(0), _version_brush(0), _version_ptr(0) {

		}

		virtual ~SplashScreen() {
			if (_bgi) delete _bgi;
			if (_arial) delete _arial;
			if (_message_brush) delete _message_brush;
			if (_version_brush) delete _version_brush;
		}

		
		virtual unsigned int Loop(LRESULT & res, UINT msg, WPARAM wp, LPARAM lp) {
			switch (msg) {
				case WM_PAINT:
				{
					PAINTSTRUCT pstr;
					Gdiplus::Graphics grp(BeginPaint(GetHandle(), &pstr));

					if (grp.GetLastStatus() == Gdiplus::Ok) {
						grp.DrawImage(_bgi, 0, 0, _bgi->GetWidth(), _bgi->GetHeight());
						grp.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
						if (_version_ptr) grp.DrawString(_version_ptr, -1, _arial, _version_pos, _version_brush);
						if (_message_ptr) grp.DrawString(_message_ptr, -1, _arial, _message_pos, _message_brush);

					}

					EndPaint(GetHandle(), &pstr);
					ValidateRect(GetHandle(), 0);

					res = 0;
				}
				break;
			}

			return 0;
		}

	public:
		static void SetInfoMessage(const wchar_t * mes_ptr) {
			if (_instance) {
				_instance->_message_ptr = mes_ptr;
			}
		}

		static void Destroy() {
			if (HANDLE delay = CreateEvent(0, 0, 0, 0)) {
				WaitForSingleObject(delay, 250);
				CloseHandle(delay);
			}

			if (_instance) {
				DestroyWindow(_instance->GetHandle());
				delete _instance;
				_instance = 0;
			}

			if (_gdi_token) Gdiplus::GdiplusShutdown(_gdi_token);
			_gdi_token = 0;

		}

		static unsigned int Create(unsigned int bitmapco, const wchar_t * ver_str, SFSco::Object * sp_buf) {
			wchar_t temp_str[64];
			unsigned int result(-1), x_size(0);
			unsigned int screen_hash[8] = {0, 0, 0, 0, 0, 0, 0, 0};

			IStream * image_stream(0);
			Gdiplus::GdiplusStartupInput gdi_input;
			Gdiplus::GdiplusStartupOutput gdi_output;
			HRSRC i_res(0);
			DWORD re_size(0);
					
			if (bitmapco) {
				if (!_instance)
				if (_instance = new SplashScreen()) {
					SFSco::GetHeaderStr(19, temp_str);
					result = _instance->ClassInit(temp_str + 1, CS_OWNDC); // L"{FE34EF7D-90AC-47E0-B174-40ADBF57328A}"

					if (!result) {
						if (Gdiplus::GdiplusStartup(&_gdi_token, &gdi_input, &gdi_output) == Gdiplus::Ok) {
							if (i_res = FindResource(GetModuleHandle(0), MAKEINTRESOURCE(bitmapco & 0x0000FFFF), MAKEINTRESOURCE(256)))
							if (void * launcher_im_ptr = LockResource(LoadResource(GetModuleHandle(0), i_res))) {
								re_size = SizeofResource(GetModuleHandle(0), i_res);

								SFSco::sha256(screen_hash, reinterpret_cast<char *>(launcher_im_ptr), re_size, 1);
																
								if (HGLOBAL stream_buf = GlobalAlloc(GMEM_MOVEABLE, re_size)) {
									if (void * stream_buf_ptr = GlobalLock(stream_buf)) {
										CopyMemory(stream_buf_ptr, launcher_im_ptr, re_size);
										GlobalUnlock(stream_buf);
	
										CreateStreamOnHGlobal(stream_buf, TRUE, &image_stream);
										if (image_stream) {

											_instance->_bgi = Gdiplus::Image::FromStream(image_stream);
											if (_instance->_bgi->GetLastStatus() == Gdiplus::Ok) {
												_instance->_arial = new Gdiplus::Font(L"Arial", 10.0f, Gdiplus::FontStyleRegular);
												_instance->_message_brush = new Gdiplus::SolidBrush(Gdiplus::Color(25, 84, 49));
												_instance->_version_brush = new Gdiplus::SolidBrush(Gdiplus::Color(110, 17, 54));

												_instance->_message_pos.X = 55.0f;
												_instance->_message_pos.Y = 210.f;

												_instance->_version_pos.X = 250.0f;
												_instance->_version_pos.Y = 170.0f;
													
												_instance->_version_ptr = ver_str;

												for (unsigned int i(0);i<8;i++) result ^= (screen_hash[i] ^ launcher_hash[i]);


												if ((bitmapco >>= 16) && (sp_buf)) {	
													if (i_res = FindResource(GetModuleHandle(0), MAKEINTRESOURCE(bitmapco), MAKEINTRESOURCE(256)))
													if (void * launcher_im_ptr = LockResource(LoadResource(GetModuleHandle(0), i_res))) {
														re_size = SizeofResource(GetModuleHandle(0), i_res);
														SFSco::sha256(screen_hash, reinterpret_cast<char *>(launcher_im_ptr), re_size, 1);

														if (HGLOBAL stream_buf = GlobalAlloc(GMEM_MOVEABLE, re_size)) {
															if (void * stream_buf_ptr = GlobalLock(stream_buf)) {
																CopyMemory(stream_buf_ptr, launcher_im_ptr, re_size);
																GlobalUnlock(stream_buf);
	
																CreateStreamOnHGlobal(stream_buf, TRUE, &image_stream);
																if (image_stream) {
																	Gdiplus::SolidBrush black_brush(Gdiplus::Color(100, 100, 100));
																	Gdiplus::Font calibri(L"Calibri", 18.0f, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
																	Gdiplus::Bitmap * about_image = Gdiplus::Bitmap::FromStream(image_stream);

																	if (about_image->GetLastStatus() == Gdiplus::Ok) {

																		Gdiplus::BitmapData bitmapData;
																		Gdiplus::Rect irect(0, 0, about_image->GetWidth(), about_image->GetHeight());																				

																		if (sp_buf->New(0, (((about_image->GetWidth()+15)&(~15)) * ((about_image->GetHeight()+15)&(~15)) + 4)*sizeof(unsigned int)) != -1) {
																			if (unsigned int * pi_ptr = reinterpret_cast<unsigned int *>(sp_buf->Acquire())) {
																				pi_ptr[0] = pi_ptr[1] = pi_ptr[2] = pi_ptr[3] = 0;

																				sp_buf->Release();
																			}

																			Gdiplus::Graphics agraph(about_image);

																	for (unsigned int i(0);i<8;i++) result ^= screen_hash[i];
														// write text

																			if (agraph.GetLastStatus() == Gdiplus::Ok) {
																				agraph.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
																				agraph.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
																				agraph.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);

																				if (ver_str) agraph.DrawString(ver_str + 2, -1, &calibri, Gdiplus::PointF(350.0f, 61.0f), &black_brush);
																				
																				about_image->LockBits(&irect, Gdiplus::ImageLockModeRead, 9 | (32 << 8) | 0x00020000, &bitmapData); // Gdiplus::PixelFormat32bppRGB

																				SFSco::sha256(screen_hash, reinterpret_cast<char *>(launcher_im_ptr), re_size, 1);

																				if (unsigned int * pi_ptr = reinterpret_cast<unsigned int *>(sp_buf->Acquire())) {
																					pi_ptr[0] = bitmapData.Width;
																					pi_ptr[1] = bitmapData.Height;
																					pi_ptr[2] = (bitmapData.Stride>>2);
																					pi_ptr[3] = ((bitmapData.Height+15)&(~15));

																					SFSco::MemoryCopy_SSE(pi_ptr+4, bitmapData.Scan0, bitmapData.Stride*bitmapData.Height);
															

																					sp_buf->Release();
																				}

																				about_image->UnlockBits(&bitmapData);
																			}
																		}
																	}
																	if (about_image) delete about_image;
																}
															}
														}
													}
												}
												
												SFSco::GetHeaderStr(21, temp_str);

												result |= _instance->::Window::Create(temp_str + 1, 0, WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_POPUP, WILOC_CENTER_CENTER | ((UI_64)(_instance->_bgi->GetWidth() & 0xFFFF)<<32) | ((UI_64)(_instance->_bgi->GetHeight() & 0xFFFF)<<48), 0, WS_EX_TOPMOST); // L"SFS splash"

												if (!result) {
							//	_instance->SetRoundRectRegion(16);
													_instance->Show(SW_SHOWNORMAL);
												}
											}
										}
									}
								}
							}
						} else {
							// gdi error
						}
					}
				} else {
					result = ERROR_OUTOFMEMORY;
				}
																	
				
			}

			return result;
		}


		static SplashScreen * GetInstance() {
			return _instance;
		}
};

SplashScreen * SplashScreen::_instance = 0;
ULONG_PTR SplashScreen::_gdi_token = 0;


// Application ===================================================================================================================================================================================================================================
Control::Application * Control::Application::_instance = 0;

Control::Application::Application() : _mutex(0) {
	

}

Control::Application::~Application() {
	if (_mutex) CloseHandle(_mutex);
}

unsigned int Control::Application::Create(const wchar_t * ana, int min) {
	unsigned int result(0);
	if (_instance) return result;

	if (_instance = new Application()) {
		wchar_t muname[256] = {0};
		UI_64 mlength(0);
		HANDLE xlog = OpenEventLog(0, L"System");
		BYTE eBuf[512];
		EVENTLOGRECORD * evt = (EVENTLOGRECORD *)eBuf;
		DWORD bRead(0), bNeed(0), zz(0);
				
		switch (min) {
			case APPLICATION_GLOBAL_SINGLE:
				muname[0] = L'G';
				muname[1] = L'l';
				muname[2] = L'o';
				muname[3] = L'b';
				muname[4] = L'a';
				muname[5] = L'l';
				muname[6] = L'\\';
				mlength = 7;
				
			case APPLICATION_LOCAL_SINGLE:
				
				SFSco::MemoryCopy(muname+mlength, ana, wcslen(ana)*sizeof(wchar_t));
				mlength += wcslen(ana);

				if (xlog) {
					if (ReadEventLog(xlog, EVENTLOG_SEQUENTIAL_READ | EVENTLOG_BACKWARDS_READ, 0, eBuf, 512, &bRead, &bNeed)) {
						zz = evt->RecordNumber;

						for (;;) {
							zz--;
							if (!zz) break;

							if (ReadEventLog(xlog, EVENTLOG_SEEK_READ | EVENTLOG_FORWARDS_READ, zz, eBuf, 512, &bRead, &bNeed)) {
								if (((evt->EventID & 0xFFFF) == 6005) && (evt->EventType == EVENTLOG_INFORMATION_TYPE)) {
									SFSco::StringToHexString(muname + mlength, &evt->TimeGenerated, 4, 1);
									mlength += 8;
									
									break;
								}
							}
						}
					}

				}

				muname[mlength] = L'\0';
				_instance->_mutex = CreateMutex(0, 0, muname);
				result = GetLastError();

			break;
		}

		if (!result) {
			// continue with application creATE
		

		}


		if (xlog) CloseEventLog(xlog);
	} else {
		result = ERROR_OUTOFMEMORY;
	}

	return result;
}

unsigned int Control::Application::Close() {
	if (_instance) delete _instance;
	_instance = 0;

	return 0;
}


unsigned int Control::Application::Run(void * cnt) {
	I_64 pool_index[2*MAX_POOL_COUNT];
	unsigned int result(0);


	BOOL running(TRUE);
	MSG threadMessage;

// set thread affinity
	SplashScreen::Destroy();

	SFSco::RegisterThread(pool_index);

//	SetThreadAffinityMask(GetCurrentThread(), 0x00000003);
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
	SetThreadExecutionState(ES_CONTINUOUS | ES_DISPLAY_REQUIRED);

	while (running) {
		if (PeekMessage(&threadMessage, 0, 0, 0, PM_REMOVE)) {
			if (threadMessage.message != INVOKE_IT) {

				switch (threadMessage.message) {
					case WM_QUIT:
						running = 0;
					break;
					case WM_KEYUP: case WM_KEYDOWN:
						switch (threadMessage.wParam) {
							case VK_SHIFT: case VK_CONTROL: case VK_MENU: case VK_INSERT:
						//	keyConfig[threadMessage.wParam] ^= 0x80;
							break;
						}
					default:
						DispatchMessage(&threadMessage);
				
				}
			} else {
				reinterpret_cast<UI_64 *>(threadMessage.wParam)[3] = Control_PunchIt(reinterpret_cast<void *>(threadMessage.wParam));
				if (threadMessage.lParam) SetEvent((HANDLE)threadMessage.lParam);
			}


		} else {
			WaitMessage();
//			GetKeyboardState(keyConfig);

		}

	// memory manager fuckup
			for (unsigned int i(0);i<MAX_POOL_COUNT;i++) {
				if (pool_index[2*i]) {
					result += 3;
				}

			}
		
		
	}

	SFSco::UnregisterThread();

	return result;

}


// Launcher =======================================================================================================================================================================

unsigned int Control::Launcher::i_count = 0;
Control::Launcher::IEntry Control::Launcher::i_list[32];

HANDLE Control::Launcher::_oblock = 0;

Control::Launcher::Launcher() {

}

Control::Launcher::~Launcher() {

}

unsigned int Control::Launcher::AddInitializer(const wchar_t * message, void * callback, const void * context1, void * context2) {
	unsigned int result(0);

	if (!i_count) {
		SFSco::MemoryFill(i_list, 32*sizeof(IEntry), 0);		
	}

	result = wcslen(message)*sizeof(wchar_t);
	if (result > 127) result = 127;

	i_list[i_count].cfg[0] = context2;
	i_list[i_count].cfg[1] = const_cast<void *>(context1);
	i_list[i_count].cfg[2] = callback;
	i_list[i_count].cfg[3] = 0; // reinterpret_cast<void *>(&result);

	SFSco::MemoryCopy(i_list[i_count++].message, message, result);

	return result;
}


DWORD __stdcall Control::Launcher::IniProc(void * context) {
	UI_64 result(0);
	
	for (unsigned int i(0);i<i_count;i++) {
// post message
		SplashScreen::SetInfoMessage(i_list[i].message);
		InvalidateRect((HWND)context, 0, 0);
	//	SendMessage((HWND)context, WM_PAINT, 0, 0);
		
		if ((reinterpret_cast<UI_64 *>(i_list[i].cfg)[0] == 1) && (context)) {
			i_list[i].cfg[0] = context;
			ResetEvent(_oblock);

			PostMessage((HWND)context, INVOKE_IT, (WPARAM)i_list[i].cfg, (LPARAM)_oblock);
				
		} else {
			if (i_list[i].cfg[0] == 0) i_list[i].cfg[0] = context;
			reinterpret_cast<UI_64 *>(i_list[i].cfg)[3] = Control_PunchIt(i_list[i].cfg);
		}

		WaitForSingleObject(_oblock, INFINITE);
				
		if (result = reinterpret_cast<UI_64 *>(i_list[i].cfg)[3]) break;		
	}

	if (context) PostMessage((HWND)context, WM_QUIT, 0, 0);

	return result;
}

UI_64 Control::Launcher::Run(unsigned int bres, const wchar_t * v_str, SFSco::Object * sp_buf) {
	UI_64 f_time;
	UI_64 result(0);
	
	GetSystemTimeAsFileTime(reinterpret_cast<FILETIME *>(&f_time));
	SFSco::ResetCal(f_time);


	SplashScreen::Create(bres, v_str, sp_buf);
		
	if (SplashScreen::GetInstance()) {

		if (_oblock = CreateEvent(0, TRUE, TRUE, 0)) {
			if (HANDLE inithread = CreateThread(0, 0, IniProc, SplashScreen::GetInstance()->GetHandle(), 0, 0)) {
				
				MSG threadMessage;
			
				for (;;) {
					if (PeekMessage(&threadMessage, 0, 0, 0, PM_REMOVE)) {
						if (threadMessage.message != WM_QUIT) {
							if (threadMessage.message != INVOKE_IT) {
								DispatchMessage(&threadMessage);
							} else {
								reinterpret_cast<UI_64 *>(threadMessage.wParam)[3] = Control_PunchIt(reinterpret_cast<void *>(threadMessage.wParam));
								if (threadMessage.lParam) SetEvent((HANDLE)threadMessage.lParam);
									
							}
						} else {
							result = threadMessage.wParam;
							break;
						}

					} else {
						WaitMessage();
					}
				}
		
				WaitForSingleObject(inithread, INFINITE);
				GetExitCodeThread(inithread, (LPDWORD)&result);

				CloseHandle(inithread);
			}
			CloseHandle(_oblock);
		}
	}	
		
	if (!result) {
		result = f_time;

		GetSystemTimeAsFileTime(reinterpret_cast<FILETIME *>(&f_time));
		SFSco::TimerCal(f_time);		

		result = f_time - result;
		result /= 1000;
	} else {
		if ((result >> 40) == 0) {
			result |= 0x444E550000000000;

		}

	}


	return result;
}







