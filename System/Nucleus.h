/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "CoreLibrary\SFSCore.h"


namespace Control {

class Nucleus: public SFSco::Object {
		
	public:
		static const size_t type_id = 3;

		struct Header {			
			UI_64 _state;
			UI_64 _r_d[2];
			UI_64 gate_idx;			
		};


		Nucleus(const Nucleus & );
		Nucleus();
		
		virtual ~Nucleus();
		
		UI_64 Create(SFSco::IAllocator *);
		virtual void Destroy();
		
		void Wait() const;
		void Wait0() const;
		UI_64 Test(UI_64 state_mask) const; // 0 - success, non-zero - fail
		UI_64 Test() const; // 0 - success, non-zero - fail

		void ResetState(UI_64 = 0);

		void SetState(UI_64 state_mask);
		UI_64 GetState();

		void SetWaitMask(UI_64 wait_mask);
		void AddWaitMask(UI_64 wait_mask);

		void ClearState(UI_64 state_mask);		

};



}