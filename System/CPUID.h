/*
** safe-fail base elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once
// ==================================================================================================================================
// enum bool: int {Enabled = -1,Disabled = 0,Undefined};

#pragma pack(push,1)

// AMD structures ===============================================================================================================================================
namespace AMD {
	struct stFeatureInfo {
		unsigned int stepping: 4;
		unsigned int baseModel: 4;

		unsigned int baseFamily: 4;
		unsigned int : 4;

		unsigned int extModel: 4;
		unsigned int extFamily: 8;
		unsigned int : 4;


		unsigned int brandID: 8;
		unsigned int CLFLUSH_size: 8;
		unsigned int logicalProcessorCount: 8;
		unsigned int localApicID: 8;

	}; // 8

	union stFeatureList {
		struct {
		bool SSE3: 1;
		bool PCLMULQDQ: 1;
		bool : 1;
		bool MONITOR: 1;

		bool : 1;					// 4
		bool : 1;
		bool : 1;
		bool : 1;
		
		bool : 1;					// 8
		bool SSSE3: 1;
		bool : 1;
		bool : 1;
		
		bool FMA: 1;				// 12
		bool CMPXCHG16B: 1;
		bool : 1;
		bool : 1;
	
		bool : 1;					// 16
		bool : 1;
		bool : 1;
		bool SSE4_1: 1;
	
		bool SSE4_2: 1;				// 20
		bool : 1;
		bool : 1;
		bool POPCNT: 1;
	
		bool : 1;					// 24
		bool AES: 1;
		bool XSAVE: 1;
		bool OSXSAVE: 1;
	
		bool AVX: 1;				// 28
		bool F16C: 1;
		bool : 1;
		bool : 1;

	
		
		bool FPU: 1; // fpu on-chip
		bool VME: 1; // virtual mode enchancements
		bool DE: 1; // debugging extensions
		bool PSE: 1; // page-size extensions
	
		bool TSC: 1; // time stamp counter
		bool MSR: 1; // model specific registers
		bool PAE: 1; // physical-address extensions
		bool MCE: 1; // machine check exception

		bool CMPXCHG8B: 1; // instruction
		bool APIC: 1; // advanced programmable interrupt controller
		bool : 1;
		bool sysEnterExit: 1; // syenter, sysexit instructions

		bool MTRR: 1; // memory-type range registers
		bool PGE: 1; // page clobal execution
		bool MCA: 1; //machine check architecture
		bool CMOV: 1; // instruction

		bool PAT: 1; // page attribute table
		bool PSE36: 1; // page translations
		bool : 1;
		bool CLFSH: 1; // instruction

		bool : 1;
		bool : 1;
		bool : 1;
		bool MMX: 1; // instruction set

		bool FXSR: 1; // fxsave, fxstore
		bool SSE: 1; // instruction set
		bool SSE2: 1; // instruction set
		bool : 1;

		bool HTT: 1; // hyper-threading technology
		bool : 1;
		bool : 1;
		bool : 1;
		};

		struct {
			int _ecx, _edx;
		};
//		int _edx;

	}; // 8

	struct stMonitorWaitIDs {
		unsigned int minLineSize: 16; // smallest monitor-line size in bytes
		int : 16;
		unsigned int maxLineSize: 16; // largest monitor-line size in bytes
		int : 16;
		
		int EMX: 1;
		int IBE: 1;
		
		int : 30;
		
		int : 32;


	}; // 16

	struct stPowerManagementFeatures {
		bool effectiveFrequencyInterface: 1;
		bool : 7;

		bool : 8;
		bool : 8;
		bool : 8;

	}; // 4

	struct stXFeatureIDs {
		bool : 2;
		bool BMI: 1;
		bool : 5;

		bool : 8;
		bool : 8;
		bool : 8;

	}; // 4

	struct stXStateEnumeration {
		__int64 suppotedFeatureMask;
		unsigned int maxEnabledSize, maxSupportedSize;


	}; // 16

	struct stXFeatureInfo {
		char brandID[3];
		char : 4;
		unsigned char packageType: 4;
	}; // 4

	struct stXFeatureList {
		bool LahfSahf: 1; // 64 instructions
		bool CmpLegacy: 1; // core multiprocessing legacy mode
		bool SVM: 1; // secure virtual machine
		bool ExtApicSpace: 1; // extended apic space

		bool AltMovCr8: 1; // mov cr8
		bool ABM: 1; // advance bit manipulation
		bool SSE4A: 1; // instruction subset
		bool MisAlignSse: 1; // misalignd sse mode

		bool IIIDNowPrefetch: 1; // instructions
		bool OSVW: 1; // os visible workaround
		bool IBS: 1; // istruction base sampling
		bool SSE5: 1; // extended operation support

		bool SKINIT: 1;
		bool WDT: 1; // watchdog timer support
		bool : 1;
		bool LWP: 1; // lightweight profiling support

		bool FMA4: 1; // 4-operand FMA instruction support
		bool : 1;
		bool : 1;
		bool NodeID: 1; // MSRC001_100C

		bool : 1;
		bool TBM: 1; // trailing bit manipulation
		bool topologyEx: 1; // topology extensions support
		bool : 1; 

		bool : 1;
		bool : 1;
		bool : 1;
		bool : 1;
	
		bool : 1;
		bool : 1;
		bool : 1;
		bool : 1;

		bool FPU: 1; // fpu on-chip
		bool VME: 1; // virtual mode enchancements
		bool DE: 1; // debugging extensions
		bool PSE: 1; // page-size extensions

		bool TSC: 1; // time stamp counter
		bool MSR: 1; // model specific registers
		bool PAE: 1; // physical-address extensions
		bool MCE: 1; // machine check exception

		bool CMPXCHG8B: 1; // instruction
		bool APIC: 1; // advanced programmable interrupt controller
		bool : 1;
		bool sysCallRet: 1; // syscall, sysret instructions

		bool MTRR: 1; // memory-type range registers
		bool PGE: 1; // page clobal execution
		bool MCA: 1; //machine check architecture
		bool CMOV: 1; // instruction

		bool PAT: 1; // page attribute table
		bool PSE36: 1; // page translations
		bool : 1;
		bool : 1;

		bool NX: 1; // no-execute page protection
		bool : 1;
		bool MmxExt: 1; // extensions to mmx
		bool MMX: 1; // instruction set

		bool FXSR: 1; // fxsave, fxstore
		bool FFXSR: 1; // fxsave, fxstore optimization
		bool Page1Gb: 1; // 1Gb page support
		bool RDTSCP: 1; // instruction

		bool : 1; 
		bool LM: 1; // long mode
		bool IIIDNowExt: 1; // instruction set
		bool IIIDNow: 1; // instruction set

	}; // 8

	struct stL1CacheInfo {
		unsigned int iTLB2_4MEntrieSize: 8; // instruction TLB number of entries for 2 MB and 4 MB pages
		unsigned int iTLB2_4MAssociativity: 8; // instruction TLB associativity for 2 MB and 4 MB pages
		unsigned int dTLB2_4MEntrieSize: 8; // data TLB number of entries for 2 MB and 4 MB pages
		unsigned int dTLB2_4MAssociativity: 8; // data TLB associativity for 2 MB and 4 MB pages

		unsigned int iTLB4KEntrieSize: 8; // instruction TLB number of entries for 4 Kb pages
		unsigned int iTLB4KAssociativity: 8; // instruction TLB associativity for 4 Kb pages
		unsigned int dTLB4KMEntrieSize: 8; // data TLB number of entries for 4 Kb pages
		unsigned int dTLB4KMAssociativity: 8; // data TLB associativity for 4 Kb pages

		unsigned int dLineSize: 8; // L1 data cache line size in bytes
		unsigned int dLinesPerTag: 8; // L1 data cache lines per tag
		unsigned int dAssociativity: 8; // L1 data cache associativity
		unsigned int dSize: 8; // L1 data cache size in KB

		unsigned int iLineSize: 8; // L1 instruction cache line size in bytes
		unsigned int iLinesPerTag: 8; // L1 instruction cache lines per tag
		unsigned int iAssociativity: 8; // L1 instruction cache associativity
		unsigned int iSize: 8; // L1 instruction cache size in KB

	}; // 16

	enum enL2L3Associativity: unsigned int {disabled = 0,direct,IIway,IVway = 4,VIIIway = 6,XVIway = 8,XXXIIway = 10,XLVIIIway,LXIVway,XCVIway,CXXVIIIway,full};
	
	struct stL2CacheInfo {
		unsigned int iTLB2_4MEntrieSize: 12; // instruction TLB number of entries for 2 MB and 4 MB pages
		enL2L3Associativity iTLB2_4MAssociativity: 4; // instruction TLB associativity for 2 MB and 4 MB pages
		unsigned int dTLB2_4MEntrieSize: 12; // data TLB number of entries for 2 MB and 4 MB pages
		enL2L3Associativity dTLB2_4MAssociativity: 4; // data TLB associativity for 2 MB and 4 MB pages

		unsigned int iTLB4KEntrieSize: 12; // instruction TLB number of entries for 4 Kb pages
		enL2L3Associativity iTLB4KAssociativity: 4; // instruction TLB associativity for 4 Kb pages
		unsigned int dTLB4KMEntrieSize: 12; // data TLB number of entries for 4 Kb pages
		enL2L3Associativity dTLB4KMAssociativity: 4; // data TLB associativity for 4 Kb pages

		unsigned int lineSize: 8; // L2 cache line size in bytes
		unsigned int linesPerTag: 4; // L2 cache lines per tag
		enL2L3Associativity associativity: 4; // L2 cache associativity
		unsigned int size: 16; // L2 cache size in KB

	}; // 12

	struct stL3CacheInfo {
		unsigned int lineSize: 8; // L3 cache line size in bytes
		unsigned int linesPerTag: 4; // L3 cache lines per tag
		enL2L3Associativity associativity: 4; // L3 cache associativity
		unsigned int : 2;
		unsigned int size: 14; // L3 cache size in KB
		

	}; // 4


	struct stAdvancedPowerManagement {
		bool tSensor: 1; // Temperature sensor
		bool FID: 1; // Frequency ID control
		bool VID: 1; // Voltage ID control
		bool TTP: 1; // THERMTRIP

		bool TM: 1; // hardware thermal control
		bool : 1; // 
		bool mulControl: 1; // 100 MHz multiplier Control
		bool hwPState: 1; // hardware P-state control

		bool tscInvatiant: 1; // TSC invariant
		bool CPB: 1; // core performance boost
		bool effFreqRO: 1; // read-only effective frequency interface
		bool : 1;

		bool : 1;
		bool : 1;
		bool : 1;
		bool : 1;

		bool : 8;
		bool : 8;


	}; // 4

	struct stLongModeAddressSizeIDs {
		unsigned int physAddrSize: 8; // Maximum physical byte address size in bits
		unsigned int linAddrSize: 8; // Maximum linear byte address size in bits
		unsigned int guestPhysAddrSize: 8; // maximum guest physical byte address size in bits
		unsigned int : 8;
	}; // 4

	struct stCoreIDs {
		unsigned int coreCount: 8; // number of physical cores - 1
		unsigned int : 4;
		unsigned int apicIdSize: 4; // APIC ID size
		unsigned int : 16;

	}; // 4

	struct stSVMInfo {
		unsigned int NASID;

		unsigned int revision: 8;
		unsigned int : 24;
		
		bool nestedPaging: 1; // nested paging
		bool lbrVirt: 1; // LBR virtualization
		bool lock: 1; // SVM lock
		bool NRIP: 1; // NRIP save

		bool tscRateMsr: 1; // MSR based TSC rate control
		bool vmcbClean: 1; // VMCB clean bits
		bool flushByASID: 1; // flush by ASID
		bool decodeAssists: 1; // decode assists

		bool : 1;
		bool : 1;
		bool pauseFilter: 1;
		bool : 1;

		bool pauseFilterThreshold: 1;
		bool : 1;
		bool : 1;
		bool : 1;

		bool : 8;
		bool : 8;

	}; // 12

	struct st1GbTLBIDs {
		unsigned int iL1EntriesCount: 12;
		enL2L3Associativity iL1Associativity: 4;
		unsigned int dL1EntriesCount: 12;
		enL2L3Associativity dL1Associativity: 4;

		unsigned int iL2EntriesCount: 12;
		enL2L3Associativity iL2Associativity: 4;
		unsigned int dL2EntriesCount: 12;
		enL2L3Associativity dL2Associativity: 4;

	}; // 8

	struct stPerformanceOptimizationIDs {
		bool FP128: 1;
		bool MOVU: 1;
		bool : 6;

		bool : 8;
		bool : 8;
		bool : 8;

	}; // 4

	struct stInstructionBasedSamplingIDs {
		bool IBSFFV: 1; // IBS feature flags valid
		bool fetchSam: 1; // IBS fetch sampling supported
		bool opSam: 1; // IBS execution sampling supported
		bool rdwrOpCnt: 1; // read write of op counter supported

		bool opCnt: 1; // op counting mode supported
		bool brnTrgt: 1; // branch target address reporting supported
		bool opCntExt: 1; // IbsOpCurCnt and IbsOpMaxCnt extend by 7 bits
		bool ripInvalidChk: 1; // invalid RIP indication supported

		bool : 8;
		bool : 8;
		bool : 8;
		
	}; // 4

	struct stProfilingCapabilities {
		bool available: 1; // LWP available
		bool VAL: 1; // LWPVAL instruction available
		bool IRE: 1; // instructions retired event available
		bool BRE: 1; // branch retired event available

		bool DME: 1; // DC miss event available
		bool CNH: 1; // core clocks not halted event available
		bool RNH: 1; // core reference clocks not halted event available
		bool : 1;

		bool : 8;
		bool : 8;
		bool : 7;
		bool interrupt: 1; // interrupt on threshold overflow available

		unsigned int cbSize: 8; // control block size
		unsigned int eventSize: 8; // event record size
		unsigned int maxEvents: 8; // maximum EventId
		unsigned int eventOffset: 8; // offset to the EventInterval1 field

		unsigned int latencyMax: 5; // latency counter bit size
		unsigned int dataAddress: 1; // data cache miss address valid
		unsigned int latencyRnd: 3; // amount cache latency is rounded

		unsigned int version: 7;

		unsigned int minBufferSize: 8; // event ring buffer size
		
		unsigned int : 4;
		
		unsigned int branchPrediction: 1; // branch prediction filtering supported
		unsigned int ipFiltering: 1; // IP filtering supported
		unsigned int cacheLevels: 1; // cache level filtering supported
		unsigned int cacheLatency: 1; // cache latency filtering supported


	}; // 12

	enum enCacheType : unsigned int {Null = 0, Data, Instruction, Unified};

	struct stCacheProperties {
		enCacheType type: 5; // cache type
		unsigned int level: 3; // cache level

		unsigned int selfInit: 1; // cache is self-initializing
		unsigned int fullyAssoc: 1; // fully associative cache
		unsigned int : 4;

		unsigned int sharingCacheNum: 12; // number of cores sharing cache
		unsigned int : 6;

		unsigned int lineSize: 12; // cache number of ways
		unsigned int physPartitions: 10; // cache physical line partitions
		unsigned int wayNum: 10; // cache number of ways

		unsigned int setNum; // cache number of sets

		bool WBINVD: 1; // Write-Back Invalidate/Invalidate
		bool cacheInclusive: 1; // cache inclusive

		bool : 6;
		bool : 8;
		bool : 8;
		bool : 8;

	}; // 16

	struct stExtendedAPIC {
		unsigned int ID;
		
		unsigned int computeUnitID: 8;
		unsigned int coresPerCUnit: 2;
		unsigned int : 22;

		unsigned int nodeID: 8;
		unsigned int nodesPerProc: 3;
		unsigned int : 21;


	}; // 12
}
// Intel structures ===========================================================================================================================================
namespace Intel {
	struct stFeatureInfo {
		unsigned int stepping: 4;
		unsigned int modelNumber: 4;
		unsigned int familyCode: 4;
		unsigned int procType: 2;
		unsigned int : 2;
		unsigned int exModel: 4;
		unsigned int exFamily: 8;
		unsigned int : 4;

		unsigned int bandIndex: 8;
		unsigned int flushSize: 8;
		unsigned int maxCOREcount: 8;
		unsigned int apicID: 8;
	}; // 8

	union stFeatureList {
		struct {
		bool SSE3: 1; // sse3 supported
		bool PCLMULDQ: 1;
		bool DTES64: 1; // 64-bit debug store
		bool MONITOR: 1; // monitor and wait instructions supported
	
		bool DS_CPL: 1; // cpl qualified debug store
		bool VMX: 1; // virtual machine extensions
		bool SMX: 1; // safer mode extensions
		bool EIST: 1; // enchances intel speedstep technology

		bool TM2: 1; // thermal monitor 2
		bool SSSE3: 1; // supplemental streaming SIMD extensions 3
		bool CNXT_ID: 1; // L1 context ID
		bool : 1;
	
		bool FMA: 1; // fused multiply add
		bool CX16: 1; // CMPXCHG16B instruction is supported
		bool xTPR: 1; // xTPR update control
		bool PDCM: 1; // perfmon and debug capability

		bool : 1;
		bool PCID: 1; // process contes identifiers
		bool DCA: 1; // direct cache address
		bool SSE4_1: 1; // SSE4.1 supported

		bool SSE4_2: 1; // sse4.2 supported
		bool x2APIC: 1; // extended xAPIC support
		bool MOVBE: 1; // movbe supported
		bool POPCNT: 1; // POPCNT supported

		bool TSC_deadline: 1; // time stamp counter deadline
		bool AES: 1; // 
		bool XSAVE: 1; // XSAVE supported
		bool OSXSAVE: 1; // os-enabled extended state management

		bool AVX: 1;
		bool F16C: 1;
		bool RDRAND: 1;
		bool : 1;

		bool FPU: 1; // floating-point on-chip
		bool VME: 1; // virtual mode extension
		bool DE: 1; // debugging extendion
		bool PSE: 1; // page size extension

		bool TSC: 1; // time stamp counter
		bool MSR: 1; // model specific registers
		bool PAE: 1; // physical address axtension
		bool MCE: 1; // machine-check exception

		bool CX8: 1; // compare and exchange 8-bytes instruction
		bool APIC: 1; // on-chip APIC
		bool : 1;
		bool SEP: 1; // fast system call

		bool MTRR: 1; // memory type range registers
		bool PGE: 1; // page global enable
		bool MCA: 1; // machine check architecture
		bool CMOV: 1; // conditional mov instruction

		bool PAT: 1; // page attribute table
		bool PSE36: 1; // 36-bit page size extension
		bool PSN: 1; // processor serial number present
		bool CLFSH: 1; // CLFLUSH instruction supported

		bool : 1;
		bool DS: 1; // debug store
		bool ACPI: 1; // thermal monitor and software controlled clock facilities
		bool MMX: 1; // mmx instruction set supported

		bool FXSR: 1; // FXSAVE and FXRSTOR instruction are supported
		bool SSE: 1; // sse instruction set supported
		bool SSE2: 1; // sse2 supported
		bool SS: 1; // self-snoop

		bool HTT: 1; // multi-threading
		bool TM: 1; // thermal monitor
		bool : 1; //
		bool PBE: 1; // pending break enable
		};

		struct {
			int _ecx, _edx;
		};

	}; // 8

	enum enCacheType : unsigned int {Null = 0, Data, Instruction, Unified};

	struct stCacheParameters {		
		enCacheType type: 5;
		unsigned int level: 3;

		unsigned int selfInit: 1;
		unsigned int fullyAssoc: 1;
		unsigned int : 1;
		unsigned int : 1;

		unsigned int : 1;
		unsigned int : 1;

		unsigned int maxSharingThredNum: 12;
		unsigned int maxCOREcount: 6;

		unsigned int coherencyLineSize: 12;
		unsigned int physLinePartitions: 10;
		unsigned int waysOfAssociativity: 10;

		unsigned int numOfSets;
		
		bool WBINDW: 1;
		bool inclusiveToLowerLevels: 1;
		bool complexIndexing: 1;
		bool : 1;

		bool : 4;
		bool : 8;
		bool : 8;
		bool : 8;


	}; // 16

	struct stMonitorWaitParams {
		unsigned int minLineSize: 16;
		int : 16;
		unsigned int maxLineSize: 16;
		int : 16;
		
		bool instEnabled: 1;
		bool breakEvents: 1;

		bool : 6;
		bool : 8;
		bool : 8;
		bool : 8;

		unsigned int C0subStateCount: 4;
		unsigned int C1subStateCount: 4;
		unsigned int C2subStateCount: 4;
		unsigned int C3subStateCount: 4;
		unsigned int C4subStateCount: 4;

		int : 12;


	}; // 16

	struct stPowerManagementParams {
		bool DTS: 1; // digital thermal sensor
		bool turboBoost: 1;
		bool ARAT: 1; // always running APIC timer
		bool : 1;

		bool PLN: 1; // power limit notification
		bool ECMD: 1; // extended clock modulation duty
		bool PTM: 1; // package thermal management
		bool : 1;

		bool : 8;
		bool : 8;
		bool : 8;

		unsigned int NIT: 4; // number of interrupt thresholds

		unsigned int : 4;
		unsigned int : 8;
		unsigned int : 16;

		bool HCF: 1; // hardware coordination feedback
		bool ACNT2: 1;
		bool : 1;
		bool PEB: 1; // performance-energy bias capability
		
		bool : 4;
		bool : 8;
		bool : 8;
		bool : 8;

	}; // 12

	struct stPerformanceMonFeatures {
		unsigned int version: 8;
		unsigned int coutersNum: 8; // number of counters per logical proc
		unsigned int counterWidth: 8; // number of bits per programmable counter
		unsigned int eventCount: 8; // number of arch events supported per logical proc

		bool coreCycles: 1; // 0 - supported
		bool iRetired: 1;
		bool refCycles: 1;
		bool lastLevCacheRef: 1;

		bool lastLevelCacheMisses: 1;
		bool branchIRetired: 1;
		bool branchMispredictRetired: 1;
		bool : 1;

		bool : 8;
		bool : 8;
		bool : 8;

		unsigned int fixedCounterNum: 4;
		unsigned int fixedCounterWidth: 8;

		int : 20;

	}; // 12

	enum enTopologyLevelType : unsigned int {Invalid=0,Thread,Core};
	struct stProcessorTopology {
		unsigned int apicShiftBits: 4;
		int : 28;

		unsigned int levelLogicalProcCount: 16;
		int : 16;

		unsigned int levelNum: 8;
		enTopologyLevelType levelType: 8;
		int : 16;

		int apicXID;

	}; // 16

	struct stXSAVEFeatures {
		unsigned int validBits;
		unsigned int maxSizeRequired;
		unsigned int maxSizeTotal;

	}; // 12

	struct stXFeatureList {
		bool : 8;
		
		bool : 3;
		bool syscall: 1;
		bool : 4;

		bool : 4;
		bool XD: 1;
		bool : 3;

		bool : 2;
		bool pages1GB: 1;
		bool RTDSCP: 1;
		bool : 1;
		bool I64: 1;
		bool : 1;
		bool : 1;

		bool LAHF: 1;
		bool : 7;
		bool : 8;
		bool : 8;
		bool : 8;

	}; // 8

	enum enCacheAssociativity: unsigned int {Disabled = 0,Enabled,IIway,IVway = 4,VIIIway = 6,XVIway = 8,Full = 15};
	struct stExtendedL2Features {
		unsigned int lineSize: 8;
		unsigned int : 4;
		enCacheAssociativity associativity: 4;
		unsigned int size: 16; // in KB

	};	// 4

	struct stAdvancedPowerManagement {
		bool : 8;
		bool TSC: 1;
		bool : 7;

		bool : 8;
		bool : 8;

	}; // 4

	struct stVPAddressSizes {
		unsigned int physicalSize: 8;
		unsigned int virtualSize: 8;
		unsigned int : 16;
	}; // 4
}
#pragma pack(pop)

class clAMDID sealed {
	friend class IDCPU;
	private:
		AMD::stFeatureInfo _procInfo;							// 8
		AMD::stFeatureList _features;							// 8
		AMD::stMonitorWaitIDs _monitorFeatures;					// 16
		AMD::stPowerManagementFeatures _powerFeatures;			// 4
		AMD::stXFeatureIDs _extendedFeatures;					// 4
		AMD::stXStateEnumeration _extendedFeatureState;			// 16

		AMD::stXFeatureInfo _xFeatureInfo;						// 4
		AMD::stXFeatureList _xFeatures;							// 8

		char _processorName[64];								// 64

		AMD::stL1CacheInfo _L1CacheInfo;						// 16
		AMD::stL2CacheInfo _L2CacheInfo;						// 12
		AMD::stL3CacheInfo _L3CacheInfo;						// 4

		AMD::stAdvancedPowerManagement _powerManagement;		// 4
		AMD::stLongModeAddressSizeIDs _longAddressIDs;			// 4
		AMD::stCoreIDs _coreInfo;								// 4
		AMD::stSVMInfo _svmInfo;								// 12
		AMD::st1GbTLBIDs _tlb1GBInfo;							// 8
		AMD::stPerformanceOptimizationIDs _performanceOptIDs;	// 4
		AMD::stInstructionBasedSamplingIDs _iSamplingInfo;		// 4
		AMD::stProfilingCapabilities _profilingCapabilities;	// 12

		AMD::stCacheProperties _cacheProperties[16];			// 16 (256)
		AMD::stExtendedAPIC _exAPICInfo;						// 12

				
		clAMDID(unsigned int,unsigned int);
	
		void * operator new(size_t);

};

class clIntelID sealed {
	friend class IDCPU;
	private:
		Intel::stFeatureInfo _procInfo;							// 8
		Intel::stFeatureList _features;							// 8
		Intel::stCacheParameters _cacheParams[16];				// 16 (256)
		Intel::stMonitorWaitParams _monitorParams;				// 16
		Intel::stPowerManagementParams _powerParams;			// 12
		unsigned int _dcaParams;								// 4
		Intel::stPerformanceMonFeatures _performanceMonitor;	// 12
		Intel::stProcessorTopology _procTopology[16];			// 16 (256)
		Intel::stXSAVEFeatures _xsave;							// 12

		Intel::stXFeatureList _xFeatures;						// 8
		char _processorName[64];								// 64
		Intel::stExtendedL2Features _L2xFeatures;				// 4
		Intel::stAdvancedPowerManagement _powerManagement;		// 4
		Intel::stVPAddressSizes _addressInfo;					// 4


		clIntelID(unsigned int,unsigned int);

		void * operator new(size_t);

};

struct stCacheDesc {
	unsigned int wayCount;
	unsigned int curvature;
	unsigned int rouphness;
	unsigned int capacity;
};

struct stCPUNumbers {
	unsigned int physicalCount, logicalCount;

	stCacheDesc L1, L2;
};

class IDCPU sealed {
	private:
		static IDCPU * _instance;
		unsigned int _funcMAX, _eFuncMAX;
		char _vendorString[16];

		clAMDID * _amdID;
		clIntelID * _intelID;
		stCPUNumbers _cpuVals;

	public:

		static int Initialize();
		static void Finalize();

		static bool tossFPU();
		static bool tossSSE3();
		static bool tossSSSE3();
		static bool tossSSE41();
		static bool tossSSE42();
		static bool tossAES();
		static bool tossAVX();
		static bool toosX64();
		static bool tossCLF();
		static bool tossRand();

		static unsigned int GetPhysicalCount();
		static const stCacheDesc & GetL1Config();
		static const stCacheDesc & GetL2Config();

		static size_t GetCoreMask(unsigned int coreselector); // input - preferred core selektor
};

extern "C" void SetLowPrecisionFloat(unsigned int roundC); // 0 - nearest, 1 - lowest, 2 - highest, 3 - truncate
extern "C" void SetStdPrecisionFloat(unsigned int roundC);

extern "C" void Intel_ID(void *, unsigned int, unsigned int);
extern "C" void AMD_ID(void *, unsigned int, unsigned int);
extern "C" void CpuFuncCount(void *);
extern "C" void RestoreFState();

