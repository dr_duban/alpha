/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "Math\Matrix.h"
#include "System\Nucleus.h"



#define HANCHOR_LEFT	0x01
#define HANCHOR_CENTER	0x02
#define VANCHOR_TOP		0x04
#define VANCHOR_CENTER	0x08

#define DANCHOR_FRONT	0x10
#define DANCHOR_CENTER	0x20


#define SCALE_H			0x0100
#define SCALE_V			0x0200
#define	SCALE_A			0x0400
#define SCALE_HV		0x0800


namespace OpenGL {
/*
	sugestion to item to hold state and provide blank action hadnler

	in general you can draw points, lines, and triangles


*/
	

	namespace Colors {
		const float white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		const float transparent[4] = {0.0, 0.0, 0.0, 0.0};

	};

	enum FitType: unsigned int {	AsIs = 0,
									FitWidth = SCALE_H,
									FitHeight = SCALE_V,
									FitBox = SCALE_H | SCALE_V,
									FitBest = SCALE_H | SCALE_V | SCALE_A
								};

	enum AnchorType : unsigned int {	None = 0x00000080,
										Special = 0x00000040,

										LeftBottomFront		= HANCHOR_LEFT	| DANCHOR_FRONT,
										LeftTopFront		= HANCHOR_LEFT	| VANCHOR_TOP	| DANCHOR_FRONT,
										RightTopFront		= VANCHOR_TOP	| DANCHOR_FRONT,
										RightBottomFront	= DANCHOR_FRONT,
										LeftBottomBack		= HANCHOR_LEFT,
										LeftTopBack			= HANCHOR_LEFT	| VANCHOR_TOP,
										RightTopBack		= VANCHOR_TOP,
										RightBottomBack		= 0,
										LeftBottomCenter	= HANCHOR_LEFT	| DANCHOR_CENTER,
										LeftTopCenter		= HANCHOR_LEFT	| VANCHOR_TOP	| DANCHOR_CENTER,
										RightTopCenter		= VANCHOR_TOP	| DANCHOR_CENTER,
										RightBottomCenter	= DANCHOR_CENTER,
										Center				= HANCHOR_CENTER| VANCHOR_CENTER| DANCHOR_CENTER,
										CenterFront			= HANCHOR_CENTER| VANCHOR_CENTER| DANCHOR_FRONT,
										CenterBack			= HANCHOR_CENTER| VANCHOR_CENTER,
										LeftCenter			= HANCHOR_LEFT	| VANCHOR_CENTER| DANCHOR_CENTER,
										RightCenter			= VANCHOR_CENTER| DANCHOR_CENTER,
										CenterTop			= HANCHOR_CENTER| VANCHOR_TOP	| DANCHOR_CENTER,
										CenterBottom		= HANCHOR_CENTER| DANCHOR_CENTER,
										CenterTopFront		= HANCHOR_CENTER| VANCHOR_TOP	| DANCHOR_FRONT,
										CenterBottomFront	= HANCHOR_CENTER| DANCHOR_FRONT,
										RightCenterFront	= VANCHOR_CENTER| DANCHOR_FRONT,
										LeftCenterFront		= HANCHOR_LEFT	| VANCHOR_CENTER| DANCHOR_FRONT,
										CenterTopBack		= HANCHOR_CENTER| VANCHOR_TOP,
										CenterBottomBack	= HANCHOR_CENTER,
										RightCenterBack		= VANCHOR_CENTER,
										LeftCenterBack		= HANCHOR_LEFT	| VANCHOR_CENTER
								};



	class AttributeArray: public Calc::MatrixF {
		friend class Element;
		friend class Core;
		private:
			static Memory::Allocator * _attributes_pool;
			static void ClearArray(void *, unsigned int, const float * = 0);
			static void GetRange(const void *, unsigned nit, float *);
			static void SetRange(void *, float, float, unsigned int);
			static void GonCalc(float *, unsigned int, float, float);

			static UI_64 DoAnchor(float *, float *, unsigned int);

			void GetLocalTransform(float *, bool = false);			
		public:
			struct Header : public Calc::MatrixF::Header {
				float min[4], max[4], center[4], box[4]; //  (box_vals)			
				
				float stretch_vector[4];
				int stretch_mask[4];
				float offset_mask[4];
				int reserved[4];				

				float scale_vector[4];
				float translate_vector[4];
				float xy_spin[5];
				
				unsigned int motion_options;
				void (__fastcall * motion_proc)(float *, unsigned int);
				
				float f_reserved[8];

				float motion_transform[16];
				float global_transform[16];
				float local_transform[16];								
				
			};
	 
			enum Type : unsigned int {Vertex = 0, Normal, Color, TexCoord, AttTypeCount, Undefined = 0xFFFFFFFF};	
			
			UI_64 Create(unsigned int aCount, const float * buffer = 0, unsigned int = 0);
			UI_64 Create(const AttributeArray &);
			virtual void Destroy();

			UI_64 AddAttributes(unsigned int, const float * = 0);

			static const UI_64 type_id = 7853*MatrixF::type_id;
			
			static int Initialize();
			static int Finalize();

			AttributeArray();
			AttributeArray(const AttributeArray &);
						
			virtual ~AttributeArray();
			virtual UI_64 IsIdentity();

			MatrixF & operator >> (MatrixF &);

			AttributeArray & Scale(const float *);
			AttributeArray & Size(const float *);
			AttributeArray & SetScale(const float *);
			AttributeArray & SetZShift(float, float);
			AttributeArray & Translate(const float *);
			AttributeArray & SetOffset(const float *);
			AttributeArray & SetOffsetXY(const float *);
			AttributeArray & SetOffsetZ(float);

			AttributeArray & RotateXY(float);

			void Polygon(unsigned int);

			void SetStretchMask(int, int, int);
			void SetOffsetMask(float, float, float);
			void SetOffsetMask(AnchorType);
			void SetStretchVector(float, float, float);
			void UpdateStretchVector(float, float, float);
			void CopyStretchVector(AttributeArray &);
			void SwapOffset(AttributeArray &);
			void ResetStretch();
			void StretchAdjust();

			void GetVolume(float *); // returns 12 floats
			UI_64 AnchorTransform(const float * origin, const float * deformation);
			UI_64 DoAnchor();
		
			void CalculateBox(AnchorType);
			void SetBox(float, float);
			void SetBox(AttributeArray &);
			void GetBox(float *);
			
			void LocalDAdjust(float *, unsigned int);
			void LocalUAdjust(float *, unsigned int);
			void GlobalDAdjust(float *, unsigned int);
			void GlobalUAdjust(float *, unsigned int);


			void Copy(AttributeArray &);

			void GetGlobalTransform(float *);
			void MatrixUpdate(float * om, const float *);
			int TransformAdjust(float *);
			void GlobalAdjust(float *);

			void Clear(const float *);

	};

	

	class Element: public Control::Nucleus {
		public:

			enum DrawType : unsigned int {Point = 0, Triangle = 0x00010000, Quad = 0x00020000, Segment = 0x00030000, Loop = 0x00040000, Line = 0x00050000, ClipRegion = 0x00060000, SceneRegion = 0x00070000, TQuad = 0x00080000, Symbol = 0x00090000, Border = 0x000A0000, DrawTypeCount = 0x000B, ClipSpecialFlag = 0x80000000};
						
			enum FlagSet : UI_64 {
											


											
											StateNotify		= 0x0000000000000002,
											StateHide		= 0x0000000000000004,
											StateShow		= 0x0000000000000008,


											StatePageRight	= 0x0000000000000100,
											StatePageLeft	= 0x0000000000000200,
											StatePageDown	= 0x0000000000000400,
											StatePageUp		= 0x0000000000000800,
											
											


											FlagNegTex		= 0x0000000000010000,
											FlagMaskTex		= 0x0000000000020000,
											FlagBlured		= 0x0000000000040000,
											FlagIdent		= 0x0000000000080000,


											FlagVisible		= 0x0000000000100000,
											FlagMovable		= 0x0000000000200000,
											FlagAction		= 0x0000000000400000,
											FlagTouch		= 0x0000000000800000,

											FlagAuHide		= 0x0000000001000000,
											FlagBorder		= 0x0000000002000000,
											FlagVisible2	= 0x0000000004000000,
											FlagTouch2		= 0x0000000008000000,

											FlagX0Drag		= 0x0000000010000000,
											FlagX1Drag		= 0x0000000020000000,
											FlagY0Drag		= 0x0000000040000000,
											FlagY1Drag		= 0x0000000080000000,
											FlagXYDrag		= 0x0000000100000000,
											FlagXSpan		= 0x0000000200000000,
											FlagYSpan		= 0x0000000400000000,
											FlagScroll		= 0x0000000800000000,
											FlagZoom		= 0x0000001000000000,
											


											FlagTUpdate		= 0x0000010000000000,											
											FlagSized		= 0x0000020000000000,
											FlagApp			= 0x0000040000000000,											
											FlagHideOn		= 0x0000080000000000,
								
											FlagAnchor		= 0x0000100000000000,


											FlagHoover		= 0x0001000000000000,
											FlagLeave		= 0x0002000000000000,
//											FlagMoved		= 0x0004000000000000,
											FlagMouseDown	= 0x0008000000000000,
											FlagMouseUp		= 0x0010000000000000,

											FlagFocus		= 0x0020000000000000,
											FlagLostFocus	= 0x0040000000000000,
								
											FlagDualUp		= 0x0080000000000000,
											FlagBlinkNoMore	= 0x0100000000000000,
											FlagMotionOn	= 0x0200000000000000,


											

											
											FlagBuilt		= 0x8000000000000000

										};

			static SFSco::Object large_map_list;

		private:
			static int Initialize();
			static void Finalize();

			static int _po_x, _po_y;
			static unsigned int large_ml_top, large_ml_runner;
			static UI_64 default_line_map, default_triangle_map;
			
	//	protected:
			struct AttRefs {				
				AttributeArray::Type _type;
				AttributeArray vals; // 
			};
			struct CfgDescriptor {
				UI_64 tex_unit_selector;
				unsigned int _index, ui_reseved;
				UI_64 unit_id;
				

				unsigned int att_flags;
				unsigned int anchor;
			};
			struct DomainSet {
				UI_64 array_id;
				UI_64 offset, size;

			};
					
			static void __fastcall CalcBorder(const float *, unsigned int, float *);
			static UI_64 __fastcall PageTest(float *, const float *);

			void DestroyAll(AttributeArray &);

		public:			
			struct RootHeader {
				int _width, _height, _maxW, _maxH, _fullW, _fullH;
	
			};

			struct SpanCfg {
				float x, y;
				float x_max, y_max;
			};

			struct Header: public Nucleus::Header {				
				UI_64 x_tra, _flags;
				SFSco::IOpenGL * _context;

				CfgDescriptor config;
				
				unsigned int findex;
				
				unsigned int vertex_count, xc_index, _reserved;

				float depth, location;
				DomainSet domain_desc[AttributeArray::AttTypeCount];
				
				UI_64 free_slot_list;

				RootHeader root_cfg;
				SpanCfg span_cfg;

			};

			struct LabelCfg {
				float offset[4];

				float c_color[4], t_color[4];
				float hc_color[4], ht_color[4];
				float dc_color[4], dt_color[4];

				float f_size, f_reserved;

				unsigned int font_id, font_style;
				AnchorType a_type;
				unsigned int max_char_count;
				
				
			};

			static const UI_64 type_id = 6229*Nucleus::type_id;			
			static const UI_64 free_slot_tid = 4022941007;
						
			Element(const Element &);
			Element();
			Element(UI_64);

			virtual ~Element();
			virtual UI_64 XPand();
			virtual UI_64 Touch(float *);

			virtual void Destroy();
			
			UI_64 InitAttributeArray(AttributeArray::Type, AttributeArray &, const float * = 0, unsigned int ccount = 0);
			

			void SetRectTexture(const float *, unsigned int tid, unsigned int level);
			void SetRect(const float *);


			UI_64 CreateEBorder(float offset = 0.0f, unsigned int flags = 0);
			void SetEBorderColor(const float *);


			UI_64 AddLabel(const LabelCfg &, UI_64 = -1);
						
			void SetXColor(unsigned short);

			void SetAnchorMode(AnchorType, AttributeArray::Type = AttributeArray::Vertex);
			void SetFitMode(FitType, AttributeArray::Type = AttributeArray::Vertex);
			void SetDragOp(UI_64);
			void ResetDragOp(UI_64);

			void SetXSpan(float);
			void SetYSpan(float);

			void SetEFlags(UI_64);
			void ClearEFlags(UI_64, UI_64 = 0);
			UI_64 TestEFlags(UI_64);
			unsigned int TestMarker();


			void ConfigurePage(float * wh);

			void SetColor(const float *);
			
			void SetSpecialColor();
			void SetDefaultRect(AttributeArray::Type = AttributeArray::Vertex);

			void SetDefaultBlink(unsigned int delay_0, unsigned int delay_1, unsigned color_0, unsigned int color_1, unsigned int xcolor_01);

			void SetTrajectory(void (__fastcall *)(float *, unsigned int), unsigned int option = 0, AttributeArray::Type = AttributeArray::Vertex);
			void NextLocation();
			void MotionReset(unsigned int);

			void Scale(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void Size(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void Rescale(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void Move(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void Relocate(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void RelocateXY(const float *, AttributeArray::Type = AttributeArray::Vertex);
			void RelocateTex(unsigned int);

			void SelectTLevel(unsigned int);
			unsigned int GetTLevel();

			void RotateXY(float, AttributeArray::Type = AttributeArray::Vertex);

			void SceneCoordsAdjust(float *, unsigned int);
			void GetPtrPos(float *);
			void GetRightMargin(float *); // offset x,y ; w, h ; scale x, scale y
			void GetRightCorner(float *);
			void GetScaleVector(float *);
			void MoveRight(float);

			void LocationSwap(Element &);

			UI_64 SetBlink(unsigned int);
			UI_64 SetTimeOut(unsigned int);

			void Hide();
			void Show();

			void SetXTra(UI_64);
			UI_64 GetXTra();

			Core * GetContext();

			void Push();
			void Pop(unsigned int = 0);
			void SetAttributeBox(AttributeArray::Type, float, float);

			unsigned int IsMovable();

			UI_64 ApplyAnchor(unsigned int, bool = false);

			void ForwardStretchVector(Element &, bool = false);
			void ClearStretch();
			void SetStretch(int, int, int);

			void Drag(int, int);
			
			UI_64 IsAnchor();
			UI_64 IsVisible();

			unsigned int IsRefresh(unsigned int);
			
			UI_64 ConfigureLarge(UI_64);

			
			void HooverON(Element &);
			void LeaveON();
			void DownON();
			void ActionON();

			void ClearDualUp();

			void SetLargeCount(unsigned int);
			void SetFlats(const int *);

			static unsigned int RegisterLargeMap(UI_64);
			static unsigned int UnRegisterLargeMap(UI_64);
			static unsigned int LargeMapRefresh();

	};


	struct DecodeRecord {		

		enum DecodeFlags: unsigned int {
											FlagDestroy =		0x00000001,
											FlagPlayAll =		0x00000002,
											FlagNextTrack =		0x00000004,
											FlagPrevTrack =		0x00000008,
											FlagReset =			0x00000010,
											FlagSourceOn =		0x00000020,
											FlagSourceOff =		0x00000040,
											FlagStart =			0x00000080,
											FlagStop =			0x00000100,
											FlagKill =			0x00000200,
											FlagProgramInfo =	0x00000400,
											FlagJump =			0x00000800,
											FlagMasterVolume =	0x00001000,
											FlagSSNext =		0x00002000,
											FlagCT =			0x00010000,


											FlagSize =			0x00020000,
											FlagTakeAShot =		0x00040000,
											FlagRefresh =		0x00080000,											

											ProgramOptions =	0x0001FFFF,
											SourceOptions =		0x000E0000,

											RecordFlags =		0xFF000000,
											TypeFlags =			0x00F00000,

											FlagPreview =		0x00100000,
											FlagCapture =		0x00200000,
											FlagFile =			0x00400000,											
											FlagProgram =		0x00800000,
											
											FlagNoSize =		0x08000000,
											SizeMinimize =		0x01000000,
											SizeMinimize15 =	0x02000000,
											SizeMaximize =		0x03000000,
											SizeSubSize =		0x04000000,					
											SizeMask =			0x0F000000,

											FlagAnimated =		0x20000000


		
		};

		UI_64 fname_id; // must be destroyed
		UI_64 pic_id;

		UI_64 info_text_id;
		UI_64 name_text_id;
		UI_64 program;

		I_64 start_time, target_time;

		unsigned int flags, precision;
		unsigned int width, height, c_width, c_height;

		float pic_box[4];
		float f_reserved[8];
		
		Element element_obj;
		
		UI_64 _reserved[8];

		DecodeRecord();
				
	};



	class Button: public Element {

		public:
			enum ClipPosition: unsigned int {ClipCorner = 0x80000000, ClipSide = 0x40000000, ClipAnchor = 0xC0000000, ClipRIDMask = 0x0000FFFF};

			static const UI_64 clip_header_tid = 4022940749;
			static const UI_64 clip_tid = 6269*Element::type_id;
			static const UI_64 type_id = 6247*Element::type_id;

			struct ClipHeader {
				float size;
				unsigned int clip_size, slot_runner, slot_top, icon_texture, icon_runner;

				struct ClipSlot {
					unsigned int first_b, b_count;

				} slots[1];


			};

			struct PaletteEntry {
				unsigned int ref_vals[2];
				unsigned short ico_xcol[4]; // normal, hoover, down, 

				float color[4], i_color[4];
				float b_color[4], ib_color[4];

				float h_color[4], hi_color[4];
				float hb_color[4], hib_color[4];

				float d_color[4], di_color[4];
				float db_color[4], dib_color[4];

			};

			struct Header {
				enum CfgFlags : unsigned int {
												XIconPresent		= 0x00080000,
												BorderPresent		= 0x00004000,
												IconPresent			= 0x00100000,
												IconBorderPresent	= 0x00200000,
												IconTextPresent		= 0x00400000,
												AutoSize			= 0x00800000,
												TypeSwitch			= 0x01000000,												
												IconColorSpecial	= 0x08000000,
												
												RemoveFunction		= 0x00010000,
												GroupMask			= 0x0000FFFF
							};

				unsigned int tline_count, flags, icon_tid, icon_flags, palette_size;
				AnchorType icon_ancor;								
				
				UI_64 reserved_64;
				void (__fastcall * icon_trajectory[AttributeArray::AttTypeCount])(float *, unsigned int);
				unsigned int icon_trajectory_option[AttributeArray::AttTypeCount];

				float icon_box[4]; // x, y, w, h

				float border_offset;
				float ib_offset;


			};

			struct SwitchConfig {
				unsigned int selector;
				wchar_t text[2][16];
			};

			struct Config {
				Button::Header header;
				PaletteEntry palette;
				LabelCfg la_cfg[2];
			};


			Button();
			Button(UI_64);
			virtual ~Button();

			unsigned int LoadIcon(unsigned int rid, HMODULE libin);

			void SetIconSpecial(unsigned int);

			unsigned int SetLabelText(unsigned int lid, const wchar_t *, unsigned int s_len = 0);
			UI_64 Configure(Button::Header &, Button::PaletteEntry &, Element::LabelCfg * = 0);
			
			UI_64 ConfigureClip(AnchorType, FitType, unsigned int tes_sel, float size, float offset);
			UI_64 ConfigureClipButton(unsigned int flags, HMODULE libin = 0);

			UI_64 ConfigureColors(unsigned int color, unsigned int bcolor);
			UI_64 ConfigureIcon(unsigned int icon_xw, unsigned int icon_yh, unsigned int text_xy, unsigned int max_char_count, const wchar_t * font_name = 0);
			
			void SetDefaultHandlers(SFSco::Object &);
			void SetBlink(Button::Header &, unsigned int delay_0, unsigned int delay_1, unsigned color_0, unsigned int color_1, unsigned int xcolor_01);

			void SetSwitchLabels(unsigned int, const wchar_t *, unsigned int, const wchar_t *, unsigned int);
			unsigned int GetSwitchState();

			UI_64 ConfigureXIcon(UI_64, float *);
			UI_64 StartPreview(unsigned int);
//			UI_64 StopPreview();
		private:
			void MUp(Config **);
			void MHoover(Config **);
			void MLeave(Config **);
			void MDown(Config **);

	};


	class Text: public Element {
		protected:
			void AdjustTQuad(float *, unsigned int);

		public:
			static const UI_64 header_tid = 4022940479;
			static const UI_64 type_id = 6257*Element::type_id;
			static const UI_64 symbol_tid = 6263*Element::type_id;

			enum Style : unsigned int {Undef = 0x80000000, Regular = 0, Left = 1, Italic = 2, Bold = 4, Vertical = 8, Mono = 16};

			struct Header {
				unsigned int font_id, font_style;
				unsigned int level_selector, max_char_count, fcolor_offset;
				float font_size;
				float font_color[4];
				unsigned int _top, _runner;

				Header() : _runner(0), _top(0), font_id(-1), font_style(0), level_selector(0), max_char_count(0), fcolor_offset(0), font_size(8.0f) {
					font_color[0] = font_color[1] = font_color[2] = 0.0f;
					font_color[3] = 1.0f;
				};

				Header & operator =(const Header & refhdr) {
					_runner = 0;
					_top = 0;

					font_id = refhdr.font_id;
					font_style = refhdr.font_style;
					level_selector = refhdr.level_selector;
					max_char_count = refhdr.max_char_count;
					fcolor_offset = refhdr.fcolor_offset;
					font_size = refhdr.font_size;

					font_color[0] = refhdr.font_color[0];
					font_color[1] = refhdr.font_color[1];
					font_color[2] = refhdr.font_color[2];
					font_color[3] = refhdr.font_color[3];
										
					return *this;
				};

			};


			Text();
			virtual ~Text();

			unsigned int SetFont(const wchar_t * fname);
			unsigned int SetFont(unsigned int);

			void SetMCCount(unsigned int);

			void SetSize(float);			
			void SetTextColor(const float *);
			void SetStyle(Style);

			UI_64 OverWrite(const wchar_t *, UI_64 = 0);
			void Rewrite(const wchar_t *, UI_64 offset, UI_64 = 0);
			void Rewrite(Text &);
			void Update(const wchar_t *, UI_64 = 0);


	};

	class ClipElement : public Element {
		public:
			static const UI_64 type_id = 6271*Element::type_id;

			UI_64 Configure(unsigned int color, float * box_vals, unsigned int sval = 3);

			ClipElement();
			virtual ~ClipElement();

	};


	class ButtonGrid : public ClipElement {
		private:
			static UI_64 PageUp(ButtonGrid &);
			static UI_64 PageDown(ButtonGrid &);
			
		public:
			typedef UI_64 (* ActionType)(ButtonGrid &, UI_64);
			struct Header {

				Element top_but, bottom_but;
								
				UI_64 (* size_action_ptr)(OpenGL::Element &);
				ActionType action_ptr;

				unsigned int (* format_button)(OpenGL::Element &, const Button::Header &, unsigned int &, unsigned int);
				unsigned int (* list_scan_down)(unsigned int &, UI_64 &);
				unsigned int (* list_scan_up)(unsigned int &, UI_64 &);
				
				unsigned int cell_count, row_count, column_count, page_size, current_page, max_page, icon_runner, ui_reserved[3];
								
				float page_width, page_height;
				float width, height;
				float cell_width, cell_height;
				float margin[4]; // left, top, right, bottom

				Element last_but;

				Button::Header button_set;

			};


			static const UI_64 type_id = 6277*ClipElement::type_id;
			static const UI_64 header_tid = 4022941001;

			static UI_64 NextPage(ButtonGrid &);
			static UI_64 PrevPage(ButtonGrid &);

			static UI_64 PreviewIcon(unsigned int);

			void __fastcall ButtonHoover(Button &);
			void __fastcall ButtonLeave(Button &);
			void __fastcall ButtonDown(Button &);
			void __fastcall ButtonUp(Button &);

			UI_64 Start(Element &);
			UI_64 Stop();
			UI_64 Select(Element &);

			ButtonGrid();
			virtual ~ButtonGrid();

			virtual UI_64 XPand();
			UI_64 XPand(const float *);

			UI_64 ResetGrid(unsigned int);
			UI_64 Jump(int, UI_64);

			unsigned int LoadIcon(unsigned short iid, HMODULE);			

			void SetButtonUpdate(void * format_but, void * li_scan_upm, void * li_scan_down, unsigned int = 0);

			void AddPaletteEntry(Button::PaletteEntry &);

			void ConfigureCell(ActionType, float width, float height, unsigned int bo_flags, float border_offset = -1.0f);
			void ConfigureIcon(unsigned int xw, unsigned int yh, unsigned int i_flags, AnchorType, float b_offset = -1.0f);
			void ConfigureIconTrajectory(void (__fastcall *)(float *, unsigned int), unsigned int option = 0, AttributeArray::Type = AttributeArray::Vertex);
			void SetXIcon();

			unsigned int ConfigureTextLine(unsigned int color, unsigned int tcolor, unsigned int flags, float x, float y, float size, unsigned int line_id = -1, const wchar_t * font_name = 0);
			unsigned int ConfigureTextLine(const LabelCfg *, unsigned int line_id = -1);

			void ConfigureCellTLHoover(unsigned int lidx, unsigned int color, unsigned int t_color);
			void ConfigureCellTLDown(unsigned int lidx, unsigned int color, unsigned int t_color);

			void SetMargin(float *);
			void SetHMode();
	};

	class StringsBoard: public Element {
		private:




		public:
			static const UI_64 type_id = 6353*Element::type_id;

			enum InfoFlags {
				InfoTypeMask =			0x0F000000,
				InfoTypeDouble =		0x01000000,
				InfoTypeINT64 =			0x02000000,
				InfoTypeTime =			0x03000000,
				InfoTypePcnt =			0x04000000,
				InfoTypeFormatted =		0x80000000,

				InfoTypeDMask =			0x0000000F,


				InfoDataPrefix =		0x00080000,

				
			};

			struct Header {
				unsigned int la_count, ui_reserved;

				LabelCfg la_cfg[1];
			};

			struct InfoLine {
				wchar_t cap[256];
				unsigned int cap_size, cap_flags;
				UI_64 val;
			};

			StringsBoard();
			virtual ~StringsBoard();



	};


	class APPlane: public Element {
		private:
			static Button function_area;
			static APPlane current_app;

			static struct FunctionSet {
				Element element[2];
				Element op_el;
				unsigned int _clock, _life_time, _count, group_id;

			} _functions;


			static UI_64 CPShow(Element &);
			static UI_64 CPHide(Element &);
						

			static void RemoveFunction(Element & button_obj);
			static void DumpFunction(Element &);
			
			void PushApp();
		public:
			static const UI_64 scale_set[];
			struct Header {
				Button fun_but;
				SFSco::Object play_list;
				OpenGL::DecodeRecord deco_rec;
				UI_64 (* screen_proc)(Element &);
				unsigned int pi_count, pic_tid, pi_sel, _reserved;

				Element board_clip;

				float cp_margin[8]; // horizontal_lrtb, vertical_lrtb
			};


			static const UI_64 type_id = 6299*ClipElement::type_id;
			enum CfgFlags : unsigned int {BgColor = 1, BgTexture = 2, NoCP = 4};

			static UI_64 TexCoordReset(Element &, unsigned int);
			static UI_64 PlaneSelect(Element &);
						
			static unsigned int AddFunction(Button & button_obj, UI_64 flags, float size);
			static void ResetFunction(Element &);
			static void FunctionAlive();			
			static UI_64 FunctionStretch(unsigned int &, unsigned int);

			static UI_64 CreateFunctionArea(OpenGL::Core *, int, unsigned int);

			static void PopApp(Element &);
			static bool TestCA();


			APPlane();
			virtual ~APPlane();

			virtual void Destroy();
			
			void SetCPMargin(const float *);
			UI_64 Configure(unsigned int c_flags, unsigned int b_color, float, unsigned int = 100);
			UI_64 SetPicture(Element &, const float *, unsigned int);
			
			UI_64 AddControl(Element &, unsigned int, unsigned int, unsigned int = 0);
			UI_64 AddControlLabel(Element::LabelCfg &);
			
			UI_64 GetFirstScreen(float *);

			void HideCPBoards();
			void GetCPBoard(Element &, unsigned int, const wchar_t *);
			UI_64 SetICPText(unsigned int, StringsBoard::InfoLine &);

			

			void RootScreen();
	};



	class SliderX: public Element {
		private:
			UI_64 Touch(float *);

			UI_64 ValueAdjust(const double * = 0);
			UI_64 SetLabels();

			void SUp();
			void SHoover();
			void SLeave();
			void SDown();

			void SBUp(Element &);
			void SBHoover();
			void SBLeave();
			void SBDown();

			void Jump(Element &);

		public:
			static const UI_64 type_id = 6323*Element::type_id;
			typedef UI_64 (* ControlFunc)(SliderX &, UI_64);


			enum SliderXFlags : unsigned int {
											
				ValTypeMask			= 0x00F00000,
				ValTypeUInt			= 0x00100000,
				ValTypeInt			= 0x00200000,
				ValTypeTime			= 0x00300000,
				ValTypeDouble		= 0x00400000,
				ValTypePcnt			= 0x00500000,


				HorizontalStretch	= 0x01000000,
				LabelTypeSS			= 0x02000000,
				LabelTypeL			= 0x04000000,
				BorderOn			= 0x08000000,
				IconOn				= 0x10000000,
				ControlOn			= 0x20000000,
				FixedControl		= 0x40000000
			};

			struct Header {
				union {
					UI_64 ui;
					__int64 i;
					double d;
				} value[4]; // min, max, current, relative

				unsigned int v_type;				
				unsigned int flags;

				double increment_val;

				float face_width, progress_width, lz_val, f_reerved;
				const UI_64 * vals_list;
				ControlFunc vc_func;

				LabelCfg label_cfg[2];
				Button::PaletteEntry palette_cfg[2];
			};
								

			UI_64 CoordsReset();


			SliderX();
			virtual ~SliderX();

			UI_64 Configure(ControlFunc vc_ptr, const Button::PaletteEntry *, unsigned int = 0);
			
			void SetLabelWidth(unsigned int, unsigned int = 0);
			void SetRange(const UI_64 *);
			void SetRange(UI_64, UI_64, UI_64);
			void SetValue(UI_64);			

			void Relocate(const float *);

	};

	class SliderClip: public Element {
		private:

		public:
			static const UI_64 type_id = 6329*Element::type_id;

			struct Header {
				unsigned int cell_count, _flags;
				float c_offset, _reserved;
				float cell_width, cell_height;

			};

			UI_64 CreateSX(SliderX & s_obj, unsigned int la_flags, const LabelCfg * la_cfg, float w = 0.0f);

			UI_64 CoordsReset(float *);

			SliderClip();
			virtual ~SliderClip();

	};


	class ListBoxX: public Element {
		private:
			static unsigned int ScanListUp(unsigned int &, UI_64 &);
			static unsigned int ScanListDown(unsigned int &, UI_64 &);

			void LUp();
			void LHoover();
			void LLeave();
			void LDown();

			void LBNUp(Element &);
			void LBPUp(Element &);

			void LBHoover();
			void LBLeave();
			void LBDown();

			
		public:
			static const UI_64 type_id = 6317*Element::type_id;

			enum LBXFlags : unsigned int {
				HorizontalStretch	= 0x01000000,
				BorderOn			= 0x02000000,
				IconOn				= 0x04000000,
				ControlOn			= 0x08000000,
				DoubleLabel			= 0x10000000,

				FixedControl		= 0x40000000
			};
			

			struct Header {				
				unsigned int flags, i_reserved;

				float face_width, face_height, control_width, f_reserved;


				LabelCfg label_cfg[2];
				Button::PaletteEntry palette_cfg[2];
			};


			UI_64 Configure(const Button::PaletteEntry *, unsigned int = 0);			
			UI_64 ConfigureGrid(void *, ButtonGrid::ActionType, const float *, Button::PaletteEntry *, unsigned int = 0);
			UI_64 ConfigureGridText(const LabelCfg *);
			UI_64 ConfigureGridIcon();

			UI_64 Relocate(const float *, unsigned int);

			ListBoxX();
			virtual ~ListBoxX();


	};


	class ListBoxClip: public Element {
		private:

		public:
			static const UI_64 type_id = 6337*Element::type_id;

			struct Header {
				unsigned int cell_count, _flags;
				float c_offset, _reserved;
				float cell_width, cell_height;
							
			};

			UI_64 CreateLBX(ListBoxX & lb_obj, unsigned int la_flags, const LabelCfg * la_cfg, unsigned int wh = 0);

			UI_64 CoordsReset(float *, const unsigned int *, unsigned int = 1);
			
			ListBoxClip();
			virtual ~ListBoxClip();

	};

	
	class D2Board: public Element {
		private:

		public:
			static const UI_64 type_id = 6343*Element::type_id;

			enum Flags: unsigned int {
				FlagBorder =			0x80000000,
				FlagCaption =			0x40000000,
				FlagLeftAxis =			0x20000000,
				FlagRightAxis =			0x10000000,

				FlagTopAxis =			0x08000000,
				FlagBottomAxis =		0x04000000,
				FlagVerticalGrid =		0x02000000,
				FlagHorizontalGrid =	0x01000000,

				FlagVAxisLabels =		0x00800000,
				FlagHAxisLabels =		0x00400000,


				FlagPlotTypeMask =		0x0000F000,
				FlagPlotTypeLine =		0x00000000,
				FlagPlotTypePoint =		0x00001000,
				FlagPlotTypeBar =		0x00002000,



				FlagHAxisLTypeMask =	0x00000C00,
				FlagVAxisLTypeMask =	0x00000300,


			};

			// 0 - double, 1 - int64, 2 - time, 3 - pcnt

			struct PlotCfg {
				unsigned int flags, capacity;
				int flat_colors[4];
				float line_color[16], grid_color[16], axis_color[16], border_color[16]; 
				LabelCfg caption_label, axis_lables[2];

			};

			D2Board();
			virtual ~D2Board();

			void SetDataScale(const float *);
			void SetDataOffset(const float *);

	};

	


	class BoardClip: public Element {
		private:



		public:
			static const UI_64 type_id = 6359*Element::type_id;
			struct Header {
				unsigned int cp_board_count, cp_flags;				

				float bc_width, bc_height;
			};

			BoardClip();
			virtual ~BoardClip();

			UI_64 Configure(const LabelCfg * info_label, unsigned int la_count, unsigned int b_color = 0);

			UI_64 CreateD2Board(D2Board &, const D2Board::PlotCfg &);

			UI_64 CoordsReset(float *);
	};

}


