/*
** safe-fail font program reader
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once
#include <Windows.h>
#include "CoreLibrary\SFScore.h"

#include "System\Memory.h"

#define RC_OTF_FONT			2
#define FLAG_COMPLETE_FONT	0x08000000

#define MAX_RANGE_COUNT		64

namespace OpenGL {

class Font: public SFSco::Object {
	friend class TTF;
	friend class CFF;

	private:
		static const unsigned int lowest_level_size = 8;
		static const unsigned int glyph_detail_level = 6;
		static const unsigned int glyph_n_size = lowest_level_size*lowest_level_size*(((1 << (glyph_detail_level*2)) - 1)/3);		
		static const unsigned int glyph_r_size = glyph_n_size + (lowest_level_size<<glyph_detail_level);
		static int box_size[2*glyph_detail_level];

		static Memory::Allocator * font_pool;

		struct SegmentRecord {
			unsigned short start_code[MAX_RANGE_COUNT];
			unsigned short end_code[MAX_RANGE_COUNT];
			unsigned int r_count, depth, tex_id, buf_id;
					
			unsigned int curves_count, point_count;

			SFSco::Object glyph_off_block, glyph_curve_block, glyph_point_block;
			SFSco::Object raster_data; // metrix and raster
	
		};
		

		struct Header {
			wchar_t font_path[256], font_name[256];
			UI_64 segments_offset, map_offset, cff_header_offset, cff_offset;
						
			unsigned int asegment_count, max_size;
			
			unsigned int font_type, _reserved;
			
		};
		
		struct GlyfHeader {
			short contour_count;

			short x_min;
			short y_min;
			short x_max;
			short y_max;

			GlyfHeader() : contour_count(0), x_min(32767), y_min(32767), x_max(-32768), y_max(-32768) {};
		};
		

		struct SegmentSet {
			const unsigned char * src_ptr;
			UI_64 glyf_index_offset, src_size;
			unsigned short first_glyf_index, p_matched, p_match;
			unsigned int of_runner, cu_runner, cu_top, po_runner, po_top, more_gly, flippy;
			float s_fac[8];

			SFSco::Object offset_block, curve_block, point_block;
		};


		static wchar_t font_dir[256];

		static unsigned int _fna_top, _fna_runner, _fli_runner, _fli_top, special_tid, special_bid;

		static unsigned char unknown_mark[glyph_r_size];
		static const int unknown_box[];
		static const unsigned int unknown_curves[], unknown_points[];


		static SFSco::Object fname_list;
		static SFSco::Object font_list;
		


		static unsigned int FindFont(const wchar_t *);

		static void RasterizeGlyph(void *, unsigned int, const unsigned int *, const float *);
		static void FormatBox(float *, float *, const float *);
		
		UI_64 InitializeRange(SFSco::Object & selector);
		UI_64 RangeAvailable(SFSco::Object & selector);
		UI_64 InitializeComplete(const unsigned char *, UI_64);

		UI_64 GetBox(unsigned int &, float *, float *);

		UI_64 RasterizeSegment(unsigned int, unsigned int, SegmentSet &);
		UI_64 LoadGlyf(Font::SegmentSet &);
		static UI_64 SpecialInit();

	public:

		static const UI_64 type_id = 4022940343;
		static const UI_64 segment_record_tid = 4022940391;
		static const UI_64 font_nodes_tid = 4022940409;
		static const UI_64 font_offsets_tid = 4022940413;
		static const UI_64 font_curves_tid = 4022940421;
		static const UI_64 font_list_tid = 4022940437;
		static const UI_64 font_names_tid = 4022940451;
		static const UI_64 segment_raster_tid = 4022940457;


		static UI_64 Initialize();
		static void Finalize();

		static unsigned int Create(const wchar_t * fname, const wchar_t * = 0, unsigned int = 0);
		static unsigned int Create(const wchar_t * fname, const unsigned char * src_ptr, unsigned int flags, UI_64 src_size);

		static UI_64 InitializeRange(unsigned int fid, SFSco::Object &, HWND m_handle);
		static UI_64 InitializeComplete(unsigned int fid, const unsigned char *, UI_64);

		static UI_64 GetMetrixBox(unsigned int, unsigned int &, SFSco::Object &, float *);

		static UI_64 OGLoad(unsigned int g256_selector);
		static unsigned int AddFont(Font &, const wchar_t *);

		Font & operator =(const SFSco::Object &);


		Font();
		~Font();
};


class CFF {
	friend class Font;
	friend class TTF;
	private:
		static const unsigned int std_string_count = 391;
		static const wchar_t * std_strings[];
		static const char float_codes[];


		struct Version {
			unsigned char major;
			unsigned char minor;
			unsigned char size;
			unsigned char offSize;

		};

		struct IndexHeader {
			unsigned short count;
			unsigned char offSize;
		};

		struct SubrsInfo {
			unsigned int local_off, global_off;
			short local_bias, global_bias;
		};


		struct Header {
			unsigned int encoding_off;
			unsigned int char_set_off;
			unsigned int char_strs_off;
			unsigned int charstring_type;

			unsigned int private_size;
			unsigned int private_off;
			unsigned int fd_array_off;
			unsigned int fd_select_off;

			SubrsInfo subrs;
									
		};


		struct GlyphRunners {
			unsigned int epkye, hint_count;
			short current_x, current_y, first_x, first_y;

			GlyphRunners() : epkye(0), hint_count(0), current_x(0), current_y(0), first_x(0), first_y(0) {};
		};

		struct SubrSet {
			const Header * cff_hdr;
			const unsigned char * root_ptr, * gl_pointer;
			unsigned int dict_i, glength;
			unsigned short gindex, s_rese;
		};

		struct PoSet {
			unsigned int * p_counts;
			unsigned char * onoff;
			int * points;
		};

		static bool BuildGHeader(Font::GlyfHeader &, GlyphRunners &, SubrSet, short *);
		static bool CollectGCurves(PoSet &, GlyphRunners &, SubrSet, short *);

		static UI_64 GetLocation(Font &, const unsigned char *, UI_64);
		
		static UI_64 LoadGlyf(Font &, Font::SegmentSet &, unsigned short);
		static void NormalizeGlyph(unsigned int *, int *, unsigned int);

		static UI_64 Create(Font &, const unsigned char *, UI_64);
		static UI_64 LoadHeader(Font &, const unsigned char *, UI_64);

		static void CharSetRemap(const unsigned char *, unsigned short, unsigned short &);

// post script
		static void FormatOffsets(unsigned int *, const unsigned char *, unsigned short, unsigned char);
		static unsigned int DecodeVal(const unsigned char *, unsigned int &);
		static short DecodeGlyphVal(const unsigned char *, unsigned int &, short *);
		static float DecodeFloat(const unsigned char *, unsigned int &);

		static UI_64 GetGlobalSubrs(Font &, const unsigned char *, unsigned int);
		static UI_64 GetLocalSubrs(Font &, const unsigned char *);

		static void FDSelect(SubrSet &, unsigned short);

		

};

class TTF {
	friend class Font;
	friend class CFF;
	private:
		static const unsigned short platform_windows = 0x0300;
		static const unsigned short unicode_id = 0x0100;
		static const unsigned short map_table_format = 0x0400;

		struct OS2_Header {
			struct PANOSE {
				unsigned char family_type;
				unsigned char serif_style;
				unsigned char weight;
				unsigned char proportion;
				unsigned char contrast;
				
				unsigned char stroke_variation;
				unsigned char arm_style;
				unsigned char letter_form;
				unsigned char midline;
				unsigned char x_height;

			};

			union CodePage1 {
				struct {
					bool Latin_1 : 1;
					bool Latin_2 : 1;
					bool Cyrillic : 1;
					bool Greek : 1;

					bool Turkish : 1;
					bool Hebrew : 1;
					bool Arabic : 1;
					bool Baltic : 1;

					char : 8;

					bool Thai : 1;
					bool Japan : 1;
					bool Chinese_S : 1;
					bool Korean_W : 1;
					
					bool Chinese_T : 1;
					bool Korean_J : 1;
					bool : 1;
					bool : 1;

					char : 4;
					
					bool : 1;
					bool US_Roman : 1;
					bool OEM : 1;
					bool Symbol : 1;


				};

				unsigned int _range;
			};

			union CodePage2 {
				struct {
					char : 8;
					char : 8;

					bool IBM_Greek : 1;
					bool MS_DOS_RU : 1;
					bool MS_DOS_NO : 1;
					bool Arabic : 1;

					bool Canadian : 1;
					bool Hebrew : 1;
					bool MS_DOS_IC : 1;
					bool MS_DOS_PO : 1;

					bool IBM_Turkish : 1;
					bool IBM_Cyrillic : 1;
					bool Latin_2 : 1;
					bool MS_DOS_BA : 1;

					bool Greek : 1;
					bool Arabic_ASMO : 1;
					bool WE : 1;
					bool US : 1;
				};

				unsigned int _range;
			};

			unsigned short version;

			short x_avg_char_width;
			unsigned short weight_class;
			unsigned short width_class;

			short type_flags;

			short subscr_x_size;
			short subscr_y_size;
			short subscr_x_offset;
			short subscr_y_offset;

			short supscr_x_size;
			short supscr_y_size;
			short supscr_x_offset;
			short supscr_y_offset;

			short strikeout_size;
			short strikeout_position;

			short family_class;
			PANOSE panose;

			unsigned int unicode_range1;
			unsigned int unicode_range2;
			unsigned int unicode_range3;
			unsigned int unicode_range4;

			char vendor_id[4];

			unsigned short selection_flags;

			unsigned short first_char_index;
			unsigned short last_char_index;

			unsigned short typo_ascender;
			unsigned short typo_descender;
			unsigned short typo_line_gap;

			unsigned short win_ascend;
			unsigned short win_descent;

			CodePage1 code_page1;
			CodePage2 code_page2;

		};

		struct NameRecord {
			enum nameids : unsigned short {Copyright = 0, FontFamily = 0x0100, SubFamily = 0x0200, UID = 0x0300, FullName = 0x0400, Version = 0x0500, Postscript = 0x0600, Trademark = 0x0700};
			unsigned short platform_id;
			unsigned short encoding_id;
			unsigned short lang_id;
			nameids name_id;
			unsigned short string_length;
			unsigned short string_offset;
		};
		
		struct ProfileTable {
			unsigned int version;

			unsigned short glyf_count;
			
			unsigned short max_points_count;
			unsigned short max_contours_count;
			unsigned short max_composite_points_count;
			unsigned short max_composite_contours_count;

			unsigned short max_zones_count;
			unsigned short max_tw_points_count;
			unsigned short max_storage_locations;

			unsigned short max_functions_defs;
			unsigned short max_instruction_defs;

			unsigned short max_stack_depth;
			unsigned short max_instruction_size;
			unsigned short max_component_count;
			unsigned short max_recurtion_depth;

		};

		struct HeaderTable {
			unsigned int version;
			unsigned int revision;
			unsigned int check_sum;
			unsigned int magic_num;
			
			unsigned short flags;
			unsigned short units_per_em;
			unsigned int created[2];
			unsigned int modified[2];

			short x_min;
			short y_min;
			short x_max;
			short y_max;

			unsigned short mac_style;
			unsigned short smallest_size;

			short direction_hint;

			short location_format;
			short glyf_format;
		};

		struct OffsetTable {
			unsigned int version_fixed;
			unsigned short directory_entry_count;
			unsigned short search_range;
			unsigned short entry_selector;
			unsigned short range_shift;

		};

		struct DirectoryEntry {
			unsigned int table_tag;
			unsigned int check_sum;
			unsigned int table_offset;
			unsigned int table_legnth;
		};

		struct CharEncodingTable {
			unsigned short version;
			unsigned short encoding_entry_count;
		};

		struct EncodingEntry {
			unsigned short platrform;
			unsigned short id;
			unsigned int offset;
		};

		struct MapTableHeader { // page 38
			unsigned short format;
			unsigned short length;
			unsigned short version;
			
			unsigned short segment_count;

			unsigned short search_range;
			unsigned short entry_selector;
			unsigned short range_shift;

		};


		struct Header {
			UI_64 location_offset, segments_offset;
			unsigned int glyf_tab_off;
						
			HeaderTable header;
			OS2_Header win_metrics;
			ProfileTable profile;			
			EncodingEntry encoding_table;
			MapTableHeader map_table_header;

		};

		static float __fastcall Fixed(unsigned int);
		static float __fastcall F2dot14(unsigned short);

		static void __fastcall PointAdjust(int *, const float *);
		static void __fastcall SetMidPoint(int *);
		static void ContourFlip(void *, unsigned char *, unsigned int);

		static UI_64 GetLocation(Font &, const unsigned char *, unsigned int, UI_64);
		static UI_64 GetMapHeader(Font &, const unsigned char *, UI_64);

		static UI_64 Create(Font &, const unsigned char *, UI_64);
		static UI_64 LoadGlyf(Font &, Font::SegmentSet &, unsigned short, unsigned int = -1);
		static void NormalizeGlyph(unsigned int *, int *, unsigned int);

		enum GlyFlags : unsigned char {OnCurve = 1, XByte = 2, YByte = 4, Repeat = 8, XSame = 16, YSame = 32};
		enum CompoFlags : unsigned short {AA_WORDS = 1, XY_VALS = 2, XY_2GRID = 4, SSCALE = 8, RESERVED = 16, MORE_CMPS = 32, XY_SCALE = 64, SCALE_22 = 128, INSTR = 256, METRICS = 512};


};




}





