/*
** safe-fail font program reader
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\Font.h"

#include <ShlObj.h>

#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\OpenGL.h"

#include "System\SysUtils.h"
#include "System\Control.h"



/*
	off_block layout:
		
		0: parent glyph id
		1: curves offset (into glyph_curve_block)
		2: points offset (into glyph_point_block)

		3: xscale factor
		4: x_min
		5: y_min
		6: width
		7: height


	glyph_curve_block layout:


	glyph_point_block layout:





*/

Memory::Allocator * OpenGL::Font::font_pool = 0;

wchar_t OpenGL::Font::font_dir[256];
unsigned int OpenGL::Font::_fli_top = 0;
unsigned int OpenGL::Font::_fli_runner = 0;
unsigned int OpenGL::Font::_fna_runner = 0;
unsigned int OpenGL::Font::_fna_top = 0;

unsigned int OpenGL::Font::special_tid = 0;
unsigned int OpenGL::Font::special_bid = 0;

SFSco::Object OpenGL::Font::font_list;
SFSco::Object OpenGL::Font::fname_list;

int OpenGL::Font::box_size[2*glyph_detail_level];

unsigned char OpenGL::Font::unknown_mark[glyph_r_size];

UI_64 OpenGL::Font::OGLoad(unsigned int ssel) {
	if (ssel) {
		ActiveTexture(GL_TEXTURE0 + OpenGL::Core::glyph_raster);
		glBindTexture(GL_TEXTURE_2D_ARRAY, ssel);
	}

	return 0;
}

UI_64 OpenGL::Font::Initialize() {
	wchar_t pool_name[] = {9, L'f',L'o', L'n', L't', L' ', L'p', L'o', L'o', L'l'};
	UI_64 result(0);

	SFSco::Object mobj;

	if (font_pool = new Memory::Allocator(0)) {
		result = font_pool->Init(new Memory::Buffer(), 0x00100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;

	if (!result) {
		result = fname_list.New(font_names_tid, 1024, font_pool);

		if (result != -1) {
			_fna_top = 512;

			result = font_list.New(font_list_tid, 128*sizeof(SFSco::Object), font_pool);
			if (result != -1) {
				_fli_top = 128;
				result = SHGetFolderPath(0, CSIDL_FONTS, 0, SHGFP_TYPE_CURRENT, font_dir);
			}	

		}

		for (unsigned int i(0);i<glyph_detail_level;i++) {
			box_size[i] = (lowest_level_size << (glyph_detail_level - i - 1));
			box_size[glyph_detail_level+i] = box_size[i]*box_size[i];
		}


		if (!result) {
			System::MemoryFill_SSE3(unknown_mark, glyph_n_size, 0, 0);

			RasterizeGlyph(unknown_mark, 0x80000001, unknown_curves, reinterpret_cast<const float *>(unknown_points));
		}

		if (!result) {
			if (result = (Create(L"Calibril") == -1)) {
				result = (Create(L"Cour") == -1);
			}
		}
	}

	return result;
}

UI_64 OpenGL::Font::SpecialInit() {
	unsigned int result(0);
	unsigned char * f_raster(0);

	glGenTextures(1, &special_tid);
	GenBuffers(1, &special_bid);
					
	ActiveTexture(GL_TEXTURE0 + OpenGL::Core::glyph_rloader);
	glBindTexture(GL_TEXTURE_2D_ARRAY, special_tid);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, Font::glyph_detail_level-1);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	for (unsigned int i(0);i<Font::glyph_detail_level;i++) {
		TexImage3D(GL_TEXTURE_2D_ARRAY, i, GL_R8, box_size[i], box_size[i], 16, 0, GL_RED, GL_UNSIGNED_BYTE, 0);		
	}


	f_raster = unknown_mark;
	for (unsigned int i(0);i<Font::glyph_detail_level;i++) {
		TexSubImage3D(GL_TEXTURE_2D_ARRAY, i, 0, 0, 0, box_size[i], box_size[i], 1, GL_RED, GL_UNSIGNED_BYTE, f_raster);
		f_raster += box_size[glyph_detail_level+i];		
	}

	BindBuffer(GL_ARRAY_BUFFER, special_bid);
	BufferData(GL_ARRAY_BUFFER, OpenGL::Core::max_texture_layers*4*sizeof(float), 0, GL_STATIC_DRAW);
	BufferSubData(GL_ARRAY_BUFFER, 0, 4*sizeof(float), unknown_box+4);


	return result;
}

void OpenGL::Font::Finalize() {

	if (font_pool) delete font_pool;
}

OpenGL::Font::Font() {
	SetTID(type_id);
}

OpenGL::Font::~Font() {

}

OpenGL::Font & OpenGL::Font::operator=(const SFSco::Object & rfo) {
	if (rfo.Is(type_id)) dynamic_cast<SFSco::Object &>(*this) = rfo;
	return *this;
}

unsigned int OpenGL::Font::FindFont(const wchar_t * fname) {
	unsigned int fid(-1);

	if (_fli_runner)
	if (wchar_t * fnames = reinterpret_cast<wchar_t *>(fname_list.Acquire())) {
		if (wchar_t * fopo = Strings::SearchWStr(fnames, fname)) {
			fopo -= 2;

			fid = fopo[0] & fopo[1];			
		}

		fname_list.Release();
	}

	return fid;
}

UI_64 OpenGL::Font::InitializeRange(unsigned int fid, SFSco::Object & s_obj, HWND m_handle) {
	UI_64 result(-2);
	Font xfont;

	if (fid < _fli_runner)
	if (SFSco::Object * flipo = reinterpret_cast<SFSco::Object * >(font_list.Acquire())) {
		xfont = flipo[fid];
		result = 0;
		font_list.Release();
	}

	if (!result) {
// forward to main thread
		if (xfont.RangeAvailable(s_obj) == 0) {
			if (GetCurrentThreadId() != GetWindowThreadProcessId(m_handle, 0)) {
				
				if (HANDLE local_block = CreateEvent(0, 0, 0, 0)) {
					UI_64 (Font::* pi_range)(SFSco::Object &) = &Font::InitializeRange;
					void * ptr_list[4];

					ptr_list[0] = &s_obj;
					ptr_list[1] = &xfont;
					ptr_list[2] = reinterpret_cast<void*&>(pi_range);
					ptr_list[3] = 0;


					PostMessage(m_handle, INVOKE_IT, (WPARAM)ptr_list, (LPARAM)local_block);
					WaitForSingleObject(local_block, INFINITE);

					result = reinterpret_cast<UI_64 *>(ptr_list)[3];

					CloseHandle(local_block);
				}
			} else {
				xfont.InitializeRange(s_obj);
			}
		}
	}

	return result;
}

UI_64 OpenGL::Font::InitializeComplete(unsigned int fid, const unsigned char * fptr, UI_64 ff_size) {
	UI_64 result(-2);

	Font xfont;

	if (fid < _fli_runner)
	if (SFSco::Object * flipo = reinterpret_cast<SFSco::Object * >(font_list.Acquire())) {
		xfont = flipo[fid];
		result = 0;
		font_list.Release();
	}

	if (!result) result = xfont.InitializeComplete(fptr, ff_size);

	return result;
}


UI_64 OpenGL::Font::GetBox(unsigned int & selector, float * coords, float * ovector) {	
	UI_64 tex_id(0);
	unsigned int ocount(0), rid(0);

	if (Header * fhdr = reinterpret_cast<Header *>(Acquire())) {
		SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);
		
		for (unsigned int i(0);i<fhdr->asegment_count;i++) {
			ocount = 0;
			for (unsigned int ri(0);ri<seg_rec[i].r_count;ri++) {
				if ((seg_rec[i].start_code[ri] <= selector) && (selector <= seg_rec[i].end_code[ri])) {
					rid = ri;

					tex_id = seg_rec[i].buf_id;
					tex_id <<= 32;
					tex_id |= seg_rec[i].tex_id;

					selector = selector - seg_rec[i].start_code[rid] + ocount;

					if (float * t_offs = reinterpret_cast<float *>(seg_rec[i].glyph_off_block.Acquire())) {
						FormatBox(coords, ovector, t_offs+selector*8+4);
						seg_rec[i].glyph_off_block.Release();
					}

					break;
				}
				ocount += (seg_rec[i].end_code[ri] - seg_rec[i].start_code[ri] + 1);
			}
			if (tex_id) break;
		}
		Release();
	}



	if (!tex_id) {
// unknown character
		selector = 0;

		tex_id = special_bid;
		tex_id <<= 32;
		tex_id |= special_tid;

		FormatBox(coords, ovector, reinterpret_cast<const float *>(unknown_box+4));
	}

			

	return tex_id;
}


UI_64 OpenGL::Font::GetMetrixBox(unsigned int fid, unsigned int & selector, SFSco::Object & aarr, float * ovector) {
	UI_64 tcode(0);
	Font xfont;

	if (fid < _fli_runner)
	if (SFSco::Object * flipo = reinterpret_cast<SFSco::Object * >(font_list.Acquire())) {
		xfont = flipo[fid];
		font_list.Release();
	}

	if (AttributeArray::Header *  ahdr = reinterpret_cast<AttributeArray::Header *>(aarr.Acquire())) {
		tcode = xfont.GetBox(selector, reinterpret_cast<float *>(ahdr + 1), ovector);
		aarr.Release();
	}

	return tcode;
}

							
UI_64 OpenGL::Font::RasterizeSegment(unsigned int sid, unsigned int gco, SegmentSet & sese) {
	Header * fhdr(0);
	UI_64 result(0);
	unsigned char * po_onoff(0);
	unsigned int ocount(0), ccount(0), pcount(0), clock_type(0);

	SFSco::Object r_store, oobj, cobj, pobj;


	if (fhdr = reinterpret_cast<Header *>(Acquire())) {
		SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

		clock_type = fhdr->font_type & 0x80000000;

		ocount = seg_rec[sid].depth;
		ccount = seg_rec[sid].curves_count;
		pcount = seg_rec[sid].point_count;

		if (seg_rec[sid].glyph_off_block) {
			oobj = seg_rec[sid].glyph_off_block;
			cobj = seg_rec[sid].glyph_curve_block;
			pobj = seg_rec[sid].glyph_point_block;

		} else {
			oobj = seg_rec[sid].glyph_off_block = sese.offset_block;
			cobj = seg_rec[sid].glyph_curve_block = sese.curve_block;
			pobj = seg_rec[sid].glyph_point_block = sese.point_block;

		}

		Release();
		fhdr = 0;
	}

	if (oobj != sese.offset_block) {
		if (oobj.Expand(gco*8*sizeof(unsigned int)) != -1) {
			if (unsigned int * toffs = reinterpret_cast<unsigned int *>(oobj.Acquire())) {
				if (unsigned int * xoffs = reinterpret_cast<unsigned int *>(sese.offset_block.Acquire())) {
					System::MemoryCopy_SSE3(toffs + ocount*8, xoffs, gco*8*sizeof(unsigned int));

					sese.offset_block.Release();
				}

				oobj.Release();
				sese.offset_block.Destroy();
			}		
		} else {
			return ERROR_OUTOFMEMORY;
		}
	}

	if (cobj != sese.curve_block) {
		if (cobj.Expand((sese.cu_runner+1)*sizeof(unsigned int)) != -1) {
			if (unsigned int * cu_po = reinterpret_cast<unsigned int *>(cobj.Acquire())) {
				if (unsigned int * xu_po = reinterpret_cast<unsigned int *>(sese.curve_block.Acquire())) {
					System::MemoryCopy_SSE3(cu_po + ccount, xu_po, (sese.cu_runner+1)*sizeof(unsigned int));

					sese.curve_block.Release();
				}

				cobj.Release();
				sese.curve_block.Destroy();
			}		
		} else {
			return ERROR_OUTOFMEMORY;
		}
	}
	
	if (pobj != sese.point_block) {
		if (pobj.Expand(sese.po_runner*2*sizeof(float)) != -1) {
			if (float * po_po = reinterpret_cast<float *>(pobj.Acquire())) {
				if (float * xo_po = reinterpret_cast<float *>(sese.point_block.Acquire())) {
					System::MemoryCopy_SSE3(po_po + pcount*2, xo_po, sese.po_runner*2*sizeof(float));

					sese.point_block.Release();
				}

				pobj.Release();
				sese.point_block.Destroy();
			}		
		} else {
			return ERROR_OUTOFMEMORY;
		}
	}


	result = r_store.New(segment_raster_tid, (gco*glyph_n_size+(lowest_level_size << (glyph_detail_level-1))), font_pool);

	if (result != -1) {
		if (void * g_pic = r_store.Acquire()) {
			if (const unsigned int * cu_desc = reinterpret_cast<const unsigned int *>(cobj.Acquire())) {
				if (const float * g_points = reinterpret_cast<const float *>(pobj.Acquire())) {
	//				System::MemoryFill_SSE3(g_pic, (gco*glyph_n_size+(lowest_level_size << (glyph_detail_level-1))), 0, 0);
												
					RasterizeGlyph(g_pic, clock_type | gco, cu_desc + ccount, g_points + 2*pcount);
	
					result = 0;

					pobj.Release();
				}
				cobj.Release();
			}

			r_store.Release();
		}
				
	} else {
		result = ERROR_OUTOFMEMORY;
	}

	if (!result) {
		if (fhdr = reinterpret_cast<Header *>(Acquire())) {
			SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

			if (seg_rec[sid].raster_data) {
				oobj = seg_rec[sid].raster_data;

			} else {
				oobj = seg_rec[sid].raster_data = r_store;

			}

			Release();
			fhdr = 0;
		}
		
		if (oobj != r_store) {
			if (oobj.Expand(gco*glyph_n_size) != -1) {
				if (unsigned char * r_po = reinterpret_cast<unsigned char *>(oobj.Acquire())) {
					if (unsigned char * x_po = reinterpret_cast<unsigned char *>(r_store.Acquire())) {
						System::MemoryCopy_SSE3(r_po + ocount*glyph_n_size, x_po, gco*glyph_n_size);

						r_store.Release();
					}

					oobj.Release();
					r_store.Destroy();
				}
			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}

		if (!result)
		if (fhdr = reinterpret_cast<Header *>(Acquire())) {
			SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);




			// load to texture
			ActiveTexture(GL_TEXTURE0 + OpenGL::Core::glyph_rloader);
			glBindTexture(GL_TEXTURE_2D_ARRAY, seg_rec[sid].tex_id);
			
			BindBuffer(GL_ARRAY_BUFFER, seg_rec[sid].buf_id);


		//	gcount = seg_rec[segid].depth - groid;
		//	if (gcount > Core::max_texture_layers) gcount = Core::max_texture_layers;
			
			if (unsigned int * f_offsets = reinterpret_cast<unsigned int *>(seg_rec[sid].glyph_off_block.Acquire())) {
				f_offsets += 4 + seg_rec[sid].depth*8; // (8*groid
				if (unsigned char * f_raster = reinterpret_cast<unsigned char *>(seg_rec[sid].raster_data.Acquire())) {
					f_raster += seg_rec[sid].depth*glyph_n_size;

					for (unsigned int gi(0);gi<gco;gi++) {
						for (unsigned int li(0);li<glyph_detail_level;li++) {
							TexSubImage3D(GL_TEXTURE_2D_ARRAY, li, 0, 0, seg_rec[sid].depth + gi, box_size[li], box_size[li], 1, GL_RED, GL_UNSIGNED_BYTE, f_raster);
							f_raster += box_size[glyph_detail_level+li];
								
						}

						BufferSubData(GL_ARRAY_BUFFER, (seg_rec[sid].depth + gi)*4*sizeof(float), 4*sizeof(float), f_offsets);
						f_offsets += 8;						
					}

					seg_rec[sid].raster_data.Release();
				}
				seg_rec[sid].glyph_off_block.Release();
			}

			



			seg_rec[sid].depth += gco;
			seg_rec[sid].curves_count += sese.cu_runner;
			seg_rec[sid].point_count += sese.po_runner;

			Release();
			fhdr = 0;
		}



	}

	return result;
}

unsigned int OpenGL::Font::Create(const wchar_t * fname, const wchar_t * path_ptr, unsigned int icom) {
	Header * fhdr(0);
	UI_64 xval(0), result(0);
	LARGE_INTEGER fsize;
	HANDLE fhandle(INVALID_HANDLE_VALUE);
	Font xfont;

	unsigned int listid(-1);

	if (!path_ptr) path_ptr = font_dir;
	xval = wcslen(path_ptr);

	listid = FindFont(fname);
	if (listid == -1) {	
// create new		
		if (xfont.New(type_id, sizeof(Header)+sizeof(TTF::Header)+sizeof(CFF::Header), font_pool) != -1) {
			if (fhdr = reinterpret_cast<Header *>(xfont.Acquire())) {
				
	//			System::MemoryFill(fhdr, sizeof(Header)+sizeof(TTF::Header)+sizeof(CFF::Header), 0);

				System::MemoryCopy(fhdr->font_path, path_ptr, xval*sizeof(wchar_t));
				fhdr->font_path[xval++] = L'\\';

				System::MemoryCopy(fhdr->font_path+xval, fname, wcslen(fname)*sizeof(wchar_t));
				xval += wcslen(fname);
		
				fhdr->font_path[xval] = L'.';
				fhdr->font_path[xval+1] = L't';
				fhdr->font_path[xval+2] = L't';
				fhdr->font_path[xval+3] = L'f';
				fhdr->font_path[xval+4] = L'\0';
				
				fhdr->font_type = 0x80000000;
				fhdr->max_size = 1;
				
				fhdr->cff_header_offset = sizeof(Header) + sizeof(TTF::Header);
				fhdr->map_offset = sizeof(Header) + sizeof(TTF::Header) + sizeof(CFF::Header);


				fhandle = CreateFile(fhdr->font_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
				if (fhandle == INVALID_HANDLE_VALUE) {
					fhdr->font_type ^= 0x80000000;
					fhdr->font_path[xval+1] = L'o';
					fhandle = CreateFile(fhdr->font_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);					
				}

				xfont.Release();
				fhdr = 0;

				if (fhandle != INVALID_HANDLE_VALUE) {
					if (GetFileSizeEx(fhandle, &fsize)) {
						if (fsize.QuadPart < 0x10000000) {
							if (HANDLE file_map = CreateFileMapping(fhandle, 0, PAGE_READONLY, 0, 0, 0)) {
								if (const unsigned char * src_ptr = reinterpret_cast<const unsigned char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0))) {
									result = TTF::Create(xfont, src_ptr, fsize.QuadPart);

									if (!result) {
										listid = AddFont(xfont, fname);
										if ((listid != -1) && (icom)) result = InitializeComplete(listid, src_ptr, fsize.QuadPart);
									}

									UnmapViewOfFile(src_ptr);
								}
								CloseHandle(file_map);
							}
						}
					}
					CloseHandle(fhandle);
				} else {
					result = GetLastError();
				}
				
			}
		} else {
			result = ERROR_OUTOFMEMORY;
		}
	}
	
	return listid;

}

unsigned int OpenGL::Font::Create(const wchar_t * fname, const unsigned char * src_ptr, unsigned int flags, UI_64 src_size) {
	Header * fhdr(0);
	UI_64 xval = wcslen(font_dir), result(0);
	HANDLE fhandle(INVALID_HANDLE_VALUE);
	Font xfont;

	unsigned int listid(-1);

	listid = FindFont(fname);
	if (listid == -1) {	
// create new		
		if (xfont.New(type_id, sizeof(Header)+sizeof(TTF::Header)+sizeof(CFF::Header), font_pool) != -1) {
			if (fhdr = reinterpret_cast<Header *>(xfont.Acquire())) {
				
//				System::MemoryFill(fhdr, sizeof(Header)+sizeof(TTF::Header)+sizeof(CFF::Header), 0);
								
				System::MemoryCopy(fhdr->font_path, fname, wcslen(fname)*sizeof(wchar_t));
				xval = wcslen(fname);
		
				fhdr->font_path[xval] = L'\0';
				
				if (flags & RC_OTF_FONT) fhdr->font_type = 0;
				else fhdr->font_type = 0x80000000;

				fhdr->max_size = 1;
				
				fhdr->cff_header_offset = sizeof(Header) + sizeof(TTF::Header);
				fhdr->map_offset = sizeof(Header) + sizeof(TTF::Header) + sizeof(CFF::Header);
				
				xfont.Release();
				fhdr = 0;

				
				result = TTF::Create(xfont, src_ptr, src_size);				

			}
		} else {
			result = ERROR_OUTOFMEMORY;
		}
		
		if (!result) listid = AddFont(xfont, fname);

	}
	
	return listid;
}



unsigned int OpenGL::Font::AddFont(Font & xfont, const wchar_t * fname) {
	UI_64 result(0);
	unsigned int listid(-1);

	UI_64 flen = wcslen(fname);
	if ((_fna_top-_fna_runner) <= (flen+4)) {
		result = fname_list.Expand((flen+4)*sizeof(unsigned short));
		if (result != -1) _fna_top += (flen+4);
								
	}

	if (_fli_runner >= _fli_top) {
		result |= font_list.Expand(16*sizeof(SFSco::Object));
		if (result != -1) _fli_top += 16;
	}

	if (result != -1) {
		if (unsigned short * fli = reinterpret_cast<unsigned short *>(fname_list.Acquire())) {
			fli += _fna_runner;
						
			fli[0] = (_fli_runner & 0x00FF) | 0xFF00;
			fli[1] = (_fli_runner & 0xFF00) | 0x00FF;

			listid = _fli_runner;
									
			System::MemoryCopy(fli+2, fname, (flen+1)*sizeof(unsigned short));
											
			_fna_runner += (flen+2);

			fname_list.Release();
		}

		if (SFSco::Object * flipo = reinterpret_cast<SFSco::Object *>(font_list.Acquire())) {
			flipo[_fli_runner++] = xfont;

			font_list.Release();
		}

	}

	return listid;
}

UI_64 OpenGL::Font::RangeAvailable(SFSco::Object & s_obj) {
	UI_64 result(0), t_count(0);
	wchar_t * line_ptr(0);

	if (s_obj.Acquire(reinterpret_cast<void **>(&line_ptr))) {
		if (Header * fhdr = reinterpret_cast<Header *>(Acquire())) {
			SegmentRecord * aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

			t_count = reinterpret_cast<unsigned int *>(line_ptr)[0] + 2;

			for (unsigned int si(2);si<t_count;si++) {
				result = 0;

				for (unsigned int i(0);i<fhdr->asegment_count;i++) {
					for (unsigned int ri(0);ri<aseg_po[i].r_count;ri++) {
						if ((aseg_po[i].start_code[ri] <= line_ptr[si]) && (line_ptr[si] <= aseg_po[i].end_code[ri])) {
							
							result = -1;
							break;
						}
					}
					
					if (result == -1) break;
				}

				if (!result) break;
			}

			Release();
		}		
			
		s_obj.Release(line_ptr);
	}




	return result;
}

UI_64 OpenGL::Font::InitializeRange(SFSco::Object & s_obj) {
	Header * fhdr(0);

	wchar_t * line_ptr(0);
	unsigned int t_count(0);
	SegmentSet sese;
	SFSco::Object tempo;

	LARGE_INTEGER fsize;

	HANDLE fhandle(INVALID_HANDLE_VALUE), file_map(0);

	SegmentRecord * aseg_po(0);
	UI_64 result(0), sind(0); // 

	unsigned short * end_code(0), * start_code(0), * id_delta(0), * id_range_offset(0), * index_array(0);
	wchar_t selector(0);

	unsigned short ecode(0), scode(0), glyph_count(0);

	sese.src_ptr = 0;


	if (!special_tid) SpecialInit();
	
	if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
		if ((fhdr->font_type & FLAG_COMPLETE_FONT) == 0) {
			fhandle = CreateFile(fhdr->font_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
			if (fhandle != INVALID_HANDLE_VALUE) {
				if (GetFileSizeEx(fhandle, &fsize))
				if (file_map = CreateFileMapping(fhandle, 0, PAGE_READONLY, 0, 0, 0)) {
					sese.src_ptr = reinterpret_cast<const unsigned char *>(MapViewOfFile(file_map, FILE_MAP_READ, 0, 0, 0));
					sese.src_size = fsize.QuadPart;
				}
			}
		}
		Release();
		fhdr = 0;
	}

	if (sese.src_ptr) {
		if (s_obj.Acquire(reinterpret_cast<void **>(&line_ptr))) {
			t_count = reinterpret_cast<unsigned int *>(line_ptr)[0];
			s_obj.Release(line_ptr);
		}

		if (t_count)
		for (;;sind++) {
			glyph_count = 0; 
			sese.of_runner = sese.cu_runner = sese.cu_top = sese.po_runner = sese.po_top = 0;

			if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
				aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);
				
				line_ptr = 0;
				if (s_obj.Acquire(reinterpret_cast<void **>(&line_ptr))) {
					for (;sind<t_count;sind++) {					
						selector = line_ptr[sind+2];
				
						result = sind;
								
						for (unsigned int i(0);i<fhdr->asegment_count;i++) {
							for (unsigned int ri(0);ri<aseg_po[i].r_count;ri++) {
								if ((aseg_po[i].start_code[ri] <= selector) && (selector <= aseg_po[i].end_code[ri])) {
							
									result = -1;
									break;
								}
							}
							if (result == -1) break;
						}						

						if (result != -1) break;
					}

					if (sind >= t_count) sind = -1;

					s_obj.Release(line_ptr);
									

					if ((result != -1) && (sind != -1)) {
						TTF::Header * ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);

						end_code = reinterpret_cast<unsigned short *>(reinterpret_cast<char *>(fhdr)+fhdr->map_offset);
						start_code = end_code + ttf_header->map_table_header.segment_count + 1;
						id_delta = start_code + ttf_header->map_table_header.segment_count;
						id_range_offset = id_delta + ttf_header->map_table_header.segment_count;
						index_array = id_range_offset + ttf_header->map_table_header.segment_count;
				
						sese.first_glyf_index = 0;
						sese.glyf_index_offset = -1;

						for (unsigned short i(0);i<ttf_header->map_table_header.segment_count;i++) {
							if (end_code[i] >= selector) {
								if (start_code[i] <= selector) {
									if (id_range_offset[i]) {								
										sese.glyf_index_offset = fhdr->map_offset + (3*ttf_header->map_table_header.segment_count + 1 + i)*sizeof(unsigned short) + id_range_offset[i];
									} else {
										sese.first_glyf_index = start_code[i] + id_delta[i];
									}

									ecode = end_code[i];
									scode = start_code[i];

									glyph_count = end_code[i] - start_code[i] + 1;
								}

								break;
							}
						}				
					}			
				}
				Release();
				fhdr = 0;
			}

			if (sind == -1) break;
		

			if (result != -1) {
				result = 0;
	// collect curves
				if ((glyph_count) && (sese.src_ptr)) {
					if (sese.offset_block.New(font_offsets_tid, (glyph_count+4)*8*sizeof(unsigned int), font_pool) != -1) {
						if (sese.curve_block.New(font_curves_tid, glyph_count*4*sizeof(unsigned int), font_pool) != -1) {
							sese.cu_top = glyph_count*4;
							if (sese.point_block.New(font_nodes_tid, glyph_count*16*2*sizeof(float), font_pool) != -1) {
								sese.po_top = glyph_count*16;									
			
								for (unsigned short i(0);i<glyph_count;i++) {
									sese.more_gly = sese.flippy = 0;
									reinterpret_cast<unsigned int *>(sese.s_fac)[0] = reinterpret_cast<unsigned int *>(sese.s_fac)[1] = reinterpret_cast<unsigned int *>(sese.s_fac)[2] =
									reinterpret_cast<unsigned int *>(sese.s_fac)[3] = reinterpret_cast<unsigned int *>(sese.s_fac)[4] = reinterpret_cast<unsigned int *>(sese.s_fac)[5] = 0;

									if (result = LoadGlyf(sese)) break;
								}
							}
						}
					}
											
//		create new segment
					if (!result) {
						result = -1;
						if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
							aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);
								
							for (unsigned int i(0);i<fhdr->asegment_count;i++) {
								if (((aseg_po[i].depth + glyph_count) <  Core::max_texture_layers) && (aseg_po[i].r_count<MAX_RANGE_COUNT)) {
									result = i;
									break;
								}				
							}
					
							Release();
							fhdr = 0;
						
							if (result == -1) {
								result = Expand(sizeof(SegmentRecord)); // do expand
			
								if (result != -1) {
									if (fhdr = reinterpret_cast<Header *>(Acquire())) {
										SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

										result = fhdr->asegment_count++;

				//						System::MemoryFill(seg_rec + result, sizeof(SegmentRecord), 0);

										

										glGenTextures(1, &seg_rec[result].tex_id);
										GenBuffers(1, &seg_rec[result].buf_id);
					
										ActiveTexture(GL_TEXTURE0 + OpenGL::Core::glyph_rloader);
										glBindTexture(GL_TEXTURE_2D_ARRAY, seg_rec[result].tex_id);
										glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, Font::glyph_detail_level-1);
										glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
										glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
										glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
										glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
										
										for (unsigned int i(0);i<Font::glyph_detail_level;i++) {
											TexImage3D(GL_TEXTURE_2D_ARRAY, i, GL_R8, box_size[i], box_size[i], OpenGL::Core::max_texture_layers, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
											
										}

										BindBuffer(GL_ARRAY_BUFFER, seg_rec[result].buf_id);
										BufferData(GL_ARRAY_BUFFER, OpenGL::Core::max_texture_layers*4*sizeof(float), 0, GL_STATIC_DRAW);


				
										System::MemoryCopy(&seg_rec[result].glyph_off_block, &tempo, sizeof(SFSco::Object));
										System::MemoryCopy(&seg_rec[result].glyph_curve_block, &tempo, sizeof(SFSco::Object));
										System::MemoryCopy(&seg_rec[result].glyph_point_block, &tempo, sizeof(SFSco::Object));

										System::MemoryCopy(&seg_rec[result].raster_data, &tempo, sizeof(SFSco::Object));
			
										Release();
										fhdr = 0;
									}
								} else {
									sese.offset_block.Destroy();
									sese.curve_block.Destroy();
									sese.point_block.Destroy();
								}
							}


							if (result != -1) {
								if (RasterizeSegment(result, glyph_count, sese) == 0) {
									if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
										aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

										aseg_po[result].start_code[aseg_po[result].r_count] = scode;
										aseg_po[result].end_code[aseg_po[result].r_count] = ecode;

										++aseg_po[result].r_count;
										
										result = 0;

										Release();
										fhdr = 0;
									}
								} else {
									result = 5647391;
								}
							}
						}
					}
				}
				if (result) break;
			}
		}
	}
	
	if (file_map) {
		if (sese.src_ptr) UnmapViewOfFile(sese.src_ptr);
		CloseHandle(file_map);

	}

	if (fhandle != INVALID_HANDLE_VALUE) {
		CloseHandle(fhandle);	
	}

	return result;
}


UI_64 OpenGL::Font::InitializeComplete(const unsigned char * f_ptr, UI_64 ff_size) {
	SegmentSet sese;
	SFSco::Object tempo;

	SegmentRecord * aseg_po(0);
	UI_64 result(0); // 

	TTF::Header * ttf_header(0);
	Header * fhdr(0);

	unsigned short * end_code(0), * start_code(0), * id_delta(0), * id_range_offset(0), * index_array(0);
	unsigned short ecode(0), scode(0), glyph_count(0), seg_count, e_val(0);

	
	if (fhdr = reinterpret_cast<Header *>(Acquire())) {
		fhdr->font_type |= FLAG_COMPLETE_FONT;
		ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);
		seg_count = ttf_header->map_table_header.segment_count;

		Release();
		fhdr = 0;
	}


	for (unsigned short i(0);i<seg_count;i++) {
		glyph_count = 0;
		sese.of_runner = sese.cu_runner = sese.cu_top = sese.po_runner = sese.po_top = 0;
		sese.src_ptr = f_ptr;
		sese.src_size = ff_size;

		if (fhdr = reinterpret_cast<Header *>(Acquire())) {
			ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);

			end_code = reinterpret_cast<unsigned short *>(reinterpret_cast<char *>(fhdr)+fhdr->map_offset);
			start_code = end_code + ttf_header->map_table_header.segment_count + 1;
			id_delta = start_code + ttf_header->map_table_header.segment_count;
			id_range_offset = id_delta + ttf_header->map_table_header.segment_count;
			index_array = id_range_offset + ttf_header->map_table_header.segment_count;
				
			sese.first_glyf_index = 0;
			sese.glyf_index_offset = -1;

			if (id_range_offset[i]) {								
				sese.glyf_index_offset = fhdr->map_offset + (3*ttf_header->map_table_header.segment_count + 1 + i)*sizeof(unsigned short) + id_range_offset[i];
			} else {
				sese.first_glyf_index = start_code[i] + id_delta[i];
			}

			ecode = end_code[i];
			scode = start_code[i];

			glyph_count = end_code[i] - start_code[i];

			if (glyph_count) glyph_count++;
			else {
				if (sese.first_glyf_index) glyph_count++;
			}

			Release();
			fhdr = 0;
		} else {
			break;
		}

		
		result = 0;
	// collect curves
		if (glyph_count) {
			if (sese.offset_block.New(font_offsets_tid, (glyph_count+4)*8*sizeof(unsigned int), font_pool) != -1) {
				if (sese.curve_block.New(font_curves_tid, glyph_count*4*sizeof(unsigned int), font_pool) != -1) {
					sese.cu_top = glyph_count*4;
					if (sese.point_block.New(font_nodes_tid, glyph_count*16*2*sizeof(float), font_pool) != -1) {
						sese.po_top = glyph_count*16;									
			
						for (unsigned short i(0);i<glyph_count;i++) {
							sese.more_gly = sese.flippy = 0;
							reinterpret_cast<unsigned int *>(sese.s_fac)[0] = reinterpret_cast<unsigned int *>(sese.s_fac)[1] = reinterpret_cast<unsigned int *>(sese.s_fac)[2] =
							reinterpret_cast<unsigned int *>(sese.s_fac)[3] = reinterpret_cast<unsigned int *>(sese.s_fac)[4] = reinterpret_cast<unsigned int *>(sese.s_fac)[5] = 0;

							if (result = LoadGlyf(sese)) break;
						}
					}
				}
			}
											
//		create new segment
			if (!result) {
				result = -1;
				if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
					aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);
						
					for (unsigned int i(0);i<fhdr->asegment_count;i++) {
						if (((aseg_po[i].depth + glyph_count) <  Core::max_texture_layers) && (aseg_po[i].r_count<MAX_RANGE_COUNT)) {
							result = i;
							break;
						}				
					}
					
					Release();
					fhdr = 0;
						
					if (result == -1) {
						result = Expand(sizeof(SegmentRecord)); // do expand
			
						if (result != -1) {
							if (fhdr = reinterpret_cast<Header *>(Acquire())) {
								SegmentRecord * seg_rec = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

								result = fhdr->asegment_count++;

		//						System::MemoryFill(seg_rec + result, sizeof(SegmentRecord), 0);

								glGenTextures(1, &seg_rec[result].tex_id);
								GenBuffers(1, &seg_rec[result].buf_id);

								ActiveTexture(GL_TEXTURE0 + OpenGL::Core::glyph_rloader);
								glBindTexture(GL_TEXTURE_2D_ARRAY, seg_rec[result].tex_id);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, Font::glyph_detail_level-1);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

								for (unsigned int i(0);i<Font::glyph_detail_level;i++) {
									TexImage3D(GL_TEXTURE_2D_ARRAY, i, GL_R8, box_size[i], box_size[i], OpenGL::Core::max_texture_layers, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
								}

								BindBuffer(GL_ARRAY_BUFFER, seg_rec[result].buf_id);
								BufferData(GL_ARRAY_BUFFER, OpenGL::Core::max_texture_layers*4*sizeof(float), 0, GL_STATIC_DRAW);
				
								System::MemoryCopy(&seg_rec[result].glyph_off_block, &tempo, sizeof(SFSco::Object));
								System::MemoryCopy(&seg_rec[result].glyph_curve_block, &tempo, sizeof(SFSco::Object));
								System::MemoryCopy(&seg_rec[result].glyph_point_block, &tempo, sizeof(SFSco::Object));

								System::MemoryCopy(&seg_rec[result].raster_data, &tempo, sizeof(SFSco::Object));
			
								Release();
								fhdr = 0;
							}
						} else {
							sese.offset_block.Destroy();
							sese.curve_block.Destroy();
							sese.point_block.Destroy();
						}
					}


					if (result != -1) {
						if (RasterizeSegment(result, glyph_count, sese) == 0) {
							if (fhdr = reinterpret_cast<Header *>(Acquire())) {			
								aseg_po = reinterpret_cast<SegmentRecord *>(reinterpret_cast<char *>(fhdr) + fhdr->segments_offset);

								aseg_po[result].start_code[aseg_po[result].r_count] = scode;
								aseg_po[result].end_code[aseg_po[result].r_count] = ecode;

								++aseg_po[result].r_count;
										
								result = 0;

								Release();
								fhdr = 0;
							}
						} else {
							result = 5647391;
						}
					}
				}
			}
		}	
	}



	return result;
}




UI_64 OpenGL::Font::LoadGlyf(Font::SegmentSet & segset) {
	UI_64 result(0);

	unsigned short gindex(-1);
			
	unsigned int tcf(-1);

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(Acquire())) {
		TTF::Header * ttf_header = reinterpret_cast<TTF::Header *>(fhdr + 1);

		gindex = segset.first_glyf_index++;
		if (segset.glyf_index_offset != (UI_64)-1) gindex = reinterpret_cast<unsigned short *>(reinterpret_cast<char *>(fhdr)+segset.glyf_index_offset)[gindex];
		
		tcf = ttf_header->glyf_tab_off;

		Release();
		fhdr = 0;


		if (tcf != -1) {
			result = TTF::LoadGlyf(*this, segset, gindex);

		} else {
			result = CFF::LoadGlyf(*this, segset, gindex);
			
		}


		if (result == 10101) {
			if (unsigned int * t_offs = reinterpret_cast<unsigned int *>(segset.offset_block.Acquire())) {				

				t_offs[segset.of_runner*8] = gindex;

				t_offs[segset.of_runner*8+1] = segset.cu_runner; // curves offset
				t_offs[segset.of_runner*8+2] = segset.po_runner; // points offset

				reinterpret_cast<float *>(t_offs)[segset.of_runner*8+3] = 1.0f;
				t_offs[segset.of_runner*8+4] = 0;
				t_offs[segset.of_runner*8+5] = 0;
				reinterpret_cast<float *>(t_offs)[segset.of_runner*8+6] = 0.45f;
				reinterpret_cast<float *>(t_offs)[segset.of_runner*8+7] = 1.0f;
				

				++segset.of_runner;
					
				segset.offset_block.Release();
			}


			if (unsigned int * cu_po = reinterpret_cast<unsigned int *>(segset.curve_block.Acquire())) {
				cu_po += segset.cu_runner;
						
				cu_po[0] = 0;
				cu_po[1] = 0;
							
				cu_po += 2;

				segset.cu_runner += 2;

				segset.curve_block.Release();
			}

			result = 0;
		}		
	}

	return result;
}


