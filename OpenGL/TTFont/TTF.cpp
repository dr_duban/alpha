/*
** safe-fail font program reader
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\Font.h"

#include "System\SysUtils.h"



UI_64 OpenGL::TTF::Create(Font & xfont, const unsigned char * src_ptr, UI_64 src_size) {
	UI_64 result(0);
	UI_64 ival(0);
	void * templo(0);
	unsigned short xdat[16];
	unsigned int locoff(0), itval(0);

	SFSco::Object tempo, tempa;
	DirectoryEntry * directory_e(0);
	Header * ttf_header(0);

	OffsetTable off_table;

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		System::MemoryCopy(&off_table, src_ptr, sizeof(OffsetTable));

		System::Swap_16(&off_table.directory_entry_count, 4);
		
		if (off_table.directory_entry_count) {
			if (directory_e = reinterpret_cast<DirectoryEntry *>(malloc(off_table.directory_entry_count*sizeof(DirectoryEntry)))) {
				
				ttf_header = reinterpret_cast<Header *>(fhdr+1);
				itval = ttf_header->glyf_tab_off = -1;

				System::MemoryCopy(directory_e, src_ptr + sizeof(OffsetTable), off_table.directory_entry_count*sizeof(DirectoryEntry));
			
				for (unsigned short i(0);i<off_table.directory_entry_count;i++) {
					System::Swap_32(&directory_e[i].table_tag, 4);

					if (directory_e[i].table_offset > src_size) {
						result = -44;
						break;
					}

					switch (directory_e[i].table_tag) {
						case 'OS/2':
							System::MemoryCopy(&ttf_header->win_metrics, src_ptr + directory_e[i].table_offset, sizeof(OS2_Header));
						
							System::Swap_16(&ttf_header->win_metrics, 16);
							System::Swap_32(&ttf_header->win_metrics.unicode_range1, 4);
							System::Swap_16(&ttf_header->win_metrics.selection_flags, 8);
							System::Swap_32(&ttf_header->win_metrics.code_page1, 2);
						break;

						case 'maxp':
							System::MemoryCopy(&ttf_header->profile, src_ptr + directory_e[i].table_offset, sizeof(ProfileTable));
							System::Swap_16(&ttf_header->profile, sizeof(ProfileTable)>>1);
						break;
											
						case 'head':
							System::MemoryCopy(&ttf_header->header, src_ptr + directory_e[i].table_offset, sizeof(HeaderTable));

							System::Swap_32(&ttf_header->header.version, 4);
							System::Swap_16(&ttf_header->header.flags, 2);
							System::Swap_32(ttf_header->header.created, 4);
							System::Swap_16(&ttf_header->header.x_min, 9);

							fhdr->max_size = ttf_header->header.units_per_em;
						break;

						case 'name':
						{
							NameRecord * nrec(0);

							System::MemoryCopy(xdat, src_ptr + directory_e[i].table_offset, 6);
							System::Swap_16(xdat, 3);

							ival = directory_e[i].table_offset + 6;
							if (templo = realloc(templo, xdat[1]*sizeof(NameRecord))) {
								System::MemoryCopy(templo, src_ptr + ival, xdat[1]*sizeof(NameRecord));

								ival += xdat[1]*sizeof(NameRecord);

								nrec = reinterpret_cast<NameRecord *>(templo);

								for (unsigned short ll(0);ll<xdat[1];ll++) {
									if ((nrec[ll].platform_id == 0x0300) && (nrec[ll].encoding_id == 0x0100) && ((nrec[ll].lang_id == 0x0904) || (nrec[ll].lang_id == 0x0908))) {
										if (nrec[ll].name_id == NameRecord::FullName) {
											System::BSWAP_16(nrec[ll].string_length);																																		
											if ((nrec[ll].string_length>>1) >= 256) nrec[ll].string_length = 255*sizeof(wchar_t);

											if ((System::BSWAP_16(nrec[ll].string_offset) + ival + nrec[ll].string_length) > src_size) {
												result = -45;												
											} else {
												System::MemoryCopy(fhdr->font_name, src_ptr + ival + nrec[ll].string_offset, nrec[ll].string_length);
												System::Swap_16(fhdr->font_name, nrec[ll].string_length >> 1);
											}
																	
											break;
										}
									}
								}
							}
						}
						break;

						case 'loca':
							locoff = directory_e[i].table_offset;
						break;
						
						case 'cmap':
						{
							CharEncodingTable cet;
							
							System::MemoryCopy(&cet, src_ptr + directory_e[i].table_offset, sizeof(CharEncodingTable));

							System::BSWAP_16(cet.encoding_entry_count);
	
							if (templo = realloc(templo, cet.encoding_entry_count*sizeof(EncodingEntry))) {
								System::MemoryCopy(templo, src_ptr + directory_e[i].table_offset + sizeof(CharEncodingTable), cet.encoding_entry_count*sizeof(EncodingEntry));

								EncodingEntry * entry = reinterpret_cast<EncodingEntry *>(templo);

								result = 11;
	
								for (unsigned short j(0);j<cet.encoding_entry_count;j++) {
									if ((entry[j].platrform == platform_windows) && (entry[j].id == unicode_id)) {
										ttf_header->encoding_table.platrform = platform_windows;
										ttf_header->encoding_table.id = unicode_id;
										ttf_header->encoding_table.offset = System::BSWAP_32(entry[j].offset) + directory_e[i].table_offset;

										result = 0;
										break;
									}
								}
							}
						}
						break;

						case 'glyf':												
							itval = ttf_header->glyf_tab_off = directory_e[i].table_offset;
						break;

						case 'VORG':
							// post script vertical origin
						break;

						case 'CFF ': // post script font program			
							fhdr->cff_offset = directory_e[i].table_offset;
						break;

						case 'post': // post script info
						
						break;
					}

				}
			

			}

		}

		xfont.Release();
		fhdr = 0; ttf_header = 0;
	}

	if (!result) {
		result = GetMapHeader(xfont, src_ptr, src_size);

		if (!result)
		if (itval != -1) {			
			result = TTF::GetLocation(xfont, src_ptr, locoff, src_size);
		} else {
			result = CFF::Create(xfont, src_ptr, src_size);
		}
	}


	if (templo) free(templo);
	if (directory_e) free(directory_e);

	return result;
}

UI_64 OpenGL::TTF::GetMapHeader(Font & xfont, const unsigned char * src_ptr, UI_64 src_size) {
	UI_64 result(-45), xval(0);

	Header * ttf_header(0);

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		ttf_header = reinterpret_cast<Header *>(fhdr+1);

		if ((ttf_header->encoding_table.offset + sizeof(MapTableHeader)) <= src_size) {
			System::MemoryCopy(&ttf_header->map_table_header, src_ptr + ttf_header->encoding_table.offset, sizeof(MapTableHeader));
										
			if (ttf_header->map_table_header.format == map_table_format) {
				System::Swap_16(&ttf_header->map_table_header.length, 6);
			
				ttf_header->map_table_header.segment_count >>= 1;					
			
				ttf_header->location_offset = (fhdr->map_offset + ttf_header->map_table_header.length) & (~(UI_64)3);
				xval = ttf_header->map_table_header.length;

				xfont.Release();
				fhdr = 0;

				if (xfont.Expand(xval) != -1) {						
					if (fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
						ttf_header = reinterpret_cast<Header *>(fhdr+1);

						System::MemoryCopy(reinterpret_cast<char *>(fhdr)+fhdr->map_offset, src_ptr + ttf_header->encoding_table.offset + sizeof(MapTableHeader), xval);
						System::Swap_16(reinterpret_cast<char *>(fhdr)+fhdr->map_offset, xval >> 1);

						result = 0;

					}
				
				} else {
					result = ERROR_OUTOFMEMORY;
				}

			} else {
				result = 36479510;

			}
		}

		if (fhdr) xfont.Release();

	}

	return result;
}

UI_64 OpenGL::TTF::GetLocation(Font & xfont, const unsigned char * src_ptr, unsigned int locoff, UI_64 src_size) {
	UI_64 result(-67), ival(0);
	
	Header * ttf_header(0);

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		ttf_header = reinterpret_cast<Header *>(fhdr+1);

		ival = (ttf_header->profile.glyf_count+1)*sizeof(unsigned int);

		xfont.Release();
		fhdr = 0;

		if ((locoff + ival) <= src_size) {
			if (xfont.Expand(ival) != -1) {						
				
				if (fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
					ttf_header = reinterpret_cast<Header *>(fhdr+1);

					unsigned int * slo = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header->location_offset);
									
					if (ttf_header->header.location_format) {
						System::MemoryCopy(slo, src_ptr + locoff, ival);
						System::Swap_32(slo, ttf_header->profile.glyf_count+1);

					} else {						
						for (unsigned int iv(0);iv<=ttf_header->profile.glyf_count;iv++) {
							slo[iv] = (((unsigned int)src_ptr[locoff+iv*2]) << 8) | (unsigned int)src_ptr[locoff+iv*2+1];
							slo[iv] <<= 1;
						}
					}
					
					result = 0;

					for (unsigned int iv(0);iv<=ttf_header->profile.glyf_count;iv++) {
						slo[iv] += ttf_header->glyf_tab_off;
						if (slo[iv] >= src_size) {
							result = -47;
							break;
						}
					}
					
					fhdr->segments_offset = xfont.GetSize();

					
				}

			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}

		if (fhdr) xfont.Release();
	}

	return result;
}


UI_64 OpenGL::TTF::LoadGlyf(Font & xfont, Font::SegmentSet & segset, unsigned short gindex, unsigned int parent) {
	UI_64 result(0);
	unsigned int glength(0), govset(0);
			
	Font::GlyfHeader gheader;
	
	DWORD bread(0);
	bool swapped(false);

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		Header * ttf_header  = reinterpret_cast<Header *>(fhdr + 1);
		unsigned int * lloff = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header ->location_offset);

		govset = lloff[gindex];
		glength = lloff[gindex+1] - lloff[gindex];

		xfont.Release();
	}
	
	if (glength < 10) return 10101;
	
	if (parent == -1) parent = gindex;

	if ((govset + 10) > segset.src_size) {
		return -48;
	}

	System::MemoryCopy(&gheader, segset.src_ptr + govset, 10); // glength
	System::Swap_16(&gheader, 5);
						
	if (unsigned int * t_offs = reinterpret_cast<unsigned int *>(segset.offset_block.Acquire())) {
		int ii(segset.of_runner);
		if (segset.of_runner) if (t_offs[(segset.of_runner-1)*8] == parent) ii = segset.of_runner - 1;
												
		if (ii == segset.of_runner) {			
			t_offs[ii*8] = parent;

			t_offs[ii*8+1] = segset.cu_runner; // curves offset
			t_offs[ii*8+2] = segset.po_runner; // points offset

			t_offs[ii*8+4] = gheader.x_min;
			t_offs[ii*8+5] = gheader.y_min;
			t_offs[ii*8+6] = gheader.x_max - gheader.x_min;
			t_offs[ii*8+7] = gheader.y_max - gheader.y_min;
				

			if ((t_offs[ii*8+6] == 0) || (t_offs[ii*8+7] == 0)) result = 10101;
			else ++segset.of_runner;
		}
					
		segset.offset_block.Release();
	}

	if (result) return result;
	
	if (gheader.contour_count < 0) {
			// composite
		const unsigned char * comp_vals = segset.src_ptr + govset + 10;
		unsigned short cflags(0);
					
		do {
			segset.s_fac[0] = segset.s_fac[1] = segset.s_fac[2] = segset.s_fac[3] = segset.s_fac[4] = segset.s_fac[5] = 0.0;

			cflags = ((unsigned short)comp_vals[0]<<8) | comp_vals[1];
			govset = ((unsigned short)comp_vals[2]<<8) | comp_vals[3];
			comp_vals += 4;

			segset.p_matched = segset.p_match = -1;
				

			if (cflags & XY_VALS) {
				if (cflags & AA_WORDS) {
					segset.s_fac[4] = static_cast<short>((unsigned short)comp_vals[0]<<8 | comp_vals[1]);
					segset.s_fac[5] = static_cast<short>((unsigned short)comp_vals[2]<<8 | comp_vals[3]);

					comp_vals += 4;
				} else {
					segset.s_fac[4] = (char)comp_vals[1];
					segset.s_fac[5] = (char)comp_vals[0];

					comp_vals += 2;
				}
			} else {
				if (cflags & AA_WORDS) {
					segset.p_matched = ((unsigned short)comp_vals[0]<<8) | comp_vals[1];
					segset.p_match = ((unsigned short)comp_vals[2]<<8) | comp_vals[3];

					comp_vals += 4;
				} else {
					segset.p_matched = comp_vals[1];
					segset.p_match = comp_vals[0];
					comp_vals += 2;
				}
			}

			if (cflags & SSCALE) {
				segset.s_fac[0] = segset.s_fac[3] = F2dot14(((unsigned short)comp_vals[0]<<8) | comp_vals[1]);
				comp_vals += 2;

			} else if (cflags & XY_SCALE) {
				segset.s_fac[0] = F2dot14(((unsigned short)comp_vals[0]<<8) | comp_vals[1]);
				segset.s_fac[3] = F2dot14(((unsigned short)comp_vals[2]<<8) | comp_vals[3]);

				comp_vals += 4;

			} else if (cflags & SCALE_22) {
				segset.s_fac[0] = F2dot14(((unsigned short)comp_vals[0]<<8) | comp_vals[1]);
				segset.s_fac[1] = F2dot14(((unsigned short)comp_vals[2]<<8) | comp_vals[3]);
				segset.s_fac[2] = F2dot14(((unsigned short)comp_vals[4]<<8) | comp_vals[5]);
				segset.s_fac[3] = F2dot14(((unsigned short)comp_vals[6]<<8) | comp_vals[7]);

				comp_vals += 8;
			}
												
			segset.flippy = ((segset.s_fac[0]*segset.s_fac[3] - segset.s_fac[1]*segset.s_fac[2]) < 0.0);
			if (result = LoadGlyf(xfont, segset, govset, parent)) break;

			segset.more_gly = cflags & MORE_CMPS;

		} while (segset.more_gly);
											
		if (cflags & INSTR) {


		}
	} else {
		// simple
		unsigned int poco(0), co_level(0);
		unsigned short max_ep(0), ustt(0), yoffs(0), ll(0), mm(0), x_kk(0), y_kk(0), e_just(0), pp(0);
		short current_x, current_y;
		const unsigned char * end_points = segset.src_ptr + govset + 10;
		unsigned char ffval(2), mmval(0);

		for (unsigned short jj(0);jj<gheader.contour_count;jj++) {
			ustt = ((unsigned short)end_points[2*jj]<<8) | end_points[2*jj+1];
			if (max_ep<ustt) max_ep = ustt;
		}

		ustt = ++max_ep;

		unsigned short instructions_length = ((unsigned short)end_points[gheader.contour_count*2]<<8) | end_points[gheader.contour_count*2+1];
		const unsigned char * instructions = end_points + 2*(gheader.contour_count + 1);
		const unsigned char * flags = instructions + instructions_length;
								
		ll = 0; pp = 0;
		mm = ((unsigned short)end_points[0]<<8) | end_points[1];


		for (unsigned short jj(0);;jj++) {
			if (flags[jj] & XByte) ++yoffs;
			else if (!(flags[jj] & XSame)) yoffs += 2;
						
			mmval = (flags[jj] & OnCurve);
			if ((ffval == 0) && (mmval == 0)) ++e_just;
																																				
			if (flags[jj] & Repeat) {
				if (flags[jj] & XByte) yoffs += flags[jj+1];
				else if (!(flags[jj] & XSame)) yoffs += 2*flags[jj+1];

				if (mmval == 0) e_just += flags[jj+1];
													
				ustt -= flags[++jj];

			}
						
			if (jj != mm) ffval = mmval;
			else {

				++ll;
				mm = ((unsigned short)end_points[2*ll]<<8) | end_points[2*ll+1];
				ffval = 2;
			}

			if (!--ustt) {
				ustt = jj+1;
				break;
			}
		}

		ll = 0; mm = 0;

		unsigned char current_flag(0), f_repeater(0);
		const unsigned char * x_vals = flags + ustt;
		const unsigned char * y_vals = x_vals + yoffs;
				
		current_x = 0; current_y = 0;
									
		if ((segset.cu_runner + gheader.contour_count + max_ep + e_just + 4) >= segset.cu_top) {
			if (segset.curve_block.Expand((gheader.contour_count + max_ep + e_just + 4)*sizeof(unsigned int)) != -1) {
				segset.cu_top += (gheader.contour_count + max_ep + e_just);
			} else {
				result = ERROR_OUTOFMEMORY;
			}

		}

		if ((segset.po_runner + 2*(max_ep + e_just)) >= segset.po_top) {
			if (segset.point_block.Expand((max_ep+e_just)*4*sizeof(float)) != -1) {
				segset.po_top += 2*(max_ep+e_just);
			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}

		if (!result)
		if (unsigned int * cu_po = reinterpret_cast<unsigned int *>(segset.curve_block.Acquire())) {
			cu_po += segset.cu_runner;
			
			cu_po[0] = segset.more_gly;
			cu_po[1] = gheader.contour_count;
							
			cu_po += 2;

			unsigned int poval = -1, epval(0);
			for (short ii(0);ii<gheader.contour_count;ii++) {
				epval = ((unsigned int)end_points[2*ii]<<8) | end_points[2*ii+1];
				cu_po[ii] = epval - poval;
				poval = epval;
			}

			segset.cu_runner += (2+gheader.contour_count);

			unsigned char * onoff = reinterpret_cast<unsigned char *>(cu_po+gheader.contour_count);


			if (int * po_po = reinterpret_cast<int *>(segset.point_block.Acquire())) {
					
				po_po += (segset.po_runner<<1);

				max_ep = ((unsigned short)end_points[0]<<8) | end_points[1];
				e_just = 0;

				for (unsigned short jj(0);;jj++) {
					if (!f_repeater) {
						current_flag = flags[ll++];
						if (current_flag & Repeat) f_repeater = flags[ll++];
						
					} else {
						f_repeater--;
					}

					if (current_flag & XByte) {
						if (current_flag & XSame) current_x += x_vals[x_kk++];
						else current_x -= x_vals[x_kk++];
					} else {
						if (!(current_flag & XSame)) {
							current_x += ((static_cast<short>(x_vals[x_kk])<<8) | x_vals[x_kk+1]);
							x_kk += 2;
						}
					}

					if (current_flag & YByte) {
						if (current_flag & YSame) current_y += y_vals[y_kk++];
						else current_y -= y_vals[y_kk++];
					} else {
						if (!(current_flag & YSame)) {
							current_y += ((static_cast<short>(y_vals[y_kk])<<8) | y_vals[y_kk+1]);
							y_kk += 2;
						}
					}
					
					onoff[poco] = (current_flag & OnCurve);
								
					po_po[poco*2] = current_x;
					po_po[poco*2+1] = current_y;

					PointAdjust(po_po + 2*poco, segset.s_fac);

					if (poco++) {
						if ((onoff[poco-2] == 0) && (onoff[poco-1] == 0)) {
							if (jj != (max_ep+1)) {
								onoff[poco-1] = 1;
								onoff[poco] = 0;

								SetMidPoint(po_po+2*poco-4);
								++poco;

								++cu_po[mm];
							} else {
								++e_just;
								max_ep = ((unsigned short)end_points[e_just*2]<<8) | end_points[e_just*2+1];
							}
						}

					}									

					if (poco >= (co_level+cu_po[mm])) {
						co_level += cu_po[mm];
						++mm;
					}
							
					if ((ll >= ustt) && (!f_repeater)) break;
				}

				if (segset.flippy) {
					for (unsigned int ik(0);ik<mm;ik++) {
						ContourFlip(po_po, onoff, cu_po[ik]);
						po_po += cu_po[ik]*2;
						onoff += cu_po[ik];
					}
				}
							
				segset.po_runner += co_level;
				co_level = ((co_level+3) >> 2);
				segset.cu_runner += co_level;
						
				cu_po[gheader.contour_count + co_level] = 0;
				segset.point_block.Release();
							
			}

			segset.curve_block.Release();
													
		}

	}
		
	

	if (!result)
	if (parent == gindex) {
		if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {

			if (unsigned int * t_offs = reinterpret_cast<unsigned int *>(segset.offset_block.Acquire())) {
				t_offs += (segset.of_runner-1)*8;

				glength = segset.po_runner - t_offs[2];

				if (int * po_po = reinterpret_cast<int *>(segset.point_block.Acquire())) {

					po_po += (t_offs[2]<<1);

					t_offs[3] = t_offs[6];
					if (t_offs[3] < t_offs[7]) t_offs[3] = t_offs[7];

					t_offs[3] += ((t_offs[3]<<1)+Font::lowest_level_size-1)/Font::lowest_level_size;

					NormalizeGlyph(t_offs, po_po, glength);

					reinterpret_cast<float *>(t_offs)[3] /= (float)fhdr->max_size;

					segset.point_block.Release();
				}

				segset.offset_block.Release();
			}

			xfont.Release();
		}
	}


	return result;

}


