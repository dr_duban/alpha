/*
** safe-fail font program reader
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\Font.h"

#include "System\SysUtils.h"


const wchar_t * OpenGL::CFF::std_strings[] = {
														L".notdef", L"space", L"exclam", L"quotedbl", L"numbersign", L"dollar", L"percent", L"ampersand", L"quoteright", L"paenleft",
														L"parenright", L"asterisk", L"plus", L"comma", L"hyphen", L"period", L"slash", L"zero", L"one", L"two",
														L"three", L"four", L"five", L"six", L"seven", L"eight", L"nine", L"colon", L"semicolon", L"less",
														L"equal", L"greater", L"quastion", L"at", L"A", L"B", L"C", L"D", L"E", L"F",
														L"G", L"H", L"I", L"J", L"K", L"L", L"M", L"N", L"O", L"P",
														L"Q", L"R", L"S", L"T", L"U", L"V", L"W", L"X", L"Y", L"Z",
														L"bracketleft", L"backslash", L"bracketright", L"asciicircum", L"underscore", L"quoteleft", L"a", L"b", L"c", L"d",
														L"e", L"f", L"g", L"h", L"i", L"j", L"k", L"l", L"m", L"n",
														L"o", L"p", L"r", L"s", L"t", L"u", L"v", L"w", L"x",
														L"y", L"z", L"braceleft", L"bar", L"braceright", L"asciitilde", L"exlamdown", L"cent", L"sterling", L"fraction",

														L"yen", L"florin", L"section", L"currency", L"quotesingle", L"quotedblleft", L"guillemotleft", L"guilsignleft", L"guilsignright", L"fi",
														L"fl", L"endash", L"dagger", L"dagegrdbl", L"preiodcenterd", L"paragraph", L"bullet", L"quotesignbase", L"quotedblbase", L"quotedblright",
														L"guillemotright", L"ellipsis", L"perthousend", L"questiondown", L"grave", L"acute", L"circumflex", L"tilde", L"macron", L"breve",
														L"dotaccent", L"dieresis", L"ring", L"cedills", L"hangarumlaut", L"ogonek", L"caron", L"emdash", L"AE", L"ordfeminine",
														L"Lslash", L"Oslash", L"OE", L"ordmasculine", L"ae", L"dotlessi", L"lslash", L"oslash", L"oe", L"germadbls",
														L"onesuperior", L"logicalnot", L"mu", L"trademark", L"Eth", L"onehalf", L"plusminus", L"Thorn", L"onequarter", L"divide",
														L"brokenbar", L"degree", L"thorn", L"threequarters", L"twosuperior", L"registered", L"minus", L"eth", L"multiply", L"threesuperior",
														L"copyright", L"Aacute", L"Acircumflex", L"Adieresis", L"Agrave", L"Aring", L"Atilde", L"Ccedilla", L"Eacute", L"Ecircumflex",
														L"Edieresis", L"Egrave", L"lacute", L"lcircumflex", L"ldieserir", L"lgrave", L"Ntilde", L"Oacute", L"Ocircumflex", L"Odieresis",
														L"Ograve", L"Otilde", L"Scaron", L"Uacute", L"Ucircumflex", L"Udieresis", L"Ugrave", L"Yacute", L"Ydieresis", L"Zcaron",

														L"aacute", L"acircumflex", L"adieresis", L"agrave", L"aring", L"atilde", L"ccedilla", L"eacute", L"ecircumflex", L"edieresis",
														L"egrave", L"iacute", L"icircumflex", L"idieresis", L"igrave", L"ntilde", L"oacute", L"ocircumflex", L"odieresis", L"ograve",
														L"otilde", L"scaron", L"uacute", L"ucircumflex", L"udieresis", L"ugrave", L"yacute", L"ydieresis", L"zcaron", L"exclamsmall",
														L"Hungarumlautsmall", L"dollaroldstyle", L"dollarsuperior", L"ampersandsmall", L"Acutesmall", L"parenleftsuperior", L"parenrightsuperior", L"twodotenleader", L"onedotenleader", L"zerooldstyle",
														L"oneoldstyle", L"twooldstyle", L"threeoldstyle", L"fouroldstyle", L"fiveoldstyle", L"sixoldstyle", L"sevenoldstyle", L"eightoldstyle", L"eightoldstyle", L"eightoldstyle", L"nineoldstyle", L"commasuperior",
														L"threequartersemdash", L"periodsuperior", L"questionsamll", L"asuperior", L"bsuperior", L"centcuperior", L"dsuperior", L"esuperior", L"centsuperior", L"dsuperior", L"esuperior", L"isuperior", L"lsuperior",
														L"msuperior", L"nsuperior", L"osuperior", L"rsuperior", L"ssuperior", L"tsuperior", L"ff", L"ffi", L"ffl", L"parenleftinferior",
														L"parenrightinferior", L"Circumflexsmall", L"hyphensuperior", L"gravesmall", L"asmall", L"Bsmall", L"Csmall", L"Dsmall", L"Esmall", L"Fsmall",
														L"Gsmall", L"Hsmall", L"Ismall", L"Jsmall", L"Ksmall", L"Lsmall", L"Msamll", L"Nsmall", L"Osmall", L"Psmall",
														L"Qsmall", L"Rsamll", L"Ssmall", L"Tsmall", L"Usmall", L"Vsmall", L"Wsmall", L"Xsmall", L"Ysmall", L"zsmall",

														L"colonmonetary", L"onefitted", L"rupiah", L"Tildesmall", L"exclamdownsmall", L"centoldstyle", L"Lslashsmall", L"Scaronsmall", L"Zcaronsmall", L"Dieresissmall",
														L"Brevesmall", L"Caronsmall", L"Dotaccentsmall", L"figuredash", L"hypheninferior", L"Ogoneksmall", L"Ringsmall", L"Cedillasmall", L"questiondownsmall",
														L"oneeighths", L"threeeoghths", L"fiveeighths", L"seveneighths", L"onethird", L"twothirds", L"zerosuperior", L"foursuperior", L"fivesuperior", L"sixsuperior",
														L"sevensuperior", L"eightsuperior", L"ninesuperior", L"zeroinferior", L"oneinferior", L"twoinferior", L"threeinferior", L"fourinferior", L"fiveinferior", L"sixinferior",
														L"seveninferior", L"eightinferior", L"nineinferior", L"centinferior", L"dollarinferior", L"periodinferior", L"commainferior", L"Agravesmall", L"Aacutesmall", L"Acircumflexsmall",
														L"Atildesmall", L"Adieresissmall", L"Ariongsmall", L"AEsmall", L"Ccedillasmall", L"Egravesmall", L"Eacutesmall", L"Ecircumflexsmall", L"Edieresissmall", L"Igravesmall",
														L"Iacutesmall", L"Icircumflexsmall", L"Idieresissmall", L"Ethsmall", L"Ntildesmall", L"Ogravesmall", L"Oacutesmall", L"Oacutesmall", L"Ocircumflexsmall", L"Otildesmall", L"Odieresissmall",
														L"OEsmall", L"Oslashsmall", L"Ugravesmall", L"Uacutesmall", L"Ucircumflexsmall", L"Udieresissmall", L"Yacutesmall", L"Thornsmall", L"Ydieresissmall", L"001.000",
														L"001.001", L"001.002", L"001.003", L"Black", L"Bold", L"Book", L"Light", L"Medium", L"Regular", L"Roman",
														L"Semibold"

													};



const char OpenGL::CFF::float_codes[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'E', 'F', 'G', '-', 0};


void OpenGL::CFF::FormatOffsets(unsigned int * off_array, const unsigned char * src_ptr, unsigned short acount, unsigned char atype) {
	unsigned short s_off(0);

	for (unsigned short i(0);i<=acount;i++) {
		s_off = 0;
		off_array[i] = 0;

		switch (atype) {
			case 4:
				off_array[i] |= src_ptr[i*atype + s_off++];
				off_array[i] <<= 8;
			case 3:
				off_array[i] |= src_ptr[i*atype + s_off++];
				off_array[i] <<= 8;
			case 2:
				off_array[i] |= src_ptr[i*atype + s_off++];
				off_array[i] <<= 8;
			case 1:
				off_array[i] |= src_ptr[i*atype + s_off];
			break;			
				
		}

		--off_array[i];
	}

}

unsigned int OpenGL::CFF::DecodeVal(const unsigned char * val_ptr, unsigned int & val_runner) {
	unsigned int result(0);

// operator
	if (val_ptr[val_runner]<22) {
		return val_ptr[val_runner++];
	}

// size 1
	if ((val_ptr[val_runner] > 31) && (val_ptr[val_runner] < 247)) {
		result = val_ptr[val_runner];
		result -= 139;

		++val_runner;

		return result;
	}

// size 2
	if ((val_ptr[val_runner] > 246) && (val_ptr[val_runner] < 251)) {
		result = (val_ptr[val_runner] - 247);
		result <<= 8;
		result += (val_ptr[val_runner+1] + 108);

		val_runner += 2;

		return result;
	}

	if ((val_ptr[val_runner] > 250) && (val_ptr[val_runner] < 255)) {
		result = 251;
		result -= val_ptr[val_runner];
		result <<= 8;
		result -= (val_ptr[val_runner+1] + 108);

		val_runner += 2;

		return result;
	}

// size 3
	if (val_ptr[val_runner] == 28) {		
		result = ((unsigned int)val_ptr[val_runner+1] << 8) | ((unsigned int)val_ptr[val_runner+2]);
		if (result & 0x8000) result |= 0xFFFF0000;

		val_runner += 3;

		return result;
	}

// size 4
	if ((val_ptr[val_runner] == 29) || (val_ptr[val_runner] == 255)) { // 255 is a fraction
		result = ((unsigned int)val_ptr[val_runner+1] << 24) | ((unsigned int)val_ptr[val_runner+2] << 16) | ((unsigned int)val_ptr[val_runner+3] << 8) | ((unsigned int)val_ptr[val_runner+4]);

		val_runner += 5;

		return result;
	}

// float
	if (val_ptr[val_runner] == 30) {
		result = val_ptr[val_runner++];	

		return result;
	}

	return -1;

}

short OpenGL::CFF::DecodeGlyphVal(const unsigned char * val_ptr, unsigned int & val_runner, short * ostack) {
	short result(0);

// operator
	if (val_ptr[val_runner]<32) {
		if (val_ptr[val_runner] == 28) {		
			result = ((unsigned int)val_ptr[val_runner+1] << 8) | ((unsigned int)val_ptr[val_runner+2]);
			if (result & 0x8000) result |= 0xFFFF0000;
			
			ostack[ostack[511]++] = result;

			val_runner += 3;
			return result;
		} else {
			ostack[ostack[511]++] = val_ptr[val_runner];
			return val_ptr[val_runner++];
		}
	}

// size 1
	if ((val_ptr[val_runner] > 31) && (val_ptr[val_runner] < 247)) {
		result = val_ptr[val_runner];
		result -= 139;

		ostack[ostack[511]++] = result;

		++val_runner;			
		return result;
	}

// size 2
	if ((val_ptr[val_runner] > 246) && (val_ptr[val_runner] < 251)) {
		result = (val_ptr[val_runner] - 247);
		result <<= 8;
		result += (val_ptr[val_runner+1] + 108);

		ostack[ostack[511]++] = result;

		val_runner += 2;
		return result;
	}

	if ((val_ptr[val_runner] > 250) && (val_ptr[val_runner] < 255)) {
		result = 251;
		result -= val_ptr[val_runner];
		result <<= 8;
		result -= (val_ptr[val_runner+1] + 108);

		ostack[ostack[511]++] = result;

		val_runner += 2;		
		return result;
	}


// size 4
	if (val_ptr[val_runner] == 255) { // fraction
		result = ((unsigned int)val_ptr[val_runner+1] << 8) | (unsigned int)val_ptr[val_runner+2];
		ostack[ostack[511]++] = result;
		
		val_runner += 5;

		return result;
	}


	return -1;

}


float OpenGL::CFF::DecodeFloat(const unsigned char * val_ptr, unsigned int & val_runner) {
	char resultstr[64];
	float result(0.0f);
	unsigned char strunner(0);
	
	for (;;) {

		if (resultstr[strunner] = float_codes[val_ptr[val_runner] >> 4]) {
			if (resultstr[strunner] == 'F') {
				resultstr[strunner] = 'E';
				resultstr[strunner+1] = '-';
				++strunner;
			}

			++strunner;

		} else {
			break;
		}

		if (resultstr[strunner] = float_codes[val_ptr[val_runner] & 0x0F]) {
			if (resultstr[strunner] == 'F') {
				resultstr[strunner] = 'E';
				resultstr[strunner+1] = '-';
				++strunner;
			}

			++strunner;

		} else {
			break;
		}
		
		if (strunner > 62) break;
		++val_runner;
	}

	++val_runner;
	resultstr[strunner] = 0;

	Strings::CStrToFloat(resultstr, result);

	return result;

}
UI_64 OpenGL::CFF::Create(Font & xfont, const unsigned char * src_ptr, UI_64 src_size) {
	UI_64 result(0);

	result = LoadHeader(xfont, src_ptr, src_size);
	if (!result) result = GetLocation(xfont, src_ptr, src_size);


	return result;
}

UI_64 OpenGL::CFF::GetLocation(Font & xfont, const unsigned char * src_ptr, UI_64 src_size) {
	UI_64 result(-49), ival(0);
	TTF::Header * ttf_header(0);
	Header * cff_header(0);

	IndexHeader i_header;

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);
		Header * cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr)+fhdr->cff_header_offset);
		
		result = fhdr->cff_offset + cff_header->char_strs_off;
		if (result < src_size) {
			src_ptr += fhdr->cff_offset + cff_header->char_strs_off;

			i_header.count = ((unsigned short)src_ptr[0] << 8) | (unsigned short)src_ptr[1];
			i_header.offSize = src_ptr[2];
			src_ptr += 3;				

			xfont.Release();
			fhdr = 0; cff_header = 0;

			ival = i_header.count;
			ival = (ival+1)*sizeof(unsigned int);

			result += i_header.count*i_header.offSize;

			if (result < src_size)
			if (xfont.Expand(ival) != -1) {
				
				if (fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
					ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);
					cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr)+fhdr->cff_header_offset);

					ttf_header->profile.glyf_count = i_header.count;
				
					unsigned int * slo = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header->location_offset);

					FormatOffsets(slo, src_ptr, i_header.count, i_header.offSize);
				
					ival = 3 + cff_header->char_strs_off + (i_header.count+1)*i_header.offSize;

					result = 0;

					for (unsigned short loco(0);loco<=i_header.count;loco++) {
						slo[loco] += ival;
						if (slo[loco] >= src_size) {
							result = -50;
							break;
						}
					}

					fhdr->segments_offset = xfont.GetSize();

					
				}

			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}
		
		if (fhdr) xfont.Release();
	}

	return result;
}



UI_64 OpenGL::CFF::LoadHeader(Font & xfont, const unsigned char * src_ptr, UI_64 src_size) {
	UI_64 result(0), src_po(0);
	IndexHeader i_header;
	
	unsigned int font_name(0);
	unsigned int * off_array(0), int_val(sizeof(IndexHeader)), font_index(0), start_po(0), dict_i(0);
	

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		Header * cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr)+fhdr->cff_header_offset);

		src_ptr += fhdr->cff_offset;
		src_po = src_ptr[2];

		if ((fhdr->font_type & 0x80000000) == 0) cff_header->charstring_type = 2;
		fhdr->font_type |= 1;

// name index
	
		i_header.count = ((unsigned short)src_ptr[src_po] << 8) | (unsigned short)src_ptr[src_po+1];
		i_header.offSize = src_ptr[src_po+2];
		src_po += 3;

		if (off_array = reinterpret_cast<unsigned int *>(malloc(4*(i_header.count+1)))) {
			FormatOffsets(off_array, src_ptr + src_po, i_header.count, i_header.offSize);
			src_po += (i_header.count+1)*i_header.offSize;

// possible multifont support
			for (unsigned short i(0);i<i_header.count;i++) {
				if (src_ptr[src_po+off_array[i]]) {

					font_index = i;
					break;
				}						
			}

			src_po += off_array[i_header.count];
		
		}



// top DICT Index
		i_header.count = ((unsigned short)src_ptr[src_po] << 8) | (unsigned short)src_ptr[src_po+1];
		i_header.offSize = src_ptr[src_po+2];
		src_po += 3;

		if (off_array = reinterpret_cast<unsigned int *>(realloc(off_array, 4*(i_header.count+1)))) {
			FormatOffsets(off_array, src_ptr + src_po, i_header.count, i_header.offSize);				
			src_po += (i_header.count+1)*i_header.offSize;

			for (unsigned short i(0);i<i_header.count;i++) {
				if (i != font_index) continue;

				dict_i = 0;								

				for (;;) {									
					int_val = DecodeVal(src_ptr + src_po, dict_i);
					if (int_val == src_ptr[src_po+dict_i-1]) {
									
						if (src_ptr[src_po + dict_i - 1] == 12) {
							switch (src_ptr[src_po + dict_i]) { // fontinfo dictionary
								case 0: // copyright

								break;
								case 1: // isFixedPitch

								break;
								case 2: // itallic angle

								break;
								case 3: // underline position

								break;
								case 4: // underline thickness

								break;
								case 5: // paint type

								break;
								case 6: // charstring type
									if (start_po != dict_i) cff_header->charstring_type = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 7: // font matrix

								break;
								case 8: // stroke width

								break;
								case 20: // synthetic Base
									font_index = DecodeVal(src_ptr + src_po, start_po);
														
								break;
								case 21: // postscript

								break;
								case 22: // basefont name

								break;
								case 23: // basefont blend

								break;
								case 30: // ROS
													
								break;
								case 31: // CID Font version

								break;
								case 32: // CID font revision

								break;
								case 33: // CID font type

								break;
								case 34: // CID Count

								break;
								case 35: // UID base

								break;
								case 36: // FD array
									if (start_po != dict_i) cff_header->fd_array_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 37: // FD select (maps GID to FD array index)
									if (start_po != dict_i) cff_header->fd_select_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 38: // font name

								break;

							}

							++dict_i;
						} else {
							switch (src_ptr[src_po + dict_i - 1]) {
								case 0: // version
								
								break;
								case 1: // notice

								break;
								case 2: // full name
									font_name = DecodeVal(src_ptr + src_po, start_po) - std_string_count;
								break;
								case 3: // family name

								break;
								case 4: // weight

								break;
								case 5: // font BBox

								break;
								case 13: // unique ID

								break;
								case 14: // XUID

								break;
								case 15: // charset (list of SID implemented in the font)
									if (start_po != dict_i) cff_header->char_set_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 16: // encoding (maps code to SID for non CID fonts)
									if (start_po != dict_i) cff_header->encoding_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 17: // char strings (glyphs decription)
									cff_header->char_strs_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 18: // private DICT size and offset
									cff_header->private_size = DecodeVal(src_ptr + src_po, start_po);
									cff_header->private_off = DecodeVal(src_ptr + src_po, start_po);
								break;
								case 30: // float val
									DecodeFloat(src_ptr + src_po, dict_i);
								break;
							}
						}
									
						start_po = dict_i;
					}

					if (dict_i >= (off_array[i+1]-off_array[i])) break;
				}

				if (i == font_index) break;
				else i = 0;
			}

			src_po += off_array[i_header.count];
		}

// strings index
		i_header.count = ((unsigned short)src_ptr[src_po] << 8) | (unsigned short)src_ptr[src_po+1];
		i_header.offSize = src_ptr[src_po+2];
		src_po += 3;

		if (off_array = reinterpret_cast<unsigned int *>(realloc(off_array, 4*(i_header.count+1)))) {
			FormatOffsets(off_array, src_ptr + src_po, i_header.count, i_header.offSize);				
			src_po += (i_header.count+1)*i_header.offSize;
				
			for (unsigned int ni(0);ni<(off_array[font_name+1]-off_array[font_name]);ni++) fhdr->font_name[ni] = src_ptr[src_po+off_array[font_name]+ni];
			fhdr->font_name[off_array[font_name+1]-off_array[font_name]] = L'\0';

			src_po += off_array[i_header.count];
		}

		

		result = cff_header->charstring_type;

		xfont.Release();
		fhdr = 0;

		if (result == 2) result = GetGlobalSubrs(xfont, src_ptr, src_po);

		result = GetLocalSubrs(xfont, src_ptr);

		
	}

	if (off_array) free(off_array);

	return result;
}



UI_64 OpenGL::CFF::GetGlobalSubrs(Font & xfont, const unsigned char * src_ptr, unsigned int str_po) {
	UI_64 result(0);
	IndexHeader i_header;
	unsigned int ival(0), * glopo(0);

	src_ptr += str_po;

	i_header.count = ((unsigned short)src_ptr[0] << 8) | (unsigned short)src_ptr[1];
	i_header.offSize = src_ptr[2];
	
	if (i_header.count) {
		ival = (i_header.count + 1)*sizeof(unsigned int);

		if (xfont.Expand(ival) != -1) {
			if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
				TTF::Header * ttf_header = reinterpret_cast<TTF::Header *>(fhdr+1);
				Header * cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr)+fhdr->cff_header_offset);

				glopo = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header->location_offset);

				cff_header->subrs.global_off = ttf_header->location_offset - fhdr->cff_header_offset;
				ttf_header->location_offset += ival;
								
				if (i_header.count < 1240) {
					cff_header->subrs.global_bias = 107;
				} else {
					if (i_header.count < 33900) {
						cff_header->subrs.global_bias = 1131;
					} else {
						cff_header->subrs.global_bias = -32768;
					}
				}
				
				FormatOffsets(glopo, src_ptr + 3, i_header.count, i_header.offSize);				
				
				ival = 3 + str_po + (i_header.count+1)*i_header.offSize;

				for (unsigned short loco(0);loco<=i_header.count;loco++) glopo[loco] += ival;

				xfont.Release();

			}
		} else {
			result = ERROR_OUTOFMEMORY;
		}
	}

	return result;
}

UI_64 OpenGL::CFF::GetLocalSubrs(Font & xfont, const unsigned char * src_ptr) {
	UI_64 result(0);
	IndexHeader i_header;
	
	unsigned int start_po(0), dict_i(0), ival(0), * lopo(0);

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		TTF::Header * ttf_header = reinterpret_cast<TTF::Header *>(fhdr + 1);
		Header * cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr) + fhdr->cff_header_offset);

		if (cff_header->private_off) {
			src_ptr += cff_header->private_off;

			start_po = dict_i = 0;								

			for (;;) {
				ival = DecodeVal(src_ptr, dict_i);
				if (ival == src_ptr[dict_i-1]) {
					if (src_ptr[dict_i-1] == 12) {
						switch (src_ptr[dict_i]) { // fontinfo dictionary
							case 9: // blue scale
								
							break;
							case 10: // blue shift

							break;
							case 11: // blue fuzz

							break;
							case 12: // stem snaph

							break;
							case 13: // stem snapv

							break;
							case 14: // force bold

							break;
							case 17: // language group
						
							break;
							case 18: // expansion factor

							break;
							case 19: // initial random seed

							break;

						}

						++dict_i;
					} else {
						switch (src_ptr[dict_i-1]) {
							case 6: // blue values

							break;
							case 7: // other blues

							break;
							case 8: // family blues

							break;
							case 9: // family other blues

							break;
							case 10: // stdHW

							break;
							case 11: // stdVW

							break;
							case 19: // subrs
								if (start_po != dict_i) cff_header->subrs.local_off = DecodeVal(src_ptr, start_po);
							break;
							case 20: // defaultWidthX

							break;
							case 21: // nominalWidthX
								
							break;
							case 30: // float val
								DecodeFloat(src_ptr, dict_i);
							break;

						}
					}
										
					start_po = dict_i;
				}

				if (dict_i >= cff_header->private_size) break;
			}
		
			src_ptr += cff_header->subrs.local_off;
			cff_header->subrs.local_off += cff_header->private_off;

			i_header.count = ((unsigned short)src_ptr[0] << 8) | (unsigned short)src_ptr[1];
			i_header.offSize = src_ptr[2];


			if (i_header.count) {
				ival = (i_header.count + 1)*sizeof(unsigned int);

				xfont.Release();
				fhdr = 0;

				if (xfont.Expand(ival) != -1) {			
					if (fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
						ttf_header = reinterpret_cast<TTF::Header *>(fhdr + 1);
						cff_header = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr) + fhdr->cff_header_offset);
			
						ival = cff_header->subrs.local_off + (i_header.count + 1)*i_header.offSize + 3;
						lopo = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header->location_offset);

						cff_header->subrs.local_off = ttf_header->location_offset - fhdr->cff_header_offset;
						ttf_header->location_offset += (i_header.count + 1)*sizeof(unsigned int);
						
						FormatOffsets(lopo, src_ptr + 3, i_header.count, i_header.offSize);				
				
						for (unsigned short loco(0);loco<=i_header.count;loco++) lopo[loco] += ival;

						if (i_header.count < 1240) {
							cff_header->subrs.local_bias = 107;
						} else {
							if (i_header.count < 33900) {
								cff_header->subrs.local_bias = 1131;
							} else {
								cff_header->subrs.local_bias = -32768;
							}
						}

					}
				} else {
					result = ERROR_OUTOFMEMORY;
				}
			}
		}

		if (fhdr) xfont.Release();
	}


	return result;
}

void OpenGL::CFF::CharSetRemap(const unsigned char * src_ptr, unsigned short gco, unsigned short & gindex) {
	unsigned short glim(0), rcount(0), gcount(0);
	switch (src_ptr[0]) {
		case 0:

		break;
		case 1:
			src_ptr++;

			for (;rcount<gco;) {
				glim = (((unsigned short)src_ptr[0])<<8 | src_ptr[1]);

				if ((gindex >= glim) && (gindex <= (glim + src_ptr[2]))){
					gindex = rcount + (gindex - glim) + 1;

					break;
				}

				rcount += (src_ptr[2]+1);
				src_ptr += 3;
			}
		break;
		case 2:
			src_ptr++;

			for (;rcount<gco;) {
				glim = (((unsigned short)src_ptr[0])<<8 | src_ptr[1]);
				gcount = (((unsigned short)src_ptr[2])<<8 | src_ptr[3]);

				if ((gindex >= glim) && (gindex <= (glim + gcount))){
					gindex = rcount + (gindex - glim) + 1;

					break;
				}

				rcount += (gcount+1);
				src_ptr += 4;
			}

		break;


	}

}


UI_64 OpenGL::CFF::LoadGlyf(Font & xfont, Font::SegmentSet & segset, unsigned short gindex) {
	short operand_stack[512];
	UI_64 result(-34785);
	unsigned int pcount(0);

	GlyphRunners gglobals;
	SubrSet gly_program;
	PoSet pointers;

	Font::GlyfHeader gheader;
		

	if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
		TTF::Header * ttf_header  = reinterpret_cast<TTF::Header *>(fhdr + 1);
		Header * cff_header  = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr) + fhdr->cff_header_offset);
		
		unsigned int * lloff = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header->location_offset);

		gly_program.root_ptr = segset.src_ptr + fhdr->cff_offset;

		gly_program.cff_hdr = cff_header;
		gly_program.gindex = gindex;
		
				
		gly_program.gl_pointer = gly_program.root_ptr + lloff[gindex];
		
		gly_program.dict_i = 0;
		gly_program.glength = lloff[gindex+1] - lloff[gindex];
		
		
// run 1: build glyph header
		operand_stack[511] = 0;
		result = BuildGHeader(gheader, gglobals, gly_program, operand_stack);
				
		xfont.Release();
	}



	if (result == 1) {
		result = 0;

		if ((gheader.x_min == gheader.y_min) && (gheader.x_min == 0x7FFF)) return 10101;

		if (unsigned int * t_offs = reinterpret_cast<unsigned int *>(segset.offset_block.Acquire())) {
			t_offs[segset.of_runner*8] = gindex;

			t_offs[segset.of_runner*8+1] = segset.cu_runner; // curves offset
			t_offs[segset.of_runner*8+2] = segset.po_runner; // points offset

			t_offs[segset.of_runner*8+4] = gheader.x_min;
			t_offs[segset.of_runner*8+5] = gheader.y_min;
			t_offs[segset.of_runner*8+6] = gheader.x_max - gheader.x_min;
			t_offs[segset.of_runner*8+7] = gheader.y_max - gheader.y_min;

			if ((t_offs[segset.of_runner*8+6] == 0) || (t_offs[segset.of_runner*8+7] == 0)) result = 10101;
			else ++segset.of_runner;
								
			segset.offset_block.Release();
		}

		if (result) return result;
// expand storage
		if ((segset.cu_runner + gheader.contour_count + gglobals.epkye + 4) >= segset.cu_top) {
			if (segset.curve_block.Expand((gheader.contour_count + gglobals.epkye + 4)*sizeof(unsigned int)) != -1) {
				segset.cu_top += (gheader.contour_count + gglobals.epkye);
			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}

		if ((segset.po_runner + 2*gglobals.epkye) >= segset.po_top) {
			if (segset.point_block.Expand(gglobals.epkye*4*sizeof(float)) != -1) {
				segset.po_top += 2*gglobals.epkye;
			} else {
				result = ERROR_OUTOFMEMORY;
			}
		}

		pcount = gglobals.epkye;
		gglobals.epkye = gglobals.hint_count = 0;
		gglobals.current_x = gglobals.current_y = gglobals.first_x = gglobals.first_y = 0;

	// run 2: build glyph
		if (!result)
		if (Font::Header * fhdr = reinterpret_cast<Font::Header *>(xfont.Acquire())) {
			TTF::Header * ttf_header  = reinterpret_cast<TTF::Header *>(fhdr + 1);
			Header * cff_header  = reinterpret_cast<Header *>(reinterpret_cast<char *>(fhdr) + fhdr->cff_header_offset);
		
			unsigned int * lloff = reinterpret_cast<unsigned int *>(reinterpret_cast<char *>(fhdr) + ttf_header ->location_offset);
			
			gly_program.cff_hdr = cff_header;
			gly_program.gindex = gindex;
			gly_program.root_ptr = segset.src_ptr + fhdr->cff_offset;
				
			gly_program.gl_pointer = gly_program.root_ptr + lloff[gindex];

			gly_program.dict_i = 0;
			gly_program.glength = lloff[gindex+1] - lloff[gindex];

			
			if (unsigned int * t_offs = reinterpret_cast<unsigned int *>(segset.offset_block.Acquire())) {
				t_offs += (segset.of_runner-1)*8;

				if (pointers.p_counts = reinterpret_cast<unsigned int *>(segset.curve_block.Acquire())) {
					pointers.p_counts += t_offs[1];
				
					pointers.p_counts[0] = 0;
					pointers.p_counts[1] = gheader.contour_count;
					pointers.p_counts += 2;
							
					segset.cu_runner += (2+gheader.contour_count);

					pointers.onoff = reinterpret_cast<unsigned char *>(pointers.p_counts+gheader.contour_count);

					if (pointers.points = reinterpret_cast<int *>(segset.point_block.Acquire())) {
						int * po_po(0);
						pointers.points += (t_offs[2]<<1);
						po_po = pointers.points;

						operand_stack[511] = 0;
						CollectGCurves(pointers, gglobals, gly_program, operand_stack);

						segset.po_runner += pcount;
						segset.cu_runner += ((pcount+3) >> 2);												
						
						pointers.p_counts[((pcount+3) >> 2)] = 0;

// normalize curves and boxes
						t_offs[3] = t_offs[6];
						if (t_offs[3] < t_offs[7]) t_offs[3] = t_offs[7];

						t_offs[3] += ((t_offs[3]<<1)+Font::lowest_level_size-1)/Font::lowest_level_size;

						NormalizeGlyph(t_offs, po_po, pcount);

						reinterpret_cast<float *>(t_offs)[3] /= (float)fhdr->max_size;

						result = 0;
						segset.point_block.Release();
					}
					segset.curve_block.Release();
				}

				segset.offset_block.Release();
			}
			
			xfont.Release();
		}

	} else {
		result = 1268423;
	}

	return result;
}

void OpenGL::CFF::FDSelect(SubrSet & current_subr, unsigned short sub_index) {
	const unsigned char * fd_ptr(0);
	IndexHeader i_header;
	unsigned int fd_index(-1), fd_offset(0), s_off(0), fd_size(0), ival(0);
	unsigned short nrng(0);

	if ((current_subr.cff_hdr->fd_select_off) && (current_subr.cff_hdr->fd_array_off)) {
		fd_ptr = current_subr.root_ptr + current_subr.cff_hdr->fd_select_off;

		switch (fd_ptr[0]) {
			case 0:
				fd_index = fd_ptr[current_subr.gindex+1];
			break;
			case 3:
				nrng = ((unsigned short)fd_ptr[1]<<8) | (unsigned short)fd_ptr[2];

				fd_ptr += 3;

				for (unsigned short ri(1);ri<=nrng;ri++) {
					if (current_subr.gindex < (((unsigned short)fd_ptr[ri*3]<<8) | (unsigned short)fd_ptr[ri*3+1])) {
						fd_index = ri-1;
						break;
					}
				}

				if (fd_index != -1) {
					fd_index = fd_ptr[fd_index*3+2];

				}
			break;

		}

		fd_ptr = current_subr.root_ptr + current_subr.cff_hdr->fd_array_off; // font dict index

		i_header.count = ((unsigned short)fd_ptr[0] << 8) | (unsigned short)fd_ptr[1];
		i_header.offSize = fd_ptr[2];

		if (fd_index < i_header.count) {
			fd_ptr += 3;

			s_off = 0;
			fd_offset = 0;

			switch (i_header.offSize) {
				case 4:
					fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
					fd_offset <<= 8;
				case 3:
					fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
					fd_offset <<= 8;
				case 2:
					fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
					fd_offset <<= 8;
				case 1:
					fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off];
				break;			
				
			}

			--fd_offset;

			fd_ptr += (i_header.count+1)*i_header.offSize + fd_offset;

			s_off = 0;

			for (;;) {									
				ival = DecodeVal(fd_ptr, s_off);
				if (ival == fd_ptr[s_off-1]) {
					if (fd_ptr[s_off-1] == 18) { // private DICT size and offset
						s_off = fd_size;

						fd_size = DecodeVal(fd_ptr, s_off);
						fd_offset = DecodeVal(fd_ptr, s_off); // from root
					
						break;
					}
					if (fd_ptr[s_off-1] == 12) ++s_off;
					fd_size = s_off;

				}
			}

			fd_ptr = current_subr.root_ptr + fd_offset; // private dict
			s_off = 0;
			fd_offset = 0;

			for (;;) {
				ival = DecodeVal(fd_ptr, s_off);
				if (ival == fd_ptr[s_off-1]) {					
					if (fd_ptr[s_off-1] == 19) {
						fd_offset = DecodeVal(fd_ptr, fd_index);

						break;
					}

					if (fd_ptr[s_off-1] == 12) ++s_off;										
					
					fd_index = s_off;
				}

				if (s_off >= fd_size) break;
			}

			if (fd_offset) {
				fd_ptr += fd_offset; // subr index


				i_header.count = ((unsigned short)fd_ptr[0] << 8) | (unsigned short)fd_ptr[1];
				i_header.offSize = fd_ptr[2];

				if (i_header.count < 1240) {
					sub_index += 107;
				} else {
					if (i_header.count < 33900) {
						sub_index += 1131;
					} else {
						sub_index += 32768;
					}
				}

				if (sub_index < i_header.count) {
					fd_index = sub_index;

					fd_ptr += 3;

					s_off = 0;
					fd_offset = 0;

					switch (i_header.offSize) {
						case 4:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 3:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 2:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 1:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off];
						break;			
				
					}

					--fd_offset;

					current_subr.gl_pointer = fd_ptr + (i_header.count+1)*i_header.offSize + fd_offset;
					current_subr.glength = fd_offset;


					s_off = 0;
					fd_offset = 0;
					++fd_index;

					switch (i_header.offSize) {
						case 4:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 3:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 2:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off++];
							fd_offset <<= 8;
						case 1:
							fd_offset |= fd_ptr[fd_index*i_header.offSize + s_off];
						break;			
				
					}

					--fd_offset;
					current_subr.glength = fd_offset - current_subr.glength;

				}
			}
		}
	}
}

bool OpenGL::CFF::BuildGHeader(Font::GlyfHeader & gheader, GlyphRunners & gglobals, SubrSet current_subr, short * ostack) {
	const unsigned int * subroff(0);
	int mm(0);
	short ref_x(0), ref_y(0), flaga(0), dix(0), diy(0);
	unsigned short sub_index(0);
	bool result(false);
	SubrSet local_sub(current_subr);

	for (;;) {

		DecodeGlyphVal(current_subr.gl_pointer, current_subr.dict_i, ostack);
		if (ostack[ostack[511]-1] == current_subr.gl_pointer[current_subr.dict_i-1]) {
			--ostack[511];
			mm = 0;

			if (current_subr.gl_pointer[current_subr.dict_i-1] == 12) {
				switch (current_subr.gl_pointer[current_subr.dict_i]) { // fontinfo dictionary
					case 35: case 37: // flex/flex1
						ref_x = gglobals.current_x; ref_y = gglobals.current_y;
					
						gglobals.current_x += ostack[0];
						gglobals.current_y += ostack[1];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[2];
						gglobals.current_y += ostack[3];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[4];
						gglobals.current_y += ostack[5];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;										

						gglobals.current_x += ostack[6];
						gglobals.current_y += ostack[7];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[8];
						gglobals.current_y += ostack[9];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;


						if (current_subr.gl_pointer[current_subr.dict_i] == 35) {
							gglobals.current_x += ostack[10];
							gglobals.current_y += ostack[11];

						} else {
							dix = ref_x - gglobals.current_x; diy = ref_y - gglobals.current_y;
							if (dix < 0) dix = -dix;
							if (diy < 0) diy = -diy;

							if (dix > diy) {
								gglobals.current_x += ostack[10];
								gglobals.current_y = ref_y;
							} else {
								gglobals.current_x = ref_x;
								gglobals.current_y += ostack[10];
							}

						}

						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;										

						ostack[511] = 0;
					break;

					case 34: // hflex
						gglobals.current_x += ostack[0];
						ref_y = gglobals.current_y;
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

						gglobals.current_x += ostack[1];
						gglobals.current_y += ostack[2];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[3];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

						gglobals.current_x += ostack[4];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

						gglobals.current_x += ostack[5];
						gglobals.current_y = ref_y;
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[6];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

						ostack[511] = 0;
					break;
					case 36: // hflex1
						ref_y = gglobals.current_y;

						gglobals.current_x += ostack[0];
						gglobals.current_y += ostack[1];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[2];
						gglobals.current_y += ostack[3];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[4];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

						gglobals.current_x += ostack[5];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

						gglobals.current_x += ostack[6];
						gglobals.current_y += ostack[7];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[8];
						gglobals.current_y = ref_y;
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

						ostack[511] = 0;
					break;
					

				}

				++current_subr.dict_i;
			} else {
				switch (current_subr.gl_pointer[current_subr.dict_i-1]) {
// path construction operators
					case 5: // rlineto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;
							
						}

						ostack[511] = 0;
					break;
					case 6: // hlineto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							
							if (ostack[511]<=mm) break;

							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;
														
						}

						ostack[511] = 0;
					break;
					case 7: // vlineto
						for (;ostack[511]>mm;) {
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;							

							if (ostack[511]<=mm) break;

							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
														
						}

						ostack[511] = 0;
					break;

					case 8: // rrcurveto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;										

						}

						ostack[511] = 0;
					break;
					case 26: // vvcurveto
						if (ostack[511] & 1) {
							gglobals.current_x += ostack[mm++];

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
														
						}
												

						for (;ostack[511]>mm;) {
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;											

						}

						ostack[511] = 0;
					break;
					case 27: // hhcurveto
						if (ostack[511] & 1) {
							gglobals.current_y += ostack[mm++];

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						}

						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

						}

						ostack[511] = 0;
					break;
					case 30: // vhcurveto
						if (flaga = (ostack[511] & 1)) --ostack[511];

						for (;ostack[511]>mm;) {							
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

							flaga |= 2;
							if (ostack[511]<=mm) break;

							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;
							
							flaga &= 1;
							
						}

						if (flaga & 1)
						if (flaga & 2) {
							gglobals.current_y += ostack[mm];

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						} else {
							gglobals.current_x += ostack[mm];

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;


						}

						ostack[511] = 0;
					break;
					case 31: // hvcurveto
						if (flaga = (ostack[511] & 1)) --ostack[511];

						for (;ostack[511]>mm;) {							
							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							flaga |= 2;
							if (ostack[511]<=mm) break;

							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							

							flaga &= 1;
							
						}

						if (flaga & 1)
						if (flaga & 2) {
							gglobals.current_x += ostack[mm];

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;

						} else {
							gglobals.current_y += ostack[mm];

							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						}

						ostack[511] = 0;
					break;

					case 24: // rcurveline
						ostack[511] -= 2;

						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;							
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;						

						}

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						ostack[511] = 0;
					break;
					case 25: // rlinecurve
						ostack[511] -= 6;
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];
							++gglobals.epkye;

							if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
							if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
							if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
							if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						}


						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];
						++gglobals.epkye;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						ostack[511] = 0;
					break;




// open/close path operators
					case 4: // vmoveto (move vertically)						

						if (gglobals.epkye) {
							if ((gglobals.first_y == gglobals.current_y) && (gglobals.first_x == gglobals.current_x)) --gglobals.epkye;
						}

						gglobals.first_y = gglobals.current_y += ostack[--ostack[511]];
						++gglobals.epkye;

						ostack[511] = 0;

						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						++gheader.contour_count;
						
					break;
					case 21: // rmoveto (general move)

						if (gglobals.epkye) {
							if ((gglobals.first_y == gglobals.current_y) && (gglobals.first_x == gglobals.current_x)) --gglobals.epkye;
						}

						gglobals.first_y = gglobals.current_y += ostack[--ostack[511]];
						gglobals.first_x = gglobals.current_x += ostack[--ostack[511]];
						
						++gglobals.epkye;

						ostack[511] = 0;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						if (gglobals.current_y < gheader.y_min) gheader.y_min = gglobals.current_y;
						if (gglobals.current_y > gheader.y_max) gheader.y_max = gglobals.current_y;

						++gheader.contour_count;
						
					break;
					case 22: // hmoveto (move horizontally)

						if (gglobals.epkye) {
							if ((gglobals.first_y == gglobals.current_y) && (gglobals.first_x == gglobals.current_x)) --gglobals.epkye;
						}

						gglobals.first_x = gglobals.current_x += ostack[--ostack[511]];
						++gglobals.epkye;

						ostack[511] = 0;

						if (gglobals.current_x < gheader.x_min) gheader.x_min = gglobals.current_x;
						if (gglobals.current_x > gheader.x_max) gheader.x_max = gglobals.current_x;
						
						++gheader.contour_count;
						
					break;

					case 14: // endchar
						result = true;

						if (gglobals.epkye) {
							if ((gglobals.first_y == gglobals.current_y) && (gglobals.first_x == gglobals.current_x)) --gglobals.epkye;
						}

					case 11: // return
						current_subr.dict_i = current_subr.glength;

					break;



// subroutines

					case 10: // callsubr
						local_sub.gl_pointer = 0;										


						if (current_subr.cff_hdr->subrs.local_off) {
							sub_index = ostack[--ostack[511]] + current_subr.cff_hdr->subrs.local_bias;
							subroff = reinterpret_cast<const unsigned int *>(reinterpret_cast<const char *>(current_subr.cff_hdr) + current_subr.cff_hdr->subrs.local_off);

							local_sub.gl_pointer = current_subr.root_ptr + subroff[sub_index];
							local_sub.glength = subroff[sub_index+1] - subroff[sub_index];							
							
						} else {							
							FDSelect(local_sub, ostack[--ostack[511]]);
						}

						if (local_sub.gl_pointer) {
							local_sub.dict_i = 0;
							if (BuildGHeader(gheader, gglobals, local_sub, ostack)) {
								current_subr.dict_i = current_subr.glength;
								result = true;
							}
						}

					break;
					case 29: // callgsubr
						if (current_subr.cff_hdr->subrs.global_off) {
							sub_index = ostack[--ostack[511]] + current_subr.cff_hdr->subrs.global_bias;
						
							subroff = reinterpret_cast<const unsigned int *>(reinterpret_cast<const char *>(current_subr.cff_hdr) + current_subr.cff_hdr->subrs.global_off);

							local_sub.gl_pointer = current_subr.root_ptr + subroff[sub_index];
							local_sub.glength = subroff[sub_index+1] - subroff[sub_index];
														
							local_sub.dict_i = 0;
							if (BuildGHeader(gheader, gglobals, local_sub, ostack)) {
								current_subr.dict_i = current_subr.glength;
								result = true;
							}
						}
					break;


					case 19: case 20: // fucking mask
						gglobals.hint_count += (ostack[511]>>1);
						current_subr.dict_i += ((gglobals.hint_count+7)>>3);
						ostack[511] = 0;
					break;

					case 1: case 3: case 18: case 23: // fucking hints						
						gglobals.hint_count += (ostack[511]>>1);
						ostack[511] = 0;
					break;
				}
			}
		}

		
		if (current_subr.dict_i >= current_subr.glength) break;
		
	}

	return result;
}


bool OpenGL::CFF::CollectGCurves(PoSet & pointers, GlyphRunners & gglobals, SubrSet current_subr, short * ostack) {
	const unsigned int * subroff(0);
	int mm(0);
	short ref_x(0), ref_y(0), flaga(0), dix(0), diy(0);
	unsigned short sub_index(0);
	bool result(false);
	SubrSet local_sub(current_subr);

	for (;;) {

		DecodeGlyphVal(current_subr.gl_pointer, current_subr.dict_i, ostack);
		if (ostack[ostack[511]-1] == current_subr.gl_pointer[current_subr.dict_i-1]) {
			--ostack[511];
			mm = 0;

			if (current_subr.gl_pointer[current_subr.dict_i-1] == 12) {
				switch (current_subr.gl_pointer[current_subr.dict_i]) { // fontinfo dictionary
					case 35: case 37: // flex/flex1
						ref_x = gglobals.current_x; ref_y = gglobals.current_y;
					
						gglobals.current_x += ostack[0];
						gglobals.current_y += ostack[1];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;


						gglobals.current_x += ostack[2];
						gglobals.current_y += ostack[3];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[4];
						gglobals.current_y += ostack[5];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[6];
						gglobals.current_y += ostack[7];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[8];
						gglobals.current_y += ostack[9];
						
						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;


						if (current_subr.gl_pointer[current_subr.dict_i] == 35) {
							gglobals.current_x += ostack[10];
							gglobals.current_y += ostack[11];

						} else {
							dix = ref_x - gglobals.current_x; diy = ref_y - gglobals.current_y;
							if (dix < 0) dix = -dix;
							if (diy < 0) diy = -diy;

							if (dix > diy) {
								gglobals.current_x += ostack[10];
								gglobals.current_y = ref_y;
							} else {
								gglobals.current_x = ref_x;
								gglobals.current_y += ostack[10];
							}

						}

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;
						ostack[511] = 0;

					break;

					case 34: // hflex
						gglobals.current_x += ostack[0];
						ref_y = gglobals.current_y;

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[1];
						gglobals.current_y += ostack[2];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[3];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[4];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[5];
						gglobals.current_y = ref_y;

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[6];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						ostack[511] = 0;
					break;
					case 36: // hflex1
						ref_y = gglobals.current_y;
						gglobals.current_x += ostack[0];
						gglobals.current_y += ostack[1];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[2];
						gglobals.current_y += ostack[3];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[4];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[5];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[6];
						gglobals.current_y += ostack[7];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[8];
						gglobals.current_y = ref_y;

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;
						ostack[511] = 0;

					break;
					

				}

				++current_subr.dict_i;
			} else {
				switch (current_subr.gl_pointer[current_subr.dict_i-1]) {
// path construction operators
					case 5: // rlineto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}

						ostack[511] = 0;
					break;
					case 6: // hlineto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							if (ostack[511]<=mm) break;

							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;
														
						}

						ostack[511] = 0;
					break;
					case 7: // vlineto
						for (;ostack[511]>mm;) {
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							if (ostack[511]<=mm) break;

							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;
							
						}

						ostack[511] = 0;
					break;

					case 8: // rrcurveto
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}

						ostack[511] = 0;
					break;
					case 26: // vvcurveto

						if (ostack[511] & 1) {
							gglobals.current_x += ostack[mm++];
						}

						for (;ostack[511]>mm;) {
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}

						ostack[511] = 0;
					break;
					case 27: // hhcurveto

						if (ostack[511] & 1) {
							gglobals.current_y += ostack[mm++];
						}

						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}

						ostack[511] = 0;
					break;
					case 30: // vhcurveto
						if (flaga = (ostack[511] & 1)) --ostack[511];

						for (;ostack[511]>mm;) {							
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							flaga |= 2;
							if (ostack[511]<=mm) break;

							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							flaga &= 1;
							
						}

						if (flaga & 1)
						if (flaga & 2) {
							gglobals.current_y += ostack[mm];
							pointers.points[-1] = gglobals.current_y;

						} else {
							gglobals.current_x += ostack[mm];
							pointers.points[-2] = gglobals.current_x;

						}

						ostack[511] = 0;
					break;
					case 31: // hvcurveto
						if(flaga = (ostack[511] & 1)) --ostack[511];

						for (;ostack[511]>mm;) {							
							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							flaga |= 2;
							if (ostack[511]<=mm) break;

							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							flaga &= 1;

						}

						if (flaga & 1)
						if (flaga & 2) {
							gglobals.current_x += ostack[mm];
							pointers.points[-2] = gglobals.current_x;

						} else {
							gglobals.current_y += ostack[mm];
							pointers.points[-1] = gglobals.current_y;

						}

						ostack[511] = 0;
					break;

					case 24: // rcurveline
						ostack[511] -= 2;

						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 0;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						ostack[511] = 0;

					break;
					case 25: // rlinecurve
						ostack[511] -= 6;
						for (;ostack[511]>mm;) {
							gglobals.current_x += ostack[mm++];
							gglobals.current_y += ostack[mm++];

							pointers.points[0] = gglobals.current_x;
							pointers.points[1] = gglobals.current_y;

							pointers.onoff[0] = 1;

							pointers.points += 2;
							pointers.onoff++;

							++gglobals.epkye;

						}


						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 0;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						gglobals.current_x += ostack[mm++];
						gglobals.current_y += ostack[mm++];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;
						ostack[511] = 0;
					break;




// open/close path operators
					case 4: // vmoveto (move vertically)

						if (gglobals.epkye) {
							if ((gglobals.first_x == gglobals.current_x) && (gglobals.first_y == gglobals.current_y)) {
								pointers.points -= 2;
								--pointers.onoff;

								--gglobals.epkye;
							}

							pointers.p_counts[0] = gglobals.epkye;
							pointers.p_counts++;
							gglobals.epkye = 0;
						}

						gglobals.first_y = gglobals.current_y += ostack[--ostack[511]];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;


						++gglobals.epkye;
						ostack[511] = 0;
					break;
					case 21: // rmoveto (general move)

						if (gglobals.epkye) {
							if ((gglobals.first_x == gglobals.current_x) && (gglobals.first_y == gglobals.current_y)) {
								pointers.points -= 2;
								--pointers.onoff;

								--gglobals.epkye;
							}

							pointers.p_counts[0] = gglobals.epkye;
							pointers.p_counts++;
							gglobals.epkye = 0;
						}

						gglobals.first_y = gglobals.current_y += ostack[--ostack[511]]; 
						gglobals.first_x = gglobals.current_x += ostack[--ostack[511]];
						
						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;

						ostack[511] = 0;
					break;
					case 22: // hmoveto (move horizontally)

						if (gglobals.epkye) {
							if ((gglobals.first_x == gglobals.current_x) && (gglobals.first_y == gglobals.current_y)) {
								pointers.points -= 2;
								--pointers.onoff;

								--gglobals.epkye;
							}

							pointers.p_counts[0] = gglobals.epkye;
							pointers.p_counts++;
							gglobals.epkye = 0;
						}

						gglobals.first_x = gglobals.current_x += ostack[--ostack[511]];

						pointers.points[0] = gglobals.current_x;
						pointers.points[1] = gglobals.current_y;

						pointers.onoff[0] = 1;

						pointers.points += 2;
						pointers.onoff++;

						++gglobals.epkye;
						ostack[511] = 0;
						
					break;

					case 14: // endchar
						result = true;

						if (gglobals.epkye) {
							if ((gglobals.first_x == gglobals.current_x) && (gglobals.first_y == gglobals.current_y)) {
								pointers.points -= 2;
								--pointers.onoff;

								--gglobals.epkye;
							}

							pointers.p_counts[0] = gglobals.epkye;
							pointers.p_counts++;
							gglobals.epkye = 0;
						}

					case 11: // return
						current_subr.dict_i = current_subr.glength;

					break;



// subroutines

					case 10: // callsubr
						local_sub.gl_pointer = 0;
						
						if (current_subr.cff_hdr->subrs.local_off) {
							sub_index = ostack[--ostack[511]] + current_subr.cff_hdr->subrs.local_bias;
							subroff = reinterpret_cast<const unsigned int *>(reinterpret_cast<const char *>(current_subr.cff_hdr) + current_subr.cff_hdr->subrs.local_off);

							local_sub.gl_pointer = current_subr.root_ptr + subroff[sub_index];
							local_sub.glength = subroff[sub_index+1] - subroff[sub_index];
														
						} else {
							FDSelect(local_sub, ostack[--ostack[511]]);
							
						}

						if (local_sub.gl_pointer) {
							local_sub.dict_i = 0;
							if (CollectGCurves(pointers, gglobals, local_sub, ostack)) {
								current_subr.dict_i = current_subr.glength;
								result = true;
							}
						}

					break;
					case 29: // callgsubr
						if (current_subr.cff_hdr->subrs.global_off) {
							sub_index = ostack[--ostack[511]] + current_subr.cff_hdr->subrs.global_bias;
							subroff = reinterpret_cast<const unsigned int *>(reinterpret_cast<const char *>(current_subr.cff_hdr) + current_subr.cff_hdr->subrs.global_off);

							local_sub.gl_pointer = current_subr.root_ptr + subroff[sub_index];
							local_sub.glength = subroff[sub_index+1] - subroff[sub_index];
														
							local_sub.dict_i = 0;
							if (CollectGCurves(pointers, gglobals, local_sub, ostack)) {
								current_subr.dict_i = current_subr.glength;
								result = true;
							}
						}
					break;


					case 19: case 20: // fucking mask
						gglobals.hint_count += (ostack[511]>>1);
						current_subr.dict_i += ((gglobals.hint_count+7)>>3);
						ostack[511] = 0;
					break;

					case 1: case 3: case 18: case 23: // fucking hints
						gglobals.hint_count += (ostack[511]>>1);
						ostack[511] = 0;
					break;
				}
			}
					
		}

		
		if (current_subr.dict_i >= current_subr.glength) break;
		
	}

	return result;
}


