
; safe-fail font program reader
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




	list_in			TEXTEQU <1>
	list_out		TEXTEQU <2>
	list_hit		TEXTEQU <4>
	list_bdr		TEXTEQU <128>


; EXTERN	?glyph_detail_level@Font@OpenGL@@0IB:DWORD
; EXTERN	?glyph_n_size@Font@OpenGL@@0IB:DWORD
; EXTERN	?lowest_level_size@Font@OpenGL@@0IB:DWORD


OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16

sign_mask	DWORD	80000000h, 80000000h, 80000000h, 80000000h
sign_half	DWORD	80000000h, 80000000h, 00000000h, 00000000h
float_one	DWORD	3F800000h, 3F800000h, 3F800000h, 3F800000h
sign_clear	DWORD	7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh
float_05	DWORD	3F000000h, 3F000000h, 3F000000h, 3F000000h
xmm_on		DWORD	0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh
spacer_a	DWORD	3E000000h, 3E000000h, 3E000000h, 3E000000h
vec_zt		DWORD	0BDD00000h, 3F800000h, 0BDD00000h, 3F800000h
float_07	DWORD	3F300000h, 3F300000h, 3F300000h, 3F300000h
float_03	DWORD	3EA00000h, 3EA00000h, 3EA00000h, 3EA00000h


fval_65536	DWORD	47800000h
fval_32768	DWORD	47000000h
fval_16384	DWORD	46800000h
fval_08192	DWORD	46000000h


.CODE


ALIGN 16
?Fixed@Font@OpenGL@@CAMI@Z	PROC
	MOV eax, ecx
	SAR ecx, 16
	AND eax, 0FFFFh

	MOVAPS xmm2, [sign_mask]

	CVTSI2SS xmm0, ecx
	CVTSI2SS xmm1, eax
	DIVSS xmm1, [fval_65536]

	ANDPS xmm2, xmm0
	XORPS xmm0, xmm2
	ADDSS xmm0, xmm1
	ORPS xmm0, xmm2

	RET
?Fixed@Font@OpenGL@@CAMI@Z	ENDP

ALIGN 16
?F2dot14@TTF@OpenGL@@CAMG@Z	PROC
	ROR cx, 8
	MOVSX eax, cx
	SAR eax, 14
	AND ecx, 03FFFh

	CVTSI2SS xmm0, eax
	CVTSI2SS xmm1, ecx
	DIVSS xmm1, [fval_16384]

	ADDSS xmm0, xmm1

	RET
?F2dot14@TTF@OpenGL@@CAMG@Z	ENDP


ALIGN 16
?ContourFlip@TTF@OpenGL@@CAXPEAXPEAEI@Z	PROC
	DEC r8
	MOV r9d, r8d	

align 16
	point_loop:
		MOV rax, [rcx]
		MOV r10, [rcx+r8*8]
		MOV [rcx+r8*8], rax
		MOV [rcx], r10

		ADD rcx, 8
	SUB r8d, 2
	JG point_loop


align 16
	curve_loop:
		MOV al, [rdx]
		MOV cl, [rdx+r9]
		MOV [rdx+r9], al
		MOV [rdx], cl

		INC rdx
	SUB r9d, 2
	JG curve_loop

	RET
?ContourFlip@TTF@OpenGL@@CAXPEAXPEAEI@Z	ENDP



ALIGN 16
?PointAdjust@TTF@OpenGL@@CAXPEAHPEBM@Z	PROC
	CVTPI2PS xmm0, QWORD PTR [rcx]	
	
	XORPS xmm3, xmm3
	MOVUPS xmm1, [rdx]
	MOVLPS xmm2, QWORD PTR [rdx+16]

	CMPPS xmm3, xmm1, 4
	MOVMSKPS eax, xmm3

	TEST eax, eax
	JZ notra
		MOVLHPS xmm0, xmm0
		MULPS xmm0, xmm1

		HADDPS xmm0, xmm0
	notra:

	ADDPS xmm0, xmm2

	MOVLPS QWORD PTR [rcx], xmm0

	RET
?PointAdjust@TTF@OpenGL@@CAXPEAHPEBM@Z	ENDP

ALIGN 16
?SetMidPoint@TTF@OpenGL@@CAXPEAH@Z	PROC
	MOVLPS xmm0, QWORD PTR [rcx]
	MOVLPS xmm1, QWORD PTR [rcx+8]

	ADDPS xmm0, xmm1
	MULPS xmm0, XMMWORD PTR [float_05]

	MOVLPS QWORD PTR [rcx+8], xmm0
	MOVLPS QWORD PTR [rcx+16], xmm1

	RET
?SetMidPoint@TTF@OpenGL@@CAXPEAH@Z	ENDP

ALIGN 16
?NormalizeGlyph@TTF@OpenGL@@CAXPEAIPEAHI@Z	PROC
	CVTSI2SS xmm1, DWORD PTR [rcx+12]
	MOVSS DWORD PTR [rcx+12], xmm1
	MOVSS xmm0, DWORD PTR [float_one]
	DIVSS xmm0, xmm1

	SHUFPS xmm0, xmm0, 0
	
	CVTPI2PS xmm1, QWORD PTR [rcx+16]
	MOVLHPS xmm1, xmm1

	MOV r9d, r8d
	AND r9d, 1

	SHR r8d, 1

	align 16
	norm_loop:
		MOVUPS xmm2, [rdx]
		SUBPS xmm2, xmm1
		MULPS xmm2, xmm0
		
		MOVUPS [rdx], xmm2

		ADD rdx, 16
	DEC r8d
	JNZ norm_loop

	TEST r9d, r9d
	JZ exit
		MOVLPS xmm2, QWORD PTR [rdx]
		SUBPS xmm2, xmm1
		MULPS xmm2, xmm0
		
		MOVLPS QWORD PTR [rdx], xmm2

	exit:

	CVTDQ2PS xmm1, [rcx+16]
	MULPS xmm1, xmm0
	MOVAPS [rcx+16], xmm1

	RET
?NormalizeGlyph@TTF@OpenGL@@CAXPEAIPEAHI@Z	ENDP

ALIGN 16
?NormalizeGlyph@CFF@OpenGL@@CAXPEAIPEAHI@Z	PROC
	CVTSI2SS xmm1, DWORD PTR [rcx+12]
	MOVSS DWORD PTR [rcx+12], xmm1
	MOVSS xmm0, DWORD PTR [float_one]
	DIVSS xmm0, xmm1

	SHUFPS xmm0, xmm0, 0
	
	CVTDQ2PS xmm1, [rcx+16]

	align 16
	norm_loop:
		CVTPI2PS xmm2, QWORD PTR [rdx]
		
		SUBPS xmm2, xmm1
		MULPS xmm2, xmm0
		
		MOVLPS QWORD PTR [rdx], xmm2

		ADD rdx, 8
	DEC r8d
	JNZ norm_loop

	MULPS xmm1, xmm0
	MOVAPS [rcx+16], xmm1

	RET
?NormalizeGlyph@CFF@OpenGL@@CAXPEAIPEAHI@Z	ENDP


ALIGN 16
?FormatBox@Font@OpenGL@@CAXPEAM0PEBM@Z	PROC
	XORPS xmm0, xmm0
	XORPS xmm5, xmm5
		
	MOVSS xmm1, DWORD PTR [r8-4]
	SHUFPS xmm1, xmm1, 0
	MOVUPS xmm4, [r8] ; minx, miny
	MULPS xmm4, xmm1
	MOVHLPS xmm5, xmm4 ; width, height
	
	MOVUPS xmm1, [rdx] ; offset
	MOVHLPS xmm2, xmm1 ; mask	

	MOVSS xmm3, DWORD PTR [rdx+32]
	SUBSS xmm3, xmm5
	MAXSS xmm3, xmm0
	
	ADDSS xmm1, xmm3

	MOVAPS xmm3, xmm5
	ADDPS xmm3, xmm4
		
	MAXPS xmm3, xmm2

	CMPPS xmm2, xmm0, 0
	MOVLHPS xmm2, xmm0
	MOVLHPS xmm1, xmm0
	
	MOVLHPS xmm4, xmm0
	
	SHUFPS xmm4, [vec_zt], 47h ; miny

	XORPS xmm2, [xmm_on]
	ANDPS xmm3, xmm2
	ANDPS xmm4, xmm2
	XORPS xmm2, [xmm_on]
	
	MOVLPS QWORD PTR [rdx+8], xmm3


	MOVAPS xmm0, xmm5
	SHUFPS xmm0, xmm0, 0A8h ; width
	SHUFPS xmm5, xmm5, 0F7h ; height


	MOVAPS xmm3, xmm4
	SHUFPS xmm3, xmm3, 1
	MINSS xmm3, DWORD PTR [rdx+16]
	MOVSS DWORD PTR [rdx+16], xmm3


	MOVAPS xmm3, xmm4
	ADDPS xmm3, xmm1
	MOVAPS [rcx], xmm3

	ADDPS xmm3, xmm0
	MOVAPS [rcx+16], xmm3

	ADDPS xmm4, xmm5
	ADDPS xmm4, xmm1
	MOVAPS [rcx+32], xmm4

	ADDPS xmm4, xmm0
	MOVAPS [rcx+48], xmm4


	ORPS xmm0, xmm5
	ADDPS xmm1, xmm0

	DIVPS xmm1, [spacer_a]
	ROUNDPS xmm1, xmm1, 2
	ADDPS xmm1, [float_one]
	MULPS xmm1, [spacer_a]

	ANDPS xmm1, xmm2

	MOVLPS QWORD PTR [rdx], xmm1
	


	RET
?FormatBox@Font@OpenGL@@CAXPEAM0PEBM@Z	ENDP

ALIGN 16
?RasterizeGlyph@Font@OpenGL@@CAXPEAXIPEBIPEBM@Z	PROC
	PUSH rbp

	PUSH r15
	PUSH r14
	PUSH r13
	PUSH r12

	PUSH rsi
	PUSH rdi

	PUSH rbx
		
	MOV rdi, rcx

	SUB r9, 8 ; points

	MOV ebp, edx
	AND ebp, 7FFFFFFFh
	SHR edx, 31

	XORPS xmm14, xmm14

	MOV ecx, 6 ; [?glyph_detail_level@Font@OpenGL@@0IB]
	MOV r15d, 8 ; [?lowest_level_size@Font@OpenGL@@0IB]
	SHL r15d, cl

	ADD r15, 32

	SUB rsp, r15

	MOV [rsp+24], rdx

align 16	
	glyco_loop:
		CMP DWORD PTR [r8+4], 0 ; curve count
		JNZ normal_loop
			ADD r8, 8

			MOV eax, 87360 ; [?glyph_n_size@Font@OpenGL@@0IB]
			ADD rdi, rax

			DEC ebp
			JNZ glyco_loop
			JMP exit
		normal_loop:


		MOV ecx, 6 ; [?glyph_detail_level@Font@OpenGL@@0IB]
		MOV r15d, 8 ; [?lowest_level_size@Font@OpenGL@@0IB]
		SHL r15d, cl
		

		MOV [rsp], r8		
		
		MOV [rsp+8], r9

		XORPS xmm0, xmm0
		XORPS xmm1, xmm1
		XORPS xmm2, xmm2		
			

	align 16
		level_loop:
			ROR ecx, 8

			SHR r15d, 1
			CVTSI2SS xmm7, r15d
			SHUFPS xmm7, xmm7, 0

			BSF cx, r15w
			
			MOV r9, [rsp+8]
			MOV r13, [rsp+8]

			MOV r8, [rsp]

		align 16
			glyph_loop:
				MOV r10d, [r8+4] ; curve count
				MOV edx, r10d
				LEA rsi, [r10*4]

				ADD r8, 8
			

			align 16
				contour_loop:
					MOV r12d, r15d

					cinit_loop:
						MOV BYTE PTR [rsp+r12+31], 2
						DEC r12d
					JNZ cinit_loop
					

					MOV r12d, [r8]
					MOV rax, 1
					XORPS xmm13, xmm13

					ADD r9, 8
					
				align 16
					curve_loop:					

						MOVLPS xmm0, QWORD PTR [r9] ; start point
					
						DEC r12d
						JNZ no_close_1
							XCHG r13, r9
						no_close_1:

						MOVLPS xmm1, QWORD PTR [r9+8]					
						MOVLPS xmm2, QWORD PTR [r9+8]

						SUBPS xmm2, xmm0 ; P1 - P0

						XORPS xmm3, xmm3
						XORPS xmm4, xmm4
					
						MOVAPS xmm8, [float_one]
						XORPS xmm9, xmm9
						

						JZ lineseg


						ADD r9, 8
						INC rsi
					
						CMP BYTE PTR [r8+rsi], 0
						JNZ lineseg
							DEC r12d
							JNZ no_close_2
								XCHG r13, r9
							no_close_2:

							ADDPS xmm8, [float_one]
							MOVAPS xmm9, [float_one]

							MOVLPS xmm2, QWORD PTR [r9+8]
							SUBPS xmm2, xmm1 ; P2 - P1

							MOVAPS xmm5, xmm2
							ANDPS xmm5, [sign_clear]
							ADDPS xmm4, xmm5

							JZ lineseg

							ADD r9, 8
							INC rsi
												

							CMP BYTE PTR [r8+rsi], 0
							JNZ lineseg
								DEC r12d
								JNZ no_close_3
									XCHG r13, r9
								no_close_3:

								ADDPS xmm8, [float_one]
								MOVAPS xmm9, xmm8

								MOVLPS xmm3, QWORD PTR [r9+8]
								
								SUBPS xmm3, xmm0 ; P3 - P0

								MOVAPS xmm5, xmm3
								ANDPS xmm5, [sign_clear]
								ADDPS xmm4, xmm5

								SUBPS xmm3, xmm2
								SUBPS xmm3, xmm2
								SUBPS xmm3, xmm2 ; P3 - 3*P2 + 3P1 - P0

								JZ lineseg

								ADD r9, 8
								INC rsi
													
						lineseg:
							SUBPS xmm1, xmm0 ; P1 - P0
							SUBPS xmm2, xmm1 ; P2 - 2*P1 + P0

							MOVAPS xmm5, xmm1
							ANDPS xmm5, [sign_clear]
							ADDPS xmm4, xmm5

							MULPS xmm4, xmm7
							ROUNDPS xmm4, xmm4, 2
							ADDPS xmm4, [float_one]
							HADDPS xmm4, xmm4
							ADDSS xmm4, xmm4
																
							MOVSS xmm5, [float_one]
							DIVSS xmm5, xmm4
							SHUFPS xmm5, xmm5, 0 ; delta
							CVTSS2SI r14d, xmm4 ; i_count
							INC r14d

							MULPS xmm0, xmm7
							MULPS xmm1, xmm7
							MULPS xmm2, xmm7
							MULPS xmm3, xmm7
							
							
							MULPS xmm1, xmm5 ; delta
							MULPS xmm3, xmm5
							
							MULPS xmm5, xmm5

							MULPS xmm2, xmm5
							MULPS xmm3, xmm5

							MULPS xmm1, xmm8
							MULPS xmm2, xmm9

							MOVLHPS xmm1, xmm1
							MOVLHPS xmm2, xmm2
							MOVLHPS xmm3, xmm3
							

							MOVAPS xmm4, xmm2
							ADDPS xmm4, xmm4 ; 2*delta^2

							MOVAPS xmm5, xmm3
							ADDPS xmm5, xmm3
							ADDPS xmm5, xmm3
							ADDPS xmm5, xmm5 ; 6*delta^3

							MOVAPS xmm10, xmm5

						; rasterize

							
							MOVAPS xmm13, xmm1
							ADDPS xmm13, xmm2
							ADDPS xmm13, xmm3
							
							SHUFPS xmm0, xmm13, 0E4h

							XORPS xmm8, xmm8
							XORPS xmm9, xmm9
							XORPS xmm13, xmm13

							XCHG rsi, [rsp+24]
			; general case
						align 16
							di_loop:
								ADDPS xmm0, xmm8
								ADDPS xmm0, xmm9
								ADDPS xmm0, xmm13


								MOVAPS xmm13, xmm0
								
								ROUNDPS xmm8, xmm0, 0
								MOVAPS xmm9, xmm8
								SHUFPS xmm9, xmm9, 0E5h
								
								CVTSS2SI rax, xmm8 ; x
								CVTSS2SI rbx, xmm9 ; y								
								
								ROR rcx, 32
									TEST ebx, 80000000h
									CMOVNZ ebx, ecx

									TEST eax, 80000000h
									CMOVNZ eax, ecx
								ROR rcx, 32

								SHL rbx, cl
								ADD rbx, rax


								MOV eax, r15d

								SHUFPS xmm13, xmm13, 0B1h
								
								SUBSS xmm8, xmm0 ; -x								
								SUBSS xmm9, xmm13 ; -y
								
								SHUFPS xmm8, xmm8, 0
								SHUFPS xmm9, xmm9, 0

								DIVPS xmm13, xmm0 ; y/x, x/y, dy/dx, dx/dy

								MOVHLPS xmm13, xmm13 ; dy/dx, dx/dy
								MOVAPS xmm12, xmm13
								ANDPS xmm12, [sign_clear]
											
								MOVAPS xmm11, xmm12
								SHUFPS xmm12, xmm12, 0E5h ; dx/dy, dx/dy, dy/dx, dx/dy abs

								COMISS xmm11, xmm12
								JNA xok
									SHUFPS xmm13, xmm13, 0E5h ; dx/dy
									XORPS xmm13, [sign_mask]
									
									MOVHLPS xmm8, xmm9
									MOVHLPS xmm9, xmm8
									MOVLHPS xmm8, xmm8 ; -y
									MOVLHPS xmm9, xmm9 ; -x

									XORPS xmm8, [sign_mask] ; y
									
									MOVSS xmm11, xmm12 ; dx/dy abs

									MOV rax, 1
						align 16
								xok:

								CMPSS xmm11, xmm14, 4 ; dx/dy != 0 ?
								SHUFPS xmm11, xmm11, 0

								SUB rbx, rax
								
								SHUFPS xmm13, xmm13, 0 ; k
								
								SUBPS xmm8, [float_05]
								MULPS xmm8, xmm13
								SUBPS xmm8, xmm9
								ADDSS xmm8, xmm13 ; C D
								SHUFPS xmm8, xmm8, 14h
								XORPS xmm8, [sign_half] ; -C -D D C

								MOVAPS xmm12, [float_05]
								DIVPS xmm12, xmm13
								ANDPS xmm12, xmm11
								ANDPS xmm12, [sign_clear]

								MULPS xmm13, xmm13
								ADDPS xmm13, [float_one]
								SQRTPS xmm13, xmm13
								MULPS xmm13, [float_05]
								

								SUBPS xmm13, [float_05]
								ADDPS xmm13, xmm8 ; -C - 0.5 | -D - 0.5 | A - 0.5 | B - 0.5


							; out of range correction


								MOVAPS xmm11, [float_05]

								CMPPS xmm11, xmm13, 1
								
								MOVAPS xmm8, xmm11
								SHUFPS xmm8, xmm8, 0F5h
								ANDPS xmm8, xmm11 ; double out
								MOVMSKPS r11d, xmm8
								MOVHLPS xmm11, xmm8
								ORPS xmm8, xmm11
								SHUFPS xmm8, xmm8, 0
								ANDPS xmm8, [float_one]
								ORPS xmm8, [sign_half]
								
								SHR r11b, 1
								JNC nodfo_1
									SUB rbx, rax
									ADDPS xmm13, xmm8								
								nodfo_1:
								SHR r11b, 2
								JNC nodfo_2								
									ADD rbx, rax
									SUBPS xmm13, xmm8
								nodfo_2:

								XOR r11, r11

								XORPS xmm11, xmm11
								CMPPS xmm11, xmm13, 1

																
								MOVAPS xmm8, xmm13
								HADDPS xmm8, xmm8
								MULPS xmm8, [float_05]
								ANDPS xmm8, [sign_clear] ; 0.5|1+C+D| | 0.5|A+B-1|
								SHUFPS xmm8, xmm8, 50h

								MULPS xmm13, xmm13
								MULPS xmm13, xmm12 ; (0.5+C)^2/2k | (0.5+D)^2/2k | (0.5-A)^2/2k | (0.5-B)^2/2k
								

								ROR rcx, 32
								ROR rdx, 32
								
									CMP rbx, 0
									JL no_floor
									; lower trap
										
										MOVAPS xmm9, xmm11
										SHUFPS xmm9, xmm9, 0B1h
										ANDPS xmm9, xmm11
										MOVAPS xmm12, xmm8
										ANDPS xmm12, xmm9
									
									; lower trian																	
										XORPS xmm9, [xmm_on]
										ANDPS xmm9, xmm11
										ANDPS xmm9, xmm13
																
										ORPS xmm12, xmm9
										SHUFPS xmm9, xmm9, 0E5h
										ORPS xmm12, xmm9


										COMISS xmm12, [float_03] ; not less
										JC no_floor

											OR BYTE PTR [rdi+rbx], 32
										

								align 16
									no_floor:
									ADD rbx, rax
									JL no_hit

								;	lower trap

									MOVAPS xmm9, xmm11
									SHUFPS xmm9, xmm9, 0B1h
									ORPS xmm9, xmm11
									XORPS xmm9, [xmm_on]
									MOVAPS xmm12, xmm8
									ANDPS xmm12, xmm9
								
								; higher trap
									MOVHLPS xmm9, xmm12
									ORPS xmm12, xmm9


								; trian
									MOVAPS xmm9, xmm11
									SHUFPS xmm9, xmm9, 0B1h
									XORPS xmm9, xmm11
																							
									ANDPS xmm9, xmm11
									SHUFPS xmm9, xmm9, 0B1h
									ANDPS xmm9, xmm13
								
									HADDPS xmm9, xmm9
									HADDPS xmm9, xmm9

									ADDSS xmm12, xmm9	


									COMISS xmm12, [float_07] ; less
									JNC no_hit										
										OR BYTE PTR [rdi+rbx], 32
									
										XORPS xmm12, xmm12
										MOVAPS xmm9, XMMWORD PTR [sign_clear]
										ANDPS xmm9, xmm0
										CMPPS xmm9, xmm12, 0
										MOVMSKPS r11d, xmm9
										SHR r11d, 3

										JNZ no_hit
								
											BSR cx, r15w
											MOV dx, bx
											SHR dx, cl

											MOV cx, r15w
											DEC cx

											AND cx, bx

											MOVMSKPS r11d, xmm0
											SHR r11d, 3
											XOR r11d, esi
											JZ god2
												TEST BYTE PTR [rdi+rbx], 128
												JNZ no_hit

												MOVZX r11, dx
												CMP BYTE PTR [rsp+r11+32], 1
												JZ no_hit
													MOV BYTE PTR [rsp+r11+32], 1

													MOVZX r11, cx
													INC r11

													SUB r11, r15
													JZ no_hit
														OR BYTE PTR [rdi+rbx], 128
														SUB rbx, r11
											align 16
												press_3:
													ADD BYTE PTR [rdi+rbx], 1
								;					SBB BYTE PTR [rdi+rbx], 0

													DEC rbx
												INC r11
												JNZ press_3
											JMP no_hit
									align 16
										god2:
											TEST BYTE PTR [rdi+rbx], 64
											JNZ no_hit

											MOVZX r11, dx
											CMP BYTE PTR [rsp+r11+32], 0
											JZ no_hit
												MOV BYTE PTR [rsp+r11+32], 0

												MOVZX r11, cx

												AND r11, rbx
												JZ no_hit
													OR BYTE PTR [rdi+rbx], 64
													SUB rbx, r11
											align 16
												press_4:
													ADD BYTE PTR [rdi+rbx], 1
								;					SBB BYTE PTR [rdi+rbx], 0

													INC rbx
												DEC r11d
												JNZ press_4
																
									no_hit:
										ADD rbx, rax

										MOVAPS xmm9, xmm11
										SHUFPS xmm9, xmm9, 0B1h
										ANDPS xmm9, xmm11
										ANDPS xmm8, xmm9
								
										XORPS xmm9, [xmm_on]								
										ANDPS xmm9, xmm11
										ANDPS xmm9, xmm13

										MOVHLPS xmm8, xmm8
										MOVHLPS xmm9, xmm9

										ORPS xmm8, xmm9
										SHUFPS xmm9, xmm9, 0E5h
										ORPS xmm8, xmm9
								
										COMISS xmm8, [float_03] ; not less
										JC no_ceil

											OR BYTE PTR [rdi+rbx], 32
									
									align 16
										no_ceil:

								XOR cx, cx
								XOR dx, dx

								ROR rcx, 32
								ROR rdx, 32


								MOVLHPS xmm0, xmm14
								
								MOVAPS xmm8, xmm1
								MOVAPS xmm9, xmm2
								MOVAPS xmm13, xmm3

								ADDPS xmm2, xmm4
								ADDPS xmm3, xmm5
																
								ADDPS xmm5, xmm10
								
							

							DEC r14d
							JNZ di_loop

						XCHG rsi, [rsp+24]
					CMP r12d, 0
					JNZ curve_loop

					ADD r8, 4
					SUB rsi, 3
			
					MOV r9, r13
														
				DEC edx
				JNZ contour_loop

				
				LEA r8, [r8+rsi+3]

				MOV rax, 3
				NOT rax
				AND r8, rax
			

			CMP DWORD PTR [r8], 0
			JNZ glyph_loop

; filter it			

			MOV rax, rcx



			SHR ecx, 24
			
			MOV r14d, r15d
			SHL r14d, cl
			SHL r14d, 2

			MOV rcx, rdi

		align 16
			adjust_loop:
				CMP BYTE PTR [rcx], 32
				JB no_adjust
					MOV BYTE PTR [rcx], 255
				no_adjust:
				INC rcx

			DEC r14d
			JNZ adjust_loop




			
			XOR rcx, rcx

			MOV r14d, r15d
		align 16
			fill_loop:
				XOR ax, ax
				MOV ecx, r15d
				MOV r11, rdi

			align 16
				line_loop:
					MOV r10, rdi
								
					MOV al, [rdi]
					REPZ SCASB
					JZ no_corr_1
						INC ecx
						DEC rdi
					no_corr_1:
					JECXZ next_line

					CMP al, 255
					JNZ second_run
						MOV r10, rdi
								
						MOV al, [rdi]
						REPZ SCASB
						JZ no_corr_2
							INC ecx
							DEC rdi
						no_corr_2:

						JECXZ next_line

					second_run:						
						CMP BYTE PTR [rdi], 255
						JNZ do_fill
							SHL rax, 8

							MOV al, [rdi]	
							REPZ SCASB
							JZ no_corr_3
								INC ecx
								DEC rdi
							no_corr_3:

							SHR rax, 8
							JECXZ next_line							

							CMP al, [rdi]
							JNZ do_fill
								REPZ SCASB
								JZ no_corr_4
									INC ecx
									DEC rdi
								no_corr_4:

								JECXZ next_line
								CMP BYTE PTR [rdi], 255
								JZ second_run
						do_fill:
							
							MOV ah, 1
							CMP al, [rdi]
							JB line_loop
								MOV ah, 0
							JZ line_loop
								MOV al, 255
								SUB rdi, r10								
								XCHG rdi, r10
								XCHG cx, r10w
																
								REP STOSB

								MOV cx, r10w
			JMP line_loop
		align 16
				next_line:
					TEST ah, ah
					JZ no_left_over
						MOV al, 255
						SUB rdi, r10
						XCHG rdi, r10
						XCHG cx, r10w

						REP STOSB
						
					no_left_over:


					LEA rdi, [r11+r15]
			DEC r14d
			JNZ fill_loop

			MOV rcx, rax
			SHR ecx, 24

			MOV r14d, r15d
			SHL r14d, cl
			SHL r14d, 2
			NEG r14

			XOR rax, rax

		align 16
			clear_loop:
				CMP BYTE PTR [rdi+r14], 255
				JZ no_clear
					MOV [rdi+r14], al
				no_clear:

			INC r14
			JNZ clear_loop



			MOV r14d, r15d
			SHL r14d, cl
			SHL r14d, 2

			MOV rsi, rdi
			SUB rsi, r14

			MOV r10d, r15d


		;	CMP r15d, 16
		;	JB kern_3

			SUB r10d, 2
			MOV r14d, r10d
			MOV r11d, r10d
				
		align 16
			aa_loop:
				CMP BYTE PTR [rsi+r15+1], 255
				JNZ skip_po
					XOR rax, rax

					CMP BYTE PTR [rsi], 0
					JZ no_po0
						ADD eax, 256
					no_po0:

					CMP BYTE PTR [rsi+1], 0
					JZ no_po1
						ADD eax, 256
					no_po1:

					CMP BYTE PTR [rsi+2], 0
					JZ no_po2
						ADD eax, 256
					no_po2:

					CMP BYTE PTR [rsi+r15], 0
					JZ no_po3
						ADD eax, 256
					no_po3:


					CMP BYTE PTR [rsi+r15+2], 0
					JZ no_po5
						ADD eax, 256
					no_po5:

					CMP BYTE PTR [rsi+r15*2], 0
					JZ no_po6
						ADD eax, 256
					no_po6:

					CMP BYTE PTR [rsi+r15*2+1], 0
					JZ no_po7
						ADD eax, 256
					no_po7:

					CMP BYTE PTR [rsi+r15*2+2], 0
					JZ no_po8
						ADD eax, 256
					no_po8:
				
					SHR eax, 3
					CMP eax, 256
					JB no_oflow
						MOV eax, 255
					no_oflow:

					MOV [rsi+r15+1], al

				skip_po:
					INC rsi

				DEC r14d
				JNZ aa_loop

				MOV r14d, r11d

				ADD rsi, 2
			DEC r10d
			JNZ aa_loop


		DEC cl
		JNZ level_loop


	DEC ebp
	JNZ glyco_loop

exit:
	MOV ecx, 6 ; [?glyph_detail_level@Font@OpenGL@@0IB]
	MOV r15d, 8 ; [?lowest_level_size@Font@OpenGL@@0IB]
	SHL r15d, cl

	ADD r15, 32

	ADD rsp, r15

	POP rbx

	POP rdi
	POP rsi

	POP r12
	POP r13
	POP r14
	POP r15

	POP rbp
	RET
?RasterizeGlyph@Font@OpenGL@@CAXPEAXIPEBIPEBM@Z	ENDP


END

