
; safe-fail OGL core components
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16
float_255	DWORD	437F0000h, 437F0000h, 437F0000h, 437F0000h



.CODE

ALIGN 16
?FormatElementID@Core@OpenGL@@CA_KPEBM@Z	PROC
	CVTSS2SI eax, DWORD PTR [rcx]
	CVTSS2SI edx, DWORD PTR [rcx+4]
	
	SHL edx, 16
	OR eax, edx

	ADD eax, 1
	
	SUB rax, 1	

	RET
?FormatElementID@Core@OpenGL@@CA_KPEBM@Z	ENDP

ALIGN 16
?FormatEIDs@Core@OpenGL@@CAXPEAII@Z	PROC
	XOR rax, rax
	SHR edx, 2

align 16
	convert_loop:
;		CVTPS2DQ xmm0, [rcx+rax*4]
;		CVTPS2DQ xmm1, [rcx+rax*4+16]
;		CVTPS2DQ xmm2, [rcx+rax*4+32]
;		CVTPS2DQ xmm3, [rcx+rax*4+48]
		
;		PUNPCKHDQ xmm0, xmm1
;		PUNPCKHDQ xmm2, xmm3

;		MOVDQA xmm1, xmm0
;		PUNPCKLQDQ xmm0, xmm2
;		PUNPCKHQDQ xmm1, xmm2

;		PSRLD xmm1, 16
;		POR xmm0, xmm1

;		MOVDQA [rcx+rax], xmm0

		MOV r8d, [rcx+rax*4+12]
		MOV r9d, [rcx+rax*4+28]
		MOV r10d, [rcx+rax*4+44]
		MOV r11d, [rcx+rax*4+60]

		MOV [rcx+rax], r8d
		MOV [rcx+rax+4], r9d
		MOV [rcx+rax+8], r10d
		MOV [rcx+rax+12], r11d

		ADD rax, 16

	DEC edx
	JNZ convert_loop


	

	RET
?FormatEIDs@Core@OpenGL@@CAXPEAII@Z	ENDP


ALIGN 16
?ColorConvert@Core@OpenGL@@SAXPEAMHI@Z	PROC
	MOV eax, edx
	AND eax, 255
	CVTSI2SS xmm0, eax
	
	MOV eax, edx
	SHR eax, 8
	AND eax, 255
	CVTSI2SS xmm1, eax
	UNPCKLPS xmm0, xmm1

	MOV eax, edx
	SHR eax, 16
	AND eax, 255
	CVTSI2SS xmm1, eax

	SHR edx, 24	
	CVTSI2SS xmm2, edx
	UNPCKLPS xmm1, xmm2

	UNPCKLPD xmm0, xmm1

	DIVPS xmm0, XMMWORD PTR [float_255]

align 16
	set_loop:
		MOVUPS [rcx], xmm0
		ADD rcx, 16
	DEC r8d
	JNZ set_loop

	RET
?ColorConvert@Core@OpenGL@@SAXPEAMHI@Z	ENDP


ALIGN 16
?ClearBuf@AttStruct@Core@OpenGL@@SAXPEAMPEBMI@Z	PROC
	MOVUPS xmm0, [rdx]

	copy_loop:
		MOVAPS [rcx], xmm0
		ADD rcx, 16
	DEC r8d
	JNZ copy_loop

	RET
?ClearBuf@AttStruct@Core@OpenGL@@SAXPEAMPEBMI@Z	ENDP

END

