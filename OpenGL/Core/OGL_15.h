/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#ifndef GL_TYPES

typedef size_t GLintptr, GLsizeiptr;

typedef char GLchar;
typedef signed char GLbyte;
typedef unsigned char GLubyte,GLboolean; 

typedef short int GLshort;
typedef unsigned short int GLushort;

typedef unsigned int GLuint, GLenum, GLhandle, GLbitfield;
typedef int GLsizei;

#define GL_TYPES 1
#endif


#define OPENGL_1_1_FUNC(n,a,t) extern "C" __declspec(dllimport) t __stdcall gl##n##a 

#ifndef OPENGL_FUNC_INI
	#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; extern P##n n
#endif


namespace OpenGL {


OPENGL_1_1_FUNC(GetBooleanv,(GLenum panme,GLboolean * params),void);	
OPENGL_1_1_FUNC(GetDoublev,(GLenum pname,double * params),void);		
OPENGL_1_1_FUNC(GetFloatv,(GLenum pname,float * params),void);		
OPENGL_1_1_FUNC(GetIntegerv,(GLenum pname,int * params),void);		
OPENGL_1_1_FUNC(GetError,(void),GLenum);	
OPENGL_1_1_FUNC(GetString,(GLenum name),const char *);


OPENGL_1_1_FUNC(Finish,(void),void);
OPENGL_1_1_FUNC(Flush,(void),void);

OPENGL_1_1_FUNC(BlendFunc,(GLenum sfcator,GLenum dfactor),void);		

// viewport
OPENGL_1_1_FUNC(Viewport,(int x,int y,GLsizei width,GLsizei height),void);
OPENGL_1_1_FUNC(DepthRange,(double zNear,double zFar),void);	


// primitives
OPENGL_1_1_FUNC(FrontFace,(GLenum mode),void);		
OPENGL_1_1_FUNC(CullFace,(GLenum mode),void);		

OPENGL_1_1_FUNC(PointSize,(float size),void);		
OPENGL_1_1_FUNC(LineWidth,(float width),void);		
OPENGL_1_1_FUNC(LineStipple,(int factor,GLushort pattern),void);



OPENGL_1_1_FUNC(DisableClientState,(GLenum _array),void);
OPENGL_1_1_FUNC(EnableClientState,(GLenum _array),void);

OPENGL_1_1_FUNC(IsEnabled,(GLenum cap),GLboolean);	
OPENGL_1_1_FUNC(Enable,(GLenum cap),void);		
OPENGL_1_1_FUNC(Disable,(GLenum cap),void);		

// cordiante transformation
OPENGL_1_1_FUNC(MultMatrixf,(float * m),void);
OPENGL_1_1_FUNC(Scalef,(float sx,float sy,float sz),void);
OPENGL_1_1_FUNC(Translatef,(float dx,float dy,float dz),void);

OPENGL_1_1_FUNC(Frustum,(double left,double right,double bottom,double top,double zNear,double zFar),void);
OPENGL_1_1_FUNC(Ortho,(double left,double right,double bottom,double top,double zNear,double zFar),void);	

// matrix replace and stack
OPENGL_1_1_FUNC(LoadMatrixf,(float * m),void);
OPENGL_1_1_FUNC(LoadIdentity,(void),void);

OPENGL_1_1_FUNC(MatrixMode,(GLenum mode),void);	
OPENGL_1_1_FUNC(PushMatrix,(void),void);	
OPENGL_1_1_FUNC(PopMatrix,(void),void);	

// buffer operations
OPENGL_1_1_FUNC(ClearColor,(float r,float g,float b,float a),void);
OPENGL_1_1_FUNC(ClearDepth,(double depth),void);				
OPENGL_1_1_FUNC(ClearAccum,(float r,float g,float b,float a),void);
OPENGL_1_1_FUNC(ClearIndex,(float c),void);				
OPENGL_1_1_FUNC(ClearStencil,(int s),void);				

OPENGL_1_1_FUNC(Clear,(GLbitfield mask),void);					

OPENGL_1_1_FUNC(Scissor,(int x,int y,GLsizei width,GLsizei height),void);					
OPENGL_1_1_FUNC(AlphaFunc,(GLenum func,float ref),void);				
OPENGL_1_1_FUNC(DepthFunc,(GLenum func),void);
OPENGL_1_1_FUNC(StencilFunc,(GLenum func,int ref,GLuint mask),void);	
OPENGL_1_1_FUNC(StencilMask,(GLuint mask),void);
OPENGL_1_1_FUNC(StencilOp,(GLenum fail,GLenum zfail,GLenum zpass),void);				

OPENGL_1_1_FUNC(Accum,(GLenum op,float value),void);				

// imaging
OPENGL_1_1_FUNC(RasterPos2f,(float x,float y),void);
OPENGL_1_1_FUNC(RasterPos2fv,(const float * v),void);
OPENGL_1_1_FUNC(RasterPos2i,(int x,int y),void);
OPENGL_1_1_FUNC(RasterPos2iv,(const int * v),void);
OPENGL_1_1_FUNC(RasterPos3f,(float x,float y,float z),void);
OPENGL_1_1_FUNC(RasterPos3fv,(const float * v),void);
OPENGL_1_1_FUNC(RasterPos4f,(float x,float y,float z,float w),void);
OPENGL_1_1_FUNC(RasterPos4fv,(const float * v),void);

OPENGL_1_1_FUNC(DrawPixels,(GLsizei width,GLsizei height,GLenum format,GLenum type,const void * pixels),void);	
OPENGL_1_1_FUNC(ReadPixels,(int x,int y,GLsizei width,GLsizei height,GLenum format,GLenum type,void * pixels),void);		
OPENGL_1_1_FUNC(CopyPixels,(int x,int y,GLsizei width,GLsizei height,GLenum type),void);

OPENGL_1_1_FUNC(DrawBuffer,(GLenum mode),void);		
OPENGL_1_1_FUNC(ReadBuffer,(GLenum mode),void);		

OPENGL_1_1_FUNC(PixelMapfv,(GLenum map,GLsizei mapsize,const float * values),void);		

OPENGL_1_1_FUNC(PixelStoref,(GLenum pname,float param),void);		
OPENGL_1_1_FUNC(PixelStorei,(GLenum pname,int param),void);	

OPENGL_1_1_FUNC(PixelTransferf,(GLenum pname,float param),void);		
OPENGL_1_1_FUNC(PixelTransferi,(GLenum pname,int param),void);		

OPENGL_1_1_FUNC(PixelZoom,(float xfactor,float yfactor),void);		

OPENGL_1_1_FUNC(ClipPlane,(GLenum plane,const double * equation),void);
OPENGL_1_1_FUNC(ColorMask,(GLboolean red,GLboolean green,GLboolean blue,GLboolean alpha),void);	
OPENGL_1_1_FUNC(ColorMaterial,(GLenum face,GLenum mode),void);		
OPENGL_1_1_FUNC(DepthMask,(GLboolean flag),void);
OPENGL_1_1_FUNC(EdgeFlag,(GLboolean flag),void);
OPENGL_1_1_FUNC(EdgeFlagv,(const GLboolean * flag),void);

// textures
OPENGL_1_1_FUNC(GenTextures,(GLsizei n,GLuint * textures),void);
OPENGL_1_1_FUNC(BindTexture,(GLenum target,GLuint texture),void);
OPENGL_1_1_FUNC(CopyTexImage1D,(GLenum target,int level,GLenum internalFormat,int x,int y,GLsizei width,int border),void);
OPENGL_1_1_FUNC(CopyTexImage2D,(GLenum target,int level,GLenum internalFormat,int x,int y,GLsizei width,GLsizei height,int border),void);
OPENGL_1_1_FUNC(CopyTexSubImage1D,(GLenum target,int level,int xoffset,int x,int y,GLsizei width),void);
OPENGL_1_1_FUNC(CopyTexSubImage2D,(GLenum target,int level,int xoffset,int yoffset,int x,int y,GLsizei width,GLsizei height),void);
OPENGL_1_1_FUNC(DeleteTextures,(GLsizei n, const GLuint * textures),void);
OPENGL_1_1_FUNC(TexSubImage1D,(GLenum target,int level,int xoffset,GLsizei width,GLenum format,GLenum type,const void * pixels),void);
OPENGL_1_1_FUNC(TexSubImage2D,(GLenum target,int level,int xoffset,int yoffset,GLsizei width,GLsizei height,GLenum format,GLenum type,const void * pixels),void);


OPENGL_1_1_FUNC(TexEnvf,(GLenum target,GLenum pname,float param),void);
OPENGL_1_1_FUNC(TexEnvfv,(GLenum,GLenum,const float *),void);
OPENGL_1_1_FUNC(TexEnvi,(GLenum,GLenum,int),void);
OPENGL_1_1_FUNC(TexEnviv,(GLenum,GLenum,const int *),void);

OPENGL_1_1_FUNC(TexGenf,(GLenum,GLenum,float),void);
OPENGL_1_1_FUNC(TexGenfv,(GLenum target,GLenum pname,const float * params),void);
OPENGL_1_1_FUNC(TexGeni,(GLenum target,GLenum pname,int param),void);
OPENGL_1_1_FUNC(TexGeniv,(GLenum target,GLenum pname,const int * params),void);

OPENGL_1_1_FUNC(TexImage1D,(GLenum target,int level,int internalForamt,GLsizei width,int border,int format,GLenum type,const void * pixels),void);
OPENGL_1_1_FUNC(TexImage2D,(GLenum target,int level,int internalFormat,GLsizei width,GLsizei height,int border,int fromat,GLenum type,const void * pixels),void);

OPENGL_1_1_FUNC(TexParameterf,(GLenum target,GLenum pname,float param),void);
OPENGL_1_1_FUNC(TexParameterfv,(GLenum target,GLenum pname,const float * params),void);
OPENGL_1_1_FUNC(TexParameteri,(GLenum target,GLenum pname,int param),void);
OPENGL_1_1_FUNC(TexParameteriv,(GLenum target,GLenum pname,const int * params),void);

// color, light, material, blending, fog
OPENGL_1_1_FUNC(ShadeModel,(GLenum mode),void);		

OPENGL_1_1_FUNC(Color4f,(float r,float g,float b,float a),void);	
OPENGL_1_1_FUNC(Color4fv,(const float * rgba),void);		

OPENGL_1_1_FUNC(Normal3f,(float x,float y,float z),void);	
OPENGL_1_1_FUNC(Normal3fv,(const float * xyz),void);	

OPENGL_1_1_FUNC(Lightf,(GLenum light,GLenum pname,float param),void);
OPENGL_1_1_FUNC(Lightfv,(GLenum light,GLenum pname,const float * params),void);

OPENGL_1_1_FUNC(Materialf,(GLenum face,GLenum pname,float param),void);
OPENGL_1_1_FUNC(Materialfv,(GLenum face,GLenum pname,const float * params),void);

OPENGL_1_1_FUNC(LightModelf,(GLenum pname,float param),void);
OPENGL_1_1_FUNC(LightModelfv,(GLenum pname,const float * params),void);	


OPENGL_1_1_FUNC(EvalCoord1f,(float u),void);
OPENGL_1_1_FUNC(EvalCoord1fv,(const float * u),void);
OPENGL_1_1_FUNC(EvalCoord2f,(float u,float v),void);
OPENGL_1_1_FUNC(EvalCoord2fv,(const float * uv),void);

OPENGL_1_1_FUNC(EvalMesh1,(GLenum mode,int i1,int i2),void);
OPENGL_1_1_FUNC(EvalMesh2,(GLenum mode,int i1,int i2,int j1,int j2),void);
OPENGL_1_1_FUNC(EvalPoint1,(int i),void);
OPENGL_1_1_FUNC(EvalPoint2,(int i,int j),void);

OPENGL_1_1_FUNC(FeedbackBuffer,(GLsizei size,GLenum type,float * buffer),void);

OPENGL_1_1_FUNC(GetClipPlane,(GLenum plane,double * equation),void);
OPENGL_1_1_FUNC(GetLightfv,(GLenum light,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetLightiv,(GLenum light,GLenum pname,int * params),void);
OPENGL_1_1_FUNC(GetMapfv,(GLenum target,GLenum query,float * v),void);
OPENGL_1_1_FUNC(GetMapiv,(GLenum target,GLenum query,int * v),void);
OPENGL_1_1_FUNC(GetMaterialfv,(GLenum face,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetMaterialiv,(GLenum face,GLenum pname,int * params),void);

OPENGL_1_1_FUNC(GetPixelMapfv,(GLenum map,float * values),void);
OPENGL_1_1_FUNC(GetPixelMapuiv,(GLenum map,GLuint * values),void);
OPENGL_1_1_FUNC(GetPointerv,(GLenum pname,void ** params),void);
OPENGL_1_1_FUNC(GetPolygonStipple,(GLubyte * mask),void);

OPENGL_1_1_FUNC(GetTexEnvfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetTexEnviv,(GLenum target,GLenum pname,int * params),void);
OPENGL_1_1_FUNC(GetTexGenfv,(GLenum coord,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetTexGeniv,(GLenum coord,GLenum pname,int * params),void);
OPENGL_1_1_FUNC(GetTexImage,(GLenum target,int level,GLenum format,GLenum type,void * pixels),void);
OPENGL_1_1_FUNC(GetTexLevelParameterfv,(GLenum target,int level,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetTexLevelParameteriv,(GLenum target,int level,GLenum pname,int * params),void);
OPENGL_1_1_FUNC(GetTexParameterfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_1_1_FUNC(GetTexParameteriv,(GLenum target,GLenum pname,int * params),void);

OPENGL_1_1_FUNC(IndexMask,(GLuint mask),void);

OPENGL_1_1_FUNC(Indexd,(double),void);
OPENGL_1_1_FUNC(Indexdv,(const double *),void);
OPENGL_1_1_FUNC(Indexf,(float),void);
OPENGL_1_1_FUNC(Indexfv,(const float *),void);
OPENGL_1_1_FUNC(Indexi,(int),void);
OPENGL_1_1_FUNC(Indexiv,(const int *),void);

OPENGL_1_1_FUNC(InitNames,(void),void);
OPENGL_1_1_FUNC(InterleavedArrays,(GLenum format,GLsizei stride,const void * pointer),void);
OPENGL_1_1_FUNC(IsTexture,(GLuint texture),GLboolean);
OPENGL_1_1_FUNC(LoadName,(GLuint name),void);
OPENGL_1_1_FUNC(LogicOp,(GLenum opcode),void);

OPENGL_1_1_FUNC(Map1f,(GLenum target,float u1,float u2,int stride,int order,const float * points),void);
OPENGL_1_1_FUNC(Map2f,(GLenum target,float u1,float u2,int ustride,int uorder,float v1,float v2,int vstride,int vorder,const float * points),void);

OPENGL_1_1_FUNC(MapGrid1f,(int un,float u1,float u2),void);
OPENGL_1_1_FUNC(MapGrid2f,(int un,float u1,float u2,int vn,float v1,float v2),void);

OPENGL_1_1_FUNC(PassTrough,(float token),void);

OPENGL_1_1_FUNC(PolygonMode,(GLenum face,GLenum mode),void);
OPENGL_1_1_FUNC(PolygonOffset,(float factor,float units),void);
OPENGL_1_1_FUNC(PolygonStipple,(const GLubyte * mask),void);

OPENGL_1_1_FUNC(PopAttrib,(void),void);
OPENGL_1_1_FUNC(PopClientAttrib,(void),void);
OPENGL_1_1_FUNC(PopName,(void),void);

OPENGL_1_1_FUNC(PushAttrib,(GLbitfield mask),void);
OPENGL_1_1_FUNC(PushClientAttrib,(void),void);
OPENGL_1_1_FUNC(PushName,(void),void);

OPENGL_1_1_FUNC(RenderMode,(GLenum mode),int);

OPENGL_1_1_FUNC(SelectBuffer,(GLsizei size,GLuint * buffer),void);

OPENGL_1_1_FUNC(TexCoord1f,(float s),void);
OPENGL_1_1_FUNC(TexCoord1fv,(const float * s),void);
OPENGL_1_1_FUNC(TexCoord2f,(float s,float t),void);
OPENGL_1_1_FUNC(TexCoord2fv,(const float * st),void);
OPENGL_1_1_FUNC(TexCoord3f,(float s,float t,float r),void);
OPENGL_1_1_FUNC(TexCoord3fv,(const float * str),void);
OPENGL_1_1_FUNC(TexCoord4f,(float s,float t,float r,float q),void);
OPENGL_1_1_FUNC(TexCoord4fv,(const float * strq),void);


// array 
OPENGL_1_1_FUNC(VertexPointer,(int size,GLenum type,GLsizei stride,const void * pointer),void);	
OPENGL_1_1_FUNC(NormalPointer,(GLenum type,GLsizei stride,const void * pointer),void);		
OPENGL_1_1_FUNC(ColorPointer,(int size,GLenum type,GLsizei stride,const void * pointer),void);	
OPENGL_1_1_FUNC(IndexPointer,(GLenum type,GLsizei stride,const void * pointer),void);		
OPENGL_1_1_FUNC(TexCoordPointer,(int size,GLuint type,GLsizei stride,const void * pointer),void);
OPENGL_1_1_FUNC(EdgeFlagPointer,(GLsizei stride,const void * pointer),void);			

OPENGL_1_1_FUNC(DrawArrays,(GLenum mode,int first,GLsizei count),void);					
OPENGL_1_1_FUNC(DrawElements,(GLenum mode,GLsizei count,GLenum type,const void * indicies),void);	

OPENGL_1_1_FUNC(ArrayElement,(int index),void);

// OpenGL 1.2 
OPENGL_FUNC_INI(BlendColor,(float r,float g,float b,float a),void);
OPENGL_FUNC_INI(BlendEquation,(GLuint mode),void);
OPENGL_FUNC_INI(CopyTexSubImage3D,(GLuint target,int level,int xoffset,int yoffset,int zoffset,int x,int y,GLsizei width,GLsizei height),void);
OPENGL_FUNC_INI(DrawRangeElements,(GLenum mode,GLuint start,GLuint end,GLsizei count,GLenum type,const void * indicies),void);
OPENGL_FUNC_INI(TexImage3D,(GLenum target,int level,int internalFormat,GLsizei width,GLsizei height,GLsizei depth,int border,GLenum format,GLenum type,const void * data),void);
OPENGL_FUNC_INI(TexSubImage3D,(GLenum target,int level,int xoffset,int yoffset,int zoffset,GLsizei width,GLsizei height,GLsizei depth,GLenum format,GLenum type,const void *data),void);

// OpenGL 1.3 
OPENGL_FUNC_INI(ActiveTexture,(GLenum texture),void);
OPENGL_FUNC_INI(ClientActiveTexture,(GLenum texture),void);
OPENGL_FUNC_INI(CompressedTexImage1D,(GLenum target,int level,GLenum internalFormat,GLsizei width,int border,GLsizei imageSize,const void * data),void);
OPENGL_FUNC_INI(CompressedTexImage2D,(GLenum target,int level,GLenum internalFormat,GLsizei width,GLsizei height,int border,GLsizei imageSize,const void * data),void);
OPENGL_FUNC_INI(CompressedTexImage3D,(GLenum target,int level,GLenum internalFormat,GLsizei width,GLsizei height,GLsizei depth,int border,GLsizei imageSize,const void * data),void);
OPENGL_FUNC_INI(CompressedTexSubImage1D,(GLenum target,int level,int xoffset,GLsizei width,GLenum format,GLsizei imageSize,const void * data),void);
OPENGL_FUNC_INI(CompressedTexSubImage2D,(GLenum target,int level,int xoffset,int yofffset,GLsizei width,GLsizei height,GLenum format,GLsizei imageSize,const void * data),void);
OPENGL_FUNC_INI(CompressedTexSubImage3D,(GLuint target,int level,int xoffset,int yoffset,int zoffset,GLsizei width,GLsizei height,GLsizei depth,GLenum format,GLsizei imageSize,const void * data),void);

OPENGL_FUNC_INI(MultiTexCoord1f,(GLenum target,float s),void);
OPENGL_FUNC_INI(MultiTexCoord1fv,(GLenum target,const float * s),void);
OPENGL_FUNC_INI(MultiTexCoord2f,(GLenum target,float s,float t),void);
OPENGL_FUNC_INI(MultiTexCoord2fv,(GLenum target,const float * st),void);
OPENGL_FUNC_INI(MultiTexCoord3f,(GLenum target,float s,float t,float r),void);
OPENGL_FUNC_INI(MultiTexCoord3fv,(GLenum target,const float * str),void);
OPENGL_FUNC_INI(MultiTexCoord4f,(GLenum target,float s,float t,float r,float q),void);
OPENGL_FUNC_INI(MultiTexCoord4fv,(GLenum target,const float * strq),void);

OPENGL_FUNC_INI(SampleCoverage,(float value,GLboolean invert),void);

// OpenGL 1.4 
OPENGL_FUNC_INI(BlendFuncSeparate,(GLenum srcRGB,GLenum dstRGB,GLenum srcA,GLenum dstA),void);

OPENGL_FUNC_INI(MultiDrawArrays,(GLenum mode,int * first,GLsizei * count,GLsizei primCount),void);
OPENGL_FUNC_INI(MultiDrawElements,(GLuint mode,GLsizei * count,GLenum type,const void ** indicies,GLsizei primCount),void);

OPENGL_FUNC_INI(PointParameterf,(GLenum pname,float param),void);
OPENGL_FUNC_INI(PointParameterfv,(GLenum pname,const float * params),void);

OPENGL_FUNC_INI(SecondaryColor3f,(float r,float g,float b),void);
OPENGL_FUNC_INI(SecondaryColor3fv,(const float * rgb),void);

OPENGL_FUNC_INI(SecondaryColorPointer,(int size,GLenum type,GLsizei stride,void * pointer),void);

OPENGL_FUNC_INI(WindowPos2f,(float x,float y),void);
OPENGL_FUNC_INI(WindowPos2fv,(const float * xy),void);
OPENGL_FUNC_INI(WindowPos2i,(int x,int y),void);
OPENGL_FUNC_INI(WindowPos2iv,(const int * xy),void);
OPENGL_FUNC_INI(WindowPos3f,(float x,float y,float z),void);
OPENGL_FUNC_INI(WindowPos3fv,(const float * xyz),void);
OPENGL_FUNC_INI(WindowPos3i,(int x,int y,int z),void);
OPENGL_FUNC_INI(WindowPos3iv,(const int * xyz),void);


// OpenGL 1.5 
OPENGL_FUNC_INI(BeginQuery,(GLenum target,GLuint id),void);
OPENGL_FUNC_INI(EndQuery,(GLenum target),void);
OPENGL_FUNC_INI(GenQueries,(GLsizei n,GLuint * ids),void);
OPENGL_FUNC_INI(DeleteQueries,(GLsizei n,const GLuint * ids),void);
OPENGL_FUNC_INI(GetQueryObjectuiv,(GLuint id,GLenum pname,GLuint * params),void);
OPENGL_FUNC_INI(GetQueryObjectiv,(GLuint id,GLenum pname,int * params),void);
OPENGL_FUNC_INI(GetQueryiv,(GLenum target,GLuint pname,int * params),void);
OPENGL_FUNC_INI(IsQuery,(GLuint id),int);

OPENGL_FUNC_INI(BindBuffer,(GLenum target,GLuint buffer),void);
OPENGL_FUNC_INI(BufferData,(GLenum target,GLsizeiptr size,const void * data,GLenum usage),void);
OPENGL_FUNC_INI(GetBufferParameteriv,(GLenum target,GLenum pname,int * data),void);
OPENGL_FUNC_INI(GetBufferPointerv,(GLenum targer,GLenum pname,void ** params),void);
OPENGL_FUNC_INI(BufferSubData,(GLenum target,GLintptr offset,GLsizeiptr size,const void * data),void);
OPENGL_FUNC_INI(GetBufferSubData,(GLenum target,GLintptr offset,GLsizeiptr size,void * data),void);
OPENGL_FUNC_INI(GenBuffers,(GLsizei n,GLuint * buffers),void);
OPENGL_FUNC_INI(DeleteBuffers,(GLsizei n,const GLuint * buffers),void);
OPENGL_FUNC_INI(IsBuffer,(GLuint buffer),GLboolean);
OPENGL_FUNC_INI(MapBuffer,(GLenum target,GLenum access),void *);
OPENGL_FUNC_INI(UnmapBuffer,(GLenum target),GLboolean);


}



// OpenGL 1.1
/* AccumOp */
#define GL_ACCUM                          0x0100
#define GL_LOAD                           0x0101
#define GL_RETURN                         0x0102
#define GL_MULT                           0x0103
#define GL_ADD                            0x0104
/* AlphaFunction */
#define GL_NEVER                          0x0200
#define GL_LESS                           0x0201
#define GL_EQUAL                          0x0202
#define GL_LEQUAL                         0x0203
#define GL_GREATER                        0x0204
#define GL_NOTEQUAL                       0x0205
#define GL_GEQUAL                         0x0206
#define GL_ALWAYS                         0x0207
/* AttribMask */
#define GL_CURRENT_BIT                    0x00000001
#define GL_POINT_BIT                      0x00000002
#define GL_LINE_BIT                       0x00000004
#define GL_POLYGON_BIT                    0x00000008
#define GL_POLYGON_STIPPLE_BIT            0x00000010
#define GL_PIXEL_MODE_BIT                 0x00000020
#define GL_LIGHTING_BIT                   0x00000040
#define GL_FOG_BIT                        0x00000080
#define GL_DEPTH_BUFFER_BIT               0x00000100
#define GL_ACCUM_BUFFER_BIT               0x00000200
#define GL_STENCIL_BUFFER_BIT             0x00000400
#define GL_VIEWPORT_BIT                   0x00000800
#define GL_TRANSFORM_BIT                  0x00001000
#define GL_ENABLE_BIT                     0x00002000
#define GL_COLOR_BUFFER_BIT               0x00004000
#define GL_HINT_BIT                       0x00008000
#define GL_EVAL_BIT                       0x00010000
#define GL_LIST_BIT                       0x00020000
#define GL_TEXTURE_BIT                    0x00040000
#define GL_SCISSOR_BIT                    0x00080000
#define GL_ALL_ATTRIB_BITS                0x000fffff
/* AlphaFunction */
#define GL_NEVER                          0x0200
#define GL_LESS                           0x0201
#define GL_EQUAL                          0x0202
#define GL_LEQUAL                         0x0203
#define GL_GREATER                        0x0204
#define GL_NOTEQUAL                       0x0205
#define GL_GEQUAL                         0x0206
#define GL_ALWAYS                         0x0207
/* BeginMode */
#define GL_POINTS                         0x0000
#define GL_LINES                          0x0001
#define GL_LINE_LOOP                      0x0002
#define GL_LINE_STRIP                     0x0003
#define GL_TRIANGLES                      0x0004
#define GL_TRIANGLE_STRIP                 0x0005
#define GL_TRIANGLE_FAN                   0x0006
#define GL_QUADS                          0x0007
#define GL_QUAD_STRIP                     0x0008
#define GL_POLYGON                        0x0009
/* BlendingFactorDest */
#define GL_ZERO                           0
#define GL_ONE                            1
#define GL_SRC_COLOR                      0x0300
#define GL_ONE_MINUS_SRC_COLOR            0x0301
#define GL_SRC_ALPHA                      0x0302
#define GL_ONE_MINUS_SRC_ALPHA            0x0303
#define GL_DST_ALPHA                      0x0304
#define GL_ONE_MINUS_DST_ALPHA            0x0305
/* BlendingFactorSrc */
#define GL_DST_COLOR                      0x0306
#define GL_ONE_MINUS_DST_COLOR            0x0307
#define GL_SRC_ALPHA_SATURATE             0x0308
/* Boolean */
#define GL_FALSE                          0
#define GL_TRUE                           1
/* ClipPlaneName */
#define GL_CLIP_PLANE0                    0x3000
#define GL_CLIP_PLANE1                    0x3001
#define GL_CLIP_PLANE2                    0x3002
#define GL_CLIP_PLANE3                    0x3003
#define GL_CLIP_PLANE4                    0x3004
#define GL_CLIP_PLANE5                    0x3005
/* DataType */
#define GL_BYTE                           0x1400
#define GL_UNSIGNED_BYTE                  0x1401
#define GL_SHORT                          0x1402
#define GL_UNSIGNED_SHORT                 0x1403
#define GL_INT                            0x1404
#define GL_UNSIGNED_INT                   0x1405
#define GL_FLOAT                          0x1406
#define GL_2_BYTES                        0x1407
#define GL_3_BYTES                        0x1408
#define GL_4_BYTES                        0x1409
#define GL_DOUBLE                         0x140A
/* DrawBufferMode */
#define GL_NONE                           0
#define GL_FRONT_LEFT                     0x0400
#define GL_FRONT_RIGHT                    0x0401
#define GL_BACK_LEFT                      0x0402
#define GL_BACK_RIGHT                     0x0403
#define GL_FRONT                          0x0404
#define GL_BACK                           0x0405
#define GL_LEFT                           0x0406
#define GL_RIGHT                          0x0407
#define GL_FRONT_AND_BACK                 0x0408
#define GL_AUX0                           0x0409
#define GL_AUX1                           0x040A
#define GL_AUX2                           0x040B
#define GL_AUX3                           0x040C
/* ErrorCode */
#define GL_NO_ERROR                       0
#define GL_INVALID_ENUM                   0x0500
#define GL_INVALID_VALUE                  0x0501
#define GL_INVALID_OPERATION              0x0502
#define GL_STACK_OVERFLOW                 0x0503
#define GL_STACK_UNDERFLOW                0x0504
#define GL_OUT_OF_MEMORY                  0x0505
/* FeedBackMode */
#define GL_2D                             0x0600
#define GL_3D                             0x0601
#define GL_3D_COLOR                       0x0602
#define GL_3D_COLOR_TEXTURE               0x0603
#define GL_4D_COLOR_TEXTURE               0x0604

/* FeedBackToken */
#define GL_PASS_THROUGH_TOKEN             0x0700
#define GL_POINT_TOKEN                    0x0701
#define GL_LINE_TOKEN                     0x0702
#define GL_POLYGON_TOKEN                  0x0703
#define GL_BITMAP_TOKEN                   0x0704
#define GL_DRAW_PIXEL_TOKEN               0x0705
#define GL_COPY_PIXEL_TOKEN               0x0706
#define GL_LINE_RESET_TOKEN               0x0707
/* FogMode */
/*      GL_LINEAR */
#define GL_EXP                            0x0800
#define GL_EXP2                           0x0801
/* ErrorCode */
#define GL_NO_ERROR                       0
#define GL_INVALID_ENUM                   0x0500
#define GL_INVALID_VALUE                  0x0501
#define GL_INVALID_OPERATION              0x0502
#define GL_OUT_OF_MEMORY                  0x0505
/* FrontFaceDirection */
#define GL_CW                             0x0900
#define GL_CCW                            0x0901
/* GetMapTarget */
#define GL_COEFF                          0x0A00
#define GL_ORDER                          0x0A01
#define GL_DOMAIN                         0x0A02
/* GetPName */
/* GetTarget */
#define GL_CURRENT_COLOR                  0x0B00
#define GL_CURRENT_INDEX                  0x0B01
#define GL_CURRENT_NORMAL                 0x0B02
#define GL_CURRENT_TEXTURE_COORDS         0x0B03
#define GL_CURRENT_RASTER_COLOR           0x0B04
#define GL_CURRENT_RASTER_INDEX           0x0B05
#define GL_CURRENT_RASTER_TEXTURE_COORDS  0x0B06
#define GL_CURRENT_RASTER_POSITION        0x0B07
#define GL_CURRENT_RASTER_POSITION_VALID  0x0B08
#define GL_CURRENT_RASTER_DISTANCE        0x0B09
#define GL_POINT_SMOOTH                   0x0B10
#define GL_POINT_SIZE                     0x0B11
#define GL_POINT_SIZE_RANGE               0x0B12
#define GL_POINT_SIZE_GRANULARITY         0x0B13
#define GL_LINE_SMOOTH                    0x0B20
#define GL_LINE_WIDTH                     0x0B21
#define GL_LINE_WIDTH_RANGE               0x0B22
#define GL_LINE_WIDTH_GRANULARITY         0x0B23
#define GL_LINE_STIPPLE                   0x0B24
#define GL_LINE_STIPPLE_PATTERN           0x0B25
#define GL_LINE_STIPPLE_REPEAT            0x0B26
#define GL_LIST_MODE                      0x0B30
#define GL_MAX_LIST_NESTING               0x0B31
#define GL_LIST_BASE                      0x0B32
#define GL_LIST_INDEX                     0x0B33
#define GL_POLYGON_MODE                   0x0B40
#define GL_POLYGON_SMOOTH                 0x0B41
#define GL_POLYGON_STIPPLE                0x0B42
#define GL_EDGE_FLAG                      0x0B43
#define GL_CULL_FACE                      0x0B44
#define GL_CULL_FACE_MODE                 0x0B45
#define GL_FRONT_FACE                     0x0B46
#define GL_LIGHTING                       0x0B50
#define GL_LIGHT_MODEL_LOCAL_VIEWER       0x0B51
#define GL_LIGHT_MODEL_TWO_SIDE           0x0B52
#define GL_LIGHT_MODEL_AMBIENT            0x0B53
#define GL_SHADE_MODEL                    0x0B54
#define GL_COLOR_MATERIAL_FACE            0x0B55
#define GL_COLOR_MATERIAL_PARAMETER       0x0B56
#define GL_COLOR_MATERIAL                 0x0B57
#define GL_FOG                            0x0B60
#define GL_FOG_INDEX                      0x0B61
#define GL_FOG_DENSITY                    0x0B62
#define GL_FOG_START                      0x0B63
#define GL_FOG_END                        0x0B64
#define GL_FOG_MODE                       0x0B65
#define GL_FOG_COLOR                      0x0B66
#define GL_DEPTH_RANGE                    0x0B70
#define GL_DEPTH_TEST                     0x0B71
#define GL_DEPTH_WRITEMASK                0x0B72
#define GL_DEPTH_CLEAR_VALUE              0x0B73
#define GL_DEPTH_FUNC                     0x0B74
#define GL_ACCUM_CLEAR_VALUE              0x0B80
#define GL_STENCIL_TEST                   0x0B90
#define GL_STENCIL_CLEAR_VALUE            0x0B91
#define GL_STENCIL_FUNC                   0x0B92
#define GL_STENCIL_VALUE_MASK             0x0B93
#define GL_STENCIL_FAIL                   0x0B94
#define GL_STENCIL_PASS_DEPTH_FAIL        0x0B95
#define GL_STENCIL_PASS_DEPTH_PASS        0x0B96
#define GL_STENCIL_REF                    0x0B97
#define GL_STENCIL_WRITEMASK              0x0B98
#define GL_MATRIX_MODE                    0x0BA0
#define GL_NORMALIZE                      0x0BA1
#define GL_VIEWPORT                       0x0BA2
#define GL_MODELVIEW_STACK_DEPTH          0x0BA3
#define GL_PROJECTION_STACK_DEPTH         0x0BA4
#define GL_TEXTURE_STACK_DEPTH            0x0BA5
#define GL_MODELVIEW_MATRIX               0x0BA6
#define GL_PROJECTION_MATRIX              0x0BA7
#define GL_TEXTURE_MATRIX                 0x0BA8
#define GL_ATTRIB_STACK_DEPTH             0x0BB0
#define GL_CLIENT_ATTRIB_STACK_DEPTH      0x0BB1
#define GL_ALPHA_TEST                     0x0BC0
#define GL_ALPHA_TEST_FUNC                0x0BC1
#define GL_ALPHA_TEST_REF                 0x0BC2
#define GL_DITHER                         0x0BD0
#define GL_BLEND_DST                      0x0BE0
#define GL_BLEND_SRC                      0x0BE1
#define GL_BLEND                          0x0BE2
#define GL_LOGIC_OP_MODE                  0x0BF0
#define GL_INDEX_LOGIC_OP                 0x0BF1
#define GL_COLOR_LOGIC_OP                 0x0BF2
#define GL_AUX_BUFFERS                    0x0C00
#define GL_DRAW_BUFFER                    0x0C01
#define GL_READ_BUFFER                    0x0C02
#define GL_SCISSOR_BOX                    0x0C10
#define GL_SCISSOR_TEST                   0x0C11
#define GL_INDEX_CLEAR_VALUE              0x0C20
#define GL_INDEX_WRITEMASK                0x0C21
#define GL_COLOR_CLEAR_VALUE              0x0C22
#define GL_COLOR_WRITEMASK                0x0C23
#define GL_INDEX_MODE                     0x0C30
#define GL_RGBA_MODE                      0x0C31
#define GL_DOUBLEBUFFER                   0x0C32
#define GL_STEREO                         0x0C33
#define GL_RENDER_MODE                    0x0C40
#define GL_PERSPECTIVE_CORRECTION_HINT    0x0C50
#define GL_POINT_SMOOTH_HINT              0x0C51
#define GL_LINE_SMOOTH_HINT               0x0C52
#define GL_POLYGON_SMOOTH_HINT            0x0C53
#define GL_FOG_HINT                       0x0C54
#define GL_TEXTURE_GEN_S                  0x0C60
#define GL_TEXTURE_GEN_T                  0x0C61
#define GL_TEXTURE_GEN_R                  0x0C62
#define GL_TEXTURE_GEN_Q                  0x0C63
#define GL_PIXEL_MAP_I_TO_I               0x0C70
#define GL_PIXEL_MAP_S_TO_S               0x0C71
#define GL_PIXEL_MAP_I_TO_R               0x0C72
#define GL_PIXEL_MAP_I_TO_G               0x0C73
#define GL_PIXEL_MAP_I_TO_B               0x0C74
#define GL_PIXEL_MAP_I_TO_A               0x0C75
#define GL_PIXEL_MAP_R_TO_R               0x0C76
#define GL_PIXEL_MAP_G_TO_G               0x0C77
#define GL_PIXEL_MAP_B_TO_B               0x0C78
#define GL_PIXEL_MAP_A_TO_A               0x0C79
#define GL_PIXEL_MAP_I_TO_I_SIZE          0x0CB0
#define GL_PIXEL_MAP_S_TO_S_SIZE          0x0CB1
#define GL_PIXEL_MAP_I_TO_R_SIZE          0x0CB2
#define GL_PIXEL_MAP_I_TO_G_SIZE          0x0CB3
#define GL_PIXEL_MAP_I_TO_B_SIZE          0x0CB4
#define GL_PIXEL_MAP_I_TO_A_SIZE          0x0CB5
#define GL_PIXEL_MAP_R_TO_R_SIZE          0x0CB6
#define GL_PIXEL_MAP_G_TO_G_SIZE          0x0CB7
#define GL_PIXEL_MAP_B_TO_B_SIZE          0x0CB8
#define GL_PIXEL_MAP_A_TO_A_SIZE          0x0CB9
#define GL_UNPACK_SWAP_BYTES              0x0CF0
#define GL_UNPACK_LSB_FIRST               0x0CF1
#define GL_UNPACK_ROW_LENGTH              0x0CF2
#define GL_UNPACK_SKIP_ROWS               0x0CF3
#define GL_UNPACK_SKIP_PIXELS             0x0CF4
#define GL_UNPACK_ALIGNMENT               0x0CF5
#define GL_PACK_SWAP_BYTES                0x0D00
#define GL_PACK_LSB_FIRST                 0x0D01
#define GL_PACK_ROW_LENGTH                0x0D02
#define GL_PACK_SKIP_ROWS                 0x0D03
#define GL_PACK_SKIP_PIXELS               0x0D04
#define GL_PACK_ALIGNMENT                 0x0D05
#define GL_MAP_COLOR                      0x0D10
#define GL_MAP_STENCIL                    0x0D11
#define GL_INDEX_SHIFT                    0x0D12
#define GL_INDEX_OFFSET                   0x0D13
#define GL_RED_SCALE                      0x0D14
#define GL_RED_BIAS                       0x0D15
#define GL_ZOOM_X                         0x0D16
#define GL_ZOOM_Y                         0x0D17
#define GL_GREEN_SCALE                    0x0D18
#define GL_GREEN_BIAS                     0x0D19
#define GL_BLUE_SCALE                     0x0D1A
#define GL_BLUE_BIAS                      0x0D1B
#define GL_ALPHA_SCALE                    0x0D1C
#define GL_ALPHA_BIAS                     0x0D1D
#define GL_DEPTH_SCALE                    0x0D1E
#define GL_DEPTH_BIAS                     0x0D1F
#define GL_MAX_EVAL_ORDER                 0x0D30
#define GL_MAX_LIGHTS                     0x0D31
#define GL_MAX_CLIP_PLANES                0x0D32
#define GL_MAX_TEXTURE_SIZE               0x0D33
#define GL_MAX_PIXEL_MAP_TABLE            0x0D34
#define GL_MAX_ATTRIB_STACK_DEPTH         0x0D35
#define GL_MAX_MODELVIEW_STACK_DEPTH      0x0D36
#define GL_MAX_NAME_STACK_DEPTH           0x0D37
#define GL_MAX_PROJECTION_STACK_DEPTH     0x0D38
#define GL_MAX_TEXTURE_STACK_DEPTH        0x0D39
#define GL_MAX_VIEWPORT_DIMS              0x0D3A
#define GL_MAX_CLIENT_ATTRIB_STACK_DEPTH  0x0D3B
#define GL_SUBPIXEL_BITS                  0x0D50
#define GL_INDEX_BITS                     0x0D51
#define GL_RED_BITS                       0x0D52
#define GL_GREEN_BITS                     0x0D53
#define GL_BLUE_BITS                      0x0D54
#define GL_ALPHA_BITS                     0x0D55
#define GL_DEPTH_BITS                     0x0D56
#define GL_STENCIL_BITS                   0x0D57
#define GL_ACCUM_RED_BITS                 0x0D58
#define GL_ACCUM_GREEN_BITS               0x0D59
#define GL_ACCUM_BLUE_BITS                0x0D5A
#define GL_ACCUM_ALPHA_BITS               0x0D5B
#define GL_NAME_STACK_DEPTH               0x0D70
#define GL_AUTO_NORMAL                    0x0D80
#define GL_MAP1_COLOR_4                   0x0D90
#define GL_MAP1_INDEX                     0x0D91
#define GL_MAP1_NORMAL                    0x0D92
#define GL_MAP1_TEXTURE_COORD_1           0x0D93
#define GL_MAP1_TEXTURE_COORD_2           0x0D94
#define GL_MAP1_TEXTURE_COORD_3           0x0D95
#define GL_MAP1_TEXTURE_COORD_4           0x0D96
#define GL_MAP1_VERTEX_3                  0x0D97
#define GL_MAP1_VERTEX_4                  0x0D98
#define GL_MAP2_COLOR_4                   0x0DB0
#define GL_MAP2_INDEX                     0x0DB1
#define GL_MAP2_NORMAL                    0x0DB2
#define GL_MAP2_TEXTURE_COORD_1           0x0DB3
#define GL_MAP2_TEXTURE_COORD_2           0x0DB4
#define GL_MAP2_TEXTURE_COORD_3           0x0DB5
#define GL_MAP2_TEXTURE_COORD_4           0x0DB6
#define GL_MAP2_VERTEX_3                  0x0DB7
#define GL_MAP2_VERTEX_4                  0x0DB8
#define GL_MAP1_GRID_DOMAIN               0x0DD0
#define GL_MAP1_GRID_SEGMENTS             0x0DD1
#define GL_MAP2_GRID_DOMAIN               0x0DD2
#define GL_MAP2_GRID_SEGMENTS             0x0DD3
#define GL_TEXTURE_1D                     0x0DE0
#define GL_TEXTURE_2D                     0x0DE1
#define GL_FEEDBACK_BUFFER_POINTER        0x0DF0
#define GL_FEEDBACK_BUFFER_SIZE           0x0DF1
#define GL_FEEDBACK_BUFFER_TYPE           0x0DF2
#define GL_SELECTION_BUFFER_POINTER       0x0DF3
#define GL_SELECTION_BUFFER_SIZE          0x0DF4
/* GetTextureParameter */
#define GL_TEXTURE_WIDTH                  0x1000
#define GL_TEXTURE_HEIGHT                 0x1001
#define GL_TEXTURE_INTERNAL_FORMAT        0x1003
#define GL_TEXTURE_BORDER_COLOR           0x1004
#define GL_TEXTURE_BORDER                 0x1005
#define GL_TEXTURE_RED_SIZE               0x805C
#define GL_TEXTURE_GREEN_SIZE             0x805D
#define GL_TEXTURE_BLUE_SIZE              0x805E
#define GL_TEXTURE_ALPHA_SIZE             0x805F
/* HintMode */
#define GL_DONT_CARE                      0x1100
#define GL_FASTEST                        0x1101
#define GL_NICEST                         0x1102
/* LightName */
#define GL_LIGHT0                         0x4000
#define GL_LIGHT1                         0x4001
#define GL_LIGHT2                         0x4002
#define GL_LIGHT3                         0x4003
#define GL_LIGHT4                         0x4004
#define GL_LIGHT5                         0x4005
#define GL_LIGHT6                         0x4006
#define GL_LIGHT7                         0x4007
/* LightParameter */
#define GL_AMBIENT                        0x1200
#define GL_DIFFUSE                        0x1201
#define GL_SPECULAR                       0x1202
#define GL_POSITION                       0x1203
#define GL_SPOT_DIRECTION                 0x1204
#define GL_SPOT_EXPONENT                  0x1205
#define GL_SPOT_CUTOFF                    0x1206
#define GL_CONSTANT_ATTENUATION           0x1207
#define GL_LINEAR_ATTENUATION             0x1208
#define GL_QUADRATIC_ATTENUATION          0x1209
/* ListMode */
#define GL_COMPILE                        0x1300
#define GL_COMPILE_AND_EXECUTE            0x1301
/* LogicOp */
#define GL_CLEAR                          0x1500
#define GL_AND                            0x1501
#define GL_AND_REVERSE                    0x1502
#define GL_COPY                           0x1503
#define GL_AND_INVERTED                   0x1504
#define GL_NOOP                           0x1505
#define GL_XOR                            0x1506
#define GL_OR                             0x1507
#define GL_NOR                            0x1508
#define GL_EQUIV                          0x1509
#define GL_INVERT                         0x150A
#define GL_OR_REVERSE                     0x150B
#define GL_COPY_INVERTED                  0x150C
#define GL_OR_INVERTED                    0x150D
#define GL_NAND                           0x150E
#define GL_SET                            0x150F
/* MatrixMode */
#define GL_MODELVIEW                      0x1700
#define GL_PROJECTION                     0x1701
#define GL_TEXTURE                        0x1702
/* MaterialParameter */
#define GL_EMISSION                       0x1600
#define GL_SHININESS                      0x1601
#define GL_AMBIENT_AND_DIFFUSE            0x1602
#define GL_COLOR_INDEXES                  0x1603
/* PixelCopyType */
#define GL_COLOR                          0x1800
#define GL_DEPTH                          0x1801
#define GL_STENCIL                        0x1802
/* PixelFormat */
#define GL_COLOR_INDEX                    0x1900
#define GL_STENCIL_INDEX                  0x1901
#define GL_DEPTH_COMPONENT                0x1902
#define GL_RED                            0x1903
#define GL_GREEN                          0x1904
#define GL_BLUE                           0x1905
#define GL_ALPHA                          0x1906
#define GL_RGB                            0x1907
#define GL_RGBA                           0x1908
#define GL_LUMINANCE                      0x1909
#define GL_LUMINANCE_ALPHA                0x190A
/* PixelType */
#define GL_BITMAP                         0x1A00
/* PolygonMode */
#define GL_POINT                          0x1B00
#define GL_LINE                           0x1B01
#define GL_FILL                           0x1B02
/* RenderingMode */
#define GL_RENDER                         0x1C00
#define GL_FEEDBACK                       0x1C01
#define GL_SELECT                         0x1C02
/* ShadingModel */
#define GL_FLAT                           0x1D00
#define GL_SMOOTH                         0x1D01
/* StencilOp */
#define GL_KEEP                           0x1E00
#define GL_REPLACE                        0x1E01
#define GL_INCR                           0x1E02
#define GL_DECR                           0x1E03
/* StringName */
#define GL_VENDOR                         0x1F00
#define GL_RENDERER                       0x1F01
#define GL_VERSION                        0x1F02
#define GL_EXTENSIONS                     0x1F03
/* TextureCoordName */
#define GL_S                              0x2000
#define GL_T                              0x2001
#define GL_R                              0x2002
#define GL_Q                              0x2003
/* TextureEnvMode */
#define GL_MODULATE                       0x2100
#define GL_DECAL                          0x2101
/* TextureEnvParameter */
#define GL_TEXTURE_ENV_MODE               0x2200
#define GL_TEXTURE_ENV_COLOR              0x2201
/* TextureEnvTarget */
#define GL_TEXTURE_ENV                    0x2300
/* TextureGenMode */
#define GL_EYE_LINEAR                     0x2400
#define GL_OBJECT_LINEAR                  0x2401
#define GL_SPHERE_MAP                     0x2402
/* TextureGenParameter */
#define GL_TEXTURE_GEN_MODE               0x2500
#define GL_OBJECT_PLANE                   0x2501
#define GL_EYE_PLANE                      0x2502
/* TextureMagFilter */
#define GL_NEAREST                        0x2600
#define GL_LINEAR                         0x2601
/* TextureMinFilter */
#define GL_NEAREST_MIPMAP_NEAREST         0x2700
#define GL_LINEAR_MIPMAP_NEAREST          0x2701
#define GL_NEAREST_MIPMAP_LINEAR          0x2702
#define GL_LINEAR_MIPMAP_LINEAR           0x2703
/* TextureParameterName */
#define GL_TEXTURE_MAG_FILTER             0x2800
#define GL_TEXTURE_MIN_FILTER             0x2801
#define GL_TEXTURE_WRAP_S                 0x2802
#define GL_TEXTURE_WRAP_T                 0x2803
/* TextureTarget */
#define GL_PROXY_TEXTURE_1D               0x8063
#define GL_PROXY_TEXTURE_2D               0x8064
/* TextureWrapMode */
#define GL_CLAMP                          0x2900
#define GL_REPEAT                         0x2901


/* ClientAttribMask */
#define GL_CLIENT_PIXEL_STORE_BIT         0x00000001
#define GL_CLIENT_VERTEX_ARRAY_BIT        0x00000002
#define GL_CLIENT_ALL_ATTRIB_BITS         0xffffffff
/* polygon_offset */
#define GL_POLYGON_OFFSET_FACTOR          0x8038
#define GL_POLYGON_OFFSET_UNITS           0x2A00
#define GL_POLYGON_OFFSET_POINT           0x2A01
#define GL_POLYGON_OFFSET_LINE            0x2A02
#define GL_POLYGON_OFFSET_FILL            0x8037
/* texture */
#define GL_ALPHA4                         0x803B
#define GL_ALPHA8                         0x803C
#define GL_ALPHA12                        0x803D
#define GL_ALPHA16                        0x803E
#define GL_LUMINANCE4                     0x803F
#define GL_LUMINANCE8                     0x8040
#define GL_LUMINANCE12                    0x8041
#define GL_LUMINANCE16                    0x8042
#define GL_LUMINANCE4_ALPHA4              0x8043
#define GL_LUMINANCE6_ALPHA2              0x8044
#define GL_LUMINANCE8_ALPHA8              0x8045
#define GL_LUMINANCE12_ALPHA4             0x8046
#define GL_LUMINANCE12_ALPHA12            0x8047
#define GL_LUMINANCE16_ALPHA16            0x8048
#define GL_INTENSITY                      0x8049
#define GL_INTENSITY4                     0x804A
#define GL_INTENSITY8                     0x804B
#define GL_INTENSITY12                    0x804C
#define GL_INTENSITY16                    0x804D
#define GL_R3_G3_B2                       0x2A10
#define GL_RGB4                           0x804F
#define GL_RGB5                           0x8050
#define GL_RGB8                           0x8051
#define GL_RGB10                          0x8052
#define GL_RGB12                          0x8053
#define GL_RGB16                          0x8054
#define GL_RGBA2                          0x8055
#define GL_RGBA4                          0x8056
#define GL_RGB5_A1                        0x8057
#define GL_RGBA8                          0x8058
#define GL_RGB10_A2                       0x8059
#define GL_RGBA12                         0x805A
#define GL_RGBA16                         0x805B
#define GL_TEXTURE_RED_SIZE               0x805C
#define GL_TEXTURE_GREEN_SIZE             0x805D
#define GL_TEXTURE_BLUE_SIZE              0x805E
#define GL_TEXTURE_ALPHA_SIZE             0x805F
#define GL_TEXTURE_LUMINANCE_SIZE         0x8060
#define GL_TEXTURE_INTENSITY_SIZE         0x8061
#define GL_PROXY_TEXTURE_1D               0x8063
#define GL_PROXY_TEXTURE_2D               0x8064

/* texture_object */
#define GL_TEXTURE_PRIORITY               0x8066
#define GL_TEXTURE_RESIDENT               0x8067
#define GL_TEXTURE_BINDING_1D             0x8068
#define GL_TEXTURE_BINDING_2D             0x8069

/* vertex_array */
#define GL_VERTEX_ARRAY                   0x8074
#define GL_NORMAL_ARRAY                   0x8075
#define GL_COLOR_ARRAY                    0x8076
#define GL_INDEX_ARRAY                    0x8077
#define GL_TEXTURE_COORD_ARRAY            0x8078
#define GL_EDGE_FLAG_ARRAY                0x8079
#define GL_VERTEX_ARRAY_SIZE              0x807A
#define GL_VERTEX_ARRAY_TYPE              0x807B
#define GL_VERTEX_ARRAY_STRIDE            0x807C
#define GL_NORMAL_ARRAY_TYPE              0x807E
#define GL_NORMAL_ARRAY_STRIDE            0x807F
#define GL_COLOR_ARRAY_SIZE               0x8081
#define GL_COLOR_ARRAY_TYPE               0x8082
#define GL_COLOR_ARRAY_STRIDE             0x8083
#define GL_INDEX_ARRAY_TYPE               0x8085
#define GL_INDEX_ARRAY_STRIDE             0x8086
#define GL_TEXTURE_COORD_ARRAY_SIZE       0x8088
#define GL_TEXTURE_COORD_ARRAY_TYPE       0x8089
#define GL_TEXTURE_COORD_ARRAY_STRIDE     0x808A
#define GL_EDGE_FLAG_ARRAY_STRIDE         0x808C
#define GL_VERTEX_ARRAY_POINTER           0x808E
#define GL_NORMAL_ARRAY_POINTER           0x808F
#define GL_COLOR_ARRAY_POINTER            0x8090
#define GL_INDEX_ARRAY_POINTER            0x8091
#define GL_TEXTURE_COORD_ARRAY_POINTER    0x8092
#define GL_EDGE_FLAG_ARRAY_POINTER        0x8093
#define GL_V2F                            0x2A20
#define GL_V3F                            0x2A21
#define GL_C4UB_V2F                       0x2A22
#define GL_C4UB_V3F                       0x2A23
#define GL_C3F_V3F                        0x2A24
#define GL_N3F_V3F                        0x2A25
#define GL_C4F_N3F_V3F                    0x2A26
#define GL_T2F_V3F                        0x2A27
#define GL_T4F_V4F                        0x2A28
#define GL_T2F_C4UB_V3F                   0x2A29
#define GL_T2F_C3F_V3F                    0x2A2A
#define GL_T2F_N3F_V3F                    0x2A2B
#define GL_T2F_C4F_N3F_V3F                0x2A2C
#define GL_T4F_C4F_N3F_V4F                0x2A2D

/* PixelInternalFormat */
#define GL_R3_G3_B2                       0x2A10
#define GL_RGB4                           0x804F
#define GL_RGB5                           0x8050
#define GL_RGB8                           0x8051
#define GL_RGB10                          0x8052
#define GL_RGB12                          0x8053
#define GL_RGB16                          0x8054
#define GL_RGBA2                          0x8055
#define GL_RGBA4                          0x8056
#define GL_RGB5_A1                        0x8057
#define GL_RGBA8                          0x8058
#define GL_RGB10_A2                       0x8059
#define GL_RGBA12                         0x805A
#define GL_RGBA16                         0x805B

// OpenGL 1.2
#define GL_UNSIGNED_BYTE_3_3_2            0x8032
#define GL_UNSIGNED_SHORT_4_4_4_4         0x8033
#define GL_UNSIGNED_SHORT_5_5_5_1         0x8034
#define GL_UNSIGNED_INT_8_8_8_8           0x8035
#define GL_UNSIGNED_INT_10_10_10_2        0x8036
#define GL_TEXTURE_BINDING_3D             0x806A
#define GL_PACK_SKIP_IMAGES               0x806B
#define GL_PACK_IMAGE_HEIGHT              0x806C
#define GL_UNPACK_SKIP_IMAGES             0x806D
#define GL_UNPACK_IMAGE_HEIGHT            0x806E
#define GL_TEXTURE_3D                     0x806F
#define GL_PROXY_TEXTURE_3D               0x8070
#define GL_TEXTURE_DEPTH                  0x8071
#define GL_TEXTURE_WRAP_R                 0x8072
#define GL_MAX_3D_TEXTURE_SIZE            0x8073
#define GL_UNSIGNED_BYTE_2_3_3_REV        0x8362
#define GL_UNSIGNED_SHORT_5_6_5           0x8363
#define GL_UNSIGNED_SHORT_5_6_5_REV       0x8364
#define GL_UNSIGNED_SHORT_4_4_4_4_REV     0x8365
#define GL_UNSIGNED_SHORT_1_5_5_5_REV     0x8366
#define GL_UNSIGNED_INT_8_8_8_8_REV       0x8367
#define GL_UNSIGNED_INT_2_10_10_10_REV    0x8368
#define GL_BGR                            0x80E0
#define GL_BGRA                           0x80E1
#define GL_MAX_ELEMENTS_VERTICES          0x80E8
#define GL_MAX_ELEMENTS_INDICES           0x80E9
#define GL_CLAMP_TO_EDGE                  0x812F
#define GL_TEXTURE_MIN_LOD                0x813A
#define GL_TEXTURE_MAX_LOD                0x813B
#define GL_TEXTURE_BASE_LEVEL             0x813C
#define GL_TEXTURE_MAX_LEVEL              0x813D
#define GL_SMOOTH_POINT_SIZE_RANGE        0x0B12
#define GL_SMOOTH_POINT_SIZE_GRANULARITY  0x0B13
#define GL_SMOOTH_LINE_WIDTH_RANGE        0x0B22
#define GL_SMOOTH_LINE_WIDTH_GRANULARITY  0x0B23
#define GL_ALIASED_LINE_WIDTH_RANGE       0x846E

// imaging
#define GL_CONSTANT_COLOR                 0x8001
#define GL_ONE_MINUS_CONSTANT_COLOR       0x8002
#define GL_CONSTANT_ALPHA                 0x8003
#define GL_ONE_MINUS_CONSTANT_ALPHA       0x8004
#define GL_BLEND_COLOR                    0x8005
#define GL_FUNC_ADD                       0x8006
#define GL_MIN                            0x8007
#define GL_MAX                            0x8008
#define GL_BLEND_EQUATION                 0x8009
#define GL_FUNC_SUBTRACT                  0x800A
#define GL_FUNC_REVERSE_SUBTRACT          0x800B

// opengl 1.3
#define GL_TEXTURE0                       0x84C0
#define GL_TEXTURE1                       0x84C1
#define GL_TEXTURE2                       0x84C2
#define GL_TEXTURE3                       0x84C3
#define GL_TEXTURE4                       0x84C4
#define GL_TEXTURE5                       0x84C5
#define GL_TEXTURE6                       0x84C6
#define GL_TEXTURE7                       0x84C7
#define GL_TEXTURE8                       0x84C8
#define GL_TEXTURE9                       0x84C9
#define GL_TEXTURE10                      0x84CA
#define GL_TEXTURE11                      0x84CB
#define GL_TEXTURE12                      0x84CC
#define GL_TEXTURE13                      0x84CD
#define GL_TEXTURE14                      0x84CE
#define GL_TEXTURE15                      0x84CF
#define GL_TEXTURE16                      0x84D0
#define GL_TEXTURE17                      0x84D1
#define GL_TEXTURE18                      0x84D2
#define GL_TEXTURE19                      0x84D3
#define GL_TEXTURE20                      0x84D4
#define GL_TEXTURE21                      0x84D5
#define GL_TEXTURE22                      0x84D6
#define GL_TEXTURE23                      0x84D7
#define GL_TEXTURE24                      0x84D8
#define GL_TEXTURE25                      0x84D9
#define GL_TEXTURE26                      0x84DA
#define GL_TEXTURE27                      0x84DB
#define GL_TEXTURE28                      0x84DC
#define GL_TEXTURE29                      0x84DD
#define GL_TEXTURE30                      0x84DE
#define GL_TEXTURE31                      0x84DF
#define GL_ACTIVE_TEXTURE                 0x84E0
#define GL_MULTISAMPLE                    0x809D
#define GL_SAMPLE_ALPHA_TO_COVERAGE       0x809E
#define GL_SAMPLE_ALPHA_TO_ONE            0x809F
#define GL_SAMPLE_COVERAGE                0x80A0
#define GL_SAMPLE_BUFFERS                 0x80A8
#define GL_SAMPLES                        0x80A9
#define GL_SAMPLE_COVERAGE_VALUE          0x80AA
#define GL_SAMPLE_COVERAGE_INVERT         0x80AB
#define GL_TEXTURE_CUBE_MAP               0x8513
#define GL_TEXTURE_BINDING_CUBE_MAP       0x8514
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X    0x8515
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X    0x8516
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y    0x8517
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y    0x8518
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z    0x8519
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z    0x851A
#define GL_PROXY_TEXTURE_CUBE_MAP         0x851B
#define GL_MAX_CUBE_MAP_TEXTURE_SIZE      0x851C
#define GL_COMPRESSED_RGB                 0x84ED
#define GL_COMPRESSED_RGBA                0x84EE
#define GL_TEXTURE_COMPRESSION_HINT       0x84EF
#define GL_TEXTURE_COMPRESSED_IMAGE_SIZE  0x86A0
#define GL_TEXTURE_COMPRESSED             0x86A1
#define GL_NUM_COMPRESSED_TEXTURE_FORMATS 0x86A2
#define GL_COMPRESSED_TEXTURE_FORMATS     0x86A3
#define GL_CLAMP_TO_BORDER                0x812D

// OpenGL 1.4
#define GL_BLEND_DST_RGB                  0x80C8
#define GL_BLEND_SRC_RGB                  0x80C9
#define GL_BLEND_DST_ALPHA                0x80CA
#define GL_BLEND_SRC_ALPHA                0x80CB
#define GL_POINT_FADE_THRESHOLD_SIZE      0x8128
#define GL_DEPTH_COMPONENT16              0x81A5
#define GL_DEPTH_COMPONENT24              0x81A6
#define GL_DEPTH_COMPONENT32              0x81A7
#define GL_MIRRORED_REPEAT                0x8370
#define GL_MAX_TEXTURE_LOD_BIAS           0x84FD
#define GL_TEXTURE_LOD_BIAS               0x8501
#define GL_INCR_WRAP                      0x8507
#define GL_DECR_WRAP                      0x8508
#define GL_TEXTURE_DEPTH_SIZE             0x884A
#define GL_TEXTURE_COMPARE_MODE           0x884C
#define GL_TEXTURE_COMPARE_FUNC           0x884D

// OpenGL 1.5
#define GL_BUFFER_SIZE                    0x8764
#define GL_BUFFER_USAGE                   0x8765
#define GL_QUERY_COUNTER_BITS             0x8864
#define GL_CURRENT_QUERY                  0x8865
#define GL_QUERY_RESULT                   0x8866
#define GL_QUERY_RESULT_AVAILABLE         0x8867
#define GL_ARRAY_BUFFER                   0x8892
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_ARRAY_BUFFER_BINDING           0x8894
#define GL_ELEMENT_ARRAY_BUFFER_BINDING   0x8895
#define GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING 0x889F
#define GL_READ_ONLY                      0x88B8
#define GL_WRITE_ONLY                     0x88B9
#define GL_READ_WRITE                     0x88BA
#define GL_BUFFER_ACCESS                  0x88BB
#define GL_BUFFER_MAPPED                  0x88BC
#define GL_BUFFER_MAP_POINTER             0x88BD
#define GL_STREAM_DRAW                    0x88E0
#define GL_STREAM_READ                    0x88E1
#define GL_STREAM_COPY                    0x88E2
#define GL_STATIC_DRAW                    0x88E4
#define GL_STATIC_READ                    0x88E5
#define GL_STATIC_COPY                    0x88E6
#define GL_DYNAMIC_DRAW                   0x88E8
#define GL_DYNAMIC_READ                   0x88E9
#define GL_DYNAMIC_COPY                   0x88EA
#define GL_SAMPLES_PASSED                 0x8914

// vertex array
#define GL_VERTEX_ARRAY                   0x8074
#define GL_NORMAL_ARRAY                   0x8075
#define GL_COLOR_ARRAY                    0x8076
#define GL_INDEX_ARRAY                    0x8077
#define GL_TEXTURE_COORD_ARRAY            0x8078
#define GL_EDGE_FLAG_ARRAY                0x8079
#define GL_VERTEX_ARRAY_SIZE              0x807A
#define GL_VERTEX_ARRAY_TYPE              0x807B
#define GL_VERTEX_ARRAY_STRIDE            0x807C
#define GL_NORMAL_ARRAY_TYPE              0x807E
#define GL_NORMAL_ARRAY_STRIDE            0x807F
#define GL_COLOR_ARRAY_SIZE               0x8081
#define GL_COLOR_ARRAY_TYPE               0x8082
#define GL_COLOR_ARRAY_STRIDE             0x8083
#define GL_INDEX_ARRAY_TYPE               0x8085
#define GL_INDEX_ARRAY_STRIDE             0x8086
#define GL_TEXTURE_COORD_ARRAY_SIZE       0x8088
#define GL_TEXTURE_COORD_ARRAY_TYPE       0x8089
#define GL_TEXTURE_COORD_ARRAY_STRIDE     0x808A
#define GL_EDGE_FLAG_ARRAY_STRIDE         0x808C
#define GL_VERTEX_ARRAY_POINTER           0x808E
#define GL_NORMAL_ARRAY_POINTER           0x808F
#define GL_COLOR_ARRAY_POINTER            0x8090
#define GL_INDEX_ARRAY_POINTER            0x8091
#define GL_TEXTURE_COORD_ARRAY_POINTER    0x8092
#define GL_EDGE_FLAG_ARRAY_POINTER        0x8093
#define GL_V2F                            0x2A20
#define GL_V3F                            0x2A21
#define GL_C4UB_V2F                       0x2A22
#define GL_C4UB_V3F                       0x2A23
#define GL_C3F_V3F                        0x2A24
#define GL_N3F_V3F                        0x2A25
#define GL_C4F_N3F_V3F                    0x2A26
#define GL_T2F_V3F                        0x2A27
#define GL_T4F_V4F                        0x2A28
#define GL_T2F_C4UB_V3F                   0x2A29
#define GL_T2F_C3F_V3F                    0x2A2A
#define GL_T2F_N3F_V3F                    0x2A2B
#define GL_T2F_C4F_N3F_V3F                0x2A2C
#define GL_T4F_C4F_N3F_V4F                0x2A2D

