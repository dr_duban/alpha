/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "..\OpenGL.h"


#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; P##n n = 0
#include "OGL_43.h"

//stGL41config clOpenGL::_c41;

void OpenGL::Core::Initialize_41() {

// OpenGL 2.0 ==============================================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(BlendEquationSeparate);
	OPENGL_FUNC_MAP(DrawBuffers);
	OPENGL_FUNC_MAP(StencilOpSeparate);
	OPENGL_FUNC_MAP(StencilFuncSeparate);
	OPENGL_FUNC_MAP(StencilMaskSeparate);
	OPENGL_FUNC_MAP(AttachShader);
	OPENGL_FUNC_MAP(BindAttribLocation);
	OPENGL_FUNC_MAP(CompileShader);
	OPENGL_FUNC_MAP(CreateProgram);
	OPENGL_FUNC_MAP(CreateShader);
	OPENGL_FUNC_MAP(DeleteProgram);
	OPENGL_FUNC_MAP(DeleteShader);
	OPENGL_FUNC_MAP(DetachShader);
	OPENGL_FUNC_MAP(DisableVertexAttribArray);
	OPENGL_FUNC_MAP(EnableVertexAttribArray);
	OPENGL_FUNC_MAP(GetActiveAttrib);
	OPENGL_FUNC_MAP(GetActiveUniform);
	OPENGL_FUNC_MAP(GetAttachedShaders);
	OPENGL_FUNC_MAP(GetAttribLocation);
	OPENGL_FUNC_MAP(GetProgramiv);
	OPENGL_FUNC_MAP(GetProgramInfoLog);
	OPENGL_FUNC_MAP(GetShaderiv);
	OPENGL_FUNC_MAP(GetShaderInfoLog);
	OPENGL_FUNC_MAP(GetShaderSource);
	OPENGL_FUNC_MAP(GetUniformLocation);
	OPENGL_FUNC_MAP(GetUniformfv);
	OPENGL_FUNC_MAP(GetUniformiv);
	OPENGL_FUNC_MAP(GetVertexAttribdv);
	OPENGL_FUNC_MAP(GetVertexAttribfv);
	OPENGL_FUNC_MAP(GetVertexAttribiv);
	OPENGL_FUNC_MAP(GetVertexAttribPointerv);
	OPENGL_FUNC_MAP(IsProgram);
	OPENGL_FUNC_MAP(IsShader);
	OPENGL_FUNC_MAP(LinkProgram);
	OPENGL_FUNC_MAP(ShaderSource);
	OPENGL_FUNC_MAP(UseProgram);
	OPENGL_FUNC_MAP(Uniform1f);
	OPENGL_FUNC_MAP(Uniform2f);
	OPENGL_FUNC_MAP(Uniform3f);
	OPENGL_FUNC_MAP(Uniform4f);
	OPENGL_FUNC_MAP(Uniform1i);
	OPENGL_FUNC_MAP(Uniform2i);
	OPENGL_FUNC_MAP(Uniform3i);
	OPENGL_FUNC_MAP(Uniform4i);
	OPENGL_FUNC_MAP(Uniform1fv);
	OPENGL_FUNC_MAP(Uniform2fv);
	OPENGL_FUNC_MAP(Uniform3fv);
	OPENGL_FUNC_MAP(Uniform4fv);
	OPENGL_FUNC_MAP(Uniform1iv);
	OPENGL_FUNC_MAP(Uniform2iv);
	OPENGL_FUNC_MAP(Uniform3iv);
	OPENGL_FUNC_MAP(Uniform4iv);
	OPENGL_FUNC_MAP(UniformMatrix2fv);
	OPENGL_FUNC_MAP(UniformMatrix3fv);
	OPENGL_FUNC_MAP(UniformMatrix4fv);
	OPENGL_FUNC_MAP(ValidateProgram);
	OPENGL_FUNC_MAP(VertexAttrib1d);
	OPENGL_FUNC_MAP(VertexAttrib1dv);
	OPENGL_FUNC_MAP(VertexAttrib1f);
	OPENGL_FUNC_MAP(VertexAttrib1fv);
	OPENGL_FUNC_MAP(VertexAttrib1s);
	OPENGL_FUNC_MAP(VertexAttrib1sv);
	OPENGL_FUNC_MAP(VertexAttrib2d);
	OPENGL_FUNC_MAP(VertexAttrib2dv);
	OPENGL_FUNC_MAP(VertexAttrib2f);
	OPENGL_FUNC_MAP(VertexAttrib2fv);
	OPENGL_FUNC_MAP(VertexAttrib2s);
	OPENGL_FUNC_MAP(VertexAttrib2sv);
	OPENGL_FUNC_MAP(VertexAttrib3d);
	OPENGL_FUNC_MAP(VertexAttrib3dv);
	OPENGL_FUNC_MAP(VertexAttrib3f);
	OPENGL_FUNC_MAP(VertexAttrib3fv);
	OPENGL_FUNC_MAP(VertexAttrib3s);
	OPENGL_FUNC_MAP(VertexAttrib3sv);
	OPENGL_FUNC_MAP(VertexAttrib4Nbv);
	OPENGL_FUNC_MAP(VertexAttrib4Niv);
	OPENGL_FUNC_MAP(VertexAttrib4Nsv);
	OPENGL_FUNC_MAP(VertexAttrib4Nub);
	OPENGL_FUNC_MAP(VertexAttrib4Nubv);
	OPENGL_FUNC_MAP(VertexAttrib4Nuiv);
	OPENGL_FUNC_MAP(VertexAttrib4Nusv);
	OPENGL_FUNC_MAP(VertexAttrib4bv);
	OPENGL_FUNC_MAP(VertexAttrib4d);
	OPENGL_FUNC_MAP(VertexAttrib4dv);
	OPENGL_FUNC_MAP(VertexAttrib4f);
	OPENGL_FUNC_MAP(VertexAttrib4fv);
	OPENGL_FUNC_MAP(VertexAttrib4iv);
	OPENGL_FUNC_MAP(VertexAttrib4s);
	OPENGL_FUNC_MAP(VertexAttrib4sv);
	OPENGL_FUNC_MAP(VertexAttrib4ubv);
	OPENGL_FUNC_MAP(VertexAttrib4uiv);
	OPENGL_FUNC_MAP(VertexAttrib4usv);
	OPENGL_FUNC_MAP(VertexAttribPointer);

	if (BlendEquationSeparate || DrawBuffers || StencilOpSeparate || StencilFuncSeparate || StencilMaskSeparate || AttachShader || BindAttribLocation || CompileShader || CreateProgram || CreateShader || DeleteProgram ||
		DeleteShader || DetachShader || DisableVertexAttribArray || EnableVertexAttribArray || GetActiveAttrib || GetActiveUniform || GetAttachedShaders || GetAttribLocation || GetProgramiv || GetProgramInfoLog ||
		GetShaderiv || GetShaderInfoLog || GetShaderSource || GetUniformLocation || GetUniformfv || GetUniformiv || GetVertexAttribdv || GetVertexAttribfv || GetVertexAttribiv || GetVertexAttribPointerv || IsProgram ||
		IsShader || LinkProgram || ShaderSource || UseProgram || Uniform1f || Uniform2f || Uniform3f || Uniform4f || Uniform1i || Uniform2i || Uniform3i || Uniform4i || Uniform1fv || Uniform2fv || Uniform3fv ||
		Uniform4fv || Uniform1iv || Uniform2iv || Uniform3iv || Uniform4iv || UniformMatrix2fv || UniformMatrix3fv || UniformMatrix4fv || ValidateProgram || VertexAttrib1d || VertexAttrib1dv || VertexAttrib1f ||
		VertexAttrib1fv || VertexAttrib1s || VertexAttrib1sv || VertexAttrib2d || VertexAttrib2dv || VertexAttrib2f || VertexAttrib2fv || VertexAttrib2s || VertexAttrib2sv || VertexAttrib3d || VertexAttrib3dv || VertexAttrib3f ||
		VertexAttrib3fv || VertexAttrib3s || VertexAttrib3sv || VertexAttrib4Nbv || VertexAttrib4Niv || VertexAttrib4Nsv || VertexAttrib4Nub || VertexAttrib4Nubv || VertexAttrib4Nuiv || VertexAttrib4Nusv || VertexAttrib4bv ||
		VertexAttrib4d || VertexAttrib4dv || VertexAttrib4f || VertexAttrib4fv || VertexAttrib4iv || VertexAttrib4s || VertexAttrib4sv || VertexAttrib4ubv || VertexAttrib4uiv || VertexAttrib4usv || VertexAttribPointer)

		_version = 2000;


// Open 2.1 ========================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(UniformMatrix2x3fv);
	OPENGL_FUNC_MAP(UniformMatrix3x2fv);
	OPENGL_FUNC_MAP(UniformMatrix2x4fv);
	OPENGL_FUNC_MAP(UniformMatrix4x2fv);
	OPENGL_FUNC_MAP(UniformMatrix3x4fv);
	OPENGL_FUNC_MAP(UniformMatrix4x3fv);

	if (UniformMatrix2x3fv || UniformMatrix3x2fv || UniformMatrix2x4fv || UniformMatrix4x2fv || UniformMatrix3x4fv || UniformMatrix4x3fv) _version = 2100;


// OpenGL 3.0 ========================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(ColorMaski);
	OPENGL_FUNC_MAP(GetBooleani_v);
	OPENGL_FUNC_MAP(GetIntegeri_v);
	OPENGL_FUNC_MAP(Enablei);
	OPENGL_FUNC_MAP(Disablei);
	OPENGL_FUNC_MAP(IsEnabledi);
	OPENGL_FUNC_MAP(BeginTransformFeedback);
	OPENGL_FUNC_MAP(EndTransformFeedback);
	OPENGL_FUNC_MAP(BindBufferRange);
	OPENGL_FUNC_MAP(BindBufferBase);
	OPENGL_FUNC_MAP(TransformFeedbackVaryings);
	OPENGL_FUNC_MAP(GetTransformFeedbackVarying);
	OPENGL_FUNC_MAP(ClampColor);
	OPENGL_FUNC_MAP(BeginConditionalRender);
	OPENGL_FUNC_MAP(EndConditionalRender);
	OPENGL_FUNC_MAP(VertexAttribIPointer);
	OPENGL_FUNC_MAP(GetVertexAttribIiv);
	OPENGL_FUNC_MAP(GetVertexAttribIuiv);
	OPENGL_FUNC_MAP(VertexAttribI1i);
	OPENGL_FUNC_MAP(VertexAttribI2i);
	OPENGL_FUNC_MAP(VertexAttribI3i);
	OPENGL_FUNC_MAP(VertexAttribI4i);
	OPENGL_FUNC_MAP(VertexAttribI1ui);
	OPENGL_FUNC_MAP(VertexAttribI2ui);
	OPENGL_FUNC_MAP(VertexAttribI3ui);
	OPENGL_FUNC_MAP(VertexAttribI4ui);
	OPENGL_FUNC_MAP(VertexAttribI1iv);
	OPENGL_FUNC_MAP(VertexAttribI2iv);
	OPENGL_FUNC_MAP(VertexAttribI3iv);
	OPENGL_FUNC_MAP(VertexAttribI4iv);
	OPENGL_FUNC_MAP(VertexAttribI1uiv);
	OPENGL_FUNC_MAP(VertexAttribI2uiv);
	OPENGL_FUNC_MAP(VertexAttribI3uiv);
	OPENGL_FUNC_MAP(VertexAttribI4uiv);
	OPENGL_FUNC_MAP(VertexAttribI4bv);
	OPENGL_FUNC_MAP(VertexAttribI4sv);
	OPENGL_FUNC_MAP(VertexAttribI4ubv);
	OPENGL_FUNC_MAP(VertexAttribI4usv);
	OPENGL_FUNC_MAP(GetUniformuiv);
	OPENGL_FUNC_MAP(BindFragDataLocation);
	OPENGL_FUNC_MAP(GetFragDataLocation);
	OPENGL_FUNC_MAP(Uniform1ui);
	OPENGL_FUNC_MAP(Uniform2ui);
	OPENGL_FUNC_MAP(Uniform3ui);
	OPENGL_FUNC_MAP(Uniform4ui);
	OPENGL_FUNC_MAP(Uniform1uiv);
	OPENGL_FUNC_MAP(Uniform2uiv);
	OPENGL_FUNC_MAP(Uniform3uiv);
	OPENGL_FUNC_MAP(Uniform4uiv);
	OPENGL_FUNC_MAP(TexParameterIiv);
	OPENGL_FUNC_MAP(TexParameterIuiv);
	OPENGL_FUNC_MAP(GetTexParameterIiv);
	OPENGL_FUNC_MAP(GetTexParameterIuiv);
	OPENGL_FUNC_MAP(ClearBufferiv);
	OPENGL_FUNC_MAP(ClearBufferuiv);
	OPENGL_FUNC_MAP(ClearBufferfv);
	OPENGL_FUNC_MAP(ClearBufferfi);
	OPENGL_FUNC_MAP(GetStringi);
	OPENGL_FUNC_MAP(IsRenderbuffer);
	OPENGL_FUNC_MAP(BindRenderbuffer);
	OPENGL_FUNC_MAP(DeleteRenderbuffers);
	OPENGL_FUNC_MAP(GenRenderbuffers);
	OPENGL_FUNC_MAP(RenderbufferStorage);
	OPENGL_FUNC_MAP(GetRenderbufferParameteriv);
	OPENGL_FUNC_MAP(IsFramebuffer);
	OPENGL_FUNC_MAP(BindFramebuffer);
	OPENGL_FUNC_MAP(DeleteFramebuffers);
	OPENGL_FUNC_MAP(GenFramebuffers);
	OPENGL_FUNC_MAP(CheckFramebufferStatus);
	OPENGL_FUNC_MAP(FramebufferTexture1D);
	OPENGL_FUNC_MAP(FramebufferTexture2D);
	OPENGL_FUNC_MAP(FramebufferTexture3D);
	OPENGL_FUNC_MAP(FramebufferRenderbuffer);
	OPENGL_FUNC_MAP(GetFramebufferAttachmentParameteriv);
	OPENGL_FUNC_MAP(GenerateMipmap);
	OPENGL_FUNC_MAP(BlitFramebuffer);
	OPENGL_FUNC_MAP(RenderbufferStorageMultisample);
	OPENGL_FUNC_MAP(FramebufferTextureLayer);
	OPENGL_FUNC_MAP(MapBufferRange);
	OPENGL_FUNC_MAP(FlushMappedBufferRange);
	OPENGL_FUNC_MAP(BindVertexArray);
	OPENGL_FUNC_MAP(DeleteVertexArrays);
	OPENGL_FUNC_MAP(GenVertexArrays);
	OPENGL_FUNC_MAP(IsVertexArray);

	if (ColorMaski || GetBooleani_v || GetIntegeri_v || Enablei || Disablei || IsEnabledi || BeginTransformFeedback || EndTransformFeedback || BindBufferRange || BindBufferBase || TransformFeedbackVaryings || GetTransformFeedbackVarying ||
		ClampColor || BeginConditionalRender || EndConditionalRender || VertexAttribIPointer || GetVertexAttribIiv || GetVertexAttribIuiv || VertexAttribI1i || VertexAttribI2i || VertexAttribI3i || VertexAttribI4i || VertexAttribI1ui ||
		VertexAttribI2ui || VertexAttribI3ui || VertexAttribI4ui || VertexAttribI1iv || VertexAttribI2iv || VertexAttribI3iv || VertexAttribI4iv || VertexAttribI1uiv || VertexAttribI2uiv || VertexAttribI3uiv || VertexAttribI4uiv ||
		VertexAttribI4bv || VertexAttribI4sv || VertexAttribI4ubv || VertexAttribI4usv || GetUniformuiv || BindFragDataLocation || GetFragDataLocation || Uniform1ui || Uniform2ui || Uniform3ui || Uniform4ui || Uniform1uiv || Uniform2uiv ||
		Uniform3uiv || Uniform4uiv || TexParameterIiv || TexParameterIuiv || GetTexParameterIiv || GetTexParameterIuiv || ClearBufferiv || ClearBufferuiv || ClearBufferfv || ClearBufferfi || GetStringi || IsRenderbuffer || BindRenderbuffer ||
		DeleteRenderbuffers || GenRenderbuffers || RenderbufferStorage || GetRenderbufferParameteriv || IsFramebuffer || BindFramebuffer || DeleteFramebuffers || GenFramebuffers || CheckFramebufferStatus || FramebufferTexture1D ||
		FramebufferTexture2D || FramebufferTexture3D || FramebufferRenderbuffer || GetFramebufferAttachmentParameteriv || GenerateMipmap || BlitFramebuffer || RenderbufferStorageMultisample || FramebufferTextureLayer || MapBufferRange ||
		FlushMappedBufferRange || BindVertexArray || DeleteVertexArrays || GenVertexArrays || IsVertexArray)

		_version = 3000;


// OpenGL 3.1 ======================================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(DrawArraysInstanced);
	OPENGL_FUNC_MAP(DrawElementsInstanced);
	OPENGL_FUNC_MAP(TexBuffer);
	OPENGL_FUNC_MAP(PrimitiveRestartIndex);
	OPENGL_FUNC_MAP(CopyBufferSubData);
	OPENGL_FUNC_MAP(GetUniformIndices);
	OPENGL_FUNC_MAP(GetActiveUniformsiv);
	OPENGL_FUNC_MAP(GetActiveUniformName);
	OPENGL_FUNC_MAP(GetUniformBlockIndex);
	OPENGL_FUNC_MAP(GetActiveUniformBlockiv);
	OPENGL_FUNC_MAP(GetActiveUniformBlockName);
	OPENGL_FUNC_MAP(UniformBlockBinding);

	if (DrawArraysInstanced || DrawElementsInstanced || TexBuffer || PrimitiveRestartIndex || CopyBufferSubData || GetUniformIndices || GetActiveUniformsiv || GetActiveUniformName || GetUniformBlockIndex ||
		GetActiveUniformBlockiv || GetActiveUniformBlockName || UniformBlockBinding)

		_version = 3100;


// OpenGL 3.2 ================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(GetInteger64i_v);
	OPENGL_FUNC_MAP(GetBufferParameteri64v);
	OPENGL_FUNC_MAP(FramebufferTexture);
	OPENGL_FUNC_MAP(DrawElementsBaseVertex);
	OPENGL_FUNC_MAP(DrawRangeElementsBaseVertex);
	OPENGL_FUNC_MAP(DrawElementsInstancedBaseVertex);
	OPENGL_FUNC_MAP(MultiDrawElementsBaseVertex);
	OPENGL_FUNC_MAP(ProvokingVertex);
	OPENGL_FUNC_MAP(FenceSync);
	OPENGL_FUNC_MAP(IsSync);
	OPENGL_FUNC_MAP(DeleteSync);
	OPENGL_FUNC_MAP(ClientWaitSync);
	OPENGL_FUNC_MAP(WaitSync);
	OPENGL_FUNC_MAP(GetInteger64v);
	OPENGL_FUNC_MAP(GetSynciv);
	OPENGL_FUNC_MAP(TexImage2DMultisample);
	OPENGL_FUNC_MAP(TexImage3DMultisample);
	OPENGL_FUNC_MAP(GetMultisamplefv);
	OPENGL_FUNC_MAP(SampleMaski);

	if (GetInteger64i_v || GetBufferParameteri64v || FramebufferTexture || DrawElementsBaseVertex || DrawRangeElementsBaseVertex || DrawElementsInstancedBaseVertex || MultiDrawElementsBaseVertex || ProvokingVertex || FenceSync || IsSync ||
		DeleteSync || ClientWaitSync || WaitSync || GetInteger64v || GetSynciv || TexImage2DMultisample || TexImage3DMultisample || GetMultisamplefv || SampleMaski)

		_version = 3200;

// OpenGL 3.3 ==================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(VertexAttribDivisor);
	OPENGL_FUNC_MAP(BindFragDataLocationIndexed);
	OPENGL_FUNC_MAP(GetFragDataIndex);
	OPENGL_FUNC_MAP(GenSamplers);
	OPENGL_FUNC_MAP(DeleteSamplers);
	OPENGL_FUNC_MAP(IsSampler);
	OPENGL_FUNC_MAP(BindSampler);
	OPENGL_FUNC_MAP(SamplerParameteri);
	OPENGL_FUNC_MAP(SamplerParameteriv);
	OPENGL_FUNC_MAP(SamplerParameterf);
	OPENGL_FUNC_MAP(SamplerParameterfv);
	OPENGL_FUNC_MAP(SamplerParameterIiv);
	OPENGL_FUNC_MAP(SamplerParameterIuiv);
	OPENGL_FUNC_MAP(GetSamplerParameteriv);
	OPENGL_FUNC_MAP(GetSamplerParameterIiv);
	OPENGL_FUNC_MAP(GetSamplerParameterfv);
	OPENGL_FUNC_MAP(GetSamplerParameterIuiv);
	OPENGL_FUNC_MAP(QueryCounter);
	OPENGL_FUNC_MAP(GetQueryObjecti64v);
	OPENGL_FUNC_MAP(GetQueryObjectui64v);
	OPENGL_FUNC_MAP(VertexP2ui);
	OPENGL_FUNC_MAP(VertexP2uiv);
	OPENGL_FUNC_MAP(VertexP3ui);
	OPENGL_FUNC_MAP(VertexP3uiv);
	OPENGL_FUNC_MAP(VertexP4ui);
	OPENGL_FUNC_MAP(VertexP4uiv);
	OPENGL_FUNC_MAP(TexCoordP1ui);
	OPENGL_FUNC_MAP(TexCoordP1uiv);
	OPENGL_FUNC_MAP(TexCoordP2ui);
	OPENGL_FUNC_MAP(TexCoordP2uiv);
	OPENGL_FUNC_MAP(TexCoordP3ui);
	OPENGL_FUNC_MAP(TexCoordP3uiv);
	OPENGL_FUNC_MAP(TexCoordP4ui);
	OPENGL_FUNC_MAP(TexCoordP4uiv);
	OPENGL_FUNC_MAP(MultiTexCoordP1ui);
	OPENGL_FUNC_MAP(MultiTexCoordP1uiv);
	OPENGL_FUNC_MAP(MultiTexCoordP2ui);
	OPENGL_FUNC_MAP(MultiTexCoordP2uiv);
	OPENGL_FUNC_MAP(MultiTexCoordP3ui);
	OPENGL_FUNC_MAP(MultiTexCoordP3uiv);
	OPENGL_FUNC_MAP(MultiTexCoordP4ui);
	OPENGL_FUNC_MAP(MultiTexCoordP4uiv);
	OPENGL_FUNC_MAP(NormalP3ui);
	OPENGL_FUNC_MAP(NormalP3uiv);
	OPENGL_FUNC_MAP(ColorP3ui);
	OPENGL_FUNC_MAP(ColorP3uiv);
	OPENGL_FUNC_MAP(ColorP4ui);
	OPENGL_FUNC_MAP(ColorP4uiv);
	OPENGL_FUNC_MAP(SecondaryColorP3ui);
	OPENGL_FUNC_MAP(SecondaryColorP3uiv);
	OPENGL_FUNC_MAP(VertexAttribP1ui);
	OPENGL_FUNC_MAP(VertexAttribP1uiv);
	OPENGL_FUNC_MAP(VertexAttribP2ui);
	OPENGL_FUNC_MAP(VertexAttribP2uiv);
	OPENGL_FUNC_MAP(VertexAttribP3ui);
	OPENGL_FUNC_MAP(VertexAttribP3uiv);
	OPENGL_FUNC_MAP(VertexAttribP4ui);
	OPENGL_FUNC_MAP(VertexAttribP4uiv);

	if (VertexAttribDivisor || BindFragDataLocationIndexed || GetFragDataIndex || GenSamplers || DeleteSamplers || IsSampler || BindSampler || SamplerParameteri || SamplerParameteriv || SamplerParameterf || SamplerParameterfv ||
		SamplerParameterIiv || SamplerParameterIuiv || GetSamplerParameteriv || GetSamplerParameterIiv || GetSamplerParameterfv || GetSamplerParameterIuiv || QueryCounter || GetQueryObjecti64v || GetQueryObjectui64v || VertexP2ui ||
		VertexP2uiv || VertexP3ui || VertexP3uiv || VertexP4ui || VertexP4uiv || TexCoordP1ui || TexCoordP1uiv || TexCoordP2ui || TexCoordP2uiv || TexCoordP3ui || TexCoordP3uiv || TexCoordP4ui || TexCoordP4uiv || MultiTexCoordP1ui ||
		MultiTexCoordP1uiv || MultiTexCoordP2ui || MultiTexCoordP2uiv || MultiTexCoordP3ui || MultiTexCoordP3uiv || MultiTexCoordP4ui || MultiTexCoordP4uiv || NormalP3ui || NormalP3uiv || ColorP3ui || ColorP3uiv || ColorP4ui || ColorP4uiv ||
		SecondaryColorP3ui || SecondaryColorP3uiv || VertexAttribP1ui || VertexAttribP1uiv || VertexAttribP2ui || VertexAttribP2uiv || VertexAttribP3ui || VertexAttribP3uiv || VertexAttribP4ui || VertexAttribP4uiv)

		_version = 3300;


// OpenGL 4.0 ==================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(MinSampleShading);
	OPENGL_FUNC_MAP(BlendEquationi);
	OPENGL_FUNC_MAP(BlendEquationSeparatei);
	OPENGL_FUNC_MAP(BlendFunci);
	OPENGL_FUNC_MAP(BlendFuncSeparatei);
	OPENGL_FUNC_MAP(DrawArraysIndirect);
	OPENGL_FUNC_MAP(DrawElementsIndirect);
	OPENGL_FUNC_MAP(Uniform1d);
	OPENGL_FUNC_MAP(Uniform2d);
	OPENGL_FUNC_MAP(Uniform3d);
	OPENGL_FUNC_MAP(Uniform4d);
	OPENGL_FUNC_MAP(Uniform1dv);
	OPENGL_FUNC_MAP(Uniform2dv);
	OPENGL_FUNC_MAP(Uniform3dv);
	OPENGL_FUNC_MAP(Uniform4dv);
	OPENGL_FUNC_MAP(UniformMatrix2dv);
	OPENGL_FUNC_MAP(UniformMatrix3dv);
	OPENGL_FUNC_MAP(UniformMatrix4dv);
	OPENGL_FUNC_MAP(UniformMatrix2x3dv);
	OPENGL_FUNC_MAP(UniformMatrix2x4dv);
	OPENGL_FUNC_MAP(UniformMatrix3x2dv);
	OPENGL_FUNC_MAP(UniformMatrix3x4dv);
	OPENGL_FUNC_MAP(UniformMatrix4x2dv);
	OPENGL_FUNC_MAP(UniformMatrix4x3dv);
	OPENGL_FUNC_MAP(GetUniformdv);
	OPENGL_FUNC_MAP(GetSubroutineUniformLocation);
	OPENGL_FUNC_MAP(GetSubroutineIndex);
	OPENGL_FUNC_MAP(GetActiveSubroutineUniformiv);
	OPENGL_FUNC_MAP(GetActiveSubroutineUniformName);
	OPENGL_FUNC_MAP(GetActiveSubroutineName);
	OPENGL_FUNC_MAP(UniformSubroutinesuiv);
	OPENGL_FUNC_MAP(GetUniformSubroutineuiv);
	OPENGL_FUNC_MAP(GetProgramStageiv);
	OPENGL_FUNC_MAP(PatchParameteri);
	OPENGL_FUNC_MAP(PatchParameterfv);
	OPENGL_FUNC_MAP(BindTransformFeedback);
	OPENGL_FUNC_MAP(DeleteTransformFeedbacks);
	OPENGL_FUNC_MAP(GenTransformFeedbacks);
	OPENGL_FUNC_MAP(IsTransformFeedback);
	OPENGL_FUNC_MAP(PauseTransformFeedback);
	OPENGL_FUNC_MAP(ResumeTransformFeedback);
	OPENGL_FUNC_MAP(DrawTransformFeedback);
	OPENGL_FUNC_MAP(DrawTransformFeedbackStream);
	OPENGL_FUNC_MAP(BeginQueryIndexed);
	OPENGL_FUNC_MAP(EndQueryIndexed);
	OPENGL_FUNC_MAP(GetQueryIndexediv);

	if (MinSampleShading || BlendEquationi || BlendEquationSeparatei || BlendFunci || BlendFuncSeparatei || DrawArraysIndirect || DrawElementsIndirect || Uniform1d || Uniform2d || Uniform3d || Uniform4d || Uniform1dv || Uniform2dv ||
		Uniform3dv || Uniform4dv || UniformMatrix2dv || UniformMatrix3dv || UniformMatrix4dv || UniformMatrix2x3dv || UniformMatrix2x4dv || UniformMatrix3x2dv || UniformMatrix3x4dv || UniformMatrix4x2dv || UniformMatrix4x3dv ||
		GetUniformdv || GetSubroutineUniformLocation || GetSubroutineIndex || GetActiveSubroutineUniformiv || GetActiveSubroutineUniformName || GetActiveSubroutineName || UniformSubroutinesuiv || GetUniformSubroutineuiv ||
		GetProgramStageiv || PatchParameteri || PatchParameterfv || BindTransformFeedback || DeleteTransformFeedbacks || GenTransformFeedbacks || IsTransformFeedback || PauseTransformFeedback || ResumeTransformFeedback ||
		DrawTransformFeedback || DrawTransformFeedbackStream || BeginQueryIndexed || EndQueryIndexed || GetQueryIndexediv)

		_version = 4000;


// OpenGL 4.1 ===========================================================================================================================================================================================================
	OPENGL_FUNC_MAP(ReleaseShaderCompiler);
	OPENGL_FUNC_MAP(ShaderBinary);
	OPENGL_FUNC_MAP(GetShaderPrecisionFormat);
	OPENGL_FUNC_MAP(DepthRangef);
	OPENGL_FUNC_MAP(ClearDepthf);
	OPENGL_FUNC_MAP(GetProgramBinary);
	OPENGL_FUNC_MAP(ProgramBinary);
	OPENGL_FUNC_MAP(ProgramParameteri);
	OPENGL_FUNC_MAP(UseProgramStages);
	OPENGL_FUNC_MAP(ActiveShaderProgram);
	OPENGL_FUNC_MAP(CreateShaderProgramv);
	OPENGL_FUNC_MAP(BindProgramPipeline);
	OPENGL_FUNC_MAP(DeleteProgramPipelines);
	OPENGL_FUNC_MAP(GenProgramPipelines);
	OPENGL_FUNC_MAP(IsProgramPipeline);
	OPENGL_FUNC_MAP(GetProgramPipelineiv);
	OPENGL_FUNC_MAP(ProgramUniform1i);
	OPENGL_FUNC_MAP(ProgramUniform1iv);
	OPENGL_FUNC_MAP(ProgramUniform1f);
	OPENGL_FUNC_MAP(ProgramUniform1fv);
	OPENGL_FUNC_MAP(ProgramUniform1d);
	OPENGL_FUNC_MAP(ProgramUniform1dv);
	OPENGL_FUNC_MAP(ProgramUniform1ui);
	OPENGL_FUNC_MAP(ProgramUniform1uiv);
	OPENGL_FUNC_MAP(ProgramUniform2i);
	OPENGL_FUNC_MAP(ProgramUniform2iv);
	OPENGL_FUNC_MAP(ProgramUniform2f);
	OPENGL_FUNC_MAP(ProgramUniform2fv);
	OPENGL_FUNC_MAP(ProgramUniform2d);
	OPENGL_FUNC_MAP(ProgramUniform2dv);
	OPENGL_FUNC_MAP(ProgramUniform2ui);
	OPENGL_FUNC_MAP(ProgramUniform2uiv);
	OPENGL_FUNC_MAP(ProgramUniform3i);
	OPENGL_FUNC_MAP(ProgramUniform3iv);
	OPENGL_FUNC_MAP(ProgramUniform3f);
	OPENGL_FUNC_MAP(ProgramUniform3fv);
	OPENGL_FUNC_MAP(ProgramUniform3d);
	OPENGL_FUNC_MAP(ProgramUniform3dv);
	OPENGL_FUNC_MAP(ProgramUniform3ui);
	OPENGL_FUNC_MAP(ProgramUniform3uiv);
	OPENGL_FUNC_MAP(ProgramUniform4i);
	OPENGL_FUNC_MAP(ProgramUniform4iv);
	OPENGL_FUNC_MAP(ProgramUniform4f);
	OPENGL_FUNC_MAP(ProgramUniform4fv);
	OPENGL_FUNC_MAP(ProgramUniform4d);
	OPENGL_FUNC_MAP(ProgramUniform4dv);
	OPENGL_FUNC_MAP(ProgramUniform4ui);
	OPENGL_FUNC_MAP(ProgramUniform4uiv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2x3fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3x2fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2x4fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4x2fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3x4fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4x3fv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2x3dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3x2dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix2x4dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4x2dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix3x4dv);
	OPENGL_FUNC_MAP(ProgramUniformMatrix4x3dv);
	OPENGL_FUNC_MAP(ValidateProgramPipeline);
	OPENGL_FUNC_MAP(GetProgramPipelineInfoLog);
	OPENGL_FUNC_MAP(VertexAttribL1d);
	OPENGL_FUNC_MAP(VertexAttribL2d);
	OPENGL_FUNC_MAP(VertexAttribL3d);
	OPENGL_FUNC_MAP(VertexAttribL4d);
	OPENGL_FUNC_MAP(VertexAttribL1dv);
	OPENGL_FUNC_MAP(VertexAttribL2dv);
	OPENGL_FUNC_MAP(VertexAttribL3dv);
	OPENGL_FUNC_MAP(VertexAttribL4dv);
	OPENGL_FUNC_MAP(VertexAttribLPointer);
	OPENGL_FUNC_MAP(GetVertexAttribLdv);
	OPENGL_FUNC_MAP(ViewportArrayv);
	OPENGL_FUNC_MAP(ViewportIndexedf);
	OPENGL_FUNC_MAP(ViewportIndexedfv);
	OPENGL_FUNC_MAP(ScissorArrayv);
	OPENGL_FUNC_MAP(ScissorIndexed);
	OPENGL_FUNC_MAP(ScissorIndexedv);
	OPENGL_FUNC_MAP(DepthRangeArrayv);
	OPENGL_FUNC_MAP(DepthRangeIndexed);
	OPENGL_FUNC_MAP(GetFloati_v);
	OPENGL_FUNC_MAP(GetDoublei_v);

	if (ReleaseShaderCompiler || ShaderBinary || GetShaderPrecisionFormat || DepthRangef || ClearDepthf || GetProgramBinary || ProgramBinary || ProgramParameteri || UseProgramStages || ActiveShaderProgram || CreateShaderProgramv ||
		BindProgramPipeline || DeleteProgramPipelines || GenProgramPipelines || IsProgramPipeline || GetProgramPipelineiv || ProgramUniform1i || ProgramUniform1iv || ProgramUniform1f || ProgramUniform1fv || ProgramUniform1d ||
		ProgramUniform1dv || ProgramUniform1ui || ProgramUniform1uiv || ProgramUniform2i || ProgramUniform2iv || ProgramUniform2f || ProgramUniform2fv || ProgramUniform2d || ProgramUniform2dv || ProgramUniform2ui || ProgramUniform2uiv ||
		ProgramUniform3i || ProgramUniform3iv || ProgramUniform3f || ProgramUniform3fv || ProgramUniform3d || ProgramUniform3dv || ProgramUniform3ui || ProgramUniform3uiv || ProgramUniform4i || ProgramUniform4iv || ProgramUniform4f ||
		ProgramUniform4fv || ProgramUniform4d || ProgramUniform4dv || ProgramUniform4ui || ProgramUniform4uiv || ProgramUniformMatrix2fv || ProgramUniformMatrix3fv || ProgramUniformMatrix4fv || ProgramUniformMatrix2dv || ProgramUniformMatrix3dv ||
		ProgramUniformMatrix4dv || ProgramUniformMatrix2x3fv || ProgramUniformMatrix3x2fv || ProgramUniformMatrix2x4fv || ProgramUniformMatrix4x2fv || ProgramUniformMatrix3x4fv || ProgramUniformMatrix4x3fv || ProgramUniformMatrix2x3dv ||
		ProgramUniformMatrix3x2dv || ProgramUniformMatrix2x4dv || ProgramUniformMatrix4x2dv || ProgramUniformMatrix3x4dv || ProgramUniformMatrix4x3dv || ValidateProgramPipeline || GetProgramPipelineInfoLog || VertexAttribL1d || VertexAttribL2d ||
		VertexAttribL3d || VertexAttribL4d || VertexAttribL1dv || VertexAttribL2dv || VertexAttribL3dv || VertexAttribL4dv || VertexAttribLPointer || GetVertexAttribLdv || ViewportArrayv || ViewportIndexedf || ViewportIndexedfv || ScissorArrayv ||
		ScissorIndexed || ScissorIndexedv || DepthRangeArrayv || DepthRangeIndexed || GetFloati_v || GetDoublei_v)

		_version = 4100;


// OpenGL 4.2 ==========================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(DrawArraysInstancedBaseInstance);
	OPENGL_FUNC_MAP(DrawElementsInstancedBaseInstance);
	OPENGL_FUNC_MAP(DrawElementsInstancedBaseVertexBaseInstance);
	OPENGL_FUNC_MAP(DrawTransformFeedbackInstanced);
	OPENGL_FUNC_MAP(DrawTransformFeedbackStreamInstanced);
	OPENGL_FUNC_MAP(GetInternalformativ);
	OPENGL_FUNC_MAP(GetActiveAtomicCounterBufferiv);
	OPENGL_FUNC_MAP(BindImageTexture);
	OPENGL_FUNC_MAP(MemoryBarrier);
	OPENGL_FUNC_MAP(TexStorage1D);
	OPENGL_FUNC_MAP(TexStorage2D);
	OPENGL_FUNC_MAP(TexStorage3D);
	OPENGL_FUNC_MAP(TextureStorage1DEXT);
	OPENGL_FUNC_MAP(TextureStorage2DEXT);
	OPENGL_FUNC_MAP(TextureStorage3DEXT);

	if (DrawArraysInstancedBaseInstance || DrawElementsInstancedBaseInstance || DrawElementsInstancedBaseVertexBaseInstance || DrawTransformFeedbackInstanced || DrawTransformFeedbackStreamInstanced || GetInternalformativ ||
		GetActiveAtomicCounterBufferiv || BindImageTexture || MemoryBarrier || TexStorage1D || TexStorage2D || TexStorage3D)

		_version = 4200;


// OpenGL 4.3 ==================================================================================================================================================================================================================================
	OPENGL_FUNC_MAP(ClearBufferData);
	OPENGL_FUNC_MAP(ClearBufferSubData);
	OPENGL_FUNC_MAP(ClearNamedBufferDataEXT);
	OPENGL_FUNC_MAP(ClearNamedBufferSubDataEXT);
	OPENGL_FUNC_MAP(DispatchCompute);
	OPENGL_FUNC_MAP(DispatchComputeIndirect);
	OPENGL_FUNC_MAP(CopyImageSubData);
	OPENGL_FUNC_MAP(DebugMessageControl);
	OPENGL_FUNC_MAP(DebugMessageInsert);
	OPENGL_FUNC_MAP(DebugMessageCallback);
	OPENGL_FUNC_MAP(GetDebugMessageLog);
	OPENGL_FUNC_MAP(PushDebugGroup);
	OPENGL_FUNC_MAP(PopDebugGroup);
	OPENGL_FUNC_MAP(ObjectLabel);
	OPENGL_FUNC_MAP(GetObjectLabel);
	OPENGL_FUNC_MAP(ObjectPtrLabel);
	OPENGL_FUNC_MAP(GetObjectPtrLabel);
	OPENGL_FUNC_MAP(FramebufferParameteri);
	OPENGL_FUNC_MAP(GetFramebufferParameteriv);
	OPENGL_FUNC_MAP(NamedFramebufferParameteriEXT);
	OPENGL_FUNC_MAP(GetNamedFramebufferParameterivEXT);
	OPENGL_FUNC_MAP(GetInternalformati64v);
	OPENGL_FUNC_MAP(InvalidateTexSubImage);
	OPENGL_FUNC_MAP(InvalidateTexImage);
	OPENGL_FUNC_MAP(InvalidateBufferSubData);
	OPENGL_FUNC_MAP(InvalidateBufferData);
	OPENGL_FUNC_MAP(InvalidateFramebuffer);
	OPENGL_FUNC_MAP(InvalidateSubFramebuffer);
	OPENGL_FUNC_MAP(MultiDrawArraysIndirect);
	OPENGL_FUNC_MAP(MultiDrawElementsIndirect);
	OPENGL_FUNC_MAP(GetProgramInterfaceiv);
	OPENGL_FUNC_MAP(GetProgramResourceIndex);
	OPENGL_FUNC_MAP(GetProgramResourceName);
	OPENGL_FUNC_MAP(GetProgramResourceiv);
	OPENGL_FUNC_MAP(GetProgramResourceLocation);
	OPENGL_FUNC_MAP(GetProgramResourceLocationIndex);
	OPENGL_FUNC_MAP(ShaderStorageBlockBinding);
	OPENGL_FUNC_MAP(TexBufferRange);
	OPENGL_FUNC_MAP(TextureBufferRangeEXT);
	OPENGL_FUNC_MAP(TexStorage2DMultisample);
	OPENGL_FUNC_MAP(TexStorage3DMultisample);
	OPENGL_FUNC_MAP(TextureStorage2DMultisampleEXT);
	OPENGL_FUNC_MAP(TextureStorage3DMultisampleEXT);
	OPENGL_FUNC_MAP(TextureView);
	OPENGL_FUNC_MAP(BindVertexBuffer);
	OPENGL_FUNC_MAP(VertexAttribFormat);
	OPENGL_FUNC_MAP(VertexAttribIFormat);
	OPENGL_FUNC_MAP(VertexAttribLFormat);
	OPENGL_FUNC_MAP(VertexAttribBinding);
	OPENGL_FUNC_MAP(VertexBindingDivisor);
	OPENGL_FUNC_MAP(VertexArrayBindVertexBufferEXT);
	OPENGL_FUNC_MAP(VertexArrayVertexAttribFormatEXT);
	OPENGL_FUNC_MAP(VertexArrayVertexAttribIFormatEXT);
	OPENGL_FUNC_MAP(VertexArrayVertexAttribLFormatEXT);
	OPENGL_FUNC_MAP(VertexArrayVertexAttribBindingEXT);
	OPENGL_FUNC_MAP(VertexArrayVertexBindingDivisorEXT);

	if (ClearBufferData || ClearBufferSubData || DispatchCompute || DispatchComputeIndirect || CopyImageSubData || DebugMessageControl || DebugMessageInsert || DebugMessageCallback || GetDebugMessageLog || PushDebugGroup || PopDebugGroup ||
		ObjectLabel || GetObjectLabel || ObjectPtrLabel || GetObjectPtrLabel || FramebufferParameteri || GetFramebufferParameteriv || GetInternalformati64v || InvalidateTexSubImage || InvalidateTexImage || InvalidateBufferSubData ||
		InvalidateBufferData || InvalidateFramebuffer || InvalidateSubFramebuffer || MultiDrawArraysIndirect || MultiDrawElementsIndirect || GetProgramInterfaceiv || GetProgramResourceIndex || GetProgramResourceName || GetProgramResourceiv ||
		GetProgramResourceLocation || GetProgramResourceLocationIndex || ShaderStorageBlockBinding || TexBufferRange || TexStorage2DMultisample || TexStorage3DMultisample || TextureView || BindVertexBuffer || VertexAttribFormat ||
		VertexAttribIFormat || VertexAttribLFormat || VertexAttribBinding || VertexBindingDivisor)

		_version = 4300;

}

