/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\OpenGL.h"
#include "OGLResources.h"

#include "OGL_43.h"


// SahderClip ============================================================================================================================================================================================================================

OpenGL::ShaderClip::ShaderClip(unsigned int type) : _type(type), _unitRunner(0), _infostr(0), _typebit(0) {

	switch (type) {
		case GL_VERTEX_SHADER:
			_typebit = GL_VERTEX_SHADER_BIT;
		break;
		case GL_TESS_CONTROL_SHADER:
			_typebit = GL_TESS_CONTROL_SHADER_BIT;
		break;
		case GL_TESS_EVALUATION_SHADER:
			_typebit = GL_TESS_EVALUATION_SHADER_BIT;
		break;
		case GL_GEOMETRY_SHADER:
			_typebit = GL_GEOMETRY_SHADER_BIT;
		break;
		case GL_FRAGMENT_SHADER:
			_typebit = GL_FRAGMENT_SHADER_BIT;
		break;
	}
		
}

OpenGL::ShaderClip::~ShaderClip() {
	if (_infostr) free(_infostr);

	for (unsigned int i(0);i<_unitRunner;i++) DeleteShader(_units[i]);
}

int OpenGL::ShaderClip::Compile(char ** infolog, int * ilength) {
	int result(1), xlen(0);
	

	if (ilength) ilength[0] = 0;
	
	if (_infostr) free(_infostr);
	_infostr = 0;

	for (unsigned int i(0);i<_unitRunner;i++) {
		CompileShader(_units[i]);

		GetShaderiv(_units[i], GL_COMPILE_STATUS, &result);

		if ((infolog) && (ilength)) {
			int tle(0);
			GetShaderiv(_units[i], GL_INFO_LOG_LENGTH, &tle);

			if (_infostr) {
				_infostr = reinterpret_cast<char *>(realloc(_infostr, ilength[0]+(tle+6)));
			} else {
				_infostr = reinterpret_cast<char *>(malloc(tle+6));
			}

			_infostr[ilength[0]] = '#';
			_infostr[ilength[0]+1] = '>';
			_infostr[ilength[0]+2] = '\n';
			_infostr[ilength[0]+3] = '\r';

			if (tle) {
				GetShaderInfoLog(_units[i], tle, 0, _infostr + ilength[0] + 4);
				ilength[0] += tle;
			}

			_infostr[ilength[0]+4] = '\n';
			_infostr[ilength[0]+5] = '\r';

			ilength[0] += 6;

		}
	}

	if (_infostr) {
		_infostr[ilength[0]-2] = _infostr[ilength[0]-1] = 0;

		if (infolog) {
			infolog[0] = _infostr;
		}
	}

	

	return result;
}

unsigned int OpenGL::ShaderClip::Attach(unsigned int program) {
	unsigned int result(0);

	for (unsigned int i(0);i<_unitRunner;i++) {
		AttachShader(program, _units[i]);
	}

	if (_unitRunner) result = _typebit;

	return result;
}

unsigned int OpenGL::ShaderClip::FromFile(const wchar_t * fname) {
	unsigned int result(0);

	if (_unitRunner >= MAX_UNITS_PER_SHADER) return ERROR_NOT_ENOUGH_MEMORY;

	if (HANDLE fhandle = CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)) {
		LARGE_INTEGER fsize;

		GetFileSizeEx(fhandle, &fsize);

		if ((fsize.HighPart == 0) && (fsize.LowPart < 0x0400000)) {
			if (char * tmpbuf = reinterpret_cast<char *>(malloc(fsize.LowPart + 10))) {
				DWORD bread(0);
				if (ReadFile(fhandle, tmpbuf, fsize.LowPart, &bread, 0)) {
					tmpbuf[fsize.LowPart] = 0;

					_units[_unitRunner] = CreateShader(_type);
					ShaderSource(_units[_unitRunner], 1, &tmpbuf, 0);

					_unitRunner++;
				} else {
					result = GetLastError();
				}

				free(tmpbuf);
			} else {
				result = ERROR_NOT_ENOUGH_MEMORY;
			}

		} else {
			result = ERROR_NOT_ENOUGH_MEMORY;
		}

		CloseHandle(fhandle);
	} else {
		result = GetLastError();
	}

	return result;
}

unsigned int OpenGL::ShaderClip::FromResource(unsigned int rid, HINSTANCE hinst) {
	unsigned int result(0);

	if (_unitRunner >= MAX_UNITS_PER_SHADER) return ERROR_NOT_ENOUGH_MEMORY;

	if (HRSRC tres = FindResource(hinst, MAKEINTRESOURCE(rid), MAKEINTRESOURCE(OGL_SHADER_CODE))) {
		if (char * src = reinterpret_cast<char *>(LockResource(LoadResource(hinst, tres)))) {
			int rsize = SizeofResource(hinst, tres);
			
			_units[_unitRunner] = CreateShader(_type);
			ShaderSource(_units[_unitRunner], 1, &src, &rsize);
			
			_unitRunner++;
		} else {
			result = GetLastError();
		}
	} else {
		result = GetLastError();
	}

	return result;
}

unsigned int OpenGL::ShaderClip::Load(const char * source, int s_size) {
	unsigned int result(0);

	if (_unitRunner >= MAX_UNITS_PER_SHADER) return ERROR_NOT_ENOUGH_MEMORY;

	_units[_unitRunner] = CreateShader(_type);
	ShaderSource(_units[_unitRunner], 1, &source, &s_size);

	_unitRunner++;

	return result;

}

// Program ================================================================================================================================================================================================================================

OpenGL::Program::Program() : _name(0), _infostr(0), _config(0), VertexShader(GL_VERTEX_SHADER), TessCtrlShader(GL_TESS_CONTROL_SHADER), TessEvalShader(GL_TESS_EVALUATION_SHADER), GeometryShader(GL_GEOMETRY_SHADER), FragmentShader(GL_FRAGMENT_SHADER) {
	
}

OpenGL::Program::~Program() {
	if (_infostr) free(_infostr);
	if (_name) DeleteProgram(_name);
}
	
unsigned int OpenGL::Program::GetConfig() {
	return _config;
}

unsigned int OpenGL::Program::Name() {
	if (!_name) _name = CreateProgram();
	return _name;
}

int OpenGL::Program::Link(char ** infolog) {
	int result(0);

	if (_infostr) free(_infostr);
	if (!_name) _name = CreateProgram();

	_config = 0;
	_infostr = 0;
	
		
	_config |= VertexShader.Attach(_name);

	_config |= TessCtrlShader.Attach(_name);

	_config |= TessEvalShader.Attach(_name);

	_config |= GeometryShader.Attach(_name);

	_config |= FragmentShader.Attach(_name);


	LinkProgram(_name);

	GetProgramiv(_name, GL_LINK_STATUS, &result);

	if (infolog) {
		int lole(0);
		GetProgramiv(_name, GL_INFO_LOG_LENGTH, &lole);
						
		_infostr = reinterpret_cast<char *>(malloc(lole+4));

		_infostr[0] = '8';
		_infostr[1] = '>';
		_infostr[2] = '\n';
		_infostr[3] = '\r';


		GetProgramInfoLog(_name, lole, 0, _infostr+4);
		_infostr[lole] = 0;


		infolog[0] = _infostr;
	}


	return result;

}

int OpenGL::Program::Build(char ** infolog, int * ilength) {
	int result(0);
	
	if (result = VertexShader.Compile(infolog, ilength)) {
		if (result = TessCtrlShader.Compile(infolog, ilength)) {
			if (result = TessEvalShader.Compile(infolog, ilength)) {
				if (result = GeometryShader.Compile(infolog, ilength)) {
					if (result = FragmentShader.Compile(infolog, ilength)) {

					}
				}
			}
		}
	}

	if (result) result = Link(infolog);

	return result;
}

// Pipeline ================================================================================================================================================================================================================================================

OpenGL::Pipeline::Pipeline() : _name(0), _config(0), _program20(0) {
	for (unsigned int i(0);i<MAX_PROGRAM_STAGES;i++) _programs[i] = 0;
}

OpenGL::Pipeline::~Pipeline() {
	if (_name) {
		BindProgramPipeline(0);	
		DeleteProgramPipelines(1, &_name);
	}
	
	if (_program20) {
		UseProgram(0);	
		DeleteProgram(_program20);
	}
}

unsigned int OpenGL::Pipeline::UseProgram(Program * progr) {
	unsigned int result(0);
	
	result = progr->GetConfig();

	if (_config & result) {
		for (unsigned int i(0);i<MAX_PROGRAM_STAGES;i++) {
			if (_programs[i])
			if (result & _programs[i]->GetConfig()) {
				_config &= (~_programs[i]->GetConfig());			
				_programs[i] = 0;
			}
		}
	}

	_config |= result;

	for (unsigned int i(0);i<MAX_PROGRAM_STAGES;i++) {
		if (!_programs[i]) {
			_programs[i] = progr;

			break;
		}
	}
	

	return _config;
}

int OpenGL::Pipeline::Activate() {
	int result(0);

	if (GenProgramPipelines && BindProgramPipeline && UseProgramStages) {
		if (!_name) {
			GenProgramPipelines(1, &_name);
			BindProgramPipeline(_name);
		}

		for (unsigned int i(0);i<MAX_PROGRAM_STAGES;i++) {
			if (_programs[i]) UseProgramStages(_name, _programs[i]->GetConfig(), _programs[i]->Name());
		}

	} else {
		unsigned int tmpprog = CreateProgram();

		for (int i(0);i<MAX_PROGRAM_STAGES;i++) {
			if (_programs[i]) {
				if (result = _programs[i]->VertexShader.Compile()) {
					if (result = _programs[i]->TessCtrlShader.Compile()) {
						if (result = _programs[i]->TessEvalShader.Compile()) {
							if (result = _programs[i]->GeometryShader.Compile()) {
								if (result = _programs[i]->FragmentShader.Compile()) {

								}
							}
						}
					}
				}

				if (result) {
					_programs[i]->VertexShader.Attach(tmpprog);
					_programs[i]->TessCtrlShader.Attach(tmpprog);
					_programs[i]->TessEvalShader.Attach(tmpprog);
					_programs[i]->GeometryShader.Attach(tmpprog);
					_programs[i]->FragmentShader.Attach(tmpprog);

				} else {
					result = -i;
					break;
				}
			}
		}

		if (result>0) {
			LinkProgram(tmpprog);

			GetProgramiv(tmpprog, GL_LINK_STATUS, &result);

			if (result) {
				OpenGL::UseProgram(tmpprog);

				DeleteProgram(_program20);
				_program20 = tmpprog;
			}

		}

		if (tmpprog != _program20) DeleteProgram(tmpprog);
		
	}

	return result;
}

