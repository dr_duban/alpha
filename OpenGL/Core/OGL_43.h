/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#ifndef OPENGL_FUNC_INI
	#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; extern P##n n
#endif

#ifndef GL_TYPES

typedef size_t GLintptr, GLsizeiptr;

typedef signed char GLbyte;
typedef unsigned char GLubyte,GLboolean; 

typedef short int GLshort;
typedef unsigned short int GLushort;

typedef unsigned int GLuint, GLenum, GLhandle, GLbitfield;
typedef int GLsizei;

#define GL_TYPES 1
#endif

namespace OpenGL {

struct stDrawArraysIndirectCommand {
	GLuint count;
	GLuint prmCount;
	GLuint first;
	GLuint reservedMustBeZero;
};

struct stDrawElementsIndirectCommand {
	GLuint count;
	GLuint primCount;
	GLuint first;
	int baseVertex;
	GLuint reservedMustBeZero;
};

// OpenGL 2.0 ==============================================================================================================================================================================================================================================
OPENGL_FUNC_INI(BlendEquationSeparate, (GLenum modeRGB, GLenum modeAlpha), void);
OPENGL_FUNC_INI(DrawBuffers, (GLsizei n, const GLenum *bufs), void);
OPENGL_FUNC_INI(StencilOpSeparate, (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass), void);
OPENGL_FUNC_INI(StencilFuncSeparate, (GLenum face, GLenum func, int ref, GLuint mask), void);
OPENGL_FUNC_INI(StencilMaskSeparate, (GLenum face, GLuint mask), void);
OPENGL_FUNC_INI(AttachShader, (GLuint program, GLuint shader), void);
OPENGL_FUNC_INI(BindAttribLocation, (GLuint program, GLuint index, const char *name), void);
OPENGL_FUNC_INI(CompileShader, (GLuint shader), void);
OPENGL_FUNC_INI(CreateProgram, (void), GLuint);
OPENGL_FUNC_INI(CreateShader, (GLenum type), GLuint);
OPENGL_FUNC_INI(DeleteProgram, (GLuint program), void);
OPENGL_FUNC_INI(DeleteShader, (GLuint shader), void);
OPENGL_FUNC_INI(DetachShader, (GLuint program, GLuint shader), void);
OPENGL_FUNC_INI(DisableVertexAttribArray, (GLuint index), void);
OPENGL_FUNC_INI(EnableVertexAttribArray, (GLuint index), void);
OPENGL_FUNC_INI(GetActiveAttrib, (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, int *size, GLenum *type, char *name), void);
OPENGL_FUNC_INI(GetActiveUniform, (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, int *size, GLenum *type, char *name), void);
OPENGL_FUNC_INI(GetAttachedShaders, (GLuint program, GLsizei maxCount, GLsizei *count, GLuint *obj), void);
OPENGL_FUNC_INI(GetAttribLocation, (GLuint program, const char *name), int);
OPENGL_FUNC_INI(GetProgramiv, (GLuint program, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetProgramInfoLog, (GLuint program, GLsizei bufSize, GLsizei *length, char *infoLog), void);
OPENGL_FUNC_INI(GetShaderiv, (GLuint shader, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetShaderInfoLog, (GLuint shader, GLsizei bufSize, GLsizei *length, char *infoLog), void);
OPENGL_FUNC_INI(GetShaderSource, (GLuint shader, GLsizei bufSize, GLsizei *length, char *source), void);
OPENGL_FUNC_INI(GetUniformLocation, (GLuint program, const char *name), int);
OPENGL_FUNC_INI(GetUniformfv, (GLuint program, int location, float *params), void);
OPENGL_FUNC_INI(GetUniformiv, (GLuint program, int location, int *params), void);
OPENGL_FUNC_INI(GetVertexAttribdv, (GLuint index, GLenum pname, double *params), void);
OPENGL_FUNC_INI(GetVertexAttribfv, (GLuint index, GLenum pname, float *params), void);
OPENGL_FUNC_INI(GetVertexAttribiv, (GLuint index, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetVertexAttribPointerv, (GLuint index, GLenum pname, void ** pointer), void);
OPENGL_FUNC_INI(IsProgram, (GLuint program), GLboolean);
OPENGL_FUNC_INI(IsShader, (GLuint shader), GLboolean);
OPENGL_FUNC_INI(LinkProgram, (GLuint program), void);
OPENGL_FUNC_INI(ShaderSource, (GLuint shader, GLsizei count, const char * const * string, const int *length), void);
OPENGL_FUNC_INI(UseProgram, (GLuint program), void);
OPENGL_FUNC_INI(Uniform1f, (int location, float v0), void);
OPENGL_FUNC_INI(Uniform2f, (int location, float v0, float v1), void);
OPENGL_FUNC_INI(Uniform3f, (int location, float v0, float v1, float v2), void);
OPENGL_FUNC_INI(Uniform4f, (int location, float v0, float v1, float v2, float v3), void);
OPENGL_FUNC_INI(Uniform1i, (int location, int v0), void);
OPENGL_FUNC_INI(Uniform2i, (int location, int v0, int v1), void);
OPENGL_FUNC_INI(Uniform3i, (int location, int v0, int v1, int v2), void);
OPENGL_FUNC_INI(Uniform4i, (int location, int v0, int v1, int v2, int v3), void);
OPENGL_FUNC_INI(Uniform1fv, (int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(Uniform2fv, (int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(Uniform3fv, (int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(Uniform4fv, (int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(Uniform1iv, (int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(Uniform2iv, (int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(Uniform3iv, (int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(Uniform4iv, (int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(UniformMatrix2fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix3fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix4fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ValidateProgram, (GLuint program), void);
OPENGL_FUNC_INI(VertexAttrib1d, (GLuint index, double x), void);
OPENGL_FUNC_INI(VertexAttrib1dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttrib1f, (GLuint index, float x), void);
OPENGL_FUNC_INI(VertexAttrib1fv, (GLuint index, const float *v), void);
OPENGL_FUNC_INI(VertexAttrib1s, (GLuint index, GLshort x), void);
OPENGL_FUNC_INI(VertexAttrib1sv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttrib2d, (GLuint index, double x, double y), void);
OPENGL_FUNC_INI(VertexAttrib2dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttrib2f, (GLuint index, float x, float y), void);
OPENGL_FUNC_INI(VertexAttrib2fv, (GLuint index, const float *v), void);
OPENGL_FUNC_INI(VertexAttrib2s, (GLuint index, GLshort x, GLshort y), void);
OPENGL_FUNC_INI(VertexAttrib2sv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttrib3d, (GLuint index, double x, double y, double z), void);
OPENGL_FUNC_INI(VertexAttrib3dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttrib3f, (GLuint index, float x, float y, float z), void);
OPENGL_FUNC_INI(VertexAttrib3fv, (GLuint index, const float *v), void);
OPENGL_FUNC_INI(VertexAttrib3s, (GLuint index, GLshort x, GLshort y, GLshort z), void);
OPENGL_FUNC_INI(VertexAttrib3sv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttrib4Nbv, (GLuint index, const GLbyte *v), void);
OPENGL_FUNC_INI(VertexAttrib4Niv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttrib4Nsv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttrib4Nub, (GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w), void);
OPENGL_FUNC_INI(VertexAttrib4Nubv, (GLuint index, const GLubyte *v), void);
OPENGL_FUNC_INI(VertexAttrib4Nuiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttrib4Nusv, (GLuint index, const GLushort *v), void);
OPENGL_FUNC_INI(VertexAttrib4bv, (GLuint index, const GLbyte *v), void);
OPENGL_FUNC_INI(VertexAttrib4d, (GLuint index, double x, double y, double z, double w), void);
OPENGL_FUNC_INI(VertexAttrib4dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttrib4f, (GLuint index, float x, float y, float z, float w), void);
OPENGL_FUNC_INI(VertexAttrib4fv, (GLuint index, const float *v), void);
OPENGL_FUNC_INI(VertexAttrib4iv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttrib4s, (GLuint index, GLshort x, GLshort y, GLshort z, GLshort w), void);
OPENGL_FUNC_INI(VertexAttrib4sv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttrib4ubv, (GLuint index, const GLubyte *v), void);
OPENGL_FUNC_INI(VertexAttrib4uiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttrib4usv, (GLuint index, const GLushort *v), void);
OPENGL_FUNC_INI(VertexAttribPointer, (GLuint index, int size, GLenum type, GLboolean normalized, GLsizei stride, const void * pointer), void);


// OpenGL 2.1 ========================================================================================================================================================================================================================
OPENGL_FUNC_INI(UniformMatrix2x3fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix3x2fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix2x4fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix4x2fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix3x4fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(UniformMatrix4x3fv, (int location, GLsizei count, GLboolean transpose, const float *value), void);


// OpenGL 3.0 ========================================================================================================================================================================================================================
OPENGL_FUNC_INI(ColorMaski, (GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a), void);
OPENGL_FUNC_INI(GetBooleani_v, (GLenum target, GLuint index, GLboolean *data), void);
OPENGL_FUNC_INI(GetIntegeri_v, (GLenum target, GLuint index, int *data), void);
OPENGL_FUNC_INI(Enablei, (GLenum target, GLuint index), void);
OPENGL_FUNC_INI(Disablei, (GLenum target, GLuint index), void);
OPENGL_FUNC_INI(IsEnabledi, (GLenum target, GLuint index), GLboolean);
OPENGL_FUNC_INI(BeginTransformFeedback, (GLenum primitiveMode), void);
OPENGL_FUNC_INI(EndTransformFeedback, (void), void);
OPENGL_FUNC_INI(BindBufferRange, (GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size), void);
OPENGL_FUNC_INI(BindBufferBase, (GLenum target, GLuint index, GLuint buffer), void);
OPENGL_FUNC_INI(TransformFeedbackVaryings, (GLuint program, GLsizei count, const char* const *varyings, GLenum bufferMode), void);
OPENGL_FUNC_INI(GetTransformFeedbackVarying, (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, char *name), void);
OPENGL_FUNC_INI(ClampColor, (GLenum target, GLenum clamp), void);
OPENGL_FUNC_INI(BeginConditionalRender, (GLuint id, GLenum mode), void);
OPENGL_FUNC_INI(EndConditionalRender, (void), void);
OPENGL_FUNC_INI(VertexAttribIPointer, (GLuint index, int size, GLenum type, GLsizei stride, const void *pointer), void);
OPENGL_FUNC_INI(GetVertexAttribIiv, (GLuint index, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetVertexAttribIuiv, (GLuint index, GLenum pname, GLuint *params), void);
OPENGL_FUNC_INI(VertexAttribI1i, (GLuint index, int x), void);
OPENGL_FUNC_INI(VertexAttribI2i, (GLuint index, int x, int y), void);
OPENGL_FUNC_INI(VertexAttribI3i, (GLuint index, int x, int y, int z), void);
OPENGL_FUNC_INI(VertexAttribI4i, (GLuint index, int x, int y, int z, int w), void);
OPENGL_FUNC_INI(VertexAttribI1ui, (GLuint index, GLuint x), void);
OPENGL_FUNC_INI(VertexAttribI2ui, (GLuint index, GLuint x, GLuint y), void);
OPENGL_FUNC_INI(VertexAttribI3ui, (GLuint index, GLuint x, GLuint y, GLuint z), void);
OPENGL_FUNC_INI(VertexAttribI4ui, (GLuint index, GLuint x, GLuint y, GLuint z, GLuint w), void);
OPENGL_FUNC_INI(VertexAttribI1iv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttribI2iv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttribI3iv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttribI4iv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(VertexAttribI1uiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttribI2uiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttribI3uiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttribI4uiv, (GLuint index, const GLuint *v), void);
OPENGL_FUNC_INI(VertexAttribI4bv, (GLuint index, const GLbyte *v), void);
OPENGL_FUNC_INI(VertexAttribI4sv, (GLuint index, const GLshort *v), void);
OPENGL_FUNC_INI(VertexAttribI4ubv, (GLuint index, const GLubyte *v), void);
OPENGL_FUNC_INI(VertexAttribI4usv, (GLuint index, const GLushort *v), void);
OPENGL_FUNC_INI(GetUniformuiv, (GLuint program, int location, GLuint *params), void);
OPENGL_FUNC_INI(BindFragDataLocation, (GLuint program, GLuint color, const char *name), void);
OPENGL_FUNC_INI(GetFragDataLocation, (GLuint program, const char *name), int);
OPENGL_FUNC_INI(Uniform1ui, (int location, GLuint v0), void);
OPENGL_FUNC_INI(Uniform2ui, (int location, GLuint v0, GLuint v1), void);
OPENGL_FUNC_INI(Uniform3ui, (int location, GLuint v0, GLuint v1, GLuint v2), void);
OPENGL_FUNC_INI(Uniform4ui, (int location, GLuint v0, GLuint v1, GLuint v2, GLuint v3), void);
OPENGL_FUNC_INI(Uniform1uiv, (int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(Uniform2uiv, (int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(Uniform3uiv, (int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(Uniform4uiv, (int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(TexParameterIiv, (GLenum target, GLenum pname, const int *params), void);
OPENGL_FUNC_INI(TexParameterIuiv, (GLenum target, GLenum pname, const GLuint *params), void);
OPENGL_FUNC_INI(GetTexParameterIiv, (GLenum target, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetTexParameterIuiv, (GLenum target, GLenum pname, GLuint *params), void);
OPENGL_FUNC_INI(ClearBufferiv, (GLenum buffer, int drawbuffer, const int *value), void);
OPENGL_FUNC_INI(ClearBufferuiv, (GLenum buffer, int drawbuffer, const GLuint *value), void);
OPENGL_FUNC_INI(ClearBufferfv, (GLenum buffer, int drawbuffer, const float *value), void);
OPENGL_FUNC_INI(ClearBufferfi, (GLenum buffer, int drawbuffer, float depth, int stencil), void);
OPENGL_FUNC_INI(GetStringi, (GLenum name, GLuint index), const GLubyte *);

// ARB_framebuffer_object
OPENGL_FUNC_INI(IsRenderbuffer, (GLuint renderbuffer), GLboolean);
OPENGL_FUNC_INI(BindRenderbuffer, (GLenum target, GLuint renderbuffer), void);
OPENGL_FUNC_INI(DeleteRenderbuffers, (GLsizei n, const GLuint *renderbuffers), void);
OPENGL_FUNC_INI(GenRenderbuffers, (GLsizei n, GLuint *renderbuffers), void);
OPENGL_FUNC_INI(RenderbufferStorage, (GLenum target, GLenum internalformat, GLsizei width, GLsizei height), void);
OPENGL_FUNC_INI(GetRenderbufferParameteriv, (GLenum target, GLenum pname, int *params), void);
OPENGL_FUNC_INI(IsFramebuffer, (GLuint framebuffer), GLboolean);
OPENGL_FUNC_INI(BindFramebuffer, (GLenum target, GLuint framebuffer), void);
OPENGL_FUNC_INI(DeleteFramebuffers, (GLsizei n, const GLuint *framebuffers), void);
OPENGL_FUNC_INI(GenFramebuffers, (GLsizei n, GLuint *framebuffers), void);
OPENGL_FUNC_INI(CheckFramebufferStatus, (GLenum target), GLenum);
OPENGL_FUNC_INI(FramebufferTexture1D, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, int level), void);
OPENGL_FUNC_INI(FramebufferTexture2D, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, int level), void);
OPENGL_FUNC_INI(FramebufferTexture3D, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, int level, int zoffset), void);
OPENGL_FUNC_INI(FramebufferRenderbuffer, (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer), void);
OPENGL_FUNC_INI(GetFramebufferAttachmentParameteriv, (GLenum target, GLenum attachment, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GenerateMipmap, (GLenum target), void);
OPENGL_FUNC_INI(BlitFramebuffer, (int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, GLbitfield mask, GLenum filter), void);
OPENGL_FUNC_INI(RenderbufferStorageMultisample, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height), void);
OPENGL_FUNC_INI(FramebufferTextureLayer, (GLenum target, GLenum attachment, GLuint texture, int level, int layer), void);

// ARB_map_buffer_range
OPENGL_FUNC_INI(MapBufferRange, (GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access), void *);
OPENGL_FUNC_INI(FlushMappedBufferRange, (GLenum target, GLintptr offset, GLsizeiptr length), void);

// ARB_vertex_array_object
OPENGL_FUNC_INI(BindVertexArray, (GLuint array), void);
OPENGL_FUNC_INI(DeleteVertexArrays, (GLsizei n, const GLuint *arrays), void);
OPENGL_FUNC_INI(GenVertexArrays, (GLsizei n, GLuint *arrays), void);
OPENGL_FUNC_INI(IsVertexArray, (GLuint array), GLboolean);


// OpenGL 3.1 ======================================================================================================================================================================================================================================
OPENGL_FUNC_INI(DrawArraysInstanced, (GLenum mode, int first, GLsizei count, GLsizei instancecount), void);
OPENGL_FUNC_INI(DrawElementsInstanced, (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount), void);
OPENGL_FUNC_INI(TexBuffer, (GLenum target, GLenum internalformat, GLuint buffer), void);
OPENGL_FUNC_INI(PrimitiveRestartIndex, (GLuint index), void);

// ARB_copy_buffer
OPENGL_FUNC_INI(CopyBufferSubData, (GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size), void);

//ARB_uniform_buffer_object
OPENGL_FUNC_INI(GetUniformIndices, (GLuint program, GLsizei uniformCount, const char * const * uniformNames, GLuint *uniformIndices), void);
OPENGL_FUNC_INI(GetActiveUniformsiv, (GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetActiveUniformName, (GLuint program, GLuint uniformIndex, GLsizei bufSize, GLsizei *length, char *uniformName), void);
OPENGL_FUNC_INI(GetUniformBlockIndex, (GLuint program, const char *uniformBlockName), GLuint);
OPENGL_FUNC_INI(GetActiveUniformBlockiv, (GLuint program, GLuint uniformBlockIndex, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetActiveUniformBlockName, (GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, char *uniformBlockName), void);
OPENGL_FUNC_INI(UniformBlockBinding, (GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding), void);


// OpenGL 3.2 ================================================================================================================================================================================================================
OPENGL_FUNC_INI(GetInteger64i_v, (GLenum target, GLuint index, __int64 *data), void);
OPENGL_FUNC_INI(GetBufferParameteri64v, (GLenum target, GLenum pname, __int64 *params), void);
OPENGL_FUNC_INI(FramebufferTexture, (GLenum target, GLenum attachment, GLuint texture, int level), void);

// ARB_draw_elements_base_vertex
OPENGL_FUNC_INI(DrawElementsBaseVertex, (GLenum mode, GLsizei count, GLenum type, const void *indices, int basevertex), void);
OPENGL_FUNC_INI(DrawRangeElementsBaseVertex, (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, int basevertex), void);
OPENGL_FUNC_INI(DrawElementsInstancedBaseVertex, (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, int basevertex), void);
OPENGL_FUNC_INI(MultiDrawElementsBaseVertex, (GLenum mode, const GLsizei *count, GLenum type, const void* const *indices, GLsizei drawcount, const int *basevertex), void);

// ARB_provoking_vertex
OPENGL_FUNC_INI(ProvokingVertex, (GLenum mode), void);

// ARB_sync
OPENGL_FUNC_INI(FenceSync, (GLenum condition, GLbitfield flags), void *);
OPENGL_FUNC_INI(IsSync, (void * sync), GLboolean);
OPENGL_FUNC_INI(DeleteSync, (void * sync), void);
OPENGL_FUNC_INI(ClientWaitSync, (void * sync, GLbitfield flags, unsigned __int64 timeout), GLenum);
OPENGL_FUNC_INI(WaitSync, (void * sync, GLbitfield flags, unsigned __int64 timeout), void);
OPENGL_FUNC_INI(GetInteger64v, (GLenum pname, unsigned __int64 *params), void);
OPENGL_FUNC_INI(GetSynciv, (void * sync, GLenum pname, GLsizei bufSize, GLsizei *length, int *values), void);

// ARB_texture_multisample
OPENGL_FUNC_INI(TexImage2DMultisample, (GLenum target, GLsizei samples, int internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations), void);
OPENGL_FUNC_INI(TexImage3DMultisample, (GLenum target, GLsizei samples, int internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations), void);
OPENGL_FUNC_INI(GetMultisamplefv, (GLenum pname, GLuint index, float *val), void);
OPENGL_FUNC_INI(SampleMaski, (GLuint index, GLbitfield mask), void);


// OpenGL 3.3 ==================================================================================================================================================================================================================
OPENGL_FUNC_INI(VertexAttribDivisor, (GLuint index, GLuint divisor), void);

// ARB_blend_func_extended
OPENGL_FUNC_INI(BindFragDataLocationIndexed, (GLuint program, GLuint colorNumber, GLuint index, const char *name), void);
OPENGL_FUNC_INI(GetFragDataIndex, (GLuint program, const char *name), int);

// ARB_sampler_objects
OPENGL_FUNC_INI(GenSamplers, (GLsizei count, GLuint *samplers), void);
OPENGL_FUNC_INI(DeleteSamplers, (GLsizei count, const GLuint *samplers), void);
OPENGL_FUNC_INI(IsSampler, (GLuint sampler), GLboolean);
OPENGL_FUNC_INI(BindSampler, (GLuint unit, GLuint sampler), void);
OPENGL_FUNC_INI(SamplerParameteri, (GLuint sampler, GLenum pname, int param), void);
OPENGL_FUNC_INI(SamplerParameteriv, (GLuint sampler, GLenum pname, const int *param), void);
OPENGL_FUNC_INI(SamplerParameterf, (GLuint sampler, GLenum pname, float param), void);
OPENGL_FUNC_INI(SamplerParameterfv, (GLuint sampler, GLenum pname, const float *param), void);
OPENGL_FUNC_INI(SamplerParameterIiv, (GLuint sampler, GLenum pname, const int *param), void);
OPENGL_FUNC_INI(SamplerParameterIuiv, (GLuint sampler, GLenum pname, const GLuint *param), void);
OPENGL_FUNC_INI(GetSamplerParameteriv, (GLuint sampler, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetSamplerParameterIiv, (GLuint sampler, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetSamplerParameterfv, (GLuint sampler, GLenum pname, float *params), void);
OPENGL_FUNC_INI(GetSamplerParameterIuiv, (GLuint sampler, GLenum pname, GLuint *params), void);

// ARB_timer_query
OPENGL_FUNC_INI(QueryCounter, (GLuint id, GLenum target), void);
OPENGL_FUNC_INI(GetQueryObjecti64v, (GLuint id, GLenum pname, __int64 *params), void);
OPENGL_FUNC_INI(GetQueryObjectui64v, (GLuint id, GLenum pname, unsigned __int64 *params), void);

// ARB_vertex_type_2_10_10_10_rev
OPENGL_FUNC_INI(VertexP2ui, (GLenum type, GLuint value), void);
OPENGL_FUNC_INI(VertexP2uiv, (GLenum type, const GLuint *value), void);
OPENGL_FUNC_INI(VertexP3ui, (GLenum type, GLuint value), void);
OPENGL_FUNC_INI(VertexP3uiv, (GLenum type, const GLuint *value), void);
OPENGL_FUNC_INI(VertexP4ui, (GLenum type, GLuint value), void);
OPENGL_FUNC_INI(VertexP4uiv, (GLenum type, const GLuint *value), void);
OPENGL_FUNC_INI(TexCoordP1ui, (GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(TexCoordP1uiv, (GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(TexCoordP2ui, (GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(TexCoordP2uiv, (GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(TexCoordP3ui, (GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(TexCoordP3uiv, (GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(TexCoordP4ui, (GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(TexCoordP4uiv, (GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(MultiTexCoordP1ui, (GLenum texture, GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(MultiTexCoordP1uiv, (GLenum texture, GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(MultiTexCoordP2ui, (GLenum texture, GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(MultiTexCoordP2uiv, (GLenum texture, GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(MultiTexCoordP3ui, (GLenum texture, GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(MultiTexCoordP3uiv, (GLenum texture, GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(MultiTexCoordP4ui, (GLenum texture, GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(MultiTexCoordP4uiv, (GLenum texture, GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(NormalP3ui, (GLenum type, GLuint coords), void);
OPENGL_FUNC_INI(NormalP3uiv, (GLenum type, const GLuint *coords), void);
OPENGL_FUNC_INI(ColorP3ui, (GLenum type, GLuint color), void);
OPENGL_FUNC_INI(ColorP3uiv, (GLenum type, const GLuint *color), void);
OPENGL_FUNC_INI(ColorP4ui, (GLenum type, GLuint color), void);
OPENGL_FUNC_INI(ColorP4uiv, (GLenum type, const GLuint *color), void);
OPENGL_FUNC_INI(SecondaryColorP3ui, (GLenum type, GLuint color), void);
OPENGL_FUNC_INI(SecondaryColorP3uiv, (GLenum type, const GLuint *color), void);
OPENGL_FUNC_INI(VertexAttribP1ui, (GLuint index, GLenum type, GLboolean normalized, GLuint value), void);
OPENGL_FUNC_INI(VertexAttribP1uiv, (GLuint index, GLenum type, GLboolean normalized, const GLuint *value), void);
OPENGL_FUNC_INI(VertexAttribP2ui, (GLuint index, GLenum type, GLboolean normalized, GLuint value), void);
OPENGL_FUNC_INI(VertexAttribP2uiv, (GLuint index, GLenum type, GLboolean normalized, const GLuint *value), void);
OPENGL_FUNC_INI(VertexAttribP3ui, (GLuint index, GLenum type, GLboolean normalized, GLuint value), void);
OPENGL_FUNC_INI(VertexAttribP3uiv, (GLuint index, GLenum type, GLboolean normalized, const GLuint *value), void);
OPENGL_FUNC_INI(VertexAttribP4ui, (GLuint index, GLenum type, GLboolean normalized, GLuint value), void);
OPENGL_FUNC_INI(VertexAttribP4uiv, (GLuint index, GLenum type, GLboolean normalized, const GLuint *value), void);


// OpenGL 4.0 ==================================================================================================================================================================================================================
OPENGL_FUNC_INI(MinSampleShading, (float value), void);
OPENGL_FUNC_INI(BlendEquationi, (GLuint buf, GLenum mode), void);
OPENGL_FUNC_INI(BlendEquationSeparatei, (GLuint buf, GLenum modeRGB, GLenum modeAlpha), void);
OPENGL_FUNC_INI(BlendFunci, (GLuint buf, GLenum src, GLenum dst), void);
OPENGL_FUNC_INI(BlendFuncSeparatei, (GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha), void);

// ARB_draw_indirect
OPENGL_FUNC_INI(DrawArraysIndirect, (GLenum mode, const void *indirect), void);
OPENGL_FUNC_INI(DrawElementsIndirect, (GLenum mode, GLenum type, const void *indirect), void);

// ARB_gpu_shader_fp64
OPENGL_FUNC_INI(Uniform1d, (int location, double x), void);
OPENGL_FUNC_INI(Uniform2d, (int location, double x, double y), void);
OPENGL_FUNC_INI(Uniform3d, (int location, double x, double y, double z), void);
OPENGL_FUNC_INI(Uniform4d, (int location, double x, double y, double z, double w), void);
OPENGL_FUNC_INI(Uniform1dv, (int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(Uniform2dv, (int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(Uniform3dv, (int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(Uniform4dv, (int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix2dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix3dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix4dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix2x3dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix2x4dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix3x2dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix3x4dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix4x2dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(UniformMatrix4x3dv, (int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(GetUniformdv, (GLuint program, int location, double *params), void);

// ARB_shader_subroutine
OPENGL_FUNC_INI(GetSubroutineUniformLocation, (GLuint program, GLenum shadertype, const char *name), int);
OPENGL_FUNC_INI(GetSubroutineIndex, (GLuint program, GLenum shadertype, const char *name), GLuint);
OPENGL_FUNC_INI(GetActiveSubroutineUniformiv, (GLuint program, GLenum shadertype, GLuint index, GLenum pname, int *values), void);
OPENGL_FUNC_INI(GetActiveSubroutineUniformName, (GLuint program, GLenum shadertype, GLuint index, GLsizei bufsize, GLsizei *length, char *name), void);
OPENGL_FUNC_INI(GetActiveSubroutineName, (GLuint program, GLenum shadertype, GLuint index, GLsizei bufsize, GLsizei *length, char *name), void);
OPENGL_FUNC_INI(UniformSubroutinesuiv, (GLenum shadertype, GLsizei count, const GLuint *indices), void);
OPENGL_FUNC_INI(GetUniformSubroutineuiv, (GLenum shadertype, int location, GLuint *params), void);
OPENGL_FUNC_INI(GetProgramStageiv, (GLuint program, GLenum shadertype, GLenum pname, int *values), void);

// ARB_tessellation_shader
OPENGL_FUNC_INI(PatchParameteri, (GLenum pname, int value), void);
OPENGL_FUNC_INI(PatchParameterfv, (GLenum pname, const float *values), void);

// ARB_transform_feedback2
OPENGL_FUNC_INI(BindTransformFeedback, (GLenum target, GLuint id), void);
OPENGL_FUNC_INI(DeleteTransformFeedbacks, (GLsizei n, const GLuint *ids), void);
OPENGL_FUNC_INI(GenTransformFeedbacks, (GLsizei n, GLuint *ids), void);
OPENGL_FUNC_INI(IsTransformFeedback, (GLuint id), GLboolean);
OPENGL_FUNC_INI(PauseTransformFeedback, (void), void);
OPENGL_FUNC_INI(ResumeTransformFeedback, (void), void);
OPENGL_FUNC_INI(DrawTransformFeedback, (GLenum mode, GLuint id), void);

// ARB_transform_feedback3
OPENGL_FUNC_INI(DrawTransformFeedbackStream, (GLenum mode, GLuint id, GLuint stream), void);
OPENGL_FUNC_INI(BeginQueryIndexed, (GLenum target, GLuint index, GLuint id), void);
OPENGL_FUNC_INI(EndQueryIndexed, (GLenum target, GLuint index), void);
OPENGL_FUNC_INI(GetQueryIndexediv, (GLenum target, GLuint index, GLenum pname, int *params), void);


// OpenGL 4.1 ===========================================================================================================================================================================================================
// ARB_ES2_compatibility
OPENGL_FUNC_INI(ReleaseShaderCompiler, (void), void);
OPENGL_FUNC_INI(ShaderBinary, (GLsizei count, const GLuint *shaders, GLenum binaryformat, const void *binary, GLsizei length), void);
OPENGL_FUNC_INI(GetShaderPrecisionFormat, (GLenum shadertype, GLenum precisiontype, int *range, int *precision), void);
OPENGL_FUNC_INI(DepthRangef, (float n, float f), void);
OPENGL_FUNC_INI(ClearDepthf, (float d), void);

// ARB_get_program_binary
OPENGL_FUNC_INI(GetProgramBinary, (GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary), void);
OPENGL_FUNC_INI(ProgramBinary, (GLuint program, GLenum binaryFormat, const void *binary, GLsizei length), void);
OPENGL_FUNC_INI(ProgramParameteri, (GLuint program, GLenum pname, int value), void);

// ARB_separate_shader_objects
OPENGL_FUNC_INI(UseProgramStages, (GLuint pipeline, GLbitfield stages, GLuint program), void);
OPENGL_FUNC_INI(ActiveShaderProgram, (GLuint pipeline, GLuint program), void);
OPENGL_FUNC_INI(CreateShaderProgramv, (GLenum type, GLsizei count, const char* const *strings), GLuint);
OPENGL_FUNC_INI(BindProgramPipeline, (GLuint pipeline), void);
OPENGL_FUNC_INI(DeleteProgramPipelines, (GLsizei n, const GLuint *pipelines), void);
OPENGL_FUNC_INI(GenProgramPipelines, (GLsizei n, GLuint *pipelines), void);
OPENGL_FUNC_INI(IsProgramPipeline, (GLuint pipeline), GLboolean);
OPENGL_FUNC_INI(GetProgramPipelineiv, (GLuint pipeline, GLenum pname, int *params), void);
OPENGL_FUNC_INI(ProgramUniform1i, (GLuint program, int location, int v0), void);
OPENGL_FUNC_INI(ProgramUniform1iv, (GLuint program, int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(ProgramUniform1f, (GLuint program, int location, float v0), void);
OPENGL_FUNC_INI(ProgramUniform1fv, (GLuint program, int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(ProgramUniform1d, (GLuint program, int location, double v0), void);
OPENGL_FUNC_INI(ProgramUniform1dv, (GLuint program, int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(ProgramUniform1ui, (GLuint program, int location, GLuint v0), void);
OPENGL_FUNC_INI(ProgramUniform1uiv, (GLuint program, int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(ProgramUniform2i, (GLuint program, int location, int v0, int v1), void);
OPENGL_FUNC_INI(ProgramUniform2iv, (GLuint program, int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(ProgramUniform2f, (GLuint program, int location, float v0, float v1), void);
OPENGL_FUNC_INI(ProgramUniform2fv, (GLuint program, int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(ProgramUniform2d, (GLuint program, int location, double v0, double v1), void);
OPENGL_FUNC_INI(ProgramUniform2dv, (GLuint program, int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(ProgramUniform2ui, (GLuint program, int location, GLuint v0, GLuint v1), void);
OPENGL_FUNC_INI(ProgramUniform2uiv, (GLuint program, int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(ProgramUniform3i, (GLuint program, int location, int v0, int v1, int v2), void);
OPENGL_FUNC_INI(ProgramUniform3iv, (GLuint program, int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(ProgramUniform3f, (GLuint program, int location, float v0, float v1, float v2), void);
OPENGL_FUNC_INI(ProgramUniform3fv, (GLuint program, int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(ProgramUniform3d, (GLuint program, int location, double v0, double v1, double v2), void);
OPENGL_FUNC_INI(ProgramUniform3dv, (GLuint program, int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(ProgramUniform3ui, (GLuint program, int location, GLuint v0, GLuint v1, GLuint v2), void);
OPENGL_FUNC_INI(ProgramUniform3uiv, (GLuint program, int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(ProgramUniform4i, (GLuint program, int location, int v0, int v1, int v2, int v3), void);
OPENGL_FUNC_INI(ProgramUniform4iv, (GLuint program, int location, GLsizei count, const int *value), void);
OPENGL_FUNC_INI(ProgramUniform4f, (GLuint program, int location, float v0, float v1, float v2, float v3), void);
OPENGL_FUNC_INI(ProgramUniform4fv, (GLuint program, int location, GLsizei count, const float *value), void);
OPENGL_FUNC_INI(ProgramUniform4d, (GLuint program, int location, double v0, double v1, double v2, double v3), void);
OPENGL_FUNC_INI(ProgramUniform4dv, (GLuint program, int location, GLsizei count, const double *value), void);
OPENGL_FUNC_INI(ProgramUniform4ui, (GLuint program, int location, GLuint v0, GLuint v1, GLuint v2, GLuint v3), void);
OPENGL_FUNC_INI(ProgramUniform4uiv, (GLuint program, int location, GLsizei count, const GLuint *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2x3fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3x2fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2x4fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4x2fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3x4fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4x3fv, (GLuint program, int location, GLsizei count, GLboolean transpose, const float *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2x3dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3x2dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix2x4dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4x2dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix3x4dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ProgramUniformMatrix4x3dv, (GLuint program, int location, GLsizei count, GLboolean transpose, const double *value), void);
OPENGL_FUNC_INI(ValidateProgramPipeline, (GLuint pipeline), void);
OPENGL_FUNC_INI(GetProgramPipelineInfoLog, (GLuint pipeline, GLsizei bufSize, GLsizei *length, char *infoLog), void);

// ARB_vertex_attrib_64bit
OPENGL_FUNC_INI(VertexAttribL1d, (GLuint index, double x), void);
OPENGL_FUNC_INI(VertexAttribL2d, (GLuint index, double x, double y), void);
OPENGL_FUNC_INI(VertexAttribL3d, (GLuint index, double x, double y, double z), void);
OPENGL_FUNC_INI(VertexAttribL4d, (GLuint index, double x, double y, double z, double w), void);
OPENGL_FUNC_INI(VertexAttribL1dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttribL2dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttribL3dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttribL4dv, (GLuint index, const double *v), void);
OPENGL_FUNC_INI(VertexAttribLPointer, (GLuint index, int size, GLenum type, GLsizei stride, const void *pointer), void);
OPENGL_FUNC_INI(GetVertexAttribLdv, (GLuint index, GLenum pname, double *params), void);

// ARB_viewport_array
OPENGL_FUNC_INI(ViewportArrayv, (GLuint first, GLsizei count, const float *v), void);
OPENGL_FUNC_INI(ViewportIndexedf, (GLuint index, float x, float y, float w, float h), void);
OPENGL_FUNC_INI(ViewportIndexedfv, (GLuint index, const float *v), void);
OPENGL_FUNC_INI(ScissorArrayv, (GLuint first, GLsizei count, const int *v), void);
OPENGL_FUNC_INI(ScissorIndexed, (GLuint index, int left, int bottom, GLsizei width, GLsizei height), void);
OPENGL_FUNC_INI(ScissorIndexedv, (GLuint index, const int *v), void);
OPENGL_FUNC_INI(DepthRangeArrayv, (GLuint first, GLsizei count, const double *v), void);
OPENGL_FUNC_INI(DepthRangeIndexed, (GLuint index, double n, double f), void);
OPENGL_FUNC_INI(GetFloati_v, (GLenum target, GLuint index, float *data), void);
OPENGL_FUNC_INI(GetDoublei_v, (GLenum target, GLuint index, double *data), void);


// OpenGL 4.2 ==========================================================================================================================================================================================================================
// ARB_base_instance
OPENGL_FUNC_INI(DrawArraysInstancedBaseInstance, (GLenum mode, int first, GLsizei count, GLsizei instancecount, GLuint baseinstance), void);
OPENGL_FUNC_INI(DrawElementsInstancedBaseInstance, (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLuint baseinstance), void);
OPENGL_FUNC_INI(DrawElementsInstancedBaseVertexBaseInstance, (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, int basevertex, GLuint baseinstance), void);

// ARB_transform_feedback_instanced
OPENGL_FUNC_INI(DrawTransformFeedbackInstanced, (GLenum mode, GLuint id, GLsizei instancecount), void);
OPENGL_FUNC_INI(DrawTransformFeedbackStreamInstanced, (GLenum mode, GLuint id, GLuint stream, GLsizei instancecount), void);

// ARB_internalformat_query
OPENGL_FUNC_INI(GetInternalformativ, (GLenum target, GLenum internalformat, GLenum pname, GLsizei bufSize, int *params), void);

// ARB_shader_atomic_counters
OPENGL_FUNC_INI(GetActiveAtomicCounterBufferiv, (GLuint program, GLuint bufferIndex, GLenum pname, int *params), void);

// ARB_shader_image_load_store
OPENGL_FUNC_INI(BindImageTexture, (GLuint unit, GLuint texture, int level, GLboolean layered, int layer, GLenum access, GLenum format), void);
OPENGL_FUNC_INI(MemoryBarrier, (GLbitfield barriers), void);

// ARB_texture_storage
OPENGL_FUNC_INI(TexStorage1D, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width), void);
OPENGL_FUNC_INI(TexStorage2D, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height), void);
OPENGL_FUNC_INI(TexStorage3D, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth), void);
OPENGL_FUNC_INI(TextureStorage1DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width), void);
OPENGL_FUNC_INI(TextureStorage2DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height), void);
OPENGL_FUNC_INI(TextureStorage3DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth), void);


// OpenGL 4.3 ==================================================================================================================================================================================================================================
// ARB_clear_buffer_object
OPENGL_FUNC_INI(ClearBufferData, (GLenum target, GLenum internalformat, GLenum format, GLenum type, const void *data), void);
OPENGL_FUNC_INI(ClearBufferSubData, (GLenum target, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void *data), void);
OPENGL_FUNC_INI(ClearNamedBufferDataEXT, (GLuint buffer, GLenum internalformat, GLenum format, GLenum type, const void *data), void);
OPENGL_FUNC_INI(ClearNamedBufferSubDataEXT, (GLuint buffer, GLenum internalformat, GLenum format, GLenum type, GLsizeiptr offset, GLsizeiptr size, const void *data), void);

// ARB_compute_shader
OPENGL_FUNC_INI(DispatchCompute, (GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z), void);
OPENGL_FUNC_INI(DispatchComputeIndirect, (GLintptr indirect), void);

// ARB_copy_image
OPENGL_FUNC_INI(CopyImageSubData, (GLuint srcName, GLenum srcTarget, int srcLevel, int srcX, int srcY, int srcZ, GLuint dstName, GLenum dstTarget, int dstLevel, int dstX, int dstY, int dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth), void);

// KHR_debug
typedef void (__stdcall *GLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char * message, void *userParam);

OPENGL_FUNC_INI(DebugMessageControl, (GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled), void);
OPENGL_FUNC_INI(DebugMessageInsert, (GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char *buf), void);
OPENGL_FUNC_INI(DebugMessageCallback, (GLDEBUGPROC callback, const void *userParam), void);
OPENGL_FUNC_INI(GetDebugMessageLog, (GLuint count, GLsizei bufsize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, char *messageLog), GLuint);
OPENGL_FUNC_INI(PushDebugGroup, (GLenum source, GLuint id, GLsizei length, const char *message), void);
OPENGL_FUNC_INI(PopDebugGroup, (void), void);
OPENGL_FUNC_INI(ObjectLabel, (GLenum identifier, GLuint name, GLsizei length, const char *label), void);
OPENGL_FUNC_INI(GetObjectLabel, (GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, char *label), void);
OPENGL_FUNC_INI(ObjectPtrLabel, (const void *ptr, GLsizei length, const char *label), void);
OPENGL_FUNC_INI(GetObjectPtrLabel, (const void *ptr, GLsizei bufSize, GLsizei *length, char *label), void);

// ARB_framebuffer_no_attachments
OPENGL_FUNC_INI(FramebufferParameteri, (GLenum target, GLenum pname, int param), void);
OPENGL_FUNC_INI(GetFramebufferParameteriv, (GLenum target, GLenum pname, int *params), void);
OPENGL_FUNC_INI(NamedFramebufferParameteriEXT, (GLuint framebuffer, GLenum pname, int param), void);
OPENGL_FUNC_INI(GetNamedFramebufferParameterivEXT, (GLuint framebuffer, GLenum pname, int *params), void);

// ARB_internalformat_query2
OPENGL_FUNC_INI(GetInternalformati64v, (GLenum target, GLenum internalformat, GLenum pname, GLsizei bufSize, __int64 *params), void);

// ARB_invalidate_subdata
OPENGL_FUNC_INI(InvalidateTexSubImage, (GLuint texture, int level, int xoffset, int yoffset, int zoffset, GLsizei width, GLsizei height, GLsizei depth), void);
OPENGL_FUNC_INI(InvalidateTexImage, (GLuint texture, int level), void);
OPENGL_FUNC_INI(InvalidateBufferSubData, (GLuint buffer, GLintptr offset, GLsizeiptr length), void);
OPENGL_FUNC_INI(InvalidateBufferData, (GLuint buffer), void);
OPENGL_FUNC_INI(InvalidateFramebuffer, (GLenum target, GLsizei numAttachments, const GLenum *attachments), void);
OPENGL_FUNC_INI(InvalidateSubFramebuffer, (GLenum target, GLsizei numAttachments, const GLenum *attachments, int x, int y, GLsizei width, GLsizei height), void);

// ARB_multi_draw_indirect
OPENGL_FUNC_INI(MultiDrawArraysIndirect, (GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride), void);
OPENGL_FUNC_INI(MultiDrawElementsIndirect, (GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride), void);

// ARB_program_interface_query
OPENGL_FUNC_INI(GetProgramInterfaceiv, (GLuint program, GLenum programInterface, GLenum pname, int *params), void);
OPENGL_FUNC_INI(GetProgramResourceIndex, (GLuint program, GLenum programInterface, const char *name), GLuint);
OPENGL_FUNC_INI(GetProgramResourceName, (GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, char *name), void);
OPENGL_FUNC_INI(GetProgramResourceiv, (GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei bufSize, GLsizei *length, int *params), void);
OPENGL_FUNC_INI(GetProgramResourceLocation, (GLuint program, GLenum programInterface, const char *name), int);
OPENGL_FUNC_INI(GetProgramResourceLocationIndex, (GLuint program, GLenum programInterface, const char *name), int);

// ARB_shader_storage_buffer_object
OPENGL_FUNC_INI(ShaderStorageBlockBinding, (GLuint program, GLuint storageBlockIndex, GLuint storageBlockBinding), void);

// ARB_texture_buffer_range
OPENGL_FUNC_INI(TexBufferRange, (GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size), void);
OPENGL_FUNC_INI(TextureBufferRangeEXT, (GLuint texture, GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size), void);

// ARB_texture_storage_multisample
OPENGL_FUNC_INI(TexStorage2DMultisample, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations), void);
OPENGL_FUNC_INI(TexStorage3DMultisample, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations), void);
OPENGL_FUNC_INI(TextureStorage2DMultisampleEXT, (GLuint texture, GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations), void);
OPENGL_FUNC_INI(TextureStorage3DMultisampleEXT, (GLuint texture, GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations), void);

// ARB_texture_view
OPENGL_FUNC_INI(TextureView, (GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers), void);

// ARB_vertex_attrib_binding
OPENGL_FUNC_INI(BindVertexBuffer, (GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride), void);
OPENGL_FUNC_INI(VertexAttribFormat, (GLuint attribindex, int size, GLenum type, GLboolean normalized, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexAttribIFormat, (GLuint attribindex, int size, GLenum type, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexAttribLFormat, (GLuint attribindex, int size, GLenum type, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexAttribBinding, (GLuint attribindex, GLuint bindingindex), void);
OPENGL_FUNC_INI(VertexBindingDivisor, (GLuint bindingindex, GLuint divisor), void);
OPENGL_FUNC_INI(VertexArrayBindVertexBufferEXT, (GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride), void);
OPENGL_FUNC_INI(VertexArrayVertexAttribFormatEXT, (GLuint vaobj, GLuint attribindex, int size, GLenum type, GLboolean normalized, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexArrayVertexAttribIFormatEXT, (GLuint vaobj, GLuint attribindex, int size, GLenum type, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexArrayVertexAttribLFormatEXT, (GLuint vaobj, GLuint attribindex, int size, GLenum type, GLuint relativeoffset), void);
OPENGL_FUNC_INI(VertexArrayVertexAttribBindingEXT, (GLuint vaobj, GLuint attribindex, GLuint bindingindex), void);
OPENGL_FUNC_INI(VertexArrayVertexBindingDivisorEXT, (GLuint vaobj, GLuint bindingindex, GLuint divisor), void);


}