/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\OpenGL.h"

#include "OGL_15.h"
#include "OGL_43.h"

unsigned int OpenGL::Core::color_matrix_count = 16;

const float OpenGL::Core::color_matrix[][16] = {
// RGB
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// 709
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		-0.1873f,	1.8556f,	0.0f,
	1.5748f,	-0.4681f,	0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// Y
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// Cb
	0.0f,		0.0f,		0.0f,		0.0f,
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,




// T 47 
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		-0.3319f,	1.7800f,	0.0f,
	1.4f,		-0.7119f,	0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// 601
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		-0.3441f,	1.772f,		0.0f,
	1.402f,		-0.7141f,	0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// Cr
	0.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		0.0f,
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,


// 240 M
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		-0.2266f,	1.826f,	0.0f,
	1.576f,		-0.4766f,	0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,





	

// YCgCo
	1.0f,		1.0f,		1.0f,		0.0f,
	-1.0f,		1.0f,		-1.0f,		0.0f,
	1.0f,		0.0f,		-1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// 2020
	1.0f,		1.0f,		1.0f,		0.0f,
	0.0f,		-0.1646f,	1.8814f,	0.0f,
	1.4746f,	-0.5714f,	0.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,



// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f,

// reserved
	1.0f,		0.0f,		0.0f,		0.0f,
	0.0f,		1.0f,		0.0f,		0.0f,
	0.0f,		0.0f,		1.0f,		0.0f,
	0.0f,		0.0f,		0.0f,		1.0f



};

// ==========================================================================================================================================================================================================================

// 10 + 10 + 15 + 30 + 20 + 15
OpenGL::Core::PrimitivePass::PrimitivePass() : tex_ms(0), builder_frame(0), builder_depth(0), draw_buffer_count(0) {
	for (unsigned int i(0);i<8;i++) draw_buffers[i] = 0;
}

OpenGL::Core::PrimitivePass::~PrimitivePass() {

}
/*
int OpenGL::Core::PrimitivePass::Destination() const {
	return destination;
}
*/

int OpenGL::Core::PrimitivePass::Initialize(int mww, int mhh) {
	int result(0), s_size(0);
	unsigned int o_val(0);
	
	draw_buffers[0] = GL_COLOR_ATTACHMENT0;
	draw_buffers[1] = GL_COLOR_ATTACHMENT1;
	draw_buffer_count = 3;

	if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 14*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 14*2 + 1];
		small_triangle.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16395, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 16*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 16*2 + 1];
		small_triangle.program.GeometryShader.Load(col_ptr + o_val, s_size); // FromResource(16397, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 15*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 15*2 + 1];
		small_triangle.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16396, libin);

		strings_collection.Release();
	}
		
	if (result = small_triangle.program.Build()) {
		small_triangle.ct_location = GetUniformLocation(small_triangle.program.Name(), "color_transform");

		small_triangle.local_cfg.location = GetUniformLocation(small_triangle.program.Name(), "local_config");

		small_triangle.cfg_flags_location = GetUniformLocation(small_triangle.program.Name(), "config_flags");
		small_triangle.cfg_offs_location = GetUniformLocation(small_triangle.program.Name(), "config_offs");

		small_triangle.attbutes.location = GetUniformLocation(small_triangle.program.Name(), "attbutes");
		small_triangle.attbutes.value.i_val = attribute_unit;
		
		small_triangle.ms_texture.location = GetUniformLocation(small_triangle.program.Name(), "ms_texture");
		small_triangle.ms_texture.value.i_val = ms_frame_unit;

		small_triangle.el_sampler.location = GetUniformLocation(small_triangle.program.Name(), "el_sampler");
		small_triangle.el_sampler.value.i_val = image_unit;

		small_triangle.msel_sampler.location = GetUniformLocation(small_triangle.program.Name(), "msel_sampler");
		small_triangle.msel_sampler.value.i_val = ml_unit;

		result = 0;
	} else {
		return 333333;
	}

	if (!result) {
		if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 14*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 14*2 + 1];
			small_line.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16395, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 17*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 17*2 + 1];
			small_line.program.GeometryShader.Load(col_ptr + o_val, s_size); // FromResource(16398, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 15*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 15*2 + 1];
			small_line.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16396, libin);

			strings_collection.Release();
		}
		
		if (result = small_line.program.Build()) {
			small_line.local_cfg.location = GetUniformLocation(small_line.program.Name(), "local_config");

			small_line.cfg_flags_location = GetUniformLocation(small_line.program.Name(), "config_flags");
			small_line.cfg_offs_location = GetUniformLocation(small_line.program.Name(), "config_offs");

			small_line.attbutes.location = GetUniformLocation(small_line.program.Name(), "attbutes");
			small_line.attbutes.value.i_val = attribute_unit;
		
			small_line.ms_texture.location = GetUniformLocation(small_line.program.Name(), "ms_texture");
			small_line.ms_texture.value.i_val = ms_frame_unit;

			small_line.el_sampler.location = GetUniformLocation(small_line.program.Name(), "el_sampler");
			small_line.el_sampler.value.i_val = image_unit;

			small_line.msel_sampler.location = GetUniformLocation(small_line.program.Name(), "msel_sampler");
			small_line.msel_sampler.value.i_val = ml_unit;

			result = 0;
		} else {
			return 444444;
		}
	}

	if (!result) {
		if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 18*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 18*2 + 1];
			large_line.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16395, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 19*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 19*2 + 1];
			large_line.program.GeometryShader.Load(col_ptr + o_val, s_size); // FromResource(16398, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 20*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 20*2 + 1];
			large_line.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16396, libin);

			strings_collection.Release();
		}
		
		if (result = large_line.program.Build()) {
			large_line.trans_location = GetUniformLocation(large_line.program.Name(), "trans_a");
			large_line.flat_location = GetUniformLocation(large_line.program.Name(), "flat_vals");

			large_line.cfg_flags_location = GetUniformLocation(large_line.program.Name(), "config_vec");

			large_line.attbutes.location = GetUniformLocation(large_line.program.Name(), "attbutes");
			large_line.attbutes.value.i_val = large_unit;
		
			large_line.ms_texture.location = GetUniformLocation(large_line.program.Name(), "ms_texture");
			large_line.ms_texture.value.i_val = ms_frame_unit;

			large_line.el_sampler.location = GetUniformLocation(large_line.program.Name(), "el_sampler");
			large_line.el_sampler.value.i_val = image_unit;

			large_line.msel_sampler.location = GetUniformLocation(large_line.program.Name(), "msel_sampler");
			large_line.msel_sampler.value.i_val = ml_unit;

			result = 0;
		} else {
			return 444444;
		}
	}


	if (!result) {
		if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 5*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 5*2 + 1];
			clear_program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16385, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 6*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 6*2 + 1];
			clear_program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16386, libin);

			strings_collection.Release();
		}
		
		if (result = clear_program.Build()) {
			clear_color.location = GetUniformLocation(clear_program.Name(), "dclear");
			result = 0;
		} else {
			return 55555;
		}
	}

	if (!result) {
		if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 5*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 5*2 + 1];
			refresh_program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16385, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 7*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 7*2 + 1];
			refresh_program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16387, libin);

			strings_collection.Release();
		}
		
		if (result = refresh_program.Build()) {			
			refresh_color.location = GetUniformLocation(refresh_program.Name(), "ms_texture");
			refresh_color.value.i_val = ms_frame_unit;
			result = 0;
		} else {
			return 55455;
		}
	}


	if (!result) {
		if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 8*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 8*2 + 1];
			stencil_set.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16388, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 9*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 9*2 + 1];
			stencil_set.program.GeometryShader.Load(col_ptr + o_val, s_size); // FromResource(16389, libin);

			o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 10*2];
			s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 10*2 + 1];
			stencil_set.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16390, libin);

			strings_collection.Release();
		}

		if (result = stencil_set.program.Build()) {
			
			stencil_set.attbutes.location = GetUniformLocation(stencil_set.program.Name(), "attbutes");
			stencil_set.attbutes.value.i_val = attribute_unit;

			stencil_set.ms_texture.location = GetUniformLocation(stencil_set.program.Name(), "ms_texture");
			stencil_set.ms_texture.value.i_val = ms_frame_unit;

			result = 0;
		} else {
			return 666666;
		}

	}





















	if (!tex_ms) glGenTextures(1, &tex_ms);
	

	ActiveTexture(GL_TEXTURE0 + ms_frame_unit);
	glBindTexture(GL_TEXTURE_2D_ARRAY, tex_ms);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA32F, mww, mhh, 1+1, 0, GL_RGBA, GL_FLOAT, 0); // viewport width & height; detail_l

	result = glGetError();

	if (!result) {

		GenRenderbuffers(1, &builder_depth);
		BindRenderbuffer(GL_RENDERBUFFER, builder_depth);
		RenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mww, mhh); // GL_DEPTH_COMPONENT32F

		BindRenderbuffer(GL_RENDERBUFFER, 0);

		GenFramebuffers(1, &builder_frame);

		BindFramebuffer(GL_FRAMEBUFFER, builder_frame); // DRAW_
//		BindFramebuffer(GL_READ_FRAMEBUFFER, builder_frame);
		for (unsigned int ii(0);ii<=1;ii++) FramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+ii, tex_ms, 0, ii); // detail_l
	
		FramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, builder_depth);

		result |= (CheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE);

		glReadBuffer(GL_COLOR_ATTACHMENT0);



	}	

	return result;
}

OpenGL::Core::PrimitivePass * OpenGL::Core::PrimitivePass::Activate() {	
	
//	for (;destination>=detail_l;destination -= detail_l);
	draw_buffers[1] = GL_COLOR_ATTACHMENT1; //  + destination++;

	UseProgram(0);
	BindFramebuffer(GL_DRAW_FRAMEBUFFER, builder_frame); // 
	DrawBuffers(draw_buffer_count, draw_buffers);



	
	small_triangle.local_cfg.value[0].i_cmp = 1; // destination;
	small_triangle.local_cfg.value[1].i_cmp = 1;
	small_triangle.local_cfg.value[3].i_cmp = 1; // detail_l;
		
	small_line.local_cfg.value[0].i_cmp = 1; // destination;
	small_line.local_cfg.value[1].i_cmp = 1;
	small_line.local_cfg.value[3].i_cmp = 1; // detail_l;



//	glDepthFunc(GL_LESS);

	
	
	return this;
}

void OpenGL::Core::PrimitivePass::Clear(unsigned int drec, unsigned int c_type) {
	if (c_type) {
		UseProgram(clear_program.Name());
	
	} else {
		UseProgram(refresh_program.Name());
		if (refresh_color.location != -1) Uniform1i(refresh_color.location, refresh_color.value.i_val);
	}

	glDisable(GL_DEPTH_TEST);

		
	BindBuffer(GL_ARRAY_BUFFER, drec);
	VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);	

	glClear(GL_DEPTH_BUFFER_BIT);

	if (c_type) {
		stencil_set.Activate();
	} else {
		glEnable(GL_DEPTH_TEST);
	}
}

void OpenGL::Core::PrimitivePass::Destroy() {
	glDeleteTextures(1, &tex_ms);

	DeleteFramebuffers(1, &builder_frame);
	DeleteRenderbuffers(1, &builder_depth);

}

void OpenGL::Core::PrimitivePass::SetElTexture(unsigned int tid) {
	ActiveTexture(GL_TEXTURE0+image_unit);

	if (tid) glBindTexture(GL_TEXTURE_2D, tid);
	
}

void OpenGL::Core::PrimitivePass::SetMlTexture(unsigned int tid) {
	ActiveTexture(GL_TEXTURE0+ml_unit);

	if (tid) glBindTexture(GL_TEXTURE_2D_ARRAY, tid);
	
}


void OpenGL::Core::PrimitivePass::SetTolor(int vl) {
	if (small_triangle.local_cfg.location != -1) {
		small_triangle.local_cfg.value[1].i_cmp = vl;

		Uniform4iv(small_triangle.local_cfg.location, 1, &small_triangle.local_cfg.value[0].i_cmp);
	}
}

/*
void OpenGL::Core::PrimitivePass::ActivateClear() {

	clear_color.value[0].float_cmp = 1.0f;
	clear_color.value[1].float_cmp = 0.0f;
	clear_color.value[2].float_cmp = 1.0f;
	clear_color.value[3].i_cmp = 0;

	UseProgram(clear_program.Name());
//	if (clear_color.location != -1) Uniform4fv(clear_color.location, 1, &clear_color.value[0].float_cmp);

}
*/
