#pragma once

#define EXT_FUNC_INI(n,a,t) typedef t (__stdcall *Pgl##n##EXT)a; extern Pgl##n##EXT gl##n##EXT

// bindable uniform 
EXT_FUNC_INI(GetUniformBufferSize,(GLuint,int),int);
EXT_FUNC_INI(GetUniformOffset,(GLuint,int),GLintptr);
EXT_FUNC_INI(UniformBuffer,(GLuint,int,GLuint),void);

// blend color
EXT_FUNC_INI(BlendColor,(float,float,float,float),void);

// blend equation separate
EXT_FUNC_INI(BlendEquationSeparate,(GLenum,GLenum),void);

// blend func separate
EXT_FUNC_INI(BlendFuncSeparate,(GLenum,GLenum,GLenum,GLenum),void);

// blend minmax
EXT_FUNC_INI(BlendEquation,(GLenum),void);

// color subtable
EXT_FUNC_INI(ColorSubTable,(GLenum,GLsizei,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(CopyColorSubTable,(GLenum,GLsizei,int,int,GLsizei),void);

// compiled vertex array 
EXT_FUNC_INI(LockArrays,(int,GLsizei),void);
EXT_FUNC_INI(UnlockArrays,(void),void);

// convolution
EXT_FUNC_INI(ConvolutionFilter1,(GLenum,GLenum,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(ConvolutionFilter2,(GLenum,GLenum,GLsizei,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(ConvolutionParameterf,(GLenum,GLenum,float),void);
EXT_FUNC_INI(ConvolutionParameterfv,(GLenum,GLenum,const float *),void);
EXT_FUNC_INI(ConvolutionParameteri,(GLenum,GLenum,int),void);
EXT_FUNC_INI(ConvolutionParameteriv,(GLenum,GLenum,const int *),void);
EXT_FUNC_INI(CopyConvolutionFilter1,(GLenum,GLenum,int,int,GLsizei),void);
EXT_FUNC_INI(CopyConvolutionfilter2,(GLenum,GLenum,int,int,GLsizei,GLsizei),void);
EXT_FUNC_INI(GetConvolutionFilter,(GLenum,GLenum,GLenum,void *),void);
EXT_FUNC_INI(GetConvolutionParameterfv,(GLenum,GLenum,float *),void);
EXT_FUNC_INI(GetConvolutionParameteriv,(GLenum,GLenum,int *),void);
EXT_FUNC_INI(GetSeparableFilter,(GLenum,GLenum,GLenum,void *,void *,void *),void);
EXT_FUNC_INI(SeparableFilter2D,(GLenum,GLenum,GLsizei,GLsizei,GLenum,GLenum,const void *,const void *),void);

// coordinate frame
EXT_FUNC_INI(BinormalPointer,(GLenum,GLsizei,void *),void);
EXT_FUNC_INI(TangentPointer,(GLenum,GLsizei,void *),void);

// copy texture
EXT_FUNC_INI(CopyTexImage1,(GLenum,int,GLenum,int,int,GLsizei,int),void);
EXT_FUNC_INI(CopyTexImage2,(GLenum,int,GLenum,int,int,GLsizei,GLsizei,int),void);
EXT_FUNC_INI(CopyTexSubImage1,(GLenum,int,int,int,int,GLsizei),void);
EXT_FUNC_INI(CopyTexSubImage2,(GLenum,int,int,int,int,int,GLsizei,GLsizei),void);
EXT_FUNC_INI(CopyTexSubImage3,(GLenum,int,int,int,int,int,int,GLsizei,GLsizei),void);

// cull vertex
EXT_FUNC_INI(CullParameterdv,(GLenum,double *),void);
EXT_FUNC_INI(CullParameterfv,(GLenum,float *),void);

// depth bounds test
EXT_FUNC_INI(DepthBounds,(double,double),void);

// draw buffers2
EXT_FUNC_INI(ColorMaskIndexed,(GLuint,GLboolean,GLboolean,GLboolean,GLboolean),void);
EXT_FUNC_INI(DisableIndexed,(GLenum,GLuint),void);
EXT_FUNC_INI(EnableIndexed,(GLenum,GLuint),void);
EXT_FUNC_INI(GetBooleanIndexedv,(GLenum,GLuint,GLboolean *),void);
EXT_FUNC_INI(GetIntegerIndexedv,(GLenum,GLuint,int *),void);
EXT_FUNC_INI(IsEnabledIndexed,(GLenum,GLuint),GLboolean);

// draw instanced
EXT_FUNC_INI(DrawArraysInstanced,(GLenum,int,GLsizei,GLsizei),void);
EXT_FUNC_INI(DrawElementsInstanced,(GLenum,GLsizei,GLenum,const void *,GLsizei),void);

// draw range elements
EXT_FUNC_INI(DrawRangeElements,(GLenum,GLuint,GLuint,GLsizei,GLenum,const void *),void);

// framebuffer multisample
EXT_FUNC_INI(RenderbufferStorageMultisample,(GLenum,GLsizei,GLenum,GLsizei,GLsizei),void);

// framebuffer object
EXT_FUNC_INI(BindFramebuffer,(GLenum,GLuint),void);
EXT_FUNC_INI(BindRenderbuffer,(GLenum,GLuint),void);
EXT_FUNC_INI(CheckFramebufferStatus,(GLenum),GLenum);
EXT_FUNC_INI(DeleteFramebuffers,(GLsizei,const GLuint *),void);
EXT_FUNC_INI(DeleteRenderbuffers,(GLsizei,const GLuint *),void);
EXT_FUNC_INI(FramebufferRenderbuffer,(GLenum,GLenum,GLenum,GLuint),void);
EXT_FUNC_INI(FramebufferTexture1D,(GLenum,GLenum,GLenum,GLuint,int),void);
EXT_FUNC_INI(FramebufferTexture2D,(GLenum,GLenum,GLenum,GLuint,int),void);
EXT_FUNC_INI(FramebufferTexture3D,(GLenum,GLenum,GLenum,GLuint,int,int),void);
EXT_FUNC_INI(GenFramebuffers,(GLsizei,GLuint *),void);
EXT_FUNC_INI(GenRenderbuffers,(GLsizei,GLuint *),void);
EXT_FUNC_INI(GenerateMipmap,(GLenum),void);
EXT_FUNC_INI(GetFramebufferAttachmentParameteriv,(GLenum,GLenum,GLenum,int *),void);
EXT_FUNC_INI(GetRenderbufferParameteriv,(GLenum,GLenum,int *),void);
EXT_FUNC_INI(IsFramebuffer,(GLuint),GLboolean);
EXT_FUNC_INI(IsRenderbuffer,(GLuint),GLboolean);
EXT_FUNC_INI(RenderbufferStorage,(GLenum,GLenum,GLsizei,GLsizei),void);

// geometry shader4
EXT_FUNC_INI(FramebufferTexture,(GLenum,GLenum,GLuint,int),void);
EXT_FUNC_INI(FramebufferTextureFace,(GLenum,GLenum,GLuint,int,GLenum),void);
EXT_FUNC_INI(FramebufferTextureLayer,(GLenum,GLenum,GLuint,int,int),void);
EXT_FUNC_INI(ProgramParameteri,(GLuint,GLenum,int),void);

// gpu program parameters
EXT_FUNC_INI(ProgramEnvParametersfv,(GLenum,GLuint,GLsizei,const float *),void);
EXT_FUNC_INI(ProgramLocalParameters4fv,(GLenum,GLuint,GLsizei,const float *),void);

// gpu shader4
EXT_FUNC_INI(BindFragDataLocation,(GLuint,GLuint,const char *),void);
EXT_FUNC_INI(GetFragDataLocation,(GLuint,const char *),int);
EXT_FUNC_INI(GetUniformuiv,(GLuint,int,GLuint *),void);
EXT_FUNC_INI(GetVertexAttribIiv,(GLuint,GLenum,int *),void);
EXT_FUNC_INI(GetVertexAttribIuiv,(GLuint,GLenum,GLuint *),void);
EXT_FUNC_INI(Uniform1ui,(int,GLuint),void);
EXT_FUNC_INI(Uniform1uiv,(int,GLsizei,const GLuint *),void);
EXT_FUNC_INI(Uniform2ui,(int,GLuint,GLuint),void);
EXT_FUNC_INI(Uniform2uiv,(int,GLsizei,const GLuint *),void);
EXT_FUNC_INI(Uniform3ui,(int,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(Uniform3uiv,(int,GLsizei,const GLuint *),void);
EXT_FUNC_INI(Uniform4ui,(int,GLuint,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(Uniform4uiv,(int,GLsizei,const GLuint *),void);
EXT_FUNC_INI(VertexAttribI1i,(GLuint,int),void);
EXT_FUNC_INI(VertexAttribI1iv,(GLuint,const int *),void);
EXT_FUNC_INI(VertexAttribI1ui,(GLuint,GLuint),void);
EXT_FUNC_INI(VertexAttribI1uiv,(GLuint,const GLuint *),void);
EXT_FUNC_INI(VertexAttribI2i,(GLuint,int,int),void);
EXT_FUNC_INI(VertexAttribI2iv,(GLuint,const int *),void);
EXT_FUNC_INI(VertexAttribI2ui,(GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(VertexAttribI2uiv,(GLuint,const GLuint *),void);
EXT_FUNC_INI(VertexAttribI3i,(GLuint,int,int,int),void);
EXT_FUNC_INI(VertexAttribI3iv,(GLuint,const int *),void);
EXT_FUNC_INI(VertexAttribI3ui,(GLuint,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(VertexAttribI3uiv,(GLuint,const GLuint *),void);
EXT_FUNC_INI(VertexAttribI4i,(GLuint,int,int,int,int),void);
EXT_FUNC_INI(VertexAttribI4iv,(GLuint,const int *),void);
EXT_FUNC_INI(VertexAttribI4ui,(GLuint,GLuint,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(VertexAttribI4uiv,(GLuint,const GLuint *),void);
EXT_FUNC_INI(VertexAttribIPointer,(GLuint,int,GLenum,GLsizei,const void *),void);

// histogram
EXT_FUNC_INI(GetHistogram,(GLenum,GLboolean,GLenum,GLenum,void *),void);
EXT_FUNC_INI(GetHistogramParameterfv,(GLenum,GLenum,float *),void);
EXT_FUNC_INI(GetHistogramParameteriv,(GLenum,GLenum,int *),void);
EXT_FUNC_INI(GetMinmax,(GLenum,GLboolean,GLenum,GLenum,void *),void);
EXT_FUNC_INI(GetMinmaxParameterfv,(GLenum,GLenum,float *),void);
EXT_FUNC_INI(GetMinmaxParameteriv,(GLenum,GLenum,int *),void);
EXT_FUNC_INI(Histogram,(GLenum,GLsizei,GLenum,GLboolean),void);
EXT_FUNC_INI(Minmax,(GLenum,GLenum,GLboolean),void);
EXT_FUNC_INI(ResetHistogram,(GLenum),void);
EXT_FUNC_INI(ResetMinmax,(GLenum),void);

// indec func
EXT_FUNC_INI(IndexFunc,(GLenum,float),void);

// index material
EXT_FUNC_INI(IndexMaterial,(GLenum,GLenum),void);

// light texture 
EXT_FUNC_INI(ApplyTexture,(GLenum),void);
EXT_FUNC_INI(TextureLight,(GLenum),void);
EXT_FUNC_INI(TextureMaterial,(GLenum,GLenum),void);

// multi draw arrays
EXT_FUNC_INI(MultiDrawArrays,(GLenum,int *,GLsizei *,GLsizei),void);
EXT_FUNC_INI(MultiDrawElements,(GLenum,GLsizei *,GLenum, const void **,GLsizei),void);

// multisample
EXT_FUNC_INI(SampleMask,(float,GLboolean),void);
EXT_FUNC_INI(SamplePattern,(GLenum),void);

// paletted texture
EXT_FUNC_INI(ColorTable,(GLenum,GLenum,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(GetColorTable,(GLenum,GLenum,GLenum,void *),void);
EXT_FUNC_INI(GetColorTableParameterfv,(GLenum,GLenum,float *),void);
EXT_FUNC_INI(GetColorTableParameteriv,(GLenum,GLenum,int),void);

// pixel transform
EXT_FUNC_INI(GetPixelTransformParameterfv,(GLenum,GLenum,const float *),void);
EXT_FUNC_INI(GetPixelTransformParameteriv,(GLenum,GLenum,const int *),void);
EXT_FUNC_INI(PixelTransformParameterf,(GLenum,GLenum,float),void);
EXT_FUNC_INI(PixelTransformParameterfv,(GLenum,GLenum,const float *),void);
EXT_FUNC_INI(PixelTransformParameteri,(GLenum,GLenum,int),void);
EXT_FUNC_INI(PixelTransformParameteriv,(GLenum,GLenum,const int *),void);

// point parameters
EXT_FUNC_INI(PointParameterf,(GLenum,float),void);
EXT_FUNC_INI(PointParameterfv,(GLenum,float *),void);

// polygon offset
EXT_FUNC_INI(PolygonOffset,(float,float),void);

// scene marker
EXT_FUNC_INI(BeginScene,(void),void);
EXT_FUNC_INI(EndScene,(void),void);

// secondary color
EXT_FUNC_INI(SecondaryColor3b,(GLbyte,GLbyte,GLbyte),void);
EXT_FUNC_INI(SecondaryColor3bv,(const GLbyte *),void);
EXT_FUNC_INI(SecondaryColor3d,(double,double,double),void);
EXT_FUNC_INI(SecondaryColor3dv,(const double *),void);
EXT_FUNC_INI(SecondaryColor3f,(float,float,float),void);
EXT_FUNC_INI(SecondaryColor3fv,(const float *),void);
EXT_FUNC_INI(SecondaryColor3i,(int,int,int),void);
EXT_FUNC_INI(SecondaryColor3iv,(const int *),void);
EXT_FUNC_INI(SecondaryColor3s,(GLshort,GLshort,GLshort),void);
EXT_FUNC_INI(SecondaryColor3sv,(const GLshort *),void);
EXT_FUNC_INI(SecondaryColor3ub,(GLubyte,GLubyte,GLubyte),void);
EXT_FUNC_INI(SecondaryColor3ubv,(const GLubyte *),void);
EXT_FUNC_INI(SecondaryColor3ui,(GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(SecondaryColor3uiv,(const GLuint *),void);
EXT_FUNC_INI(SecondaryColor3us,(GLushort,GLushort,GLushort),void);
EXT_FUNC_INI(SecondaryColor3usv,(const GLushort *),void);
EXT_FUNC_INI(SecondaryColorPointer,(int,GLenum,GLsizei,void *),void);

// stencil two side
EXT_FUNC_INI(ActiveStencilFace,(GLenum),void);

// subtexture
EXT_FUNC_INI(TexSubImage1D,(GLenum,int,int,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(TexSubImage2D,(GLenum,int,int,int,GLsizei,GLsizei,GLenum,GLenum,const void *),void);
EXT_FUNC_INI(TexSubImage3D,(GLenum,int,int,int,int,GLsizei,GLsizei,GLsizei,GLenum,GLenum,const void *),void);

// texture3d
EXT_FUNC_INI(TexImage3D,(GLenum,int,GLenum,GLsizei,GLsizei,GLsizei,int,GLenum,GLenum,const void *),void);

// texture buffer object
EXT_FUNC_INI(TexBuffer,(GLenum,GLenum,GLuint),void);

// textuerr integer
EXT_FUNC_INI(ClearColorIi,(int,int,int,int),void);
EXT_FUNC_INI(ClearColorIui,(GLuint,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(GetTexParameterIiv,(GLenum,GLenum,int *),void);
EXT_FUNC_INI(GetTexParameterIuiv,(GLenum,GLenum,GLuint *),void);
EXT_FUNC_INI(TexParameterIiv,(GLenum,GLenum,const int *),void);
EXT_FUNC_INI(TexParameterIuiv,(GLenum,GLenum,const GLuint *),void);

// texture object
EXT_FUNC_INI(AreTexturesResident,(GLsizei,const GLuint *,GLboolean *),GLboolean);
EXT_FUNC_INI(BindTexture,(GLenum,GLuint),void);
EXT_FUNC_INI(DeleteTextures,(GLsizei,const GLuint *),void);
EXT_FUNC_INI(GenTextures,(GLsizei,GLuint *),void);
EXT_FUNC_INI(IsTexture,(GLuint),GLboolean);
EXT_FUNC_INI(PrioritizeTextures,(GLsizei,const GLuint *,const float *),void);

// texture pertrub normal
EXT_FUNC_INI(TextureNormal,(GLenum),void);

// timer query
EXT_FUNC_INI(GetQueryObjecti64v,(GLuint,GLenum,__int64 *),void);
EXT_FUNC_INI(GetQueryObjectui64v,(GLuint,GLenum,unsigned __int64 *),void);

// vertex array
EXT_FUNC_INI(ArrayElement,(int),void);
EXT_FUNC_INI(ColorPointer,(int,GLenum,GLsizei,GLsizei,const void *),void);
EXT_FUNC_INI(DrawArrays,(GLenum,int,GLsizei),void);
EXT_FUNC_INI(EdgeFlagPointer,(GLsizei,GLsizei,const GLboolean *),void);
EXT_FUNC_INI(GetPointerv,(GLenum,void **),void);
EXT_FUNC_INI(IndexPointer,(GLenum,GLsizei,GLsizei,const void *),void);
EXT_FUNC_INI(NormalPointer,(GLenum,GLsizei,GLsizei,const void *),void);
EXT_FUNC_INI(TexCoordPointer,(int,GLenum,GLsizei,GLsizei,const void *),void);
EXT_FUNC_INI(VertexPointer,(int,GLenum,GLsizei,GLsizei,const void *),void);

// vertex shader
EXT_FUNC_INI(BeginVertexShader,(void),void);
EXT_FUNC_INI(BindLightParameter,(GLenum,GLenum),GLuint);
EXT_FUNC_INI(BindMaterialParameter,(GLenum,GLenum),GLuint);
EXT_FUNC_INI(BindParameter,(GLenum),GLuint);
EXT_FUNC_INI(BindTexGenParameter,(GLenum,GLenum,GLenum),GLuint);
EXT_FUNC_INI(BindTextureUnitParameter,(GLenum,GLenum),GLuint);
EXT_FUNC_INI(BindVertexShader,(GLuint),void);
EXT_FUNC_INI(DeleteVertexShader,(GLuint),void);
EXT_FUNC_INI(DisableVariantClientState,(GLuint),void);
EXT_FUNC_INI(EnableVariantClientState,(GLuint),void);
EXT_FUNC_INI(EndVertexShader,(void),void);
EXT_FUNC_INI(ExtractComponent,(GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(GenSymbols,(GLenum,GLenum,GLenum,GLuint),GLuint);
EXT_FUNC_INI(GenVertexShaders,(GLuint),GLuint);
EXT_FUNC_INI(GetInvariantBooleanv,(GLuint,GLenum,GLboolean *),void);
EXT_FUNC_INI(GetInvariantFloatv,(GLuint,GLenum,float *),void);
EXT_FUNC_INI(GetInvariantIntegerv,(GLuint,GLenum,int *),void);
EXT_FUNC_INI(GetLocalConstantBooleanv,(GLuint,GLenum,GLboolean *),void);
EXT_FUNC_INI(GetLocalConstantFloatv,(GLuint,GLenum,float *),void);
EXT_FUNC_INI(GetLocalConstantIntegerv,(GLuint,GLenum,int *),void);
EXT_FUNC_INI(GetVariantBooleanv,(GLuint,GLenum,GLboolean *),void);
EXT_FUNC_INI(GetVariantFloatv,(GLuint,GLenum,float *),void);
EXT_FUNC_INI(GetVariantIntegerv,(GLuint,GLenum,int *),void);
EXT_FUNC_INI(GetVariantPointer,(GLuint,GLenum,void **),void);
EXT_FUNC_INI(InsertComponent,(GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(IsVariantEnabled,(GLuint,GLenum),GLboolean);
EXT_FUNC_INI(SetInvariant,(GLuint,GLenum,void *),void);
EXT_FUNC_INI(SetLocalConstant,(GLuint,GLenum,void *),void);
EXT_FUNC_INI(ShaderOp1,(GLenum,GLuint,GLuint),void);
EXT_FUNC_INI(ShaderOp2,(GLenum,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(ShaderOp3,(GLenum,GLuint,GLuint,GLuint,GLuint),void);
EXT_FUNC_INI(Swizzle,(GLuint,GLuint,GLenum,GLenum,GLenum,GLenum),void);
EXT_FUNC_INI(VariantPointer,(GLuint,GLenum,GLuint,void *),void);
EXT_FUNC_INI(Variantbv,(GLuint,GLbyte *),void);
EXT_FUNC_INI(Variantdv,(GLuint,double *),void);
EXT_FUNC_INI(Variantfv,(GLuint,float *),void);
EXT_FUNC_INI(Variantiv,(GLuint,int *),void);
EXT_FUNC_INI(Variantsv,(GLuint,GLshort *),void);
EXT_FUNC_INI(Variantubv,(GLuint,GLubyte *),void);
EXT_FUNC_INI(Variantuiv,(GLuint,GLuint *),void);
EXT_FUNC_INI(Variantusv,(GLuint,GLushort *),void);
EXT_FUNC_INI(WriteMask,(GLuint,GLuint,GLenum,GLenum,GLenum,GLenum),void);

// vertex weighting
EXT_FUNC_INI(VertexWeightPointer,(int,GLenum,GLsizei,void *),void);
EXT_FUNC_INI(VertexWeightf,(float),void);
EXT_FUNC_INI(VertexWeightfv,(float *),void);

//EXT_FUNC_INI(,(),void);

// 422 pixels
#define GL_EXT__422								 0x80CC
#define GL_EXT__422_REV							 0x80CD
#define GL_EXT__422_AVERAGE						 0x80CE
#define GL_EXT__422_REV_AVERAGE					 0x80CF

// Cg shader
#define GL_EXT_CG_VERTEX_SHADER					 0x890E
#define GL_EXT_CG_FRAGMENT_SHADER					 0x890F

// abgr
#define GL_EXT_ABGR								 0x8000

// bgra
#define GL_EXT_BGR								 0x80E0
#define GL_EXT_BGRA								 0x80E1

// bindable uniform
#define GL_EXT_MAX_VERTEX_BINDABLE_UNIFORMS		 0x8DE2
#define GL_EXT_MAX_FRAGMENT_BINDABLE_UNIFORMS		 0x8DE3
#define GL_EXT_MAX_GEOMETRY_BINDABLE_UNIFORMS		 0x8DE4
#define GL_EXT_MAX_BINDABLE_UNIFORM_SIZE			 0x8DED
#define GL_EXT_UNIFORM_BUFFER						 0x8DEE
#define GL_EXT_UNIFORM_BUFFER_BINDING				 0x8DEF

// blend color
#define GL_EXT_CONSTANT_COLOR						 0x8001
#define GL_EXT_ONE_MINUS_CONSTANT_COLOR			 0x8002
#define GL_EXT_CONSTANT_ALPHA						 0x8003
#define GL_EXT_ONE_MINUS_CONSTANT_ALPHA			 0x8004
#define GL_EXT_BLEND_COLOR						 0x8005

//blend equation separate
#define GL_EXT_BLEND_EQUATION_RGB					 0x8009
#define GL_EXT_BLEND_EQUATION_ALPHA				 0x800A

// blend func separate
#define GL_EXT_BLEND_DST_RGB						 0x80C8
#define GL_EXT_BLEND_SRC_RGB						 0x80C9
#define GL_EXT_BLEND_DST_ALPHA					 0x80CA
#define GL_EXT_BLEND_SRC_ALPHA					 0x80CB

// blend minmax
#define GL_EXT_FUNC_ADD							 0x8006
#define GL_EXT_MIN								 0x8007
#define GL_EXT_MAX								 0x8008
#define GL_EXT_BLEND_EQUATION						 0x8009

// blend subtract
#define GL_EXT_FUNC_SUBTRACT						 0x800A
#define GL_EXT_FUNC_REVERSE_SUBTRACT				 0x800B

// cmyka
#define GL_EXT_CMYK								 0x800C
#define GL_EXT_CMYKA								 0x800D
#define GL_EXT_PACK_CMYK_HINT						 0x800E
#define GL_EXT_UNPACK_CMYK_HINT					 0x800F

// convolution
#define GL_EXT_CONVOLUTION_1D						 0x8010
#define GL_EXT_CONVOLUTION_2D						 0x8011
#define GL_EXT_SEPARABLE_2D						 0x8012
#define GL_EXT_CONVOLUTION_BORDER_MODE			 0x8013
#define GL_EXT_CONVOLUTION_FILTER_SCALE			 0x8014
#define GL_EXT_CONVOLUTION_FILTER_BIAS			 0x8015
#define GL_EXT_REDUCE								 0x8016
#define GL_EXT_CONVOLUTION_FORMAT					 0x8017
#define GL_EXT_CONVOLUTION_WIDTH					 0x8018
#define GL_EXT_CONVOLUTION_HEIGHT					 0x8019
#define GL_EXT_MAX_CONVOLUTION_WIDTH				 0x801A
#define GL_EXT_MAX_CONVOLUTION_HEIGHT				 0x801B
#define GL_EXT_POST_CONVOLUTION_RED_SCALE			 0x801C
#define GL_EXT_POST_CONVOLUTION_GREEN_SCALE		 0x801D
#define GL_EXT_POST_CONVOLUTION_BLUE_SCALE		 0x801E
#define GL_EXT_POST_CONVOLUTION_ALPHA_SCALE		 0x801F
#define GL_EXT_POST_CONVOLUTION_RED_BIAS			 0x8020
#define GL_EXT_POST_CONVOLUTION_GREEN_BIAS		 0x8021
#define GL_EXT_POST_CONVOLUTION_BLUE_BIAS			 0x8022
#define GL_EXT_POST_CONVOLUTION_ALPHA_BIAS		 0x8023

// coordinate frame
#define GL_EXT_TANGENT_ARRAY						 0x8439
#define GL_EXT_BINORMAL_ARRAY						 0x843A
#define GL_EXT_CURRENT_TANGENT					 0x843B
#define GL_EXT_CURRENT_BINORMAL					 0x843C
#define GL_EXT_TANGENT_ARRAY_TYPE					 0x843E
#define GL_EXT_TANGENT_ARRAY_STRIDE				 0x843F
#define GL_EXT_BINORMAL_ARRAY_TYPE				 0x8440
#define GL_EXT_BINORMAL_ARRAY_STRIDE				 0x8441
#define GL_EXT_TANGENT_ARRAY_POINTER				 0x8442
#define GL_EXT_BINORMAL_ARRAY_POINTER				 0x8443
#define GL_EXT_MAP1_TANGENT						 0x8444
#define GL_EXT_MAP2_TANGENT						 0x8445
#define GL_EXT_MAP1_BINORMAL						 0x8446
#define GL_EXT_MAP2_BINORMAL						 0x8447

// depth bounds test
#define GL_EXT_DEPTH_BOUNDS_TEST					 0x8890
#define GL_EXT_DEPTH_BOUNDS						 0x8891

// draw range elements
#define GL_EXT_MAX_ELEMENTS_VERTICES				 0x80E8
#define GL_EXT_MAX_ELEMENTS_INDICES				 0x80E9

//frame buffer multisample
#define GL_EXT_RENDERBUFFER_SAMPLES				 0x8CAB
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE	 0x8D56
#define GL_EXT_MAX_SAMPLES						 0x8D57

// framebuffer object
#define GL_EXT_INVALID_FRAMEBUFFER_OPERATION		 0x0506
#define GL_EXT_MAX_RENDER_BUFFER_SIZE				 0x84E8
#define GL_EXT_FRAMEBUFFER_BINDING				 0x8CA6
#define GL_EXT_RENDERBUFfER_BINDING				 0x8CA7
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE				 0x8CD0
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME				 0x8CD1
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL			 0x8CD2
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE	 0x8CD3
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET		 0x8CD4
#define GL_EXT_FRAMEBUFFER_COMPLETE							 0x8CD5
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_ATTACHEMENT				 0x8CD6
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMNET		 0x8CD7
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_DIMENTIONS				 0x8CD9
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_FORMATS					 0x8CDA
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER				 0x8CDB
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_READ_BUFFER				 0x8CDC
#define GL_EXT_FRAMEBUFFER_UNSUPPORTED						 0x8CDD
#define GL_EXT_MAX_COLOR_ATTACHMENTS				 0x8CDF
#define GL_EXT_COLOR_ATTACHMENT0					 0x8CE0
#define GL_EXT_DEPTH_ATTACHMENT					 0x8D00
#define GL_EXT_STENCIL_ATTACHMENT					 0x8D20
#define GL_EXT_FRAMEBUFFER						 0x8D40
#define GL_EXT_RENDERBUFFER						 0x8D41
#define GL_EXT_RENDERBUFFER_WIDTH					 0x8D42
#define GL_EXT_RENDERBUFFER_HEIGHT				 0x8D43
#define GL_EXT_RENDERBUFFER_INTERNAL_FORMAT		 0x8D44
#define GL_EXT_STENCIL_INDEX1						 0x8D46
#define GL_EXT_STENCIL_INDEX4						 0x8D47
#define GL_EXT_STENCIL_INDEX8						 0x8D48
#define GL_EXT_STENCIL_INDEX16					 0x8D49
#define GL_EXT_RENDERBUFFER_RED_SIZE				 0x8D50
#define GL_EXT_RENDERBUFFER_GREEN_SIZE			 0x8D51
#define GL_EXT_RENDERBUFFER_BLUE_SIZE				 0x8D52
#define GL_EXT_RENDERBUFFER_ALPHA_SIZE			 0x8D53
#define GL_EXT_RENDERBUFFER_DEPTH_SIZE			 0x8D54
#define GL_EXT_REDNERBUFFER_STENCIL_SIZE			 0x8D55

// framebuffer sRGB
#define GL_EXT_FRAMEBUFFER_SRGB					 0x8DB9
#define GL_EXT_FRAMEBUFFER_SRGB_CAPABLE			 0x8DBA

// geometry shader4
#define GL_EXT_LINES_ADJACENCY					 0x000A
#define GL_EXT_LINE_STrIP_ADJACENCY				 0x000B
#define GL_EXT_TRIANGLES_ADJACENCY				 0x000C
#define GL_EXT_TRIANLE_STRIP_ADJACENCY			 0x000D
#define GL_EXT_PROGRAM_POINT_SIZE					 0x8642
#define GL_EXT_MAX_VARYING_COMPONENTS				 0x8B4B
#define GL_EXT_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS	 0x8C29
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEYER	 0x8CD4
#define GL_EXT_FRAMEBUFFER_ATTACHMENT_LAYERED			 0x8DA7
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS	 0x8DA8
#define GL_EXT_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT		 0x8DA9
#define GL_EXT_GEOMETRY_SAHDER					 0x8DD9
#define GL_EXT_GEOMETRY_VERTICES_OUT				 0x8DDA
#define GL_EXT_GEOMETRY_INPUT_TYPE				 0x8DDB
#define GL_EXT_GEOMETRY_OUTPUT_TYPE				 0x8DDC
#define GL_EXT_MAX_GEOMETRY_VARYING_COMPONENTS	 0x8DDD
#define GL_EXT_MAX_VERTEX_VARYING_COMPONENTS		 0x8DDE
#define GL_EXT_MAX_GEOMETRY_UNIFORM_COMPONENTS	 0x8DDF
#define GL_EXT_MAX_GEOMETRY_OUTPUT_VERTICES		 0x8DE0
#define GL_EXT_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS 0x8DE1

// gpu shader
#define GL_EXT_VERTEX_ATTRIB_ARRAY_INTEGER		 0x88FD
#define GL_EXT_SAMPLER_1D_ARRAY					 0x8DC0
#define GL_EXT_SAMPLER_2D_ARRAY					 0x8DC1
#define GL_EXT_SAMPLER_BUFFER						 0x8DC2
#define GL_EXT_SAMPLER_1D_ARRAY_SHADOW			 0x8DC3
#define GL_EXT_SAMPLER_2D_ARRAY_SHADOW			 0x8DC4
#define GL_EXT_SAMPLER_CUBE_SHADOW				 0x8DC5
#define GL_EXT_UNSIGNED_INT_VEC2					 0x8DC6
#define GL_EXT_UNSIGNED_INT_VEC3					 0x8DC7
#define GL_EXT_UNSIGNED_INT_VEC4					 0x8DC8
#define GL_EXT_INT_SAMPLER_1D						 0x8DC9
#define GL_EXT_INT_SAMPLER_2D						 0x8DCA
#define GL_EXT_INT_SAMPLER_3D						 0x8DCB
#define GL_EXT_INT_SAMPLER_CUBE					 0x8DCC
#define GL_EXT_INT_SAMPLER_2D_RECT				 0x8DCD
#define GL_EXT_INT_SAMPLER_1D_ARRAY				 0x8DCE
#define GL_EXT_INT_SAMPLER_2D_ARRAY				 0x8DCF
#define GL_EXT_INT_SAMPLER_BUFFER					 0x8DD0
#define GL_EXT_UNSIGNED_INT_SAMPLER_1D			 0x8DD1
#define GL_EXT_UNSIGNED_INT_SAMPLER_2D			 0x8DD2
#define GL_EXT_UNSIGNED_INT_SAMPLER_3D			 0x8DD3
#define GL_EXT_UNSIGNED_INT_SAMPLER_CUBE			 0x8DD4
#define GL_EXT_UNSIGNED_INT_SAMPLER_2D_RECT		 0x8DD5
#define GL_EXT_UNSIGNED_INT_SAMPLER_1D_ARRAY		 0x8DD6
#define GL_EXT_UNSIGNED_INT_SAMPLER_2D_ARRAY		 0x8DD7
#define GL_EXT_UNSIGNED_INT_SAMPLER_BUFFER		 0x8DD8

// histogram
#define GL_EXT_HISTOGRAM							 0x8024
#define GL_EXT_PROXY_HISTOGRAM					 0x8025
#define GL_EXT_HISTOGRAM_WIDTH					 0x8026
#define GL_EXT_HISTOGRAM_FORMAT					 0x8027
#define GL_EXT_HISTOGRAM_RED_SIZE					 0x8028
#define GL_EXT_HISTOGRAM_GREEN_SIZE				 0x8029
#define GL_EXT_HISTOGRAM_BLUE_SIZE				 0x802A
#define GL_EXT_HISTOGRAM_ALPHA_SIZE				 0x802B
#define GL_EXT_HISTOGRAM_LUMINANCE_SIZE			 0x802C
#define GL_EXT_HISTOGRAM_SINK						 0x802D
#define GL_EXT_MINMAX								 0x802E
#define GL_EXT_MINMAX_FORMAT						 0x802F
#define GL_EXT_MINMAX_SINK						 0x8030

// light texture
#define GL_EXT_FRAGMENT_MATERIAL					 0x8349
#define GL_EXT_FRAGMENT_NORMAL					 0x834A
#define GL_EXT_FRAGMENT_COLOR						 0x834C
#define GL_EXT_ATTENUATION						 0x834D
#define GL_EXT_DHADOW_ATTENUATION					 0x834E
#define GL_EXT_TEXTURE_APPLICATION_MODE			 0x834F
#define GL_EXT_TEXTURE_LIGHT						 0x8350
#define GL_EXT_TEXTURE_MATERIAL_FACE				 0x8351
#define GL_EXT_TEXTURE_MATERIAL_PARAMETER			 0x8352
#define GL_EXT_FRAGMENT_DEPTH						 0x8353

// multisample
#define GL_EXT_MULTISAMPLE						 0x809D
#define GL_EXT_SAMPLE_ALPHA_TO_MASK				 0x809E
#define GL_EXT_SAMPLE_ALPHA_TO_ONE				 0x809F
#define GL_EXT_SAMPLE_MASK						 0x80A0
#define GL_EXT__1PASS								 0x80A1
#define GL_EXT__2PASS_0							 0x80A2
#define GL_EXT__2PASS_1							 0x80A3
#define GL_EXT__4PASS_0							 0x80A4
#define GL_EXT__4PASS_1							 0x80A5
#define GL_EXT__4PASS_2							 0x80A6
#define GL_EXT__4PASS_3							 0x80A7
#define GL_EXT_SAMPLE_BUFFERS						 0x80A8
#define GL_EXT_SAMPLES							 0x80A9
#define GL_EXT_SAMPLE_MASK_VALUE					 0x80AA
#define GL_EXT_SAMPLE_MASK_INVERT					 0x80AB
#define GL_EXT_SAMPLE_PATTERN						 0x80AC
#define GL_EXT_MULTISAMPLE_BIT					 0x20000000

// packed depth stencil
#define GL_EXT_DEPTH_STENCIL						 0x84F9
#define GL_EXT_UNSIGNED_INT_24_8					 0x84FA
#define GL_EXT_DEPTH24_STENCIL8					 0x88F0
#define GL_EXT_TEXTURE_STENCIL_SIZE				 0x88F1

// packed float
#define GL_EXT_R11F_G11F_B10F						 0x8C3A
#define GL_EXT_UNSIGNED_INT_10F_11F_11F_REV		 0x8C3B
#define GL_EXT_RGBA_SIGNED_COMPONENTS				 0x8C3C

// packed pixels
#define GL_EXT_UNSIGNED_BYTE_3_3_2				 0x8032
#define GL_EXT_UNSIGNED_SHORT_4_4_4_4				 0x8033
#define GL_EXT_UNSIGNED_SHORT_5_5_5_1				 0x8034
#define GL_EXT_UNSIGNED_INT_8_8_8_8				 0x8035
#define GL_EXT_UNSIGNED_INT_10_10_10_2			 0x8036

// paletted texture
#define GL_EXT_COLOR_TABLE_FORMAT					 0x80D8
#define GL_EXT_COLOR_TABLE_WIDTH					 0x80D9
#define GL_EXT_COLOR_TABLE_RED_SIZE				 0x80DA
#define GL_EXT_COLOR_TABLE_GREEN_SIZE				 0x80DB
#define GL_EXT_COLOR_TABLE_BLUE_SIZE				 0x80DC
#define GL_EXT_COLOR_TABLE_ALPHA_SIZE				 0x80DD
#define GL_EXT_COLOR_TABLE_LUMINANCE_SIZE			 0x80DE
#define GL_EXT_COLOR_INDEX_1						 0x80E2
#define GL_EXT_COLOR_INDEX_2						 0x80E3
#define GL_EXT_COLOR_INDEX_4						 0x80E4
#define GL_EXT_COLOR_INDEX_8						 0x80E5
#define GL_EXT_COLOR_INDEX_12						 0x80E6
#define GL_EXT_COLOR_INDEX_16						 0x80E7
#define GL_EXT_TEXTURE_INDEX_SIZE					 0x80ED
	
#define GL_EXT_PROXY_TEXTURE_MAP					 0x851B

// pixel buffer object
#define GL_EXT_PIXEL_PACK_BUFFER					 0x88EB
#define GL_EXT_PIXEL_UNPACK_BUFFER				 0x88EC
#define GL_EXT_PIXEL_PACK_BUFFER_BINDING			 0x88ED
#define GL_EXT_PIXEL_UNPACK_BUFFER_BINDING		 0x88EF

// pixel transform
#define GL_EXT_PIXEL_TRANSFORM_2D					 0x8330
#define GL_EXT_PIXEL_MAG_FILTER					 0x8331
#define GL_EXT_PIXEL_MIN_FILTER					 0x8332
#define GL_EXT_PIXEL_CUBIC_WEIGHT					 0x8833
#define GL_EXT_CUBIC								 0x8334
#define GL_EXT_AVARAGE							 0x8335
#define GL_EXT_PIXEL_TRANSFORM_2D_STACK_DEPTH		 0x8336
#define GL_EXT_MAX_PIXEL_TRANSFORM_2D_STACK_DEPTH	 0x8337
#define GL_EXT_PIXEL_TRNASFORM_2D_MATRIX			 0x8338

// point parameters
#define GL_EXT_POINT_SIZE_MIN						 0x8126
#define GL_EXT_POINT_SIZE_MAX						 0x8127
#define GL_EXT_POINT_FADE_THRESHOLD				 0x8128
#define GL_EXT_DISTANCE_ATTENUATION				 0x8129

// polygon offset
#define GL_EXT_POLYGON_OFFSET						 0x8037
#define GL_EXT_POLYGON_OFFSET_FACTOR				 0x8038
#define GL_EXT_POLYGON_OFFSET_BIAS				 0x8039

// secondary color
#define GL_EXT_COLOR_SUM							 0x8458
#define GL_EXT_CURRENT_SECONDARY_COLOR			 0x8459
#define GL_EXT_SECONDARY_COLOR_ARRAY_SIZE			 0x845A
#define GL_EXT_SECONDARY_COLOR_ARRAY_TYPE			 0x845B
#define GL_EXT_SECONDARY_COLOR_ARRAY_STRIDE		 0x845C
#define GL_EXT_SECONDARY_COLOR_ARRAY_POINTER		 0x845D
#define GL_EXT_SECONDARY_COLOR_ARRAY				 0x845E

// separate specular color
#define GL_EXT_LIGHT_MODEL_COLOR_CONTROL			 0x81F8
#define GL_EXT_SINGLE_COLOR						 0x81F9
#define GL_EXT_SEPARATE_SPECULAR_COLOR			 0x81FA

// shared texture palette
#define GL_EXT_SHARED_TEXTURE_PALETTE				 0x81FB

// stencil clear tag
#define GL_EXT_STENCIL_TAG_BITS					 0x88F2
#define GL_EXT_STENCIL_CLEAR_TAG_VALUE			 0x88F3

// stencil two side
#define GL_EXT_STENCIL_TEST_TWO_SIDE				 0x8910
#define GL_EXT_ACTIVE_STANCIL_FACE				 0x8911

// stencil wrap
#define GL_EXT_INCR_WRAP							 0x8507
#define GL_EXT_DECR_WRAP							 0x8508

// texture
#define GL_EXT_ALPHA4								 0x803B
#define GL_EXT_ALPHA8								 0x803C
#define GL_EXT_ALPHA12							 0x803D
#define GL_EXT_ALPHA16							 0x803E
#define GL_EXT_LUMINANCE4							 0x803F
#define GL_EXT_LUMINANCE8							 0x8040
#define GL_EXT_LUMINANCE12						 0x8041
#define GL_EXT_LUMINANCE16						 0x8042
#define GL_EXT_LUMINANCE4_ALPHA4					 0x8043
#define GL_EXT_LUMINANCE6_ALPHA2					 0x8044
#define GL_EXT_LUMINANCE8_ALPHA8					 0x8045
#define GL_EXT_LUMINANCE12_ALPHA4					 0x8046
#define GL_EXT_LUMINANCE12_ALPHA12				 0x8047
#define GL_EXT_LUMINANCE16_ALPHA16				 0x8048
#define GL_EXT_INTENSITY							 0x8049
#define GL_EXT_INTENSITY4							 0x804A
#define GL_EXT_INTENSITY8							 0x804B
#define GL_EXT_INTENSITY12						 0x804C
#define GL_EXT_INTENSITY16						 0x804D
#define GL_EXT_RGB2								 0x804E
#define GL_EXT_RGB4								 0x804F
#define GL_EXT_RGB5								 0x8050
#define GL_EXT_RGB8								 0x8051
#define GL_EXT_RGB10								 0x8052
#define GL_EXT_RGB12								 0x8053
#define GL_EXT_RGB16								 0x8054
#define GL_EXT_RGBA2								 0x8055
#define GL_EXT_RGBA4								 0x8056
#define GL_EXT_RGB5_A1							 0x8057
#define GL_EXT_RGBA8								 0x8058
#define GL_EXT_RGB10_A2							 0x8059
#define GL_EXT_RGBA12								 0x805A
#define GL_EXT_RGBA16								 0x805B
#define GL_EXT_TEXTURE_RED_SIZE					 0x805C
#define GL_EXT_TEXTURE_GREEN_SIZE					 0x805D
#define GL_EXT_TEXTURE_BLUE_SIZE					 0x805E
#define GL_EXT_TEXTURE_ALPHA_SIZE					 0x805F
#define GL_EXT_TEXTURE_LUMINANCE_SIZE				 0x8060
#define GL_EXT_TEXTURE_INTENSITY_SIZE				 0x8061
#define GL_EXT_REPLACE							 0x8062
#define GL_EXT_PROXY_TEXTURE_1D					 0x8063
#define GL_EXT_PROXY_TEXTURE_2D					 0x8064

// texture 3d
#define GL_EXT_PACK_SKIP_IMAGES					 0x806B
#define GL_EXT_PACK_IMAGE_HEIGHT					 0x806C
#define GL_EXT_UNPACK_SKIP_IMAGES					 0x806D
#define GL_EXT_UNPACK_IMAGE_HEIGHT				 0x806E
#define GL_EXT_TEXTURE_3D							 0x806F
#define GL_EXT_PROXY_TEXTURE_3D					 0x8070
#define GL_EXT_TEXTURE_DEPTH						 0x8071
#define GL_EXT_TEXTURE_WRAP_R						 0x8072
#define GL_EXT_MAX_3D_TEXTURE_SIZE				 0x8073

// texture array
#define GL_EXT_COMPARE_REF_DEPTH_TO_TEXTURE		 0x884E
#define GL_EXT_MAX_ARRAY_TEXTURE_LAYERS			 0x88FF
#define GL_EXT_TEXTURE_1D_ARRAY					 0x8C18
#define GL_EXT_PROXY_TEXTURE_1D_ARRAY				 0x8C19
#define GL_EXT_TEXTURE_2D_ARRAY					 0x8C1A
#define GL_EXT_PROXY_TEXTURE_2D_ARRAY				 0x8C1B
#define GL_EXT_TEXTURE_BINDING_1D_ARRAY			 0x8C1C
#define GL_EXT_TEXTURE_BINDING_2D_ARRAY			 0x8C1D

// texture buffer object
#define GL_EXT_TEXTURE_BUFFER						 0x8C2A
#define GL_EXT_MAX_TEXTURE_BUFFER_SIZE			 0x8C2B
#define GL_EXT_TEXTURE_BINDING_BUFFER				 0x8C2C
#define GL_EXT_TEXTURE_BUFFER_DATA_STORE_BINDING	 0x8C2D
#define GL_EXT_TEXTURE_BUFFER_FORMAT				 0x8C2E

// texture compression dxt1
#define GL_EXT_COMPRESSED_RGB_S3TC_DXT1			 0x83F0
#define GL_EXT_COMPRESSED_RGBA_S3TC_DXT1			 0x83F1

// texture compression latc
#define GL_EXT_COMPRESSED_LUMINANCE_LATC1			 0x8C70
#define GL_EXT_COMPRESSED_SIGNED_LUMINANCE_LATC1	 0x8C71
#define GL_EXT_COMPRESSED_LUMINANCE_ALPHA_LATC2	 0x8C72
#define GL_EXT_COMPRESSED_SIGNED_LUMINANCE_ALPHA_LATC2	 0x8C73

// texture compression rgtc
#define GL_EXT_COMPRESSED_RED_RGTC1				 0x8DBB
#define GL_EXT_COMPRESSED_SIGNED_RED_RGTC1		 0x8DBC
#define GL_EXT_COMPRESSED_RED_GREEN_RGTC2			 0x8DBD
#define GL_EXT_COMPRESSED_SIGNED_RED_GREEN_RGTC2	 0x8DBE

// texture compression s3tc
#define GL_EXT_COMPRESSED_RGBA_S3TC_DXT3			 0x83F2
#define GL_EXT_COMPRESSED_RGBA_S3TC_DXT5			 0x83F3

// texture cube map
#define GL_EXT_NORMAL_MAP							 0x8511
#define GL_EXT_REFLECTION_MAP						 0x8512
#define GL_EXT_TEXTURE_CUBE_MAP					 0x8513
#define GL_EXT_TEXTURE_BINDING_CUBE_MAP			 0x8514
#define GL_EXT_TEXTURE_CUBE_MAP_POSITIVE_X		 0x8515
#define GL_EXT_TEXTURE_CUBE_MAP_NEGATIVE_X		 0x8516
#define GL_EXT_TEXTURE_CUBE_MAP_POSITIVE_Y		 0x8517
#define GL_EXT_TEXTURE_CUBE_MAP_NEGATIVE_Y		 0x8518
#define GL_EXT_TEXTURE_CUBE_MAP_POSITIVE_Z		 0x8519
#define GL_EXT_TEXTURE_CUBE_MAP_NEGATIVE_Z		 0x851A
#define GL_EXT_PROXY_TEXTURE_CUBE_MAP				 0x851B
#define GL_EXT_MAX_CUBE_MAP_TEXTURE_SIZE			 0x851C

// texture edge clamp
#define GL_EXT_CLAMP_TO_EDGGE						 0x812F

// texture env
#define GL_EXT_TEXTURE_ENV0						 0x0000
#define GL_EXT_ENV_BLEND							 0x0000
#define GL_EXT_TEXTURE_ENV_SHIFT					 0x0000
#define GL_EXT_ENV_REPLACE						 0x0000
#define GL_EXT_ENV_ADD							 0x0000
#define GL_EXT_ENV_SUBTRACT						 0x0000
#define GL_EXT_TEXTURE_ENV_MODE_ALPHA				 0x0000
#define GL_EXT_ENV_REVERSE_SUBTRACT				 0x0000
#define GL_EXT_ENV_REVERSE_BLEND					 0x0000
#define GL_EXT_ENV_COPY							 0x0000
#define GL_EXT_ENV_MODULATE						 0x0000

// texture env combine
#define GL_EXT_COMBINE							 0x8570
#define GL_EXT_COMBINE_RGB						 0x8571
#define GL_EXT_COMBINE_ALPHA						 0x8572
#define GL_EXT_RGB_SCALE							 0x8573
#define GL_EXT_ADD_SIGNED							 0x8574
#define GL_EXT_INTERPOLATE						 0x8575
#define GL_EXT_CONSTANT							 0x8576
#define GL_EXT_PRIMARY_COLOR						 0x8577
#define GL_EXT_PREVIOUS							 0x8578
#define GL_EXT_SOURCE0_RGB						 0x8580
#define GL_EXT_SOURCE1_RGB						 0x8581
#define GL_EXT_SOURCE2_RGB						 0x8582
#define GL_EXT_SOURCE0_ALPHA						 0x8588
#define GL_EXT_SOURCE1_ALPHA						 0x8589
#define GL_EXT_SOURCE2_ALPHA						 0x858A
#define GL_EXT_OPERAND0_RGB						 0x8590
#define GL_EXT_OPERAND1_RGB						 0x8591
#define GL_EXT_OPERAND2_RGB						 0x8592
#define GL_EXT_OPERAND0_ALPHA						 0x8598
#define GL_EXT_OPERAND1_ALPHA						 0x8599
#define GL_EXT_OPERAND2_ALPHA						 0x859A

// texture env dot3
#define GL_EXT_DOT3_RGB							 0x8740
#define GL_EXT_DOT3_RGBA							 0x8741

// texture filter anisotropic
#define GL_EXT_TEXTURE_MAX_ANISOTROPY				 0x84FE
#define GL_EXT_MAX_TEXTURE_MAX_ANISOTROPY			 0x84FF

// texture integer
#define GL_EXT_RGBA32UI							 0x8D70
#define GL_EXT_RGB32UI							 0x8D71
#define GL_EXT_ALPHA32UI							 0x8D72
#define GL_EXT_INTENSITY32UI						 0x8D73
#define GL_EXT_LUMINANCE32UI						 0x8D74
#define GL_EXT_LUMINANCE_ALPHA32UI				 0x8D75
#define GL_EXT_RGBA16UI							 0x8D76
#define GL_EXT_RGB16UI							 0x8D77
#define GL_EXT_ALPHA16UI							 0x8D78
#define GL_EXT_INTENSITY16UI						 0x8D79
#define GL_EXT_LUMINANCE16UI						 0x8D7A
#define GL_EXT_LUMINANCE_ALPHA16UI				 0x8D7B
#define GL_EXT_RGBA8UI							 0x8D7C
#define GL_EXT_RGB8UI								 0x8D7D
#define GL_EXT_ALPHA8UI							 0x8D7E
#define GL_EXT_INTENSITY8UI						 0x8D7F
#define GL_EXT_LUMINANCE8UI						 0x8D80
#define GL_EXT_LUMINANCE_ALPHA8UI					 0x8D81
#define GL_EXT_RGBA32I							 0x8D82
#define GL_EXT_RGB32I								 0x8D83
#define GL_EXT_ALPHA32I							 0x8D84
#define GL_EXT_INTENSITY32I						 0x8D85
#define GL_EXT_LUMINANCE32I						 0x8D86
#define GL_EXT_LUMINANCE_ALPHA32I					 0x8D87
#define GL_EXT_RGBA16I							 0x8D88
#define GL_EXT_RGB16I								 0x8D89
#define GL_EXT_ALPHA16I							 0x8D8A
#define GL_EXT_INTENSITY16I						 0x8D8B
#define GL_EXT_LUMINANCE16I						 0x8D8C
#define GL_EXT_LUMINANCE_ALPHA16I					 0x8D8D
#define GL_EXT_RGBA8I								 0x8D8E
#define GL_EXT_RGB8I								 0x8D8F
#define GL_EXT_ALPHA8I							 0x8D90
#define GL_EXT_INTENSITY8I						 0x8D91
#define GL_EXT_LUMINANCE8I						 0x8D92
#define GL_EXT_LUMINANCE_ALPHA8I					 0x8D93
#define GL_EXT_RED_INTEGER						 0x8D94
#define GL_EXT_GREEN_INTEGER						 0x8D95
#define GL_EXT_BLUE_INTEGER						 0x8D96
#define GL_EXT_ALPHA_INTEGER						 0x8D97
#define GL_EXT_RGB_INTEGER						 0x8D98
#define GL_EXT_RGBA_INTEGER						 0x8D99
#define GL_EXT_BGR_INTEGER						 0x8D9A
#define GL_EXT_BGRA_INTEGER						 0x8D9B
#define GL_EXT_LUMINANCE_INTEGER					 0x8D9C
#define GL_EXT_LUMINANCE_ALPHA_INTEGER			 0x8D9D
#define GL_EXT_RGBA_INTEGER_MODE					 0x8D9E

// texture lod bias
#define GL_EXT_MAX_TEXTURE_LOD_BIAS				 0x84FD
#define GL_EXT_TEXTURE_FILTER_CONTROL				 0x8500
#define GL_EXT_TEXTURE_LOD_BIAS					 0x8501

// texture mirror clamp
#define GL_EXT_MIRROR_CLAMP						 0x8742
#define GL_EXT_MIRROR_CLAMP_TO_EDGE				 0x8743
#define GL_EXT_MIRROR_CLAMP_TO_BORDER				 0x8912

// texture object
#define GL_EXT_TEXTURE_PRIORITY					 0x8066
#define GL_EXT_TEXTURE_RESIDENT					 0x8067
#define GL_EXT_TEXTURE_1D_BINDING					 0x8068
#define GL_EXT_TEXTURE_2D_BINDING					 0x8069
#define GL_EXT_TEXTURE_3D_BINDING					 0x806A

// texture pertrub normal
#define GL_EXT_PERTRUB							 0x85AE
#define GL_EXT_TEXTURE_NORMAL						 0x85AF

// texture rectangle
#define GL_EXT_TEXTURE_RECTANGLE					 0x84F5
#define GL_EXT_TEXTURE_BINDING_RECTANGLE			 0x84F6
#define GL_EXT_PROXY_TEXTURE_RECTANGLE			 0x84F7
#define GL_EXT_MAX_RECTANGLE_TEXTURE_SIZE			 0x84F8

// texture srgb
#define GL_EXT_SRGB								 0x8C40
#define GL_EXT_SRGB8								 0x8C41
#define GL_EXT_SRGB_ALPHA							 0x8C42
#define GL_EXT_SRGB8_ALPHA8						 0x8C43
#define GL_EXT_SLUMINANCE_ALPHA					 0x8C44
#define GL_EXT_SLUMINANCE8_ALPHA8					 0x8C45
#define GL_EXT_SLUMINANCE							 0x8C46
#define GL_EXT_SLUMINANCE8						 0x8C47
#define GL_EXT_COMPRESSED_SRGB					 0x8C48
#define GL_EXT_COMPRESSED_RGB_ALPHA				 0x8C49
#define GL_EXT_COMPRESSED_SLUMINANCE				 0x8C4A
#define GL_EXT_COMPRESSED_SLUMINANCE_ALPHA		 0x8C4B
#define GL_EXT_COMPRESSED_SRGB_S3RC_DXT1			 0x8C4C
#define GL_EXT_COMPRESSED_SRGB_ALPHA_S3TC_DXT1	 0x8C4D
#define GL_EXT_COMPRESSED_SRGB_ALPHA_S3TC_DXT3	 0x8C4E
#define GL_EXT_COMPRESSED_SRGB_ALPHA_S3TC_DXT5	 0x8C4F

// texture shared exponent
#define GL_EXT_RGB9_E5							 0x8C3D
#define GL_EXT_UNSIGNED_INT_5_9_9_9				 0x8C3E
#define GL_EXT_TEXTURE_SHARED_SIZE				 0x8C3F

//  time elapsed
#define GL_EXT_TIME_ELAPSED						 0x88BF

// vertex array
#define GL_EXT_DOUBLE								 0x140A
#define GL_EXT_VERTEX_ARRAY						 0x8074
#define GL_EXT_NORMAL_ARRAY						 0x8075
#define GL_EXT_COLOR_ARRAY						 0x8076
#define GL_EXT_INDEX_ARRAY						 0x8077
#define GL_EXT_TEXTURE_COORD_ARRAY				 0x8078
#define GL_EXT_EDGE_FLAG_ARRAY					 0x8079
#define GL_EXT_VERTEX_ARRAY_SIZE					 0x807A
#define GL_EXT_VERTEX_ARRAY_TYPE					 0x807B
#define GL_EXT_VERTEX_ARRAY_STRIDE				 0x807C
#define GL_EXT_VERTEX_ARRAY_COUNT					 0x807D
#define GL_EXT_NORMAL_ARRAY_TYPE					 0x807E
#define GL_EXT_NORMAL_ARRAY_STRIDE				 0x807F
#define GL_EXT_NORMAL_ARRAY_COUNT					 0x8080
#define GL_EXT_COLOR_ARRAY_SIZE					 0x8081
#define GL_EXT_COLOR_ARRAY_TYPE					 0x8082
#define GL_EXT_COLOR_ARRAY_STRIDE					 0x8083
#define GL_EXT_COLOR_ARRAY_COUNT					 0x8084
#define GL_EXT_INDEX_ARRAY_TYPE					 0x8085
#define GL_EXT_INDEX_ARRAY_STRIDE					 0x8086
#define GL_EXT_INDEX_ARRAY_COUNT					 0x8087
#define GL_EXT_TEXTURE_COORD_ARRAY_SIZE			 0x8088
#define GL_EXT_TEXTURE_COORD_ARRAY_TYPE			 0x8089
#define GL_EXT_TEXTURE_COORD_ARRAY_STRIDE			 0x808A
#define GL_EXT_TEXTURE_COORD_ARRAY_COUNT			 0x808B
#define GL_EXT_EDGE_FLAG_ARRAY_STRIDE				 0x808C
#define GL_EXT_EDGE_FLAG_ARRAY_COUNT				 0x808D
#define GL_EXT_VERTEX_ARRAY_POINTER				 0x808E
#define GL_EXT_NORMAL_ARRAY_POINTER				 0x808F
#define GL_EXT_COLOR_ARRAY_POINTER				 0x8090
#define GL_EXT_INDEX_ARRAY_POINTER				 0x8091
#define GL_EXT_TEXTURE_COORD_ARRAY_POINTER		 0x8092
#define GL_EXT_EDGE_FLAG_ARRAY_POINTER			 0x8093
	
// vertex shader
#define GL_EXT_VERTEX_SHADER						 0x8780
#define GL_EXT_VERTEX_SHADERR_BINDING				 0x8781
#define GL_EXT_OP_INDEX							 0x8782
#define GL_EXT_OP_NEGATE							 0x8783
#define GL_EXT_OP_DOT3							 0x8784
#define GL_EXT_OP_DOT4							 0x8785
#define GL_EXT_OP_MUL								 0x8786
#define GL_EXT_OP_ADD								 0x8787
#define GL_EXT_OP_MADD							 0x8788
#define GL_EXT_OP_FRAC							 0x8789
#define GL_EXT_OP_MAX								 0x878A
#define GL_EXT_OP_MIN								 0x878B
#define GL_EXT_OP_SET_GE							 0x878C
#define GL_EXT_OP_SET_LT							 0x878D
#define GL_EXT_OP_CLAMP							 0x878E
#define GL_EXT_OP_FLOOR							 0x878F
#define GL_EXT_OP_ROUND							 0x8790
#define GL_EXT_OP_EXP_BASE_2						 0x8791
#define GL_EXT_OP_LOG_BASE_2						 0x8792
#define GL_EXT_OP_POWER							 0x8793
#define GL_EXT_OP_RECIP							 0x8794
#define GL_EXT_OP_RECIP_SQRT						 0x8795
#define GL_EXT_OP_SUB								 0x8796
#define GL_EXT_OP_CROSS_PRODUCT					 0x8797
#define GL_EXT_OP_MULTIPLY_VERTEX					 0x8798
#define GL_EXT_OP_MOV								 0x8799
#define GL_EXT_OUTPUT_VERTEX						 0x879A
#define GL_EXT_OUTPUT_COLOR0						 0x879B
#define GL_EXT_OUTPUT_COLOR1						 0x879C
#define GL_EXT_OUTPUT_TEXTURE_COORD0				 0x879D
#define GL_EXT_OUTPUT_FOG							 0x87BD
#define GL_EXT_SCALAR								 0x87BE
#define GL_EXT_VECTOR								 0x87BF
#define GL_EXT_MATRIX								 0x87C0
#define GL_EXT_VARIANT							 0x87C1
#define GL_EXT_INVARIANT							 0x87C2
#define GL_EXT_LOCAL_CONSTANT						 0x87C3
#define GL_EXT_LOCAL								 0x87C4
#define GL_EXT_MAX_VERTEX_SHADER_INSTRUCTIONS		 0x87C5
#define GL_EXT_MAX_VERTEX_SHADER_VARIANTS			 0x87C6
#define GL_EXT_MAX_VERTEX_SHADER_INVARIANTS		 0x87C7
#define GL_EXT_MAX_VERTEX_SHADER_LOCAL_CONSTANTS	 0x87C8
#define GL_EXT_MAX_VERTEX_SHADERS_LOCAL			 0x87C9
#define GL_EXT_MAX_OPTIMIZED_VERTEX_SHADER_INSTRUCTIONS		 0x87CA
#define GL_EXT_MAX_OPTIMIZED_VERTEX_SHADER_VARIANTS			 0x87CB
#define GL_EXT_MAX_OPTIMIZED_VERTEX_SHADER_INVARIANTS			 0x87CC
#define GL_EXT_MAX_OPTIMIZED_VERTEX_SHADER_LOCAL_CONSTANTS	 0x87CD
#define GL_EXT_MAX_OPTIMIZED_VERTEX_SHADER_LOCALS				 0x87CE
#define GL_EXT_VERTEX_SHADER_INSTRUCTIONS			 0x87CF
#define GL_EXT_VERTEX_SHADER_VARIANTS				 0x87D0
#define GL_EXT_VERTEX_SHADER_INVARIANTS			 0x87D1
#define GL_EXT_VERTEX_SHADER_LOCAL_CONSTANTS		 0x87D2
#define GL_EXT_VERTEX_SHADER_LOACALS				 0x87D3
#define GL_EXT_VERTEX_SHADER_OPTIMIZED			 0x87D4
#define GL_EXT_X									 0x87D5
#define GL_EXT_Y									 0x87D6
#define GL_EXT_Z									 0x87D7
#define GL_EXT_W									 0x87D8
#define GL_EXT_NEGATIVE_X							 0x87D9	
#define GL_EXT_NEGATIVE_Y							 0x87DA
#define GL_EXT_NEGATIVE_Z							 0x87DB
#define GL_EXT_NEGATIVE_W							 0x87DC
#define GL_EXT_ZERO								 0x87DD
#define GL_EXT_ONE								 0x87DE
#define GL_EXT_NEGATIVE_ONE						 0x87DF
#define GL_EXT_NORMALIZED_RANGE					 0x87E0
#define GL_EXT_FULL_RANGE							 0x87E1
#define GL_EXT_CURRENT_VERTEX						 0x87E2
#define GL_EXT_MVP_MATRIX							 0x87E3
#define GL_EXT_VARIANT_VALUE						 0x87E4
#define GL_EXT_VARIANT_DATATYPE					 0x87E5
#define GL_EXT_VARIANT_ARRAY_STRIDE				 0x87E6
#define GL_EXT_VARIANT_ARRAY_TYPE					 0x87E7
#define GL_EXT_VARIANT_ARRAY						 0x87E8
#define GL_EXT_VARIANT_ARRAY_POINTER				 0x87E9
#define GL_EXT_INVARIANT_VALUE					 0x87EA
#define GL_EXT_INVARIANT_DATATYPE					 0x87EB
#define GL_EXT_LOCAL_CONSTANT_VALUE				 0x87EC
#define GL_EXT_LOCAL_CONSTANT_DATATYPE			 0x87ED

// vertex weighting
#define GL_EXT_MODELVIEW0_STACK_DEPTH				 0x0BA3
#define GL_EXT_MODELVIEW0_MATRIX					 0x0BA6
#define GL_EXT_MODELVIEW0							 0x1700
#define GL_EXT_MODELVIEW1_STACK_DEPTH				 0x8502
#define GL_EXT_MODELVIEW1_MATRIX					 0x8506
#define GL_EXT_MODELVIEW1							 0x850A
#define GL_EXT_VERTEX_WEIGHTING					 0x8509
#define GL_EXT_CURRENT_VERTEX_WEIGHT				 0x850B
#define GL_EXT_VERTEX_WEIGHT_ARRAY				 0x850C
#define GL_EXT_VERTEX_WEIGHT_ARRAY_SIZE			 0x850D
#define GL_EXT_VERTEX_WEIGHT_ARRAY_TYPE			 0x850E
#define GL_EXT_VERTEX_WEIGHT_ARRAY_STRIDE			 0x850F
#define GL_EXT_VERTEX_WEIGHT_ARRAY_POINTER		 0x8510

