/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http:ssafe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#pragma warning(disable:4244)

//#include "OpenGL\OpenGL.h"
//#include "OpenGL\Element.h"

#include "Apps\NotificationArea.h"

#include "System\SysUtils.h"
#include "OpenGL\Font.h"

#include "Pics\Image.h"
#include "Media\Source.h"

#include "OGL_15.h"
#include "OGL_43.h"

#include "OGL_WGL.h"

#include "System\Control.h"

#include "Math\CalcSpecial.h"



__declspec(align(16)) short OpenGL::Core::mag_filter[65536];

GLenum default_bufs[] = {GL_FRONT_LEFT, GL_BACK_LEFT, GL_NONE, GL_FRONT_LEFT, GL_NONE};

PIXELFORMATDESCRIPTOR OpenGL::Core::pixelFormatDesc = {0};
int OpenGL::Core::_pfIndex = 0;

unsigned int OpenGL::Core::_version = 0;
unsigned int OpenGL::Core::max_supported_tunits = 0;

unsigned int OpenGL::Core::max_geometry_out = 0;
unsigned int OpenGL::Core::max_color_bufs = 0;
unsigned int OpenGL::Core::max_tbuffer_size = 0;
unsigned int OpenGL::Core::max_texture_layers = 0;
unsigned int OpenGL::Core::uniform_alignment = 0;

int OpenGL::Core::max_tex_vals[4];

int OpenGL::Core::_globalX = 0x7FFFFFFF;
int OpenGL::Core::_globalY = 0;
int OpenGL::Core::_globalPX = 0;
int OpenGL::Core::_globalPY = 0;
int OpenGL::Core::_lockedX = 0;
int OpenGL::Core::_lockedY = 0;

int OpenGL::Core::_x16 = 0;
int OpenGL::Core::_y16 = 0;

unsigned int OpenGL::Core::special_x_size = 1024;

__declspec(align(256)) unsigned int OpenGL::Core::loc_buf[OpenGL::Core::loc_buf_scale*OpenGL::Core::loc_buf_scale];

int OpenGL::Core::_compatibility = 1;
const char * OpenGL::Core::extensionsString = 0;

const int OpenGL::Core::db_rect[] = {0, 0, 64, 64};

const float OpenGL::Core::default_rect[] = {	-1.0f, -1.0f, 0.0f, 1.0f,
												1.0f, -1.0f, 0.0f, 1.0f,
												-1.0f, 1.0f, 0.0f, 1.0f,
												1.0f, 1.0f, 0.0f, 1.0f,
										
												-1.0f, -1.0f, 0.0f, 1.0f,
												1.0f, -1.0f, 0.0f, 1.0f,
												-1.0f, 1.0f, 0.0f, 1.0f,
												1.0f, 1.0f, 0.0f, 1.0f
											};

unsigned int OpenGL::Core::monitor_count = 0;
unsigned int OpenGL::Core::max_width = 0;
unsigned int OpenGL::Core::max_height = 0;

SFSco::Object OpenGL::Core::strings_collection;

OpenGL::DecodeRecord::DecodeRecord() {
	System::MemoryFill(this, sizeof(DecodeRecord), 0);
}


// ===========================================================================================================================================================================================

OpenGL::Core::Core() : _dc(0), _rc(0), _hwnd(0), _element_pool(0), _frame_rect(0), _inertion_count(0), _status(0), _terminate(1), _current_selection(-1), _current_focus(-1), chain_marker(0), _cs_locked(0), _vao(0), _menu_corner(SFSco::IOpenGL::RightBottom), _cursor_id(-1), blink_runner(0), blink_top(0), fback_runner(0), fback_top(0), motion_runner(0), motion_top(0), autohide_runner(0), autohide_top(0), blur_level(3) {
	
	system_icos = sys_ico_runner = warning_icon = pause_icon = play_icon = slider_icon = size_icon = shot_icon = info_pic = delete_icon = rec_icon = blist_tid = blist_lid = dec_icon = inc_icon = vol_icon = left_icon = right_icon = up_icon = down_icon = close_icon = zoom_icon = 0;
	
	bg_color[0] = bg_color[1] = bg_color[2] = 0.0f;
	bg_color[3] = 1.0f;

	_builder_thread = 0;
	render_count = 0;
	cursor_count = 0;
	
	f_lags = 0;
//	f_lags = Memory::Chain::IncLock();
//	_ulist_selector = f_lags + 32;
}

OpenGL::Core::~Core() {
	_terminate = 0;
	if (_builder_thread) {		
		WaitForSingleObject(_builder_thread, INFINITE);
		CloseHandle(_builder_thread);
	}


	if (_dc) {
		if (_rc) {
			DeleteVertexArrays(1, &_vao);
			DeleteBuffers(1, &_frame_rect);
						
			primPass.Destroy();
			textPass.Destroy();
			cursor.Destroy();
//			videoPass.Destroy();

			att_domain.Destroy();
		}

		wglMakeCurrent(_dc, 0);

		if (_rc) wglDeleteContext(_rc);
	
		ReleaseDC(_hwnd, _dc);
						
	}

	if (_element_pool) delete _element_pool;
		
	screen_buf.Destroy();

}

void OpenGL::Core::GetGlobal(float * x_ptr) {
	x_ptr[0] = _globalX;
	x_ptr[1] = _globalY;
}

void OpenGL::Core::SetElement(Element & el_obj, UI_64 el_id) {
	el_obj.Set(_element_pool, el_id);
}

bool OpenGL::Core::NoScene(Element & t_obj) {
	return (t_obj != _scene);
}

bool OpenGL::Core::IsRoot(Element & t_obj) {
	return (t_obj == _root);
}


void OpenGL::Core::RootAdjust(float * b_ptr, unsigned int b_co) {
	AttributeArray elattr;
	_scene.InitAttributeArray(AttributeArray::Vertex, elattr);
	elattr.LocalUAdjust(b_ptr, b_co);						
}

unsigned int OpenGL::Core::GetPFI() {
	return _pfIndex;
}

const PIXELFORMATDESCRIPTOR * OpenGL::Core::GetPFD() {
	return &pixelFormatDesc;
}

HWND OpenGL::Core::GetWinHandle() {
	return _hwnd;
}

unsigned int OpenGL::Core::GetTimeCount() {
	return render_count;
}

unsigned int __fastcall OpenGL::Core::LockedRCShare(HGLRC src) {	
	if (!wglShareLists(_rc, src)) return GetLastError();
	return 0;
}

unsigned int OpenGL::Core::ShareRC(HGLRC scntx) {
	if (GetCurrentThreadId() != GetWindowThreadProcessId(_hwnd, 0)) {
		if (HANDLE local_block = CreateEvent(0, 0, 0, 0)) {
			unsigned int (__fastcall Core::* srcl)(HGLRC) = &Core::LockedRCShare;
			void * ptr_list[4];
			ptr_list[0] = scntx;
			ptr_list[1] = this;
			ptr_list[2] = reinterpret_cast<void*&>(srcl);
			ptr_list[3] = 0;

			
			PostMessage(_hwnd, INVOKE_IT, (WPARAM)ptr_list, (LPARAM)local_block);
			WaitForSingleObject(local_block, INFINITE);

			CloseHandle(local_block);

			return reinterpret_cast<UI_64 *>(ptr_list)[3];
		}		
	} else {
		return LockedRCShare(scntx);
	}
	return -2;
}

int __fastcall OpenGL::Core::Initialize(void * hwnd) {
	int result(0);
	
	double some_vals[32];

	if (!result)
	if (hwnd) {

		mag_filter[0] = 0;
		mag_filter[1] = 0;
		mag_filter[2] = 0;
		mag_filter[3] = 0;
			
		mag_filter[4] = 0;
		mag_filter[5] = 32767;
		mag_filter[6] = 0;
		mag_filter[7] = 0;
			
		mag_filter[8] = 0;
		mag_filter[9] = 0;
		mag_filter[10] = 0;
		mag_filter[11] = 0;

		mag_filter[12] = 0;
		mag_filter[13] = 0;
		mag_filter[14] = 0;
		mag_filter[15] = 0;


		for (unsigned int i(1);i<64;i++) {
			some_vals[16] = 0.0;
			some_vals[0] = (830.02313639803102366826056261409/(64.0 + i)/(64.0 + i))*Calc::RealSin(180.0*(64.0 + i)/64.0)*Calc::RealSin(180.0*(64.0 + i)/128.0); some_vals[16] += some_vals[0];
			some_vals[1] = (830.02313639803102366826056261409/(i)/(i))*Calc::RealSin(180.0*(i)/64.0)*Calc::RealSin(180.0*(i)/128.0); some_vals[16] += some_vals[1];
			some_vals[2] = (830.02313639803102366826056261409/(64.0 - i)/(64.0 - i))*Calc::RealSin(180.0*(64.0 - i)/64.0)*Calc::RealSin(180.0*(64.0 - i)/128.0); some_vals[16] += some_vals[2];
			some_vals[3] = (830.02313639803102366826056261409/(128.0 - i)/(128.0 - i))*Calc::RealSin(180.0*(128.0 - i)/64.0)*Calc::RealSin(180.0*(128.0 - i)/128.0); some_vals[16] += some_vals[3];

			some_vals[16] = 32767.0/some_vals[16];

			some_vals[0] *= some_vals[16];
			some_vals[1] *= some_vals[16];
			some_vals[2] *= some_vals[16];
			some_vals[3] *= some_vals[16];


			mag_filter[i*16 + 0] = 0;
			mag_filter[i*16 + 1] = some_vals[0];
			mag_filter[i*16 + 2] = 0;
			mag_filter[i*16 + 3] = 0;
			
			mag_filter[i*16 + 4] = 0;
			mag_filter[i*16 + 5] = some_vals[1];
			mag_filter[i*16 + 6] = 0;
			mag_filter[i*16 + 7] = 0;
			
			mag_filter[i*16 + 8] = 0;
			mag_filter[i*16 + 9] = some_vals[2];
			mag_filter[i*16 + 10] = 0;
			mag_filter[i*16 + 11] = 0;

			mag_filter[i*16 + 12] = 0;
			mag_filter[i*16 + 13] = some_vals[3];
			mag_filter[i*16 + 14] = 0;
			mag_filter[i*16 + 15] = 0;

		}


		for (unsigned int j(1);j<64;j++) {
			some_vals[16] = 0.0;
			some_vals[0] = (830.02313639803102366826056261409/(64.0 + j)/(64.0 + j))*Calc::RealSin(180.0*(64.0 + j)/64.0)*Calc::RealSin(180.0*(64.0 + j)/128.0); some_vals[16] += some_vals[0];
			some_vals[1] = (830.02313639803102366826056261409/(j)/(j))*Calc::RealSin(180.0*(j)/64.0)*Calc::RealSin(180.0*(j)/128.0); some_vals[16] += some_vals[1];
			some_vals[2] = (830.02313639803102366826056261409/(64.0 - j)/(64.0 - j))*Calc::RealSin(180.0*(64.0 - j)/64.0)*Calc::RealSin(180.0*(64.0 - j)/128.0); some_vals[16] += some_vals[2];
			some_vals[3] = (830.02313639803102366826056261409/(128.0 - j)/(128.0 - j))*Calc::RealSin(180.0*(128.0 - j)/64.0)*Calc::RealSin(180.0*(128.0 - j)/128.0); some_vals[16] += some_vals[3];

			some_vals[16] = 32767.0/some_vals[16];
		
			some_vals[0] *= some_vals[16];
			some_vals[1] *= some_vals[16];
			some_vals[2] *= some_vals[16];
			some_vals[3] *= some_vals[16];

			mag_filter[j*64*16 + 0] = 0;
			mag_filter[j*64*16 + 1] = 0;
			mag_filter[j*64*16 + 2] = 0;
			mag_filter[j*64*16 + 3] = 0;
			
			mag_filter[j*64*16 + 4] = some_vals[0];
			mag_filter[j*64*16 + 5] = some_vals[1];
			mag_filter[j*64*16 + 6] = some_vals[2];
			mag_filter[j*64*16 + 7] = some_vals[3];
			
			mag_filter[j*64*16 + 8] = 0;
			mag_filter[j*64*16 + 9] = 0;
			mag_filter[j*64*16 + 10] = 0;
			mag_filter[j*64*16 + 11] = 0;

			mag_filter[j*64*16 + 12] = 0;
			mag_filter[j*64*16 + 13] = 0;
			mag_filter[j*64*16 + 14] = 0;
			mag_filter[j*64*16 + 15] = 0;

		}

		for (unsigned int i(1);i<64;i++) {
			for (unsigned int j(1);j<64;j++) {
				some_vals[16] = 0.0;

				some_vals[0] = (688938.40695602441313926840749454/(64.0 + i)/(64.0 + i)/(64.0 + j)/(64.0 + j))*Calc::RealSin(180.0*(64.0 + i)/64.0)*Calc::RealSin(180.0*(64.0 + i)/128.0)*Calc::RealSin(180.0*(64.0 + j)/64.0)*Calc::RealSin(180.0*(64.0 + j)/128.0); some_vals[16] += some_vals[0];
				some_vals[1] = (688938.40695602441313926840749454/(i)/(i)/(64.0 + j)/(64.0 + j))*Calc::RealSin(180.0*(i)/64.0)*Calc::RealSin(180.0*(i)/128.0)*Calc::RealSin(180.0*(64.0 + j)/64.0)*Calc::RealSin(180.0*(64.0 + j)/128.0); some_vals[16] += some_vals[1];
				some_vals[2] = (688938.40695602441313926840749454/(64.0 - i)/(64.0 - i)/(64.0 + j)/(64.0 + j))*Calc::RealSin(180.0*(64.0 - i)/64.0)*Calc::RealSin(180.0*(64.0 - i)/128.0)*Calc::RealSin(180.0*(64.0 + j)/64.0)*Calc::RealSin(180.0*(64.0 + j)/128.0); some_vals[16] += some_vals[2];
				some_vals[3] = (688938.40695602441313926840749454/(128.0 - i)/(128.0 - i)/(64.0 + j)/(64.0 + j))*Calc::RealSin(180.0*(128.0 - i)/64.0)*Calc::RealSin(180.0*(128.0 - i)/128.0)*Calc::RealSin(180.0*(64.0 + j)/64.0)*Calc::RealSin(180.0*(64.0 + j)/128.0); some_vals[16] += some_vals[3];
		
				some_vals[4] = (688938.40695602441313926840749454/(64.0 + i)/(64.0 + i)/(j)/(j))*Calc::RealSin(180.0*(64.0 + i)/64.0)*Calc::RealSin(180.0*(64.0 + i)/128.0)*Calc::RealSin(180.0*(j)/64.0)*Calc::RealSin(180.0*(j)/128.0); some_vals[16] += some_vals[4];
				some_vals[5] = (688938.40695602441313926840749454/(i)/(i)/(j)/(j))*Calc::RealSin(180.0*(i)/64.0)*Calc::RealSin(180.0*(i)/128.0)*Calc::RealSin(180.0*(j)/64.0)*Calc::RealSin(180.0*(j)/128.0); some_vals[16] += some_vals[5];
				some_vals[6] = (688938.40695602441313926840749454/(64.0 - i)/(64.0 - i)/(j)/(j))*Calc::RealSin(180.0*(64.0 - i)/64.0)*Calc::RealSin(180.0*(64.0 - i)/128.0)*Calc::RealSin(180.0*(j)/64.0)*Calc::RealSin(180.0*(j)/128.0); some_vals[16] += some_vals[6];
				some_vals[7] = (688938.40695602441313926840749454/(128.0 - i)/(128.0 - i)/(j)/(j))*Calc::RealSin(180.0*(128.0 - i)/64.0)*Calc::RealSin(180.0*(128.0 - i)/128.0)*Calc::RealSin(180.0*(j)/64.0)*Calc::RealSin(180.0*(j)/128.0); some_vals[16] += some_vals[7];
			
				some_vals[8] = (688938.40695602441313926840749454/(64.0 + i)/(64.0 + i)/(64.0 - j)/(64.0 - j))*Calc::RealSin(180.0*(64.0 + i)/64.0)*Calc::RealSin(180.0*(64.0 + i)/128.0)*Calc::RealSin(180.0*(64.0 - j)/64.0)*Calc::RealSin(180.0*(64.0 - j)/128.0); some_vals[16] += some_vals[8];
				some_vals[9] = (688938.40695602441313926840749454/(i)/(i)/(64.0 - j)/(64.0 - j))*Calc::RealSin(180.0*(i)/64.0)*Calc::RealSin(180.0*(i)/128.0)*Calc::RealSin(180.0*(64.0 - j)/64.0)*Calc::RealSin(180.0*(64.0 - j)/128.0); some_vals[16] += some_vals[9];
				some_vals[10] = (688938.40695602441313926840749454/(64.0 - i)/(64.0 - i)/(64.0 - j)/(64.0 - j))*Calc::RealSin(180.0*(64.0 - i)/64.0)*Calc::RealSin(180.0*(64.0 - i)/128.0)*Calc::RealSin(180.0*(64.0 - j)/64.0)*Calc::RealSin(180.0*(64.0 - j)/128.0); some_vals[16] += some_vals[10];
				some_vals[11] = (688938.40695602441313926840749454/(128.0 - i)/(128.0 - i)/(64.0 - j)/(64.0 - j))*Calc::RealSin(180.0*(128.0 - i)/64.0)*Calc::RealSin(180.0*(128.0 - i)/128.0)*Calc::RealSin(180.0*(64.0 - j)/64.0)*Calc::RealSin(180.0*(64.0 - j)/128.0); some_vals[16] += some_vals[11];

				some_vals[12] = (688938.40695602441313926840749454/(64.0 + i)/(64.0 + i)/(128.0 - j)/(128.0 - j))*Calc::RealSin(180.0*(64.0 + i)/64.0)*Calc::RealSin(180.0*(64.0 + i)/128.0)*Calc::RealSin(180.0*(128.0 - j)/64.0)*Calc::RealSin(180.0*(128.0 - j)/128.0); some_vals[16] += some_vals[12];
				some_vals[13] = (688938.40695602441313926840749454/(i)/(i)/(128.0 - j)/(128.0 - j))*Calc::RealSin(180.0*(i)/64.0)*Calc::RealSin(180.0*(i)/128.0)*Calc::RealSin(180.0*(128.0 - j)/64.0)*Calc::RealSin(180.0*(128.0 - j)/128.0); some_vals[16] += some_vals[13];
				some_vals[14] = (688938.40695602441313926840749454/(64.0 - i)/(64.0 - i)/(128.0 - j)/(128.0 - j))*Calc::RealSin(180.0*(64.0 - i)/64.0)*Calc::RealSin(180.0*(64.0 - i)/128.0)*Calc::RealSin(180.0*(128.0 - j)/64.0)*Calc::RealSin(180.0*(128.0 - j)/128.0); some_vals[16] += some_vals[14];
				some_vals[15] = (688938.40695602441313926840749454/(128.0 - i)/(128.0 - i)/(128.0 - j)/(128.0 - j))*Calc::RealSin(180.0*(128.0 - i)/64.0)*Calc::RealSin(180.0*(128.0 - i)/128.0)*Calc::RealSin(180.0*(128.0 - j)/64.0)*Calc::RealSin(180.0*(128.0 - j)/128.0); some_vals[16] += some_vals[15];

				some_vals[16] = 32767.0/some_vals[16];

				some_vals[0] *= some_vals[16];
				some_vals[1] *= some_vals[16];
				some_vals[2] *= some_vals[16];
				some_vals[3] *= some_vals[16];

				some_vals[4] *= some_vals[16];
				some_vals[5] *= some_vals[16];
				some_vals[6] *= some_vals[16];
				some_vals[7] *= some_vals[16];

				some_vals[8] *= some_vals[16];
				some_vals[9] *= some_vals[16];
				some_vals[10] *= some_vals[16];
				some_vals[11] *= some_vals[16];

				some_vals[12] *= some_vals[16];
				some_vals[13] *= some_vals[16];
				some_vals[14] *= some_vals[16];
				some_vals[15] *= some_vals[16];

		
				mag_filter[i*64*16 + j*16 + 0] = some_vals[0];
				mag_filter[i*64*16 + j*16 + 1] = some_vals[1];
				mag_filter[i*64*16 + j*16 + 2] = some_vals[2];
				mag_filter[i*64*16 + j*16 + 3] = some_vals[3];
		
				mag_filter[i*64*16 + j*16 + 4] = some_vals[4];
				mag_filter[i*64*16 + j*16 + 5] = some_vals[5];
				mag_filter[i*64*16 + j*16 + 6] = some_vals[6];
				mag_filter[i*64*16 + j*16 + 7] = some_vals[7];
			
				mag_filter[i*64*16 + j*16 + 8] = some_vals[8];
				mag_filter[i*64*16 + j*16 + 9] = some_vals[9];
				mag_filter[i*64*16 + j*16 + 10] = some_vals[10];
				mag_filter[i*64*16 + j*16 + 11] = some_vals[11];

				mag_filter[i*64*16 + j*16 + 12] = some_vals[12];
				mag_filter[i*64*16 + j*16 + 13] = some_vals[13];
				mag_filter[i*64*16 + j*16 + 14] = some_vals[14];
				mag_filter[i*64*16 + j*16 + 15] = some_vals[15];
	
			}
		}



		if (!_pfIndex) {
			if (HDC hdc = GetDC(reinterpret_cast<HWND>(hwnd))) {
				System::MemoryFill(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR), 0);

				pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
				pixelFormatDesc.nVersion = 1;
	
				pixelFormatDesc.cColorBits = 24;
				pixelFormatDesc.cAlphaBits = 8;


				pixelFormatDesc.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_GENERIC_ACCELERATED | PFD_DOUBLEBUFFER;
				pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;				

				if (_pfIndex = ChoosePixelFormat(hdc, &pixelFormatDesc)) {

					DescribePixelFormat(hdc, _pfIndex, sizeof(PIXELFORMATDESCRIPTOR), &pixelFormatDesc);

					if (SetPixelFormat(hdc, _pfIndex, &pixelFormatDesc)) {
						if (HGLRC hrc = wglCreateContext(hdc)) {
							if (wglMakeCurrent(hdc, hrc)) {
								Initialize_WGL(hdc, hrc);

								Initialize_41();
								Initialize_15();
																
								wglMakeCurrent(hdc, 0);
								
							} else {
								result = GetLastError();
							}
							wglDeleteContext(hrc);
						} else {
							result = GetLastError();
						}
					} else {
						result = GetLastError();
					}
				} else {
					result = GetLastError();
				}

				ReleaseDC(reinterpret_cast<HWND>(hwnd), hdc);
			} else {
				result = GetLastError();
			}
		}
	} else {
		result = ERROR_INVALID_HANDLE;
	}

	if (!result) {
		result = AttributeArray::Initialize();
	}

	return result;

}

int __fastcall OpenGL::Core::Finalize() {
	return AttributeArray::Finalize();
	
}

unsigned int OpenGL::Core::LoadIcon(unsigned int w, unsigned int h, unsigned int r_id, HMODULE lib_id, unsigned int ml_tid, unsigned int ml_lid) {
	unsigned int result(0), x_val(0), x_count(w*h), x_idx(0);
		
	unsigned int c_buf[4096];

	x_count >>= 3;

	if (x_count <= 512)
	if (HRSRC tres = FindResource(lib_id, (LPCWSTR)r_id, (LPCWSTR)33333)) {
		if (unsigned int * src = reinterpret_cast<unsigned int *>(LockResource(LoadResource(lib_id, tres)))) {
			int rsize = SizeofResource(lib_id, tres);
															
			for (unsigned int i(0);i<x_count;i++) {
				x_val = src[i+16];

				c_buf[x_idx++] = src[(x_val>>4) & 0x0F];
				c_buf[x_idx++] = src[x_val & 0x0F];
				x_val >>= 8;
				c_buf[x_idx++] = src[(x_val>>4) & 0x0F];
				c_buf[x_idx++] = src[x_val & 0x0F];
				x_val >>= 8;
				c_buf[x_idx++] = src[(x_val>>4) & 0x0F];
				c_buf[x_idx++] = src[x_val & 0x0F];
				x_val >>= 8;
				c_buf[x_idx++] = src[(x_val>>4) & 0x0F];
				c_buf[x_idx++] = src[x_val & 0x0F];
			}								
			
			if (ml_tid) {
				if (ml_tid == system_icos) ml_tid = -2;

				switch (ml_tid) {
					case -1:
						if (ml_lid < Core::max_texture_layers) {
							glGenTextures(1, &result);
							if (result) {
								glBindTexture(GL_TEXTURE_2D_ARRAY, result);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
								glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

								TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, db_rect[2], db_rect[3], ml_lid, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0); // max_texture_layers
							
								TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, c_buf);
							}
						}
					break;
					case -2:
						for (unsigned int i(0);i<sys_ico_runner;i++) {
							if ((sys_ico_list[i].libid == lib_id) && (sys_ico_list[i].rid == r_id)) {
								result = sys_ico_list[i].lid;
								break;
							}
						}

						if (result) break;

						sys_ico_list[sys_ico_runner].libid = lib_id;
						sys_ico_list[sys_ico_runner].rid = r_id;
						sys_ico_list[sys_ico_runner].lid = sys_ico_runner;


						ml_tid = system_icos;
						ml_lid = sys_ico_runner++;
											

					default:
						result = ml_lid;

						glBindTexture(GL_TEXTURE_2D_ARRAY, ml_tid);
						TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, ml_lid, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, c_buf);
						
					
				}
			} else {
				glGenTextures(1, &result);
				if (result) {
					glBindTexture(GL_TEXTURE_2D, result);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, c_buf);
				}
			}
		}
	}

	return result;
}

UI_64 OpenGL::Core::CollectIcons() {
	unsigned int blank_vals[64*64];
	UI_64 result(-1);	
	glGenTextures(1, &system_icos);

	if (system_icos) {		

		System::MemoryFill(blank_vals, db_rect[2]*db_rect[3]*sizeof(unsigned int), 0xFF402020);

		glBindTexture(GL_TEXTURE_2D_ARRAY, system_icos);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, db_rect[2], db_rect[3], max_texture_layers, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, sys_ico_runner++, db_rect[2], db_rect[3], 1, GL_RGBA, GL_UNSIGNED_BYTE, blank_vals);


		if (HMODULE ico_module = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {

			blist_tid = LoadIcon(32, 32, 17105, ico_module, -1, 8);
			if (blist_tid) {
				LoadIcon(32, 32, 17106, ico_module, blist_tid, ++blist_lid);
				LoadIcon(32, 32, 17100, ico_module, blist_tid, ++blist_lid);
				LoadIcon(32, 32, 17101, ico_module, blist_tid, ++blist_lid);
			}

			warning_icon = LoadIcon(db_rect[2], db_rect[3], 17110, ico_module, -2);
			pause_icon = LoadIcon(db_rect[2], db_rect[3], 17102, ico_module, -2);
			play_icon = LoadIcon(db_rect[2], db_rect[3], 17103, ico_module, -2);
			size_icon = LoadIcon(db_rect[2], db_rect[3], 17104, ico_module, -2);
			shot_icon = LoadIcon(db_rect[2], db_rect[3], 17107, ico_module, -2);
			delete_icon = LoadIcon(db_rect[2], db_rect[3], 17108, ico_module, -2);
			rec_icon = LoadIcon(db_rect[2], db_rect[3], 17109, ico_module, -2);
			dec_icon = LoadIcon(db_rect[2], db_rect[3], 17111, ico_module, -2);
			inc_icon = LoadIcon(db_rect[2], db_rect[3], 17112, ico_module, -2);
			vol_icon = LoadIcon(db_rect[2], db_rect[3], 17113, ico_module, -2);

			up_icon = LoadIcon(db_rect[2], db_rect[3], 17114, ico_module, -2);
			down_icon = LoadIcon(db_rect[2], db_rect[3], 17115, ico_module, -2);

			right_icon = LoadIcon(db_rect[2], db_rect[3], 17116, ico_module, -2);
			left_icon = LoadIcon(db_rect[2], db_rect[3], 17117, ico_module, -2);

			close_icon = LoadIcon(db_rect[2], db_rect[3], 17118, ico_module, -2);

			zoom_icon = LoadIcon(db_rect[2], db_rect[3], 17119, ico_module, -2);

			result = 0;

		}
	}

	return result;
}

void OpenGL::Core::Resize() {
	RECT wrect;
	AttributeArray scatt;
	int ww(0), hh(0);
	float wh[4];


	if (_current_selection != -1) {		
		_selection.LeaveON();

	}

	_current_selection = -1;


	GetWindowRect(_hwnd, &wrect);

	ww = wrect.right - wrect.left;
	hh = wrect.bottom - wrect.top;

	wh[0] = -1.0f;
	wh[1] = -1.0f;
	wh[2] = 0.0f;
	wh[3] = 1.0f;
	
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(_root.Acquire())) {
		if ((ww != elhdr->root_cfg._width) || (hh != elhdr->root_cfg._height)) {
			wh[1] += 2.0f*hh/float(elhdr->root_cfg._fullH);
			if (_scene.InitAttributeArray(AttributeArray::Vertex, scatt) != -1) {
				scatt.SetOffset(wh);					
			}

			_root.InitAttributeArray(AttributeArray::Vertex, scatt);
			scatt.UpdateStretchVector(2.0f*(ww - elhdr->root_cfg._width)/float(elhdr->root_cfg._fullW), 2.0f*(hh - elhdr->root_cfg._height)/float(elhdr->root_cfg._fullH), 0.0);
		
			elhdr->_flags |= (UI_64)Element::FlagAnchor;	
						
			_ss_cfg.full_width = elhdr->root_cfg._width = ww;
			_ss_cfg.full_height = elhdr->root_cfg._height = hh;
			
			ww = -1;
		}
		_root.Release();
	}

	if (ww < 0) {
		Apps::NotificationArea::Resize(hh);

		ResetElement(_root);
	}
}

void OpenGL::Core::GetRootBox(float * b_vals) {
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(_root.Acquire())) {
		b_vals[0] = el_hdr->root_cfg._width - 3;
		b_vals[1] = el_hdr->root_cfg._height - 3;

		_root.Release();
	}
}

void OpenGL::Core::SetInfoPic(SFSco::Object * i_obj) {
	if (i_obj) {
		if (info_pic == 0) glGenTextures(1, &info_pic);

		glBindTexture(GL_TEXTURE_2D, info_pic);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		if (unsigned int * pi_ptr = reinterpret_cast<unsigned int *>(i_obj->Acquire())) {
			pinfo_box[0] = pi_ptr[2];
			pinfo_box[1] = pi_ptr[3];
			pinfo_box[2] = pi_ptr[0];
			pinfo_box[3] = pi_ptr[1];

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pi_ptr[2], pi_ptr[3], 0, GL_BGRA, GL_UNSIGNED_BYTE, pi_ptr+4);

			i_obj->Release();
		}
	}
}

UI_64 OpenGL::Core::LoadStrings() {
	UI_64 result(0);

	SFSco::Object temp_obj;

	HMODULE libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME));

	if (!strings_collection)
	if (Pack::Deflate * deflator = new Pack::Deflate()) {
		if (void * rc_font_ptr = LockResource(LoadResource(libin, FindResource(libin, MAKEINTRESOURCE(13020), MAKEINTRESOURCE(1024))))) {				
			deflator->Reset();

			deflator->SetUSource(reinterpret_cast<unsigned char *>(rc_font_ptr), SizeofResource(libin, FindResource(libin, MAKEINTRESOURCE(13020), MAKEINTRESOURCE(1024))));
			result = deflator->UnPack(temp_obj);

			if ((result) && (deflator->Success())) {
				if (strings_collection.New(ogl_strings_tid, result) != -1) {
					if (void * str_ptr = strings_collection.Acquire()) {
						if (void * ff_ptr = temp_obj.Acquire()) {
							System::MemoryCopy_SSE3(str_ptr, ff_ptr, result);

							temp_obj.Release();
						}
						strings_collection.Release();
					}
				} else {

				}
			}
		}

		delete deflator;
	}

	return result;
}

void OpenGL::Core::GetInfoString(unsigned int s_id, wchar_t * s_1, wchar_t * s_2) {
	unsigned int o_val;
	if (wchar_t * col_ptr = reinterpret_cast<wchar_t *>(strings_collection.Acquire())) {
		o_val = (reinterpret_cast<unsigned int *>(col_ptr)[s_id*2] >> 1);		
		if (s_1) {
			s_1[0] = reinterpret_cast<unsigned int *>(col_ptr)[s_id*2 + 1];

			for (wchar_t i(0);i<=s_1[0];i++) s_1[i+1] = col_ptr[o_val + i];
		}

		o_val = (reinterpret_cast<unsigned int *>(col_ptr)[++s_id*2] >> 1);
		if (s_2) {
			s_2[0] = reinterpret_cast<unsigned int *>(col_ptr)[s_id*2 + 1];

			for (wchar_t i(0);i<=s_2[0];i++) s_2[i+1] = col_ptr[o_val + i];
		}

		strings_collection.Release();
	}
}


void OpenGL::Core::GetHeaderString(unsigned int s_id, wchar_t * s_1) {
	unsigned int o_val;
	
	s_id += 1024;
	if (wchar_t * col_ptr = reinterpret_cast<wchar_t *>(strings_collection.Acquire())) {
		o_val = (reinterpret_cast<unsigned int *>(col_ptr)[s_id*2] >> 1);		

		s_1[0] = reinterpret_cast<unsigned int *>(col_ptr)[s_id*2 + 1];

		for (wchar_t i(0);i<=s_1[0];i++) s_1[i+1] = col_ptr[o_val + i];
		
		strings_collection.Release();
	}
}



UI_64 OpenGL::Core::Activate(void * hwnd, SFSco::IOpenGL::MenuCorner mcorn) {
	wchar_t pool_name[] = {12, L'e',L'l', L'e', L'm', L'e', L'n', L't', L' ', L'p', L'o', L'o', L'l'};
	wchar_t q_name[] = {16, L'o',L'g', L'l', L' ', L'u', L'p', L'd', L'a', L't', L'e', L' ', L'q', L'u', L'e', L'u', L'e'};

	UI_64 result(0);
	AttributeArray elattr;
	Element::RootHeader roo_hdr;

	float color_val[4] = {0.0, 0.0, 0.0, 1.0f}, trans_vec[4] = {0.0, 0.0, 0.0, 0.0};

	float ax(0.0), ay(0.0);
	unsigned int bwi(0), bhi(0);
		

	if (_element_pool) return -1;





	pinfo_box[0] = pinfo_box[1] = pinfo_box[2] = pinfo_box[3] = 16;

	System::MemoryFill_SSE3(loc_buf, 256*4*sizeof(unsigned int), -1, -1);

	if (_element_pool = new Memory::Allocator(1)) {
		result = _element_pool->Init(new Memory::Buffer(), 0x00100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;

	if (!result) {
		result = element_ulist.Init(-1, MIN_UPDATE_LIST_CAPACITY, q_name, _element_pool, update_list_tid);

	}

	if (!result) {
		result = blink_list.New(blink_list_tid, MIN_UPDATE_LIST_CAPACITY*sizeof(BlinkSet), _element_pool);

		if (result != -1) {
			blink_top = MIN_UPDATE_LIST_CAPACITY;
			result = 0;
		}

		if (!result) {
			result = fback_list.New(fback_list_tid, MIN_FBACK_LIST_CAPACITY*sizeof(FBackSet));

			if (result != -1) {
				fback_top = MIN_FBACK_LIST_CAPACITY;
				result = 0;
			}
		}

		if (!result) {
			result = motion_list.New(motion_list_tid, MIN_UPDATE_LIST_CAPACITY*sizeof(Element));

			if (result != -1) {
				motion_top = MIN_UPDATE_LIST_CAPACITY;
				result = 0;
			}
		}


		if (!result) {
			result = autohide_list.New(ahide_list_tid, MIN_UPDATE_LIST_CAPACITY*sizeof(Element));

			if (result != -1) {
				autohide_top = MIN_UPDATE_LIST_CAPACITY;
				result = 0;
			}
		}

	}


	if ((!result) && (pfl_count))  {
		if (hwnd) {				
			_hwnd = reinterpret_cast<HWND>(hwnd);

			if (_dc = GetDC(_hwnd)) {				
				System::MemoryFill(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR), 0);
				pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
				int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3, WGL_CONTEXT_MINOR_VERSION_ARB, 2, WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0, 0};
				
				for (unsigned int ii(0);ii<pfl_count;ii++) {
					DescribePixelFormat(_dc, pf_list[ii], sizeof(PIXELFORMATDESCRIPTOR), &pixelFormatDesc);
					if (SetPixelFormat(_dc, pf_list[ii], &pixelFormatDesc)) {
						if (_rc = WGL::CreateContextAttribs(_dc, 0, attribs)) {
							if (WGL::MakeContextCurrent(_dc, _dc, _rc)) {
							
								_pfIndex = pf_list[ii];
								_compatibility = 0;
								break;

							} else {
								wglDeleteContext(_rc);
								_rc = 0;
							}
						}
					} else {
						result = GetLastError();
					}
				}
			

				if (_compatibility == 0) {
					
					glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, (int *)&max_supported_tunits);
					glGetIntegerv(GL_MAX_TEXTURE_SIZE, max_tex_vals + 2);
					max_tex_vals[0] = max_tex_vals[2] - 1;
					max_tex_vals[1] = System::BitIndexHigh_32(max_tex_vals[2]);
					max_tex_vals[3] = 0;

					glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, (int *)&max_tbuffer_size);
					if (max_tbuffer_size < 0x00200000) {
										// report not enough resources
					}

					glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, (int *)&max_texture_layers);
					if (max_texture_layers < 256) {
						result = -35;
					} else {
						max_texture_layers = 256;

						glGetIntegerv(GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS, (int *)&max_geometry_out);
						max_geometry_out /= 6*4;

						if (max_geometry_out < 32) {
							result = -34;
						} else {
							max_geometry_out = 32;
										
											// do shaders adjustment

							glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, (int *)&max_color_bufs);
							if (max_color_bufs<4) {
								result = -33;
							} else {



								glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, (int *)&uniform_alignment);
												
								glClearDepth(1.0);
								glDepthFunc(GL_LESS);

								glDisable(GL_DEPTH_TEST);
								glDisable(GL_CULL_FACE);
								glDisable(GL_STENCIL_TEST);
								glDisable(GL_SCISSOR_TEST);
								glDisable(GL_SCISSOR_TEST);
								glDisable(GL_BLEND);
												


								GenVertexArrays(1, &_vao);
								BindVertexArray(_vao);

								GenBuffers(1, &_frame_rect);
								BindBuffer(GL_ARRAY_BUFFER, _frame_rect);
								BufferData(GL_ARRAY_BUFFER, 32*sizeof(float), default_rect, GL_STATIC_DRAW);

																		
								ProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
									
								EnableVertexAttribArray(0);
									
								glReadBuffer(GL_BACK);

								ClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);

								glPixelStorei(GL_PACK_ALIGNMENT, 4);
								glPixelStorei(GL_PACK_ROW_LENGTH, 0);
								glPixelStorei(GL_PACK_SKIP_ROWS, 0);
								glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
									
								result = glGetError();
							}										
						}
					}				
				} else {
					result = -36;
				}
			} else {
				result = GetLastError();
			}
		} else {
			result = ERROR_INVALID_HANDLE;
		}
	
	}


	
	if (!result) result = att_domain.Init();
	
	if (!result) {
		RECT prect;
		prect.bottom = prect.left = prect.right = prect.top = 0;

		MonitorCounter();

		SystemParametersInfo(SPI_GETWORKAREA, 0, &prect, 0);
												
		roo_hdr._maxW = prect.right - prect.left;
		roo_hdr._maxH = prect.bottom - prect.top;
				
		bwi = roo_hdr._fullW = max_width;
		bhi = roo_hdr._fullH = max_height;
				

		GetWindowRect(_hwnd, &prect);
		_ss_cfg.full_width = roo_hdr._width = prect.right - prect.left;
		_ss_cfg.full_height = roo_hdr._height = prect.bottom - prect.top;

		_ss_cfg.cmp_count = 4;

				
		ax = -1.0f + 2.0f*roo_hdr._width/float(roo_hdr._fullW);
		ay = -1.0f + 2.0f*roo_hdr._height/float(roo_hdr._fullH);

		glViewport(0, 0, roo_hdr._fullW, roo_hdr._fullH);

		special_x_size = (roo_hdr._fullH + 640) & (~511);
		
		result = CollectIcons();
		
		result |= primPass.Initialize(roo_hdr._fullW, roo_hdr._fullH);				


//		result |= videoPass.Initialize();
		result |= textPass.Initialize();
		result |= cursor.Initialize();

		result |= BuildDefaultPrograms();

		textPass.SetScreen(roo_hdr._fullW, roo_hdr._fullH);

		BindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		glReadBuffer(default_bufs[1]);
				
	}


	if (!result) {
		result = screen_buf.New(Control::Nucleus::type_id, bwi*bhi*4 + 64 + sizeof(Control::Nucleus::Header), SFSco::large_mem_mgr);
		result |= pic_size_buf.New(0, bwi*bhi*8, SFSco::xl_mem_mgr);

		if (result != -1) {
			screen_buf.Set(&screen_buf, sizeof(Control::Nucleus::Header));
			screen_buf.ResetState();

			result = 0;
		} else {
			result = ERROR_OUTOFMEMORY;
		}
	}



	if (!result) {
		_builder_thread = CreateThread(0, 0, BuilderProc, this, 0, 0);
		
		if (_builder_thread) {

			if (CreateElement(Element::ClipRegion, _root) != -1) {
				_scene = _root;
				_scene.Down();								

				result = _root.InitAttributeArray(AttributeArray::Vertex, elattr);

				if (result != -1) {
					elattr[0][0] = -1.0f;
					elattr[0][1] = -1.0f;
					elattr[0][2] = 0.9999999f;
					
					elattr[1][0] = ax;
					elattr[1][1] = -1.0f;
					elattr[1][2] = 0.9999999f;

					elattr[2][0] = -1.0f;
					elattr[2][1] = ay;
					elattr[2][2] = 0.9999999f;

					elattr[3][0] = ax;
					elattr[3][1] = ay;
					elattr[3][2] = 0.9999999f;

					result = 0;
				}

				elattr.SetStretchMask(-1, -1 ,-1);
				elattr.CalculateBox(LeftTopFront);
				
				result = _root.InitAttributeArray(AttributeArray::Color, elattr);

				result = _scene.InitAttributeArray(AttributeArray::Vertex, elattr);
				if (result != -1) {
					float x_vec[4];
					x_vec[0] = x_vec[1] = x_vec[2] = 0.0f; x_vec[3] = 1.0f;
					if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(_root.Acquire())) {
						elhdr->config.anchor = LeftTopFront;

						elhdr->root_cfg = roo_hdr;

						x_vec[0] = -1.0f; x_vec[1] = ay;
						elattr.SetOffset(x_vec);

						x_vec[0] = 2.0f/elhdr->root_cfg._fullW; x_vec[1] = -2.0f/elhdr->root_cfg._fullH; x_vec[2] = 1.0f;
						elattr.SetScale(x_vec);
							

						result = 0;

						_root.Release();
					}
					elattr.SetStretchMask(-1, -1, -1);
				}											
			}





			if (result == 0) {
				_menu_corner = mcorn;
				
				trans_vec[0] = trans_vec[1] = trans_vec[2] = trans_vec[3] = 0.0f;

				
				_root.CreateEBorder(1.1f, 0x80000000);

				color_val[0] = 0.6f;
				color_val[1] = 0.6f;
				color_val[2] = 0.6f;
				
				_root.SetEBorderColor(color_val);
			
				SetBgColor(0.15f, 0.15f, 0.15f);


				result = APPlane::CreateFunctionArea(this, db_rect[2], system_icos);
			}


		} else {
			result = GetLastError();
		}
	}

	return result;

}

void OpenGL::Core::MonitorCounter() {
	DISPLAY_DEVICE dide;
	DEVMODE dmo;

	unsigned int wm(0), wh(0);

	dide.cb = sizeof(DISPLAY_DEVICE);
	dmo.dmSize = sizeof(DEVMODE);

	for (DWORD dnum(0);EnumDisplayDevices(0, dnum, &dide, 0);dnum++) {
		if (dide.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP)
		if (EnumDisplaySettings(dide.DeviceName, ENUM_CURRENT_SETTINGS, &dmo)) {
			if (dmo.dmPelsWidth > max_width) max_width = dmo.dmPelsWidth;
			if (dmo.dmPelsHeight > max_height) max_height = dmo.dmPelsHeight;
		}
	}
}

void OpenGL::Core::Destroy() {
	delete this;			
}

unsigned int OpenGL::Core::Version() {
	return _version;
}

/*
void OpenGL::Core::GetFunctionCoords(AttributeArray & farr) {
	function_area.InitAttributeArray(AttributeArray::Vertex, farr);
}
*/

void OpenGL::Core::SetBgColor(float r, float g, float b) {
	bg_color[0] = r;
	bg_color[1] = g;
	bg_color[2] = b;
	
	_root.SetColor(bg_color);
}

const float * OpenGL::Core::GetBgColor() {
	return bg_color;
}

const unsigned int * OpenGL::Core::GetStringsPtr() {
	return reinterpret_cast<unsigned int *>(strings_collection.Acquire());
}

void OpenGL::Core::ReleaseStringsPtr() {
	strings_collection.Release();
}

void OpenGL::Core::DestroyElement(Element & el_obj) {
	el_obj.Destroy();

	InterlockedOr64(&f_lags, RootRebuild);
}


UI_64 OpenGL::Core::CreateElement(Element::DrawType dtype, Element & newel, unsigned int vertex_count, const Element * parent, unsigned int x_size) {
	UI_64 result(-1), el_tid(newel.GetTID());
	float p_lock(0.0f);

	Element bdre;
	AttributeArray tempa;
	Calc::MatrixF tempm;
	SFSco::Object tempo;
	UI_64 relocate_event(-1);
	unsigned int clip_special = dtype & Element::ClipSpecialFlag;

	if (!el_tid) return -1;

	result = -2;
	
	x_size += sizeof(Element::Header);

	(unsigned int &)dtype &= ~(Element::ClipSpecialFlag);

	switch (dtype) {
		case Element::Point:
			if (!vertex_count) vertex_count = 1;
		break;
		case Element::Segment:
			if (vertex_count<2) vertex_count = 2;
		break;
		break;
		case Element::Triangle:
			if (vertex_count<3) vertex_count = 3;			
		break;
		
		case Element::Quad: case Element::ClipRegion:
			if (vertex_count<4) vertex_count = 4;
		break;
		default:
			if (!vertex_count) vertex_count = 4;
	}
	
	if ((vertex_count) && (result != -1)) {
		if (_root) {
			if (parent) newel(*parent);
			else newel(_root);
			
			if (Element::Header * phdr = reinterpret_cast<Element::Header *>(newel.Acquire())) {
				bool clipr = (((phdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) && (clip_special == 0));

				if (dtype == Element::Symbol) {
					if ((phdr->config.att_flags & 0xFFFF0000) != Element::TQuad) vertex_count = 0;

				}

				newel.Release();

				if ((clipr) && (vertex_count)) newel.Down();
			}
			
			if ((x_size & 0x80000000) == 0) {
				if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(newel.Acquire())) {
					if (ehdr->free_slot_list != -1) {
						tempo.Set(SFSco::mem_mgr, ehdr->free_slot_list);
						if (unsigned int * fslots = reinterpret_cast<unsigned int *>(tempo.Acquire())) {
							if (fslots[0]) p_lock = reinterpret_cast<float *>(fslots)[--fslots[0] + 2];

							tempo.Release();
						}
						newel.Release();
					} else {
						p_lock = ehdr->depth;
						ehdr->depth += 2.0f;

						newel.Release();

						newel.InitAttributeArray(AttributeArray::Vertex, tempa);

						tempa.SetZShift(1.0f/ehdr->depth, 2.0f);

						tempa.Blank();
					}					
				}
			} else {
				if (dtype != Element::SceneRegion)
				if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(newel.Acquire())) {
					p_lock = ehdr->depth - 2.0f;

					newel.Release();
				}
			}

			if (vertex_count) result = newel.Child(el_tid, x_size & 0x7FFFFFFF);
		} else {
			result = newel.New(el_tid, x_size & 0x7FFFFFFF, _element_pool);
			dtype = Element::ClipRegion;
		}

		if (result != -1) {
			
			if (Element::Header * hdr = reinterpret_cast<Element::Header *>(newel.Acquire())) {
//				System::MemoryFill(hdr, x_size & 0x7FFFFFFF, 0);

				hdr->gate_idx = -1;

				hdr->_context = this;
				hdr->findex = att_domain.frame_id - 1;
				
				hdr->_flags = (UI_64)Element::FlagVisible;

				switch (dtype) {
					case Element::Border:
						hdr->config.att_flags |= Element::Loop;
					break;
					case Element::SceneRegion:
						hdr->_flags |= (UI_64)Element::FlagMovable;					
					default:
						hdr->config.att_flags |= dtype;
				}

				hdr->config.unit_id = -1;

				hdr->config.anchor = None | AsIs;

				hdr->vertex_count = vertex_count;

				hdr->depth = 1.0f; hdr->location = p_lock;
				
				for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
					hdr->domain_desc[i].array_id = -1;
				}

				hdr->free_slot_list = -1;

				newel.Release();
			}
			
			newel.InitAttributeArray(AttributeArray::Vertex, tempa);
			
			switch (dtype) {
				case Element::ClipRegion:
				{
					Element scene;

					newel.InitAttributeArray(AttributeArray::Color, tempa);

					if (CreateElement(Element::SceneRegion, scene, vertex_count, &newel, 0x80000000) != -1) {					
						scene.SetAnchorMode(Special);				

					} else {
						result = -1;
					}


				}
				break;
				case Element::SceneRegion:
					tempa.SetStretchMask(-1, -1, -1);

				break;
				
			}

			newel.Push();

		}
	}
	
	return result;
}

void OpenGL::Core::ResetElement(Element & uel, unsigned int r_type) {
	UI_64 xval(0), large_id(0), large_index(-1);

	if (f_lags & RootRebuild) return;

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(uel.Acquire())) {
		if (r_type) elhdr->_flags |= (UI_64)Element::FlagTUpdate;
		if (elhdr->vertex_count > max_geometry_out) {
			large_id = elhdr->config.unit_id;
			large_index = elhdr->config._index;
		}

		xval = (elhdr->_flags & (UI_64)Element::FlagBuilt);

		uel.Release();
	}

	if (uel == _root) {
		InterlockedOr64(&f_lags, RootRebuild);
		return;
	} else {
		if (xval == 0) return;
	}


	if (large_index != -1) {
		att_domain.ResetLarge(large_id, large_index);
	}
	
	element_ulist.Push(&uel);

}


int OpenGL::Core::BuildDefaultPrograms() {
	int result(0), s_size(0);
	unsigned int o_val(0);

	if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 3*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 3*2 + 1];
		main_frame.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16380, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 4*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 4*2 + 1];
		main_frame.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16381, libin);

		strings_collection.Release();
	}

	if (result = main_frame.program.Build()) {
		main_frame.local_cfg.location = GetUniformLocation(main_frame.program.Name(), "local_config");
		blur_cfg.location = GetUniformLocation(main_frame.program.Name(), "blur_cfg");
		main_frame.ms_texture.location = GetUniformLocation(main_frame.program.Name(), "ms_texture");
		main_frame.ms_texture.value.i_val = ms_frame_unit;

		result = 0;
	} else {
		return 111111;
	}


	return result;
}

/*
	top level - texture_units
		unsigned int - texture unit selector

		configuration_units are direct children of texture units
			unsigned int - draw type
			unsigned int - configuration
		
			UI_64 - buffer runner
			UI_64 - buffers top


			buffers - direct children of configuration_units

*/






UI_64 OpenGL::Core::RenderLarge() {
	UI_64 result(0);
	UI_64 nlink(-1), llink(-1), klink(-1);
	int ctranon(1);

	UI_64 tlink = att_domain.GetTop();


	// update some map buffer if ness
	Element::LargeMapRefresh();


	if (tlink != -1) {
		nlink = tlink;

		for (;;) {
			if (unsigned int * drw_t = att_domain.GetBuf(nlink)) {
				if (drw_t[0] != Element::Symbol) {
					att_domain.cfg_offs_location = att_domain.cfg_offs_location = -1;

					switch (drw_t[0]) {
						case Element::Point:

						break;
						case Element::Line: case Element::Loop: case Element::Segment:
							primPass.large_line.Activate();							
							att_domain.cfg_flags_location = primPass.large_line.cfg_flags_location;
							att_domain.trans_location = primPass.large_line.trans_location;
							att_domain.flat_location = primPass.large_line.flat_location;
						break;
/*
						default:

							primPass.large_triangle.Activate();
							att_domain.cfg_offs_location = primPass.large_triangle.cfg_offs_location;
							att_domain.cfg_flags_location = primPass.large_triangle.cfg_flags_location;

							if (int ct_location = GetUniformLocation(primPass.large_triangle.program.Name(), "color_transform")) {
								if (ct_location != -1) {
									UniformMatrix4fv(ct_location, 16, 0, reinterpret_cast<const float *>(color_matrix));

								}
							}
*/
					}
					

					llink = klink = att_domain.DownBuf(nlink);

					for (;;) {
						if (unsigned int * txel = att_domain.GetBuf(klink)) {
							if (txel[0]) {
								ctranon = 1;
								if ((txel[0]>>28) == 9) {
									primPass.SetElTexture();
									ctranon ^= Pics::Image::OGLoad(txel[0]);
									
								} else {
									if (txel[1] & LAYERED_TEXTURE) primPass.SetMlTexture(txel[0]);
									else {
										primPass.SetElTexture(txel[0]);
									}
								}
								
								primPass.SetTolor(ctranon);
							}
						
							att_domain.RenderLarge(att_domain.DownBuf(klink), _element_pool);

							klink = att_domain.LeftBuf(klink);
							if (klink == llink) break;
						}
					}

				} else {


				}
			}

			nlink = att_domain.LeftBuf(nlink);
			if (nlink == tlink) break;
		}
	}

		
	return result;
}


UI_64 OpenGL::Core::RenderText() {
	UI_64 result(0);	
	UI_64 nlink(-1), xlink(-1);

	UI_64 tlink = att_domain.GetTop();

	if (tlink != -1) {
		nlink = tlink;
						

		for (;;) {

			if (unsigned int * drw_t = att_domain.GetBuf(nlink)) {
				if (drw_t[0] == Element::Symbol) {
					xlink = nlink;
					break;
				}
				nlink = att_domain.LeftBuf(nlink);
				if (nlink == tlink) break;

			}
		}

		if (xlink != -1) {
			att_domain.cfg_offs_location = textPass.t_renderer.cfg_offs_location;
			att_domain.cfg_flags_location = textPass.t_renderer.cfg_flags_location;

			nlink = tlink = att_domain.DownBuf(xlink);

			EnableVertexAttribArray(1);

			for (;;) {
				if (unsigned int * fsel = att_domain.GetBuf(nlink)) {
					Font::OGLoad(fsel[0]);
					textPass.BindBT(fsel[1]);

					att_domain.RenderSmall(att_domain.DownBuf(nlink));

				}

				nlink = att_domain.LeftBuf(nlink);
				if (nlink == tlink) break;

			}			

			DisableVertexAttribArray(1);
		}		

	}
		
	return result;
}

UI_64 OpenGL::Core::RenderSmall() {
	UI_64 result(0);
	UI_64 nlink(-1), llink(-1), klink(-1);
	int ctranon(1);

	UI_64 tlink = att_domain.GetTop();

	if (tlink != -1) {
		nlink = tlink;
						
		EnableVertexAttribArray(1);



		for (;;) {
			if (unsigned int * drw_t = att_domain.GetBuf(nlink)) {
				if ((drw_t[0] != Element::Point) && (drw_t[0] != Element::Symbol)) {
					att_domain.cfg_offs_location = att_domain.cfg_offs_location = -1;

					switch (drw_t[0]) {
						case Element::Line: case Element::Loop: case Element::Segment:
							primPass.small_line.Activate();
							att_domain.cfg_offs_location = primPass.small_line.cfg_offs_location;
							att_domain.cfg_flags_location = primPass.small_line.cfg_flags_location;

						break;
						default:
							primPass.small_triangle.Activate();
							att_domain.cfg_offs_location = primPass.small_triangle.cfg_offs_location;
							att_domain.cfg_flags_location = primPass.small_triangle.cfg_flags_location;

					}
					

					llink = klink = att_domain.DownBuf(nlink);

					for (;;) {
						if (unsigned int * txel = att_domain.GetBuf(klink)) {
							if (txel[0]) {
								ctranon = 1;
								if ((txel[0]>>28) == 9) {
									primPass.SetElTexture();
									ctranon ^= Pics::Image::OGLoad(txel[0]);
									
								} else {
									if (txel[1] & LAYERED_TEXTURE) primPass.SetMlTexture(txel[0]);
									else {
										primPass.SetElTexture(txel[0]);
									}
								}
								
								primPass.SetTolor(ctranon);
							}
						
							att_domain.RenderSmall(att_domain.DownBuf(klink));

							klink = att_domain.LeftBuf(klink);
							if (klink == llink) break;
						}
					}

				} else {
					switch (drw_t[0]) {
						case Element::Symbol:

						break;
						case Element::Point:

						break;

					}
				}
			}

			nlink = att_domain.LeftBuf(nlink);
			if (nlink == tlink) break;
		}


		DisableVertexAttribArray(1);

	}

		
	return result;
}

int OpenGL::Core::GetXYShifts(int & a, int & b) {	
	a = _globalX - _lockedX;
	b = _lockedY - _globalY;
	
	_lockedX += a;
	_lockedY -= b;

	return ((_globalX - _lockedX) | (_lockedY - _globalY));
}

UI_64 OpenGL::Core::SelectionTest(int * x_ptr) {
	UI_64 result(-1);
	int ptr16(0);
	Element result_el;

	if (_globalX != 0x7FFFFFFF) {
		ptr16 = (_globalX & (loc_buf_scale-1));
		ptr16 += ((((_ss_cfg.full_height - _globalY - 1) & (loc_buf_scale-1)))*loc_buf_scale);

		if ((ptr16 >= 0) && (ptr16 < (loc_buf_scale*loc_buf_scale))) {		
			result = loc_buf[ptr16] & 0x7FFFFFFF;
		}
	}

	if (result != _current_selection) {
		result_el.Set(_element_pool, result);
		
		result_el.HooverON(_selection);

		_current_selection = result;
		_selection.Set(_element_pool, _current_selection);
												
	} else {
		if ((_current_selection != -1) && (x_ptr)) x_ptr[0] = 1;
	}

	return result;
}

UI_64 OpenGL::Core::PoMove(int a, int b) {
	UI_64 result(1);
	int dx(0), dy(0);

	if ((_globalX == a) && (_globalY == b)) return 0;

	if ((a != 0x7FFFFFFF) && (b != 0x7FFFFFFF)) {
		cursor_count = render_count;
		_cursor_id &= 0x7FFFFFFF;

		_globalPX = _globalX; _globalPY = _globalY;
	
		_globalX = a; _globalY = b;
				
		dx = _globalX - _globalPX;
		dy = _globalY - _globalPY;

		if (dx < 0) dx = -dx;
		if (dy < 0) dy = -dy;

		if ((dx > 1) || (dy > 1)) _cs_locked |= SFSco::IOpenGL::NoButton;

		if (_current_selection != -1) if (_selection.IsMovable()) result = 0;

		_status |= CURSOR_FLAG;
	} else {
		if (_current_selection != -1) {
			_selection.LeaveON();			
		}

		_current_selection = -1;
		_selection.Blank();

		_globalX = 0x7FFFFFFF;

		_cursor_id |= 0x80000000;

		_cs_locked |= SFSco::IOpenGL::NoButton;
	}
	
	

	return result;
}

UI_64 OpenGL::Core::Scroll(int dist) {
	UI_64 result(0);
	Element srlel;

	if (_focus) {
		srlel = _focus;

		for (;;) {
			if (Element::Header * m_hdr = reinterpret_cast<Element::Header *>(srlel.Acquire())) {
				if (m_hdr->_flags & (UI_64)Element::FlagScroll) result = 1;
				if ((m_hdr->config.att_flags & 0xFFFF0000) == Element::SceneRegion) result |= 2;

				srlel.Release();
			} else {
				break;
			}

			if (result) break;
			else srlel.Up();
				
		}
		
		if (result & 1) {
			srlel.Drag(0, dist);
		}
	}

	return result;
}

UI_64 OpenGL::Core::MoCursor(unsigned int cid) {
	_cursor_id = cid;
	return 0;
}

UI_64 OpenGL::Core::PoLock(SFSco::IOpenGL::InputKeys key) {	
	UI_64 result(1);	

	if (_current_selection != -1) {		

		if (_selection != _root) {
			_drag = _selection;

			for (;;) {
				if (Element::Header * m_hdr = reinterpret_cast<Element::Header *>(_drag.Acquire())) {
					if (m_hdr->_flags & (UI_64)(Element::FlagMovable | Element::FlagTouch | Element::FlagTouch2)) result = 0;

					_drag.Release();
				} else break;

				if (result) _drag.Up();
				else break;
			}
		
			if (_drag == _scene) {
				_drag.Blank();
				result = 1;
			}
		}
		
		

		_selection.DownON();
		SetFocus(_selection);
	}

	_lockedX = _globalX;
	_lockedY = _globalY;

	_cs_locked = key;

	return result;
}

void OpenGL::Core::SetFocus(Element & new_fo) {
	unsigned int clip_t(0);

	if (new_fo.GetID() != _current_focus) {
		if (_current_focus != -1) {
			_focus.ClearEFlags((UI_64)(Element::FlagFocus | Element::FlagMouseDown | Element::FlagMouseUp, (UI_64)Element::FlagLostFocus));
			_focus.SetState((UI_64)Element::FlagLostFocus);
		}		

		_focus = new_fo;

		if (Element::Header * fhdr = reinterpret_cast<Element::Header * >(_focus.Acquire())) {
			if ((fhdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) clip_t = 1;
			_focus.Release();
		}

		if (clip_t) _focus.Down().Down();
		else {
			// until action, movable, touch, or first no scene

		}

		_focus.ClearEFlags((UI_64)(Element::FlagLostFocus | Element::FlagMouseUp, (UI_64)Element::FlagFocus));
		_focus.SetState((UI_64)Element::FlagFocus);

		_current_focus = _focus.GetID();
	}
}

UI_64 OpenGL::Core::PoUnLock(SFSco::IOpenGL::InputKeys key) {
	UI_64 result = 1;
	
	if (_current_selection != -1) {
		if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(_selection.Acquire())) {
			result = s_hdr->_flags & (UI_64)(Element::FlagMouseDown | Element::FlagHoover);
			s_hdr->_flags &= ~(UI_64)(Element::FlagMouseDown);

			_selection.Release();
		}

		if ((result == (UI_64)(Element::FlagMouseDown | Element::FlagHoover))  && ((_cs_locked & SFSco::IOpenGL::NoButton) == 0)) _selection.ActionON();
	}
	
	_cs_locked = 0;
	
	return result;
}

void OpenGL::Core::Update() {
	if (_root) InterlockedOr64(&f_lags, RootRebuild);
}

unsigned int OpenGL::Core::IsOn() {
	return (_terminate & 0x80000000);
}

UI_64 OpenGL::Core::MotionStep() {
	UI_64 result(0);

	if (Element * mo_el = reinterpret_cast<Element *>(motion_list.Acquire())) {
		for (unsigned int i(0);i<motion_runner;i++) {
			if (mo_el[i]) {
				mo_el[i].NextLocation();
				ResetElement(mo_el[i], 1);
			}
		}

		motion_list.Release();
	}

	return result;
}

UI_64 OpenGL::Core::StartMoving(Element & m_el) {
	UI_64 result(-1);

	m_el.SetEFlags(Element::FlagMotionOn);

	if (Element * mo_el = reinterpret_cast<Element *>(motion_list.Acquire())) {
		for (unsigned int i(0);i<motion_runner;i++) {
			if (!mo_el) {
				result = i;

				break;
			}
		}

		motion_list.Release();
	}

	if (result == -1) result = motion_runner++;

	if (motion_runner >= motion_top) {
		if (motion_list.Expand(MIN_UPDATE_LIST_CAPACITY*sizeof(Element)) != -1) {
			motion_top += MIN_UPDATE_LIST_CAPACITY;
		}	
	}

	if (motion_runner < motion_top) {
		if (Element * mo_el = reinterpret_cast<Element *>(motion_list.Acquire())) {
			mo_el[result] = m_el;

			motion_list.Release();
		}
	}

	return result;
}

UI_64 OpenGL::Core::StopMoving(Element & m_el, unsigned int reset_mo) {
	UI_64 result(0);

	if (Element * mo_el = reinterpret_cast<Element *>(motion_list.Acquire())) {
		for (unsigned int i(0);i<motion_runner;i++) {
			if (mo_el[i]) {
				result++;

				if (mo_el[i] == m_el) {
					if (reset_mo) mo_el[i].MotionReset(reset_mo);
					mo_el[i].Blank();

					result--;					
				}	
			}
		}
		
		motion_list.Release();

		if (result == 0) motion_runner = 0;
	}


	return result;
}

UI_64 OpenGL::Core::SetFBack(Element & f_el, UI_64 (* update_proc)(Element &)) {
	UI_64 result(-1);

	if (FBackSet * fb_ptr = reinterpret_cast<FBackSet *>(fback_list.Acquire())) {
		for (unsigned int i(0);i<fback_runner;i++) {
			if (fb_ptr[i].f_el == f_el) {
				fb_ptr[i].update_proc = update_proc;
				result = 0;

				break;
			}
		}

		fback_list.Release();
	}

	if (!result) return 0;


	if (FBackSet * fb_ptr = reinterpret_cast<FBackSet *>(fback_list.Acquire())) {
		for (unsigned int i(0);i<fback_runner;i++) {
			if (!fb_ptr[i].update_proc) {
				result = i;

				break;
			}
		}

		fback_list.Release();
	}

	if (result == -1) result = fback_runner++;

	if (fback_runner >= fback_top) {
		if (fback_list.Expand(MIN_FBACK_LIST_CAPACITY*sizeof(FBackSet)) != -1) {
			fback_top += MIN_FBACK_LIST_CAPACITY;
		}	
	}

	if (fback_runner < fback_top) {
		if (FBackSet * fb_ptr = reinterpret_cast<FBackSet *>(fback_list.Acquire())) {
			fb_ptr[result].f_el = f_el;
			fb_ptr[result].update_proc = update_proc;

			fback_list.Release();
		}
	}

	return result;
}

UI_64 OpenGL::Core::ClearFBack(Element & f_el) {
	UI_64 result(0);

	if (FBackSet* fb_ptr = reinterpret_cast<FBackSet *>(fback_list.Acquire())) {
		for (unsigned int i(0);i<fback_runner;i++) {
			if (fb_ptr[i].update_proc) {
				result++;

				if (fb_ptr[i].f_el == f_el) {
					fb_ptr[i].f_el.Blank();
					fb_ptr[i].update_proc = 0;

					result--;					
				}	
			}
		}
		
		fback_list.Release();

		if (result == 0) fback_runner = 0;
	}

	

	return result;
}

UI_64 OpenGL::Core::FBackReset() {		
	if (FBackSet * fb_ptr = reinterpret_cast<FBackSet *>(fback_list.Acquire())) {
		for (unsigned int i(0);i<fback_runner;i++) {
			if (fb_ptr[i].update_proc) {
				fb_ptr[i].update_proc(fb_ptr[i].f_el);

			}
		}

		fback_list.Release();
	}

	return 0;
}




UI_64 OpenGL::Core::SetBlink(Element & el_obj, unsigned int bcount) {
	UI_64 result(0);

	if (blink_runner >= blink_top) {
		if (blink_list.Expand(MIN_UPDATE_LIST_CAPACITY*sizeof(BlinkSet)) != -1) {
			blink_top += MIN_UPDATE_LIST_CAPACITY;
		}
	}

	if (blink_runner < blink_top) {
		if (BlinkSet * bs_arr = reinterpret_cast<BlinkSet *>(blink_list.Acquire())) {
			result = -1;
			for (unsigned int i(0);i<blink_runner;i++) {
				if (bs_arr[i]._count == 0) {
					result = i;
					break;
				}
			}

			if (result == -1) result = blink_runner++;

			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
				if (elhdr->_flags & Element::FlagVisible) {
					elhdr->_flags &= (~(UI_64)Element::FlagBlinkNoMore);

					bs_arr[result].belem = el_obj;

					bs_arr[result].delay[0] = elhdr->root_cfg._width;
					bs_arr[result].delay[1] = elhdr->root_cfg._height;

					ColorConvert(bs_arr[result].color[0], elhdr->root_cfg._maxW);
					ColorConvert(bs_arr[result].color[1], elhdr->root_cfg._maxH);

					bs_arr[result].xcolor[0] = elhdr->root_cfg._fullW;
					bs_arr[result].xcolor[1] = elhdr->root_cfg._fullH;


					bs_arr[result]._count = bcount;
				} else {
					bs_arr[result]._count = 0;
				}
				el_obj.Release();
			}

			blink_list.Release();
		}
	}
	return result;
}



UI_64 OpenGL::Core::SetBlink(BlinkSet & bset) {
	UI_64 result(0);

	if (bset.delay[0] == 0) {
		if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(bset.belem.Acquire())) {

			ehdr->_flags &= (~(UI_64)Element::FlagBlinkNoMore);

			bset.delay[0] = ehdr->root_cfg._width;
			bset.delay[1] = ehdr->root_cfg._height;

			ColorConvert(bset.color[0], ehdr->root_cfg._maxW);
			ColorConvert(bset.color[1], ehdr->root_cfg._maxH);

			bset.xcolor[0] = ehdr->root_cfg._fullW;
			bset.xcolor[1] = ehdr->root_cfg._fullH;
						

			bset.belem.Release();
		}
	}

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(bset.belem.Acquire())) {
		if (ehdr->_flags & (UI_64)Element::FlagVisible)
		if (ehdr->config.att_flags & (1 << AttributeArray::Color)) {

			bset._elapsed = render_count + bset.delay[0];
			bset._phase = 0;
			bset._count++;

			result = 1;
		}

		bset.belem.Release();
	}


	if (result) {
		if (blink_runner >= blink_top) {
			if (blink_list.Expand(MIN_UPDATE_LIST_CAPACITY*sizeof(BlinkSet)) != -1) {
				blink_top += MIN_UPDATE_LIST_CAPACITY;
			}
		}

		if (blink_runner < blink_top) {
			if (BlinkSet * bs_arr = reinterpret_cast<BlinkSet *>(blink_list.Acquire())) {
				result = -1;
				for (unsigned int i(0);i<blink_runner;i++) {
					if (bs_arr[i]._count == 0) {
						result = i;
						break;
					}
				}

				if (result == -1) result = blink_runner++;

				bs_arr[result] = bset;

				blink_list.Release();
			}
		}
	}

	return result;
}

UI_64 OpenGL::Core::SetAutoHide(Element & el_obj, unsigned int tout) {
	UI_64 result(0);

	if (autohide_runner >= autohide_top) {
		if (autohide_list.Expand(MIN_UPDATE_LIST_CAPACITY*sizeof(Element)) != -1) {
			autohide_top += MIN_UPDATE_LIST_CAPACITY;
		}
	}

	if (autohide_runner < autohide_top) {
		el_obj.SetTimeOut(tout);

		if (Element * els = reinterpret_cast<Element *>(autohide_list.Acquire())) {
			els[autohide_runner++] = el_obj;

			autohide_list.Release();
		}

	}

	return result;
}

UI_64 OpenGL::Core::FBackUpdate() {
	UI_64 result(0);



	return result;
}

UI_64 OpenGL::Core::ElementHide() {
	UI_64 result(0);

	if (Element * h_el = reinterpret_cast<Element *>(autohide_list.Acquire())) {

		for (unsigned int i(0);i<autohide_runner;i++) {
			result = 0;

			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(h_el[i].Acquire())) {
				if (el_hdr->_flags & (UI_64)Element::FlagHideOn)
				if (render_count > (unsigned int)el_hdr->root_cfg._fullW) {
					el_hdr->_flags &= ~(UI_64)Element::FlagHideOn;
					result = 1;
				}

				h_el[i].Release();
			}

			if (result) {
				h_el[i].Hide();
				ResetElement(h_el[i]);
			}
		}

		autohide_list.Release();
	}

	return result;
}

UI_64 OpenGL::Core::ElementBlink() {
	UI_64 result(1);

	if (BlinkSet * bs_arr = reinterpret_cast<BlinkSet *>(blink_list.Acquire())) {
		for (unsigned int i(0);i<blink_runner;i++) {
			if (bs_arr[i].belem.TestEFlags(Element::FlagBlinkNoMore) == 0) {
				bs_arr[i]._count = 1;
				bs_arr[i]._phase = 1;

				bs_arr[i]._elapsed = render_count - 1;
			}


			if (bs_arr[i]._count) {
				result = 0;

				if (render_count > bs_arr[i]._elapsed) {
					if (bs_arr[i]._phase) {
						if (--bs_arr[i]._count <= 0) {
							bs_arr[i].belem.ClearDualUp();

						}
					}

					bs_arr[i]._phase ^= 1;
					bs_arr[i]._elapsed = render_count + bs_arr[i].delay[bs_arr[i]._phase];

					att_domain.Blink(bs_arr[i]);
				}				
			}
		}
		blink_list.Release();
	}


	return result;
}



void OpenGL::Core::ConfigureBlur() {
	if (blur_cfg.value[0].ui_cmp != blur_level) {

		blur_cfg.value[1].ui_cmp = blur_cfg.value[0].ui_cmp = blur_level;
		
		blur_cfg.value[2].ui_cmp = (blur_level + 1);
		blur_cfg.value[1].ui_cmp *= blur_cfg.value[1].ui_cmp; // (max + 1)^2
		blur_cfg.value[1].ui_cmp *= 2;
		blur_cfg.value[1].ui_cmp++;

		blur_cfg.value[3].ui_cmp = (2*blur_level + 1);
		blur_cfg.value[2].ui_cmp *= blur_cfg.value[3].ui_cmp;

		blur_cfg.value[3].ui_cmp *= blur_cfg.value[3].ui_cmp;
		blur_cfg.value[3].ui_cmp *= blur_cfg.value[1].ui_cmp;


		blur_cfg.value[2].ui_cmp *= blur_level;
		blur_cfg.value[2].ui_cmp /= 6;
		blur_cfg.value[2].ui_cmp *= (8*blur_level + 4);

		blur_cfg.value[3].ui_cmp -= blur_cfg.value[2].ui_cmp;

		blur_cfg.value[2].float_cmp = 1.0f/blur_cfg.value[3].ui_cmp;

		if (blur_cfg.location != -1) Uniform4iv(blur_cfg.location, 1, &blur_cfg.value[0].i_cmp);
	}
}

UI_64 OpenGL::Core::Render() {
	UI_64 result(0);
	unsigned int * pibu_po(0);
	unsigned int tapif(_status);
	int h_still(0);

	if ((tapif & ONLINE_FLAG) == 0) return 0;

	if (render_count & 1) {
		if ((tapif & TEST_FLAG) || (_x16 != (_globalX & (~(loc_buf_scale-1)))) || (_y16 != ((_ss_cfg.full_height - _globalY - 1) & (~(loc_buf_scale-1))))) {
			_x16 = (_globalX & (~(loc_buf_scale-1)));
			_y16 = ((_ss_cfg.full_height - _globalY - 1) & (~(loc_buf_scale-1)));

			glReadPixels(_x16, _y16, loc_buf_scale, loc_buf_scale, GL_RGBA, GL_UNSIGNED_BYTE, loc_buf);
		}		

		if (tapif & (TEST_FLAG | CURSOR_FLAG)) {
			SelectionTest();

			if (tapif & CURSOR_FLAG) {
				result = SelectionTest(&h_still);

				if (h_still) {
					
					switch (_cs_locked & SFSco::IOpenGL::LRButtons) {
						case SFSco::IOpenGL::LeftButton:
							if (_drag) {
								_drag.Drag(_globalX - _lockedX, _globalY - _lockedY);
								_lockedX = _globalX;
								_lockedY = _globalY;
							}
						break;
						case SFSco::IOpenGL::RightButton:

						break;
					}
				}
			}
		}

		tapif &= (~TEST_FLAG);
	}

// ================================================================================================================


	if (tapif & REFRESH_FLAG) {
		if (tapif & REWRITE_FLAG) {

			result = att_domain.Rewrite(_element_pool);
			_terminate |= 0x80000000;

		} else if (tapif & TRANSFORM_FLAG) {

			result = att_domain.Transform();

			if (tapif & UPDATE_FLAG) {
				
				result |= att_domain.Update(_element_pool);
			}
		}

		_inertion_count = 1 + 1; // local_config[0]
	
	}
	
	_status = (ONLINE_FLAG | (tapif & TEST_FLAG));
	render_count++;
	
	if (Media::Source::TexUpdate(this) | Pics::Image::TexUpdate(this)) _inertion_count = 1 + 1; // PrimitivePass::detail_l

	if (APPlane::FunctionStretch(_inertion_count, 1 + 1) & ElementBlink()) { // PrimitivePass::detail_l

		if (_inertion_count) _inertion_count--;

		if (_inertion_count == (tapif & CURSOR_FLAG)) {
			return 0;
		}

	}
		
	
	
	
// ================================================================================================================================
		
	primPass.Activate();

	if (tapif & STENCIL_FLAG) {
		primPass.Clear(_frame_rect, 1);		
		att_domain.StencilSet();

	} else {
		primPass.Clear(_frame_rect, 0);
		glFlush();
	}

if (glGetError()) {
	result = 4;
}

	RenderSmall();
glFlush();
if (glGetError()) {
	result = 1;
}

	RenderLarge();
glFlush();	
if (glGetError()) {
	result = 2;
}

	textPass.Activate();
	RenderText();
glFlush();				
if (glGetError()) {
	result = 3;
}
	


// =================================================================================================================================

	BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	DrawBuffers(3, default_bufs); // 

	glDisable(GL_DEPTH_TEST);

	BindBuffer(GL_ARRAY_BUFFER, _frame_rect);
	VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	main_frame.local_cfg.value[0].i_cmp = 1;//  primPass.Destination();
		
		
	main_frame.local_cfg.value[3].i_cmp = 1; // PrimitivePass::detail_l;
	main_frame.Activate();
	ConfigureBlur();

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);



	if (tapif  & GET_SCREEN_FLAG) {			
		screen_buf.SetState(1);

		if (screen_buf.Acquire(reinterpret_cast<void **>(&pibu_po))) {
			glReadBuffer(default_bufs[0]);

			glReadPixels(_ss_cfg.offset_x, _ss_cfg.offset_y, _ss_cfg.screen_width, _ss_cfg.screen_height, GL_RGBA, GL_UNSIGNED_BYTE, pibu_po);

			glReadBuffer(default_bufs[1]);

			screen_buf.Release(pibu_po);				

			_ss_cfg.offset_x = _ss_cfg.offset_y = -1;

			Pics::Image::Encode(screen_buf, _ss_cfg);
		}
			
	}

	if (_cursor_id < 16) {
		cursor.Render(_cursor_id, 1, _ss_cfg.full_height); // primPass.Destination()
	}

	if ((render_count - cursor_count) > 512) {
		_cursor_id |= 0x80000000;
		tapif = 0;
	}

	glFlush();

	InterlockedOr((long *)&_status, RENDERED_FLAG);

	if (glGetError()) {
		for (;glGetError(););
		
		glFinish();

		Update();
	}
	
	if (tapif & (REWRITE_FLAG | TRANSFORM_FLAG)) _status |= TEST_FLAG;
	
	return result;

}

void OpenGL::Core::Refresh() {
	InterlockedOr((long *)&_status, REFRESH_FLAG);
}

unsigned int OpenGL::Core::IsRendered() {
	return (_status & RENDERED_FLAG);
}


void OpenGL::Core::ScreenShot(const SFSco::IOpenGL::ScreenSet & sdata) {
	if (screen_buf.Test(1)) {		
		if ((sdata.screen_width == 0) || (sdata.screen_width > _ss_cfg.full_width)) _ss_cfg.screen_width = _ss_cfg.full_width;
		if ((sdata.screen_height == 0) || (sdata.screen_height > _ss_cfg.full_height)) _ss_cfg.screen_height = _ss_cfg.full_height;

		if (sdata.offset_x > _ss_cfg.full_width) _ss_cfg.offset_x = 0;
		if (sdata.offset_y > _ss_cfg.full_height) _ss_cfg.offset_y = 0;

		if ((_ss_cfg.offset_x + _ss_cfg.screen_width) > _ss_cfg.full_width) _ss_cfg.offset_x = _ss_cfg.full_width - _ss_cfg.screen_width;
		if ((_ss_cfg.offset_y + _ss_cfg.screen_height) > _ss_cfg.full_height) _ss_cfg.offset_y = _ss_cfg.full_height - _ss_cfg.screen_height;
	
		_ss_cfg.i_type = Pics::Image::shot_set.i_type;
		_ss_cfg.q_factor = Pics::Image::shot_set.q_factor;
		_ss_cfg.i_flags = 0;
	
		InterlockedOr((long *)&_status, REFRESH_FLAG | GET_SCREEN_FLAG);
	}
}

unsigned int OpenGL::Core::BuilderLoop() {
	unsigned int m_counter(0), build_count(0);

	I_64 pool_index[2*MAX_POOL_COUNT];

	SFSco::Object ulject;
	
	Memory::Chain::RegisterThread(pool_index);

	if (HANDLE timev = CreateEvent(0, 0, 0, 0)) {
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);

		for (;_terminate;) {
			WaitForSingleObject(timev, 19);

			if (_status & UPLOAD_FLAG) continue;

			ElementHide();

			if (++build_count & 1) Apps::NotificationArea::Refresh(build_count >> 1);


			if (fback_runner) {		
				if ((build_count & 7) == 0) {
					FBackReset();
				}
			}
	
			if (motion_runner) {
				if ((m_counter++ & 3) == 0) {
					// change local transform for motion elements, do a transform reseet for those eleemnts
					MotionStep();
				}
			}
			

			if (f_lags & RootRebuild) {				
				att_domain.frame_id++;
	
				att_domain.incoming_id = _root.GetID();

				att_domain.Reass(_element_pool);

				att_domain.ResetBufferUnits();
										
				att_domain.Constructor(_element_pool);

				_status = REWRITE_FLAG | ONLINE_FLAG | REFRESH_FLAG | TRANSFORM_FLAG | UPLOAD_FLAG | STENCIL_FLAG;

				element_ulist.Drop();

				InterlockedXor64(&f_lags, RootRebuild);

			} else {
				if (UI_64 ii = element_ulist.Pop(&ulject)) {
					att_domain.frame_id++;

					for (;;ii = element_ulist.Pop(&ulject)) {
						att_domain.incoming_id = ulject.GetID();

						att_domain.Reass(_element_pool);
						
						if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(ulject.Acquire())) {
							att_domain.ResetMatrixStack(_element_pool);
							if (ehdr->_flags & (UI_64)Element::FlagTUpdate) { // buffer trasform
								_status |= att_domain.Transformer(_element_pool);
								ehdr->_flags ^= (UI_64)Element::FlagTUpdate;
							} else { // buffer rewrite									
								_status |= att_domain.Updator(_element_pool); // UPDATE_FLAG
							}
							ulject.Release();
						}

						if (ii == 2) break;
					}

					InterlockedOr((long *)&_status, ONLINE_FLAG | REFRESH_FLAG | TRANSFORM_FLAG | UPLOAD_FLAG);
				}
			}

	// memory manager fuckup
			for (unsigned int i(0);i<MAX_POOL_COUNT;i++) {
				if (pool_index[2*i]) {
					build_count += 3;
				}

			}

		}
		
		CloseHandle(timev);
	}

	Memory::Chain::UnregisterThread();

	return build_count;
}

DWORD OpenGL::Core::BuilderProc(void * cnts) {
	return reinterpret_cast<OpenGL::Core *>(cnts)->BuilderLoop();
}

SFSco::IOpenGL::MenuCorner OpenGL::Core::GetMenuCorner() {
	return _menu_corner;
}

unsigned int OpenGL::Core::GetSystemTID() {
	return system_icos;
}

unsigned int OpenGL::Core::GetWarningTID() {
	return warning_icon;
}

unsigned int OpenGL::Core::GetPauseTID() {
	return pause_icon;
}

unsigned int OpenGL::Core::GetPlayTID() {
	return play_icon;
}

unsigned int OpenGL::Core::GetSizeTID() {
	return size_icon;
}

unsigned int OpenGL::Core::GetShotTID() {
	return shot_icon;
}

unsigned int OpenGL::Core::GetBListTID() {
	return blist_tid;
}

unsigned int OpenGL::Core::GetInfoTID() {
	return info_pic;
}

unsigned int OpenGL::Core::GetRecTID() {
	return rec_icon;
}

unsigned int OpenGL::Core::GetDeleteTID() {
	return delete_icon;
}

unsigned int OpenGL::Core::GetCloseTID() {
	return close_icon;
}

unsigned int OpenGL::Core::GetZoomTID() {
	return zoom_icon;
}

unsigned int OpenGL::Core::GetDecTID() {
	return dec_icon;
}

unsigned int OpenGL::Core::GetIncTID() {
	return inc_icon;
}

unsigned int OpenGL::Core::GetVolTID() {
	return vol_icon;
}

unsigned int OpenGL::Core::GetLeftTID() {
	return left_icon;
}

unsigned int OpenGL::Core::GetRightTID() {
	return right_icon;
}

unsigned int OpenGL::Core::GetUpTID() {
	return up_icon;
}

unsigned int OpenGL::Core::GetDownTID() {
	return down_icon;
}



const float * OpenGL::Core::GetInfoBox() {
	return pinfo_box;
}

// =============================================================================================

OpenGL::Core::BlinkSet & OpenGL::Core::BlinkSet::operator =(const OpenGL::Core::BlinkSet & ref) {
	System::MemoryCopy(this, &ref, sizeof(BlinkSet));
	return *this;
}



OpenGL::Core::Belt::Belt() : cfg_offs_location(-1), cfg_flags_location(-1), ct_location(-1), trans_location(-1), flat_location(-1) {

}

int OpenGL::Core::Belt::Activate() {
	UseProgram(program.Name());

	if (local_cfg.location != -1) Uniform4iv(local_cfg.location, 1, &local_cfg.value[0].i_cmp);
	if (ms_texture.location != -1) Uniform1i(ms_texture.location, ms_texture.value.i_val);
	if (el_sampler.location != -1) Uniform1i(el_sampler.location, el_sampler.value.i_val);
	if (msel_sampler.location != -1) Uniform1i(msel_sampler.location, msel_sampler.value.i_val);
	if (attbutes.location != -1) Uniform1i(attbutes.location, attbutes.value.i_val);
	if (ct_location != -1) UniformMatrix4fv(ct_location, 16, 0, reinterpret_cast<const float *>(color_matrix));


	return 0;
}

