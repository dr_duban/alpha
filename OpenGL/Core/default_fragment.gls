/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#version 330 core

layout(location = 0) out vec4 f_color;
layout(location = 1) out vec4 id_color;

uniform sampler2DArray ms_texture; // binding=4, 
uniform ivec4 local_config;
uniform ivec4 blur_cfg = ivec4(1, 4, 0x3DE38E39, 9);

uniform vec4 scale_fac[3] = vec4[](vec4(20.0, 80.0, 0.0, 0.0), vec4(15.0, 20.0, 65.0, 0.0), vec4(7.0, 15.0, 28.0, 50.0));

void main() {
	vec4 e_region = texelFetch(ms_texture, ivec3(gl_FragCoord.xy, 0), 0);
	ivec4 l_sel = ivec4(local_config[0], 0, 0, floatBitsToInt(e_region[3]));

	id_color = vec4(l_sel[3] & 0x0FF, (l_sel[3]>>8) & 0x0FF, (l_sel[3]>>16) & 0x0FF, (l_sel[3]>>24) & 0x0FF)/255.0f;

	f_color = vec4(0.0, 0.0, 0.0, 0.0);

	if (l_sel[3] >= 0) {
		f_color = texelFetch(ms_texture, ivec3(gl_FragCoord.xy, 1), 0);

	} else {		

		for (l_sel[0] = -blur_cfg[0];l_sel[0]<=blur_cfg[0];l_sel[0]++) {
			for (l_sel[1] = -blur_cfg[0];l_sel[1]<=blur_cfg[0];l_sel[1]++) {
				e_region[0] = blur_cfg[1] - (l_sel[0]*l_sel[0] + l_sel[1]*l_sel[1]);
				f_color += texelFetch(ms_texture, ivec3(gl_FragCoord.x + l_sel[0], gl_FragCoord.y + l_sel[1], 1), 0)*e_region[0];
			}
		}

		f_color *= intBitsToFloat(blur_cfg[2]);

	}
}

