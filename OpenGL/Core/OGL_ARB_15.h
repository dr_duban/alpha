/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#ifndef OPENGL_FUNC_INI
	#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; extern P##n n
#endif

// #define ARB_FUNC_INI(n,a,t) typedef t (__stdcall *P##n##ARB)a; P##n##ARB n##ARB

namespace OpenGL {
// imaging
OPENGL_FUNC_INI(ColorSubTable,(GLenum target,GLsizei start,GLsizei count,GLenum format,GLenum type,const void * data),void);
OPENGL_FUNC_INI(ColorTable,(GLenum target,GLenum internalFormat,GLsizei width,GLenum format,GLenum type,const void * data),void);
OPENGL_FUNC_INI(ColorTableParameterfv,(GLenum target,GLenum pname,const float * params),void);
OPENGL_FUNC_INI(ColorTableParameteriv,(GLenum target,GLenum pname,const int * params),void);
OPENGL_FUNC_INI(ConvolutionFilter1D,(GLenum target,GLenum internalFormat,GLsizei width,GLenum format,GLenum type,const void * data),void);
OPENGL_FUNC_INI(ConvolutionFilter2D,(GLenum target,GLenum internalFormat,GLsizei width,GLsizei height,GLenum format,GLenum type,const void * data),void);
OPENGL_FUNC_INI(ConvolutionParameterf,(GLenum target,GLenum pname,float param),void);
OPENGL_FUNC_INI(ConvolutionParameterfv,(GLenum target,GLenum pname,const float * params),void);
OPENGL_FUNC_INI(ConvolutionParameteriv,(GLenum target,GLenum pname,const int * params),void);
OPENGL_FUNC_INI(ConvolutionParameteri,(GLenum target,GLenum pname,int params),void);
OPENGL_FUNC_INI(CopyColorSubTable,(GLenum target,GLsizei start,int x,int y,GLsizei width),void);
OPENGL_FUNC_INI(CopyConvolutionFilter1D,(GLenum target,GLenum internalFormat,int x,int y,GLsizei width),void);
OPENGL_FUNC_INI(CopyConvolutionFilter2D,(GLenum target,GLenum internalFormat,int x,int y,GLsizei width,GLsizei height),void);
OPENGL_FUNC_INI(GetColorTable,(GLenum target,GLenum format,GLenum type,void * table),void);
OPENGL_FUNC_INI(GetColorTableParameterfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_FUNC_INI(GetColorTableParameteriv,(GLenum target,GLenum pname,int * params),void);
OPENGL_FUNC_INI(GetConvolutionFilter,(GLenum target,GLenum format,GLenum type,void * image),void);
OPENGL_FUNC_INI(GetConvolutionParameterfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_FUNC_INI(GetConvolutionParameteriv,(GLenum target,GLenum pname,int * params),void);
OPENGL_FUNC_INI(GetHistogram,(GLenum target,GLboolean reset,GLenum format,GLenum type,void * values),void);
OPENGL_FUNC_INI(GetHistogramParameterfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_FUNC_INI(GetHistogramParameteriv,(GLenum target,GLenum pname,int * params),void);
OPENGL_FUNC_INI(GetMinmax,(GLenum target,GLboolean reset,GLenum format,GLenum types,void * values),void);
OPENGL_FUNC_INI(GetMinmaxParameterfv,(GLenum target,GLenum pname,float * params),void);
OPENGL_FUNC_INI(GetMinmaxParameteriv,(GLenum target,GLenum pname,int * params),void);
OPENGL_FUNC_INI(GetSeparableFilter,(GLenum target,GLenum foramt,GLenum type,void * row,void * column,void * span),void);
OPENGL_FUNC_INI(Histogram,(GLenum target,GLsizei width,GLenum internalFormat,GLboolean sink),void);
OPENGL_FUNC_INI(Minmax,(GLenum target,GLenum internalFormat,GLboolean sink),void);
OPENGL_FUNC_INI(ResetHistogram,(GLenum target),void);
OPENGL_FUNC_INI(ResetMinmax,(GLenum target),void);
OPENGL_FUNC_INI(SeparableFilter2D,(GLenum target,GLenum internalFormat,GLsizei width,GLsizei height,GLenum format,GLenum type,const void * row,const void * column),void);


}

namespace ARB {



//vertex buffer object
OPENGL_FUNC_INI(BindBuffer,(GLenum, GLuint),void);
OPENGL_FUNC_INI(BufferData,(GLenum, GLsizeiptr, const void *, GLenum),void);
OPENGL_FUNC_INI(BufferSubData,(GLenum, GLintptr, GLsizeiptr,const void *),void);
OPENGL_FUNC_INI(DeleteBuffers,(GLsizei, const GLuint *),void);
OPENGL_FUNC_INI(GenBuffers,(GLsizei, GLuint *),void);
OPENGL_FUNC_INI(GetBufferParameteriv,(GLenum, GLenum, int *),void);
OPENGL_FUNC_INI(GetBufferPointerv,(GLenum, GLenum, void **),void);
OPENGL_FUNC_INI(GetBufferSubData,(GLenum, GLintptr, GLsizeiptr,void *),void);
OPENGL_FUNC_INI(IsBuffer,(GLuint), GLboolean);
OPENGL_FUNC_INI(MapBuffer,(GLenum, GLenum),void *);
OPENGL_FUNC_INI(UnmapBuffer,(GLenum),GLboolean);

}

// imaging
#define GL_ARB_CONSTANT_COLOR							0x8001
#define GL_ARB_ONE_MINUS_CONSTANT_COLOR					0x8002
#define GL_ARB_CONSTANT_ALPHA							0x8003
#define GL_ARB_ONE_MINUS_CONSTANT_ALPHA					0x8004
#define GL_ARB_BLEND_COLOR								0x8005
#define GL_ARB_FUNC_ADD									0x8006
#define GL_ARB_FUNC_MIN									0x8007
#define GL_ARB_FUNC_MAX									0x8008
#define GL_ARB_BLEND_EQUATION							0x8009
#define GL_GL_FUNC_SUBTRACT								0x800A
#define GL_GL_FUNC_REVERSE_SUBTRACT						0x800B

#define GL_ARB_CONVOLUTION_1D							0x8010
#define GL_ARB_CONVOLUTION_2D							0x8011
#define GL_ARB_SEPARABLE_2D								0x8012
#define GL_ARB_CONVOLUTION_BORDER_MODE					0x8013
#define GL_ARB_CONVOLUTION_FILTER_SCALE					0x8014
#define GL_ARB_CONVOLUTION_FILTER_BIAS					0x8015
#define GL_ARB_REDUCEE									0x8016
#define GL_ARB_CONVOLUTION_FORMAT						0x8017
#define GL_ARB_CONVOLUTION_WIDTH						0x8018
#define GL_ARB_CONVOLUTION_HEIGHT						0x8019
#define GL_ARB_MAX_CONVOLUTION_WIDTH					0x801A
#define GL_ARB_MAX_CONVOLUTION_HEIGHT					0x801B

#define GL_ARB_POST_CONVOLUTION_RED_SCALE				0x801C
#define GL_ARB_POST_CONVOLUTION_GREEN_SCALE				0x801D
#define GL_ARB_POST_CONVOLUTION_BLUE_SCALE				0x801E
#define GL_ARB_POST_CONVOLUTION_ALPHA_SCALE				0x801F
#define GL_ARB_POST_CONVOLUTION_RED_BIAS				0x8020
#define GL_ARB_POST_CONVOLUTION_GREEN_BIAS				0x8021
#define GL_ARB_POST_CONVOLUTION_BLUE_BIAS				0x8022
#define GL_ARB_POST_CONVOLUTION_ALPHA_BIAS				0x8023

#define GL_ARB_HISTOGRAM								0x8024
#define GL_ARB_PROXY_HISTOGRAM							0x8025
#define GL_ARB_HISTOGRAM_WIDTH							0x8026
#define GL_ARB_HISTOGRAM_FORMAT							0x8027
#define GL_ARB_HISTOGRAM_RED_SIZE						0x8028
#define GL_ARB_HISTOGRAM_GREEN_SIZE						0x8029
#define GL_ARB_HISTOGRAM_BLUE_SIZE						0x802A
#define GL_ARB_HISTOGRAM_ALPHA_SIZE						0x802B
#define GL_ARB_HISTOGRAM_LUMINANCE_SIZE					0x802C
#define GL_ARB_HISTOGRAM_SINK							0x802D
#define GL_ARB_MINMAX									0x802E
#define GL_ARB_MINMAX_FORMAT							0x802F
#define GL_ARB_MINMAX_SINK								0x8030
#define GL_ARB_TABLE_TOO_LARGE							0x8031

#define GL_ARB_COLOR_MATRIX								0x80B1
#define GL_ARB_COLOR_MATRIX_STACK_DEPTH					0x80B2
#define GL_ARB_MAC_COLOR_MATRIX_STACK_DEPTH				0x80B3
#define GL_ARB_POST_COLOR_MATRIX_RED_SCALE				0x80B4
#define GL_ARB_POST_COLOR_MATRIX_GREEN_SCALE			0x80B5
#define GL_ARB_POST_C0LOR_MATRIX_BLUE_SCALE				0x80B6
#define GL_ARB_POST_COLOR_MATRIX_ALPHA_SCALE			0x80B7
#define GL_ARB_POST_COLOR_MATRIX_RED_BIAS				0x80B8
#define GL_ARB_POST_COLOR_MATRIX_GREEN_BIAS				0x80B9
#define GL_ARB_POST_C0LOR_MATRIX_BLUE_BIAS				0x80BA
#define GL_ARB_POST_COLOR_MATRIX_ALPHA_BIAS				0x80BB

#define GL_ARB_COLOR_TABLE								0x80D0
#define GL_ARB_POST_CONVOLUTION_COLOR_TABLE				0x80D1
#define GL_ARB_POST_COLOR_MATRIX_COLOR_TABLE			0x80D2
#define GL_ARB_PROXY_COLOR_TABLE						0x80D3
#define GL_ARB_PROXY_POST_CONVOLUYION_COLOR_TABLE		0x80D4
#define GL_ARB_PROXY_POST_COLOR_MATRIX_COLOR_TABLE		0x80D5
#define GL_ARB_COLOR_TABLE_SCALE						0x80D6
#define GL_ARB_COLOR_TABLE_BIAS							0x80D7
#define GL_ARB_COLOR_TABLE_FORMAT						0x80D8
#define GL_ARB_COLOR_TABLE_WIDTH						0x80D9
#define GL_ARB_COLOR_TABLE_RED_SIZE						0x80DA
#define GL_ARB_COLOR_TABLE_GREEN_SIZE					0x80DB
#define GL_ARB_COLOR_TABLE_BLUE_SIZE					0x80DC
#define GL_ARB_COLOR_TABLE_ALPHA_SIZE					0x80DD
#define GL_ARB_COLOR_TABLE_LUMINANCE_SIZE				0x80DE
#define GL_ARB_COLOR_TABLE_INTENSITY_SIZE				0x80DF

#define GL_ARB_IGNORE_BORDER							0x8150
#define GL_ARB_CONSTANT_BORDER							0x8151
#define GL_ARB_WRAP_BORDER								0x8152
#define GL_ARB_REPLACE_BORDER							0x8153
#define GL_ARB_CONVOLUTION_BORDER_COLOR					0x8154

// point sprite
#define GL_ARB_POINT_SPRITE								0x8861
#define GL_ARB_COORD_REPLACE							0x8862


// vertex buffer object
#define GL_ARB_BUFFER_SIZE								0x8764
#define GL_ARB_BUFFER_USAGE								0x8765
#define GL_ARB_ARRAY_BUFFER								0x8892
#define GL_ARB_ELEMENT_ARRAY_BUFFER						0x8893
#define GL_ARB_ARRAY_BUFFER_BINDING						0x8894
#define GL_ARB_ELEMENT_ARRAY_BUFFER_BINDING				0x8895
#define GL_ARB_VERTEX_ARRAY_BUFFER_BINDING				0x8896
#define GL_ARB_NORMAL_ARRAY_BUFFER_BINDING				0x8897
#define GL_ARB_COLOR_ARRAY_BUFFER_BINDING				0x8898
#define GL_ARB_INDEX_ARRAY_BUFFER_BINDING				0x8899
#define GL_ARB_TEXTURE_COORD_ARRAY_BUFFER_BINDING		0x889A
#define GL_ARB_EDGE_FLAG_ARRAY_BUFFER_BINDING			0x889B
#define GL_ARB_SECONDARY_COLOR_ARRAY_BUFFER_BINDING		0x889C
#define GL_ARB_FOG_COORDINATE_ARRAY_BUFFER_BINDING		0x889D
#define GL_ARB_WEIGHT_ARRAY_BUFFER_BINDING				0x889E
#define GL_ARB_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING		0x889F

#define GL_ARB_READ_ONLY								0x88B8
#define GL_ARB_WRITE_ONLY								0x88B9
#define GL_ARB_READ_WRITE								0x88BA
#define GL_ARB_BUFFER_ACCESS							0x88BB
#define GL_ARB_BUFFER_MAPPED							0x88BC
#define GL_ARB_BUFFER_MAP_POINTER						0x88BD
	
#define GL_ARB_STREAM_DRAW								0x88E0
#define GL_ARB_STREAM_READ								0x88E1
#define GL_ARB_STREAM_COPY								0x88E2

#define GL_ARB_STATIC_DRAW								0x88E4
#define GL_ARB_STATIC_READ								0x88E5
#define GL_ARB_STATIC_COPY								0x88E6
	
#define GL_ARB_DYNAMIC_DRAW								0x88E8
#define GL_ARB_DYNAMIC_READ								0x88E9
#define GL_ARB_DYNAMIC_COPY								0x88EA
