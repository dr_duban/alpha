/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/



#include "..\OpenGL.h"

#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; P##n n = 0

#include "OGL_15.h"
#include "OGL_ARB_15.h"


bool OpenGL::Core::arb_imaging = false;


void OpenGL::Core::Initialize_15() {
// OpenGL 1.1
	if (_version < 1500) {
		_version = 1100;

		extensionsString = glGetString(GL_EXTENSIONS);
	}

// OpenGL 1.2
	OPENGL_FUNC_MAP(BlendColor);
	OPENGL_FUNC_MAP(BlendEquation);
	OPENGL_FUNC_MAP(CopyTexSubImage3D);
	OPENGL_FUNC_MAP(DrawRangeElements);
	OPENGL_FUNC_MAP(TexImage3D);
	OPENGL_FUNC_MAP(TexSubImage3D);

	if (_version == 1100)
		if (BlendColor || BlendEquation || CopyTexSubImage3D || DrawRangeElements || TexImage3D || TexSubImage3D) _version = 1200;


// OpenGL 1.3
	OPENGL_FUNC_MAP(ActiveTexture);
	OPENGL_FUNC_MAP(SampleCoverage);
	OPENGL_FUNC_MAP(ClientActiveTexture);
	OPENGL_FUNC_MAP(CompressedTexImage1D);
	OPENGL_FUNC_MAP(CompressedTexImage2D);
	OPENGL_FUNC_MAP(CompressedTexImage3D);
	OPENGL_FUNC_MAP(CompressedTexSubImage1D);
	OPENGL_FUNC_MAP(CompressedTexSubImage2D);
	OPENGL_FUNC_MAP(CompressedTexSubImage3D);

	if (_version == 1500) {
		OPENGL_FUNC_MAP(MultiTexCoord1f);
		OPENGL_FUNC_MAP(MultiTexCoord1fv);
		OPENGL_FUNC_MAP(MultiTexCoord2f);
		OPENGL_FUNC_MAP(MultiTexCoord2fv);
		OPENGL_FUNC_MAP(MultiTexCoord3f);
		OPENGL_FUNC_MAP(MultiTexCoord3fv);
		OPENGL_FUNC_MAP(MultiTexCoord4f);
		OPENGL_FUNC_MAP(MultiTexCoord4fv);

		if (ActiveTexture || ClientActiveTexture || CompressedTexImage1D || CompressedTexImage2D || CompressedTexImage3D ||
			CompressedTexSubImage1D || CompressedTexSubImage2D || CompressedTexSubImage3D || 
			MultiTexCoord1f || MultiTexCoord1fv || MultiTexCoord2f || MultiTexCoord2fv || MultiTexCoord3f || MultiTexCoord3fv ||
			MultiTexCoord4f || MultiTexCoord4fv || SampleCoverage) _version = 1300;

	}

// OpenGL 1.4
	OPENGL_FUNC_MAP(BlendFuncSeparate);

	OPENGL_FUNC_MAP(MultiDrawArrays);
	OPENGL_FUNC_MAP(MultiDrawElements);

	OPENGL_FUNC_MAP(PointParameterf);
	OPENGL_FUNC_MAP(PointParameterfv);

	if (_version == 1300) {
		OPENGL_FUNC_MAP(SecondaryColor3f);
		OPENGL_FUNC_MAP(SecondaryColor3fv);

		OPENGL_FUNC_MAP(SecondaryColorPointer);

		OPENGL_FUNC_MAP(WindowPos2f);
		OPENGL_FUNC_MAP(WindowPos2fv);
		OPENGL_FUNC_MAP(WindowPos2i);
		OPENGL_FUNC_MAP(WindowPos2iv);
		OPENGL_FUNC_MAP(WindowPos3f);
		OPENGL_FUNC_MAP(WindowPos3fv);
		OPENGL_FUNC_MAP(WindowPos3i);
		OPENGL_FUNC_MAP(WindowPos3iv);

		if (BlendFuncSeparate || MultiDrawArrays ||	MultiDrawElements || PointParameterf || PointParameterfv ||
			SecondaryColor3f || SecondaryColor3fv || SecondaryColorPointer ||
			WindowPos2f || WindowPos2fv || WindowPos3f || WindowPos3fv) _version = 1400;

	}

// OpenGL 1.5

	OPENGL_FUNC_MAP(BeginQuery);
	OPENGL_FUNC_MAP(EndQuery);
	OPENGL_FUNC_MAP(GenQueries);
	OPENGL_FUNC_MAP(DeleteQueries);
	OPENGL_FUNC_MAP(GetQueryObjectuiv);
	OPENGL_FUNC_MAP(GetQueryObjectiv);
	OPENGL_FUNC_MAP(GetQueryiv);
	OPENGL_FUNC_MAP(IsQuery);

	OPENGL_FUNC_MAP(BindBuffer);
	OPENGL_FUNC_MAP(BufferData);
	OPENGL_FUNC_MAP(GetBufferParameteriv);
	OPENGL_FUNC_MAP(GetBufferPointerv);
	OPENGL_FUNC_MAP(BufferSubData);
	OPENGL_FUNC_MAP(GetBufferSubData);
	OPENGL_FUNC_MAP(GenBuffers);
	OPENGL_FUNC_MAP(DeleteBuffers);
	OPENGL_FUNC_MAP(IsBuffer);
	OPENGL_FUNC_MAP(MapBuffer);
	OPENGL_FUNC_MAP(UnmapBuffer);

	if (_version == 1400)
		if (BeginQuery || EndQuery || GenQueries || DeleteQueries || GetQueryObjectiv || GetQueryiv || IsQuery || BindBuffer ||
			BufferData || GetBufferParameteriv || GetBufferPointerv || BufferSubData || GetBufferSubData || GenBuffers || DeleteBuffers ||
			IsBuffer || MapBuffer || UnmapBuffer) _version = 1500;
		else CollectARB_15();

	if (_version == 1500) CollectSpecials_15();

}


void OpenGL::Core::CollectARB_15() {	

// vertex buffer object	
	ARB_FUNC_MAP(BindBuffer);
	ARB_FUNC_MAP(DeleteBuffers);
	ARB_FUNC_MAP(GenBuffers);
	ARB_FUNC_MAP(IsBuffer);
	ARB_FUNC_MAP(BufferData);
	ARB_FUNC_MAP(BufferSubData);
	ARB_FUNC_MAP(GetBufferSubData);
	ARB_FUNC_MAP(MapBuffer);
	ARB_FUNC_MAP(UnmapBuffer);
	ARB_FUNC_MAP(GetBufferParameteriv);
	ARB_FUNC_MAP(GetBufferPointerv);

	if (ARB::BindBuffer || ARB::DeleteBuffers || ARB::GenBuffers || ARB::IsBuffer || ARB::BufferData || ARB::BufferSubData || ARB::GetBufferSubData || ARB::MapBuffer || ARB::UnmapBuffer || ARB::GetBufferParameteriv || ARB::GetBufferPointerv) {
		BindBuffer = ARB::BindBuffer;
		BufferData = ARB::BufferData;
		GetBufferParameteriv = ARB::GetBufferParameteriv;
		GetBufferPointerv = ARB::GetBufferPointerv;
		BufferSubData = ARB::BufferSubData;
		GetBufferSubData = ARB::GetBufferSubData;
		GenBuffers = ARB::GenBuffers;
		DeleteBuffers = ARB::DeleteBuffers;
		IsBuffer = ARB::IsBuffer;
		MapBuffer = ARB::MapBuffer;
		UnmapBuffer = ARB::UnmapBuffer;
		
		_version = 1500;
	}
}

void OpenGL::Core::CollectSpecials_15() {
	// imaging
	OPENGL_FUNC_MAP(ColorSubTable);
	OPENGL_FUNC_MAP(ColorTable);
	OPENGL_FUNC_MAP(ColorTableParameterfv);
	OPENGL_FUNC_MAP(ColorTableParameteriv);
	OPENGL_FUNC_MAP(ConvolutionFilter1D);
	OPENGL_FUNC_MAP(ConvolutionFilter2D);
	OPENGL_FUNC_MAP(ConvolutionParameterf);
	OPENGL_FUNC_MAP(ConvolutionParameterfv);
	OPENGL_FUNC_MAP(ConvolutionParameteriv);
	OPENGL_FUNC_MAP(ConvolutionParameteri);
	OPENGL_FUNC_MAP(CopyColorSubTable);
	OPENGL_FUNC_MAP(CopyConvolutionFilter1D);
	OPENGL_FUNC_MAP(CopyConvolutionFilter2D);
	OPENGL_FUNC_MAP(GetColorTable);
	OPENGL_FUNC_MAP(GetColorTableParameterfv);
	OPENGL_FUNC_MAP(GetColorTableParameteriv);
	OPENGL_FUNC_MAP(GetConvolutionFilter);
	OPENGL_FUNC_MAP(GetConvolutionParameterfv);
	OPENGL_FUNC_MAP(GetConvolutionParameteriv);
	OPENGL_FUNC_MAP(GetHistogram);
	OPENGL_FUNC_MAP(GetHistogramParameterfv);
	OPENGL_FUNC_MAP(GetHistogramParameteriv);
	OPENGL_FUNC_MAP(GetMinmax);
	OPENGL_FUNC_MAP(GetMinmaxParameterfv);
	OPENGL_FUNC_MAP(GetMinmaxParameteriv);
	OPENGL_FUNC_MAP(GetSeparableFilter);
	OPENGL_FUNC_MAP(Histogram);
	OPENGL_FUNC_MAP(Minmax);
	OPENGL_FUNC_MAP(ResetHistogram);
	OPENGL_FUNC_MAP(ResetMinmax);
	OPENGL_FUNC_MAP(SeparableFilter2D);

	arb_imaging = (ColorSubTable || ColorTable || ColorTableParameterfv || ColorTableParameteriv || ConvolutionFilter1D || ConvolutionFilter2D || 
			ConvolutionParameterf || ConvolutionParameterfv || ConvolutionParameteriv || ConvolutionParameteri || CopyColorSubTable || CopyConvolutionFilter1D || 
			CopyConvolutionFilter2D || GetColorTable || GetColorTableParameterfv || GetColorTableParameteriv || GetConvolutionFilter || GetConvolutionParameterfv || 
			GetConvolutionParameteriv || GetHistogram || GetHistogramParameterfv || GetHistogramParameteriv || GetMinmax || GetMinmaxParameterfv || GetMinmaxParameteriv || 
			GetSeparableFilter ||	Histogram || Minmax || ResetHistogram || ResetMinmax || SeparableFilter2D);




}

