/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "OpenGL\OpenGL.h"

#include "OGL_15.h"
#include "OGL_43.h"

#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"

#pragma warning(disable:4244)

#define MATRIX_STACK_DEPTH			256

#define CONSTRUCTOR_STACK_DEPTH		256

// ==========================================================================================================================================================================
OpenGL::ButtonGrid bgrid_ref;
OpenGL::Element lmnt_ref;

OpenGL::Core::AttStruct::AttStruct() : bfn_top(-1), large_count(0), large_offset(0), att_buffer(0), current_frame_ucount(0), _buffers_net(0), _stencil_buf(0), _stencil_count(0), frame_id(-1), _ms_runner(0), _ms_top(0), constructor_runner(0), constructor_top(0) {
	buf_texture[0] = buf_texture[1] = 0;
}


OpenGL::Core::AttStruct::~AttStruct() {
	i_buffers.Destroy();

	config_backb.Destroy();

	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) store[i].buffer.Destroy();

	if (_buffers_net) delete _buffers_net;

}

UI_64 OpenGL::Core::AttStruct::GetTop() {
	return bfn_top;
}
/*
void OpenGL::Core::AttStruct::Lock() {
	_buffers_net->Lock();
}

void OpenGL::Core::AttStruct::Unlock() {
	_buffers_net->Unlock();
}
*/
unsigned int * OpenGL::Core::AttStruct::GetBuf(UI_64 id) {
	return reinterpret_cast<unsigned int *>(_buffers_net->GetLinkPtr(id));
}

UI_64 OpenGL::Core::AttStruct::LeftBuf(UI_64 id) {
	return _buffers_net->LeftLink(id);
}

UI_64 OpenGL::Core::AttStruct::DownBuf(UI_64 id) {
	return _buffers_net->DownLink(id);
}

UI_64 OpenGL::Core::AttHide(Element & e_obj) {
	return att_domain.Hide(e_obj);
}

UI_64 OpenGL::Core::AttShow(Element & e_obj) {
	return att_domain.Show(e_obj);
}


int OpenGL::Core::AttStruct::Init() {
	wchar_t pool_name[] = {15, L'o',L'g', L'l', L' ', L'b', L'u', L'f', L'f', L'e', L'r', L's', L' ', L'n', L'e', L't'};
	UI_64 result(0);

	glGenTextures(2, buf_texture);
	GenBuffers(1, &att_buffer);


		
	ActiveTexture(GL_TEXTURE0 + attribute_unit);
	glBindTexture(GL_TEXTURE_BUFFER, buf_texture[0]);

	ActiveTexture(GL_TEXTURE0 + large_unit);
	glBindTexture(GL_TEXTURE_BUFFER, buf_texture[1]);

	result = glGetError();
	
	if (!result) {
		if ((temp_co_ma.Create(0, 0, 4, 4) != -1) && (tran_mat.Create(0, 0, 4, 4) != -1)) {
			
			if (_buffers_net = new Memory::Allocator(1)) {
				result = _buffers_net->Init(new Memory::Buffer(), 0x00100000, pool_name);
			} else result = ERROR_OUTOFMEMORY;

			if (!result) {
				result = config_backb.New(att_config_buf, (2+32*8)*sizeof(int));
				if (int * cbb = reinterpret_cast<int *>(config_backb.Acquire())) {
					cbb[0] = 0;
					cbb[1] = 32;

					config_backb.Release();
				}
			}
		
			if (result != -1)
			for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
				result |= store[i].buffer.New(buf_unit_tid, 1024*4*sizeof(float));
				if (result != -1) {
					store[i].runner = 0;
					store[i].top = 1024;						
					store[i].o_ffset = 0;
				}
			}
		
			if (result != -1) {
				result = i_buffers.New(buf_names_buf, 130*sizeof(unsigned int));
				if (unsigned int * ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
					ibu[0] = 0;
					ibu[1] = 128;
							
					i_buffers.Release();
				}

				if (result != -1) result = 0;
		
			}
		} else {
			result = ERROR_OUTOFMEMORY;
		}
	} else {
		result = ERROR_OUTOFMEMORY;
	}

	if (!result) {
		result = buffer_ulist.New(update_list_tid, (MIN_UPDATE_LIST_CAPACITY+1)*4*sizeof(UI_64));

		if (result != -1) {
			if (UI_64 * bule = reinterpret_cast<UI_64 *>(buffer_ulist.Acquire())) {
				bule[0] = 0;
				bule[1] = MIN_UPDATE_LIST_CAPACITY;

				buffer_ulist.Release();
			}

			result = 0;
		}

	}

	if (!result) {
		result = cfg_ulist.New(update_list_tid, (MIN_UPDATE_LIST_CAPACITY+1)*4*sizeof(UI_64));

		if (result != -1) {
			if (UI_64 * cule = reinterpret_cast<UI_64 *>(cfg_ulist.Acquire())) {
				cule[0] = 0;
				cule[1] = MIN_UPDATE_LIST_CAPACITY;

				cfg_ulist.Release();
			}

			result = 0;
		}

	}


	if (!result) {
		result = matrix_stack.New(matrix_stack_tid, AttributeArray::AttTypeCount*MATRIX_STACK_DEPTH*16*sizeof(float));
		if (result != -1) {
			_ms_top = MATRIX_STACK_DEPTH;

			result = 0;
		}

	}

	if (!result) {
		result = constructor_stack.New(constructor_stack_tid, CONSTRUCTOR_STACK_DEPTH*sizeof(ConstructorState));
		if (result != -1) {
			constructor_top = CONSTRUCTOR_STACK_DEPTH;

			result = 0;
		}

	}

	return result;
}

void OpenGL::Core::AttStruct::PushState(const ConstructorState & ssave) {
	if (constructor_runner < constructor_top)
	if (ConstructorState * stack_ptr = reinterpret_cast<ConstructorState *>(constructor_stack.Acquire())) {		
		stack_ptr[constructor_runner].refli = ssave.refli;

		stack_ptr[constructor_runner].target = ssave.target;
		stack_ptr[constructor_runner].x_flag = ssave.x_flag;

		stack_ptr[constructor_runner].zval = ssave.zval;
		stack_ptr[constructor_runner].depth = ssave.depth;

		stack_ptr[constructor_runner]._visible = ssave._visible;
		stack_ptr[constructor_runner]._reserved = ssave._reserved;

		stack_ptr[constructor_runner]._reserved64 = ssave._reserved64;

		constructor_runner++;

		constructor_stack.Release();
	}

	if (constructor_runner >= constructor_top) {
		if (constructor_stack.Expand(CONSTRUCTOR_STACK_DEPTH*sizeof(ConstructorState)) != -1) {
			constructor_top += CONSTRUCTOR_STACK_DEPTH;
		}
	}
}

void OpenGL::Core::AttStruct::PopState(ConstructorState & srest) {
	if (constructor_runner)
	if (ConstructorState * stack_ptr = reinterpret_cast<ConstructorState *>(constructor_stack.Acquire())) {
		--constructor_runner;

		srest.refli = stack_ptr[constructor_runner].refli;

		srest.target = stack_ptr[constructor_runner].target;
		srest.x_flag = stack_ptr[constructor_runner].x_flag;

		srest.zval = stack_ptr[constructor_runner].zval;
		srest.depth = stack_ptr[constructor_runner].depth;

		srest._visible = stack_ptr[constructor_runner]._visible;
		srest._reserved = stack_ptr[constructor_runner]._reserved;

		srest._reserved64 = stack_ptr[constructor_runner]._reserved64;

		constructor_stack.Release();
	}
}


void OpenGL::Core::AttStruct::Destroy() {
	
	glDeleteTextures(2, buf_texture);

	DeleteBuffers(1, &att_buffer);

	if (unsigned int * ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
		DeleteBuffers(ibu[0], ibu+2);

		i_buffers.Release();				
	}

	temp_co_ma.Destroy();
	tran_mat.Destroy();
}



UI_64 OpenGL::Core::AttStruct::Reass(Memory::Allocator * e_pool) {
	UI_64 result(0);

	Element nelem, incoming_el;	
	ConstructorState _state;

	incoming_el.Set(e_pool, incoming_id);

	if (incoming_el.IsAnchor() == 0) return 0;
	
	_state.refli = incoming_el;
	_state.target = 0;
	_state.x_flag = -1;
	_state.zval = 0;
	_state.depth = 0;
	_state._visible = 1;
	_state._reserved = 0;

	constructor_runner = 0;


	if (incoming_el.ApplyAnchor(frame_id, true)) {
		incoming_el.XPand();

		PushState(_state);
				
		_state.refli.Down();
		
		if (_state.refli != incoming_el) {

			incoming_el.ForwardStretchVector(_state.refli, true);

			for (;_state.refli != incoming_el;) {
				if (_state.target) goto target_1;
				
				nelem = _state.refli;
				nelem.Left();

				if (nelem != _state.refli) {
					_state.refli.ForwardStretchVector(nelem);
				}

				if (_state.refli.ApplyAnchor(frame_id)) {
					reinterpret_cast<Element &>(reinterpret_cast<SFSco::Object &>(_state.refli)).XPand();

					nelem = _state.refli;
					nelem.Down();

					if (nelem != _state.refli) {
						_state.refli.ForwardStretchVector(nelem);
						
						_state.target = 1;
						PushState(_state);

						_state.refli = nelem;
						switch (_state.refli.GetTID()) {
							case ButtonGrid::type_id:
								reinterpret_cast<UI_64 *>(&_state.refli)[0] = reinterpret_cast<UI_64 *>(&bgrid_ref)[0];
							break;
							default:
								reinterpret_cast<UI_64 *>(&_state.refli)[0] = reinterpret_cast<UI_64 *>(&lmnt_ref)[0];
						}

						_state.target = 0;

						continue;	
					}										
				}
target_1:
				_state.refli.ClearStretch();

				nelem = _state.refli;
				nelem.Left();
				if (nelem != _state.refli) {
					if (nelem.IsRefresh(frame_id)) {
						_state.refli = nelem;
						switch (_state.refli.GetTID()) {
							case ButtonGrid::type_id:
								reinterpret_cast<UI_64 *>(&_state.refli)[0] = reinterpret_cast<UI_64 *>(&bgrid_ref)[0];
							break;
							default:
								reinterpret_cast<UI_64 *>(&_state.refli)[0] = reinterpret_cast<UI_64 *>(&lmnt_ref)[0];

						}

						_state.target = 0;

						continue;
					}
				}

				PopState(_state);
			}
		}
	}

	return result;
}


void OpenGL::Core::AttStruct::MatrixStackLoad(Element::Header * ehdr) {
	float * up_level(0);
	unsigned int tsval(ehdr->config.att_flags & 0x0000FFFF);
	AttributeArray v_arr;

	if (float * current_matrix = reinterpret_cast<float *>(matrix_stack.Acquire())) {
		current_matrix += _ms_runner*AttributeArray::AttTypeCount*16;

		for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
			if (tsval & 1) {
				if (_ms_runner) {
						up_level = current_matrix - AttributeArray::AttTypeCount*16;
					} else {
						up_level = 0;
					}
					
				v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[i].array_id);

				v_arr.MatrixUpdate(current_matrix, up_level);
								
			} else {
				current_matrix[0] = 1.0f;
				current_matrix[1] = current_matrix[2] = current_matrix[3] = current_matrix[4] = 0.0;
				current_matrix[5] = 1.0f;
				current_matrix[6] = current_matrix[7] = current_matrix[8] = current_matrix[9] = 0.0;
				current_matrix[10] = 1.0f;
				current_matrix[11] = current_matrix[12] = current_matrix[13] = current_matrix[14] = 0.0;
				current_matrix[15] = 1.0f;
			}

			tsval >>= 1;
			current_matrix += 16;
		}

		matrix_stack.Release();
	}
}

void OpenGL::Core::AttStruct::ResetMatrixStack(Memory::Allocator * e_pool) {
	Element::Header * ehdr = reinterpret_cast<Element::Header *>(e_pool->GetLinkPtr(incoming_id));
	unsigned int tsval(ehdr->config.att_flags & 0x0000FFFF);
	AttributeArray v_arr;

	_ms_runner = 1;

	if (float * current_matrix = reinterpret_cast<float *>(matrix_stack.Acquire())) {
		for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {

			if (tsval & 1) {
				v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[i].array_id);
				v_arr.GetGlobalTransform(current_matrix);
								
			} else {
				current_matrix[0] = 1.0f;
				current_matrix[1] = current_matrix[2] = current_matrix[3] = current_matrix[4] = 0.0;
				current_matrix[5] = 1.0f;
				current_matrix[6] = current_matrix[7] = current_matrix[8] = current_matrix[9] = 0.0;
				current_matrix[10] = 1.0f;
				current_matrix[11] = current_matrix[12] = current_matrix[13] = current_matrix[14] = 0.0;
				current_matrix[15] = 1.0f;
			}

			current_matrix += 16;

			tsval >>= 1;
		}
		matrix_stack.Release();
	}
}



UI_64 OpenGL::Core::AttStruct::Constructor(Memory::Allocator * e_pool) {
	Text::Header text_hdr;
	AttributeArray v_arr;
	Element::Header * ehdr(0);
	UI_64 nlink(-1);
	float * t_ptr(0);
	unsigned int ts_val(0), stencil_val(0);
	SFSco::Object cfg_unit;
	
	ConstructorState _state;

	unsigned int ival(0), jval(0);

	large_count = 0;

	_state.refli._reserved = incoming_id;

	_state.target = 0;
	_state.x_flag = 0;
	_state.zval = 0;
	_state.depth = 0;
	_state._visible = 1;
	_state._reserved = sizeof(ConstructorState);

	constructor_runner = 0;
		
	for (;;) {
		ehdr = reinterpret_cast<Element::Header *>(e_pool->GetLinkPtr(_state.refli._reserved));

		switch (_state.target) {
			case 1: goto target_1;
			case 2: goto target_2;
		}
		
		MatrixStackLoad(ehdr);

		if ((ehdr->config.att_flags & 0xFFFF0000) != Element::SceneRegion) {

			if (ehdr->config.unit_id = SelectCfgUnit(ehdr->config.tex_unit_selector, ehdr->config.att_flags)) {
				 
				ival = ehdr->config.att_flags & 0x0000FFFF;
			
				for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
					ehdr->domain_desc[i].offset = store[i].runner;
					if ((ival & 1) && (ehdr->vertex_count <= max_geometry_out)) ehdr->domain_desc[i].size = ehdr->vertex_count;
					else ehdr->domain_desc[i].size = 0;

					ival >>= 1;
				}

				switch (ehdr->config.att_flags & 0xFFFF0000) { 
					case Element::TQuad:
						reinterpret_cast<Text::Header *>(ehdr + 1)->fcolor_offset = store[AttributeArray::Color].runner;
						text_hdr = reinterpret_cast<Text::Header *>(ehdr + 1)[0];


						if ((store[AttributeArray::Color].top - store[AttributeArray::Color].runner) < 4) {							
							if (store[AttributeArray::Color].buffer.Expand(1024*4*sizeof(float)) != -1) store[AttributeArray::Color].top += 1024;

						}

						temp_co_ma.Set(&store[AttributeArray::Color].buffer, store[AttributeArray::Color].runner<<4);
						store[AttributeArray::Color].runner += 4;
												

						if (temp_co_ma.Acquire(reinterpret_cast<void **>(&t_ptr))) {
							Calc::FloatClear4N(t_ptr, text_hdr.font_color, 4);

							temp_co_ma.Release(t_ptr);
						}
					
						tran_mat.Set(&matrix_stack, (_ms_runner*AttributeArray::AttTypeCount + AttributeArray::Color)<<6);

						if (!tran_mat.IsIdentity()) {
							temp_co_ma.SetColumnCount(4);
							tran_mat >> temp_co_ma;
						}


					break;
				}

				
				nlink = e_pool->DownLink(_state.refli._reserved);
				if (nlink != _state.refli._reserved) {
					if ((ehdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) {
						_state.zval = stencil_val++;
					}

					_state.target = 1;
					PushState(_state);

					if ((ehdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) {
						_state.depth = _state.zval;
					}

					_state.refli._reserved = nlink;

					_state.target = 0;
					if (_state._visible) _state._visible = (ehdr->_flags & (UI_64)Element::FlagVisible);
					ehdr->_flags &= ~(UI_64)Element::FlagVisible2;

					_ms_runner++;

					continue;
						
target_1:

					for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
						ehdr->domain_desc[i].size += store[i].runner - ehdr->domain_desc[i].offset;
					}

					_ms_runner--;
				}
								
			
				ts_val = ehdr->config.att_flags & 0x0000FFFF;
				if (ehdr->vertex_count <= max_geometry_out) {
					for (unsigned int ti(0);ts_val;ti++) {
						if (ts_val & 1) {
							if ((store[ti].top - store[ti].runner) < ehdr->vertex_count) {
								ival = (ehdr->vertex_count<=1024)?1024:ehdr->vertex_count;
								if (store[ti].buffer.Expand(ival*4*sizeof(float)) != -1) store[ti].top += ival;

							}


							v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[ti].array_id);

							if (float * buf_ptr = reinterpret_cast<float *>(store[ti].buffer.Acquire())) {
								if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
									Calc::FloatCopy4(buf_ptr + (store[ti].runner<<2), ahdr + 1, ehdr->vertex_count);
									v_arr.Release();
								}
								store[ti].buffer.Release();
							}



							temp_co_ma.Set(&store[ti].buffer, store[ti].runner<<4);
							store[ti].runner += ehdr->vertex_count;

							tran_mat.Set(&matrix_stack, (_ms_runner*AttributeArray::AttTypeCount+ti)<<6);

							if (!tran_mat.IsIdentity()) {
								temp_co_ma.SetColumnCount(ehdr->vertex_count);
								tran_mat >> temp_co_ma;
							}
						}
						ts_val >>= 1;
					}

					cfg_unit.Set(_buffers_net, ehdr->config.unit_id);
					if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
						if (cfgu->index_top <= cfgu->i_runner) {
							ival = cfgu->index0.Expand(256*4*sizeof(unsigned int));
							ival |= cfgu->index1.Expand(256*4*sizeof(unsigned int));
							if (ival != -1) cfgu->index_top += 256;

						}

						if (cfgu->index_top > cfgu->i_runner) {
						
							ehdr->config._index = cfgu->i_runner++;

							if (unsigned int * ind1 = reinterpret_cast<unsigned int *>(cfgu->index1.Acquire())) {
						
								ival = 0;
								jval = ehdr->config.att_flags & 0x0000FFFF;
								jval >>= 1;
							
								for (unsigned int j(1);j<AttributeArray::AttTypeCount;j++) {
									if (jval & 1) {									
										ind1[4*ehdr->config._index + ival++] = ehdr->domain_desc[j].offset + ehdr->domain_desc[j].size - ehdr->vertex_count;								
									}
									jval >>= 1;

									if ((jval == 0) || (ival >= 4)) break;
								}

								switch (ehdr->config.att_flags & 0xFFFF0000) {
									case Element::Symbol:
										ind1[4*ehdr->config._index] = text_hdr.fcolor_offset;
										if (ehdr->_reserved < max_texture_layers) {
											ind1[4*ehdr->config._index + 3] = ehdr->_reserved | text_hdr.level_selector;
										} else {
											ind1[4*ehdr->config._index + 3] = 0;
										}
																		
									break;

								}

								cfgu->index1.Release();
							}
				
							if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {

								if ((ehdr->_flags & (UI_64)Element::FlagVisible) && (_state._visible)) {
									ind0[4*ehdr->config._index] = ehdr->vertex_count | (ehdr->_flags & 0x000F0000) | ((ehdr->config.tex_unit_selector >> 32) & 0xFF000000);
															
									switch (ehdr->config.att_flags & 0xFFFF0000) {
										case Element::Loop:
											ind0[4*ehdr->config._index] |= ehdr->_reserved; //0x80000000;
										break;
									}
								} else {
									ind0[4*ehdr->config._index] = 0x10000000;
								}

								ind0[4*ehdr->config._index+1] = ehdr->domain_desc[AttributeArray::Vertex].offset + ehdr->domain_desc[AttributeArray::Vertex].size - ehdr->vertex_count;
								ind0[4*ehdr->config._index+2] = _state.refli._reserved & 0xFFFFFFFF;
								
								if ((ehdr->config.att_flags & 0xFFFF0000) != Element::ClipRegion) {
									ind0[4*ehdr->config._index+3] = _state.depth | (ehdr->xc_index<<16);
								} else {
									ind0[4*ehdr->config._index+3] = (_state.depth<<16) | _state.zval;
								}
							

								cfgu->index0.Release();
							}
								
							small_ucount[ehdr->config.att_flags >> 16]++;
						}
						cfg_unit.Release();
					} 
				} else {					
					// improve large 
					cfg_unit.Set(_buffers_net, ehdr->config.unit_id);
					if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
						if (cfgu->gl_laridx == 0) {
							cfgu->gl_laridx = -1;
							current_frame_ucount++;
						}


						if (cfgu->large_top <= cfgu->large_runner) {				
							ival = cfgu->large_index.Expand(32*sizeof(LargeCfg));
							if (ival != -1) cfgu->large_top += 32;
						}

						for (ival = 0;ts_val;ival += (ts_val & 1), ts_val>>=1);

						ehdr->config._index = cfgu->large_runner++;						

						if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
							if (ehdr->config._index == 0) l_cfg[0].t_count = 0;
							
							l_cfg[ehdr->config._index].el_ref = _state.refli._reserved;
							
							l_cfg[ehdr->config._index].aa_offset = l_cfg[0].t_count; // large_count;
							
							l_cfg[ehdr->config._index].cfg_vec[0] = l_cfg[0].t_count; // large_count;
							l_cfg[ehdr->config._index].cfg_vec[1] = ehdr->vertex_count;
							l_cfg[ehdr->config._index].cfg_vec[2] = (_state.depth << 16) | ((ehdr->config.att_flags >> 1) & 0x0F) | ((ehdr->_flags >> 12) & 0x00F0) | ((ehdr->config.tex_unit_selector >> 48) & 0x0F00) | ((ehdr->config.tex_unit_selector >> 28) & 0x00F000);
							l_cfg[ehdr->config._index].cfg_vec[3] = 0;

							l_cfg[0].t_count += ival*ehdr->vertex_count;
							large_count += ival*ehdr->vertex_count;

							if ((ehdr->_flags & (UI_64)Element::FlagVisible) && (_state._visible)) {
								l_cfg[ehdr->config._index].la_flags = 1;
							} else {
								l_cfg[ehdr->config._index].la_flags = 2;
							}

							cfgu->large_index.Release();
						}
						cfg_unit.Release();
					}					
				}
			}
		} else {

			for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
				ehdr->domain_desc[i].offset = store[i].runner;
				ehdr->domain_desc[i].size = 0;
			}
			
			nlink = e_pool->DownLink(_state.refli._reserved);
			if (nlink != _state.refli._reserved) {
					
				_state.target = 2;
				PushState(_state);
					
				_state.refli._reserved = nlink;
		
				_state.target = 0;
				if (_state._visible) _state._visible = (ehdr->_flags & (UI_64)Element::FlagVisible);
				
				_ms_runner++;

				continue;
target_2:
		
					
				_ms_runner--;

				for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
					ehdr->domain_desc[i].size += store[i].runner - ehdr->domain_desc[i].offset;
				}
			}
		}		

		ehdr->_flags |= (UI_64)Element::FlagBuilt;

		if (_state.refli._reserved != incoming_id) {
			nlink = e_pool->LeftLink(_state.refli._reserved);

			if (nlink != _state.refli._reserved) {
				if (e_pool->UpLink(nlink) == nlink) {
					_state.refli._reserved = nlink;
					_state.target = 0;

					continue;
		
				}
			}
		} else {
			UpdateBufferList(ehdr);
			break;
		}

		PopState(_state);

		if (_ms_runner == 3) {
			_state.depth = _state.depth;
		}
	}

	return 0;
}

unsigned int OpenGL::Core::AttStruct::Updator(Memory::Allocator * e_pool) {
	Text::Header text_hdr;
	AttributeArray v_arr;
	Element::Header * ehdr(0);
	UI_64 nlink(-1);
	float * t_ptr(0);
	unsigned int ts_val(0), i_result(0);

	ConstructorState _state;

	UI_64 result(0), u_sel(-1), x_val(0);
	SFSco::Object a_cfg, b_cfg;

	unsigned int ival(0);
		
	_state.refli._reserved = incoming_id;
	_state.target = 0;
	_state.x_flag = 0;
	_state.zval = 0;
	_state.depth = 0;
	_state._visible = 1;
	_state._reserved = -1;

	constructor_runner = 0;

	for (;;) {
		ehdr = reinterpret_cast<Element::Header *>(e_pool->GetLinkPtr(_state.refli._reserved));

		switch (_state.target) {
			case 1: goto target_1;
			case 2: goto target_2;
		}


		MatrixStackLoad(ehdr);

		if ((ehdr->config.att_flags & 0xFFFF0000) != Element::SceneRegion) {
			if (_state._reserved = SelectCfgUnit(ehdr->config.tex_unit_selector, ehdr->config.att_flags)) {

				switch ((ehdr->config.att_flags & 0xFFFF0000)) {
					case Element::TQuad:
						text_hdr = reinterpret_cast<Text::Header *>(ehdr + 1)[0];

						temp_co_ma.Set(&store[AttributeArray::Color].buffer, text_hdr.fcolor_offset<<4);																		

						if (temp_co_ma.Acquire(reinterpret_cast<void **>(&t_ptr))) {
							if (t_ptr) Calc::FloatClear4N(t_ptr, text_hdr.font_color, 4);

							temp_co_ma.Release(t_ptr);
						}
					
						tran_mat.Set(&matrix_stack, (_ms_runner*AttributeArray::AttTypeCount + AttributeArray::Color)<<6);

						if (!tran_mat.IsIdentity()) {
							temp_co_ma.SetColumnCount(4);
							tran_mat >> temp_co_ma;
						}


					break;
				
				}

				
				nlink = e_pool->DownLink(_state.refli._reserved);
				if (nlink != _state.refli._reserved) {
					_state.target = 1;

					if (ehdr->_flags & (UI_64)Element::FlagVisible2) _state._visible &= (UI_64)Element::FlagVisible;

					PushState(_state);
					
					_state.refli._reserved = nlink;
					_state.target = 0;

					if (_state._visible) _state._visible = (ehdr->_flags & (UI_64)Element::FlagVisible);
					ehdr->_flags &= ~(UI_64)Element::FlagVisible2;

					_ms_runner++;
					continue;
target_1:

					_ms_runner--;
				}
								
			
				ts_val = ehdr->config.att_flags & 0x0000FFFF;
				if (ehdr->vertex_count <= max_geometry_out) {
					for (unsigned int ti(0);ts_val;ti++) {
						if (ts_val & 1) {
							x_val = (ehdr->domain_desc[ti].offset + ehdr->domain_desc[ti].size - ehdr->vertex_count);
							x_val <<= 2;

if (x_val > 0xF0000000) {
	x_val = 4; // fuck situation
}

							v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[ti].array_id);

							if (float * buf_ptr = reinterpret_cast<float *>(store[ti].buffer.Acquire())) {
								if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
									Calc::FloatCopy4(buf_ptr + x_val, ahdr + 1, ehdr->vertex_count);
									v_arr.Release();
								}
								store[ti].buffer.Release();
							}

							temp_co_ma.Set(&store[ti].buffer, x_val<<2);

							tran_mat.Set(&matrix_stack, (_ms_runner*AttributeArray::AttTypeCount+ti)<<6);
							
							if (!tran_mat.IsIdentity()) {
								temp_co_ma.SetColumnCount(ehdr->vertex_count);
								tran_mat >> temp_co_ma;
							}
						}
						ts_val >>= 1;
					}


					if (_state._reserved != ehdr->config.unit_id) {
						u_sel = -1;

						a_cfg.Set(_buffers_net, ehdr->config.unit_id);
						b_cfg.Set(_buffers_net, _state._reserved);

						if (CfgUnit * old_cfgu = reinterpret_cast<CfgUnit *>(a_cfg.Acquire())) {
							if (CfgUnit * new_cfgu = reinterpret_cast<CfgUnit *>(b_cfg.Acquire())) {											
								if (new_cfgu->index_top <= new_cfgu->i_runner) {
									ival = new_cfgu->index0.Expand(256*4*sizeof(unsigned int));
									ival |= new_cfgu->index1.Expand(256*4*sizeof(unsigned int));
									if (ival != -1) new_cfgu->index_top += 256;
								}

								if (new_cfgu->index_top > new_cfgu->i_runner) {
									if (unsigned int * old_ind0 = reinterpret_cast<unsigned int *>(old_cfgu->index0.Acquire())) {
										if (unsigned int * new_ind0 = reinterpret_cast<unsigned int *>(new_cfgu->index0.Acquire())) {
											for (UI_64 i(0);i<new_cfgu->index_runner;i++) {
												if (new_ind0[4*i] == 0) {
													u_sel = i;
													break;
												}
											}

											if (u_sel == -1) u_sel = new_cfgu->i_runner++;

											new_ind0[4*u_sel + 0] = old_ind0[4*ehdr->config._index + 0];
											new_ind0[4*u_sel + 1] = old_ind0[4*ehdr->config._index + 1];
											new_ind0[4*u_sel + 2] = old_ind0[4*ehdr->config._index + 2];
											new_ind0[4*u_sel + 3] = old_ind0[4*ehdr->config._index + 3];

											old_ind0[4*ehdr->config._index + 0] = 0;
											old_ind0[4*ehdr->config._index + 1] = 0;
											old_ind0[4*ehdr->config._index + 2] = 0;
											old_ind0[4*ehdr->config._index + 3] = 0;

											new_cfgu->index0.Release();
										}
										old_cfgu->index0.Release();
									}



									if (u_sel != -1) {
										if (unsigned int * old_ind1 = reinterpret_cast<unsigned int *>(old_cfgu->index1.Acquire())) {
											if (unsigned int * new_ind1 = reinterpret_cast<unsigned int *>(new_cfgu->index1.Acquire())) {
										
												new_ind1[4*u_sel + 0] = old_ind1[4*ehdr->config._index + 0];
												new_ind1[4*u_sel + 1] = old_ind1[4*ehdr->config._index + 1];
												new_ind1[4*u_sel + 2] = old_ind1[4*ehdr->config._index + 2];
												new_ind1[4*u_sel + 3] = old_ind1[4*ehdr->config._index + 3];

												old_ind1[4*ehdr->config._index + 0] = 0;
												old_ind1[4*ehdr->config._index + 1] = 0;
												old_ind1[4*ehdr->config._index + 2] = 0;
												old_ind1[4*ehdr->config._index + 3] = 0;

												new_cfgu->index1.Release();
											}
											old_cfgu->index1.Release();
										}
							
										UpdateCfg(ehdr->config.unit_id, ehdr->config._index);
										old_cfgu->ui_reserved[0] = 1;

										UpdateCfg(_state._reserved, u_sel);
										ehdr->config._index = u_sel;
										result = u_sel + 1;
									}
								}																				

								b_cfg.Release();
							}

							a_cfg.Release();
						}

						if (u_sel != -1) ehdr->config.unit_id = _state._reserved;
					}
				
					a_cfg.Set(_buffers_net, ehdr->config.unit_id);
					if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(a_cfg.Acquire())) {
			
						if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
							if ((ind0[4*ehdr->config._index] & 0x0000FFFF) == 0) {
								if ((ehdr->_flags & (UI_64)Element::FlagVisible) && (_state._visible)) {
									ind0[4*ehdr->config._index] = ehdr->vertex_count | (ehdr->_flags & 0x000F0000) | ((ehdr->config.tex_unit_selector >> 32) & 0xFF000000);
							
									switch (ehdr->config.att_flags & 0xFFFF0000) {
										case Element::Loop:
											ind0[4*ehdr->config._index] |= ehdr->_reserved;
										break;
										case Element::ClipRegion:
											i_result = STENCIL_FLAG;
										break;
									}

									result |= 0x80000001;
								}
							} else {							

								if (((ehdr->_flags & (UI_64)Element::FlagVisible) == 0) || (_state._visible == 0)) {								
									if (ehdr->_flags & (UI_64)Element::FlagVisible) ehdr->_flags |= (UI_64)Element::FlagVisible2;
								
									ind0[4*ehdr->config._index] = 0x10000000;

									switch (ehdr->config.att_flags & 0xFFFF0000) {
										case Element::ClipRegion:
											i_result = STENCIL_FLAG;
										break;
									}

									result |= 0x80000001;
								} else {
									if (ehdr->config.tex_unit_selector & 0x0FFFFFFFF)
									if (((ind0[4*ehdr->config._index] ^ (ehdr->config.tex_unit_selector >> 32)) & 0xFF000000) != 0) {
										ind0[4*ehdr->config._index] = ehdr->vertex_count | (ehdr->_flags & 0x000F0000) | ((ehdr->config.tex_unit_selector >> 32) & 0xFF000000);

										result |= 0x80000001;
									}
								}
							}
						

							if ((ehdr->config.att_flags & 0xFFFF0000) != Element::ClipRegion) {
								if (ehdr->xc_index != (ind0[4*ehdr->config._index + 3]>>16)) {
									ind0[4*ehdr->config._index + 3] &= 0x0000FFFF;
									ind0[4*ehdr->config._index + 3] |= (ehdr->xc_index<<16);
									result |= 0x80000001;
								}
							}

							if (ind0[4*ehdr->config._index] & 0x0000FFFF) {
								switch (ehdr->config.att_flags & 0xFFFF0000) { 
									case Element::Symbol:
										if (unsigned int * ind1 = reinterpret_cast<unsigned int *>(cfgu->index1.Acquire())) {
											if (ehdr->_reserved < max_texture_layers) {
												ind1[4*ehdr->config._index + 3] = ehdr->_reserved | text_hdr.level_selector;
											}

											result |= 0x80000001;
																									
											cfgu->index1.Release();
										}

									break;
								}
							}
							
							cfgu->index0.Release();
						}
			
						if (result & 0x80000000) cfgu->ui_reserved[0] = 1;

						a_cfg.Release();

						if (result & 0x80000000) {
							result ^= 0x80000000;
							UpdateCfg(ehdr->config.unit_id, ehdr->config._index);
						}

					}
				} else { // large
					a_cfg.Set(_buffers_net, ehdr->config.unit_id);
					if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(a_cfg.Acquire())) {
						
						if (cfgu->gl_laridx > 0) {
							if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
								if ((l_cfg[ehdr->config._index].la_flags & 2) == 0) {
									if ((ehdr->_flags & (UI_64)Element::FlagVisible) && (_state._visible)) {
										l_cfg[ehdr->config._index].la_flags |= 1;  // some states
									} else {
										l_cfg[ehdr->config._index].la_flags &= 0xFFFFFFFE;

										if (_state.refli._reserved != incoming_id) {
											l_cfg[ehdr->config._index].la_flags |= 2;
										}
									}
								} else {
									if (_state.refli._reserved != incoming_id) {
										if ((ehdr->_flags & (UI_64)Element::FlagVisible) && (_state._visible)) {
											l_cfg[ehdr->config._index].la_flags ^= 2;
											l_cfg[ehdr->config._index].la_flags |= 1;  // some states
										}
									}
								}

								if ((l_cfg[ehdr->config._index].la_flags & 0x80000001) == 0x80000001) i_result |= UPDATE_FLAG;
							
								cfgu->large_index.Release();
							}

							a_cfg.Release();

							UpdateCfg(ehdr->config.unit_id, ehdr->config._index | 0x80000000);
						}
					}
				}				
			}

		} else {
			if (ehdr->_flags & (UI_64)Element::FlagNegTex) i_result = STENCIL_FLAG;

			nlink = e_pool->DownLink(_state.refli._reserved);
			if (nlink != _state.refli._reserved) {
				_state.target = 2;
				PushState(_state);

				_state.refli._reserved = nlink;
				_state.target = 0;
				if (_state._visible) _state._visible = (ehdr->_flags & (UI_64)Element::FlagVisible);
				_ms_runner++;

				continue;
target_2:
				
				_ms_runner--;				
			}
		}
		
		if (_state.refli._reserved != incoming_id) {
			nlink = e_pool->LeftLink(_state.refli._reserved);

			if (nlink != _state.refli._reserved) {
				if (e_pool->UpLink(nlink) == nlink) {
					_state.refli._reserved = nlink;
					_state.target = 0;
					continue;
				}
			}
		} else {
			UpdateBufferList(ehdr);
			break;
		}

		PopState(_state);
		
	}
	
	if (result) i_result |= UPDATE_FLAG;

	return i_result;
}


unsigned int OpenGL::Core::AttStruct::Transformer(Memory::Allocator * e_pool) {
	UI_64 result(0);
	AttributeArray v_arr;
	Element::Header * ohdr(0), * ehdr(0);
	unsigned int ts_val(0), i_result(0);

	if (float * matrix_ptr = reinterpret_cast<float *>(matrix_stack.Acquire())) {
		ehdr = reinterpret_cast<Element::Header *>(e_pool->GetLinkPtr(incoming_id));
		ts_val = ehdr->config.att_flags & 0x0000FFFF;
		
		
		for (unsigned int ti(0);ts_val;ti++) {
			if (ts_val & 1) {
				v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[ti].array_id);

				if (v_arr.TransformAdjust(matrix_ptr)) {

					if (float * b_ptr = reinterpret_cast<float *>(store[ti].buffer.Acquire())) {
						b_ptr += ehdr->domain_desc[ti].offset*4;
						Calc::MF_Multiply44(matrix_ptr, ehdr->domain_desc[ti].size, b_ptr, b_ptr);
						
						store[ti].buffer.Release();
					}

			// propagate global matrix change				
					if (((ehdr->config.att_flags & 0xFFFF0000) == Element::SceneRegion) && (ehdr->_flags & (UI_64)Element::FlagNegTex)) {
						i_result = STENCIL_FLAG;
					}
			
					if (UI_64 * stack_ptr = reinterpret_cast<UI_64 *>(constructor_stack.Acquire())) {
						stack_ptr[0] = incoming_id;
						
						
						if (stack_ptr[0] != e_pool->DownLink(stack_ptr[0])) {
							
							for (result = 1;result>0;) {
								stack_ptr[2*result+1] = stack_ptr[2*result] = e_pool->DownLink(stack_ptr[2*(result-1)]);

								for (;result>0;) {
							
									ohdr = reinterpret_cast<Element::Header *>(e_pool->GetLinkPtr(stack_ptr[2*result]));
										
									if (ohdr->config.att_flags & (1<<ti)) {
										v_arr.Set(AttributeArray::_attributes_pool, ohdr->domain_desc[ti].array_id);

										v_arr.GlobalAdjust(matrix_ptr);
									}
									
									if (stack_ptr[2*result] != e_pool->DownLink(stack_ptr[2*result])) {
										result++;
										break;
									}

									for (stack_ptr[2*result] = e_pool->LeftLink(stack_ptr[2*result]);stack_ptr[2*result] == stack_ptr[2*result+1];stack_ptr[2*result] = e_pool->LeftLink(stack_ptr[2*result])) {
										if (--result == 0) break;
									}
								}
							}
						}

						constructor_stack.Release();
					}
				}
			}
			ts_val >>= 1;
		}
		matrix_stack.Release();
	}

	UpdateBufferList(ehdr);

	return i_result;
}

void OpenGL::Core::AttStruct::ResetLarge(UI_64 u_id, UI_64 idx) {
	SFSco::Object a_cfg;

	a_cfg.Set(_buffers_net, u_id);
	if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(a_cfg.Acquire())) {
		if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
			l_cfg[idx].la_flags |= 0x80000000;

			cfgu->large_index.Release();
		}

		a_cfg.Release();
	}
}

void OpenGL::Core::AttStruct::UpdateCfg(UI_64 c_id, UI_64 c_idx) {
	UI_64 result(-1);

	if (UI_64 * cule = reinterpret_cast<UI_64 *>(cfg_ulist.Acquire())) {
		for (UI_64 i(1);i<=cule[0];i++) {
			if (cule[i*4] == c_id) {
				if ((c_idx & 0x80000000) == 0) {
					if ((c_idx >= cule[i*4+1]) && (c_idx < (cule[i*4+1]+cule[i*4+2]))) {
						result = 0;
						break;
					}

					if (((c_idx + 1) == cule[i*4+1]) || (c_idx == (cule[i*4+1]+cule[i*4+2]))) {
						if ((c_idx+1) == cule[i*4+1]) cule[i*4+1] = c_idx;
						cule[i*4+2]++;

						result = 0;
						break;
					}
				} else {
					result = 0;
					break;
				}
			}
		}

		if (result) {
			result = ++cule[0];

			cule[result*4] = c_id;
			cule[result*4+1] = c_idx;
			cule[result*4+2] = 1;

			if (result < cule[1]) result = 0;
		}	
		
		cfg_ulist.Release();
	}


	if (result) {
		if (cfg_ulist.Expand(MIN_UPDATE_LIST_CAPACITY*4*sizeof(UI_64)) != -1) {
			if (UI_64 * cule = reinterpret_cast<UI_64 *>(cfg_ulist.Acquire())) {
				cule[1] += MIN_UPDATE_LIST_CAPACITY;
				cfg_ulist.Release();
			}
		}
	}

}


UI_64 OpenGL::Core::AttStruct::Blink(BlinkSet & bset) {
	UI_64 x_val(0);
	AttributeArray col_ar;
	SFSco::Object cfg_unit;

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(bset.belem.Acquire())) {
		cfg_unit.Set(_buffers_net, elhdr->config.unit_id);
		if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
			if (cfgu->configuration & (1<<AttributeArray::Color)) {
				BindBuffer(GL_ARRAY_BUFFER, att_buffer);

				if (bset._count) {				
					if (float * st_ptr = reinterpret_cast<float *>(store[AttributeArray::Color].buffer.Acquire())) {
						x_val = elhdr->domain_desc[AttributeArray::Color].offset + elhdr->domain_desc[AttributeArray::Color].size - elhdr->vertex_count;
						st_ptr += (x_val<<2);

						ClearBuf(st_ptr, bset.color[bset._phase], elhdr->vertex_count);
						BufferSubData(GL_ARRAY_BUFFER, (store[AttributeArray::Color].o_ffset + x_val)<<4, elhdr->vertex_count<<4, st_ptr);
						
						store[AttributeArray::Color].buffer.Release();
					}

					if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
		
						ind0[4*elhdr->config._index+3] &= 0x0000FFFF;
						ind0[4*elhdr->config._index+3] |= (bset.xcolor[bset._phase]<<16);

						BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
						BufferSubData(GL_ARRAY_BUFFER, elhdr->config._index<<4, 4*sizeof(unsigned int), ind0 + 4*elhdr->config._index);

						cfgu->index0.Release();
					}

				} else {
					bset.belem.InitAttributeArray(AttributeArray::Color, col_ar);

					if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(col_ar.Acquire())) {
						if (float * st_ptr = reinterpret_cast<float *>(store[AttributeArray::Color].buffer.Acquire())) {
							x_val = elhdr->domain_desc[AttributeArray::Color].offset + elhdr->domain_desc[AttributeArray::Color].size - elhdr->vertex_count;
							st_ptr += (x_val<<2);

							Calc::FloatCopy4(st_ptr, reinterpret_cast<float *>(ahdr + 1), elhdr->vertex_count);
							BufferSubData(GL_ARRAY_BUFFER, (store[AttributeArray::Color].o_ffset + x_val)<<4, elhdr->vertex_count<<4, st_ptr);
					
							store[AttributeArray::Color].buffer.Release();
						}
						col_ar.Release();
					}

					if (elhdr->config.att_flags & (1 << AttributeArray::TexCoord)) {
						bset.belem.InitAttributeArray(AttributeArray::TexCoord, col_ar);
						
						if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(col_ar.Acquire())) {
							if (float * st_ptr = reinterpret_cast<float *>(store[AttributeArray::TexCoord].buffer.Acquire())) {
								x_val = elhdr->domain_desc[AttributeArray::TexCoord].offset + elhdr->domain_desc[AttributeArray::TexCoord].size - elhdr->vertex_count;
								st_ptr += (x_val<<2);

								Calc::FloatCopy4(st_ptr, reinterpret_cast<float *>(ahdr + 1), elhdr->vertex_count);
								BufferSubData(GL_ARRAY_BUFFER, (store[AttributeArray::TexCoord].o_ffset + x_val)<<4, elhdr->vertex_count<<4, st_ptr);
					
								store[AttributeArray::TexCoord].buffer.Release();
							}
							col_ar.Release();
						}
					}


					if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
			
						ind0[4*elhdr->config._index+3] &= 0x0000FFFF;
						ind0[4*elhdr->config._index+3] |= (elhdr->xc_index<<16);

						BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
						BufferSubData(GL_ARRAY_BUFFER, elhdr->config._index<<4, 4*sizeof(unsigned int), ind0 + 4*elhdr->config._index);

						cfgu->index0.Release();
					}

				}
			}

			cfg_unit.Release();
		}
		bset.belem.Release();
	}

	return 0;
}

UI_64 OpenGL::Core::AttStruct::Hide(Element & el_obj) {
	SFSco::Object cfg_unit;
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
		elhdr->_flags &= ~(UI_64)Element::FlagVisible;

		cfg_unit.Set(_buffers_net, elhdr->config.unit_id);
		if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
			if (elhdr->vertex_count <= max_geometry_out) {				
				if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
					ind0[4*elhdr->config._index] = 0x10000000;

					BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
					BufferSubData(GL_ARRAY_BUFFER, elhdr->config._index<<4, 4*sizeof(unsigned int), ind0 + 4*elhdr->config._index);

					cfgu->index0.Release();
				}

			} else {
				if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
					l_cfg[elhdr->config._index].la_flags &= 0xFFFFFFFE;

					cfgu->large_index.Release();
				}

			}

			cfg_unit.Release();
		}
		el_obj.Release();
	}


	return 0;
}


UI_64 OpenGL::Core::AttStruct::Show(Element & el_obj) {
	SFSco::Object cfg_unit;
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
		elhdr->_flags |= (UI_64)Element::FlagVisible;
		
		cfg_unit.Set(_buffers_net, elhdr->config.unit_id);
		if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
			if (elhdr->vertex_count <= max_geometry_out) {
				if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
					ind0[4*elhdr->config._index] = elhdr->vertex_count | (elhdr->_flags & 0x000F0000) | ((elhdr->config.tex_unit_selector >> 32) & 0xFF000000);

					switch ((elhdr->config.att_flags & 0xFFFF0000)) {
						case Element::Loop:
							ind0[4*elhdr->config._index] |= elhdr->_reserved;
						break;

					}
	//				ind0[4*elhdr->config._index+3] &= 0x0000FFFF;
	//				ind0[4*elhdr->config._index+3] |= ((~elhdr->xc_index)<<16);

					BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
					BufferSubData(GL_ARRAY_BUFFER, elhdr->config._index<<4, 4*sizeof(unsigned int), ind0 + 4*elhdr->config._index);

					cfgu->index0.Release();
				}
			} else {
				if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
					l_cfg[elhdr->config._index].la_flags |= 1;

					cfgu->large_index.Release();
				}

			}

			cfg_unit.Release();
		}
		el_obj.Release();
	}


	return 0;
}

UI_64 OpenGL::Core::AttStruct::ResetXC(Element & el_obj) {
	SFSco::Object cfg_unit;

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
		cfg_unit.Set(_buffers_net, elhdr->config.unit_id);
		if (CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(cfg_unit.Acquire())) {
			if (elhdr->vertex_count <= max_geometry_out) {
				if (unsigned int * ind0 = reinterpret_cast<unsigned int *>(cfgu->index0.Acquire())) {
					ind0[4*elhdr->config._index+3] &= 0x0000FFFF;
					ind0[4*elhdr->config._index+3] |= (elhdr->xc_index<<16);

					BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
					BufferSubData(GL_ARRAY_BUFFER, elhdr->config._index<<4, 4*sizeof(unsigned int), ind0 + 4*elhdr->config._index);

					cfgu->index0.Release();
				}
			} else {

			}

			cfg_unit.Release();
		}
		el_obj.Release();
	}


	return 0;
}



UI_64 OpenGL::Core::AttStruct::UpdateBufferList(Element::Header * ehdr) {
	UI_64 result(0);
	// add to update list
	if (UI_64 * uval = reinterpret_cast<UI_64 *>(buffer_ulist.Acquire())) {
		result = ((uval[0]+AttributeArray::AttTypeCount) >= uval[1]);
		buffer_ulist.Release();
	}		
	
	if (result) result = buffer_ulist.Expand(MIN_UPDATE_LIST_CAPACITY*4*sizeof(UI_64));
	
	if (result != -1) {
		if (UI_64 * uval = reinterpret_cast<UI_64 *>(buffer_ulist.Acquire())) {
			if (result) uval[1] += MIN_UPDATE_LIST_CAPACITY;
			
			result = 0;

			for (UI_64 mu(1);mu<=uval[0];mu++) {
				if ((ehdr->domain_desc[uval[mu*4]].size) && ((result & ((UI_64)1 << uval[mu*4])) == 0)) {
					if ((ehdr->domain_desc[uval[mu*4]].offset+ehdr->domain_desc[uval[mu*4]].size) == uval[mu*4+1]) {
						uval[mu*4+1] = ehdr->domain_desc[uval[mu*4]].offset;
						uval[mu*4+2] += ehdr->domain_desc[uval[mu*4]].size;
						result |= ((UI_64)1<<uval[mu*4]);

					} else if (ehdr->domain_desc[uval[mu*4]].offset == (uval[mu*4+1]+uval[mu*4+2])) {
						uval[mu*4+2] += ehdr->domain_desc[uval[mu*4]].size;
						result |= ((UI_64)1<<uval[mu*4]);
					
					}
				}
			}

			
			for (unsigned int gg(0);gg<AttributeArray::AttTypeCount;gg++) {
				if ((ehdr->domain_desc[gg].size) && ((result & ((UI_64)1 << gg)) == 0)) {
					uval[++uval[0]*4] = gg;
					uval[uval[0]*4+1] = ehdr->domain_desc[gg].offset;
					uval[uval[0]*4+2] = ehdr->domain_desc[gg].size;
				}
			}

			buffer_ulist.Release();
		}
	}

	return 0;
}

UI_64 OpenGL::Core::AttStruct::Transform() {
	UI_64 result(1);
	char * store_pointers[AttributeArray::AttTypeCount];

	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
		store_pointers[i] = reinterpret_cast<char *>(store[i].buffer.Acquire());
		if (!store_pointers[i]) {
			result = 0;
			break;
		}
	}

	if (result)
	if (UI_64 * uval = reinterpret_cast<UI_64 *>(buffer_ulist.Acquire())) {
		result = uval[0];
		
		BindBuffer(GL_ARRAY_BUFFER, att_buffer);

		for (UI_64 jj(1);jj<=result;jj++) {
			BufferSubData(GL_ARRAY_BUFFER, (store[uval[jj*4]].o_ffset+uval[jj*4+1])<<4, uval[jj*4+2]<<4, store_pointers[uval[jj*4]]+(uval[jj*4+1]<<4));

		}

		uval[0] -= result;
		buffer_ulist.Release();
	}

	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
		if (store_pointers[i]) store[i].buffer.Release();
	}

	return 0;
}

UI_64 OpenGL::Core::AttStruct::Update(Memory::Allocator * e_pool) {
	UI_64 result(-1), la_off(0);
	unsigned int xval(0), a_cfg(0);
	SFSco::Object c_unit;
	Element l_el;
	AttributeArray a_arr;

	_buffers_net->Lock();

		if (unsigned int * ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
			if (ibu[0]<current_frame_ucount) {
				if (current_frame_ucount > ibu[1]) {
					xval = (current_frame_ucount + 127 - ibu[1]) >> 7;
					i_buffers.Release();
					ibu = 0;

					xval <<= 7;
					if (i_buffers.Expand(xval*sizeof(unsigned int)) != -1) {
						if (ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
							ibu[1] += xval;
						}
					}
				}

				if (ibu) {
					xval = ibu[0] + 2;

					GenBuffers(current_frame_ucount - ibu[0], ibu + xval);				
					ibu[0] = current_frame_ucount;
				}
			}

			if (ibu) {

				if (UI_64 * cule =  reinterpret_cast<UI_64 *>(cfg_ulist.Acquire())) {

					for (UI_64 j(1);j<=cule[0];j++) {
						if ((result == cule[j*4]) || (cule[j*4] == -1)) continue;
						result = cule[j*4];

						c_unit.Set(_buffers_net, result);
						if (CfgUnit * e_cfg = reinterpret_cast<CfgUnit *>(c_unit.Acquire())) {

							if ((cule[j*4 + 1] & 0x80000000) == 0) {						
							
								if (e_cfg->gl_refidx == 0) e_cfg->gl_refidx = ibu[xval++];
													
								BindBuffer(GL_ARRAY_BUFFER, e_cfg->gl_refidx);

								if (e_cfg->index_runner != e_cfg->i_runner) {
									if (e_cfg->i_runner > e_cfg->index_runner) e_cfg->index_runner = e_cfg->i_runner;
									else e_cfg->i_runner = e_cfg->index_runner;

									BufferData(GL_ARRAY_BUFFER, e_cfg->index_runner<<5, 0, GL_STATIC_DRAW);
								
									if (unsigned int * ipot = reinterpret_cast<unsigned int *>(e_cfg->index0.Acquire())) {
										BufferSubData(GL_ARRAY_BUFFER, 0, e_cfg->index_runner<<4, ipot);
										e_cfg->index0.Release();
									}
							
									if (unsigned int * ipot = reinterpret_cast<unsigned int *>(e_cfg->index1.Acquire())) {
										BufferSubData(GL_ARRAY_BUFFER, e_cfg->index_runner<<4, e_cfg->index_runner<<4, ipot);
										e_cfg->index1.Release();
									}
							
									e_cfg->ui_reserved[0] = 0;
								}

								if (unsigned int * ipot0 = reinterpret_cast<unsigned int *>(e_cfg->index0.Acquire())) {
									if (unsigned int * ipot1 = reinterpret_cast<unsigned int *>(e_cfg->index1.Acquire())) {
										for (UI_64 i(j);i<=cule[0];i++) {
											if (result == cule[i*4]) {
												if (e_cfg->ui_reserved[0]) {									
													BufferSubData(GL_ARRAY_BUFFER, cule[i*4+1]<<4, cule[i*4+2]<<4, ipot0 + cule[i*4+1]*4);
													BufferSubData(GL_ARRAY_BUFFER, (e_cfg->index_runner+cule[i*4+1])<<4, cule[i*4+2]<<4, ipot1 + (cule[i*4+1]*4));
												}

												cule[i*4] = -1;
											}
										}
										e_cfg->index1.Release();
									}
									e_cfg->index0.Release();
								}
							} else {
								cule[4*j] = -1;
								
								if ((e_cfg->large_runner) && (e_cfg->gl_laridx > 0)) {
																	
									BindBuffer(GL_ARRAY_BUFFER, e_cfg->gl_laridx); //att_buffer

									if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(e_cfg->large_index.Acquire())) {

										for (unsigned int li(0);li<e_cfg->large_runner;li++) {
											if ((l_cfg[li].la_flags & 0x80000001) == 0x80000001) {
												l_cfg[li].la_flags ^= 0x80000000;

												l_el.Set(e_pool, l_cfg[li].el_ref);

												if (Element::Header * l_hdr = reinterpret_cast<Element::Header *>(l_el.Acquire())) {
													a_cfg = l_hdr->config.att_flags & 0x0000FFFF;
													la_off = 0;
										
													for (unsigned int a_id(AttributeArray::Vertex);a_cfg;a_cfg>>=1, a_id++) {
														if (a_cfg & 1) {
															l_el.InitAttributeArray(reinterpret_cast<AttributeArray::Type &>(a_id), a_arr);
											
															if (AttributeArray::Header * la_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
											
																BufferSubData(GL_ARRAY_BUFFER, (l_cfg[li].aa_offset + la_off)<<4, l_hdr->vertex_count<<4, la_hdr + 1); // large_offset
																	
																a_arr.Release();
															}

															la_off += l_hdr->vertex_count;
														}
													}

													l_el.Release();
												}
											}
										}

										e_cfg->large_index.Release();
									}
								}
							}
							c_unit.Release();
						}
					}

					cule[0] = 0;

					cfg_ulist.Release();
				}
				
				i_buffers.Release();
			}
		}

	_buffers_net->Unlock();

	return result;
}


UI_64 OpenGL::Core::AttStruct::Rewrite(Memory::Allocator * e_pool) {	
	UI_64 result(-1), la_off(0);
	unsigned int xval(0), oval(0), bval(0), a_cfg(0);
	Element l_el;
	AttributeArray a_arr;

	UI_64 boff(0);

	if (UI_64 * uval = reinterpret_cast<UI_64 *>(buffer_ulist.Acquire())) {
		uval[0] = 0;
		buffer_ulist.Release();
	}


		// attribute upload to server
	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) boff += store[i].runner;

	large_offset = -1;
	large_offset <<= max_tex_vals[1];
	large_offset = (boff + max_tex_vals[0]) & large_offset;
	
	ActiveTexture(GL_TEXTURE0 + attribute_unit);
	TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, 0);

	BindBuffer(GL_ARRAY_BUFFER, att_buffer);
	BufferData(GL_ARRAY_BUFFER, large_offset<<4, 0, GL_DYNAMIC_DRAW); //  + large_count)
	
	TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, att_buffer);


	boff = 0;
	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
		if (store[i].runner)
		if (void * dpo = store[i].buffer.Acquire()) {
			BufferSubData(GL_ARRAY_BUFFER, boff<<4, store[i].runner<<4, dpo);
			store[i].buffer.Release();

			store[i].o_ffset = boff;
			boff += store[i].runner;
		}
	}

	_buffers_net->Lock();

	if (unsigned int * cbb = reinterpret_cast<unsigned int *>(config_backb.Acquire())) {
			
		for (unsigned int kk(0);kk<cbb[0];kk++) {
			oval = 0; bval = 0;

			if (xval = (cbb[2+kk*8+4] & 0x00FF))				
			for (;;) {
				oval++;

				if (xval & 1) {
					cbb[2+kk*8+bval++] = store[oval].o_ffset;
					if (bval>=4) break;
				}
									
				if (!(xval >>= 1)) break;
			}

			cbb[2+kk*8+5] = bval;

		}
		config_backb.Release();
	}


	if (unsigned int * ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
		if (ibu[0]<current_frame_ucount) {
			if (current_frame_ucount > ibu[1]) {
				xval = (current_frame_ucount + 127 - ibu[1]) >> 7;
				i_buffers.Release();
				ibu = 0;

				xval <<= 7;
				if (i_buffers.Expand(xval*sizeof(unsigned int)) != -1) {
					if (ibu = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
						ibu[1] += xval;
					}
				}
			}

			if (ibu) {
				xval = ibu[0] + 2;
				GenBuffers(current_frame_ucount - ibu[0], ibu + xval);
				ibu[0] = current_frame_ucount;
			}
		}

		if (ibu) {
			if (bfn_top != -1) {
				UI_64 tlink(bfn_top), nlink(bfn_top), olink(-1), plink(-1), klink(-1), llink(-1);

				for (;;) {
					unsigned int * drwt = reinterpret_cast<unsigned int *>(_buffers_net->GetLinkPtr(nlink));
					plink = olink = _buffers_net->DownLink(nlink);
					for (;;) {
						llink = klink = _buffers_net->DownLink(plink);

						for (;;) {
							CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(_buffers_net->GetLinkPtr(klink));

							if (cfgu->index_runner = cfgu->i_runner) {
							
								if (cfgu->gl_refidx == 0) cfgu->gl_refidx = ibu[xval++];

								if (drwt[0] == Element::ClipRegion) {
									_stencil_buf = cfgu->gl_refidx;
									_stencil_count = cfgu->index_runner;
								}

								BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);
								BufferData(GL_ARRAY_BUFFER, cfgu->index_runner<<5, 0, GL_STATIC_DRAW);
															
								if (void * ipot = cfgu->index0.Acquire()) {
									BufferSubData(GL_ARRAY_BUFFER, 0, cfgu->index_runner<<4, ipot);

									cfgu->index0.Release();
								}

								if (void * ipot = cfgu->index1.Acquire()) {
									BufferSubData(GL_ARRAY_BUFFER, cfgu->index_runner<<4, cfgu->index_runner<<4, ipot);
									cfgu->index1.Release();
								}

							}
						
							if (cfgu->large_runner) {
								if (cfgu->gl_laridx < 0) {
									cfgu->gl_laridx = ibu[xval++];
								}

								BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_laridx); // att_buffer

								if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
									la_off = -1;
									la_off <<= max_tex_vals[1];
									l_cfg[0].t_count = (l_cfg[0].t_count + max_tex_vals[0]) & la_off;

									BufferData(GL_ARRAY_BUFFER, l_cfg[0].t_count<<4, 0, GL_DYNAMIC_DRAW);


									for (unsigned int li(0);li<cfgu->large_runner;li++) {
							//			l_cfg[li].cfg_vec[0] += large_offset;

										l_el.Set(e_pool, l_cfg[li].el_ref);
										if (Element::Header * l_hdr = reinterpret_cast<Element::Header *>(l_el.Acquire())) {
											a_cfg = l_hdr->config.att_flags & 0x0000FFFF;
											la_off = 0;
											for (unsigned int a_id(AttributeArray::Vertex);a_cfg;a_cfg>>=1, a_id++) {
												if (a_cfg & 1) {
													l_el.InitAttributeArray(reinterpret_cast<AttributeArray::Type &>(a_id), a_arr);
										
													if (AttributeArray::Header * la_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
														
														BufferSubData(GL_ARRAY_BUFFER, (l_cfg[li].aa_offset + la_off)<<4, l_hdr->vertex_count<<4, la_hdr + 1); // large_offset

														a_arr.Release();
													}

													la_off += l_hdr->vertex_count;
												}
											}

											l_el.Release();
										}
									}
								
									cfgu->large_index.Release();
								}
							}

							klink = _buffers_net->LeftLink(klink);
							if (klink == llink) break;
						}

						plink = _buffers_net->LeftLink(plink);
						if (plink == olink) break;

					}

					nlink = _buffers_net->LeftLink(nlink);
					if (nlink == tlink) break;

				}
			}
				
			i_buffers.Release();
		}
	} else {
		result = -2;
	}

	_buffers_net->Unlock();

	glFlush();

	return result;
}

void OpenGL::Core::AttStruct::ResetBufferUnits() {
	UI_64 tlink(-1), nlink(-1);
	
	_ms_runner = 0;

	for (unsigned int i(0);i<Element::DrawTypeCount;i++) small_ucount[i] = 0;
	
	_buffers_net->Lock();
	
	
	tlink = _buffers_net->GetFirst(cfg_unit_tid);
	if (tlink != -1) {
		nlink = tlink;

		for (;;) {
			CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(_buffers_net->GetLinkPtr(nlink));
			
			cfgu->i_runner = 0;
			cfgu->l_runner = 0;

			cfgu->large_runner = 0;

			cfgu->ui_reserved[0] = 0;

			nlink = _buffers_net->GetNext(nlink);
			if (nlink == tlink) break;
		}
	}
	
	
	_buffers_net->Unlock();

	for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
		store[i].runner = 0;
		store[i].o_ffset = 0;
	}

}



UI_64 OpenGL::Core::AttStruct::SelectCfgUnit(UI_64 tu_selector, unsigned int cfg_selector) {
	UI_64 tlink(-1), nlink(-1), alink(-1), blink(-1), xlink(-1), ylink(-1);
	UI_64 xval(1);
	unsigned int mlayer_tex_type((tu_selector >> 32) & 0x0000FFFF);
	
	SFSco::Object nobj, bobj, yobj, ti0, ti1, tlel;
	unsigned int drw_selector(cfg_selector & 0xFFFF0000);
	cfg_selector &= 0x0000FFFF;

	if (drw_selector == Element::Symbol) {
		cfg_selector |= (1 << AttributeArray::Color);

	} else {
		cfg_selector |= ((mlayer_tex_type & LAYERED_TEXTURE) << 1);
	}

	_buffers_net->Lock();
	
	if (bfn_top != -1) {
		nlink = tlink = bfn_top;

		for (;;) {
			unsigned int * drw_id = reinterpret_cast<unsigned int * >(_buffers_net->GetLinkPtr(nlink));
			
			if (drw_id[0] == drw_selector) {
				xlink = _buffers_net->DownLink(nlink);

				if (xlink != nlink) {
					ylink = xlink;

					for (;;) {
						unsigned int * texu_id = reinterpret_cast<unsigned int *>(_buffers_net->GetLinkPtr(ylink));
						if (texu_id[0] == (tu_selector & 0xFFFFFFFF)) {
							
							alink = _buffers_net->DownLink(ylink);

							if (alink != ylink) {
								blink = alink;

								for (;;) {
									CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(_buffers_net->GetLinkPtr(blink));
									if (cfgu->configuration == cfg_selector) {
										xval = 4;
										break;
									}
											
									blink = _buffers_net->LeftLink(blink);
									if (blink == alink) {
										xval = 3;
										break;
									}
								}

								if (xval) break;
							} else {
								xval = 3;
								break;
							}

						}

						ylink = _buffers_net->LeftLink(ylink);
						if (ylink == xlink) {
							xval = 2;
							break;
						}

					}

					if (xval) break;
				} else {
					xval = 2;
					break;
				}

			}

			

			nlink = _buffers_net->LeftLink(nlink);
			if (nlink == tlink) {
				xval = 1;
				break;
			}
		}

	}


	if (nlink != -1) nobj.Set(_buffers_net, nlink); // tex
	if (ylink != -1) yobj.Set(_buffers_net, ylink); // drw		
	if (blink != -1) bobj.Set(_buffers_net, blink); // cfg

	_buffers_net->Unlock();
	
	
	switch (xval) {
		case 1: // draw unit
			xval = nobj.New(drw_unit_tid, 4*sizeof(unsigned int), _buffers_net);
			if (bfn_top == -1) bfn_top = xval;

			if (unsigned int * drw_id = reinterpret_cast<unsigned int *>(nobj.Acquire())) {
				drw_id[0] = drw_selector;
				nobj.Release();				
			}
		case 2: // no tunit
			if (!nobj) break;
			yobj = nobj;
			xval = yobj.Child(tex_unit_tid, 4*sizeof(unsigned int));

			if (unsigned int * tex_id = reinterpret_cast<unsigned int *>(yobj.Acquire())) {
				tex_id[0] = tu_selector & 0xFFFFFFFF;
				tex_id[1] = mlayer_tex_type;
				yobj.Release();
			}
		case 3: // cfg unit, create all buffers
			if (!yobj) break;

			bobj = yobj;
			xval = bobj.Child(cfg_unit_tid, sizeof(CfgUnit));
			

			xval = ti0.New(buf_unit_tid, 256*4*sizeof(unsigned int));
			xval |= ti1.New(buf_unit_tid, 256*4*sizeof(unsigned int));
			
			xval |= tlel.New(buf_unit_tid, 32*sizeof(LargeCfg));

			if (xval != -1)
			if (CfgUnit * cfgu = reinterpret_cast<CfgUnit * >(bobj.Acquire())) {
				cfgu->configuration = cfg_selector;
				
				cfgu->index0 = ti0;
				cfgu->index1 = ti1;
				cfgu->large_index = tlel;
				
									
				cfgu->index_top = 256;
				cfgu->large_top = 32;
				current_frame_ucount++;

				if (unsigned int * cbb = reinterpret_cast<unsigned int *>(config_backb.Acquire())) {
					cfg_selector >>= 1;
					
					if (drw_selector != Element::Symbol) cfg_selector |= mlayer_tex_type;

					for (unsigned int ii(0);ii<cbb[0];ii++) {
						if ((cbb[2+ii*8+4] & (0x00FF | LAYERED_TEXTURE)) == cfg_selector) {
							cfgu->selector = ii;
							cfg_selector = -1;
							break;
						}
					}

					if (cfg_selector != -1) {							
						cfgu->selector = cbb[0]++;

						cbb[2+cfgu->selector*8+4] = cfg_selector;
						cbb[2+cfgu->selector*8+5] = 0;
						cbb[2+cfgu->selector*8+6] = 0;
						cbb[2+cfgu->selector*8+7] = 0;

						if (cbb[0]>=cbb[1]) cfg_selector = 1;
						else cfg_selector = 0;

						config_backb.Release();
						cbb = 0;

						if (cfg_selector) {
							if (config_backb.Expand(32*8*sizeof(int)) != -1) {
								if (int * cbb = reinterpret_cast<int *>(config_backb.Acquire())) {
									cbb[1] += 32;									
								}
							}
						}							
					}

					if (cbb) {
						config_backb.Release();
					}
				}

				bobj.Release();
			}

		break;
	}

	if (xval == -1) bobj.Blank();

	return bobj.GetID();
}

UI_64 OpenGL::Core::AttStruct::StencilSet() {

	glDisable(GL_DEPTH_TEST);

	if ((_stencil_buf) && (_stencil_count)) {
		BindBuffer(GL_ARRAY_BUFFER, _stencil_buf);

		for (unsigned int i(_stencil_count);i>0;i--) {
			VertexAttribIPointer(0, 4, GL_INT, 0, reinterpret_cast<void *>((i-1)*4*sizeof(int)));
			glDrawArrays(GL_POINTS, 0, 1);
			glFlush();
		}

	}

	glEnable(GL_DEPTH_TEST);
	

	return 0;
}

int OpenGL::Core::AttStruct::RenderSmall(UI_64 olink) {
	UI_64 plink(olink);

	int result(0);

	if (unsigned int * ipo = reinterpret_cast<unsigned int *>(i_buffers.Acquire())) {
		if (int * cbb = reinterpret_cast<int *>(config_backb.Acquire())) {

		
			for (;;) {
				CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(_buffers_net->GetLinkPtr(plink));
				if ((cfgu->index_runner) && (cfgu->gl_refidx)) {
					if (cfg_offs_location != -1) Uniform4iv(cfg_offs_location, 1, cbb+cfgu->selector*8+2);
					if (cfg_flags_location != -1) Uniform4iv(cfg_flags_location, 1, cbb+cfgu->selector*8+6);

							
					BindBuffer(GL_ARRAY_BUFFER, cfgu->gl_refidx);

					VertexAttribIPointer(0, 4, GL_INT, 0, 0);
					VertexAttribIPointer(1, 4, GL_INT, 0, reinterpret_cast<void *>(cfgu->index_runner*4*sizeof(unsigned int)));

					glDrawArrays(GL_POINTS, 0, cfgu->index_runner); //

				}
					
				plink = _buffers_net->LeftLink(plink);
				if (plink == olink) break;
			}

			config_backb.Release();
		}
		
		i_buffers.Release();
	}

	return 0;
}

int OpenGL::Core::AttStruct::RenderLarge(UI_64 olink, SFSco::IAllocator * e_pool) {
	UI_64 plink(olink);
	Element la_obj;
	AttributeArray a_arr;

	int result(0), att_cfg(0);



	Calc::FloatClear16(temp_ma, AttributeArray::AttTypeCount + 1);

	ActiveTexture(GL_TEXTURE0 + large_unit);


	if (UI_64 * lm_ptr = reinterpret_cast<UI_64 *>(Element::large_map_list.Acquire())) {
		for (;;) {
			CfgUnit * cfgu = reinterpret_cast<CfgUnit *>(_buffers_net->GetLinkPtr(plink));

			if ((cfgu->large_runner) && (cfgu->gl_laridx > 0)) {
				TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, cfgu->gl_laridx);

				if (LargeCfg * l_cfg = reinterpret_cast<LargeCfg *>(cfgu->large_index.Acquire())) {
		
					for (unsigned int i(0);i<cfgu->large_runner;i++) {
						if (l_cfg[i].la_flags & 1) {
							la_obj.Set(e_pool, l_cfg[i].el_ref);

							if (Element::Header * la_hdr = reinterpret_cast<Element::Header *>(la_obj.Acquire())) {
								att_cfg = (la_hdr->config.att_flags & 0x00FF);
								for (unsigned int ai(AttributeArray::Vertex);att_cfg;att_cfg>>=1,ai++) {
									if (att_cfg & 1) {
										la_obj.InitAttributeArray(reinterpret_cast<AttributeArray::Type &>(ai), a_arr);

										if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
											Calc::FloatCopy16(temp_ma + 16*ai, a_hdr->local_transform, 1);
											Calc::MF_Multiply44(a_hdr->global_transform, 4, temp_ma + 16*ai, temp_ma + 16*ai);

											a_arr.Release();
										}
									}
								}

								ColorConvert(temp_ma + 16*AttributeArray::AttTypeCount, reinterpret_cast<int &>(la_hdr->span_cfg.x));
								ColorConvert(temp_ma + 16*AttributeArray::AttTypeCount + 4, reinterpret_cast<int &>(la_hdr->span_cfg.y));
								ColorConvert(temp_ma + 16*AttributeArray::AttTypeCount + 8, reinterpret_cast<int &>(la_hdr->span_cfg.x_max));
								ColorConvert(temp_ma + 16*AttributeArray::AttTypeCount + 12, reinterpret_cast<int &>(la_hdr->span_cfg.y_max));
								
								if (cfg_flags_location != -1) Uniform4iv(cfg_flags_location, 1, reinterpret_cast<int *>(l_cfg[i].cfg_vec));
								if (flat_location != -1) Uniform4fv(flat_location, 4, temp_ma + 16*AttributeArray::AttTypeCount);
								if (trans_location != -1) UniformMatrix4fv(trans_location, 4, 0, temp_ma);



								BindBuffer(GL_ARRAY_BUFFER, lm_ptr[2*la_hdr->x_tra + 1]);
								VertexAttribIPointer(0, 4, GL_INT, 0, 0);
							

								glDrawArrays(GL_POINTS, 0, la_hdr->xc_index);

								la_obj.Release();							
							}
						}
					}
					
					cfgu->large_index.Release();
				}
				
			}
					
			plink = _buffers_net->LeftLink(plink);
			if (plink == olink) break;
		}

		Element::large_map_list.Release();
	}

	TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, 0);

	return 0;
}
