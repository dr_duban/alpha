/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\OpenGL.h"

#include "OpenGL\Font.h"

#include "OGLResources.h"


#include "OGL_15.h"
#include "OGL_43.h"



OpenGL::Core::CursorPass::CursorPass() : coords_buf(0), cursor_xt(0) {

}

OpenGL::Core::CursorPass::~CursorPass() {
	
}



int OpenGL::Core::CursorPass::Initialize() {
	int result(0), iidx(0), s_size(0);
	unsigned int c_buf[4096], o_val(0);
	
	HMODULE libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME));

	GenBuffers(1, &coords_buf);

	if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 0*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 0*2 + 1];
		program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16998, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 1*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 1*2 + 1];
		program.GeometryShader.Load(col_ptr + o_val, s_size); // .FromResource(16999, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 2*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 2*2 + 1];
		program.FragmentShader.Load(col_ptr + o_val, s_size); // .FromResource(16700, libin);

		strings_collection.Release();
	}
		
	if (result = program.Build()) {
		local_cfg.location = GetUniformLocation(program.Name(), "local_config");
		local_cfg.value[0].i_cmp = max_width;
		local_cfg.value[1].i_cmp = max_height;

		ms_texture.location = GetUniformLocation(program.Name(), "ms_texture");
		ms_texture.value.i_val = ms_frame_unit;

		el_sampler.location = GetUniformLocation(program.Name(), "cursor_tex");
		el_sampler.value.i_val = cursor_list;


// load textures
		
		glGenTextures(1, &cursor_xt);		
		
		ActiveTexture(GL_TEXTURE0+cursor_list);
		glBindTexture(GL_TEXTURE_2D_ARRAY, cursor_xt);
		TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, 16, 16, 16, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
//		TexImage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA, 32, 32, 16, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);



		if (HRSRC tres = FindResource(libin, (LPCWSTR)17000, (LPCWSTR)CURSOR_PICTURE)) {
			if (unsigned int * src = reinterpret_cast<unsigned int *>(LockResource(LoadResource(libin, tres)))) {
				int rsize = SizeofResource(libin, tres);

				iidx = 0;
				for (unsigned int i(0);i<32;i++) {
					result = src[i+16];

					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
				}								

				TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, 16, 16, 1, GL_RGBA, GL_UNSIGNED_BYTE, c_buf);

				result = glGetError();
			} else {
				result = GetLastError();
			}
		} else {
			result = GetLastError();
		}

	/*	
		if (HRSRC tres = FindResource(libin, (LPCWSTR)17001, (LPCWSTR)CURSOR_PICTURE)) {
			if (unsigned int * src = reinterpret_cast<unsigned int *>(LockResource(LoadResource(libin, tres)))) {
				int rsize = SizeofResource(libin, tres);	

				iidx = 0;
				for (unsigned int i(0);i<128;i++) {
					result = src[i+16];

					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
					result >>= 8;
					c_buf[iidx++] = src[(result>>4) & 0x0F];
					c_buf[iidx++] = src[result & 0x0F];
				}								

				TexImage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA, 32, 32, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, c_buf);
			} else {
				result |= GetLastError();
			}
		} else {
			result |= GetLastError();
		}

	*/			



	} else {
		return 323231;
	}

	
	return result;
}


int OpenGL::Core::CursorPass::Render(int c_type, int ddest, int fhv) {
	int result(0);
	
	local_cfg.value[2].i_cmp = c_type;
	local_cfg.value[3].i_cmp = ddest | 0x00010000; // (PrimitivePass::detail_l << 16);

	coords[0] = _globalPX;
	coords[1] = fhv - _globalPY;
	coords[2] = 0.0;
	coords[3] = 0.0;

	coords[4] = _globalX;
	coords[5] = fhv - _globalY;
	coords[6] = 0.0;
	coords[7] = 1.0;

	Activate();

	glDisable(GL_DEPTH_TEST);

	BindBuffer(GL_ARRAY_BUFFER, coords_buf);
	BufferData(GL_ARRAY_BUFFER, 8*sizeof(float), coords, GL_DYNAMIC_DRAW);

	VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_POINTS, 0, 2);
		
//	result = glGetError();

	return result;
}

void OpenGL::Core::CursorPass::Destroy() {
	if (coords_buf) DeleteBuffers(1, &coords_buf);
	if (cursor_xt) glDeleteTextures(1, &cursor_xt);
}





