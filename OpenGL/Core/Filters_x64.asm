
; safe-fail image codecs
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;



EXTERN	?mag_filter@Core@OpenGL@@0PAFA:BYTE

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE



.CODE



ALIGN 16
?Average3_1@Core@OpenGL@@CAXPEAIPEBI1@Z	PROC
	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		MOV r9d, [r8] ; frame_w		
		MOV r12d, [r8+12] ; result_h
		MOV r10d, [r8+16] ; original_cw
		
		MOV eax, [r8 + 32] ; offset x
		SHR eax, 2
		SHL eax, 4
		
		LEA r15, [rdx + rax]
		
		XOR rdx, rdx
		MOV eax, [r8 + 36] ; offset y
		MUL r10

		LEA rdx, [r15 + rax*4] ; corner offset

		MOV eax, [r8+8] ; result_w

		SHL r10, 2
		SHL r9, 3

		PXOR xmm4, xmm4
		PXOR xmm6, xmm6
		PXOR xmm7, xmm7
		PXOR xmm8, xmm8
		PXOR xmm9, xmm9
		
	
		
		MOV rsi, rdx
		MOV r11, rcx
		MOV r14, rcx
		XOR r13, r13

		ADD eax, 7
		SHR eax, 3
		MOV r15d, eax
		
		SHR r12d, 1

	align 16
		line0_loop:

			MOVDQA xmm0, [rdx]
			MOVDQA xmm2, xmm0
				
			PUNPCKLBW xmm0, xmm7 ; ab
			PUNPCKHBW xmm2, xmm7 ; cd
					
			MOVDQA xmm1, xmm0
			PSRLDQ xmm1, 8
			MOVDQA xmm3, xmm2
			PSRLDQ xmm3, 8
					

			PADDW xmm8, xmm0
			PUNPCKLQDQ xmm4, xmm6
			PUNPCKLQDQ xmm9, xmm8

			MOVDQA [r11], xmm4
			MOVDQA [r11+16], xmm9

			ADD r11, 32


			PADDW xmm0, xmm1
			PADDW xmm0, xmm1
			PADDW xmm0, xmm2

			PADDW xmm1, xmm2
			PADDW xmm1, xmm2
			PADDW xmm1, xmm3






			MOVDQA xmm2, [rdx + 16]
			MOVDQA xmm4, xmm2
				
			PUNPCKLBW xmm2, xmm7 ; ef
			PUNPCKHBW xmm4, xmm7 ; gh

			MOVDQA xmm5, xmm2
			PSRLDQ xmm5, 8
			MOVDQA xmm6, xmm4
			PSRLDQ xmm6, 8
					
			PADDW xmm3, xmm2
			PADDW xmm3, xmm2
			PADDW xmm3, xmm4

			PADDW xmm2, xmm5
			PADDW xmm2, xmm5
			PADDW xmm2, xmm4


			PUNPCKLQDQ xmm0, xmm1
			PUNPCKLQDQ xmm3, xmm2


			PADDW xmm4, xmm6
			PADDW xmm4, xmm6




			MOVDQA xmm5, [rdx+32]					
			MOVDQA xmm8, xmm5
				
			PUNPCKLBW xmm5, xmm7 ; ij
			PUNPCKHBW xmm8, xmm7 ; kl

			MOVDQA xmm9, xmm5
			PSRLDQ xmm9, 8
			MOVDQA xmm10, xmm8
			PSRLDQ xmm10, 8
					
			PADDW xmm4, xmm5
			
			PADDW xmm6, xmm5
			PADDW xmm6, xmm5
			PADDW xmm6, xmm9



			PADDW xmm9, xmm8
			PADDW xmm9, xmm8
			PADDW xmm9, xmm10

			PADDW xmm8, xmm10
			PADDW xmm8, xmm10




			ADD rdx, 48


			MOVDQA [r11], xmm0
			MOVDQA [r11+16], xmm3

			ADD r11, 32


		DEC eax ; result_w
		JNZ line0_loop


		ADD rsi, r10
			
		
		
	align 16
		pic_loop:			
			
			PXOR xmm4, xmm4
			PXOR xmm9, xmm9

			MOV r11, rcx
			MOV rdx, rsi

			MOV eax, r15d
			



			align 16
				line1_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, xmm0
				
					PUNPCKLBW xmm0, xmm7 ; ab
					PUNPCKHBW xmm2, xmm7 ; cd
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA [r11+r9], xmm4
					MOVDQA [r11+r9+16], xmm9

					PSLLW xmm4, 1
					PSLLW xmm9, 1
					PADDW xmm4, [r11]
					PADDW xmm9, [r11+16]
					MOVDQA [r11], xmm4
					MOVDQA [r11+16], xmm9

					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx+16]
					MOVDQA xmm4, xmm2
				
					PUNPCKLBW xmm2, xmm7 ; ef
					PUNPCKHBW xmm4, xmm7 ; gh

					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx+32]					
					MOVDQA xmm8, xmm5
				
					PUNPCKLBW xmm5, xmm7 ; ij
					PUNPCKHBW xmm8, xmm7 ; kl

					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 48


					MOVDQA [r11+r9], xmm0
					MOVDQA [r11+r9+16], xmm3

					PSLLW xmm0, 1
					PSLLW xmm3, 1
					PADDW xmm0, [r11]
					PADDW xmm3, [r11+16]
					MOVDQA [r11], xmm0
					MOVDQA [r11+16], xmm3

					ADD r11, 32


				DEC eax ; result_w
				JNZ line1_loop

				ADD rsi, r10




				PXOR xmm4, xmm4
				PXOR xmm9, xmm9

				MOV r11, rcx

				MOV rdi, r14
				SHR r9, 1
				ADD r14, r9
				SHL r9, 1

				MOV rdx, rsi

				MOV eax, r15d
				XOR r13, r13


			align 16
				line2_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, xmm0
				
					PUNPCKLBW xmm0, xmm7 ; ab
					PUNPCKHBW xmm2, xmm7 ; cd
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA xmm6, xmm4
					MOVDQA xmm8, xmm9
					PSLLW xmm6, 1
					PSLLW xmm8, 1
					PADDW xmm6, [r11+r9]
					PADDW xmm8, [r11+r9+16]
					MOVDQA [r11+r9], xmm6
					MOVDQA [r11+r9+16], xmm8

					PADDW xmm4, [r11]
					PADDW xmm9, [r11+16]
				
					
				PSRLW xmm4, 4
				PSRLW xmm9, 4
				PACKUSWB xmm4, xmm9


				MOVNTDQ [rdi + r13], xmm4
				ADD rdi, 16
				MOV r13, -16

					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx+16]
					MOVDQA xmm4, xmm2
				
					PUNPCKLBW xmm2, xmm7 ; ef
					PUNPCKHBW xmm4, xmm7 ; gh

					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx+32]					
					MOVDQA xmm8, xmm5
				
					PUNPCKLBW xmm5, xmm7 ; ij
					PUNPCKHBW xmm8, xmm7 ; kl

					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 48

					MOVDQA xmm1, xmm0
					MOVDQA xmm2, xmm3
					PSLLW xmm1, 1
					PSLLW xmm2, 1

					PADDW xmm1, [r11+r9]
					PADDW xmm2, [r11+r9+16]
					MOVDQA [r11+r9], xmm1
					MOVDQA [r11+r9+16], xmm2

					PADDW xmm0, [r11]
					PADDW xmm3, [r11+16]
				
				PSRLW xmm0, 4
				PSRLW xmm3, 4
				PACKUSWB xmm0, xmm3

				MOVNTDQ [rdi + r13], xmm0
				ADD rdi, 16

					ADD r11, 32


				DEC eax ; result_w
				JNZ line2_loop






				ADD rsi, r10
				ADD rcx, r9

				PXOR xmm4, xmm4
				PXOR xmm9, xmm9



				MOV r11, rcx

				MOV rdi, r14
				SHR r9, 1
				ADD r14, r9
				SHL r9, 1

				MOV rdx, rsi

				MOV eax, r15d
				XOR r13, r13

			align 16
				line3_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, xmm0
				
					PUNPCKLBW xmm0, xmm7 ; ab
					PUNPCKHBW xmm2, xmm7 ; cd
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA [r11+r9], xmm4
					MOVDQA [r11+r9+16], xmm9

					PADDW xmm4, [r11]
					PADDW xmm9, [r11+16]

				PSRLW xmm4, 4
				PSRLW xmm9, 4
				PACKUSWB xmm4, xmm9

				MOVNTDQ [rdi + r13], xmm4
				ADD rdi, 16
				MOV r13, -16
					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx+16]
					MOVDQA xmm4, xmm2
				
					PUNPCKLBW xmm2, xmm7 ; ef
					PUNPCKHBW xmm4, xmm7 ; gh

					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx+32]					
					MOVDQA xmm8, xmm5
				
					PUNPCKLBW xmm5, xmm7 ; ij
					PUNPCKHBW xmm8, xmm7 ; kl

					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 48


					MOVDQA [r11+r9], xmm0
					MOVDQA [r11+r9+16], xmm3

					PADDW xmm0, [r11]
					PADDW xmm3, [r11+16]

				PSRLW xmm0, 4
				PSRLW xmm3, 4
				PACKUSWB xmm0, xmm3

				MOVNTDQ [rdi + r13], xmm0
				ADD rdi, 16

					ADD r11, 32


				DEC eax ; result_w
				JNZ line3_loop



				ADD rsi, r10


				ADD rcx, r9
				MOV r11, r14

				SUB r11, rcx

				MOV rax, r9
				SHR eax, 6

			align 16
				copy_some:
					MOVDQA xmm0, [rcx]
					MOVDQA xmm1, [rcx + 16]
					MOVDQA xmm2, [rcx + 32]
					MOVDQA xmm3, [rcx + 48]

					MOVDQA [rcx + r11], xmm0
					MOVDQA [rcx + r11 + 16], xmm1
					MOVDQA [rcx + r11 + 32], xmm2
					MOVDQA [rcx + r11 + 48], xmm3

					ADD rcx, 64
				DEC eax
				JNZ copy_some



				MOV rcx, r14




		DEC r12d ; result_h
		JNZ pic_loop

		SFENCE

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	RET
?Average3_1@Core@OpenGL@@CAXPEAIPEBI1@Z	ENDP

ALIGN 16
?Average_1@Core@OpenGL@@CAXPEAIPEBI1@Z	PROC
	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		MOV r9d, [r8] ; frame_w		
		MOV r12d, [r8+12] ; result_h
		MOV r10d, [r8+16] ; original_cw
		MOV r13d, [r8+24] ; av_val
		
		MOV eax, [r8 + 32]
		SHR eax, 2
		SHL eax, 4

		LEA r15, [rdx + rax]
		
		XOR rdx, rdx
		MOV eax, [r8 + 36]
		MUL r10

		LEA rdx, [r15 + rax*4]

		MOV eax, [r8 + 8] ; result_w

		SHL r10, 2
		SHL r9, 2

		PXOR xmm7, xmm7
		PXOR xmm8, xmm8
		PXOR xmm9, xmm9

		XOR r15d, r15d
		NOT r15d

		MOVD xmm12, r15d
		PSHUFD xmm12, xmm12, 11h
	
		MOV r15d, eax
		
		MOV rsi, rdx

		MOV rdx, 1
		XOR rax, rax
		DIV r13d

		XOR rdx, rdx
		DIV r13d

		MOVD xmm6, eax
		PSHUFD xmm6, xmm6, 44h

		MOVD xmm11, r13d
		PSHUFD xmm11, xmm11, 0
		PSRLD xmm11, 1
		
	align 16
		pic_loop:
			MOV r8d, r13d
			MOV eax, r15d

			ADD eax, 3
			SHR eax, 2

			MOV r11, rcx

			align 16
				clear_loop:
					MOVDQA [r11], xmm7
					MOVDQA [r11+16], xmm7
					MOVDQA [r11+32], xmm7
					MOVDQA [r11+48], xmm7

					ADD r11, 64
				DEC eax
				JNZ clear_loop

		align 16
			avh_loop:
				MOV r11, rcx
				MOV rdx, rsi

				MOV eax, r15d
				
				XOR r14, r14

			align 16
				line_loop:					
					ADD r14d, r13d
					JZ lp_offload
				align 16
					av4_loop:
						MOVDQA xmm0, [rdx]
						MOVDQA xmm2, xmm0
				
						PUNPCKLBW xmm0, xmm7
						PUNPCKHBW xmm2, xmm7
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2

						PUNPCKLWD xmm0, xmm7
						PUNPCKHWD xmm1, xmm7
						PUNPCKLWD xmm2, xmm7
						PUNPCKHWD xmm3, xmm7

						ADD rdx, 16

						CMP r14d, 4
						JBE less_than_4

							PADDD xmm8, xmm0
							PADDD xmm8, xmm1
							PADDD xmm8, xmm2
							PADDD xmm8, xmm3
				
						SUB r14d, 4
						JMP av4_loop

				align 16
					less_than_4:
						PADDD xmm8, xmm0
						PADDD xmm8, xmm1
						PADDD xmm8, xmm2
						PADDD xmm8, xmm3

						DEC r14d
					
						PADDD xmm9, xmm1

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10

						PADDD xmm9, xmm2

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10

						PADDD xmm9, xmm3

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10


						PSUBD xmm8, xmm9


				align 16
					lp_offload:		

						PADDD xmm8, [r11]
						MOVDQA [r11], xmm8

						MOVDQA xmm8, xmm9

						PXOR xmm9, xmm9

						ADD r11, 16


				DEC eax ; result_w
				JNZ line_loop

				ADD rsi, r10

			DEC r8d
			JNZ avh_loop


	
	
	
	
			XOR r11, r11
			MOV eax, r15d
			ADD eax, 3
			SHR eax, 2

		align 16
			normalize_loop:					
				MOVDQA xmm0, [rcx + r11*4]
				PADDD xmm0, xmm11
				MOVDQA xmm1, xmm0

				PSRLDQ xmm1, 4				

				PMULUDQ xmm0, xmm6
				PMULUDQ xmm1, xmm6

				PSRLQ xmm0, 32
				PAND xmm1, xmm12
				POR xmm0, xmm1


				MOVDQA xmm1, [rcx + r11*4 + 16]
				PADDD xmm1, xmm11
				MOVDQA xmm2, xmm1

				PSRLDQ xmm2, 4

				PMULUDQ xmm1, xmm6
				PMULUDQ xmm2, xmm6

				PSRLQ xmm1, 32
				PAND xmm2, xmm12
				POR xmm1, xmm2

				MOVDQA xmm2, [rcx + r11*4 + 32]
				PADDD xmm2, xmm11
				MOVDQA xmm3, xmm2

				PSRLDQ xmm3, 4

				PMULUDQ xmm2, xmm6
				PMULUDQ xmm3, xmm6

				PSRLQ xmm2, 32
				PAND xmm3, xmm12
				POR xmm2, xmm3

				MOVDQA xmm3, [rcx + r11*4 + 48]
				PADDD xmm3, xmm11
				MOVDQA xmm4, xmm3

				PSRLDQ xmm4, 4

				PMULUDQ xmm3, xmm6
				PMULUDQ xmm4, xmm6

				PSRLQ xmm3, 32
				PAND xmm4, xmm12
				POR xmm3, xmm4

				PACKSSDW xmm0, xmm1
				PACKSSDW xmm2, xmm3

				PACKUSWB xmm0, xmm2

				MOVNTDQ [rcx+r11], xmm0

				ADD r11, 16

			DEC eax ; result_w
			JNZ normalize_loop



			ADD rcx, r9

		DEC r12d ; result_h
		JNZ pic_loop

	SFENCE


	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	RET
?Average_1@Core@OpenGL@@CAXPEAIPEBI1@Z	ENDP



ALIGN 16
?SubImage_1@Core@OpenGL@@CAXPEAIPEBI1@Z	PROC
	
	MOV r11d, [r8+12] ; result_h
	MOV r10d, [r8+16] ; original_cw

		
	MOV eax, [r8 + 32]
	SHR eax, 2
	SHL eax, 4

	LEA r9, [rdx + rax]
		
	XOR rdx, rdx
	MOV eax, [r8 + 36]
	MUL r10

	LEA rdx, [r9 + rax*4]

	MOV eax, [r8] ; result_w
	SHR eax, 4

	SHL r10, 2
	
	MOV r9d, eax		
	MOV r8, rdx

		
align 16
	pic_loop:
			MOVDQA xmm0, [rdx]
			MOVDQA xmm1, [rdx + 16]
			MOVDQA xmm2, [rdx + 32]
			MOVDQA xmm3, [rdx + 48]

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx + 16], xmm1
			MOVNTDQ [rcx + 32], xmm2
			MOVNTDQ [rcx + 48], xmm3


			ADD rcx, 64
			ADD rdx, 64

		DEC eax
		JNZ pic_loop

			ADD r8, r10
			MOV rdx, r8

			MOV eax, r9d
	DEC r11d
	JNZ pic_loop

	SFENCE

	RET
?SubImage_1@Core@OpenGL@@CAXPEAIPEBI1@Z	ENDP


ALIGN 16
?SubImage_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	PROC
	
	MOV r11d, [r8+12] ; result_h
	MOV r10d, [r8+16] ; original_cw

		
	MOV eax, [r8 + 32]
	SHR eax, 2
	SHL eax, 5

	LEA r9, [rdx + rax]
		
	XOR rdx, rdx
	MOV eax, [r8 + 36]
	MUL r10

	LEA rdx, [r9 + rax*8]

	MOV eax, [r8] ; result_w
	SHR eax, 4

	SHL r10, 3
	
	MOV r9d, eax		
	MOV r8, rdx

		
align 16
	pic_loop:
			MOVDQA xmm0, [rdx]
			PSRLW xmm0, 8
			MOVDQA xmm1, [rdx + 16]
			PSRLW xmm1, 8
			MOVDQA xmm2, [rdx + 32]
			PSRLW xmm2, 8
			MOVDQA xmm3, [rdx + 48]
			PSRLW xmm3, 8
			MOVDQA xmm4, [rdx + 64]
			PSRLW xmm4, 8
			MOVDQA xmm5, [rdx + 80]
			PSRLW xmm5, 8
			MOVDQA xmm6, [rdx + 96]
			PSRLW xmm6, 8
			MOVDQA xmm7, [rdx + 112]
			PSRLW xmm7, 8

			PACKUSWB xmm0, xmm1
			PACKUSWB xmm2, xmm3
			PACKUSWB xmm4, xmm5
			PACKUSWB xmm6, xmm7

			MOVNTDQ [rcx], xmm0
			MOVNTDQ [rcx + 16], xmm2
			MOVNTDQ [rcx + 32], xmm4
			MOVNTDQ [rcx + 48], xmm6

			ADD rcx, 64
			ADD rdx, 128

		DEC eax
		JNZ pic_loop

			ADD r8, r10
			MOV rdx, r8

			MOV eax, r9d
	DEC r11d
	JNZ pic_loop

	SFENCE

	RET
?SubImage_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	ENDP



ALIGN 16
?Average3_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	PROC
	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		MOV r9d, [r8] ; frame_w		
		MOV r12d, [r8 + 12] ; result_h
		MOV r10d, [r8 + 16] ; original_cw
		
		MOV eax, [r8 + 32] ; offset x
		SHR eax, 2
		SHL eax, 5
		
		LEA r15, [rdx + rax]
		
		XOR rdx, rdx
		MOV eax, [r8 + 36] ; offset y
		MUL r10

		LEA rdx, [r15 + rax*8] ; corner offset

		MOV eax, [r8 + 8] ; result_w

		SHL r10, 3
		SHL r9, 3

		PXOR xmm4, xmm4
		PXOR xmm6, xmm6
		PXOR xmm7, xmm7
		PXOR xmm8, xmm8
		PXOR xmm9, xmm9
		
	
		
		MOV rsi, rdx
		MOV r11, rcx
		MOV r14, rcx
		XOR r13, r13

		ADD eax, 7
		SHR eax, 3
		MOV r15d, eax
		
		SHR r12d, 1

	align 16
		line0_loop:

			MOVDQA xmm0, [rdx]
			MOVDQA xmm2, [rdx + 16]
				
			PSRLW xmm0, 4
			PSRLW xmm2, 4

					
			MOVDQA xmm1, xmm0
			PSRLDQ xmm1, 8
			MOVDQA xmm3, xmm2
			PSRLDQ xmm3, 8
					

			PADDW xmm8, xmm0
			PUNPCKLQDQ xmm4, xmm6
			PUNPCKLQDQ xmm9, xmm8

			MOVDQA [r11], xmm4
			MOVDQA [r11+16], xmm9

			ADD r11, 32


			PADDW xmm0, xmm1
			PADDW xmm0, xmm1
			PADDW xmm0, xmm2

			PADDW xmm1, xmm2
			PADDW xmm1, xmm2
			PADDW xmm1, xmm3






			MOVDQA xmm2, [rdx + 32]
			MOVDQA xmm4, [rdx + 48]
				
			PSRLW xmm2, 4
			PSRLW xmm4, 4


			MOVDQA xmm5, xmm2
			PSRLDQ xmm5, 8
			MOVDQA xmm6, xmm4
			PSRLDQ xmm6, 8
					
			PADDW xmm3, xmm2
			PADDW xmm3, xmm2
			PADDW xmm3, xmm4

			PADDW xmm2, xmm5
			PADDW xmm2, xmm5
			PADDW xmm2, xmm4


			PUNPCKLQDQ xmm0, xmm1
			PUNPCKLQDQ xmm3, xmm2


			PADDW xmm4, xmm6
			PADDW xmm4, xmm6




			MOVDQA xmm5, [rdx + 64]					
			MOVDQA xmm8, [rdx + 80]
				
			PSRLW xmm5, 4
			PSRLW xmm8, 4


			MOVDQA xmm9, xmm5
			PSRLDQ xmm9, 8
			MOVDQA xmm10, xmm8
			PSRLDQ xmm10, 8
					
			PADDW xmm4, xmm5
			
			PADDW xmm6, xmm5
			PADDW xmm6, xmm5
			PADDW xmm6, xmm9



			PADDW xmm9, xmm8
			PADDW xmm9, xmm8
			PADDW xmm9, xmm10

			PADDW xmm8, xmm10
			PADDW xmm8, xmm10




			ADD rdx, 96


			MOVDQA [r11], xmm0
			MOVDQA [r11+16], xmm3

			ADD r11, 32


		DEC eax ; result_w
		JNZ line0_loop


		ADD rsi, r10
			
		
		
	align 16
		pic_loop:			
			
			PXOR xmm4, xmm4
			PXOR xmm9, xmm9

			MOV r11, rcx
			MOV rdx, rsi

			MOV eax, r15d
			



			align 16
				line1_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, [rdx + 16]

					PSRLW xmm0, 4
					PSRLW xmm2, 4
				
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA [r11 + r9], xmm4
					MOVDQA [r11 + r9 + 16], xmm9

					PSLLW xmm4, 1
					PSLLW xmm9, 1
					PADDW xmm4, [r11]
					PADDW xmm9, [r11 + 16]
					MOVDQA [r11], xmm4
					MOVDQA [r11 + 16], xmm9

					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx + 32]
					MOVDQA xmm4, [rdx + 48]
				
					PSRLW xmm2, 4
					PSRLW xmm4, 4


					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx + 64]					
					MOVDQA xmm8, [rdx + 80]
				
					PSRLW xmm5, 4
					PSRLW xmm8, 4


					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 96


					MOVDQA [r11 + r9], xmm0
					MOVDQA [r11 + r9 + 16], xmm3

					PSLLW xmm0, 1
					PSLLW xmm3, 1
					PADDW xmm0, [r11]
					PADDW xmm3, [r11 + 16]
					MOVDQA [r11], xmm0
					MOVDQA [r11 + 16], xmm3

					ADD r11, 32


				DEC eax ; result_w
				JNZ line1_loop

				ADD rsi, r10




				PXOR xmm4, xmm4
				PXOR xmm9, xmm9

				MOV r11, rcx

				MOV rdi, r14
				SHR r9, 1
				ADD r14, r9
				SHL r9, 1

				MOV rdx, rsi

				MOV eax, r15d
				XOR r13, r13


			align 16
				line2_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, [rdx + 16]
			
					PSRLW xmm0, 4
					PSRLW xmm2, 4
				
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA xmm6, xmm4
					MOVDQA xmm8, xmm9
					PSLLW xmm6, 1
					PSLLW xmm8, 1
					PADDW xmm6, [r11 + r9]
					PADDW xmm8, [r11 + r9 + 16]
					MOVDQA [r11 + r9], xmm6
					MOVDQA [r11 + r9 + 16], xmm8

					PADDW xmm4, [r11]
					PADDW xmm9, [r11 + 16]
				
					
				PSRLW xmm4, 8
				PSRLW xmm9, 8
				PACKUSWB xmm4, xmm9


				MOVNTDQ [rdi + r13], xmm4
				ADD rdi, 16
				MOV r13, -16

					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx + 32]
					MOVDQA xmm4, [rdx + 48]
				
					PSRLW xmm2, 4
					PSRLW xmm4, 4


					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx + 64]					
					MOVDQA xmm8, [rdx + 80]
				
					PSRLW xmm5, 4
					PSRLW xmm8, 4


					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 96

					MOVDQA xmm1, xmm0
					MOVDQA xmm2, xmm3
					PSLLW xmm1, 1
					PSLLW xmm2, 1

					PADDW xmm1, [r11 + r9]
					PADDW xmm2, [r11 + r9 + 16]
					MOVDQA [r11 + r9], xmm1
					MOVDQA [r11 + r9 + 16], xmm2

					PADDW xmm0, [r11]
					PADDW xmm3, [r11 + 16]
				
				PSRLW xmm0, 8
				PSRLW xmm3, 8
				PACKUSWB xmm0, xmm3

				MOVNTDQ [rdi + r13], xmm0
				ADD rdi, 16

					ADD r11, 32


				DEC eax ; result_w
				JNZ line2_loop






				ADD rsi, r10
				ADD rcx, r9

				PXOR xmm4, xmm4
				PXOR xmm9, xmm9



				MOV r11, rcx

				MOV rdi, r14
				SHR r9, 1
				ADD r14, r9
				SHL r9, 1

				MOV rdx, rsi

				MOV eax, r15d
				XOR r13, r13

			align 16
				line3_loop:


					MOVDQA xmm0, [rdx]
					MOVDQA xmm2, [rdx + 16]
			
					PSRLW xmm0, 4
					PSRLW xmm2, 4
				
					
					MOVDQA xmm1, xmm0
					PSRLDQ xmm1, 8
					MOVDQA xmm3, xmm2
					PSRLDQ xmm3, 8
					

					PADDW xmm8, xmm0
					PUNPCKLQDQ xmm4, xmm6
					PUNPCKLQDQ xmm9, xmm8

					MOVDQA [r11 + r9], xmm4
					MOVDQA [r11 + r9 + 16], xmm9

					PADDW xmm4, [r11]
					PADDW xmm9, [r11 + 16]

				PSRLW xmm4, 8
				PSRLW xmm9, 8
				PACKUSWB xmm4, xmm9

				MOVNTDQ [rdi + r13], xmm4
				ADD rdi, 16
				MOV r13, -16
					ADD r11, 32


					PADDW xmm0, xmm1
					PADDW xmm0, xmm1
					PADDW xmm0, xmm2

					PADDW xmm1, xmm2
					PADDW xmm1, xmm2
					PADDW xmm1, xmm3






					MOVDQA xmm2, [rdx + 32]
					MOVDQA xmm4, [rdx + 48]
				
					PSRLW xmm2, 4
					PSRLW xmm4, 4


					MOVDQA xmm5, xmm2
					PSRLDQ xmm5, 8
					MOVDQA xmm6, xmm4
					PSRLDQ xmm6, 8
					
					PADDW xmm3, xmm2
					PADDW xmm3, xmm2
					PADDW xmm3, xmm4

					PADDW xmm2, xmm5
					PADDW xmm2, xmm5
					PADDW xmm2, xmm4


					PUNPCKLQDQ xmm0, xmm1
					PUNPCKLQDQ xmm3, xmm2


					PADDW xmm4, xmm6
					PADDW xmm4, xmm6


					MOVDQA xmm5, [rdx + 64]					
					MOVDQA xmm8, [rdx + 80]
				
					PSRLW xmm5, 4
					PSRLW xmm8, 4


					MOVDQA xmm9, xmm5
					PSRLDQ xmm9, 8
					MOVDQA xmm10, xmm8
					PSRLDQ xmm10, 8
					
					PADDW xmm4, xmm5

					PADDW xmm6, xmm5
					PADDW xmm6, xmm5
					PADDW xmm6, xmm9



					PADDW xmm9, xmm8
					PADDW xmm9, xmm8
					PADDW xmm9, xmm10

					PADDW xmm8, xmm10
					PADDW xmm8, xmm10




					ADD rdx, 96


					MOVDQA [r11 + r9], xmm0
					MOVDQA [r11 + r9 + 16], xmm3

					PADDW xmm0, [r11]
					PADDW xmm3, [r11 + 16]

				PSRLW xmm0, 8
				PSRLW xmm3, 8
				PACKUSWB xmm0, xmm3

				MOVNTDQ [rdi + r13], xmm0
				ADD rdi, 16

					ADD r11, 32


				DEC eax ; result_w
				JNZ line3_loop



				ADD rsi, r10


				ADD rcx, r9
				MOV r11, r14

				SUB r11, rcx

				MOV rax, r9
				SHR rax, 6

			align 16
				copy_some:
					MOVDQA xmm0, [rcx]
					MOVDQA xmm1, [rcx + 16]
					MOVDQA xmm2, [rcx + 32]
					MOVDQA xmm3, [rcx + 48]

					MOVDQA [rcx + r11], xmm0
					MOVDQA [rcx + r11 + 16], xmm1
					MOVDQA [rcx + r11 + 32], xmm2
					MOVDQA [rcx + r11 + 48], xmm3

					ADD rcx, 64
				DEC eax
				JNZ copy_some



				MOV rcx, r14




		DEC r12d ; result_h
		JNZ pic_loop

		SFENCE

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi


	RET
?Average3_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	ENDP

ALIGN 16
?Average_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	PROC
	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		MOV r9d, [r8] ; frame_w
		
		MOV r12d, [r8+12] ; result_h
		MOV r10d, [r8+16] ; original_cw
		MOV r13d, [r8+24] ; av_val
		
		MOV eax, [r8 + 32]
		SHR eax, 2
		SHL eax, 5
		LEA r15, [rdx + rax]

		XOR rdx, rdx
		MOV eax, [r8 + 36]
		MUL r10
		LEA rdx, [r15 + rax*8]


		MOV eax, [r8+8] ; result_w

		SHL r10, 3
		SHL r9, 2

		PXOR xmm7, xmm7
		PXOR xmm8, xmm8
		PXOR xmm9, xmm9

		XOR r15d, r15d
		NOT r15d

		MOVD xmm12, r15d
		PSHUFD xmm12, xmm12, 11h
	
		MOV r15d, eax
		
		MOV rsi, rdx

		XOR rdx, rdx
		MOV eax, 01000000h
		DIV r13d

		XOR rdx, rdx
		DIV r13d

		MOVD xmm6, eax
		PSHUFD xmm6, xmm6, 44h

		
	align 16
		pic_loop:
			MOV r8d, r13d
			MOV eax, r15d

			ADD eax, 3
			SHR eax, 2

			MOV r11, rcx

			align 16
				clear_loop:
					MOVDQA [r11], xmm7
					MOVDQA [r11+16], xmm7
					MOVDQA [r11+32], xmm7
					MOVDQA [r11+48], xmm7

					ADD r11, 64
				DEC eax
				JNZ clear_loop

		align 16
			avh_loop:
				MOV r11, rcx
				MOV rdx, rsi

				MOV eax, r15d
				
				XOR r14, r14

			align 16
				line_loop:					
					ADD r14d, r13d
					JZ lp_offload
				align 16
					av4_loop:
						MOVDQA xmm0, [rdx]
						MOVDQA xmm2, [rdx+16]
				
						MOVDQA xmm1, xmm0
						MOVDQA xmm3, xmm2

						PUNPCKLWD xmm0, xmm7
						PUNPCKHWD xmm1, xmm7
						PUNPCKLWD xmm2, xmm7
						PUNPCKHWD xmm3, xmm7

						ADD rdx, 32

						CMP r14d, 4
						JBE less_than_4

							PADDD xmm8, xmm0
							PADDD xmm8, xmm1
							PADDD xmm8, xmm2
							PADDD xmm8, xmm3
				
						SUB r14d, 4
						JMP av4_loop

				align 16
					less_than_4:
						PADDD xmm8, xmm0
						PADDD xmm8, xmm1
						PADDD xmm8, xmm2
						PADDD xmm8, xmm3

						DEC r14d
					
						PADDD xmm9, xmm1

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10

						PADDD xmm9, xmm2

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10

						PADDD xmm9, xmm3

						DEC r14d
						MOVD xmm10, r14d
						PSHUFD xmm10, xmm10, 0
						PSRAD xmm10, 31
						PAND xmm9, xmm10


						PSUBD xmm8, xmm9


				align 16
					lp_offload:		

						PADDD xmm8, [r11]
						MOVDQA [r11], xmm8

						MOVDQA xmm8, xmm9

						PXOR xmm9, xmm9

						ADD r11, 16


				DEC eax ; result_w
				JNZ line_loop

				ADD rsi, r10

			DEC r8d
			JNZ avh_loop


	
	
	
	
			XOR r11, r11
			MOV eax, r15d
			ADD eax, 3
			SHR eax, 2

		align 16
			normalize_loop:					
				MOVDQA xmm0, [rcx + r11*4]
				MOVDQA xmm1, xmm0

				PSRLDQ xmm1, 4				

				PMULUDQ xmm0, xmm6
				PMULUDQ xmm1, xmm6

				PSRLQ xmm0, 32
				PAND xmm1, xmm12
				POR xmm0, xmm1


				MOVDQA xmm1, [rcx + r11*4 + 16]
				MOVDQA xmm2, xmm1

				PSRLDQ xmm2, 4

				PMULUDQ xmm1, xmm6
				PMULUDQ xmm2, xmm6

				PSRLQ xmm1, 32
				PAND xmm2, xmm12
				POR xmm1, xmm2

				MOVDQA xmm2, [rcx + r11*4 + 32]
				MOVDQA xmm3, xmm2

				PSRLDQ xmm3, 4

				PMULUDQ xmm2, xmm6
				PMULUDQ xmm3, xmm6

				PSRLQ xmm2, 32
				PAND xmm3, xmm12
				POR xmm2, xmm3

				MOVDQA xmm3, [rcx + r11*4 + 48]
				MOVDQA xmm4, xmm3

				PSRLDQ xmm4, 4

				PMULUDQ xmm3, xmm6
				PMULUDQ xmm4, xmm6

				PSRLQ xmm3, 32
				PAND xmm4, xmm12
				POR xmm3, xmm4

				PACKSSDW xmm0, xmm1
				PACKSSDW xmm2, xmm3

				PACKUSWB xmm0, xmm2

				MOVNTDQ [rcx+r11], xmm0

				ADD r11, 16

			DEC eax ; result_w
			JNZ normalize_loop



			ADD rcx, r9

		DEC r12d ; result_h
		JNZ pic_loop

	SFENCE


	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	RET
?Average_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	ENDP


ALIGN 16
?Magnify_1@Core@OpenGL@@CAXPEAIPEBI1@Z	PROC
	PUSH rbx

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		LEA rbx, [?mag_filter@Core@OpenGL@@0PAFA]

		MOV rdi, rcx

		MOV r9d, [r8] ; frame_w		
		MOV ecx, [r8+12] ; result_h
		MOV r10d, [r8+16] ; original_cw
		MOV r13d, [r8+24] ; av_val
		
		

		MOV eax, [r8 + 32]
		LEA r15, [rdx + rax*4]		

		MOV edx, 2
		XOR eax, eax
		DIV r13d
		
		XOR r12, r12
		XOR r13, r13

		MOV r14d, eax
		
		

		XOR rdx, rdx
		MOV eax, [r8 + 36]
		MUL r10

		LEA rdx, [r15 + rax*4]
		MOV rsi, rdx

		SHL r10, 2
		LEA r11, [r10 + r10*2]

		MOV eax, r9d
		
				
		PXOR xmm4, xmm4
		PXOR xmm5, xmm5
		PXOR xmm6, xmm6
		PXOR xmm7, xmm7

		PXOR xmm15, xmm15

	align 16
		pic_loop:
			PSRLDQ xmm4, 4
			MOVD xmm8, DWORD PTR [rdx]
			PSLLDQ xmm8, 12
			POR xmm4, xmm8
			
			PSRLDQ xmm5, 4
			MOVD xmm8, DWORD PTR [rdx + r10]
			PSLLDQ xmm8, 12
			POR xmm5, xmm8

			PSRLDQ xmm6, 4
			MOVD xmm8, DWORD PTR [rdx + r10*2]
			PSLLDQ xmm8, 12
			POR xmm6, xmm8

			PSRLDQ xmm7, 4
			MOVD xmm8, DWORD PTR [rdx + r11]
			PSLLDQ xmm8, 12
			POR xmm7, xmm8



			MOVDQA xmm0, xmm4
			MOVDQA xmm2, xmm4
			
			PUNPCKLBW xmm0, xmm5
			PUNPCKHBW xmm2, xmm5
			
			MOVDQA xmm8, xmm6
			MOVDQA xmm9, xmm6

			PUNPCKLBW xmm8, xmm7
			PUNPCKHBW xmm9, xmm7
			
			MOVDQA xmm1, xmm0
			MOVDQA xmm3, xmm2

			PUNPCKLWD xmm0, xmm8
			PUNPCKHWD xmm1, xmm8
			PUNPCKLWD xmm2, xmm9
			PUNPCKHWD xmm3, xmm9

			MOV r8d, r13d
			SHR r8d, 26
			SHL r8, 11

		align 16
			pixel_loop:
				
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm0
				MOVDQA xmm10, xmm1
				MOVDQA xmm11, xmm1

				PUNPCKLBW xmm8, xmm15
				PUNPCKHBW xmm9, xmm15
				PUNPCKLBW xmm10, xmm15
				PUNPCKHBW xmm11, xmm15

				
				MOV r15d, r12d
				SHR r15d, 26
				SHL r15, 5

				ADD r15, r8


				PSHUFD xmm12, [rbx + r15], 044h
				PSHUFD xmm13, [rbx + r15], 0EEh


				PMADDWD xmm8, xmm12
				PMADDWD xmm9, xmm12
				PMADDWD xmm10, xmm13
				PMADDWD xmm11, xmm13
			
				PHADDD xmm8, xmm9
				PHADDD xmm10, xmm11

				PADDD xmm8, xmm10


				MOVDQA xmm9, xmm2
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				MOVDQA xmm12, xmm3

				PUNPCKLBW xmm9, xmm15
				PUNPCKHBW xmm10, xmm15
				PUNPCKLBW xmm11, xmm15
				PUNPCKHBW xmm12, xmm15


				PSHUFD xmm13, [rbx + r15 + 16], 044h
				PSHUFD xmm14, [rbx + r15 + 16], 0EEh


				PMADDWD xmm9, xmm13
				PMADDWD xmm10, xmm13
				PMADDWD xmm11, xmm14
				PMADDWD xmm12, xmm14
			
				PHADDD xmm9, xmm10
				PHADDD xmm11, xmm12

				PADDD xmm9, xmm11

				PADDD xmm8, xmm9

				PSRAD xmm8, 15

				PACKSSDW xmm8, xmm8
				PACKUSWB xmm8, xmm8

				MOVD DWORD PTR [rdi], xmm8
				ADD rdi, 4

				DEC eax
				JZ next_line

			ADD r12d, r14d
			JNC pixel_loop


			ADD rdx, 4
			JMP pic_loop
	align 16
		next_line:
			DEC ecx
			JZ exit
				PXOR xmm4, xmm4
				PXOR xmm5, xmm5
				PXOR xmm6, xmm6
				PXOR xmm7, xmm7

				MOV eax, r9d
				XOR r12, r12
				MOV rdx, rsi

			ADD r13d, r14d
			JNC pic_loop
			
			
			ADD rsi, r10
			MOV rdx, rsi
					
		JMP pic_loop
	align 16
		exit:

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	POP rbx

	RET
?Magnify_1@Core@OpenGL@@CAXPEAIPEBI1@Z	ENDP


ALIGN 16
?Magnify_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	PROC
	PUSH rbx

	PUSH rsi
	PUSH rdi

	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15
			
		LEA rbx, [?mag_filter@Core@OpenGL@@0PAFA]

		MOV rdi, rcx

		MOV r9d, [r8] ; frame_w		
		MOV ecx, [r8+12] ; result_h
		MOV r10d, [r8+16] ; original_cw
		MOV r13d, [r8+24] ; av_val
		
		

		MOV eax, [r8 + 32]
		LEA r15, [rdx + rax*8]		

		MOV edx, 2
		XOR eax, eax
		DIV r13d
		
		XOR r12, r12
		XOR r13, r13

		MOV r14d, eax
		
		

		XOR rdx, rdx
		MOV eax, [r8 + 36]
		MUL r10

		LEA rdx, [r15 + rax*8]
		MOV rsi, rdx

		SHL r10, 3
		LEA r11, [r10 + r10*2]

		MOV eax, r9d
		
		PXOR xmm0, xmm0
		PXOR xmm1, xmm1
		PXOR xmm2, xmm2
		PXOR xmm3, xmm3		
		PXOR xmm4, xmm4
		PXOR xmm5, xmm5
		PXOR xmm6, xmm6
		PXOR xmm7, xmm7

		

	align 16
		pic_loop:
			MOVD xmm6, QWORD PTR [rdx]
			MOVD xmm8, QWORD PTR [rdx + r10]
			PUNPCKLWD xmm6, xmm8
			MOVD xmm9, QWORD PTR [rdx + r10*2]
			MOVD xmm10, QWORD PTR [rdx + r11]
			PUNPCKLWD xmm9, xmm10
			MOVDQA xmm7, xmm6
			PUNPCKLDQ xmm6, xmm9
			PUNPCKHDQ xmm7, xmm9

			PSRLW xmm6, 1
			PSRLW xmm7, 1

			MOV r8d, r13d
			SHR r8d, 26
			SHL r8, 11

		align 16
			pixel_loop:
				
				MOVDQA xmm8, xmm0
				MOVDQA xmm9, xmm1
				MOVDQA xmm10, xmm2
				MOVDQA xmm11, xmm3
				
				
				MOV r15d, r12d
				SHR r15d, 26
				SHL r15, 5

				ADD r15, r8


				PSHUFD xmm12, [rbx + r15], 044h
				PSHUFD xmm13, [rbx + r15], 0EEh


				PMADDWD xmm8, xmm12
				PMADDWD xmm9, xmm12
				PMADDWD xmm10, xmm13
				PMADDWD xmm11, xmm13
			
				PHADDD xmm8, xmm9
				PHADDD xmm10, xmm11

				PADDD xmm8, xmm10


				MOVDQA xmm9, xmm4
				MOVDQA xmm10, xmm5
				MOVDQA xmm11, xmm6
				MOVDQA xmm12, xmm7

				PSHUFD xmm13, [rbx + r15 + 16], 044h
				PSHUFD xmm14, [rbx + r15 + 16], 0EEh


				PMADDWD xmm9, xmm13
				PMADDWD xmm10, xmm13
				PMADDWD xmm11, xmm14
				PMADDWD xmm12, xmm14
			
				PHADDD xmm9, xmm10
				PHADDD xmm11, xmm12

				PADDD xmm9, xmm11

				PADDD xmm8, xmm9

				PSRAD xmm8, 22

				PACKSSDW xmm8, xmm8
				PACKUSWB xmm8, xmm8
				
				MOVD DWORD PTR [rdi], xmm8
				ADD rdi, 4

				DEC eax
				JZ next_line

			ADD r12d, r14d
			JNC pixel_loop

			MOVDQA xmm0, xmm2
			MOVDQA xmm1, xmm3
			MOVDQA xmm2, xmm4
			MOVDQA xmm3, xmm5
			MOVDQA xmm4, xmm6
			MOVDQA xmm5, xmm7

			ADD rdx, 8
			JMP pic_loop
	align 16
		next_line:
			DEC ecx
			JZ exit
				PXOR xmm0, xmm0
				PXOR xmm1, xmm1
				PXOR xmm2, xmm2
				PXOR xmm3, xmm3
				PXOR xmm4, xmm4
				PXOR xmm5, xmm5
				PXOR xmm6, xmm6
				PXOR xmm7, xmm7

				MOV eax, r9d
				XOR r12, r12
				MOV rdx, rsi

			ADD r13d, r14d
			JNC pic_loop
			
			
			ADD rsi, r10
			MOV rdx, rsi
					
		JMP pic_loop
	align 16
		exit:

	POP r15
	POP r14
	POP r13
	POP r12

	POP rdi
	POP rsi

	POP rbx



	RET
?Magnify_2@Core@OpenGL@@CAXPEAIPEB_KPEBI@Z	ENDP



END

