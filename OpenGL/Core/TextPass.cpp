#include "OpenGL\OpenGL.h"

#include "OpenGL\Font.h"

#include "OGL_15.h"
#include "OGL_43.h"


// text pass ==============================================================================================================================================================================================================================

OpenGL::Core::TextPass::TextPass() : tex_buf(0), detail_l(0) {

}

OpenGL::Core::TextPass::~TextPass() {

}



int OpenGL::Core::TextPass::Initialize() {
	int result(0), s_size(0);
	unsigned int o_val(0);

	if (char * col_ptr = reinterpret_cast<char *>(strings_collection.Acquire())) {
		
		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 11*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 11*2 + 1];
		t_renderer.program.VertexShader.Load(col_ptr + o_val, s_size); // FromResource(16501, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 12*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 12*2 + 1];
		t_renderer.program.GeometryShader.Load(col_ptr + o_val, s_size); // FromResource(16502, libin);

		o_val = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 13*2];
		s_size = reinterpret_cast<unsigned int *>(col_ptr)[3840 + 13*2 + 1];
		t_renderer.program.FragmentShader.Load(col_ptr + o_val, s_size); // FromResource(16503, libin);

		strings_collection.Release();
	}
		
	if (result = t_renderer.program.Build()) {
		t_renderer.local_cfg.location = GetUniformLocation(t_renderer.program.Name(), "local_config");

	//	t_renderer.cfg_flags_location = GetUniformLocation(t_renderer.program.Name(), "config_flags");
		
		t_renderer.cfg_offs_location = GetUniformLocation(t_renderer.program.Name(), "config_offs");
		
		t_renderer.attbutes.location = GetUniformLocation(t_renderer.program.Name(), "attbutes");
		t_renderer.attbutes.value.i_val = attribute_unit;
		
		t_renderer.ms_texture.location = GetUniformLocation(t_renderer.program.Name(), "ms_texture");
		t_renderer.ms_texture.value.i_val = ms_frame_unit;

		ms_glyph.location = GetUniformLocation(t_renderer.program.Name(), "ms_font");
		ms_glyph.value.i_val = glyph_raster;	

		ms_glyph_box.location = GetUniformLocation(t_renderer.program.Name(), "sb_font");
		ms_glyph_box.value.i_val = glyph_box;

		screen_scale.location = GetUniformLocation(t_renderer.program.Name(), "screen_scale");

		result = 0;
	} else {
		return 323231;
	}

	glGenTextures(1, &tex_buf);


//	result = glGetError();
	
	return result;
}

void OpenGL::Core::TextPass::BindBT(unsigned int bufid) {
//	BindBuffer(GL_ARRAY_BUFFER, bufid);
	ActiveTexture(GL_TEXTURE0+glyph_box);
	glBindTexture(GL_TEXTURE_BUFFER, tex_buf);
	TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, bufid);
}

OpenGL::Core::TextPass * OpenGL::Core::TextPass::Activate() {
	t_renderer.local_cfg.value[0].i_cmp = 1; // ddest;
	t_renderer.Activate();
	
	if (ms_glyph.location != -1) Uniform1i(ms_glyph.location, ms_glyph.value.i_val);
	if (ms_glyph_box.location != -1) Uniform1i(ms_glyph_box.location, ms_glyph_box.value.i_val);	
	if (screen_scale.location != -1) Uniform4fv(screen_scale.location, 1, &screen_scale.value[0].float_cmp);
	
	return this;
}

void OpenGL::Core::TextPass::SetScreen(int w ,int h) {
	screen_scale.value[0].float_cmp = 0.5f*(float)w;		
	screen_scale.value[1].float_cmp = 0.5f*(float)h;	

	screen_scale.value[2].float_cmp = 1.0f;
	screen_scale.value[3].float_cmp = 1.0f;
}

void OpenGL::Core::TextPass::Destroy() {
	glDeleteTextures(1, &tex_buf);
	
}





