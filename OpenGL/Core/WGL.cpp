/*
** safe-fail OGL core components
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "..\OpenGL.h"
#include "System\SysUtils.h"

#define OPENGL_FUNC_INI(n,a,t) typedef t (__stdcall *P##n)a; P##n n = 0

#include "OGL_WGL.h"

extern "C" __declspec(dllimport) void __stdcall glViewport(int, int, int, int); 


int OpenGL::Core::pf_list[128];
unsigned int OpenGL::Core::pfl_count = 0;

unsigned int OpenGL::Core::Initialize_WGL(HDC & hdc, HGLRC & hrc) {
// pixel format
	unsigned int result(0);

	WGL_FUNC_MAP(GetPixelFormatAttribiv);
	WGL_FUNC_MAP(GetPixelFormatAttribfv);
	WGL_FUNC_MAP(ChoosePixelFormat);

// make current read
	WGL_FUNC_MAP(MakeContextCurrent);
	WGL_FUNC_MAP(GetCurrentReadDC);

// create context
	WGL_FUNC_MAP(CreateContextAttribs);

	if (WGL::GetPixelFormatAttribiv && WGL::GetPixelFormatAttribfv && WGL::ChoosePixelFormat && WGL::MakeContextCurrent && WGL::GetCurrentReadDC && WGL::CreateContextAttribs) {
		int pfconfig[] = {	WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
							WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
							WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
							WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
							WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
							WGL_COLOR_BITS_ARB, 32,
							WGL_DEPTH_BITS_ARB, 24,
							WGL_ALPHA_BITS_ARB, 8,
							WGL_ACCUM_BITS_ARB, 0,
							WGL_STENCIL_BITS_ARB, 0,
							WGL_AUX_BUFFERS_ARB, 0,

							0, 0};
			

		if (!WGL::ChoosePixelFormat(hdc, pfconfig, 0, 128, pf_list, &pfl_count)) {
			result = GetLastError();
		}

	}

	
	WGL_FUNC_MAP(CreateBufferRegion);
	WGL_FUNC_MAP(DeleteBufferRegion);
	WGL_FUNC_MAP(SaveBufferRegion);
	WGL_FUNC_MAP(RestoreBufferRegion);

// extensions
	WGL_FUNC_MAP(GetExtensionsString);


// pbuffer
	WGL_FUNC_MAP(CreatePbuffer);
	WGL_FUNC_MAP(GetPbufferDC);
	WGL_FUNC_MAP(ReleasePbufferDC);
	WGL_FUNC_MAP(DestroyPbuffer);
	WGL_FUNC_MAP(QueryPbuffer);

// render texture
	WGL_FUNC_MAP(BindTexImage);
	WGL_FUNC_MAP(ReleaseTexImage);
	WGL_FUNC_MAP(SetPbufferAttrib);

	return result;


}

