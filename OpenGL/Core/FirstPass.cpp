#include "OpenGL\OpenGL.h"

#include "OGL_15.h"
#include "OGL_43.h"


int OpenGL::Core::FirstPass::Initialize() {
	int result(0);
	
	HINSTANCE libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME));
	
	draw_buffers[0] = GL_COLOR_ATTACHMENT0;
	draw_buffer_count = 2;

	small_triangle.program.VertexShader.FromResource(16385, libin);
	small_triangle.program.GeometryShader.FromResource(16387, libin);
	small_triangle.program.FragmentShader.FromResource(16386, libin);
		
	if (result = small_triangle.program.Build()) {
		small_triangle.view_matrix = GetUniformLocation(small_triangle.program.GetName(), "local_view"); // view matrix
		result = GetUniformBlockIndex(small_triangle.program.GetName(), "ConfigBlock");
		UniformBlockBinding(small_triangle.program.GetName(), result, 0);

		result = 0;
	} else {
		return 333333;
	}

	GenFramebuffers(1, &builder_frame);
	BindFramebuffer(GL_FRAMEBUFFER, builder_frame);	
	FramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, tex_ids, 0);

	result = (CheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE);
	
	return result;
}

int OpenGL::Core::FirstPass::Resize(int ww, int hh) {
	if (!tex_ids) glGenTextures(1, &tex_ids);

	ActiveTexture(GL_TEXTURE0+1);
	
	glBindTexture(GL_TEXTURE_RECTANGLE, tex_ids);	
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA32F, ww, hh, 0, GL_RGBA, GL_FLOAT, 0); // viewport width & height

	return 0;
}

void OpenGL::Core::FirstPass::Destroy() {
	glDeleteTextures(1, &tex_ids);

	DeleteFramebuffers(1, &builder_frame);

}

OpenGL::Core::RenderPass * OpenGL::Core::FirstPass::Activate() {
	__super::Activate();

	return this;
}

