/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\SysUtils.h"

#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"
#include "OpenGL\Font.h"

#ifdef _DEBUG
const wchar_t alphab[] = L"_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ_:0456789).abcdefghijklmnopqrstuvwxyz|ABCDEFGHIJKLMNOPQRSTUVWXYZ";
#else
const wchar_t alphab[] = L"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ";
#endif

OpenGL::Text::Text() {
	SetTID(type_id);
}

OpenGL::Text::~Text() {

}

UI_64 OpenGL::Element::AddLabel(const LabelCfg & l_cfg, UI_64 right_id) {
	Core * core_ogl(0);
	Text t_obj;	
	UI_64 result(0), s_len(1024), x_len(0);

	if (l_cfg.max_char_count <= 1024) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
			core_ogl = reinterpret_cast<Core *>(el_hdr->_context);

			Release();
		}

		if (core_ogl) {
			result = core_ogl->CreateElement(Element::TQuad, t_obj, 0, this, sizeof(Text::Header) + l_cfg.max_char_count*sizeof(wchar_t));

			if (result != -1) {
				if (right_id != -1) {
					t_obj.ShiftRight(right_id);

				}

				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(t_obj.Acquire())) {
					Text::Header * t_hdr = reinterpret_cast<Text::Header *>(el_hdr + 1);
				

					t_hdr->font_style = l_cfg.font_style;
						
					t_hdr->_top = t_hdr->max_char_count = l_cfg.max_char_count;

					t_hdr->font_color[0] = l_cfg.c_color[0];
					t_hdr->font_color[1] = l_cfg.c_color[1];
					t_hdr->font_color[2] = l_cfg.c_color[2];
					t_hdr->font_color[3] = l_cfg.c_color[3];
						
					t_obj.Release();
				}

				if (l_cfg.t_color[3] > 0.0) t_obj.SetColor(l_cfg.t_color);

				if (t_obj.SetFont(l_cfg.font_id) == -1) {
					if (t_obj.SetFont(L"Calibril") == -1)
						t_obj.SetFont(L"Cour");
				}

				if (l_cfg.max_char_count) t_obj.OverWrite(alphab, l_cfg.max_char_count);
				else t_obj.OverWrite(alphab, 32);

				t_obj.SetSize(l_cfg.f_size);
				t_obj.Move(l_cfg.offset);

			}
		}
	}

	return result;

}


UI_64 OpenGL::Core::CreateText(const wchar_t * text_line, Text & t_obj, const Element * parent, const wchar_t * fname) {
		
	UI_64 result = CreateElement(Element::TQuad, t_obj, 0, parent, sizeof(Text::Header) + 64*sizeof(wchar_t));
		
	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(t_obj.Acquire())) {
			Text::Header * t_hdr = reinterpret_cast<Text::Header *>(el_hdr + 1);
				
	
			t_hdr->font_id = -1;
			t_hdr->font_size = 8.0f;
			t_hdr->font_color[3] = 1.0f;
			t_hdr->_top = 64;
					
			t_obj.Release();
		}


		if (fname) {
			if (t_obj.SetFont(fname) == -1) fname = 0;
		}
	
		if (!fname) {
			if (t_obj.SetFont(L"Calibril") == -1) t_obj.SetFont(L"Cour");
		}

		t_obj.OverWrite(text_line);

	}

	return result;
}

void OpenGL::Text::SetMCCount(unsigned int m_count) {
	Element a_obj(*this);

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);

		if (m_count <= thdr->_top) {
			thdr->max_char_count = m_count;
		} else {
			thdr->max_char_count = thdr->_top;
		}

		if (thdr->_runner > thdr->max_char_count) {
			a_obj.Down();

			for (unsigned int i(0);i<thdr->max_char_count;i++) {
				a_obj.Left();
			}

			for (unsigned int i(thdr->max_char_count);i<thdr->_runner;i++) {
				a_obj.Hide();
				a_obj.Left();
			}
		}

		Release();
	}
}

void OpenGL::Core::RewriteText(UI_64 tid, const wchar_t * str, unsigned int s_len) {	
	Text telem;

	SetElement(telem, tid);
	
	telem.Rewrite(str, 0, s_len);
	ResetElement(telem);
}

void OpenGL::Text::SetSize(float fs) {
	float ffs[4];
	if (fs<8.0f) fs = 8.0f;

	AttributeArray vrtx;
	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);
		thdr->font_size = fs;

		if (fs <= 12.0f) {
			thdr->level_selector = 0x00030000;
		} else {
			if (fs <= 24.0f) {
				thdr->level_selector = 0x00020000;
			} else {
				if (fs <= 48.0f) {
					thdr->level_selector = 0x00010000;
				} else {
					thdr->level_selector = 0x00000000;
				}
			}
		}

		Release();
	}

	InitAttributeArray(AttributeArray::Vertex, vrtx);
	
	ffs[0] = fs; ffs[1] = -fs; ffs[2] = ffs[3] = 1.0f;
	vrtx.Scale(ffs);

}




void OpenGL::Text::SetTextColor(const float * rgba) {
	Element aobj = *this;
	aobj.Down();

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);
			
		thdr->font_color[0] = rgba[0];
		thdr->font_color[1] = rgba[1];
		thdr->font_color[2] = rgba[2];
		thdr->font_color[3] = rgba[3];

		Release();
	}
}

void OpenGL::Text::AdjustTQuad(float * ovector, unsigned int tyl) {
	float * a_ptr(0);
	AttributeArray atarr;

	if (InitAttributeArray(AttributeArray::Vertex, atarr) != -1) {
		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(atarr.Acquire())) {
			a_ptr = reinterpret_cast<float *>(ahdr + 1);

			if (tyl & Vertical) {
				a_ptr[0] = ovector[7];
				a_ptr[1] = ovector[7];
			
				a_ptr[4] = ovector[7];
				a_ptr[5] = ovector[1] - 0.125f;

				a_ptr[8] = ovector[2];
				a_ptr[9] = ovector[7];

				a_ptr[12] = ovector[2];
				a_ptr[13] = ovector[1] - 0.125f;

			} else {
				a_ptr[0] = ovector[7];
				a_ptr[1] = ovector[4];
			
				a_ptr[4] = ovector[0] - 0.125f;
				a_ptr[5] = ovector[4];

				a_ptr[8] = ovector[7];
				a_ptr[9] = ovector[3];

				a_ptr[12] = ovector[0] - 0.125f;
				a_ptr[13] = ovector[3]; // ovector[4] + 
			}			
			
			ovector[0] = ovector[7];
			ovector[1] = -ovector[3];
			ovector[2] = ovector[7];
			ovector[3] = 1.0f;

//			atarr.SetOffset(ovector);

			

			if (tyl & Italic) {
				// set transformation according to style
			}

			atarr.Release();
/*
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				atarr.CalculateBox(static_cast<AnchorType>(el_hdr->config.anchor & 0x00FF));
				Release();
			}
*/
		}
	}
}

unsigned int OpenGL::Text::SetFont(const wchar_t * fname) {
	unsigned int fid = Font::Create(fname);
	
	if (fid == -1)  return -1;

	return SetFont(fid);
}

unsigned int OpenGL::Text::SetFont(unsigned int fid) {
	const wchar_t * line_ptr(0);	
	float ovector[16];
	AttributeArray atarr;
	Core * ogl_core(0);

	unsigned int tyl(-1), t_count(0);
	
	Element aobj;
	
	ovector[0] = ovector[1] = 0.0; // offset vector
	ovector[2] = 0.0f; ovector[3] = 1.e-4f; // mask style dependent
	ovector[4] = 100.0f;
	ovector[5] = ovector[6] = ovector[7] = 0.0;

	ovector[8] = ovector[9] = ovector[10] = ovector[11] = 0.0f;

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);
		
		ogl_core = reinterpret_cast<Core *>(ehdr->_context);

		if (fid != thdr->font_id) {
			thdr->font_id = fid;

			tyl = thdr->font_style;
			t_count = thdr->_runner;


			if (thdr->font_style & Left) {

			}

			if (thdr->font_style & Vertical) {
				ovector[2] = 1.e-4f; ovector[3] = 0.0f;
			} else {
				if (thdr->font_style & Mono) {
					ovector[8] = 1.0f;
				}
			}								
		}

		Release();
	}
	
	if ((t_count) && (ogl_core)) {
		aobj = *this;
		aobj.Set(&aobj, sizeof(Element::Header) + sizeof(Text::Header) - 4);

		Font::InitializeRange(fid, aobj, ogl_core->GetWinHandle());
		
		aobj.Set((SFSco::Object *)0, 0);
		aobj.Down();
	
		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
			Text::Header * t_hdr = reinterpret_cast<Text::Header *>(elhdr + 1);
			line_ptr = reinterpret_cast<const wchar_t *>(t_hdr+1);
				
			for (unsigned int i(0);i<t_hdr->_runner;i++) {
				aobj.InitAttributeArray(AttributeArray::Vertex, atarr);
				
				if (Element::Header * sym_hdr = reinterpret_cast<Element::Header *>(aobj.Acquire())) {
					sym_hdr->_reserved = line_ptr[i];
					sym_hdr->config.tex_unit_selector = Font::GetMetrixBox(fid, sym_hdr->_reserved, atarr, ovector);

					aobj.Release();
				}
				
				aobj.Left();
			}
			Release();
		}

		// use ovector to adjust tquad
		AdjustTQuad(ovector, tyl);
	}	

	return fid;
}

void OpenGL::Text::SetStyle(Style tstyle) {
	float ovector[16];	

	Element el_obj(Text::symbol_tid), aobj;
	AttributeArray atarr;

	ovector[0] = ovector[1] = 0.0; // offset vector
	ovector[2] = 0.0f; ovector[3] = 1.e-4f; // mask style dependent
	ovector[4] = 100.0f;
	ovector[5] = ovector[6] = ovector[7] = 0.0;
	ovector[8] = ovector[9] = ovector[10] = ovector[11] = 0.0f;

	aobj = *this;
	aobj.Down();

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);
		
		thdr->font_style = tstyle;
		(unsigned int &)tstyle |= Undef;

		if (thdr->font_style & Left) {

		}

		if (thdr->font_style & Vertical) {
			ovector[2] = 1.e-4f; ovector[3] = 0.0f;
		} else {
			if (thdr->font_style & Mono) {
				ovector[8] = 1.0f;
			}
		}


		for (unsigned int i(0);i<thdr->_runner;i++) {
			aobj.InitAttributeArray(AttributeArray::Vertex, atarr);
					
			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(aobj.Acquire())) {
				elhdr->_reserved = reinterpret_cast<wchar_t *>(thdr+1)[i];
				elhdr->config.tex_unit_selector = Font::GetMetrixBox(thdr->font_id, elhdr->_reserved, atarr, ovector);
					
				aobj.Release();
			}

			aobj.Left();
		}

		Release();
	}

		
	if (tstyle & Undef) {
		AdjustTQuad(ovector, tstyle);
	}		
}

UI_64 OpenGL::Text::OverWrite(const wchar_t * text, UI_64 slen) {	
	float ovector[16];
	Core * core_ptr(0);
	unsigned int texpo(-1), fid(-1), tyl(0), result(0);

	Element el_obj(Text::symbol_tid), aobj, bobj;

	AttributeArray atarr;


	if (slen == 0) slen = wcslen(text);
	
	ovector[0] = ovector[1] = 0.0; // offset vector
	ovector[2] = 0.0f; ovector[3] = 1.e-4f; // mask style dependent
	ovector[4] = 100.0f;
	ovector[5] = ovector[6] = ovector[7] = 0.0;
	ovector[8] = ovector[9] = ovector[10] = ovector[11] = 0.0f;
	
	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);

		core_ptr = reinterpret_cast<Core *>(ehdr->_context);
		
		
		fid = thdr->font_id;

		tyl = thdr->font_style;

		if (slen < thdr->_top) {
			System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr + 1), text, slen*sizeof(wchar_t));
			reinterpret_cast<wchar_t *>(thdr + 1)[slen] = 0;
			thdr->_runner = slen;
			texpo = 0;
		} else {
			texpo = slen - thdr->_top;
		}

		if (thdr->font_style & Left) {

		}

		if (thdr->font_style & Vertical) {
			ovector[2] = 1.e-4f; ovector[3] = 0.0f;
		} else {
			if (thdr->font_style & Mono) {
				ovector[8] = 1.0f;
			}
		}


		Release();
	}

	if (texpo) {
		if (Expand((texpo + 16)*sizeof(wchar_t)) != -1) {
			if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
				Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);

				thdr->_top = slen+16;
				thdr->_runner = slen;
				
				System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr + 1), text, slen*sizeof(wchar_t));
				reinterpret_cast<wchar_t *>(thdr + 1)[slen] = 0;

				Release();
			}

			texpo = 0;
		}
	}

	if (core_ptr) {
		aobj = *this;
		aobj.Set(&aobj, sizeof(Element::Header) + sizeof(Text::Header) - 4);
		Font::InitializeRange(fid, aobj, core_ptr->GetWinHandle());
			
		aobj.Set((SFSco::Object *)0, 0);
		aobj.Down();
		
		bobj = aobj;

		if (aobj != *this) {
			for (texpo = 0;texpo<slen;) {
				aobj.InitAttributeArray(AttributeArray::Vertex, atarr);

				if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(aobj.Acquire())) {
					elhdr->_flags |= Element::FlagVisible;

					elhdr->_reserved = text[texpo++];
					elhdr->config.tex_unit_selector = Font::GetMetrixBox(fid, elhdr->_reserved, atarr, ovector);

					aobj.Release();
				}


				if (reinterpret_cast<SFSco::Object &>(aobj.Left()) == bobj) break;
			}
					

			if (texpo < slen) {
				for (result = texpo;result<slen;result++) {
					if (core_ptr->CreateElement(Element::Symbol, el_obj, 0, this, 0x80000000) != -1) {
						el_obj.InitAttributeArray(AttributeArray::Vertex, atarr);
					
						if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
							elhdr->_reserved = text[result];
							elhdr->config.tex_unit_selector = Font::GetMetrixBox(fid, elhdr->_reserved, atarr, ovector);

							el_obj.Release();
						}
					}
				}
				
			} else {
				for(;aobj != bobj;aobj.Left()) {
					aobj.Hide();				
				}				
			}
		} else {
			
		// just create cildren
			for (result = 0;result<slen;result++) {
				if (core_ptr->CreateElement(Element::Symbol, el_obj, 0, this, 0x80000000) != -1) {
					el_obj.InitAttributeArray(AttributeArray::Vertex, atarr);
						
					if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
						elhdr->_reserved = text[result];
						elhdr->config.tex_unit_selector = Font::GetMetrixBox(fid, elhdr->_reserved, atarr, ovector);
					
						el_obj.Release();
					}
				}
			}
		}
	

		// use ovector to adjust tquad
		AdjustTQuad(ovector, tyl);
	}

			
	return result;
}

void OpenGL::Text::Rewrite(Text & t_ref) {
	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(t_ref.Acquire())) {
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);

		Rewrite(reinterpret_cast<wchar_t *>(thdr + 1), 0, thdr->_runner);

		t_ref.Release();
	}
}

void OpenGL::Text::Rewrite(const wchar_t * r_str, UI_64 pos, UI_64 slen) {

	float ovector[16], * v_ptr(0);
	unsigned int fid(-1), tyl(0), co_vals[4]; // , t_count(0);
	Core * core_ptr(0);

	Element el_obj(Text::symbol_tid), aobj;
	AttributeArray atarr;


	if (slen == 0) slen = wcslen(r_str);

	ovector[0] = ovector[1] = 0.0; // offset vector
	ovector[2] = 0.0f; ovector[3] = 1.e-4f; // mask style dependent
	ovector[4] = 100.0f;
	ovector[5] = ovector[6] = ovector[7] = 0.0;
	ovector[8] = ovector[9] = ovector[10] = ovector[11] = 0.0f;

	InitAttributeArray(AttributeArray::Vertex, atarr);

	if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
		
		Text::Header * thdr = reinterpret_cast<Text::Header *>(ehdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(ehdr->_context);

		fid = thdr->font_id;
		
		tyl = thdr->font_style;
		
		if (thdr->font_style & Left) {

		}

		if (thdr->font_style & Vertical) {
			ovector[2] = 1.e-4f; ovector[3] = 0.0f;
		} else {
			if (thdr->font_style & Mono) {
				ovector[8] = 1.0f;
			}
		}


		if (thdr->max_char_count) {
			pos = 0;
			if (slen > thdr->max_char_count) {
				if (thdr->max_char_count > 6) {
					System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr+1), r_str, (thdr->max_char_count - 6)*sizeof(wchar_t));
					reinterpret_cast<wchar_t *>(thdr+1)[thdr->max_char_count - 6] = L'.';
					reinterpret_cast<wchar_t *>(thdr+1)[thdr->max_char_count - 5] = L'.';

					System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr+1) + thdr->max_char_count - 4, r_str + slen - 4, 4*sizeof(wchar_t));
					
				} else {
					System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr+1), r_str, thdr->max_char_count*sizeof(wchar_t));

				}

				slen = thdr->max_char_count;
			} else {
				System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr+1), r_str, slen*sizeof(wchar_t));
			}
			
			thdr->_runner = slen;



		} else {
			if (pos < thdr->_runner) {
				if ((slen + pos) > thdr->_runner) slen = thdr->_runner - pos + 1;
				System::MemoryCopy(reinterpret_cast<wchar_t *>(thdr+1) + pos, r_str, slen*sizeof(wchar_t));

				slen = thdr->_runner;

			} else {
				pos = -1;
			}

		}
		
/*
		if (pos != -1) {			

			if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(atarr.Acquire())) {
				v_ptr = reinterpret_cast<float *>(ahdr + 1);

				if (thdr->font_style & Left) {

				}

				if (thdr->font_style & Vertical) {
					ovector[2] = 1.e-4f; ovector[3] = 0.0f;

					ovector[2] = v_ptr[2*4];
					ovector[7] = v_ptr[0];
				} else {
					ovector[3] = v_ptr[2*4+1];
					ovector[4] = v_ptr[1];
					ovector[7] = v_ptr[0];

					if (thdr->font_style & Mono) {
						ovector[8] = 1.0f;
					}
				}

				atarr.Release();
			}

			Release();
		} else {
			pos = -1;
		}
*/
		Release();
	}


	if ((pos != -1) && (core_ptr)) {
		aobj = *this;
		aobj.Set(&aobj, sizeof(Element::Header) + sizeof(Text::Header) - 4);
		Font::InitializeRange(fid, aobj, core_ptr->GetWinHandle());
			
		aobj.Set((SFSco::Object *)0, 0);
		aobj.Down();
		

		for (unsigned int i(0);i<pos;i++) aobj.Left();

		aobj.InitAttributeArray(AttributeArray::Vertex, atarr);
		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(atarr.Acquire())) {
			v_ptr = reinterpret_cast<float *>(ahdr + 1);
			
			if (tyl & Vertical) {
				ovector[1] = v_ptr[9];
			} else {
				ovector[0] = v_ptr[0] + ovector[8]*(v_ptr[4] - v_ptr[0] - 1.0f);
			}

			atarr.Release();
		}



		if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(Acquire())) {

			co_vals[0] = reinterpret_cast<Text::Header *>(ehdr + 1)->max_char_count;
			co_vals[1] = reinterpret_cast<Text::Header *>(ehdr + 1)->_runner;

			Release();
			ehdr = 0;

			if (co_vals[0]) {
				for (unsigned int i(0);i<co_vals[1];i++) {
					if (ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
						co_vals[2] = reinterpret_cast<wchar_t *>(reinterpret_cast<Text::Header *>(ehdr + 1) + 1)[i];

						Release();
						ehdr = 0;

						aobj.InitAttributeArray(AttributeArray::Vertex, atarr);

						if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(aobj.Acquire())) {
							elhdr->_flags |= Element::FlagVisible;
							elhdr->_reserved = co_vals[2]; // reinterpret_cast<wchar_t *>(thdr+1)[i];
							elhdr->config.tex_unit_selector = Font::GetMetrixBox(fid, elhdr->_reserved, atarr, ovector);
				
							aobj.Release();
						}
				
						aobj.Left();
					}
				}

				for (unsigned int i(co_vals[1]);i<co_vals[0];i++) {				
					aobj.Hide();
					aobj.Left();
				}



			} else {
				for (unsigned int i(pos);i<co_vals[1];i++) {
					if (ehdr = reinterpret_cast<Element::Header *>(Acquire())) {
						co_vals[2] = reinterpret_cast<wchar_t *>(reinterpret_cast<Text::Header *>(ehdr + 1) + 1)[i];

						Release();
						ehdr = 0;

						aobj.InitAttributeArray(AttributeArray::Vertex, atarr);

						if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(aobj.Acquire())) {
							elhdr->_reserved = co_vals[2]; // reinterpret_cast<wchar_t *>(thdr+1)[i];
							elhdr->config.tex_unit_selector = Font::GetMetrixBox(fid, elhdr->_reserved, atarr, ovector);
				
							aobj.Release();
						}
				
						aobj.Left();
					}
				}
			}
			
			if (ehdr) Release();
		}
		
		// use ovector to adjust tquad
		AdjustTQuad(ovector, tyl);
	}

}

void OpenGL::Text::Update(const wchar_t * l_ptr, UI_64 l_len) {
	
	if (l_len == 0) l_len = wcslen(l_ptr);





	
}






