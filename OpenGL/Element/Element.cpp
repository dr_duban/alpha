#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "Math\CalcSpecial.h"

#include "Pics\Image.h"
#include "Media\Source.h"

#include "System\SysUtils.h"

#include "OpenGL\Core\OGL_15.h"

#pragma warning(disable:4244)

#define MAX_AEL	256

int OpenGL::Element::_po_x = 0;
int OpenGL::Element::_po_y = 0;

unsigned int OpenGL::Element::large_ml_runner = 0;
unsigned int OpenGL::Element::large_ml_top = 0;

SFSco::Object OpenGL::Element::large_map_list;

UI_64 OpenGL::Element::default_line_map = -1;
UI_64 OpenGL::Element::default_triangle_map = -1;


// Element ====================================================================================================================================================================================================
OpenGL::Element::Element(UI_64 tid) {
	SetTID(tid);
}

OpenGL::Element::Element() {
	SetTID(type_id);
	//default attribute count for all arrays is 4

}

OpenGL::Element::Element(const Element & ref) : Nucleus(ref) {

}

OpenGL::Element::~Element() {

}

void OpenGL::Element::HooverON(Element & current_hoover) {
	UI_64 result(1);
	Element e_obj(*this);

	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		reinterpret_cast<Core *>(el_hdr->_context)->chain_marker += 2;
		Release();
	}

	ClearEFlags((UI_64)(FlagMouseDown | FlagMouseUp | FlagLeave), (UI_64)FlagHoover);
	SetState((UI_64)FlagHoover);

	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		elhdr->_state &= ~(UI_64)FlagLeave;
		elhdr->_flags &= ~(UI_64)FlagHideOn;

		if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(*this)) result = 0;
		Release();	
	}

	for (;result & 1;) {
		e_obj.Up();

		if (e_obj == current_hoover) {
			result |= 4;
		}


		if (Header * elhdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
			elhdr->_state &= ~(UI_64)FlagLeave;
			elhdr->_flags &= ~(UI_64)FlagHideOn;

			if ((elhdr->config.att_flags & 0xFFFF0000) == SceneRegion) {
				result |= 2;
			}

			if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(e_obj)) result ^= 1;

			e_obj.Release();
		} else break;
		
		if (result & 2) {
			result ^= 2;
			continue;
		}

		e_obj.ClearEFlags((UI_64)(FlagMouseDown | FlagMouseUp | FlagLeave), (UI_64)FlagHoover);
		e_obj.SetState((UI_64)FlagHoover);

	}

	if (result < 4) {
		result = 1;
		
		if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
			--reinterpret_cast<Core *>(el_hdr->_context)->chain_marker;
			Release();
		}


		current_hoover.ClearEFlags((UI_64)(FlagHoover | FlagMouseDown | FlagMouseUp), (UI_64)FlagLeave);
		current_hoover.SetState((UI_64)FlagLeave); // get chain_marker value

		result = 1;

		if (Header * elhdr = reinterpret_cast<Header *>(current_hoover.Acquire())) {
			if (elhdr->_flags & (UI_64)FlagAuHide) {
				(unsigned int &)elhdr->root_cfg._fullW = reinterpret_cast<Core *>(elhdr->_context)->render_count + (unsigned int)elhdr->root_cfg._fullH;
				elhdr->_flags |= (UI_64)FlagHideOn;
			}

			if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(current_hoover)) result = 0;

			current_hoover.Release();
		}

		for (;result;) {
			current_hoover.Up();
				
			if (Header * elhdr = reinterpret_cast<Header *>(current_hoover.Acquire())) {
				if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(current_hoover)) result = 0;
				else result = 1;

				if ((elhdr->config.att_flags & 0xFFFF0000) == SceneRegion)	{
					result = 3;
				}
				
				current_hoover.Release();
			} else break;
			
			if (result == 3) continue;

			if (current_hoover.TestMarker()) break;

			current_hoover.ClearEFlags((UI_64)(FlagHoover | FlagMouseDown | FlagMouseUp), (UI_64)FlagLeave);
			current_hoover.SetState((UI_64)FlagLeave);

			if (Header * elhdr = reinterpret_cast<Header *>(current_hoover.Acquire())) {
				if (elhdr->_flags & (UI_64)FlagAuHide) {
					(unsigned int &)elhdr->root_cfg._fullW = reinterpret_cast<Core *>(elhdr->_context)->render_count + (unsigned int)elhdr->root_cfg._fullH;
					elhdr->_flags |= (UI_64)FlagHideOn;
				}
				current_hoover.Release();
			}
		}		
	}
}


void OpenGL::Element::LeaveON() {
	UI_64 result(1);
	Element e_obj(*this);

	ClearEFlags((UI_64)(FlagHoover | FlagMouseDown | FlagMouseUp), (UI_64)FlagLeave);
	SetState((UI_64)FlagLeave);
	
	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		if (elhdr->_flags & (UI_64)FlagAuHide) {
			(unsigned int &)elhdr->root_cfg._fullW = reinterpret_cast<Core *>(elhdr->_context)->render_count + (unsigned int)elhdr->root_cfg._fullH;
			elhdr->_flags |= (UI_64)FlagHideOn;
		}

		if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(*this)) result = 0;
		Release();	
	}



	for (;result;) {		
		e_obj.Up();

		if (Header * elhdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
			if (elhdr->_flags & (UI_64)FlagAuHide) {
				(unsigned int &)elhdr->root_cfg._fullW = reinterpret_cast<Core *>(elhdr->_context)->render_count + (unsigned int)elhdr->root_cfg._fullH;
				elhdr->_flags |= (UI_64)FlagHideOn;
			}

			if (reinterpret_cast<OpenGL::Core *>(elhdr->_context)->IsRoot(e_obj)) result = 0;
			else result = 1;

			if ((elhdr->config.att_flags & 0xFFFF0000) == SceneRegion) {
				result = 2;
			}

			e_obj.Release();
		} else break;
				
		if (result == 2) continue;
		
		e_obj.ClearEFlags((UI_64)(FlagHoover | FlagMouseDown | FlagMouseUp), (UI_64)FlagLeave);
		e_obj.SetState((UI_64)FlagLeave);

	}
}


void OpenGL::Element::DownON() {
	UI_64 result(0);
	Element e_obj(*this);

	ClearEFlags((UI_64)FlagMouseUp, (UI_64)FlagMouseDown);
	SetState((UI_64)FlagMouseDown);

	for (;;e_obj.Up()) {
		if (Header * elhdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
			result = elhdr->_flags & (UI_64)(FlagAction | FlagMovable | FlagTouch | FlagTouch2);

			switch (elhdr->config.att_flags & 0xFFFF0000) {
				case ClipRegion:
					result = -1;
				break;
				case SceneRegion:
					result = 0;
				break;
			} 

			e_obj.Release();	
		} else break;
		
		if (result) break;
	}
	
	if (e_obj != *this) {
		e_obj.ClearEFlags((UI_64)FlagMouseUp, (UI_64)FlagMouseDown);
		e_obj.SetState((UI_64)FlagMouseDown);
	}
	
}


void OpenGL::Element::ActionON() {
	UI_64 result(0);
	Element e_obj(*this);
	
	for (;;e_obj.Up()) {
		if (Header * elhdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
			result = elhdr->_flags & (UI_64)FlagAction;

			if ((elhdr->config.att_flags & 0xFFFF0000) == ClipRegion) {
				result = -1;
			}		

			e_obj.Release();	
		} else break;

		if (result) break;
	}

	e_obj.ClearEFlags((UI_64)(FlagMouseDown), (UI_64)FlagMouseUp);
	e_obj.SetState((UI_64)FlagMouseUp);
}

void OpenGL::Element::ClearDualUp() {
	UI_64 result(0);
	Element e_obj(*this);
	
	for (;;e_obj.Up()) {
		if (Header * elhdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
			result = elhdr->_flags & (UI_64)FlagAction;

			if ((elhdr->config.att_flags & 0xFFFF0000) == ClipRegion) {
				result = -1;
			}		

			e_obj.Release();	
		} else break;

		if (result) break;
	}

	if (Header * e_hdr = reinterpret_cast<Header *>(e_obj.Acquire())) {
		if (e_hdr->_flags & FlagDualUp) {
			e_hdr->_state &= ~(UI_64)FlagDualUp;

			if (Header * ya_hdr = reinterpret_cast<Header *>(Acquire())) {
				RelocateTex((ya_hdr->_r_d[1] >> 32));

				Release();
			}
		}

		e_obj.Release();
	}

}




UI_64 OpenGL::Element::IsAnchor() {
	UI_64 result(0);
	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		if ((elhdr->config.anchor & 0x00FF) != None) result = elhdr->_flags & (UI_64)FlagAnchor;

		Release();
	}

	return result;
}

UI_64 OpenGL::Element::IsVisible() {
	Element ell(*this);
	UI_64 result(FlagVisible);

	for (;ell && result;ell.Up())
	if (Header * elhdr = reinterpret_cast<Header *>(ell.Acquire())) {
		result &= elhdr->_flags;

		if (reinterpret_cast<Core *>(elhdr->_context)->IsRoot(ell)) {
			ell.Release();
			ell.Blank();
		} else {
			ell.Release();
		}		
	}

	return result;
}


unsigned int OpenGL::Element::IsRefresh(unsigned int fidx) {	
	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		fidx ^= elhdr->findex;
		Release();
	}

	return fidx;
}


UI_64 OpenGL::Element::XPand() {
	return 0;
}

UI_64 OpenGL::Element::Touch(float * ovec) {
	UI_64 result(0);
	DecodeRecord de_rec;

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		result = elhdr->_flags & (UI_64)FlagTouch2;
		elhdr->root_cfg._maxW -= ovec[0];
		elhdr->root_cfg._maxH -= ovec[1];

		Release();
	}
	
	if (result) {
		de_rec.flags = DecodeRecord::RecordFlags | DecodeRecord::FlagRefresh;
		de_rec.element_obj = *this;

		if (!Pics::Image::Decode(de_rec)) Media::Program::Decode(de_rec);
	}

	return result;
}

void OpenGL::Element::Destroy() {
	Element x_lem(*this);
	AttributeArray v_arr;

	x_lem.Down();
	if (x_lem != *this) x_lem.DestroyAll(v_arr);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		if (hdr->_context) reinterpret_cast<Core *>(hdr->_context)->ClearFBack(*this);

		for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
			if (hdr->domain_desc[i].array_id != -1) {
				v_arr.Set(AttributeArray::_attributes_pool, hdr->domain_desc[i].array_id);

				v_arr.Destroy();
			}
		}

		Release();
	}

	Nucleus::Destroy();
}

void OpenGL::Element::DestroyAll(AttributeArray & v_arr) {
	Element lelem(*this), x_lem(*this);
	

	for (Element delem(x_lem);delem;delem = lelem) {		
		x_lem = lelem;
	
		lelem.Left();

		if (lelem == x_lem) lelem.Blank();

		delem.Down();
		if (delem != x_lem) delem.DestroyAll(v_arr);
			
		if (Header * hdr = reinterpret_cast<Header *>(x_lem.Acquire())) {
			if (hdr->_context) reinterpret_cast<Core *>(hdr->_context)->ClearFBack(*this);

			for (unsigned int i(0);i<AttributeArray::AttTypeCount;i++) {
				if (hdr->domain_desc[i].array_id != -1) {
					v_arr.Set(AttributeArray::_attributes_pool, hdr->domain_desc[i].array_id);

					v_arr.Destroy();
				}
			}

			x_lem.Release();
		}

		x_lem.Nucleus::Destroy();
	}	
}


UI_64 OpenGL::Element::InitAttributeArray(AttributeArray::Type atype, AttributeArray & aarr, const float * src, unsigned int ccount) {
	UI_64 result(0);

	aarr.Blank();

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {		
		if (hdr->domain_desc[atype].array_id == -1) {
			result = aarr.Create(hdr->vertex_count, src, ccount);

			if (result != -1) {
				hdr->domain_desc[atype].array_id = result;
				hdr->config.att_flags |= (1<<atype);
			}
		} else {
			aarr.Set(AttributeArray::_attributes_pool, hdr->domain_desc[atype].array_id);

		}
	
		aarr.Set(&aarr, sizeof(AttributeArray::Header));

		Release();
	}

	return result;
}

UI_64 OpenGL::Element::ConfigureLarge(UI_64 map_id) {	
	UI_64 result(0);
	SFSco::Object m_obj;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {

		if (el_hdr->vertex_count > Core::max_geometry_out) {
			switch (map_id) {
				case -1:
					if (default_line_map != -1) {
						m_obj.Set(SFSco::mem_mgr, default_line_map);

						if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
							if (m_ptr[0] < el_hdr->vertex_count) result = el_hdr->vertex_count - m_ptr[0];

							m_obj.Release();
						}

						if (result) {
							if (m_obj.Expand(result*4*sizeof(float)) != -1) {
								// set map buff rewrite flag

								if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
									m_ptr[1] = 1;

									for (unsigned int ii(m_ptr[0]);ii<el_hdr->vertex_count;ii++) {
										m_ptr[ii*4 + 4] = ii;
										m_ptr[ii*4 + 5] = ii + 1;
										m_ptr[ii*4 + 6] = 0;
										m_ptr[ii*4 + 7] = 0;
									}

									m_ptr[0] = el_hdr->vertex_count;

									m_obj.Release();
								}
							}
						}
					} else {
						default_line_map = m_obj.New(0, (el_hdr->vertex_count+1)*4*sizeof(float));

						if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
									
							for (unsigned int ii(0);ii<el_hdr->vertex_count;ii++) {
								m_ptr[ii*4 + 4] = ii;
								m_ptr[ii*4 + 5] = ii + 1;
								m_ptr[ii*4 + 6] = 0;
								m_ptr[ii*4 + 7] = 0;
							}

							m_ptr[0] = el_hdr->vertex_count;
							m_ptr[1] = 1;
							m_ptr[2] = 0;
							m_ptr[3] = 0;

							m_obj.Release();
						}
					}

					el_hdr->x_tra = RegisterLargeMap(default_line_map);
					el_hdr->xc_index = el_hdr->vertex_count - 1;
				break;
				case -2:
					if (default_triangle_map != -1) {
						m_obj.Set(SFSco::mem_mgr, default_triangle_map);
					
						if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
							if (m_ptr[0] < el_hdr->vertex_count) result = el_hdr->vertex_count - m_ptr[0];

							m_obj.Release();
						}

						if (result) {
							if (m_obj.Expand(result*4*sizeof(float)) != -1) {
								if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
									m_ptr[1] = 1;

									for (unsigned int ii(m_ptr[0]);ii<el_hdr->vertex_count;ii++) {
										m_ptr[ii*4 + 4] = ii;
										m_ptr[ii*4 + 5] = ii + 1;
										m_ptr[ii*4 + 6] = ii + 2;
										m_ptr[ii*4 + 7] = 0;
									}

									m_ptr[0] = el_hdr->vertex_count;

									m_obj.Release();
								}
							}
						}
					} else {
						default_triangle_map = m_obj.New(0, (el_hdr->vertex_count+1)*4*sizeof(float));

						if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {									
							for (unsigned int ii(0);ii<el_hdr->vertex_count;ii++) {
								m_ptr[ii*4 + 4] = ii;
								m_ptr[ii*4 + 5] = ii + 1;
								m_ptr[ii*4 + 6] = ii + 2;
								m_ptr[ii*4 + 7] = 0;
							}

							m_ptr[0] = el_hdr->vertex_count;
							m_ptr[1] = 1; // resest flag
							m_ptr[2] = 0; // range reset
							m_ptr[3] = 0;

							m_obj.Release();
						}
					}

					el_hdr->x_tra = RegisterLargeMap(default_triangle_map);

				break;
				default:			
					el_hdr->x_tra = RegisterLargeMap(map_id);


			}
		}


		Release();
	}



	return result;
}

unsigned int OpenGL::Element::RegisterLargeMap(UI_64 map_id) {
	unsigned int result(-1), e_slot(-1);

	if (large_ml_runner >= large_ml_top) {
		if (large_map_list) {
			if (large_map_list.New(0, 128*2*sizeof(UI_64)) != -1) {
				large_ml_top = 128;
			}

		} else {
			if (large_map_list.Expand(128*2*sizeof(UI_64)) != -1) {
				large_ml_top += 128;
			}
		}
	}


	if (large_ml_runner < large_ml_top) {
		if (UI_64 * lm_ptr = reinterpret_cast<UI_64 *>(large_map_list.Acquire())) {
			for (unsigned int i(0);i<large_ml_runner;i++) {
				if (lm_ptr[2*i]) {
					if (lm_ptr[2*i] == map_id) {
						result = i;
						break;
					}
				} else {
					if (e_slot == -1) e_slot = i;
				}
			}

			if (result == -1) {
				if (e_slot == -1) {
					result = large_ml_runner++;
				} else {
					result = e_slot;
				}

				lm_ptr[2*result] = map_id;
			}

			large_map_list.Release();
		}
	}

	return result;
}

unsigned int OpenGL::Element::UnRegisterLargeMap(UI_64 map_id) {
	if (UI_64 * lm_ptr = reinterpret_cast<UI_64 *>(large_map_list.Acquire())) {
		for (unsigned int i(0);i<large_ml_runner;i++) {
			if (lm_ptr[2*i] == map_id) {
				lm_ptr[2*i] = 0;
				break;
			}
		}
		large_map_list.Release();
	}

	return 0;
}

unsigned int OpenGL::Element::LargeMapRefresh() {
	SFSco::Object m_obj;
	if (UI_64 * lm_ptr = reinterpret_cast<UI_64 *>(large_map_list.Acquire())) {
		for (unsigned int i(0);i<large_ml_runner;i++) {
			if (lm_ptr[2*i]) {
				if (lm_ptr[2*i + 1] == 0) GenBuffers(1, reinterpret_cast<unsigned int *>(lm_ptr + 2*i + 1));

				if (lm_ptr[2*i + 1]) {
					m_obj.Set(SFSco::mem_mgr, lm_ptr[2*i]);
					if (unsigned int * m_ptr = reinterpret_cast<unsigned int *>(m_obj.Acquire())) {
						if (m_ptr[1] | (m_ptr[3])) {							
							BindBuffer(GL_ARRAY_BUFFER, lm_ptr[2*i + 1]);

							if (m_ptr[1]) {
								BufferData(GL_ARRAY_BUFFER, m_ptr[0]*4*sizeof(unsigned int), m_ptr + 4, GL_STATIC_DRAW);
							} else {
								BufferSubData(GL_ARRAY_BUFFER, m_ptr[2]*4*sizeof(int), m_ptr[3]*4*sizeof(unsigned int), m_ptr + 4 + m_ptr[2]);
							}


							m_ptr[1] = 0;
							m_ptr[2] = 0;
							m_ptr[3] = 0;
						}


						m_obj.Release();
					}
				}

			} else {
				if (lm_ptr[2*i + 1]) {
					DeleteBuffers(1, reinterpret_cast<unsigned int *>(lm_ptr + 2*i + 1));
					lm_ptr[2*i + 1] = 0;
				}
			}
		}
		large_map_list.Release();
	}


	return 0;
}


void OpenGL::Element::SceneCoordsAdjust(float * sc_coords, unsigned int vcount) {
	AttributeArray v_arr;
	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		reinterpret_cast<Core *>(el_hdr->_context)->RootAdjust(sc_coords, vcount);

		Release();
	}

	v_arr.GlobalDAdjust(sc_coords, vcount);
	v_arr.LocalDAdjust(sc_coords, vcount);

}

void OpenGL::Element::GetPtrPos(float * ptr_pos) {
	Core::GetGlobal(ptr_pos);
	ptr_pos[2] = 0.0;
	ptr_pos[3] = 1.0f;

	SceneCoordsAdjust(ptr_pos, 1);
}


void OpenGL::Element::Drag(int dx, int dy) {
	__declspec(align(16)) float o_vector[4] = {0.0, 0.0, 0.0, 0.0};
	int a(0), b(0);
	Core * core_ogl(0);	
	Element p_el(*this);
	AttributeArray v_arr;
		
	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		core_ogl = reinterpret_cast<Core *>(elhdr->_context);
		

		switch (elhdr->_flags & (UI_64)(FlagX0Drag | FlagX1Drag)) {
			case (UI_64)FlagX0Drag: // neg only
				if (dx > 0) dx = 0;
			break;
			case (UI_64)FlagX1Drag:
				if (dx < 0) dx = 0;
			break;
			case (UI_64)(FlagX0Drag | FlagX1Drag):
				dx = 0;
			break;
		}


		switch (elhdr->_flags & (UI_64)(FlagY0Drag | FlagY1Drag)) {
			case (UI_64)FlagY0Drag: // neg only				
				if (dy > 0) dy = 0;
			break;
			case (UI_64)FlagY1Drag:
				if (dy < 0) dy = 0;
			break;
			case (UI_64)(FlagY0Drag | FlagY1Drag):
				dy = 0;
			break;
		}


		if (elhdr->_flags & (UI_64)FlagXYDrag) {
			if (dx < 0) a = -dx;
			else a = dx;
			if (dy < 0) b = -dy;
			else b = dy;

			if (a >= b) {
				dy = 0;
			} else {
				dx = 0;
			}
		}

		Release();
	}

	b = 0;
	if ((core_ogl) && (dx | dy)) {
		o_vector[0] = dx; o_vector[1] = dy;

		p_el.Up();
		p_el.SceneCoordsAdjust(o_vector, 1);
/*
		for (p_el.Up();core_ogl->NoScene(p_el);p_el.Up()) {
			p_el.InitAttributeArray(AttributeArray::Vertex, v_arr);
			v_arr.CoordsUAdjust(o_vector, 1);
		}
*/		
		if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
			b = (elhdr->_flags & (UI_64)(FlagTouch | FlagTouch2));

			if (elhdr->_flags & (UI_64)FlagXSpan) {
				elhdr->span_cfg.x += o_vector[0];

				if (elhdr->span_cfg.x < 0.0f) {
					o_vector[0] -= elhdr->span_cfg.x;
					elhdr->span_cfg.x = 0.0;
				} else {
					if (elhdr->span_cfg.x > elhdr->span_cfg.x_max) {
						o_vector[0] -= (elhdr->span_cfg.x - elhdr->span_cfg.x_max);
						elhdr->span_cfg.x = elhdr->span_cfg.x_max;
					}
				}
			}

			if (elhdr->_flags & (UI_64)FlagYSpan) {
				elhdr->span_cfg.y -= o_vector[1];

				if (elhdr->span_cfg.y < 0.0f) {
					o_vector[1] += elhdr->span_cfg.y;
					elhdr->span_cfg.y = 0.0;
				} else {
					if (elhdr->span_cfg.y > elhdr->span_cfg.y_max) {
						o_vector[1] += (elhdr->span_cfg.y - elhdr->span_cfg.y_max);
						elhdr->span_cfg.y = elhdr->span_cfg.y_max;
					}
				}
			}

			Release();
		}
				
		if (b) {
			Touch(o_vector);

//			core_ogl->ResetElement(*this);
		} else {			
			Move(o_vector);
			core_ogl->ResetElement(*this, 1);
		}

		
	}


}


unsigned int OpenGL::Element::IsMovable() {
	unsigned int result(0);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		result = (hdr->_flags & (UI_64)FlagMovable);		

		Release();
	}
	
	return result;
}


void OpenGL::Element::SetDragOp(UI_64 fs) {
	bool clip_o(false);

	if (fs &= (UI_64)(FlagX0Drag | FlagX1Drag | FlagY0Drag | FlagY1Drag | FlagXYDrag))	
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		if ((hdr->config.att_flags & 0xFFFF0000) != ClipRegion) {			
			hdr->_flags |= fs;
		} else {
			clip_o = true;
		}
		Release();
	}

	if (clip_o) {
		Element selem(*this);
		selem.Down();

		if (Header * shdr = reinterpret_cast<Header *>(selem.Acquire())) {
			shdr->_flags |= fs;
			selem.Release();
		}
	}
}

void OpenGL::Element::ResetDragOp(UI_64 fs) {
	bool clip_o(false);

	fs &= (UI_64)(FlagX0Drag | FlagX1Drag | FlagY0Drag | FlagY1Drag | FlagXYDrag);

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		if ((hdr->config.att_flags & 0xFFFF0000) != ClipRegion) {
			hdr->_flags &= ~(UI_64)(FlagX0Drag | FlagX1Drag | FlagY0Drag | FlagY1Drag | FlagXYDrag);
			hdr->_flags |= fs;
		} else {
			clip_o = true;
		}
		Release();
	}

	if (clip_o) {
		Element selem(*this);
		selem.Down();

		if (Header * shdr = reinterpret_cast<Header *>(selem.Acquire())) {
			shdr->_flags &= ~(UI_64)(FlagX0Drag | FlagX1Drag | FlagY0Drag | FlagY1Drag | FlagXYDrag);
			shdr->_flags |= fs;
			selem.Release();
		}
	}
}

void OpenGL::Element::SetXSpan(float s_val) {
	bool clip_o(false);
		
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		if ((hdr->config.att_flags & 0xFFFF0000) != ClipRegion) {			
			hdr->_flags |= (UI_64)FlagXSpan;

			hdr->span_cfg.x = s_val;
			hdr->span_cfg.x_max = s_val;
		} else {
			clip_o = true;
		}
		Release();
	}

	if (clip_o) {
		Element selem(*this);
		selem.Down();

		if (Header * shdr = reinterpret_cast<Header *>(selem.Acquire())) {
			shdr->_flags |= (UI_64)FlagXSpan;

			shdr->span_cfg.x = s_val;
			shdr->span_cfg.x_max = s_val;

			selem.Release();
		}
	}
}

void OpenGL::Element::SetYSpan(float s_val) {
	bool clip_o(false);
		
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		if ((hdr->config.att_flags & 0xFFFF0000) != ClipRegion) {			
			hdr->_flags |= (UI_64)FlagYSpan;

			hdr->span_cfg.y = 0.0f;
			hdr->span_cfg.y_max = s_val;
		} else {
			clip_o = true;
		}
		Release();
	}

	if (clip_o) {
		Element selem(*this);
		selem.Down();

		if (Header * shdr = reinterpret_cast<Header *>(selem.Acquire())) {
			shdr->_flags |= (UI_64)FlagYSpan;

			shdr->span_cfg.y = 0.0f;
			shdr->span_cfg.y_max = s_val;

			selem.Release();
		}
	}
}

void OpenGL::Element::SetEFlags(UI_64 fs) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->_r_d[0] = reinterpret_cast<Core *>(hdr->_context)->chain_marker;
		
		hdr->_flags |= fs;	

		Release();
	}
}

void OpenGL::Element::ClearEFlags(UI_64 fs, UI_64 ns) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->_r_d[0] = reinterpret_cast<Core *>(hdr->_context)->chain_marker;

		hdr->_flags &= ~fs;
		hdr->_flags |= ns;		

		Release();
	}
}

unsigned int OpenGL::Element::TestMarker() {
	unsigned int result(0);

	if (Header * hdr = reinterpret_cast<Header  *>(Acquire())) {
		if (hdr->_r_d[0] > reinterpret_cast<Core *>(hdr->_context)->chain_marker) result = 1;

		Release();
	}

	return result;
}


UI_64 OpenGL::Element::TestEFlags(UI_64 fs) {	
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		fs = ((hdr->_flags & fs) ^ fs);

		Release();
	}
	return fs;
}


void OpenGL::Element::Scale(const float * scale, AttributeArray::Type  atype) {
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	elattr.Scale(scale);
}

void OpenGL::Element::Size(const float * scale, AttributeArray::Type atype) {
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	elattr.Size(scale);
}


void OpenGL::Element::Rescale(const float * scale, AttributeArray::Type atype) {
	AttributeArray elattr;

	InitAttributeArray(AttributeArray::Vertex, elattr);

	elattr.SetScale(scale);
}

void OpenGL::Element::SetXColor(unsigned short xidx) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->xc_index = xidx & 0x7FFF;

		Release();
	}
}

void OpenGL::Element::SetLargeCount(unsigned int l_count) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->xc_index = l_count;

		Release();
	}
}

void OpenGL::Element::SetFlats(const int * abcd) {
	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		reinterpret_cast<int &>(hdr->span_cfg.x) = abcd[0];
		reinterpret_cast<int &>(hdr->span_cfg.y) = abcd[1];
		reinterpret_cast<int &>(hdr->span_cfg.x_max) = abcd[2];
		reinterpret_cast<int &>(hdr->span_cfg.y_max) = abcd[3];

		Release();
	}
}

void OpenGL::Element::SetColor(const float * rgba) {
	AttributeArray elattr;

	InitAttributeArray(AttributeArray::Color, elattr);

	elattr.Clear(rgba);
}

void OpenGL::Element::SetSpecialColor() {
	AttributeArray elattr;

	InitAttributeArray(AttributeArray::Color, elattr);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		elattr.Clear(&elhdr->span_cfg.x);

		Release();
	}
}

void OpenGL::Element::SetDefaultRect(AttributeArray::Type atype) {
	float * vecs(0);
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
		vecs = reinterpret_cast<float *>(ahdr + 1);

		vecs[4] = 1.0f;
		vecs[9] = 1.0f;
		vecs[12] = 1.0f;
		vecs[13] = 1.0f;

		elattr.Release();
	}	
	
}

void OpenGL::Element::ConfigurePage(float * wh_ptr) {
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {		
		elhdr->root_cfg._width = reinterpret_cast<int *>(wh_ptr)[0];
		elhdr->root_cfg._height = reinterpret_cast<int *>(wh_ptr)[1];
		elhdr->root_cfg._maxW = reinterpret_cast<int *>(wh_ptr)[2];
		elhdr->root_cfg._maxH = reinterpret_cast<int *>(wh_ptr)[3];

		Release();
	}
}


void OpenGL::Element::Move(const float * xyzt, AttributeArray::Type atype) {
	Element upel;
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	elattr.Translate(xyzt);

	if (Header * ehdr = reinterpret_cast<Header*>(Acquire())) {
		if (((ehdr->config.att_flags & 0xFFFF0000) == SceneRegion) && (ehdr->root_cfg._maxW | ehdr->root_cfg._maxH)) {
			upel = *this;
			upel.Up();

			upel.SetState(PageTest(reinterpret_cast<float *>(&ehdr->root_cfg), xyzt));

		}

		Release();
	}
}

void OpenGL::Element::SelectTLevel(unsigned int il) {
	AttributeArray elattr;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		if (el_hdr->config.att_flags & (1 << AttributeArray::TexCoord)) {
			InitAttributeArray(AttributeArray::TexCoord, elattr);
			
			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
				for (unsigned int i(0);i<a_hdr->coCount;i++) reinterpret_cast<float *>(a_hdr + 1)[i*4 + 2] = il;

				elattr.Release();
			}
		}
		Release();
	}
}

unsigned int OpenGL::Element::GetTLevel() {
	unsigned int result(0);
	AttributeArray elattr;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		if (el_hdr->config.att_flags & (1 << AttributeArray::TexCoord)) {
			InitAttributeArray(AttributeArray::TexCoord, elattr);
			
			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
				result = reinterpret_cast<float *>(a_hdr + 1)[2];

				elattr.Release();
			}
		}
		Release();
	}

	return result;
}

void OpenGL::Element::RelocateTex(unsigned int tl) {
	AttributeArray elattr;

	InitAttributeArray(AttributeArray::TexCoord, elattr);

	if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
		for (unsigned int i(0);i<a_hdr->coCount;i++) {
			reinterpret_cast<float *>(a_hdr+1)[i*4 + 2] = tl;

		}

		elattr.Release();
	}

}


void OpenGL::Element::Relocate(const float * xyzt, AttributeArray::Type atype) {
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	elattr.SetOffset(xyzt);

}

void OpenGL::Element::RelocateXY(const float * xyzt, AttributeArray::Type atype) {
	AttributeArray elattr;

	InitAttributeArray(atype, elattr);

	elattr.SetOffsetXY(xyzt);

}

void OpenGL::Element::LocationSwap(Element & refel) {
	AttributeArray a_arr, b_arr;

	InitAttributeArray(AttributeArray::Vertex, a_arr);
	refel.InitAttributeArray(AttributeArray::Vertex, b_arr);

	a_arr.SwapOffset(b_arr);
}

void OpenGL::Element::SetDefaultBlink(unsigned int delay_0, unsigned int delay_1, unsigned int color_0, unsigned int color_1, unsigned int xcolor_01) {
	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {

		elhdr->root_cfg._width = delay_0;
		elhdr->root_cfg._height = delay_1;
		elhdr->root_cfg._maxW = color_0;
		elhdr->root_cfg._maxH = color_1;

		elhdr->root_cfg._fullW = (xcolor_01 & 0x0000FFFF);
		elhdr->root_cfg._fullH = (xcolor_01 >> 16);

		Release();
	}
}


void OpenGL::Element::Show() {
	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
		if (ehdr->_flags & (UI_64)FlagAuHide) {
			(unsigned int &)ehdr->root_cfg._fullW = reinterpret_cast<Core *>(ehdr->_context)->render_count + (unsigned int)ehdr->root_cfg._fullH;
			ehdr->_flags |= (UI_64)FlagHideOn;
		}
		
		if ((ehdr->_flags & (UI_64)FlagVisible) == 0) {
			ehdr->_flags |= (UI_64)FlagVisible;

			if ((ehdr->_flags & (UI_64)FlagApp) && Is(APPlane::type_id)) {
				APPlane::PopApp(*this);
			}

			ResetState(StateShow);
		}

		Release();
	}
	

}

void OpenGL::Element::Hide() {
	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
		if (ehdr->_flags & (UI_64)FlagVisible) {
			ehdr->_flags &= ~(UI_64)(FlagVisible | FlagHoover | FlagLeave | FlagMouseDown | FlagMouseUp | FlagFocus | FlagLostFocus | FlagHideOn);
		
			ResetState(StateHide);
		}
		Release();
	}
}

void OpenGL::Element::SetXTra(UI_64 x_val) {
	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		el_hdr->x_tra = x_val;

		Release();
	}
}

UI_64 OpenGL::Element::GetXTra() {
	UI_64 result(0);

	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		result = el_hdr->x_tra;

		Release();
	}


	return result;
}


OpenGL::Core * OpenGL::Element::GetContext() {
	Core * result(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		result = reinterpret_cast<Core *>(el_hdr->_context);
		Release();
	}

	return result;
}

void OpenGL::Element::Push() {

	AttributeArray elattr;
	InitAttributeArray(AttributeArray::Vertex, elattr);

	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {

		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
			ahdr->translate_vector[2] = ehdr->location;
			ahdr->reserved[3] |= 1;
		
			elattr.Release();
		}
		Release();
	}
}

void OpenGL::Element::Pop(unsigned int p_flag) {
	Element parent(*this);
	parent.Up();

	AttributeArray elattr;
	InitAttributeArray(AttributeArray::Vertex, elattr);
	
	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
/*
		if (p_flag & (UI_64)FlagPop) {
			ehdr->_flags |= (UI_64)FlagPop;
			reinterpret_cast<Core *>(ehdr->_context)->SetPop(this);
		}
*/
		if (Element::Header * p_hdr = reinterpret_cast<Element::Header *>(parent.Acquire())) {
			if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
				ahdr->translate_vector[2] = ehdr->location - p_hdr->depth;
				ahdr->reserved[3] |= 1;
		
				elattr.Release();
			}
			parent.Release();
		}
		Release();
	}
}


void OpenGL::Element::SetAttributeBox(AttributeArray::Type atype, float w, float h) {
	AttributeArray elattr;
	InitAttributeArray(atype, elattr);

	elattr.SetBox(w, h);
}


void OpenGL::Element::SetFitMode(FitType fty, AttributeArray::Type atype) {
	float parent_box[16], box_vals[16];
	AttributeArray elattr;

	Element parent = *this;
	parent.Up();

	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
		
		if (fty != AsIs) {
			if (parent) {
				if (const Header * phdr = reinterpret_cast<Header *>(parent.Acquire())) {
					if ((phdr->config.att_flags & 0xFFFF0000) == SceneRegion) {
						parent.Release();
						parent.Up();

						phdr = reinterpret_cast<Header *>(parent.Acquire());
					}
				
					if(phdr) {
						parent.InitAttributeArray(atype, elattr);
						elattr.GetBox(parent_box);
						if ((phdr->config.att_flags & 0xFFFF0000) == ClipRegion) {
							parent.Release();
							parent.Down();
						}

						parent.InitAttributeArray(atype, elattr);
						elattr.LocalDAdjust(parent_box, 4);
					}
				}
			}

			InitAttributeArray(atype, elattr);
	//		elattr.SetStretchMask(0, 0, 0);
			elattr.GetBox(box_vals);

			parent_box[4] -= parent_box[0];
			reinterpret_cast<unsigned int *>(parent_box)[4] &= 0x7FFFFFFF;
			parent_box[5] -= parent_box[1];
			reinterpret_cast<unsigned int *>(parent_box)[5] &= 0x7FFFFFFF;

			box_vals[4] -= box_vals[0];
			reinterpret_cast<unsigned int *>(box_vals)[4] &= 0x7FFFFFFF;
			box_vals[5] -= box_vals[1];
			reinterpret_cast<unsigned int *>(box_vals)[5] &= 0x7FFFFFFF;



			parent_box[4] /= box_vals[4];
			parent_box[5] /= box_vals[5];
			parent_box[6] = 1.0f/ehdr->depth;
			parent_box[7] = 1.0f;
			parent_box[10] = ehdr->location; parent_box[11] = 1.0f;

			switch (fty & (SCALE_H | SCALE_V)) {
				case FitBox:				
					SetAnchorMode(Center);

					if (parent_box[4] > parent_box[5]) {
						unsigned int * ftyp(reinterpret_cast<unsigned int *>(&fty));

						parent_box[4] = parent_box[5];
						ftyp[0] |= SCALE_HV;
					} else {
						parent_box[5] = parent_box[4];
					}

					parent_box[8] -= parent_box[4]*box_vals[8]; parent_box[9] -= parent_box[4]*box_vals[9];

					if (fty & SCALE_A) {
						if (parent_box[4] > 1.0f) {
							parent_box[4] = 1.0f - parent_box[4];

							parent_box[8] -= parent_box[4]*box_vals[8]; parent_box[9] -= parent_box[4]*box_vals[9];
							parent_box[4] = 1.0f;
						} else {
							if (fty & SCALE_HV) parent_box[9] = 0.0f;
							else parent_box[8] = 0.0f;;
						}
					}

					elattr.SetOffset(parent_box+8);
					elattr.SetScale(parent_box+4);

					

				break;
				case FitWidth:
					if (ehdr->config.anchor & VANCHOR_CENTER) {
						parent_box[5] = 1.0f;
						parent_box[8] = 0.0f;

						parent_box[9] -= parent_box[5]*box_vals[9];

						elattr.SetOffset(parent_box+8);
						elattr.SetScale(parent_box+4);
					}
					
				break;
				case FitHeight:
					if (ehdr->config.anchor & HANCHOR_CENTER) {
						parent_box[4] = 1.0f;
						parent_box[9] = 0.0f;

						parent_box[8] -= parent_box[4]*box_vals[8];

						elattr.SetOffset(parent_box+8);
						elattr.SetScale(parent_box+4);
					}

				break;
			}

		}

		ehdr->config.anchor &= 0x00FF;
		ehdr->config.anchor |= (fty & 0xFF00);

		Release();
	}
}

void OpenGL::Element::SetAnchorMode(AnchorType aty, AttributeArray::Type atype) {
	// sets the vector, and point
	AttributeArray elattr;

	if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
		hdr->config.anchor &= 0xFF00;
		hdr->config.anchor |= (aty & 0x00FF);

		InitAttributeArray(atype, elattr);
		elattr.SetOffsetMask(aty);
		
		Release();
	}


}



void OpenGL::Element::ForwardStretchVector(Element & aelem, bool clearv) {
	AttributeArray a_arr, b_arr;
	if (Header * zhdr = reinterpret_cast<Header *>(aelem.Acquire())) {
		b_arr.Set(AttributeArray::_attributes_pool, zhdr->domain_desc[AttributeArray::Vertex].array_id);

		if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
			a_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[AttributeArray::Vertex].array_id);

			b_arr.CopyStretchVector(a_arr);
			if (clearv) a_arr.SetStretchVector(0.0, 0.0, 0.0);

			Release();
		}
		aelem.Release();
	}
}

void OpenGL::Element::ClearStretch() {
	AttributeArray v_arr;

	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
		v_arr.Set(AttributeArray::_attributes_pool, ehdr->domain_desc[AttributeArray::Vertex].array_id);

		v_arr.SetStretchVector(0.0, 0.0, 0.0);

		Release();
	}
}

void OpenGL::Element::SetStretch(int x, int y, int z) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);
	v_arr.SetStretchMask(x, y, z);
}


UI_64 OpenGL::Element::ApplyAnchor(unsigned int fidx, bool inco) {
	UI_64 result(0);
	AttributeArray v_arr, t_arr;
	

	if (Header * elhdr = reinterpret_cast<Header *>(Acquire())) {
		elhdr->_flags &= ~(UI_64)FlagAnchor;
		elhdr->findex = fidx;

		v_arr.Set(AttributeArray::_attributes_pool, elhdr->domain_desc[AttributeArray::Vertex].array_id);

		if ((elhdr->config.anchor & 0x00FF) != None)
		if ((elhdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) {
			v_arr.ResetStretch();
			result = 1;

		} else {
			if ((elhdr->config.att_flags & 0xFFFF0000) != Element::SceneRegion) {
				result = v_arr.DoAnchor();
				if ((elhdr->config.anchor & 0xFF00) != AsIs) SetFitMode(static_cast<FitType>(elhdr->config.anchor & 0xFF00));
			} else result = 1;

			if (!inco) v_arr.StretchAdjust();
		}

		if (result) {
			v_arr.CalculateBox(static_cast<AnchorType>(elhdr->config.anchor & 0x00FF));

			if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
				if ((v_hdr->motion_options == -1) && (v_hdr->motion_proc)) {
					v_hdr->motion_proc((float *)this, 0);

				}

				v_arr.Release();
			}
		}

		Release();
	}

	return result;	
}

void OpenGL::Element::SetRect(const float * i_box) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {

		reinterpret_cast<float *>(a_hdr+1)[0] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[1] = 0.0f; //i_box[3];
		reinterpret_cast<float *>(a_hdr+1)[2] = 0.0f;

		reinterpret_cast<float *>(a_hdr+1)[4] = i_box[2];
		reinterpret_cast<float *>(a_hdr+1)[5] = 0.0f; // i_box[3];
		reinterpret_cast<float *>(a_hdr+1)[6] = 0.0f;

		reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[9] = i_box[3]; // 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[10] = 0.0f;

		reinterpret_cast<float *>(a_hdr+1)[12] = i_box[2];
		reinterpret_cast<float *>(a_hdr+1)[13] = i_box[3]; // 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[14] = 0.0f;
		
		v_arr.Release();
	}

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		v_arr.CalculateBox(static_cast<AnchorType>(el_hdr->config.anchor & 0x00FF));
		Release();
	}
}

void OpenGL::Element::SetRectTexture(const float * i_box, unsigned int tid, unsigned int level) {
	AttributeArray t_arr;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		el_hdr->_flags |= ((UI_64)level & (UI_64)(FlagNegTex | FlagMaskTex));
		el_hdr->config.tex_unit_selector = (level & LAYERED_TEXTURE);
		el_hdr->config.tex_unit_selector <<= 32;
		el_hdr->config.tex_unit_selector |= tid;

		Release();
	}

	InitAttributeArray(AttributeArray::TexCoord, t_arr);

	level &= (Core::max_texture_layers - 1);

	if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(t_arr.Acquire())) {

		reinterpret_cast<float *>(a_hdr+1)[0] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[1] = i_box[3];
		reinterpret_cast<float *>(a_hdr+1)[2] = level;

		reinterpret_cast<float *>(a_hdr+1)[4] = i_box[2];
		reinterpret_cast<float *>(a_hdr+1)[5] = i_box[3];
		reinterpret_cast<float *>(a_hdr+1)[6] = level;

		reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[9] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[10] = level;

		reinterpret_cast<float *>(a_hdr+1)[12] = i_box[2];
		reinterpret_cast<float *>(a_hdr+1)[13] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[14] = level;

		t_arr.Release();
	}
}


UI_64 OpenGL::Element::CreateEBorder(float offset, unsigned int flags) {
	__declspec(align(16)) float t_vals[8] = {offset, offset, 0.0, 0.0,		0.0 ,0.0, 0.0, 0.0};
	float * bcoords(0);
	unsigned int f_vals(0);
	DrawType clip_flag(Element::Border);

	Core * core_ptr(0);

	Element bdr;
	AttributeArray elattr, blattr;	

	UI_64 result(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {		
		core_ptr = reinterpret_cast<Core *>(elhdr->_context);
		if ((elhdr->config.att_flags & 0xFFFF0000) == ClipRegion) (unsigned int &)clip_flag |= ClipSpecialFlag;
		f_vals = elhdr->_flags & (UI_64)FlagBorder;

		Release();
		elhdr = 0;

		if ((!f_vals) && (core_ptr)) {
			result = core_ptr->CreateElement(clip_flag, bdr, 4, this, 0x80000000);

			if (result != -1) { // do stuff
				bdr.InitAttributeArray(AttributeArray::Vertex, blattr);
				
				InitAttributeArray(AttributeArray::Vertex, elattr);
				
				blattr.Copy(elattr);
				
				if (AttributeArray::Header * bhdr = reinterpret_cast<AttributeArray::Header *>(blattr.Acquire())) {
					bcoords = reinterpret_cast<float *>(bhdr + 1);

					if (core_ptr->IsRoot(*this)) {
						core_ptr->RootAdjust(t_vals, 1);
						
					}
					

					t_vals[4] = bcoords[8];
					t_vals[5] = bcoords[9];
					t_vals[6] = bcoords[10];
					t_vals[7] = bcoords[11];

					bcoords[8] = bcoords[12];
					bcoords[9] = bcoords[13];
					bcoords[10] = bcoords[14];
					bcoords[11] = bcoords[15];

					bcoords[12] = t_vals[4];
					bcoords[13] = t_vals[5];
					bcoords[14] = t_vals[6];
					bcoords[15] = t_vals[7];


					CalcBorder(bcoords, 4, t_vals);

					blattr.Release();
				}


				blattr.SetStretchMask(-1, -1, 0);
				bdr.SetAnchorMode(LeftTopFront);
				blattr.CalculateBox(LeftTopFront);

				bdr.InitAttributeArray(AttributeArray::Color, blattr);
			}			
			

			if (elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
				elhdr->_flags |= (UI_64)FlagBorder;

				Release();
			}
				
			if (Element::Header * bhdr = reinterpret_cast<Element::Header *>(bdr.Acquire())) {

				bhdr->_reserved = (flags & 0xFF000000);
				bdr.Release();
			}
		}
	}

	return result;
}

void OpenGL::Element::SetEBorderColor(const float * rgba) {
	Element bdr(*this);
	bdr.Down();

	if (Header * ehdr = reinterpret_cast<Header *>(Acquire())) {
		if (ehdr->_flags & (UI_64)FlagBorder) {
			if ((ehdr->config.att_flags & 0xFFFF0000) == ClipRegion) {
				bdr.Left();
			}

			reinterpret_cast<Element &>(bdr).SetColor(rgba);
		}

		Release();
	}	
}


void OpenGL::Element::GetRightMargin(float * mgr) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
		mgr[0] = ahdr->translate_vector[0];
		mgr[1] = ahdr->translate_vector[1];
		mgr[2] = ahdr->scale_vector[0]*reinterpret_cast<float *>(ahdr+1)[12];
		mgr[3] = ahdr->scale_vector[1]*reinterpret_cast<float *>(ahdr+1)[13];

		v_arr.Release();
	}
}

void OpenGL::Element::GetRightCorner(float * mgr) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
		mgr[0] = ahdr->translate_vector[0];
		mgr[1] = ahdr->translate_vector[1];
		mgr[2] = reinterpret_cast<float *>(ahdr+1)[12];
		mgr[3] = reinterpret_cast<float *>(ahdr+1)[13];

		v_arr.Release();
	}
}

void OpenGL::Element::GetScaleVector(float * s_vec) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
		s_vec[0] = ahdr->scale_vector[0];
		s_vec[1] = ahdr->scale_vector[1];

		v_arr.Release();
	}

}

void OpenGL::Element::MoveRight(float w) {
	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
		ahdr->translate_vector[0] = w - ahdr->scale_vector[0]*reinterpret_cast<float *>(ahdr+1)[4];		
		ahdr->reserved[3] |= 1;

		v_arr.Release();
	}
}

void OpenGL::Element::MotionReset(unsigned int a_mask) {
	AttributeArray a_arr;

	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		a_mask &= el_hdr->config.att_flags;

		Release();
	}

	for (unsigned int atype(AttributeArray::Vertex);a_mask;a_mask>>=1, atype++) {
		if (a_mask & 1) {
			InitAttributeArray(reinterpret_cast<AttributeArray::Type &>(atype), a_arr);

			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
				a_hdr->motion_transform[0] = 1.0f; a_hdr->motion_transform[1] = 0.0f; a_hdr->motion_transform[2] = 0.0f; a_hdr->motion_transform[3] = 0.0f;
				a_hdr->motion_transform[4] = 0.0f; a_hdr->motion_transform[5] = 1.0f; a_hdr->motion_transform[6] = 0.0f; a_hdr->motion_transform[7] = 0.0f;
				a_hdr->motion_transform[8] = 0.0f; a_hdr->motion_transform[9] = 0.0f; a_hdr->motion_transform[10] = 1.0f; a_hdr->motion_transform[11] = 0.0f;
				a_hdr->motion_transform[12] = 0.0f; a_hdr->motion_transform[13] = 0.0f; a_hdr->motion_transform[14] = 0.0f; a_hdr->motion_transform[15] = 1.0f;

				a_arr.Release();
			}
		}
	}
}


void OpenGL::Element::SetTrajectory(void (__fastcall * m_proc)(float *, unsigned int), unsigned int option, AttributeArray::Type atype) {
	AttributeArray a_arr;

	InitAttributeArray(atype, a_arr);

	if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
		a_hdr->motion_options = option;
		a_hdr->motion_proc = m_proc;

		a_arr.Release();
	}
}

void OpenGL::Element::NextLocation() {
	unsigned int a_cfg(0), restart_flag(0);

	AttributeArray a_arr;

	if (Header * el_hdr = reinterpret_cast<Header *>(Acquire())) {
		a_cfg = el_hdr->config.att_flags & 0x0000FFFF;
		if (el_hdr->_flags & (UI_64)FlagMotionOn) {
			el_hdr->_flags ^= (UI_64)FlagMotionOn;
			restart_flag = 1;
		}

		Release();
	}

	for (unsigned int a_type(0);a_cfg;a_type++) {
		if (a_cfg & 1) {
			InitAttributeArray((AttributeArray::Type)a_type, a_arr);

			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
				if (a_hdr->motion_proc) {
					a_hdr->motion_proc(a_hdr->motion_transform, restart_flag);

					a_hdr->reserved[3] = 1;
				}

				a_arr.Release();
			}
		}

		a_cfg >>= 1;
	}
}

UI_64 OpenGL::Element::SetTimeOut(unsigned int tval) {
	UI_64 result(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		el_hdr->_flags |= (UI_64)FlagAuHide;
		
		el_hdr->root_cfg._fullW = 0;
		(unsigned int &)el_hdr->root_cfg._fullH = tval;

		Release();
	}

	return result;
}
