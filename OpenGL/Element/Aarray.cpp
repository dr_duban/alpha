/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\SysUtils.h"


#include "OpenGL\Element.h"

#include "Math\CalcSpecial.h"


// AttributeArray ========================================================================================================================================================================================
Memory::Allocator * OpenGL::AttributeArray::_attributes_pool = 0;

int OpenGL::AttributeArray::Initialize() {
	wchar_t pool_name[] = {15, L'a',L't', L't', L'r', L'i', L'b', L'u', L't', L'e', L's', L' ', L'p', L'o', L'o', L'l'};
	int result(0);
	if (_attributes_pool) return 0;

	if (_attributes_pool = new Memory::Allocator(0)) {
		result = _attributes_pool->Init(new Memory::Buffer(), 0x00100000, pool_name);
	} else result = ERROR_OUTOFMEMORY;

	return result;
}

int OpenGL::AttributeArray::Finalize() {
	if (_attributes_pool) delete _attributes_pool;
	_attributes_pool = 0;
	return 0;
}

OpenGL::AttributeArray::AttributeArray() {
	SetTID(type_id);
}

OpenGL::AttributeArray::AttributeArray(const AttributeArray & ref) : MatrixF(ref) {

}

OpenGL::AttributeArray::~AttributeArray() {

}

void OpenGL::AttributeArray::Destroy() {

	MatrixF::Destroy();
}


void OpenGL::AttributeArray::Clear(const float * rgba) {	
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		Calc::FloatClear4N(reinterpret_cast<float *>(ahdr + 1), rgba, ahdr->coCount);

		Release();
	}


}

void OpenGL::AttributeArray::SetStretchMask(int x, int y, int z) {	
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->stretch_mask[0] = x;
		ahdr->stretch_mask[1] = y;
		ahdr->stretch_mask[2] = z;

		Release();
	}
}

void OpenGL::AttributeArray::SetOffsetMask(float x, float y, float z) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->offset_mask[0] = x;
		ahdr->offset_mask[1] = y;
		ahdr->offset_mask[2] = z;

		Release();
	}
}


void OpenGL::AttributeArray::Polygon(unsigned int vcount) {
	float sin_vl[2], gon_vec[2];

	gon_vec[0] = 0.0f;
	gon_vec[1] = 1.0f;

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (vcount <= ahdr->coCount) {
			sin_vl[1] = Calc::FloatSin(360.f/ahdr->coCount);
			sin_vl[0] = Calc::FloatSqrt(1.0f - sin_vl[1]*sin_vl[1]);
			
			GonCalc(reinterpret_cast<float *>(ahdr + 1), ahdr->coCount, sin_vl[0], sin_vl[1]);

		}

		Release();
	}
}

void OpenGL::AttributeArray::StretchAdjust() {
	__declspec(align(16)) struct m_struct {
		MatrixF::Header _mhdr;
		float comps[16];
	} xma = {	{4, 1, 4, 1, 4, 0.0, 0, 0}, 
	
				{1.0f, 0.0, 0.0, 0.0,		0.0, 1.0f, 0.0, 0.0,	0.0, 0.0, 1.0f, 0.0,	0.0, 0.0, 0.0, 1.0f}
			};

	int sima[4] = {0, 0, 0, 0};


	GetLocalTransform(xma.comps);
	
	
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		sima[0] = reinterpret_cast<int *>(ahdr->stretch_vector)[0] & 0x80000000;
		sima[1] = reinterpret_cast<int *>(ahdr->stretch_vector)[1] & 0x80000000;
		sima[2] = reinterpret_cast<int *>(ahdr->stretch_vector)[2] & 0x80000000;
							
		MF_InverseMultiply44(&xma._mhdr, xma.comps, 1, ahdr->stretch_vector);

		reinterpret_cast<int *>(ahdr->stretch_vector)[0] &= 0x7FFFFFFF;
		reinterpret_cast<int *>(ahdr->stretch_vector)[0] |= sima[0];
		reinterpret_cast<int *>(ahdr->stretch_vector)[0] &= reinterpret_cast<int *>(ahdr->stretch_vector)[4];

		reinterpret_cast<int *>(ahdr->stretch_vector)[1] &= 0x7FFFFFFF;
		reinterpret_cast<int *>(ahdr->stretch_vector)[1] |= sima[1];
		reinterpret_cast<int *>(ahdr->stretch_vector)[1] &= reinterpret_cast<int *>(ahdr->stretch_vector)[5];

		reinterpret_cast<int *>(ahdr->stretch_vector)[2] &= 0x7FFFFFFF;
		reinterpret_cast<int *>(ahdr->stretch_vector)[2] |= sima[2];
		reinterpret_cast<int *>(ahdr->stretch_vector)[2] &= reinterpret_cast<int *>(ahdr->stretch_vector)[6];

		Release();
	}
}

void OpenGL::AttributeArray::GlobalDAdjust(float * bvals, unsigned int vcount) {
	__declspec(align(16)) struct m_struct {
		MatrixF::Header _mhdr;
		float comps[16];
	} xma = {	{4, 1, 4, 1, 4, 0.0, 0, 0}, 
	
				{1.0f, 0.0, 0.0, 0.0,		0.0, 1.0f, 0.0, 0.0,	0.0, 0.0, 1.0f, 0.0,	0.0, 0.0, 0.0, 1.0f}
			};

	

	GetGlobalTransform(xma.comps);

	MF_InverseMultiply44(&xma._mhdr, xma.comps, vcount, bvals);
}

void OpenGL::AttributeArray::GlobalUAdjust(float * d_vals, unsigned int vcount) {
	__declspec(align(16)) struct m_struct {
		MatrixF::Header _mhdr;
		float comps[16];
	} xma = {	{4, 1, 4, 1, 4, 0.0, 0, 0}, 
	
				{1.0f, 0.0, 0.0, 0.0,		0.0, 1.0f, 0.0, 0.0,	0.0, 0.0, 1.0f, 0.0,	0.0, 0.0, 0.0, 1.0f}
			};

	

	GetGlobalTransform(xma.comps);

	Calc::MF_Multiply44(xma.comps, vcount, d_vals, d_vals);
}


void OpenGL::AttributeArray::LocalDAdjust(float * bvals, unsigned int vcount) {
	__declspec(align(16)) struct m_struct {
		MatrixF::Header _mhdr;
		float comps[16];
	} xma = {	{4, 1, 4, 1, 4, 0.0, 0, 0}, 
	
				{1.0f, 0.0, 0.0, 0.0,		0.0, 1.0f, 0.0, 0.0,	0.0, 0.0, 1.0f, 0.0,	0.0, 0.0, 0.0, 1.0f}
			};

	

	GetLocalTransform(xma.comps);

	MF_InverseMultiply44(&xma._mhdr, xma.comps, vcount, bvals);
}

void OpenGL::AttributeArray::LocalUAdjust(float * d_vals, unsigned int vcount) {
	__declspec(align(16)) struct m_struct {
		MatrixF::Header _mhdr;
		float comps[16];
	} xma = {	{4, 1, 4, 1, 4, 0.0, 0, 0}, 
	
				{1.0f, 0.0, 0.0, 0.0,		0.0, 1.0f, 0.0, 0.0,	0.0, 0.0, 1.0f, 0.0,	0.0, 0.0, 0.0, 1.0f}
			};

	

	GetLocalTransform(xma.comps);

	Calc::MF_Multiply44(xma.comps, vcount, d_vals, d_vals);
}

void OpenGL::AttributeArray::Copy(AttributeArray & elattr) {
	if (AttributeArray::Header * bhdr = reinterpret_cast<AttributeArray::Header *>(Acquire())) {
		if (AttributeArray::Header * phdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
			Calc::FloatCopy4(reinterpret_cast<float *>(bhdr + 1), reinterpret_cast<float *>(phdr + 1), bhdr->coCount);
						
			elattr.Release();
		}

		Release();
	}
}


void OpenGL::AttributeArray::SetOffsetMask(AnchorType atype) {

	if (atype != Special)
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
				
		if (atype & HANCHOR_LEFT) {
			ahdr->box[0] = ahdr->min[0];
			ahdr->offset_mask[0] = 0.0f;
		} else {
			if (atype & HANCHOR_CENTER) {
				ahdr->box[0] = (ahdr->min[0] + ahdr->max[0])*0.5f;
				ahdr->offset_mask[0] = 0.5f;
			} else {
				ahdr->box[0] = ahdr->max[0];
				ahdr->offset_mask[0] = 1.0f;
			}
		}

		if (atype & VANCHOR_TOP) {
			ahdr->box[1] = ahdr->max[1];
			ahdr->offset_mask[1] = 1.0f;
		} else {
			if (atype & VANCHOR_CENTER) {
				ahdr->box[1] = (ahdr->min[1] + ahdr->max[1])*0.5f;
				ahdr->offset_mask[1] = 0.5f;
			} else {
				ahdr->box[1] = ahdr->min[1];
				ahdr->offset_mask[1] = 0.0f;
			}
		}

		if (atype & DANCHOR_FRONT) {
			ahdr->box[2] = ahdr->min[2];
			ahdr->offset_mask[2] = 0.0f;
		} else {
			if (atype & DANCHOR_CENTER) {
				ahdr->box[2] = (ahdr->min[2] + ahdr->max[2])*0.5f;
				ahdr->offset_mask[2] = 0.5f;
			} else {
				ahdr->box[2] = ahdr->max[2];
				ahdr->offset_mask[2] = 1.0f;
			}
		}

		Release();
	}


}

void OpenGL::AttributeArray::CalculateBox(AnchorType atype) {

	if (atype != Special)
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {

		GetRange(reinterpret_cast<float *>(ahdr + 1), ahdr->coCount, ahdr->min);
				
		if (atype & HANCHOR_LEFT) {
			ahdr->box[0] = ahdr->min[0];
			ahdr->offset_mask[0] = 0.0f;
		} else {
			if (atype & HANCHOR_CENTER) {
				ahdr->box[0] = (ahdr->min[0] + ahdr->max[0])*0.5f;
				ahdr->offset_mask[0] = 0.5f;
			} else {
				ahdr->box[0] = ahdr->max[0];
				ahdr->offset_mask[0] = 1.0f;
			}
		}

		if (atype & VANCHOR_TOP) {
			ahdr->box[1] = ahdr->max[1];
			ahdr->offset_mask[1] = 1.0f;
		} else {
			if (atype & VANCHOR_CENTER) {
				ahdr->box[1] = (ahdr->min[1] + ahdr->max[1])*0.5f;
				ahdr->offset_mask[1] = 0.5f;
			} else {
				ahdr->box[1] = ahdr->min[1];
				ahdr->offset_mask[1] = 0.0f;
			}
		}

		if (atype & DANCHOR_FRONT) {
			ahdr->box[2] = ahdr->min[2];
			ahdr->offset_mask[2] = 0.0f;
		} else {
			if (atype & DANCHOR_CENTER) {
				ahdr->box[2] = (ahdr->min[2] + ahdr->max[2])*0.5f;
				ahdr->offset_mask[2] = 0.5f;
			} else {
				ahdr->box[2] = ahdr->max[2];
				ahdr->offset_mask[2] = 1.0f;
			}
		}


		Release();
	}

}

void OpenGL::AttributeArray::GetBox(float * bpo) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		Calc::FloatCopy16(bpo, ahdr->min, 1);

		Release();
	}
}

void OpenGL::AttributeArray::SetBox(float w, float h) {

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		SetRange(reinterpret_cast<float *>(ahdr + 1), w, h, ahdr->coCount);

		Release();
	}
}

void OpenGL::AttributeArray::SetBox(AttributeArray & sarray) {
	float * apo(0);
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		apo = reinterpret_cast<float *>(ahdr + 1);

		if (ahdr->coCount>=4)
		if (Header * s_ahdr = reinterpret_cast<Header *>(sarray.Acquire())) {
			apo[4] = s_ahdr->max[0] - s_ahdr->min[0];
			apo[9] = s_ahdr->max[1] - s_ahdr->min[1];
			apo[12] = apo[4];
			apo[13] = apo[9];

			sarray.Release();
		}

		Release();
	}

}

void OpenGL::AttributeArray::UpdateStretchVector(float xo, float yo, float zo) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->stretch_vector[0] += xo;
		ahdr->stretch_vector[1] += yo;
		ahdr->stretch_vector[2] += zo;
		
		Release();
	}
}

void OpenGL::AttributeArray::SetStretchVector(float xo, float yo, float zo) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->stretch_vector[0] = xo;
		ahdr->stretch_vector[1] = yo;
		ahdr->stretch_vector[2] = zo;
		
		Release();
	}
}

void OpenGL::AttributeArray::CopyStretchVector(AttributeArray & refa) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (Header * ref_hdr = reinterpret_cast<Header *>(refa.Acquire())) {
			ahdr->stretch_vector[0] = ref_hdr->stretch_vector[0];
			ahdr->stretch_vector[1] = ref_hdr->stretch_vector[1];
			ahdr->stretch_vector[2] = ref_hdr->stretch_vector[2];

			refa.Release();
		}
		Release();
	}
}



void OpenGL::AttributeArray::ResetStretch() {
	float * vepo(0);
	if (Header * ahdr = reinterpret_cast<AttributeArray::Header *>(Acquire())) {
		vepo = reinterpret_cast<float *>(ahdr + 1);

		if (ahdr->stretch_mask[0]) {
			vepo[4] += ahdr->stretch_vector[0];
			vepo[12] += ahdr->stretch_vector[0];
		}

		if (ahdr->stretch_mask[1]) {
			vepo[9] += ahdr->stretch_vector[1];		
			vepo[13] += ahdr->stretch_vector[1];
		}

		Release();
	}
}

UI_64 OpenGL::AttributeArray::DoAnchor() {
	UI_64 result(0);

	if (Header * ahdr = reinterpret_cast<AttributeArray::Header *>(Acquire())) {		
		result = DoAnchor(ahdr->min, reinterpret_cast<float *>(ahdr + 1), ahdr->coCount);
		ahdr->reserved[3] |= 1;

		Release();
	}

	return result;
}


UI_64 OpenGL::AttributeArray::Create(unsigned int aCount, const float * buffer, unsigned int fflag) {	
	UI_64 result = New(type_id, sizeof(Header) + (((aCount+3)>>2)<<6), _attributes_pool);

	if (result != -1) {
		
		if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
//			System::MemoryFill(hdr, sizeof(Header), 0);

			MatrixF::Init(hdr, 4, aCount);			

			hdr->scale_vector[0] = hdr->scale_vector[1] = hdr->scale_vector[2] = hdr->scale_vector[3] = 1.0f;
			hdr->xy_spin[0] = 1.0f; hdr->xy_spin[1] = 0.0f; hdr->xy_spin[2] = 0.0f;

			hdr->motion_transform[0] = hdr->motion_transform[5] = hdr->motion_transform[10] = hdr->motion_transform[15] = 1.0f;
			hdr->global_transform[0] = hdr->global_transform[5] = hdr->global_transform[10] = hdr->global_transform[15] = 1.0f;
			hdr->local_transform[0] = hdr->local_transform[5] = hdr->local_transform[10] = hdr->local_transform[15] = 1.0f;


			if (buffer) {
				if (fflag) ClearArray(hdr+1, aCount, buffer);
				else Calc::FloatCopy4(hdr+1, buffer, aCount);
			} else ClearArray(hdr+1, aCount);

			Release();
		}		
	}

	return result;
}


UI_64 OpenGL::AttributeArray::Create(const AttributeArray & ref) {
	UI_64 result(-1);

	if (*this) {
		*this = ref;
		result = GetID();
	} else {
		*dynamic_cast<SFSco::Object *>(this) = ref;

		result = Clone((GetColumnCount()<<4) + sizeof(Header), sizeof(Header));
	}
	
	return result;
}

UI_64 OpenGL::AttributeArray::AddAttributes(unsigned int aCount, const float * src) {
	UI_64 result = GetColumnCount();
	UI_64 coc = result + aCount;

	result = ((result+3)&(~3))-result;

	if (aCount > result) {		
		aCount -= result;		

		result = Expand(((aCount+3)>>2)<<6);

		if (result != -1) {
			if (Header * hdr = reinterpret_cast<Header *>(Acquire())) {
				SetColumnCount(coc);

				Release();
			}
		}
	}

	return result;
}

Calc::MatrixF & OpenGL::AttributeArray::operator >> (MatrixF & dstm) {
	float * dptr(0);

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (MatrixF::Header * dhdr = reinterpret_cast<MatrixF::Header *>(dstm.Acquire(reinterpret_cast<void **>(dptr)))) {
			if (!dptr) dptr = reinterpret_cast<float *>(dhdr + 1);
			Calc::MF_Multiply44(ahdr->local_transform, dhdr->coCount, dptr, dptr);

			dstm.Release(dhdr);	
		}

		Release();
	}
	
	return dstm;
}

UI_64 OpenGL::AttributeArray::IsIdentity() {
	UI_64 result(0);

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		result = Calc::MF_IdentityCheck(ahdr->local_transform, 1);

		Release();
	}
		
	return result;
}


OpenGL::AttributeArray & OpenGL::AttributeArray::SetScale(const float * xyzt) {

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->scale_vector[0] = xyzt[0];
		ahdr->scale_vector[1] = xyzt[1];
		ahdr->scale_vector[2] = xyzt[2];
		ahdr->scale_vector[3] = xyzt[3];

		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}

OpenGL::AttributeArray & OpenGL::AttributeArray::SetZShift(float zscal, float zadd) {
	float * vepo(0);

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		vepo = reinterpret_cast<float *>(ahdr+1);

		ahdr->scale_vector[2] = zscal;
		ahdr->reserved[3] |= 1;
		if (zadd != 0.0f) for (unsigned int i(0);i<ahdr->coCount;i++) vepo[4*i+2] += zadd;

		Release();
	}

	return *this;
}


OpenGL::AttributeArray & OpenGL::AttributeArray::Scale(const float * xyzt) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->scale_vector[0] *= xyzt[0];
		ahdr->scale_vector[1] *= xyzt[1];
		ahdr->scale_vector[2] *= xyzt[2];
		ahdr->scale_vector[3] *= xyzt[3];

		ahdr->reserved[3] |= 1;
		
		Release();
	}

	return *this;
}

OpenGL::AttributeArray & OpenGL::AttributeArray::Size(const float * xyzt) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		Calc::MF_Scale44(reinterpret_cast<float *>(ahdr + 1), xyzt, ahdr->coCount);
		
		Release();
	}

	return *this;
}

void OpenGL::AttributeArray::SwapOffset(AttributeArray & refa) {
	float t_val[4];
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		t_val[0] = ahdr->translate_vector[0];
		t_val[1] = ahdr->translate_vector[1];
		t_val[2] = ahdr->translate_vector[2];
		t_val[3] = ahdr->translate_vector[3];

		if (Header * ref_hdr = reinterpret_cast<Header *>(refa.Acquire())) {
			ahdr->translate_vector[0] = ref_hdr->translate_vector[0];
			ahdr->translate_vector[1] = ref_hdr->translate_vector[1];
			ahdr->translate_vector[2] = ref_hdr->translate_vector[2];
			ahdr->translate_vector[3] = ref_hdr->translate_vector[3];

			ref_hdr->translate_vector[0] = t_val[0];
			ref_hdr->translate_vector[1] = t_val[1];
			ref_hdr->translate_vector[2] = t_val[2];
			ref_hdr->translate_vector[3] = t_val[3];


			ref_hdr->reserved[3] = 1;
			refa.Release();
		}
		ahdr->reserved[3] = 1;
		Release();
	}
}


OpenGL::AttributeArray & OpenGL::AttributeArray::SetOffset(const float * xyzt) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->translate_vector[0] = xyzt[0];
		ahdr->translate_vector[1] = xyzt[1];
		ahdr->translate_vector[2] = xyzt[2];
		ahdr->translate_vector[3] = xyzt[3];
		
		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}

OpenGL::AttributeArray & OpenGL::AttributeArray::SetOffsetXY(const float * xyzt) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->translate_vector[0] = xyzt[0];
		ahdr->translate_vector[1] = xyzt[1];
		
		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}

OpenGL::AttributeArray & OpenGL::AttributeArray::RotateXY(float alpha) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->xy_spin[0] = Calc::FloatCos(alpha);
		ahdr->xy_spin[1] = Calc::FloatSin(alpha);
		ahdr->xy_spin[2] = alpha;
		
		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}


OpenGL::AttributeArray & OpenGL::AttributeArray::SetOffsetZ(float z_val) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->translate_vector[2] = z_val;
				
		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}


OpenGL::AttributeArray & OpenGL::AttributeArray::Translate(const float * xyzt) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		ahdr->translate_vector[0] += xyzt[0];
		ahdr->translate_vector[1] += xyzt[1];
		ahdr->translate_vector[2] += xyzt[2];
		ahdr->translate_vector[3] += xyzt[3];

		ahdr->reserved[3] |= 1;

		Release();
	}

	return *this;
}







void OpenGL::AttributeArray::MatrixUpdate(float * oma, const float * ima) {	
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (ima) Calc::FloatCopy16(ahdr->global_transform, ima, 1);
		else {
			ahdr->global_transform[0] = 1.0f;
			ahdr->global_transform[1] = ahdr->global_transform[2] = ahdr->global_transform[3] = ahdr->global_transform[4] = 0.0;
			ahdr->global_transform[5] = 1.0f;
			ahdr->global_transform[6] = ahdr->global_transform[7] = ahdr->global_transform[8] = ahdr->global_transform[9] = 0.0;
			ahdr->global_transform[10] = 1.0f;
			ahdr->global_transform[11] = ahdr->global_transform[12] = ahdr->global_transform[13] = ahdr->global_transform[14] = 0.0;
			ahdr->global_transform[15] = 1.0f;
		}

		GetLocalTransform(oma, true);
	
		Calc::MF_Multiply44(ahdr->global_transform, 4, oma, oma);

		Release();
	}
}


void OpenGL::AttributeArray::GetLocalTransform(float * xcomps, bool csi) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (ahdr->reserved[3]) {
			ahdr->local_transform[0] = ahdr->scale_vector[0]*ahdr->xy_spin[0];	ahdr->local_transform[1] = ahdr->scale_vector[0]*ahdr->xy_spin[1];	ahdr->local_transform[2] = 0.0;						ahdr->local_transform[3] = 0.0;
			ahdr->local_transform[4] = -ahdr->scale_vector[1]*ahdr->xy_spin[1];	ahdr->local_transform[5] = ahdr->scale_vector[1]*ahdr->xy_spin[0];	ahdr->local_transform[6] = 0.0;						ahdr->local_transform[7] = 0.0;
			ahdr->local_transform[8] = 0.0;										ahdr->local_transform[9] = 0.0;										ahdr->local_transform[10] = ahdr->scale_vector[2];	ahdr->local_transform[11] = 0.0;

			ahdr->local_transform[12] = ahdr->translate_vector[0];
			ahdr->local_transform[13] = ahdr->translate_vector[1];
			ahdr->local_transform[14] = ahdr->translate_vector[2];
			ahdr->local_transform[15] = 1.0;
		}
		
		if (xcomps != ahdr->local_transform) Calc::FloatCopy16(xcomps, ahdr->local_transform, 1);

		if (csi) ahdr->reserved[3] = 0;

		Release();
	}

}

int OpenGL::AttributeArray::TransformAdjust(float * oma) {
	int result(0);

	MatrixF::Header _mhdr = {4, 1, 4, 1, 4, 0.0, 0, 0};

	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		if (result = ahdr->reserved[3]) {
			ahdr->reserved[3] = 0;

			oma[0] = 1.0f;
			oma[1] = oma[2] = oma[3] = oma[4] = 0.0;
			oma[5] = 1.0f;
			oma[6] = oma[7] = oma[8] = oma[9] = 0.0;
			oma[10] = 1.0f;
			oma[11] = oma[12] = oma[13] = oma[14] = 0.0;
			oma[15] = 1.0f;

					
			Calc::MF_InverseMultiply44(&_mhdr, ahdr->global_transform, 4, oma);
			Calc::MF_InverseMultiply44(&_mhdr, ahdr->local_transform, 4, oma);

			ahdr->local_transform[0] = ahdr->scale_vector[0]*ahdr->xy_spin[0];	ahdr->local_transform[1] = ahdr->scale_vector[0]*ahdr->xy_spin[1];	ahdr->local_transform[2] = 0.0;						ahdr->local_transform[3] = 0.0;
			ahdr->local_transform[4] = -ahdr->scale_vector[1]*ahdr->xy_spin[1];	ahdr->local_transform[5] = ahdr->scale_vector[1]*ahdr->xy_spin[0];	ahdr->local_transform[6] = 0.0;						ahdr->local_transform[7] = 0.0;
			ahdr->local_transform[8] = 0.0;										ahdr->local_transform[9] = 0.0;										ahdr->local_transform[10] = ahdr->scale_vector[2];	ahdr->local_transform[11] = 0.0;

			ahdr->local_transform[12] = ahdr->local_transform[13] = ahdr->local_transform[14] = 0.0;
			ahdr->local_transform[15] = 1.0f;

			if ((ahdr->motion_proc) && (ahdr->motion_options != -1)) {
				switch (ahdr->motion_options) {
					case 1:
						Calc::MF_Multiply44(ahdr->motion_transform, 1, ahdr->translate_vector, ahdr->translate_vector);
					break;
					default:
						Calc::MF_Multiply44(ahdr->motion_transform, 4, ahdr->local_transform, ahdr->local_transform);
				}

				ahdr->local_transform[12] += ahdr->translate_vector[0];
				ahdr->local_transform[13] += ahdr->translate_vector[1];
				ahdr->local_transform[14] += ahdr->translate_vector[2];
				ahdr->local_transform[15] = 1.0;

			} else {
				ahdr->local_transform[12] = ahdr->translate_vector[0];
				ahdr->local_transform[13] = ahdr->translate_vector[1];
				ahdr->local_transform[14] = ahdr->translate_vector[2];
				ahdr->local_transform[15] = 1.0;
			}

			Calc::MF_Multiply44(ahdr->local_transform, 4, oma, oma);			
			Calc::MF_Multiply44(ahdr->global_transform, 4, oma, oma);
			
		}

		Release();
	}

	return result;
}


void OpenGL::AttributeArray::GetGlobalTransform(float * oma) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		Calc::FloatCopy16(oma, ahdr->global_transform, 1);

		Release();
	}
}

void OpenGL::AttributeArray::GlobalAdjust(float * ima) {
	if (Header * ahdr = reinterpret_cast<Header *>(Acquire())) {
		Calc::MF_Multiply44(ima, 4, ahdr->global_transform, ahdr->global_transform);

		Release();
	}
}

