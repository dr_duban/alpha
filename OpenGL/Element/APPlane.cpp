/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


//#include "OpenGL\OpenGL.h"
#include "Apps\MainMenu.h"
#include "Apps\NotificationArea.h"

#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"

#include "Apps\XFiles.h"

#include "System\SysUtils.h"
#include "Pics\Image.h"
#include "Media\Source.h"

#include "Math\CalcSpecial.h"

#pragma warning(disable:4244)

const UI_64 OpenGL::APPlane::scale_set[] = { // capacity
	94,

	0xC050000000000000, 0xC04F800000000000, 0xC04F000000000000, 0xC04E800000000000, 0xC04E000000000000, 0xC04D800000000000, 0xC04D000000000000, 0xC04C800000000000,
	0xC04C000000000000, 0xC04B800000000000, 0xC04B000000000000, 0xC04A800000000000, 0xC04A000000000000, 0xC049800000000000, 0xC049000000000000, 0xC048800000000000,
	0xC048000000000000, 0xC047800000000000, 0xC047000000000000, 0xC046800000000000, 0xC046000000000000, 0xC045800000000000, 0xC045000000000000, 0xC044800000000000,
	0xC044000000000000, 0xC043800000000000, 0xC043000000000000, 0xC042800000000000, 0xC042000000000000, 0xC041800000000000, 0xC041000000000000, 0xC040800000000000,
	0xC040000000000000, 0xC03F000000000000, 0xC03E000000000000, 0xC03D000000000000, 0xC03C000000000000, 0xC03B000000000000, 0xC03A000000000000, 0xC039000000000000,
	0xC038000000000000, 0xC037000000000000, 0xC036000000000000, 0xC035000000000000, 0xC034000000000000, 0xC033000000000000, 0xC032000000000000, 0xC031000000000000,
	0xC030000000000000, 0xC02E000000000000, 0xC02C000000000000, 0xC02A000000000000, 0xC028000000000000, 0xC026000000000000, 0xC024000000000000, 0xC022000000000000,
	0xC020000000000000, 0xC01C000000000000, 0xC018000000000000, 0xC014000000000000, 0xC010000000000000, 0xC008000000000000, 0xC000000000000000, 0xBFF8000000000000,

	0x0000000000000000, 0x3FF8000000000000, 0x4000000000000000, 0x4004000000000000, 0x4008000000000000, 0x400C000000000000, 0x4010000000000000, 0x4012000000000000,
	0x4014000000000000, 0x4016000000000000, 0x4018000000000000, 0x401A000000000000, 0x401C000000000000, 0x401E000000000000, 0x4020000000000000, 0x4021000000000000,
	0x4022000000000000, 0x4023000000000000, 0x4024000000000000, 0x4025000000000000, 0x4026000000000000, 0x4027000000000000, 0x4028000000000000, 0x4029000000000000,
	0x402A000000000000, 0x402B000000000000, 0x402C000000000000, 0x402D000000000000, 0x402E000000000000, 0x402F000000000000,	0x4030000000000000



};

OpenGL::APPlane OpenGL::APPlane::current_app;

OpenGL::APPlane::APPlane() {
	SetTID(type_id);
}

OpenGL::APPlane::~APPlane() {

}

void OpenGL::APPlane::Destroy() {
	unsigned int x_val(0);
	OpenGL::Core * core_ptr(0);
	OpenGL::Button fun_but;
	Element screen_el(*this), ref_el;

	Apps::NotificationArea::RemoveAppEntry(*this);
	if (current_app == *this) {
		current_app.Blank();
		Apps::NotificationArea::SetPlayList(current_app);
	}

	screen_el.Down().Left().Down();
	ref_el = screen_el;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(screen_el.Acquire())) {
		if (x_val = (el_hdr->config.tex_unit_selector & 0xFFFFFFFF)) {
			glDeleteTextures(1, &x_val);
		}

		screen_el.Release();
	}

	for (screen_el.Left();screen_el != ref_el;screen_el.Left()) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(screen_el.Acquire())) {
			if (x_val = (el_hdr->config.tex_unit_selector & 0xFFFFFFFF)) {
				glDeleteTextures(1, &x_val);
			}

			screen_el.Release();
		}
	}

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);
		fun_but = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->fun_but;

		reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list.Destroy();

		Release();
	}
	
	
	DumpFunction(fun_but);

	if (core_ptr) {	

		__super::Destroy();

		core_ptr->Update();
	}


}


UI_64 OpenGL::APPlane::AddControlLabel(Element::LabelCfg & lacfg) {
	Element control_plane(*this);
	control_plane.Down();

	return control_plane.AddLabel(lacfg);

}

UI_64 OpenGL::APPlane::AddControl(Element & c_el, unsigned int w, unsigned int h, unsigned int x) {
	UI_64 result(-1);
	
	Element control_plane(*this);
	control_plane.Down();

	if (Core * ogl_core = GetContext())
	switch (c_el.GetTID()) {
		case Button::type_id:
			result = ogl_core->CreateButton((Button &)c_el, w, h, &control_plane);			
		break;
		case SliderClip::type_id:
			result = ogl_core->CreateSXClip((SliderClip &)c_el, w, h, x, &control_plane);
		break;
		case ListBoxClip::type_id:
			result = ogl_core->CreateLBXClip((ListBoxClip &)c_el, w, h, x, &control_plane);
		break;
		case BoardClip::type_id:
			result = ogl_core->CreateBoardClip((BoardClip &)c_el, w, h, x, &control_plane);
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				reinterpret_cast<APPlane::Header *>(el_hdr + 1)->board_clip = c_el;

				Release();
			}
		break;
		default:
			c_el.Blank();

	}


	return result;
}

void OpenGL::APPlane::HideCPBoards() {
	Element s_el, b_el;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		s_el = reinterpret_cast<APPlane::Header *>(el_hdr + 1)->board_clip;
		Release();
	}

	if (s_el) {
		s_el.Down();
		b_el = s_el;
	
		for (b_el.Left();b_el != s_el;b_el.Left()) b_el.Hide();
	}

}

void OpenGL::APPlane::GetCPBoard(OpenGL::Element & result, unsigned int bid, const wchar_t * la_cap) {
	float temp_vals[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	Text text_el;
	unsigned int b_count(0);

	result.Blank();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		result = reinterpret_cast<APPlane::Header *>(el_hdr + 1)->board_clip;		
		Release();
	}

	if (result) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(result.Acquire())) {
			b_count = reinterpret_cast<BoardClip::Header *>(el_hdr + 1)->cp_board_count;

			result.Release();
		}

		if (bid < b_count) {
			result.Down().Left();			

			for (unsigned int i(0);i<bid;i++) {
				result.Left();
			}

			text_el(result).Down();

			if (Element::Header * eb_hdr = reinterpret_cast<Element::Header *>(result.Acquire())) {
				if (reinterpret_cast<D2Board::PlotCfg *>(eb_hdr + 1)->flags & D2Board::FlagBorder) reinterpret_cast<Element &>(text_el).Left();
				temp_vals[0] = reinterpret_cast<D2Board::PlotCfg *>(eb_hdr + 1)->caption_label.offset[0];
				temp_vals[1] = reinterpret_cast<D2Board::PlotCfg *>(eb_hdr + 1)->caption_label.offset[1] + reinterpret_cast<D2Board::PlotCfg *>(eb_hdr + 1)->caption_label.f_size;
				result.Release();
			}

			if (la_cap) {
				text_el.RelocateXY(temp_vals);
				text_el.Rewrite(la_cap + 1, 0, la_cap[0]);
				text_el.Show();
			} else {
				text_el.Hide();
			}
			
		}
	}

}

UI_64 OpenGL::APPlane::SetICPText(unsigned int bid, StringsBoard::InfoLine & i_line) {	

	Element result;
	Text text_el;
	unsigned int la_count(0), s_length(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		result = reinterpret_cast<APPlane::Header *>(el_hdr + 1)->board_clip;
		Release();
	}

	if (result) {
		result.Down();

		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(result.Acquire())) {
			la_count = reinterpret_cast<StringsBoard::Header *>(el_hdr + 1)->la_count;
			result.Release();
		}

		if (bid < la_count) {
			text_el(result).Down();

			for (unsigned int i(0);i<bid;i++) reinterpret_cast<Element &>(text_el).Left();

			switch (i_line.cap_flags & StringsBoard::InfoTypeMask) {
				case StringsBoard::InfoTypeDouble:
					s_length = Strings::DoubleToCStr_SSSE3(reinterpret_cast<char *>(i_line.cap + 64), reinterpret_cast<double &>(i_line.val), i_line.cap_flags & (StringsBoard::InfoTypeFormatted | StringsBoard::InfoTypeDMask));
				break;
				case StringsBoard::InfoTypeINT64:
					s_length = Strings::Int64ToCStr_SSSE3(reinterpret_cast<char *>(i_line.cap + 64), reinterpret_cast<__int64 &>(i_line.val), 0);
				break;
				case StringsBoard::InfoTypeTime:
					s_length = Strings::TimeToCStr(reinterpret_cast<char *>(i_line.cap + 64), i_line.val);
				break;
				case StringsBoard::InfoTypePcnt:
					Strings::IntToCStr_SSSE3(reinterpret_cast<char *>(i_line.cap + 64), reinterpret_cast<double &>(i_line.val)*100.0, 0);
				break;

			}

			Strings::A2W(i_line.cap + 64);

			if (i_line.cap_flags & StringsBoard::InfoDataPrefix) {
				for (unsigned int i(0);i<s_length;i++) i_line.cap[i + 128] = i_line.cap[i + 64];
				i_line.cap[s_length++ + 128] = L' ';
				for (unsigned int i(0);i<(i_line.cap_size & 0x00FF);i++) i_line.cap[s_length++ + 128] = i_line.cap[i];
			} else {
				for (unsigned int i(0);i<(i_line.cap_size & 0x00FF);i++) i_line.cap[i + 128] = i_line.cap[i];
				i_line.cap[(i_line.cap_size & 0x00FF) + 128] = L' ';
				for (unsigned int i(0);i<s_length;i++) i_line.cap[i + (i_line.cap_size & 0x00FF) + 129] = i_line.cap[i + 64];

				s_length += (i_line.cap_size & 0x00FF) + 1;
			}

			for (unsigned int i(0);i<(i_line.cap_size >> 16);i++) i_line.cap[s_length++ + 128] = i_line.cap[i + (i_line.cap_size & 0x00FF)];

			text_el.Rewrite(i_line.cap + 128, 0, s_length);
		}
	}

	return result;
}



void OpenGL::APPlane::RootScreen() {
	Core * core_ptr(0);
	float temp_vals[4] = {0.0, 0.0, 0.0, 0.0};

	AttributeArray a_arr, b_arr;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
		if (el_hdr->span_cfg.x != el_hdr->span_cfg.x_max) {
			temp_vals[0] = el_hdr->span_cfg.x;
			el_hdr->span_cfg.x_max = el_hdr->span_cfg.x;
		} else {
			el_hdr->span_cfg.x_max = 0.0;
		}
		

		Release();
	}


	if (core_ptr) {
		InitAttributeArray(AttributeArray::Vertex, a_arr);

		if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {

			function_area.InitAttributeArray(AttributeArray::Vertex, b_arr);
			if (AttributeArray::Header * b_hdr = reinterpret_cast<AttributeArray::Header *>(b_arr.Acquire())) {
				if (temp_vals[0]) {
					reinterpret_cast<float *>(a_hdr+1)[4] = reinterpret_cast<float *>(a_hdr+1)[12] = -reinterpret_cast<float *>(b_hdr+1)[4] - temp_vals[0] - 1.0f;
				} else {
					reinterpret_cast<float *>(a_hdr+1)[4] = reinterpret_cast<float *>(a_hdr+1)[12] = 0.0f;
				}

				reinterpret_cast<float *>(a_hdr+1)[9] = reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(b_hdr+1)[13];

				if (b_hdr->translate_vector[0] > 10.0f) a_hdr->translate_vector[0] = temp_vals[0];
				else a_hdr->translate_vector[0] = reinterpret_cast<float *>(b_hdr+1)[4];

				a_hdr->translate_vector[1] = b_hdr->translate_vector[1];

				b_arr.Release();
			}

			core_ptr->GetRootBox(temp_vals);
			reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] += temp_vals[0]; // reinterpret_cast<float *>(b_hdr+1)[4];
/*
			core_ptr->GetNotificationCoords(b_arr);
			if (AttributeArray::Header * b_hdr = reinterpret_cast<AttributeArray::Header *>(b_arr.Acquire())) {
				reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] += reinterpret_cast<float *>(b_hdr+1)[4];

				a_hdr->translate_vector[0] += b_hdr->translate_vector[0];

				b_arr.Release();
			}
*/

			a_hdr->reserved[3] = 1;

			a_arr.CalculateBox(LeftBottomFront);

			a_arr.Release();
			
			TexCoordReset(*this, 0);

			core_ptr->ResetElement(*this);

		}
	}
}

void OpenGL::APPlane::SetCPMargin(const float * cp_val) {
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[0] = cp_val[0];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[1] = cp_val[1];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[2] = cp_val[2];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[3] = cp_val[3];

		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[4] = cp_val[4];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[5] = cp_val[5];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[6] = cp_val[6];
		reinterpret_cast<APPlane::Header *>(el_hdr + 1)->cp_margin[7] = cp_val[7];

		Release();
	}
}

UI_64 OpenGL::APPlane::Configure(unsigned int c_flags, unsigned int bcolor, float x_shift, unsigned int cp_to) {
	UI_64 result(0), _event(0);
	float color_val[4] = {0.0, 0.0, 0.0, 1.0};
	Core * core_ogl(0);	
	AttributeArray a_arr, b_arr;
	Element control_plane(*this);

	SetTrajectory((void (__fastcall *)(float *, unsigned int))TexCoordReset, -1); // 	 // rebuild ico els if required, calculate texsize



	control_plane.Down();	

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ogl = reinterpret_cast<Core *>(el_hdr->_context);
		el_hdr->config.anchor = LeftBottomFront;
		
		el_hdr->span_cfg.x = el_hdr->span_cfg.x_max = x_shift;

		Release();
	}

	if (core_ogl) {
		InitAttributeArray(AttributeArray::Vertex, a_arr);
// adjust coords
		

		if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {

			function_area.InitAttributeArray(AttributeArray::Vertex, b_arr);
			if (AttributeArray::Header * b_hdr = reinterpret_cast<AttributeArray::Header *>(b_arr.Acquire())) {
				reinterpret_cast<float *>(a_hdr+1)[4] = reinterpret_cast<float *>(a_hdr+1)[12] = -reinterpret_cast<float *>(b_hdr+1)[4] - x_shift - 1.0f;
				reinterpret_cast<float *>(a_hdr+1)[9] = reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(b_hdr+1)[13];

				if (b_hdr->translate_vector[0] > 10.0f) a_hdr->translate_vector[0] = x_shift;
				else a_hdr->translate_vector[0] = reinterpret_cast<float *>(b_hdr+1)[4];

				a_hdr->translate_vector[1] = b_hdr->translate_vector[1];

				b_arr.Release();
			}


			core_ogl->GetRootBox(color_val);
			reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] += color_val[0]; // reinterpret_cast<float *>(b_hdr+1)[4];

/*
			core_ogl->GetNotificationCoords(b_arr);

			if (AttributeArray::Header * b_hdr = reinterpret_cast<AttributeArray::Header *>(b_arr.Acquire())) {
				
				a_hdr->translate_vector[0] += b_hdr->translate_vector[0];
				b_arr.Release();
			}
*/
			

			a_arr.Release();
		}
		
		a_arr.SetStretchMask(-1, -1, 0);
		a_arr.CalculateBox(LeftBottomFront);


		control_plane.Hide();

		if ((c_flags & NoCP) == 0) {
			_event = SFSco::Event_CreateDrain(PlaneSelect, this, 0);
			SFSco::Event_SetGate(_event, *this, FlagMouseUp);
			SetEFlags(FlagAction);


			control_plane.SetAnchorMode(LeftBottomFront);
			control_plane.InitAttributeArray(AttributeArray::Vertex, b_arr);		
			b_arr.Copy(a_arr);
			b_arr.SetStretchMask(-1, -1, 0);
			b_arr.CalculateBox(LeftBottomFront);
				
			control_plane.Pop();
			
			_event = SFSco::Event_CreateDrain(CPShow, this, 0);
			SFSco::Event_SetGate(_event, control_plane, OpenGL::Element::StateShow);
	
			_event = SFSco::Event_CreateDrain(CPHide, this, 0);
			SFSco::Event_SetGate(_event, control_plane, OpenGL::Element::StateHide);

			core_ogl->SetAutoHide(control_plane, cp_to);

		}

		if (c_flags & BgTexture) {
			InitAttributeArray(AttributeArray::TexCoord, a_arr);


		} else {
			InitAttributeArray(AttributeArray::Color, a_arr);
			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
				OpenGL::Core::ColorConvert(reinterpret_cast<float *>(a_hdr + 1), bcolor, 4);
				a_arr.Release();
			}
			
		}




		Pop(0);
	



	}



	return result;
}

UI_64 OpenGL::APPlane::CPShow(Element & a_plane) {
	UI_64 result(0);
	float box_vals[16] = {1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	Element screen_collection(a_plane);

	screen_collection.Down().Left();

	screen_collection.Rescale(box_vals);
	screen_collection.GetRightMargin(box_vals+4); // collection
	screen_collection.Down();
	screen_collection.GetRightMargin(box_vals+8); // screen
	screen_collection.Up();


	box_vals[0] = 2.0f*(box_vals[4] + box_vals[8]);
	box_vals[1] = 2.0f*(box_vals[5] + box_vals[9]);


// center vertically

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(a_plane.Acquire())) {
		APPlane::Header * app_hdr = reinterpret_cast<APPlane::Header *>(el_hdr+1);
	// horizontal cfg	
		box_vals[12] = box_vals[0] - (app_hdr->cp_margin[0] + app_hdr->cp_margin[1]);
		box_vals[13] = box_vals[1] - (app_hdr->cp_margin[2] + app_hdr->cp_margin[3]);
	// vertical cfg	
		box_vals[14] = box_vals[0] - (app_hdr->cp_margin[4] + app_hdr->cp_margin[5]);
		box_vals[15] = box_vals[1] - (app_hdr->cp_margin[6] + app_hdr->cp_margin[7]);



		if ((box_vals[12] < 0.0f) || (box_vals[13] < 0.0f)) {			
			if ((box_vals[14] < 0.0f) || (box_vals[15] < 0.0f)) {
				// set scale
								
				box_vals[0] = box_vals[12]*box_vals[13];
				box_vals[1] = box_vals[14]*box_vals[15];

				Calc::Float4NAbs(box_vals);
				// deside on type

				box_vals[2] = box_vals[6] - 2.0f*box_vals[8];
				box_vals[3] = box_vals[7] - 2.0f*box_vals[9];

				if (box_vals[1] < box_vals[0]) {
					// vertical type
					box_vals[2] = (box_vals[2] + box_vals[14])/box_vals[2];
					box_vals[3] = (box_vals[3] + box_vals[15])/box_vals[3];

					result = 4;
				} else {
					// horizontal type
					box_vals[2] = (box_vals[2] + box_vals[12])/box_vals[2];
					box_vals[3] = (box_vals[3] + box_vals[13])/box_vals[3];

				}

				if (box_vals[3] < box_vals[2]) box_vals[2] = box_vals[3];

			} else {
				result = 4;
			}
		} else {
			box_vals[0] = box_vals[12]*box_vals[13];
			box_vals[1] = 0.67f*box_vals[14]*box_vals[15];
			
			if (box_vals[1] > box_vals[0]) result = 4;
		}

		if (result != 4) {
			box_vals[0] = 0.5f*(box_vals[6] - box_vals[2]*(2.0f*box_vals[8] + box_vals[10])) + box_vals[8]/box_vals[2]*(app_hdr->cp_margin[0] - app_hdr->cp_margin[1])/(app_hdr->cp_margin[0] + app_hdr->cp_margin[1]);
			box_vals[1] = 0.5f*(box_vals[7] - box_vals[2]*(2.0f*box_vals[9] + box_vals[11])) + box_vals[9]/box_vals[2]*(app_hdr->cp_margin[2] - app_hdr->cp_margin[3])/(app_hdr->cp_margin[2] + app_hdr->cp_margin[3]);
		} else {
			box_vals[0] = 0.5f*(box_vals[6] - box_vals[2]*(2.0f*box_vals[8] + box_vals[10])) + box_vals[8]/box_vals[2]*(app_hdr->cp_margin[4] - app_hdr->cp_margin[5])/(app_hdr->cp_margin[4] + app_hdr->cp_margin[5]);
			box_vals[1] = 0.5f*(box_vals[7] - box_vals[2]*(2.0f*box_vals[9] + box_vals[11])) + box_vals[9]/box_vals[2]*(app_hdr->cp_margin[6] - app_hdr->cp_margin[7])/(app_hdr->cp_margin[6] + app_hdr->cp_margin[7]);
		}



		screen_collection.RelocateXY(box_vals);

		box_vals[0] = box_vals[1] = box_vals[2]; box_vals[2] = box_vals[3] = 1.0f;
		screen_collection.Rescale(box_vals);

		reinterpret_cast<Core *>(el_hdr->_context)->ResetElement(screen_collection);

		screen_collection.Right();
		
		el_hdr->config.anchor &= 0x0000FFFF;
		el_hdr->config.anchor |= 0x00030000;

		a_plane.SetState(Element::StateNotify);
		a_plane.Release();

		
	}


	return result;
}

UI_64 OpenGL::APPlane::CPHide(Element & a_plane) {
	UI_64 result(0);
	float box_vals[12] = {1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	Element screen_collection(a_plane);

	a_plane.GetRightMargin(box_vals + 4);

	
	screen_collection.Down().Left();

	screen_collection.GetRightCorner(box_vals + 8);
		
// calc some offsets
	box_vals[4] = 0.5f*(box_vals[6] - box_vals[10]);
	box_vals[5] = 0.5f*(box_vals[7] - box_vals[11]);

	
	
	screen_collection.RelocateXY(box_vals + 4);
	
	screen_collection.Rescale(box_vals);
	

	if (Core * ogl_ptr = a_plane.GetContext()) {
		ogl_ptr->ResetElement(screen_collection);

		screen_collection.Right();

	}


	return result;
}

UI_64 OpenGL::APPlane::PlaneSelect(Element & plane_el) {
	UI_64 result(0);	
	Element control_plane(plane_el);
		

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(plane_el.Acquire())) {
		result = (el_hdr->span_cfg.x == el_hdr->span_cfg.x_max);

		plane_el.Release();
	}

	if (result) {
		control_plane.Down();
		control_plane.Show();
	} else {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(plane_el.Acquire())) {
			el_hdr->config.anchor &= 0x0000FFFF;
			el_hdr->config.anchor |= 0x00010000;

			plane_el.Release();
		}

		plane_el.SetState(Element::StateNotify);
	}
	// reset function
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(plane_el.Acquire())) {
		ResetFunction(reinterpret_cast<APPlane::Header *>(el_hdr+1)->fun_but);

		plane_el.Release();

		reinterpret_cast<Core *>(el_hdr->_context)->ResetElement(control_plane);
	}

	return result;
}

UI_64 OpenGL::Core::CreateAPPlane(OpenGL::APPlane & a_plane, UI_64 (* u_proc)(Element &)) {
	UI_64 result(-1);
	Button t_but;
	SFSco::Object t_obj;
	Element cpa_el;

	result = CreateElement(Element::Quad, a_plane, 0, 0, sizeof(APPlane::Header));

	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(a_plane.Acquire())) {
			el_hdr->_flags |= Element::FlagBlured | Element::FlagIdent;

			reinterpret_cast<APPlane::Header *>(el_hdr+1)->fun_but = t_but;
			reinterpret_cast<APPlane::Header *>(el_hdr+1)->play_list = t_obj;
			reinterpret_cast<APPlane::Header *>(el_hdr+1)->screen_proc = u_proc;

			reinterpret_cast<APPlane::Header *>(el_hdr+1)->pi_count = reinterpret_cast<APPlane::Header *>(el_hdr+1)->pic_tid = 0;
			
			a_plane.Release();
		}

		result = CreateElement(Element::Quad, cpa_el, 0, &a_plane);
		result = CreateElement(Element::Quad, cpa_el, 0, &a_plane);
	}

	return result;
}

UI_64 OpenGL::Core::ClearDecodeView(Element & pic_el, bool opdate) {
	
	unsigned int result(0);
	Core * core_ptr(0);

	Element app_plane(pic_el);
	AttributeArray v_arr;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(pic_el.Acquire())) {
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		if (result = (el_hdr->config.tex_unit_selector & 0xFFFFFFFF)) {
			OpenGL::glDeleteTextures(1, &result);
		}

		pic_el.Release();
	}

	pic_el.Destroy();


	app_plane.Up().Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(app_plane.Acquire())) {
		result = --reinterpret_cast<APPlane::Header *>(el_hdr+1)->pi_count;

		app_plane.Release();
	}

	APPlane::TexCoordReset(app_plane, 0);

	if ((core_ptr) && (opdate)) core_ptr->Update();

	return result;
}

UI_64 OpenGL::APPlane::TexCoordReset(Element & app_plane, unsigned int) {
	UI_64 result(0);
	float some_vals[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	unsigned int t_vals[4] = {0, 0, 0, 0};

	AttributeArray v_arr, t_arr;

	Element ico_el(app_plane);
		


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(app_plane.Acquire())) {
		el_hdr->config.anchor &= 0x0000FFFF;
		el_hdr->config.anchor |= 0x00020000;

		result = reinterpret_cast<APPlane::Header *>(el_hdr+1)->pi_count;

		app_plane.Release();
	}

	app_plane.GetRightMargin(some_vals);

// calculate box properly
	
	some_vals[0] = 1.5f*some_vals[3]/some_vals[2];
	some_vals[1] = 1.0f/some_vals[0];

	if (some_vals[1] > some_vals[0]) some_vals[0] = some_vals[1];
	
	t_vals[0] = some_vals[0];


	some_vals[4] = t_vals[0];
	some_vals[4] *= result;
	
	some_vals[4] = Calc::FloatSqrt(some_vals[4]);
	t_vals[0] = some_vals[4];

	if (t_vals[0] == 0) t_vals[0] = 1;

	if (some_vals[0] == some_vals[1]) { // w > h, col_count
		t_vals[1] = (result + t_vals[0] - 1)/t_vals[0];
	} else {
		t_vals[1] = t_vals[0];
		t_vals[0] = (result + t_vals[1] - 1)/t_vals[1];
	}

	some_vals[0] = some_vals[2];
	some_vals[1] = some_vals[3];

	some_vals[2] /= t_vals[0];
	some_vals[3] /= t_vals[1];

	t_vals[2] = some_vals[2];
	t_vals[3] = some_vals[3];

	t_vals[2]--;
	t_vals[3]--;

	some_vals[2] = t_vals[2];
	some_vals[3] = t_vals[3];

	some_vals[6] = (some_vals[2]+1.0f)*t_vals[0] - 1.0f;
	some_vals[7] = (some_vals[3]+1.0f)*t_vals[1] - 1.0f;
	some_vals[4] = 0.5f*(some_vals[0] - some_vals[6]);
	some_vals[5] = 0.5f*(some_vals[1] - some_vals[7]);

	ico_el.Down().Left();

	ico_el.SetRect(some_vals + 4);
	ico_el.RelocateXY(some_vals + 4);



	some_vals[6] = 0.0f;
	some_vals[7] = 0.0f;

	ico_el.Down();

	
	for (t_vals[2] = 0, t_vals[3] = result;t_vals[3]--;ico_el.Left()) {
		
		ico_el.InitAttributeArray(AttributeArray::Vertex, v_arr);
		ico_el.InitAttributeArray(AttributeArray::TexCoord, t_arr);

	// get scale factor, smaller facor deciedes
	

		if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
			if (AttributeArray::Header * t_hdr = reinterpret_cast<AttributeArray::Header *>(t_arr.Acquire())) {			

				some_vals[0] = t_hdr->offset_mask[2]/some_vals[2];
				some_vals[1] = t_hdr->offset_mask[3]/some_vals[3];

				if (some_vals[0] < some_vals[1]) some_vals[0] = some_vals[1];
				else some_vals[1] = some_vals[0];

				if (t_hdr->stretch_mask[0] & 1) { // not bigger than device box

					if (some_vals[0] <= 1.0f) { // no_size

						t_hdr->stretch_vector[0] = t_hdr->offset_mask[0];
						t_hdr->stretch_vector[1] = t_hdr->offset_mask[1];
						t_hdr->stretch_vector[2] = t_hdr->offset_mask[2];
						t_hdr->stretch_vector[3] = t_hdr->offset_mask[3];

						t_hdr->stretch_mask[3] = 1;

					} else {
						if (some_vals[0] > 1.5f) {
							t_hdr->stretch_mask[3] = some_vals[0];
							if ((some_vals[0] - t_hdr->stretch_mask[3]) > 0.0) t_hdr->stretch_mask[3]++;

							some_vals[0] = some_vals[1] = t_hdr->stretch_mask[3];

						} else {
							some_vals[0] = some_vals[1] = 1.5f;							

							t_hdr->stretch_mask[3] = 0;
						}

						t_hdr->stretch_vector[2] = t_hdr->offset_mask[2]/some_vals[0];
						t_hdr->stretch_vector[3] = t_hdr->offset_mask[3]/some_vals[1];


						t_hdr->stretch_mask[2] = t_hdr->stretch_vector[2];
						t_hdr->stretch_mask[2] += 15;
						t_hdr->stretch_mask[2] &= ~15;
						t_hdr->stretch_vector[0] = t_hdr->stretch_mask[2];

						t_hdr->stretch_mask[2] = t_hdr->stretch_vector[3];
						t_hdr->stretch_mask[2] += 15;
						t_hdr->stretch_mask[2] &= ~15;
						t_hdr->stretch_vector[1] = t_hdr->stretch_mask[2];

						
					}
				}

				t_vals[1] = t_hdr->stretch_vector[2];
				t_hdr->stretch_vector[2] = t_vals[1];

				t_hdr->stretch_mask[2] = t_hdr->stretch_vector[3];
				t_hdr->stretch_vector[3] = t_hdr->stretch_mask[2];

				if (t_hdr->stretch_mask[3] == 0) {
					--t_vals[1] &= ~3;					
				}


				v_hdr->translate_vector[0] = 0.5f*(some_vals[2] - t_vals[1]) + some_vals[6];
				v_hdr->translate_vector[1] = 0.5f*(some_vals[3] - t_hdr->stretch_vector[3]) + some_vals[7];


				reinterpret_cast<float *>(v_hdr + 1)[0] = reinterpret_cast<float *>(v_hdr + 1)[1] = 0.0f;
				reinterpret_cast<float *>(v_hdr + 1)[4] = t_vals[1]; reinterpret_cast<float *>(v_hdr + 1)[5] = 0.0f;

				reinterpret_cast<float *>(v_hdr + 1)[8] = 0.0f; reinterpret_cast<float *>(v_hdr + 1)[9] = t_hdr->stretch_vector[3];
				reinterpret_cast<float *>(v_hdr + 1)[12] = t_vals[1]; reinterpret_cast<float *>(v_hdr + 1)[13] = t_hdr->stretch_vector[3];



				reinterpret_cast<float *>(t_hdr + 1)[0] = reinterpret_cast<float *>(t_hdr + 1)[1] = 0.0f;
				reinterpret_cast<float *>(t_hdr + 1)[4] = t_vals[1] - 0.1f; reinterpret_cast<float *>(t_hdr + 1)[5] = 0.0f;

				reinterpret_cast<float *>(t_hdr + 1)[8] = 0.0f; reinterpret_cast<float *>(t_hdr + 1)[9] = t_hdr->stretch_vector[3] - 0.1f;
				reinterpret_cast<float *>(t_hdr + 1)[12] = t_vals[1] - 0.1f; reinterpret_cast<float *>(t_hdr + 1)[13] = t_hdr->stretch_vector[3] - 0.1f;


				t_hdr->stretch_mask[1] = 1;
				v_hdr->reserved[3] = 1;

				t_arr.Release();
			}

			v_arr.Release();
		}

		if (++t_vals[2] == t_vals[0]) {
			some_vals[6] = 0.0f;
			some_vals[7] += (some_vals[3] + 1.0f);

			t_vals[2] = 0;
		} else {
			some_vals[6] += (some_vals[2] + 1.0f);
		}
	}

	app_plane.SetState(Element::StateNotify);

	return result;
}

UI_64 OpenGL::APPlane::SetPicture(Element & pic_el, const float * pi_box, unsigned int tid) {
	UI_64 result(-1);
	AttributeArray t_arr;
	Element screen_collection(*this);
	screen_collection.Down().Left();

	if (Core * ogl_core = GetContext()) {
		result = ogl_core->CreateElement(OpenGL::Element::Quad, pic_el, 4, &screen_collection);

		if (result != -1) {
			pic_el.SetRect(pi_box);
			pic_el.SetRectTexture(pi_box, tid, 0);

			pic_el.InitAttributeArray(AttributeArray::TexCoord, t_arr);
			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(t_arr.Acquire())) {
				a_hdr->stretch_mask[0] = 1; // device box on
				a_hdr->stretch_mask[3] = 1;

				a_hdr->offset_mask[0] = pi_box[0];
				a_hdr->offset_mask[1] = pi_box[1];
				a_hdr->offset_mask[2] = pi_box[2];
				a_hdr->offset_mask[3] = pi_box[3];

				t_arr.Release();
			}


			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				reinterpret_cast<APPlane::Header *>(el_hdr+1)->pi_count++;

				Release();
			}

			TexCoordReset(*this, 0);
		}
	}

	return result;
}


UI_64 OpenGL::Core::SetDecodeView(DecodeRecord & de_rec, void * ico_test) {
	UI_64 result(0);
	Element ico_el, screen_collection(de_rec.element_obj);
			
	AttributeArray v_arr;
	
	screen_collection.Down().Left();

	// call app motion_proc after creation
	if (Core * ogl_ptr = de_rec.element_obj.GetContext()) {
		if ((de_rec.flags & DecodeRecord::FlagRefresh) == 0) { // (de_rec.flags & DecodeRecord::FlagCapture) || (
			result = ogl_ptr->CreateElement(Element::Quad, ico_el, 4, &screen_collection);
		
			if (result != -1) {
				if (ico_test) {
					result = SFSco::Event_CreateDrain(ico_test, &ico_el, 0);
					SFSco::Event_SetGate(result, ico_el, Element::FlagMouseUp);

//					ico_el.SetEFlags(Element::FlagAction);
				}

		
				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(ico_el.Acquire())) {
					el_hdr->config.tex_unit_selector = 0;
					OpenGL::glGenTextures(1, (unsigned int *)&el_hdr->config.tex_unit_selector);

					el_hdr->config.tex_unit_selector |= (de_rec._reserved[5] << 56);

					ico_el.Release();
				}

				ico_el.InitAttributeArray(AttributeArray::TexCoord, v_arr);
				if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
					a_hdr->stretch_mask[0] = 1; // device box on
					a_hdr->stretch_mask[1] = 1;

					a_hdr->offset_mask[0] = de_rec.c_width;
					a_hdr->offset_mask[1] = de_rec.c_height;
					a_hdr->offset_mask[2] = de_rec.width;
					a_hdr->offset_mask[3] = de_rec.height;


					v_arr.Release();
				}


				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(de_rec.element_obj.Acquire())) {
					reinterpret_cast<APPlane::Header *>(el_hdr+1)->pi_count++;

					reinterpret_cast<APPlane::Header *>(el_hdr+1)->deco_rec.target_time = de_rec.target_time;

					if (reinterpret_cast<APPlane::Header *>(el_hdr+1)->screen_proc) {
						ogl_ptr->SetFBack(ico_el, reinterpret_cast<APPlane::Header *>(el_hdr+1)->screen_proc);
					}

					de_rec.element_obj.Release();
				}

// rebuild app_plane
				APPlane::TexCoordReset(de_rec.element_obj, 0);

				de_rec.element_obj = ico_el;

		// set some hanlers
				ogl_ptr->Update();

			} else {
				result = 0;
			}
		} else {
			ico_el(de_rec.element_obj);
			ico_el.Down().Left().Down();

			ico_el.InitAttributeArray(AttributeArray::TexCoord, v_arr);
			if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
				a_hdr->stretch_mask[0] = 1; // device box on				
				a_hdr->stretch_mask[3] = 1;

				a_hdr->offset_mask[0] = de_rec.c_width;
				a_hdr->offset_mask[1] = de_rec.c_height;
				a_hdr->offset_mask[2] = de_rec.width;
				a_hdr->offset_mask[3] = de_rec.height;

				if (de_rec.flags & DecodeRecord::FlagSize) {
					a_hdr->stretch_mask[0] |= 2;

					if (((I_64)de_rec._reserved[4] >> 63) == -1) {
						a_hdr->offset_mask[2] /= -reinterpret_cast<double *>(de_rec._reserved)[4];
						a_hdr->offset_mask[3] /= -reinterpret_cast<double *>(de_rec._reserved)[4];

						if ((a_hdr->offset_mask[2] < 16.0f) || (a_hdr->offset_mask[3] < 16.0f)) {
							a_hdr->offset_mask[2] = de_rec.width;
							a_hdr->offset_mask[3] = de_rec.height;

							if (de_rec.width > de_rec.height) {
								reinterpret_cast<double *>(de_rec._reserved)[4] = (de_rec.height >> 4);
							} else {
								reinterpret_cast<double *>(de_rec._reserved)[4] = (de_rec.width >> 4);
							}

							a_hdr->offset_mask[2] /= reinterpret_cast<double *>(de_rec._reserved)[4];
							a_hdr->offset_mask[3] /= reinterpret_cast<double *>(de_rec._reserved)[4];

							reinterpret_cast<double *>(de_rec._reserved)[4] = -reinterpret_cast<double *>(de_rec._reserved)[4];
						}

					} else {
						if (de_rec._reserved[4]) {
							a_hdr->offset_mask[2] *= reinterpret_cast<double *>(de_rec._reserved)[4];
							a_hdr->offset_mask[3] *= reinterpret_cast<double *>(de_rec._reserved)[4];

							a_hdr->offset_mask[3] -= 15;
						}
					}

					result = a_hdr->offset_mask[2];
					a_hdr->offset_mask[0] = (result + 15) & ~15;
					result = a_hdr->offset_mask[3];
					a_hdr->offset_mask[1] = (result + 15) & ~15;
				}

				v_arr.Release();
			}


			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(de_rec.element_obj.Acquire())) {
				reinterpret_cast<APPlane::Header *>(el_hdr+1)->deco_rec.target_time = de_rec.target_time;

				de_rec.element_obj.Release();
			}

			APPlane::TexCoordReset(de_rec.element_obj, 0);

			ogl_ptr->ResetElement(de_rec.element_obj); // Update();

			de_rec.element_obj = ico_el;
			

			result = 0;
		}
	}

	return result;
}

UI_64 OpenGL::APPlane::GetFirstScreen(float * box_vals) {
	UI_64 result(0);
	Element ico_el(*this);

	ico_el.Down().Left().Down();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(ico_el.Acquire())) {
		result = el_hdr->config.tex_unit_selector;

		ico_el.Release();
	}

	ico_el.GetRightMargin(box_vals);

	return result;
}


UI_64 OpenGL::Core::SetDecodeFrame(DecodeRecord & de_rec, SFSco::Object & pic_obj, UI_64 color_scheme) {
	UI_64 result(0), pic_offset(color_scheme >> 16);
	double normal_scale(0.0);
	unsigned int tex_box[16];
	unsigned int t_idx(0);
	AttributeArray t_arr;

	color_scheme &= 0x0000FFFF;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(de_rec.element_obj.Acquire())) {
		if (t_idx = el_hdr->config.tex_unit_selector & 0xFFFFFFFF) {
			// do somemething about color scheme
			if ((el_hdr->config.tex_unit_selector >> 56) != color_scheme) {
				el_hdr->config.tex_unit_selector <<= 8;
				color_scheme <<= 56;
				el_hdr->config.tex_unit_selector >>= 8;
				el_hdr->config.tex_unit_selector |= color_scheme;
				
				color_scheme = 1;
			} else {
				color_scheme = 0;
			}
		}

		de_rec.element_obj.Release();

		if (color_scheme) {
			ResetElement(de_rec.element_obj);

		}
	}

	

	if (t_idx) {		

		OpenGL::glBindTexture(GL_TEXTURE_2D, t_idx);

		de_rec.element_obj.InitAttributeArray(AttributeArray::TexCoord, t_arr);

		if (AttributeArray::Header * t_hdr = reinterpret_cast<AttributeArray::Header *>(t_arr.Acquire())) {

				if ((t_hdr->stretch_mask[0]) && (t_hdr->stretch_mask[1])) {
				// change de_rec
					de_rec.flags &= (~DecodeRecord::SizeMask);
					
					if (t_hdr->stretch_mask[0] & 2) {
 // scale override
						if (t_hdr->stretch_mask[3] == 1) {
							de_rec.element_obj.ClearEFlags(Element::FlagTouch2);
						} else {
							de_rec.element_obj.SetEFlags(Element::FlagTouch2);
						}

						if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(de_rec.element_obj.Acquire())) {
							el_hdr->root_cfg._maxW = 0;
							el_hdr->root_cfg._maxH = 0;

							de_rec.element_obj.Release();
						}

						if (de_rec._reserved[4]) {
							if (reinterpret_cast<double *>(de_rec._reserved)[4] < 0.0) {
								if (reinterpret_cast<double *>(de_rec._reserved)[4] == -1.5) {
									t_hdr->stretch_mask[3] = 0;
								} else {							
									t_hdr->stretch_mask[3] = -reinterpret_cast<double *>(de_rec._reserved)[4];
								}
							} else {
								t_hdr->stretch_mask[3] = 0x80000000 | (int)(2.0f*reinterpret_cast<double *>(de_rec._reserved)[4]);
							}						
						} else {
							if (t_hdr->stretch_mask[3] != 1) {
								de_rec.flags |= DecodeRecord::SizeSubSize;
							}

							t_hdr->stretch_mask[3] = 1;
						}
					}
				
					de_rec._reserved[4] = 0;


					if ((t_hdr->stretch_mask[3] & 0x80000000) == 0) {

						if (t_hdr->stretch_mask[3] > 64) t_hdr->stretch_mask[3] = 64;


						switch (t_hdr->stretch_mask[3]) {
							case 0:
								de_rec.flags |= DecodeRecord::SizeMinimize15;
								de_rec._reserved[4] = 0xBFF8000000000000;
							break;
							case 1:
								if ((de_rec.flags & DecodeRecord::SizeSubSize) == 0) {
									de_rec.flags |= DecodeRecord::FlagNoSize;
									de_rec._reserved[4] = 0;
								}
							break;
							default:
								de_rec.flags |= DecodeRecord::SizeMinimize;
								reinterpret_cast<double *>(de_rec._reserved)[4] = -t_hdr->stretch_mask[3];
								// feed back streth_mask[3]

						}


					} else { // maximize op
						t_hdr->stretch_mask[3] &= 0x0FFFFFFF;
						if (t_hdr->stretch_mask[3] > 32) t_hdr->stretch_mask[3] = 32;
						if (t_hdr->stretch_mask[3] < 3) t_hdr->stretch_mask[3] = 3;
						
						de_rec.flags |= DecodeRecord::SizeMaximize;
						de_rec._reserved[4] = 0x3FE0000000000000;
						reinterpret_cast<double *>(de_rec._reserved)[4] *= t_hdr->stretch_mask[3];

					}


					de_rec.pic_box[0] = t_hdr->stretch_vector[0];
					de_rec.pic_box[1] = t_hdr->stretch_vector[1];
					de_rec.pic_box[2] = t_hdr->stretch_vector[2];
					de_rec.pic_box[3] = t_hdr->stretch_vector[3];

					de_rec.precision &= 0xF0000000;
					de_rec.precision |= t_hdr->stretch_mask[3] & 0x0FFFFFFF; // height

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

					tex_box[0] = de_rec.pic_box[0];
				//	tex_box[0] += 255; tex_box[0] &= (~255);

					tex_box[1] = de_rec.pic_box[1];
				//	tex_box[1] += 255; tex_box[1] &= (~255);

					if (de_rec.precision & 0xF0000000) OpenGL::glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_box[0], tex_box[1], 0, GL_RGBA, GL_UNSIGNED_SHORT, 0);
					else OpenGL::glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_box[0], tex_box[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

					t_hdr->stretch_mask[1] = 0;
					result = 1;
				}
			t_arr.Release();
		}

		if (t_idx) {
			result |= 2;
		
			if (de_rec.flags & DecodeRecord::FlagNoSize) {

				if (unsigned char * pipo = reinterpret_cast<unsigned char *>(pic_obj.Acquire())) {
					if (de_rec.precision & 0xF0000000) OpenGL::glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, de_rec.pic_box[0], de_rec.pic_box[1], GL_RGBA, GL_UNSIGNED_SHORT, pipo + pic_offset);
					else OpenGL::glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, de_rec.pic_box[0], de_rec.pic_box[1], GL_RGBA, GL_UNSIGNED_BYTE, pipo + pic_offset);

					pic_obj.Release();
				}


			} else {			
				tex_box[0] = de_rec.pic_box[0]; tex_box[1] = de_rec.pic_box[1];
				tex_box[2] = de_rec.pic_box[2]; tex_box[3] = de_rec.pic_box[3];
				tex_box[4] = de_rec.c_width; tex_box[5] = de_rec.c_height;

				tex_box[6] = tex_box[7] = de_rec.precision & 0x0FFFFFFF;

				tex_box[8] = tex_box[9] = 0;
				tex_box[10] = tex_box[11] = 0;

				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(de_rec.element_obj.Acquire())) {

					switch (de_rec.flags & DecodeRecord::SizeMask) {
						case DecodeRecord::SizeMinimize:
							tex_box[14] = (tex_box[0]*tex_box[6]); tex_box[15] = (tex_box[1]*tex_box[7]);
							if ((el_hdr->root_cfg._maxW + tex_box[14]) > de_rec.c_width) {
								el_hdr->root_cfg._maxW = de_rec.c_width - tex_box[14];
							}

							if ((el_hdr->root_cfg._maxH + tex_box[15]) > de_rec.c_height) {
								el_hdr->root_cfg._maxH = de_rec.c_height - tex_box[15];
							}
						break;
						case DecodeRecord::SizeMinimize15:
							tex_box[14] = ((tex_box[0]*3)>>1); tex_box[15] = ((tex_box[1]*3)>>1);
							if ((el_hdr->root_cfg._maxW + tex_box[14]) > de_rec.c_width) {
								el_hdr->root_cfg._maxW = de_rec.c_width - tex_box[14];
							}

							if ((el_hdr->root_cfg._maxH + tex_box[15]) > de_rec.c_height) {
								el_hdr->root_cfg._maxH = de_rec.c_height - tex_box[15];
							}
						break;
						case DecodeRecord::SizeMaximize:
							tex_box[14] = ((tex_box[0]<<1)/tex_box[6]); tex_box[15] = ((tex_box[1]<<1)/tex_box[7]);
							if ((el_hdr->root_cfg._maxW + tex_box[14]) > de_rec.c_width) {
								el_hdr->root_cfg._maxW = de_rec.c_width - tex_box[14];
							}

							if ((el_hdr->root_cfg._maxH + tex_box[15]) > de_rec.c_height) {
								el_hdr->root_cfg._maxH = de_rec.c_height - tex_box[15];
							}
						break;
						case DecodeRecord::SizeSubSize:
							if ((el_hdr->root_cfg._maxW + tex_box[0]) > de_rec.c_width) {
								el_hdr->root_cfg._maxW = de_rec.c_width - tex_box[0];
							}

							if ((el_hdr->root_cfg._maxH + tex_box[1]) > de_rec.c_height) {
								el_hdr->root_cfg._maxH = de_rec.c_height - tex_box[1];
							}
						break;
					
					}

					if (el_hdr->root_cfg._maxW < 0) el_hdr->root_cfg._maxW = 0;
					if (el_hdr->root_cfg._maxH < 0) el_hdr->root_cfg._maxH = 0;

					tex_box[8] = el_hdr->root_cfg._maxW; tex_box[9] = el_hdr->root_cfg._maxH;
				
					de_rec.element_obj.Release();	
				}

				if (unsigned int * spi_ptr = reinterpret_cast<unsigned int *>(pic_size_buf.Acquire())) {
					if (unsigned char * im_ptr = reinterpret_cast<unsigned char *>(pic_obj.Acquire())) {

						switch (de_rec.flags & DecodeRecord::SizeMask) {
							case DecodeRecord::SizeMinimize:
								if (de_rec.precision & 0xF0000000) Average_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
								else Average_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

							break;
							case DecodeRecord::SizeMinimize15:
								if (de_rec.precision & 0xF0000000) Average3_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
								else Average3_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

							break;
							case DecodeRecord::SizeMaximize:
								if (de_rec.precision & 0xF0000000) Magnify_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
								else Magnify_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

							break;
							case DecodeRecord::SizeSubSize:
								if (de_rec.precision & 0xF0000000) SubImage_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
								else SubImage_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

							break;

						}

						pic_obj.Release();
	
					}

					OpenGL::glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, tex_box[0], tex_box[1], GL_RGBA, GL_UNSIGNED_BYTE, spi_ptr);

					pic_size_buf.Release();
				}
			}
		}
	}

	return result;

}

// function area =======================================================================================================================================================================================================================================
OpenGL::Button OpenGL::APPlane::function_area;
OpenGL::APPlane::FunctionSet OpenGL::APPlane::_functions;


unsigned int OpenGL::APPlane::AddFunction(Button & button_obj, UI_64 button_group_id, float size) {
	unsigned int result(0);
	float size_vec[4];
	unsigned int slot_top(0);

	size_vec[0] = size_vec[1] = size_vec[2] = size_vec[3] = 1.0f;

	if (Core * core_ogl = function_area.GetContext()) {

		result = core_ogl->CreateElement(Element::Quad, button_obj, 4, &function_area);
	

		if (result != -1) {

			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(function_area.Acquire())) {
				Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(elhdr+1);

				slot_top = bc_hdr->slot_top - bc_hdr->slot_runner;

				if ((elhdr->config.anchor & 0xFF00) == FitHeight) {					
					size_vec[1] = size;
				} else {
					size_vec[0] = size; 
				}

				if (size_vec[0] == 1.0f) {
					size_vec[0] = bc_hdr->size;
				} else {
					size_vec[1] = bc_hdr->size;
				}

				function_area.Release();
			}
		
			if (slot_top == 0) {
				if (function_area.Expand(32*sizeof(Button::ClipHeader::ClipSlot)) != -1) {
					if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(function_area.Acquire())) {
					
						reinterpret_cast<Button::ClipHeader *>(ehdr+1)->slot_top += 32;
						function_area.Release();
					}
				}
			}
		
			result = button_group_id & Button::Header::GroupMask;

			if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(function_area.Acquire())) {
				Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(ehdr+1);

				if (result > bc_hdr->slot_runner) {
					result = bc_hdr->slot_runner++;
					
					if (result) {
						bc_hdr->slots[result].first_b = bc_hdr->slots[result-1].first_b + bc_hdr->slots[result-1].b_count;
					} else {
						bc_hdr->slots[result].first_b = 0;
					}
					bc_hdr->slots[result].b_count = 1;
				} else {
					bc_hdr->slots[result].b_count++;
				}


				function_area.Release();
			}
		}

		button_obj.SetDefaultRect();
		button_obj.Size(size_vec);


		if (Element::Header * b_hdr = reinterpret_cast<Element::Header *>(button_obj.Acquire())) {
			b_hdr->_reserved = result & Button::Header::GroupMask;		

			if (button_group_id >>= 32) {			
				b_hdr->x_tra = button_group_id;
			}
			button_obj.Release();
		}
	}
	
	return result;
}




void OpenGL::APPlane::RemoveFunction(Element & fun_but) {	
	Element blank;
	unsigned int gid(-1), bcount(0);
	
	if (Element::Header * b_hdr = reinterpret_cast<Element::Header *>(fun_but.Acquire())) {
		gid = b_hdr->_reserved & Button::Header::GroupMask;

		fun_but.Release();
	}

	if (gid != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(function_area.Acquire())) {
			Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(el_hdr+1);

			bcount = bc_hdr->slots[gid].b_count;

			if (gid < --bc_hdr->slot_runner) {
				System::MemoryCopy(bc_hdr->slots + gid, bc_hdr->slots + gid + 1, (bc_hdr->slot_runner - gid)*sizeof(Button::ClipHeader::ClipSlot));

				for (unsigned int i(gid);i<bc_hdr->slot_runner;i++) {
					bc_hdr->slots[i].first_b -= bcount;
				}
			}

			function_area.Release();
		}

		for (unsigned int i(0);i<bcount;i++) {
			blank = fun_but;
			fun_but.Left();

			blank.Destroy();			
		}
	}
}

bool OpenGL::APPlane::TestCA() {
	return current_app;
}

void OpenGL::APPlane::ResetFunction(Element & fbut) {
	_functions.element[1] = fbut;
}

void OpenGL::APPlane::DumpFunction(Element & fbut) {
	if (Apps::XFiles::SelectFL()) _functions.element[1].Blank();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(fbut.Acquire())) {
		el_hdr->_reserved |= Button::Header::RemoveFunction;
		fbut.Release();
	}
}

UI_64 OpenGL::APPlane::FunctionStretch(unsigned int & i_count, unsigned int i_reset) {
	UI_64 result(1);
	
	if (Core * core_ogl = function_area.GetContext())
	if (_functions.element[0] != _functions.element[1]) {
		
		result = 0;
		
	//	if (_functions._clock++ & 1) {
			if (_functions.element[0]) { // current function hide			

				if (_functions._count--) {
					core_ogl->AttHide(_functions.op_el);
					_functions.op_el.Right();

					result = 1;
				}

				if (_functions._count == 0) {
					result = 0;
					if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(_functions.element[0].Acquire())) {
						result = el_hdr->_reserved & Button::Header::RemoveFunction;

						_functions.element[0].Release();
					}

					if (result) RemoveFunction(_functions.element[0]);

					_functions.element[0].Blank();
					
					result = 0;
					if (_functions.element[1]) result = 1;
				}
		
			} else {
				// show new, when show complete duplicate function

				if (_functions._count == 0) {
					_functions.op_el = _functions.element[1];
					if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(_functions.op_el.Acquire())) {
						_functions.group_id = ehdr->_reserved & Button::Header::GroupMask;
						_functions._life_time = ehdr->x_tra;
						_functions.op_el.Release();
					}
				}

				core_ogl->AttShow(_functions.op_el);
				_functions.op_el.Left();

				if (_functions.op_el != _functions.element[1]) {
					if (Element::Header * ehdr = reinterpret_cast<Element::Header *>(_functions.op_el.Acquire())) {
						if (_functions.group_id != ehdr->_reserved) result = 1;

						_functions.op_el.Release();
					}
				} else {
					result = 1;
				}
			
				if (result) {
					_functions.op_el.Right();
					_functions.element[0] = _functions.element[1];
					_functions._clock = _functions._life_time;

					i_count = i_reset;
//					_inertion_count = PrimitivePass::detail_l + 1;
/*
					for (unsigned int i(0);i<=_functions._count;i++) {
						att_domain.ResetXC(_functions.element[0]);
						_functions.element[0].Left();
					
					}

					_functions.element[0] = _functions.element[1];

*/
				}

				_functions._count++;
			}
//		}

	} else {
		if (_functions._life_time) {
			if (--_functions._clock == 0) {
				_functions._life_time = 0;
				_functions.element[1].Blank();

				i_count = i_reset;
//				_inertion_count = PrimitivePass::detail_l + 1;
			}
		}
	}

	return result;
}

void OpenGL::APPlane::FunctionAlive() {
	_functions._clock = _functions._life_time;
}

UI_64 OpenGL::APPlane::CreateFunctionArea(OpenGL::Core * core_ogl, int f_w, unsigned int sicos) {
	UI_64 result(-1);

	_functions._clock = 0;
	_functions._life_time = -1;
	_functions._count = 0;
	_functions.group_id = 0;


	result = core_ogl->CreateButtonClip(function_area);
	if (result != -1) {
		if (core_ogl->GetMenuCorner() & 2) function_area.ConfigureClip(LeftBottomFront, FitHeight, sicos, f_w, 3.0f);
		else function_area.ConfigureClip(RightBottomFront, FitHeight, sicos, f_w, 3.0f);

		result = 0;
	}

	return result;
}

void OpenGL::APPlane::PopApp(Element & p_el) {
	
	if (current_app)
	if (current_app != p_el) {		
		current_app.PushApp();	
	}
	
	current_app(p_el);		
	Apps::NotificationArea::SetPlayList(current_app);
}

void OpenGL::APPlane::PushApp() {
	Hide();
	if (Core * core_ogl = GetContext()) core_ogl->ResetElement(*this);

	Apps::NotificationArea::AddAppEntry(*this);
}






