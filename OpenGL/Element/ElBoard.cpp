#include "OpenGL\OpenGL.h"

#include "System\SysUtils.h"

OpenGL::StringsBoard::StringsBoard() {
	SetTID(type_id);
}

OpenGL::StringsBoard::~StringsBoard() {

}

UI_64 OpenGL::Core::CreateStringsBoard(StringsBoard & s_board, unsigned int la_count, const Element::LabelCfg * la_cfg, const Element * parent) {
	UI_64 result = CreateElement(Element::Quad, s_board, 4, parent, sizeof(StringsBoard::Header) + la_count*sizeof(Element::LabelCfg));

	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(s_board.Acquire())) {
			reinterpret_cast<StringsBoard::Header *>(el_hdr + 1)->la_count = la_count;
			System::MemoryCopy(reinterpret_cast<StringsBoard::Header *>(el_hdr + 1)->la_cfg, la_cfg, la_count*sizeof(Element::LabelCfg));
			s_board.Release();
		}

		for (unsigned int i(0);i<la_count;i++) s_board.AddLabel(la_cfg[i]);
	}

	return result;
}


// ============================================================================================================================================================================


OpenGL::D2Board::D2Board() {
	SetTID(type_id);
}


OpenGL::D2Board::~D2Board() {

}

void OpenGL::D2Board::SetDataScale(const float * s_vals) {
	Element d_el(*this);

	d_el.Down().Right();
	d_el.Rescale(s_vals);

}

void OpenGL::D2Board::SetDataOffset(const float * d_vals) {
	Element d_el(*this);

	d_el.Down().Right();
	d_el.RelocateXY(d_vals);

}

// =================================================================================================================================================================================

OpenGL::BoardClip::BoardClip() {
	SetTID(type_id);
}

OpenGL::BoardClip::~BoardClip() {


}

UI_64 OpenGL::Core::CreateBoardClip(BoardClip & lb_el, float w, float h, unsigned int flags, const Element * parent) {
	UI_64 result = CreateElement(Element::Quad, lb_el, 4, parent, sizeof(BoardClip::Header));

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(lb_el.Acquire())) {
		reinterpret_cast<BoardClip::Header *>(el_hdr + 1)->cp_flags = flags;

		reinterpret_cast<BoardClip::Header *>(el_hdr + 1)->bc_width = w;
		reinterpret_cast<BoardClip::Header *>(el_hdr + 1)->bc_height = h;

		lb_el.Release();

		lb_el.SetAnchorMode(LeftBottomFront);
	}

	return result;
}


UI_64 OpenGL::BoardClip::Configure(const LabelCfg * info_label, unsigned int la_count, unsigned int b_color) {
	UI_64 result(-1);
	StringsBoard s_board;

	float temp_vals[4];

	if (Core * ogl_core = GetContext()) {
		result = ogl_core->CreateStringsBoard(s_board, la_count, info_label, this);

		if (result != -1) {
			if (b_color) {
				Core::ColorConvert(temp_vals, b_color);
				s_board.CreateEBorder(1.1f, 0x80000000);
				s_board.SetEBorderColor(temp_vals);
			}
		}
	}


	return result;
}

UI_64 OpenGL::BoardClip::CreateD2Board(D2Board & p_board, const D2Board::PlotCfg & p_cfg) {
	UI_64 result(-1);
	Element plot_el;

	float temp_vals[4] = {0.0f, 0.0f, 1.0f, 1.0f};

	if (Core * ogl_core = GetContext()) {
		result = ogl_core->CreateElement(Element::Quad, p_board, 4, this, sizeof(D2Board::PlotCfg));

		if (result != -1) {
			if (Element::Header * cl_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->cp_board_count++;

				temp_vals[0] = reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->bc_width;
				temp_vals[1] = reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->bc_height;

				Release();
			}

			p_board.SetRect(temp_vals);			
			p_board.Rescale(temp_vals);

			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(p_board.Acquire())) {
				el_hdr->_flags |= Element::FlagIdent;

				System::MemoryCopy(el_hdr + 1, &p_cfg, sizeof(D2Board::PlotCfg));

				p_board.Release();
			}


			if (p_cfg.flags & D2Board::FlagBorder) {
				result = p_board.CreateEBorder(0.0f, 0x80000000);
				p_board.SetEBorderColor(p_cfg.border_color);
			}

			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagCaption) {
					result = p_board.AddLabel(p_cfg.caption_label);
				}
			}



			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagLeftAxis) {


				}
			}

			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagRightAxis) {


				}
			}

			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagTopAxis) {


				}
			}

			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagBottomAxis) {


				}
			}


			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagVerticalGrid) {


				}
			}

			if (result != -1) {
				if (p_cfg.flags & D2Board::FlagHorizontalGrid) {


				}
			}

			if (result != -1) {
				switch (p_cfg.flags & D2Board::FlagPlotTypeMask) {
					case D2Board::FlagPlotTypeLine:
						result = ogl_core->CreateElement(Element::Line, plot_el, p_cfg.capacity, &p_board);
						plot_el.ConfigureLarge(-1);					
					break;
					case D2Board::FlagPlotTypePoint:

					break;
					case D2Board::FlagPlotTypeBar:

					break;

				}

				plot_el.SetFlats(p_cfg.flat_colors);
			}

		}
	}

	return result;
}



UI_64 OpenGL::BoardClip::CoordsReset(float * box_vals) {
	Element b_info(*this), b_data, el_text;
	unsigned int b_count(0);
	float temp_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f};

	RelocateXY(box_vals);

	if (Element::Header * cl_hdr = reinterpret_cast<Element::Header *>(b_info.Acquire())) {
		reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->cp_board_count++;

		temp_vals[8] = reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->bc_width;
		temp_vals[9] = reinterpret_cast<BoardClip::Header *>(cl_hdr + 1)->bc_height;

		b_info.Release();
	}


	b_info.Down();
	el_text(b_info).Down();

// info board
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(b_info.Acquire())) {
		StringsBoard::Header * str_hdr = reinterpret_cast<StringsBoard::Header *>(el_hdr + 1);

		temp_vals[0] = 0.0f;
		for (unsigned int i(0);i<str_hdr->la_count;i++) {
			temp_vals[1] += str_hdr->la_cfg[i].f_size;
			el_text.RelocateXY(temp_vals);
			el_text.GetRightMargin(temp_vals + 4);

			if (temp_vals[6] > temp_vals[2]) temp_vals[2] = temp_vals[6];

			el_text.Left();
		}

		b_info.Release();
	}



	b_data = b_info;
	for (b_data.Left();b_data != b_info;b_data.Left()) if (b_data.IsVisible()) b_count++;
	
	if (b_count) {
		box_vals[2] -= (temp_vals[2] + 10.0f*b_count);
		box_vals[2] /= b_count;


		temp_vals[0] = temp_vals[2] + 10.0f;
		temp_vals[1] = 0.0f;

		temp_vals[8] = box_vals[2];		

		for (b_data.Left();b_data != b_info;b_data.Left()) {
			if (b_data.IsVisible()) {
				b_data.RelocateXY(temp_vals);
				b_data.Rescale(temp_vals + 8);

				temp_vals[0] += box_vals[2] + 10.0f;

				if (Element::Header * b_hdr = reinterpret_cast<Element::Header *>(b_data.Acquire())) {
					el_text(b_data).Down();
					if (reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->flags & D2Board::FlagBorder) el_text.Left();

					temp_vals[12] = reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->caption_label.offset[0]/temp_vals[8];
					temp_vals[13] = (reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->caption_label.f_size + reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->caption_label.offset[1])/temp_vals[9];
					el_text.RelocateXY(temp_vals + 12);
					
					temp_vals[12] = reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->caption_label.f_size/temp_vals[8];
					temp_vals[13] = -reinterpret_cast<D2Board::PlotCfg *>(b_hdr + 1)->caption_label.f_size/temp_vals[9];
					el_text.Rescale(temp_vals + 12);

					b_data.Release();
				}
			}
		}
	}
	
	return 0;
}


