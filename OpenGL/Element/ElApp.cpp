/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "OpenGL\Font.h"

#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"


#include "System\SysUtils.h"


#pragma warning(disable:4244)

OpenGL::ClipElement::ClipElement() {
	SetTID(type_id);
}

OpenGL::ClipElement::~ClipElement() {

}

UI_64 OpenGL::ClipElement::Configure(unsigned int color, float  * box_vals, unsigned int sval) { // width_adjust, height_adjust, full_width, full_height
	UI_64 result(0);	
	float * elptr(0), * b_ptr(0), temp_vals[4];
	Core * core_ptr(0);
	AttributeArray elarr, butarr;
	Element prnt(*this);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<Core *>(elhdr->_context);
		Release();
	}

	prnt.Up().Up();


	if (core_ptr) {
		// configure sizes, anchors and border
		InitAttributeArray(AttributeArray::Vertex, elarr);

		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elarr.Acquire())) {
			elptr = reinterpret_cast<float *>(ahdr + 1);
			
			if (core_ptr->IsRoot(prnt)) {
				core_ptr->GetRootBox(temp_vals);
/*
				core_ptr->GetFunctionCoords(butarr);
				if (AttributeArray::Header * bhdr = reinterpret_cast<AttributeArray::Header *>(butarr.Acquire())) {
					b_ptr = reinterpret_cast<float *>(bhdr + 1);
*/
				elptr[4] = -Core::db_rect[2]; // b_ptr[0] - b_ptr[4];
				
				elptr[9] = temp_vals[1]; // b_ptr[9];

				if (box_vals[1] > 0.0f) {
					elptr[9] -= box_vals[1];
					ahdr->translate_vector[1] = box_vals[1];
				} else {
					elptr[9] += box_vals[1];
				}
								
				if (box_vals[3] > 0.0f) {
					if (box_vals[1] < 0.0f) {
						ahdr->translate_vector[1] = elptr[9]; //  + box_vals[1];

					}

					elptr[9] = box_vals[3];
				}

				elptr[13] = elptr[9];						
/*
					butarr.Release();
				}
*/

				
				if (box_vals[0] > 0.0f) {
					elptr[4] -= box_vals[0];
					ahdr->translate_vector[0] = box_vals[0];
						
				} else elptr[4] += box_vals[0];


				if (box_vals[2] > 0.0f) {			
					if (box_vals[0] < 0.0f) {
						ahdr->translate_vector[0] = elptr[4]; //  + box_vals[0];						
					}

					elptr[4] = box_vals[2];
				} else {
					elptr[4] += (temp_vals[0] + box_vals[2]);
				}

				elptr[12] = elptr[4];

//				ahdr->translate_vector[0] += bhdr->translate_vector[0];
//				ahdr->translate_vector[1] += bhdr->translate_vector[1];

				ahdr->reserved[3] |= 1;


			} else {
				result = 0;

				if (Element::Header * p_hdr = reinterpret_cast<Element::Header *>(prnt.Acquire())) {
					if ((p_hdr->config.att_flags & 0xFFFF0000) == Element::ClipRegion) result = 1;
					prnt.Release();
				}

				if (result) {
					// get vertex 3
					prnt.InitAttributeArray(AttributeArray::Vertex, butarr);
					if (AttributeArray::Header * bhdr = reinterpret_cast<AttributeArray::Header *>(butarr.Acquire())) {
						b_ptr = reinterpret_cast<float *>(bhdr + 1);

						elptr[12] = b_ptr[12];
						elptr[13] = b_ptr[13];
		
						butarr.Release();
					}

					prnt.Down();

					// adjust coords
					prnt.InitAttributeArray(AttributeArray::Vertex, butarr);
					butarr.LocalDAdjust(elptr + 12, 1);

				} else {
					prnt.Down();

					prnt.InitAttributeArray(AttributeArray::Vertex, butarr);
					if (AttributeArray::Header * bhdr = reinterpret_cast<AttributeArray::Header *>(butarr.Acquire())) {
						b_ptr = reinterpret_cast<float *>(bhdr + 1);

						elptr[12] = b_ptr[12];
						elptr[13] = b_ptr[13];
		
						butarr.Release();
					}
				}

				elptr[14] = elptr[2];
				elptr[15] = elptr[3];


				if (box_vals[1] > 0.0f) {
					elptr[13] -= box_vals[1];
					ahdr->translate_vector[1] = box_vals[1];
				} else {
					elptr[13] += box_vals[1];
				}
				
				
				if (box_vals[3] > 0.0f) {
					if (box_vals[1] < 0.0f) {
						ahdr->translate_vector[1] = elptr[13];

					}

					elptr[13] = box_vals[3];
				}

				elptr[9] = elptr[13];

				if (box_vals[0] > 0.0f) {
					elptr[12] -= box_vals[0];
					ahdr->translate_vector[0] = box_vals[0];
						
				} else elptr[12] += box_vals[0];


				if (box_vals[2] > 0.0f) {			
					if (box_vals[0] < 0.0f) {
						ahdr->translate_vector[0] = elptr[12]; //  + box_vals[0];						
					}

					elptr[12] = box_vals[2];
				}

				elptr[4] = elptr[12];

			}

			elarr.Release();
		}

		sval &= 3;
		elarr.SetStretchMask(-(sval & 1), -(sval >> 1), 0);
		elarr.CalculateBox(LeftBottomFront);
		
		InitAttributeArray(AttributeArray::Color, elarr);
		
		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elarr.Acquire())) {
			Core::ColorConvert(reinterpret_cast<float *>(ahdr + 1), (int)color, 4);

			elarr.Release();
		}
							
	}

	return result;
}

UI_64 OpenGL::Core::CreateClip(ClipElement & c_el, const Element * parent, unsigned int s_signal) {
	UI_64 result = CreateElement(Element::ClipRegion, c_el, 0, parent);

	if ((result != -1) && (s_signal)) {
		Element scene(c_el);
		scene.Down();

		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(scene.Acquire())) {
			el_hdr->_flags |= Element::FlagNegTex;
			scene.Release();
		}
	}

	return result;
}

// ==============================================================================================================================================================================================================================

OpenGL::ButtonGrid::ButtonGrid() {
	SetTID(type_id);
}

OpenGL::ButtonGrid::~ButtonGrid() {

}

UI_64 OpenGL::Core::CreateButtonGrid(ButtonGrid & a_obj, const Element * p_obj, UI_64 (* size_cbk)(Element &), unsigned int t_val) {
	UI_64 result(-1);
	Element scel, x_btn;

	result = CreateElement(Element::ClipRegion, a_obj, 4, p_obj, sizeof(ButtonGrid::Header));
	if (result != -1) {
		scel = a_obj;
		scel.Down();
		scel.SetEFlags(Element::FlagScroll);



		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(a_obj.Acquire())) {
			ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

			bg_hdr->top_but = x_btn;
			bg_hdr->bottom_but = x_btn;

			bg_hdr->size_action_ptr = size_cbk;

			if (t_val == 0) glGenTextures(1, &bg_hdr->button_set.icon_tid);
			else {
				bg_hdr->button_set.icon_tid = t_val;
				bg_hdr->icon_runner = -1;
			}

			elhdr->config.anchor = LeftBottomFront;

			a_obj.Release();
		}


		a_obj.SetDefaultRect();
	}

	return result;
}

unsigned int OpenGL::ButtonGrid::LoadIcon(unsigned short iid, HMODULE libin) {
	unsigned int result(0);
	
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(el_hdr + 1);

		reinterpret_cast<Core *>(el_hdr->_context)->LoadIcon(bg_hdr->button_set.icon_box[2], bg_hdr->button_set.icon_box[3], iid, libin, bg_hdr->button_set.icon_tid, bg_hdr->icon_runner);

		result = bg_hdr->icon_runner++; // if (glGetError() == 0) 
		if (bg_hdr->icon_runner >= Core::max_texture_layers) bg_hdr->icon_runner = Core::max_texture_layers - 1;

		Release();	
	}


	return result;
}

void OpenGL::ButtonGrid::SetHMode() {
	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(Acquire())) {
		reinterpret_cast<ButtonGrid::Header *>(el_hdr + 1)->ui_reserved[1] = 1;

		Release();
	}
}

UI_64 OpenGL::ButtonGrid::Jump(int b_id, UI_64 selector) {
	int but_offset[2] = {0, 0};
	OpenGL::Element scene;
	Button b_obj;


	scene(*this);
	scene.Down();

	if (OpenGL::Element::Header * fl_hdr = reinterpret_cast<OpenGL::Element::Header *>(Acquire())) {		
		OpenGL::ButtonGrid::Header * b_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(fl_hdr + 1);


		if (b_obj(b_hdr->last_but)) {
			if (Element::Header * bel_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
				bel_hdr->xc_index = 0;
				b_obj.Release();
			}

			ButtonLeave(b_obj);

			if (b_id > 0) {
				for (b_obj.Left();b_obj != b_hdr->last_but;b_obj.Left()) {
					if (Element::Header * bel_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
						if (selector == bel_hdr->x_tra) {
							bel_hdr->xc_index = 1;
							selector = -1;
						}
						b_obj.Release();
					}
					if (selector == -1) break;
				}
			} else {
				for (b_obj.Right();b_obj != b_hdr->last_but;b_obj.Right()) {
					if (Element::Header * bel_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
						if (selector == bel_hdr->x_tra) {
							bel_hdr->xc_index = 1;
							selector = -1;
						}
						b_obj.Release();
					}
					if (selector == -1) break;
				}
			}

			ButtonHoover(b_obj);

			b_hdr->last_but = b_obj;
		}


		if (b_hdr->ui_reserved[1]) {
			but_offset[0] = (b_hdr->cell_width + 2.0f)*b_id;

		} else {
			but_offset[1] = (b_hdr->cell_height + 2.0f)*b_id;
		}

		Release();
	}

	scene.Drag(but_offset[0], -but_offset[1]);

	return b_id;
}

UI_64 OpenGL::ButtonGrid::ResetGrid(unsigned int count_er) {
	__declspec(align(16)) char zombie_vals[512];
	UI_64 result(0);
	OpenGL::Core * ogl_core(0);
	float but_offset[16] = {0.0, 0.0, 0.0, 0.0,		0.0, 0.0, 0.0, 0.0,		0.0, 0.0, 0.0, 0.0,		2.0f, 0.0, 0.0, 0.0};
	unsigned int col_id(0), center_p(0), b_counter(0), f_pos(-1), f_type(-1), cf_type(-1), h_type(0);

	OpenGL::ButtonGrid::Header bg_hdr;
	OpenGL::Element scene, tt_el;



	bg_hdr.top_but(*this);
	bg_hdr.top_but.Down().Down();

	if (OpenGL::Element::Header * fl_hdr = reinterpret_cast<OpenGL::Element::Header *>(Acquire())) {
		ogl_core = reinterpret_cast<OpenGL::Core *>(fl_hdr->_context);
		fl_hdr->_flags &= ~(UI_64)Element::FlagSized;

		OpenGL::ButtonGrid::Header * b_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(fl_hdr + 1);
			
		h_type = b_hdr->ui_reserved[1];
		b_hdr->top_but = bg_hdr.top_but;
		b_hdr->bottom_but = bg_hdr.top_but;
		b_hdr->bottom_but.Right();
			
		System::MemoryCopy(&bg_hdr, b_hdr, sizeof(OpenGL::ButtonGrid::Header));

		Release();
	}

	scene(*this);
	scene.Down();

	
	if (count_er) {
		if (h_type == 0) {
			if ((count_er + 5) < bg_hdr.page_size) {
				col_id = ((count_er + bg_hdr.row_count - 1)/ bg_hdr.row_count);
		
				if (col_id < bg_hdr.column_count) {
					col_id = (bg_hdr.row_count<<1)/3;

					b_counter = 1;
				
					for (unsigned int s(col_id);s<count_er;s+=col_id) {
						b_counter++;
					}
					if (b_counter < bg_hdr.column_count) bg_hdr.column_count = b_counter;
				}

				b_counter = 0;
				col_id = 0;
			}

			but_offset[11] = (bg_hdr.width - bg_hdr.cell_width*bg_hdr.column_count - (bg_hdr.margin[0] + bg_hdr.margin[2]))/(bg_hdr.column_count + 3);

			but_offset[4] = bg_hdr.margin[0] + 2.0f*but_offset[11];
			but_offset[5] = (bg_hdr.cell_height + 2.0f);
			but_offset[6] = (bg_hdr.cell_width + but_offset[11]);
			but_offset[7] = bg_hdr.margin[1] + 2.0f;
		} else {
			if ((count_er + 5) < bg_hdr.page_size) {
				col_id = ((count_er + bg_hdr.column_count - 1)/ bg_hdr.column_count);
		
				if (col_id < bg_hdr.row_count) {
					col_id = (bg_hdr.column_count<<1)/3;

					b_counter = 1;
				
					for (unsigned int s(col_id);s<count_er;s+=col_id) {
						b_counter++;
					}
					if (b_counter < bg_hdr.row_count) bg_hdr.row_count = b_counter;
				}

				b_counter = 0;
				col_id = 0;
			}

			but_offset[11] = (bg_hdr.height - bg_hdr.cell_height*bg_hdr.row_count - (bg_hdr.margin[1] + bg_hdr.margin[3]))/(bg_hdr.row_count + 3);

			but_offset[4] = bg_hdr.margin[0] + 2.0f;
			but_offset[5] = (bg_hdr.cell_height + but_offset[11]);
			but_offset[6] = (bg_hdr.cell_width + 2.0f);
			but_offset[7] = bg_hdr.margin[1] + 2.0f*but_offset[11];




		}


		but_offset[0] = but_offset[4];
		but_offset[1] = but_offset[7];

			

		for (;;) {
System::XMMPush(zombie_vals);
			f_type = bg_hdr.format_button(bg_hdr.top_but, bg_hdr.button_set, f_pos, bg_hdr.ui_reserved[0]);
System::XMMPop(zombie_vals);
			
			if (bg_hdr.ui_reserved[0]) cf_type = -1;

			if (f_pos == -1) break;
			if (!f_type) continue;

									
			if (f_type != cf_type) {

				if (cf_type != -1) {
					if (h_type == 0) {
						if ((((b_counter - col_id + 1) >= bg_hdr.cell_count) && (bg_hdr.column_count == 1)) ||
							(((b_counter + (bg_hdr.column_count<<1) - col_id) >= bg_hdr.cell_count) && (bg_hdr.column_count > 1))) {

							result = 0;
							if (bg_hdr.column_count > 1)
								if ((b_counter + (bg_hdr.column_count<<1) - col_id) > bg_hdr.cell_count) result = 1;
							result <<= 62;
							
							for (;b_counter < bg_hdr.cell_count;b_counter++) {
								bg_hdr.top_but.RelocateXY(but_offset);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr.top_but.Acquire())) {
									bb_hdr->x_tra = f_pos;
									bg_hdr.top_but.Release();
								}

								bg_hdr.top_but.Hide();
								bg_hdr.top_but.Left();

								but_offset[0] += but_offset[6];

								if (++col_id >= bg_hdr.column_count) {
									col_id = 0;

									but_offset[0] = but_offset[4];
									but_offset[1] += but_offset[5];
								}
							}

							if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr.bottom_but.Acquire())) {
								bb_hdr->x_tra = result | f_pos;

								bg_hdr.bottom_but.Release();
							}

							// set some ids for bottom_but

							
						} else {
							tt_el = bg_hdr.top_but;
							tt_el.Left();

							for (unsigned int k(col_id);k<bg_hdr.column_count;k++) {
								tt_el.RelocateXY(but_offset);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
									bb_hdr->x_tra = f_pos;
									tt_el.Release();
								}

								tt_el.Hide();
								tt_el.Left();

								but_offset[0] += but_offset[6];
								b_counter++;
							}

							but_offset[0] = but_offset[4];
							but_offset[1] += but_offset[5];


							if ((bg_hdr.column_count > 1) && (col_id)) {
								for (unsigned int k(0);k<bg_hdr.column_count;k++) {
									tt_el.RelocateXY(but_offset);

									if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
										bb_hdr->x_tra = f_pos;
										tt_el.Release();
									}

									tt_el.Hide();
									tt_el.Left();
	
									but_offset[0] += but_offset[6];
									b_counter++;
								}
								but_offset[0] = but_offset[4];
								but_offset[1] += but_offset[5];
							}

							bg_hdr.top_but.ShiftLeft(tt_el.GetID());
					
							col_id = 0;
						}
					} else { // horizontal
						if ((((b_counter - col_id + 1) >= bg_hdr.cell_count) && (bg_hdr.row_count == 1)) ||
							(((b_counter + (bg_hdr.row_count<<1) - col_id) >= bg_hdr.cell_count) && (bg_hdr.row_count > 1))) {

							result = 0;
							if (bg_hdr.row_count > 1)
								if ((b_counter + (bg_hdr.row_count<<1) - col_id) > bg_hdr.cell_count) result = 1;
							result <<= 62;
							
							for (;b_counter < bg_hdr.cell_count;b_counter++) {
								bg_hdr.top_but.RelocateXY(but_offset);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr.top_but.Acquire())) {
									bb_hdr->x_tra = f_pos;
									bg_hdr.top_but.Release();
								}

								bg_hdr.top_but.Hide();
								bg_hdr.top_but.Left();

								but_offset[1] += but_offset[5];

								if (++col_id >= bg_hdr.row_count) {
									col_id = 0;

									but_offset[0] += but_offset[6];
									but_offset[1] = but_offset[7];
								}
							}

							if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr.bottom_but.Acquire())) {
								bb_hdr->x_tra = result | f_pos;

								bg_hdr.bottom_but.Release();
							}

							// set some ids for bottom_but

							
						} else {
							tt_el = bg_hdr.top_but;
							tt_el.Left();

							for (unsigned int k(col_id);k<bg_hdr.row_count;k++) {
								tt_el.RelocateXY(but_offset);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
									bb_hdr->x_tra = f_pos;
									tt_el.Release();
								}

								tt_el.Hide();
								tt_el.Left();

								but_offset[1] += but_offset[5];
								b_counter++;
							}

							but_offset[0] += but_offset[6];
							but_offset[1] = but_offset[7];


							if ((bg_hdr.row_count > 1) && (col_id)) {
								for (unsigned int k(0);k<bg_hdr.row_count;k++) {
									tt_el.RelocateXY(but_offset);

									if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
										bb_hdr->x_tra = f_pos;
										tt_el.Release();
									}

									tt_el.Hide();
									tt_el.Left();
	
									but_offset[1] += but_offset[5];
									b_counter++;
								}
								but_offset[0] += but_offset[6];
								but_offset[1] = but_offset[7];
							}

							bg_hdr.top_but.ShiftLeft(tt_el.GetID());
					
							col_id = 0;
						}


					}
				}
				cf_type = f_type;
			}

			if (b_counter < bg_hdr.cell_count) {
				bg_hdr.top_but.RelocateXY(but_offset);

				bg_hdr.top_but.Show();
				bg_hdr.top_but.Left();

				if (h_type == 0) {
					but_offset[0] += but_offset[6];

					if (++col_id >= bg_hdr.column_count) {
						col_id = 0;

						but_offset[0] = but_offset[4];
						but_offset[1] += but_offset[5];
					}
				} else {
					but_offset[1] += but_offset[5];

					if (++col_id >= bg_hdr.row_count) {
						col_id = 0;

						but_offset[0] += but_offset[6];
						but_offset[1] = but_offset[7];
					}



				}
			}

			if (++b_counter >= bg_hdr.cell_count) break;
			if (++center_p >= count_er) break;
		}

		
		for (bg_hdr.current_page = b_counter;bg_hdr.current_page < bg_hdr.cell_count;bg_hdr.current_page++) {
			bg_hdr.top_but.Hide();
			bg_hdr.top_but.Left();

		}


		if (OpenGL::Element::Header * fl_hdr = reinterpret_cast<OpenGL::Element::Header *>(Acquire())) {
			OpenGL::ButtonGrid::Header * b_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(fl_hdr + 1);

			if (h_type == 0) {
				b_hdr->page_width = but_offset[6]*b_hdr->column_count;
				b_hdr->page_height = but_offset[1];
			} else {
				b_hdr->page_width = but_offset[0];
				b_hdr->page_height = but_offset[5]*b_hdr->row_count;
			}


			but_offset[0] = but_offset[2] = but_offset[3] = 0.0f;


			if (bg_hdr.current_page == b_counter) b_hdr->current_page = 0;
			else b_hdr->current_page = -1;

			if ((b_counter >= b_hdr->page_size) && (b_counter < count_er)) b_counter = count_er;
			
			if (b_hdr->page_size) b_hdr->max_page = (b_counter + b_hdr->page_size - 1)/b_hdr->page_size;
			else b_hdr->max_page = 1;

			if (b_hdr->max_page > 1) {				
				if (h_type == 0) {
					if (b_hdr->margin[3] < 0) but_offset[1] = 0.0f;
					else but_offset[1] = b_hdr->margin[1]; // but_offset[5]*0.5f;
					
				
					but_offset[8] = (b_hdr->max_page-2)*b_hdr->row_count + (((b_counter - (b_hdr->max_page-1)*b_hdr->page_size)*b_hdr->row_count)/b_hdr->page_size);

				} else { // horizontal
					if (b_hdr->margin[2] < 0) but_offset[0] = 0.0f;
					else but_offset[0] = b_hdr->margin[0]; // but_offset[5]*0.5f;

					but_offset[8] = (b_hdr->max_page-2)*b_hdr->column_count + (((b_counter - (b_hdr->max_page-1)*b_hdr->page_size)*b_hdr->column_count)/b_hdr->page_size);					

					but_offset[5] = but_offset[6];
				}

				if (b_hdr->ui_reserved[0]) but_offset[8] += 1.0f;
				else but_offset[8] += 8.0f;


				but_offset[8] *= but_offset[5];
			
				b_counter = 0;

			} else {
				if (b_hdr->margin[3] < 0.0f) but_offset[1] = (b_hdr->height - but_offset[1])*0.3f;
				else but_offset[1] = b_hdr->margin[1];

				b_counter = 1;
			}

			

			Release();
		}


	} else {
		for (b_counter=0;b_counter<bg_hdr.cell_count;b_counter++) {
			bg_hdr.top_but.Hide();
			bg_hdr.top_but.Left();
		}
	}


	scene.RelocateXY(but_offset);

	if (b_counter) {
	// vertical stop
		ResetDragOp((UI_64)(OpenGL::Element::FlagX0Drag | OpenGL::Element::FlagX1Drag | OpenGL::Element::FlagY0Drag | OpenGL::Element::FlagY1Drag | OpenGL::Element::FlagXYDrag));
	} else {
		// y span

		if (h_type) {
			ResetDragOp((UI_64)(OpenGL::Element::FlagY0Drag | OpenGL::Element::FlagY1Drag | OpenGL::Element::FlagXYDrag));
			SetXSpan(but_offset[8]);
		} else {
			ResetDragOp((UI_64)(OpenGL::Element::FlagX0Drag | OpenGL::Element::FlagX1Drag | OpenGL::Element::FlagXYDrag));
			SetYSpan(but_offset[8]);
		}
	}



	return result;
}

UI_64 OpenGL::ButtonGrid::NextPage(ButtonGrid & b_grid) {
	UI_64 result(0);

	int xo(0), yo(0);
	Element scene(b_grid);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(b_grid.Acquire())) {
				
		xo = reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->cell_width*reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->column_count;
		yo = reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->cell_height*reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->row_count;

		b_grid.Release();
	}

	scene.Down();
	scene.Drag(xo, yo);


	return result;
}

UI_64 OpenGL::ButtonGrid::PrevPage(ButtonGrid & b_grid) {
	UI_64 result(0);

	int xo(0), yo(0);
	Element scene(b_grid);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(b_grid.Acquire())) {
				
		xo = reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->cell_width*reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->column_count;
		yo = reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->cell_height*reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->row_count;

		b_grid.Release();
	}

	scene.Down();
	scene.Drag(-xo, -yo);


	return result;
}


UI_64 OpenGL::ButtonGrid::PageUp(ButtonGrid & b_grid) {
	UI_64 result(0);
	float o_vector[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	unsigned int cf_type(-1), f_type(-1), col_id(0), f_idx(0), b_counter(0);
	OpenGL::Core * ogl_core(0);
	OpenGL::Element tt_el;


	
	if (OpenGL::Element::Header * elhdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
		ogl_core = reinterpret_cast<OpenGL::Core *>(elhdr->_context);

		OpenGL::ButtonGrid::Header * bg_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(elhdr + 1);

		if (bg_hdr->current_page < bg_hdr->max_page)
		if (bg_hdr->current_page++ > 1) {
			if (bg_hdr->current_page < bg_hdr->max_page) {
				if (OpenGL::Element::Header * elhdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->bottom_but.Acquire())) {
					result = elhdr->x_tra >> 62;
					f_idx = elhdr->x_tra & 0x00FFFFFF;

					if ((elhdr->_flags & OpenGL::Element::FlagVisible) == 0) result |= 2;

					bg_hdr->bottom_but.Release();
				}

				o_vector[1] = bg_hdr->page_height;
				


				cf_type = bg_hdr->list_scan_up(f_idx, result);

		
				if (result & 1) { // hidden row
					for (;b_counter < bg_hdr->column_count;b_counter++) {
						bg_hdr->top_but.Move(o_vector);

						if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->top_but.Acquire())) {											
							bb_hdr->x_tra = f_idx;
							bg_hdr->top_but.Release();
						}

						bg_hdr->top_but.Hide();
						bg_hdr->top_but.Left();
					}							
				}
				
				for (;;) {

					f_type = bg_hdr->format_button(bg_hdr->top_but, bg_hdr->button_set, f_idx, bg_hdr->ui_reserved[0]);
					if (bg_hdr->ui_reserved[0]) cf_type = -1;
					
					if (f_idx == -1) break;
					if (!f_type) continue;
							

					if ((f_type != cf_type) && (cf_type != -1)) {
						if (((b_counter + bg_hdr->column_count + ((col_id>0)?bg_hdr->column_count:0) - col_id) >= bg_hdr->page_size)) {
							result = 0;
							if ((b_counter + bg_hdr->column_count + ((col_id>0)?bg_hdr->column_count:0) - col_id) > bg_hdr->page_size) result = 1;
							result <<= 62;
							
							for (;b_counter < bg_hdr->page_size;b_counter++) {
								bg_hdr->top_but.Move(o_vector);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->top_but.Acquire())) {											
									bb_hdr->x_tra = result | f_idx;

									bg_hdr->top_but.Release();
								}

								bg_hdr->top_but.Hide();
								bg_hdr->top_but.Left();

							}

						} else {
							tt_el = bg_hdr->top_but;
							tt_el.Left();
								
							for (unsigned int k(col_id);k<bg_hdr->column_count;k++) {
								tt_el.Move(o_vector);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
									bb_hdr->x_tra = f_idx;
									tt_el.Release();
								}

								tt_el.Hide();
								tt_el.Left();

								b_counter++;
							}
				
							if ((bg_hdr->column_count > 1) && (col_id)) {
								for (unsigned int k(0);k<bg_hdr->column_count;k++) {
									tt_el.Move(o_vector);

									if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
										bb_hdr->x_tra = f_idx;
										tt_el.Release();
									}

									tt_el.Hide();
									tt_el.Left();

									b_counter++;
								}
							}

							tt_el.Right();
							bg_hdr->top_but.ShiftRight(tt_el.GetID());										
																				
							col_id = 0;
						}

						cf_type = f_type;
					}
							
					if (b_counter < bg_hdr->page_size) {
						bg_hdr->top_but.Move(o_vector);
						if (tt_el) {
							bg_hdr->top_but.LocationSwap(tt_el);
							tt_el.Blank();
						}

						bg_hdr->top_but.Show();

						if (++col_id >= bg_hdr->column_count) col_id = 0;
					}
								
					if (b_counter < bg_hdr->page_size) bg_hdr->top_but.Left();

					if (++b_counter >= bg_hdr->page_size) break;
				}
				if (b_counter) {
					bg_hdr->bottom_but = bg_hdr->top_but;
					bg_hdr->bottom_but.Right();
				}

			}
		}
		
		b_grid.Release();

		if (b_counter) ogl_core->ResetElement(b_grid);		
	}

	return result;
}

UI_64 OpenGL::ButtonGrid::PageDown(ButtonGrid & b_grid) {
	UI_64 result(0);
	float o_vector[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	unsigned int cf_type(-1), f_type(-1), col_id(0), f_idx(0), p_idx(0), b_counter(0);
	OpenGL::Core * ogl_core(0);
	OpenGL::Element tt_el;

	if (OpenGL::Element::Header * elhdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
		ogl_core = reinterpret_cast<OpenGL::Core *>(elhdr->_context);

		OpenGL::ButtonGrid::Header * bg_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(elhdr + 1);

		if (bg_hdr->current_page > 0)
		if (bg_hdr->current_page-- < (bg_hdr->max_page-2)) {
			if (bg_hdr->current_page) {
				if (OpenGL::Element::Header * elhdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->top_but.Acquire())) {
					result = (elhdr->x_tra >> 62);
					p_idx = f_idx = (elhdr->x_tra & 0x00FFFFFF);
					if (elhdr->_flags & OpenGL::Element::FlagVisible) result |= 1;

					bg_hdr->top_but.Release();
				}

				o_vector[1] = -bg_hdr->page_height;

				cf_type &= bg_hdr->list_scan_down(p_idx, result);

				if (result & 2) { // hidden row
					for (;b_counter < bg_hdr->column_count;b_counter++) {
						bg_hdr->bottom_but.Move(o_vector);
						if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->bottom_but.Acquire())) {
							bb_hdr->x_tra = f_idx;
							bg_hdr->bottom_but.Release();
						}

						bg_hdr->bottom_but.Hide();
						bg_hdr->bottom_but.Right();
					}							
				}


				for (;;) {
					p_idx = f_idx;

					f_type = bg_hdr->format_button(bg_hdr->bottom_but, bg_hdr->button_set, f_idx, bg_hdr->ui_reserved[0] | 0x80000000);
					if (bg_hdr->ui_reserved[0]) cf_type = -1;

					if (f_idx == -1) break;
					if (!f_type) continue;
							

					if ((f_type != cf_type) && (cf_type != -1)) {
						if (((b_counter + bg_hdr->column_count + ((col_id>0)?bg_hdr->column_count:0) - col_id) >= bg_hdr->page_size)) {

							result = 0;
							if ((b_counter + bg_hdr->column_count + ((col_id>0)?bg_hdr->column_count:0) - col_id) > bg_hdr->page_size) result = 2;
							result <<= 62;		
							
							for (;b_counter < bg_hdr->page_size;b_counter++) {
								bg_hdr->bottom_but.Move(o_vector);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(bg_hdr->bottom_but.Acquire())) {
									bb_hdr->x_tra = result | p_idx;

									bg_hdr->bottom_but.Release();
								}

								bg_hdr->bottom_but.Hide();
								bg_hdr->bottom_but.Right();

							}

						} else {
							tt_el = bg_hdr->bottom_but;
							tt_el.Right();
									
							for (unsigned int k(col_id);k<bg_hdr->column_count;k++) {
								tt_el.Move(o_vector);

								if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
									bb_hdr->x_tra = p_idx;
									tt_el.Release();
								}

								tt_el.Hide();
								tt_el.Right();

								b_counter++;
							}
				
							if ((bg_hdr->column_count > 1) && (col_id)) {
								for (unsigned int k(0);k<bg_hdr->column_count;k++) {
									tt_el.Move(o_vector);

									if (OpenGL::Element::Header * bb_hdr = reinterpret_cast<OpenGL::Element::Header *>(tt_el.Acquire())) {
										bb_hdr->x_tra = p_idx;
										tt_el.Release();
									}

									tt_el.Hide();
									tt_el.Right();

									b_counter++;
								}
							}

							tt_el.Left();
							bg_hdr->bottom_but.ShiftLeft(tt_el.GetID());										
																				
							col_id = 0;
						}
						cf_type = f_type;
					}
																							

					if (b_counter < bg_hdr->page_size) {
						bg_hdr->bottom_but.Move(o_vector);
						if (tt_el) {
							bg_hdr->bottom_but.LocationSwap(tt_el);
							tt_el.Blank();
							}

						bg_hdr->bottom_but.Show();

						if (++col_id >= bg_hdr->column_count) col_id = 0;
					}
								
					if (b_counter < bg_hdr->page_size) bg_hdr->bottom_but.Right();

					if (++b_counter >= bg_hdr->page_size) break;
				}
					
				if (b_counter) {
					bg_hdr->top_but = bg_hdr->bottom_but;
					bg_hdr->top_but.Left();
				}
			}
		}
		
		b_grid.Release();

		if (b_counter) ogl_core->ResetElement(b_grid);
		
	}

	return result;
}


void OpenGL::ButtonGrid::SetButtonUpdate(void * f_ptr, void * li_up, void * li_down, unsigned int aid) {
	UI_64 _event(0);

	if (f_ptr) {
		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
			reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->ui_reserved[0] = aid;

			reinterpret_cast<void *&>(reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->format_button) = f_ptr;
			reinterpret_cast<void *&>(reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->list_scan_up) = li_up;
			reinterpret_cast<void *&>(reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->list_scan_down) = li_down;

			Release();
		}

		_event = SFSco::Event_CreateDrain(PageUp, this, 0);
		SFSco::Event_SetGate(_event, *this, OpenGL::Element::StatePageUp);

		_event = SFSco::Event_CreateDrain(PageDown, this, 0);
		SFSco::Event_SetGate(_event, *this, OpenGL::Element::StatePageDown);
	}
	
}

void OpenGL::ButtonGrid::SetMargin(float * m_box) {
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->margin[0] = m_box[0];
		reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->margin[1] = m_box[1];
		reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->margin[2] = m_box[2];
		reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->margin[3] = m_box[3];

		Release();
	}

}

void OpenGL::ButtonGrid::ConfigureCell(ActionType a_ptr, float w, float h, unsigned int bflags, float border_offset) {

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

		bg_hdr->action_ptr = a_ptr;

		bg_hdr->cell_width = w;
		bg_hdr->cell_height = h;


		if (border_offset >= 0.0) {
			bg_hdr->button_set.flags |= Button::Header::BorderPresent | (bflags & 0xF0000000);
			bg_hdr->button_set.border_offset = border_offset;


		}

		Release();
	}
}

void OpenGL::ButtonGrid::SetXIcon() {	
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
				
		bg_hdr->button_set.flags |= Button::Header::XIconPresent;
					
		Release();
	}
}

void OpenGL::ButtonGrid::ConfigureIconTrajectory(void (__fastcall * t_func)(float *, unsigned int), unsigned int option, AttributeArray::Type a_type) {
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

		bg_hdr->button_set.icon_trajectory[a_type] = t_func;
		bg_hdr->button_set.icon_trajectory_option[a_type] = option;

		Release();
	}
}

void OpenGL::ButtonGrid::ConfigureIcon(unsigned int xw, unsigned int yh, unsigned int i_flags, AnchorType atype, float b_offset) {
	UI_64 result(0);
	SFSco::Object c_buf;
	unsigned int w(xw>>16), h(yh>>16);
	
	if (c_buf.New(0, w*h*sizeof(unsigned int)) != -1) {
	
		if (unsigned int * cbuf_ptr = reinterpret_cast<unsigned int * >(c_buf.Acquire())) {
			System::MemoryFill_SSE3(cbuf_ptr, w*h*sizeof(unsigned int), -1, -1);

			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
				ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

				bg_hdr->button_set.icon_box[0] = xw & 0x0000FFFF;
				bg_hdr->button_set.icon_box[1] = yh & 0x0000FFFF;
				bg_hdr->button_set.icon_box[2] = w;
				bg_hdr->button_set.icon_box[3] = h;


				if (bg_hdr->icon_runner == 0) {
					ActiveTexture(GL_TEXTURE0+Core::icon_list2);
					glBindTexture(GL_TEXTURE_2D_ARRAY, bg_hdr->button_set.icon_tid);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

					TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, w, h, Core::max_texture_layers, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0); // Core::max_texture_layers
								
					TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, cbuf_ptr);

					bg_hdr->icon_runner = 1;
				}

				bg_hdr->button_set.icon_flags = i_flags;

				bg_hdr->button_set.icon_ancor = atype;

				if (i_flags & Button::Header::IconColorSpecial) bg_hdr->button_set.flags |= Button::Header::IconColorSpecial;


				bg_hdr->button_set.flags |= Button::Header::IconPresent;
				if (b_offset >= 0.0) {
					bg_hdr->button_set.flags |= Button::Header::IconBorderPresent;
					bg_hdr->button_set.ib_offset = b_offset;

				}

			
				Release();
			}
			c_buf.Release();
		}
		c_buf.Destroy();
	}
}

void OpenGL::ButtonGrid::AddPaletteEntry(Button::PaletteEntry & plte) {
	unsigned int x_val(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		x_val = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->button_set.tline_count;
		Release();
	}

	if (x_val == 0) {
		if (Expand(sizeof(Button::PaletteEntry)) != -1) {
			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
				ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
				Button::PaletteEntry * plt_ptr = reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1);

				System::MemoryCopy(plt_ptr + bg_hdr->button_set.palette_size++, &plte, sizeof(Button::PaletteEntry));

				Release();
			}
		}
	}
}

unsigned int OpenGL::ButtonGrid::ConfigureTextLine(const LabelCfg * la_cfg, unsigned int line_id) {
	unsigned int result(-1), la_cap(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

		la_cap = bg_hdr->button_set.palette_size;
		if (line_id < bg_hdr->button_set.tline_count) {
			result = line_id;

			System::MemoryCopy(reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + la_cap) + la_cap*result, la_cfg, la_cap*sizeof(LabelCfg));

		}
		Release();
	}
		
	if ((result == -1) && (la_cap)) {
		if (Expand(la_cap*sizeof(LabelCfg)) != -1) {				

			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
				ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

				if (line_id == -2) {
					bg_hdr->button_set.flags |= (Button::Header::IconTextPresent | Button::Header::IconPresent);
					bg_hdr->button_set.flags += 0x01000000;
				}

				result = bg_hdr->button_set.tline_count++;

				System::MemoryCopy(reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + la_cap) + la_cap*result, la_cfg, la_cap*sizeof(LabelCfg));

				Release();
			}
		}
	}


	return result;
}

unsigned int OpenGL::ButtonGrid::ConfigureTextLine(unsigned int color, unsigned int tcolor, unsigned int flags, float x, float y, float size, unsigned int line_id, const wchar_t * font_name) {
	unsigned int result(-1), f_id(-1);
	LabelCfg * t_cfg(0);

	if (reinterpret_cast<UI_64>(font_name) < 65536) {
		f_id = reinterpret_cast<UI_64>(font_name);
	} else {
		f_id = Font::Create(font_name);
	}

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		t_cfg = reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + bg_hdr->button_set.palette_size);

		if (line_id < bg_hdr->button_set.tline_count) {				
			result = line_id;

			t_cfg[result].offset[0] = x;
			t_cfg[result].offset[1] = y;
			t_cfg[result].offset[2] = 0.0;
			t_cfg[result].offset[3] = 0.0;

			Core::ColorConvert(t_cfg[result].c_color, color);
			Core::ColorConvert(t_cfg[result].t_color, tcolor);

			Core::ColorConvert(t_cfg[result].hc_color, color);
			Core::ColorConvert(t_cfg[result].ht_color, tcolor);
			Core::ColorConvert(t_cfg[result].dc_color, color);
			Core::ColorConvert(t_cfg[result].dt_color, tcolor);

			t_cfg[result].f_size = size;

			t_cfg[result].font_id = f_id;
			t_cfg[result].font_style = ((flags >> 8) & 0x00FF);
			t_cfg[result].a_type = (AnchorType)(flags & 0x00FF);
			t_cfg[result].max_char_count = (flags >> 16);

		}
		Release();
	}
		
	if (result == -1) {
		if (Expand(sizeof(LabelCfg)) != -1) {				

			if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
				ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
				t_cfg = reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + bg_hdr->button_set.palette_size);


				if (line_id == -2) {
					bg_hdr->button_set.flags |= (Button::Header::IconTextPresent | Button::Header::IconPresent);
					bg_hdr->button_set.flags += 0x01000000;
				}

				result = bg_hdr->button_set.tline_count++;

				t_cfg[result].offset[0] = x;
				t_cfg[result].offset[1] = y;
				t_cfg[result].offset[2] = 0.0;
				t_cfg[result].offset[3] = 0.0;

				Core::ColorConvert(t_cfg[result].c_color, color);
				Core::ColorConvert(t_cfg[result].t_color, tcolor);

				Core::ColorConvert(t_cfg[result].hc_color, color);
				Core::ColorConvert(t_cfg[result].ht_color, tcolor);
				Core::ColorConvert(t_cfg[result].dc_color, color);
				Core::ColorConvert(t_cfg[result].dt_color, tcolor);


				t_cfg[result].f_size = size;

				t_cfg[result].font_id = f_id;
				t_cfg[result].font_style = ((flags >> 8) & 0x00FF);
				t_cfg[result].a_type = (AnchorType)(flags & 0x00FF);
				t_cfg[result].max_char_count = (flags >> 16);
					

				Release();
			}
		}
	}


	return result;
}

void OpenGL::ButtonGrid::ConfigureCellTLHoover(unsigned int lidx, unsigned int color, unsigned int t_color) {
	LabelCfg * t_cfg(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		t_cfg = reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + bg_hdr->button_set.palette_size);

		if (lidx < bg_hdr->button_set.tline_count) {
			Core::ColorConvert(t_cfg[lidx].hc_color, color);
			Core::ColorConvert(t_cfg[lidx].ht_color, t_color);
		}

		Release();
	}
}

void OpenGL::ButtonGrid::ConfigureCellTLDown(unsigned int lidx, unsigned int color, unsigned int t_color) {
	LabelCfg * t_cfg(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		t_cfg = reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1) + bg_hdr->button_set.palette_size);

		if (lidx < bg_hdr->button_set.tline_count) {				
			Core::ColorConvert(t_cfg[lidx].dc_color, color);
			Core::ColorConvert(t_cfg[lidx].dt_color, t_color);
		}

		Release();
	}
}



UI_64 OpenGL::ButtonGrid::Start(OpenGL::Element & bobj) {	
	OpenGL::Core * core_ptr(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);
		Release();
	}

	if (core_ptr) {
		Show();

		core_ptr->ResetElement(*this);
		APPlane::ResetFunction(bobj);
	}

	return 0;
}

UI_64 OpenGL::ButtonGrid::Stop() {
	OpenGL::Core * core_ptr(0);
	Button blankbut;

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);
		Release();
	}

	if (core_ptr) {
		Hide();

		APPlane::ResetFunction(blankbut);
		core_ptr->ResetElement(*this);
	}

	return 0;
}


UI_64 OpenGL::ButtonGrid::Select(OpenGL::Element & bobj) {	
	OpenGL::Core * core_ptr(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ptr = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);
		Release();
	}

	if (core_ptr) {
		// pop it maybe
				
		APPlane::ResetFunction(bobj);
	}

	return 0;
}


UI_64 OpenGL::ButtonGrid::XPand(const float * bx_vals) {
	UI_64 result(0);
	float * elptr(0), * b_ptr(0);

	Element prnt(*this);
	AttributeArray elarr, butarr;
	



	InitAttributeArray(AttributeArray::Vertex, elarr);

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elarr.Acquire())) {
		elptr = reinterpret_cast<float *>(ahdr + 1);
			

		ahdr->translate_vector[0] += bx_vals[0];
		ahdr->translate_vector[1] += bx_vals[1];
		
		elptr[4] = elptr[12] = bx_vals[2];
		elptr[9] = elptr[13] = bx_vals[3];

	}

	elarr.Release();
	elarr.CalculateBox(LeftBottomFront);

	return XPand();
}

UI_64 OpenGL::ButtonGrid::XPand() {
	UI_64 result(0), _event(0);
	Core * core_ogl(0);

	LabelCfg line_cfg[16];	
	ButtonGrid::Header bg_hdro;
	Button::PaletteEntry palette;

	Button _button;
	Element chi_el(*this);
	void (__fastcall ButtonGrid::* b_hoo)(Button &) = &ButtonGrid::ButtonHoover;
	void (__fastcall ButtonGrid::* b_lea)(Button &) = &ButtonGrid::ButtonLeave;
	void (__fastcall ButtonGrid::* b_dwn)(Button &) = &ButtonGrid::ButtonDown;
	void (__fastcall ButtonGrid::* b_up)(Button &) = &ButtonGrid::ButtonUp;

	unsigned int but_top(0);
	float but_offset[4] = {0.0, 0.0, 0.0, 0.0}; 

	AttributeArray v_arr;

	InitAttributeArray(AttributeArray::Vertex, v_arr);

	System::MemoryFill(&line_cfg, 16*sizeof(LabelCfg), 0);
	

	if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
	
		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {

			ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);

			core_ogl = reinterpret_cast<Core *>(elhdr->_context);
			bg_hdr->width = ahdr->max[0] - ahdr->min[0];
			bg_hdr->column_count = ((bg_hdr->width - (bg_hdr->margin[0] + bg_hdr->margin[2]))/bg_hdr->cell_width);
			if (bg_hdr->column_count < 2) bg_hdr->column_count = 1;

			bg_hdr->height = (ahdr->max[1] - ahdr->min[1]);
			bg_hdr->row_count = ((bg_hdr->height - (bg_hdr->margin[1] + bg_hdr->margin[3]))/bg_hdr->cell_height);
			if (bg_hdr->row_count < 2) bg_hdr->row_count = 1;
			else bg_hdr->row_count = bg_hdr->row_count - (((unsigned int)(bg_hdr->height - bg_hdr->row_count*bg_hdr->cell_height)<<1) < bg_hdr->cell_height);

			bg_hdr->page_size = bg_hdr->column_count*bg_hdr->row_count;
			but_top = (bg_hdr->page_size<<2) + bg_hdr->page_size;

	// adjust root width, height
			but_offset[2] = bg_hdr->cell_width*bg_hdr->column_count;
			but_offset[3] = bg_hdr->cell_height*bg_hdr->row_count;

			System::MemoryCopy(&bg_hdro, bg_hdr, sizeof(ButtonGrid::Header));
			System::MemoryCopy(&palette, bg_hdr+1, sizeof(Button::PaletteEntry));

			if (bg_hdro.button_set.tline_count > 16) bg_hdro.button_set.tline_count = 16;

			for (unsigned int j(0);j<bg_hdro.button_set.tline_count;j++) {
				System::MemoryCopy(line_cfg + j, reinterpret_cast<LabelCfg *>(reinterpret_cast<Button::PaletteEntry *>(bg_hdr+1) + bg_hdr->button_set.palette_size) + j*bg_hdr->button_set.palette_size, sizeof(Element::LabelCfg));
			}

			Release();
		}

		v_arr.Release();
	}

	result = 0;



	if (core_ogl) {
		chi_el.Down();
		chi_el.ConfigurePage(but_offset);

		but_offset[0] = but_offset[1] = but_offset[2] = but_offset[3] = 0.0;


		for (;bg_hdro.cell_count < but_top;bg_hdro.cell_count++) {
			
			result |= core_ogl->CreateButton(_button, bg_hdro.cell_width, bg_hdro.cell_height, this, (bg_hdro.cell_count == 0)?0:0x80000000);
			
			if (result != -1) {				
				_button.Hide();

				if (bg_hdro.icon_runner != -1) palette.ref_vals[1] = bg_hdro.cell_count; //_button.Configure(bg_hdro.button_set, palette, line_cfg, bg_hdro.cell_count);
				
				_button.Configure(bg_hdro.button_set, palette, line_cfg);

				//set hoover
				_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_hoo), &_button, this);
				SFSco::Event_SetGate(_event, _button, Element::FlagHoover);
				// set leave
				_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_lea), &_button, this);
				SFSco::Event_SetGate(_event, _button, Element::FlagLeave);

				// set down
				_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_dwn), &_button, this);
				SFSco::Event_SetGate(_event, _button, Element::FlagMouseDown);
				
				// set mouse up
				_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_up), &_button, this);
				SFSco::Event_SetGate(_event, _button, Element::FlagMouseUp);
				_button.SetEFlags(Element::FlagAction);
				

			} else {
				break;
			}
		}
		
		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
			reinterpret_cast<ButtonGrid::Header *>(elhdr + 1)->cell_count = bg_hdro.cell_count;

			Release();
		}

		if (bg_hdro.size_action_ptr) bg_hdro.size_action_ptr(*this);

	}


	return result;
}

void OpenGL::ButtonGrid::ButtonUp(Button & b_obj) {
	UI_64 x_val(-1);
	ButtonGrid b_grid;
	Element x_el(b_obj);
	ActionType a_func(0);

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(x_el.Acquire())) {
		x_val = el_hdr->x_tra;

		x_el.Release();
	}
	
	x_el.Up().Up();
	b_grid(x_el); // scene, grid
	

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
		OpenGL::ButtonGrid::Header * bg_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1);

		a_func = bg_hdr->action_ptr;

		if (bg_hdr->last_but) {
			if (Element::Header * bel_hdr = reinterpret_cast<Element::Header *>(bg_hdr->last_but.Acquire())) {
				bel_hdr->xc_index = 0;
				bg_hdr->last_but.Release();
			}

			b_grid.ButtonLeave(reinterpret_cast<Button &>(bg_hdr->last_but));

			if (Element::Header * bel_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
				bel_hdr->xc_index = 1;
				b_obj.Release();
			}

			b_grid.ButtonHoover(b_obj);

			bg_hdr->last_but = b_obj;
		}

		b_grid.Release();
	}
	
	if (a_func) a_func(b_grid, x_val);	

}


void OpenGL::ButtonGrid::ButtonHoover(Button & b_obj) {
	Core * ogl_core(0);
	
	Element chip(b_obj);
	unsigned int lidx(0), ilc(0), plt_idx(0);
		

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ogl_core = reinterpret_cast<Core *>(elhdr->_context);

		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		Button::PaletteEntry * plt_ptr = reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1);
		Element::LabelCfg * l_cfg = reinterpret_cast<Element::LabelCfg *>(plt_ptr + bg_hdr->button_set.palette_size);
		
		if (Element::Header * but_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
			if (but_hdr->xc_index < bg_hdr->button_set.palette_size) plt_idx = but_hdr->xc_index;

			b_obj.Release();
		}		

		if (plt_ptr[plt_idx].color[3] > 0.0) {
			chip.SetColor(plt_ptr[plt_idx].h_color);
		}

		chip.Down();
		if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {
			chip.SetColor(plt_ptr[plt_idx].hb_color);
			chip.Left();
		}
	
		if (bg_hdr->button_set.flags & Button::Header::IconPresent) {
			if ((plt_ptr[plt_idx].i_color[3] > 0.0) || (bg_hdr->button_set.flags & Button::Header::IconColorSpecial)) chip.SetColor(plt_ptr[plt_idx].hi_color);
			
			chip.SetXColor(plt_ptr[plt_idx].ico_xcol[1]);

			if (bg_hdr->button_set.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (bg_hdr->button_set.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(plt_ptr[plt_idx].hib_color);
					ico_el.Left();
				}

				if (l_cfg)
				if (bg_hdr->button_set.flags & Button::Header::IconTextPresent) {
					ilc = ((bg_hdr->button_set.flags & 0x03000000) >> 24);
					for (lidx = 0; lidx<ilc; lidx++) {
						if (l_cfg[lidx].t_color[3] > 0.0) ico_el.SetColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].ht_color);
						reinterpret_cast<Text &>(ico_el).SetTextColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].hc_color);

						ico_el.Left();
					}
				}
					
			}

			chip.Left();
		}

		if (l_cfg)
		for (unsigned int i(lidx);i<bg_hdr->button_set.tline_count;i++) {
			if (l_cfg[i].t_color[3] > 0.0) chip.SetColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].ht_color);
			reinterpret_cast<Text &>(chip).SetTextColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].hc_color);

			chip.Left();
		}

		Release();
	}

	if (ogl_core) ogl_core->ResetElement(b_obj);
}

void OpenGL::ButtonGrid::ButtonLeave(Button & b_obj) {
	Core * ogl_core(0);
	
	Element chip(b_obj);
	unsigned int lidx(0), ilc(0), plt_idx(0);

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ogl_core = reinterpret_cast<Core *>(elhdr->_context);

		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		Button::PaletteEntry * plt_ptr = reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1);
		Element::LabelCfg * l_cfg = reinterpret_cast<Element::LabelCfg *>(plt_ptr + bg_hdr->button_set.palette_size);

		if (Element::Header * but_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
			if (but_hdr->xc_index < bg_hdr->button_set.palette_size) plt_idx = but_hdr->xc_index;

			b_obj.Release();
		}

		if (plt_ptr[plt_idx].color[3] > 0.0) {
			chip.SetColor(plt_ptr[plt_idx].color);
		}

		chip.Down();
		if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {
			chip.SetColor(plt_ptr[plt_idx].b_color);
			chip.Left();
		}

		if (bg_hdr->button_set.flags & Button::Header::IconPresent) {
			if (plt_ptr[plt_idx].i_color[3] > 0.0) chip.SetColor(plt_ptr[plt_idx].i_color);
			else {
				if (bg_hdr->button_set.flags & Button::Header::IconColorSpecial) {
					if (Element::Header * b_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
						chip.SetColor(&b_hdr->span_cfg.x);

						b_obj.Release();
					}
				}
			}
			
			chip.SetXColor(plt_ptr[plt_idx].ico_xcol[0]);

			if (bg_hdr->button_set.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (bg_hdr->button_set.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(plt_ptr[plt_idx].ib_color);
					ico_el.Left();
				}

				if (l_cfg)
				if (bg_hdr->button_set.flags & Button::Header::IconTextPresent) {
					ilc = ((bg_hdr->button_set.flags & 0x03000000) >> 24);
					for (lidx = 0; lidx<ilc; lidx++) {
						if (l_cfg[lidx].t_color[3] > 0.0) ico_el.SetColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].t_color);
						reinterpret_cast<Text &>(ico_el).SetTextColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].c_color);

						ico_el.Left();
					}
					
				}
			}

			chip.Left();
		}

		if (l_cfg)
		for (unsigned int i(lidx);i<bg_hdr->button_set.tline_count;i++) {
			if (l_cfg[i].t_color[3] > 0.0) chip.SetColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].t_color);
			reinterpret_cast<Text &>(chip).SetTextColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].c_color);

			chip.Left();
		}

		Release();
	}
		

	if (ogl_core) {
		b_obj.ClearEFlags(OpenGL::Element::FlagLeave); // 
		ogl_core->ResetElement(b_obj);
	}
}

void OpenGL::ButtonGrid::ButtonDown(Button & b_obj) {
	Core * ogl_core(0);
	
	Element chip(b_obj);
	unsigned int lidx(0), ilc(0), plt_idx(0);
		

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ogl_core = reinterpret_cast<Core *>(elhdr->_context);

		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(elhdr + 1);
		Button::PaletteEntry * plt_ptr = reinterpret_cast<Button::PaletteEntry *>(bg_hdr + 1);
		Element::LabelCfg * l_cfg = reinterpret_cast<Element::LabelCfg *>(plt_ptr + bg_hdr->button_set.palette_size);

		if (Element::Header * but_hdr = reinterpret_cast<Element::Header *>(b_obj.Acquire())) {
			if (but_hdr->xc_index < bg_hdr->button_set.palette_size) plt_idx = but_hdr->xc_index;

			b_obj.Release();
		}

		if (plt_ptr[plt_idx].color[3] > 0.0) {
			chip.SetColor(plt_ptr[plt_idx].d_color);
		}

		chip.Down();
		if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {
			chip.SetColor(plt_ptr[plt_idx].db_color);
			chip.Left();
		}
	
		if (bg_hdr->button_set.flags & Button::Header::IconPresent) {
			if ((plt_ptr[plt_idx].i_color[3] > 0.0) || (bg_hdr->button_set.flags & Button::Header::IconColorSpecial)) chip.SetColor(plt_ptr[plt_idx].di_color);
			
			chip.SetXColor(plt_ptr[plt_idx].ico_xcol[2]);

			if (bg_hdr->button_set.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (bg_hdr->button_set.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(plt_ptr[plt_idx].dib_color);
					ico_el.Left();
				}

				if (l_cfg)
				if (bg_hdr->button_set.flags & Button::Header::IconTextPresent) {
					ilc = ((bg_hdr->button_set.flags & 0x03000000) >> 24);
					for (lidx = 0; lidx<ilc; lidx++) {
						if (l_cfg[lidx].t_color[3] > 0.0) ico_el.SetColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].dt_color);
						reinterpret_cast<Text &>(ico_el).SetTextColor(l_cfg[lidx*bg_hdr->button_set.palette_size + plt_idx].dc_color);

						ico_el.Left();
					}
				}
					
			}

			chip.Left();
		}

		if (l_cfg)
		for (unsigned int i(lidx);i<bg_hdr->button_set.tline_count;i++) {
			if (l_cfg[i].t_color[3] > 0.0) chip.SetColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].dt_color);
			reinterpret_cast<Text &>(chip).SetTextColor(l_cfg[i*bg_hdr->button_set.palette_size + plt_idx].dc_color);

			chip.Left();
		}

		Release();
	}

	if (ogl_core) ogl_core->ResetElement(b_obj);
}

UI_64 OpenGL::Core::SetDecodePreview(DecodeRecord & de_rec, SFSco::Object & pic_obj, SFSco::Object & small_size, UI_64 pic_offset) {
	UI_64 result(0);
	OpenGL::Core * ogl_core(0);
	
	float f_vals[24];
	unsigned int tex_box[16], t_level(0);
	int i_val(0);

	OpenGL::Element telem, icoel;
	OpenGL::AttributeArray v_arr;
	
	wchar_t tmp_str[256];



	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(de_rec.element_obj.Acquire())) {
		ogl_core = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);

		de_rec.element_obj.Release();
	}

	if (ogl_core) {
		if (de_rec.pic_id != -1) {
			ogl_core->SetElement(icoel, de_rec.pic_id);
			ogl_core->StopMoving(icoel, 1);

			if (Element::Header * i_hdr = reinterpret_cast<Element::Header *>(icoel.Acquire())) {
				i_hdr->config.tex_unit_selector = i_hdr->x_tra;
				i_hdr->x_tra = 0;

				icoel.Release();
			}

			t_level = -1;
			tex_box[0] = de_rec.pic_box[2]; // target width
			tex_box[1] = de_rec.pic_box[3]; // target height

			f_vals[16] = f_vals[17] = f_vals[18] = f_vals[19] = f_vals[20] = f_vals[21] = f_vals[22] = f_vals[23] = 0.0f;

			if (small_size.Resize(tex_box[0]*tex_box[1]*6) != -1) {
				if (pic_obj) {
																
					tex_box[2] = de_rec.c_width;
					tex_box[3] = de_rec.c_height;
								
														
					if ((tex_box[2] <= tex_box[0]) && (tex_box[3] <= tex_box[1])) { // no resize
																													
						icoel.InitAttributeArray(OpenGL::AttributeArray::Vertex, v_arr);
						if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
							f_vals[16] = a_hdr->translate_vector[0] = de_rec.pic_box[0] + 0.5f*(de_rec.pic_box[2] - de_rec.width);
							f_vals[21] = f_vals[17] = a_hdr->translate_vector[1] = de_rec.pic_box[1] + 0.5f*(de_rec.pic_box[3] - de_rec.height);

							f_vals[16] += reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] = de_rec.width;
							f_vals[17] += reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(a_hdr+1)[9] = de_rec.height;


							a_hdr->reserved[3] |= 1;

							v_arr.Release();
						}

						icoel.InitAttributeArray(OpenGL::AttributeArray::TexCoord, v_arr);
						if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
							t_level = reinterpret_cast<float *>(a_hdr+1)[2];

							reinterpret_cast<float *>(a_hdr+1)[0] = 0.0;
							reinterpret_cast<float *>(a_hdr+1)[1] = 0.0f;

							reinterpret_cast<float *>(a_hdr+1)[4] = de_rec.width - 0.1f;
							reinterpret_cast<float *>(a_hdr+1)[5] = 0.0f;

							reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
							reinterpret_cast<float *>(a_hdr+1)[9] = de_rec.height - 0.1f;

							reinterpret_cast<float *>(a_hdr+1)[12] = de_rec.width - 0.1f;
							reinterpret_cast<float *>(a_hdr+1)[13] = de_rec.height - 0.1f;

							v_arr.Release();
						}

						if (unsigned int * spi_ptr = reinterpret_cast<unsigned int *>(small_size.Acquire())) {																			
							if (unsigned char * im_ptr = reinterpret_cast<unsigned char *>(pic_obj.Acquire())) {
								tex_box[2] <<= 2;								
								im_ptr += pic_offset;

								for (unsigned int li(0);li<tex_box[3];li++) {
									System::MemoryCopy_SSE3(spi_ptr, im_ptr, tex_box[2]);
									im_ptr += tex_box[2];
									spi_ptr += tex_box[0];
								}
								pic_obj.Release();
							}
					
							small_size.Release();
						}

					} else {
			// do image size
						f_vals[0] = de_rec.c_width; f_vals[1] = de_rec.c_height;
						f_vals[0] /= de_rec.pic_box[2]; f_vals[1] /= de_rec.pic_box[3];

						if (f_vals[0] > f_vals[1]) f_vals[1] = f_vals[0];
						else f_vals[0] = f_vals[1];


						tex_box[4] = de_rec.c_width;
						tex_box[5] = de_rec.c_height;

						tex_box[8] = tex_box[9] = 0;

						if (f_vals[0] > 1.5f) {
							tex_box[6] = f_vals[0];
							if ((f_vals[0] - tex_box[6])>0.0) tex_box[6]++;

							f_vals[0] = f_vals[1] = tex_box[6];

							tex_box[2] = de_rec.width; tex_box[3] = de_rec.height;
							tex_box[2] /= f_vals[0]; tex_box[3] /= f_vals[0];
							
							tex_box[7] = tex_box[6];

							if (unsigned int * spi_ptr = reinterpret_cast<unsigned int *>(small_size.Acquire())) {
								if (unsigned char * im_ptr = reinterpret_cast<unsigned char *>(pic_obj.Acquire())) {
									if (de_rec.precision & 0xF0000000) Average_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
									else Average_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

									pic_obj.Release();
		
								}
								small_size.Release();
							}

						} else {
							tex_box[2] = de_rec.width/1.5f;
							tex_box[3] = de_rec.height/1.5f;

							if (unsigned int * spi_ptr = reinterpret_cast<unsigned int *>(small_size.Acquire())) {
								if (unsigned char * im_ptr = reinterpret_cast<unsigned char *>(pic_obj.Acquire())) {
									if (de_rec.precision & 0xF0000000) Average3_2(spi_ptr, reinterpret_cast<UI_64 *>(im_ptr + pic_offset), tex_box);
									else Average3_1(spi_ptr, reinterpret_cast<unsigned int *>(im_ptr + pic_offset), tex_box);

									pic_obj.Release();
		
								}
								small_size.Release();
							}

							--tex_box[2] &= ~3;
							
						}
												
						
						icoel.InitAttributeArray(OpenGL::AttributeArray::Vertex, v_arr);
						if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
							a_hdr->translate_vector[0] = de_rec.pic_box[0] + 0.5f*(de_rec.pic_box[2] - tex_box[2]);
							f_vals[21] = f_vals[17] = a_hdr->translate_vector[1] = de_rec.pic_box[1] + 0.5f*(de_rec.pic_box[3] - tex_box[3]);
							
							f_vals[16] += reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] = tex_box[2];
							f_vals[17] += reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(a_hdr+1)[9] = tex_box[3];

							a_hdr->reserved[3] |= 1;
								
							v_arr.Release();
						}
					
						icoel.InitAttributeArray(OpenGL::AttributeArray::TexCoord, v_arr);
						if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
							t_level = reinterpret_cast<float *>(a_hdr+1)[2];

							reinterpret_cast<float *>(a_hdr+1)[0] = 0.0;
							reinterpret_cast<float *>(a_hdr+1)[1] = 0.0f;

							reinterpret_cast<float *>(a_hdr+1)[4] = tex_box[2] - 0.1f;
							reinterpret_cast<float *>(a_hdr+1)[5] = 0.0f;

							reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
							reinterpret_cast<float *>(a_hdr+1)[9] = tex_box[3] - 0.1f;

							reinterpret_cast<float *>(a_hdr+1)[12] = tex_box[2] - 0.1f;
							reinterpret_cast<float *>(a_hdr+1)[13] = tex_box[3] - 0.1f;
							
							v_arr.Release();
						}
					}			
					
					switch (de_rec._reserved[4]) {
						case -1:
							de_rec.target_time = Strings::FormatTimeVal(de_rec.target_time);

							i_val = 0;

							if (de_rec.target_time & 0x0FFFFFFF) {
								i_val += Strings::IntToCStr(reinterpret_cast<char *>(tmp_str), de_rec.target_time & 0x0FFFFFFF, 0);
								reinterpret_cast<char *>(tmp_str)[i_val++] = ':';
							}
							de_rec.target_time >>= 32;

							if ((i_val) || (de_rec.target_time & 0x00FF)) {
								i_val += Strings::IntToCStr(reinterpret_cast<char *>(tmp_str) + i_val, de_rec.target_time & 0x00FF, 2);
								reinterpret_cast<char *>(tmp_str)[i_val++] = ':';
							}
							de_rec.target_time >>= 8;

							if ((i_val) || (de_rec.target_time & 0x00FF)) {
								i_val += Strings::IntToCStr(reinterpret_cast<char *>(tmp_str) + i_val, de_rec.target_time & 0x00FF, 2);
								reinterpret_cast<char *>(tmp_str)[i_val++] = ':';

							}
							de_rec.target_time >>= 8;
							i_val += Strings::IntToCStr(reinterpret_cast<char *>(tmp_str) + i_val, de_rec.target_time & 0x00FF, 2);


						break;
						default:
							i_val = Strings::IntToCStr(reinterpret_cast<char *>(tmp_str), de_rec.width, 0);
							reinterpret_cast<char *>(tmp_str)[i_val++] = ' ';
							reinterpret_cast<char *>(tmp_str)[i_val++] = 'x';
							reinterpret_cast<char *>(tmp_str)[i_val++] = ' ';
							i_val += Strings::IntToCStr(reinterpret_cast<char *>(tmp_str)+i_val, de_rec.height, 0);
					}

				} else {
			// initiate texture with noise
					switch (pic_offset) {
						case 16:
							tex_box[2] = tex_box[0];
							tex_box[3] = (tex_box[1]<<2)/5;
						break;
						default:
							tex_box[2] = (tex_box[0]>>1);
							tex_box[3] = (tex_box[1]<<1)/3;
					}
																																		
					icoel.InitAttributeArray(OpenGL::AttributeArray::Vertex, v_arr);
					if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
						f_vals[16] = a_hdr->translate_vector[0] = de_rec.pic_box[0] + 0.5f*(tex_box[0] - tex_box[2]);
						f_vals[21] = f_vals[17] = a_hdr->translate_vector[1] = de_rec.pic_box[1] + 0.5f*(tex_box[1] - tex_box[3]);

						f_vals[16] += reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] = tex_box[2];
						f_vals[17] += reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(a_hdr+1)[9] = tex_box[3];

						a_hdr->reserved[3] |= 1;

						v_arr.Release();
					}

					icoel.InitAttributeArray(OpenGL::AttributeArray::TexCoord, v_arr);
					if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
						t_level = reinterpret_cast<float *>(a_hdr+1)[2];

						reinterpret_cast<float *>(a_hdr+1)[0] = 0.0f;
						reinterpret_cast<float *>(a_hdr+1)[5] = reinterpret_cast<float *>(a_hdr+1)[1] = tex_box[3];
																		
						reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] = tex_box[2];
																	

						reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
						reinterpret_cast<float *>(a_hdr+1)[9] = 0.0f;

						reinterpret_cast<float *>(a_hdr+1)[13] = 0.0f;

						v_arr.Release();
					}

					if (unsigned int *spi_ptr = reinterpret_cast<unsigned int *>(small_size.Acquire())) {
						
						switch (pic_offset) {
							case 16:
								for (unsigned int li(0);li<tex_box[3];li++) {
									for (unsigned int lj(0);lj<tex_box[2];lj++) {
										spi_ptr[lj] = 0xFF000000;
									}

									spi_ptr += tex_box[0];
								}


							break;
							default:
								for (unsigned int li(0);li<tex_box[3];li++) {
									for (unsigned int lj(0);lj<tex_box[2];lj++) {
										spi_ptr[lj] = 0xFF000000 | System::Gen32();
									}

									spi_ptr += tex_box[0];
								}
						}
																			
						small_size.Release();
					}


					reinterpret_cast<char *>(tmp_str)[0] = '?';
					reinterpret_cast<char *>(tmp_str)[1] = ' ';
					reinterpret_cast<char *>(tmp_str)[2] = 'x';
					reinterpret_cast<char *>(tmp_str)[3] = ' ';
					reinterpret_cast<char *>(tmp_str)[4] = '?';

					i_val = 5;

				}
			

				if (t_level != -1)
				if (unsigned int * spi_ptr = reinterpret_cast<unsigned int *>(small_size.Acquire())) {
					if (OpenGL::Element::Header * i_hdr = reinterpret_cast<OpenGL::Element::Header *>(icoel.Acquire())) {
						if (i_hdr->config.tex_unit_selector & 0xFFFFFFFF) {
							if ((i_hdr->config.tex_unit_selector >> 56) != de_rec._reserved[5]) {
								i_hdr->config.tex_unit_selector <<= 8;								
								i_hdr->config.tex_unit_selector >>= 8;

								i_hdr->config.tex_unit_selector |= (de_rec._reserved[5] << 56);
							}

							OpenGL::glBindTexture(GL_TEXTURE_2D_ARRAY, i_hdr->config.tex_unit_selector & 0xFFFFFFFF);
						
							OpenGL::TexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, t_level, tex_box[0], tex_box[1], 1, GL_RGBA, GL_UNSIGNED_BYTE, spi_ptr);
							OpenGL::glFlush();
						}

						icoel.Release();
					}
																			
					small_size.Release();
				}

				if (de_rec.name_text_id != -1) {
					ogl_core->SetElement(telem, de_rec.name_text_id);

					f_vals[20] = 25.0f;
					f_vals[21] -= 12.0f;
					telem.RelocateXY(f_vals+20);
				}
			
				if (de_rec.info_text_id != -1) {
					ogl_core->SetElement(telem, de_rec.info_text_id);

					Strings::A2W(tmp_str);
																	
					reinterpret_cast<OpenGL::Text &>(telem).Rewrite(tmp_str, 0, i_val);
					telem.GetRightMargin(f_vals + 20);
					f_vals[16] = de_rec.pic_box[2] - f_vals[22] - 25.0f;
					f_vals[17] += 20.0f;
					telem.RelocateXY(f_vals+16);

				}

				ogl_core->ResetElement(reinterpret_cast<OpenGL::Element &>(de_rec.element_obj));
			}
		}
	}

	return result;
}


UI_64 OpenGL::Button::StartPreview(unsigned int p_tid) {
	AttributeArray v_arr;
	ButtonGrid b_grid;
	Element b_el(*this);

	b_el.Down();

	b_grid(*this).Up().Up();


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(b_grid.Acquire())) {
		ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(el_hdr + 1);

		if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {			
			b_el.Left();
		}

		b_grid.Release();
	}

	if (OpenGL::Element::Header * i_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_el.Acquire())) {
		i_hdr->x_tra = i_hdr->config.tex_unit_selector;
		i_hdr->config.tex_unit_selector = p_tid; // config change

		b_el.Release();
	}

	b_el.InitAttributeArray(OpenGL::AttributeArray::Vertex, v_arr);
	if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
		a_hdr->translate_vector[0] = 64.0f;
		a_hdr->translate_vector[1] = 64.0f;

		reinterpret_cast<float *>(a_hdr+1)[12] = reinterpret_cast<float *>(a_hdr+1)[4] = 40.0f;
		reinterpret_cast<float *>(a_hdr+1)[13] = reinterpret_cast<float *>(a_hdr+1)[9] = 40.0f;

		a_hdr->reserved[3] |= 1;

		v_arr.Release();
	}

	b_el.InitAttributeArray(OpenGL::AttributeArray::TexCoord, v_arr);
	if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(v_arr.Acquire())) {
		reinterpret_cast<float *>(a_hdr+1)[0] = 0.0;
		reinterpret_cast<float *>(a_hdr+1)[1] = 0.0f;

		reinterpret_cast<float *>(a_hdr+1)[4] = 40.0f;
		reinterpret_cast<float *>(a_hdr+1)[5] = 0.0f;

		reinterpret_cast<float *>(a_hdr+1)[8] = 0.0f;
		reinterpret_cast<float *>(a_hdr+1)[9] = 40.0f;

		reinterpret_cast<float *>(a_hdr+1)[12] = 40.0f;
		reinterpret_cast<float *>(a_hdr+1)[13] = 40.0f;

		v_arr.Release();
	}

	if (Core * ogl_core = GetContext()) ogl_core->StartMoving(b_el);
	// start moving

	return 0;
}


