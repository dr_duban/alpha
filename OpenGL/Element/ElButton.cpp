/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "System\SysUtils.h"

#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "OpenGL\Core\OGL_15.h"
#include "OpenGL\Core\OGL_43.h"

#include "System\Control.h"

#pragma warning(disable:4244)

OpenGL::Button::Button() {
	SetTID(type_id);
}

OpenGL::Button::Button(UI_64 tid) : Element(tid) {
	
}


OpenGL::Button::~Button() {
	
}

UI_64 OpenGL::Core::CreateButton(Button & b_obj, float w, float h, const Element * parent, unsigned int dup) {
	float * aa_ptr(0);
	UI_64 result(0);
	AttributeArray v_arr;
	

	result = CreateElement(OpenGL::Element::Quad, b_obj, 4, parent, dup);
	

	if (result != -1) {
		b_obj.InitAttributeArray(AttributeArray::Vertex, v_arr);

		if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
			reinterpret_cast<float *>(v_hdr + 1)[4] = reinterpret_cast<float *>(v_hdr + 1)[12] = w;
			reinterpret_cast<float *>(v_hdr + 1)[9] = reinterpret_cast<float *>(v_hdr + 1)[13] = h;

			v_arr.Release();
		}

	}
	
	return result;
}



UI_64 OpenGL::Core::CreateButtonClip(Button & clip_obj, const Element * parent) {
	UI_64 result(0);
	
	result = CreateElement(OpenGL::Element::Quad, clip_obj, 4, parent, sizeof(Button::ClipHeader) + 32*sizeof(Button::ClipHeader::ClipSlot));
	

	if (result != -1) {


		if (Element::Header * e_hdr = reinterpret_cast<Element::Header *>(clip_obj.Acquire())) {
			Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(e_hdr + 1);

//			bc_hdr->slot_top = 32;
			bc_hdr->size = db_rect[2];
			bc_hdr->icon_texture = system_icos;

			clip_obj.Release();
		}

		clip_obj.SetDefaultRect();
	
	}


	return result;

}


UI_64 OpenGL::Button::ConfigureClip(AnchorType atype, FitType ftype, unsigned int ico_id, float size, float offset) {
	Element el_obj;
	AttributeArray elattr, pattr;
	float * elcoords(0);
	bool ctype(false);


	
	el_obj = *this;

	el_obj.Up();

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(el_obj.Acquire())) {
		
		ctype = ((elhdr->config.att_flags & 0xFFFF0000) == Element::SceneRegion);

		el_obj.Release();



		InitAttributeArray(AttributeArray::Vertex, elattr);
		

		if (ctype) {
			Element clip = el_obj;
			clip.Up();
			clip.InitAttributeArray(AttributeArray::Vertex, pattr);

		} else el_obj.InitAttributeArray(AttributeArray::Vertex, pattr);
				
		elattr.SetBox(pattr);

		el_obj.InitAttributeArray(AttributeArray::Vertex, pattr);

		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(elattr.Acquire())) {
			elcoords = reinterpret_cast<float *>(ahdr + 1);

			pattr.LocalDAdjust(elcoords, 4);
			
			ahdr->box[0] = elcoords[4] - elcoords[0];			
			ahdr->box[1] = elcoords[9] - elcoords[1];
			
			reinterpret_cast<unsigned int *>(ahdr->box)[0] &= 0x7FFFFFFF;
			reinterpret_cast<unsigned int *>(ahdr->box)[1] &= 0x7FFFFFFF;


			elcoords[0] = elcoords[1] = 0.0f;
			
			if (ftype == FitHeight) {
				elcoords[4] = size;
				elcoords[5] = 0.0f;

				elcoords[8] = 0.0f;
				elcoords[9] = ahdr->box[1] - 2.0f*offset;

				elcoords[12] = size;
				elcoords[13] = elcoords[9];

				if (atype & HANCHOR_LEFT) {
					ahdr->translate_vector[0] = offset;
					ahdr->translate_vector[1] = offset;

				} else { // right
					ahdr->translate_vector[0] = ahdr->box[0] - offset - size;
					ahdr->translate_vector[1] = offset;
				}


				elattr.SetStretchMask(0, -1, 0);

			} else {
				ftype = FitWidth;

				elcoords[4] = ahdr->box[0] - 2.0f*offset;
				elcoords[5] = 0.0f;

				elcoords[8] = 0.0f;
				elcoords[9] = size;

				elcoords[12] = elcoords[4];
				elcoords[13] = size;


				if ((atype & VANCHOR_TOP) == 0) {
					ahdr->translate_vector[0] = offset;
					ahdr->translate_vector[1] = offset;

				} else { // top
					ahdr->translate_vector[0] = offset;
					ahdr->translate_vector[1] = ahdr->box[1] - offset - size;
				}

				elattr.SetStretchMask(-1, 0, 0);
			}

			ahdr->reserved[3] |= 1;

			elcoords[2] = elcoords[6] = elcoords[10] = elcoords[14] = 1.0f;
			elattr.Release();
		}

		if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
			elhdr->config.anchor = atype | ftype;
			Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(elhdr + 1);

			

			if (ico_id) {
				bc_hdr->size = Core::db_rect[2];
				bc_hdr->icon_texture = reinterpret_cast<Core *>(elhdr->_context)->GetSystemTID();

			} else {
				bc_hdr->size = size;					

				glGenTextures(1, &bc_hdr->icon_texture);

				ActiveTexture(GL_TEXTURE0+Core::icon_list1);
				glBindTexture(GL_TEXTURE_2D_ARRAY, bc_hdr->icon_texture);
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				TexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, size, size, Core::max_texture_layers, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0); // viewport width & height
			}

			Release();
		}

		elattr.CalculateBox(atype);

	}

	return 0;
}

void OpenGL::Button::SetIconSpecial(unsigned int xcol) {
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		elhdr->_reserved |= Button::Header::IconColorSpecial;

		Core::ColorConvert(&elhdr->span_cfg.x, xcol);

		Release();
	}
}

UI_64 OpenGL::Button::ConfigureXIcon(UI_64 x_tid, float * box_vals) {
	Element ico_el(*this);
	unsigned int flag_val(0);
	AttributeArray a_arr;

	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		flag_val = elhdr->_reserved;

		Release();
	}
	
	
	ico_el.Down();

	if (flag_val & Header::BorderPresent) {		
		ico_el.Left();
	}

	if (flag_val & Header::XIconPresent) {
		if (Element::Header * i_hdr = reinterpret_cast<Element::Header *>(ico_el.Acquire())) {
			i_hdr->config.tex_unit_selector = x_tid;
			ico_el.Release();
		}

		ico_el.InitAttributeArray(AttributeArray::Vertex, a_arr);

		if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
			a_hdr->translate_vector[0] = box_vals[4];
			a_hdr->translate_vector[1] = box_vals[5];

			reinterpret_cast<float *>(a_hdr + 1)[4] = reinterpret_cast<float *>(a_hdr + 1)[12] = box_vals[6];
			reinterpret_cast<float *>(a_hdr + 1)[9] = reinterpret_cast<float *>(a_hdr + 1)[13] = box_vals[7];

			a_hdr->reserved[3] |= 1;

			a_arr.Release();
		}

		ico_el.InitAttributeArray(AttributeArray::TexCoord, a_arr);
		if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {

			reinterpret_cast<float *>(a_hdr + 1)[4] = reinterpret_cast<float *>(a_hdr + 1)[12] = box_vals[2];
			reinterpret_cast<float *>(a_hdr + 1)[9] = reinterpret_cast<float *>(a_hdr + 1)[13] = box_vals[3];

/*
			reinterpret_cast<float *>(a_hdr+1)[1] = box_vals[3];			

			reinterpret_cast<float *>(a_hdr+1)[4] = box_vals[2];
			reinterpret_cast<float *>(a_hdr+1)[5] = box_vals[3];			

			reinterpret_cast<float *>(a_hdr+1)[12] = box_vals[2];			
*/
			a_arr.Release();
		}
	}
	
	return 0;
}


UI_64 OpenGL::Button::Configure(Button::Header & but_cfg, Button::PaletteEntry & plte, Element::LabelCfg * line_cfg) {
	UI_64 result(0);
	float * v_ptr(0);
	Element icon_el;
	unsigned int line_id(0), ilc(0);
	Core * core_ogl;
	AttributeArray v_arr;

		
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		core_ogl = reinterpret_cast<Core *>(el_hdr->_context);
		el_hdr->_reserved = but_cfg.flags;		

		Release();
	

		if (plte.color[3] > 0.0) SetColor(plte.color);

		if (but_cfg.flags & Button::Header::BorderPresent) {
			CreateEBorder(but_cfg.border_offset, but_cfg.flags & 0xF0000000);
			SetEBorderColor(plte.b_color);
		}

		SetAnchorMode(LeftBottomFront);

		InitAttributeArray(AttributeArray::Vertex, v_arr);
		v_arr.CalculateBox(LeftBottomFront);

		if (but_cfg.flags & (Button::Header::IconPresent | Button::Header::XIconPresent)) {
			result |= core_ogl->CreateElement(Element::Quad, icon_el, 4, this);

			if (result != -1) {
				for (unsigned int a(0);a<AttributeArray::AttTypeCount;a++) {
					if (but_cfg.icon_trajectory[a]) icon_el.SetTrajectory(but_cfg.icon_trajectory[a], but_cfg.icon_trajectory_option[a], reinterpret_cast<AttributeArray::Type &>(a));
				}

				if ((but_cfg.flags & Button::Header::XIconPresent) == 0) {
					icon_el.InitAttributeArray(AttributeArray::Vertex, v_arr);
			
					if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
						v_ptr = reinterpret_cast<float *>(ahdr + 1);

						ahdr->translate_vector[0] = but_cfg.icon_box[0];
						ahdr->translate_vector[1] = but_cfg.icon_box[1];

						v_ptr[4] = v_ptr[12] = but_cfg.icon_box[2];
						v_ptr[9] = v_ptr[13] = but_cfg.icon_box[3];

						ahdr->reserved[3] |= 1;

						v_arr.Release();
					}

					icon_el.SetAnchorMode(but_cfg.icon_ancor);
					
					icon_el.SetXColor(plte.ico_xcol[0]);

					if ((plte.i_color[3] > 0.0) || (but_cfg.flags & Button::Header::IconColorSpecial)) icon_el.SetColor(plte.i_color);

					if (but_cfg.icon_tid) {
						if (Element::Header * ico_hdr = reinterpret_cast<Element::Header *>(icon_el.Acquire())) {
							ico_hdr->_r_d[1] = reinterpret_cast<UI_64 *>(plte.ref_vals)[0];
							icon_el.Release();
						}

						icon_el.SetRectTexture(but_cfg.icon_box, but_cfg.icon_tid, but_cfg.icon_flags | plte.ref_vals[1]);
					}

					if (but_cfg.flags & Button::Header::IconBorderPresent) {
						icon_el.CreateEBorder(but_cfg.ib_offset, 0x80000000);
						icon_el.SetEBorderColor(plte.ib_color);
					}


					if (but_cfg.flags & Button::Header::IconTextPresent) {
						ilc = ((but_cfg.flags & 0x03000000) >> 24);
						for (line_id;line_id<ilc;line_id++) icon_el.AddLabel(line_cfg[line_id]);
					}
				} else {
					icon_el.InitAttributeArray(AttributeArray::TexCoord, v_arr);
				}				
			}
		}

		for (;line_id < but_cfg.tline_count;) {
			AddLabel(line_cfg[line_id++]);
		}
	}


	return result;
}

unsigned int OpenGL::Button::LoadIcon(unsigned int rid, HMODULE libin) {
	unsigned int iidx(0);
	
	if (Element::Header * p_hdr = reinterpret_cast<Element::Header *>(Acquire())) {

		iidx = reinterpret_cast<Core *>(p_hdr->_context)->LoadIcon(Core::db_rect[2], Core::db_rect[3], rid, libin, -2);

		Release();
	}

	return iidx;
}

UI_64 OpenGL::Button::ConfigureClipButton(unsigned int flags, HMODULE libin) {
	Core * core_ptr(0);
	UI_64 result(0);
	unsigned int iidx(0);
	float * bvecs(0), * pvecs(0), i_size(1.0f);
	AttributeArray varray, parray;
	Button b_clip = *this;
	b_clip.Up();

	result = InitAttributeArray(AttributeArray::Vertex, varray);
	result |= b_clip.InitAttributeArray(AttributeArray::Vertex, parray);

	if (Element::Header * bhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		bhdr->_flags |= Element::FlagAction;
		core_ptr = reinterpret_cast<Core *>(bhdr->_context);

		if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(parray.Acquire())) {
			pvecs = reinterpret_cast<float *>(ahdr + 1);

			if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(varray.Acquire())) {
				bvecs = reinterpret_cast<float *>(v_hdr + 1);

				switch (flags & ClipAnchor) {
					case ClipCorner:
						switch (bhdr->_context->GetMenuCorner()) {
							case SFSco::IOpenGL::LeftTop:
								bhdr->config.anchor = LeftTopFront;
								varray.SetOffsetMask(LeftTopFront);
								
								v_hdr->translate_vector[0] = 0.0f;
								v_hdr->translate_vector[1] = pvecs[13] - bvecs[13];
					
							break;
							case SFSco::IOpenGL::RightTop:
								bhdr->config.anchor = RightTopFront;
								varray.SetOffsetMask(RightTopFront);
								
								v_hdr->translate_vector[0] = pvecs[12] - bvecs[12];
								v_hdr->translate_vector[1] = pvecs[13] - bvecs[13];

							break;
							case SFSco::IOpenGL::RightBottom:
								bhdr->config.anchor = RightBottomFront;
								varray.SetOffsetMask(RightBottomFront);
								
								v_hdr->translate_vector[0] = pvecs[12] - bvecs[12];
								v_hdr->translate_vector[1] = 0.0f;

							break;
							case SFSco::IOpenGL::LeftBottom:
								bhdr->config.anchor = LeftBottomFront;
								varray.SetOffsetMask(LeftBottomFront);
								
								v_hdr->translate_vector[0] = 0.0f;
								v_hdr->translate_vector[1] = 0.0f;

							break;

						}

					break;
					case ClipSide:
						switch (bhdr->_context->GetMenuCorner()) {
							case SFSco::IOpenGL::LeftTop:
								bhdr->config.anchor = RightBottomFront;
								varray.SetOffsetMask(RightBottomFront);
								
								v_hdr->translate_vector[0] = pvecs[12] - bvecs[12];
								v_hdr->translate_vector[1] = 0.0f;

							break;
							case SFSco::IOpenGL::RightTop:
								bhdr->config.anchor = LeftBottomFront;
								varray.SetOffsetMask(LeftBottomFront);
								
								v_hdr->translate_vector[0] = 0.0f;
								v_hdr->translate_vector[1] = 0.0f;

							break;
							case SFSco::IOpenGL::RightBottom:
								bhdr->config.anchor = LeftTopFront;
								varray.SetOffsetMask(LeftTopFront);
								
								v_hdr->translate_vector[0] = 0.0f;
								v_hdr->translate_vector[1] = pvecs[13] - bvecs[13];

							break;
							case SFSco::IOpenGL::LeftBottom:
								bhdr->config.anchor = RightTopFront;
								varray.SetOffsetMask(RightTopFront);
								
								v_hdr->translate_vector[0] = pvecs[12] - bvecs[12];
								v_hdr->translate_vector[1] = pvecs[13] - bvecs[13];

							break;

						}

					break;
					default:
						i_size = ((flags >> 16) & 0x00FF) + 1;

						switch (bhdr->_context->GetMenuCorner()) {
							case SFSco::IOpenGL::LeftTop:
								bhdr->config.anchor = LeftTopFront;
								varray.SetOffsetMask(LeftTopFront);
								
								if (pvecs[12] > pvecs[13]) {
									v_hdr->translate_vector[0] = (bvecs[12]+1.0f)*i_size;
									v_hdr->translate_vector[1] = 0.0f;
								} else {
									v_hdr->translate_vector[0] = 0.0f;
									v_hdr->translate_vector[1] = pvecs[13] - (bvecs[13]+1.0f)*i_size;
								}
					
							break;
							case SFSco::IOpenGL::RightTop:
								bhdr->config.anchor = RightTopFront;
								varray.SetOffsetMask(RightTopFront);
	
								if (pvecs[12] > pvecs[13]) {
									v_hdr->translate_vector[0] = pvecs[12] - (bvecs[12]+1.0f)*i_size;
									v_hdr->translate_vector[1] = 0.0f;
								} else {
									v_hdr->translate_vector[0] = 0.0f;
									v_hdr->translate_vector[1] = pvecs[13] - (bvecs[13]+1.0f)*i_size;
								}

							break;
							case SFSco::IOpenGL::RightBottom:
								bhdr->config.anchor = RightBottomFront;
								varray.SetOffsetMask(RightBottomFront);

								if (pvecs[12] > pvecs[13]) {
									v_hdr->translate_vector[0] = pvecs[12] - (bvecs[12]+1.0f)*i_size;
									v_hdr->translate_vector[1] = 0.0f;
								} else {
									v_hdr->translate_vector[0] = 0.0f;
									v_hdr->translate_vector[1] = (bvecs[13]+1.0f)*i_size;
								}

							break;
							case SFSco::IOpenGL::LeftBottom:
								bhdr->config.anchor = LeftBottomFront;
								varray.SetOffsetMask(LeftBottomFront);
								
								if (pvecs[12] > pvecs[13]) {
									v_hdr->translate_vector[0] = (bvecs[12]+1.0f)*i_size;
									v_hdr->translate_vector[1] = 0.0f;
								} else {
									v_hdr->translate_vector[0] = 0.0f;
									v_hdr->translate_vector[1] = (bvecs[13]+1.0f)*i_size;
								}

							break;

						}
									
				}

				v_hdr->reserved[3] |= 1;

				varray.Release();
			}
			parray.Release();
		}

		if ((libin) && (core_ptr)) {
			flags &= Button::Header::GroupMask;
			iidx = 0;

			if (Element::Header * p_hdr = reinterpret_cast<Element::Header *>(b_clip.Acquire())) {
				Button::ClipHeader * bc_hdr = reinterpret_cast<Button::ClipHeader *>(p_hdr+1);
				
				i_size = bc_hdr->size;
				result = bc_hdr->icon_texture;

				b_clip.Release();
			}

			if (GetCurrentThreadId() != GetWindowThreadProcessId(core_ptr->GetWinHandle(), 0)) {
				if (HANDLE local_block = CreateEvent(0, 0, 0, 0)) {
					unsigned int (Button::* loco)(unsigned int, HMODULE) = &Button::LoadIcon;
					void * ptr_list[4];
					ptr_list[0] = reinterpret_cast<void*&>(flags);
					ptr_list[1] = &b_clip;
					ptr_list[2] = reinterpret_cast<void*&>(loco);
					ptr_list[3] = libin;

					PostMessage(core_ptr->GetWinHandle(), INVOKE_IT, (WPARAM)ptr_list, (LPARAM)local_block);
					WaitForSingleObject(local_block, INFINITE);

					CloseHandle(local_block);

					iidx = reinterpret_cast<UI_64 *>(ptr_list)[3];
				}
			} else {
				iidx = b_clip.LoadIcon(flags, libin);
			}
		}

		if (iidx) {
			bhdr->_flags |= Element::FlagNegTex;

			bhdr->config.tex_unit_selector = LAYERED_TEXTURE;
			bhdr->config.tex_unit_selector <<= 32;
			bhdr->config.tex_unit_selector |= result;

		}

		Release();
	}


	if (iidx) {
		InitAttributeArray(AttributeArray::TexCoord, varray);

		if (AttributeArray::Header * a_hdr = reinterpret_cast<AttributeArray::Header *>(varray.Acquire())) {

			reinterpret_cast<float *>(a_hdr+1)[1] = i_size;
			reinterpret_cast<float *>(a_hdr+1)[2] = iidx;

			reinterpret_cast<float *>(a_hdr+1)[4] = i_size;
			reinterpret_cast<float *>(a_hdr+1)[5] = i_size;
			reinterpret_cast<float *>(a_hdr+1)[6] = iidx;

			reinterpret_cast<float *>(a_hdr+1)[10] = iidx;

			reinterpret_cast<float *>(a_hdr+1)[12] = i_size;
			reinterpret_cast<float *>(a_hdr+1)[14] = iidx;
			

			varray.Release();
		}
	}



	return result;
}



unsigned int OpenGL::Button::SetLabelText(unsigned int lid, const wchar_t * text, unsigned int t_len) {
	float location[4] = {0.0, 0.0, 0.0, 0.0};
	unsigned int result(0);
	unsigned int flag_val(0);
	Element but_el(*this), border_el;
	AttributeArray v_arr;

	
	if (Element::Header * elhdr = reinterpret_cast<Element::Header *>(Acquire())) {
		flag_val = elhdr->_reserved;

		Release();
	}
	
	
	but_el.Down();

	if (flag_val & Header::BorderPresent) {
		border_el = but_el;
		but_el.Left();
	}

	if (flag_val & Header::IconPresent) {
		if (flag_val & Header::IconTextPresent) {
			if (lid == 0) {
				Element icon_el = but_el;

				icon_el.Down();

				if (flag_val & Header::IconBorderPresent) {
					icon_el.Left();
				}
			}
			reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(text, 0, t_len);
			// icon label to set
	
			lid--;
		}
					
		but_el.Left();
	}

	if (lid != -1) {	
		for (unsigned int i(0);i<lid;lid++) but_el.Left();

		
		// set label
		result = reinterpret_cast<OpenGL::Text &>(but_el).OverWrite(text, t_len);


		if (flag_val & Header::AutoSize) { // configure button size
			but_el.GetRightMargin(location);

			location[0] *= 2.0f;
			location[0] += location[2];

			InitAttributeArray(AttributeArray::Vertex, v_arr);

			if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
				location[2] = location[0] - reinterpret_cast<float *>(ahdr+1)[4];
				reinterpret_cast<float *>(ahdr+1)[4] = reinterpret_cast<float *>(ahdr+1)[12] = location[0];
				ahdr->max[0] = location[0];
				v_arr.Release();
			}

			if (border_el) {
				border_el.InitAttributeArray(AttributeArray::Vertex, v_arr);
	
				v_arr.SetStretchVector(location[2], 0.0, 0.0);
				v_arr.DoAnchor();

				if (AttributeArray::Header * ahdr = reinterpret_cast<AttributeArray::Header *>(v_arr.Acquire())) {
					ahdr->max[0] += location[2];
					v_arr.Release();
				}
			}
		}
	}


	return result;
}




void OpenGL::Button::MHoover(Config ** b_cfg) {
	unsigned int lidx(0), p_idx(0);
	Element chip(*this);
	

	if (Core * ogl_core = GetContext()) {
		if (b_cfg[1][0].header.flags & Button::Header::TypeSwitch) {
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				p_idx = reinterpret_cast<SwitchConfig *>(el_hdr + 1)->selector;

				Release();
			}
		}



		if (b_cfg[1][p_idx].palette.color[3] > 0.0) {
			chip.SetColor(b_cfg[1][p_idx].palette.h_color);
		}

		chip.Down();
		if (b_cfg[1][p_idx].header.flags & Button::Header::BorderPresent) {
			chip.SetColor(b_cfg[1][p_idx].palette.hb_color);
			chip.Left();
		}
	
		if (b_cfg[1][p_idx].header.flags & Button::Header::IconPresent) {
			if ((b_cfg[1][p_idx].palette.i_color[3] > 0.0) || (b_cfg[1][p_idx].header.flags & Button::Header::IconColorSpecial)) chip.SetColor(b_cfg[1][p_idx].palette.hi_color);
			
			chip.SetXColor(b_cfg[1][p_idx].palette.ico_xcol[1]);

			if (b_cfg[1][p_idx].header.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(b_cfg[1][p_idx].palette.hib_color);
					ico_el.Left();
				}

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconTextPresent) {
					lidx = 1;
					if (b_cfg[1][p_idx].la_cfg[1].t_color[3] > 0.0) ico_el.SetColor(b_cfg[1][p_idx].la_cfg[1].ht_color);
					reinterpret_cast<Text &>(ico_el).SetTextColor(b_cfg[1][p_idx].la_cfg[1].hc_color);

					ico_el.Left();
				}
			}
		
			chip.Left();
		}		
	

		if (b_cfg[1][p_idx].header.tline_count > lidx) {
			if (b_cfg[1][p_idx].la_cfg[0].t_color[3] > 0.0) chip.SetColor(b_cfg[1][p_idx].la_cfg[0].ht_color);
			reinterpret_cast<Text &>(chip).SetTextColor(b_cfg[1][p_idx].la_cfg[0].hc_color);

			chip.Left();
		}

		ogl_core->ResetElement(*this);
	}
}

void OpenGL::Button::MLeave(Config ** b_cfg) {
	unsigned int lidx(0), p_idx(0);
	Element chip(*this);

	if (Core * ogl_core = GetContext()) {
		if (b_cfg[1][0].header.flags & Button::Header::TypeSwitch) {
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				p_idx = reinterpret_cast<SwitchConfig *>(el_hdr + 1)->selector;

				Release();
			}
		}


		if (b_cfg[1][p_idx].palette.color[3] > 0.0) {
			chip.SetColor(b_cfg[1][p_idx].palette.color);
		}

		chip.Down();
		if (b_cfg[1][p_idx].header.flags & Button::Header::BorderPresent) {
			chip.SetColor(b_cfg[1][p_idx].palette.b_color);
			chip.Left();
		}
	
		if (b_cfg[1][p_idx].header.flags & Button::Header::IconPresent) {
			if ((b_cfg[1][p_idx].palette.i_color[3] > 0.0) || (b_cfg[1][p_idx].header.flags & Button::Header::IconColorSpecial)) chip.SetColor(b_cfg[1][p_idx].palette.i_color);
			
			chip.SetXColor(b_cfg[1][p_idx].palette.ico_xcol[0]);

			if (b_cfg[1][p_idx].header.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(b_cfg[1][p_idx].palette.ib_color);
					ico_el.Left();
				}

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconTextPresent) {
					lidx = 1;
					if (b_cfg[1][p_idx].la_cfg[1].t_color[3] > 0.0) ico_el.SetColor(b_cfg[1][p_idx].la_cfg[1].t_color);
					reinterpret_cast<Text &>(ico_el).SetTextColor(b_cfg[1][p_idx].la_cfg[1].c_color);

					ico_el.Left();
				}
			}
		
			chip.Left();
		}

		
	

		if (b_cfg[1][p_idx].header.tline_count > lidx) {
			if (b_cfg[1][p_idx].la_cfg[0].t_color[3] > 0.0) chip.SetColor(b_cfg[1][p_idx].la_cfg[0].t_color);
			reinterpret_cast<Text &>(chip).SetTextColor(b_cfg[1][p_idx].la_cfg[0].c_color);

			chip.Left();
		}

		ClearEFlags(Element::FlagLeave);
		ogl_core->ResetElement(*this);
	}
}

void OpenGL::Button::MDown(Config ** b_cfg) {
	unsigned int lidx(0), p_idx(0);
	Element chip(*this);

	if (Core * ogl_core = GetContext()) {
		if (b_cfg[1][0].header.flags & Button::Header::TypeSwitch) {
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
				p_idx = reinterpret_cast<SwitchConfig *>(el_hdr + 1)->selector;

				Release();
			}
		}


		if (b_cfg[1][p_idx].palette.color[3] > 0.0) {
			chip.SetColor(b_cfg[1][p_idx].palette.d_color);
		}

		chip.Down();
		if (b_cfg[1][p_idx].header.flags & Button::Header::BorderPresent) {
			chip.SetColor(b_cfg[1][p_idx].palette.db_color);
			chip.Left();
		}
	
		if (b_cfg[1][p_idx].header.flags & Button::Header::IconPresent) {
			if ((b_cfg[1][p_idx].palette.i_color[3] > 0.0) || (b_cfg[1][p_idx].header.flags & Button::Header::IconColorSpecial)) chip.SetColor(b_cfg[1][p_idx].palette.di_color);
			
			chip.SetXColor(b_cfg[1][p_idx].palette.ico_xcol[2]);

			if (b_cfg[1][p_idx].header.flags & (Button::Header::IconBorderPresent | Button::Header::IconTextPresent)) {
				Element ico_el(chip);

				ico_el.Down();

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconBorderPresent) {
					ico_el.SetColor(b_cfg[1][p_idx].palette.dib_color);
					ico_el.Left();
				}

				if (b_cfg[1][p_idx].header.flags & Button::Header::IconTextPresent) {
					lidx = 1;
					if (b_cfg[1][p_idx].la_cfg[1].t_color[3] > 0.0) ico_el.SetColor(b_cfg[1][p_idx].la_cfg[1].dt_color);
					reinterpret_cast<Text &>(ico_el).SetTextColor(b_cfg[1][p_idx].la_cfg[1].dc_color);

					ico_el.Left();
				}
			}
		
			chip.Left();
		}
		
	

		if (b_cfg[1][p_idx].header.tline_count > lidx) {
			if (b_cfg[1][p_idx].la_cfg[0].t_color[3] > 0.0) chip.SetColor(b_cfg[1][p_idx].la_cfg[0].dt_color);
			reinterpret_cast<Text &>(chip).SetTextColor(b_cfg[1][p_idx].la_cfg[0].dc_color);

			chip.Left();
		}

		ogl_core->ResetElement(*this);
	}
}

void OpenGL::Button::MUp(Config ** b_cfg) {
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	Element chip(*this);
	chip.Down();

	GetRightCorner(some_vals);
	

	if (b_cfg[1][0].header.flags & Button::Header::TypeSwitch) {
		chip.Right();

		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
			SwitchConfig * sw_cfg = reinterpret_cast<SwitchConfig *>(el_hdr + 1);
			sw_cfg->selector ^= 1;

			reinterpret_cast<Text &>(chip).Rewrite(sw_cfg->text[sw_cfg->selector] + 1, 0, sw_cfg->text[sw_cfg->selector][0]);
			Release();
		}

		chip.GetRightMargin(some_vals + 4);
		some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
		some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);

		chip.RelocateXY(some_vals);
		chip.Left();
	} 


	if (TestEFlags((UI_64)Element::FlagDualUp) == 0) {		
		
		

		if (b_cfg[1][0].header.flags & Button::Header::BorderPresent) {
			chip.Left();
		}
	
		if (b_cfg[1][0].header.flags & Button::Header::IconPresent) {
			if (Element::Header * ico_hdr = reinterpret_cast<Element::Header *>(chip.Acquire())) {
				chip.RelocateTex(ico_hdr->_r_d[1] & 0x0FFFFFFFF);

				reinterpret_cast<Core *>(ico_hdr->_context)->SetBlink(chip, 10);

				chip.Release();
			}
		}

		SetState((UI_64)Element::FlagDualUp);
	} else {
		SetEFlags(Element::FlagBlinkNoMore); // clear dual up, stop blink

	}

	MHoover(b_cfg);
}

unsigned int OpenGL::Button::GetSwitchState() {
	unsigned int result(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		result = reinterpret_cast<Button::SwitchConfig *>(el_hdr + 1)->selector;
		Release();
	}
	return result;
}

void OpenGL::Button::SetSwitchLabels(unsigned int a_le, const wchar_t * l_a, unsigned int b_le, const wchar_t * l_b, unsigned int s_id) {
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

	Element chip(*this);
	
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SwitchConfig * sw_cfg = reinterpret_cast<SwitchConfig *>(el_hdr + 1);

		sw_cfg->selector = s_id;

		sw_cfg->text[0][0] = a_le;
		for (unsigned int i(0);i<a_le;i++) sw_cfg->text[0][i+1] = l_a[i];

		sw_cfg->text[1][0] = b_le;
		for (unsigned int i(0);i<b_le;i++) sw_cfg->text[1][i+1] = l_b[i];

		Release();
	}

	GetRightCorner(some_vals);
	chip.Down().Right();

	if (s_id) {
		l_a = l_b;
		a_le = b_le;
	}

	reinterpret_cast<Text &>(chip).Rewrite(l_a, 0, a_le);


	chip.GetRightMargin(some_vals + 4);

	some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
	some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);

	chip.RelocateXY(some_vals);
}


void OpenGL::Button::SetDefaultHandlers(SFSco::Object & c_obj) {
	UI_64 _event(0);
	
	void (Button::* h_ptr)(Button::Config **);

	h_ptr = &Button::MHoover;
	_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), &c_obj, this);
	SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagHoover);

	h_ptr = &Button::MDown;
	_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), &c_obj, this);
	SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseDown);

	h_ptr = &Button::MLeave;
	_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), &c_obj, this);
	SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagLeave);
	
	h_ptr = &Button::MUp;
	_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), &c_obj, this);
	SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseUp);


}



void OpenGL::Button::SetBlink(Button::Header & b_hdr, unsigned int delay_0, unsigned int delay_1, unsigned color_0, unsigned int color_1, unsigned int xcolor_01) {	
	Element chip(*this);
	
	chip.Down();

	if (b_hdr.flags & Button::Header::BorderPresent) {
		chip.Left();
	}
	
	if (b_hdr.flags & Button::Header::IconPresent) {
		chip.SetDefaultBlink(delay_0, delay_1, color_0, color_1, xcolor_01);
	}
}
