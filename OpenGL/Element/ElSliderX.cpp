/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "System\SysUtils.h"

#pragma warning(disable:4244)


UI_64 OpenGL::Core::CreateSlider(SliderX & s_el, unsigned int la_flags, const Element::LabelCfg * la_cfg, const Element * parent) { // unsigned int la_flags, , float wh
	float some_vals[8] = {0.0f, 0.0f, (la_flags & 0x00FF), ((la_flags >> 8) & 0x00FF),		0.0f, 0.0f, 1.0f, 1.0f};
	UI_64 result = CreateElement(Element::Quad, s_el, 4, parent, sizeof(SliderX::Header));
	Element s_elem;

	AttributeArray a_arr;
	
	la_flags &= SliderX::ValTypeMask;

	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(s_el.Acquire())) {
			reinterpret_cast<SliderX::Header *>(el_hdr + 1)->face_width = some_vals[2];
			reinterpret_cast<SliderX::Header *>(el_hdr + 1)->lz_val = some_vals[3];
			reinterpret_cast<SliderX::Header *>(el_hdr + 1)->flags = la_flags | SliderX::FixedControl | SliderX::LabelTypeL;
			reinterpret_cast<SliderX::Header *>(el_hdr + 1)->v_type = (la_flags & SliderX::ValTypeMask);
			reinterpret_cast<SliderX::Header *>(el_hdr + 1)->increment_val = 0.01;

			System::MemoryCopy(reinterpret_cast<SliderX::Header *>(el_hdr + 1)->label_cfg, la_cfg, sizeof(Element::LabelCfg));

			s_el.Release();
					
			s_elem = s_el;
			s_elem.SetRect(some_vals + 4);

			if (s_el.AddLabel(la_cfg[0]) != -1) {
				s_elem.Down();					

				some_vals[0] = 10.0f;
				some_vals[1] = la_cfg[0].f_size + 10.0f;
						
				s_elem.RelocateXY(some_vals);


				result = CreateElement(Element::Quad, s_elem, 4, &s_el); // progress bar
				if (result != -1) {
					s_elem.SetRect(some_vals + 4);
					s_elem.SetAnchorMode(LeftBottomFront);
					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

				}

				if (result != -1) {
					result = CreateElement(Element::Quad, s_elem, 4, &s_el); // progress rect
					if (result != -1) {
						s_elem.SetRect(some_vals + 4);
						s_elem.SetAnchorMode(LeftBottomFront);
						s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

						s_elem.CreateEBorder(0.0f, 0x80000000);

					}
				}


				if (result != -1) {
					result = CreateElement(Element::Quad, s_elem, 4, &s_el); // dec
					if (result != -1) { // set color & texture, configure events
						s_elem.SetAnchorMode(LeftBottomFront);							
						s_elem.SetEFlags(Element::FlagAction);

						some_vals[2] *= 0.6666666666666666666f;
						s_elem.SetRect(some_vals);

						s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
						some_vals[2] *= 1.5f;
						s_elem.SetRectTexture(some_vals, GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetDecTID());
					
					}


					result |= CreateElement(Element::Quad, s_elem, 4, &s_el); // increment
					if (result != -1) { // set color & texture, configure events				
						s_elem.SetAnchorMode(RightBottomFront);
						s_elem.SetEFlags(Element::FlagAction);

						some_vals[2] *= 0.6666666666666666666f;
						s_elem.SetRect(some_vals);

						s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

						some_vals[2] *= 1.5f;
						s_elem.SetRectTexture(some_vals, GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetIncTID());
				
					}
				}


				if (result != -1) { // icon screen
					result = CreateElement(Element::Quad, s_elem, 4, &s_el);

					if (result != -1) {
						s_elem.Hide();

						s_elem.SetAnchorMode(LeftBottomFront);
						s_elem.InitAttributeArray(AttributeArray::Vertex, a_arr);
						if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
							reinterpret_cast<float *>(v_hdr + 1)[4] = reinterpret_cast<float *>(v_hdr + 1)[12] = some_vals[2];
							reinterpret_cast<float *>(v_hdr + 1)[9] = reinterpret_cast<float *>(v_hdr + 1)[13] = some_vals[3];

							a_arr.Release();
						}

						a_arr.CalculateBox(LeftBottomFront);

						s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

						if (la_flags & SliderX::BorderOn) {
							s_elem.CreateEBorder(1.1f, 0x80000000);
						}
					}				
				}
			}
		}
	}

	return result;
}


UI_64 OpenGL::Core::CreateSXClip(SliderClip & sc_el, float w, float h, unsigned int flags, const Element * parent) {
	AttributeArray a_arr;
	UI_64 result = CreateElement(Element::Quad, sc_el, 4, parent, sizeof(SliderClip::Header));

	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(sc_el.Acquire())) {
			reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_width = w;
			reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_height = h;
			reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->_flags = flags;

			sc_el.Release();
		}

		sc_el.SetAnchorMode(LeftBottomFront);

		sc_el.InitAttributeArray(AttributeArray::Vertex, a_arr);
		if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
			reinterpret_cast<float *>(v_hdr + 1)[4] = reinterpret_cast<float *>(v_hdr + 1)[12] = 64.0f;
			reinterpret_cast<float *>(v_hdr + 1)[9] = reinterpret_cast<float *>(v_hdr + 1)[13] = 64.0f;

			a_arr.Release();
		}

		a_arr.CalculateBox(LeftBottomFront);
	}

	return result;

}

UI_64 OpenGL::SliderClip::CreateSX(SliderX & s_obj, unsigned int la_flags, const LabelCfg * la_cfg, float wh) {
	Core * ogl_ptr(0);
	float ico_box[16] = {0.0f, 0.0f, 0.0f, 0.0f,	0.0f, 0.0f, 1.0f, 1.0f,		0.0f, 0.0f, 1.0f, 1.0f,		0.0f, 0.0f, 0.0f, 0.0f};

	UI_64 result(-1);
	unsigned int sx_idx(0);
	
	Element s_elem;
	AttributeArray a_arr;


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		sx_idx = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_count++;
		

		if (wh == 0.0) ico_box[2] = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_width;
		else ico_box[2] = wh;

		ico_box[3] = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_height;
		ico_box[0] = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->c_offset;

		reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->c_offset += ico_box[2] + 6.0f;

		ico_box[10] = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_width;
		ico_box[11] = reinterpret_cast<SliderClip::Header *>(el_hdr + 1)->cell_height;

		Release();
	}

	if (ogl_ptr = GetContext()) {
		result = ogl_ptr->CreateElement(Element::Quad, s_obj, 4, this, sizeof(SliderX::Header));

		if (result != -1) {
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(s_obj.Acquire())) {
				el_hdr->_flags |= Element::FlagAction;

				reinterpret_cast<SliderX::Header *>(el_hdr + 1)->face_width = ico_box[2];
				reinterpret_cast<SliderX::Header *>(el_hdr + 1)->flags = la_flags;
				reinterpret_cast<SliderX::Header *>(el_hdr + 1)->v_type = (la_flags & SliderX::ValTypeMask);
				reinterpret_cast<SliderX::Header *>(el_hdr + 1)->increment_val = 0.01;

				System::MemoryCopy(reinterpret_cast<SliderX::Header *>(el_hdr + 1)->label_cfg, la_cfg, ((la_flags & SliderX::LabelTypeSS)?2:1)*sizeof(LabelCfg));

				s_obj.Release();
			}

			s_obj.SetAnchorMode(LeftBottomFront);
			s_obj.RelocateXY(ico_box);


			if (result != -1) {
				s_elem = s_obj;
				if (s_obj.AddLabel(la_cfg[0]) != -1) {
					s_elem.Down();					

					ico_box[0] = 10.0f;
					ico_box[1] = la_cfg[0].f_size + 10.0f;
						
					s_elem.RelocateXY(ico_box);


					if (la_flags & SliderX::LabelTypeSS) {
						if (s_obj.AddLabel(la_cfg[1]) != -1) {							

							ico_box[0] = 30.0f;
							ico_box[1] = la_cfg[0].f_size + la_cfg[1].f_size + 30.0f;
							s_elem.Left();
							s_elem.RelocateXY(ico_box);

						}
					} else {
						s_elem.Hide();
					}
				}
			}

			if (result != -1) {
				result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &s_obj); // progress bar
				if (result != -1) {
					s_elem.SetRect(ico_box + 4);
					s_elem.SetAnchorMode(LeftBottomFront);
					
					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

				}
			}

			if (result != -1) {
				result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &s_obj); // progress rect
				if (result != -1) {
					s_elem.SetRect(ico_box + 4);
					s_elem.SetAnchorMode(LeftBottomFront);
					
					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

					s_elem.CreateEBorder(0.0f, 0x80000000);

					s_elem.Hide();					
				}
			}


			if (result != -1) {
				result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &s_obj); // decrement
				if (result != -1) { // set color & texture, configure events
					s_elem.SetAnchorMode(LeftBottomFront);							
					s_elem.SetEFlags(Element::FlagAction);

					ico_box[10] *= 0.6666666666666666666f;
					s_elem.SetRect(ico_box + 8);

					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
					ico_box[10] *= 1.5f;
					s_elem.SetRectTexture(ico_box + 8, ogl_ptr->GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | ogl_ptr->GetDecTID());
				
					s_elem.Hide();
				}


				result |= ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &s_obj); // increment
				if (result != -1) { // set color & texture, configure events				
					s_elem.SetAnchorMode(LeftBottomFront);
					s_elem.SetEFlags(Element::FlagAction);

					ico_box[10] *= 0.6666666666666666666f;
					s_elem.SetRect(ico_box + 8);

					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

					ico_box[10] *= 1.5f;
					s_elem.SetRectTexture(ico_box + 8, ogl_ptr->GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | ogl_ptr->GetIncTID());
				

					s_elem.Hide();
				}
			}


			if (result != -1) { // icon screen
				result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &s_obj);

				if (result != -1) {
					s_elem.SetAnchorMode(LeftBottomFront);
					s_elem.InitAttributeArray(AttributeArray::Vertex, a_arr);
					if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
						reinterpret_cast<float *>(v_hdr + 1)[4] = reinterpret_cast<float *>(v_hdr + 1)[12] = ico_box[2];
						reinterpret_cast<float *>(v_hdr + 1)[9] = reinterpret_cast<float *>(v_hdr + 1)[13] = ico_box[3];

						a_arr.Release();
					}

					a_arr.CalculateBox(LeftBottomFront);

					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);

					if (la_flags & SliderX::BorderOn) {
						s_elem.CreateEBorder(1.1f, 0x80000000);
					}				
				}
			}
		}
	}


	return result;
}


UI_64 OpenGL::SliderClip::CoordsReset(float * bx_vals) {
	UI_64 x_val(0);
	float o_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f};
	unsigned sx_count(0);
		
	Element x_el(*this), s_elem;
	

	RelocateXY(bx_vals);		


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderClip::Header * sliclip_hdr = reinterpret_cast<SliderClip::Header *>(el_hdr + 1);

		bx_vals[0] = bx_vals[2] - sliclip_hdr->c_offset - 2.3333333333333f*sliclip_hdr->cell_width;
		bx_vals[1] = bx_vals[3] - sliclip_hdr->c_offset - 2.3333333333333f*sliclip_hdr->cell_height;

		sx_count = sliclip_hdr->cell_count;

		for (x_el.Down();sx_count--;x_el.Left()) {
			x_el.RelocateXY(o_vals + 8);
			s_elem = x_el; s_elem.Down();


			if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(x_el.Acquire())) {
				SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(s_hdr + 1);
				if (sli_hdr->flags & SliderX::ControlOn) {
					sli_hdr->flags ^= SliderX::ControlOn;
				}

				if (sli_hdr->flags & SliderX::LabelTypeSS) {
					o_vals[0] = 10.0f;
					o_vals[1] = sli_hdr->label_cfg[0].f_size + 10.0f;;
					s_elem.RelocateXY(o_vals);
							
					s_elem.Left(); // label zwei
					s_elem.GetRightMargin(o_vals + 4);

					o_vals[0] = sli_hdr->face_width - o_vals[6] - 10.0f;
					o_vals[1] = o_vals[5];

					s_elem.RelocateXY(o_vals);

				} else {
					s_elem.Hide();
				}

				s_elem.Left(); // progressbar
				s_elem.SetColor(sli_hdr->palette_cfg[0].i_color);

				s_elem.RelocateXY(o_vals + 12);
						
				o_vals[0] = sli_hdr->face_width*sli_hdr->value[3].d;
				o_vals[1] = sliclip_hdr->cell_height;
				o_vals[2] = 1.0f;
				o_vals[3] = 1.0f;
				s_elem.Rescale(o_vals);


				s_elem.Left(); // progress rect
				s_elem.Hide();

				s_elem.Left(); // dec
				s_elem.Hide();

				s_elem.Left(); // inc
				s_elem.Hide();

				s_elem.Left();
				s_elem.Show();
					
				s_elem.SetColor(sli_hdr->palette_cfg[0].color);
				s_elem.SetXColor(sli_hdr->palette_cfg[0].ico_xcol[0]);
			
					
				o_vals[8] += sli_hdr->face_width + 6.0f;


				sli_hdr->progress_width = bx_vals[0];
				sli_hdr->progress_width += sli_hdr->face_width;

				x_el.Release();
			}
		}

		Release();
	}

	return 0;
}


OpenGL::SliderClip::SliderClip() {
	SetTID(type_id);
}


OpenGL::SliderClip::~SliderClip() {


}


// ===================================================================================================================================================

OpenGL::SliderX::SliderX() {
	SetTID(type_id);
}



OpenGL::SliderX::~SliderX() {
	
}

UI_64 OpenGL::SliderX::CoordsReset() { // reset labels, and progress bar if ness
	float mu_val[16] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	Element s_elem(*this);
	

	s_elem.Down().Right(); // icon el
	s_elem.GetRightMargin(mu_val + 4);

	
	s_elem.Left();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);

				
		if (sli_hdr->flags & SliderX::LabelTypeSS) {
			// double label

			if (sli_hdr->flags & (SliderX::ControlOn | SliderX::FixedControl)) { // different alignment
				s_elem.Left(); // label zwei
				s_elem.GetRightMargin(mu_val + 12);

				mu_val[0] = sli_hdr->lz_val - mu_val[14];
				mu_val[1] = mu_val[13];

				s_elem.RelocateXY(mu_val);

				s_elem.Left();
				

				mu_val[0] = sli_hdr->progress_width*sli_hdr->value[3].d;
				mu_val[1] = 10.0f;
				mu_val[2] = 1.0f;
				mu_val[3] = 1.0f;
				s_elem.Rescale(mu_val);


			} else {
				s_elem.Left(); // label zwei
				s_elem.GetRightMargin(mu_val + 12);

				mu_val[0] = mu_val[6] - mu_val[14] - 10.0f;
				mu_val[1] = mu_val[13];

				s_elem.RelocateXY(mu_val);

				s_elem.Left();

				mu_val[0] = mu_val[6]*sli_hdr->value[3].d;
				mu_val[1] = mu_val[7];
				mu_val[2] = 1.0f;
				mu_val[3] = 1.0f;
				s_elem.Rescale(mu_val);


			}

		} else {
			// single label center it
			if (sli_hdr->flags & (SliderX::ControlOn | SliderX::FixedControl)) {
				
				s_elem.GetRightMargin(mu_val + 12);

				mu_val[0] = sli_hdr->lz_val + 0.5f*(sli_hdr->progress_width - mu_val[14]);
				mu_val[1] = mu_val[13];

				s_elem.RelocateXY(mu_val);


				s_elem.Left();				

				mu_val[0] = sli_hdr->progress_width*sli_hdr->value[3].d;
				mu_val[1] = 10.0f;
				mu_val[2] = 1.0f;
				mu_val[3] = 1.0f;
				s_elem.Rescale(mu_val);



			} else {
				

			}
		}


		Release();
	}

	return 0;
}

void OpenGL::SliderX::Relocate(const float * bx_vals) {
	UI_64 x_val(0);
	float o_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f};
	unsigned sx_count(0);
		
	Element x_el(*this);
	x_el.Down();

	RelocateXY(bx_vals);		



	if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(s_hdr + 1);

		sli_hdr->progress_width = bx_vals[2] - 2.0f*sli_hdr->face_width;

		if (sli_hdr->flags & SliderX::FixedControl) {
	/*		
			sli_hdr->value[3].d = bx_vals[3];

			ValueAdjust();
			SetLabels();
	*/		

			x_el.GetRightMargin(o_vals + 4);
			o_vals[4] = 0.5f*(sli_hdr->progress_width - o_vals[6]) + sli_hdr->face_width;			
			x_el.RelocateXY(o_vals + 4);

			o_vals[4] = sli_hdr->face_width;
			o_vals[5] = sli_hdr->label_cfg[0].f_size + 16.0f;


			x_el.Left(); // progressbar
			x_el.RelocateXY(o_vals + 4);
						
			o_vals[0] = sli_hdr->progress_width*sli_hdr->value[3].d;
			o_vals[1] = 10.0f;
			o_vals[2] = 1.0f;
			o_vals[3] = 1.0f;
			x_el.Rescale(o_vals);


			x_el.Left(); // progress rect
			x_el.RelocateXY(o_vals + 4);

			o_vals[0] = sli_hdr->progress_width;
			o_vals[1] = 10.0f;

			x_el.Rescale(o_vals);


			x_el.Left(); // dec
		

			x_el.Left(); // inc
			o_vals[4] = sli_hdr->progress_width + 1.3333333333333333f*sli_hdr->face_width;		
			o_vals[5] = 0.0f;
			x_el.RelocateXY(o_vals + 4);
		}

		Release();
	}
}


void OpenGL::SliderX::SetValue(UI_64 val) {
	UI_64 u_vals[2] = {0, 0};
	Core * core_ptr(0);
	SliderX::Header * sli_hdr(0);

	if (IsVisible()) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

			sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr+1);				

			if (sli_hdr->vals_list == 0) {
				switch (sli_hdr->v_type & SliderX::ValTypeMask) {
					case SliderX::ValTypeUInt: case SliderX::ValTypeTime:
						if (val < sli_hdr->value[0].ui) val = sli_hdr->value[0].ui;
						if (val > sli_hdr->value[1].ui) val = sli_hdr->value[1].ui;

						sli_hdr->value[3].d = val;
						sli_hdr->value[3].d -= sli_hdr->value[0].ui;
						sli_hdr->value[3].d /= (sli_hdr->value[1].ui - sli_hdr->value[0].ui);

					break;
					case SliderX::ValTypeInt:
						if (reinterpret_cast<__int64 &>(val) < sli_hdr->value[0].i) val = sli_hdr->value[0].ui;
						if (reinterpret_cast<__int64 &>(val) > sli_hdr->value[1].i) val = sli_hdr->value[1].ui;

						sli_hdr->value[3].d = reinterpret_cast<__int64 &>(val);
						sli_hdr->value[3].d -= sli_hdr->value[0].i;
						sli_hdr->value[3].d /= (sli_hdr->value[1].i - sli_hdr->value[0].i);

					break;
					case SliderX::ValTypeDouble: case SliderX::ValTypePcnt:
						if (reinterpret_cast<double &>(val) < sli_hdr->value[0].d) val = sli_hdr->value[0].ui;
						if (reinterpret_cast<double &>(val) > sli_hdr->value[1].d) val = sli_hdr->value[1].ui;
				
						sli_hdr->value[3].d = reinterpret_cast<double &>(val);
						sli_hdr->value[3].d -= sli_hdr->value[0].d;
						sli_hdr->value[3].d /= (sli_hdr->value[1].d - sli_hdr->value[0].d);

					break;
				}
			} else {
				switch (sli_hdr->v_type & SliderX::ValTypeMask) {
					case SliderX::ValTypeUInt: case SliderX::ValTypeTime:
						u_vals[0] = -1;
						sli_hdr->value[3].ui = val;
						val = 0;

						for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
							u_vals[1] = sli_hdr->vals_list[ii + 1] - sli_hdr->value[3].ui;
						
							if (u_vals[1] < u_vals[0]) {
								u_vals[0] = u_vals[1];
								val = ii;
							}
						}

					break;
					case SliderX::ValTypeInt:
						u_vals[0] = 0x7FFFFFFFFFFFFFFF;
						sli_hdr->value[3].i = val;
						val = 0;

						for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
							reinterpret_cast<int *>(u_vals)[1] = reinterpret_cast<const int *>(sli_hdr->vals_list)[ii + 1] - sli_hdr->value[3].i;
						
							if (reinterpret_cast<int *>(u_vals)[1] < 0) reinterpret_cast<int *>(u_vals)[1] = -reinterpret_cast<int *>(u_vals)[1];
							if (reinterpret_cast<int *>(u_vals)[1] < reinterpret_cast<int *>(u_vals)[0]) {
								u_vals[0] = u_vals[1];
								val = ii;
							}
						}

					break;
					case SliderX::ValTypeDouble: case SliderX::ValTypePcnt:					
						u_vals[0] = 0x7FF0000000000000;
						sli_hdr->value[3].ui = val;
						val = 0;

						for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
							reinterpret_cast<double *>(u_vals)[1] = reinterpret_cast<const double *>(sli_hdr->vals_list)[ii + 1] - sli_hdr->value[3].d;
							u_vals[1] &= 0x7FFFFFFFFFFFFFFF;
							if (reinterpret_cast<double *>(u_vals)[1] < reinterpret_cast<double *>(u_vals)[0]) {
								u_vals[0] = u_vals[1];
								val = ii;
							}
						}
										
					break;
				}

				sli_hdr->value[3].d = 0.1 + val - sli_hdr->value[0].ui;
				sli_hdr->value[3].d /= (sli_hdr->value[1].ui - sli_hdr->value[0].ui);

			}

			if (val != sli_hdr->value[2].ui) sli_hdr->value[2].ui = val;
			else val = -1;

			Release();
		

			if (val != -1) {
				SetLabels();
	
				CoordsReset();

				if (core_ptr) core_ptr->ResetElement(*this);
			}
		}
	}
}

void OpenGL::SliderX::SetLabelWidth(unsigned int wv, unsigned int ff) {
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		reinterpret_cast<SliderX::Header *>(el_hdr+1)->v_type &= SliderX::ValTypeMask;
		reinterpret_cast<SliderX::Header *>(el_hdr+1)->v_type |= (wv & 0x001F) | ff;
		Release();
	}
}

void OpenGL::SliderX::SetRange(const UI_64 * li_ptr) {
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		reinterpret_cast<SliderX::Header *>(el_hdr+1)->vals_list = li_ptr;

		Release();
	}
}

void OpenGL::SliderX::SetRange(UI_64 min, UI_64 max, UI_64 val) {
	SliderX::Header * sli_hdr(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr+1);

		sli_hdr->value[0].ui = min;
		sli_hdr->value[1].ui = max;
		sli_hdr->value[2].ui = min;
		sli_hdr->value[3].ui = 0;

		if (sli_hdr->vals_list == 0) {
			switch (sli_hdr->v_type & SliderX::ValTypeMask) {
				case SliderX::ValTypeUInt: case SliderX::ValTypeTime:
					if (val < sli_hdr->value[0].ui) val = sli_hdr->value[0].ui;
					if (val > sli_hdr->value[1].ui) val = sli_hdr->value[1].ui;

					sli_hdr->value[3].d = val;
					sli_hdr->value[3].d -= sli_hdr->value[0].ui;
					sli_hdr->value[3].d /= (sli_hdr->value[1].ui - sli_hdr->value[0].ui);

				break;
				case SliderX::ValTypeInt:
					if (reinterpret_cast<__int64 &>(val) < sli_hdr->value[0].i) val = sli_hdr->value[0].ui;
					if (reinterpret_cast<__int64 &>(val) > sli_hdr->value[1].i) val = sli_hdr->value[1].ui;
				
					sli_hdr->value[3].d = reinterpret_cast<__int64 &>(val);
					sli_hdr->value[3].d -= sli_hdr->value[0].i;
					sli_hdr->value[3].d /= (sli_hdr->value[1].i - sli_hdr->value[0].i);

				break;
				case SliderX::ValTypeDouble: case SliderX::ValTypePcnt:
					if (reinterpret_cast<double &>(val) < sli_hdr->value[0].d) val = sli_hdr->value[0].ui;
					if (reinterpret_cast<double &>(val) > sli_hdr->value[1].d) val = sli_hdr->value[1].ui;
				
					sli_hdr->value[3].d = reinterpret_cast<double &>(val);
					sli_hdr->value[3].d -= sli_hdr->value[0].d;
					sli_hdr->value[3].d /= (sli_hdr->value[1].d - sli_hdr->value[0].d);
				break;
			}
		} else {
			if ((max > sli_hdr->vals_list[0]) || (max == 0)) sli_hdr->value[1].ui = sli_hdr->vals_list[0];

			if (min > max) {
				sli_hdr->value[0].ui = 0;
				sli_hdr->value[2].ui = 0;
			}
			
			switch (sli_hdr->v_type & SliderX::ValTypeMask) {
				case SliderX::ValTypeUInt: case SliderX::ValTypeTime:
					min = -1;
					sli_hdr->value[3].ui = val;
					val = 0;

					for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
						max = sli_hdr->vals_list[ii + 1] - sli_hdr->value[3].ui;
						
						if (max < min) {
							min = max;
							val = ii;
						}
					}

				break;
				case SliderX::ValTypeInt:
					min = 0x7FFFFFFFFFFFFFFF;
					sli_hdr->value[3].i = val;
					val = 0;

					for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
						reinterpret_cast<int &>(max) = reinterpret_cast<const int *>(sli_hdr->vals_list)[ii + 1] - sli_hdr->value[3].i;
						
						if (reinterpret_cast<int &>(max) < 0) reinterpret_cast<int &>(max) = -reinterpret_cast<int &>(max);
						if (reinterpret_cast<int &>(max) < reinterpret_cast<int &>(min)) {
							min = max;
							val = ii;
						}
					}

				break;
				case SliderX::ValTypeDouble: case SliderX::ValTypePcnt:					
					min = 0x7FF0000000000000;
					sli_hdr->value[3].ui = val;
					val = 0;

					for (UI_64 ii(sli_hdr->value[0].ui);ii<=sli_hdr->value[1].ui;ii++) {
						reinterpret_cast<double &>(max) = reinterpret_cast<const double *>(sli_hdr->vals_list)[ii + 1] - sli_hdr->value[3].d;
						max &= 0x7FFFFFFFFFFFFFFF;
						if (reinterpret_cast<double &>(max) < reinterpret_cast<double &>(min)) {
							min = max;
							val = ii;
						}
					}
										
				break;
			}

			sli_hdr->value[3].d = 0.1 + val - sli_hdr->value[0].ui;
			sli_hdr->value[3].d /= (sli_hdr->value[1].ui - sli_hdr->value[0].ui);

			sli_hdr->increment_val = 1.0/(sli_hdr->value[1].ui - sli_hdr->value[0].ui);
		}

		sli_hdr->value[2].ui = val;



		Release();
	}

	SetLabels();
}

UI_64 OpenGL::SliderX::ValueAdjust(const double * s_ptr) {
	UI_64 result(0);
	double scale_fac(0);

	SliderX::Header * sli_hdr(0);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr+1);

		if (s_ptr) scale_fac = s_ptr[0];
		else scale_fac = sli_hdr->value[3].d;

		if (sli_hdr->vals_list == 0) {
			switch (sli_hdr->v_type & SliderX::ValTypeMask) {
				case SliderX::ValTypeUInt: case SliderX::ValTypeTime:
					result = sli_hdr->value[0].ui + scale_fac*(sli_hdr->value[1].ui - sli_hdr->value[0].ui); // sli_hdr->value[2].ui

				break;
				case SliderX::ValTypeInt:
					reinterpret_cast<I_64 &>(result) = sli_hdr->value[0].i + scale_fac*(sli_hdr->value[1].i - sli_hdr->value[0].i);

				break;
				case SliderX::ValTypeDouble: case SliderX::ValTypePcnt:
					reinterpret_cast<double &>(result) = sli_hdr->value[0].d + scale_fac*(sli_hdr->value[1].d - sli_hdr->value[0].d); // sli_hdr->value[2].d
				break;
			}

//			result = sli_hdr->value[2].ui;
		} else {
			result = sli_hdr->value[0].ui + scale_fac*(sli_hdr->value[1].ui - sli_hdr->value[0].ui); // sli_hdr->value[2].ui

			result = sli_hdr->vals_list[result + 1]; // sli_hdr->value[2].ui
		}

		Release();
	}

	return result;
}

UI_64 OpenGL::SliderX::SetLabels() {
	UI_64 result(0);
	wchar_t t_str[64];
	OpenGL::Text el_text(reinterpret_cast<OpenGL::Text &>(*this));

	SliderX::Header * sli_hdr(0);


	el_text.Down();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr+1);


		switch (sli_hdr->v_type & SliderX::ValTypeMask) {
			case SliderX::ValTypeUInt:
				if (sli_hdr->vals_list) {
					result = Strings::UInt64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->vals_list[sli_hdr->value[2].ui + 1], 0);
				} else {
					result = Strings::UInt64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[2].ui, 0);
				}
			
			break;
			case SliderX::ValTypeInt:
				if (sli_hdr->vals_list) {
					result = Strings::Int64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), reinterpret_cast<const int *>(sli_hdr->vals_list)[sli_hdr->value[2].ui + 1], 0);
				} else {
					result = Strings::Int64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[2].i, 0);
				}

			break;
			case SliderX::ValTypeTime:
				if (sli_hdr->vals_list) {
					result = Strings::TimeToCStr(reinterpret_cast<char *>(t_str), sli_hdr->vals_list[sli_hdr->value[2].ui + 1]);
				} else {
					result = Strings::TimeToCStr(reinterpret_cast<char *>(t_str), sli_hdr->value[2].ui);
				}
				
			break;
			case SliderX::ValTypeDouble:
				if (sli_hdr->vals_list) {
					result = Strings::DoubleToCStr_SSSE3(reinterpret_cast<char *>(t_str), reinterpret_cast<const double *>(sli_hdr->vals_list)[sli_hdr->value[2].ui + 1], sli_hdr->v_type & (~SliderX::ValTypeMask));
				} else {
					result = Strings::DoubleToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[2].d, sli_hdr->v_type & (~SliderX::ValTypeMask));
				}

			break;
			case SliderX::ValTypePcnt:
				if (sli_hdr->vals_list) {
					result = Strings::IntToCStr_SSSE3(reinterpret_cast<char *>(t_str), reinterpret_cast<const double *>(sli_hdr->vals_list)[sli_hdr->value[2].ui + 1]*100.0, 0);
				} else {
					result = Strings::IntToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[2].d*100.0, 0);
				}

				reinterpret_cast<char *>(t_str)[result++] = '%';
				reinterpret_cast<char *>(t_str)[result] = '\0';
				
			break;
		}

		Strings::A2W(t_str);
		el_text.Rewrite(t_str, 0, result);

		
		if ((sli_hdr->flags & SliderX::LabelTypeSS) && (sli_hdr->vals_list == 0)) {
			reinterpret_cast<SFSco::Object &>(el_text).Left();

			switch (sli_hdr->v_type & SliderX::ValTypeMask) {
				case SliderX::ValTypeUInt:
					result = Strings::UInt64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[1].ui - sli_hdr->value[2].ui, 0);
	
				break;
				case SliderX::ValTypeInt:
					result = Strings::Int64ToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[1].i - sli_hdr->value[2].i, 0);

				break;
				case SliderX::ValTypeTime:
					result = Strings::TimeToCStr(reinterpret_cast<char *>(t_str), sli_hdr->value[1].ui - sli_hdr->value[2].ui);
				
				break;
				case SliderX::ValTypeDouble:
					result = Strings::DoubleToCStr_SSSE3(reinterpret_cast<char *>(t_str), sli_hdr->value[1].d - sli_hdr->value[2].d, sli_hdr->v_type & (~SliderX::ValTypeMask));

				break;
				case SliderX::ValTypePcnt:
					result = Strings::IntToCStr_SSSE3(reinterpret_cast<char *>(t_str), (sli_hdr->value[1].d - sli_hdr->value[2].d)*100.0, 0);
					reinterpret_cast<char *>(t_str)[result++] = '%';
					reinterpret_cast<char *>(t_str)[result] = '\0';

				break;
			}

			Strings::A2W(t_str);
			el_text.Rewrite(t_str, 0, result);
					
		}

		Release();
	}

	return result;
}

void OpenGL::SliderX::Jump(Element & s_el) {
	UI_64 s_val(0);
	__declspec(align(16)) float temp_vec[4] = {0.0, 0.0, 0.0, 1.0};
	double d_val(0);

	ControlFunc control_ptr(0);
	Core * core_ptr(0);

	s_el.GetPtrPos(temp_vec);
	d_val = temp_vec[0];

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {		
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		if (sli_hdr->flags & (SliderX::ControlOn | SliderX::FixedControl)) {
			control_ptr = sli_hdr->vc_func;

//			sli_hdr->value[3].d = temp_vec[0];

			if (d_val < 0.0) d_val = 0.0;
			if (d_val > 1.0) d_val = 1.0;
		}

		Release();
		el_hdr = 0;

		if (control_ptr) {
			s_val = ValueAdjust(&d_val);
		
			if (control_ptr(*this, s_val)) {
				SetValue(s_val);
			}
		}
	}
}

UI_64 OpenGL::SliderX::Touch(float * d_vec) {
	UI_64 result(0);

	return result;
}


UI_64 OpenGL::SliderX::Configure(ControlFunc vc_ptr, const Button::PaletteEntry * id_plte, unsigned int f_ico) {	
	UI_64 _event(0);

	float some_vals[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	void (SliderX::* h_ptr)();
	void (SliderX::* b_ptr)(Element &);

	UI_64 result(0);
	Core * ogl_ptr(0);
	Element s_elem(*this), b_elem;

	unsigned int b_on;
	
	s_elem.Down();


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ogl_ptr = reinterpret_cast<Core *>(el_hdr->_context);
		b_on = reinterpret_cast<SliderX::Header *>(el_hdr + 1)->flags;
		if (f_ico) reinterpret_cast<SliderX::Header *>(el_hdr + 1)->flags |= SliderX::IconOn;
	
		reinterpret_cast<SliderX::Header *>(el_hdr+1)->vc_func = vc_ptr; // works with progress bar

		System::MemoryCopy(reinterpret_cast<SliderX::Header *>(el_hdr + 1)->palette_cfg, id_plte, 2*sizeof(Button::PaletteEntry));

		Release();


		if ((b_on & SliderX::FixedControl) == 0) {
			h_ptr = &SliderX::SHoover;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagHoover);

			h_ptr = &SliderX::SDown;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseDown);

			h_ptr = &SliderX::SLeave;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagLeave);
	
			h_ptr = &SliderX::SUp;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseUp);
		}







		// label einz		
		

		if (b_on & SliderX::LabelTypeSS) {
			s_elem.Left(); // label zwei

		}

		s_elem.Left(); // progress bar
		if (b_on & SliderX::FixedControl) s_elem.SetColor(id_plte[1].b_color);
		else s_elem.SetColor(id_plte[0].i_color);
		b_elem = s_elem;

		s_elem.Left(); // progress rect
		s_elem.SetColor(some_vals);
		s_elem.SetEBorderColor(id_plte[1].db_color);

		b_ptr = &SliderX::Jump;

		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), &s_elem, this);
		SFSco::Event_SetGate(_event, b_elem, (UI_64)Element::FlagMouseDown);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseDown);




		s_elem.Left();
		s_elem.SetColor(id_plte[1].i_color); // dec
		s_elem.SetXColor(id_plte[1].ico_xcol[0]);

		h_ptr = &SliderX::SBHoover;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagHoover);

		h_ptr = &SliderX::SBDown;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseDown);

		h_ptr = &SliderX::SBLeave;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagLeave);
	
		b_ptr = &SliderX::SBUp;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), &s_elem, this);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseUp);


		
		s_elem.Left();				
		s_elem.SetColor(id_plte[1].i_color); // inc
		s_elem.SetXColor(id_plte[1].ico_xcol[0]);

		h_ptr = &SliderX::SBHoover;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagHoover);

		h_ptr = &SliderX::SBDown;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseDown);

		h_ptr = &SliderX::SBLeave;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagLeave);
	
		b_ptr = &SliderX::SBUp;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), &s_elem, this);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseUp);


		if ((b_on & SliderX::FixedControl) == 0) {
			s_elem.Left(); // ico

			if (f_ico) {
				s_elem.SetRectTexture(id_plte[0].ib_color, ogl_ptr->GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | f_ico);

				s_elem.SetXColor(id_plte[0].ico_xcol[0]);
			}


			s_elem.SetColor(id_plte[0].color);
		

			if (b_on & SliderX::BorderOn) {			
				s_elem.SetEBorderColor(id_plte[0].b_color);
			}
		}


	}

	return result;
}



void OpenGL::SliderX::SHoover() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down().Right();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);		

		if ((sli_hdr->flags & SliderX::ControlOn) == 0) {
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
			b_elem.SetColor(sli_hdr->palette_cfg[0].h_color);
			

			if (sli_hdr->flags & IconOn) {
				b_elem.SetXColor(sli_hdr->palette_cfg[0].ico_xcol[1]);
			}

			if (sli_hdr->flags & SliderX::BorderOn) {
				b_elem.SetEBorderColor(sli_hdr->palette_cfg[0].hb_color);
				b_elem.Left();
			}



			b_elem.Left(); // label einz


			if (sli_hdr->flags & SliderX::LabelTypeSS) {
				b_elem.Left(); // lable zwei
			}

			b_elem.Left(); // progress bar
			b_elem.SetColor(sli_hdr->palette_cfg[0].hi_color);

		}

		Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SLeave() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down().Right();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);

		if ((sli_hdr->flags & SliderX::ControlOn) == 0) {
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

			b_elem.SetColor(sli_hdr->palette_cfg[0].color);
			

			if (sli_hdr->flags & IconOn) {
				b_elem.SetXColor(sli_hdr->palette_cfg[0].ico_xcol[0]);
			}

			if (sli_hdr->flags & SliderX::BorderOn) {
				b_elem.SetEBorderColor(sli_hdr->palette_cfg[0].b_color);
				b_elem.Left();
			}

			b_elem.Left(); // label einz

			if (sli_hdr->flags & SliderX::LabelTypeSS) {
				b_elem.Left(); // lable zwei
			}

			b_elem.Left(); // progress bar
			b_elem.SetColor(sli_hdr->palette_cfg[0].i_color);
		}

		Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SDown() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down().Right();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		b_elem.SetColor(sli_hdr->palette_cfg[0].d_color);
		

		if (sli_hdr->flags & IconOn) {
			b_elem.SetXColor(sli_hdr->palette_cfg[0].ico_xcol[2]);
		}

		if (sli_hdr->flags & SliderX::BorderOn) {
			b_elem.SetEBorderColor(sli_hdr->palette_cfg[0].db_color);
			b_elem.Left();
		}


		Release();

		core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SUp() {
	Core * core_ptr(0);
	float some_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f};
	AttributeArray a_arr;
	Element s_elem, x_clip(*this), b_elem;	
	x_clip.Up();

	s_elem = x_clip;
	s_elem.Down();
	// first op: if a mamber  of the clip has controlon do control off

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(x_clip.Acquire())) {
		SliderClip::Header * sc_hdr = reinterpret_cast<SliderClip::Header *>(el_hdr + 1);
		unsigned int sx_count = sc_hdr->cell_count;

		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
				

		for (;sx_count--;s_elem.Left()) {
			// relocate elemnts to a new order
			
			b_elem = s_elem;
			b_elem.Down();


			if (s_elem != *this) {
				s_elem.RelocateXY(some_vals + 8);

				if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(s_elem.Acquire())) {
					SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(s_hdr + 1);
								
					if (sli_hdr->flags & SliderX::ControlOn) {

						sli_hdr->flags ^= SliderX::ControlOn;

						if (sli_hdr->flags & SliderX::LabelTypeSS) {
							some_vals[0] = 10.0f;
							some_vals[1] = sli_hdr->label_cfg[0].f_size + 10.0f;;
							b_elem.RelocateXY(some_vals);
							
							b_elem.Left(); // label zwei
							b_elem.GetRightMargin(some_vals + 4);

							some_vals[0] = sli_hdr->face_width - some_vals[6] - 10.0f;
							some_vals[1] = some_vals[5];

							b_elem.RelocateXY(some_vals);

						} else {
							b_elem.Hide();
						}

						b_elem.Left(); // progressbar
						b_elem.SetColor(sli_hdr->palette_cfg[0].i_color);

						b_elem.RelocateXY(some_vals + 12);
						
						some_vals[0] = sli_hdr->face_width*sli_hdr->value[3].d;
						some_vals[1] = sc_hdr->cell_height;
						some_vals[2] = 1.0f;
						some_vals[3] = 1.0f;
						b_elem.Rescale(some_vals);


						b_elem.Left(); // progress rect
						b_elem.Hide();

						b_elem.Left(); // dec
						b_elem.Hide();

						b_elem.Left(); // inc
						b_elem.Hide();

						b_elem.Left();
						b_elem.Show();
					
						b_elem.SetColor(sli_hdr->palette_cfg[0].color);
						b_elem.SetXColor(sli_hdr->palette_cfg[0].ico_xcol[0]);
					}
					
					some_vals[8] += sli_hdr->face_width + 6.0f;
									
					s_elem.Release();
				}
			} else {
				some_vals[8] += some_vals[10];
				s_elem.RelocateXY(some_vals + 8);
				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(s_elem.Acquire())) {
					SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);

					sli_hdr->flags |= SliderX::ControlOn;


					if (sli_hdr->flags & SliderX::LabelTypeSS) {
						some_vals[0] = sc_hdr->cell_width*0.6666666666666666666f + 10.0f;
						some_vals[1] = sli_hdr->label_cfg[0].f_size + 10.0f;
						b_elem.RelocateXY(some_vals);
							
						b_elem.Left(); // label zwei
						b_elem.GetRightMargin(some_vals + 4);

						sli_hdr->lz_val = some_vals[0] + sli_hdr->progress_width;
						some_vals[0] = sli_hdr->lz_val - some_vals[6];
						some_vals[1] = some_vals[5];

						b_elem.RelocateXY(some_vals);


						b_elem.Left(); // progress bar
						b_elem.SetColor(sli_hdr->palette_cfg[1].b_color);

						some_vals[0] = sc_hdr->cell_width*0.6666666666666666666f + 10.0f;
						some_vals[1] = sli_hdr->label_cfg[0].f_size + 16.0f;

						some_vals[4] = sli_hdr->progress_width*sli_hdr->value[3].d;
						some_vals[5] = 10.0f;
						some_vals[6] = 1.0f;
						some_vals[7] = 1.0f;

						b_elem.RelocateXY(some_vals);
						b_elem.Rescale(some_vals + 4);

						b_elem.Left();
						b_elem.Show();
												
						some_vals[4] = sli_hdr->progress_width;
						some_vals[5] = 10.0f;

						b_elem.RelocateXY(some_vals);
						b_elem.Rescale(some_vals + 4);


					} else {
						b_elem.Show();
						b_elem.GetRightMargin(some_vals + 4);

						sli_hdr->lz_val = sc_hdr->cell_width*0.6666666666666666666f + 10.0f;
						some_vals[0] = sli_hdr->lz_val + 0.5f*(sli_hdr->progress_width - some_vals[6]);
						some_vals[1] = some_vals[5];

						b_elem.RelocateXY(some_vals);

						b_elem.Left(); // progress bar
						b_elem.SetColor(sli_hdr->palette_cfg[1].b_color);

						some_vals[0] = sc_hdr->cell_width*0.6666666666666666666f + 10.0f;
						some_vals[1] = sli_hdr->label_cfg[0].f_size + 22.0f;

						some_vals[4] = sli_hdr->progress_width*sli_hdr->value[3].d;
						some_vals[5] = 10.0f;
						some_vals[6] = 1.0f;
						some_vals[7] = 1.0f;

						b_elem.RelocateXY(some_vals);
						b_elem.Rescale(some_vals + 4);

						b_elem.Left();
						b_elem.Show();


						some_vals[4] = sli_hdr->progress_width;
						some_vals[5] = 10.0f;

						b_elem.RelocateXY(some_vals);
						b_elem.Rescale(some_vals + 4);

					}


					b_elem.Left();
					b_elem.Show();

					b_elem.Left();
					b_elem.Show();

					some_vals[0] = sli_hdr->progress_width + sc_hdr->cell_width*0.6666666666666666666f + 20.0f;
					some_vals[1] = 0.0f;
					b_elem.RelocateXY(some_vals);
					
					b_elem.Left();
					b_elem.Hide();
		

					some_vals[8] += some_vals[0] + sc_hdr->cell_width*0.6666666666666666666f + 32.0f;

					s_elem.Release();		
				}
			}

			some_vals[10] = 32.0f;
		}

		x_clip.Release();
	}



	if (core_ptr) core_ptr->ResetElement(x_clip);


}



void OpenGL::SliderX::SBHoover() {
	Core * core_ptr(0);
	Element slilem(*this);

	slilem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(slilem.Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(sli_hdr->palette_cfg[1].hi_color);
		SetXColor(sli_hdr->palette_cfg[1].ico_xcol[1]);
	
		slilem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SBLeave() {
	Core * core_ptr(0);
	Element slilem(*this);

	slilem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(slilem.Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(sli_hdr->palette_cfg[1].i_color);
		SetXColor(sli_hdr->palette_cfg[1].ico_xcol[0]);
	
		slilem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SBDown() {
	Core * core_ptr(0);
	Element slilem(*this);

	slilem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(slilem.Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(sli_hdr->palette_cfg[1].di_color);
		SetXColor(sli_hdr->palette_cfg[1].ico_xcol[2]);
	
		slilem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::SliderX::SBUp(Element & b_elem) {
	UI_64 s_val(0);
	double d_val(0);

	Core * core_ptr(0);
	ControlFunc control_ptr(0);

	Element s_elem(*this);
	s_elem.Down().Right().Right().Right();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		SliderX::Header * sli_hdr = reinterpret_cast<SliderX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
		control_ptr = sli_hdr->vc_func;

		b_elem.SetColor(sli_hdr->palette_cfg[1].hi_color);
		b_elem.SetXColor(sli_hdr->palette_cfg[1].ico_xcol[1]);

		

		if (s_elem == b_elem) {
			d_val = sli_hdr->value[3].d - sli_hdr->increment_val;
		} else {
			d_val = sli_hdr->value[3].d + sli_hdr->increment_val;
		}

		s_elem.Right().Right();
		

		if (d_val < 0.0) d_val = 0.0;
		if (d_val > 1.0) d_val = 1.0;

		Release();
			

		if (control_ptr) {
			s_val = ValueAdjust(&d_val);

			if (control_ptr(*this, s_val)) {
				SetValue(s_val);
				s_val = -1;
			}
		}

		if ((core_ptr) && (s_val != -1)) core_ptr->ResetElement(*this);
	}
}



