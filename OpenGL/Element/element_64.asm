
; safe-fail OGL elements
; Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation
;
; This program is distributed WITHOUT ANY WARRANTY. See the
; GNU General Public License for more details.
;
; Any non-GPL usage of this software or parts of this software is strictly
; forbidden.
;




	stretch_vector			TEXTEQU	<64>
	stretch_mask			TEXTEQU <80>

	offset_mask				TEXTEQU <96>

	scale_vector			TEXTEQU <128>
	trans_vector			TEXTEQU <144>



OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE


.CONST

ALIGN 16

float_01		DWORD	00000000h, 3F800000h, 00000000h, 3F800000h
float_one		DWORD	3F800000h, 3F800000h, 3F800000h, 3F800000h
float_origin_4	DWORD	00000000h, 00000000h, 3F800000h, 3F800000h
float_top		DWORD	7F800000h, 7F800000h, 7F800000h, 7F800000h
float_sign		DWORD	80000000h, 80000000h, 80000000h, 80000000h

vector_mask		DWORD	0FFFFFFFFh, 0FFFFFFFFh, 0FFFFFFFFh, 0
sign_mask		DWORD	7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh, 7FFFFFFFh


.CODE

ALIGN 16
?PageTest@Element@OpenGL@@CA_KPEAMPEBM@Z	PROC	
	MOVAPS xmm3, XMMWORD PTR [sign_mask]
	MOVAPS xmm4, XMMWORD PTR [float_sign]

	MOVLPS xmm0, QWORD PTR [rdx] ; offset
	UNPCKLPS xmm0, xmm0
	MOVLPS xmm1, QWORD PTR [rcx] ; current page position
	MOVLPS xmm2, QWORD PTR [rcx+8] ; page size
	UNPCKLPS xmm1, xmm1
	UNPCKLPS xmm2, xmm2
	

	ADDPS xmm1, xmm0


	XORPS xmm0, xmm0
	CMPPS xmm0, xmm2, 4

	ANDPS xmm1, xmm0

	ANDPS xmm3, xmm1 ; abs offset	
	CMPPS xmm3, xmm2, 6 ; not less or eq to page
	
	ANDPS xmm2, xmm3
	
	ANDPS xmm4, xmm1 ; offset sign
	ORPS xmm2, xmm4
	
	SUBPS xmm1, xmm2

	XORPS xmm3, xmm3
	CMPPS xmm2, xmm3, 4
	
	SHUFPS xmm1, xmm1, 08h
	MOVLPS QWORD PTR [rcx], xmm1

	MOVMSKPS eax, xmm2 ; page adjust
	MOVMSKPS edx, xmm4 ; sign

	XOR edx, 5 ; sign fix
	AND eax, edx

	SHL eax, 8

	RET
?PageTest@Element@OpenGL@@CA_KPEAMPEBM@Z	ENDP

ALIGN 16
?ClearArray@AttributeArray@OpenGL@@CAXPEAXIPEBM@Z	PROC
	MOVAPS xmm0, [float_origin_4]	
	
	CMP r8, 0
	JZ filoop
	MOVUPS xmm0, [r8]
align 16
	filoop:
		MOVNTPS [rcx], xmm0
		ADD rcx, 16

		SUB rdx, 1
	JNZ filoop

	RET
?ClearArray@AttributeArray@OpenGL@@CAXPEAXIPEBM@Z	ENDP


ALIGN 16
?GetRange@AttributeArray@OpenGL@@CAXPEBXIPEAM@Z	PROC
	MOVAPS xmm3, [float_top]
	MOVAPS xmm2, [float_top]
	ORPS xmm2, [float_sign]
	XORPS xmm1, xmm1
	
	CVTSI2SS xmm5, edx
	SHUFPS xmm5, xmm5, 0

align 16
	coloop:
		MOVAPS xmm0, [rcx]
		MAXPS xmm2, xmm0
		MINPS xmm3, xmm0
		ADDPS xmm1, xmm0

		ADD rcx, 16
	SUB edx, 1
	JNZ coloop

	DIVPS xmm1, xmm5

	MOVAPS [r8], xmm3
	MOVAPS [r8+16], xmm2
	MOVAPS [r8+32], xmm1

	RET
?GetRange@AttributeArray@OpenGL@@CAXPEBXIPEAM@Z	ENDP


ALIGN 16
?SetRange@AttributeArray@OpenGL@@CAXPEAXMMI@Z	PROC
	MOVAPS xmm4, xmm2
	SHUFPS xmm4, xmm4, 51h
	ORPS xmm4, xmm1

	MOVAPS xmm3, [float_top]
	MOVAPS xmm2, [float_top]
	ORPS xmm2, [float_sign]
	XORPS xmm1, xmm1
	
	MOV edx, r9d

align 16
	coloop:
		MOVAPS xmm0, [rcx]
		MAXPS xmm2, xmm0
		MINPS xmm3, xmm0

		ADD rcx, 16
	SUB edx, 1
	JNZ coloop

	SUBPS xmm2, xmm3

	DIVPS xmm4, xmm2

align 16
	boxloop:
		SUB rcx, 16

		MOVAPS xmm0, [rcx]
		SUBPS xmm0, xmm3
		MULPS xmm0, xmm4

		MOVLPS QWORD PTR [rcx], xmm0
				
	SUB r9d, 1
	JNZ boxloop
	
	RET
?SetRange@AttributeArray@OpenGL@@CAXPEAXMMI@Z	ENDP


ALIGN 16
?DoAnchor@AttributeArray@OpenGL@@CA_KPEAM0I@Z	PROC
	
	MOVAPS xmm0, [rcx+16]
	SUBPS xmm0, [rcx]

	MOVAPS xmm4, [rcx+48] ; box
		
	MOVAPS xmm5, [rcx + stretch_vector]
	MOVAPS xmm3, [rcx + stretch_mask]
	ANDPS xmm3, xmm5
	MULPS xmm5, [rcx + offset_mask]

	ADDPS xmm5, [rcx+trans_vector]
	MOVAPS [rcx+trans_vector], xmm5

	XORPS xmm2, xmm2
	CMPPS xmm2, xmm3, 4

	MOVMSKPS rax, xmm2

	TEST rax, rax
	JZ exito
		DIVPS xmm3, xmm0
		ANDPS xmm3, xmm2

		XORPS xmm2, xmm2
		CMPPS xmm2, xmm0, 4

		ANDPS xmm3, xmm2

align 16
	rdxloop:
		MOVAPS xmm1, [rdx] ; vertex in
		MOVAPS xmm2, [rdx]
			
		SUBPS xmm1, xmm4		
		MULPS xmm1, xmm3

		ADDPS xmm1, xmm2
			
		MOVAPS [rdx], xmm1
	
		ADD rdx, 16
	SUB r8d, 1
	JNZ rdxloop

	exito:
	RET
?DoAnchor@AttributeArray@OpenGL@@CA_KPEAM0I@Z	ENDP


ALIGN 16
?GonCalc@AttributeArray@OpenGL@@CAXPEAMIMM@Z	PROC	
	XORPS xmm0, xmm0
	XORPS xmm1, xmm1
	UNPCKLPS xmm1, XMMWORD PTR [float_one] ; 0, 1, 0, 1

	MOVLHPS xmm0, xmm1
	MOVLHPS xmm1, xmm0

	MOVAPS xmm4, xmm2

	UNPCKLPS xmm2, xmm3 ; cos, sin
	XORPS xmm3, XMMWORD PTR [float_sign]
	UNPCKLPS xmm3, xmm4 ; -sin, cos
	MOVLHPS xmm2, xmm3 ; cos, sin, -sin, cos

	XORPS xmm3, xmm3
	

align 16
	calc_loop:
		MOVAPS [rcx], xmm0

		ADDPS xmm0, xmm1
		MOVLHPS xmm1, xmm1

		MULPS xmm1, xmm2
		HADDPS xmm1, xmm3
		
		ADD rcx, 16
	DEC edx
	JNZ calc_loop

	RET
?GonCalc@AttributeArray@OpenGL@@CAXPEAMIMM@Z	ENDP



ALIGN 16
?CalcBorder@Element@OpenGL@@CAXPEBMIPEAM@Z	PROC
	DEC edx
	
	MOVDQA xmm4, XMMWORD PTR [float_01]
	MOVDQA xmm2, [r8]

	ANDPS xmm2, XMMWORD PTR [sign_mask]

	MOV eax, edx
	SHL eax, 4

	MOVLPS xmm1, QWORD PTR [rcx+rax] ; -1
	MOVLPS xmm5, QWORD PTR [rcx]

align 16
	calc_loop:
		MOVAPS xmm0, xmm1
		MOVDDUP xmm1, QWORD PTR [rcx] ; 0
		MOVHPS xmm0, QWORD PTR [rcx+16] ; 1
		
		SUBPS xmm0, xmm1
		
		MOVAPS xmm3, xmm0
		MULPS xmm3, xmm3
		HADDPS xmm3, xmm3

		SHUFPS xmm3, xmm3, 0F0h

		SQRTPS xmm3, xmm3

		DIVPS xmm0, xmm3

		MOVHLPS xmm3, xmm0
		ADDPS xmm0, xmm3
		MULPS xmm0, xmm2

		ADDPS xmm0, xmm1
		MOVLHPS xmm0, xmm4
		MOVAPS [rcx], xmm0

		ADD rcx, 16

	DEC edx
	JNZ calc_loop

	MOVAPS xmm0, xmm1
	MOVDDUP xmm1, QWORD PTR [rcx] ; 0
	MOVLHPS xmm0, xmm5
		
	SUBPS xmm0, xmm1
		
	MOVAPS xmm3, xmm0
	MULPS xmm3, xmm3
	HADDPS xmm3, xmm3

	SHUFPS xmm3, xmm3, 0F0h

	SQRTPS xmm3, xmm3

	DIVPS xmm0, xmm3

	MOVHLPS xmm3, xmm0
	ADDPS xmm0, xmm3
	MULPS xmm0, xmm2

	ADDPS xmm0, xmm1
	MOVLHPS xmm0, xmm4
	MOVAPS [rcx], xmm0

	RET
?CalcBorder@Element@OpenGL@@CAXPEBMIPEAM@Z	ENDP


END

