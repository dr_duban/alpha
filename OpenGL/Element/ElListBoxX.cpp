/*
** safe-fail OGL elements
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "System\SysUtils.h"

UI_64 OpenGL::Core::CreateListBox(ListBoxX & lb_obj, unsigned int la_flags, const Element::LabelCfg * la_cfg, const Element * parent) {
	float some_vals[8] = {0.0f, 0.0f, (la_flags & 0x00FF), ((la_flags >> 8) & 0x00FF),		0.0f, 0.0f, 64.0f, 64.0f};

	UI_64 result = CreateElement(Element::Quad, lb_obj, 4, parent, sizeof(ListBoxX::Header)), up_t(0), down_t(0);

	unsigned int sx_idx(0);
	
	Element s_elem;
	ButtonGrid b_grid;

	AttributeArray a_arr;
	
	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(lb_obj.Acquire())) {			

			reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->face_width = some_vals[2];
			reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->face_height = some_vals[3];
			reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->flags = la_flags | ListBoxX::FixedControl;
			
			System::MemoryCopy(reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->label_cfg, la_cfg, 2*sizeof(Element::LabelCfg));

			lb_obj.Release();
		}
		
		
		result = CreateElement(Element::Quad, s_elem, 4, &lb_obj); // face element

		if (result != -1) {
			s_elem.Hide();

			s_elem.SetAnchorMode(LeftBottomFront);
			s_elem.SetRect(some_vals);
			s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
				// set border possibly



			result = s_elem.AddLabel(la_cfg[0]); // face elem label
			result |= s_elem.AddLabel(la_cfg[0]); // face elem second label

		}

		
		if (result != -1) {
			if (la_flags & ListBoxX::HorizontalStretch) {
				some_vals[2] = 42.666666666666f;
				some_vals[3] = 64.0f;
				
				up_t = (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetRightTID();
				down_t = (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetLeftTID();
			} else {
				some_vals[2] = 64.0f;
				some_vals[3] = 42.666666666666f;

				up_t = (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetUpTID();
				down_t = (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | GetDownTID();
			}

			result = CreateElement(Element::Quad, s_elem, 4, &lb_obj); // up button

			if (result != -1) {
				s_elem.SetAnchorMode(LeftBottomFront);
				s_elem.SetRect(some_vals);
				s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
				s_elem.SetRectTexture(some_vals + 4, GetSystemTID(), up_t);

				result = CreateElement(Element::Quad, s_elem, 4, &lb_obj); // down button
		
			}
		}

		if (result != -1) {
			s_elem.SetAnchorMode(LeftBottomFront);
			s_elem.SetRect(some_vals);
			s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
			s_elem.SetRectTexture(some_vals + 4, GetSystemTID(), down_t);


			result = CreateButtonGrid(b_grid, &lb_obj);

			if (result != -1) {
				b_grid.SetAnchorMode(LeftBottomFront);
				if (la_flags & ListBoxX::HorizontalStretch) b_grid.SetHMode();
			}
		}
	}


	return result;
}


UI_64 OpenGL::Core::CreateLBXClip(ListBoxClip & lbx_el, float w, float h, unsigned int flags, const Element * parent) {
	UI_64 result = CreateElement(Element::Quad, lbx_el, 4, parent, sizeof(ListBoxClip::Header));
	AttributeArray a_arr;

	if (result != -1) {
		if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(lbx_el.Acquire())) {
			reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1)->cell_width = w;
			reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1)->cell_height = h;
			reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1)->_flags = flags;
			
			lbx_el.Release();
		}

		lbx_el.SetAnchorMode(LeftBottomFront);
/*
		lbx_el.InitAttributeArray(AttributeArray::Vertex, a_arr);
		if (AttributeArray::Header * v_hdr = reinterpret_cast<AttributeArray::Header *>(a_arr.Acquire())) {
			reinterpret_cast<float *>(v_hdr + 1)[4] = reinterpret_cast<float *>(v_hdr + 1)[12] = 64.0f;
			reinterpret_cast<float *>(v_hdr + 1)[9] = reinterpret_cast<float *>(v_hdr + 1)[13] = 64.0f;

			a_arr.Release();
		}

		a_arr.CalculateBox(LeftBottomFront);
*/

	}

	return result;
}

UI_64 OpenGL::ListBoxClip::CreateLBX(ListBoxX & lb_obj, unsigned int la_flags, const LabelCfg * la_cfg, unsigned int wh) {
	UI_64 result(-1);

	Core * ogl_ptr(0);
	float ico_box[16] = {0.0f, 0.0f, (wh & 0x0000FFFF), ((wh >> 16) & 0x0000FFFF),	0.0f, 0.0f, 64.0f, 64.0f,		0.0f, 0.0f, 1.0f, 1.0f,		0.0f, 0.0f, 0.0f, 0.0f};

	unsigned int sx_idx(0);
	
	Element s_elem;
	ButtonGrid b_grid;

	AttributeArray a_arr;


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxClip::Header * lbc_hdr = reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1);

		sx_idx = lbc_hdr->cell_count++;


		if (lbc_hdr->_flags & ListBoxX::HorizontalStretch) {
			if (ico_box[2] == 0.0f) ico_box[2] = lbc_hdr->cell_width;
			
			ico_box[3] = lbc_hdr->cell_height;
			ico_box[0] = lbc_hdr->c_offset;

			lbc_hdr->c_offset += ico_box[2] + 6.0f;

			ico_box[10] = 40.0f; // lbc_hdr->cell_width*0.6666666666666666666f;
			ico_box[11] = lbc_hdr->cell_height;

		} else {
			if (ico_box[3] == 0.0f) ico_box[3] = lbc_hdr->cell_height;

			ico_box[2] = lbc_hdr->cell_width;
			ico_box[1] = lbc_hdr->c_offset;

			lbc_hdr->c_offset += ico_box[3] + 6.0f;

			ico_box[10] = lbc_hdr->cell_width;
			ico_box[11] = 40.0f; // lbc_hdr->cell_height*0.6666666666666666666f;

		}





		Release();
	}

	if (ogl_ptr = GetContext()) {
		result = ogl_ptr->CreateElement(Element::Quad, lb_obj, 4, this, sizeof(ListBoxX::Header));

		if (result != -1) {
			if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(lb_obj.Acquire())) {
				el_hdr->_flags |= Element::FlagAction;

				reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->face_width = ico_box[2];
				reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->face_height = ico_box[3];

				reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->flags = la_flags;

				System::MemoryCopy(reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->label_cfg, la_cfg, 2*sizeof(LabelCfg));

				lb_obj.Release();
			}


			lb_obj.SetAnchorMode(LeftBottomFront);
			result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &lb_obj); // face element

			if (result != -1) {
				s_elem.SetAnchorMode(LeftBottomFront);
				s_elem.SetRect(ico_box);
				s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
				// set border possibly



				result = s_elem.AddLabel(la_cfg[0]); // face elem label
				result |= s_elem.AddLabel(la_cfg[0]); // face elem second label
			}

			if (result != -1) {
				result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &lb_obj); // up button

				if (result != -1) {
					s_elem.SetAnchorMode(LeftBottomFront);
					s_elem.SetRect(ico_box + 8);
					s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
					s_elem.SetRectTexture(ico_box + 4, ogl_ptr->GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | ogl_ptr->GetUpTID());

					s_elem.Hide();

					result = ogl_ptr->CreateElement(Element::Quad, s_elem, 4, &lb_obj); // down button

				}
			}

			if (result != -1) {
				s_elem.SetAnchorMode(LeftBottomFront);
				s_elem.SetRect(ico_box + 8);
				s_elem.InitAttributeArray(AttributeArray::Color, a_arr);
				s_elem.SetRectTexture(ico_box + 4, ogl_ptr->GetSystemTID(), (UI_64)Element::FlagNegTex | LAYERED_TEXTURE | ogl_ptr->GetDownTID());

				s_elem.Hide();

				result = ogl_ptr->CreateButtonGrid(b_grid, &lb_obj); // , 0, core_ptr->GetBListTID()

				if (result != -1) {
					b_grid.SetAnchorMode(LeftBottomFront);
					if (la_flags & ListBoxX::HorizontalStretch) b_grid.SetHMode();
					b_grid.Hide();
				}

			}
		}
	}

	return result;
}

UI_64 OpenGL::ListBoxClip::CoordsReset(float * bx_vals, const unsigned int * cp_vals, unsigned int do_reloc) {
	UI_64 result(0);
	float o_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f};
		
	Element x_el(*this), s_elem, t_elem;
	ButtonGrid b_grid;
	Text b_label;

	unsigned int x_dir(1);

	if (do_reloc) RelocateXY(bx_vals);

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxClip::Header * lbclip_hdr = reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1);

		bx_vals[0] = bx_vals[2] - lbclip_hdr->c_offset - 2.3333333333333f*lbclip_hdr->cell_width;
		bx_vals[1] = bx_vals[3] - lbclip_hdr->c_offset - 2.3333333333333f*lbclip_hdr->cell_height;

		if ((el_hdr->config.anchor & VANCHOR_TOP) && ((lbclip_hdr->_flags & ListBoxX::HorizontalStretch) == 0)) {
			x_dir = 0;			
		}

		
		x_el.Down();
		for (unsigned int li(0);li<lbclip_hdr->cell_count;li++, x_el.Left()) {			
			x_el.RelocateXY(o_vals + 8);
			s_elem = x_el; s_elem.Down();

			b_grid(s_elem).Right();

			if (lbclip_hdr)
			if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(x_el.Acquire())) {
				ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(s_hdr + 1);
				if (lbx_hdr->flags & ListBoxX::ControlOn) {
					lbx_hdr->flags ^= ListBoxX::ControlOn;
				}

				o_vals[12] = lbx_hdr->control_width;
		
				b_grid.Hide();

				
				if (lbclip_hdr->_flags & ListBoxX::HorizontalStretch) {
					if (x_el.IsVisible()) {
						o_vals[8] += lbx_hdr->face_width + 6.0f;
					}
					
					lbx_hdr->control_width = bx_vals[0];
					lbx_hdr->control_width += lbx_hdr->face_width;

					o_vals[14] = lbx_hdr->control_width;
					o_vals[15] = lbclip_hdr->cell_height + 2.0f;
				} else {
					if (x_el.IsVisible()) {
						if (x_dir) o_vals[9] += lbx_hdr->face_height + 6.0f;
						else o_vals[9] -= (lbx_hdr->face_height + 6.0f);
					}

					lbx_hdr->control_width = bx_vals[1];
					lbx_hdr->control_width += lbx_hdr->face_height;
					
					o_vals[14] = lbclip_hdr->cell_width + 2.0f;
					o_vals[15] = lbx_hdr->control_width;
				}

				o_vals[13] = lbx_hdr->control_width;








				x_el.Release();
				s_hdr = 0; lbx_hdr = 0;




				if (o_vals[12] != o_vals[13]) {					
					Release(); lbclip_hdr = 0;									

						o_vals[12] = 0.0f; // -2.0f;
						o_vals[13] = 0.0f; // -2.0f;

						result |= b_grid.XPand(o_vals + 12);

					if (el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
						lbclip_hdr = reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1);
					}
				}

				if (cp_vals) {
					b_grid.ResetGrid(cp_vals[li]);
				}







				if (s_hdr = reinterpret_cast<Element::Header *>(x_el.Acquire())) {
					lbx_hdr = reinterpret_cast<ListBoxX::Header *>(s_hdr + 1);
				


					s_elem.Show(); // face element

					s_elem.SetColor(lbx_hdr->palette_cfg[0].color);
					if (lbx_hdr->flags & ListBoxX::IconOn) s_elem.SetXColor(lbx_hdr->palette_cfg[0].ico_xcol[0]);

			

					t_elem = s_elem;
				
					t_elem.Down();
					if (lbx_hdr->flags & ListBoxX::BorderOn) {					
						// do something about border
						t_elem.Left();
					}



					if (Element::Header * bge_hdr = reinterpret_cast<Element::Header *>(b_grid.Acquire())) {
						ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(bge_hdr + 1);

						if (b_label(bg_hdr->last_but)) {
							b_label.Down();

							if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {
								b_label.::SFSco::Object::Left();
							}
	
							if (bg_hdr->button_set.flags & (Button::Header::IconPresent | Button::Header::XIconPresent)) {
								b_label.::SFSco::Object::Left();
							}
						}
					
					
						if (lbx_hdr->flags & ListBoxX::DoubleLabel) {
						// label 1
							if (b_label) reinterpret_cast<Text &>(t_elem).Rewrite(b_label);

							t_elem.GetRightMargin(o_vals + 4);
					
							o_vals[0] = 0.5f*(lbx_hdr->face_width - o_vals[6]);
							o_vals[1] = 0.333333333333333333f*(lbx_hdr->face_height - 2.0f*lbx_hdr->label_cfg[0].f_size) + lbx_hdr->label_cfg[0].f_size;
					
							t_elem.RelocateXY(o_vals);
							reinterpret_cast<Text &>(t_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);

							t_elem.Left();
							if (b_label) {
								b_label.::SFSco::Object::Left(); // label 2
								reinterpret_cast<Text &>(t_elem).Rewrite(b_label);
							}
							t_elem.GetRightMargin(o_vals + 4);

							o_vals[0] = 0.5f*(lbx_hdr->face_width - o_vals[6]);
							o_vals[1] *= 2.0f;
					
							t_elem.RelocateXY(o_vals);
							reinterpret_cast<Text &>(t_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);
						} else {
							if (b_label) reinterpret_cast<Text &>(t_elem).Rewrite(b_label);

							t_elem.GetRightMargin(o_vals + 4);
					
							o_vals[0] = 0.5f*(lbx_hdr->face_width - o_vals[6]);
							o_vals[1] = 0.5f*(lbx_hdr->face_height + lbx_hdr->label_cfg[0].f_size);
					
							t_elem.RelocateXY(o_vals);
							reinterpret_cast<Text &>(t_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);

							t_elem.Left();
							t_elem.Hide();
						}

						b_grid.Release();
					}

					x_el.Release();
				}


				
				

				s_elem.Left();
				s_elem.Hide();

				s_elem.Left(); // button down
				s_elem.Hide();

			}
		}

		if (lbclip_hdr) Release();		
	}

	return result;
}

OpenGL::ListBoxClip::ListBoxClip() {
	SetTID(type_id);
}

OpenGL::ListBoxClip::~ListBoxClip() {

}

// ==========================================================================================================================================================

OpenGL::ListBoxX::ListBoxX() {
	SetTID(type_id);
}


OpenGL::ListBoxX::~ListBoxX() {
	
}

UI_64 OpenGL::ListBoxX::Relocate(const float * bx_vals, unsigned int lbb_count) {
	float o_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

	UI_64 result(0), h_val(0);

	ButtonGrid b_grid;
	
	b_grid(*this).Down().Right();

	o_vals[0] = bx_vals[0];
	o_vals[1] = bx_vals[1];

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);

		o_vals[4] = lbx_hdr->control_width;
		lbx_hdr->control_width = bx_vals[2];

		if (lbx_hdr->flags & ListBoxX::FixedControl) {
			RelocateXY(o_vals);

			if (lbx_hdr->flags & ListBoxX::HorizontalStretch) {
				h_val = 1;

				o_vals[0] = lbx_hdr->face_width;
				o_vals[1] = 0.5f*(64.0f - lbx_hdr->face_height) - 4.0f;

				lbx_hdr->control_width -= 2.0f*lbx_hdr->face_width;

				o_vals[6] = lbx_hdr->control_width;
				o_vals[7] = lbx_hdr->face_height + 2.0f;

				o_vals[3] = 0.333333333333333333f*lbx_hdr->face_width;
			} else {
				o_vals[0] = 0.5f*(64.0f - lbx_hdr->face_width) - 4.0f;
				o_vals[1] = lbx_hdr->face_height;

				lbx_hdr->control_width -= 2.0f*lbx_hdr->face_height;

				o_vals[6] = lbx_hdr->face_width + 2.0f;
				o_vals[7] = lbx_hdr->control_width;

				o_vals[3] = 0.333333333333333333f*lbx_hdr->face_height;
			}

			o_vals[2] = lbx_hdr->control_width;
		}


		Release();

		b_grid.RelocateXY(o_vals);

		if (o_vals[4] != o_vals[2]) {
			o_vals[4] = 0.0f; // -2.0f;
			o_vals[5] = 0.0f; // -2.0f;			

			result |= b_grid.XPand(o_vals + 4);
		}

		if (lbb_count) b_grid.ResetGrid(lbb_count);


		if (h_val) {
			o_vals[0] += o_vals[2] + o_vals[3];
			o_vals[1] = 0.0f;
		} else {
			o_vals[0] = 0.0f;
			o_vals[1] += o_vals[2] + o_vals[3];
		}

		b_grid.Right();
		b_grid.RelocateXY(o_vals);
	}

	return result;
}

UI_64 OpenGL::ListBoxX::Configure(const Button::PaletteEntry * fb_palette, unsigned int f_ico) {

	float some_vals[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	void (ListBoxX::* h_ptr)();
	void (ListBoxX::* b_ptr)(Element &);

	UI_64 result(0), _event(0);
	Core * ogl_ptr(0);
	Element s_elem(*this);

	unsigned int b_on;
	
	s_elem.Down();


	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ogl_ptr = reinterpret_cast<Core *>(el_hdr->_context);
		b_on = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->flags;
		if (f_ico) reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->flags |= ListBoxX::IconOn;
	

		System::MemoryCopy(reinterpret_cast<ListBoxX::Header *>(el_hdr + 1)->palette_cfg, fb_palette, 2*sizeof(Button::PaletteEntry));

		Release();


		if ((b_on & ListBoxX::FixedControl) == 0) {
			h_ptr = &ListBoxX::LHoover;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagHoover);

			h_ptr = &ListBoxX::LDown;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseDown);

			h_ptr = &ListBoxX::LLeave;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagLeave);
	
			h_ptr = &ListBoxX::LUp;
			_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, this);
			SFSco::Event_SetGate(_event, *this, (UI_64)Element::FlagMouseUp);
		}






		s_elem.Left();
		s_elem.SetColor(fb_palette[1].i_color); // up
		s_elem.SetXColor(fb_palette[1].ico_xcol[0]);

		h_ptr = &ListBoxX::LBHoover;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagHoover);

		h_ptr = &ListBoxX::LBDown;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseDown);

		h_ptr = &ListBoxX::LBLeave;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagLeave);
	
		b_ptr = &ListBoxX::LBPUp;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), this, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseUp);
		s_elem.SetEFlags(Element::FlagAction);

//		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), &s_elem, this);
//		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseUp);


		
		s_elem.Left();				
		s_elem.SetColor(fb_palette[1].i_color); // down
		s_elem.SetXColor(fb_palette[1].ico_xcol[0]);

		h_ptr = &ListBoxX::LBHoover;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagHoover);

		h_ptr = &ListBoxX::LBDown;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseDown);

		h_ptr = &ListBoxX::LBLeave;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(h_ptr), 0, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagLeave);
	
		b_ptr = &ListBoxX::LBNUp;
		_event = SFSco::Event_CreateDrain(reinterpret_cast<void *&>(b_ptr), this, &s_elem);
		SFSco::Event_SetGate(_event, s_elem, (UI_64)Element::FlagMouseUp);
		s_elem.SetEFlags(Element::FlagAction);




// configure grid
		s_elem.Left();
		// button proc, cell, no icon, some handlers 



	}


	return result;
}


void OpenGL::ListBoxX::LHoover() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);		

		if ((lbx_hdr->flags & ListBoxX::ControlOn) == 0) {
			
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
			b_elem.SetColor(lbx_hdr->palette_cfg[0].h_color);
			

			if (lbx_hdr->flags & IconOn) {
				b_elem.SetXColor(lbx_hdr->palette_cfg[0].ico_xcol[1]);
			}


			b_elem.Down();

			if (lbx_hdr->flags & ListBoxX::BorderOn) {
				b_elem.SetEBorderColor(lbx_hdr->palette_cfg[0].hb_color);
				b_elem.Left();
			}


			
		// label einz
			reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].hc_color);

			

			if (lbx_hdr->flags & ListBoxX::DoubleLabel) {
				b_elem.Left(); // lable zwei

				reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].hc_color);
			}
		}

		Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LLeave() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);		

		if ((lbx_hdr->flags & ListBoxX::ControlOn) == 0) {
			
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
			b_elem.SetColor(lbx_hdr->palette_cfg[0].color);
			

			if (lbx_hdr->flags & IconOn) {
				b_elem.SetXColor(lbx_hdr->palette_cfg[0].ico_xcol[0]);
			}

			b_elem.Down();

			if (lbx_hdr->flags & ListBoxX::BorderOn) {
				b_elem.SetEBorderColor(lbx_hdr->palette_cfg[0].b_color);
				b_elem.Left();
			}


			
		// label einz
			reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);
			

			if (lbx_hdr->flags & ListBoxX::DoubleLabel) {
				b_elem.Left(); // lable zwei

				reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);
			}
		}

		Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LDown() {
	Core * core_ptr(0);
	Element b_elem(*this);

	b_elem.Down();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);		

		if ((lbx_hdr->flags & ListBoxX::ControlOn) == 0) {
			
			core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
			b_elem.SetColor(lbx_hdr->palette_cfg[0].d_color);
			

			if (lbx_hdr->flags & IconOn) {
				b_elem.SetXColor(lbx_hdr->palette_cfg[0].ico_xcol[2]);
			}

			b_elem.Down();

			if (lbx_hdr->flags & ListBoxX::BorderOn) {
				b_elem.SetEBorderColor(lbx_hdr->palette_cfg[0].db_color);
				b_elem.Left();
			}


			
		// label einz
			reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].dc_color);
			

			if (lbx_hdr->flags & ListBoxX::DoubleLabel) {
				b_elem.Left(); // lable zwei

				reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].dc_color);
			}
		}

		Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LUp() {
	Core * core_ptr(0);
	float some_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 1.0f};
	AttributeArray a_arr;
	Element s_elem, x_clip(*this), b_elem;
	ButtonGrid b_grid;
	Text b_label;
	unsigned int x_dir(1);

	x_clip.Up();

	s_elem = x_clip;
	s_elem.Down();
	// first op: if a mamber  of the clip has controlon do control off

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(x_clip.Acquire())) {
		ListBoxClip::Header * lbc_hdr = reinterpret_cast<ListBoxClip::Header *>(el_hdr + 1);
		unsigned int sx_count = lbc_hdr->cell_count;

		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);
		
		if ((el_hdr->config.anchor & VANCHOR_TOP) && ((lbc_hdr->_flags & ListBoxX::HorizontalStretch) == 0)) {
			x_dir = 0;
			some_vals[9] = lbc_hdr->cell_height;
		}

		for (;sx_count--;s_elem.Left()) {
			// relocate elemnts to a new order
			
			b_elem = s_elem;
			b_elem.Down();


			if (s_elem != *this) {
				if (x_dir) s_elem.RelocateXY(some_vals + 8);

				if (Element::Header * s_hdr = reinterpret_cast<Element::Header *>(s_elem.Acquire())) {
					ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(s_hdr + 1);
								
					if (lbx_hdr->flags & ListBoxX::ControlOn) {

						lbx_hdr->flags ^= ListBoxX::ControlOn;
						
						b_elem.Right(); // grid
						b_elem.Hide();
						b_grid(b_elem);

						b_elem.Right(); // button down
						b_elem.Hide();

						b_elem.Right(); // button up
						b_elem.Hide();


						b_elem.Right();
						b_elem.Show(); // face on

						b_elem.SetColor(lbx_hdr->palette_cfg[0].color);
						if (lbx_hdr->flags & ListBoxX::IconOn) {
							b_elem.SetXColor(lbx_hdr->palette_cfg[0].ico_xcol[0]);
						}


						b_elem.Down();

						if (lbx_hdr->flags & ListBoxX::BorderOn) {

							b_elem.Left();
						}

						// get grid selected item
						// set labels
						if (Element::Header * bge_hdr = reinterpret_cast<Element::Header *>(b_grid.Acquire())) {
							ButtonGrid::Header * bg_hdr = reinterpret_cast<ButtonGrid::Header *>(bge_hdr + 1);

							if (b_label(bg_hdr->last_but)) {
								b_label.Down();

								if (bg_hdr->button_set.flags & Button::Header::BorderPresent) {
									b_label.::SFSco::Object::Left();
								}
	
								if (bg_hdr->button_set.flags & (Button::Header::IconPresent | Button::Header::XIconPresent)) {
									b_label.::SFSco::Object::Left();
								}

								reinterpret_cast<Text &>(b_elem).Rewrite(b_label);
								reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);
							}


							

							b_elem.Left();
							if (lbx_hdr->flags & ListBoxX::DoubleLabel) {
								b_elem.Show();

								if (b_label) {
									b_label.::SFSco::Object::Left();
									reinterpret_cast<Text &>(b_elem).Rewrite(b_label);
									reinterpret_cast<Text &>(b_elem).SetTextColor(lbx_hdr->label_cfg[0].c_color);
								}
								
							} else {
								b_elem.Hide();
							}

							b_grid.Release();
						}
					}
					
					if (lbc_hdr->_flags & ListBoxX::HorizontalStretch) {
						some_vals[8] += lbx_hdr->face_width + 6.0f;
					} else {
						if (x_dir) some_vals[9] += lbx_hdr->face_height + 6.0f;
						else some_vals[9] -= lbx_hdr->face_height;
					}				

					s_elem.Release();

					if (x_dir == 0) {
						s_elem.RelocateXY(some_vals + 8);
						some_vals[9] -= 6.0f;
					}
				}
			} else {
				if (lbc_hdr->_flags & ListBoxX::HorizontalStretch) {
					some_vals[8] += some_vals[10];
				} else {
					if (x_dir) some_vals[9] += some_vals[10];
					else some_vals[9] -= some_vals[10];
				}

				if (x_dir) s_elem.RelocateXY(some_vals + 8);

				
				if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(s_elem.Acquire())) {
					ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);

					lbx_hdr->flags |= ListBoxX::ControlOn;

					b_elem.Hide();

					b_elem.Left();
					b_elem.Show(); // button up
					b_elem.SetColor(lbx_hdr->palette_cfg[1].i_color);
					b_elem.SetXColor(lbx_hdr->palette_cfg[1].ico_xcol[0]);

					b_elem.Left(); // button down
					b_elem.Show();
					b_elem.SetColor(lbx_hdr->palette_cfg[1].i_color);
					b_elem.SetXColor(lbx_hdr->palette_cfg[1].ico_xcol[0]);


					if (lbc_hdr->_flags & ListBoxX::HorizontalStretch) {
						some_vals[0] = 40.0f + 20.0f + lbx_hdr->control_width;
						some_vals[1] = 0.0f;

						b_elem.RelocateXY(some_vals);

						some_vals[8] += some_vals[0] + 40.0f + 32.0f;
						some_vals[0] = 40.0f + 10.0f;

					} else {
						some_vals[0] = 0.0f;
						some_vals[1] = 40.0f + 20.0f + lbx_hdr->control_width;
						if (x_dir == 0) some_vals[9] -= (some_vals[1] + 40.0f);

						b_elem.RelocateXY(some_vals);
												
						if (x_dir) some_vals[9] += some_vals[1] + 40.0f + 32.0f;

						some_vals[1] = 40.0f + 10.0f;
					}

					b_elem.Left(); // grid
					b_elem.Show();
					
					b_elem.RelocateXY(some_vals);

					// adjust grid


					s_elem.Release();		
				}

				if (x_dir == 0) {
					s_elem.RelocateXY(some_vals + 8);
					some_vals[9] -= 32.0f;
				}

			}

			some_vals[10] = 32.0f;
			
			
		}


		x_clip.Release();
	}



	if (core_ptr) core_ptr->ResetElement(x_clip);


}



void OpenGL::ListBoxX::LBHoover() {
	Core * core_ptr(0);
	Element l_elem(*this);

	l_elem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(l_elem.Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(lbx_hdr->palette_cfg[1].hi_color);
		SetXColor(lbx_hdr->palette_cfg[1].ico_xcol[1]);
	
		l_elem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LBLeave() {
	Core * core_ptr(0);
	Element l_elem(*this);

	l_elem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(l_elem.Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(lbx_hdr->palette_cfg[1].i_color);
		SetXColor(lbx_hdr->palette_cfg[1].ico_xcol[0]);
	
		l_elem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LBDown() {
	Core * core_ptr(0);
	Element l_elem(*this);

	l_elem.Up();

	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(l_elem.Acquire())) {
		ListBoxX::Header * lbx_hdr = reinterpret_cast<ListBoxX::Header *>(el_hdr + 1);
		
		core_ptr = reinterpret_cast<Core *>(el_hdr->_context);

		SetColor(lbx_hdr->palette_cfg[1].di_color);
		SetXColor(lbx_hdr->palette_cfg[1].ico_xcol[2]);
	
		l_elem.Release();

		if (core_ptr) core_ptr->ResetElement(*this);
	}
}

void OpenGL::ListBoxX::LBNUp(Element & b_elem) {
	ButtonGrid b_grid;
	
	b_grid(b_elem).Down().Right();

	OpenGL::ButtonGrid::NextPage(b_grid);

	LBHoover();
}

void OpenGL::ListBoxX::LBPUp(Element & b_elem) {
	ButtonGrid b_grid;
	
	b_grid(b_elem).Down().Right();

	OpenGL::ButtonGrid::PrevPage(b_grid);

	LBHoover();
}


unsigned int OpenGL::ListBoxX::ScanListUp(unsigned int & idx, UI_64 & oval) {
	idx++;
	oval = 0;	

	return 1;
}

unsigned int OpenGL::ListBoxX::ScanListDown(unsigned int & idx, UI_64 & oval) {
	idx--;
	oval = 0;	

	return 1;
}


UI_64 OpenGL::ListBoxX::ConfigureGrid(void * fb_ptr, ButtonGrid::ActionType select_ptr, const float * box_vals, Button::PaletteEntry * plte, unsigned int bo_flags) {

	float temp_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	ButtonGrid x_grid;

	x_grid(*this).Down().Right(); // border, ico, header, footer

	x_grid.SetColor(plte[0].i_color);

// grid border
	if (box_vals[3] >= 0.0f) {


	}

// button proc
	x_grid.SetButtonUpdate(fb_ptr, ScanListUp, ScanListDown, 1);


// configure cell
	x_grid.ConfigureCell(select_ptr, box_vals[0], box_vals[1], bo_flags, box_vals[2]);


// add palette

	if (plte[0].ref_vals[0] == 0)
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(x_grid.Acquire())) {
		plte[0].ref_vals[1] = 2 + reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->button_set.palette_size;

		x_grid.Release();
	}

	x_grid.AddPaletteEntry(plte[0]);

	if (plte[1].ref_vals[0] == 0)
	if (Element::Header * el_hdr = reinterpret_cast<Element::Header *>(x_grid.Acquire())) {
		plte[1].ref_vals[1] = 2 + reinterpret_cast<ButtonGrid::Header *>(el_hdr+1)->button_set.palette_size;

		x_grid.Release();
	}

	x_grid.AddPaletteEntry(plte[1]);



	return 0;
}

UI_64 OpenGL::ListBoxX::ConfigureGridIcon() {
	ButtonGrid x_grid;

	x_grid(*this).Down().Right(); // border, ico, header, footer

	x_grid.SetXIcon();

	return 0;
}

UI_64 OpenGL::ListBoxX::ConfigureGridText(const LabelCfg * la_cfg) {
	UI_64 result(0);

	ButtonGrid x_grid;

	x_grid(*this).Down().Right(); // border, ico, header, footer

	result |= x_grid.ConfigureTextLine(la_cfg, -1);	

	return result;
}




