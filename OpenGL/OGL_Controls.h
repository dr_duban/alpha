#pragma once

#include "Element.h"

namespace OpenGL {
	class Button : public Element {
		protected:
			int h_Hoover();
			int h_Move();


		public:
			static const size_t type_id = 569*Element::type_id;

	};


}