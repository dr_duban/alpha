/*
** safe-fail alpha main menu
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "OpenGL\OpenGL.h"

#include "CoreLibrary\SFScore.h"

// cache file belongs to main menu

namespace Apps {

	
	class MainMenu: public SFSco::IGLApp {
		private:
			static unsigned int FormatLNGButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int);
			static unsigned int FormatIMGButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int);
					
			static void __fastcall AlphaLeave(OpenGL::Element &);
			static void __fastcall AlphaUp(OpenGL::Element &);
						
			static void __fastcall FunctionLeave(OpenGL::Element &);
			static void __fastcall FunctionDown(OpenGL::Element &);
			static void __fastcall ExitHoover(OpenGL::Element &);
			static void __fastcall ExitLeave(OpenGL::Element &);
			static void __fastcall ExitDown(OpenGL::Element &);
		
			static void ShowInfo(OpenGL::APPlane &);
			static void ShowCP(OpenGL::APPlane &);
			static void HideInfo(OpenGL::APPlane &);

			static void DDonate(OpenGL::Element &);
			static void DDoover(OpenGL::Element &);
			static void DDeave(OpenGL::Element &);
			static void DDown(OpenGL::Element &);
		
			
			
			static UI_64 CPDefaultVolume(OpenGL::SliderX &, UI_64);
			static UI_64 SelectDLNG(OpenGL::ButtonGrid &, UI_64);
			static UI_64 SelectSIMG(OpenGL::ButtonGrid &, UI_64);
			static UI_64 CPIMGQuality(OpenGL::SliderX &, UI_64);

			static UI_64 SetSON(OpenGL::Button &);
			static UI_64 SetPRepeat(OpenGL::Button &);



			static OpenGL::Button::PaletteEntry don_palette;
			static OpenGL::Element::LabelCfg don_label;

			static OpenGL::Button::Config exit_but_cfg, switch_but_cfg[2];
			static OpenGL::Element::LabelCfg cp_head_label, cp_section_label, cp_option_label, cp_switch_labels[2], cp_slider_label, cp_lbox_labels[4];
			static OpenGL::Button::PaletteEntry cp_slider_palette[2], cp_lbgrid_palette[2], cp_lbox_palette[2];

			static unsigned int text_font, time_font;

			HANDLE cf_handle;

			OpenGL::Core * core_ogl;
			HMODULE _libin;

			UI_64 l_button;
			unsigned int function_group_id, sequence_on, list_top;			
			OpenGL::APPlane info_plane, control_plane;
			
			void InitCPPalette();
			UI_64 FormatCP();
						
			virtual void Change();
			virtual UI_64 AddFilter(SFSco::IGLApp *);

			virtual UI_64 ConfigurePreview(SFSco::Object &);
			virtual UI_64 CreatePreview(UI_64, SFSco::Object &, UI_64, UI_64, UI_64, unsigned int);

			virtual const wchar_t * GetSupportedList();
			virtual unsigned int GetSupportedCount();
			virtual UI_64 Test(const wchar_t *);

			virtual void * GetSelectorProc();
		public:
			static void __fastcall FunctionHoover(OpenGL::Element &);
			static void SetFunctionHandlers(OpenGL::Button &);

			MainMenu();
			virtual ~MainMenu();

			virtual UI_64 Initialize(SFSco::IOpenGL **, void *, void *);
			virtual UI_64 Finalize();

			
			UI_64 AddFunction(OpenGL::Button &, unsigned int);
			
			
			static unsigned int GetTextFont();
			static unsigned int GetTimeFont();

	};


}



