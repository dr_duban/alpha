/*
** safe-fail image viewer
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Apps\MainMenu.h"

#include "Apps\XPic.h"
#include "Apps\XFiles.h"


#include "OpenGL\Element.h"
#include "Pics\Image.h"

#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"

const wchar_t Apps::XPic::supported_xt_list[] = {	
													L'J', L'P', L'G', L'\0',
													L'J', L'P', L'E', L'G',
													L'B', L'M', L'P', L'\0',
													L'I', L'C', L'O', L'\0',
													L'G', L'I', L'F', L'\0',
													
													L'P', L'N', L'G', L'\0',
													L'T', L'I', L'F', L'F',
													L'T', L'I', L'F', L'\0',

													L'D', L'N', L'G', L'\0'
			
												};


OpenGL::Button::Config Apps::XPic::delete_but, Apps::XPic::op_but;

OpenGL::Element::LabelCfg Apps::XPic::fname_cfg, Apps::XPic::zoom_labels;

OpenGL::Button::PaletteEntry Apps::XPic::zoom_palette[2];

__declspec(align(16)) float Apps::XPic::progress_transform[2][16];

Apps::XPic::XPic() {


}

Apps::XPic::~XPic() {


}

UI_64 Apps::XPic::Initialize(SFSco::IOpenGL **, void *, void *) {
	UI_64 result(0);

	System::MemoryFill(zoom_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	zoom_palette[0].ico_xcol[0] = 0x73DF; zoom_palette[0].ico_xcol[1] = 0x7F0F; zoom_palette[0].ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(zoom_palette[0].color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(zoom_palette[0].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[0].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(zoom_palette[0].i_color, 0x00000020); // progress color
	OpenGL::Core::ColorConvert(zoom_palette[0].hi_color, 0x00FFFFFF);
	OpenGL::Core::ColorConvert(zoom_palette[0].di_color, 0x00000026);


	zoom_palette[0].ib_color[2] = zoom_palette[0].ib_color[3] = 64.0f;
	

	zoom_palette[1].ico_xcol[0] = 0x73DF; zoom_palette[1].ico_xcol[1] = 0x3F9F; zoom_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(zoom_palette[1].color, 0x010000FF); // buttons color
	OpenGL::Core::ColorConvert(zoom_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(zoom_palette[1].b_color, 0xFF38B4EB); // progress bar , 0xFFEBB438, 
	OpenGL::Core::ColorConvert(zoom_palette[1].hb_color, 0xFF38B4EB); // progress bar high
	OpenGL::Core::ColorConvert(zoom_palette[1].db_color, 0xFF38B4EB); // progress rect

	OpenGL::Core::ColorConvert(zoom_palette[1].i_color, 0x010000FF); // inc/dec icon color
	OpenGL::Core::ColorConvert(zoom_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[1].di_color, 0xFFFFE0A8);


	System::MemoryFill(&zoom_labels, sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(zoom_labels.c_color, 0xFF80FFFF);
		
	zoom_labels.f_size = 21.0f;
	zoom_labels.font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
//	zoom_labels.font_style = OpenGL::Text::Mono;
	zoom_labels.a_type = OpenGL::None;
	zoom_labels.max_char_count = 6;



	System::MemoryFill(&pic_but_palette, sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(pic_but_palette.color, 0xFF262626);
	OpenGL::Core::ColorConvert(pic_but_palette.h_color, 0xFF202020);
	OpenGL::Core::ColorConvert(pic_but_palette.d_color, 0xFF262626);

	OpenGL::Core::ColorConvert(pic_but_palette.b_color, 0x05FFFFFF);
	OpenGL::Core::ColorConvert(pic_but_palette.hb_color, 0x809DCCEF);
	OpenGL::Core::ColorConvert(pic_but_palette.db_color, 0x05FFFFFF);

	OpenGL::Core::ColorConvert(pic_but_palette.ib_color, 0xFF000000);
	OpenGL::Core::ColorConvert(pic_but_palette.hib_color, 0xFF000000);
	OpenGL::Core::ColorConvert(pic_but_palette.dib_color, 0xFF000000);



	System::MemoryFill(&delete_but, sizeof(OpenGL::Button::Config), 0);
	
	delete_but.header.flags = OpenGL::Button::Header::IconPresent;
	delete_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	
	delete_but.header.icon_box[0] = 0.0f; delete_but.header.icon_box[1] = 0.0f; delete_but.header.icon_box[2] = 64.0f; delete_but.header.icon_box[3] = 64.0f;

	delete_but.palette.ico_xcol[0] = 0x0014; delete_but.palette.ico_xcol[1] = 0x001F; delete_but.palette.ico_xcol[2] = 0x73DF;
	OpenGL::Core::ColorConvert(delete_but.palette.i_color, 0x010000FF);
	OpenGL::Core::ColorConvert(delete_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(delete_but.palette.di_color, 0xFF0000FF);

	System::MemoryFill(&op_but, sizeof(OpenGL::Button::Config), 0);

	op_but.header.flags = OpenGL::Button::Header::IconPresent;
	op_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	
	op_but.header.icon_box[0] = 0.0f; op_but.header.icon_box[1] = 0.0f; op_but.header.icon_box[2] = 64.0f; op_but.header.icon_box[3] = 64.0f;
	op_but.palette.ico_xcol[0] = 0x73DF; op_but.palette.ico_xcol[1] = 0x7F0F; op_but.palette.ico_xcol[2] = 0;

	OpenGL::Core::ColorConvert(op_but.palette.i_color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(op_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(op_but.palette.di_color, 0xFFA8E0FF);



	System::MemoryFill(&fname_cfg, sizeof(OpenGL::Element::LabelCfg), 0);
	fname_cfg.offset[0] = 24.0f;
	fname_cfg.offset[1] = 35.0f;

	OpenGL::Core::ColorConvert(fname_cfg.c_color, 0xFFCAFEB4);

	fname_cfg.f_size = 25.0f;
	fname_cfg.font_id = -1;
	fname_cfg.font_style = OpenGL::Text::Regular;
	fname_cfg.a_type = OpenGL::LeftBottomFront;
	fname_cfg.max_char_count = 256;









	System::MemoryFill_SSE3(progress_transform, 2*16*sizeof(float), 0, 0);

	progress_transform[0][0] = Calc::FloatCos(7.9f);
	progress_transform[0][1] = Calc::FloatSin(7.9f);

	progress_transform[0][4] = -progress_transform[0][1];
	progress_transform[0][5] = progress_transform[0][0];

	progress_transform[0][10] = progress_transform[0][15] = 1.0f;

	progress_transform[0][12] = 20.0f*(1.0f - progress_transform[0][0] + progress_transform[0][1]);
	progress_transform[0][13] = 20.0f*(1.0f - progress_transform[0][0] - progress_transform[0][1]);



	progress_transform[1][0] = Calc::FloatCos(2.6f);
	progress_transform[1][1] = Calc::FloatSin(2.6f);

	progress_transform[1][8] = Calc::FloatCos(1.2f);
	progress_transform[1][9] = Calc::FloatSin(1.2f);



	return result;
}

UI_64 Apps::XPic::Finalize() {
	UI_64 result(0);



	return result;
}


void Apps::XPic::Change() {


}


UI_64 Apps::XPic::AddFilter(SFSco::IGLApp *) {
	return 0;
}

UI_64 Apps::XPic::CreatePreview(UI_64 path_id, SFSco::Object & button_obj, UI_64 icon_id, UI_64 text_id, UI_64 name_id, unsigned int reset_flag) {
	UI_64 result(0);

	OpenGL::DecodeRecord de_rec;

	de_rec.fname_id = path_id;
	de_rec.pic_id = icon_id;
	de_rec.info_text_id = text_id;
	de_rec.flags = OpenGL::DecodeRecord::FlagPreview | reset_flag;
	de_rec.name_text_id = name_id;

	de_rec.pic_box[0] = 2.0f;
	de_rec.pic_box[1] = 32.0f;
	de_rec.pic_box[2] = 320.0f;
	de_rec.pic_box[3] = 320.0f;


	de_rec.element_obj = reinterpret_cast<OpenGL::Element &>(button_obj);	

	Pics::Image::Decode(de_rec);

	return result;
}

UI_64 Apps::XPic::ConfigurePreview(SFSco::Object & bgrid_el) {
	UI_64 result(0);
	float m_vals[4] = {10.0f, 10.0f, 0.0, -1.0f};
	OpenGL::ButtonGrid prew_grid;
	unsigned int lidx(0);

	prew_grid(bgrid_el);
	prew_grid.ConfigureIconTrajectory(ProgressCircleP);
	
	prew_grid.SetMargin(m_vals);
	prew_grid.ConfigureCell(FileSelect, 324.0f, 376.0f, 0x80000000, 1.1f); // , 0xFF262626 0x05FFFFFF, 
	prew_grid.ConfigureIcon(0x01400002, 0x01400020, LAYERED_TEXTURE, OpenGL::LeftBottomFront); // 0, 0xFF000000, 0x0000, 

	prew_grid.AddPaletteEntry(pic_but_palette);

	lidx = prew_grid.ConfigureTextLine(0xFFBAEEA4, 0, 0x00100000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 190.0f, 366.0f, 13.5f, -1, L"Calibril"); //reinterpret_cast<const wchar_t *>(mmenu->GetTextFont())); // 777799

	lidx = prew_grid.ConfigureTextLine(0xFFF0E080, 0, 0x00200000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 25.0f, 24.0f, 15.5f, -1, L"Calibril");


	return result;
}


const wchar_t * Apps::XPic::GetSupportedList() {
	return supported_xt_list;
}

unsigned int Apps::XPic::GetSupportedCount() {
	return 9;
}

UI_64 Apps::XPic::Test(const wchar_t * sext) {
	return (UI_64)Strings::SearchBlock(supported_xt_list, 9*4*sizeof(wchar_t), sext, 4*sizeof(wchar_t));
}



void Apps::XPic::ControlPlaneReset(OpenGL::Element & app_plane) {
	float some_vals[16] = {8.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f, 0.0f, 0.0f};
	OpenGL::Element control_el(app_plane);



	control_el.Down().Left(); // screen collection
	control_el.GetScaleVector(some_vals + 12);
	control_el.GetRightCorner(some_vals + 4);
	control_el.Down();
	control_el.GetRightMargin(some_vals + 8);
	control_el.Up();

	control_el.Right().Down();


	// name label

	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 50.0f;
	some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 50.0f;

	control_el.RelocateXY(some_vals);
	reinterpret_cast<OpenGL::Text &>(control_el).SetMCCount(0.06f*some_vals[6]);

	control_el.Left();

	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4];
	some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5] + 150.0f;

	if ((some_vals[1] + 70.0f) > some_vals[7]) some_vals[1] = some_vals[7] - 70.0f;
	some_vals[2] = some_vals[10]*some_vals[12];
	some_vals[3] = some_vals[11]*some_vals[13] + 150.0f;
		
	reinterpret_cast<OpenGL::SliderClip &>(control_el).CoordsReset(some_vals);
	


	// delete button
	control_el.Left();

	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 75.0f;
	some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 20.0f;
	control_el.RelocateXY(some_vals);

	// shot button
	control_el.Left();
	some_vals[1] += 150.0f;
	control_el.RelocateXY(some_vals);


	control_el.Up();
	if (OpenGL::Core * ogl_ptr = app_plane.GetContext())
		ogl_ptr->ResetElement(control_el);
}

void * Apps::XPic::GetSelectorProc() {
	return FileSelect;
}


UI_64 Apps::XPic::FileSelect(OpenGL::ButtonGrid & b_obj, UI_64 fl_id) {
	UI_64 result(-1), _event(0), pl_id(-1);
	OpenGL::Core * core_ogl(0);

	float color_val[4] = {0.0, 0.0, 0.0, 1.0}, box_vals[12] = {0.0, 0.0, 0.0f, 0.0f, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0};
	double d_val[2] = {-4.0, 11.0};

	OpenGL::APPlane a_plane;
	OpenGL::Button ss_button;

	OpenGL::SliderClip slider_clip;
	OpenGL::SliderX x_slider;

	
	SFSco::Object p_list, temp_obj;

	OpenGL::DecodeRecord de_rec;
	OpenGL::Button fun_but, _button;
	unsigned int fun_gid(-1), lidx(-1);

	if (HMODULE _libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {

		core_ogl = b_obj.GetContext();
	
	// copy folder list with filter
		pl_id = XFiles::BuildPlayList(fl_id, p_list);
	

	// create app_plane, get but id, format path string, add video queu without preview flag


		if (core_ogl) {
			result = core_ogl->CreateAPPlane(a_plane, SourceFeedBack);

			if (result != -1) {
				a_plane.SetEFlags(OpenGL::Element::FlagApp);

				de_rec.flags = OpenGL::DecodeRecord::FlagFile;
				de_rec.fname_id = pl_id;
		
				de_rec.pic_id = 0;
				de_rec.info_text_id = -1;
				de_rec.name_text_id = -1;
				de_rec.program = 0;
		
				de_rec.pic_box[0] = 0.0f;
				de_rec.pic_box[1] = 0.0f;
				de_rec.pic_box[2] = 0.0f;
				de_rec.pic_box[3] = 0.0f;

				de_rec.element_obj = a_plane;



				a_plane.Configure(1, 0xB0000000, 255.0f, 300);
				a_plane.GetRightMargin(color_val);

				box_vals[4] = 100.0f; box_vals[5] = 100.0f; box_vals[6] = 80.0f; box_vals[7] = 200.0f;
				box_vals[8] = 100.0f; box_vals[9] = 200.0f; box_vals[10] = 80.0f; box_vals[11] = 80.0f;

				a_plane.SetCPMargin(box_vals + 4);

				_event = SFSco::Event_CreateDrain(XProc, &a_plane, 0);
				SFSco::Event_SetGate(_event, a_plane, OpenGL::Element::StateNotify);

			// file name label
				de_rec.name_text_id = a_plane.AddControlLabel(fname_cfg);
				if (de_rec.name_text_id != -1) {

					if (a_plane.AddControl(slider_clip, 64, 64, OpenGL::SliderX::HorizontalStretch) != -1) { // C0C0 xFFE0F0FF
						result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypeDouble | OpenGL::SliderX::LabelTypeL, &zoom_labels);
						if (result != -1) {
							x_slider.Configure(FrameSizeFunc, zoom_palette, core_ogl->GetZoomTID());
							x_slider.SetLabelWidth(1, 0x40000000);
							x_slider.SetRange(OpenGL::APPlane::scale_set);									
							x_slider.SetRange(0, 0, 0);
						}

						if (result != -1) {
					// delete button
							if (a_plane.AddControl(ss_button, 64, 64) != -1) {

								delete_but.header.icon_tid = core_ogl->GetSystemTID();
								delete_but.palette.ref_vals[0] = core_ogl->GetWarningTID();
								delete_but.palette.ref_vals[1] = core_ogl->GetDeleteTID();

								ss_button.Configure(delete_but.header, delete_but.palette);

								ss_button.SetEFlags(OpenGL::Element::FlagDualUp);
								ss_button.SetBlink(delete_but.header, 20, 10, 0xFF0000FF, 0xFF00000, 0x001F0000); // F3FF

								reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &delete_but;
								ss_button.SetDefaultHandlers(temp_obj);
						
								_event = SFSco::Event_CreateDrain(FileKill, &ss_button, 0);
								SFSco::Event_SetGate(_event, ss_button, (UI_64)(OpenGL::Element::FlagMouseUp | OpenGL::Element::FlagDualUp));
								ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


						// shot button
								if (a_plane.AddControl(ss_button, 64, 64) != -1) {
									op_but.header.icon_tid = core_ogl->GetSystemTID();
									op_but.palette.ref_vals[1] = core_ogl->GetShotTID();
									ss_button.Configure(op_but.header, op_but.palette);

									reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &op_but;
									ss_button.SetDefaultHandlers(temp_obj);

									_event = SFSco::Event_CreateDrain(SourceShot, &a_plane, 0);
									SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
									ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

								}
							}
						}
					}
				}



// slide show
				result = -1;
				fun_gid = OpenGL::APPlane::AddFunction(fun_but, result, 64.00001f);

				color_val[0] = color_val[1] = color_val[2] = 0.2f; color_val[3] = 1.0f;

				if (fun_gid != -1) {
					fun_but.SetColor(color_val);
				
					fun_but.ConfigureClipButton(0x00020000 | 14109, _libin);

					fun_but.Hide();
							
					MainMenu::SetFunctionHandlers(fun_but);


					_event = SFSco::Event_CreateDrain(&SlideShow, &a_plane, 0);
					SFSco::Event_SetGate(_event, fun_but, (UI_64)OpenGL::Element::FlagMouseUp);
					fun_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);


// next track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00002f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00040000 | 14107, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&NextTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}


// prev track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00003f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00060000 | 14108, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&PrevTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}

// exit
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00004f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(OpenGL::Button::ClipSide | 17118, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&Cancel, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}
				}









				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->fun_but = fun_but;
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list = p_list;
					System::MemoryCopy(&reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->deco_rec, &de_rec, sizeof(OpenGL::DecodeRecord));

					a_plane.Release();
				}


				OpenGL::APPlane::ResetFunction(fun_but);
		
				Pics::Image::Decode(de_rec);

				OpenGL::APPlane::PopApp(a_plane);
			}
		}		
	}

	return result;
}

UI_64 Apps::XPic::SourceFeedBack(OpenGL::Element & screen) {
	OpenGL::Element some_l(screen);
	
// maybe some switch
	some_l.Up().Up().Down().Down().Left().Down().Left().Left();
	reinterpret_cast<OpenGL::SliderX &>(some_l).SetRange(0, 0, screen.GetXTra());


	return 0;
}

UI_64 Apps::XPic::NextTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagNextTrack;
	de_rec.element_obj = a_plane;
	
	Pics::Image::Decode(de_rec);
		
	return 0;
}

UI_64 Apps::XPic::PrevTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagPrevTrack;
	de_rec.element_obj = a_plane;
	
	Pics::Image::Decode(de_rec);

	return 0;
}

UI_64 Apps::XPic::SlideShow(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

// some a_plane adjustment please do


	de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
	de_rec.element_obj = a_plane;
	
	Pics::Image::Decode(de_rec);
	
	return 0;
}

UI_64 Apps::XPic::Cancel(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagDestroy;
	de_rec.element_obj = a_plane;
	
	Pics::Image::Decode(de_rec);

	XFiles::SelectFL();

	return 0;
}

UI_64 Apps::XPic::FileKill(OpenGL::Element & ss_but) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagKill;
	de_rec.element_obj = ss_but;
	de_rec.element_obj.Up().Up();


	ss_but.Down();

	if (delete_but.header.flags & OpenGL::Button::Header::BorderPresent) {
		ss_but.Left();
	}
	
	ss_but.SetEFlags(OpenGL::Element::FlagBlinkNoMore);

	

	Pics::Image::Decode(de_rec);

	return 0;
}


UI_64 Apps::XPic::SourceShot(OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;

	de_rec.element_obj = a_plane;
//	de_rec.element_obj.Down();

	de_rec.flags = OpenGL::DecodeRecord::FlagTakeAShot;

	Pics::Image::Decode(de_rec);

	return result;
}

UI_64 Apps::XPic::XProc(OpenGL::APPlane & a_plane) { // notify handler
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;
	OpenGL::Element control_plane(a_plane);

	control_plane.Down();

	de_rec.element_obj = a_plane;
	

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		result = el_hdr->config.anchor >> 16;
		el_hdr->config.anchor &= 0x0000FFFF;

		a_plane.Release();

		switch (result) {
			case 1:
				de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
			break;
			case 2:
				de_rec.flags = OpenGL::DecodeRecord::FlagReset;
				
				if (control_plane.IsVisible()) {
					control_plane.Show();
					control_plane.SetState((UI_64)OpenGL::Element::StateShow);
				}
			break;
			case 3: // cp show
				ControlPlaneReset(a_plane);
			break;
		}		
	}	

	if (de_rec.flags) Pics::Image::Decode(de_rec);

	return result;
}

UI_64 Apps::XPic::FrameSizeFunc(OpenGL::SliderX & v_sli, UI_64 t_val) {
	UI_64 result(0);

	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagSize;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec._reserved[4] = t_val;

	select_rec.element_obj(v_sli).Up().Up().Up(); // app plane


	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(select_rec.element_obj.Acquire())) {
		result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_sel;

		select_rec.element_obj.Release();
	}
		
	select_rec.element_obj.Down().Left().Down();

	for (;result;result--) {
		select_rec.element_obj.Left();
	}

	Pics::Image::Decode(select_rec);

	return 0;
}

void Apps::XPic::ProgressCircleP(float * motion_t, unsigned int restart_flag) {
	motion_t[12] -= motion_t[-4];
	motion_t[13] -= motion_t[-3];

		

	if (!restart_flag) {
		Calc::MF_Multiply44(progress_transform[0], 4, motion_t, motion_t);

	// x
		progress_transform[1][4] = progress_transform[1][0]*motion_t[-8] - progress_transform[1][1]*motion_t[-7];
		progress_transform[1][5] = progress_transform[1][1]*motion_t[-8] + progress_transform[1][0]*motion_t[-7];
		motion_t[-8] = progress_transform[1][4];
		motion_t[-7] = progress_transform[1][5];



	// y
		progress_transform[1][12] = progress_transform[1][8]*motion_t[-6] - progress_transform[1][9]*motion_t[-5];
		progress_transform[1][13] = progress_transform[1][9]*motion_t[-6] + progress_transform[1][8]*motion_t[-5];
		motion_t[-6] = progress_transform[1][12];
		motion_t[-5] = progress_transform[1][13];


	} else {
		progress_transform[1][7] = System::Gen32();
		progress_transform[1][7] *= 0.8381903171539306640625e-7f;
		motion_t[-8] = Calc::FloatCos(progress_transform[1][7]);
		motion_t[-7] = Calc::FloatSin(progress_transform[1][7]);

		progress_transform[1][14] = System::Gen32();
		progress_transform[1][14] *= 0.8381903171539306640625e-7f;
		motion_t[-6] = Calc::FloatCos(progress_transform[1][14]);
		motion_t[-5] = Calc::FloatSin(progress_transform[1][14]);

		motion_t[0] = 1.0f; motion_t[1] = 0.0f; motion_t[2] = 0.0f; motion_t[3] = 0.0f;
		motion_t[4] = 0.0f; motion_t[5] = 1.0f; motion_t[6] = 0.0f; motion_t[7] = 0.0f;
		motion_t[8] = 0.0f; motion_t[9] = 0.0f; motion_t[10] = 1.0f; motion_t[11] = 0.0f;
		motion_t[12] = 0.0f; motion_t[13] = 0.0f; motion_t[14] = 0.0f; motion_t[15] = 1.0f;

	}

	motion_t[-4] = 78.0f*(1.0f + motion_t[-8]);
	motion_t[-3] = 104.0f*(1.0f + motion_t[-5]);

	motion_t[12] += motion_t[-4];
	motion_t[13] += motion_t[-3];
}


