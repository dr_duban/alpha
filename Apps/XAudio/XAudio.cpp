/*
** safe-fail audio player
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Apps\XAudio.h"

#include "System\SysUtils.h"
#include "Apps\XFiles.h"
#include "Media\Source.h"

#include "Audio\AudioRenderer.h"
#include "Apps\MainMenu.h"

#include "Math\CalcSpecial.h"

#define VIDEO_SOURCE_LIST	1
#define AUDIO_SOURCE_LIST	2



__declspec(align(16)) float Apps::XAudio::progress_transform[2][16];

const wchar_t Apps::XAudio::supported_xt_list[] = {	
													L'M', L'P', L'3', L'\0',
													L'M', L'P', L'2', L'\0',
													L'M', L'P', L'1', L'\0',
			
												};


			

Apps::XAudio::XAudio() {


}

Apps::XAudio::~XAudio() {


}


void Apps::XAudio::ResetCP(OpenGL::Element & a_plane) {
	float some_vals[16] = {8.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	OpenGL::Element cp_elem(a_plane);
	unsigned int t_val(0);

	cp_elem.Down().Left(); // screen collection
	cp_elem.GetScaleVector(some_vals + 12);
	cp_elem.GetRightCorner(some_vals + 4);
	cp_elem.Down();
	cp_elem.GetRightMargin(some_vals + 8);
	
	cp_elem.Up();

	cp_elem.Right().Down();

	// name text

	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 50.0f;
	some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 50.0f;
	cp_elem.RelocateXY(some_vals);
	reinterpret_cast<OpenGL::Text &>(cp_elem).SetMCCount(0.06f*some_vals[6]);

	cp_elem.Left();

// slider clip
	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4];
	some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5] + 150.0f;
	some_vals[2] = some_vals[10]*some_vals[12];
	some_vals[3] = some_vals[11]*some_vals[13] + 150.0f;

	reinterpret_cast<OpenGL::SliderClip &>(cp_elem).CoordsReset(some_vals);
	cp_elem.Left();


// delete button
	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 75.0f;
	some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 20.0f;

	cp_elem.RelocateXY(some_vals);
	cp_elem.Left();

// rec button
	some_vals[1] += 150.0f;
	if ((some_vals[11]*some_vals[13]) < 280.0f) some_vals[1] += some_vals[11]*some_vals[13] - 300.0f;
	some_vals[15] = some_vals[1];

	cp_elem.Hide();
	cp_elem.RelocateXY(some_vals);
	cp_elem.Left();


// shot butoon
	some_vals[1] +=  100.0f;
	cp_elem.Show();
	cp_elem.RelocateXY(some_vals);
	cp_elem.Left();



// pause button
	some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5];	cp_elem.Show();
	cp_elem.RelocateXY(some_vals);
	cp_elem.Left();

// play button
	some_vals[1] += 70.0f;

	cp_elem.Show();
	cp_elem.RelocateXY(some_vals);
	cp_elem.Left();

	


// signal clip
	some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4];
	some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5] + 40.0f;
	
	reinterpret_cast<OpenGL::BoardClip &>(cp_elem).CoordsReset(some_vals);

	cp_elem.Left();
		
	cp_elem.Up();


	if (OpenGL::Core * ogl_core = a_plane.GetContext()) ogl_core->ResetElement(cp_elem);
}

void Apps::XAudio::Change() {


}


UI_64 Apps::XAudio::AddFilter(SFSco::IGLApp *) {
	return 0;
}

UI_64 Apps::XAudio::CreatePreview(UI_64 path_id, SFSco::Object & button_obj, UI_64 icon_id, UI_64 text_id, UI_64 name_id, unsigned int reset_flag) {
	UI_64 result(1);
	result <<= 63;

	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagPreview | OpenGL::DecodeRecord::FlagFile | reset_flag;

	de_rec.fname_id = path_id;	
	de_rec.pic_id = icon_id;


	de_rec.info_text_id = text_id;
	de_rec.name_text_id = name_id;
	de_rec.program = 0;


	de_rec.pic_box[0] = 2.0f;
	de_rec.pic_box[1] = 32.0f;
	de_rec.pic_box[2] = 320.0f;
	de_rec.pic_box[3] = 240.0f;
		
	de_rec.element_obj = reinterpret_cast<OpenGL::Element &>(button_obj);

	Media::Program::Decode(de_rec);
	
	return result;
}

void * Apps::XAudio::GetSelectorProc() {
	return FileSelect;
}


UI_64 Apps::XAudio::ConfigurePreview(SFSco::Object & bgrid_el) {
	UI_64 result(0);
	float m_vals[4] = {10.0f, 10.0f, 0.0f, -1.0f};
	OpenGL::ButtonGrid prew_grid;
	unsigned int lidx(0);

	prew_grid(bgrid_el);
	prew_grid.ConfigureIconTrajectory(ProgressCircleP);

	prew_grid.SetMargin(m_vals);
	prew_grid.ConfigureCell(FileSelect, 324.0f, 296.0f, 0x80000000, 1.1f);
	prew_grid.ConfigureIcon(0x01400002, 0x00F00020, LAYERED_TEXTURE, OpenGL::LeftBottomFront);
	

	prew_grid.AddPaletteEntry(video_but_palette);

	lidx = prew_grid.ConfigureTextLine(0xFFBAEEA4, 0, 0x00100000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 190.0f, 286.0f, 13.5f, -1, L"Calibril"); //reinterpret_cast<const wchar_t *>(mmenu->GetTextFont())); // 777799

	lidx = prew_grid.ConfigureTextLine(0xFFF0E080, 0, 0x00200000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 25.0f, 24.0f, 15.5f, -1, L"Calibril");


	return result;
}


UI_64 Apps::XAudio::FileSelect(OpenGL::ButtonGrid & b_obj, UI_64 fl_id) {
	UI_64 result(-1), _event(0), pl_id(-1);
	OpenGL::Core * core_ogl(0);

	float color_val[4] = {0.2f, 0.2f, 0.2f, 1.0f}, box_vals[12] = {0.0, 0.0, 0.0f, 0.0f, 0.0, 0.0, 0.0, 0.0, 1.0f, 0.0, 0.0, 0.0};

	OpenGL::APPlane a_plane;
	OpenGL::SliderClip slider_clip;
	OpenGL::SliderX x_slider;

	OpenGL::Button ss_button;
	OpenGL::ListBoxClip lb_clip;
	OpenGL::ListBoxX l_box;
	
	OpenGL::BoardClip signal_clip;
	OpenGL::D2Board l_board;

	SFSco::Object p_list, temp_obj;

	OpenGL::DecodeRecord de_rec;
	OpenGL::Button fun_but, _button;
	unsigned int fun_gid(-1), lidx(-1);

	if (HMODULE _libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {

		core_ogl = b_obj.GetContext();
	
	// copy folder list with filter
		pl_id = XFiles::BuildPlayList(fl_id, p_list);
	

	// create app_plane, get but id, format path string, add video queu without preview flag


		if (core_ogl) {
			result = core_ogl->CreateAPPlane(a_plane); 

			if (result != -1) {
				a_plane.SetEFlags(OpenGL::Element::FlagApp);

				de_rec.flags = OpenGL::DecodeRecord::FlagFile;
				de_rec.fname_id = pl_id;
				
		
				de_rec.pic_id = 0;
				
				de_rec.info_text_id = 0;
				de_rec.name_text_id = 0;
				de_rec.program = 0;
		
				de_rec.pic_box[0] = 0.0f;
				de_rec.pic_box[1] = 0.0f;
				de_rec.pic_box[2] = 0.0f;
				de_rec.pic_box[3] = 0.0f;

				de_rec.element_obj = a_plane;


				a_plane.Configure(1, 0xB0000000, 255.0f, 300);
				
				box_vals[4] = 100.0f; box_vals[5] = 100.0f; box_vals[6] = 100.0f; box_vals[7] = 350.0f;
				box_vals[8] = 100.0f; box_vals[9] = 100.0f; box_vals[10] = 100.0f; box_vals[11] = 350.0f;

				a_plane.SetCPMargin(box_vals + 4);


				_event = SFSco::Event_CreateDrain(XProc, &a_plane, 0);
				SFSco::Event_SetGate(_event, a_plane, OpenGL::Element::StateNotify);

				de_rec.name_text_id = a_plane.AddControlLabel(fname_cfg);
				if (de_rec.name_text_id != -1) {

				// slider clip
					if (a_plane.AddControl(slider_clip, 64, 64, OpenGL::SliderX::HorizontalStretch) != -1) { // C0C0 xFFE0F0FF

						result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypeTime | OpenGL::SliderX::LabelTypeSS, position_labels, 250.0f);
						if (result != -1) {
						// position slider
							x_slider.Configure(AudioPositionFunc, position_palette); // , 0xFFB4EB38, 0xFFB4EB38
							x_slider.SetRange(0, 1111, 0);
				
						// volume slider
							result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypePcnt | OpenGL::SliderX::LabelTypeL, &volume_labels);
							if (result != -1) {
								x_slider.Configure(VolumePositionFunc, volume_palette, core_ogl->GetVolTID());
							
					//		x_slider.SetLabelWidth(1, 0x40000000);
								x_slider.SetRange(Audio::DefaultRenderer::volume_set);
								x_slider.SetRange(0, 0, SFSco::program_cfg.audio_default_volume);

							}
						}

						if (result != -1) {
						// delete button
							if (a_plane.AddControl(ss_button, 64, 64) != -1) {
								delete_but.header.icon_tid = core_ogl->GetSystemTID();
								delete_but.palette.ref_vals[0] = core_ogl->GetWarningTID();
								delete_but.palette.ref_vals[1] = core_ogl->GetDeleteTID();

								ss_button.Configure(delete_but.header, delete_but.palette);
								ss_button.SetEFlags(OpenGL::Element::FlagDualUp);
								ss_button.SetBlink(delete_but.header, 20, 10, 0xFF0000FF, 0xFF00000, 0x001F0000); // F3FF

								reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &delete_but;
								ss_button.SetDefaultHandlers(temp_obj);

								ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

							// rec button
								if (a_plane.AddControl(ss_button, 64, 64) != -1) {
									delete_but.header.icon_tid = core_ogl->GetSystemTID();
									delete_but.palette.ref_vals[1] = core_ogl->GetRecTID();
									ss_button.Configure(delete_but.header, delete_but.palette);

									ss_button.SetDefaultHandlers(temp_obj);


								// shot button
									if (a_plane.AddControl(ss_button, 64, 64) != -1) {
										control_but.header.icon_tid = core_ogl->GetSystemTID();
										control_but.palette.ref_vals[1] = core_ogl->GetShotTID();
										ss_button.Configure(control_but.header, control_but.palette);
													
										reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &control_but;
										ss_button.SetDefaultHandlers(temp_obj);

										_event = SFSco::Event_CreateDrain(SourceShot, &a_plane, 0);
										SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
										ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);



									// pause button
										if (a_plane.AddControl(ss_button, 64, 64) != -1) {
											control_but.header.icon_tid = core_ogl->GetSystemTID();
											control_but.palette.ref_vals[1] = core_ogl->GetPauseTID();
											ss_button.Configure(control_but.header, control_but.palette);
											
											ss_button.SetDefaultHandlers(temp_obj);

											_event = SFSco::Event_CreateDrain(PauseProgram, &a_plane, 0);
											SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
											ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

										// play button
											if (a_plane.AddControl(ss_button, 64, 64) != -1) {
												control_but.header.icon_tid = core_ogl->GetSystemTID();
												control_but.palette.ref_vals[1] = core_ogl->GetPlayTID();
												ss_button.Configure(control_but.header, control_but.palette);
													
												ss_button.SetDefaultHandlers(temp_obj);

												_event = SFSco::Event_CreateDrain(PlayProgram, &a_plane, 0);
												SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
												ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


											// signal clip
												if (a_plane.AddControl(signal_clip, 150, 80) != -1) {
													if (signal_clip.Configure(signal_text, 3) != -1) {

														box_vals[0] = 0.0f;
														box_vals[1] = 0.5f;
														box_vals[2] = 1.0f;
														box_vals[3] = 1.0f;

														box_vals[4] = 1.2208521548040532291539494567208e-4f;
														box_vals[5] = 0.5f;
														box_vals[6] = 1.0f;
														box_vals[7] = 1.0f;
													
														for (unsigned int ci(0);ci<8;ci++) {
															if (signal_clip.CreateD2Board(l_board, plot_cfg) != -1) {
																l_board.SetDataScale(box_vals + 4);
																l_board.SetDataOffset(box_vals);
																// some configur probably
															}
														}
													}
												}
											}
										}											
									}
								}						
							}
						}
					}
				}

				
// slide show
				result = -1;
				fun_gid = OpenGL::APPlane::AddFunction(fun_but, result, 64.00001f);

//				color_val[0] = color_val[1] = color_val[2] = 0.2f; color_val[3] = 1.0f;

				if (fun_gid != -1) {
					fun_but.SetColor(color_val);
				
					fun_but.ConfigureClipButton(0x00020000 | 14109, _libin);

					fun_but.Hide();
							
					MainMenu::SetFunctionHandlers(fun_but);


					_event = SFSco::Event_CreateDrain(&SlideShow, &a_plane, 0);
					SFSco::Event_SetGate(_event, fun_but, (UI_64)OpenGL::Element::FlagMouseUp);
					fun_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);


// next track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00002f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00040000 | 14107, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&NextTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}


// prev track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00003f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00060000 | 14108, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&PrevTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}

// exit
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00004f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(OpenGL::Button::ClipSide | 17118, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&Cancel, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}
				}









				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->fun_but = fun_but;
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list = p_list;
					System::MemoryCopy(&reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->deco_rec, &de_rec, sizeof(OpenGL::DecodeRecord));

					a_plane.Release();
				}


				OpenGL::APPlane::ResetFunction(fun_but);
		
				Media::Program::Decode(de_rec);

				OpenGL::APPlane::PopApp(a_plane);

				core_ogl->SetFBack(a_plane, ProgramPositionUpdate);
			}
		}		
	}

	return result;
}

UI_64 Apps::XAudio::ProgramPositionUpdate(OpenGL::Element & a_plane) {
	OpenGL::Element slider(a_plane);
	
	slider.Down().Down().Left().Down();
	reinterpret_cast<OpenGL::SliderX &>(slider).SetValue(a_plane.GetXTra());

	return 0;
}

UI_64 Apps::XAudio::PauseProgram(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagStop;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);


	return 0;
}

UI_64 Apps::XAudio::PlayProgram(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagStart;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);


	return 0;
}


UI_64 Apps::XAudio::NextTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagNextTrack;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);
		
	return 0;
}

UI_64 Apps::XAudio::PrevTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagPrevTrack;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);

	return 0;
}

UI_64 Apps::XAudio::SlideShow(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

// some a_plane adjustment please do


	de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);
	
	return 0;
}

UI_64 Apps::XAudio::Cancel(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagDestroy;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);

	XFiles::SelectFL();

	return 0;
}

const wchar_t * Apps::XAudio::GetSupportedList() {
	return supported_xt_list;
}

unsigned int Apps::XAudio::GetSupportedCount() {
	return (sizeof(supported_xt_list)>>3);
}

UI_64 Apps::XAudio::Test(const wchar_t * sext) {
	return 0;
}



UI_64 Apps::XAudio::AudioPositionFunc(OpenGL::SliderX & v_sli, UI_64 t_val) {	
	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagJump; // | OpenGL::DecodeRecord::FlagRefresh;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec.target_time = t_val;

	select_rec.element_obj(v_sli).Up().Up().Up(); // app plane

	Media::Program::Decode(select_rec);

	return 0;
}

UI_64 Apps::XAudio::VolumePositionFunc(OpenGL::SliderX & a_sli, UI_64 t_val) {
	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagMasterVolume;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec.target_time = t_val;

	select_rec.element_obj(a_sli).Up().Up().Up(); // app plane

	Media::Program::Decode(select_rec);


	return 1;
}

UI_64 Apps::XAudio::XProc(OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;
	OpenGL::Element control_plane(a_plane);

	control_plane.Down();

	de_rec.element_obj = a_plane;
	

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		result = el_hdr->config.anchor >> 16;
		el_hdr->config.anchor &= 0x0000FFFF;

		a_plane.Release();

		switch (result) {
			case 1:
				de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
			break;
			case 2:
				de_rec.flags = OpenGL::DecodeRecord::FlagReset;
				
				if (control_plane.IsVisible()) {
					control_plane.Show();
					control_plane.SetState((UI_64)OpenGL::Element::StateShow);
				}

				// go for slider update
				result = 0;
				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
					result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->deco_rec.target_time;

					a_plane.Release();
				}

				if (result) {
					control_plane.Down().Left().Down();
					reinterpret_cast<OpenGL::SliderX &>(control_plane).SetRange(0, result, 0);
				
					control_plane.Left();
					reinterpret_cast<OpenGL::SliderX &>(control_plane).SetRange(0, 0, SFSco::program_cfg.audio_default_volume);
				}

			break;
			case 3: // cp show
				ResetCP(a_plane);
			break;
		}		
	}	

	if (de_rec.flags) Media::Program::Decode(de_rec);

	return result;
}

void Apps::XAudio::ProgressCircleP(float * motion_t, unsigned int restart_flag) {
	motion_t[12] -= motion_t[-4];
	motion_t[13] -= motion_t[-3];

		

	if (!restart_flag) {
		Calc::MF_Multiply44(progress_transform[0], 4, motion_t, motion_t);

	// x
		progress_transform[1][4] = progress_transform[1][0]*motion_t[-8] - progress_transform[1][1]*motion_t[-7];
		progress_transform[1][5] = progress_transform[1][1]*motion_t[-8] + progress_transform[1][0]*motion_t[-7];
		motion_t[-8] = progress_transform[1][4];
		motion_t[-7] = progress_transform[1][5];



	// y
		progress_transform[1][12] = progress_transform[1][8]*motion_t[-6] - progress_transform[1][9]*motion_t[-5];
		progress_transform[1][13] = progress_transform[1][9]*motion_t[-6] + progress_transform[1][8]*motion_t[-5];
		motion_t[-6] = progress_transform[1][12];
		motion_t[-5] = progress_transform[1][13];


	} else {
		progress_transform[1][7] = System::Gen32();
		progress_transform[1][7] *= 0.8381903171539306640625e-7f;
		motion_t[-8] = Calc::FloatCos(progress_transform[1][7]);
		motion_t[-7] = Calc::FloatSin(progress_transform[1][7]);

		progress_transform[1][14] = System::Gen32();
		progress_transform[1][14] *= 0.8381903171539306640625e-7f;
		motion_t[-6] = Calc::FloatCos(progress_transform[1][14]);
		motion_t[-5] = Calc::FloatSin(progress_transform[1][14]);

		motion_t[0] = 1.0f; motion_t[1] = 0.0f; motion_t[2] = 0.0f; motion_t[3] = 0.0f;
		motion_t[4] = 0.0f; motion_t[5] = 1.0f; motion_t[6] = 0.0f; motion_t[7] = 0.0f;
		motion_t[8] = 0.0f; motion_t[9] = 0.0f; motion_t[10] = 1.0f; motion_t[11] = 0.0f;
		motion_t[12] = 0.0f; motion_t[13] = 0.0f; motion_t[14] = 0.0f; motion_t[15] = 1.0f;

	}

	motion_t[-4] = 78.0f*(1.0f + motion_t[-8]);
	motion_t[-3] = 64.0f*(1.0f + motion_t[-5]);

	motion_t[12] += motion_t[-4];
	motion_t[13] += motion_t[-3];
}

UI_64 Apps::XAudio::SourceShot(OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_sel;

		a_plane.Release();
	}

	de_rec.element_obj = a_plane;
	de_rec.element_obj.Down().Left().Down();

	de_rec.flags = OpenGL::DecodeRecord::FlagTakeAShot;

	for (;result;result--) {
		de_rec.element_obj.Left();
	}

	Media::Program::Decode(de_rec);

	return result;
}

