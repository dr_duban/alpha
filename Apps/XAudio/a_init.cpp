/*
** safe-fail audio player
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Apps\XAudio.h"
#include "Apps\MainMenu.h"

#include "System\SysUtils.h"
#include "Math\CalcSpecial.h"


OpenGL::Button::Config Apps::XAudio::control_but, Apps::XAudio::delete_but;

OpenGL::Element::LabelCfg Apps::XAudio::position_labels[2];
OpenGL::Element::LabelCfg Apps::XAudio::volume_labels;

OpenGL::Button::PaletteEntry Apps::XAudio::video_but_palette, Apps::XAudio::position_palette[2], Apps::XAudio::volume_palette[2];

OpenGL::Element::LabelCfg Apps::XAudio::fname_cfg;

OpenGL::Element::LabelCfg Apps::XAudio::signal_text[4];

OpenGL::D2Board::PlotCfg Apps::XAudio::plot_cfg;




UI_64 Apps::XAudio::Initialize(SFSco::IOpenGL ** gl_ptr, void *, void *) {	
	UI_64 result(0);
	OpenGL::Core * core_ptr(0);

	if (gl_ptr) core_ptr = reinterpret_cast<OpenGL::Core *>(gl_ptr[0]);

	System::MemoryFill(&video_but_palette, sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(video_but_palette.color, 0xFF262626);
	OpenGL::Core::ColorConvert(video_but_palette.h_color, 0xFF202020);
	OpenGL::Core::ColorConvert(video_but_palette.d_color, 0xFF262626);

	OpenGL::Core::ColorConvert(video_but_palette.b_color, 0x05FFFFFF);
	OpenGL::Core::ColorConvert(video_but_palette.hb_color, 0x809DCCEF);
	OpenGL::Core::ColorConvert(video_but_palette.db_color, 0x05FFFFFF);


	System::MemoryFill(position_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);
	OpenGL::Core::ColorConvert(position_palette[0].color, 0xFF000000);
	OpenGL::Core::ColorConvert(position_palette[0].h_color, 0xFF202020);
	OpenGL::Core::ColorConvert(position_palette[0].d_color, 0xFF662626);
	
	OpenGL::Core::ColorConvert(position_palette[0].i_color, 0xFF202020); // progress color
	OpenGL::Core::ColorConvert(position_palette[0].hi_color, 0xFF404040);
	OpenGL::Core::ColorConvert(position_palette[0].di_color, 0xFF662626);
	

	position_palette[1].ico_xcol[0] = 0x73DF; position_palette[1].ico_xcol[1] = 0x7F0F; position_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(position_palette[1].color, 0x010000FF);
	OpenGL::Core::ColorConvert(position_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(position_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(position_palette[1].b_color, 0xFFB4EB38); // progress bar
	OpenGL::Core::ColorConvert(position_palette[1].hb_color, 0xFFB4EB38); // progress bar high
	OpenGL::Core::ColorConvert(position_palette[1].db_color, 0xFFB4EB38); // progress rect

	OpenGL::Core::ColorConvert(position_palette[1].i_color, 0xFF000000); // inc/dec icon color
	OpenGL::Core::ColorConvert(position_palette[1].hi_color, 0xFF202020);
	OpenGL::Core::ColorConvert(position_palette[1].di_color, 0xFFA8FFE0);



	System::MemoryFill(volume_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);
	volume_palette[0].ico_xcol[0] = 0x73DF; volume_palette[0].ico_xcol[1] = 0x7F0F; volume_palette[0].ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(volume_palette[0].color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(volume_palette[0].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[0].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(volume_palette[0].i_color, 0x00000020); // progress color
	OpenGL::Core::ColorConvert(volume_palette[0].hi_color, 0x00FFFFFF);
	OpenGL::Core::ColorConvert(volume_palette[0].di_color, 0x00000026);


	volume_palette[0].ib_color[2] = volume_palette[0].ib_color[3] = 64.0f;
	

	volume_palette[1].ico_xcol[0] = 0x73DF; volume_palette[1].ico_xcol[1] = 0x7F0F; volume_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(volume_palette[1].color, 0x010000FF); // buttons color
	OpenGL::Core::ColorConvert(volume_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(volume_palette[1].b_color, 0xFFEBB438); // progress bar , 0xFFEBB438, 
	OpenGL::Core::ColorConvert(volume_palette[1].hb_color, 0xFFEBB438); // progress bar high
	OpenGL::Core::ColorConvert(volume_palette[1].db_color, 0xFFEBB438); // progress rect

	OpenGL::Core::ColorConvert(volume_palette[1].i_color, 0x010000FF); // inc/dec icon color
	OpenGL::Core::ColorConvert(volume_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[1].di_color, 0xFFA8E0FF);



	System::MemoryFill(&control_but, sizeof(OpenGL::Button::Config), 0);
	control_but.header.flags = OpenGL::Button::Header::IconPresent;
	control_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	control_but.header.icon_box[0] = 0.0f; control_but.header.icon_box[1] = 0.0f; control_but.header.icon_box[2] = 64.0f; control_but.header.icon_box[3] = 64.0f;

	control_but.palette.ico_xcol[0] = 0x73DF; control_but.palette.ico_xcol[1] = 0x7F0F; control_but.palette.ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(control_but.palette.i_color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(control_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(control_but.palette.di_color, 0xFFA8E0FF);
	


	System::MemoryFill(&delete_but, sizeof(OpenGL::Button::Config), 0);	
	delete_but.header.flags = OpenGL::Button::Header::IconPresent;
	delete_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	delete_but.header.icon_box[0] = 0.0f; delete_but.header.icon_box[1] = 0.0f; delete_but.header.icon_box[2] = 64.0f; delete_but.header.icon_box[3] = 64.0f;
	
	delete_but.palette.ico_xcol[0] = 0x0014; delete_but.palette.ico_xcol[1] = 0x001F; delete_but.palette.ico_xcol[2] = 0x73DF;
	OpenGL::Core::ColorConvert(delete_but.palette.i_color, 0x010000FF);
	OpenGL::Core::ColorConvert(delete_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(delete_but.palette.di_color, 0xFF0000FF);






	System::MemoryFill(position_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(position_labels[0].c_color, 0xFF80FF80);	
		
	position_labels[0].f_size = 15.0f;
	position_labels[0].font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	position_labels[0].font_style = OpenGL::Text::Mono;
	position_labels[0].a_type = OpenGL::None;
	position_labels[0].max_char_count = 16;


	OpenGL::Core::ColorConvert(position_labels[1].c_color, 0xFF4C33B2);
			
	position_labels[1].f_size = 11.5f;
	position_labels[1].font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	position_labels[1].font_style = OpenGL::Text::Mono;
	position_labels[1].a_type = OpenGL::None;
	position_labels[1].max_char_count = 16;



	OpenGL::Core::ColorConvert(volume_labels.c_color, 0xFFFFFF80);
		
	volume_labels.f_size = 20.5f;
	volume_labels.font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	volume_labels.font_style = OpenGL::Text::Mono;
	volume_labels.a_type = OpenGL::None;
	volume_labels.max_char_count = 6;






	System::MemoryFill(&fname_cfg, sizeof(OpenGL::Element::LabelCfg), 0);
	fname_cfg.offset[0] = 24.0f;
	fname_cfg.offset[1] = 35.0f;

	OpenGL::Core::ColorConvert(fname_cfg.c_color, 0xFFCAFEB4);

	fname_cfg.f_size = 25.0f;
	fname_cfg.font_id = -1;
	fname_cfg.font_style = OpenGL::Text::Regular;
	fname_cfg.a_type = OpenGL::LeftBottomFront;
	fname_cfg.max_char_count = 256;





	System::MemoryFill(signal_text, 4*sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(signal_text[0].c_color, 0xFF908040);	//80FF80
	signal_text[0].f_size = 21.0f;
	signal_text[0].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[0].font_style = OpenGL::Text::Regular;
	signal_text[0].a_type = OpenGL::None;
	signal_text[0].max_char_count = 8;

	OpenGL::Core::ColorConvert(signal_text[1].c_color, 0xFF908040);	// 80FF80
	signal_text[1].f_size = 21.0f;
	signal_text[1].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[1].font_style = OpenGL::Text::Regular;
	signal_text[1].a_type = OpenGL::None;
	signal_text[1].max_char_count = 12;

	OpenGL::Core::ColorConvert(signal_text[2].c_color, 0xFF908040);	// 80FF80
	signal_text[2].f_size = 21.0f;
	signal_text[2].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[2].font_style = OpenGL::Text::Regular;
	signal_text[2].a_type = OpenGL::None;
	signal_text[2].max_char_count = 12;

	OpenGL::Core::ColorConvert(signal_text[3].c_color, 0xFF908040);	// 80FF80
	signal_text[3].f_size = 21.0f;
	signal_text[3].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[3].font_style = OpenGL::Text::Regular;
	signal_text[3].a_type = OpenGL::None;
	signal_text[3].max_char_count = 8;




	System::MemoryFill(&plot_cfg, sizeof(OpenGL::D2Board::PlotCfg), 0);

	plot_cfg.flags = OpenGL::D2Board::FlagBorder | OpenGL::D2Board::FlagPlotTypeLine | OpenGL::D2Board::FlagCaption;
	
	OpenGL::Core::ColorConvert(plot_cfg.caption_label.c_color, 0xFFE7D272);
	plot_cfg.caption_label.offset[0] = 10.0f;
	plot_cfg.caption_label.offset[1] = 0.0f;
	plot_cfg.caption_label.f_size = 25.0f;
	plot_cfg.caption_label.font_id = MainMenu::GetTextFont(); //MainMenu::GetTimeFont(); // 
	plot_cfg.caption_label.font_style = OpenGL::Text::Regular;
	plot_cfg.caption_label.a_type = OpenGL::None;
	plot_cfg.caption_label.max_char_count = 4;


	OpenGL::Core::ColorConvert(plot_cfg.line_color, 0xFF0000FF);
	OpenGL::Core::ColorConvert(plot_cfg.border_color, 0x40E7D272);
	plot_cfg.capacity = 8192;
	
	plot_cfg.flat_colors[0] = 0xFFFF0000;
	plot_cfg.flat_colors[1] = 0xFF00FFFF;
	plot_cfg.flat_colors[2] = 0xFF000000;
	plot_cfg.flat_colors[3] = 0xFF000000;



	System::MemoryFill_SSE3(progress_transform, 2*16*sizeof(float), 0, 0);

	progress_transform[0][0] = Calc::FloatCos(7.9f);
	progress_transform[0][1] = Calc::FloatSin(7.9f);

	progress_transform[0][4] = -progress_transform[0][1];
	progress_transform[0][5] = progress_transform[0][0];

	progress_transform[0][10] = progress_transform[0][15] = 1.0f;

	progress_transform[0][12] = 20.0f*(1.0f - progress_transform[0][0] + progress_transform[0][1]);
	progress_transform[0][13] = 20.0f*(1.0f - progress_transform[0][0] - progress_transform[0][1]);



	progress_transform[1][0] = Calc::FloatCos(1.4f);
	progress_transform[1][1] = Calc::FloatSin(1.4f);

	progress_transform[1][8] = Calc::FloatCos(3.15f);
	progress_transform[1][9] = Calc::FloatSin(3.15f);





	return result;
}


UI_64 Apps::XAudio::Finalize() {
	UI_64 result(0);
	



	return result;
}

