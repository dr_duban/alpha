/*
** safe-fail file browser
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Apps\XFiles.h"
#include "Apps\MainMenu.h"

#include "System\SysUtils.h"
#include "Pics\Image.h"

#include "Math\CalcSpecial.h"

#include "Media\Capture.h"
#include "Apps\XVideo.h"

#include <LM.h>

#define HARD_DRIVE_TYPE			0x00000000
#define REMOVABLE_TYPE			0x00000001
#define CD_DRIVE_TYPE			0x00000002

#define SERVICE_TYPE			0x00000004
#define CAPTURE_TYPE			0x00000008

#define DRIVE_TYPE				0x00000080
#define SERVER_TYPE				0x00000040
#define SHARE_TYPE				0x00000020


Apps::XFiles * Apps::XFiles::_instance = 0;

SFSco::Object Apps::XFiles::extention_list;
SFSco::Object Apps::XFiles::extention_map;

unsigned int Apps::XFiles::ext_runner = 0;
unsigned int Apps::XFiles::ext_top = 0;

unsigned int Apps::XFiles::xcolor_state[4] = {3, 0, 1, 0};
unsigned int Apps::XFiles::xcolor_selector = 0;

unsigned int Apps::XFiles::progress_tid = 0;
unsigned int Apps::XFiles::preview_progress_tid = 0;

Apps::XFiles::AppRecord Apps::XFiles::app_record[256];

__declspec(align(16)) float Apps::XFiles::progress_transform[2][16];

Apps::XFiles::XFiles() : core_ogl(0), _libin(0), current_path_pos(0), current_path_type(PathTypeService), page_element_count(0), _files_top(0), _files_runner(0), _path_top(0), _path_runner(0), texture_320(0), t320_runner(0), app_count(1), current_app(0), function_group_id(0), menu_event(-1) {
	right_margin[0] = right_margin[1] = right_margin[2] = right_margin[3] = 0.0f;
}

Apps::XFiles::~XFiles() {
	
}

unsigned int Apps::XFiles::GetPreviewTID() {
	return preview_progress_tid;
}

void * Apps::XFiles::GetSelectorProc() {
	return 0;
}

unsigned int Apps::XFiles::CalcXColor() {
	unsigned int result(1);

	if (++xcolor_selector>=3) xcolor_selector = 0;

	result ^= ((xcolor_state[xcolor_selector] & 4) >> 2);
	result ^= ((xcolor_state[xcolor_selector] & 16) >> 4);

	xcolor_state[xcolor_selector] <<= 1;
	xcolor_state[xcolor_selector] &= 31;
	xcolor_state[xcolor_selector] |= result;

	if ((xcolor_state[0] == xcolor_state[1]) && (xcolor_state[1] == xcolor_state[2])) {
		result = 1;
		result ^= ((xcolor_state[xcolor_selector] & 4) >> 2);
		result ^= ((xcolor_state[xcolor_selector] & 16) >> 4);

		xcolor_state[xcolor_selector] <<= 1;
		xcolor_state[xcolor_selector] &= 31;
		xcolor_state[xcolor_selector] |= result;
	}

	result = 0xFF404040 + ((xcolor_state[0] << 2) | (xcolor_state[1] << 10) | (xcolor_state[2] << 18));

	return result;
}


UI_64 Apps::XFiles::Initialize(SFSco::IOpenGL ** glctx, void * mm_ptr, void * _ptr) {
	MainMenu * mmenu = reinterpret_cast<MainMenu *>(mm_ptr);
	float color_val[4] = {0.1f, 0.1f, 0.1f, 1.0f}, box_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 10.0f, 5.0f, 0.0f, -1.0f};

	UI_64 result(2), _event(0);
	unsigned int lidx(0);
	OpenGL::Element pic_el0, pic_el1;
	OpenGL::Button _button, fun_but;//, zbutton;
	OpenGL::AttributeArray a_arr;
	OpenGL::Element::LabelCfg la_cfg;
	
	if (!glctx) return -52;



	if (!core_ogl)
	if (core_ogl = reinterpret_cast<OpenGL::Core *>(*glctx)) {
		_instance = this;

		System::MemoryFill_SSE3(progress_transform, 2*16*sizeof(float), 0, 0);
		progress_transform[0][0] = progress_transform[0][5] = progress_transform[0][10] = progress_transform[0][15] = 1.0f;
		progress_transform[1][0] = progress_transform[1][5] = progress_transform[1][10] = progress_transform[1][15] = 1.0f;

		System::MemoryFill(&la_cfg, sizeof(OpenGL::Element::LabelCfg), 0);
		System::MemoryFill(app_record, 256*sizeof(AppRecord), 0);

		if (core_ogl->CreateClip(app_clip, 0, 1) != -1) {
			app_clip.SetAnchorMode(OpenGL::LeftBottomFront);
			// configure app clip
			
			box_vals[0] = 256.0f; box_vals[1] = 65.0f;
			app_clip.Configure(0xFF303030, box_vals);
			app_clip.ResetDragOp((UI_64)(OpenGL::Element::FlagX0Drag | OpenGL::Element::FlagX1Drag | OpenGL::Element::FlagY0Drag | OpenGL::Element::FlagY1Drag | OpenGL::Element::FlagXYDrag));
			app_clip.Hide();

			app_clip.GetRightMargin(la_cfg.dc_color);
			la_cfg.dc_color[0] = 0.5f*(la_cfg.dc_color[2] - 500.0f);
			la_cfg.dc_color[1] = 0.5f*(la_cfg.dc_color[3] - 500.0f);
			la_cfg.dc_color[2] = 0.0f;
			la_cfg.dc_color[3] = 0.0f;

			core_ogl->CreateElement((OpenGL::Element::DrawType)(OpenGL::Element::Quad | OpenGL::Element::ClipSpecialFlag), progress_el, 4, &app_clip);
			progress_el.SetAnchorMode(OpenGL::CenterFront);			
			progress_el.Move(la_cfg.dc_color);
			
			la_cfg.dc_color[0] = 0.0; la_cfg.dc_color[1] = 20.0f;
			core_ogl->CreateElement(OpenGL::Element::Quad, pic_el0, 4, &progress_el);
			pic_el0.Move(la_cfg.dc_color);

			la_cfg.max_char_count = 16;
			OpenGL::Core::ColorConvert(la_cfg.c_color, 0xFFCFFAD9);
			
			la_cfg.offset[0] = 140.0f; la_cfg.offset[1] = 220.0f;
			la_cfg.font_id = mmenu->GetTimeFont();
			la_cfg.font_style = OpenGL::Text::Regular;
			la_cfg.a_type = OpenGL::LeftTopFront;
			la_cfg.f_size = 40.5f;

			pic_el0.AddLabel(la_cfg);

			la_cfg.offset[1] = 240.0f;
			la_cfg.f_size = 20.5f;

			pic_el0.AddLabel(la_cfg);

			progress_el.Hide();

			System::MemoryFill(&path_but_hdr, sizeof(OpenGL::Button::Header), 0);
			System::MemoryFill(&path_but_palette, sizeof(OpenGL::Button::PaletteEntry), 0);
			System::MemoryFill(&path_line_cfg, sizeof(OpenGL::Element::LabelCfg), 0);

			System::MemoryFill(&file_but_palette, sizeof(OpenGL::Button::PaletteEntry), 0);


			file_but_palette.ico_xcol[0] = 0x0040; file_but_palette.ico_xcol[1] = 0x0040; file_but_palette.ico_xcol[2] = 0x5FB4;

			OpenGL::Core::ColorConvert(file_but_palette.color, 0x02DDFFFF);
			OpenGL::Core::ColorConvert(file_but_palette.h_color, 0xFF202020);
			OpenGL::Core::ColorConvert(file_but_palette.d_color, 0xFF181818);

			OpenGL::Core::ColorConvert(file_but_palette.b_color, 0x05FFFFFF);
			OpenGL::Core::ColorConvert(file_but_palette.hb_color, 0x809DCCEF);
			OpenGL::Core::ColorConvert(file_but_palette.db_color, 0xFF404040);


			OpenGL::Core::ColorConvert(file_but_palette.hi_color, 0xFF383030);
			OpenGL::Core::ColorConvert(file_but_palette.di_color, 0xFF181818);

			OpenGL::Core::ColorConvert(file_but_palette.ib_color, 0xFF302830);			
			OpenGL::Core::ColorConvert(file_but_palette.hib_color, 0x020000FF);			
			OpenGL::Core::ColorConvert(file_but_palette.dib_color, 0x020000FF);



			path_but_hdr.flags = OpenGL::Button::Header::BorderPresent | OpenGL::Button::Header::AutoSize | 0xE0000000;
			
			path_but_hdr.border_offset = 3.0f;
			path_but_hdr.tline_count = 1;

			OpenGL::Core::ColorConvert(path_but_palette.color, 0x01000000);
			OpenGL::Core::ColorConvert(path_but_palette.h_color, 0xFF1A1A1A);
			OpenGL::Core::ColorConvert(path_but_palette.d_color, 0xFF0D0D0D);
			
			OpenGL::Core::ColorConvert(path_but_palette.b_color, 0xFF262626);
			OpenGL::Core::ColorConvert(path_but_palette.hb_color, 0xFF262626);
			OpenGL::Core::ColorConvert(path_but_palette.db_color, 0xFF262626);


			OpenGL::Core::ColorConvert(path_line_cfg.c_color, 0xFF9CCCF0);
			OpenGL::Core::ColorConvert(path_line_cfg.dc_color, 0xFFA8E0FF);

			path_line_cfg.offset[0] = 20.0f; path_line_cfg.offset[1] = 40.0f;
			path_line_cfg.font_id = -1;
			path_line_cfg.font_style = OpenGL::Text::Regular;
			path_line_cfg.a_type = OpenGL::LeftTopFront;
			path_line_cfg.f_size = 18.5f;


			if (_libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {

				if (core_ogl->CreateButtonGrid(file_list, &app_clip, &XFiles::PageSize) != -1) {

					file_list.ResetDragOp((UI_64)(OpenGL::Element::FlagX0Drag | OpenGL::Element::FlagX1Drag | OpenGL::Element::FlagXYDrag));
					box_vals[0] = 0.0f; box_vals[1] = 0.0f;
					file_list.Configure(0xFF303030, box_vals);
				
					file_list.SetButtonUpdate(FormatButton, ListScanUp, ListScanDown, 0);

					file_list.CreateEBorder(1.1f, 0xE0000000);
					file_list.SetEBorderColor(color_val);

		//			file_list.Hide();
					file_list.SetMargin(box_vals+4);

					file_list.ConfigureCell(FileSelect, 512.0f, 64.00001f, 0x80000000, 1.1f); //  0x02DDFFFF, 0x05FFFFFF, 
					file_list.ConfigureIcon(0x00400000, 0x00400000, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE | OpenGL::Button::Header::IconColorSpecial, OpenGL::LeftTopFront, 0.1f); // 0, 0xFF302830, 0x0040, 
					
				// load textures
					folder_ico = file_list.LoadIcon(14105, _libin);

					file_list.AddPaletteEntry(file_but_palette);

					lidx = file_list.ConfigureTextLine(0xFF001400, 0, 0x00040000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 10.1f, 34.0f, 40.0f, -2, reinterpret_cast<const wchar_t *>(mmenu->GetTextFont()));
					file_list.ConfigureCellTLDown(lidx, 0xFFBAEEA4, 0);
// second iconlab
					lidx = file_list.ConfigureTextLine(0xFF600000, 0, 0x00050000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 4.0f, 58.0f, 18.8f, -2, reinterpret_cast<const wchar_t *>(mmenu->GetTextFont()));
					file_list.ConfigureCellTLHoover(lidx, 0xFFFFE080, 0);
					file_list.ConfigureCellTLDown(lidx, 0xFFDDFFFF, 0);

// 0xFF99AAAA	0xFFFFDDBB	0xFFF0F0C0
					lidx = file_list.ConfigureTextLine(0xFFF0E080, 0, 0x00280000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 80.0f, 24.0f, 18.5f, -1, L"Calibril");
					file_list.ConfigureCellTLHoover(lidx, 0xFFDDFFFF, 0);

					lidx = file_list.ConfigureTextLine(0xFF000000, 0, 0x00180000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 90.0f, 41.0f, 13.5f, -1, L"Calibril");
					file_list.ConfigureCellTLHoover(lidx, 0xFF008000, 0);
					file_list.ConfigureCellTLDown(lidx, 0xFFBAEEA4, 0);
// 0xFFC8C8FF
					lidx = file_list.ConfigureTextLine(0xFF808099, 0, 0x00100000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 90.0f, 57.0f, 13.5f, -1, L"Calibril");

//					file_list.ClearEFlags((UI_64)OpenGL::Element::FlagBuilt); // if (file_list.TestEFlags((UI_64)OpenGL::Element::FlagBuilt)) 
					file_list.XPand();
				

					file_list.GetRightMargin(right_margin);
					right_margin[0] = right_margin[2];

					if (core_ogl->CreateClip(path_list) != -1) {
						path_list.SetAnchorMode(OpenGL::LeftBottomFront);
						path_list.ResetDragOp((UI_64)(OpenGL::Element::FlagY0Drag | OpenGL::Element::FlagY1Drag));

						box_vals[0] = 257.0f; box_vals[1] = 0.0f; box_vals[2] = -128.0f; box_vals[3] = 64.00002f;
						path_list.Configure(0xFF333333, box_vals, 1);
						path_list.Hide();



					// page left button
						result = -1;						
						function_group_id = -1;
						function_group_id = OpenGL::APPlane::AddFunction(fun_but, result, 64.00003f); // exit_button

						color_val[0] = color_val[1] = color_val[2] = 0.2f;

						if (function_group_id != -1) {
							fun_but.SetColor(color_val);
				
							fun_but.ConfigureClipButton(0x00020000 | 14101, _libin);

							fun_but.Hide();
							
							MainMenu::SetFunctionHandlers(fun_but);

							_event = SFSco::Event_CreateDrain(&TurnLeft, &fun_but, 0);
							SFSco::Event_SetGate(_event, fun_but, (UI_64)OpenGL::Element::FlagMouseUp);
							fun_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);


						// page right button
							if (OpenGL::APPlane::AddFunction(_button, function_group_id, 64.00004f) != -1) {
								_button.SetColor(color_val);
				
								_button.ConfigureClipButton(0x00030000 | 14102, _libin);

								_button.Hide();

								MainMenu::SetFunctionHandlers(_button);
							
								_event = SFSco::Event_CreateDrain(&TurnRight, &_button, 0);
								SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
								_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
							}

						// page up button
							if (OpenGL::APPlane::AddFunction(_button, function_group_id, 64.00005f) != -1) {
								_button.SetColor(color_val);
				
								_button.ConfigureClipButton(0x00050000 | 17114, _libin);

								_button.Hide();

								MainMenu::SetFunctionHandlers(_button);

								_event = SFSco::Event_CreateDrain(&RollUp, &_button, 0);
								SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
								_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


							}

						// page down button
							if (OpenGL::APPlane::AddFunction(_button, function_group_id, 64.00006f) != -1) {
								_button.SetColor(color_val);
				
								_button.ConfigureClipButton(0x00060000 | 17115, _libin);

								_button.Hide();

								MainMenu::SetFunctionHandlers(_button);

								_event = SFSco::Event_CreateDrain(&RollDown, &_button, 0);
								SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
								_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

							}

						// exit button
							if (OpenGL::APPlane::AddFunction(_button, function_group_id, 64.00007f) != -1) {
								_button.SetColor(color_val);

								_button.ConfigureClipButton(OpenGL::Button::ClipSide | 17118, _libin);

								_button.Hide();

								MainMenu::SetFunctionHandlers(_button);



								_event = SFSco::Event_CreateDrain(&Stop, 0, 0);
								SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
								_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


								menu_event = SFSco::Event_CreateDrain(&Select, &fun_but, 0);
								SFSco::Event_SetGate(menu_event, file_list, (UI_64)OpenGL::Element::FlagMouseDown);
							}
						}

			
						if (mmenu) {
							if (mmenu->AddFunction(_button, 14100)) { // add app button to main menu
								_event = SFSco::Event_CreateDrain(&Start, &fun_but, 0);
								SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
								_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);
							}
						}
				


						if (current_path.New(0, 32767*sizeof(wchar_t))!= -1) {

							if (current_files.New(0, 128*sizeof(WIN32_FIND_DATA))!= -1) {
								_files_top = 128;
								_files_runner = 0;	
							
							}
						}

						InitPage(0x80000000);

						result = extention_list.New(0, 256*4*sizeof(wchar_t));
						result |= extention_map.New(0, 256*sizeof(XMapEntry));

						if (result != -1) {
							ext_top = 256;

							result = 0;
						}
					}
				}
			}
		}
		
		if (result == 0) {
			result = Pics::Image::LoadRes(14106, 33353, _libin);
		
			if (result != -1) {
				if (progress_tid = Pics::Image::OGLBind(result, box_vals, 0, 0)) {


					box_vals[0] = 0.25f*box_vals[2];
					box_vals[1] = 0.25f*box_vals[3];

					box_vals[0] += box_vals[0];
					box_vals[1] += box_vals[1];


					progress_transform[0][0] = Calc::FloatCos(4.1f);
					progress_transform[0][1] = Calc::FloatSin(4.1f);

					progress_transform[0][4] = -progress_transform[0][1];
					progress_transform[0][5] = progress_transform[0][0];

			//			progress_transform[0][12] = box_vals[0] - (box_vals[0]*progress_transform[0][0] - box_vals[1]*progress_transform[0][1]);
			//			progress_transform[0][13] = box_vals[1] - (box_vals[0]*progress_transform[0][1] + box_vals[1]*progress_transform[0][0]);

					progress_transform[1][0] = Calc::FloatCos(3.3f);
					progress_transform[1][1] = Calc::FloatSin(3.3f);

					progress_transform[1][4] = -progress_transform[1][1];
					progress_transform[1][5] = progress_transform[1][0];

					progress_transform[1][12] = box_vals[0] - (box_vals[0]*progress_transform[1][0] - box_vals[1]*progress_transform[1][1]);
					progress_transform[1][13] = box_vals[1] - (box_vals[0]*progress_transform[1][1] + box_vals[1]*progress_transform[1][0]);




					pic_el0.SetTrajectory(ProgressCircle0, 1);

					core_ogl->CreateElement(OpenGL::Element::Quad, pic_el1, 4, &pic_el0);

					pic_el1.SetTrajectory(ProgressCircle1);
						
					pic_el1.InitAttributeArray(OpenGL::AttributeArray::Vertex, a_arr);
					if (OpenGL::AttributeArray::Header * a_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(a_arr.Acquire())) {
						reinterpret_cast<float *>(a_hdr+1)[4] = box_vals[2];
						reinterpret_cast<float *>(a_hdr+1)[9] = box_vals[3];
						reinterpret_cast<float *>(a_hdr+1)[12] = box_vals[2];
						reinterpret_cast<float *>(a_hdr+1)[13] = box_vals[3];

						a_arr.Release();
					}
						
					pic_el1.SetRectTexture(box_vals, progress_tid, 0);
				}

				result = Pics::Image::LoadRes(14110, 33353, _libin);
				if (result != -1) {
					if (preview_progress_tid = Pics::Image::OGLBind(result, box_vals, 0, 0)) {
					
					
					

					}

					result = 0;
				}
			}
		}
	}

	return result;
}


UI_64 Apps::XFiles::Finalize() {
	UI_64 result(0);

	if (current_path) current_path.Destroy();
	if (current_files) current_files.Destroy();

	core_ogl = 0;

	return result;
}

/*
	button layout

		border	icon quad		label		label		label
				border
				label

*/

void Apps::XFiles::PathHoover(OpenGL::Button & b_el) {	

	if (_instance) {
		if (OpenGL::Core * core_ptr = b_el.GetContext()) {
			b_el.SetColor(_instance->path_but_palette.h_color);
			core_ptr->ResetElement(b_el);
		}
	}
}

void Apps::XFiles::PathLeave(OpenGL::Button & b_el) {	

	if (_instance) {
		if (OpenGL::Core * core_ptr = b_el.GetContext()) {
			b_el.SetColor(_instance->path_but_palette.color);
			core_ptr->ResetElement(b_el);
		}
	}
}

void Apps::XFiles::PathDown(OpenGL::Button & b_el) {
	if (_instance) {
		if (OpenGL::Core * core_ptr = b_el.GetContext()) {
			b_el.SetColor(_instance->path_but_palette.d_color);
			core_ptr->ResetElement(b_el);
		}
	}

}

UI_64 Apps::XFiles::Select(OpenGL::Button & f_but) {
	if (_instance) {
		if (!OpenGL::APPlane::TestCA()) OpenGL::APPlane::ResetFunction(f_but);
	}

	return 0;
}

UI_64 Apps::XFiles::PathSelect(OpenGL::Button & b_el) {
	unsigned int result(0);

	if (_instance) {
		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_el.Acquire())) {
			_instance->current_path_pos = el_hdr->x_tra & 0x00FFFFFF;
			_instance->current_path_type = el_hdr->x_tra & PTMaskFull;

			_instance->_path_runner = el_hdr->x_tra>>32;

			b_el.Release();
		}
		
		_instance->InitPage();

	}

	return result;
}

unsigned int Apps::XFiles::SetPathButton(OpenGL::Core * ogl_core) {
	float location[4] = {0.0, 0.0, 0.0, 0.0};

	wchar_t path_buf[256];
	UI_64 _event(0), pas_pos(0);
	unsigned int result(0), ival(0);
	OpenGL::Button el_but, right_but;

	if (_path_runner < _path_top) {
		el_but(path_list);

		el_but.Down().Down();

		for (unsigned int i(0);i<_path_runner;i++) el_but.Left();

	} else {

		if (ogl_core->CreateButton(el_but, 100.0f, 64.0f, &path_list) != -1) {
			el_but.Configure(path_but_hdr, path_but_palette, &path_line_cfg);
		
			
			el_but.SetDefaultBlink(4, 4, 0x01000000, 0xFF0D0D0D, 0);

			_event = SFSco::Event_CreateDrain(PathHoover, &el_but, 0);
			SFSco::Event_SetGate(_event, el_but, (UI_64)OpenGL::Element::FlagHoover);

			_event = SFSco::Event_CreateDrain(PathLeave, &el_but, 0);
			SFSco::Event_SetGate(_event, el_but, (UI_64)OpenGL::Element::FlagLeave);

			_event = SFSco::Event_CreateDrain(PathDown, &el_but, 0);
			SFSco::Event_SetGate(_event, el_but, (UI_64)OpenGL::Element::FlagMouseDown);


			_event = SFSco::Event_CreateDrain(PathSelect, &el_but, 0);
			SFSco::Event_SetGate(_event, el_but, (UI_64)OpenGL::Element::FlagMouseUp);
			el_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);

			result = 1;
			_path_top++;
		}
	}

	if (OpenGL::Element::Header * elhdr = reinterpret_cast<OpenGL::Element::Header *>(el_but.Acquire())) {
		elhdr->x_tra = _path_runner;
		elhdr->x_tra <<= 32;
		elhdr->x_tra |= current_path_pos | current_path_type;

		el_but.Release();
	}

	el_but.Show();
	// do some shit
	switch (_path_runner) {
		case 0:
			OpenGL::Core::GetHeaderString(40, path_buf);
			result += el_but.SetLabelText(0, path_buf + 1, path_buf[0]); // location
			location[0] = 25.0f;

		break;
		case 1:
			right_but(el_but);
			right_but.Right();
			right_but.GetRightMargin(location);

			switch (current_path_type & PTMask) {
				case PathTypeLocal:
					OpenGL::Core::GetHeaderString(11, path_buf);
				break;
				case PathTypeRemote:
					OpenGL::Core::GetHeaderString(38, path_buf);
				break;
			}

			result += el_but.SetLabelText(0, path_buf + 1, path_buf[0]); // L"Local"

			location[0] += location[2]; // - 8.0f;
			location[2] = 0.0;

		break;

	
	
		default:
			right_but(el_but);
			right_but.Right();
			right_but.GetRightMargin(location);
				

			System::MemoryFill(path_buf, 256*sizeof(wchar_t), 0);

			ival = 0;
			if (wchar_t * p_ptr = reinterpret_cast<wchar_t *>(_instance->current_path.Acquire())) {			

				for (unsigned int i(_instance->current_path_pos-1);i>2;i--) {
					if (p_ptr[i] == L'\\') {
						ival = _instance->current_path_pos - i - 1;
						if (result < 256) {
							System::MemoryCopy(path_buf, p_ptr + i + 1, ival*sizeof(wchar_t));
						} else {
							ival = 0;
						}
					
						break;
					}
				}
			
				_instance->current_path.Release();

			}


			if (ival) result += el_but.SetLabelText(0, path_buf, ival);

			location[0] += location[2]; // - 8.0f;
			location[2] = 0.0;
	}	

	el_but.RelocateXY(location);

	core_ogl->SetBlink(el_but, 2);


	for (unsigned int i(_path_runner+1);i<_path_top;i++) {
		el_but.Left();
		el_but.Hide();
	}

	

	return result;
}

UI_64 Apps::XFiles::FileSelect(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {
	UI_64 result(0), ival(0), ival_2(0);

	if (_instance) {
		_instance->file_list.SetState((UI_64)OpenGL::Element::FlagMouseDown);

		result = s_val & 0x00FFFFFF;

		_instance->current_path_type &= ~PathTypeCapture;

		if (result < _instance->_files_runner) {
			if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(_instance->current_files.Acquire())) {
				if (cf_list[result].dwFileAttributes & (0x00001000 | DRIVE_TYPE | SERVER_TYPE | SHARE_TYPE | CAPTURE_TYPE)) { // drive / folder
					// path change
					if (wchar_t * p_ptr = reinterpret_cast<wchar_t *>(_instance->current_path.Acquire())) {
						p_ptr[_instance->current_path_pos++] = L'\\';

						if ((cf_list[result].dwFileAttributes & 0x0F) == CAPTURE_TYPE) {
							ival = wcslen(cf_list[result].cFileName + 4);
							System::MemoryCopy(p_ptr + _instance->current_path_pos, cf_list[result].cFileName + 4, ival*sizeof(wchar_t));
							_instance->current_path_pos += ival;
						} else {
							System::MemoryCopy(p_ptr + _instance->current_path_pos, cf_list[result].cFileName, cf_list[result].cAlternateFileName[0]*sizeof(wchar_t));
							_instance->current_path_pos += cf_list[result].cAlternateFileName[0];
						}
						
						p_ptr[_instance->current_path_pos] = L'\0';

						_instance->_path_runner++;
						
						_instance->current_path.Release();

						switch (result = (cf_list[result].dwFileAttributes & 0x0F)) {
							case CAPTURE_TYPE:
								_instance->current_path_type |= PathTypeCapture;

							break;
							default:								
								result = 2;
						}
					}

					
					// set path button
										
				} else {
					ival = result;
					result = 0;

					if (cf_list[ival].dwFileAttributes != SERVICE_TYPE) {
						if (cf_list[ival].cAlternateFileName[2] < _instance->app_count) {
							if ((app_record[cf_list[ival].cAlternateFileName[2]].app_ptr) && (app_record[cf_list[ival].cAlternateFileName[2]].f_selector)) {
								ival_2 = _instance->current_app;
								_instance->current_app = cf_list[ival].cAlternateFileName[2];

								result = -222;
							}
						}
					} else {
						switch (ival) {
							case 0: // local
								_instance->current_path_type = PathTypeLocal;

								if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(_instance->current_path.Acquire())) {
									cp_ptr[0] = L'\\';
									cp_ptr[1] = L'\\';
									cp_ptr[2] = L'?';
									cp_ptr[3] = L'\0';

									_instance->current_path_pos = 3;

									_instance->current_path.Release();
								}

							break;
							case 1:
								_instance->current_path_type = PathTypeRemote;

								if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(_instance->current_path.Acquire())) {
									cp_ptr[0] = L'\\';
									cp_ptr[1] = L'\\';
									cp_ptr[2] = L'?';
									cp_ptr[3] = L'\\';
									cp_ptr[4] = L'U';
									cp_ptr[5] = L'N';
									cp_ptr[6] = L'C';
									cp_ptr[7] = L'\0';
											
									_instance->current_path_pos = 7;

									_instance->current_path.Release();
								}

							break;

						}

						result = 1;

						_instance->_path_runner++;
					}
				}


				_instance->current_files.Release();
			}

			if (result) {
				if (result == -222) {
					app_record[_instance->current_app].f_selector(_instance->file_list, ival);
					_instance->current_app = ival_2;
				} else {
					_instance->InitPage(result);
				}
			}
		}
	}


	return result;
}

UI_64 Apps::XFiles::SelectPLItem(UI_64 iidx, SFSco::Object & p_list) {
	UI_64 result(0);

	if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(p_list.Acquire())) {
		result = reinterpret_cast<UI_64 *>(pl_ptr)[0];

		if (iidx <= reinterpret_cast<UI_64 *>(pl_ptr+result)[0]) {
			result = reinterpret_cast<UI_64 *>(pl_ptr+result)[iidx];
			System::MemoryCopy(pl_ptr + 8 + pl_ptr[4] + 1, pl_ptr + result + 1, pl_ptr[result]*sizeof(wchar_t));
		} else {
			result = 0;
		}

		p_list.Release();
	}

	return result;
}

UI_64 Apps::XFiles::BuildPlayList(UI_64 e_idx, SFSco::Object & p_list) {
	UI_64 result(-1);
	unsigned int f_idx(-1), pl_count(0), fn_size(0), li_length(0);

	p_list.Blank();

	if (_instance) {
		if ((_instance->current_path_type & PathTypeCapture) == 0)
		if (p_list.New(0, (_instance->current_path_pos + MAX_PATH + 1 + 8 + 4)*sizeof(wchar_t)) != -1) {
			if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(p_list.Acquire())) {
				reinterpret_cast<UI_64 *>(pl_ptr)[0] = 0;
				reinterpret_cast<UI_64 *>(pl_ptr)[1] = 0;

				if (const wchar_t * cp_ptr = reinterpret_cast<const wchar_t *>(_instance->current_path.Acquire())) {
					pl_ptr[8] = _instance->current_path_pos + 1;
					System::MemoryCopy(pl_ptr + 9, cp_ptr, _instance->current_path_pos*sizeof(wchar_t));
					pl_ptr[_instance->current_path_pos + 9] = L'\\';

					li_length = _instance->current_path_pos + MAX_PATH + 9;
					reinterpret_cast<UI_64 *>(pl_ptr + li_length)[0] = 0;
					
					_instance->current_path.Release();
				}

				p_list.Release();
			}

			if (const WIN32_FIND_DATA * cf_list = reinterpret_cast<const WIN32_FIND_DATA *>(_instance->current_files.Acquire())) {
				f_idx = cf_list[0].ftCreationTime.dwLowDateTime;


				for (;f_idx < _instance->_files_runner;) {
					if (cf_list[f_idx].cAlternateFileName[2] == _instance->current_app) {
						fn_size = cf_list[f_idx].cAlternateFileName[0];

						_instance->current_files.Release();
						cf_list = 0;


						if (p_list.Expand((fn_size + 1 + 4)*sizeof(wchar_t)) == -1) break;
						cf_list = reinterpret_cast<const WIN32_FIND_DATA *>(_instance->current_files.Acquire());
						if (!cf_list) break;

						if (wchar_t * pl_ptr = reinterpret_cast<wchar_t *>(p_list.Acquire())) {
							System::MemoryCopy(pl_ptr + li_length + fn_size + 1, pl_ptr + li_length, (pl_count + 1)*sizeof(UI_64));

							pl_ptr[li_length] = fn_size;
							System::MemoryCopy(pl_ptr + li_length + 1, cf_list[f_idx].cFileName, fn_size*sizeof(wchar_t));
							li_length += fn_size + 1;
							reinterpret_cast<UI_64 *>(pl_ptr)[0] = li_length;
							reinterpret_cast<UI_64 *>(pl_ptr + li_length)[0] = ++pl_count;
							reinterpret_cast<UI_64 *>(pl_ptr + li_length)[pl_count] = li_length - fn_size - 1;

							p_list.Release();
						}

						if (f_idx == e_idx) result = pl_count;
					}

					f_idx = cf_list[f_idx].dwReserved0;
				}


				if (cf_list) _instance->current_files.Release();
			}
		}
	}

	return result;
}







// settrajectory for preview somewhere

void Apps::XFiles::SetAppButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_hdr, unsigned int & f_idx, unsigned & app_id) {
	UI_64 ico_id(-1), path_id(-1), text_id(-1), name_id(-1);
	unsigned int c_count(0);
	wchar_t sizes_line[256];

	SFSco::Object path_obj;
	OpenGL::Element but_el(g_but);
	

	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {		

		if (f_idx < _files_runner) {
			if (app_id & 0x80000000) {
				f_idx = cf_list[f_idx].dwReserved1;
			} else {
				f_idx = cf_list[f_idx].dwReserved0;
			}
		} else {
			f_idx = cf_list[0].ftCreationTime.dwLowDateTime;
		}

		app_id &= 0x0000FFFF;

		if (f_idx <_files_runner) {
			if (cf_list[f_idx].cAlternateFileName[2] == app_id) {

				if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
					bel_hdr->x_tra = f_idx;
					g_but.Release();
				}

				but_el.Down();

				if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
					but_el.Left();
				}

				if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
					ico_id = but_el.GetID();
						
					but_el.Left();
				}

		// first label
					
				c_count = cf_list[f_idx].cAlternateFileName[0];
				System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName, c_count*sizeof(wchar_t));

				current_files.Release();
				cf_list = 0;

				text_id = but_el.GetID();				
				name_id = but_el.Left().GetID();
				

				if (but_hdr.tline_count > 1) {
					reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, c_count);
				}			

				
				if (app_id == current_app) {
					// add file path, button id, icon id, text id
					if (app_record[current_app].app_ptr) {
						if (current_path_type & PathTypeCapture) {
							path_id = 1; path_id <<= 63;
							path_id |= f_idx;
						} else {
							path_id = path_obj.New(0, (current_path_pos + MAX_PATH)*sizeof(wchar_t));

							if (path_id != -1) {
								if (wchar_t * po_ptr = reinterpret_cast<wchar_t *>(path_obj.Acquire())) {
									if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(current_path.Acquire())) {
										System::MemoryCopy(po_ptr, cp_ptr, current_path_pos*sizeof(wchar_t));
										po_ptr[current_path_pos] = L'\\';

										if (cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
											System::MemoryCopy(po_ptr + current_path_pos + 1, cf_list[f_idx].cFileName, cf_list[f_idx].cAlternateFileName[0]*sizeof(wchar_t));
											po_ptr[current_path_pos + cf_list[f_idx].cAlternateFileName[0] + 1] = L'\0';										
										}

										current_path.Release();
									}
									path_obj.Release();
								}
							}
						}
																	
						reinterpret_cast<OpenGL::Button &>(g_but).StartPreview(preview_progress_tid);
						app_record[current_app].app_ptr->CreatePreview(path_id, g_but, ico_id, text_id, name_id, 0);						
					}
				}

			} else {
				app_id = 0;
			}
		}
		
		if (cf_list) current_files.Release();
	}	

}


void Apps::XFiles::SetFileButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_hdr, unsigned int & f_idx, unsigned int & app_id) {
	SYSTEMTIME s_time;
	wchar_t sizes_line[256];
	const wchar_t * src_ptr(0);

	int i_val(0), o_val(0);

	OpenGL::Element icon_el, but_el(g_but);
	
	
	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
		

		if (f_idx < _files_runner) {
			if (app_id & 0x80000000) {
				f_idx = cf_list[f_idx].dwReserved1;
			} else {
				f_idx = cf_list[f_idx].dwReserved0;
			}
		} else {
			f_idx = cf_list[0].ftCreationTime.dwLowDateTime;
		}

		app_id &= 0x0000FFFF;

		if (f_idx <_files_runner) {

			if (!(cf_list[f_idx].dwFileAttributes & (FILE_ATTRIBUTE_OFFLINE << 8))) {
				app_id = cf_list[f_idx].dwFileAttributes;

				if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
					bel_hdr->x_tra = f_idx;
					g_but.Release();
				}

				reinterpret_cast<OpenGL::Button &>(g_but).SetIconSpecial(0xFF343438);

				but_el.Down();

				

				switch (_path_runner) {
					case 0:
						if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
							but_el.Left();
						}

						if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
							icon_el = but_el;

//							icon_el.SetXColor(0x3000);
							icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE);
								

							icon_el.Down();

							if (but_hdr.flags & OpenGL::Button::Header::IconBorderPresent) {
								icon_el.Left();
							}

							if (but_hdr.flags & OpenGL::Button::Header::IconTextPresent) {								
								icon_el.Hide();

								icon_el.Left(); // second label
								icon_el.Hide();

							}						

							if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
								but_el.SetColor(&bel_hdr->span_cfg.x);
								g_but.Release();
							}
						
							but_el.Left();
						}

						i_val = cf_list[f_idx].cAlternateFileName[0];
						System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName, i_val*sizeof(wchar_t));

						current_files.Release();
						cf_list = 0;

						reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);

						but_el.Show();
						but_el.Left();
						
						but_el.Hide();
						but_el.Left();
										
						but_el.Hide();

					break;
					case 1:
						switch (current_path_type & PTMask) {
							case PathTypeRemote:
								if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
									but_el.Left();
								}

								if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
									icon_el = but_el;

//									icon_el.SetXColor(0x3000);
									icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE);
								

									icon_el.Down();

									if (but_hdr.flags & OpenGL::Button::Header::IconBorderPresent) {
										icon_el.Left();
									}

									if (but_hdr.flags & OpenGL::Button::Header::IconTextPresent) {								
										icon_el.Hide();

										icon_el.Left(); // second label
										icon_el.Hide();

									}						

									if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
										but_el.SetColor(&bel_hdr->span_cfg.x);
										g_but.Release();
									}
						
									but_el.Left();
								}

								i_val = cf_list[f_idx].cAlternateFileName[0];
								System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName, i_val*sizeof(wchar_t));

								current_files.Release();
								cf_list = 0;

								reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);

								but_el.Show();
								but_el.Left();
						
								but_el.Hide();
								but_el.Left();
										
								but_el.Hide();


							break;
							default:
								if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
									but_el.Left();
								}

								if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
									icon_el = but_el;
					
//									icon_el.SetXColor(0x3000);
									icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE);

									icon_el.Down();

									if (but_hdr.flags & OpenGL::Button::Header::IconBorderPresent) {
										icon_el.Left();
									}

									if (but_hdr.flags & OpenGL::Button::Header::IconTextPresent) {
										reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(cf_list[f_idx].cFileName, 0, 2);
										icon_el.Show();

										icon_el.Left(); // second label
										icon_el.Show();

										switch (cf_list[f_idx].dwFileAttributes & 0x0F) {
											case HARD_DRIVE_TYPE:
												OpenGL::Core::GetHeaderString(12, sizes_line);
												reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(sizes_line + 1, 0, sizes_line[0]); // L"HDD"
											break;
											case REMOVABLE_TYPE:
												OpenGL::Core::GetHeaderString(13, sizes_line);
												reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(sizes_line + 1, 0, sizes_line[0]); // L"RD"
											break;
											case CD_DRIVE_TYPE:
												OpenGL::Core::GetHeaderString(14, sizes_line);
												reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(sizes_line + 1, 0, sizes_line[0]); // L"CD"
											break;
											case CAPTURE_TYPE: // capture source button
												OpenGL::Core::GetHeaderString(15, sizes_line);
												reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(sizes_line + 1, 0, sizes_line[0]); // L"CAP"
											break;
											default:
												icon_el.Hide();

										}

										icon_el.MoveRight(59.001f);
									}
						
									if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
										but_el.SetColor(&bel_hdr->span_cfg.x);
										g_but.Release();
									}

									but_el.Left();
								}

			// first label
								i_val = wcslen(cf_list[f_idx].cFileName + 4);
								System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName + 4, i_val*sizeof(wchar_t));

								current_files.Release();
								cf_list = 0;

								if (i_val) {
									reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);
									but_el.Show();
								} else but_el.Hide();
														
				
								if (cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
									if ((cf_list[f_idx].dwFileAttributes & 0x0000000F) < SERVICE_TYPE) {
										but_el.Left();

										i_val = SFSco::Int64ToCStr(reinterpret_cast<char *>(sizes_line), reinterpret_cast<__int64 *>(&cf_list[f_idx].ftLastWriteTime)[0], 0);
										Strings::A2W(sizes_line);
										i_val = Strings::FormatNumberString(sizes_line, i_val, 4, L'B');
										sizes_line[i_val++] = L' ';
										sizes_line[i_val++] = L'f';
										sizes_line[i_val++] = L'r';
										sizes_line[i_val++] = L'e';
										sizes_line[i_val++] = L'e';
										sizes_line[i_val++] = L'/';

										o_val = SFSco::Int64ToCStr(reinterpret_cast<char *>(sizes_line + i_val), reinterpret_cast<__int64 *>(&cf_list[f_idx].ftCreationTime)[0], 0);
										Strings::A2W(sizes_line + i_val);
										i_val += Strings::FormatNumberString(sizes_line + i_val, o_val, 4, L'B');

										reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);
										but_el.Show();

										but_el.Left();
										reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(cf_list[f_idx].cAlternateFileName+1, 0);
										but_el.Show();
						
									} else {
										but_el.Left();
										but_el.Hide();
										but_el.Left();
										but_el.Hide();

									}
								} else {
									but_el.Left();
									but_el.Hide();
									but_el.Left();
									but_el.Hide();
								}
					

						}
					break;
					case 2:
						if ((current_path_type & PTMask) == PathTypeRemote) { // share
							if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
								but_el.Left();
							}

							if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
								icon_el = but_el;

//								icon_el.SetXColor(0x3000);
								icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE);
								

								icon_el.Down();

								if (but_hdr.flags & OpenGL::Button::Header::IconBorderPresent) {
									icon_el.Left();
								}

								if (but_hdr.flags & OpenGL::Button::Header::IconTextPresent) {								
									icon_el.Hide();

									icon_el.Left(); // second label
									icon_el.Hide();

								}						

								if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
									but_el.SetColor(&bel_hdr->span_cfg.x);
									g_but.Release();
								}
						
								but_el.Left();
							}

							i_val = cf_list[f_idx].cAlternateFileName[0];
							System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName, i_val*sizeof(wchar_t));

							current_files.Release();
							cf_list = 0;

							reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);

							but_el.Show();
							but_el.Left();
						
							but_el.Hide();
							but_el.Left();
										
							but_el.Hide();


							break;
						}

					
					default:
						if (but_hdr.flags & OpenGL::Button::Header::BorderPresent) {
							but_el.Left();
						}

						if (but_hdr.flags & OpenGL::Button::Header::IconPresent) {
							icon_el = but_el;

							if (cf_list[f_idx].dwFileAttributes & 0x00001000) {
//								icon_el.SetXColor(0x0040);
								icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE | folder_ico);

							} else {
//								icon_el.SetXColor(0x3000);
								icon_el.SetRectTexture(but_hdr.icon_box, but_hdr.icon_tid, (UI_64)OpenGL::Element::FlagNegTex | LAYERED_TEXTURE);
								
							}

							icon_el.Down();

							if (but_hdr.flags & OpenGL::Button::Header::IconBorderPresent) {
								icon_el.Left();
							}

							if (but_hdr.flags & OpenGL::Button::Header::IconTextPresent) {								
								icon_el.Hide();

								icon_el.Left(); // second label
								icon_el.Hide();


								if (cf_list[f_idx].cAlternateFileName[1]) {
									if (XMapEntry * x_ptr = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {
										reinterpret_cast<OpenGL::Button &>(g_but).SetIconSpecial(x_ptr[cf_list[f_idx].cAlternateFileName[3]].x_color[0]);
												 
										extention_map.Release();
									}
									

									reinterpret_cast<OpenGL::Text &>(icon_el).Rewrite(cf_list[f_idx].cAlternateFileName + 8, 0, cf_list[f_idx].cAlternateFileName[1]);
									icon_el.MoveRight(59.0f);

									icon_el.Show();
								}
							}
						

							if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
								but_el.SetColor(&bel_hdr->span_cfg.x);
								g_but.Release();
							}
						
							but_el.Left();
						}

		// first label
					
						i_val = cf_list[f_idx].cAlternateFileName[0] - cf_list[f_idx].cAlternateFileName[1];
						if (i_val == 0) i_val = 1;

						System::MemoryCopy(sizes_line, cf_list[f_idx].cFileName, i_val*sizeof(wchar_t));

						current_files.Release();
						cf_list = 0;
						reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);

						but_el.Show();
						but_el.Left();
						
						if (cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
							switch (cf_list[f_idx].dwFileAttributes & 0x0000000F) {
								case CAPTURE_TYPE:
									if (src_ptr = Video::CaptureSource::GetCurrentFormatString(f_idx)) {									
										for (i_val = 1; i_val <= src_ptr[0]; i_val++) {
											if (src_ptr[i_val] == L':') break;
										}
										i_val -= 2;

										reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + 1, 0, i_val);
										but_el.Show();

										but_el.Left();
										
										i_val += 3;
									
										if (i_val < src_ptr[0]) {
											reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + i_val + 1, 0, src_ptr[0] - i_val);
											but_el.Show();
										}
									}

								break;
								default:
									FileTimeToSystemTime(&cf_list[f_idx].ftLastWriteTime, &s_time);
						
									i_val = Strings::IntToCStr(reinterpret_cast<char *>(sizes_line), s_time.wYear, 0);
									reinterpret_cast<char *>(sizes_line)[i_val++] = '.';
									i_val += Strings::IntToCStr(reinterpret_cast<char *>(sizes_line)+i_val, s_time.wMonth, 2);
									reinterpret_cast<char *>(sizes_line)[i_val++] = '.';
									i_val += Strings::IntToCStr(reinterpret_cast<char *>(sizes_line)+i_val, s_time.wDay, 2);
					
									reinterpret_cast<char *>(sizes_line)[i_val++] = ' ';
					
									i_val += Strings::IntToCStr(reinterpret_cast<char *>(sizes_line)+i_val, s_time.wHour, 2);
									reinterpret_cast<char *>(sizes_line)[i_val++] = ':';
									i_val += Strings::IntToCStr(reinterpret_cast<char *>(sizes_line)+i_val, s_time.wMinute, 2);
									reinterpret_cast<char *>(sizes_line)[i_val++] = ':';
									i_val += Strings::IntToCStr(reinterpret_cast<char *>(sizes_line)+i_val, s_time.wSecond, 2);

									Strings::A2W(sizes_line);
					
									reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);
									but_el.Show();

						
						
// sizes label
	
									but_el.Left();
									i_val = SFSco::Int64ToCStr(reinterpret_cast<char *>(sizes_line), reinterpret_cast<__int64 *>(&cf_list[f_idx].nFileSizeHigh)[0], 0);
												
									if (cf_list[f_idx].dwFileAttributes & 0x00001000) {
										but_el.Hide();
									} else {
										Strings::A2W(sizes_line);
										i_val = Strings::FormatNumberString(sizes_line, i_val, 4, L'B');

										reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(sizes_line, 0, i_val);
										but_el.Show();
									}
							}
						}
				}
			}
		}
		
		if (cf_list) current_files.Release();
	}	
}





UI_64 Apps::XFiles::InitPage(int f_init) {
	OpenGL::Core * ogl_core = file_list.GetContext();
	float o_val[4] = {0.0, 0.0, 0.0, 0.0};
	UI_64 result(0);

	OpenGL::Element x_scene(app_clip), p_el(progress_el), o_el(progress_el);
	OpenGL::ButtonGrid a_grid(file_list);

	path_list.ClearEFlags((UI_64)OpenGL::Element::FlagSized);

	x_scene.Down();
	p_el.Down().Down();

	o_el.Down();

	if (f_init) {
		if (ogl_core) {
	// show content loading element is required
			
			ogl_core->SetFBack(p_el, ItemsFoundUpdate);
			ogl_core->StartMoving(o_el);
			ogl_core->StartMoving(reinterpret_cast<OpenGL::Element &>(o_el.Down().Left().Left()));

			progress_el.Show();

			x_scene.Hide();
			ogl_core->ResetElement(app_clip);
		}


		for (unsigned int i(0);i<app_count;i++) {
			app_record[i].count_er = 0;
			app_record[i].initiated = 0;
		}


		if (current_path_type & PathTypeCapture) {
			PrepareCapturePage();
		} else {
			switch (current_path_type & PTMask) {
				case PathTypeService:
					PrepareLocationPage();
				break;
				case PathTypeRemote:
					switch (_path_runner) {
						case 1:
							PrepareServerPage();
						break;
						case 2:
							PrepareSharePage();
						break;
						default:
							PrepareDirectoryPage(reinterpret_cast<OpenGL::Text &>(p_el));

					}
				break;
				default:
					if (_path_runner > 1) {
						PrepareDirectoryPage(reinterpret_cast<OpenGL::Text &>(p_el));
					} else {					
						PrepareLocalPage();
					}
			}
		}

		app_record[0].count_er = _files_runner;
		app_record[0].initiated = 1;
	}

// adjust app grid headers
	// change caption
	for (unsigned int api(0);api<app_count;api++) {		
		a_grid.ResetGrid(app_record[api].count_er);
		a_grid.Left();
	}

	if (app_record[current_app].count_er == 0) {
		current_app = 0;

	}

	o_val[0] = -right_margin[2]*_instance->current_app;
	x_scene.RelocateXY(o_val);

	app_record[current_app].initiated = 1;

	if (ogl_core) {
		if (f_init) {
			f_init &= 0x80000000;
			f_init |= SetPathButton(ogl_core);

		// hide content loading element
			ogl_core->StopMoving(o_el);
			ogl_core->StopMoving(reinterpret_cast<OpenGL::Element &>(o_el.Up()));
			
			p_el = progress_el;
			ogl_core->ClearFBack(reinterpret_cast<OpenGL::Element &>(p_el.Down().Down()));

			progress_el.Hide();
			
			x_scene.Show();

		}
		
		if (f_init == 0) {
			ogl_core->ResetElement(app_clip);
			ogl_core->ResetElement(path_list);
		} else {
			if ((f_init & 0x80000000) == 0) ogl_core->Update();
		}

		p_el = file_list;
		for (unsigned int i(0);i<current_app;i++) {
			p_el.Left();
		}

		ogl_core->SetFocus(p_el);

	}

	return result;
}

UI_64 Apps::XFiles::PrepareLocationPage() {
	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {

		_files_runner = 2;

		current_app = 0;
		app_record[0].count_er = _files_runner;
		app_record[0].initiated = 0;

		System::MemoryFill(cf_list, _files_runner*sizeof(WIN32_FIND_DATA), 0);




		OpenGL::Core::GetHeaderString(11, cf_list[0].cFileName);
		cf_list[0].cAlternateFileName[0] = cf_list[0].cFileName[0];
		for (wchar_t i(0);i<cf_list[0].cAlternateFileName[0];i++) cf_list[0].cFileName[i] = cf_list[0].cFileName[i+1];

		cf_list[0].dwFileAttributes = SERVICE_TYPE;

		cf_list[0].cAlternateFileName[2] = 0;

		if (0 < (_files_runner - 1)) cf_list[0].dwReserved0 = 1;
		else cf_list[0].dwReserved0 = -1;

		if (0) cf_list[0].dwReserved1 = -1;
		else cf_list[0].dwReserved1 = -1;



		OpenGL::Core::GetHeaderString(38, cf_list[1].cFileName);
		cf_list[1].cAlternateFileName[0] = cf_list[1].cFileName[0];
		for (wchar_t i(0);i<cf_list[1].cAlternateFileName[0];i++) cf_list[1].cFileName[i] = cf_list[1].cFileName[i+1];

		cf_list[1].dwFileAttributes = SERVICE_TYPE;

		cf_list[1].cAlternateFileName[2] = 0;

		if (0 < (_files_runner - 1)) cf_list[1].dwReserved0 = 1;
		else cf_list[1].dwReserved0 = -1;

		if (0) cf_list[1].dwReserved1 = -1;
		else cf_list[1].dwReserved1 = -1;





		current_files.Release();
	}

	return 0;
}

UI_64 Apps::XFiles::PrepareCapturePage() {
	UI_64 result(0);

	_files_runner = Video::CaptureSource::GetSourceCount();

	if (_files_runner > _files_top) {
		result = ((_files_runner - _files_top + 127) & 0xFFFFFF80);
		result = current_files.Expand(sizeof(WIN32_FIND_DATA)*result);
		if (result != -1) _files_top += result;
	}
	

	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {

		for (unsigned int i(0);i<app_count;i++) {
			if (app_record[i].app_ptr) {
				if (app_record[i].app_ptr->Test(XVideo::supported_xt_list)) {
					current_app = i;

					app_record[i].count_er = _files_runner;
					app_record[i].initiated = 0;
					break;
				}
			}
		}


		for (unsigned int i(0);i<_files_runner;i++) {
			System::MemoryFill(cf_list + i, sizeof(WIN32_FIND_DATA), 0);

			cf_list[i].cAlternateFileName[0] = Media::Source::GetName(i, cf_list[i].cFileName);
			cf_list[i].dwFileAttributes = CAPTURE_TYPE;

			cf_list[i].cAlternateFileName[2] = current_app;

			if (i < (_files_runner - 1)) cf_list[i].dwReserved0 = i + 1;
			else cf_list[i].dwReserved0 = -1;

			if (i) cf_list[i].dwReserved1 = i - 1;
			else cf_list[i].dwReserved1 = -1;
		}

		current_files.Release();
	}

	return result;
}


UI_64 Apps::XFiles::PrepareServerPage() {
	UI_64 result(0);
	SERVER_INFO_101 * serv_buf(0);

	DWORD serv_count[2];

	_files_runner = 0;

	if (NetServerEnum(0, 101, (unsigned char **)&serv_buf, MAX_PREFERRED_LENGTH, serv_count, serv_count + 1, -1, 0, 0) == 0) {
		if ((serv_buf) && (serv_count[0])) {
			if (serv_count[0] > _files_top) {
				result = ((serv_count[0] - _files_top + 127) & 0xFFFFFF80);
				result = current_files.Expand(sizeof(WIN32_FIND_DATA)*result);
				if (result != -1) _files_top += result;
			}

			if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
				System::MemoryFill_SSE3(cf_list, serv_count[0]*sizeof(WIN32_FIND_DATA), 0, 0);
				
				for (unsigned int i(0);i<serv_count[0];i++) {					
					cf_list[_files_runner].cAlternateFileName[0] = wcslen(serv_buf[i].sv101_name);
					for (unsigned int j(0);j<cf_list[_files_runner].cAlternateFileName[0];j++) cf_list[_files_runner].cFileName[j] = serv_buf[i].sv101_name[j];


					cf_list[_files_runner].dwFileAttributes = SERVER_TYPE;									

					cf_list[_files_runner].dwReserved0 = _files_runner + 1;
					cf_list[_files_runner].dwReserved1 = _files_runner - 1;
					
					_files_runner++;
				}
				
				cf_list[0].dwReserved1 = -1;
				if (_files_runner) cf_list[_files_runner - 1].dwReserved0 = -1;
				else cf_list[0].dwReserved0 = -1;

				current_files.Release();
			}

			NetApiBufferFree(serv_buf);
		}
	}


	return 0;
}

UI_64 Apps::XFiles::PrepareSharePage() {
	UI_64 result(0);
	SHARE_INFO_2 * share_info(0);
	DWORD share_count[2];

	wchar_t server_name[256];

	_files_runner = 0;

	if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(current_path.Acquire())) {
		System::MemoryCopy(server_name, cp_ptr + 8, (current_path_pos - 8)*sizeof(wchar_t));
		server_name[current_path_pos - 8] = L'\0';

		current_path.Release();	
		
		if (NetShareEnum(server_name, 2, (unsigned char **)&share_info, MAX_PREFERRED_LENGTH, share_count, share_count + 1, 0) == 0) {
			if ((share_info) && (share_count[0])) {

				if (share_count[0] > _files_top) {
					result = ((share_count[0] - _files_top + 127) & 0xFFFFFF80);
					result = current_files.Expand(sizeof(WIN32_FIND_DATA)*result);
					if (result != -1) _files_top += result;
				}
				

				if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
					System::MemoryFill_SSE3(cf_list, share_count[0]*sizeof(WIN32_FIND_DATA), 0, 0);
				
					for (unsigned int i(0);i<share_count[0];i++) {
						if ((share_info[i].shi2_type == 0) && (wcslen(share_info[i].shi2_remark) == 0)) {
							cf_list[_files_runner].cAlternateFileName[0] = wcslen(share_info[i].shi2_netname);
							for (unsigned int j(0);j<cf_list[_files_runner].cAlternateFileName[0];j++) cf_list[_files_runner].cFileName[j] = share_info[i].shi2_netname[j];
							
							cf_list[_files_runner].dwFileAttributes = SHARE_TYPE;

							cf_list[_files_runner].dwReserved0 = _files_runner + 1;
							cf_list[_files_runner].dwReserved1 = _files_runner - 1;
					
							_files_runner++;
						}
					}
				
					cf_list[0].dwReserved1 = -1;
					if (_files_runner) cf_list[_files_runner - 1].dwReserved0 = -1;
					else cf_list[0].dwReserved0 = -1;

					current_files.Release();
				}


				NetApiBufferFree(share_info);
			}
		}
	}
	

	return 0;
}

UI_64 Apps::XFiles::PrepareLocalPage() {
	UI_64 result(0);
	DWORD ecode(0), d_pos[4][2] = {{-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}};
	unsigned int last_jump(0);

	if (DWORD ld_mask = GetLogicalDrives()) {
		wchar_t disk_a[] = {L'\\', L'\\', L'?', L'\\', L'A', L':', L'\\', L'\0'};
		_files_runner = 0;

		if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
			for (disk_a[4] = L'A';ld_mask;disk_a[4]++) {
				if (ld_mask & 1) {					
					System::MemoryFill_SSE3(cf_list + _files_runner, sizeof(WIN32_FIND_DATA), 0, 0);

					switch (cf_list[_files_runner].dwFileAttributes = GetDriveType(disk_a)) {
						case DRIVE_REMOVABLE: case DRIVE_FIXED: case DRIVE_REMOTE: case DRIVE_CDROM:
							cf_list[_files_runner].dwFileAttributes &= 3;
							cf_list[_files_runner].dwFileAttributes ^= 3;

							cf_list[_files_runner].dwReserved0 = -1;
							cf_list[_files_runner].dwReserved1 = -1;

							if (d_pos[cf_list[_files_runner].dwFileAttributes][0] != -1) {
								cf_list[d_pos[cf_list[_files_runner].dwFileAttributes][1]].dwReserved0 = _files_runner;
								cf_list[_files_runner].dwReserved1 = d_pos[cf_list[_files_runner].dwFileAttributes][1];
							} else {
								d_pos[cf_list[_files_runner].dwFileAttributes][0] = _files_runner;
							}
							
							d_pos[cf_list[_files_runner].dwFileAttributes][1] = _files_runner;

							cf_list[_files_runner].cFileName[0] = disk_a[4];
							cf_list[_files_runner].cFileName[1] = disk_a[5];
						
							cf_list[_files_runner].cAlternateFileName[0] = 2;
							
							if (GetVolumeInformation(disk_a, cf_list[_files_runner].cFileName+4, 256, 0, &cf_list[_files_runner].nFileSizeHigh, 0, cf_list[_files_runner].cAlternateFileName+1, 12)) {
								GetDiskFreeSpaceEx(disk_a, 0, (PULARGE_INTEGER)&cf_list[_files_runner].ftCreationTime, (PULARGE_INTEGER)&cf_list[_files_runner].ftLastWriteTime);								
							} else {
								ecode = GetLastError(); // 21 - not ready
								if (ecode == 21) {
									cf_list[_files_runner].dwFileAttributes |= (FILE_ATTRIBUTE_OFFLINE << 8);

								}
							}
							
							cf_list[_files_runner].dwFileAttributes |= DRIVE_TYPE;

							_files_runner++;
						break;
					}
				}

				ld_mask >>= 1;
			}



			cf_list[0].ftCreationTime.dwLowDateTime = 0;
			last_jump = -1;

			for (unsigned int t_id = 0;t_id<4;t_id++) {
				if (d_pos[t_id][0] != -1) {
					if (last_jump != -1) {
						cf_list[last_jump].dwReserved0 = d_pos[t_id][0];
						cf_list[d_pos[t_id][0]].dwReserved1 = last_jump;
					} else {
						cf_list[0].ftCreationTime.dwLowDateTime = d_pos[t_id][0];
					}	

					last_jump = d_pos[t_id][1];
				}
			}

			if (last_jump != -1) {
				if (Video::CaptureSource::GetSourceCount()) {
					System::MemoryFill_SSE3(cf_list + _files_runner, sizeof(WIN32_FIND_DATA), 0, 0);

					cf_list[_files_runner].dwFileAttributes = CAPTURE_TYPE;

					cf_list[_files_runner].dwReserved0 = -1;
					cf_list[_files_runner].dwReserved1 = -1;

				
					cf_list[last_jump].dwReserved0 = _files_runner;
					cf_list[_files_runner].dwReserved1 = last_jump;
				
					cf_list[_files_runner].cFileName[0] = L'|';
					cf_list[_files_runner].cFileName[1] = L'|';						
					cf_list[_files_runner].cAlternateFileName[0] = 2;
				
					OpenGL::Core::GetHeaderString(16, cf_list[_files_runner].cFileName + 3);
//					System::MemoryCopy(cf_list[_files_runner].cFileName+4, , 12*sizeof(wchar_t)); // L"Cams & Video"

							
					_files_runner++;

				}			
			}


			current_files.Release();
		}
	}


	return result;
}

UI_64 Apps::XFiles::ItemsFoundUpdate(OpenGL::Element & label) {
	wchar_t t_str[64];
	OpenGL::Core * ogl_core(0);
	UI_64 result(0);
	unsigned int i_val(0);
	
	if (OpenGL::Element::Header * la_hdr = reinterpret_cast<OpenGL::Element::Header *>(label.Acquire())) {
		ogl_core = reinterpret_cast<OpenGL::Core *>(la_hdr->_context);
		result = la_hdr->x_tra;

		label.Release();

			
		i_val = Strings::Int64ToCStr(reinterpret_cast<char *>(t_str), result, 0);

		Strings::A2W(t_str);

		reinterpret_cast<OpenGL::Text &>(label).Rewrite(t_str, 0, i_val);

		if (ogl_core) ogl_core->ResetElement(label);
	}

	return 0;
}


UI_64 Apps::XFiles::PrepareDirectoryPage(OpenGL::Text & fb_label) {
	wchar_t temp_str[32];
	UI_64 result(0), x_selector(0);
	DWORD f_code(0);
	int cmp_val(0);
	unsigned int jumper(0), last_jump(0), t_id(0), d_pos[8][2] = {{-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}};
	OpenGL::Text cap_label(fb_label);

	cap_label.Object::Left();

	if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(current_path.Acquire())) {
		cp_ptr[current_path_pos] = L'\\';
		cp_ptr[current_path_pos+1] = L'*';
		cp_ptr[current_path_pos+2] = L'\0';

		if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
			_files_runner = 0;
			HANDLE first_file = FindFirstFile(cp_ptr, cf_list);
		
			cp_ptr[current_path_pos] = L'\0';
			current_path.Release();
			cp_ptr = 0;

			OpenGL::Core::GetHeaderString(17, temp_str);
			cap_label.Rewrite(temp_str + 1, 0, temp_str[0]); // L"items found"
						
		
			if (first_file != INVALID_HANDLE_VALUE) {
				System::MemoryFill(cf_list[_files_runner].cAlternateFileName, 14*sizeof(wchar_t), 0);

				cf_list[_files_runner].cAlternateFileName[0] = wcslen(cf_list[_files_runner].cFileName);

				f_code = cf_list[_files_runner].nFileSizeHigh;
				cf_list[_files_runner].nFileSizeHigh = cf_list[_files_runner].nFileSizeLow;
				cf_list[_files_runner].nFileSizeLow = f_code;

				cf_list[_files_runner].dwReserved0 = -1;
				cf_list[_files_runner].dwReserved1 = -1;

				if (cf_list[_files_runner].cAlternateFileName[0] < 130) {
					System::MemoryCopy(cf_list[_files_runner].cFileName + 130, cf_list[_files_runner].cFileName, (cf_list[_files_runner].cAlternateFileName[0]+1)*sizeof(wchar_t));
					CharUpper(cf_list[_files_runner].cFileName + 130);
				}

					
				switch (cf_list[_files_runner].dwFileAttributes & (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM)) {
					case FILE_ATTRIBUTE_DIRECTORY:							
						t_id = 0;							
					break;
					case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM): case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_SYSTEM): case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN):
						cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_HIDDEN;
						cf_list[_files_runner].dwFileAttributes &= ~FILE_ATTRIBUTE_SYSTEM;

						t_id = 1;
					break;
					case (FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM): case FILE_ATTRIBUTE_SYSTEM: case FILE_ATTRIBUTE_HIDDEN:						
						cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_HIDDEN;
						cf_list[_files_runner].dwFileAttributes &= ~FILE_ATTRIBUTE_SYSTEM;

						t_id = 3;
					break;
					default:
						cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_NORMAL;
						t_id = 2;					
				}

				if (t_id & 2) {
					for (unsigned int kk(2);kk<=5;kk++) {
						if (kk < cf_list[_files_runner].cAlternateFileName[0]) {
							if (cf_list[_files_runner].cFileName[cf_list[_files_runner].cAlternateFileName[0] - kk] == L'.') {
								cf_list[_files_runner].cAlternateFileName[1] = kk;

								break;
							}
						} else break;
					}

					if (cf_list[_files_runner].cAlternateFileName[1]) {
						System::MemoryCopy(cf_list[_files_runner].cAlternateFileName+8, cf_list[_files_runner].cFileName + cf_list[_files_runner].cAlternateFileName[0] - cf_list[_files_runner].cAlternateFileName[1], cf_list[_files_runner].cAlternateFileName[1]*sizeof(wchar_t));									
						CharUpper(cf_list[_files_runner].cAlternateFileName+8);

						x_selector = -1;

						if (ext_runner)
						if (void * x_ptr = extention_list.Acquire()) {
							if (x_selector = reinterpret_cast<UI_64>(Strings::SearchBlock(x_ptr, ext_runner*4*sizeof(wchar_t), cf_list[_files_runner].cAlternateFileName+9, 4*sizeof(wchar_t)))) {
								x_selector -= reinterpret_cast<UI_64>(x_ptr);
								x_selector >>= 3;
							} else {
								x_selector = -1;
							}

							extention_list.Release();
						}

						if (x_selector == -1) {
							if (ext_runner >= ext_top) {
								// resize extention blocks


							}

							if (ext_runner < ext_top) {
								x_selector = ext_runner++;

								if (wchar_t * x_ptr = reinterpret_cast<wchar_t *>(extention_list.Acquire())) {
									System::MemoryCopy(x_ptr + x_selector*4, cf_list[_files_runner].cAlternateFileName + 9, 4*sizeof(wchar_t));

									extention_list.Release();
								}

								if (XMapEntry * x_ptr = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {
									System::MemoryFill(x_ptr + x_selector, sizeof(XMapEntry), 0);

									x_ptr[x_selector].x_color[0] = CalcXColor();

									extention_map.Release();
								}
							}
						}
						
						
						if (XMapEntry * x_ptr = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {
							cf_list[_files_runner].cAlternateFileName[2] = x_ptr[x_selector]._reserved;

							app_record[x_ptr[x_selector]._reserved].count_er++;

							extention_map.Release();
						}

						cf_list[_files_runner].cAlternateFileName[3] = x_selector;
					}
				}
					
				cf_list[_files_runner].dwFileAttributes <<= 8;
			
				if (((cf_list[_files_runner].cAlternateFileName[0] == 1) && (cf_list[_files_runner].cFileName[0] == L'.')) ||
					((cf_list[_files_runner].cAlternateFileName[0] == 2) && (cf_list[_files_runner].cFileName[0] == L'.') && (cf_list[_files_runner].cFileName[1] == L'.'))) {

					_files_runner--;
				} else {
					d_pos[t_id][0] = _files_runner;
					d_pos[t_id][1] = _files_runner;
				}


				f_code = 0;

				for (;;) { // f_code != ERROR_NO_MORE_FILES
					if (++_files_runner >= _files_top) {
						current_files.Release();
						cf_list = 0;
						
						if (current_files.Expand(128*sizeof(WIN32_FIND_DATA)) != -1) {
							_files_top += 128;

							cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire());
						}
					}

					if (!cf_list) {
						break;
					}



					if (FindNextFile(first_file, cf_list + _files_runner) == 0) {
						if (f_code = GetLastError()) {
							break;
						}
					}

					System::MemoryFill(cf_list[_files_runner].cAlternateFileName, 14*sizeof(wchar_t), 0);

					cf_list[_files_runner].cAlternateFileName[0] = wcslen(cf_list[_files_runner].cFileName);
					if (((cf_list[_files_runner].cAlternateFileName[0] == 1) && (cf_list[_files_runner].cFileName[0] == L'.')) ||
						((cf_list[_files_runner].cAlternateFileName[0] == 2) && (cf_list[_files_runner].cFileName[0] == L'.') && (cf_list[_files_runner].cFileName[1] == L'.'))) {
							_files_runner--;
							continue;
					}


					f_code = cf_list[_files_runner].nFileSizeHigh;
					cf_list[_files_runner].nFileSizeHigh = cf_list[_files_runner].nFileSizeLow;
					cf_list[_files_runner].nFileSizeLow = f_code;

					cf_list[_files_runner].dwReserved0 = -1;
					cf_list[_files_runner].dwReserved1 = -1;

					if (cf_list[_files_runner].cAlternateFileName[0] < 130) {
						System::MemoryCopy(cf_list[_files_runner].cFileName + 130, cf_list[_files_runner].cFileName, (cf_list[_files_runner].cAlternateFileName[0]+1)*sizeof(wchar_t));
						CharUpper(cf_list[_files_runner].cFileName + 130);
					}
					// sort it

					fb_label.SetXTra(_files_runner);
					
					switch (cf_list[_files_runner].dwFileAttributes & (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM)) {
						case FILE_ATTRIBUTE_DIRECTORY:							
							t_id = 0;							
						break;
						case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM): case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_SYSTEM): case (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_HIDDEN):
							cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_HIDDEN;
							cf_list[_files_runner].dwFileAttributes &= ~FILE_ATTRIBUTE_SYSTEM;

							t_id = 1;
						break;
						case (FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM): case FILE_ATTRIBUTE_SYSTEM: case FILE_ATTRIBUTE_HIDDEN:
							cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_HIDDEN;
							cf_list[_files_runner].dwFileAttributes &= ~FILE_ATTRIBUTE_SYSTEM;

							t_id = 3;
						break;
						default:
							cf_list[_files_runner].dwFileAttributes |= FILE_ATTRIBUTE_NORMAL;
							t_id = 2;					
					}

					if (t_id & 2) {
						for (unsigned int kk(2);kk<=5;kk++) {
							if (kk < cf_list[_files_runner].cAlternateFileName[0]) {
								if (cf_list[_files_runner].cFileName[cf_list[_files_runner].cAlternateFileName[0] - kk] == L'.') {
									cf_list[_files_runner].cAlternateFileName[1] = kk;

									break;
								}
							} else break;
						}

						if (cf_list[_files_runner].cAlternateFileName[1]) {
							System::MemoryCopy(cf_list[_files_runner].cAlternateFileName+8, cf_list[_files_runner].cFileName + cf_list[_files_runner].cAlternateFileName[0] - cf_list[_files_runner].cAlternateFileName[1], cf_list[_files_runner].cAlternateFileName[1]*sizeof(wchar_t));									
							CharUpper(cf_list[_files_runner].cAlternateFileName+8);

							x_selector = -1;

							if (ext_runner)
							if (void * x_ptr = extention_list.Acquire()) {
								if (x_selector = reinterpret_cast<UI_64>(Strings::SearchBlock(x_ptr, ext_runner*4*sizeof(wchar_t), cf_list[_files_runner].cAlternateFileName+9, 4*sizeof(wchar_t)))) {
									x_selector -= reinterpret_cast<UI_64>(x_ptr);
									x_selector >>= 3;
								} else {
									x_selector = -1;
								}

								extention_list.Release();
							}

							if (x_selector == -1) {
								if (ext_runner >= ext_top) {
								// resize extention blocks


								}

								if (ext_runner < ext_top) {
									x_selector = ext_runner++;

									if (wchar_t * x_ptr = reinterpret_cast<wchar_t *>(extention_list.Acquire())) {
										System::MemoryCopy(x_ptr + x_selector*4, cf_list[_files_runner].cAlternateFileName + 9, 4*sizeof(wchar_t));

										extention_list.Release();
									}

									if (XMapEntry * x_ptr = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {
										System::MemoryFill(x_ptr + x_selector, sizeof(XMapEntry), 0);

										x_ptr[x_selector].x_color[0] = CalcXColor();

										extention_map.Release();
									}
								}
							}

							if (XMapEntry * x_ptr = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {
								cf_list[_files_runner].cAlternateFileName[2] = x_ptr[x_selector]._reserved;
								app_record[x_ptr[x_selector]._reserved].count_er++;

								extention_map.Release();
							}

							cf_list[_files_runner].cAlternateFileName[3] = x_selector;
						}
					}

					
					cf_list[_files_runner].dwFileAttributes <<= 8;
					cf_list[_files_runner].dwFileAttributes |= 

					cmp_val = 0;

					last_jump = -1;
					jumper = d_pos[t_id][0];
					
					if (jumper == -1) {
						d_pos[t_id][0] = _files_runner;
						d_pos[t_id][1] = _files_runner;
					}
										
					for (;jumper != -1;) {
						if ((cf_list[_files_runner].cAlternateFileName[0] < 130) && (cf_list[jumper].cAlternateFileName[0] < 130)) {
							cmp_val = Strings::CmpWStr(cf_list[_files_runner].cFileName + 130, cf_list[_files_runner].cAlternateFileName[0], cf_list[jumper].cFileName + 130, cf_list[jumper].cAlternateFileName[0]);
						} else {
							if (cf_list[_files_runner].cAlternateFileName[0] < 130) {
								cmp_val = Strings::CmpWStr(cf_list[_files_runner].cFileName + 130, cf_list[_files_runner].cAlternateFileName[0], cf_list[jumper].cFileName, cf_list[jumper].cAlternateFileName[0]);
							} else {
								if (cf_list[jumper].cAlternateFileName[0] < 130) {
									cmp_val = Strings::CmpWStr(cf_list[_files_runner].cFileName, cf_list[_files_runner].cAlternateFileName[0], cf_list[jumper].cFileName + 130, cf_list[jumper].cAlternateFileName[0]);
								} else {
									cmp_val = Strings::CmpWStr(cf_list[_files_runner].cFileName, cf_list[_files_runner].cAlternateFileName[0], cf_list[jumper].cFileName, cf_list[jumper].cAlternateFileName[0]);
								}
							}
						}

						if (!cmp_val) {
							cmp_val = cf_list[jumper].cAlternateFileName[0];
							cmp_val -= cf_list[_files_runner].cAlternateFileName[0];
						}

						// sort decision
						if (cmp_val > 0) { // test string is below
							if (last_jump != -1) {
								cf_list[last_jump].dwReserved0 = _files_runner;
								cf_list[_files_runner].dwReserved1 = last_jump;
							}
							
							cf_list[_files_runner].dwReserved0 = jumper;
							cf_list[jumper].dwReserved1 = _files_runner;

							if (jumper == d_pos[t_id][0]) d_pos[t_id][0] = _files_runner;
							
							cmp_val = 0;
							break;
						}

						last_jump = jumper;
						jumper = cf_list[jumper].dwReserved0;
						
					}

					if (cmp_val) {
						d_pos[t_id][1] = _files_runner;
						cf_list[last_jump].dwReserved0 = _files_runner;
						cf_list[_files_runner].dwReserved1 = last_jump;
					}					
				}


				// link sorted lists
				cf_list[0].ftCreationTime.dwLowDateTime = 0;
				last_jump = -1;

				for (t_id = 0;t_id<4;t_id++) {
					if (d_pos[t_id][0] != -1) {
						if (last_jump != -1) {
							cf_list[last_jump].dwReserved0 = d_pos[t_id][0];
							cf_list[d_pos[t_id][0]].dwReserved1 = last_jump;
						} else {
							cf_list[0].ftCreationTime.dwLowDateTime = d_pos[t_id][0];
						}
						
						last_jump = d_pos[t_id][1];
					}
				}

				FindClose(first_file);				
			}

			if (cf_list) current_files.Release();
						
		}

		if (cp_ptr) current_path.Release();	
	}

	return result;
}





unsigned int Apps::XFiles::FormatButton(OpenGL::Element & b_el, const OpenGL::Button::Header & but_hdr, unsigned int & f_idx, unsigned int a_id) {
	unsigned int f_type(a_id);


	if (_instance) {		
		if (f_type & 0x0000FFFF) {			
			_instance->SetAppButton(b_el, but_hdr, f_idx, f_type);
		} else _instance->SetFileButton(b_el, but_hdr, f_idx, f_type);

		f_type &= (0x00008000 | 0x00001000 | 0x00000200 | 0x000000FF);
	}

	return f_type;
}

unsigned int Apps::XFiles::ListScanUp(unsigned int & f_idx, UI_64 & oval) {
	unsigned int cf_type(-1);

	if (_instance)
	if (f_idx < _instance->_files_runner)
	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(_instance->current_files.Acquire())) {
		cf_type = (cf_list[f_idx].dwFileAttributes & (0x00008000 | 0x00001000 | 0x00000200 | 0x000000FF));

		if (oval) f_idx = cf_list[f_idx].dwReserved1;
							
		_instance->current_files.Release();

		if (_instance->current_app != 0) oval = 0;
	}

	return cf_type;
}

unsigned int Apps::XFiles::ListScanDown(unsigned int & p_idx, UI_64 & oval) {
	unsigned int cf_type(-1);

	if (_instance)
	if (p_idx < _instance->_files_runner)
	if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(_instance->current_files.Acquire())) {
		p_idx = cf_list[p_idx].dwReserved1;
		if (p_idx != -1)
			cf_type = (cf_list[p_idx].dwFileAttributes & (0x00008000 | 0x00001000 | 0x00000200 | 0x000000FF));
		
		_instance->current_files.Release();
	}

	return cf_type;
}



UI_64 Apps::XFiles::PageSize(OpenGL::Element & app_grid) {
	float box_vals[4] = {0.0, 0.0, 0.0, 0.0};
	OpenGL::Element app_el;
	unsigned int resize_ready(1);
	// set expanded flag to app_grid

	if (app_grid.TestEFlags((UI_64)OpenGL::Element::FlagBuilt)) return -3;

	app_grid.SetEFlags((UI_64)OpenGL::Element::FlagSized);

	if (_instance) {
		app_el(_instance->file_list);
		

		for (unsigned int i(0);i<_instance->app_count;i++) {
			// test xpand flag, set resize ready
			if (app_el.TestEFlags((UI_64)OpenGL::Element::FlagSized)) {

				resize_ready = 0;
				break;
			}
			app_el.Left();
		}

		if (resize_ready) {
			_instance->InitPage(0);

		
			app_el.GetRightMargin(_instance->right_margin);
			_instance->right_margin[0] = _instance->right_margin[2];

			for (unsigned int i(1);i<_instance->app_count;i++) {
				box_vals[0] = _instance->right_margin[0];
				app_el.Left();
				app_el.RelocateXY(box_vals);
				_instance->right_margin[0] += _instance->right_margin[2];
			}

			box_vals[0] = -_instance->right_margin[2]*_instance->current_app;
			app_el(_instance->app_clip);
			app_el.Down();
			app_el.RelocateXY(box_vals);

		}
	}


	return 0;
}


UI_64 Apps::XFiles::Start(OpenGL::Button & fun_but) {
	OpenGL::APPlane a_plane;
	if (_instance) {
		_instance->app_clip.Show();
		_instance->path_list.Show();
		
		_instance->core_ogl->ResetElement(_instance->app_clip);
		_instance->core_ogl->ResetElement(_instance->path_list);
		

		OpenGL::APPlane::ResetFunction(fun_but);
		OpenGL::APPlane::PopApp(a_plane);

	}


	return 0;
}

UI_64 Apps::XFiles::Stop() {	
	OpenGL::Button blankbut;

	if (_instance) {
		_instance->app_clip.Hide();
		_instance->path_list.Hide();

		_instance->core_ogl->ResetElement(_instance->app_clip);
		_instance->core_ogl->ResetElement(_instance->path_list);

		OpenGL::APPlane::ResetFunction(blankbut);
		
	}

	return 0;
}

UI_64 Apps::XFiles::SelectFL() {
	UI_64 result(1);
	OpenGL::Element scene;

	if (_instance) {
		if (_instance->app_clip.IsVisible()) {
			scene = _instance->file_list;
			if (OpenGL::Core * core_ptr = scene.GetContext()) {			
				for (unsigned int i(0);i<_instance->current_app;i++) {
					scene.Left();
				}			
				core_ptr->SetFocus(scene);
			}

			_instance->file_list.SetState(OpenGL::Element::FlagMouseDown);

			result = 0;
		}
	}

	return result;
}
/*
UI_64 Apps::XFiles::Select(OpenGL::Button & fun_but) {
	if (_instance) {
		OpenGL::APPlane::ResetFunction(fun_but);
	}

	return 0;
}

*/

void Apps::XFiles::Change() {
	if (current_path_pos <= 3) {
		InitPage();
		
	}
}


UI_64 Apps::XFiles::AddFilter(SFSco::IGLApp * f_app) {
	UI_64 result(0), _event(0);
	float box_vals[4] = {0.0, 0.0, 0.0, 0.0};
	const wchar_t * xt_list(f_app->GetSupportedList());
	unsigned int xt_count(f_app->GetSupportedCount());

	OpenGL::ButtonGrid filter_bgrid;

	if ((ext_runner + xt_count) >= ext_top) {
		result = extention_list.Expand(256*4*sizeof(wchar_t));
		result |= extention_map.Expand(256*sizeof(XMapEntry));

		if (result != -1) {
			ext_top += 256;
		}
	}
	
	if (ext_top > ext_runner) {
		result = 0;

		if (wchar_t * x_ptr = reinterpret_cast<wchar_t *>(extention_list.Acquire())) {
			if (XMapEntry * x_map = reinterpret_cast<XMapEntry *>(extention_map.Acquire())) {

				if (ext_runner) {

					for (unsigned int i(0);i<xt_count;i++) {
						if (result = reinterpret_cast<UI_64>(Strings::SearchBlock(x_ptr, ext_runner*4*sizeof(wchar_t), xt_list+i*4, 4*sizeof(wchar_t)))) {
							result -= reinterpret_cast<UI_64>(x_ptr);
							result >>= 3;

							x_map[result].app_filter = f_app;
							x_map[result]._reserved = app_count;

						} else {
							System::MemoryCopy(x_ptr + ext_runner*4, xt_list+i*4, 4*sizeof(wchar_t));

							x_map[ext_runner].app_filter = f_app;							
							x_map[ext_runner]._reserved = app_count;
							x_map[ext_runner].x_color[0] = CalcXColor();

							ext_runner++;
						}
					}
				} else {
					System::MemoryCopy(x_ptr,  xt_list, xt_count*4*sizeof(wchar_t));

					for (ext_runner = 0;ext_runner<xt_count;ext_runner++) {						
						x_map[ext_runner].app_filter = f_app;
						x_map[ext_runner]._reserved = app_count;
						x_map[ext_runner].x_color[0] = CalcXColor();
					}
				}
				extention_map.Release();
			}
			extention_list.Release();
		}
	}


// create button grid

	if (core_ogl->CreateButtonGrid(filter_bgrid, &app_clip, &XFiles::PageSize) != -1) {
		// configure grid
		app_record[app_count].count_er = 0;
		app_record[app_count].initiated = 0;
		app_record[app_count].app_ptr = f_app;
		app_record[app_count].f_selector = reinterpret_cast<fs_type>(f_app->GetSelectorProc());

		filter_bgrid.ResetDragOp((UI_64)(OpenGL::Element::FlagY0Drag | OpenGL::Element::FlagY1Drag | OpenGL::Element::FlagXYDrag));
		filter_bgrid.Configure(0xFF282828, box_vals);
		filter_bgrid.SetButtonUpdate(FormatButton, ListScanUp, ListScanDown, app_count++);

		box_vals[0] = 0.1f; box_vals[1] = 0.1f; box_vals[2] = 0.1f; box_vals[3] = 0.65f;
		filter_bgrid.CreateEBorder(1.1f, 0x80000000);
		filter_bgrid.SetEBorderColor(box_vals);

		box_vals[0] = right_margin[0];
		box_vals[1] = 0.0f; box_vals[2] = 0.0f; box_vals[3] = 0.0f;

		filter_bgrid.RelocateXY(box_vals);

		right_margin[0] += right_margin[2];

		f_app->ConfigurePreview(filter_bgrid);
		
		SFSco::Event_SetGate(menu_event, filter_bgrid, (UI_64)OpenGL::Element::FlagMouseDown);

//		filter_bgrid.ClearEFlags((UI_64)OpenGL::Element::FlagBuilt); // if (filter_bgrid.TestEFlags((UI_64)OpenGL::Element::FlagBuilt)) 
		filter_bgrid.XPand();
	}

	return result;
}

UI_64 Apps::XFiles::InitAppGrid() {
	UI_64 icon_id(-1), text_id(-1), fname_id(-1), name_id(-1);
	OpenGL::Element app_grid(file_list), but_el, bb_el;
	SFSco::Object fname_obj;
	unsigned int prev_reset(1);

	for (unsigned int i(0);i<current_app;i++) app_grid.Left();

	but_el(app_grid);
	but_el.Down().Down();

	if (OpenGL::Element::Header * ag_hdr = reinterpret_cast<OpenGL::Element::Header *>(app_grid.Acquire())) {
		OpenGL::ButtonGrid::Header * b_hdr = reinterpret_cast<OpenGL::ButtonGrid::Header *>(ag_hdr + 1);

		for (unsigned int i(0);i<b_hdr->cell_count;i++) {
			bb_el(but_el);
			bb_el.Down();

			if (OpenGL::Element::Header * but_hdr = reinterpret_cast<OpenGL::Element::Header *>(but_el.Acquire())) {
				if (but_hdr->_flags & (UI_64)OpenGL::Element::FlagVisible) {
					if (but_hdr->_flags & (UI_64)OpenGL::Element::FlagBorder) bb_el.Left();

					if (b_hdr->button_set.flags & OpenGL::Button::Header::IconPresent) {
						icon_id = bb_el.GetID();
						bb_el.Left();
					}

					if (b_hdr->button_set.tline_count) {
						text_id = bb_el.GetID();
						name_id = bb_el.Left().GetID();

						if ((but_hdr->x_tra & 0x00FFFFFF) < _files_runner) {
							fname_id = fname_obj.New(0, (current_path_pos + MAX_PATH)*sizeof(wchar_t));

							if (fname_id != -1) {
								if (wchar_t * fn_ptr = reinterpret_cast<wchar_t *>(fname_obj.Acquire())) {
									if (wchar_t * cp_ptr = reinterpret_cast<wchar_t *>(current_path.Acquire())) {
										System::MemoryCopy(fn_ptr, cp_ptr, current_path_pos*sizeof(wchar_t));
										fn_ptr[current_path_pos] = L'\\';

										if (WIN32_FIND_DATA * cf_list = reinterpret_cast<WIN32_FIND_DATA *>(current_files.Acquire())) {
											System::MemoryCopy(fn_ptr + current_path_pos + 1, cf_list[but_hdr->x_tra & 0x00FFFFFF].cFileName, cf_list[but_hdr->x_tra & 0x00FFFFFF].cAlternateFileName[0]*sizeof(wchar_t));
											fn_ptr[current_path_pos + cf_list[but_hdr->x_tra & 0x00FFFFFF].cAlternateFileName[0] + 1] = L'\0';
											current_files.Release();
										}

										current_path.Release();
									}

									fname_obj.Release();
								}
								reinterpret_cast<OpenGL::Button &>(but_el).StartPreview(preview_progress_tid);
								app_record[current_app].app_ptr->CreatePreview(fname_id, but_el, icon_id, text_id, name_id, prev_reset);
								prev_reset = 0;
							}
						}
					}
				}

				but_el.Release();
			}
			but_el.Left();
		}

		app_grid.Release();
	}

	return 0;
}

void Apps::XFiles::TurnLeft(OpenGL::Element & f_but) {	
	float o_val[4] = {0.0, 0.0, 0.0, 0.0};
	OpenGL::Element scene;


	MainMenu::FunctionHoover(f_but);

	if (_instance) {
		scene(_instance->app_clip);
		scene.Down();

		if (OpenGL::Core * core_ptr = scene.GetContext()) {
			if (app_record[0].count_er) {
				for (;;) {
					if (_instance->current_app == 0) _instance->current_app = _instance->app_count;
					_instance->current_app--;
					if (app_record[_instance->current_app].count_er) break;
				}

				o_val[0] = -_instance->right_margin[2]*_instance->current_app;
				scene.RelocateXY(o_val);

				if ((app_record[_instance->current_app].initiated == 0) && (app_record[_instance->current_app].app_ptr)) {
					_instance->InitAppGrid();

					app_record[_instance->current_app].initiated = 1;
				}

				core_ptr->ResetElement(scene);

				scene = _instance->file_list;
				for (unsigned int i(0);i<_instance->current_app;i++) {
					scene.Left();
				}

				core_ptr->SetFocus(scene);

			}
		}
	}
}


void Apps::XFiles::TurnRight(OpenGL::Element & f_but) {
	float o_val[4] = {0.0, 0.0, 0.0, 0.0};
	OpenGL::Element scene;
	
	MainMenu::FunctionHoover(f_but);

	if (_instance) {
		scene(_instance->app_clip);
		scene.Down();

		if (OpenGL::Core * core_ptr = scene.GetContext()) {
			if (app_record[0].count_er) {
				for (;;) {
					_instance->current_app++;
					if (_instance->current_app == _instance->app_count) _instance->current_app = 0;
					
					if (app_record[_instance->current_app].count_er) break;
				}

				o_val[0] = -_instance->right_margin[2]*_instance->current_app;
				scene.RelocateXY(o_val);

				if ((app_record[_instance->current_app].initiated == 0) && (app_record[_instance->current_app].app_ptr)) {
					_instance->InitAppGrid();

					app_record[_instance->current_app].initiated = 1;
				}

				core_ptr->ResetElement(scene);

				scene = _instance->file_list;
				for (unsigned int i(0);i<_instance->current_app;i++) {
					scene.Left();
				}

				core_ptr->SetFocus(scene);
			}
		}
	}
}

void Apps::XFiles::RollDown(OpenGL::Element & f_but) {
	OpenGL::ButtonGrid b_grid;
	
	MainMenu::FunctionHoover(f_but);

	if (_instance) {
		b_grid(_instance->file_list);

		for (unsigned int api(0);api<_instance->current_app;api++) b_grid.Left();
		
		OpenGL::ButtonGrid::PrevPage(b_grid);
	}
}

void Apps::XFiles::RollUp(OpenGL::Element & f_but) {
	OpenGL::ButtonGrid b_grid;
	
	MainMenu::FunctionHoover(f_but);

	if (_instance) {
		b_grid(_instance->file_list);

		for (unsigned int api(0);api<_instance->current_app;api++) b_grid.Left();
		
		OpenGL::ButtonGrid::NextPage(b_grid);
	}
}



UI_64 Apps::XFiles::CreatePreview(UI_64, SFSco::Object &, UI_64, UI_64, UI_64, unsigned int) {
	return 0;
}

UI_64 Apps::XFiles::ConfigurePreview(SFSco::Object &) {
	return 0;
}

const wchar_t * Apps::XFiles::GetSupportedList() {
	return 0;
}

unsigned int Apps::XFiles::GetSupportedCount() {
	return 0;
}

void Apps::XFiles::ProgressCircle0(float * motion_t, unsigned int) {
	Calc::FloatCopy16(motion_t, progress_transform[0], 1);
}

void Apps::XFiles::ProgressCircle1(float * motion_t, unsigned int) {
	Calc::MF_Multiply44(progress_transform[1], 4, motion_t, motion_t);

}


UI_64 Apps::XFiles::Test(const wchar_t *) {
	return 0;
}


/*

wchar_t volume_guid[256];
				HANDLE first_volume = FindFirstVolume(volume_guid, 256);
				if (first_volume != INVALID_HANDLE_VALUE) {
					DWORD v_code = GetLastError();

					local_d_count = 0;

					for (;v_code != ERROR_NO_MORE_FILES;local_d_count++) {
						switch (GetDriveType(volume_guid)) {
							case DRIVE_REMOVABLE:

							break;
							case DRIVE_FIXED:

							break;
							case DRIVE_REMOTE:

							break;
							case DRIVE_CDROM:

							break;
						}

						if (GetVolumeInformation(volume_guid, disk_s, 256, 0, &maximum_file_length, 0, disk_s+256, 256)) {
							GetDiskFreeSpaceEx(volume_guid, 0, &total_bytes, &free_bytes);
						} else {
							ecode = GetLastError(); // 21 - not ready
						}
						
						FindNextVolume(first_volume, volume_guid, 256);
						v_code = GetLastError();
					}


					FindVolumeClose(first_volume);
				}
*/

