#pragma once

#include "OpenGL\OpenGL.h"
#include "CoreLibrary\SFScore.h"

namespace Apps {

class NotificationArea {
	private:
		static SYSTEMTIME current_time;
		static OpenGL::Element::LabelCfg action_list_labels[4], aapp_list_labels[2], play_list_labels[2], lb_info_labels[2];
		static OpenGL::Button::PaletteEntry action_list_palette[2], aapp_list_palette[2], play_list_palette[2];

		static SFSco::Object info_collection, apps_collection;
		static unsigned int ie_runner, ie_top, ie_current, ap_runner, ap_top, ap_op, ao_current, ao_reset;
		static UI_64 pl_pointer, pl_set_val;

		static OpenGL::Element notification_clip;

		static OpenGL::Text clock_line_1, clock_line_2;

		static OpenGL::ListBoxClip list_clip;
		static OpenGL::ListBoxX play_list, action_list, aapp_list;
		static OpenGL::APPlane current_app;

		static unsigned int FormatActionButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction);
		static unsigned int FormatAppButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction);
		static unsigned int FormatPlayItemButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int dircetion);
		static UI_64 SelectPLItem(OpenGL::ButtonGrid &, UI_64);
		static UI_64 SelectALItem(OpenGL::ButtonGrid &, UI_64);

		NotificationArea();
		~NotificationArea();



	public:		
		struct AppEntry {
			static const UI_64 type_id = 982451179;			
			OpenGL::Element a_plane;
		};

		static UI_64 Refresh(unsigned int);
		static UI_64 Create(OpenGL::Core *, unsigned int, float, float);
		static void Resize(int);

		static unsigned int AddInfoEntry(const SFSco::InfoEntry &);
		static unsigned int AddAppEntry(const OpenGL::APPlane &);
		static void RemoveAppEntry(const OpenGL::Element &);
		static void SetPlayList(const OpenGL::APPlane &);
};



}

