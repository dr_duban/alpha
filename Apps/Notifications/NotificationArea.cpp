#include "Apps\NotificationArea.h"
#include "Apps\MainMenu.h"

#include "System\SysUtils.h"

#include "Media\Source.h"
#include "Pics\Image.h"

SYSTEMTIME Apps::NotificationArea::current_time;

OpenGL::Element::LabelCfg Apps::NotificationArea::action_list_labels[4], Apps::NotificationArea::aapp_list_labels[2], Apps::NotificationArea::play_list_labels[2], Apps::NotificationArea::lb_info_labels[2];
OpenGL::Button::PaletteEntry Apps::NotificationArea::action_list_palette[2], Apps::NotificationArea::aapp_list_palette[2], Apps::NotificationArea::play_list_palette[2];

OpenGL::Element Apps::NotificationArea::notification_clip;

OpenGL::Text Apps::NotificationArea::clock_line_1, Apps::NotificationArea::clock_line_2;

OpenGL::ListBoxClip Apps::NotificationArea::list_clip;
OpenGL::ListBoxX Apps::NotificationArea::play_list, Apps::NotificationArea::action_list, Apps::NotificationArea::aapp_list;

OpenGL::APPlane Apps::NotificationArea::current_app;

unsigned int Apps::NotificationArea::ao_reset = 2;

UI_64 Apps::NotificationArea::pl_pointer = -1;
UI_64 Apps::NotificationArea::pl_set_val = -2;

void Apps::NotificationArea::Resize(int hh) {
	float temp_vals[4];
	
	temp_vals[0] = 0.0f;
	temp_vals[1] = 0.0;
	temp_vals[2] = 256.0f;
	temp_vals[3] = hh - 224.0f;
	list_clip.CoordsReset(temp_vals, 0, 0);

}

UI_64 Apps::NotificationArea::Create(OpenGL::Core * core_ogl, unsigned int time_font, float c_w, float c_h) {
	UI_64 result(-1);
	float box_vals[4] = {0.0f, 0.0f, 0.0f, 0.0f}, color_val[4] = {0.0, 0.0, 0.0, 0.0};
	OpenGL::Button clock_but;
	OpenGL::AttributeArray a_arr;
	OpenGL::Element t_elem;

	if (notification_clip) return -2;


	System::MemoryFill(&current_time, sizeof(SYSTEMTIME), 0);

	result = core_ogl->CreateElement(OpenGL::Element::ClipRegion, notification_clip);
	if (result != -1) {
		notification_clip.SetColor(core_ogl->GetBgColor());
		t_elem = notification_clip;
		t_elem.Down();
		t_elem.ClearEFlags(OpenGL::Element::FlagMovable);

		if (core_ogl->GetMenuCorner() & 1) {
			// notification_area.ConfigureClip(LeftTopFront, FitWidth, system_icos, db_rect[2], 3.0f);
						
		} else {
			notification_clip.SetAnchorMode(OpenGL::LeftBottomFront);		

			core_ogl->GetRootBox(color_val);
			notification_clip.InitAttributeArray(OpenGL::AttributeArray::Vertex, a_arr);
			if (OpenGL::AttributeArray::Header * at_hdr = reinterpret_cast<OpenGL::AttributeArray::Header *>(a_arr.Acquire())) {
//							at_hdr->translate_vector[1] = 0.0f;

				at_hdr->stretch_mask[0] = 0;
				at_hdr->stretch_mask[1] = -1;
				at_hdr->stretch_mask[2] = 0;

				reinterpret_cast<float *>(at_hdr + 1)[4] = reinterpret_cast<float *>(at_hdr + 1)[12] = c_w;
				reinterpret_cast<float *>(at_hdr + 1)[9] = reinterpret_cast<float *>(at_hdr + 1)[13] = color_val[1];

				a_arr.Release();

				a_arr.CalculateBox(OpenGL::LeftBottomFront);
			}
		}
			
		

		result = core_ogl->CreateButton(clock_but, c_w, c_h, &notification_clip);

		if (result != -1) {
			clock_but.SetAnchorMode(OpenGL::LeftBottomFront);

			result = core_ogl->CreateText(L"2015.03.13", clock_line_1, &clock_but);
			if (result != -1) {
				color_val[0] = 0.6f; color_val[1] = 0.5f; color_val[2] = 0.5f;

				clock_line_1.SetStyle(OpenGL::Text::Mono);
				clock_line_1.SetFont(time_font); // 
				clock_line_1.SetSize(12.1f);
				clock_line_1.SetTextColor(color_val);

				clock_line_1.SetAnchorMode(OpenGL::RightBottomFront);

				box_vals[0] = 10.0f;
				box_vals[1] = 20.0f;
				box_vals[2] = 0.0f;
				box_vals[3] = 0.0f;
				clock_line_1.Move(box_vals);

				result = core_ogl->CreateText(L"00:00:00", clock_line_2, &clock_but);
				if (result != -1) {					
					color_val[0] = 0.7f; color_val[1] = 0.7f; color_val[2] = 0.6f;

					clock_line_2.SetStyle(OpenGL::Text::Mono);
					clock_line_2.SetFont(time_font); //
					clock_line_2.SetSize(25.0f);
					clock_line_2.SetTextColor(color_val);

					clock_line_2.SetAnchorMode(OpenGL::RightBottomFront);

					box_vals[0] = 7.0f;
					box_vals[1] = 50.0f;
					box_vals[2] = 0.0f;
					box_vals[3] = 0.0f;
					clock_line_2.Move(box_vals);
				}
			}



















			System::MemoryFill(lb_info_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);

			OpenGL::Core::ColorConvert(lb_info_labels[0].c_color, 0xFFAAEEBB);
			OpenGL::Core::ColorConvert(lb_info_labels[0].hc_color, 0xFF000000);
			OpenGL::Core::ColorConvert(lb_info_labels[0].dc_color, 0xFFFFEBC8);

			lb_info_labels[0].f_size = 15.5f;
			lb_info_labels[0].font_id = -1; // MainMenu::GetTimeFont(); // 
			lb_info_labels[0].font_style = OpenGL::Text::Regular;
			lb_info_labels[0].a_type = OpenGL::LeftBottomFront;
			lb_info_labels[0].max_char_count = 24;


			OpenGL::Core::ColorConvert(lb_info_labels[1].c_color, 0xFFAAEEBB);
			OpenGL::Core::ColorConvert(lb_info_labels[1].hc_color, 0xFFFFEBC8);
			OpenGL::Core::ColorConvert(lb_info_labels[1].dc_color, 0xFFA0A0CC);

			lb_info_labels[1].f_size = 15.5f;
			lb_info_labels[1].font_id = -1; //MainMenu::GetTimeFont(); // 
			lb_info_labels[1].font_style = OpenGL::Text::Regular;
			lb_info_labels[1].a_type = OpenGL::LeftBottomFront;
			lb_info_labels[1].max_char_count = 24;

					

			result = core_ogl->CreateLBXClip(list_clip, 251, 44, 0, &notification_clip);									
			
			list_clip.SetAnchorMode(OpenGL::LeftTopFront);
			result = list_clip.CreateLBX(action_list, 0, lb_info_labels);

			if (result != -1) {
				System::MemoryFill(action_list_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

				OpenGL::Core::ColorConvert(action_list_palette[0].color, 0xFF303030);
				OpenGL::Core::ColorConvert(action_list_palette[0].h_color, 0xFFA0C0FF); // 0xFFE0FFA0
				OpenGL::Core::ColorConvert(action_list_palette[0].d_color, 0xFF200000);
	
				OpenGL::Core::ColorConvert(action_list_palette[0].i_color, 0xFF202020);
				OpenGL::Core::ColorConvert(action_list_palette[0].hi_color, 0xFF404040);
				OpenGL::Core::ColorConvert(action_list_palette[0].di_color, 0xFF662626);
	

				action_list_palette[1].ico_xcol[0] = 0x0000; action_list_palette[1].ico_xcol[1] = 0x7F0F; action_list_palette[1].ico_xcol[2] = 0; // up/down 73DF
				OpenGL::Core::ColorConvert(action_list_palette[1].color, 0xFF000030);
				OpenGL::Core::ColorConvert(action_list_palette[1].h_color, 0xFF000000);
				OpenGL::Core::ColorConvert(action_list_palette[1].d_color, 0xFFA8E0FF);

				OpenGL::Core::ColorConvert(action_list_palette[1].i_color, 0xFF202020); // up/down icon color
				OpenGL::Core::ColorConvert(action_list_palette[1].hi_color, 0xFF000000);
				OpenGL::Core::ColorConvert(action_list_palette[1].di_color, 0xFFA8E0FF);



				System::MemoryFill(action_list_labels, 4*sizeof(OpenGL::Element::LabelCfg), 0);
				OpenGL::Core::ColorConvert(action_list_labels[0].c_color, 0xFF800000);
				OpenGL::Core::ColorConvert(action_list_labels[0].hc_color, 0xFF200000); // 0xFFAA8000
				OpenGL::Core::ColorConvert(action_list_labels[0].dc_color, 0xFFFFAABB);
	
				action_list_labels[0].f_size = 13.0001f;
				action_list_labels[0].font_id = -1;
				action_list_labels[0].font_style = OpenGL::Text::Regular;
				action_list_labels[0].a_type = OpenGL::LeftBottomFront;
				action_list_labels[0].max_char_count = 30;


				OpenGL::Core::ColorConvert(action_list_labels[1].c_color, 0xFFAAEEBB);
				OpenGL::Core::ColorConvert(action_list_labels[1].hc_color, 0xFFACBBEE); // 0xFFAA8000
				OpenGL::Core::ColorConvert(action_list_labels[1].dc_color, 0xFF000040);
	
				action_list_labels[1].f_size = 13.0002f;
				action_list_labels[1].font_id = -1;
				action_list_labels[1].font_style = OpenGL::Text::Regular;
				action_list_labels[1].a_type = OpenGL::LeftBottomFront;
				action_list_labels[1].max_char_count = 30;
					

				OpenGL::Core::ColorConvert(action_list_labels[2].c_color, 0xFFFFFFFF);
				OpenGL::Core::ColorConvert(action_list_labels[2].hc_color, 0xFF01C0FF); // 0xFFAA8000
				OpenGL::Core::ColorConvert(action_list_labels[2].dc_color, 0xFFFFFFFF);
	
				action_list_labels[2].f_size = 10.5003f;
				action_list_labels[2].font_id = -1;
				action_list_labels[2].font_style = OpenGL::Text::Regular;
				action_list_labels[2].a_type = OpenGL::LeftBottomFront;
				action_list_labels[2].max_char_count = 40;

				OpenGL::Core::ColorConvert(action_list_labels[3].c_color, 0xFFFFFFFF);
				OpenGL::Core::ColorConvert(action_list_labels[3].hc_color, 0xFF01C0FF); // 0xFFAA8000
				OpenGL::Core::ColorConvert(action_list_labels[3].dc_color, 0xFF400000);
	
				action_list_labels[3].f_size = 10.5003f;
				action_list_labels[3].font_id = -1;
				action_list_labels[3].font_style = OpenGL::Text::Regular;
				action_list_labels[3].a_type = OpenGL::LeftBottomFront;
				action_list_labels[3].max_char_count = 40;

				action_list.Configure(action_list_palette);

				box_vals[0] = 250.0f;
				box_vals[1] = 40.0f;
				box_vals[2] = -1.0f; // border offset, negative - no border
				box_vals[3] = -2.0f; // grid border

				action_list.ConfigureGrid(FormatActionButton, 0, box_vals, action_list_palette, 0); // no stretch
				action_list.ConfigureGridText(action_list_labels);
				action_list.ConfigureGridText(action_list_labels + 2);
			}

			if (result != -1) { // app box, my favor
				result = list_clip.CreateLBX(aapp_list, 0, lb_info_labels + 1);
				if (result != -1) {
					aapp_list.Hide();

					System::MemoryFill(aapp_list_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

					OpenGL::Core::ColorConvert(aapp_list_palette[0].color, 0xFF181818);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].h_color, 0xFF404040);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].d_color, 0xFFA8E0FF);

					OpenGL::Core::ColorConvert(aapp_list_palette[0].i_color, 0xFF181818);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].hi_color, 0xFF404040);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].di_color, 0xFFA8E0FF);
	
					OpenGL::Core::ColorConvert(aapp_list_palette[0].b_color, 0xFF000000);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].hb_color, 0xFF000000);
					OpenGL::Core::ColorConvert(aapp_list_palette[0].db_color, 0xFF000000);

					aapp_list_palette[1].ico_xcol[0] = 0x0000; aapp_list_palette[1].ico_xcol[1] = 0x7F0F; aapp_list_palette[1].ico_xcol[2] = 0; // up/down 73DF

					OpenGL::Core::ColorConvert(aapp_list_palette[1].color, 0xFF181818);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].h_color, 0xFF404040);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].d_color, 0xFFA8E0FF);

					OpenGL::Core::ColorConvert(aapp_list_palette[1].i_color, 0xFF181818);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].hi_color, 0xFF404040);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].di_color, 0xFFA8E0FF);
	
					OpenGL::Core::ColorConvert(aapp_list_palette[1].b_color, 0xFF000000);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].hb_color, 0xFF000000);
					OpenGL::Core::ColorConvert(aapp_list_palette[1].db_color, 0xFF000000);


					System::MemoryFill(aapp_list_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);
					OpenGL::Core::ColorConvert(aapp_list_labels[0].c_color, 0xFF800000);
					OpenGL::Core::ColorConvert(aapp_list_labels[0].hc_color, 0xFF200000); // 0xFFAA8000
					OpenGL::Core::ColorConvert(aapp_list_labels[0].dc_color, 0xFFFFAABB);
	
					aapp_list_labels[0].f_size = 10.5006f;
					aapp_list_labels[0].font_id = -1;
					aapp_list_labels[0].font_style = OpenGL::Text::Regular;
					aapp_list_labels[0].a_type = OpenGL::LeftBottomFront;
					aapp_list_labels[0].max_char_count = 40;


					OpenGL::Core::ColorConvert(aapp_list_labels[1].c_color, 0xFFAAEEBB);
					OpenGL::Core::ColorConvert(aapp_list_labels[1].hc_color, 0xFFACBBEE); // 0xFFAA8000
					OpenGL::Core::ColorConvert(aapp_list_labels[1].dc_color, 0xFF000040);
	
					aapp_list_labels[1].f_size = 10.5007f;
					aapp_list_labels[1].font_id = -1;
					aapp_list_labels[1].font_style = OpenGL::Text::Regular;
					aapp_list_labels[1].a_type = OpenGL::LeftBottomFront;
					aapp_list_labels[1].max_char_count = 40;
					

					aapp_list.Configure(aapp_list_palette);

					box_vals[0] = 220.0002f;
					box_vals[1] = 180.0f;
					box_vals[2] = -5.0f; // border offset, negative - no border
					box_vals[3] = -6.0f; // grid border

					aapp_list.ConfigureGrid(FormatAppButton, SelectALItem, box_vals, aapp_list_palette, 0x40000000); // no stretch
					aapp_list.ConfigureGridText(aapp_list_labels);
					aapp_list.ConfigureGridIcon();
				}
			}


			if (result != -1) {
				result = list_clip.CreateLBX(play_list, 0, lb_info_labels);

				if (result != -1) {
					play_list.Hide();

					System::MemoryFill(play_list_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

					OpenGL::Core::ColorConvert(play_list_palette[0].color, 0xFF303030); // 0xFFF0BA74
					OpenGL::Core::ColorConvert(play_list_palette[0].h_color, 0xFFA0C0FF); // 0xFFFFE0A0
					OpenGL::Core::ColorConvert(play_list_palette[0].d_color, 0xFF400000);
	
					OpenGL::Core::ColorConvert(play_list_palette[0].i_color, 0xFF181818);
					OpenGL::Core::ColorConvert(play_list_palette[0].hi_color, 0xFF404040);
					OpenGL::Core::ColorConvert(play_list_palette[0].di_color, 0xFF662626);
	

					play_list_palette[1].ico_xcol[0] = 0x0000; play_list_palette[1].ico_xcol[1] = 0x5299; play_list_palette[1].ico_xcol[2] = 0; // up/down 73DF 7F0F
					OpenGL::Core::ColorConvert(play_list_palette[1].color, 0xFFD0F0F0);
					OpenGL::Core::ColorConvert(play_list_palette[1].h_color, 0xFFA0C0FF);
					OpenGL::Core::ColorConvert(play_list_palette[1].d_color, 0xFFA8E0FF);

					OpenGL::Core::ColorConvert(play_list_palette[1].i_color, 0xFF181818); // up/down icon color
					OpenGL::Core::ColorConvert(play_list_palette[1].hi_color, 0xFF000000);
					OpenGL::Core::ColorConvert(play_list_palette[1].di_color, 0xFFA8E0FF);

					OpenGL::Core::ColorConvert(play_list_palette[1].b_color, 0xFFB4EB38);
					OpenGL::Core::ColorConvert(play_list_palette[1].hb_color, 0xFFB4EB38);
					OpenGL::Core::ColorConvert(play_list_palette[1].db_color, 0xFFB4EB38);


					System::MemoryFill(play_list_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);
					OpenGL::Core::ColorConvert(play_list_labels[0].c_color, 0xFF80C0C0);
					OpenGL::Core::ColorConvert(play_list_labels[0].hc_color, 0xFF200000); // 0xFFAA8000
					OpenGL::Core::ColorConvert(play_list_labels[0].dc_color, 0xFFFFAABB);
	
					play_list_labels[0].f_size = 13.5004f;
					play_list_labels[0].font_id = -1; // MainMenu::GetTimeFont();
					play_list_labels[0].font_style = OpenGL::Text::Regular;
					play_list_labels[0].a_type = OpenGL::LeftBottomFront;
					play_list_labels[0].max_char_count = 30;


					OpenGL::Core::ColorConvert(play_list_labels[1].c_color, 0xFF000010);
					OpenGL::Core::ColorConvert(play_list_labels[1].hc_color, 0xFF200000); // 0xFFAA8000
					OpenGL::Core::ColorConvert(play_list_labels[1].dc_color, 0xFF000040);
	
					play_list_labels[1].f_size = 13.5005f;
					play_list_labels[1].font_id = -1; // MainMenu::GetTimeFont();
					play_list_labels[1].font_style = OpenGL::Text::Regular;
					play_list_labels[1].a_type = OpenGL::LeftBottomFront;
					play_list_labels[1].max_char_count = 30;
					


					play_list.Configure(play_list_palette);

					box_vals[0] = 240.0001f;
					box_vals[1] = 32.0001f;
					box_vals[2] = -3.0f; // border offset, negative - no border
					box_vals[3] = -4.0f; // grid border

					play_list.ConfigureGrid(FormatPlayItemButton, SelectPLItem, box_vals, play_list_palette, 0x80000000); // no stretch
					play_list.ConfigureGridText(play_list_labels);
										
				}
			}
		}

		core_ogl->GetRootBox(box_vals);
		box_vals[0] = 3.0;
		box_vals[2] = 256.001f;		
		box_vals[3] = box_vals[1] - 224.0f;
		box_vals[1] -= 68.0f;

		list_clip.CoordsReset(box_vals, 0);

	}


	return result;
}


unsigned int Apps::NotificationArea::FormatActionButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	OpenGL::Core * core_ogl(g_but.GetContext());
	wchar_t temp_str[256];
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	
	OpenGL::Element but_el(g_but);
	OpenGL::ButtonGrid b_grid;

	unsigned int plt_idx(0), s_size[2], mu_id(0), o_val(0);


	b_grid(g_but).Up().Up();
	

	if (direction & 0x80000000) list_id--;
	else list_id++;

	if (list_id == 0) {
		plt_idx = 1;

		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
			reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

			b_grid.Release();
		}
	}

	but_el.SetColor(action_list_palette[plt_idx].color);
				
	but_el.Down();

	if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
		but_el.SetColor(action_list_palette[plt_idx].b_color);

		but_el.Left();
	}

// label					
	if (list_id < ie_runner) {
		if (SFSco::InfoEntry * ie_ptr = reinterpret_cast<SFSco::InfoEntry *>(info_collection.Acquire())) {
			mu_id = ie_ptr[list_id].ie_type;

			if (const unsigned int * str_table = core_ogl->GetStringsPtr()) {
				o_val = (str_table[mu_id*2] >> 1);
				s_size[0] = str_table[mu_id*2 + 1];				
				for (unsigned int i(0);i<s_size[0];i++) {
					temp_str[i] = reinterpret_cast<const wchar_t *>(str_table)[o_val + i]; 
				}

				mu_id++;
				o_val = (str_table[mu_id*2] >> 1);
				s_size[1] = str_table[mu_id*2 + 1];
				for (unsigned int i(0);i<s_size[1];i++) {
					temp_str[64 + i] = reinterpret_cast<const wchar_t *>(str_table)[o_val + i]; 
				}							

				core_ogl->ReleaseStringsPtr();
			}

			

			if (ie_ptr[list_id].val_type) {
				temp_str[64 + s_size[1]++] = L' ';
				
				switch (ie_ptr[list_id].val_type) {
					case 1:
						o_val = Strings::UInt64ToCStr_SSSE3(reinterpret_cast<char *>(temp_str + 64 + s_size[1]), ie_ptr[list_id].ie_val, 0);			
					break;
					case 2:
						o_val = Strings::Int64ToCStr_SSSE3(reinterpret_cast<char *>(temp_str + 64 + s_size[1]), ie_ptr[list_id].ie_val, 0);
					break;
					case 3:
						o_val = Strings::TimeToCStr(reinterpret_cast<char *>(temp_str + 64 + s_size[1]), ie_ptr[list_id].ie_val);
					break;
					case 4:
						o_val = Strings::DoubleToCStr_SSSE3(reinterpret_cast<char *>(temp_str + 64 + s_size[1]), reinterpret_cast<double &>(ie_ptr[list_id].ie_val), 0); // sli_hdr->v_type & (~SliderX::ValTypeMask)
					break;
					case 5:
						o_val = Strings::IntToCStr_SSSE3(reinterpret_cast<char *>(temp_str + 64 + s_size[1]), reinterpret_cast<double &>(ie_ptr[list_id].ie_val)*100.0, 0);

						reinterpret_cast<char *>(temp_str + 64 + s_size[1])[o_val++] = '%';
						reinterpret_cast<char *>(temp_str + 64 + s_size[1])[o_val] = '\0';
				
					break;
				}

				Strings::A2W(temp_str + 64 + s_size[1]);

				s_size[1] += o_val;
			}


			info_collection.Release();
		}
		
		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(temp_str, 0, s_size[0]);

		if (action_list_labels[plt_idx].t_color[0]) but_el.SetColor(action_list_labels[plt_idx].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(action_list_labels[plt_idx].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = plt_idx;

			g_but.Release();
		}

		g_but.GetRightMargin(some_vals);
		but_el.GetRightMargin(some_vals + 4);

		some_vals[0] = 19.0f; // 0.5f*(some_vals[2] - some_vals[6]);
		some_vals[1] = 4.01f - some_vals[7]; // 0.5f*(some_vals[3] - some_vals[7]);

		but_el.RelocateXY(some_vals);


		but_el.Left();
		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(temp_str + 64, 0, s_size[1]);

		if (action_list_labels[2 + plt_idx].t_color[0]) but_el.SetColor(action_list_labels[2 + plt_idx].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(action_list_labels[2 + plt_idx].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = plt_idx;

			g_but.Release();
		}

		some_vals[0] = 34.0f;
		some_vals[1] *= 1.91f; // 0.5f*(some_vals[3] - some_vals[7]);

		but_el.RelocateXY(some_vals);

	} else {
		list_id = -1;
	}

	return 1;
}

unsigned int Apps::NotificationArea::FormatAppButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	unsigned int result(1);
	UI_64 t_val(0);
	OpenGL::Core * core_ogl(g_but.GetContext());
	wchar_t temp_str[16] = {L'A', L'c', L't', L'i', L'v', L'e', L' ', L'V', L'i', L'e', L'w', L's', 0};
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	
	OpenGL::Element but_el(g_but);
	OpenGL::ButtonGrid b_grid;
	OpenGL::Button ap_but;

	unsigned int plt_idx(0), mu_id(0), o_val(0);


	b_grid(g_but).Up().Up();
	

	if (direction & 0x80000000) list_id--;
	else list_id++;

	if (list_id == 0) {
		plt_idx = 1;

		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
			reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

			b_grid.Release();
		}
	}

	but_el.SetColor(aapp_list_palette[plt_idx].color);
				
	but_el.Down();

	if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
		but_el.SetColor(aapp_list_palette[plt_idx].b_color);

		but_el.Left();
	}

// label					
	if (list_id < ap_runner) {
		if (OpenGL::Element * apl_ptr = reinterpret_cast<OpenGL::Element *>(apps_collection.Acquire())) {
			if (apl_ptr[list_id]) {
				ap_but(g_but);

				t_val = reinterpret_cast<OpenGL::APPlane &>(apl_ptr[list_id]).GetFirstScreen(some_vals);

// adjust vals				
				some_vals[6] = (160.0f/some_vals[3]);
				some_vals[7] = (200.0f/some_vals[2]);
				
				if (some_vals[6] > some_vals[7]) {
					some_vals[6] = 200.0f;
					some_vals[7] *= some_vals[3];

				} else {
					some_vals[6] *= some_vals[2];
					some_vals[7] = 160.0f;
				}
				
				some_vals[4] = 0.5f*(220.0f - some_vals[6]);
				some_vals[5] = 0.5f*(180.0f - some_vals[7]);

				ap_but.ConfigureXIcon(t_val, some_vals);
			} else {
				result = 0;
			}

			apps_collection.Release();

		}

		but_el.Left();
		
		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(temp_str, 0, 12);

		if (action_list_labels[0].t_color[0]) but_el.SetColor(action_list_labels[0].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(action_list_labels[0].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = 0;

			g_but.Release();
		}

		some_vals[0] = 270.0f; // 0.5f*(some_vals[2] - some_vals[6]);
		some_vals[1] = 0.0f; // 0.5f*(some_vals[3] - some_vals[7]);

		but_el.RelocateXY(some_vals);

	} else {
		list_id = -1;
	}


	return result;
}

UI_64 Apps::NotificationArea::SelectALItem(OpenGL::ButtonGrid & b_grid, UI_64 b_id) {
	unsigned int empty_val(1);

	if (OpenGL::Core * core_ogl = b_grid.GetContext()) {
		if (b_id < ap_runner) {
			if (OpenGL::Element * apl_ptr = reinterpret_cast<OpenGL::Element *>(apps_collection.Acquire())) {
				if (apl_ptr[b_id]) {
					apl_ptr[b_id].Show();
					
					if (OpenGL::Element::Header * ap_hdr = reinterpret_cast<OpenGL::Element::Header *>(apl_ptr[b_id].Acquire())) {
						OpenGL::APPlane::ResetFunction(reinterpret_cast<OpenGL::APPlane::Header *>(ap_hdr+1)->fun_but);
						apl_ptr[b_id].Release();
					}					
					
					core_ogl->ResetElement(apl_ptr[b_id]);
					
					apl_ptr[b_id].Blank();

					for (unsigned int i(0);i<ap_runner;i++) {
						if (apl_ptr[i]) {
							empty_val = 0;
							break;
						}
					}

					if (empty_val) ap_runner = 0;

					ap_op++;
				}

				apps_collection.Release();
			}
		}
		core_ogl->ResetElement(b_grid);
	}

	return 0;
}


void Apps::NotificationArea::SetPlayList(const OpenGL::APPlane & c_app) {

	if (c_app) {
		if (OpenGL::Element::Header * al_hdr = reinterpret_cast<OpenGL::Element::Header *>(const_cast<OpenGL::APPlane &>(c_app).Acquire())) {
			OpenGL::APPlane::Header * app_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(al_hdr + 1);

			if (app_hdr->deco_rec.program) reinterpret_cast<Media::Program *>(app_hdr->deco_rec.program)->AudioViewOn();

			const_cast<OpenGL::APPlane &>(c_app).Release();
		}

		current_app = c_app;
		play_list.Show();
	} else {
		current_app.Blank();
		play_list.Hide();
	}	

	if (OpenGL::Core * core_ogl = list_clip.GetContext()) {
		core_ogl->ResetElement(play_list);
	}

	pl_pointer = -1;
	pl_set_val = -2;
}

unsigned int Apps::NotificationArea::FormatPlayItemButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	float some_vals[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	wchar_t temp_str[64];

	OpenGL::Element but_el(g_but);
	OpenGL::ButtonGrid b_grid;

	unsigned int plt_idx(0), f_length(0), o_val(0);


	b_grid(g_but).Up().Up();


	if (direction & 0x80000000) list_id--;
	else list_id++;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header * >(current_app.Acquire())) {
		if (const wchar_t * pl_ptr = reinterpret_cast<const wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->play_list.Acquire())) {
			const UI_64 * offset_table = reinterpret_cast<const UI_64 *>(pl_ptr + reinterpret_cast<const UI_64 *>(pl_ptr)[0]);

			if (list_id < offset_table[0]) {

				if (reinterpret_cast<const UI_64 *>(pl_ptr)[1] == (list_id + 1)) {
					plt_idx = 1;
				}

				f_length = pl_ptr[offset_table[list_id + 1]];
				if (f_length > play_list_labels[0].max_char_count) f_length = play_list_labels[0].max_char_count;

				System::MemoryCopy(temp_str, pl_ptr + offset_table[list_id + 1] + 1, f_length*sizeof(wchar_t));
				temp_str[f_length] = L'\0';

			} else {
				list_id = -1;
			}

			reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->play_list.Release();
		}

		current_app.Release();
	}

	if ((list_id != -1) && (f_length)) {
		if (plt_idx)
		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
			reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

			b_grid.Release();
		}
					
		but_el.SetColor(play_list_palette[plt_idx].color);
				
		but_el.Down();

		if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
			but_el.SetColor(play_list_palette[plt_idx].b_color);

			but_el.Left();
		}


		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(temp_str, 0, f_length);

		if (play_list_labels[plt_idx].t_color[0]) but_el.SetColor(play_list_labels[plt_idx].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(play_list_labels[plt_idx].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = plt_idx;

			g_but.Release();
		}

		g_but.GetRightMargin(some_vals);
		but_el.GetRightMargin(some_vals + 4);

		some_vals[0] = (some_vals[2] - some_vals[6] - 3.0f);
		some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);

		but_el.RelocateXY(some_vals);

	}

	return 1;
}

UI_64 Apps::NotificationArea::SelectPLItem(OpenGL::ButtonGrid &, UI_64 s_id) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagSSNext;
	de_rec.pic_id = s_id;
	de_rec.element_obj = current_app;

	pl_set_val = s_id + 1;

	Media::Program::Decode(de_rec);
	Pics::Image::Decode(de_rec);

	return 0;
}

UI_64 Apps::NotificationArea::Refresh(unsigned int t_count) {
	float temp_vals[4];
	UI_64 f_time(0);

	wchar_t t_str[256];
	SYSTEMTIME n_time;
	int l1u(0), l2u(0), l3u(0);

	OpenGL::ButtonGrid b_grid;
	OpenGL::Element lilem;

	
	if (OpenGL::Core * core_ogl = notification_clip.GetContext()) {
		if ((t_count & 0x0F) == 0) { // time update

			GetLocalTime(&n_time);
			GetSystemTimeAsFileTime(reinterpret_cast<FILETIME *>(&f_time));
			SFSco::TimerCal(f_time);


			if (n_time.wYear != current_time.wYear) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wYear, 0);
				Strings::A2W(t_str);
				clock_line_1.Rewrite(t_str, 0);

				current_time.wYear = n_time.wYear;
				l1u = 1;
			}
		
			if (n_time.wMonth != current_time.wMonth) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wMonth, 2);
				Strings::A2W(t_str);
				clock_line_1.Rewrite(t_str, 5);

				current_time.wMonth = n_time.wMonth;
				l1u = 1;
			}


			if (n_time.wDay != current_time.wDay) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wDay, 2);
				Strings::A2W(t_str);
				clock_line_1.Rewrite(t_str, 8);

				current_time.wDay = n_time.wDay;
				l1u = 1;
			}



			if (n_time.wHour != current_time.wHour) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wHour, 2);
				Strings::A2W(t_str);
				clock_line_2.Rewrite(t_str, 0);

				current_time.wHour = n_time.wHour;
				l2u = 1;
			}

			if (n_time.wMinute != current_time.wMinute) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wMinute, 2);
				Strings::A2W(t_str);
				clock_line_2.Rewrite(t_str, 3);

				current_time.wMinute = n_time.wMinute;
				l2u = 1;
			}

			if (n_time.wSecond != current_time.wSecond) {
				Strings::IntToCStr(reinterpret_cast<char *>(t_str), n_time.wSecond, 2);
				Strings::A2W(t_str);
				clock_line_2.Rewrite(t_str, 6);

				current_time.wSecond = n_time.wSecond;
				l2u = 1;
			}
		

			if (l1u) {
				core_ogl->ResetElement(clock_line_1);
			}

			if (l2u) {
				core_ogl->ResetElement(clock_line_2);
			}

		}


		if ((t_count & 7) == 0) { // info update
			if (ie_runner != ie_current) {
				ie_current = ie_runner;
				
				lilem = action_list;
				lilem.Down();

				b_grid(lilem).Right();
				b_grid.ResetGrid(ie_current);

				if (lilem.IsVisible()) { // face is visible
					lilem.Down();

					if (SFSco::InfoEntry * ie_ptr = reinterpret_cast<SFSco::InfoEntry *>(info_collection.Acquire())) {
						l1u = ie_ptr[ie_current - 1].ie_type;

						if (const unsigned int * str_table = core_ogl->GetStringsPtr()) {
							l2u = (str_table[l1u*2] >> 1);
							l1u = str_table[l1u*2 + 1];				
							for (int i(0);i<l1u;i++) {
								t_str[i] = reinterpret_cast<const wchar_t *>(str_table)[l2u + i]; 
							}

							core_ogl->ReleaseStringsPtr();
						}

						info_collection.Release();
					
						reinterpret_cast<OpenGL::Text &>(lilem).Rewrite(t_str, 0, l1u);

						lilem.GetRightMargin(temp_vals);
					
						temp_vals[0] = 0.5f*(250.0f - temp_vals[2]);
						temp_vals[1] = 0.5f*(44.0f + action_list_labels[0].f_size);
					
						lilem.RelocateXY(temp_vals);						
					}
					core_ogl->ResetElement(action_list);
				}
			}



			if (ap_op != ao_current) {
				ao_current = ap_op;

				if (ap_runner) {
					if (ao_reset) aapp_list.Show();

					lilem = aapp_list;
					lilem.Down();

					b_grid(lilem).Right();

					b_grid.ResetGrid(ap_runner);

					if (lilem.IsVisible()) { // face is visible
						lilem.Down();

						t_str[0] = L'A'; t_str[1] = L'c'; t_str[2] = L't'; t_str[3] = L'i'; t_str[4] = L'v'; t_str[5] = L'e'; t_str[6] = L' '; t_str[7] = L'V'; t_str[8] = L'i'; t_str[9] = L'e'; t_str[10] = L'w'; t_str[11] = L's'; t_str[12] = 0;
						reinterpret_cast<OpenGL::Text &>(lilem).Rewrite(t_str, 0, 12);

						lilem.GetRightMargin(temp_vals);
					
						temp_vals[0] = 0.5f*(250.0f - temp_vals[2]);
						temp_vals[1] = 0.5f*(44.0f + action_list_labels[0].f_size);
					
						lilem.RelocateXY(temp_vals);
					}

					core_ogl->ResetElement(aapp_list);

				} else {
					ao_reset = 1;
					aapp_list.Hide();
				}

				if (ao_reset) {
					core_ogl->GetRootBox(temp_vals);
					temp_vals[0] = 3.0;
					temp_vals[2] = 256.001f;		
					temp_vals[3] = temp_vals[1] - 224.0f;
					temp_vals[1] -= 68.0f;

					list_clip.CoordsReset(temp_vals, 0);
					core_ogl->ResetElement(list_clip);

					if (++ao_reset > 2) ao_reset = 0;
				}
			}

			if (current_app) {
				l1u = l2u = l3u = 0;
				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header * >(current_app.Acquire())) {
					if (const wchar_t * pl_ptr = reinterpret_cast<const wchar_t *>(reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->play_list.Acquire())) {
						if (reinterpret_cast<const UI_64 *>(pl_ptr)[1] != pl_pointer) {
							const UI_64 * offset_table = reinterpret_cast<const UI_64 *>(pl_ptr + reinterpret_cast<const UI_64 *>(pl_ptr)[0]);
							// move op

							l1u = offset_table[0];
							if (pl_pointer == -1) {
								pl_pointer = 1;
								
							} else {
								l1u = -l1u;
							}

							l3u = reinterpret_cast<const UI_64 *>(pl_ptr)[1] - pl_pointer;														

							pl_pointer = reinterpret_cast<const UI_64 *>(pl_ptr)[1];

							if (pl_pointer == pl_set_val) {
								l3u = 0;
							}
							pl_set_val = -2;

							l2u = pl_ptr[offset_table[pl_pointer]];
							
							if (l2u > play_list_labels[0].max_char_count) l2u = play_list_labels[0].max_char_count;

							System::MemoryCopy(t_str, pl_ptr + offset_table[pl_pointer] + 1, l2u*sizeof(wchar_t));							

							
						}

						reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->play_list.Release();
					}

					current_app.Release();
				}

				if (l1u) {
					lilem = play_list;
					lilem.Down();

					b_grid(lilem).Right();

					if (l1u > 0) b_grid.ResetGrid(l1u);
					if (l3u) b_grid.Jump(l3u, pl_pointer - 1);

					if (lilem.IsVisible()) { // face is visible
						lilem.Down();

						reinterpret_cast<OpenGL::Text &>(lilem).Rewrite(t_str, 0, l2u);

						lilem.GetRightMargin(temp_vals);
					
						temp_vals[0] = 0.5f*(250.0f - temp_vals[2]);
						temp_vals[1] = 0.5f*(44.0f + action_list_labels[0].f_size);
					
						lilem.RelocateXY(temp_vals);
					}


					core_ogl->ResetElement(play_list);

				}
			}
		}
	}

	return 0;
}

// list updators =====================================================================================================================================================================================================================================
SFSco::Object Apps::NotificationArea::info_collection;
SFSco::Object Apps::NotificationArea::apps_collection;

unsigned int Apps::NotificationArea::ie_runner = 0;
unsigned int Apps::NotificationArea::ie_top = 0;
unsigned int Apps::NotificationArea::ie_current = 0;

unsigned int Apps::NotificationArea::ap_runner = 0;
unsigned int Apps::NotificationArea::ap_top = 0;
unsigned int Apps::NotificationArea::ap_op = 0;
unsigned int Apps::NotificationArea::ao_current = 0;


unsigned int Apps::NotificationArea::AddInfoEntry(const SFSco::InfoEntry & i_block) {
	UI_64 result(0);
	if (ie_runner >= ie_top) {
		if (ie_top) {
			result = info_collection.Expand(256*sizeof(SFSco::InfoEntry));
		} else {
			result = info_collection.New(SFSco::InfoEntry::type_id, 256*sizeof(SFSco::InfoEntry));
		}

		if (result != -1) {
			ie_top += 256;
		}
	}

	if (ie_runner < ie_top) {
		if (SFSco::InfoEntry * iel_ptr = reinterpret_cast<SFSco::InfoEntry *>(info_collection.Acquire())) {
			iel_ptr[System::LockedInc_32(&ie_runner)] = i_block;

			info_collection.Release();

		}
	}
	return ie_runner;
}


unsigned int Apps::NotificationArea::AddAppEntry(const OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	unsigned int r_idx(-1);
	
	if (OpenGL::Element * apl_ptr = reinterpret_cast<OpenGL::Element *>(apps_collection.Acquire())) {
		for (unsigned int i(0);i<ap_runner;i++) {
			if (!apl_ptr[i]) {				
				r_idx = i;
				break;
			}
		}
		apps_collection.Release();
	}
/*
	if (r_idx != -1) {
		ap_op++;
		return r_idx;
	}
*/
	if (r_idx == -1) {
		if (ap_runner >= ap_top) {
			if (ie_top) {
				result = apps_collection.Expand(32*sizeof(OpenGL::Element));
			} else {
				result = apps_collection.New(AppEntry::type_id, 32*sizeof(OpenGL::Element));
			}

			if (result != -1) {
				ap_top += 32;
			}
		}

		if (ap_runner < ap_top) {
			r_idx = System::LockedInc_32(&ap_runner);
		}
	}

	
	if (r_idx != -1) {
		if (OpenGL::Element * apl_ptr = reinterpret_cast<OpenGL::Element *>(apps_collection.Acquire())) {			
			apl_ptr[r_idx] = a_plane; // , sizeof(AppEntry));

			apps_collection.Release();
		}

		if (OpenGL::Element::Header * al_hdr = reinterpret_cast<OpenGL::Element::Header *>(const_cast<OpenGL::APPlane &>(a_plane).Acquire())) {
			OpenGL::APPlane::Header * app_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(al_hdr + 1);

			if (app_hdr->deco_rec.program) reinterpret_cast<Media::Program *>(app_hdr->deco_rec.program)->AudioViewOff();

			const_cast<OpenGL::APPlane &>(a_plane).Release();
		}
	}



	ap_op++;
	return r_idx;
}

void Apps::NotificationArea::RemoveAppEntry(const OpenGL::Element & a_plane) {
	unsigned int empty_val(1);

	if (OpenGL::Element * apl_ptr = reinterpret_cast<OpenGL::Element *>(apps_collection.Acquire())) {		
		for (unsigned int i(0);i<ap_runner;i++) {
			if (apl_ptr[i] == a_plane) {

				apl_ptr[i].Blank();
				break;
			} else {
				if (apl_ptr[i]) empty_val = 0;
			}
		}			
		apps_collection.Release();

		if (empty_val) ap_runner = 0;
	}
	ap_op++;
}

