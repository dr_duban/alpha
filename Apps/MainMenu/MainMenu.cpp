/*
** safe-fail alpha main menu
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Apps\MainMenu.h"
#include "System\SysUtils.h"

#include "Compression\Deflate.h"
#include "OpenGL\Font.h"

#include "Pics\Image.h"
#include "Apps\NotificationArea.h"


OpenGL::Button::PaletteEntry Apps::MainMenu::don_palette;
OpenGL::Element::LabelCfg Apps::MainMenu::don_label;

unsigned int Apps::MainMenu::text_font = -1;
unsigned int Apps::MainMenu::time_font = -1;


Apps::MainMenu::MainMenu() : function_group_id(-1), sequence_on(1), core_ogl(0), _libin(0), list_top(2), l_button(-1), cf_handle(INVALID_HANDLE_VALUE) {

}

Apps::MainMenu::~MainMenu() {

}

void * Apps::MainMenu::GetSelectorProc() {
	return 0;
}

unsigned int Apps::MainMenu::GetTextFont() {
	return text_font;
}

unsigned int Apps::MainMenu::GetTimeFont() {
	return time_font;
}


UI_64 Apps::MainMenu::Initialize(SFSco::IOpenGL ** glctx, void * sd_ptr, void * mm_ptr) {
	float color_val[4] = {0.0, 0.0, 0.0, 1.0f}, trans_vec[4] = {0.0, 0.0, 0.0, 0.0};
	
	wchar_t lib_name[1024];
	UI_64 result(-1), ln_length(0), _event(-1), e_val(0);
	OpenGL::Button _button, abutton;
	OpenGL::Element info_el, do_but;
	SFSco::Object temp_obj; //, hoover_event, leave_event, down_event;
	
	if (!glctx) return -52;


	if (!core_ogl)
	if (core_ogl = reinterpret_cast<OpenGL::Core *>(*glctx)) {
		// load fonts

		if (_libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {
			GetModuleFileName(_libin, lib_name, 1024);

			ln_length = wcslen(lib_name);
			lib_name[ln_length++] = L'@';

			// load resource
			result = 0;
			if (Pack::Deflate * deflator = new Pack::Deflate()) {
				if (void * rc_font_ptr = LockResource(LoadResource(_libin, FindResource(_libin, MAKEINTRESOURCE(13000), MAKEINTRESOURCE(1024))))) {				
					deflator->Reset();

					deflator->SetUSource(reinterpret_cast<unsigned char *>(rc_font_ptr), SizeofResource(_libin, FindResource(_libin, MAKEINTRESOURCE(13000), MAKEINTRESOURCE(1024))));
					result = deflator->UnPack(temp_obj);

					if ((result) && (deflator->Success())) {
						if (unsigned char * fnt_ptr = reinterpret_cast<unsigned char *>(VirtualAlloc(0, result, MEM_COMMIT, PAGE_READWRITE))) {
							if (void * ff_ptr = temp_obj.Acquire()) {
								System::MemoryCopy_SSE3(fnt_ptr, ff_ptr, result);

								temp_obj.Release();
							}
					
							lib_name[ln_length] = L't';
							lib_name[ln_length+1] = L'e';
							lib_name[ln_length+2] = L'x';
							lib_name[ln_length+3] = L't';

							lib_name[ln_length+4] = L'_';

							lib_name[ln_length+5] = L'f';
							lib_name[ln_length+6] = L'o';
							lib_name[ln_length+7] = L'n';
							lib_name[ln_length+8] = L't';
							lib_name[ln_length+9] = L'.';
							lib_name[ln_length+10] = L'd';
							lib_name[ln_length+11] = L'f';
							lib_name[ln_length+12] = L'l';
							lib_name[ln_length+13] = 0;

							text_font = OpenGL::Font::Create(lib_name, fnt_ptr, RC_OTF_FONT, result);
							if (text_font != -1) OpenGL::Font::InitializeComplete(text_font, fnt_ptr, result);

							VirtualFree(fnt_ptr, 0, MEM_RELEASE);
						}
					}					
				}

				if (void * rc_font_ptr = LockResource(LoadResource(_libin, FindResource(_libin, MAKEINTRESOURCE(13001), MAKEINTRESOURCE(1024))))) {
					deflator->Reset();

					deflator->SetUSource(reinterpret_cast<unsigned char *>(rc_font_ptr), SizeofResource(_libin, FindResource(_libin, MAKEINTRESOURCE(13001), MAKEINTRESOURCE(1024))));
					result = deflator->UnPack(temp_obj);

					if ((result) && (deflator->Success())) {
						if (unsigned char * fnt_ptr = reinterpret_cast<unsigned char *>(VirtualAlloc(0, result, MEM_COMMIT, PAGE_READWRITE))) {
							if (void * ff_ptr = temp_obj.Acquire()) {
								System::MemoryCopy_SSE3(fnt_ptr, ff_ptr, result);

								temp_obj.Release();
							}
					
							lib_name[ln_length] = L't';
							lib_name[ln_length+1] = L'i';
							lib_name[ln_length+2] = L'm';
							lib_name[ln_length+3] = L'e';

							lib_name[ln_length+4] = L'_';

							lib_name[ln_length+5] = L'f';
							lib_name[ln_length+6] = L'o';
							lib_name[ln_length+7] = L'n';
							lib_name[ln_length+8] = L't';
							lib_name[ln_length+9] = L'.';
							lib_name[ln_length+10] = L'd';
							lib_name[ln_length+11] = L'f';
							lib_name[ln_length+12] = L'l';
							lib_name[ln_length+13] = 0;


							time_font = OpenGL::Font::Create(lib_name, fnt_ptr, 0, result);
							if (time_font != -1) OpenGL::Font::InitializeComplete(time_font, fnt_ptr, result);

							VirtualFree(fnt_ptr, 0, MEM_RELEASE);
						}
					}					
				}

				delete deflator;
			}

//			if (result == 0) {


				if (result != -1) {
					result = core_ogl->CreateAPPlane(info_plane);
					if (result != -1) {
						info_plane.Hide();
				
						info_plane.Configure(OpenGL::APPlane::NoCP, 0xB0000000, 255.0f);
				
						if (info_plane.SetPicture(info_el, core_ogl->GetInfoBox(), core_ogl->GetInfoTID()) != -1) {
					
							_event = SFSco::Event_CreateDrain(HideInfo, &info_plane, 0);
							SFSco::Event_SetGate(_event, info_el, (UI_64)(OpenGL::Element::FlagLostFocus | OpenGL::Element::FlagLeave));


							if (core_ogl->CreateElement(OpenGL::Element::Quad, do_but, 4, &info_el) != -1) {
								// set texture to ppal

								trans_vec[0] = 330.0f;
								trans_vec[1] = 235.0f;

								trans_vec[2] = 102.0f;
								trans_vec[3] = 48.0f;

								do_but.SetRect(trans_vec);
								do_but.RelocateXY(trans_vec);

								System::MemoryFill(&don_palette, sizeof(OpenGL::Button::PaletteEntry), 0);

								OpenGL::Core::ColorConvert(don_palette.color, 0xFFA8E0FF);
								OpenGL::Core::ColorConvert(don_palette.h_color, 0xFF2CACFF);
								OpenGL::Core::ColorConvert(don_palette.d_color, 0xFF663300);

								OpenGL::Core::ColorConvert(don_palette.b_color, 0x20800000);
								OpenGL::Core::ColorConvert(don_palette.hb_color, 0x40800000);
								OpenGL::Core::ColorConvert(don_palette.db_color, 0xFF2CACFF);
						
								do_but.SetColor(don_palette.color);
	
								do_but.CreateEBorder(1.1f, 0x80000000);
								do_but.SetEBorderColor(don_palette.b_color);

								System::MemoryFill(&don_label, sizeof(OpenGL::Element::LabelCfg), 0);

								don_label.offset[0] = 12.0f; don_label.offset[1] = 30.0f;
								don_label.font_id = text_font;
								don_label.font_style = OpenGL::Text::Regular;
								don_label.a_type = OpenGL::LeftTopFront;
								don_label.f_size = 19.5f;
								don_label.max_char_count = 8;

								OpenGL::Core::ColorConvert(don_label.c_color, 0xFF993300);
								OpenGL::Core::ColorConvert(don_label.hc_color, 0xFF000000);
								OpenGL::Core::ColorConvert(don_label.dc_color, 0xFFE0FFFF);

								do_but.AddLabel(don_label);


								_event = SFSco::Event_CreateDrain(DDoover, &do_but, 0);
								SFSco::Event_SetGate(_event, do_but, (UI_64)OpenGL::Element::FlagHoover);

								_event = SFSco::Event_CreateDrain(DDeave, &do_but, 0);
								SFSco::Event_SetGate(_event, do_but, (UI_64)OpenGL::Element::FlagLeave);

								_event = SFSco::Event_CreateDrain(DDown, &do_but, 0);
								SFSco::Event_SetGate(_event, do_but, (UI_64)OpenGL::Element::FlagMouseDown);

								_event = SFSco::Event_CreateDrain(DDonate, &do_but, 0);
								SFSco::Event_SetGate(_event, do_but, (UI_64)OpenGL::Element::FlagMouseUp);
								do_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);


								do_but.Down().Left();

								OpenGL::Core::GetHeaderString(1, lib_name);
								reinterpret_cast<OpenGL::Text &>(do_but).Rewrite(lib_name + 1, 0, lib_name[0]);
							}
						}
					}
				}
		
				if (result != -1) {
					result = core_ogl->CreateAPPlane(control_plane);
					if (result != -1) {
						control_plane.Hide();

						InitCPPalette();
						result = FormatCP();
					}
				}

				if (result != -1) result = NotificationArea::Create(core_ogl, time_font, 256.0f, 64.0f);
				
				if (result != -1) {

			// add action button to notification area, most right
					if (OpenGL::APPlane::AddFunction(abutton, -1, 64.00002f) != -1) {
						color_val[0] = 0.15f; color_val[1] = 0.15f; color_val[2] = 0.15f;
						abutton.SetColor(color_val);
				
						abutton.SetXColor(0xF3FF);
						abutton.ConfigureClipButton(OpenGL::Button::ClipCorner | 13100, _libin);


						abutton.SetDefaultBlink(8, 40, 0x01000000, 0xFFE6FFFF, 0x0000F3FF);
						core_ogl->SetBlink(abutton, 2);


						_event = SFSco::Event_CreateDrain(ExitHoover, &abutton, 0);
						SFSco::Event_SetGate(_event, abutton, (UI_64)OpenGL::Element::FlagHoover);				
				
						_event = SFSco::Event_CreateDrain(AlphaLeave, &abutton, 0);
						SFSco::Event_SetGate(_event, abutton, (UI_64)OpenGL::Element::FlagLeave);
				

						_event = SFSco::Event_CreateDrain(ExitDown, &abutton, 0);
						SFSco::Event_SetGate(_event, abutton, (UI_64)OpenGL::Element::FlagMouseDown);
				
	

			// add functions
/* contacts
				if (core_ogl->AddFunction(_button, function_group_id, 64.0f) != -1) {

					color_val[0] = 0.2f; color_val[1] = 0.2f; color_val[2] = 0.2f;
					_button.SetColor(color_val);

					_button.Configure(0x00020000 | 13102, libin);				

					_button.Hide();
					
			// set event
					_event = SFSco::Event_CreateDrain(FunctionHoover, &_button, 0);
					SFSco::Event_SetGate(_event, _button, OpenGL::Element::Hoover);
					

					_event = SFSco::Event_CreateDrain(FunctionLeave, &_button, 0);
					SFSco::Event_SetGate(_event, _button, OpenGL::Element::Leave);
				

					_event = SFSco::Event_CreateDrain(FunctionDown, &_button, 0);
					SFSco::Event_SetGate(_event, _button, OpenGL::Element::MouseDown);
				}


*/

			// about
					function_group_id = -1;
					result = 0x00000400;
					result <<= 32;
					result |= function_group_id;

					function_group_id = OpenGL::APPlane::AddFunction(_button, result, 64.00001f);
					if (function_group_id != -1) {
					// settings
						color_val[0] = 0.2f; color_val[1] = 0.2f; color_val[2] = 0.2f;
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00020000 | 13104, _libin);

						_button.Hide();

					// set event
						_event = SFSco::Event_CreateDrain(FunctionHoover, &_button, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagHoover);
				

						_event = SFSco::Event_CreateDrain(FunctionLeave, &_button, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagLeave);
				

						_event = SFSco::Event_CreateDrain(FunctionDown, &_button, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseDown);
				
						_event = SFSco::Event_CreateDrain(ShowInfo, &info_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


						_event = SFSco::Event_CreateDrain(AlphaUp, &_button, 0);
						SFSco::Event_SetGate(_event, abutton, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						list_top = 3;

					// settings
						if (AddFunction(_button, 13103)) {
							_event = SFSco::Event_CreateDrain(ShowCP, &control_plane, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
							_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);
						}



					// maximize
						if (AddFunction(_button, 13105)) {
							_event = SFSco::Event_CreateDrain(mm_ptr, 0, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
							_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);
						}

					// exit button
			
						if (OpenGL::APPlane::AddFunction(_button, function_group_id, 64.00003f) != -1) {
							color_val[0] = 0.15f; color_val[1] = 0.15f; color_val[2] = 0.15f;
							_button.SetColor(color_val);
							
							_button.SetXColor(0x0018);
							_button.ConfigureClipButton(OpenGL::Button::ClipSide | 13101, _libin);

							_button.Hide();


						// set event
							_event = SFSco::Event_CreateDrain(ExitHoover, &_button, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagHoover);
				

							_event = SFSco::Event_CreateDrain(ExitLeave, &_button, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagLeave);
				
				
							_event = SFSco::Event_CreateDrain(ExitDown, &_button, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseDown);
				

							_event = SFSco::Event_CreateDrain(sd_ptr, 0, 0);
							SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
							_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);


							result = 0;
						}
					}
				}
			}		
		}
	}

	return result;
}


UI_64 Apps::MainMenu::AddFunction(OpenGL::Button & el_but, unsigned int res_id) {
	float color_val[4] = {0.2f, 0.2f, 0.2f, 1.0f};
	UI_64 result(0);
	
	if ((core_ogl) && (function_group_id != -1)) {
		if (OpenGL::APPlane::AddFunction(el_but, function_group_id, 64.0f) != -1) {
			el_but.ShiftRight(l_button);

			l_button = el_but.GetID();
			
			el_but.SetColor(color_val);
			
			el_but.ConfigureClipButton(res_id | (list_top++ << 16), _libin);

			el_but.Hide();

			SetFunctionHandlers(el_but);

			result = 1;
		}
	}


	return result;
}

void Apps::MainMenu::SetFunctionHandlers(OpenGL::Button & but_el) {
	UI_64 _event(-1);

	_event = SFSco::Event_CreateDrain(FunctionHoover, &but_el, 0);
	SFSco::Event_SetGate(_event, but_el, (UI_64)OpenGL::Element::FlagHoover);
				

	_event = SFSco::Event_CreateDrain(FunctionLeave, &but_el, 0);
	SFSco::Event_SetGate(_event, but_el, (UI_64)OpenGL::Element::FlagLeave);
				

	_event = SFSco::Event_CreateDrain(FunctionDown, &but_el, 0);
	SFSco::Event_SetGate(_event, but_el, (UI_64)OpenGL::Element::FlagMouseDown);


}

void Apps::MainMenu::AlphaUp(OpenGL::Element & fbut) {
	if (OpenGL::Core * core_ptr = fbut.GetContext()) {
		OpenGL::APPlane::FunctionAlive();

		OpenGL::APPlane::ResetFunction(fbut);
	}
}

void Apps::MainMenu::FunctionHoover(OpenGL::Element & bobj) {
	float xcol[4] = {0.2f, 0.2f, 0.2f, 1.0f};

	OpenGL::APPlane::FunctionAlive();
	if (OpenGL::Core * core_ptr = bobj.GetContext()) {		

		bobj.SetXColor(0xF3FF);
		bobj.SetColor(xcol);
		
		core_ptr->ResetElement(bobj);		
	}

}

void Apps::MainMenu::FunctionLeave(OpenGL::Element & bobj) {
	float xcol[4] = {0.2f, 0.2f, 0.2f, 1.0f};

	if (OpenGL::Core * core_ptr = bobj.GetContext()) {
		bobj.SetXColor(0);
		bobj.SetColor(xcol);

		core_ptr->ResetElement(bobj);
	}

}

void Apps::MainMenu::FunctionDown(OpenGL::Element & bobj) {
	float xcol[4] = {1.0f, 1.0f, 0.9f, 1.0f};

	OpenGL::APPlane::FunctionAlive();
	if (OpenGL::Core * core_ptr = bobj.GetContext()) {		

		bobj.SetXColor(0);
		bobj.SetColor(xcol);

		core_ptr->ResetElement(bobj);
	}


}

void Apps::MainMenu::ExitHoover(OpenGL::Element & bobj) {
	float xcol[4] = {0.1f, 0.1f, 0.1f, 1.0f};

	OpenGL::APPlane::FunctionAlive();
	if (OpenGL::Core * core_ptr = bobj.GetContext()) {		

		bobj.SetXColor(0x001F);
		bobj.SetColor(xcol);
		
		core_ptr->ResetElement(bobj);
	}

}

void Apps::MainMenu::ExitDown(OpenGL::Element & bobj) {
	float xcol[4] = {1.0f, 1.0f, 0.9f, 1.0f};

	OpenGL::APPlane::FunctionAlive();
	if (OpenGL::Core * core_ptr = bobj.GetContext()) {	

		bobj.SetXColor(0);
		bobj.SetColor(xcol);

		core_ptr->ResetElement(bobj);
	}

}

void Apps::MainMenu::ExitLeave(OpenGL::Element & bobj) {
	float xcol[4] = {0.15f, 0.15f, 0.15f, 1.0f};

	if (OpenGL::Core * core_ptr = bobj.GetContext()) {
		bobj.SetXColor(0x0018);
		bobj.SetColor(xcol);

		core_ptr->ResetElement(bobj);
	}

}



void Apps::MainMenu::AlphaLeave(OpenGL::Element & bobj) {
	float xcol[4] = {0.15f, 0.15f, 0.15f, 1.0f};

	if (OpenGL::Core * core_ptr = bobj.GetContext()) {
		bobj.SetXColor(0xF3FF);
		bobj.SetColor(xcol);

		core_ptr->ResetElement(bobj);
	}

}


UI_64 Apps::MainMenu::Finalize() {
	UI_64 result(0);

	core_ogl = 0;

	return result;
}

void Apps::MainMenu::ShowInfo(OpenGL::APPlane & i_plane) {
	OpenGL::Element im_el(i_plane);

	i_plane.Pop();
	i_plane.Show();
	
	if (OpenGL::Core * core_ptr = i_plane.GetContext()) {
		im_el.Down().Left().Down();
		core_ptr->SetFocus(im_el);

		im_el.SetState((UI_64)OpenGL::Element::FlagLeave);
		
		core_ptr->ResetElement(i_plane);

	}
}


void Apps::MainMenu::HideInfo(OpenGL::APPlane & i_plane) {
	i_plane.Hide();	
		
	if (OpenGL::Core * core_ptr = i_plane.GetContext()) {
		core_ptr->ResetElement(i_plane);

	}
}

void Apps::MainMenu::DDoover(OpenGL::Element & b_el) {
	OpenGL::Element t_el(b_el);

	b_el.SetColor(don_palette.h_color);

	t_el.Down();
	t_el.SetColor(don_palette.hb_color);

	t_el.Left();
	reinterpret_cast<OpenGL::Text &>(t_el).SetTextColor(don_label.hc_color);

	if (OpenGL::Core * core_ptr = b_el.GetContext()) {
		core_ptr->ResetElement(b_el);
	}
}

void Apps::MainMenu::DDown(OpenGL::Element & b_el) {
	OpenGL::Element t_el(b_el);
	b_el.SetColor(don_palette.d_color);

	t_el.Down();
	t_el.SetColor(don_palette.db_color);

	t_el.Left();
	reinterpret_cast<OpenGL::Text &>(t_el).SetTextColor(don_label.dc_color);

	if (OpenGL::Core * core_ptr = b_el.GetContext()) {
		core_ptr->ResetElement(b_el);
	}

}

void Apps::MainMenu::DDeave(OpenGL::Element & b_el) {
	OpenGL::Element t_el(b_el);
	b_el.SetColor(don_palette.color);	

	t_el.Down();
	t_el.SetColor(don_palette.b_color);

	t_el.Left();
	reinterpret_cast<OpenGL::Text &>(t_el).SetTextColor(don_label.c_color);
	
	if (OpenGL::Core * core_ptr = b_el.GetContext()) {
		core_ptr->ResetElement(b_el);
	}
}

void Apps::MainMenu::DDonate(OpenGL::Element & b_el) {
	wchar_t temp_str[512];
	OpenGL::Element t_el(b_el);
	b_el.SetColor(don_palette.h_color);

	t_el.Down();
	t_el.SetColor(don_palette.hb_color);

	t_el.Left();
	reinterpret_cast<OpenGL::Text &>(t_el).SetTextColor(don_label.hc_color);

	if (OpenGL::Core * core_ptr = b_el.GetContext()) {
		core_ptr->ResetElement(b_el);
	}

	OpenGL::Core::GetHeaderString(2, temp_str);
	OpenGL::Core::GetHeaderString(0, temp_str + 32);
	ShellExecute(0, temp_str + 1, temp_str + 33, 0, 0, SW_SHOWNORMAL); // L"open"
}

void Apps::MainMenu::Change() {

}

UI_64 Apps::MainMenu::AddFilter(SFSco::IGLApp *) {
	return 0;
}

UI_64 Apps::MainMenu::CreatePreview(UI_64, SFSco::Object &, UI_64, UI_64, UI_64, unsigned int) {
	return 0;
}

UI_64 Apps::MainMenu::ConfigurePreview(SFSco::Object &) {
	return 0;
}

const wchar_t * Apps::MainMenu::GetSupportedList() {
	return 0;
}

unsigned int Apps::MainMenu::GetSupportedCount() {
	return 0;
}

UI_64 Apps::MainMenu::Test(const wchar_t *) {
	return 0;
}

