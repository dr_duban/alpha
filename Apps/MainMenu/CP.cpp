/*
** safe-fail alpha main menu
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/

#include "Apps\MainMenu.h"
#include "System\SysUtils.h"

#include "Audio\AudioRenderer.h"
#include "Media\CodedSource.h"

#include "Pics\Image.h"

OpenGL::Button::Config Apps::MainMenu::exit_but_cfg, Apps::MainMenu::switch_but_cfg[2];
OpenGL::Element::LabelCfg Apps::MainMenu::cp_head_label, Apps::MainMenu::cp_section_label, Apps::MainMenu::cp_option_label, Apps::MainMenu::cp_switch_labels[2], Apps::MainMenu::cp_slider_label, Apps::MainMenu::cp_lbox_labels[4];
OpenGL::Button::PaletteEntry Apps::MainMenu::cp_slider_palette[2], Apps::MainMenu::cp_lbox_palette[2], Apps::MainMenu::cp_lbgrid_palette[2];


void Apps::MainMenu::InitCPPalette() {
	System::MemoryFill(&exit_but_cfg, sizeof(OpenGL::Button::Config), 0);

	exit_but_cfg.header.flags = OpenGL::Button::Header::IconPresent;
	exit_but_cfg.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	exit_but_cfg.header.icon_box[0] = 0.0f; exit_but_cfg.header.icon_box[1] = 0.0f; exit_but_cfg.header.icon_box[2] = 64.0f; exit_but_cfg.header.icon_box[3] = 64.0f;

	exit_but_cfg.palette.ico_xcol[0] = 0x3190; exit_but_cfg.palette.ico_xcol[1] = 0x7F0F; exit_but_cfg.palette.ico_xcol[2] = 0; // 0x73DF
	OpenGL::Core::ColorConvert(exit_but_cfg.palette.i_color, 0x01000000); // 0x01A8E0FF
	OpenGL::Core::ColorConvert(exit_but_cfg.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(exit_but_cfg.palette.di_color, 0xFFA8E0FF);



	System::MemoryFill(switch_but_cfg, 2*sizeof(OpenGL::Button::Config), 0);

	switch_but_cfg[0].header.flags = switch_but_cfg[1].header.flags = OpenGL::Button::Header::TypeSwitch;
	switch_but_cfg[0].header.tline_count = switch_but_cfg[1].header.tline_count = 1;

	OpenGL::Core::ColorConvert(switch_but_cfg[0].palette.color, 0xFFA0C0FF); // 0x01A8E0FF
	OpenGL::Core::ColorConvert(switch_but_cfg[0].palette.h_color, 0xFF0000FF);
	OpenGL::Core::ColorConvert(switch_but_cfg[0].palette.d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(switch_but_cfg[1].palette.color, 0xFFC0FFA0); // 0x01A8E0FF
	OpenGL::Core::ColorConvert(switch_but_cfg[1].palette.h_color, 0xFF00FF00); // FFE0A0
	OpenGL::Core::ColorConvert(switch_but_cfg[1].palette.d_color, 0xFFA8E0FF);





	switch_but_cfg[0].la_cfg[0].font_id = -1;
	switch_but_cfg[0].la_cfg[0].font_style = OpenGL::Text::Regular;
	switch_but_cfg[0].la_cfg[0].a_type = OpenGL::LeftBottomFront;
	switch_but_cfg[0].la_cfg[0].f_size = 20.5f;
	switch_but_cfg[0].la_cfg[0].max_char_count = 8;
	
	OpenGL::Core::ColorConvert(switch_but_cfg[0].la_cfg[0].c_color, 0xFF000040); // 0xFFAAEECC
	OpenGL::Core::ColorConvert(switch_but_cfg[0].la_cfg[0].hc_color, 0xFF400000);
	OpenGL::Core::ColorConvert(switch_but_cfg[0].la_cfg[0].dc_color, 0xFFE0FFFF);

	switch_but_cfg[1].la_cfg[0].font_id = -1;
	switch_but_cfg[1].la_cfg[0].font_style = OpenGL::Text::Regular;
	switch_but_cfg[1].la_cfg[0].a_type = OpenGL::LeftBottomFront;
	switch_but_cfg[1].la_cfg[0].f_size = 20.5f;
	switch_but_cfg[1].la_cfg[0].max_char_count = 8;
	
	OpenGL::Core::ColorConvert(switch_but_cfg[1].la_cfg[0].c_color, 0xFF004000); // 0xFFAAEECC
	OpenGL::Core::ColorConvert(switch_but_cfg[1].la_cfg[0].hc_color, 0xFF400000);
	OpenGL::Core::ColorConvert(switch_but_cfg[1].la_cfg[0].dc_color, 0xFFE0FFFF);








	System::MemoryFill(&cp_head_label, sizeof(OpenGL::Element::LabelCfg), 0);
		
	cp_head_label.font_id = text_font;
	cp_head_label.font_style = OpenGL::Text::Regular;
	cp_head_label.a_type = OpenGL::RightTopFront;
	cp_head_label.f_size = 48.5f;
	cp_head_label.max_char_count = 8;
	
	OpenGL::Core::ColorConvert(cp_head_label.c_color, 0xFFFFCC99);
	OpenGL::Core::ColorConvert(cp_head_label.hc_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_head_label.dc_color, 0xFFE0FFFF);

	
	
	
	System::MemoryFill(&cp_section_label, sizeof(OpenGL::Element::LabelCfg), 0);
	
	cp_section_label.font_id = time_font;
	cp_section_label.font_style = OpenGL::Text::Regular;
	cp_section_label.a_type = OpenGL::LeftBottomFront;
	cp_section_label.f_size = 27.5f;
	cp_section_label.max_char_count = 10;
	
	OpenGL::Core::ColorConvert(cp_section_label.c_color, 0xFF91E06C); // 0xFFAAEECC
	OpenGL::Core::ColorConvert(cp_section_label.hc_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_section_label.dc_color, 0xFFE0FFFF);
	


	System::MemoryFill(&cp_option_label, sizeof(OpenGL::Element::LabelCfg), 0);
	cp_option_label.font_id = text_font;
	cp_option_label.font_style = OpenGL::Text::Regular;
	cp_option_label.a_type = OpenGL::LeftBottomFront;
	cp_option_label.f_size = 23.5f;
	cp_option_label.max_char_count = 16;
	
	OpenGL::Core::ColorConvert(cp_option_label.c_color, 0xFFD0FFFF); // 0xFFAABBEE
	OpenGL::Core::ColorConvert(cp_option_label.hc_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_option_label.dc_color, 0xFFE0FFFF);
	
	
	
	
	System::MemoryFill(cp_switch_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);
	
	
	
	
	
	System::MemoryFill(&cp_slider_label, sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(cp_slider_label.c_color, 0xFFFFFF80);
		
	cp_slider_label.f_size = 21.0f;
	cp_slider_label.font_id = GetTimeFont(); // GetTextFont(); //
	cp_slider_label.font_style = OpenGL::Text::Mono;
	cp_slider_label.a_type = OpenGL::None;
	cp_slider_label.max_char_count = 6;
	
	
	
	
	
	System::MemoryFill(cp_lbox_labels, 4*sizeof(OpenGL::Element::LabelCfg), 0);

	OpenGL::Core::ColorConvert(cp_lbox_labels[0].c_color, 0xFFB0D0D0);
	OpenGL::Core::ColorConvert(cp_lbox_labels[0].hc_color, 0xFF200000); // 0xFFAA8000
	OpenGL::Core::ColorConvert(cp_lbox_labels[0].dc_color, 0xFFFFAABB);
	
	cp_lbox_labels[0].f_size = 18.5f;
	cp_lbox_labels[0].font_id = -1;
	cp_lbox_labels[0].font_style = OpenGL::Text::Regular;
	cp_lbox_labels[0].a_type = OpenGL::LeftBottomFront;
	cp_lbox_labels[0].max_char_count = 4;


	OpenGL::Core::ColorConvert(cp_lbox_labels[1].c_color, 0xFFFFFF80); // 0xFFAAEEBB
	OpenGL::Core::ColorConvert(cp_lbox_labels[1].hc_color, 0xFFACBBEE); // 0xFFAA8000
	OpenGL::Core::ColorConvert(cp_lbox_labels[1].dc_color, 0xFF000040);
	
	cp_lbox_labels[1].f_size = 18.5f;
	cp_lbox_labels[1].font_id = -1;
	cp_lbox_labels[1].font_style = OpenGL::Text::Regular;
	cp_lbox_labels[1].a_type = OpenGL::LeftBottomFront;
	cp_lbox_labels[1].max_char_count = 5;

	
	OpenGL::Core::ColorConvert(cp_lbox_labels[2].c_color, 0xFFFFFFFF);
	
	cp_lbox_labels[2].f_size = 18.5f;
	cp_lbox_labels[2].font_id = -1;
	cp_lbox_labels[2].font_style = OpenGL::Text::Regular;
	cp_lbox_labels[2].a_type = OpenGL::LeftBottomFront;
	cp_lbox_labels[2].max_char_count = 5;




	
	
	
	System::MemoryFill(&cp_lbgrid_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);
	

	cp_lbgrid_palette[0].ico_xcol[0] = 0x0012; cp_lbgrid_palette[0].ico_xcol[1] = 0x207F; cp_lbgrid_palette[0].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6B5F
	
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].color, 0xFF202020); // 0xFFEBB438 0xFFFFE0A0 0xFF808099 0xFFB8AEEC
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].h_color, 0xFFF0BA74);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].d_color, 0xFF303018);

	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].b_color, 0x01000000);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].hb_color, 0x01000000);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].db_color, 0x01000000);

	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].i_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[0].di_color, 0xFF000000);


	cp_lbgrid_palette[1].ico_xcol[0] = 0x0240; cp_lbgrid_palette[1].ico_xcol[1] = 0x23E3; cp_lbgrid_palette[1].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6BFA

	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].color, 0xFF402020); // 0xFF809980 0xFFBDDEA5 0xFF88A450
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].h_color, 0xFF604000);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].d_color, 0xFFBBAA80);

	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].b_color, 0xFFFFE0A0); //0xFF88BB99
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].hb_color, 0xFFACBBEE);
	OpenGL::Core::ColorConvert(cp_lbgrid_palette[1].db_color, 0xFF000080);



	System::MemoryFill(cp_lbox_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(cp_lbox_palette[0].color, 0xFFF0BA74);
	OpenGL::Core::ColorConvert(cp_lbox_palette[0].h_color, 0xFFFFE0A0);
	OpenGL::Core::ColorConvert(cp_lbox_palette[0].d_color, 0xFF400000);
	
	OpenGL::Core::ColorConvert(cp_lbox_palette[0].i_color, 0xFF202020);
	OpenGL::Core::ColorConvert(cp_lbox_palette[0].hi_color, 0xFF404040);
	OpenGL::Core::ColorConvert(cp_lbox_palette[0].di_color, 0xFF662626);
	

	cp_lbox_palette[1].ico_xcol[0] = 0x73DF; cp_lbox_palette[1].ico_xcol[1] = 0x7F0F; cp_lbox_palette[1].ico_xcol[2] = 0; // up/down
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].color, 0x010000FF);
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(cp_lbox_palette[1].b_color, 0xFFB4EB38);
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].hb_color, 0xFFB4EB38);
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].db_color, 0xFFB4EB38);

	OpenGL::Core::ColorConvert(cp_lbox_palette[1].i_color, 0x010000FF); // up/down icon color
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_lbox_palette[1].di_color, 0xFFA8E0FF);





	
	
	System::MemoryFill(cp_slider_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);
	cp_slider_palette[0].ico_xcol[0] = 0x73DF; cp_slider_palette[0].ico_xcol[1] = 0x7F0F; cp_slider_palette[0].ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(cp_slider_palette[0].color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(cp_slider_palette[0].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_slider_palette[0].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(cp_slider_palette[0].i_color, 0x00000020); // progress color
	OpenGL::Core::ColorConvert(cp_slider_palette[0].hi_color, 0x00FFFFFF);
	OpenGL::Core::ColorConvert(cp_slider_palette[0].di_color, 0x00000026);


	cp_slider_palette[0].ib_color[2] = cp_slider_palette[0].ib_color[3] = 64.0f;
	

	cp_slider_palette[1].ico_xcol[0] = 0x73DF; cp_slider_palette[1].ico_xcol[1] = 0x7F0F; cp_slider_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(cp_slider_palette[1].color, 0x010000FF); // buttons color
	OpenGL::Core::ColorConvert(cp_slider_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_slider_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(cp_slider_palette[1].b_color, 0xFFEBB438); // progress bar , 0xFFEBB438, 
	OpenGL::Core::ColorConvert(cp_slider_palette[1].hb_color, 0xFFEBB438); // progress bar high
	OpenGL::Core::ColorConvert(cp_slider_palette[1].db_color, 0xFFEBB438); // progress rect

	OpenGL::Core::ColorConvert(cp_slider_palette[1].i_color, 0x010000FF); // inc/dec icon color
	OpenGL::Core::ColorConvert(cp_slider_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(cp_slider_palette[1].di_color, 0xFFA8E0FF);
	
	


}


void Apps::MainMenu::ShowCP(OpenGL::APPlane & c_plane) {
	float box_vals[16] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

	UI_64 result(0);

	OpenGL::Element cp_el(c_plane);

	c_plane.Pop();
	c_plane.Show();
	
	c_plane.GetRightCorner(box_vals + 4);
		

	if (OpenGL::Core * core_ptr = c_plane.GetContext()) {
		core_ptr->SetFocus(c_plane);
		c_plane.SetState(OpenGL::Element::FlagLeave);

		cp_el.Down().Left();

		cp_el.Left(); // close but
		box_vals[0] = 10.0f;
		box_vals[1] = 10.0f;
		cp_el.RelocateXY(box_vals);





		cp_el.Left(); // setiings label
		cp_el.GetRightMargin(box_vals + 8);

		box_vals[0] = box_vals[6] - box_vals[10] - 80.0f;
		box_vals[1] = 50.0f;

		cp_el.RelocateXY(box_vals);


	// general label
		cp_el.Left();
		box_vals[0] = 30.0f + 50.0f;
		box_vals[1] = 100.0f;
		cp_el.RelocateXY(box_vals);



	// repeat label
		cp_el.Left();
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 140.0f;
		cp_el.RelocateXY(box_vals);


		cp_el.Left(); // audio label
		box_vals[0] = 30.0f + 50.0f;
		box_vals[1] = 200.0f + 60.0f;
		cp_el.RelocateXY(box_vals);


		cp_el.Left(); // volume label
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 240.0f + 60.0f;
		cp_el.RelocateXY(box_vals);

		cp_el.GetRightMargin(box_vals + 8);

		cp_el.Left(); // video label
		box_vals[0] = 30.0f + 50.0f;
		box_vals[1] = 350.0f + 60.0f;
		cp_el.RelocateXY(box_vals);

		cp_el.Left(); // auto play label
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 390.0f + 60.0f;
		cp_el.RelocateXY(box_vals);

		cp_el.GetRightMargin(box_vals + 12);
		if ((box_vals[12] + box_vals[14]) > (box_vals[8] + box_vals[10])) {
			box_vals[8] = box_vals[12];
			box_vals[10] = box_vals[14];
		}


		cp_el.Left(); // language label
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 450.0f + 60.0f;
		cp_el.RelocateXY(box_vals);

		cp_el.GetRightMargin(box_vals + 12);
		if ((box_vals[12] + box_vals[14]) > (box_vals[8] + box_vals[10])) {
			box_vals[8] = box_vals[12];
			box_vals[10] = box_vals[14];
		}

		cp_el.Left(); // screenshot
		box_vals[0] = 30.0f + 50.0f;
		box_vals[1] = 550.0f + 60.0f;
		cp_el.RelocateXY(box_vals);


		cp_el.Left(); // type
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 590.0f + 60.0f;
		cp_el.RelocateXY(box_vals);


		cp_el.Left(); // quality
		box_vals[0] = 80.0f + 50.0f;
		box_vals[1] = 650.0f + 60.0f;
		cp_el.RelocateXY(box_vals);



// repeat button
		cp_el.Left();
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f + 64.0f;
		box_vals[1] = 115.0f;		
		cp_el.RelocateXY(box_vals);

		cp_el.Left(); // volume slider
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f;
		box_vals[1] = 195.0f + 60.0f;
		box_vals[2] = box_vals[6] - box_vals[0] - 120.0f;

		reinterpret_cast<OpenGL::SliderX &>(cp_el).Relocate(box_vals);

		
		cp_el.Left(); // autostart button
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f + 64.0f;
		box_vals[1] = 365.0f + 60.0f;
		cp_el.RelocateXY(box_vals);


		cp_el.Left(); // language list
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f;
		box_vals[1] = 415.0f + 60.0f;
		
		result = reinterpret_cast<OpenGL::ListBoxX &>(cp_el).Relocate(box_vals, Media::Source::GetLNGIDCount());




		cp_el.Left(); // img type list
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f;
		box_vals[1] = 555.0f + 60.0f;
		
		result = reinterpret_cast<OpenGL::ListBoxX &>(cp_el).Relocate(box_vals, Pics::Image::GetIMGTCount());


		cp_el.Left(); // quality slider
		box_vals[0] = box_vals[8] + box_vals[10] + 80.0f;
		box_vals[1] = 605.0f + 60.0f;
		box_vals[2] = box_vals[6] - box_vals[0] - 120.0f;

		reinterpret_cast<OpenGL::SliderX &>(cp_el).Relocate(box_vals);



		if (result) {
			core_ptr->Update();
		} else {
			core_ptr->ResetElement(c_plane);
		}		
	}
}


UI_64 Apps::MainMenu::FormatCP() {
	wchar_t temp_str[64];
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	UI_64 result(-1), _event(0);
	SFSco::Object temp_obj;
	
	OpenGL::Element cp_el(control_plane);
	OpenGL::SliderX x_slider;
	OpenGL::ListBoxX x_box;
	OpenGL::Button e_button;

	cp_el.Down().Left();


	if (OpenGL::Core * core_ptr = control_plane.GetContext()) {

		control_plane.Configure(OpenGL::APPlane::NoCP, 0xB0000000, 255.0f);

		_event = SFSco::Event_CreateDrain(HideInfo, &control_plane, 0);
		SFSco::Event_SetGate(_event, control_plane, (UI_64)OpenGL::Element::FlagLostFocus | OpenGL::Element::FlagLeave);
		

		result = core_ptr->CreateButton(e_button, 64.0f, 64.0f, &control_plane);

		if (result != -1) {
			exit_but_cfg.header.icon_tid = core_ogl->GetSystemTID();
			exit_but_cfg.palette.ref_vals[1] = core_ogl->GetCloseTID();
			e_button.Configure(exit_but_cfg.header, exit_but_cfg.palette);

			reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &exit_but_cfg;
			e_button.SetDefaultHandlers(temp_obj);

			_event = SFSco::Event_CreateDrain(HideInfo, &control_plane, 0);
			SFSco::Event_SetGate(_event, e_button, (UI_64)OpenGL::Element::FlagMouseUp);
			e_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);
			
			cp_el.Left();
		}



	// create setting label
		if (result != -1)
		if (control_plane.AddLabel(cp_head_label) != -1) {
			result = 0;
			cp_el.Left();
			cp_el.SetAnchorMode(OpenGL::RightBottomFront);
			OpenGL::Core::GetHeaderString(3, temp_str);
			reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"settings"
		
	// may use some line separators or border elements

	// create general label
			result = control_plane.AddLabel(cp_section_label);
			if (result != -1) {
				cp_el.Left();
				OpenGL::Core::GetHeaderString(31, temp_str);
				reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"general"

				result = control_plane.AddLabel(cp_option_label);
				if (result != -1) {
					cp_el.Left();
					OpenGL::Core::GetHeaderString(32, temp_str);
					reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"repeat"

				}
			}


	// create audio label
			if (result != -1) {
				result = control_plane.AddLabel(cp_section_label);
				if (result != -1) {
					cp_el.Left();
					OpenGL::Core::GetHeaderString(4, temp_str);
					reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"audio"

					result = control_plane.AddLabel(cp_option_label);
					if (result != -1) {
						cp_el.Left();
						OpenGL::Core::GetHeaderString(5, temp_str);
						reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"default volume"

					}
	// aac label
		// coding quality

				}
			}


			if (result != -1) {				
				result = control_plane.AddLabel(cp_section_label);

				if (result != -1) {
					cp_el.Left();
					OpenGL::Core::GetHeaderString(6, temp_str);
					reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"video"

					result = control_plane.AddLabel(cp_option_label);
					if (result != -1) {
						cp_el.Left();
						OpenGL::Core::GetHeaderString(7, temp_str);
						reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"play on load"


						result = control_plane.AddLabel(cp_option_label);
						if (result != -1) {
							cp_el.Left();
							OpenGL::Core::GetHeaderString(8, temp_str);
							reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"default language"



	// h264 label
		// coding quality


						}
					}
				}
			}


			if (result != -1) { // screenshot
				result = control_plane.AddLabel(cp_section_label);

				if (result != -1) {

					cp_el.Left();
					OpenGL::Core::GetHeaderString(35, temp_str);
					reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"audio"

					result = control_plane.AddLabel(cp_option_label);
					if (result != -1) {
						cp_el.Left();
						OpenGL::Core::GetHeaderString(36, temp_str);
						reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"default volume"

						result = control_plane.AddLabel(cp_option_label);
						if (result != -1) {
							cp_el.Left();
							OpenGL::Core::GetHeaderString(37, temp_str);
							reinterpret_cast<OpenGL::Text &>(cp_el).Rewrite(temp_str + 1, 0, temp_str[0]); // L"default volume"
						}
					}
				}
			}



			if (result != -1) {
				// repeat type
				result = core_ptr->CreateButton(e_button, 160.0f, 40.0f, &control_plane, sizeof(OpenGL::Button::SwitchConfig));
				if (result != -1) {

					e_button.Configure(switch_but_cfg[SFSco::program_cfg.media_repeat].header, switch_but_cfg[SFSco::program_cfg.media_repeat].palette, switch_but_cfg[SFSco::program_cfg.media_repeat].la_cfg);
					OpenGL::Core::GetHeaderString(33, temp_str);
					OpenGL::Core::GetHeaderString(34, temp_str + 32);
					e_button.SetSwitchLabels(temp_str[0], temp_str + 1, temp_str[32], temp_str + 33, SFSco::program_cfg.media_repeat); // L"file" L"folder"

					reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = switch_but_cfg;
					e_button.SetDefaultHandlers(temp_obj);

					_event = SFSco::Event_CreateDrain(SetPRepeat, &e_button, 0);
					SFSco::Event_SetGate(_event, e_button, (UI_64)OpenGL::Element::FlagMouseUp);
					e_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);				

				}



				// create audio volume slider
				if (result != -1) {

					result = core_ptr->CreateSlider(x_slider, OpenGL::SliderX::ValTypePcnt | 0x4040, &cp_slider_label, &control_plane);
					if (result != -1) {
						x_slider.Configure(CPDefaultVolume, cp_slider_palette);
						x_slider.SetAnchorMode(OpenGL::LeftBottomFront);

						x_slider.SetRange(Audio::DefaultRenderer::volume_set);
						x_slider.SetRange(0, 0, SFSco::program_cfg.audio_default_volume);

//						x_slider.SetRange(0, 0x4000000000000000, Audio::DefaultRenderer::default_volume);
					}
				}

				if (result != -1) {
					result = core_ptr->CreateButton(e_button, 160.0f, 40.0f, &control_plane, sizeof(OpenGL::Button::SwitchConfig));

					if (result != -1) {
						e_button.Configure(switch_but_cfg[SFSco::program_cfg.media_start_on_load].header, switch_but_cfg[SFSco::program_cfg.media_start_on_load].palette, switch_but_cfg[SFSco::program_cfg.media_start_on_load].la_cfg);
						OpenGL::Core::GetHeaderString(10, temp_str);
						OpenGL::Core::GetHeaderString(9, temp_str + 32);
						e_button.SetSwitchLabels(temp_str[0], temp_str + 1, temp_str[32], temp_str + 33, SFSco::program_cfg.media_start_on_load); // L"enabled" L"disabled"

						reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = switch_but_cfg;
						e_button.SetDefaultHandlers(temp_obj);

						_event = SFSco::Event_CreateDrain(SetSON, &e_button, 0);
						SFSco::Event_SetGate(_event, e_button, (UI_64)OpenGL::Element::FlagMouseUp);
						e_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

					}
				}


				if (result != -1) {
					result = core_ptr->CreateListBox(x_box, OpenGL::ListBoxX::HorizontalStretch | 0x2840, cp_lbox_labels, &control_plane); // 

					if (result != -1) {
						x_box.Configure(cp_lbox_palette);

						some_vals[0] = 64.0f;
						some_vals[1] = 40.0f;
						some_vals[2] = 0.1f; // border offset, negative - no border
						some_vals[3] = -1.0f; // grid border

						x_box.ConfigureGrid(FormatLNGButton, SelectDLNG, some_vals, cp_lbgrid_palette, 0x80000000); //  no stretch
						x_box.ConfigureGridText(cp_lbox_labels);
					}
				}


				if (result != -1) {
					result = core_ptr->CreateListBox(x_box, OpenGL::ListBoxX::HorizontalStretch | 0x2840, cp_lbox_labels, &control_plane); // 

					if (result != -1) {
						x_box.Configure(cp_lbox_palette);

						some_vals[0] = 64.000001f;
						some_vals[1] = 40.000001f;
						some_vals[2] = 0.1000002f; // border offset, negative - no border
						some_vals[3] = -1.00001f; // grid border

						x_box.ConfigureGrid(FormatIMGButton, SelectSIMG, some_vals, cp_lbgrid_palette, 0x80000000); //  no stretch
						x_box.ConfigureGridText(cp_lbox_labels);



						result = core_ptr->CreateSlider(x_slider, OpenGL::SliderX::ValTypePcnt | 0x4040, &cp_slider_label, &control_plane);
						if (result != -1) {
							x_slider.Configure(CPIMGQuality, cp_slider_palette);
							x_slider.SetAnchorMode(OpenGL::LeftBottomFront);
							
							x_slider.SetRange(0, 0x3FF0000000000000, Pics::Image::shot_set.qd_factor);

						}
					}					
				}
			}
		}
	}

	return result;
}

UI_64 Apps::MainMenu::CPDefaultVolume(OpenGL::SliderX & x_slider, UI_64 v_val) {
	SFSco::program_cfg.audio_default_volume = v_val;
	
	return 1;
}

UI_64 Apps::MainMenu::CPIMGQuality(OpenGL::SliderX & x_slider, UI_64 v_val) {
	Pics::Image::shot_set.qd_factor = v_val;
	Pics::Image::shot_set.q_factor = reinterpret_cast<double &>(Pics::Image::shot_set.qd_factor)*100.0;
	return 1;
}


UI_64 Apps::MainMenu::SelectDLNG(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {	
	if (SFSco::program_cfg.media_program_language != s_val) {
		SFSco::program_cfg.media_program_language = s_val;

		if (OpenGL::Core * core_ptr = b_obj.GetContext()) core_ptr->ResetElement(b_obj);
	}
	return s_val;
}

UI_64 Apps::MainMenu::SelectSIMG(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {

	if (Pics::Image::shot_set.i_type != s_val) {
		Pics::Image::shot_set.i_type = s_val;

		if (OpenGL::Core * core_ptr = b_obj.GetContext()) core_ptr->ResetElement(b_obj);
	}

	return s_val;
}


UI_64 Apps::MainMenu::SetSON(OpenGL::Button & b_el) {
	SFSco::program_cfg.media_start_on_load = b_el.GetSwitchState();
	
	return 0;
}

UI_64 Apps::MainMenu::SetPRepeat(OpenGL::Button & b_el) {
	SFSco::program_cfg.media_repeat = b_el.GetSwitchState();
	
	return 0;
}


unsigned int Apps::MainMenu::FormatLNGButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
//	OpenGL::Element a_plane;
	OpenGL::Element but_el(g_but);

	OpenGL::ButtonGrid b_grid;

	unsigned int first_time(0), t_val(0), plt_idx(0);
	const wchar_t * src_ptr(0);

	b_grid(g_but).Up().Up();


	if (direction & 0x80000000) list_id--;
	else list_id++;


	// media language selector
	if (list_id == SFSco::program_cfg.media_program_language) {
		plt_idx = 1;

		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
			reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

			b_grid.Release();
		}
	}


	but_el.SetColor(cp_lbgrid_palette[plt_idx].color);
				
	but_el.Down();

	if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
		but_el.SetColor(cp_lbgrid_palette[plt_idx].b_color);

		but_el.Left();
	}

// label					
	if (src_ptr = Media::CodedSource::GetLNGIDString(list_id)) {
		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + 1, 0, src_ptr[0]);

		if (cp_lbox_labels[plt_idx].t_color[3]) but_el.SetColor(cp_lbox_labels[plt_idx].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(cp_lbox_labels[plt_idx].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = plt_idx;

			g_but.Release();
		}

		g_but.GetRightMargin(some_vals);
		but_el.GetRightMargin(some_vals + 4);

		some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
		some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);
			

		but_el.RelocateXY(some_vals);

	} else {
		list_id = -1;
	}

	
	return 1;
}


unsigned int Apps::MainMenu::FormatIMGButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {

	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
//	OpenGL::Element a_plane;
	OpenGL::Element but_el(g_but);

	OpenGL::ButtonGrid b_grid;

	unsigned int first_time(0), t_val(0), plt_idx(0);
	const wchar_t * src_ptr(0);

	b_grid(g_but).Up().Up();


	if (direction & 0x80000000) list_id--;
	else list_id++;


	// media language selector
	if (list_id == Pics::Image::shot_set.i_type) {
		plt_idx = 1;

		if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
			reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

			b_grid.Release();
		}
	}


	but_el.SetColor(cp_lbgrid_palette[plt_idx].color);
				
	but_el.Down();

	if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
		but_el.SetColor(cp_lbgrid_palette[plt_idx].b_color);

		but_el.Left();
	}

// label					
	if (src_ptr = Pics::Image::GetIMGTString(list_id)) {
		reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + 1, 0, src_ptr[0]);

		if (cp_lbox_labels[plt_idx].t_color[3]) but_el.SetColor(cp_lbox_labels[plt_idx].t_color);
		reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(cp_lbox_labels[plt_idx].c_color);

		if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
			bel_hdr->x_tra = list_id;
			bel_hdr->xc_index = plt_idx;

			g_but.Release();
		}

		g_but.GetRightMargin(some_vals);
		but_el.GetRightMargin(some_vals + 4);

		some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
		some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);
			

		but_el.RelocateXY(some_vals);

	} else {
		list_id = -1;
	}

	
	return 1;
}


