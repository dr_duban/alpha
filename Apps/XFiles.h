/*
** safe-fail file browser
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "OpenGL\OpenGL.h"

#include "CoreLibrary\SFScore.h"


namespace Apps {

	
	class XFiles: public SFSco::IGLApp {
		private:		
			typedef UI_64 (* fs_type)(OpenGL::ButtonGrid &, UI_64);

			struct XMapEntry {
				SFSco::IGLApp * app_filter;
				UI_64 _reserved;

				unsigned int x_color[4];

			};

			static struct AppRecord {
				UI_64 count_er, initiated;
				SFSco::IGLApp * app_ptr;
				fs_type f_selector;
			
			} app_record[256];

			static __declspec(align(16)) float progress_transform[2][16];

			static unsigned int ListScanUp(unsigned int &, UI_64 &);
			static unsigned int ListScanDown(unsigned int &, UI_64 &);
			static unsigned int FormatButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int);
	//		static OpenGL::Button function_but;
			static XFiles * _instance;
			static SFSco::Object extention_list, extention_map;
			static unsigned int ext_runner, ext_top, xcolor_state[4], xcolor_selector, progress_tid, preview_progress_tid;
			
			static unsigned int CalcXColor();

			static DWORD __stdcall SequenceProcessor(void *);
			static HANDLE sequence_thread;

			static UI_64 PageLeft(OpenGL::ButtonGrid &);
			static UI_64 PageRight(OpenGL::ButtonGrid &);

			static UI_64 PageSize(OpenGL::Element &);

			static UI_64 Start(OpenGL::Button &);
			static UI_64 Stop();
			static UI_64 Select(OpenGL::Button &);

			static UI_64 FileSelect(OpenGL::ButtonGrid &, UI_64);
			static UI_64 PathSelect(OpenGL::Button &);

			static void PathHoover(OpenGL::Button &);
			static void PathLeave(OpenGL::Button &);
			static void PathDown(OpenGL::Button &);

			static void TurnLeft(OpenGL::Element &);
			static void TurnRight(OpenGL::Element &);

			static void RollDown(OpenGL::Element &);
			static void RollUp(OpenGL::Element &);

			static UI_64 ItemsFoundUpdate(OpenGL::Element &);

			static void __fastcall ProgressCircle0(float * trajectory, unsigned int);
			static void __fastcall ProgressCircle1(float * trajectory, unsigned int);
						

			SFSco::Object current_path, current_files;

			OpenGL::Core * core_ogl;
			HMODULE _libin;

			OpenGL::Element progress_el;
			OpenGL::ButtonGrid file_list;
			OpenGL::ClipElement path_list, app_clip;
			
			OpenGL::Button::Header path_but_hdr;
			OpenGL::Button::PaletteEntry path_but_palette;
			OpenGL::Element::LabelCfg path_line_cfg;
			
			OpenGL::Button::PaletteEntry file_but_palette;
			
			float right_margin[4];
			unsigned int function_group_id, current_path_pos, current_path_type, page_element_count, _files_top, _files_runner, _path_top, _path_runner, texture_320, t320_runner, app_count, current_app;
			unsigned int folder_ico;
			UI_64 menu_event;

//			UI_64 GridReset();

			UI_64 PrepareServerPage();
			UI_64 PrepareSharePage();

			UI_64 PrepareLocationPage();

			UI_64 PrepareCapturePage();

			UI_64 PrepareLocalPage();
			UI_64 PrepareDirectoryPage(OpenGL::Text &);

			UI_64 InitAppGrid();

			UI_64 InitPage(int = 1);

			UI_64 CreatePath(OpenGL::Core *);

			void SetAppButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int &); // , unsigned int
			void SetFileButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int &); // , unsigned int
			unsigned int SetPathButton(OpenGL::Core *);

			virtual UI_64 ConfigurePreview(SFSco::Object & );
			virtual UI_64 CreatePreview(UI_64, SFSco::Object &, UI_64, UI_64, UI_64, unsigned int);

			virtual const wchar_t * GetSupportedList();
			virtual unsigned int GetSupportedCount();
			virtual UI_64 Test(const wchar_t *);

			virtual void * GetSelectorProc();

		public:
			enum PathType : unsigned int {PathTypeLocal = 0x00000000, PathTypeFilter = 0x10000000, PathTypeService = 0x20000000, PathTypeRemote = 0x30000000, PathTypeCapture = 0x80000000, PTMaskFull = 0xF0000000, PTMask = 0x70000000};

			static UI_64 BuildPlayList(UI_64 e_idx, SFSco::Object & p_list);
			static UI_64 SelectPLItem(UI_64 iidx, SFSco::Object & p_list);
			static UI_64 SelectFL();
			
			unsigned int GetPreviewTID();

			XFiles();
			virtual ~XFiles();

						
			virtual UI_64 Initialize(SFSco::IOpenGL **, void *, void *);
			virtual UI_64 Finalize();

			virtual void Change();
			virtual UI_64 AddFilter(SFSco::IGLApp *);

			

	};


}
