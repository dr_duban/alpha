/*
** safe-fail video player
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#include "Apps\XVideo.h"
#include "Apps\MainMenu.h"

#include "Media\Capture.h"

#include "System\SysUtils.h"
#include "Apps\XFiles.h"

#include "Pics\Image.h"

#include "Audio\AudioRenderer.h"

#include "Math\CalcSpecial.h"

#define VIDEO_SOURCE_LIST	1
#define AUDIO_SOURCE_LIST	2


__declspec(align(16)) float Apps::XVideo::progress_transform[2][16];

const wchar_t Apps::XVideo::supported_xt_list[] = {	
													L'T', L'S', L'\0', L'\0',
													L'M', L'K', L'V', L'\0',
													
													L'M', L'P', L'4', L'\0',

													L'M', L'2', L'T', L'S'

//													L'A', L'V', L'I', L'\0'

			
												};


			

Apps::XVideo::XVideo() {


}

Apps::XVideo::~XVideo() {


}


unsigned int Apps::XVideo::FormatCMButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	OpenGL::Element a_plane;
	OpenGL::Element but_el(g_but);

	OpenGL::ButtonGrid b_grid;

	unsigned int plt_idx(0);
	const wchar_t * src_ptr(0);

	b_grid(g_but).Up().Up();

	a_plane(g_but).Up().Up().Up().Up(); // scene, grid, list box, lb clip


	a_plane.Up().Up(); // control plane, app plane

	if (direction & 0x80000000) list_id--;
	else list_id++;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		OpenGL::APPlane::Header * app_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1);

		// get list parameters
		if (app_hdr->deco_rec.program) {
			if (list_id == reinterpret_cast<Media::Program *>(app_hdr->deco_rec.program)->GetCurrentCT()) {
				plt_idx = 1;

				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
					reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

					b_grid.Release();
				}
			}
		}

		but_el.SetColor(lbox_grid_palette[plt_idx].color);
				
		but_el.Down();

		if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
			but_el.SetColor(lbox_grid_palette[plt_idx].b_color);

			but_el.Left();
		}

// label					
		if (src_ptr = Media::Source::GetCTString(list_id)) {
			reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + 1, 0, src_ptr[0]);

			if (grid_labels[plt_idx].t_color[0]) but_el.SetColor(grid_labels[plt_idx].t_color);
			reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(grid_labels[plt_idx].c_color);

			if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
				bel_hdr->x_tra = list_id;
				bel_hdr->xc_index = plt_idx;

				g_but.Release();
			}

			g_but.GetRightMargin(some_vals);
			but_el.GetRightMargin(some_vals + 4);

			some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
			some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);
			

			but_el.RelocateXY(some_vals);

		} else {
			list_id = -1;
		}

		a_plane.Release();
	}
	
	return 1;
}


unsigned int Apps::XVideo::FormatLNGButton(OpenGL::Element & g_but, const OpenGL::Button::Header & but_cfg, unsigned int & list_id, unsigned int direction) {
	float some_vals[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	OpenGL::Element a_plane;
	OpenGL::Element but_el(g_but);

	OpenGL::ButtonGrid b_grid;

	unsigned int plt_idx(0);
	const wchar_t * src_ptr(0);

	b_grid(g_but).Up().Up();

	a_plane(g_but).Up().Up().Up().Up(); // scene, grid, list box, lb clip


	a_plane.Up().Up(); // control plane, app plane

	if (direction & 0x80000000) list_id--;
	else list_id++;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		OpenGL::APPlane::Header * app_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1);

		// get list parameters
		if (app_hdr->deco_rec.program) {
			if (reinterpret_cast<Media::Program *>(app_hdr->deco_rec.program)->IsAudioActive(list_id)) {
				plt_idx = 1;

				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(b_grid.Acquire())) {
					reinterpret_cast<OpenGL::ButtonGrid::Header *>(el_hdr + 1)->last_but = g_but;

					b_grid.Release();
				}
			}
		

			but_el.SetColor(lbox_grid_palette[plt_idx].color);
				
			but_el.Down();

			if (but_cfg.flags & OpenGL::Button::Header::BorderPresent) {
				but_el.SetColor(lbox_grid_palette[plt_idx].b_color);

				but_el.Left();
			}

// label					
			if (src_ptr = Media::Source::GetLNGString(reinterpret_cast<Media::Program *>(app_hdr->deco_rec.program)->GetAudioLNG(list_id), list_id)) {
				reinterpret_cast<OpenGL::Text &>(but_el).Rewrite(src_ptr + 1, 0, src_ptr[0]);

				if (grid_labels[plt_idx].t_color[0]) but_el.SetColor(grid_labels[plt_idx].t_color);
				reinterpret_cast<OpenGL::Text &>(but_el).SetTextColor(grid_labels[plt_idx].c_color);

				if (OpenGL::Element::Header * bel_hdr = reinterpret_cast<OpenGL::Element::Header *>(g_but.Acquire())) {
					bel_hdr->x_tra = list_id;
					bel_hdr->xc_index = plt_idx;

					g_but.Release();
				}

				g_but.GetRightMargin(some_vals);
				but_el.GetRightMargin(some_vals + 4);

				some_vals[0] = 0.5f*(some_vals[2] - some_vals[6]);
				some_vals[1] = 0.5f*(some_vals[3] - some_vals[7]);
			

				but_el.RelocateXY(some_vals);

			} else {
				list_id = -1;
			}
		}

		a_plane.Release();
	}
	
	return 1;
}



void Apps::XVideo::ResetCP(OpenGL::Element & a_plane) {
	UI_64 result(0);
	float some_vals[16] = {8.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	unsigned int cp_vals[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, la_count(0);

	OpenGL::Element cp_elem(a_plane);
	OpenGL::Core * core_ptr(0);
	

	cp_elem.Down().Left(); // screen collection
	cp_elem.GetScaleVector(some_vals + 12);
	cp_elem.GetRightCorner(some_vals + 4);
	cp_elem.Down();
	cp_elem.GetRightMargin(some_vals + 8); // screen
	
	cp_elem.Up();

	cp_elem.Right().Down();

	some_vals[0] = 100.0f;
	some_vals[1] = 50.0f;
	
	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		OpenGL::APPlane::Header * ap_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1);

		cp_vals[0] = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1)->deco_rec.flags;
		core_ptr = reinterpret_cast<OpenGL::Core *>(el_hdr->_context);
		
		if (ap_hdr->deco_rec.program) {
			la_count = reinterpret_cast<Media::Program *>(ap_hdr->deco_rec.program)->GetAudioSourceCount();
		}

		a_plane.Release();

		// name text
		some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 50.0f;
		some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 50.0f;
		cp_elem.RelocateXY(some_vals);
		reinterpret_cast<OpenGL::Text &>(cp_elem).SetMCCount(0.06f*some_vals[6]);

		cp_elem.Left();


//		some_vals[6] -= 2.0f*some_vals[8]*some_vals[12];
//		some_vals[7] -= 2.0f*some_vals[9]*some_vals[13];

		if (cp_vals[0] & OpenGL::DecodeRecord::FlagCapture) {
		// slider clip
			cp_elem.Hide();
			cp_elem.Left();

		// listbox clip
			cp_elem.Show();
			cp_elem.Left();

		// delete button
			cp_elem.Hide();
			cp_elem.Left();

		// rec button
			some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 75.0f;
			some_vals[1] += 150.0f;
			if ((some_vals[11]*some_vals[13]) < 280.0f) some_vals[1] += some_vals[11]*some_vals[13] - 300.0f;
			some_vals[15] = some_vals[1];

			cp_elem.Hide();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

		// pause button
			cp_elem.Hide();
			cp_elem.Left();

		// play button
			cp_elem.Hide();
			cp_elem.Left();

		// shot butoon
			some_vals[1] = some_vals[15] + 70.0f;
			cp_elem.Show();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

	


		// extra label group


		} else {
		// slider clip
			some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4];
			some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5] + 150.0f;
			some_vals[2] = some_vals[10]*some_vals[12];
			some_vals[3] = some_vals[11]*some_vals[13] + 150.0f;

			cp_elem.Show();
			reinterpret_cast<OpenGL::SliderClip &>(cp_elem).CoordsReset(some_vals);
			cp_elem.Left();

		// listbox clip
			some_vals[0] = (some_vals[8] + some_vals[10])*some_vals[12] + some_vals[4] + 10.0f;
			some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5];
			cp_elem.Show();

			cp_vals[0] = Media::Source::GetCTCount();
			cp_vals[1] = la_count;

			result = reinterpret_cast<OpenGL::ListBoxClip &>(cp_elem).CoordsReset(some_vals, cp_vals);			
			cp_elem.Left();

		// delete button
			some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4] - 75.0f;
			some_vals[1] = some_vals[9]*some_vals[13] + some_vals[5] - 20.0f;
			cp_elem.Show();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

		// rec button
			some_vals[1] += 150.0f;
			if ((some_vals[11]*some_vals[13]) < 280.0f) some_vals[1] += some_vals[11]*some_vals[13] - 300.0f;
			some_vals[15] = some_vals[1];

			cp_elem.Hide();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

		// pause button			
			some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5];
			cp_elem.Show();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

		// play button			
			some_vals[1] += 70.0f;
			
			cp_elem.Show();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();

		// shot butoon
			some_vals[1] = some_vals[15] + 70.0f;
			cp_elem.Show();
			cp_elem.RelocateXY(some_vals);
			cp_elem.Left();
						

		}
		
	// signal clip
		some_vals[0] = some_vals[8]*some_vals[12] + some_vals[4];
		some_vals[1] = (some_vals[9] + some_vals[11])*some_vals[13] + some_vals[5] + 40.0f;
	
		reinterpret_cast<OpenGL::BoardClip &>(cp_elem).CoordsReset(some_vals);

		cp_elem.Left();



		cp_elem.Up();

		


		if (core_ptr) {
			if (result) {
				core_ptr->Update();
			} else {
				core_ptr->ResetElement(cp_elem);
			}
		}
	}
	
}

void Apps::XVideo::Change() {


}


UI_64 Apps::XVideo::AddFilter(SFSco::IGLApp *) {
	return 0;
}

UI_64 Apps::XVideo::CreatePreview(UI_64 path_id, SFSco::Object & button_obj, UI_64 icon_id, UI_64 text_id, UI_64 name_id, unsigned int reset_flag) {
	UI_64 result(1);
	result <<= 63;

	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagPreview | reset_flag;

	if (path_id & result) {
		de_rec.flags |= OpenGL::DecodeRecord::FlagCapture;
		path_id &= 0xFFFFFFFF;
	} else {
		de_rec.flags |= OpenGL::DecodeRecord::FlagFile;
	}

	de_rec.fname_id = path_id;	
	de_rec.pic_id = icon_id;


	de_rec.info_text_id = text_id;
	de_rec.name_text_id = name_id;
	de_rec.program = 0;


	de_rec.pic_box[0] = 2.0f;
	de_rec.pic_box[1] = 32.0f;
	de_rec.pic_box[2] = 480.0f;
	de_rec.pic_box[3] = 320.0f;
		
	de_rec.element_obj = reinterpret_cast<OpenGL::Element &>(button_obj);

	Media::Program::Decode(de_rec);
	
	return result;
}

void * Apps::XVideo::GetSelectorProc() {
	return FileSelect;
}

UI_64 Apps::XVideo::ConfigurePreview(SFSco::Object & bgrid_el) {
	UI_64 result(0);
	float m_vals[4] = {10.0f, 10.0f, 0.0f, -1.0f};
	OpenGL::ButtonGrid prew_grid;
	unsigned int lidx(0);

	prew_grid(bgrid_el);

	prew_grid.ConfigureIconTrajectory(ProgressCircleP);

	prew_grid.SetMargin(m_vals);
	prew_grid.ConfigureCell(FileSelect, 484.0f, 376.0f, 0x80000000, 1.1f);
	prew_grid.ConfigureIcon(0x01E00002, 0x01400020, LAYERED_TEXTURE, OpenGL::LeftBottomFront);
	

	prew_grid.AddPaletteEntry(video_but_palette);

	lidx = prew_grid.ConfigureTextLine(0xFFBAEEA4, 0, 0x00100000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 290.0f, 366.0f, 13.5f, -1, L"Calibril"); //reinterpret_cast<const wchar_t *>(mmenu->GetTextFont())); // 777799

	lidx = prew_grid.ConfigureTextLine(0xFF80E0F0, 0, 0x00300000 | (OpenGL::Text::Regular << 8) | OpenGL::LeftTopFront, 25.0f, 24.0f, 15.5f, -1, L"Calibril");


	return result;
}

UI_64 Apps::XVideo::FileSelect(OpenGL::ButtonGrid & b_obj, UI_64 fl_id) {
	UI_64 result(-1), _event(0), pl_id(-1);
	OpenGL::Core * core_ogl(0);

	float color_val[4] = {0.2f, 0.2f, 0.2f, 1.0f}, box_vals[12] = {0.0, 0.0, 0.0f, 0.0f, 0.0, 0.0, 0.0, 0.0, 1.0f, 0.0, 0.0, 0.0};

	OpenGL::APPlane a_plane;
	
	OpenGL::SliderClip slider_clip;
	OpenGL::SliderX x_slider;

	OpenGL::ListBoxClip lb_clip;
	OpenGL::ListBoxX x_lb;

	OpenGL::Button ss_button;
	
	OpenGL::BoardClip signal_clip;
	OpenGL::D2Board l_board;

	SFSco::Object p_list, temp_obj;

	OpenGL::DecodeRecord de_rec;
	OpenGL::Button fun_but, _button;
	unsigned int fun_gid(-1), lidx(-1);

	if (HMODULE _libin = GetModuleHandle(STR_LSTR(SFS_LIB_QNAME))) {

		core_ogl = b_obj.GetContext();
	
	// copy folder list with filter
		pl_id = XFiles::BuildPlayList(fl_id, p_list);
	

	// create app_plane, get but id, format path string, add video queu without preview flag


		if (core_ogl) {
			result = core_ogl->CreateAPPlane(a_plane, SourceFeedBack);

			if (result != -1) {
				a_plane.SetEFlags(OpenGL::Element::FlagApp);

				de_rec.flags = OpenGL::DecodeRecord::FlagFile;
				de_rec.fname_id = pl_id;
				
				if (pl_id == -1) {
					de_rec.flags = OpenGL::DecodeRecord::FlagCapture;
					de_rec.fname_id = fl_id;

				}
						
				
				de_rec.info_text_id = 0;
				de_rec.name_text_id = 0;
				de_rec.program = 0;
		
				de_rec.pic_box[0] = 0.0f;
				de_rec.pic_box[1] = 0.0f;
				de_rec.pic_box[2] = 0.0f;
				de_rec.pic_box[3] = 0.0f;

				de_rec.element_obj = a_plane;


				a_plane.Configure(1, 0xB0000000, 255.0f, 300);
				
				box_vals[4] = 100.0f; box_vals[5] = 100.0f; box_vals[6] = 100.0f; box_vals[7] = 350.0f;
				box_vals[8] = 100.0f; box_vals[9] = 100.0f; box_vals[10] = 100.0f; box_vals[11] = 350.0f;

				a_plane.SetCPMargin(box_vals + 4);


				_event = SFSco::Event_CreateDrain(XProc, &a_plane, 0);
				SFSco::Event_SetGate(_event, a_plane, OpenGL::Element::StateNotify);

				de_rec.name_text_id = a_plane.AddControlLabel(fname_cfg);
				if (de_rec.name_text_id != -1) {

				// video position slider					
					if (a_plane.AddControl(slider_clip, 64, 64, OpenGL::SliderX::HorizontalStretch) != -1) { // C0C0 xFFE0F0FF
						result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypeTime | OpenGL::SliderX::LabelTypeSS, position_labels, 250.0f); //  != -1) { //  | OpenGL::SliderX::BorderOn
						if (result != -1) {
							x_slider.Configure(VideoPositionFunc, position_palette);
							x_slider.SetRange(0, 1111, 0);
							
							result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypePcnt | OpenGL::SliderX::LabelTypeL, &volume_labels);
							if (result != -1) {
								x_slider.Configure(VolumePositionFunc, volume_palette, core_ogl->GetVolTID());

								x_slider.SetRange(Audio::DefaultRenderer::volume_set);
								x_slider.SetRange(0, 0, SFSco::program_cfg.audio_default_volume);

								result = slider_clip.CreateSX(x_slider, OpenGL::SliderX::ValTypeDouble | OpenGL::SliderX::LabelTypeL, &zoom_labels);
								if (result != -1) {
									x_slider.Configure(FrameSizeFunc, zoom_palette, core_ogl->GetZoomTID());
									x_slider.SetLabelWidth(1, 0x40000000); // 1 digit after d point, convertion flag
									x_slider.SetRange(OpenGL::APPlane::scale_set);
									x_slider.SetRange(0, 0, 0);
									
								}
							}
						}


						if (result != -1) {
							if (a_plane.AddControl(lb_clip, 64, 64, 0) != -1) { //OpenGL::ListBoxX::HorizontalStretch

								if (de_rec.flags & OpenGL::DecodeRecord::FlagCapture) {
									// some list boxes


								} else {
									result = lb_clip.CreateLBX(x_lb, 0, &lface_labels);

									if (result != -1) {
										x_lb.Configure(colorm_palette);

										box_vals[0] = 64.0f;
										box_vals[1] = 64.0f;
										box_vals[2] = 0.1f; // border offset, negative - no border
										box_vals[3] = -1.0f; // grid border

										x_lb.ConfigureGrid(FormatCMButton, SelectCM, box_vals, lbox_grid_palette, 0x80000000); // no stretch
										x_lb.ConfigureGridText(grid_labels);
										
									}

									if (result != -1) {
										result = lb_clip.CreateLBX(x_lb, 0, &lface_labels);

										if (result != -1) {
											x_lb.Configure(colorm_palette);

											box_vals[0] = 64.00001f;
											box_vals[1] = 64.00001f;
											box_vals[2] = 0.1f; // border offset, negative - no border
											box_vals[3] = -1.0f; // grid border

											x_lb.ConfigureGrid(FormatLNGButton, SelectLNG, box_vals, lbox_grid_palette, 0x80000000); // no stretch
											x_lb.ConfigureGridText(grid_labels);
										
										}
									}

								}


								if (result != -1) {

								// delete button
									if (a_plane.AddControl(ss_button, 64, 64) != -1) {
										delete_but.header.icon_tid = core_ogl->GetSystemTID();
										delete_but.palette.ref_vals[0] = core_ogl->GetWarningTID();
										delete_but.palette.ref_vals[1] = core_ogl->GetDeleteTID();

										ss_button.Configure(delete_but.header, delete_but.palette);
										ss_button.SetEFlags(OpenGL::Element::FlagDualUp);
										ss_button.SetBlink(delete_but.header, 20, 10, 0xFF0000FF, 0xFF00000, 0x001F0000); // F3FF

										reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &delete_but;
										ss_button.SetDefaultHandlers(temp_obj);

										ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

								// rec button
										if (a_plane.AddControl(ss_button, 64, 64) != -1) {
											delete_but.header.icon_tid = core_ogl->GetSystemTID();
											delete_but.palette.ref_vals[1] = core_ogl->GetRecTID();
											ss_button.Configure(delete_but.header, delete_but.palette);
										
											ss_button.SetDefaultHandlers(temp_obj);

										// pause button
											if (a_plane.AddControl(ss_button, 64, 64) != -1) {
												control_but.header.icon_tid = core_ogl->GetSystemTID();
												control_but.palette.ref_vals[1] = core_ogl->GetPauseTID();
												ss_button.Configure(control_but.header, control_but.palette);

												reinterpret_cast<OpenGL::Button::Config **>(&temp_obj)[1] = &control_but;
												ss_button.SetDefaultHandlers(temp_obj);

												_event = SFSco::Event_CreateDrain(PauseProgram, &a_plane, 0);
												SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
												ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

											// play button
												if (a_plane.AddControl(ss_button, 64, 64) != -1) {
													control_but.header.icon_tid = core_ogl->GetSystemTID();
													control_but.palette.ref_vals[1] = core_ogl->GetPlayTID();
													ss_button.Configure(control_but.header, control_but.palette);
													
													ss_button.SetDefaultHandlers(temp_obj);

													_event = SFSco::Event_CreateDrain(PlayProgram, &a_plane, 0);
													SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
													ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

												// shot button
													if (a_plane.AddControl(ss_button, 64, 64) != -1) {
														control_but.header.icon_tid = core_ogl->GetSystemTID();
														control_but.palette.ref_vals[1] = core_ogl->GetShotTID();
														ss_button.Configure(control_but.header, control_but.palette);
													
														ss_button.SetDefaultHandlers(temp_obj);

														_event = SFSco::Event_CreateDrain(SourceShot, &a_plane, 0);
														SFSco::Event_SetGate(_event, ss_button, (UI_64)OpenGL::Element::FlagMouseUp);
														ss_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);




													// signal clip
														if (a_plane.AddControl(signal_clip, 150, 80) != -1) {
															if (signal_clip.Configure(signal_text, 3) != -1) {

																box_vals[0] = 0.0f;
																box_vals[1] = 0.5f;
																box_vals[2] = 1.0f;
																box_vals[3] = 1.0f;

																box_vals[4] = 1.2208521548040532291539494567208e-4f;
																box_vals[5] = 0.5f;
																box_vals[6] = 1.0f;
																box_vals[7] = 1.0f;
															
																for (unsigned int ci(0);ci<8;ci++) {
																	if (signal_clip.CreateD2Board(l_board, plot_cfg) != -1) {
																		l_board.SetDataScale(box_vals + 4);
																		l_board.SetDataOffset(box_vals);
																	// some configur probably

																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

				
// slide show
				result = -1;
				fun_gid = OpenGL::APPlane::AddFunction(fun_but, result, 64.00002f);

//				color_val[0] = color_val[1] = color_val[2] = 0.2f; color_val[3] = 1.0f;

				if (fun_gid != -1) {
					fun_but.SetColor(color_val);
				
					fun_but.ConfigureClipButton(0x00020000 | 14109, _libin);

					fun_but.Hide();
							
					MainMenu::SetFunctionHandlers(fun_but);


					_event = SFSco::Event_CreateDrain(&SlideShow, &a_plane, 0);
					SFSco::Event_SetGate(_event, fun_but, (UI_64)OpenGL::Element::FlagMouseUp);
					fun_but.SetEFlags((UI_64)OpenGL::Element::FlagAction);


// next track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00003f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00040000 | 14107, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&NextTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}


// prev track
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00004f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(0x00060000 | 14108, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&PrevTrack, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}

// exit
					if (OpenGL::APPlane::AddFunction(_button, fun_gid, 64.00005f) != -1) {
						_button.SetColor(color_val);
				
						_button.ConfigureClipButton(OpenGL::Button::ClipSide | 17118, _libin);

						_button.Hide();

						MainMenu::SetFunctionHandlers(_button);
							
						_event = SFSco::Event_CreateDrain(&Cancel, &a_plane, 0);
						SFSco::Event_SetGate(_event, _button, (UI_64)OpenGL::Element::FlagMouseUp);
						_button.SetEFlags((UI_64)OpenGL::Element::FlagAction);

						
					}
				}









				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->fun_but = fun_but;
					reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->play_list = p_list;
					System::MemoryCopy(&reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->deco_rec, &de_rec, sizeof(OpenGL::DecodeRecord));

					a_plane.Release();
				}


				OpenGL::APPlane::ResetFunction(fun_but);
		
				Media::Program::Decode(de_rec);

				OpenGL::APPlane::PopApp(a_plane);

				core_ogl->SetFBack(a_plane, ProgramPositionUpdate);
			}			
		}		
	}

	return result;
}

UI_64 Apps::XVideo::ProgramPositionUpdate(OpenGL::Element & a_plane) {
	OpenGL::Element slider(a_plane);
	
	slider.Down().Down().Left().Down();
	reinterpret_cast<OpenGL::SliderX &>(slider).SetValue(a_plane.GetXTra());

	return 0;
}

UI_64 Apps::XVideo::SourceFeedBack(OpenGL::Element & screen) {
	OpenGL::Element some_l(screen);
	
// maybe some switch
	some_l.Up().Up().Down().Down().Left().Down().Left().Left();
	reinterpret_cast<OpenGL::SliderX &>(some_l).SetRange(0, 0, screen.GetXTra());


	return 0;
}



UI_64 Apps::XVideo::PauseProgram(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagStop;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);


	return 0;
}

UI_64 Apps::XVideo::PlayProgram(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagStart;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);


	return 0;
}


UI_64 Apps::XVideo::NextTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagNextTrack;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);
		
	return 0;
}

UI_64 Apps::XVideo::PrevTrack(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagPrevTrack;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);

	return 0;
}

UI_64 Apps::XVideo::SlideShow(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

// some a_plane adjustment please do


	de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);
	
	return 0;
}

UI_64 Apps::XVideo::Cancel(OpenGL::APPlane & a_plane) {
	OpenGL::DecodeRecord de_rec;

	de_rec.flags = OpenGL::DecodeRecord::FlagDestroy;
	de_rec.element_obj = a_plane;
	
	Media::Program::Decode(de_rec);

	XFiles::SelectFL();

	return 0;
}

const wchar_t * Apps::XVideo::GetSupportedList() {
	return supported_xt_list;
}

unsigned int Apps::XVideo::GetSupportedCount() {
	return (sizeof(supported_xt_list)>>3);
}

UI_64 Apps::XVideo::Test(const wchar_t * sext) {
	return (UI_64)Strings::SearchBlock(supported_xt_list, 4*4*sizeof(wchar_t), sext, 4*sizeof(wchar_t));
}


UI_64 Apps::XVideo::FrameSizeFunc(OpenGL::SliderX & v_sli, UI_64 t_val) {
	UI_64 result(0);

	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagSize;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec._reserved[4] = t_val;

	select_rec.element_obj(v_sli).Up().Up().Up(); // app plane


	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(select_rec.element_obj.Acquire())) {
		result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_sel;

		select_rec.element_obj.Release();
	}
		
	select_rec.element_obj.Down().Left().Down();

	for (;result;result--) {
		select_rec.element_obj.Left();
	}

	Media::Program::Decode(select_rec);

	return 0;
}


UI_64 Apps::XVideo::VideoPositionFunc(OpenGL::SliderX & v_sli, UI_64 t_val) {	
	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagJump;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec.target_time = t_val;

	select_rec.element_obj(v_sli).Up().Up().Up(); // app plane

	Media::Program::Decode(select_rec);

	return 0;
}

UI_64 Apps::XVideo::VolumePositionFunc(OpenGL::SliderX & a_sli, UI_64 t_val) {
	OpenGL::DecodeRecord select_rec;
	
	select_rec.flags = OpenGL::DecodeRecord::FlagMasterVolume;
	select_rec.fname_id = -1;
	select_rec.pic_id = -1;

	select_rec.target_time = t_val;

	select_rec.element_obj(a_sli).Up().Up().Up(); // app plane

	Media::Program::Decode(select_rec);


	return 1;
}


UI_64 Apps::XVideo::SelectCM(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {	
	OpenGL::Element a_plane(b_obj);

	a_plane.Up().Up().Up().Up(); // l box, lb clip, scene, plane

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		OpenGL::APPlane::Header * ap_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1);

		if (ap_hdr->deco_rec.program) {
			s_val = reinterpret_cast<Media::Program *>(ap_hdr->deco_rec.program)->SetCurrentCT(s_val);

		}

		a_plane.Release();
	}
	
	return s_val;
}

UI_64 Apps::XVideo::SelectLNG(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {	
	OpenGL::Element a_plane(b_obj);

	a_plane.Up().Up().Up().Up(); // l box, lb clip, scene, plane

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		OpenGL::APPlane::Header * ap_hdr = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr + 1);

		if (ap_hdr->deco_rec.program) {
			s_val = reinterpret_cast<Media::Program *>(ap_hdr->deco_rec.program)->EnableAudioSource(s_val, a_plane);
			
		}

		a_plane.Release();
	}
	
	return s_val;
}


UI_64 Apps::XVideo::SelectVideoSource(OpenGL::ButtonGrid & b_obj, UI_64 s_val) {
	OpenGL::Element a_plane(b_obj);

	
	return s_val;

}

UI_64 Apps::XVideo::SourceShot(OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->pi_sel;

		a_plane.Release();
	}

	de_rec.element_obj = a_plane;
	de_rec.element_obj.Down().Left().Down();

	de_rec.flags = OpenGL::DecodeRecord::FlagTakeAShot;

	for (;result;result--) {
		de_rec.element_obj.Left();
	}

	Media::Program::Decode(de_rec);

	return result;
}

UI_64 Apps::XVideo::XProc(OpenGL::APPlane & a_plane) {
	UI_64 result(0);
	OpenGL::DecodeRecord de_rec;
	OpenGL::Element control_plane(a_plane);

	control_plane.Down();

	de_rec.element_obj = a_plane;
	

	if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
		result = (el_hdr->config.anchor >> 16);
		el_hdr->config.anchor &= 0x0000FFFF;

		a_plane.Release();



		switch (result) {
			case 1:
				de_rec.flags = OpenGL::DecodeRecord::FlagPlayAll;
			break;
			case 2:
				de_rec.flags = OpenGL::DecodeRecord::FlagReset;
				
				if (control_plane.IsVisible()) {
					control_plane.Show();
					control_plane.SetState((UI_64)OpenGL::Element::StateShow);
				}

				// go for slider update
				result = 0;
				if (OpenGL::Element::Header * el_hdr = reinterpret_cast<OpenGL::Element::Header *>(a_plane.Acquire())) {
					result = reinterpret_cast<OpenGL::APPlane::Header *>(el_hdr+1)->deco_rec.target_time;

					a_plane.Release();
				}

				if (result) {
					control_plane.Down().Left().Down();
					reinterpret_cast<OpenGL::SliderX &>(control_plane).SetRange(0, result, 0);
				
					control_plane.Left();
					reinterpret_cast<OpenGL::SliderX &>(control_plane).SetRange(0, 0, SFSco::program_cfg.audio_default_volume);
				}

			break;
			case 3: // cp show
				ResetCP(a_plane);
			break;
		}		
	}

	

	if (de_rec.flags) Media::Program::Decode(de_rec);

	return result;
}

void Apps::XVideo::ProgressCircleP(float * motion_t, unsigned int restart_flag) {
	motion_t[12] -= motion_t[-4];
	motion_t[13] -= motion_t[-3];

		

	if (!restart_flag) {
		Calc::MF_Multiply44(progress_transform[0], 4, motion_t, motion_t);

	// x
		progress_transform[1][4] = progress_transform[1][0]*motion_t[-8] - progress_transform[1][1]*motion_t[-7];
		progress_transform[1][5] = progress_transform[1][1]*motion_t[-8] + progress_transform[1][0]*motion_t[-7];
		motion_t[-8] = progress_transform[1][4];
		motion_t[-7] = progress_transform[1][5];



	// y
		progress_transform[1][12] = progress_transform[1][8]*motion_t[-6] - progress_transform[1][9]*motion_t[-5];
		progress_transform[1][13] = progress_transform[1][9]*motion_t[-6] + progress_transform[1][8]*motion_t[-5];
		motion_t[-6] = progress_transform[1][12];
		motion_t[-5] = progress_transform[1][13];


	} else {
		progress_transform[1][7] = System::Gen32();
		progress_transform[1][7] *= 0.8381903171539306640625e-7f;
		motion_t[-8] = Calc::FloatCos(progress_transform[1][7]);
		motion_t[-7] = Calc::FloatSin(progress_transform[1][7]);

		progress_transform[1][14] = System::Gen32();
		progress_transform[1][14] *= 0.8381903171539306640625e-7f;
		motion_t[-6] = Calc::FloatCos(progress_transform[1][14]);
		motion_t[-5] = Calc::FloatSin(progress_transform[1][14]);

		motion_t[0] = 1.0f; motion_t[1] = 0.0f; motion_t[2] = 0.0f; motion_t[3] = 0.0f;
		motion_t[4] = 0.0f; motion_t[5] = 1.0f; motion_t[6] = 0.0f; motion_t[7] = 0.0f;
		motion_t[8] = 0.0f; motion_t[9] = 0.0f; motion_t[10] = 1.0f; motion_t[11] = 0.0f;
		motion_t[12] = 0.0f; motion_t[13] = 0.0f; motion_t[14] = 0.0f; motion_t[15] = 1.0f;

	}

	motion_t[-4] = 158.0f*(1.0f + motion_t[-8]);
	motion_t[-3] = 104.0f*(1.0f + motion_t[-5]);

	motion_t[12] += motion_t[-4];
	motion_t[13] += motion_t[-3];
}

