#include "Apps\XVideo.h"

#include "System\SysUtils.h"

#include "Apps\MainMenu.h"
#include "Math\CalcSpecial.h"


OpenGL::Button::Config Apps::XVideo::control_but, Apps::XVideo::delete_but;

OpenGL::Button::PaletteEntry Apps::XVideo::video_but_palette, Apps::XVideo::position_palette[2], Apps::XVideo::volume_palette[2], Apps::XVideo::zoom_palette[2], Apps::XVideo::colorm_palette[2], Apps::XVideo::lbox_grid_palette[2];

OpenGL::Element::LabelCfg Apps::XVideo::volume_labels, Apps::XVideo::zoom_labels, Apps::XVideo::position_labels[2], Apps::XVideo::fname_cfg, Apps::XVideo::lface_labels, Apps::XVideo::grid_labels[4], Apps::XVideo::signal_text[4];

OpenGL::D2Board::PlotCfg Apps::XVideo::plot_cfg;



UI_64 Apps::XVideo::Initialize(SFSco::IOpenGL ** gl_ptr, void *, void *) {
	UI_64 result(0);
	OpenGL::Core * core_ptr(0);

	if (gl_ptr) core_ptr = reinterpret_cast<OpenGL::Core *>(gl_ptr[0]);

	System::MemoryFill(&video_but_palette, sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(video_but_palette.color, 0xFF262626);
	OpenGL::Core::ColorConvert(video_but_palette.h_color, 0xFF101010);
	OpenGL::Core::ColorConvert(video_but_palette.d_color, 0xFF262626);

	OpenGL::Core::ColorConvert(video_but_palette.b_color, 0x05FFFFFF);
	OpenGL::Core::ColorConvert(video_but_palette.hb_color, 0x809DCCEF);
	OpenGL::Core::ColorConvert(video_but_palette.db_color, 0x05FFFFFF);


	System::MemoryFill(position_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(position_palette[0].color, 0xFF000000);
	OpenGL::Core::ColorConvert(position_palette[0].h_color, 0xFF202020);
	OpenGL::Core::ColorConvert(position_palette[0].d_color, 0xFF662626);
	
	OpenGL::Core::ColorConvert(position_palette[0].i_color, 0xFF202020); // progress color
	OpenGL::Core::ColorConvert(position_palette[0].hi_color, 0xFF404040);
	OpenGL::Core::ColorConvert(position_palette[0].di_color, 0xFF662626);
	

	position_palette[1].ico_xcol[0] = 0x73DF; position_palette[1].ico_xcol[1] = 0x7F0F; position_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(position_palette[1].color, 0x010000FF);
	OpenGL::Core::ColorConvert(position_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(position_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(position_palette[1].b_color, 0xFFB4EB38); // progress bar
	OpenGL::Core::ColorConvert(position_palette[1].hb_color, 0xFFB4EB38); // progress bar high
	OpenGL::Core::ColorConvert(position_palette[1].db_color, 0xFFB4EB38); // progress rect

	OpenGL::Core::ColorConvert(position_palette[1].i_color, 0xFF000000); // inc/dec icon color
	OpenGL::Core::ColorConvert(position_palette[1].hi_color, 0xFF202020);
	OpenGL::Core::ColorConvert(position_palette[1].di_color, 0xFFA8FFE0);





	System::MemoryFill(volume_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	volume_palette[0].ico_xcol[0] = 0x73DF; volume_palette[0].ico_xcol[1] = 0x7F0F; volume_palette[0].ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(volume_palette[0].color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(volume_palette[0].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[0].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(volume_palette[0].i_color, 0x00000020); // progress color
	OpenGL::Core::ColorConvert(volume_palette[0].hi_color, 0x00FFFFFF);
	OpenGL::Core::ColorConvert(volume_palette[0].di_color, 0x00000026);


	volume_palette[0].ib_color[2] = volume_palette[0].ib_color[3] = 64.0f;
	

	volume_palette[1].ico_xcol[0] = 0x73DF; volume_palette[1].ico_xcol[1] = 0x7F0F; volume_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(volume_palette[1].color, 0x010000FF); // buttons color
	OpenGL::Core::ColorConvert(volume_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(volume_palette[1].b_color, 0xFFEBB438); // progress bar , 0xFFEBB438, 
	OpenGL::Core::ColorConvert(volume_palette[1].hb_color, 0xFFEBB438); // progress bar high
	OpenGL::Core::ColorConvert(volume_palette[1].db_color, 0xFFEBB438); // progress rect

	OpenGL::Core::ColorConvert(volume_palette[1].i_color, 0x010000FF); // inc/dec icon color
	OpenGL::Core::ColorConvert(volume_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(volume_palette[1].di_color, 0xFFA8E0FF);




	System::MemoryFill(zoom_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	zoom_palette[0].ico_xcol[0] = 0x73DF; zoom_palette[0].ico_xcol[1] = 0x7F0F; zoom_palette[0].ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(zoom_palette[0].color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(zoom_palette[0].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[0].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(zoom_palette[0].i_color, 0x00000020); // progress color
	OpenGL::Core::ColorConvert(zoom_palette[0].hi_color, 0x00FFFFFF);
	OpenGL::Core::ColorConvert(zoom_palette[0].di_color, 0x00000026);


	zoom_palette[0].ib_color[2] = zoom_palette[0].ib_color[3] = 64.0f;
	

	zoom_palette[1].ico_xcol[0] = 0x73DF; zoom_palette[1].ico_xcol[1] = 0x3F9F; zoom_palette[1].ico_xcol[2] = 0; // inc/dec
	OpenGL::Core::ColorConvert(zoom_palette[1].color, 0x010000FF); // buttons color
	OpenGL::Core::ColorConvert(zoom_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(zoom_palette[1].b_color, 0xFF38B4EB); // progress bar , 0xFFEBB438, 
	OpenGL::Core::ColorConvert(zoom_palette[1].hb_color, 0xFF38B4EB); // progress bar high
	OpenGL::Core::ColorConvert(zoom_palette[1].db_color, 0xFF38B4EB); // progress rect

	OpenGL::Core::ColorConvert(zoom_palette[1].i_color, 0x010000FF); // inc/dec icon color
	OpenGL::Core::ColorConvert(zoom_palette[1].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(zoom_palette[1].di_color, 0xFFFFE0A8);



	System::MemoryFill(colorm_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);

	OpenGL::Core::ColorConvert(colorm_palette[0].color, 0xFFF0BA74);
	OpenGL::Core::ColorConvert(colorm_palette[0].h_color, 0xFFFFE0A0);
	OpenGL::Core::ColorConvert(colorm_palette[0].d_color, 0xFF400000);
	
	OpenGL::Core::ColorConvert(colorm_palette[0].i_color, 0xFF202020);
	OpenGL::Core::ColorConvert(colorm_palette[0].hi_color, 0xFF404040);
	OpenGL::Core::ColorConvert(colorm_palette[0].di_color, 0xFF662626);
	

	colorm_palette[1].ico_xcol[0] = 0x73DF; colorm_palette[1].ico_xcol[1] = 0x7F0F; colorm_palette[1].ico_xcol[2] = 0; // up/down
	OpenGL::Core::ColorConvert(colorm_palette[1].color, 0x010000FF);
	OpenGL::Core::ColorConvert(colorm_palette[1].h_color, 0xFF000000);
	OpenGL::Core::ColorConvert(colorm_palette[1].d_color, 0xFFA8E0FF);

	OpenGL::Core::ColorConvert(colorm_palette[1].b_color, 0xFFB4EB38);
	OpenGL::Core::ColorConvert(colorm_palette[1].hb_color, 0xFFB4EB38);
	OpenGL::Core::ColorConvert(colorm_palette[1].db_color, 0xFFB4EB38);

	OpenGL::Core::ColorConvert(colorm_palette[1].i_color, 0xFF000000); // up/down icon color
	OpenGL::Core::ColorConvert(colorm_palette[1].hi_color, 0xFF202020);
	OpenGL::Core::ColorConvert(colorm_palette[1].di_color, 0xFFA8E0FF);







	System::MemoryFill(&control_but, sizeof(OpenGL::Button::Config), 0);
	control_but.header.flags = OpenGL::Button::Header::IconPresent;
	control_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	control_but.header.icon_box[0] = 0.0f; control_but.header.icon_box[1] = 0.0f; control_but.header.icon_box[2] = 64.0f; control_but.header.icon_box[3] = 64.0f;

	control_but.palette.ico_xcol[0] = 0x73DF; control_but.palette.ico_xcol[1] = 0x7F0F; control_but.palette.ico_xcol[2] = 0;
	OpenGL::Core::ColorConvert(control_but.palette.i_color, 0x01A8E0FF);
	OpenGL::Core::ColorConvert(control_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(control_but.palette.di_color, 0xFFA8E0FF);
	


	System::MemoryFill(&delete_but, sizeof(OpenGL::Button::Config), 0);	
	delete_but.header.flags = OpenGL::Button::Header::IconPresent;
	delete_but.header.icon_flags = LAYERED_TEXTURE | OpenGL::Element::FlagNegTex;
	delete_but.header.icon_box[0] = 0.0f; delete_but.header.icon_box[1] = 0.0f; delete_but.header.icon_box[2] = 64.0f; delete_but.header.icon_box[3] = 64.0f;
	
	delete_but.palette.ico_xcol[0] = 0x0014; delete_but.palette.ico_xcol[1] = 0x001F; delete_but.palette.ico_xcol[2] = 0x73DF;
	OpenGL::Core::ColorConvert(delete_but.palette.i_color, 0x010000FF);
	OpenGL::Core::ColorConvert(delete_but.palette.hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(delete_but.palette.di_color, 0xFF0000FF);






	System::MemoryFill(position_labels, 2*sizeof(OpenGL::Element::LabelCfg), 0);

	OpenGL::Core::ColorConvert(position_labels[0].c_color, 0xFF80FF80);
		
	position_labels[0].f_size = 15.0f;
	position_labels[0].font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	position_labels[0].font_style = OpenGL::Text::Mono;
	position_labels[0].a_type = OpenGL::None;
	position_labels[0].max_char_count = 16;


	OpenGL::Core::ColorConvert(position_labels[1].c_color, 0xFF8080FF);
		
	position_labels[1].f_size = 11.5f;
	position_labels[1].font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	position_labels[1].font_style = OpenGL::Text::Mono;
	position_labels[1].a_type = OpenGL::None;
	position_labels[1].max_char_count = 16;


	System::MemoryFill(&volume_labels, sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(volume_labels.c_color, 0xFFFFFF80);
		
	volume_labels.f_size = 21.0f;
	volume_labels.font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
	volume_labels.font_style = OpenGL::Text::Mono;
	volume_labels.a_type = OpenGL::None;
	volume_labels.max_char_count = 6;

	System::MemoryFill(&zoom_labels, sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(zoom_labels.c_color, 0xFF80FFFF);
		
	zoom_labels.f_size = 21.0f;
	zoom_labels.font_id = MainMenu::GetTimeFont(); // GetTextFont(); //
//	zoom_labels.font_style = OpenGL::Text::Mono;
	zoom_labels.a_type = OpenGL::None;
	zoom_labels.max_char_count = 6;



	System::MemoryFill(&lface_labels, sizeof(OpenGL::Element::LabelCfg), 0);

	OpenGL::Core::ColorConvert(lface_labels.c_color, 0xFF800000);
	OpenGL::Core::ColorConvert(lface_labels.hc_color, 0xFF000000);
	OpenGL::Core::ColorConvert(lface_labels.dc_color, 0xFFFFEBC8);

	lface_labels.f_size = 18.5f;
	lface_labels.font_id = -1;
	lface_labels.font_style = OpenGL::Text::Regular;
	lface_labels.a_type = OpenGL::LeftBottomFront;
	lface_labels.max_char_count = 5;







	System::MemoryFill(lbox_grid_palette, 2*sizeof(OpenGL::Button::PaletteEntry), 0);


	lbox_grid_palette[0].ico_xcol[0] = 0x0012; lbox_grid_palette[0].ico_xcol[1] = 0x207F; lbox_grid_palette[0].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6B5F
	
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].color, 0xFFFFE0A0); // 0xFF808099 0xFFB8AEEC
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].h_color, 0xFFF0BA74);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].d_color, 0xFF303018);

	OpenGL::Core::ColorConvert(lbox_grid_palette[0].b_color, 0x01000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].hb_color, 0x01000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].db_color, 0x01000000);

	OpenGL::Core::ColorConvert(lbox_grid_palette[0].i_color, 0xFF000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].hi_color, 0xFF000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].di_color, 0xFF000000);


	lbox_grid_palette[1].ico_xcol[0] = 0x0240; lbox_grid_palette[1].ico_xcol[1] = 0x23E3; lbox_grid_palette[1].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6BFA

	OpenGL::Core::ColorConvert(lbox_grid_palette[1].color, 0xFF400000); // 0xFF809980 0xFFBDDEA5 0xFF88A450
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].h_color, 0xFF604000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].d_color, 0xFFBBAA80);

	OpenGL::Core::ColorConvert(lbox_grid_palette[1].b_color, 0xFF88BB99);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].hb_color, 0xFFACBBEE);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].db_color, 0xFF000080);


/*
	lbox_grid_palette[0].ico_xcol[0] = 0x0012; lbox_grid_palette[0].ico_xcol[1] = 0x207F; lbox_grid_palette[0].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6B5F

	OpenGL::Core::ColorConvert(lbox_grid_palette[0].color, 0xFFACBBEE); // 0xFF808099 0xFFB8AEEC
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].h_color, 0xFFE6FFFF);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].d_color, 0xFF181818);

	OpenGL::Core::ColorConvert(lbox_grid_palette[0].b_color, 0x01000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].hb_color, 0xFF000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].db_color, 0xFF808099);

	OpenGL::Core::ColorConvert(lbox_grid_palette[0].i_color, 0xFF0000FF);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].hi_color, 0xFF00FF00);
	OpenGL::Core::ColorConvert(lbox_grid_palette[0].di_color, 0xFFFF0000);


	lbox_grid_palette[1].ico_xcol[0] = 0x0240; lbox_grid_palette[1].ico_xcol[1] = 0x23E3; lbox_grid_palette[1].ico_xcol[2] = 0xFF00; // 0x0FF0; 0x207F 0x6BFA

	OpenGL::Core::ColorConvert(lbox_grid_palette[1].color, 0xFFBDDEA5); // 0xFF809980 0xFFBDDEA5 0xFF88A450
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].h_color, 0xFFE6FFFF);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].d_color, 0xFF181818);

	OpenGL::Core::ColorConvert(lbox_grid_palette[1].b_color, 0x01000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].hb_color, 0xFF000000);
	OpenGL::Core::ColorConvert(lbox_grid_palette[1].db_color, 0xFF809980);

*/

	System::MemoryFill(grid_labels, 4*sizeof(OpenGL::Element::LabelCfg), 0);


	OpenGL::Core::ColorConvert(grid_labels[0].c_color, 0xFF800000);
	OpenGL::Core::ColorConvert(grid_labels[0].hc_color, 0xFF200000); // 0xFFAA8000
	OpenGL::Core::ColorConvert(grid_labels[0].dc_color, 0xFFFFAABB);
	
	grid_labels[0].f_size = 18.5f;
	grid_labels[0].font_id = -1;
	grid_labels[0].font_style = OpenGL::Text::Regular;
	grid_labels[0].a_type = OpenGL::LeftBottomFront;
	grid_labels[0].max_char_count = 5;


	OpenGL::Core::ColorConvert(grid_labels[1].c_color, 0xFFAAEEBB);
	OpenGL::Core::ColorConvert(grid_labels[1].hc_color, 0xFFACBBEE); // 0xFFAA8000
	OpenGL::Core::ColorConvert(grid_labels[1].dc_color, 0xFF000040);
	
	grid_labels[1].f_size = 18.5f;
	grid_labels[1].font_id = -1;
	grid_labels[1].font_style = OpenGL::Text::Regular;
	grid_labels[1].a_type = OpenGL::LeftBottomFront;
	grid_labels[1].max_char_count = 5;



	OpenGL::Core::ColorConvert(grid_labels[2].c_color, 0xFFFFFFFF);
	
	grid_labels[2].f_size = 18.5f;
	grid_labels[2].font_id = -1;
	grid_labels[2].font_style = OpenGL::Text::Regular;
	grid_labels[2].a_type = OpenGL::LeftBottomFront;
	grid_labels[2].max_char_count = 5;


/*
	grid_labels[0].c_color[0] = 0.65f; grid_labels[0].c_color[1] = 1.0f; grid_labels[0].c_color[2] = 0.93f; grid_labels[0].c_color[3] = 1.0f;
	grid_labels[0].hc_color[0] = 0.65f; grid_labels[0].hc_color[1] = 1.0f; grid_labels[0].hc_color[2] = 0.93f; grid_labels[0].hc_color[3] = 1.0f;
	grid_labels[0].dc_color[0] = 0.0f; grid_labels[0].dc_color[1] = 0.5f; grid_labels[0].dc_color[2] = 0.5f; grid_labels[0].dc_color[3] = 1.0f;

	grid_labels[0].f_size = 18.5f;
	grid_labels[0].font_id = -1;
	grid_labels[0].font_style = OpenGL::Text::Regular;
	grid_labels[0].a_type = OpenGL::AnchorType::LeftBottomFront;
	grid_labels[0].max_char_count = 5;


	grid_labels[1].c_color[0] = 0.5f; grid_labels[1].c_color[1] = 0.5f; grid_labels[1].c_color[2] = 0.7f; grid_labels[1].c_color[3] = 1.0f;

	grid_labels[1].f_size = 18.5f;
	grid_labels[1].font_id = -1;
	grid_labels[1].font_style = OpenGL::Text::Regular;
	grid_labels[1].a_type = OpenGL::AnchorType::LeftBottomFront;
	grid_labels[1].max_char_count = 5;

*/


	System::MemoryFill(&fname_cfg, sizeof(OpenGL::Element::LabelCfg), 0);
	fname_cfg.offset[0] = 24.0f;
	fname_cfg.offset[1] = 35.0f;

	OpenGL::Core::ColorConvert(fname_cfg.c_color, 0xFFCAFEB4);

	fname_cfg.f_size = 25.0f;
	fname_cfg.font_id = -1;
	fname_cfg.font_style = OpenGL::Text::Regular;
	fname_cfg.a_type = OpenGL::LeftBottomFront;
	fname_cfg.max_char_count = 256;



	System::MemoryFill(signal_text, 4*sizeof(OpenGL::Element::LabelCfg), 0);
	OpenGL::Core::ColorConvert(signal_text[0].c_color, 0xFF908040);	//80FF80
	signal_text[0].f_size = 21.0f;
	signal_text[0].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[0].font_style = OpenGL::Text::Regular;
	signal_text[0].a_type = OpenGL::None;
	signal_text[0].max_char_count = 8;

	OpenGL::Core::ColorConvert(signal_text[1].c_color, 0xFF908040);	// 80FF80
	signal_text[1].f_size = 21.0f;
	signal_text[1].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[1].font_style = OpenGL::Text::Regular;
	signal_text[1].a_type = OpenGL::None;
	signal_text[1].max_char_count = 12;

	OpenGL::Core::ColorConvert(signal_text[2].c_color, 0xFF908040);	// 80FF80
	signal_text[2].f_size = 21.0f;
	signal_text[2].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[2].font_style = OpenGL::Text::Regular;
	signal_text[2].a_type = OpenGL::None;
	signal_text[2].max_char_count = 12;

	OpenGL::Core::ColorConvert(signal_text[3].c_color, 0xFF908040);	// 80FF80
	signal_text[3].f_size = 21.0f;
	signal_text[3].font_id = MainMenu::GetTextFont(); // MainMenu::GetTimeFont(); // GetTextFont(); //
	signal_text[3].font_style = OpenGL::Text::Regular;
	signal_text[3].a_type = OpenGL::None;
	signal_text[3].max_char_count = 8;




	System::MemoryFill(&plot_cfg, sizeof(OpenGL::D2Board::PlotCfg), 0);

	plot_cfg.flags = OpenGL::D2Board::FlagBorder | OpenGL::D2Board::FlagPlotTypeLine | OpenGL::D2Board::FlagCaption;
	
	OpenGL::Core::ColorConvert(plot_cfg.caption_label.c_color, 0xFFE7D272);
	plot_cfg.caption_label.offset[0] = 10.0f;
	plot_cfg.caption_label.offset[1] = 0.0f;
	plot_cfg.caption_label.f_size = 25.0f;
	plot_cfg.caption_label.font_id = MainMenu::GetTextFont(); //MainMenu::GetTimeFont(); // 
	plot_cfg.caption_label.font_style = OpenGL::Text::Regular;
	plot_cfg.caption_label.a_type = OpenGL::None;
	plot_cfg.caption_label.max_char_count = 4;


	OpenGL::Core::ColorConvert(plot_cfg.line_color, 0xFF0000FF);
	OpenGL::Core::ColorConvert(plot_cfg.border_color, 0x40E7D272);
	plot_cfg.capacity = 8192;
	
	plot_cfg.flat_colors[0] = 0xFFFF0000;
	plot_cfg.flat_colors[1] = 0xFF00FFFF;
	plot_cfg.flat_colors[2] = 0xFF000000;
	plot_cfg.flat_colors[3] = 0xFF000000;










	System::MemoryFill_SSE3(progress_transform, 2*16*sizeof(float), 0, 0);

	progress_transform[0][0] = Calc::FloatCos(7.9f);
	progress_transform[0][1] = Calc::FloatSin(7.9f);

	progress_transform[0][4] = -progress_transform[0][1];
	progress_transform[0][5] = progress_transform[0][0];

	progress_transform[0][10] = progress_transform[0][15] = 1.0f;

	progress_transform[0][12] = 20.0f*(1.0f - progress_transform[0][0] + progress_transform[0][1]);
	progress_transform[0][13] = 20.0f*(1.0f - progress_transform[0][0] - progress_transform[0][1]);



	progress_transform[1][0] = Calc::FloatCos(2.1f);
	progress_transform[1][1] = Calc::FloatSin(2.1f);

	progress_transform[1][8] = Calc::FloatCos(5.2f);
	progress_transform[1][9] = Calc::FloatSin(5.2f);



	return result;
}

UI_64 Apps::XVideo::Finalize() {
	UI_64 result(0);
	



	return result;
}


