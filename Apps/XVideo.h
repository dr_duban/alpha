/*
** safe-fail video player
** Copyright (C) 2015 f.hump, dr.duban, http://safe-fail.net
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation
**
** This program is distributed WITHOUT ANY WARRANTY. See the
** GNU General Public License for more details.
**
** Any non-GPL usage of this software or parts of this software is strictly
** forbidden.
**
**/


#pragma once

#include "OpenGL\OpenGL.h"
#include "OpenGL\Element.h"

#include "CoreLibrary\SFScore.h"


namespace Apps {

	class XVideo: public SFSco::IGLApp {
		private:
			static __declspec(align(16)) float progress_transform[2][16];

			static OpenGL::Button::Config control_but, delete_but;
			static OpenGL::Element::LabelCfg position_labels[2], volume_labels, lface_labels, zoom_labels;

			static OpenGL::Button::PaletteEntry video_but_palette, position_palette[2], volume_palette[2], zoom_palette[2], colorm_palette[2], lbox_grid_palette[2]; // , lbox_header_palette, lbox_footer_palette, lbox_grid_palette[2];
			static OpenGL::Element::LabelCfg fname_cfg, grid_labels[4], signal_text[4];

			static OpenGL::D2Board::PlotCfg plot_cfg;



			static void ResetCP(OpenGL::Element &);
			static unsigned int FormatCMButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int);
			static unsigned int FormatLNGButton(OpenGL::Element &, const OpenGL::Button::Header &, unsigned int &, unsigned int);

			static void __fastcall ProgressCircleP(float * trajectory, unsigned int);

			static UI_64 SelectVideoSource(OpenGL::ButtonGrid &, UI_64);
			static UI_64 SelectCM(OpenGL::ButtonGrid &, UI_64);
			static UI_64 SelectLNG(OpenGL::ButtonGrid &, UI_64);

			static UI_64 VideoPositionFunc(OpenGL::SliderX &, UI_64);
			static UI_64 VolumePositionFunc(OpenGL::SliderX &, UI_64);
			static UI_64 FrameSizeFunc(OpenGL::SliderX &, UI_64);

			static UI_64 SourceShot(OpenGL::APPlane &);
	
			static UI_64 FileSelect(OpenGL::ButtonGrid &, UI_64);

			static UI_64 NextTrack(OpenGL::APPlane &);
			static UI_64 PrevTrack(OpenGL::APPlane &);
			static UI_64 SlideShow(OpenGL::APPlane &);
			static UI_64 Cancel(OpenGL::APPlane &);

			static UI_64 PauseProgram(OpenGL::APPlane &);
			static UI_64 PlayProgram(OpenGL::APPlane &);

			static UI_64 Select();

			static UI_64 XProc(OpenGL::APPlane &);
			static UI_64 ProgramPositionUpdate(OpenGL::Element &);
			static UI_64 SourceFeedBack(OpenGL::Element &);

			virtual void Change();

			virtual void * GetSelectorProc();

		public:
			static const wchar_t supported_xt_list[];

			XVideo();
			virtual ~XVideo();

						
			virtual UI_64 Initialize(SFSco::IOpenGL **, void *, void *);
			virtual UI_64 Finalize();

			
			virtual UI_64 AddFilter(SFSco::IGLApp *);

			virtual UI_64 ConfigurePreview(SFSco::Object & );
			virtual UI_64 CreatePreview(UI_64 path_id, SFSco::Object & button_obj, UI_64 icon_id, UI_64 text_id, UI_64 name_id, unsigned int);

			virtual const wchar_t * GetSupportedList();
			virtual unsigned int GetSupportedCount();
			virtual UI_64 Test(const wchar_t *);
	};


}